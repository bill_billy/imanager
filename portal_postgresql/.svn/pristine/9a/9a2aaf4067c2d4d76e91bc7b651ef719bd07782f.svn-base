package com.iplanbiz.iportal.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.SecurityUtil;
import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.CustomDBDataService;

@Controller
public class CustomDBDataController {

	@Autowired
	CustomDBDataService customService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/customDBData/getCognosLoginEmpTyp", method=RequestMethod.POST)
	public void getCognosLoginEmpTyp ( HttpServletResponse response, HttpServletRequest request
			                           , @RequestParam(required=false) String empID ) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		JSONArray returnValue = new JSONArray();
		
		try {
			returnValue = customService.getCognosLoginEmpTyp(empID);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Cognos Login Emp Typ Error : {}", e);
		}
		msg.send(response);
	}
 
	@RequestMapping(value="/customDBData/getCognosAccountEmpID", method=RequestMethod.POST)
	public void getCognosAccountEmpID ( HttpServletResponse response, HttpServletRequest request
			                            , @RequestParam(required=false) String empID
			                            , @RequestParam(required=false) String rownum ) throws Exception {
		JSONObject returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = customService.getCognosAccountEmpID(empID, rownum);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Cognos Account EMP ID Error : {}", e);
		}
		msg.send(response);
	} 
	@RequestMapping(value="/customDBData/getConosAccountEmpEmail", method=RequestMethod.POST)
	public void getConosAccountEmpEmail ( HttpServletResponse response, HttpServletRequest request
			                              , @RequestParam(required=false) String empID
			                              , @RequestParam(required=false) String rownum ) throws Exception {
		JSONObject returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = customService.getConosAccountEmpEmail(empID, rownum);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Cognos Account EMP email : {}", e);
		}
		msg.send(response);
	}
	@RequestMapping(value="/customDBData/getCognosAccountUserGrpID", method=RequestMethod.POST)
	public void getCognosAccountUserGrpID ( HttpServletResponse response, HttpServletRequest request
			                                , @RequestParam(required=false) String empID
			                                , @RequestParam(required=false) String rownum ) throws Exception {
		JSONObject returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = customService.getCognosAccountUserGrpID(empID, rownum);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Cognos Account User Grp ID Error : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/customDBData/getCognosAccountRoleID", method=RequestMethod.POST)
	public void getCognosAccountRoleID ( HttpServletResponse response, HttpServletRequest request 
			                             , @RequestParam(required=false) String empID
			                             , @RequestParam(required=false) String rownum ) throws Exception {
		JSONObject returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = customService.getCognosAccountRoleID(empID, rownum);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Cognos Accoutn Role ID Error : {}", e);
		}
		msg.send(response);
	}
	@RequestMapping(value="/customDBData/getCognosAccountLocale", method=RequestMethod.POST)
	public void getCognosAccountLocale ( HttpServletResponse response, HttpServletRequest request
			                             , @RequestParam(required=false) String empID
			                             , @RequestParam(required=false) String rownum ) throws Exception {
		JSONObject returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = customService.getCognosAccountLocale(empID, rownum);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Cognos Account Locale Error : {}", e);
		}
		msg.send(response);
	}
	@RequestMapping(value="/customDBData/getCognosMemberShip", method= RequestMethod.POST)
	public void getCognosMemberShip ( HttpServletResponse response, HttpServletRequest request
			                          , @RequestParam(required=false) String empID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = customService.getCognosMemberShip(empID);
			logger.info("RETURNVALUE = " + Utils.getString(returnValue.toString(), "").replaceAll("[\r\n]",""));
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Cognos Member Ship Data Error : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/customDBData/getCognosQueryMember", method=RequestMethod.POST)
	public void getCognosQueryMember ( HttpServletResponse response, HttpServletRequest request
			                           , @RequestParam(required=false) String userGrpID 
			                           , @RequestParam(required=false, defaultValue="") String sortOrder ) throws Exception {
		JSONArray returnValue = new JSONArray();
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = customService.getCognosQueryMember(userGrpID, sortOrder);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Cognos Query Member Error : {}" ,e);
		}
		msg.send(response);
	}
	@RequestMapping(value="/customDBData/getCognosQueryMemberByRole", method=RequestMethod.POST)
	public void getCognosQueryMemberByRole ( HttpServletResponse response, HttpServletRequest request
			                                 , @RequestParam(required=false) String gID
			                                 , @RequestParam(required=false, defaultValue="") String sortOrder ) throws Exception {
		JSONArray returnValue = new JSONArray();
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = customService.getCognosQueryMemberByRole(gID, sortOrder);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Cognos Query Member By Role Data Error : {}", e);
		}
		msg.send(response);
	}
	@RequestMapping(value="/customDBData/getCognosQuery", method=RequestMethod.POST)
	public void getCognosQuery ( HttpServletResponse response, HttpServletRequest request 
			                     , @RequestParam(required=false) String sortOrder
			                     , @RequestParam(required=false) String sqlWhere ) throws Exception {
		JSONArray returnValue = new JSONArray();
		AjaxMessage msg = new AjaxMessage();
		
		try {
			sqlWhere = SecurityUtil.unEscape(sqlWhere);
			sortOrder = SecurityUtil.unEscape(sortOrder);
			logger.info("##############################");
			logger.info("sortOrder:{}",sortOrder);
			logger.info("sqlWhere:{}",sqlWhere);			
			logger.info("##############################");
			returnValue = customService.getCognosQuery(sortOrder, sqlWhere);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Cognos Query Data Error : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/customDBData/crudTest", method=RequestMethod.GET)
	public ModelAndView curdTest ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/customDBData/crudTest");
		
		return modelAndView;
	}
	@RequestMapping(value="/customDBData/getCrudTest", method=RequestMethod.POST)
	public void getCrudTest ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		JSONArray returnValue = new JSONArray();
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = customService.getCrudTest();
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get PDA Crud Test Error : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/customDBData/crudInsert", method=RequestMethod.POST)
	public void insertCrudTest ( HttpServletResponse response, HttpServletRequest request
			                     , @RequestParam(required=false) String comCod
			                     , @RequestParam(required=false) String comName ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = customService.insertCrudTest(comCod, comName);
			if(returnValue == 0) {
				msg.setSuccessText("저장되었습니다");
			} else {
				msg.setExceptionText("오류가 발생했습니다");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error > Get PDA Crud Test Error : {}" ,e);
		}
		msg.send(response);
	}
	@RequestMapping(value="/customDBData/crudDelete", method=RequestMethod.POST)
	public void deleteCrudTest ( HttpServletResponse response, HttpServletRequest request
			                     , @RequestParam(required=false) String comCod ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = customService.deleteCrudTest(comCod);
			if(returnValue == 0) {
				msg.setSuccessText("삭제 되었습니다");
			} else {
				msg.setExceptionText("오류가 발생했습니다");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get PDA Crud Test Error : {}", e);
		}
		msg.send(response);
	}
}
