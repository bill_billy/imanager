/**
 * @author Sungjin, Woo (newanto@gmail.net)
 *
 * Date : 2012. 08. 09
 * 
 * 설명 : Source 테이블에서 Target 테이블로 이동시키는 함수.
 * 사용방법 : 
 * 		moveAll("a", "b");
 * 			-> table id 가 a 인것에서 table id 가 b 인곳으로 모두 이동한다.
 * 		moveOne("a", "b");
 * 			-> table id 가 a 인 테이블에서 선택된 것들이 table b 로 이동한다.
 * 
 * 		settingTable(source, target);
 * 			-> 최초 table 에 이벤트를 넣어주는 함수. document.ready 후에 실행되어져야 한다.
 * 
 * 		getTableId("a", ":::");
 * 			-> table id : a 에 두번째 row 부터 id 값을 반환한다. ":::" 는 배열을 붙여주는 인자 이다.
 */

function moveAll(source, target){
	
	var obj = $("#"+source+" tr:gt(0)");
	
	obj.clone(true).appendTo("#"+target);
	obj.remove();
	$("#"+target+" tr").removeClass("odd");
	$("#"+target+" tr:even").addClass("odd");
}

function moveOne(source, target){
	
	var obj = $("#"+source+" tr:gt(0)").filter(".selected");
	
	obj.clone(true).appendTo("#"+target).toggleClass("selected");
	obj.remove();
	$("#"+source+" tr").removeClass("odd");
	$("#"+source+" tr:even").addClass("odd");
	$("#"+target+" tr").removeClass("odd");
	$("#"+target+" tr:even").addClass("odd");

}

function settingTable(source, target, selectOneFlag){
	$("#"+source+" tr:gt(0)").click(function(){
		if(selectOneFlag){
			delTableSelected(source);
		}
		$(this).toggleClass("selected");
	});
	
	$("#"+target+" tr:gt(0)").click(function(){
		if(selectOneFlag){
			delTableSelected(target);
		}
		$(this).toggleClass("selected");
	});
}

function delTableSelected(tableId){
	$("#"+tableId+" tr:gt(0)").each(function (i){
		$(this).removeClass("selected");
	});

}

function getTableId(tableId, glue){
	var values = new Array();
	$("#"+tableId+" tr:gt(0)").each(function (i){
		values[i] = $(this).attr("id");
	});
	if(!glue){
		glue = ","; 
	}
	return values.join(glue);
}

/**
 * @author Sungjin, Woo (newanto@gmail.net)
 *
 * Date : 2012. 08. 23
 * 
 * 설명 : Source dTree 에서 Target 테이블로 이동시키는 함수.
 * 사용방법 : 
 * 		chkboxRemoveOne("a", "b");
 * 			-> table id 가 b 인곳에 선택되어진 tr 을 제거하고 dTree id 가 a 인곳에 checked 를 false 시킨다.
 * 		chkboxAddOne("a", "b");
 * 			-> dTree id 가 a 인것을 table id 가 b 인곳에 추가 시킨다.
 * 			
 *		chkboxRemoveAll("a", "b");
 * 			-> table id 가 b 인곳의 tr 을 모두 remove 시키고 dTree a 의 모든것을 checked false 시킨다.
 * 		chkboxAddAll("a", "b");
 * 			-> dTree a 의 모든것을 checked true 시키고 table id 가 b 인곳에 모두 추가시킨다.
 */
function chkboxAddOne(treeId, tableId){
	
	$("#"+tableId+" tr:gt(0)").remove();
	$("#"+treeId+" input:checkbox:checked").each(function (){
		var checkedText = $(this).attr("pText");
		var tmp = $(this).attr("value").split("-");
		var checkedValue = tmp[0];
		
		$("#"+tableId).append("<tr id='"+checkedValue+"'><td onclick='selectedTr(this)'>"+checkedText+"</td></tr>");
	});
}


//새로 추가 2013-01-21 김윤희
//table id 가 a 인 테이블에서 체크 박스로 선택된 것들이 table b 로 이동한다.
function moveSelected(source, target){
	
	if($("#"+source+" tr:gt(0) input:checked").size() == 0){
		alert('옮길 대상이 없습니다.');
	}
	var obj = $("#"+source+" tr:gt(0) input:checked").parent().parent();
	
	obj.clone(true).appendTo("#"+target);
	obj.remove();
	$("#"+source+" tr:gt(0)").removeClass("odd");
	$("#"+source+" tr:odd").addClass("odd");
	$("#"+target+" tr:gt(0)").removeClass("odd");
	$("#"+target+" tr:odd").addClass("odd");
	
	$("#"+source+" tr input:checked").attr("checked", false);
	$("#"+target+" tr input:checked").attr("checked", false);
}

function selectedTr(obj){
	$(obj).toggleClass("selected");
}

function chkboxRemoveOne(treeId, tableId){
	
	$("#"+tableId+" .selected").each(function (){
		var removeId = $(this).attr("id"); 
		$("#"+treeId+" input:checkbox:checked").each(function (){
			if($(this).attr("value") == removeId){
				$(this).attr("checked", false);
			}
		});
		$(this).remove();
	});
}

function chkboxRemoveAll(treeId, tableId){
	$("#"+tableId+" tr:gt(0)").remove();
	 
	$("#"+treeId+" input:checkbox").each(function (){
		$(this).attr("checked", false);
	});

}

function chkboxAddAll(treeId, tableId){
	$("#"+tableId+" tr:gt(0)").remove();
	$("#"+treeId+" input:checkbox").each(function (){
		$(this).attr("checked", true);
		var checkedText = $(this).attr("pText");
		var tmp = $(this).attr("value").split("-");
		var checkedValue = tmp[0];
		
		$("#"+tableId).append("<tr id='"+checkedValue+"'><td onclick='selectedTr(this)'>"+checkedText+"</td></tr>");
	});									
}
