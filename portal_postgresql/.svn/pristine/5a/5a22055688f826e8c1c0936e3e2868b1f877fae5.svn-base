package com.iplanbiz.iportal.dao.auth;

import java.io.BufferedReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
 
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.system.CustomSqlHouseDAO;
import com.iplanbiz.iportal.dao.system.SqlHouseExecuteDAO;

import hidden.org.codehaus.plexus.interpolation.util.StringUtils;
 
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.session.LoginSessionInfo; 

@Repository("empView")
public class EmpView {
 
	@Autowired
	private DBMSDAO dbmsDao;
	
	@Autowired
	private CustomSqlHouseDAO customSqlHouseDao;	
	
	@Autowired SqlSessionFactory sqlSessionFactory;
	
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	public LinkedHashMap<String, Object> getLoginUserInfo(String userID, String acctID) throws Exception{ 
		LinkedHashMap<String,Object> param = new LinkedHashMap<String,Object>();
		param.put("S_EMP_ID", userID);
 
		param.put("S_ACCT_ID", acctID);
		List<LinkedHashMap<String, Object>> list = customSqlHouseDao.executeCustomQuery("EmpView.getLoginUserInfo", param);
		if(list.size()!=0) {
			return list.get(0);
		}
		else
			return null;
		
	}
	
	public List<LinkedHashMap<String, Object>> getList() throws Exception { 
		return this.search("","", "", loginSessionInfoFactory.getObject().getAcctID(),"");
	}
	public String decodePassword(String empID, String pwd, String acctID) throws Exception {
		 
		
		LinkedHashMap<String,Object> param = new LinkedHashMap<String,Object>();
		param.put("S_USER_ID", empID);
		param.put("S_PWD",pwd);
		param.put("S_ACCT_ID", acctID);
		List<LinkedHashMap<String,Object>> list = customSqlHouseDao.executeCustomQuery("EmpView.decodePwd", param);
		String decodedPassword = null;
		if(list.size()!=0) {
			for(int i = 0;i < list.size();i++) {
				if(list.get(i).get("PWD")!=null&&list.get(i).get("PWD").toString().equals("")==false||list.get(i).get("PWD").toString().equals(pwd)==false)
					decodedPassword = list.get(i).get("PWD").toString();
			}
			return decodedPassword;
		}		
		return null;
	}
	public List<LinkedHashMap<String, Object>> search(String empIDs,String empName, String userGroups, String acctID,String excludeUsers) throws Exception {
		if(empIDs==null)empIDs="";
		if(empName==null)empName="";
		if(userGroups==null)userGroups=""; 
		if(excludeUsers==null)excludeUsers="";
		
		LinkedHashMap<String,Object> param = new LinkedHashMap<String,Object>();
		if(empIDs.indexOf(",")==-1) {
			param.put("S_EMP_ID", empIDs);
			param.put("SI_USER_ID", "");
		}
		else {
			param.put("S_EMP_ID", "");
			param.put("SI_USER_ID", empIDs);
		}
		param.put("S_EMP_NM", empName);
		param.put("SI_USER_GRP_ID", userGroups);
		param.put("SNI_USER_ID", excludeUsers);
		param.put("S_ACCT_ID", acctID);
		return customSqlHouseDao.executeCustomQuery("EmpView", param);
	}
	
	public List<LinkedHashMap<String, Object>> getListByUserID(String userID) throws Exception {
		return this.search(userID, "", "", loginSessionInfoFactory.getObject().getAcctID(),"");
	}
	public List<LinkedHashMap<String, Object>> getListByUserID(String userID,String acctID) throws Exception {
		return this.search(userID, "", "", acctID,"");
	}
	public List<LinkedHashMap<String, Object>> getListByUserName(String userName) throws Exception {
		
		return this.search("", userName, "", loginSessionInfoFactory.getObject().getAcctID(),"");
	}
	public List<LinkedHashMap<String, Object>> getListByGroupID(String groupID) throws Exception {
		return this.search("", "", groupID, loginSessionInfoFactory.getObject().getAcctID(),"");
	}
	public List<LinkedHashMap<String, Object>> getListByUserIDList(List<String> userID) throws Exception { 
		String userIDStr = org.apache.commons.lang.StringUtils.join(userID, ",");
		return this.search(userIDStr, "", "", loginSessionInfoFactory.getObject().getAcctID(),"");
	}
	public List<LinkedHashMap<String, Object>> getListByGroupIDList(List<String> groupID) throws Exception {
		String groupStr = org.apache.commons.lang.StringUtils.join(groupID, ","); 
		return this.search("", "",groupStr, loginSessionInfoFactory.getObject().getAcctID(),"");
	}
	
	public List<LinkedHashMap<String, Object>> getListByRoleID(String roleID) throws Exception {
		TableCondition iect7010 = new TableCondition();
		iect7010.put("GID", roleID);
		List<String> userList = new ArrayList<String>();
		List<String> groupList = new ArrayList<String>();
		List<LinkedHashMap<String, Object>> list = dbmsDao.selectTable(false,WebConfig.getCustomTableName("IECT7010"), iect7010);
		for(int i = 0; i<list.size();i++) {
			String typ = list.get(i).get("TYPE").toString();
			if(typ.equals("u")) userList.add(list.get(i).get("FKEY").toString());
			else if(typ.equals("p")) groupList.add(list.get(i).get("FKEY").toString());
		}
		List<LinkedHashMap<String, Object>> data = new ArrayList<LinkedHashMap<String,Object>>();
		if(groupList.size()!=0)
			data.addAll(this.getListByGroupIDList(groupList));
		if(userList.size()!=0)
			data.addAll(this.getListByUserIDList(userList)); 
		
		return data;
	}

}
