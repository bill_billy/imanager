<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.iplanbiz.iportal.dao.MonthReportConnectionDAO" >
	<resultMap id="hashmap" type="java.util.HashMap"/>
	<select id="getMonthReportYear" parameterType="hashmap" resultType="hashmap">
		SELECT DISTINCT TO_CHAR(INSERT_TIME,'YYYY') AS YYYY_COD,TO_CHAR(INSERT_TIME,'YYYY')||'년' AS YYYY_NM
		FROM iportal_custom.IECT7017
		WHERE INSERT_TIME BETWEEN (SELECT DISTINCT MIN(INSERT_TIME) FROM iportal_custom.IECT7017) AND (SELECT DISTINCT MAX(INSERT_TIME) FROM iportal_custom.IECT7017)
		AND ACTION_TYPE_NO = '1'
		AND TARGET_TYPE_NO NOT IN ('1000','1001')
		ORDER BY 1 DESC
	</select>
	<select id="getMonthReportConnectionTotal" parameterType="hashmap" resultType="hashmap">
		SELECT SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '01' THEN 1 ELSE 0 END) AS JAN
		            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '02' THEN 1 ELSE 0 END) AS FEB
		            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '03' THEN 1 ELSE 0 END) AS MAR
		            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '04' THEN 1 ELSE 0 END) AS APR
		            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '05' THEN 1 ELSE 0 END) AS MAY
		            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '06' THEN 1 ELSE 0 END) AS JUN
		            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '07' THEN 1 ELSE 0 END) AS JUL
		            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '08' THEN 1 ELSE 0 END) AS AUG
		            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '09' THEN 1 ELSE 0 END) AS SEP
		            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '10' THEN 1 ELSE 0 END) AS OCT
		            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '11' THEN 1 ELSE 0 END) AS NOV
		            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '12' THEN 1 ELSE 0 END) AS DEC
		            ,COUNT(*) AS TOTAL
		FROM iportal_custom.IECT7017 A
		WHERE A.ACCT_ID = #{ACCT_ID}
		AND      A.ACTION_TYPE_NO = '1'
		AND      A.TARGET_TYPE_NO NOT IN ('1000','1001')
		AND      UPPER(A.USER_ID) NOT LIKE 'IPLAN_%'
		AND      UPPER(A.USER_ID) != 'ADMIN'
		AND      A.INSERT_TIME BETWEEN TO_DATE(#{YEAR}||'0101000000','YYYYMMDDHH24MISS') AND TO_DATE(#{YEAR}+1||'0101000000','YYYYMMDDHH24MISS')
	</select>
	<select id="getMonthReportConnectionList" parameterType="hashmap" resultType="hashmap">
		SELECT ROW_NUMBER() OVER() AS RNUM
		            ,A.TARGET_ID AS C_ID
		            ,A.TARGET_VALUE AS REPORT_NAME
		            ,A.TARGET_PATH AS REPORT_PATH
		            ,A.JAN
		            ,A.FEB
		            ,A.MAR
		            ,A.APR
		            ,A.MAY
		            ,A.JUN
		            ,A.JUL
		            ,A.AUG
		            ,A.SEP
		            ,A.OCT
		            ,A.NOV
		            ,A.DEC
		            ,A.TOTAL
		FROM(
			SELECT TARGET_ID
						,TARGET_VALUE
						,MAX(TARGET_PATH) AS TARGET_PATH
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '01' THEN 1 ELSE 0 END) AS JAN
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '02' THEN 1 ELSE 0 END) AS FEB
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '03' THEN 1 ELSE 0 END) AS MAR
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '04' THEN 1 ELSE 0 END) AS APR
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '05' THEN 1 ELSE 0 END) AS MAY
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '06' THEN 1 ELSE 0 END) AS JUN
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '07' THEN 1 ELSE 0 END) AS JUL
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '08' THEN 1 ELSE 0 END) AS AUG
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '09' THEN 1 ELSE 0 END) AS SEP
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '10' THEN 1 ELSE 0 END) AS OCT
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '11' THEN 1 ELSE 0 END) AS NOV
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '12' THEN 1 ELSE 0 END) AS DEC
			            ,COUNT(*) AS TOTAL
			FROM iportal_custom.IECT7017 A 
			WHERE A.ACCT_ID = #{ACCT_ID}
			AND      A.ACTION_TYPE_NO = '1'
			AND      A.TARGET_TYPE_NO NOT IN (1000, 1001)
			AND      UPPER(A.USER_ID) NOT LIKE 'IPLAN_%'
			AND      UPPER(A.USER_ID) != 'ADMIN'
			AND      A.INSERT_TIME BETWEEN TO_DATE(#{YEAR}||'0101000000','YYYYMMDDHH24MISS') AND TO_DATE(#{YEAR}+1||'0101000000','YYYYMMDDHH24MISS')
			GROUP BY A.TARGET_ID, A.TARGET_VALUE
		) A
		ORDER BY A.TOTAL DESC, A.TARGET_VALUE
	</select>
	<select id="getMonthReportConnectionListByExcel" parameterType="hashmap" resultType="hashmap">
		SELECT ROW_NUMBER() OVER() AS RNUM
		            ,A.TARGET_ID AS C_ID
		            ,A.TARGET_VALUE AS REPORT_NAME
		            ,A.TARGET_PATH AS REPORT_PATH
		            ,A.JAN
		            ,A.FEB
		            ,A.MAR
		            ,A.APR
		            ,A.MAY
		            ,A.JUN
		            ,A.JUL
		            ,A.AUG
		            ,A.SEP
		            ,A.OCT
		            ,A.NOV
		            ,A.DEC
		            ,A.TOTAL
		FROM(
			SELECT TARGET_ID
						,TARGET_VALUE
						,MAX(TARGET_PATH) AS TARGET_PATH
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '01' THEN 1 ELSE 0 END) AS JAN
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '02' THEN 1 ELSE 0 END) AS FEB
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '03' THEN 1 ELSE 0 END) AS MAR
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '04' THEN 1 ELSE 0 END) AS APR
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '05' THEN 1 ELSE 0 END) AS MAY
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '06' THEN 1 ELSE 0 END) AS JUN
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '07' THEN 1 ELSE 0 END) AS JUL
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '08' THEN 1 ELSE 0 END) AS AUG
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '09' THEN 1 ELSE 0 END) AS SEP
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '10' THEN 1 ELSE 0 END) AS OCT
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '11' THEN 1 ELSE 0 END) AS NOV
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '12' THEN 1 ELSE 0 END) AS DEC
			            ,COUNT(*) AS TOTAL
			FROM iportal_custom.IECT7017 A 
			WHERE A.ACCT_ID = #{ACCT_ID}
			AND      A.ACTION_TYPE_NO = '1'
			AND      A.TARGET_TYPE_NO NOT IN (1000, 1001)
			AND      UPPER(A.USER_ID) NOT LIKE 'IPLAN_%'
			AND      UPPER(A.USER_ID) != 'ADMIN'
			AND      A.INSERT_TIME BETWEEN TO_DATE(#{YEAR}||'0101000000','YYYYMMDDHH24MISS') AND TO_DATE(#{YEAR}+1||'0101000000','YYYYMMDDHH24MISS')
			GROUP BY A.TARGET_ID, A.TARGET_VALUE
		) A
		ORDER BY A.TOTAL DESC, A.TARGET_VALUE
	</select>
	<select id="getMonthReportConnectionListByExcelAll" parameterType="hashmap" resultType="hashmap">
		SELECT ROW_NUMBER() OVER() AS RNUM
		            ,A.TARGET_ID AS C_ID
		            ,A.TARGET_VALUE AS REPORT_NAME
		            ,A.TARGET_PATH AS REPORT_PATH
		            ,A.USER_ID AS EMP_ID
		            ,A.EMP_NM
		            ,A.JAN
		            ,A.FEB
		            ,A.MAR
		            ,A.APR
		            ,A.MAY
		            ,A.JUN
		            ,A.JUL
		            ,A.AUG
		            ,A.SEP
		            ,A.OCT
		            ,A.NOV
		            ,A.DEC
		            ,A.TOTAL
		FROM(
			SELECT TARGET_ID
						,TARGET_VALUE
						,MAX(TARGET_PATH) AS TARGET_PATH
						,A.USER_ID
						,A.USER_NM AS EMP_NM
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '01' THEN 1 ELSE 0 END) AS JAN
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '02' THEN 1 ELSE 0 END) AS FEB
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '03' THEN 1 ELSE 0 END) AS MAR
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '04' THEN 1 ELSE 0 END) AS APR
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '05' THEN 1 ELSE 0 END) AS MAY
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '06' THEN 1 ELSE 0 END) AS JUN
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '07' THEN 1 ELSE 0 END) AS JUL
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '08' THEN 1 ELSE 0 END) AS AUG
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '09' THEN 1 ELSE 0 END) AS SEP
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '10' THEN 1 ELSE 0 END) AS OCT
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '11' THEN 1 ELSE 0 END) AS NOV
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '12' THEN 1 ELSE 0 END) AS DEC
			            ,COUNT(*) AS TOTAL
			FROM iportal_custom.IECT7017 A 
			WHERE A.ACCT_ID = #{ACCT_ID}
			AND      A.ACTION_TYPE_NO = '1'
			AND      A.TARGET_TYPE_NO NOT IN (1000, 1001)
			AND      UPPER(A.USER_ID) NOT LIKE 'IPLAN_%'
			AND      UPPER(A.USER_ID) != 'ADMIN'
			AND      A.INSERT_TIME BETWEEN TO_DATE(#{YEAR}||'0101000000','YYYYMMDDHH24MISS') AND TO_DATE(#{YEAR}+1||'0101000000','YYYYMMDDHH24MISS')
			GROUP BY A.TARGET_ID, A.TARGET_VALUE, A.USER_ID, A.USER_NM
		) A
		ORDER BY A.TOTAL DESC, A.TARGET_VALUE
	</select>
	<select id="getMonthReportConnectionDetail" parameterType="hashmap" resultType="hashmap">
		SELECT A.USER_ID AS EMP_ID, A.EMP_NM,A.USER_GRP_NM,A.JAN,A.FEB,A.MAR,A.APR,A.MAY,A.JUN,A.JUL,A.AUG,A.SEP,A.OCT,A.NOV,A.DEC,A.TOTAL, A.TARGET_VALUE, A.TARGET_PATH
		FROM(
			SELECT A.ACCT_ID
			            ,A.USER_ID
			            ,A.USER_NM AS EMP_NM
						,A.USER_GRP_NM
						,A.TARGET_VALUE
						,A.TARGET_PATH
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '01' THEN 1 ELSE 0 END) AS JAN
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '02' THEN 1 ELSE 0 END) AS FEB
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '03' THEN 1 ELSE 0 END) AS MAR
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '04' THEN 1 ELSE 0 END) AS APR
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '05' THEN 1 ELSE 0 END) AS MAY
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '06' THEN 1 ELSE 0 END) AS JUN
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '07' THEN 1 ELSE 0 END) AS JUL
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '08' THEN 1 ELSE 0 END) AS AUG
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '09' THEN 1 ELSE 0 END) AS SEP
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '10' THEN 1 ELSE 0 END) AS OCT
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '11' THEN 1 ELSE 0 END) AS NOV
			            ,SUM(CASE WHEN TO_CHAR(A.INSERT_TIME,'MM') = '12' THEN 1 ELSE 0 END) AS DEC
			            ,COUNT(*) AS TOTAL
			FROM iportal_custom.IECT7017 A 
			WHERE A.ACCT_ID = #{ACCT_ID}
		    AND      UPPER(A.USER_ID) NOT LIKE 'IPLAN_%'
			AND      UPPER(A.USER_ID) != 'ADMIN'
			AND      A.ACTION_TYPE_NO = '1'
			AND      A.TARGET_TYPE_NO NOT IN ('1000','1001')
			AND      A.INSERT_TIME BETWEEN TO_DATE(#{YEAR}||'0101000000','YYYYMMDDHH24MISS') AND TO_DATE(#{YEAR}+1||'0101000000','YYYYMMDDHH24MISS')
			AND      A.TARGET_ID = #{TARGET_ID}
			GROUP BY A.ACCT_ID, A.USER_ID, A.USER_NM, A.USER_GRP_NM, A.TARGET_VALUE, A.TARGET_PATH
		) A ORDER BY A.TOTAL DESC
	</select>
</mapper>