package com.iplanbiz.iportal.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.MonthReportConnectionService;

@Controller
public class MonthReportConnectionController {

	@Autowired
	MonthReportConnectionService monthReportService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */

	@RequestMapping(value="/connectionStatus/monthReportConnection/list", method= RequestMethod.GET)
	public ModelAndView list () throws Exception{
		ModelAndView modelAndView = new ModelAndView("/connectionStatus/monthReportConnection/list");
		
		return modelAndView;
	}
	@RequestMapping(value="/connectionStatus/monthReportConnection/popupMonthReportDetail", method= RequestMethod.GET)
	public ModelAndView popupMonthReportDetail() throws Exception{
		ModelAndView modelAndView = new ModelAndView("/connectionStatus/monthReportConnection/popupMonthReportDetail");
		
		return modelAndView;
	}
	@RequestMapping(value="/connectionStatus/monthReportConnection/getMonthReportYear", method=RequestMethod.POST)
	public void getMonthReportYear ( HttpServletResponse response, HttpServletRequest request) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = monthReportService.getMonthReportYear();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Month Report Year Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/connectionStatus/monthReportConnection/getMonthReportConnectionTotal", method=RequestMethod.POST)
	public void getMonthReportConnectionTotal (HttpServletResponse response, HttpServletRequest request
																	,@RequestParam(required=false) String year) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = monthReportService.getMonthReportConnectionTotal(year);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Month Report Connection Total Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/connectionStatus/monthReportConnection/getMonthReportConnectionList", method=RequestMethod.POST)
	public void getMonthReportConnectionList ( HttpServletResponse response, HttpServletRequest request
																	,@RequestParam(required=false) String year) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = monthReportService.getMonthReportConnectionList(year);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Month Report Connection List Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);	
	}
	@RequestMapping(value="/connectionStatus/monthReportConnection/monthExcel", method= RequestMethod.GET)
	public ModelAndView monthExcel ( @RequestParam(required=false) String year) throws Exception{
		ModelAndView modelAndView = new ModelAndView("/connectionStatus/monthReportConnection/excelDownByReport");
		
		modelAndView.addObject("mainList", monthReportService.getMonthReportConnectionListByExcel(year));
		
		return modelAndView;
	}
	@RequestMapping(value="/connectionStatus/monthReportConnection/monthExcelAll", method=RequestMethod.GET)
	public ModelAndView monthExcelAll ( @RequestParam(required=false) String year ) throws Exception{
		ModelAndView modelAndView = new ModelAndView("/connectionStatus/monthReportConnection/excelDownByReportAll");
		
		modelAndView.addObject("mainList", monthReportService.getMonthReportConnectionListByExcelAll(year));
		
		return modelAndView;
	}
	@RequestMapping(value="/connectionStatus/monthReportConnection/popupMonthReportExcel", method=RequestMethod.GET)
	public ModelAndView popupMonthReportExcel ( @RequestParam(required=false) String year,@RequestParam(required=false) String targetID ) throws Exception{
		ModelAndView modelAndView = new ModelAndView("/connectionStatus/monthReportConnection/popupMonthReportExcel");
		
		modelAndView.addObject("mainList", monthReportService.getMonthReportConnectionDetail(year, targetID));
		
		return modelAndView;
	}
	@RequestMapping(value="/connectionStatus/monthReportConnection/getMonthReportConnectionDetail", method=RequestMethod.POST)
	public void getMonthReportConnectionDetail ( HttpServletResponse response, HttpServletRequest request
																	  ,@RequestParam(required=false) String year
																	  ,@RequestParam(required=false) String targetID) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = monthReportService.getMonthReportConnectionDetail(year, targetID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Month Connection Detail Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
}
