package com.iplanbiz.iportal.controller.portlet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.SecurityUtil;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.portlet.PortletManagerService;
import com.iplanbiz.iportal.service.portlet.PortletService;
import com.iplanbiz.iportal.service.system.SystemService;
import com.nhncorp.lucy.security.xss.listener.SecurityUtils;

@Controller
public class PortletManagerController {

	@Autowired PortletManagerService portletManagerService;
	@Autowired SystemService systemService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	@RequestMapping(value="/portlet/portletManager/list", method=RequestMethod.GET)
	public ModelAndView portletManagerList () throws Exception {
		ModelAndView modelAndView = new ModelAndView("/portlet/portletManager/list");
		
		return modelAndView;
	}
	@RequestMapping(value="/portlet/portletManager/detail", method=RequestMethod.GET)
	public ModelAndView portletManagerDetail ( @RequestParam(required=false) String plID ) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/portlet/portletManager/detail");
		
		modelAndView.addObject("plInfo"				, portletManagerService.getPortletInfo(plID));
		
		return modelAndView;
	}
	@RequestMapping(value="/portlet/portletManager/addPortlet", method=RequestMethod.GET)
	public ModelAndView addPortlet ( ) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/portlet/portletManager/addPortlet");
		
		return modelAndView;
	}
	@RequestMapping(value="/portlet/portletManager/admin_l{idx}", method={RequestMethod.GET})
	public ModelAndView admin_portelt ( @PathVariable(value="idx") String idx, @RequestParam(required=false) String plID ) throws Exception {
		String viewName = "portlet/loader/portlet_layout_"+Integer.parseInt(idx);
		ModelAndView modelAndView = new ModelAndView(viewName);
		 
		Map<String, Object> systemConfigMap = systemService.getSystemConfig();
		
		modelAndView.addObject("systemConfig", systemConfigMap);
		modelAndView.addObject("plid", plID);
		modelAndView.addObject("portletUserType", "admin");
		
		return modelAndView;
	}
	@RequestMapping(value="/portlet/portletManager/popup", method= RequestMethod.GET)
	public ModelAndView popup () throws Exception {
		ModelAndView modelAndView = new ModelAndView("/portlet/portletManager/popup");
		
		return modelAndView;
	}
	@RequestMapping(value="/portlet/portletManager/getPortletManagerList", method=RequestMethod.POST)
	public void getPortletManagerList ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletManagerService.getPortletManagerList();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("ERROR > Get Portlet Manager List Data Error : {}" ,e); 
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage()); 
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletManager/defalutPortletSetting", method=RequestMethod.POST)
	public void defalutPortletSetting ( HttpServletResponse response, HttpServletRequest request 
			                                        ,@RequestParam(required=false) String plID) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletManagerService.defalutPortletSetting(plID);
			if(returnValue == 0){
				msg.setSuccessText("기본 셋팅이 변경되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("ERROR > Delete All User Portlet  Default : {}" , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletManager/defaultUserPortlet", method=RequestMethod.POST)
	public void defaultUserPortlet ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletManagerService.defaultUserPortlet();
			if(returnValue == 0){
				msg.setSuccessText("전부 초기화 되었습니다");
			}else{
				 msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("ERROR > Default User Portlet Setting Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletManager/getPageletPortletList", method=RequestMethod.POST)
	public void getPageletPortletList ( HttpServletResponse response, HttpServletRequest request
													,@RequestParam(required=false) String pID
													,@RequestParam(required=false, defaultValue="") String searchValue) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletManagerService.getPageletPortletList(pID, searchValue);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("ERROR > Get Pagelet Portlet List Data Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletManager/getPortletCategory", method=RequestMethod.POST)
	public void getPortletCategory ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletManagerService.getPortletCategory();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("ERROR > Get Portlet Category Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " +  e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletManager/updatePageletInfo", method={RequestMethod.POST})
	public void updatePageletInfo ( HttpServletResponse response, HttpServletRequest request
											   ,@RequestParam(required=false) String plName
											   ,@RequestParam(required=false) String plLayout
											   ,@RequestParam(required=false) String plID ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletManagerService.updatePageletInfo(plName, plLayout, plID);
			if(returnValue == 0){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Update Pagelet Info Data Error : {}", e);
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletManager/deletePageletPortlet", method=RequestMethod.POST)
	public void deletePageletPortlet ( HttpServletResponse response, HttpServletRequest request 
												   ,@RequestParam(required=false) String plID) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletManagerService.deletePageletPortlet(plID);
			if(returnValue == 0){
				msg.setSuccessText("성공했습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Delete Pagelet Portlet Data Error : {} " ,e);
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletManager/insertPageletPortlet", method=RequestMethod.POST)
	public void insertPageletPortlet ( HttpServletResponse response, HttpServletRequest request
												  ,@RequestParam(required=false) String plID
												  ,@RequestParam(required=false) String poID
												  ,@RequestParam(required=false) String xAxis
												  ,@RequestParam(required=false) String yAxis
												  ,@RequestParam(required=false) String cssInfo ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			JSONParser parser = new JSONParser(); 
			Object obj = parser.parse(SecurityUtil.unEscape(cssInfo));
			JSONObject jsonObj = (JSONObject)obj;
			returnValue = portletManagerService.insertPageletPortlet(plID, poID, xAxis, yAxis, jsonObj);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Insert Pagelet Portlet Data Error : {} " ,e);
			msg.setExceptionText("에러가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletManager/updatePageletTheme", method=RequestMethod.POST)
	public void updatePageletTheme ( HttpServletResponse response, HttpServletRequest request
													,@RequestParam(required=false) String portletType
													,@RequestParam(required=false) String plID
													,@RequestParam(required=false) String poID ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			System.out.println("테마::"+portletType);
			returnValue = portletManagerService.updatePageletTheme(portletType, plID, poID);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Update Pagelet Theme Data Error : {}" ,e);
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletManager/InsertPageletInfo", method={RequestMethod.POST})
	public void InsertPageletInfo ( HttpServletResponse response, HttpServletRequest request
											   ,@RequestParam(required=false) String plName
											   ,@RequestParam(required=false) String plLayout ) throws Exception {
		JSONObject returnValue = new JSONObject();
		int plID = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			plID = portletManagerService.insertPageletInfo(plName, plLayout);
			logger.info("Controller Param PLID : {}" , plID);
			returnValue.put("PLID", plID);
			if(Integer.parseInt(returnValue.get("PLID").toString()) != 1){
				msg.setSuccessMessage(returnValue);
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Insert Pagelet Info Data Error : {}" ,e);
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletManager/deletePageletInfo", method=RequestMethod.POST)
	public void deletePageletInfo ( HttpServletResponse response, HttpServletRequest request
												,@RequestParam(required=false) String plID ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletManagerService.deletePageletInfo(plID);
			if(returnValue == 0){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Delete Pagelet Info Data Error : {} " ,e);
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	} 
}
