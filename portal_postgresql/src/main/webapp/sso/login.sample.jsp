<%@ page contentType="text/html; charset=utf-8" %>
<%@page import="com.ksign.access.sso.sso10.SSO10Conf"%>
<%@ page import="com.ksign.access.api.*" %>
<%@ page import="org.springframework.web.context.WebApplicationContext"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ page import="java.lang.Exception"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="org.springframework.beans.factory.ObjectFactory"%>
<%@ page import="com.iplanbiz.core.session.LoginSessionInfo"%>
<%@ page import="com.iplanbiz.iportal.comm.session.SessionManager"%>

<%@ page import="java.util.HashMap"%>
<%@ page language="java" import="java.net.InetAddress" %>
<%@ page import="com.iplanbiz.iportal.dao.auth.EmpView"%>
<%@ page import="com.iplanbiz.iportal.config.WebConfig"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.LinkedHashMap"%>
<%
 
	SSORspData rspData = null;
	SSOService ssoService = SSOService.getInstance();
    rspData = ssoService.ssoGetLoginData(request);	
	
	WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(((HttpServletRequest) request).getSession().getServletContext(),org.springframework.web.servlet.FrameworkServlet.SERVLET_CONTEXT_PREFIX + "SpringDispatcher");
	
	EmpView empView = (EmpView)wac.getBean("empView");
	
	
	Object userIDObject = rspData.getAttribute(SSO10Conf.UIDKey);
	String userID = null;
	if(userIDObject!=null){
		userID=userIDObject.toString();
	}
	
	if(userID!=null){
		LinkedHashMap<String, Object> userInfo = empView.getLoginUserInfo(userID,WebConfig.getAcctID());
		if(userInfo!=null){
			
			//ObjectFactory<LoginSessionInfo> loginSessionInfoFactory  = (ObjectFactory<LoginSessionInfo>)wac.getBean("loginSessionInfoFactory");
			Object Session = session.getAttribute("loginSessionInfo");
			if(Session==null){
				Session = new LoginSessionInfo();
				session.setAttribute("loginSessionInfo",Session);
			}
			com.iplanbiz.core.session.LoginSessionInfo info = (com.iplanbiz.core.session.LoginSessionInfo)Session;
			
			info.setAcctID(WebConfig.getAcctID());
			info.setIpAddress(request.getRemoteAddr());
			info.setLogin(true);
			info.setUserId(userID);
			info.setUserName(userInfo.get("EMP_NM").toString());
			info.setState(LoginSessionInfo.State.NORMAL);
			info.setLocale("ko");
			info.setIsAdmin(false);
			info.setExternalInfo("checkPwdUpdate", false); 
			%>
			<!DOCTYPE html>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
			<title></title>
			<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
			<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>   
			<script src="../../resources/js/util/remoteInfo.js"></script>
			<%-- <script type="text/javascript" src="${WEB.ROOT}/dwr/interface/groupManagerDwr.js"></script> --%>
			<script type="text/javascript">
				var userId="${sessionScope.loginSessionInfo.userId}";
				var accesInfo = "${accesInfo}";
				
				/* 프레임 로드 체크 */
				var totcnt = 0;
				var totalRes = 0;
				function checkFrameLoad() {
					totcnt++;
					
					if(totcnt == totalRes) {
						location.href = '../../';
					}
				 
				}
				function addSession(){
					var data = $i.post("/login/action/addLoginSessionInfo").done(function(res){ 
					});
				}
				function login(){
					addSession();
					setLog();
					if(accesInfo != ""){
						$.cookie("fullScreenMode", "false"); 
						location.href = "/";
					}else{
			 
						location.href = '../../'; 
					}
				}
				function goLogout(){
					location.href = '/login/action/logout?dp=duplication'; 
				}
				$(document).ready(function() {
					var isLogin = "${isLogin}";
					console.log(isLogin+"gggggg");
					var rtime = 5000;
					if(isLogin=='true'){//중복로그인 정보가 있을때
						$i.dialog.confirm('SYSTEM','동일 사용자가 이미 로그인 되어있습니다. 로그인 하시겠습니까?',function(){
							login();
						},function(){
							goLogout()
						});//yes
						//setTimeout("goLogout()",rtime);//no 
					}else{
						//alert(2);
						login();
					}
				});          
				function setLog() {
					getOSInfoStr();
					var actionTypeNo = "3";
					var targetTypeNo = "1000";
					$i.post("/login/action/insertUserLog", {actionTypeNo : actionTypeNo , targetTypeNo: targetTypeNo, targetValue:"",cID:"",remoteIP:'<%=request.getRemoteAddr() %>', osInfo:getOSInfoStr(), browserInfo:getBrowserName(), browserVersion:getBrowserVer(),targetPath:""}).done(function(res){
						
					});
				}
			</script>
			</head>
			<body>
			</body>
			</html>

			<%
		}
	}else{
		%>
			<script>
			location.href="/";
			//alert(1);
			</script>
		<%
	}
	
%> 