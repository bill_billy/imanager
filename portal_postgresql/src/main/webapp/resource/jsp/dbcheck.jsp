<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>  
<%@ page import="java.lang.Exception"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%> 
<%


String url="jdbc:oracle:thin:@XXX.168.12.110:1521:iplandb";
String user="XXXX";
String pwd="XXXX";
  
Connection conn = null; 
PreparedStatement pstmt = null;
ResultSet rs = null; 
Boolean connect = false;
	try{
		Class.forName("oracle.jdbc.driver.OracleDriver");
		conn = DriverManager.getConnection(url, user, pwd);
		connect = true;
		  
		String seqNo = "";
		String seqQuery = "SELECT 1 as CNT FROM DUAL";
		pstmt = conn.prepareStatement(seqQuery);
		rs = pstmt.executeQuery();   
		rs.next();
		Object result = rs.getObject("CNT");
		out.println(result);
	 
		 
	}finally {
		if(rs != null) try{rs.close();}catch(SQLException sqle){}            // Resultset 객체 해제

		if(pstmt != null) try{pstmt.close();}catch(SQLException sqle){}   // PreparedStatement 객체 해제

		if(conn != null) try{conn.close();}catch(SQLException sqle){}   // Connection 해제
	}

%>
