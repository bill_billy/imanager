<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SQL</title>
		<link rel="stylesheet" type="text/css" href="../../../../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
		<link rel="stylesheet" type="text/css" href="../../css/style.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
		<link rel="stylesheet" type="text/css" href="../../css/jqx.blueish.css"/>
		<!-- <script src="../../../../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> -->
	    <script src="../../../../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../../../../resources/js/jquery/jquery.fixheadertable.js"></script>
	    <script src="../../../../../resources/js/jquery/jquery.fixheadertable_v3.js"></script>
	    <script src="../../../../../resources/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../../../../resources/cmresource/js/jquery/jquery.form.min.js"></script>
	    <script src="../../../../../resources/js/jquery/jquery.cookie.js"></script>
	    <script src="../../../../../resources/js/util/hashMap.js"></script>
	    <script src="../../../../../resources/js/util/json2xml.js"></script>
	    <script src="../../../../../resources/js/util/xml2json.js"></script>
	    <script src="../../../../../resources/cmresource/js/thirdparty/MSPL/linq/linq.min.js"></script>
	    <script src="../../../../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../../../../resources/cmresource/js/iplanbiz/web/ui.alert.js"></script>
	    <script src="../../../../../resources/cmresource/js/iplanbiz/web/jquery.extension.table.js"></script>
	    <!-- <script src="../../../../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script> -->
        <script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcore.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxangular.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbargauge.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbulletchart.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbuttongroup.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbuttons.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscrollbar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscrollview.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtabs.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtagcloud.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtree.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxpanel.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxpasswordinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxpopover.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxexpander.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxmenu.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnavbar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdata.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdata.export.js"></script> 
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdatatable.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxlistbox.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxlistmenu.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcheckbox.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcombobox.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcomplexinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxradiobutton.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxrating.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxrangeselector.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdropdownbutton.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdropdownlist.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.sort.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.storage.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.selection.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.columnsresize.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.columnsreorder.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.pager.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.edit.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.export.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.aggregates.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.filter.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.grouping.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtreegrid.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtreemap.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtooltip.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtouch.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcalendar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdatetimeinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxsplitter.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.rangeselector.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.waterfall.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.annotations.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.api.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.core.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdraw.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgauge.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcolorpicker.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnavigationbar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnotification.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnumberinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxmaskedinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxswitchbutton.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxprogressbar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxslider.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxsortable.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxwindow.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdocking.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdockinglayout.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdockpanel.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdragdrop.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxresponse.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxresponsivepanel.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxribbon.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxeditor.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxfileupload.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxformattedinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtextarea.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtoolbar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscheduler.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscheduler.api.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxkanban.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxknob.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxknockout.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxlayout.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxloader.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxvalidator.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdate.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/globalization/globalize.js"></script>
		<script src="../../../../../resources/js/jquery/jquery-ui-tabs-paging.js"></script>
		<script src="../../../../../resources/js/util/common.js"></script>
        <script src="../../../../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <script>
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$("#btnSend").jqxButton({width:'', theme:'blueish'});
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		search();
        	}
        	//조회
        	function search(){
        		var kpiGbn = "${param.kpiGbn}";
        		var kpiID = "${param.kpiID}";
        		
        		$("#txtareaSqlData").val($("#txtareaSql", opener.document).val());
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function sendParentData(){
				$("#txtareaSql", opener.document).val($("#txtareaSqlData").val()) ;
				self.close();
        	}
        </script>
    </head>
    <body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:600px; min-width:600px; height:610px; min-height:610px;  margin:0 1%;">
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="table f_left" style=" width:100%; margin-right:0;">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<thead>
							<th>SQL</th>
						</thead>
						<tbody>
							<td>
								<textarea id="txtareaSqlData" class="textarea" style="width:98%; height:500px; margin:3px 5px !important;"></textarea>
							</td>
						</tbody>
					</table>
				</div>
				<div class="group_button f_right">
					<div class="button type2 f_left" style="margin-bottom:0;">
						<input type="button" value="전송" id='btnSend' width="100%" height="100%" onclick="sendParentData();"/>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

