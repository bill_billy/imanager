<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>대시보드 지표 목표 관리</title>
		<link rel="stylesheet" type="text/css" href="../../../../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
		<link rel="stylesheet" type="text/css" href="../../css/style.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
		<link rel="stylesheet" type="text/css" href="../../css/jqx.blueish.css"/>
	    <script src="../../../../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../../../../resources/js/jquery/jquery.fixheadertable.js"></script>
	    <script src="../../../../../resources/js/jquery/jquery.fixheadertable_v3.js"></script>
	    <script src="../../../../../resources/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../../../../resources/cmresource/js/jquery/jquery.form.min.js"></script>
	    <script src="../../../../../resources/js/jquery/jquery.cookie.js"></script>
	    <script src="../../../../../resources/js/util/hashMap.js"></script>
	    <script src="../../../../../resources/js/util/json2xml.js"></script>
	    <script src="../../../../../resources/js/util/xml2json.js"></script>
	    <script src="../../../../../resources/cmresource/js/thirdparty/MSPL/linq/linq.min.js"></script>
	    <script src="../../../../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../../../../resources/cmresource/js/iplanbiz/web/ui.alert.js"></script>
	    <script src="../../../../../resources/cmresource/js/iplanbiz/web/jquery.extension.table.js"></script>
        <script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcore.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxangular.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbargauge.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbulletchart.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbuttongroup.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbuttons.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscrollbar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscrollview.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtabs.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtagcloud.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtree.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxpanel.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxpasswordinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxpopover.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxexpander.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxmenu.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnavbar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdata.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdata.export.js"></script> 
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdatatable.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxlistbox.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxlistmenu.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcheckbox.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcombobox.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcomplexinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxradiobutton.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxrating.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxrangeselector.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdropdownbutton.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdropdownlist.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.sort.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.storage.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.selection.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.columnsresize.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.columnsreorder.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.pager.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.edit.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.export.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.aggregates.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.filter.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.grouping.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtreegrid.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtreemap.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtooltip.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtouch.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcalendar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdatetimeinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxsplitter.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.rangeselector.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.waterfall.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.annotations.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.api.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.core.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdraw.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgauge.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcolorpicker.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnavigationbar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnotification.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnumberinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxmaskedinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxswitchbutton.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxprogressbar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxslider.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxsortable.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxwindow.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdocking.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdockinglayout.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdockpanel.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdragdrop.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxresponse.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxresponsivepanel.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxribbon.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxeditor.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxfileupload.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxformattedinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtextarea.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtoolbar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscheduler.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscheduler.api.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxkanban.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxknob.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxknockout.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxlayout.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxloader.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxvalidator.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdate.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/globalization/globalize.js"></script>
		<script src="../../../../../resources/js/jquery/jquery-ui-tabs-paging.js"></script>
		<script src="../../../../../resources/js/util/common.js"></script>
        <script src="../../../../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
		<script>
		$i.config.contexturi=function() {
			return '../../../../..';
		};
		</script>
		<script>
			$(document).ready(function(){
				init();
			});
			function init(){
				setCombobox();
				$('#btnSearch').jqxButton({ width: '',  theme:'blueish'});
				$("#btnNew").jqxButton({ width: '', theme : 'blueish'});
				$("#btnInsert").jqxButton({ width: '',  theme:'blueish'});
				$("#btnDelete").jqxButton({ width: '',  theme:'blueish'});
				$("#txtSearch").jqxInput({placeHolder: "", height: 22, width: 250, minLength: 1, theme:'blueish' });
				$("#cboUseYn").jqxComboBox({ selectedIndex: 0, animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,  theme:'blueish'});
			}
			function search(){
				var kpiListData = $i.post("../../js/query.dashboardTargetManager.jsp", {programid:'getDashboardKpiList', kpiGbn:$("#cboSearchGubn").val(), comlNm:$("#txtSearch").val()});
				kpiListData.done(function(data){
					var source =
		            {
		                datatype: "json",
		                datafields: [
		                	{ name: 'COML_COD', type: 'string' },
		                    { name: 'COML_NM', type: 'string' },
		                    { name: 'KPI_DIR', type: 'string' },
		                    { name: 'USE_UNIT', type: 'string'}
		                ],
		                localdata: data.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var detailrow = function(row,columnfield,value,defaultHtml, property, rowdata){
						var kpiID = rowdata.COML_COD;
						var kpiName = rowdata.COML_NM;
						var newValue=$i.secure.scriptToText(value);
						var link = "<a href=\"javascript:tableDetail('"+kpiID+"','"+replaceAll(replaceAll(escape(escape(kpiName)),"&lt;","<"),"&gt;",">")+ "','')\" style='color:black; text-decoration:underline;' >" + newValue + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>"; 
					}
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridDashboardKpiList").jqxGrid(
		            {
		              	width: '100%',
		                height: '100%',
						altrows:true,
						pageable: true,
						sortable: true,
		                source: dataAdapter,
		                pagesize: 100,
						pagesizeoptions:['100', '200', '300'],
						columnsheight:25,
						rowsheight: 25,
						theme:'blueish', 
		                columnsresize: true,
		                columns: [
		                	{ text: '지표ID', dataField: 'COML_COD', width:'10%', align:'center', cellsalign: 'center'},
			                { text: '지표명', dataField: 'COML_NM', align:'center',  cellsalign: 'left', cellsrenderer:detailrow},
			                { text: '단위', dataField: 'USE_UNIT', width: '10%', align: 'center', cellsalign: 'center'},
			                { text: '극성', dataField: 'KPI_DIR', width: '10%', align: 'center', cellsalign: 'center'}
		                ]
		            });
		            if($("#hiddenComlCod").val() == ""){
		            	tableDetail('','','');	
		            }
				});
			}
			function replaceAll(str,orgStr,repStr){
			    return str.split(orgStr).join(repStr);
			}
			function tableDetail(comlCod, comlNm, msg){
				$("#labelcomlCod").html("지표ID : <span class='colorchange'>" + comlCod.replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/'"'/g,'&quot;')+ "</span>");
				$("#labelcomlNm").html("지표명 : <span class='colorchange'>" + unescape(unescape(unescape(comlNm).replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/'"'/g,'&quot;')+ "</span>")));
				$("#hiddenComlCod").val(replaceAll(replaceAll(comlCod,"&lt;","<"),"&gt;",">"));
				$("#hiddenComlNm").val(replaceAll(replaceAll(unescape(comlNm),"&lt;","<"),"&gt;",">"));
				var tableData = $i.post("../../js/query.dashboardTargetManager.jsp",{programid:"getDashboardKpiTargerValueList", comlCod:comlCod});
				tableData.done(function(data){
		            var source =
		            {
		                localdata: data.returnArray,
		                datafields:
		                [
		                    { name: 'COML_COD', type: 'string' },
		                    { name: 'COM_COD', type: 'string' },
		                    { name: 'COM_NM', type: 'string' },
		                    { name: 'SORT_ORDER', type: 'string' },
		                    { name: 'COM_UDC1', type:'string'},
							{ name: 'BIGO', type: 'string' },
							{ name: 'USE_YN', type: 'string' }
		                   
		                    //{ name: 'total', type: 'number' }
		                ],
		                datatype: "array"   
		            };
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            var getRow = function(row,datafield,value,defautHtml, property, rowdata){
		            	var comlCod = rowdata.COML_COD;
		            	var comCod = rowdata.COM_COD;
		            	var link = "<a href=\"javaScript:goRow('"+comlCod+"','"+comCod+"')\" style='color:black;text-decoration:underline;'>" + value+ "</a>";
		            	
		            	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px; color:black;'>" + link + "</div>";
		            };
		            var uRendererLeft = function (row, columnfield, value, defaulthtml, columnproperties, rowdata) {//왼쪽정렬
						if (value < 20) {
							return '<span style="margin:7px 10px 7px 20px; float: ' + columnproperties.cellsalign + '; color: ;">' + value+ '</span>';
						}
						else {
							return '<span style="margin: 7px 10px 7px 20px; float: ' + columnproperties.cellsalign + '; color: ;">' + value+ '</span>';
						}
					};
					var changeValue = function(row, columnfield, value, defaulthtml, columnproperties, rowdata) {//왼쪽정렬
						return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px; color:black;'>"+ value+ "</div>"; 
					};
						
					var dataAdapter = new $.jqx.dataAdapter(source, {
						downloadComplete: function (data, status, xhr) { },
						loadComplete: function (data) { },
						loadError: function (xhr, status, error) { }
					});

					var dataAdapter = new $.jqx.dataAdapter(source);
					$("#gridDetailTarget").jqxGrid({
						width: '100%',
						height: '100%',
						source: dataAdapter,
						columnsresize: false,
						sortable: false,
						//showfilterrow:false,
						//filtermode:false,
						theme:'blueish',
						//pageable: true,
						selectionmode: 'checkbox' ,
						altrows:true,
						ready: function () {
							$("#gridDetailTarget").jqxGrid();
						},
						columns: [
							{ text: '년도'    , dataField: 'COM_COD'   , width: '10%', cellsalign: 'center', align:'center' ,cellsrenderer:getRow},
							{ text: '사용여부' , dataField: 'USE_YN'    , width: '10%', cellsalign: 'center', align:'center'},
							{ text: '목표'    , dataField: 'COM_UDC1', width: '20%', cellsalign: 'center', align:'center'},
							{ text: '비고'    , dataField: 'BIGO'      , cellsalign: 'left'  , align:'center' , cellsrenderer: uRendererLeft}
						]
					});
					
					if(msg != ""){
						alert(msg);
					}
				});
			}
			function goRow(comlCod, comCod){
				var rowData = $i.post("../../js/query.dashboardTargetManager.jsp",{programid:"getDashboardKpiTargetValueOne", comlCod:comlCod, comCod:comCod});
				rowData.done(function(data){
					resetForm();
					$("#hiddenComlCod").val(replaceAll(replaceAll(comlCod,"&lt;","<"),"&gt;",">"));
					$("#txtOrgComCod").val(replaceAll(replaceAll(data.returnArray[0].COM_COD,"&lt;","<"),"&gt;",">"));
					$("#txtComCod").val(replaceAll(replaceAll(data.returnArray[0].COM_COD,"&lt;","<"),"&gt;",">"));
					$("#txtComNm").val(replaceAll(replaceAll(data.returnArray[0].COM_NM,"&lt;","<"),"&gt;",">"));
					$("#cboUseYn").jqxComboBox("selectItem", data.returnArray[0].USE_YN);
					$("#txtComUdc1").val(data.returnArray[0].COM_UDC1);
					$("#txtBigo").val(replaceAll(replaceAll(data.returnArray[0].BIGO,"&lt","<"),"&gt",">"));
				});
			}
			function resetForm(){
				$("#txtOrgComCod").val("");
				document.saveForm.reset();
			}
			function insert(){
				if($("#hiddenComlCod").val() == ""){
					$i.dialog.error("SYSTEM", "지표를 선택해주세요");
					return false;
				}
				if($("#txtComCod").val() == ""){
					$i.dialog.error("SYSTEM", "년도를 입력하세요");
					return false;
				}else if($("#txtComCod").val != ""){
					if(isNaN($("#txtComCod").val())){
						$i.dialog.error("SYSTEM", "년도는 숫자로 입력하세요");
						return false;
					}
				}
				if($("#txtComUdc1").val() == ""){
					$i.dialog.error("SYSTEM", "목표를 입력하세요");
					return false;
				}else if($("#txtComUdc1").val != ""){
					if(isNaN($("#txtComUdc1").val())){
						$i.dialog.error("SYSTEM", "목표는 숫자로 입력하세요");
						return false;
					}
				}
				$.ajax({
					type: 'post',
					url: '../../js/query.dashboardTargetManager.jsp',
					data: $('#saveForm').serialize()+'&programid=insert',
					success: function(args) {
						if(args.returnCode == "EXCEPTION"){
							$i.dialog.error("SYSTEM", args.returnMessage);
						}else{
							$i.dialog.alert("SYSTEM", args.returnMessage, function(){
								search();
								tableDetail($("#hiddenComlCod").val(),$("#hiddenComlNm").val(),'');
							});
						}
					},
					error: function(args) {
						//$i.dialog.error("SYSTEM", args.returnMessage);
						$i.dialog.error("SYSTEM", "오류가 발생했습니다.");
					}
				});
			}
			function remove(){
				var indexs = $('#gridDetailTarget').jqxGrid('getselectedrowindexes');
				var rows = $('#gridDetailTarget').jqxGrid('getrows');
				var comCod = "";
				if(indexs.length == 0){
					alert("삭제할 년도를 선택해 주세요");
					return false;
				}else{
					$i.dialog.confirm("SYSTEM", "삭제하시겠습니까?",function(){
						for(var i=0;i<indexs.length;i++){
							comCod += rows[indexs[i]].COM_COD + ",";
						}
						$("#txtOrgComCod").val(comCod);
						$.ajax({
							type: 'post',
							url: '../../js/query.dashboardTargetManager.jsp',
							data: $('#saveForm').serialize()+'&programid=remove',
							success: function(args) {
								if(args.returnCode == "EXCEPTION"){
									$i.dialog.error("SYSTEM", args.returnMessage);
								}else{
									$i.dialog.alert("SYSTEM", args.returnMessage, function(){
										search();
										tableDetail($("#hiddenComlCod").val(),$("#hiddenComlNm").val(),'');
									});
								}
							},
							error: function(args) {
								$i.dialog.error("SYSTEM", "오류가 발생했습니다.");
							}
						});
					});
				}
			}
			function showSql(){
				var kpiID = $("#hiddenKpiID").val();
				if(kpiID == ""){
					kpiID = "NULL";
				}
				window.open('./popupMappingSqlData.jsp?kpiGbn='+$("#cboSearchGubn").val()+'&kpiID='+kpiID, 'popupMappingSql','scrollbars=no, resizable=no, width=630, height=620');
        	}
			function setCombobox() {
					
				var kpiGbnCombobox = $i.post("../../js/query.dashboardTargetManager.jsp",{programid:'getDashboardCodeList', comlCod:"KPI_GBN"});
				kpiGbnCombobox.done(function(kpiGbn){
					var source =
					{
						datatype: "json",
						//datatype: "array",
						datafields: [
							{ name: 'COM_COD' },
							{ name: 'COM_NM' }
						],
						id: 'id',
						localdata:kpiGbn.returnArray,
						async: false
					};
					
					var dataAdapter = new $.jqx.dataAdapter(source);
					$("#cboSearchGubn").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 120, dropDownHeight: 80, width: 120, height: 22,  theme:'blueish'});
					var userYnCombobox = $i.post("../../js/query.dashboardTargetManager.jsp",{programid:'groupCode', comlCod:'USE_YN'});
					userYnCombobox.done(function(useYn){
						var source =
						{
							datatype: "json",
							//datatype: "array",
							datafields: [
								{ name: 'COM_COD' },
								{ name: 'COM_NM' }
							],
							id: 'id',
							localdata:useYn.returnArray,
							async: false
						};
						
						var dataAdapter = new $.jqx.dataAdapter(source);
						$("#cboSearchUseYn").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,  theme:'blueish'});
						search();
					});
				});
			}
		</script>
	</head>
	<body class="blueish">
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 10px">
			<form id="frm" name="frm" method="get" class="padding_top" >
				<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
					<div class="label type1 f_left">구분 : </div>
					<div class="combobox f_left"  id="cboSearchGubn" name="searchGubn"></div>
					<div class="label type1 f_left">사용여부 : </div>
					<div class="combobox f_left" id="cboSearchUseYn" name="searchUseYn"></div>
					<div class="label type1 f_left">제목 : </div>
					<div class="inputf_left">
						<input type="text" id="txtSearch" style="margin:2px 5px;" />
					</div>
					<div class="group_button f_right">                                                                                                                                                                                                                                            
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
						<div class="button type3 f_left">
							<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();" />
						</div>
					</div>
				</div>
			</form>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style="width:34%; margin-right:1%;">
					<div class="grid f_left" style="width:100%; height:600px;">	
						<div id="gridDashboardKpiList"> </div>
					</div>
				</div>
				<div class="content f_right" style="width:65%;">
					<div class="label type2 f_left" style="float:left;"><span id="labelcomlCod">지표ID： </span></div>
					<div class="label type2 f_left m_l10" style="float:left;"><span id="labelcomlNm">지표명： </span></div>
					<div class="grid f_left" style="width:100%; height:380px;">	
						<div id="gridDetailTarget"> </div>
					</div>
					<div class="table f_left" style="width:100%; height:100%; margin:0;">
						<form id="saveForm" name="saveForm" action="" method="post">
							<input type="hidden" id="ac" name="ac" />
							<input type="hidden" id="hiddenComlCod" name="comlCod" />
							<input type="hidden" id="hiddenComlNm" name="comlNm" />
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<tr>
									<th class="required-icon" width="30%" style="text-align: center !important; padding-left: 0 !important;">년도<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th>
									<th class="required-icon" width="10%" style="text-align: center !important; padding-left: 0 !important;">사용여부<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th>
									<th class="required-icon" width="30%" style="text-align: center !important; padding-left: 0 !important;">목표<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th>
									<th class="required-icon" width="" style="text-align: center !important; padding-left: 0 !important;">비고</th>
								</tr>
								<tr>
									<td> 
										<input type="hidden" id="txtOrgComCod" name="orgComCod" />
										<input type="text" id="txtComCod" name="comCod" class="input type2 f_left not_kr only_en_sc" style="width:95%; margin:3px 5px !important;" onblur="checkValue('comCod','20' ,'en');"/>
									</td>
									<td>
										<select id="cboUseYn" name="useYn" class="table_combobox">
											<option value="Y" <c:if test="${param.useYn == 'Y'}">selected</c:if>>사용</option>	
											<option value="N" <c:if test="${param.useYn == 'N'}">selected</c:if>>미사용</option>	     
										</select>
									</td>  
									<td >
										<input type="text" id="txtComUdc1" name="comUdc1" class="input type2 f_left" style="width:95%; margin:3px 5px !important;" onkeypress="return isNumber(event)"/>									
									</td>
									<td >
										<input type="text" id="txtBigo" name="bigo" class="input type2 f_left" style="width:95%; margin:3px 5px !important;" onblur="checkInput('bigo','750', 'all');"/>
									</td>
								</tr>
							</table>
						</form>
					</div>
					<div class="label type4 f_left" style="float:left; width:50% !important;"><p class="iwidget_grid_tip" style="height: auto;">셀에 마우스 클릭시 편집 및 선택할 수 있습니다.</p></div>
					<div class="f_right m_t5">
						<div class="button type2 f_left">
							<input type="button" value="신규" id='btnNew' width="100%" height="100%" onclick="resetForm();" />
						</div>
						<div class="button type2 f_left">
							<input type="button" value="저장" id='btnInsert' width="100%" height="100%" onclick="insert();" />
						</div>
					</div>
				</div> 
			</div>
		</div>
	</body>
</html>