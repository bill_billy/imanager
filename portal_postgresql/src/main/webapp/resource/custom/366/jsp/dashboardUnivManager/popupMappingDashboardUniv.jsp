<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>${title}</title>
	<link rel="stylesheet" type="text/css" href="../../../../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	<link rel="stylesheet" type="text/css" href="../../css/style.blueish.css"/>
	<link rel="stylesheet" type="text/css" href="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	<link rel="stylesheet" type="text/css" href="../../css/jqx.blueish.css"/>
    <script src="../../../../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
    <script src="../../../../../resources/js/jquery/jquery.fixheadertable.js"></script>
    <script src="../../../../../resources/js/jquery/jquery.fixheadertable_v3.js"></script>
    <script src="../../../../../resources/js/jquery/jquery-ui-1.9.1.custom.js"></script>
    <script src="../../../../../resources/cmresource/js/jquery/jquery.form.min.js"></script>
    <script src="../../../../../resources/js/jquery/jquery.cookie.js"></script>
    <script src="../../../../../resources/js/util/hashMap.js"></script>
    <script src="../../../../../resources/js/util/json2xml.js"></script>
    <script src="../../../../../resources/js/util/xml2json.js"></script>
    <script src="../../../../../resources/cmresource/js/thirdparty/MSPL/linq/linq.min.js"></script>
    <script src="../../../../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
    <script src="../../../../../resources/cmresource/js/iplanbiz/web/ui.alert.js"></script>
    <script src="../../../../../resources/cmresource/js/iplanbiz/web/jquery.extension.table.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcore.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxangular.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbargauge.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbulletchart.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbuttongroup.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbuttons.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscrollbar.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscrollview.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtabs.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtagcloud.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtree.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxpanel.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxpasswordinput.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxpopover.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxexpander.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxmenu.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnavbar.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdata.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdata.export.js"></script> 
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdatatable.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxinput.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxlistbox.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxlistmenu.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcheckbox.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcombobox.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcomplexinput.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxradiobutton.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxrating.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxrangeselector.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdropdownbutton.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdropdownlist.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.sort.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.storage.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.selection.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.columnsresize.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.columnsreorder.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.pager.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.edit.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.export.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.aggregates.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.filter.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.grouping.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtreegrid.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtreemap.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtooltip.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtouch.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcalendar.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdatetimeinput.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxsplitter.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.rangeselector.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.waterfall.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.annotations.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.api.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.core.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdraw.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgauge.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcolorpicker.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnavigationbar.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnotification.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnumberinput.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxmaskedinput.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxswitchbutton.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxprogressbar.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxslider.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxsortable.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxwindow.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdocking.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdockinglayout.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdockpanel.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdragdrop.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxresponse.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxresponsivepanel.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxribbon.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxeditor.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxfileupload.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxformattedinput.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtextarea.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtoolbar.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscheduler.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscheduler.api.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxkanban.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxknob.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxknockout.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxlayout.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxloader.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxvalidator.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdate.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/globalization/globalize.js"></script>
	<script src="../../../../../resources/js/jquery/jquery-ui-tabs-paging.js"></script>
	<script src="../../../../../resources/js/util/common.js"></script>
	<script src="../../../../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		init();
	});
	function init(){
		$("#btnSearch").jqxButton({ width : '', theme : 'blueish' });
		$("#txtSearch").jqxInput({placeHolder: "", height: 23, width: 250, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return search(); } });
		search();
	}
	function search(){
		var gridData = $i.post("../../js/query.univManager.jsp", {programid:'getDashboardPopupUnivList', univName:$("#txtSearch").val()});
		
		gridData.done(function(res){
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: 'UNIV_ID', type: 'string' },
					{ name: 'UNIV_NM', type: 'string' },
					{ name: 'UNIV_GRP_NM', type: 'string' },
					{ name: 'UNIV_GRP1_NM', type: 'string' },
					{ name: 'UNIV_GRP2_NM', type: 'string' }
				],
				localdata: res.returnArray
			};
			
			var dataAdapter = new $.jqx.dataAdapter(source);
			
			//columns
			var nameCellsrenderer = function(row, datafieId, value, defaultHtml, property, rowdata) {
				var univID = rowdata.UNIV_ID;
				var univName = rowdata.UNIV_NM;
				var link = "<a href=\"javaScript:setData('"+univID+"', '"+univName+"');\" style='color: #000000;'>" + value + "</a>";
			
				return "<div style='text-align: left; padding-bottom:2px; margin-top:5px;margin-left:10px;'>" + link + "</div>";
			};
	
			$('#gridUnivList').jqxGrid({
				width: '100%',
				height: '100%',
				altrows:true,
				pageable: true,
				pagesize: 100,
				pagesizeoptions:['100', '200', '300'],
				source: dataAdapter,
				theme:'blueish',
				columnsheight:28,
				rowsheight: 28,
				columnsresize: true,
				columns: [
					{ text: '대학ID', datafield: 'UNIV_ID', width: '20%', align:'center', cellsalign: 'center'},
					{ text: '대학교명', datafield: 'UNIV_NM', width: '40%', align:'center', cellsalign: 'center', cellsrenderer:nameCellsrenderer},
					{ text: '학교설립', datafield: 'UNIV_GRP_NM', width: '20%', align:'center', cellsalign: 'center' },
					{ text: '학교종류', datafield: 'UNIV_GRP2_NM', width: '20%', align:'center', cellsalign: 'center' }
				]
			});
			
			//pager width
			$('#gridpagerlist' + $('#gridTableList').attr('id')).width('44px');
			$('#dropdownlistContentgridpagerlist' + $('#gridTableList').attr('id')).width('19px');
		});
	}
	function searchStart() {
	}
	function setData(univID, univName) {
		$('#hiddenUnivID',opener.document).val(univID);
		$("#labelUnivID", opener.document).html(univID);
		$("#labelUnivName", opener.document).html(univName);
		window.close();
	}
	</script>
</head>
<body class='blueish'>
	<div class="wrap" style="width:96%; margin:0 auto;">
		<div class="header f_left" style="width:99%; height:97% !important; margin:3px 5px 5px !important;">
			<div class="label type1 f_left">학교명 : </div>
			<div class="input f_left">
				<input type="text" id="txtSearch" name="searchValue" />
			</div>
			<div class="group_button f_right">
				<div class="button type1 f_left">
					<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
				</div>
			</div>
		</div>
		<div class="container  f_left" style="width:100%; margin:10px 0;">
			<div class="content f_left" style="width:100%; margin-right:1%;">
				<div class="grid f_left" style="width:99%; height:530px; margin:3px 5px 5px !important;">
					<div id="gridUnivList"></div>
				</div>
			</div>
		</div>
	</div><!--wrap-->		
</body>
</html>
