<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>대시보드 지표 관리</title>
		<link rel="stylesheet" type="text/css" href="../../../../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
		<link rel="stylesheet" type="text/css" href="../../css/style.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
		<link rel="stylesheet" type="text/css" href="../../css/jqx.blueish.css"/>
	    <script src="../../../../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../../../../resources/js/jquery/jquery.fixheadertable.js"></script>
	    <script src="../../../../../resources/js/jquery/jquery.fixheadertable_v3.js"></script>
	    <script src="../../../../../resources/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../../../../resources/cmresource/js/jquery/jquery.form.min.js"></script>
	    <script src="../../../../../resources/js/jquery/jquery.cookie.js"></script>
	    <script src="../../../../../resources/js/util/hashMap.js"></script>
	    <script src="../../../../../resources/js/util/json2xml.js"></script>
	    <script src="../../../../../resources/js/util/xml2json.js"></script>
	    <script src="../../../../../resources/cmresource/js/thirdparty/MSPL/linq/linq.min.js"></script>
	    <script src="../../../../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../../../../resources/cmresource/js/iplanbiz/web/ui.alert.js"></script>
	    <script src="../../../../../resources/cmresource/js/iplanbiz/web/jquery.extension.table.js"></script>
        <script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcore.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxangular.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbargauge.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbulletchart.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbuttongroup.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbuttons.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscrollbar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscrollview.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtabs.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtagcloud.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtree.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxpanel.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxpasswordinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxpopover.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxexpander.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxmenu.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnavbar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdata.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdata.export.js"></script> 
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdatatable.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxlistbox.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxlistmenu.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcheckbox.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcombobox.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcomplexinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxradiobutton.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxrating.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxrangeselector.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdropdownbutton.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdropdownlist.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.sort.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.storage.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.selection.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.columnsresize.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.columnsreorder.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.pager.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.edit.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.export.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.aggregates.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.filter.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.grouping.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtreegrid.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtreemap.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtooltip.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtouch.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcalendar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdatetimeinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxsplitter.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.rangeselector.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.waterfall.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.annotations.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.api.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.core.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdraw.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgauge.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcolorpicker.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnavigationbar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnotification.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnumberinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxmaskedinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxswitchbutton.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxprogressbar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxslider.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxsortable.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxwindow.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdocking.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdockinglayout.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdockpanel.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdragdrop.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxresponse.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxresponsivepanel.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxribbon.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxeditor.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxfileupload.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxformattedinput.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtextarea.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtoolbar.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscheduler.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscheduler.api.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxkanban.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxknob.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxknockout.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxlayout.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxloader.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxvalidator.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdate.js"></script>
		<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/globalization/globalize.js"></script>
		<script src="../../../../../resources/js/jquery/jquery-ui-tabs-paging.js"></script>
		<script src="../../../../../resources/js/util/common.js"></script>
        <script src="../../../../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
		<script>
		$i.config.contexturi=function() {
			return '../../../../..';
		};
		</script>
		<script>
			$(document).ready(function(){
				init();
			});
			function init(){
				setCombobox();
				$('#btnSearch').jqxButton({ width: '',  theme:'blueish'});
				$("#btnNew").jqxButton({ width: '', theme : 'blueish'});
				$("#btnSave").jqxButton({ width: '',  theme:'blueish'});
				$("#btnDelete").jqxButton({ width: '',  theme:'blueish'});
			}
			function search(){
				var kpiListData = $i.post("../../js/query.dashboardKpiManager.jsp", {programid:'getDashboardKpiList', kpiGbn:$("#cboSearchGubn").val()});
				
				kpiListData.done(function(data){
					var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'KPI_ID', type: 'string'},
		                    { name: 'KPI_NM', type: 'string'},
		                    { name: 'USE_NM', type: 'string'},
		                    { name: 'USE_YN', type: 'string'},
		                    { name: 'KPI_DIR_NM', type: 'string'}
		                ],
		                localdata: data.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var detailrow = function(row,columnfield,value,defaultHtml, property, rowdata){
						var kpiID = rowdata.KPI_ID;
						var newValue=$i.secure.scriptToText(value);
						var link = "<a href=\"javascript:getKpiDetail('"+kpiID+"')\" style='color:black; text-decoration:underline;' >" + newValue + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>"; 
					}
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridDashboardKpiList").jqxGrid(
		            {
		              	width: '100%',
		                height: '100%',
						altrows:true,
						pageable: true,
						sortable: true,
		                source: dataAdapter,
		                pagesize: 100,
						pagesizeoptions:['100', '200', '300'],
						columnsheight:25,
						rowsheight: 25,
						theme:'blueish', 
		                columnsresize: true,
		                columns: [
		                   { text: '지표ID', datafield: 'KPI_ID', width: '10%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '지표명', datafield: 'KPI_NM', width: '60%', align:'center', cellsalign: 'center', cellsrenderer: detailrow},
						   { text: '단위', datafield: 'USE_NM', width: '10%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
						   { text: '사용여부', datafield: 'USE_YN', width: '10%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
						   { text: '극성', datafield: 'KPI_DIR_NM', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
		                ]
		            });
				});
			}
			function insert(){
				if($("#txtSortOrder").val() == ""){
					$("#txtSortOrder").val("0");
				}
				if(isNaN($("#txtSortOrder").val())){
					$i.dialog.error("SYSTEM", "우선순위는 숫자로 입력하세요");
					return false;
				}
				if($("#txtKpiName").val() == ""){
					$i.dialog.error("SYSTEM", "지표명을 입력하세요");
					return false;
				}
				
				$.ajax({
					type: 'post',
					url: '../../js/query.dashboardKpiManager.jsp',
					data: $('#saveForm').serialize()+'&programid=insert',
					success: function(args) {
						
						if(args.returnCode == "EXCEPTION"){
							$i.dialog.error("SYSTEM", args.returnMessage);
						}else{
							$i.dialog.alert("SYSTEM", args.returnMessage, function(){
								search();
								newData();
								$("#gridDashboardKpiList").jqxGrid('clearselection');
							});
						}
					},
					error: function(args) {
						//$i.dialog.error("SYSTEM", args.returnMessage);
						$i.dialog.error("SYSTEM", "오류가 발생했습니다.");
					}
				});
			}
			function remove(){
				$i.dialog.confirm("SYSTEM", "삭제하시겠습니까?",function(){
					$.ajax({
						type: 'post',
						url: '../../js/query.dashboardKpiManager.jsp',
						data: $('#saveForm').serialize()+'&programid=remove',
						success: function(args) {
							if(args.returnCode == "EXCEPTION"){
								$i.dialog.error("SYSTEM", args.returnMessage);
							}else{
								$i.dialog.alert("SYSTEM", args.returnMessage, function(){
									search();
									newData();
									$("#gridDashboardKpiList").jqxGrid('clearselection');
								});
							}
						},
						error: function(args) {
							$i.dialog.error("SYSTEM", "오류가 발생했습니다.");
						}
					});
					
				});
			}
			function showSql(){
				var kpiID = $("#hiddenKpiID").val();
				if(kpiID == ""){
					kpiID = "NULL";
				}
				window.open('./popupMappingSqlData.jsp?kpiGbn='+$("#cboSearchGubn").val()+'&kpiID='+kpiID, 'popupMappingSql','scrollbars=no, resizable=no, width=630, height=620');
        	}
			function newData(){
				$("#hiddenKpiID").val("");
				$("#labelKpiID").html("");
				$("#cboKpiGubn").val($("#cboSearchGubn").val());
				$("#txtKpiName").val("");
				$("#textareaSol").val("");
				$("#textareaBIgo").val("");
				$("#txtStartDat").val("");
				$("#txtEndDat").val("");
				$("#cboMeas").jqxComboBox({ selectedIndex: 0});
				$("#txtSortOrder").val("");
				$("#cboUseUnit").jqxComboBox({ selectedIndex: 0});
				$("#cboColSystem").jqxComboBox({ selectedIndex: 0});
				$("#cboKpiDir").jqxComboBox({ selectedIndex: 0});
				$("#txtOwnerUser").val("");
				$("#txtSDate").val("");
				$("#txtEDate").val("");
				$("#txtKpiUdc1").val("");
				$("#txtKpiUdc2").val("");
				$("#txtKpiUdc3").val("");
				$("#txtKpiUdc4").val("");
				$("#txtKpiUdc5").val("");
				$("#cboUseYn").jqxComboBox({ selectedIndex : 0 });
				$("#txtareaSql").val("");
				$("[name='mon']").prop("checked","");
			}
			function getKpiDetail(kpiID){
				var detailData = $i.post("../../js/query.dashboardKpiManager.jsp",{programid:'getDashboardKpiDetail', kpiID: kpiID});
				
				detailData.done(function(data){
					newData();
					$("#hiddenKpiID").val(data.returnArray[0].KPI_ID);
					$("#labelKpiID").html(data.returnArray[0].KPI_ID);
					$("#cboKpiGubn").val(data.returnArray[0].KPI_GBN);   
					$("#txtKpiName").val(data.returnArray[0].KPI_NM);
					$("#textareaSol").val(data.returnArray[0].ETC_DESC);
					$("#textareaBIgo").val(data.returnArray[0].KPI_DESC);
					$("#txtStartDat").val(data.returnArray[0].DATA_START_DAT);
					$("#txtEndDat").val(data.returnArray[0].DATA_END_DAT);
					$("#cboMeas").val(data.returnArray[0].MEAS_CYCLE);
					$("#txtSortOrder").val(data.returnArray[0].SORT_ORDER);
					$("#cboUseUnit").val(data.returnArray[0].USE_UNIT);
					$("#cboColSystem").val(data.returnArray[0].COL_SYSTEM);
					$("#cboKpiDir").val(data.returnArray[0].KPI_DIR);
					$("#txtOwnerUser").val(data.returnArray[0].OWNER_USER_ID);
					$("#txtSDate").val(data.returnArray[0].START_DAT);
					$("#txtEDate").val(data.returnArray[0].END_DAT);
					$("#txtKpiUdc1").val(data.returnArray[0].KPI_UDC1);
					$("#txtKpiUdc2").val(data.returnArray[0].KPI_UDC2);
					$("#txtKpiUdc3").val(data.returnArray[0].KPI_UDC3);
					$("#txtKpiUdc4").val(data.returnArray[0].KPI_UDC4);
					$("#txtKpiUdc5").val(data.returnArray[0].KPI_UDC5);
					$("#cboUseYn").val(data.returnArray[0].USE_YN);
					$("#txtareaSql").val(data.returnArray[0].SQL_DATA);
					var checkMonthData = $i.post("../../js/query.dashboardKpiManager.jsp",{programid:'getDashboardCheckMm', kpiID: kpiID});
					checkMonthData.done(function(month){
						if(month.returnCode == "EXCEPTION"){
							$i.dialog.error("SYSTEM", month.returnMessage);
						}else{
							if(month.returnArray.length>0){
								for(var i=0;i<month.returnArray.length;i++){
									$("input[name='mon'][value="+month.returnArray[i].DISP_MM+"]").prop("checked","checked");
								}
							}
						}
					});
				});
			}
			
			function setCombobox() {
					
				var kpiGbnCombobox = $i.post("../../js/query.dashboardKpiManager.jsp",{programid:'getDashboardCodeList', comlCod:"KPI_GBN"});
				kpiGbnCombobox.done(function(kpiGbn){
					var source =
					{
						datatype: "json",
						//datatype: "array",
						datafields: [
							{ name: 'COM_COD' },
							{ name: 'COM_NM' }
						],
						id: 'id',
						localdata:kpiGbn.returnArray,
						async: false
					};
					
					var dataAdapter = new $.jqx.dataAdapter(source);
					$("#cboSearchGubn").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 120, dropDownHeight: 80, width: 120, height: 22,  theme:'blueish'});

					search();
					var source = 
					{
						datatype: "json",
						datafields: [
							{ name : "COM_COD"},
							{ name : "COM_NM"}
						],
						id:"id", 
						localdata:kpiGbn.returnArray,
						async: false
					};
					var dataAdapter = new $.jqx.dataAdapter(source);
					$("#cboKpiGubn").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 120, dropDownHeight: 80, width: 120, height: 22,  theme:'blueish'});
				});
				
				var measCycleData = $i.post("../../js/query.dashboardKpiManager.jsp",{programid:'getDashboardCodeList', comlCod:"MEAS_CYCLE"});
				measCycleData.done(function(measCycle){
					var source = 
					{
						datatype: "json",
						datafields: [
							{ name : "COM_COD"},
							{ name : "COM_NM"}
						],
						id:"id",
						localdata:measCycle.returnArray,
						async:false
					};
					var dataAdapter = new $.jqx.dataAdapter(source);
					$("#cboMeas").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 120, dropDownHeight: 80, width: 120, height: 22,  theme:'blueish'});
				});
				var useUnitData = $i.post("../../js/query.dashboardKpiManager.jsp",{programid:'getDashboardCodeList', comlCod:"USEUNIT"});
				useUnitData.done(function(useUnit){
					var source = 
					{
						datatype: "json",
						datafields:[
							{ name : "COM_COD"},
							{ name : "COM_NM"}
						],
						id:"id",
						localdata:useUnit.returnArray,
						async:false
					};
					var dataAdapter = new $.jqx.dataAdapter(source);
					$("#cboUseUnit").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 120, dropDownHeight: 80, width: 120, height: 22,  theme:'blueish'});	
				});
				var colSystemData = $i.post("../../js/query.dashboardKpiManager.jsp",{programid:'getDashboardCodeList', comlCod:"COL_SYSTEM"});
				colSystemData.done(function(colSystem){
					var source = 
					{
						datatype: "json",
						datafields:[
							{ name : "COM_COD"},
							{ name : "COM_NM"}
						],
						id:"id",
						localdata:colSystem.returnArray,
						async: false
					};
					var dataAdapter = new $.jqx.dataAdapter(source);
					$("#cboColSystem").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 150, dropDownHeight: 80, width: 150, height: 22,  theme:'blueish'});
				});
				var kpiDirData = $i.post("../../js/query.dashboardKpiManager.jsp",{programid:'getDashboardCodeList', comlCod:"KPI_DIR"});
				kpiDirData.done(function(kpiDir){
					var source = 
					{
						datatype: "json",
						datafields:[
							{ name : "COM_COD"},
							{ name : "COM_NM"}
						],
						id:"id",
						localdata:kpiDir.returnArray,
						async: false
					};
					var dataAdapter = new $.jqx.dataAdapter(source);
					$("#cboKpiDir").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 120, dropDownHeight: 80, width: 120, height: 22,  theme:'blueish'});	
				});
				var useYnData = $i.post("../../js/query.dashboardKpiManager.jsp",{programid:'getDashboardCodeList', comlCod:"USE_YN"});
				useYnData.done(function(useYn){
					var source = 
					{
						datatype: "json",
						datafields:[
							{ name : "COM_COD"},
							{ name : "COM_NM"}
						],
						id:"id",
						localdata:useYn.returnArray,
						async:false
					};
					var dataAdapter = new $.jqx.dataAdapter(source);
					$("#cboUseYn").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 120, dropDownHeight: 80, width: 120, height: 22,  theme:'blueish'});
				});
			}
		</script>
	</head>
	<body class="blueish">
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 10px">
			<form id="frm" name="frm" method="get" class="padding_top" >
				<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
					<div class="label type1 f_left">구분 : </div>
					<div class="combobox f_left"  id="cboSearchGubn" name="searchGubn"></div>
					<div class="group_button f_right">                                                                                                                                                                                                                                            
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
					</div>
				</div>
			</form>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style="width:34%; margin-right:1%;">
					<div class="grid f_left" style="width:100%; height:600px;">	
						<div id="gridDashboardKpiList"> </div>
					</div>
				</div>
				<div class="content f_right" style="width:65%;">
					<form id="saveForm" name="saveForm" action="">
						<input type="hidden" id="ac" name="ac" />
						<div class="table f_left" style="width:100%; height:100%; margin:0;">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<tr>
									<th style="width:20% !important; font-weight:bold !important;">지표ID</th>
									<td style="width:30% !important;">
										<input type="hidden" id="hiddenKpiID" name="kpiID" />
										<span id="labelKpiID" style="margin:3px 5px !important;"></span>
									</td>
									<th style="width:20% !important; font-weight:bold !important;">구분</th>
									<td style="width:30% !important;">
										<div class="table_combobox" style="margin:3px 5px !important;" id="cboKpiGubn" name="kpiGbn"></div>
									</td>
								</tr>
								<tr>
									<th>지표명</th>
									<td colspan="3">
										<input type="text" id="txtKpiName" name="kpiName" class="input type2 f_left" style="width:98%; margin:3px 5px !important;" />
									</td>
								</tr>
								<tr>
									<th>설명</th>
									<td colspan="3">
										<textarea class="textarea type2 f_left" style="width:98.5%;height:30px;margin:3px 5px !important;" id="textareaSol" name="sol"></textarea>
									</td>
								</tr>
								<tr>
									<th>비고</th>
									<td colspan="3">
										<textarea class="textarea type2 f_left" style="width:98.5%;height:30px;margin:3px 5px !important;" id="textareaBIgo" name="bigo"></textarea>
									</td>
								</tr>
								<tr>
									<th>SQL<span class="th_view pointer" onclick="showSql();">[전체보기]</span></th>
									<td colspan="3">
										<textarea class="textarea type2 f_left" style="width:98.5%;height:160px;margin:3px 5px !important;" id="txtareaSql" name="sql"></textarea>
									</td>
								</tr>
								<tr>
									<th>게시월</th>
									<td colspan="3">
										<input type="checkbox" name="mon" value="03" style="margin: 3px 5px !important;" /> : 03월&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="mon" value="04" /> : 04월&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="mon" value="05" /> : 05월&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="mon" value="06" /> : 06월&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="mon" value="07" /> : 07월&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="mon" value="08" /> : 08월&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="mon" value="09" /> : 09월&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="mon" value="10" /> : 10월&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="mon" value="11" /> : 11월&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="mon" value="12" /> : 12월&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="mon" value="01" /> : 01월&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="checkbox" name="mon" value="02" /> : 02월&nbsp;&nbsp;&nbsp;&nbsp;
									</td>
								</tr>
								<tr>
									<th style="width:20% !important; font-weight:bold !important;">DATA 생성 시작일</th>
									<td>
										<input type="text" id="txtStartDat" name="startDat" class="input type2 f_left" style="width:95%; margin:3px 5px !important;" />
									</td>
									<th style="width:20% !important; font-weight:bold !important;">DATA 생성 종료일</th>
									<td>
										<input type="text" id="txtEndDat" name="endDat" class="input type2 f_left" style="width:95%; margin:3px 5px !important;" />
									</td>
								</tr>
								<tr>
									<th style="width:20% !important; font-weight:bold !important;">입력주기</th>
									<td>
										<div class="table_combobox" style="margin:3px 5px !important;" id="cboMeas" name="measCycle"></div>
									</td>
									<th style="width:20% !important; font-weight:bold !important;">우선순위</th>
									<td>
										<input type="text" id="txtSortOrder" name="sortOrder" class="input type2 f_left" style="width:95%; margin:3px 5px !important;" />
									</td>
								</tr>
								<tr>
									<th style="width:20% !important; font-weight:bold !important;">단위</th>
									<td>
										<div class="table_combobox" style="margin:3px 5px !important;" id="cboUseUnit" name="useUnit"></div>
									</td>
									<th style="width:20% !important; font-weight:bold !important;">입력구분</th>
									<td>
										<div class="table_combobox" style="margin:3px 5px !important;" id="cboColSystem" name="colSystem"></div>
									</td>
								</tr>
								<tr>
									<th style="width:20% !important; font-weight:bold !important;">극성</th>
									<td>
										<div class="table_combobox" style="margin:3px 5px !important;" id="cboKpiDir" name="kpiDir"></div>
									</td>
									<th style="width:20% !important; font-weight:bold !important;">사용여부</th>   
									<td>  
										<div class="table_combobox" style="margin:3px 5px !important;" id="cboUseYn" name="useYn"></div>
									</td>
								</tr>
								<tr>
									<th style="width:20% !important; font-weight:bold !important;">담당자</th>
									<td>
										<input type="text" id="txtOwnerUser" name="ownerUser" class="input type2 f_left" style="width:95%; margin:3px 5px !important;" />
									</td>
									<th style="width:20% !important; font-weight:bold !important;">담당부서</th>
									<td>
										<input type="text" id="txtDivision" name="division" class="input type2 f_left" style="width:95%; margin:3px 5px !important;" />
									</td>
								</tr>
								<tr>
									<th style="width:20% !important; font-weight:bold !important;">사용시작일</th>
									<td>
										<input type="text" id="txtSDate" name="sDate" class="input type2 f_left" style="width:95%; margin:3px 5px !important;" />
									</td>
									<th style="width:20% !important; font-weight:bold !important;">사용종료일</th>
									<td>
										<input type="text" id="txtEDate" name="eDate" class="input type2 f_left" style="width:95%; margin:3px 5px !important;" />
									</td>
								</tr>
								<tr>
									<th style="width:20% !important; font-weight:bold !important;">KPI_UDC1</th>
									<td>
										<input type="text" id="txtKpiUdc1" name="kpiUdc1" class="input type2 f_left" style="width:95%; margin:3px 5px !important;" />
									</td>
									<th style="width:20% !important; font-weight:bold !important;">KPI_UDC2</th>
									<td>
										<input type="text" id="txtKpiUdc2" name="kpiUdc2" class="input type2 f_left" style="width:95%; margin:3px 5px !important;" />
									</td>
								</tr>
								<tr>
									<th style="width:20% !important; font-weight:bold !important;">KPI_UDC3</th>
									<td>
										<input type="text" id="txtKpiUdc3" name="kpiUdc3" class="input type2 f_left" style="width:95%; margin:3px 5px !important;" />
									</td>
									<th style="width:20% !important; font-weight:bold !important;">KPI_UDC4</th>
									<td>
										<input type="text" id="txtKpiUdc4" name="kpiUdc4" class="input type2 f_left" style="width:95%; margin:3px 5px !important;" />
									</td>
								</tr>
								<tr>
									<th style="width:20% !important; font-weight:bold !important;">KPI_UDC5</th>
									<td>
										<input type="text" id="txtKpiUdc5" name="kpiUdc5" class="input type2 f_left" style="width:95%; margin:3px 5px !important;" />
									</td>
									<th style="width:20% !important; font-weight:bold !important;"></th>
									<td>
									</td>
								</tr>
							</table>
						</div>
						<div class="f_right m_t5">
							<div class="button type2 f_left">
								<input type="button" value="신규" id='btnNew' width="100%" height="100%" onclick="newData();" />
							</div>
							<div class="button type2 f_left">
								<input type="button" value="저장" id='btnSave' width="100%" height="100%" onclick="insert();" />
							</div>
							<div class="button type3 f_left">
								<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();" />
							</div>
						</div>
					</form>
				</div> 
			</div>
		</div>
	</body>
</html>