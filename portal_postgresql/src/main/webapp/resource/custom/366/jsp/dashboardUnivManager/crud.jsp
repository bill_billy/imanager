<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>대시보드 비교대학관리</title>
	<link rel="stylesheet" type="text/css" href="../../../../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	<link rel="stylesheet" type="text/css" href="../../css/style.blueish.css"/>
	<link rel="stylesheet" type="text/css" href="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	<link rel="stylesheet" type="text/css" href="../../css/jqx.blueish.css"/>
    <script src="../../../../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
    <script src="../../../../../resources/js/jquery/jquery.fixheadertable.js"></script>
    <script src="../../../../../resources/js/jquery/jquery.fixheadertable_v3.js"></script>
    <script src="../../../../../resources/js/jquery/jquery-ui-1.9.1.custom.js"></script>
    <script src="../../../../../resources/cmresource/js/jquery/jquery.form.min.js"></script>
    <script src="../../../../../resources/js/jquery/jquery.cookie.js"></script>
    <script src="../../../../../resources/js/util/hashMap.js"></script>
    <script src="../../../../../resources/js/util/json2xml.js"></script>
    <script src="../../../../../resources/js/util/xml2json.js"></script>
    <script src="../../../../../resources/cmresource/js/thirdparty/MSPL/linq/linq.min.js"></script>
    <script src="../../../../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
    <script src="../../../../../resources/cmresource/js/iplanbiz/web/ui.alert.js"></script>
    <script src="../../../../../resources/cmresource/js/iplanbiz/web/jquery.extension.table.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcore.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxangular.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbargauge.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbulletchart.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbuttongroup.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxbuttons.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscrollbar.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscrollview.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtabs.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtagcloud.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtree.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxpanel.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxpasswordinput.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxpopover.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxexpander.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxmenu.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnavbar.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdata.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdata.export.js"></script> 
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdatatable.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxinput.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxlistbox.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxlistmenu.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcheckbox.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcombobox.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcomplexinput.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxradiobutton.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxrating.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxrangeselector.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdropdownbutton.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdropdownlist.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.sort.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.storage.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.selection.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.columnsresize.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.columnsreorder.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.pager.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.edit.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.export.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.aggregates.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.filter.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgrid.grouping.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtreegrid.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtreemap.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtooltip.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtouch.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcalendar.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdatetimeinput.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxsplitter.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.rangeselector.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.waterfall.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.annotations.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.api.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.core.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdraw.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxgauge.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcolorpicker.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnavigationbar.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnotification.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxnumberinput.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxmaskedinput.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxswitchbutton.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxprogressbar.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxslider.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxsortable.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxwindow.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdocking.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdockinglayout.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdockpanel.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdragdrop.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxresponse.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxresponsivepanel.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxribbon.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxeditor.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxfileupload.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxformattedinput.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtextarea.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxtoolbar.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscheduler.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxscheduler.api.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxkanban.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxknob.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxknockout.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxlayout.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxloader.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxvalidator.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdate.js"></script>
	<script src="../../../../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/globalization/globalize.js"></script>
	<script src="../../../../../resources/js/jquery/jquery-ui-tabs-paging.js"></script>
	<script src="../../../../../resources/js/util/common.js"></script>
	<script src="../../../../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
	<script>
		$i.config.contexturi=function() {
			return '../../../../..';
		};
	</script>
	<script type="text/javascript">
		var menudata = null;
		$(document).ready(function(){
			init();
		});
		function init(){
			
			$.ajax({
				type: 'POST',
				url: '../../js/query.univManager.jsp',
				data: 'programid=getDashboardUnivScGubn',
				async : false,
				success: function(data) {
					$('#spanScnm').text(data.returnArray[0].SC_NM);
					$('input[name="scID"]').val(data.returnArray[0].SC_ID);
					$('input[name="univGrp1"]').val(data.returnArray[0].UNIV_GRP1);
				}
			});

			$("#btnSave").jqxButton({ width: '', theme: 'blueish' });
			$("#btnDelete").jqxButton({ width: '', theme: 'blueish' });
			$("#btnReset").jqxButton({ width: '', theme: 'blueish' });
			$("#btnSearch").jqxButton({ width: '', theme: 'blueish' });
			$("#btnUnivSearch").jqxButton({ width: '', theme: 'blueish'});
			
			search();
		}
		function search(){
			$("#gridDashboardUnivList").jqxGrid('clearselection');
			var kpiListData = $i.post("../../js/query.univManager.jsp", {programid:'getDashboardUnivList', scID:$("#hiddenScID").val()});
			kpiListData.done(function(data){
				var source =
	            {
	                datatype: "json",
	                datafields: [
	                    { name: 'UNIV_ID', type: 'string'},
	                    { name: 'UNIV_NM', type: 'string'},
	                    { name: 'UNIV_GRP1', type: 'string'},
	                    { name: 'SC_ID', type: 'string'}
	                ],
	                localdata: data.returnArray,
	                updaterow: function (rowid, rowdata, commit) {
	                    commit(true);
	                }
	            };
	            var alginCenter = function (row, columnfield, value) {//left정렬
					return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
				}; 
				var alginLeft = function (row, columnfield, value) {//left정렬
					return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
				}; 
				var alginRight = function (row, columnfield, value) {//right정렬
					return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
				};
	            var dataAdapter = new $.jqx.dataAdapter(source, {
	                downloadComplete: function (data, status, xhr) { },
	                loadComplete: function (data) { },
	                loadError: function (xhr, status, error) { }
	            });
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#gridDashboardUnivList").jqxGrid(
	            {
	              	width: '100%',
	                height: '100%',
					altrows:true,
					pageable: true,
					sortable: true,
	                source: dataAdapter,
	                pagesize: 100,
					pagesizeoptions:['100', '200', '300'],
					theme:'blueish', 
	                columnsresize: true,
	                selectionmode:'checkbox',
	                columns: [
	                   { text: '대학ID', datafield: 'UNIV_ID', width:'28%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
					   { text: '대학명', datafield: 'UNIV_NM', width:'70%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
	                ]
	            });
			});
		}
		function insert(){
			if($("#hiddenUnivID").val() == ""){
				$i.dialog.error("SYSTEM", "대학을 선택하세요");
				return false;
			}
			/*
			$i.insert("#saveForm").done(function(args){
				if(args.returnCode == "EXCEPTION"){
					$i.dialog.error("SYSTEM", args.returnMessage);
				}else{
					$i.dialog.alert("SYSTEM", args.returnMessage, function(){
						search();
						resetForm();
					});
				}
			});
			*/
			$.ajax({
				type: 'post',
				url: '../../js/query.univManager.jsp',
				data: $('#saveForm').serialize()+'&programid=insert',
				success: function(args) {
					
					if(args.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", args.returnMessage);
					}else{
						$i.dialog.alert("SYSTEM", args.returnMessage, function(){
							search();
							resetForm();
						});
					}
				},
				error: function(args) {
					$i.dialog.error("SYSTEM", "오류가 발생했습니다.");
				}
			});
		}
		function remove(){
			var checkRows = $("#gridDashboardUnivList").jqxGrid("getselectedrowindexes");
			var gridRows = $("#gridDashboardUnivList").jqxGrid("getRows");
			if(checkRows.length == 0){
				$i.dialog.error("SYSTEM", "삭제 할 대학을 선택하세요");
				return false;
			}
			if(checkRows.length> 0){
				var html = "";
				for(var i=0;i<checkRows.length;i++){
					html += "<input type='hidden' name='univID' value='"+gridRows[checkRows[i]].UNIV_ID+"' />";
				}
				$("#deleteForm").append(html);
			}
			/*
			$i.dialog.confirm("SYSTEM", "삭제하시겠습니까?", function(){
				$i.remove("#deleteForm").done(function(args){
					if(args.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", args.returnMessage);
					}else{
						$i.dialog.alert("SYSTEM", args.returnMessage, function(){
							search();
							resetForm();
							$("#deleteForm").find("[name='univID']").remove();
						});
					}
				});
			});
			*/
			$i.dialog.confirm("SYSTEM", "삭제하시겠습니까?",function(){
				$.ajax({
					type: 'post',
					url: '../../js/query.univManager.jsp',
					data: $('#deleteForm').serialize()+'&programid=remove',
					success: function(args) {
						if(args.returnCode == "EXCEPTION"){
							$i.dialog.error("SYSTEM", args.returnMessage);
						}else{
							$i.dialog.alert("SYSTEM", args.returnMessage, function(){
								search();
								resetForm();
								$("#deleteForm").find("[name='univID']").remove();
							});
						}
					},
					error: function(args) {
						$i.dialog.error("SYSTEM", "오류가 발생했습니다.");
					}
				});
			});
		}
		function univSearch(){
			window.open('./popupMappingDashboardUniv.jsp', 'popupMappingDashboardUniv','scrollbars=no, resizable=no, width=630, height=620');
		}
		function resetForm(){
			$("#hiddenUnivID").val("");
			$("#labelUnivID").html("");
			$("#labelUnivName").html("");
		}
	</script>
</head>
<body class='blueish'>
	<div class="wrap" style="width:98%; min-width:1160px; margin:0 10px;">
		<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
			<div class="label type1 f_left">평가구분 : <span id="spanScnm"></span></div>
			<div class="group_button f_right">                                                                                                                                                                                                                                            
				<div class="button type1 f_left">
					<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="searchStart();" />
				</div>
				<div class="button type3 f_left">
					<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();" />
				</div>
			</div>
		</div>
		<div class="container  f_left" style="width:100%; margin:10px 0;">
			<div class="content f_left" style=" width:100%; margin-right:1%;">
				<form id="saveForm" name="saveForm" action="./insert">
					<input type="hidden" id="hiddenScID" name="scID" value="" />
					<input type="hidden" id="hiddenUnivGrp1" name="univGrp1" value="" />
					<div class="grid f_left" style="width:100%; height:500px;">	
						<div id="gridDashboardUnivList"> </div>
					</div>
					<div class="blueish table f_left"  style="width:100%; height:; margin:10px 0;">
						<table  width="100%" cellspacing="0" cellpadding="0" border="0">
							<thead>
								<tr>
									<th scope="col" style="width:40%; height:40px;">대학ID</th>
									<th scope="col" style="width:60%; height:40px;">대학명</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<div class="cell">
											<input type="hidden" id="hiddenUnivID" name="univID" class="input type2  f_left  m_r10"  style="width:95%; margin:3px 0; "/>
											<span id="labelUnivID" style="float:left; margin:5px 0 0 0 ;"></span>
										</div>
									</td>
									<td>
										<div class="cell">
											<span id="labelUnivName" style="float:left; margin:5px 0 0 0 ;"></span>
										</div>
										<div class="group_button f_right">
											<div class="button type2 f_left">
												<input type="button" value="검색" id='btnUnivSearch' width="100%" height="100%" onclick="univSearch();" />
											</div>
											<div class="button type2 f_left">
												<input type="button" value="초기화" id='btnReset' width="100%" height="100%" onclick="resetForm();" />
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="group_button f_right">
						<div class="button type2 f_left">
							<input type="button" value="저장" id='btnSave' width="100%" height="100%" onclick="insert();" />
						</div>
					</div>
				</form>
				<form id="deleteForm" name="deleteForm">
					<input type="hidden" name="scID" value="" />
				</form>
			</div>
		</div>          
	</div>
</body>
</html>