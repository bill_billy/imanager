﻿<%@page import="com.iplanbiz.comm.Utils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ page contentType="application/json; charset=UTF-8" %>

<%@ page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@ page import="org.springframework.context.ApplicationContext"%>
<%@ page import="org.springframework.web.context.WebApplicationContext"%>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ page import="org.springframework.beans.factory.ObjectFactory"%>
<%@ page import="org.json.simple.JSONArray"%>
<%@ page import="org.json.simple.JSONObject"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Enumeration"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.net.InetAddress"%>
<%@ page import="java.io.*" %>
<%@ page import="java.lang.Exception"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="com.iplanbiz.core.io.dbms.DBMSService"%>
<%@ page import="com.iplanbiz.core.session.LoginSessionInfo"%>
<%@ page import="com.iplanbiz.iportal.comm.session.SessionManager"%>
<%

	Logger logger = LoggerFactory.getLogger(this.getClass());

	InetAddress inet = InetAddress.getLocalHost();
	String svrIP = inet.getHostAddress();
	JSONArray jarray = new JSONArray();
	JSONObject returnobj = new JSONObject();
	
	String programid = "";
	programid = request.getParameter("programid").toString();
	
	try{

		WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(((HttpServletRequest) request).getSession().getServletContext(),org.springframework.web.servlet.FrameworkServlet.SERVLET_CONTEXT_PREFIX + "SpringDispatcher");
		DBMSService service = (DBMSService)wac.getBean("dbmsService");
		LoginSessionInfo loginSessionInfo = (LoginSessionInfo)session.getAttribute("loginSessionInfo");
		String userid = loginSessionInfo.getUserId();
		
		String query = "";
		String dbkey = "CUSTOM_TIBERO";
		String acctid = "366";
		List<LinkedHashMap<String, Object>> list = null;
		
		if(programid.equals("getDashboardUnivList")) {
			query = "SELECT A.SC_ID, A.UNIV_ID, B.UNIV_NM, A.UNIV_GRP1 " +
					"FROM IVFE1001 A " +
					"INNER JOIN IECT0007 B ON A.UNIV_ID = B.UNIV_ID " +
					"WHERE 1=1 " +
					"AND A.ACCT_ID = " + acctid + " " +
					"AND A.UNIV_GRP1 = (SELECT SF_IECT0008() FROM DUAL) " +
					"AND A.SC_ID = " + request.getParameter("scID");
		}
		else if(programid.equals("getDashboardUnivScGubn")) {
			query = "SELECT SC_ID, SC_NM, UNIV_GRP1 " +
					"FROM IVFE1002 " +
					"WHERE UNIV_GRP1 = (SELECT SF_IECT0008() FROM DUAL) " +
					"AND SC_ID IN ('107', '108') ";
		}
		else if(programid.equals("getDashboardPopupUnivList")) {
			query = "SELECT UNIV_ID, UNIV_NM, B.COM_NM AS UNIV_GRP_NM, C.COM_NM AS UNIV_GRP1_NM, D.COM_NM AS UNIV_GRP2_NM " +
					"FROM IECT0007 A " +
					"INNER JOIN IECT0004 B ON B.COML_COD = 'UNIV_GRP' AND A.UNIV_GRP = B.COM_COD " +
					"INNER JOIN IECT0004 C ON C.COML_COD = 'UNIV_GRP1' AND A.UNIV_GRP1 = C.COM_COD " +
					"INNER JOIN IECT0004 D ON D.COML_COD = 'UNIV_GRP2' AND A.UNIV_GRP2 = D.COM_COD " +
					"WHERE 1=1 " +
					"AND UNIV_GRP1 = (SELECT SF_IECT0008() FROM DUAL) " +
					"AND UNIV_NM LIKE '%'||'"+request.getParameter("univName")+"'||'%' ";
		}
		
		//execute - start
		//insert
		if(programid.equals("insert")) {
			query = "INSERT INTO IVFE1001(ACCT_ID, UNIV_GRP1, SC_ID, UNIV_ID, INSERT_DAT, INSERT_EMP) " + 
					" VALUES ( " + 
					"	" + acctid + ", " + 
					"	'"+request.getParameter("univGrp1")+"', " +							
					"	'"+request.getParameter("scID")+"', " + 
					"	"+request.getParameter("univID")+", " + 
					"	SYSDATE,  " + 
					"	'" + userid + "' " + 
					") ";
			
			service.executeUpdate(query, dbkey);
			
			returnobj.put("returnMessage", "저장 되었습니다.");
				
		}
		
		//delete
		else if(programid.equals("remove")) {
			
			String scID = request.getParameter("scID");
			String[] univID = request.getParameterValues("univID");
			
			for(int i=0; i<univID.length; i++) {
				query = "DELETE FROM IVFE1001 WHERE ACCT_ID = "+acctid+" AND SC_ID = "+scID+" AND UNIV_ID = "+univID[i];
				
				service.executeUpdate(query, dbkey);
			}
			
			returnobj.put("returnMessage", "삭제 되었습니다.");
			
		}
		//select
		else {
			list = service.executeQuery(query, dbkey);
			
			for(int i = 0; i<list.size();i++){
				JSONObject json = new JSONObject();
				
				for(Map.Entry<String,Object> entry : list.get(i).entrySet()) {
					String key = entry.getKey();
		            Object value = entry.getValue();
		            String strValue = "";
		            if(value!=null) strValue = value.toString();
		            
		            json.put(key,strValue);
				}
				jarray.add(json);
			}
			
			returnobj.put("returnArray", jarray);
		}
		//execute - end
				
	}catch(Exception e){
		returnobj.put("returnCode", "EXCEPTION");
		returnobj.put("returnMessage", "오류가 발생했습니다." + e.getMessage());
		
		e.printStackTrace();
	}
%><%=returnobj.toJSONString()%>