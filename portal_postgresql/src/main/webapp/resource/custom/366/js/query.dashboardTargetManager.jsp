﻿<%@page import="com.iplanbiz.comm.Utils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ page contentType="application/json; charset=UTF-8" %>

<%@ page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@ page import="org.springframework.context.ApplicationContext"%>
<%@ page import="org.springframework.web.context.WebApplicationContext"%>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ page import="org.springframework.beans.factory.ObjectFactory"%>
<%@ page import="org.json.simple.JSONArray"%>
<%@ page import="org.json.simple.JSONObject"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Enumeration"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.net.InetAddress"%>
<%@ page import="java.io.*" %>
<%@ page import="java.lang.Exception"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="com.iplanbiz.core.io.dbms.DBMSService"%>
<%@ page import="com.iplanbiz.core.session.LoginSessionInfo"%>
<%@ page import="com.iplanbiz.iportal.comm.session.SessionManager"%>
<%

	Logger logger = LoggerFactory.getLogger(this.getClass());

	InetAddress inet = InetAddress.getLocalHost();
	String svrIP = inet.getHostAddress();
	JSONArray jarray = new JSONArray();
	JSONObject returnobj = new JSONObject();
	
	String programid = "";
	programid = request.getParameter("programid").toString();
	
	try{

		WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(((HttpServletRequest) request).getSession().getServletContext(),org.springframework.web.servlet.FrameworkServlet.SERVLET_CONTEXT_PREFIX + "SpringDispatcher");
		DBMSService service = (DBMSService)wac.getBean("dbmsService");
		LoginSessionInfo loginSessionInfo = (LoginSessionInfo)session.getAttribute("loginSessionInfo");
		String userid = loginSessionInfo.getUserId();
		
		String query = "";
		String dbkey = "CUSTOM_TIBERO";
		String acctid = "339";
		List<LinkedHashMap<String, Object>> list = null;
		
		logger.info("programid:"+programid);
		if(programid.equals("getDashboardCodeList")) {
			query = "SELECT COM_COD, COM_NM " +
					"FROM IDFE0001 " +
					"WHERE COML_COD = '"+request.getParameter("comlCod")+"' " +
					"AND USE_YN = 'Y' " +
					"ORDER BY SORT_ORDER ";	
		}
		else if(programid.equals("groupCode")){
			query = "SELECT COM_COD,COM_NM FROM IECT0004 " + 
		            "WHERE COML_COD = '"+request.getParameter("comlCod")+"' " +
					"AND   USE_YN = 'Y' " +
		            "ORDER BY SORT_ORDER";
		}
		else if(programid.equals("getDashboardKpiList")){
			query = "SELECT KPI_ID AS COML_COD, KPI_NM AS COML_NM, B.COM_NM AS KPI_DIR, C.COM_NM AS USE_UNIT " +
		            "FROM IDFE0100 A LEFT OUTER JOIN IDFE0001 B ON B.COML_COD = 'KPI_DIR' AND A.KPI_DIR = B.COM_COD " +
					"                LEFT OUTER JOIN IDFE0001 C ON C.COML_COD = 'USEUNIT' AND A.USE_UNIT = C.COM_COD " +
		            "WHERE 1=1 " +
					"AND A.ACCT_ID = " + acctid + " "+
					"AND A.KPI_GBN = '"+request.getParameter("kpiGbn")+"' " +
		            "AND A.KPI_NM LIKE '%'||'"+request.getParameter("comlNm")+"'||'%' " +
					"ORDER BY A.SORT_ORDER ASC";
		}
		else if(programid.equals("getDashboardKpiTargerValueList")){
			query = "SELECT KPI_ID AS COML_COD, YYYY AS COM_COD, YYYY AS COM_NM,'' SORT_ORDER,USE_YN, BIGO, TGT_VALUE AS COM_UDC1 " +
		            "FROM IDFE0005 " +
					"WHERE 1=1 " +
		            "AND   KPI_ID = '"+request.getParameter("comlCod")+"' " +
					"ORDER BY YYYY ASC" ;
		}
		else if(programid.equals("getDashboardKpiTargetValueOne")){
			query = "SELECT KPI_ID AS COML_COD, YYYY AS COM_COD, YYYY AS COM_NM,'' SORT_ORDER,USE_YN, BIGO, TGT_VALUE AS COM_UDC1 " +
		            "FROM IDFE0005 " +
					"WHERE 1=1 " +
		            "AND   KPI_ID = '"+request.getParameter("comlCod")+"' " +
					"AND   YYYY = '"+request.getParameter("comCod")+"' "+
					"ORDER BY YYYY ASC" ;
		}
		//execute - start
		//insert
		if(programid.equals("insert")) {
		
			String orgComCod = Utils.getString(request.getParameter("orgComCod"), "");
			
			if(orgComCod.equals("")) {
				
				query = "SELECT COALESCE(MAX(CAST(KPI_ID AS INTEGER))+1, 100) AS KPI_ID " +
						"FROM IDFE0100 " +
						"WHERE ACCT_ID = " + acctid + " ";
				
				
				query = "INSERT INTO IDFE0005(KPI_ID, YYYY, USE_YN, TGT_VALUE, INSERT_DAT, INSERT_EMP, BIGO) "+
				        "VALUES('"+request.getParameter("comlCod")+"','"+request.getParameter("comCod")+"','"+request.getParameter("useYn")+"','"+request.getParameter("comUdc1")+"',SYSDATE,'"+userid+"','"+request.getParameter("bigo")+"')";
				
				service.executeUpdate(query, dbkey);
				// IDFE0100 - END
				
				returnobj.put("returnMessage", "저장 되었습니다.");
				
			} else {
				query = "UPDATE IDFE0005 " + 
						"SET YYYY = '"+request.getParameter("comCod")+"' " +
						"    ,USE_YN = '"+request.getParameter("useYn")+"' " +
						"    ,TGT_VALUE = '"+request.getParameter("comUdc1")+"' " +
						"    ,UPDATE_DAT = SYSDATE" +
						"    ,UPDATE_EMP = '"+userid+"' " +
						"    ,BIGO = '"+request.getParameter("bigo")+"' " +
						"WHERE KPI_ID = '"+request.getParameter("comlCod")+"' " +
						"AND   YYYY = '"+orgComCod+"'";
				
				service.executeUpdate(query, dbkey);
				//IDFE0100 UPDATE - END
				
				returnobj.put("returnMessage", "수정 되었습니다.");
			}
		}
		//delete
		else if(programid.equals("remove")) {
			String orgComCod = request.getParameter("orgComCod");
			
			for(int i=0;i<orgComCod.split(",").length;i++){
				query = "DELETE FROM IDFE0005 WHERE YYYY = '"+orgComCod.split(",")[i]+"' AND KPI_ID = '"+request.getParameter("comlCod")+"'";
				service.executeUpdate(query, dbkey);	
			}
			returnobj.put("returnMessage", "삭제 되었습니다.");
		}
		//select
		else {
			list = service.executeQuery(query, dbkey);
			
			for(int i = 0; i<list.size();i++){
				JSONObject json = new JSONObject();
				
				for(Map.Entry<String,Object> entry : list.get(i).entrySet()) {
					String key = entry.getKey();
		            Object value = entry.getValue();
		            String strValue = "";
		            if(value!=null) strValue = value.toString();
		            
		            json.put(key,strValue);
				}
				jarray.add(json);
			}
			
			returnobj.put("returnArray", jarray);
		}
		//execute - end
				
	}catch(Exception e){
		returnobj.put("returnCode", "EXCEPTION");
		returnobj.put("returnMessage", "오류가 발생했습니다." + e.getMessage());
		
		e.printStackTrace();
	}
%><%=returnobj.toJSONString()%>