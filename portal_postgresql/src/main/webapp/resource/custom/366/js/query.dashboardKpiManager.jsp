﻿<%@page import="com.iplanbiz.comm.Utils"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ page contentType="application/json; charset=UTF-8" %>

<%@ page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@ page import="org.springframework.context.ApplicationContext"%>
<%@ page import="org.springframework.web.context.WebApplicationContext"%>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ page import="org.springframework.beans.factory.ObjectFactory"%>
<%@ page import="org.json.simple.JSONArray"%>
<%@ page import="org.json.simple.JSONObject"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Enumeration"%>
<%@ page import="java.util.LinkedHashMap"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.net.InetAddress"%>
<%@ page import="java.io.*" %>
<%@ page import="java.lang.Exception"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="com.iplanbiz.core.io.dbms.DBMSService"%>
<%@ page import="com.iplanbiz.core.session.LoginSessionInfo"%>
<%@ page import="com.iplanbiz.iportal.comm.session.SessionManager"%>
<%

	Logger logger = LoggerFactory.getLogger(this.getClass());

	InetAddress inet = InetAddress.getLocalHost();
	String svrIP = inet.getHostAddress();
	JSONArray jarray = new JSONArray();
	JSONObject returnobj = new JSONObject();
	
	String programid = "";
	programid = request.getParameter("programid").toString();
	
	try{

		WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(((HttpServletRequest) request).getSession().getServletContext(),org.springframework.web.servlet.FrameworkServlet.SERVLET_CONTEXT_PREFIX + "SpringDispatcher");
		DBMSService service = (DBMSService)wac.getBean("dbmsService");
		LoginSessionInfo loginSessionInfo = (LoginSessionInfo)session.getAttribute("loginSessionInfo");
		String userid = loginSessionInfo.getUserId();
		
		String query = "";
		String dbkey = "CUSTOM_TIBERO";
		String acctid = "366";
		List<LinkedHashMap<String, Object>> list = null;
		
		logger.info("programid:"+programid);
		if(programid.equals("getDashboardCodeList")) {
			query = "SELECT COM_COD, COM_NM " +
					"FROM IDFE0001 " +
					"WHERE COML_COD = '"+request.getParameter("comlCod")+"' " +
					"AND USE_YN = 'Y' " +
					"ORDER BY SORT_ORDER ";	
		}
		else if(programid.equals("getDashboardKpiList")) { 
			query = "SELECT A.KPI_ID " +
					", A.KPI_NM " +
					", B.COM_NM AS USE_NM " +
					", A.USE_UNIT " +
					", A.KPI_DIR " +
					", C.COM_NM AS KPI_DIR_NM " +
					", A.USE_YN " +
					"FROM IDFE0100 A LEFT OUTER JOIN IDFE0001 B ON B.COML_COD = 'USEUNIT' AND A.USE_UNIT = B.COM_COD " +
					"                LEFT OUTER JOIN IDFE0001 C ON C.COML_COD = 'KPI_DIR' AND A.KPI_DIR = C.COM_COD " +
					"WHERE 1=1 " +
					"AND      A.ACCT_ID = " + acctid + " " +
					"AND      A.KPI_GBN = '"+request.getParameter("kpiGbn")+"' " +
					"ORDER BY KPI_NM ASC ";
		}
		else if(programid.equals("getDashboardKpiDetail")) {
			query = "SELECT A.KPI_ID " +
					"        , D.COM_COD AS KPI_GBN " +
					"        , A.KPI_NM " +
					"        , A.ETC_DESC " +
					"        , A.KPI_DESC " +
					"        , A.DATA_START_DAT " +
					"        , A.DATA_END_DAT " +
					"        , A.SORT_ORDER " +
					"        , C.COM_NM AS MEAS_NM " +
					"        , A.MEAS_CYCLE " +
					"        , B.COM_NM AS USE_NM " +
					"        , A.USE_UNIT " +
					"        , A.COL_SYSTEM " +
					"        , E.COM_NM AS COL_NM " +
					"        , A.KPI_DIR " +
					"        , F.COM_NM AS KPI_DIR_NM " +
					"        , A.OWNER_USER_ID " +
					"        , A.START_DAT " +
					"        , A.END_DAT " +
					"        , A.USE_YN " +
					"        , A.KPI_UDC1 " +
					"        , A.KPI_UDC2 " +
					"        , A.KPI_UDC3 " +
					"        , A.KPI_UDC4 " +
					"        , A.KPI_UDC5 " +
					"        , TO_CHAR(A.SQL_DATA) AS SQL_DATA " +
					"FROM IDFE0100 A LEFT OUTER JOIN IDFE0001 B ON B.COML_COD = 'USEUNIT' AND A.USE_UNIT = B.COM_COD " +
					"                LEFT OUTER JOIN IDFE0001 C ON C.COML_COD = 'MEAS_CYCLE' AND A.MEAS_CYCLE = C.COM_COD " +
					"                INNER JOIN IDFE0001 D ON D.COML_COD = 'KPI_GBN' AND A.KPI_GBN = D.COM_COD " +
					"                LEFT OUTER JOIN IDFE0001 E ON E.COML_COD = 'COL_SYSTEM' AND A.COL_SYSTEM = E.COM_COD " +
					"                LEFT OUTER JOIN IDFE0001 F ON F.COML_COD = 'KPI_DIR' AND A.KPI_DIR = F.COM_COD " +
					"WHERE 1=1 " +
					"AND A.ACCT_ID = " + acctid + " " +
					"AND A.KPI_ID = '" + request.getParameter("kpiID") + "' " +
					"ORDER BY KPI_NM ASC ";
		}
		else if(programid.equals("getDashboardCheckMm")) {
			query = "SELECT DISP_MM " +
					"FROM IDFE0101 " +
					"WHERE ACCT_ID = " + acctid + " " +
					"AND      KPI_ID = '" + request.getParameter("kpiID") + "' ";
		}
		
		//execute - start
		//insert
		if(programid.equals("insert")) {
		
			String kpiID = Utils.getString(request.getParameter("kpiID"), "");
			
			if(kpiID.equals("")) {
				
				query = "SELECT COALESCE(MAX(CAST(KPI_ID AS INTEGER))+1, 100) AS KPI_ID " +
						"FROM IDFE0100 " +
						"WHERE ACCT_ID = " + acctid + " ";
				
				list = service.executeQuery(query, dbkey);
				
				kpiID =  String.valueOf(list.get(0).get("KPI_ID"));
				
				// IDFE0100 - START
				query = "INSERT INTO IDFE0100 " +
						"( " +
						"	ACCT_ID, " +
						"	KPI_ID, " +
						"	KPI_NM, " +
						"	KPI_DESC, " +
						"	ETC_DESC, " +					
						"	KPI_GBN, " +
						"	DATA_START_DAT, " +
						"	DATA_END_DAT, " +
						"	OWNER_USER_ID, " +
						"	MEAS_CYCLE, " +
						"	COL_SYSTEM, " +
						"	USE_UNIT, " +
						"	SORT_ORDER, " +
						"	START_DAT, " +
						"	END_DAT, " +
						"	KPI_UDC1, " +
						"	KPI_UDC2, " +
						"	KPI_UDC3, " +
						"	KPI_UDC4, " +
						"	KPI_UDC5, " +
						"	KPI_DIR, " +
						"	USE_YN, " +
						"	SQL_DATA, " +
						"	INSERT_DAT, " +
						"	INSERT_EMP " +
						") VALUES ( " +
						"	"+acctid+", " +
						"	'"+kpiID+"', " +
						"	'"+request.getParameter("kpiName")+"', " +
						"	'"+request.getParameter("bigo")+"', " +
						"	'"+request.getParameter("sol")+"', " +
						"	'"+request.getParameter("kpiGbn")+"', " +
						"	'"+request.getParameter("startDat")+"', " +
						"	'"+request.getParameter("endDat")+"', " +
						"	'"+request.getParameter("ownerUser")+"', " +
						"	'"+request.getParameter("measCycle")+"', " +
						"	'"+request.getParameter("colSystem")+"', " +
						"	'"+request.getParameter("useUnit")+"', " +
						"	'"+request.getParameter("sortOrder")+"', " +
						"	'"+request.getParameter("sDate")+"', " +
						"	'"+request.getParameter("eDate")+"', " +
						"	'"+request.getParameter("kpiUdc1")+"', " +
						"	'"+request.getParameter("kpiUdc2")+"', " +
						"	'"+request.getParameter("kpiUdc3")+"', " +
						"	'"+request.getParameter("kpiUdc4")+"', " +
						"	'"+request.getParameter("kpiUdc5")+"', " +
						"	'"+request.getParameter("kpiDir")+"', " +
						"	'"+request.getParameter("useYn")+"', " +
						"	'"+request.getParameter("sql")+"', " +
						"	SYSDATE, " +
						"	'"+userid+"' " +
						") ";
				
				service.executeUpdate(query, dbkey);
				// IDFE0100 - END
				
				returnobj.put("returnMessage", "저장 되었습니다.");
				
			} else {

				//IDFE0100 UPDATE - START
				query = "UPDATE IDFE0100 SET " +
						"  KPI_NM = '"+request.getParameter("kpiName")+"'" +
						", KPI_DESC = '"+request.getParameter("bigo")+"'" +
						", ETC_DESC = '"+request.getParameter("sol")+"'" +					
						", KPI_GBN = '"+request.getParameter("kpiGbn")+"'" +
						", DATA_START_DAT = '"+request.getParameter("startDat")+"'" +
						", DATA_END_DAT = '"+request.getParameter("endDat")+"'" +
						", OWNER_USER_ID = '"+request.getParameter("ownerUser")+"'" +
						", MEAS_CYCLE = '"+request.getParameter("measCycle")+"'" +
						", COL_SYSTEM = '"+request.getParameter("colSystem")+"'" +
						", USE_UNIT = '"+request.getParameter("useUnit")+"'" +
						", SORT_ORDER = '"+request.getParameter("sortOrder")+"'" +
						", START_DAT = '"+request.getParameter("sDate")+"'" +
						", END_DAT = '"+request.getParameter("eDate")+"'" +
						", KPI_UDC1 = '"+request.getParameter("kpiUdc1")+"'" +
						", KPI_UDC2 = '"+request.getParameter("kpiUdc2")+"'" +
						", KPI_UDC3 = '"+request.getParameter("kpiUdc3")+"'" +
						", KPI_UDC4 = '"+request.getParameter("kpiUdc4")+"'" +
						", KPI_UDC5 = '"+request.getParameter("kpiUdc5")+"'" +
						", KPI_DIR = '"+request.getParameter("kpiDir")+"'" +
						", USE_YN = '"+request.getParameter("useYn")+"'" +
						", SQL_DATA = '"+request.getParameter("sql")+"'" +
						", UPDATE_DAT = SYSDATE" +
						", UPDATE_EMP = '"+userid+"'" + 
						"WHERE 1 = 1 " + 
						"AND ACCT_ID = '"+acctid+"'" +
						"AND KPI_ID = '"+kpiID+"'";
				
				service.executeUpdate(query, dbkey);
				//IDFE0100 UPDATE - END
				
				returnobj.put("returnMessage", "수정 되었습니다.");
			}
			
			//IDFE0101 DELETE - START
			query = "DELETE FROM IDFE0101 " +
					"WHERE 1=1 " +
					"AND ACCT_ID = '"+acctid+"'" +
					"AND KPI_ID = '"+kpiID+"'";
			
			service.executeUpdate(query, dbkey);
			//IDFE0101 DELETE - END
			
			//IDFE0101 INSERT - START
			String[] mon = request.getParameterValues("mon");
			for(int i=0; mon != null && i<mon.length; i++ ) {
				query = "INSERT INTO IDFE0101(ACCT_ID, KPI_ID, DISP_MM, INSERT_DAT, INSERT_EMP) " +
						" VALUES ( "+ acctid+", '"+kpiID+"', '"+mon[i]+"', SYSDATE, '"+userid+"' )";
				
				service.executeUpdate(query, dbkey);
			}
			//IDFE0101 INSERT - END
			
			
		}
		//delete
		else if(programid.equals("remove")) {
			String kpiid = request.getParameter("kpiID");
			
			query = "DELETE FROM IDFE0100 WHERE ACCT_ID = " + acctid + " AND KPI_ID = '"+kpiid+"'";
			service.executeUpdate(query, dbkey);
			
			query = "DELETE FROM IDFE0101 WHERE ACCT_ID = " + acctid + " AND KPI_ID = '"+kpiid+"'";
			service.executeUpdate(query, dbkey);
			
			returnobj.put("returnMessage", "삭제 되었습니다.");
		}
		//select
		else {
			list = service.executeQuery(query, dbkey);
			
			for(int i = 0; i<list.size();i++){
				JSONObject json = new JSONObject();
				
				for(Map.Entry<String,Object> entry : list.get(i).entrySet()) {
					String key = entry.getKey();
		            Object value = entry.getValue();
		            String strValue = "";
		            if(value!=null) strValue = value.toString();
		            
		            json.put(key,strValue);
				}
				jarray.add(json);
			}
			
			returnobj.put("returnArray", jarray);
		}
		//execute - end
				
	}catch(Exception e){
		returnobj.put("returnCode", "EXCEPTION");
		returnobj.put("returnMessage", "오류가 발생했습니다." + e.getMessage());
		
		e.printStackTrace();
	}
%><%=returnobj.toJSONString()%>