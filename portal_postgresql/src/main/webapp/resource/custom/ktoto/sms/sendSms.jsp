<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@ page import="org.springframework.web.context.WebApplicationContext"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ page import="java.lang.Exception"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="org.springframework.beans.factory.ObjectFactory"%>
<%@ page language="java" import="java.net.InetAddress" %>
<%@ page import="com.iplanbiz.core.secure.GeneratePassword" %>
<%@ page import="com.iplanbiz.iportal.comm.session.SessionManager"%>
<%@ page import="com.iplanbiz.iportal.dao.PasswordDAO"%>
<%
WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(((HttpServletRequest) request).getSession().getServletContext(),org.springframework.web.servlet.FrameworkServlet.SERVLET_CONTEXT_PREFIX + "SpringDispatcher");
PasswordDAO passwordDAO = (PasswordDAO)wac.getBean("passwordDAO");
String newPwd = GeneratePassword.getPassword(); //임시비밀번호생성

JSONArray returnValue = new JSONArray();
String url="jdbc:oracle:thin:@192.168.12.110:1521:iplandb";
String user="iplan_v1";
String pwd="iplan1234";
if(request.getLocalAddr().equals(request.getRemoteAddr())==true) {
	String acctId = request.getParameter("acctId");
	String remoteIP = request.getParameter("remoteIP");
	String remoteOS = request.getParameter("remoteOS");
	String remoteBrowser= request.getParameter("remoteBrowser");
	
	String sendId = request.getParameter("sendId");//보내는사람id
	String sendName = request.getParameter("sendName");//보내는사람이름
	String sphone1 = request.getParameter("sphone1");//보내는사람핸드폰번호
	String sphone2 = request.getParameter("sphone2");//보내는사람핸드폰번호
	String sphone3 = request.getParameter("sphone3");//보내는사람핸드폰번호
	
	String recvId = request.getParameter("recvId");//받는사람id
	String recvName = request.getParameter("recvName");//받는사람이름
	String rphone1 = request.getParameter("rphone1");//받는사람핸드폰번호
	String rphone2 = request.getParameter("rphone2");//받는사람핸드폰번호
	String rphone3 = request.getParameter("rphone3");//받는사람핸드폰번호
	

	String msg = "임시비밀번호는 "+newPwd+"입니다.";//sms내용

	passwordDAO.initPassword(acctId, recvId, sendId, sendName, newPwd, remoteIP, remoteOS, remoteBrowser);
	
	int result = 0;
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	Boolean connect = false;
	try{
		Class.forName("oracle.jdbc.driver.OracleDriver");
		conn = DriverManager.getConnection(url, user, pwd);
		connect = true;
		
		String seqNo = "";
		String seqQuery = "SELECT TO_CHAR(NEXT_ID) AS NEXT_ID FROM IPLAN_V1.IDS WHERE TABLE_NAME=?";
		pstmt = conn.prepareStatement(seqQuery);
		pstmt.setString(1,"SMSDATA");
		rs = pstmt.executeQuery();   
		while(rs.next()){
			seqNo=rs.getString("NEXT_ID");
		}
		int no = 0;
		
		String query = "insert into IPLAN_V1.SMSDATA(SEQNO,INDATE,INTIME,SENDNAME,RPHONE1,RPHONE2,RPHONE3,RECVNAME,MSG,KIND,SPHONE1,SPHONE2,SPHONE3) "
		+"values(?,TO_CHAR(SYSDATE,'YYYYMMDD'),TO_CHAR(SYSDATE,'HH24MISS'),?,?,?,?,?,?,?,?,?,?)";
		 pstmt = conn.prepareStatement(query);
		 
	 if(seqNo==null||seqNo.equals("")){
		no = 0;
	 }else{
		no = Integer.parseInt(seqNo);
	 }
	 	 pstmt.setInt(1, no+1);//SEQNO
		 pstmt.setString(2, sendName);//SENDNAME
		 pstmt.setString(3, rphone1);//RPHONE1
		 pstmt.setString(4, rphone2);//RPHONE2
		 pstmt.setString(5, rphone3);//RPHONE3
		 pstmt.setString(6, recvName);//RECVNAME
		 pstmt.setString(7, msg);//MSG
		 pstmt.setString(8, "S");//KIND
		 pstmt.setString(9, sphone1);//SPHONE1
		 pstmt.setString(10, sphone2);//SPHONE2
		 pstmt.setString(11, sphone3);//SPHONE3
//		 pstmt.setString(, "");//URL
//		 pstmt.setString(, "");//RDATE
//		 pstmt.setString(, "");//RTIME
//		 pstmt.setString(, "");//RESULT
//		 pstmt.setString(, "");//ERRCODE
//		 pstmt.setString(, "");//RECVTIME
		 
		 
		 result = pstmt.executeUpdate();
		 if(result==1){
			if(seqNo==null||seqNo.equals("")){
				query = "INSERT INTO IPLAN_V1.IDS VALUES(?,?)";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1,"SMSDATA");
				pstmt.setInt(2,1);
			}else{
				query = "UPDATE IPLAN_V1.IDS SET NEXT_ID=NEXT_ID+1 WHERE TABLE_NAME=?";
				pstmt = conn.prepareStatement(query);
				pstmt.setString(1,"SMSDATA");
			}
			result = pstmt.executeUpdate();
			if(result==1){
				JSONObject value = new JSONObject();
				value.put("result", "SUCCESS");
				returnValue.add(value);
			 }else{
				JSONObject value = new JSONObject();
				value.put("result", "FAIL");
				returnValue.add(value);
			 }
		 }

		 
	}catch(Exception e){
	    connect = false;
	    e.printStackTrace();
	    JSONObject value = new JSONObject();
		value.put("result", "FAIL");
		returnValue.add(value);
	}finally {
		if(rs != null) try{rs.close();}catch(SQLException sqle){}            // Resultset 객체 해제

		if(pstmt != null) try{pstmt.close();}catch(SQLException sqle){}   // PreparedStatement 객체 해제

		if(conn != null) try{conn.close();}catch(SQLException sqle){}   // Connection 해제
	}
}
%>
<%=returnValue.toJSONString()%>