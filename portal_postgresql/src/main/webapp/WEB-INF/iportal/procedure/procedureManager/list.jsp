<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>프로시져 관리</title>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
		
		<script language="javascript" type="text/javascript">
		var yearList = new Array();
		var proList = '';
		var loadingMsg = '데이터 로딩중 입니다.';
		$(document).ready(function(){
			jQuery.browser = {};
			jQuery.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase());
			jQuery.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
			jQuery.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
			jQuery.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());
			jQuery.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase());
			
			$(window).ajaxStart(showMsg);
    		$(window).ajaxStop(function(){
    			$i.dialog.hideLoader();
    		});
    		
			$("#sText").jqxInput({placeHolder: "", height: 23, width: 250, minLength: 1, theme:'blueish'});
			$("#jqxButton1").jqxButton({ width: '',  theme:'blueish'});
			$("#sText").keydown(function(event){
	        	if(event.keyCode == 13)
	        		return searchStart();
	        });
			
			<c:forEach items="${yearList}" var="item">
				yearList.push({com_cod:"${item.com_cod}",com_nm:"${item.com_nm}"});
			</c:forEach>
			
			searchStart();
			proList.done(function(){
				goDetail('','','');
			});
		});
		function showMsg(){
        	$i.dialog.showLoader('SYSTEM',loadingMsg); 
        }
		function searchStart(){
			$('#proList').jqxGrid('clearselection');			
			$('#listGrid').jqxGrid('clearselection');
			proList='';
			makeProcedureList();
			proList.done(function(){
				goDetail('','','');
			});
		}
		function makeProcedureList(){
			var sText = $("#sText").val();
			proList = $i.post("../../procedure/procedureManager/getProcedureList",{procedureNm:sText});
			proList.done(function(res){
				var source =
	            {
	                datatype: "json",
	                datafields: [
	                    { name: 'CDPROCEDURE', type: 'string' },
	                    { name: 'CDPROCEDURE_NM', type: 'string' },
	                    { name: 'DB_GUBN', type: 'string' },
	                    { name: 'DB_USER', type: 'string'},
	                    { name: 'RESULT_TIME', type: 'string'},
	                    { name: 'SOF_YN', type: 'string'},
	                    { name: 'BIGO', type: 'string'}
	                ],
	                localdata: res.returnArray,
	                updaterow: function (rowid, rowdata, commit) {
	                    commit(true);
	                }
	            };
	            var dataAdapter = new $.jqx.dataAdapter(source);
                var noScript = function(row, columnfield, value){
                	var newValue = $i.secure.scriptToText(value);
                	return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 4px;">' + newValue + '</div>';
                };
	           
	            $("#proList").jqxGrid(
	            {
	              	width: '100%',
	                height:'100%',
					altrows:true,
	                source: dataAdapter,
					theme:'blueish',
	                columnsresize: true,
	                pageable: false,
	            //    selectionmode: 'none',
	                columns: [
						{ text: 'No', datafield:'No', width:'5%', align:'center', cellsalign:'center',cellsrenderer:function (row, columnfield, value, defaultHtml, property, rowdata) {
							return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:5px;'>" + Number(row+1) + "</div>";
							}
						},
						{ text: '공시', datafield: 'CDPROCEDURE_NM', width: '15%', align:'center', cellsalign: 'center',cellsrenderer:noScript},
						{ text: '프로시져', datafield: 'CDPROCEDURE', width: '15%', align:'center', cellsalign: 'center',cellsrenderer:noScript},
						{ text: '년도', datafield: 'YYYY', width: '5%', align:'center', cellsalign: 'center', cellsrenderer: function (row, columnfield, value, defaultHtml, property, rowdata) {
							var html = '';
							html+="<select id='pYyyy"+row+"' name='pYyyy' style='padding:0;' class='jqxComboInit'>"
							for(var i=0;i<yearList.length;i++){
								if(i==0)
									html+="<option value='"+yearList[i].com_cod+"' selected>"+yearList[i].com_nm+"</option>"w
									html+="<option value='"+yearList[i].com_cod+"'>"+yearList[i].com_nm+"</option>";
							}
							html+="</select>"
							return "<div style='text-align:center;margin-top:4px;margin-left:3px; color:black;'>" + html + "</div>";
						}},
						{ text: '최종 집계시간', datafield: 'RESULT_TIME',  width: '5%', align:'center', cellsalign: 'center'},
						{ text: '성공여부', datafield: 'SOF_YN', width: '5%', align:'center', cellsalign: 'center', cellsrenderer: function (row, columnfield, value, defaultHtml, property, rowdata) {
							var newValue = "";
							if(value=='SUCCESS')	newValue="성 공";
							else if(value=='ERROR') newValue="실 패";
							else newValue = "-";
			            	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:5px; color:blue;'>" + newValue + "</div>";
							}
						},
						{ text: '마감', datafield: 'c', width: '5%', align:'center', cellsalign: 'center', cellsrenderer: function (row, columnfield, value, defaultHtml, property, rowdata) {
							var link = "<img src='../../resources/img/icon/icon_cell_button_play.png' onclick='callSpStart("+row+")'/>";
			            	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:5px; color:black;'>" + link + "</div>";
							}
						},
						{ text: '설명', datafield: 'BIGO', width: '45%', align:'center', cellsalign: 'left',cellsrenderer:noScript}
					]
				});
	         /*   
	            $(".jqxComboInit").jqxComboBox({
	    			animationType : 'fade',
	    			dropDownHorizontalAlignment : 'right',
	    			dropDownHeight : 140,
	    			width : '90%',
	    			height : 19,
	    			theme : 'blueish'
	    		});*/
			});	
		}
		function callSpStart(row){
			loadingMsg="프로시져 실행중..";
			var rowData = $("#proList").jqxGrid("getrowdata",row);
			//var pYyyyList = $("select[name=pYyyy]");
			$("#ac1").val("sp");
			var idx = row;
			var spName = rowData.CDPROCEDURE;
			//var yyyy = pYyyyList[row].value;
			var yyyy = $("#pYyyy"+row).val();
			var dbGubn = rowData.DB_GUBN;
			var dbUser = rowData.DB_USER;
			var callSp = $i.post("../../procedure/procedureManager/callSp",{idx:row,spName:spName,yyyy:yyyy,dbGubn:dbGubn,dbUser:dbUser});
			callSp.done(function(res){
				loadingMsg = '데이터 로딩중 입니다.';
				if(res.returnCode=="EXCEPTION"){
					$i.dialog.error("SYSTEM",res.returnMessage,function(){
						proList='';
						makeProcedureList();
						proList.done(function(){
							goDetail(dbGubn,dbUser,spName);
						});
					});
				}else{
					$i.dialog.alert("SYSTEM",res.returnMessage,function(){
						proList='';
						makeProcedureList();
						proList.done(function(){
							goDetail(dbGubn,dbUser,spName);
						});
					});
				}
			});
		}
		function goDetail(dbgubn,dbuser,paramProcedureNm){
			var paramProcedureName = paramProcedureNm;
			var dbGubn = dbgubn;
			var dbUser = dbuser;
			if(dbGubn == ""){
				var rowData = $("#proList").jqxGrid("getrowdata",0);
				console.log(rowData);
				dbGubn = rowData.DB_GUBN;
				dbUser = rowData.DB_USER;
			}
 
			var resultList = $i.post("../../procedure/procedureManager/procedureResultListCogdw",{dbGubn:dbGubn,dbUser:dbUser,paramProcedureName:paramProcedureName});
			resultList.done(function(data){
				var source =
				{
					datatype: "json",
					datafields: [
						{ name: 'PROCEDUREID', type: 'String'},
						{ name: 'DTINSERT', type: 'String'},
						{ name: 'START_TIME', type: 'String'},
						{ name: 'END_TIME', type: 'String'},
						{ name: 'SOF_YN', type: 'String'},
						{ name: 'ERRORCOMMENT', type: 'String'}
					],
					localdata: data.returnArray
				};
				
				var dataAdapter = new $.jqx.dataAdapter(source);
				var leftRenderer = function (row, columnfield, value) {//left정렬
                       return '<div id="userName-' + row + '"style="text-align: left; margin:4px 0px 0px 10px;">' + value + '</div>';
							
                };
                var sofRender = function (row, columnfield, value) {
                	var sofYn = $("#listGrid").jqxGrid("getrowdata", row).SOF_YN;
                	var html = "";
                	if(sofYn == "SUCCESS") {
                		html = "<div style='text-align:center; padding-bottom:2px; margin-top:5px; color:blue;'>성  공</div>";
                	}else if(sofYn == "ERROR"){
                		html = "<div style='text-align:center; padding-bottom:2px; margin-top:5px; color:red;'>실  패</div>";
                	}else{
                		html = "<div style='text-align:center; padding-bottom:2px; margin-top:5px; color:black;'> - </div>";
                	}
                	return html;
                };
				$("#listGrid").jqxGrid({
					width: '100%',
					height: '100%',
					altrows:true,
					source: dataAdapter,
					theme:'blueish',
					columnsresize: true,
					columns: [
						{ text: '프로시져', datafield: 'PROCEDUREID', width: 100, align:'center', cellsalign: 'center'},
						{ text: '실행시간', datafield: 'DTINSERT', width: 120 , align:'center', cellsalign: 'center'},
						{ text: '시작시작', datafield: 'START_TIME', width: 120 , align:'center', cellsalign: 'center'},
						{ text: '종료시간', datafield: 'END_TIME', width: 120, align:'center', cellsalign: 'center'},
						{ text: '성공여부', datafield: 'SOF_YN', width: 80 ,align:'center', cellsalign:'center', cellsrenderer: sofRender},
						{ text: '오류메시지', datafield: 'ERRORCOMMENT', align:'center', cellsalign: 'left'}
					]
				});
			});
		}
		</script>
	</head>     
	<body class="blueish">
		<div class="wrap" style="width:98%; min-width:1160px; margin:0 10px;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<div class="label type1 f_left">프로시져명 :</div>
				<div style='float: left;'><input type="text" id="sText" name="sText" style="margin-left:5px;" value=""/></div>
				<div class="group_button f_right">
					<div class="button type1 f_left">
						<input type="button" value="조회" id='jqxButton1' width="100%" height="100%" onclick="searchStart();" />
					</div>
				</div>
			</div>
			
			<div class="container  f_left" style="width:100%; margin:10px 0;">
					<input type="hidden" id="ac1" name="ac" />
					<input type="hidden" id="idx" name="idx"/>
					<input type="hidden" id="spName" name="spName" />
					<input type="hidden" id="dbGubn" name="dbGubn" />
					<input type="hidden" id="dbUser" name="dbUser" />
					<input type="hidden" id="yyyy" name="yyyy" />
				<div  class="content f_left" style="width:100%;height:450px; margin-bottom:20px;">
					<div class="grid f_left" style="width:100%; height:100%;">
						<div id="proList"></div>
					</div>
				</div>
				<div  class="content f_left" style="width:100%;height:270px;">
					<div class="grid f_left" style="width:100%; height:100% !important;">
						<div style="width:100% !important; height:100%;" id='listGrid'></div>
					</div>
				</div> 
			</div>
		</div>
	</body> 
</html> 
