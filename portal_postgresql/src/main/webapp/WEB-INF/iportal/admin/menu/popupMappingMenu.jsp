<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>${title}</title>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		init();
	});
	function init(){
		$('#jqxSplitter').jqxSplitter({ height: 520, width: "100%",  panels: [{ size: '25%' }, { size: '75%'}], theme:"blueish"  });
		$("#btnSearch").jqxButton({ width : '', theme : 'blueish' });
		$("#txtSearch").jqxInput({placeHolder: "", height: 23, width: 250, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return makeGrid(); } });
		makeTree();
		makeGrid();
	}
	function makeTree() {
		var makeTreeData = $i.post("./getPopupMenuTreeData",{searchValue:''});
		makeTreeData.done(function(res){
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: 'C_ID'},
					{ name: 'PID'},
					{ name: 'NAME'},
					{ name: 'MENU_TYPE'}
				],
				icons:true,
				altrows:false,
				pageable:false,  
				localdata: res,
				
				hierarchy:{
					keyDataField:{name:'C_ID'},
					parentDataField:{name:'PID'} 
				},
				id:'C_ID'
			};
			var dataAdapter = new $.jqx.dataAdapter(source); 
			
			$("#treeGridMenuList").jqxTreeGrid({
					source: dataAdapter, 
					height: '100%', 
					width: '100%',
					theme:"blueish",
				icons:function(rowKey, rowdata){
	     			var icon = ""; 
	     			if(rowdata.MENU_TYPE!=null&&rowdata.MENU_TYPE=="2"){
	     				//파일
// 	     				icon = '${WWW.CSS}/images/img_icons/pageIcon.png';
	     				icon = '../../resources/cmresource/image/icon-page.png';
	     			}
	     			else{
	     				//폴더
	     				//icon = 'folderopenIcon.png';
// 	     				icon = '${WWW.CSS}/images/img_icons/folderIcon.png';
						icon = '../../resources/cmresource/image/icon-folder.png';
	     			}
	     			//label = "<span item-title='truec' style='vertical-align:middle;'><img style='float: left; margin-right: 5px;' src='${WWW.CSS}/images/img_icons/" + icon + "'/>" + rowdata.NAME + "</span>";	
	     			
	     			return icon;     
	     		}, 
				columns:[	 
							 
				         	{ dataField:'C_ID',text:'C_ID',align:'center', hidden:true},
				         	{ dataField:'PID',text:'PID',align:'center', hidden:true}, 
				         	{ dataField:'NAME',text:'NAME', algin:'center', 
				         		cellsrenderer:function(row,datafield,value,rowdata){
				         			var newValue = $i.secure.scriptToText(rowdata.NAME);
				         			if(rowdata.records!=null){
				         				var cntlabel = "<span style='color: Blue;' data-role=='treefoldersize'> (" + rowdata.records.length + ")</span>";
					         			
					         			return "<a href='javascript:viewDetail("+rowdata.C_ID+");' style='color:#000000;'>"+newValue+cntlabel+"</a>";
				         			}
				         			else
				         				return "<a href='javascript:viewDetail("+rowdata.C_ID+");' style='color:#000000;'>"+newValue+"</a>";
				         		}
				         	} 
				         ],
				ready:function(){
					$("#treeGridMenuList").jqxTreeGrid('expandRow', 5999);     
				}
			});
			$("#treeGridMenuList").jqxTreeGrid('selectRow', 5999);   
			
			//add event 
			$("#treeGridMenuList")
				.on("rowSelect", function(event) { 
					// event args.
				    var args = event.args;
				    // row data.
				    var row = args.row;
				    // row key.
				    var key = args.key;
					 

					setData(row.C_ID, row.NAME);
				}); 
		});
	}
	function makeGrid(){
		var gridData = $i.post("./getPopupMenuTreeData",{searchValue:$("#txtSearch").val()});
		gridData.done(function(res){
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: 'C_ID', type: 'string' },
					{ name: 'NAME', type: 'string' },
					{ name: 'PID', type: 'string' },
					{ name: 'P_NAME', type: 'string'}
				],
				localdata: res.returnArray
			};
			
			var dataAdapter = new $.jqx.dataAdapter(source);
			
			//columns
			var nameCellsrenderer = function(row, datafieId, value, defaultHtml, property, rowdata) {
				var newValue = $i.secure.scriptToText(value);
				var cid = rowdata.C_ID;
				var cname = rowdata.NAME;
				var link = "<a style='color: #000000;text-decoration:underline;'>" + newValue + "</a>";
			
				return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px;'>" + link + "</div>";
			};
			var noScript = function (row, columnfield, value) {
    			var newValue = $i.secure.scriptToText(value);
				return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
			}
	
			$('#gridMenuList').jqxGrid({
				width: '100%',
				height: '100%',
				altrows:true,
				pageable: true,
				source: dataAdapter,
				theme:'blueish',
				columnsheight:28,
				rowsheight: 28,
				columnsresize: true,
				columns: [
					{ text: '상위메뉴 ID', datafield: 'PID', width: '25%', align:'center', cellsalign: 'center'},
					{ text: '상위메뉴 명', datafield: 'P_NAME', width: '25%', align:'center', cellsalign: 'center', cellsrenderer:noScript},
					{ text: '메뉴 ID', datafield: 'C_ID', width: '25%', align:'center', cellsalign: 'center' },
					{ text: '메뉴 명', datafield: 'NAME', width: '25%', align:'center', cellsalign: 'center' , cellsrenderer:nameCellsrenderer}	
				]
			});
			 $('#gridMenuList').on('cellclick', function (event) {
				 var args = event.args;
				 var index = args.rowindex;
				 var dataField = args.datafield;
				 var value = args.value;
				 var cid="";
				 var cname="";
				 if(dataField=='NAME'){
					 var data = $('#gridMenuList').jqxGrid('getrowdata', index);
					 cid=data.C_ID;
					 cname=$i.secure.scriptToText(value);
					 setData(cid,cname);
				 }
			 });
			//pager width
			$('#gridpagerlist' + $('#gridMenuList').attr('id')).width('44px');
			$('#dropdownlistContentgridpagerlist' + $('#gridMenuList').attr('id')).width('19px');
		});
	}
	function searchStart() {
	}
	function setData(cid, cname) {
		var pCid = "${param.cid}";
		var gubn = "${param.gubn}";
		if(gubn == "tree"){
			$('#label'+pCid, opener.document).html(cname);    
			$("[name='pid'][data-cid='"+pCid+"']", opener.document).val(cid);
		}else{
			$('#hiddenBtmPID', opener.document).val(cid);    
			$('#txtBtmPName', opener.document).val(cname);
		}
		window.close();
	}
	</script>
	<style type="text/css">
 		#treeGridMenuList .jqx-grid-header-blueish { 
    			display : none !important; 
 		} 
	</style>
</head>
<body class='blueish'>
	<div class="wrap" style="width:96%; margin:0 auto;">
		<div class="container  f_left" style="width:100%; margin:10px 0;">
			<div class="content f_left" style="width:100%; margin-right:1%;">
				<div class="splitter  f_left" style="width:100%; height:; margin:0;">
					<div id="jqxSplitter">
						<div>
							<div class="grid f_left" style="width:100%; height:500px; margin:5px 0;">
								<div id="treeGridMenuList" class="treegrid"> </div>
							</div>
						</div>
						<div>
							<div class="content f_left" style="width:99%;">
								<div class="header f_left" style="width:99%; height:97% !important; margin:3px 5px 5px !important;">
									<div class="label type1 f_left">메뉴명 : </div>
									<div class="input f_left">
										<input type="text" id="txtSearch" name="searchValue" />
									</div>
									<div class="group_button f_right">
										<div class="button type1 f_left">
											<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="makeGrid();" />
										</div>
									</div>
								</div>
								<div class="grid f_left" style="width:99%; height:450px; margin:3px 5px 5px !important;">
									<div id="gridMenuList"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!--wrap-->		
</body>
</html>
