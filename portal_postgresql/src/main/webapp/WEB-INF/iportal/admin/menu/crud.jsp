<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>${title}</title>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
    <link rel="stylesheet" href="../../resources/css/iplanbiz/styles/jqx.ui-redmond_lotte.css" type="text/css" />
	<link rel="stylesheet" href="../../resources/css/iplanbiz/styles/jqx.Lotte-depart.css" type="text/css" />
<%--     <script src="${WWW.JS}/iplanbiz/core/iplanbiz.web.include.jsp"></script> --%>
<%-- 	<script src="${WEB.JSDWR}/interface/groupManagerDwr.js"></script> --%>
	<script type="text/javascript">
		var orgSortOrder = "";
		var sortOrder = "";
		var orgProgID = "";
		var progID = "";
		var orgClink = "";
		var cLink = "";
		var changeData = "";
		var changeID = [];
		var menudata = null;
		$(document).ready(function(){
			init();
		});
		function init(){
			makeTreeGridList();
			useYnCombo();
			folderCombo();
			commonCombo();
			sourceCombo();
			$("#btnTreeGridSave").jqxButton({ width: '', theme: 'blueish' });
			$("#btnLanguage").jqxButton({ width: '', theme: 'blueish' });
			$("#btnSave").jqxButton({ width: '', theme: 'blueish' });
			$("#btnDelete").jqxButton({ width: '', theme: 'blueish' });
			$("#btnReset").jqxButton({ width: '', theme: 'blueish' });
			setLanguageButton();
			$("#treegridMenuList").on("cellBeginEdit", function(event){
				var args = event.args;
				if(args.dataField == "SORT_ORDER"){
					orgSortOrder = args.value;
				}else if(args.dataField == "PROG_ID"){
					orgProgID = args.value;
				}else if(args.dataField == "C_LINK"){
					orgClink = args.value;
				}
			});
			$("#treegridMenuList").on("cellEndEdit", function(event){
				var args = event.args;
				var cid = String(args.row.C_ID);
				if(args.dataField == "SORT_ORDER"){
					sortOrder = args.value;
				}else if(args.dataField == "PROG_ID"){
					progID = args.value;
				}else if(args.dataField == "C_LINK"){
					cLink = args.value;
				}
				if(orgClink != cLink){
					changeID[changeID.length] = cid;
					changeData = "change";
				}
				if(orgProgID != progID){
					changeID[changeID.length] = cid;
					changeData = "change";
				}
				if(orgSortOrder != sortOrder){
					changeID[changeID.length] = cid;
					changeData = "change";
				}
				valueChange("other",cid);
// 				if($("[name='changeValue'][data-cid='"+cid+"']").val() == "no" || $("[name='changeValue'][data-cid='"+cid+"']").val() == ""){
// 					if(orgClink != cLink){
// 						$("[name='changeValue'][data-cid='"+cid+"']")[0].value = "change"
// 						$("[name='changeValue'][data-cid='"+cid+"']").val("change");
// 					}
// 					if(orgProgID != progID){
// 						$("[name='changeValue'][data-cid='"+cid+"']")[0].value = "change"
// 						$("[name='changeValue'][data-cid='"+cid+"']").val("change");
// 					}
// 					if(orgSortOrder != sortOrder){
// 						$("[name='changeValue'][data-cid='"+cid+"']")[0].value = "change"
// 						$("[name='changeValue'][data-cid='"+cid+"']").val("change");
// 					}
// 				}
			});
		}
		function useYnCombo(){
			var dat  = [
				{value:'Y',text:'사용'},
				{value:'N',text:'미사용'} 

			];
            var source =
            {
                datatype: "json",
                datafields: [
                     { name: 'value' },
                     { name: 'text' }
                ],
                id: 'id',
				localdata:dat,
                async: false
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#cboUseYn").jqxDropDownList({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "text", valueMember: "value", dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,theme:'blueish'});
		}
		function folderCombo(){
			var dat  = [
				{value:'2',text:'일반'} ,
				{value:'1',text:'폴더'}
			];
            var source =
            {
                datatype: "json",
                datafields: [
                     { name: 'value' },
                     { name: 'text' }
                ],
                id: 'id',
				localdata:dat,
                async: false
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#cboFolder").jqxDropDownList({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "text", valueMember: "value", dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,theme:'blueish'});
		}
		function commonCombo(){
			var dat  = [
				{value:'common',text:'권한'},
				{value:'all',text:'전체'}
			];
            var source =
            {
                datatype: "json",
                datafields: [
                     { name: 'value' },
                     { name: 'text' }
                ],
                id: 'id',
				localdata:dat,
                async: false
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#cboCommon").jqxDropDownList({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "text", valueMember: "value", dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,theme:'blueish'});
		}
		function sourceCombo(){
			var divisionData = $i.post("./getMenuDomain",{});
			divisionData.done(function(dat){
				var source =
	            {
	                datatype: "json",
	                datafields: [
	                     { name: 'DIVISION' },
	                     { name: 'DIVISION_NM' }
	                ],
	                id: 'id',
					localdata:dat.returnArray,
	                async: false
	            };
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#cboSource").jqxDropDownList({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "DIVISION_NM", valueMember: "DIVISION", dropDownWidth: 150, dropDownHeight: 80, width: 150, height: 22,theme:'blueish'});
			});
		}
		function valueChange(inputbox, cid){
			var org = "";
			var value = "";
			var okValue = "";
			var novalue = "";
			if(inputbox == "useYn"){
				org = "orgCheckUseYn";
				value = "useYn";
				okValue = "Y";
				noValue = "N";
			}else if(inputbox == "folderYn"){
				org = "orgCheckFolderYn";
				value = "folderYn";
				okValue = "1";
				noValue = "2";
			}else if(inputbox == 'other'){
				if(changeData == "change"){
					$("[name='changeValue'][data-cid='"+changeID+"']").val("change");
				}
			}else{
				org = "orgCheckOpenYn";
				value = "openYn";
				okValue = "all";
				noValue = "common";
			}
			if(inputbox != "other"){
				if($("[name='"+value+"'][data-cid='"+cid+"']")[0].checked == true){
					$("[name='"+value+"'][data-cid='"+cid+"']").val(okValue);
				}else{
					$("[name='"+value+"'][data-cid='"+cid+"']").val(noValue);
				}	
			}
			if($("[name='changeValue'][data-cid='"+cid+"']").val() == "no" || $("[name='changeValue'][data-cid='"+cid+"']").val() == ""){
				if(inputbox != "other"){
					if($("[name='"+org+"'][data-cid='"+cid+"']").val() == $("[name='"+value+"'][data-cid='"+cid+"']").val()){
						$("[name='changeValue'][data-cid='"+cid+"']").val("no");
					}else{
						$("[name='changeValue'][data-cid='"+cid+"']").val("change");
					}	
				}else{
					if(changeData == "change"){
						$("[name='changeValue'][data-cid='"+changeID+"']").val("change");
					}
				}
			}
		}
		function openWindow(cid, gubn){
			var url = "./popupMappingMenu?cid="+cid+"&gubn="+gubn;
			
			var nWin = window.open(url,"_blank","left=0,top=0,width=1040,height=550,toolbar=no,scrollbars=0,status=yes,resizable=yes");
			nWin.focus();
		}
		function replaceAll(str,orgStr,repStr){
		    return str.split(orgStr).join(repStr);
		}
		function saveStart(){
			var arrayCID = "";
			var arrayPID = "";
			var arraySortOrder = "";
			var arrayUseYn = "";
			var arrayProgID = "";
			var arrayMenuType = "";
			var arrayMenuOpenType = "";
			var arrayClink = "";
			var arraySourceOwner = "";
			var changeInput = changeID.length;
			for(var j=0;j<changeInput;j++){
				$("[name='changeValue'][data-cid='"+changeID[j]+"']").val("change");
			}
			var index = $("[name='changeValue']").length;
			for(var i=0;i<index;i++){
				var cID = $("[name='changeValue']").eq(i).data("cid");
				var rowKey = $("[name='changeValue']").eq(i).data("rowkey");
				if($("[name='changeValue']")[i].value == "change"){
					arrayCID += $("[name='changeValue']").eq(i).data("cid");
					arrayPID += $("[name='pid'][data-cid='"+cID+"']").val();
					arraySortOrder += $("#treegridMenuList").jqxTreeGrid("getRow", rowKey).SORT_ORDER;
					arrayUseYn += $("[name='useYn'][data-cid='"+cID+"']").val();
					if($("#treegridMenuList").jqxTreeGrid("getRow", rowKey).PROG_ID == ""){
						arrayProgID += " ";	
					}else{
						arrayProgID += $("#treegridMenuList").jqxTreeGrid("getRow", rowKey).PROG_ID;
					}
					arrayMenuType += $("[name='folderYn'][data-cid='"+cID+"']").val();
					arrayMenuOpenType += $("[name='openYn'][data-cid='"+cID+"']").val();
					if($("#treegridMenuList").jqxTreeGrid("getRow", rowKey).C_LINK == ""){
						arrayClink += " ";
					}else{
						arrayClink += $("#treegridMenuList").jqxTreeGrid("getRow", rowKey).C_LINK;
					}
					
					arraySourceOwner += $("#treegridMenuList").jqxTreeGrid("getRow", rowKey).SOURCE_OWNER;
					if(i != 0){
						arrayCID += ",";
						arrayPID += ",";
						arraySortOrder += ",";
						arrayUseYn += ",";
						arrayProgID += ",";
						arrayMenuType += ",";
						arrayMenuOpenType += ",";
						arrayClink += ",";
						arraySourceOwner += ",";
					}
				}
			}
			if(arrayCID.length > 0){
				$("#txtArrayCID").val(arrayCID);
				$("#txtArrayPID").val(arrayPID);
				$("#txtArraySortORder").val(arraySortOrder);
				$("#txtArrayUseYn").val(arrayUseYn);
				$("#txtArrayProgID").val(arrayProgID);
				$("#txtArrayMenuType").val(arrayMenuType);
				$("#txtArrayMenuOpenType").val(arrayMenuOpenType);
				$("#txtArrayClink").val(arrayClink);
				$("#txtArraySourceOwner").val(arraySourceOwner);
				$i.post("./insertArray", "#saveAllForm").done(function(data){
					if(data.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", data.returnMessage);
					}else{
						$i.dialog.alert("SYSTEM", data.returnMessage, function(){
							makeTreeGridList();
						});
					}
				});
			}else{
				$i.dialog.error("SYSTEM", "변경된 항목이 존재하지 않습니다.");
			}
			
		}
		function saveStartOne(){
			if($("#cboFolder").val() != "2" && $("#cboCommon").val() != "common"){
				$i.dialog.error("SYSTEM", "폴더는 권한공개만 가능합니다");
				return ;
			}
			if($('#hiddenBtmPID').val() == '') {
				$i.dialog.error("SYSTEM", "상위 메뉴를 선택하세요");
				return; 
			}
			var locale = "";
			var formMenuName = "";
			var validation = true;
			$("input[name=formMenuName]").each(function(i){
				if(i != 0){
					locale += ",";
					formMenuName += ",";  
				}
				locale += $("select[name=locale]").eq(i).val(); 
				formMenuName  += $("input[name=formMenuName]").eq(i).val();
				if(formMenuName.trim()=="") {
					validation = false ;
				}
			});
			if(validation==false){
				$i.dialog.error("SYSTEM", "메뉴명을 입력하세요.");
				return ;
			}else{
				$("#hiddenLocale").val(locale);
				$("#hiddenCName").val(formMenuName);
			}
			if($("#txtBtmSortOrder").val() == ""){
				$i.dialog.error("SYSTEM", "정렬순서를 입력하세요");
				return ;
			}
			if(isNaN($("#txtBtmSortOrder").val())){
				$i.dialog.error("SYSTEM", "정렬순서를 숫자로 입력하세요");
				return ;
			}
			$i.post("./insertOne", "#saveForm").done(function(data){
				if(data.returnCode == "EXCEPTION"){
					$i.dialog.error("SYSTEM", data.returnMessage);
				}else{
					$i.dialog.alert("SYSTEM", data.returnMessage, function(){
						makeTreeGridList();
						resetForm();
					});
				}
			});
		}
		function deleteStart(){
			if($("#txtbtmCID").val() == ""){
				$i.dialog.error("SYSTEM","삭제할 메뉴를 선택하세요");
				return false;
			}
			$i.dialog.confirm("SYSTEM", "삭제 하시겠습니까?", function(){
				$i.post("./remove", "#saveForm").done(function(data){
					if(data.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", data.returnMessage);
					}else{
						$i.dialog.alert("SYSTEM", data.returnMessage, function(){
							makeTreeGridList();
							resetForm();
						});
					}
				});
			});
		}
		function resetForm(){
			$("#txtbtmCID").val("");
			$("#txtbtmCName").val("");
			//$("#hiddenBtmPID").val("");
			//$("#txtBtmPName").val("");
			$("#txtBtmSortOrder").val("");
			$("#cboUseYn").jqxDropDownList({ selectedIndex : 0 });
			$("#txtBtmProgmID").val("");
			$("#cboFolder").jqxDropDownList({ selectedIndex : 0 });
			$("#cboCommon").jqxDropDownList({ selectedIndex : 0 });
			$("#cboSource").jqxDropDownList({ selectedIndex : 0 });
			$("#txtBtmCLink").val("");
			$("#hiddenLocale").val("");
			$("#hiddenCName").val("");
			
			var localeHtml = '';
			localeHtml += '<div>';
			localeHtml += '<select class="m_r10" name="locale" style="width:25%;float:left;border:1px solid #cccccc;">';
			localeHtml += '<option value="ko">한국어</option>';
			localeHtml += '<option value="en">영어</option>';
			localeHtml += '</select>';
			localeHtml += '<input type="text" name="formMenuName" class="input type1 f_left m_r10" style="margin:0px; width:60%; height:19px;" value="" />';
			localeHtml += '<img src="../../resources/css/images/img_icons/icon-plus.png" alt="추가" class="button_plus_minus"  width="19" height="19" style="padding-top:1px !important;" onclick="addLanguage();">';
			localeHtml += '</div>';
			$('#language_content').html(localeHtml);
			$('.language_label').hide();
		}
		function getMenuDetail(cid){
			var infoData = $i.post("./getMenuInfoDetail", {cID : cid});
			infoData.done(function(info){
				if(info.returnCode == "EXCEPTION"){
					$i.dialog.error("SYSTEM", info.returnMessage);
				}else{
					if(info.returnArray.length > 0){
						$("#txtbtmCID").val(info.returnArray[0].C_ID);
						$("#txtbtmCName").val($i.secure.TextToScript(info.returnArray[0].C_NAME));
						$("#hiddenBtmPID").val(info.returnArray[0].P_ID);
						$("#txtBtmPName").val(info.returnArray[0].PID_NAME);
						$("#txtBtmSortOrder").val(info.returnArray[0].SORT_ORDER);
						$("#cboUseYn").val(info.returnArray[0].USE_YN);
						$("#txtBtmProgmID").val(info.returnArray[0].PROG_ID);
						$("#cboFolder").val(info.returnArray[0].MENU_TYPE);
						$("#cboCommon").val(info.returnArray[0].MENU_OPEN_TYPE);
						$("#cboSource").val(info.returnArray[0].SOURCE_OWNER);
						$("#txtBtmCLink").val($i.secure.TextToScript(info.returnArray[0].C_LINK));
						setLocaleHtml(info.returnArray);
					}
				}
			});
		}
		function setLocaleHtml(res){
			
			var localeHtml = '';
			for(var i=0; i<res.length; i++) {
				localeHtml += '<div>';
				localeHtml += '<select name="locale" class="m_r10" style="width:25%;float:left;border:1px solid #cccccc;">';
				localeHtml += '<option value="ko" ' + (res[i].LOCALE == 'ko'?'selected="selected"':'') + '>한국어</option>';
				localeHtml += '<option value="en" ' + (res[i].LOCALE == 'en'?'selected="selected"':'') + '>영어</option>';
				localeHtml += '</select>';
				localeHtml += '<input type="text" name="formMenuName" class="input type1 f_left m_r10" style=" margin:0px; width:60%; height:19px;" value="'+res[i].C_NAME+'" />';
				
				if(i == 0) {
					localeHtml += '<img src="../../resources/css/images/img_icons/icon-plus.png" alt="추가" class="button_plus_minus"  width="19" height="19" style="padding-top:1px !important;" onclick="addLanguage();">';	
				} else {
					localeHtml += '<img src="../../resources/css/images/img_icons/icon-plus.png" alt="삭제" class="button_plus_minus"  width="19" height="19" style="padding-top:1px !important;" onclick="delLanguage(this);">';	
				}
				localeHtml += '</div>';
			}
			$('#language_content').html(localeHtml);
		}
		function addLanguage() {
			
			var localeHtml = '';
			localeHtml += '<div>';
			localeHtml += '<select name="locale" class="m_r10" style="width:25%;float:left;border:1px solid #cccccc;">';
			localeHtml += '<option value="ko">한국어</option>';
			localeHtml += '<option value="en">영어</option>';
			localeHtml += '</select>';
			localeHtml += '<input type="text" name="formMenuName" class="input type1 f_left m_r10" style="margin:0px; width:60%; height:19px;" value="" />';
			localeHtml += '<img src="../../resources/css/images/img_icons/icon-minus.png" alt="삭제" class="button_plus_minus"  width="19" height="19" style="padding-top:1px !important;" onclick="delLanguage(this);">';
			localeHtml += '</div>';
			$('#language_content').append(localeHtml);
		}
		function delLanguage(obj) {
			$(obj).parent().remove();
		}
		function setLanguageButton() {
			
			var localeHtml = '';
			localeHtml += '<div>';
			localeHtml += '<select name="locale" class="m_r10" style="width:25%;float:left;border:1px solid #cccccc;">';
			localeHtml += '<option value="ko">한국어</option>';
			localeHtml += '<option value="en">영어</option>';
			localeHtml += '</select>';
			localeHtml += '<input type="text" name="formMenuName" class="input type1 f_left m_r10" style="margin:0px; width:60%; height:19px;" value="" />';
			localeHtml += '<img src="../../resources/css/images/img_icons/icon-plus.png" alt="추가" class="button_plus_minus"  width="19" height="19" style="padding-top:1px !important;" onclick="addLanguage();">';
			localeHtml += '</div>';
			$('#language_content').append(localeHtml);
			
			$('#btnLanguage').click(
				function() {
					var language_label = $('.language_label');
					var display = language_label.css('display');
					
					if(display == 'none') {
						language_label.show();
						language_label.css({top:$('#txtbtmCName').position().top+24, left:$('#txtbtmCName').position().left});
					} else if(display == 'block') {
						language_label.hide();
					}
				}
			);
		}
		function makeTreeGridList(){  
			var treeMenuData = $i.post("./getTreeGridMenuList",{});
			treeMenuData.done(function(res){
				var source =
				{
					dataType:"json",
					dataFields:[
						{ name : "C_ID", type: "String"},
						{ name : "MENU_TYPE", type: "String"},
						{ name : "P_ID", type: "String"},
						{ name : "C_NAME", type: "String"},
						{ name : "P_NAME", type: "String"},
						{ name : "SORT_ORDER", type: "String"},
						{ name : "MENU_OPEN_TYPE", type: "String"},
						{ name : "USE_YN", type: "String"},
						{ name : "C_LINK", type: "String"}, 
						{ name : "PROG_ID", type: "String"},
						{ name : "SOURCE_OWNER", type: "String"}
					],
					hierarchy:
					{
						keyDataField: { name: 'C_ID' },
                 		parentDataField: { name: 'P_ID' }
					},
					localData: res.returnArray,
					id:"id"
				};
				var dataAdapter = new $.jqx.dataAdapter(source,{loadComplete:function(){}});
				$("#treegridMenuList").jqxTreeGrid({
					width:"100%",			
					height: "100%",				
                	source: dataAdapter,
                	altRows: true,
					icons: true,
              		sortable: true,
			  		pageable:true ,
			   		pageSize: 100,
			   		pageSizeOptions: ['100', '200', '300'],
			   		pagerMode: "advanced" ,
			   		editable: true,
			   		editSettings: {
				 		saveOnPageChange: true,
				 		saveOnBlur: true,
				 		saveOnSelectionChange: true,
				 		cancelOnEsc: true,
				 		saveOnEnter: true,
				 		editSingleCell: true,
				 		editOnDoubleClick: true,
				 		editOnF2: true
			    	},
					enableHover: true ,
					columnsHeight: 40,
					theme:"blueish",
					icons:function(rowKey, rowdata){
		     			var icon = ""; 
		     			if(rowdata.MENU_TYPE!=null&&rowdata.MENU_TYPE=="2"){
		     				//파일
		     				icon = '../../resources/cmresource/image/icon-page.png';
		     			}
		     			else{
		     				//폴더
		     				//icon = 'folderopenIcon.png';
		     				icon = '../../resources/cmresource/image/icon-folder.png';
		     			}
		     			//label = "<span item-title='truec' style='vertical-align:middle;'><img style='float: left; margin-right: 5px;' src='${WWW.CSS}/images/img_icons/" + icon + "'/>" + rowdata.NAME + "</span>";	
		     			
		     			return icon;      
		     		}, 
					columns: [
                  		{ text: '메뉴명', datafield: 'C_NAME', width: '20%', align:'center', cellsalign: 'left', editable: false,
                  			cellsRenderer: function (rowKey, dataField, value, data) {
                  				var newValue = $i.secure.scriptToText(value);
                  				var rowValue = $("#treegridMenuList").jqxTreeGrid("getRow", rowKey);
                  				var link = "<a href=\"javascript:getMenuDetail('"+rowValue.C_ID+"')\" style='color:black; text-decoration:underline;' >" + newValue + "</a>";
								return link; 
                  			}
                  		},
				  		{ text: '상위메뉴', datafield: 'P_NAME', width: '15%', align:'center', cellsalign: 'left',
                  			cellsRenderer: function (rowKey, dataField, value, data) {
                  				var newValue = $i.secure.scriptToText(value);
                  				var rowValue = $("#treegridMenuList").jqxTreeGrid('getRow', rowKey);

                  				return "<div style='margin: 0px 5px;'>"+ 
                        		"<input type='hidden' data-cid='"+rowValue.C_ID+"' data-rowkey='"+rowKey+"' name='changeValue' value='no' />"+
                        		"<input type='hidden' data-cid='"+rowValue.C_ID+"' data-rowkey='"+rowKey+"' name='cid' value='"+rowValue.C_ID+"' />"+
                        		"<input type='hidden' data-cid='"+rowValue.C_ID+"' data-rowkey='"+rowKey+"' name='pid' value='"+rowValue.P_ID+"' />"+
                        		"<img width='16' height='16' style='float: right;' src='../../resources/cmresource/image/icon-search.png' onclick=\"openWindow('"+rowValue.C_ID+"','tree');\" /><span id='label"+rowValue.C_ID+"' style='margin-left: 4px; float: left;'>"+newValue+"</span><div style='clear: both;'></div></div>";
                      		}, editable: false
						},
				  		{ text: '정렬순서', datafield: 'SORT_ORDER', width: '5%', align:'center', cellsalign: 'right'},
                  		{ text: '사용여부', datafield: 'USE_YN', width: '5%', align:'center', cellsalign: 'center',  columnType: "template",
                   			cellsRenderer: function (rowKey, dataField, value, data) {
                   				var rowValue = $("#treegridMenuList").jqxTreeGrid('getRow', rowKey);
                   				var checked = "";
                   				if(value == "Y"){
                   					checked = "checked='checked'";
                   				}else{
                   					checked = "";
                   				}
                   				return "<input type='hidden' name='orgCheckUseYn' data-cid='"+rowValue.C_ID+"' data-rowkey='"+rowKey+"' value='"+value+"' /><input type='checkbox' data-cid='"+rowValue.C_ID+"' data-rowkey='"+rowKey+"' name='useYn' value='"+value+"' "+checked+" onchange=\"javascript:valueChange('useYn','"+rowValue.C_ID+"');\" />";
                      		}, editable: false
				    	},
				  		{ text: '프로그램ID', datafield: 'PROG_ID', width: '10%', align:'center', cellsalign: 'center',
				    		cellsRenderer: function (row, columnfield, value) {
				    			var newValue = $i.secure.scriptToText(value);
        						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
        					}
				  		},
                  		{ text: '폴더여부', datafield: 'MENU_TYPE', width: '5%', align:'center',  cellsalign: 'center', columnType: "template",
                        	cellsRenderer: function (rowKey, dataField, value, data) {
                        		var rowValue = $("#treegridMenuList").jqxTreeGrid('getRow', rowKey);
                        		var checked = "";
                   				if(value == "1"){
                   					checked = "checked='checked'";   
                   				}else{
                   					checked = "";
                   				}
                   				return "<input type='hidden' name='orgCheckFolderYn' data-cid='"+rowValue.C_ID+"' data-rowkey='"+rowKey+"' value='"+value+"' /><input type='checkbox' data-cid='"+rowValue.C_ID+"' data-rowkey='"+rowKey+"' name='folderYn' value='"+value+"' "+checked+" onchange=\"javascript:valueChange('folderYn','"+rowValue.C_ID+"');\" />";
                      		}, editable: false
				 		},
				 		{ text: '전체공개여부', datafield: 'MENU_OPEN_TYPE', width: '5%', align:'center',  cellsalign: 'center', columnType: "template",
                         	cellsRenderer: function (rowKey, dataField, value, data) {
                         		var rowValue = $("#treegridMenuList").jqxTreeGrid('getRow', rowKey);
                         		var checked = "";
                   				if(value == "all"){
                   					checked = "checked='checked'";
                   				}else{
                   					checked = "";
                   				}
                   				return "<input type='hidden' name='orgCheckOpenYn' data-cid='"+rowValue.C_ID+"' data-rowkey='"+rowKey+"' value='"+value+"' /><input type='checkbox' data-cid='"+rowValue.C_ID+"' data-rowkey='"+rowKey+"' name='openYn' value='"+value+"' "+checked+" onchange=\"javascript:valueChange('openYn','"+rowValue.C_ID+"');\" />";
	                      	}, editable: false
				 		},
                  		{ text: '자원소유구분', datafield: 'SOURCE_OWNER', width: '10%', align:'center',  cellsalign: 'center', columnType: "template",
							createEditor: function (row, cellvalue, editor, cellText, width, height) {
								$iv.mybatis("dwrGetDomainList", {ACCT_ID:$iv.userinfo.acctid(), DIVISION:$iv.envinfo.division},{callback:function(dat){
									var source =
					                {
					                    datatype: "json",
						                datafields: [
						                     { name: 'DIVISION' },
						                     { name: 'DIVISION_NM' }
						                ],
						                id: 'id',
										localdata:dat,
						                async: false
					                };
									var dataAdapter = new $.jqx.dataAdapter(source);
									editor.jqxDropDownList({
								   		autoDropDownHeight: true, 
								   		source: dataAdapter, 
								   		displayMember: "DIVISION_NM", 
								   		valueMember: "DIVISION",
								   		width: '100%', 
								   		height: '100%', 
								   		theme: 'blueish' ,
								   		placeHolder: "i-Vision+",
								   		selectedIndex: 2,
								   		autoOpen: false 
									});
								}});
                   			},
                   			initEditor: function (row, cellvalue, editor, celltext, width, height) {
                       			editor.jqxDropDownList('selectItem', cellvalue);
                   			},
                   			getEditorValue: function (row, cellvalue, editor) {
                       			return editor.val();
                   			},
					   		renderer: function (text, align, height) {
                          		return "<div style='margin: 5px; text-align:center;'>자원소유<br>구분</span><div style='clear: both;'></div></div>";
                      		}
				 		},      
				  		{ text: 'URL', datafield: 'C_LINK', width: '25%', align:'center', cellsalign: 'left',
				  			cellsRenderer: function (row, columnfield, value) {
				    			var newValue = $i.secure.scriptToText(value);
        						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 0px;">' + newValue + '</div>';
        					}
				 		}
                	]
            	});
				$("#treegridMenuList").jqxTreeGrid('expandRow', $("#treegridMenuList").jqxTreeGrid("getRows")[0].uid);
			});
			
		}
		function folderManage(){
			var popid = 'booklist';
			if($("#"+popid).length!=0) $("#"+popid).remove(); 
			
			var popup = "<div id='"+popid+"' style='position:absolute;z-index:999999;border: 1px solid #003664;'>"+
				"<div>폴더관리</div>"+
				"<div style='padding:0;width:570px !important;height:590px !important;'>"+ 
					"<iframe style='width:570px !important;height:544px !important;' frameborder='0' src='../../admin/menu/manageMenuList'></iframe>"+
				"</div>"+
			"</div>";
			$(document).find("body").append(popup);
			 
			var offset = $("body").offset();
			var size = ($("body").width()-570)/2; 
			
			$("#"+popid).jqxWindow({
				
				theme:'Lotte-depart',
	            position: { x: offset.left + size, y: offset.top + 150} ,
	            maxHeight: 590, maxWidth:  570, minHeight:590, minWidth:  570, height: 590, width: 570,
	            resizable: true, isModal: true, modalOpacity: 0,
	            initContent: function (a,b,c,d,e) {
	            	$("#"+popid).find("iframe").css({width:"100%",height:"100%"}); 

	            }
	        });
			 
			$("#"+popid).find("iframe").css({width:"100%",height:"100%"});
		}
		function jqxpopupclose(id){
			$('#'+id).jqxWindow('close');
			$('#'+id).remove();  
		}
	</script>
	<style>
	
	.jqx-window-header-Lotte-depart{
		background: #003664 !important;
	    height: 35px !important;
	    margin: 0 !important;
	}
	.jqx-window-header-Lotte-depart > div:first-child{
	width: 85%;
	    line-height: 30px; 
	    float: left;
	    padding-left: 27px;
	    background: url(../../resources/img/ktoto/title_ico.png) 10px 10px no-repeat;
	    font-size: 15px;
	    color: #FFF;
	    letter-spacing: -1px;
	    font-weight: bold;
	}
	.jqx-window-close-button-Lotte-depart{
		background-image: url(../../resources/img/ktoto/btn_close.png);
		cursor:pointer;
		margin-right:3px;
		margin-top:3px;
	}
	.jqx-window-close-button-background-Lotte-depart{
		margin-right:15px !important;
		margin-top:5px;
	}
	</style>
</head>
<body class='blueish'>
	<div class="wrap" style="width:98%; min-width:1360px; margin:0 10px;">
		<div class="container  f_left" style="width:100%; margin:10px 0;">
			<div class="content f_left" style=" width:100%; margin-right:1%;">
				<form id="saveForm" name="saveForm" action="./insertOne">
					<input type="hidden" id="hiddenLocale" name="saveLocale" />
					<input type="hidden" id="hiddenCName" name="saveCname" />
					<div class="group_button f_right">
					<div class="button type2 f_left">
							<input type="button" value="폴더관리" id='btnFolder' width="100%" height="100%" onclick="folderManage();" />
						</div>
						<div class="button type2 f_left">
							<input type="button" value="저장" id='btnTreeGridSave' width="100%" height="100%" onclick="saveStart();" />
						</div>
					</div>
					<div class="grid f_left" style="width:100%; height:500px; margin:5px 0;">
						<div id="treegridMenuList"></div>
					</div>
					<div class="blueish table f_left"  style="width:100%; height:; margin:10px 0;">
						<table  width="100%" cellspacing="0" cellpadding="0" border="0">
							<thead>
								<tr>
									<th scope="col" style="width:20%; height:40px;">메뉴명</th>
									<th scope="col" style="width:15%;">상위메뉴</th>
									<th scope="col" style="width:5%;">정렬순서</th>
									<th scope="col" style="width:5%;">사용여부</th>
									<th scope="col" style="width:10%;">프로그램ID</th>
									<th scope="col" style="width:5%;"> 폴더<br>여부</th>
									<th scope="col" style="width:5%;">전체<br>공개여부</th>
									<th scope="col" style="width:10%;">자원<br>소유구분</th>
									<th scope="col" style="width:25%;">URL</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<div class="cell">
											<input type="hidden" id="txtbtmCID" name="btmCID" />
											<input type="text" id="txtbtmCName" name="btmCName" class="input type2  f_left  m_r10"  style="width:70%; margin:3px 0; " readonly/>
											<div class="button type2 f_right" style="margin:3px 5px;">
												<input type="button" value="다국어" id='btnLanguage' width="100%" height="100%" style="padding:2px 5px 4px 5px !important;" />
											</div>
											<div class="language_label" style="position: absolute; width: 450px; display: none;">
												<p style="float:left;width:100%; margin:0px 0 5px 0;">
													<img src="../../resources/css/images/img_icons/language_icon.png" onclick="$('.language_label').hide();" alt="language" style="padding-left:0 !important;"/>
												</p>
												<div id="language_content"></div>
											</div>
										</div>
									</td>
									<td>
										<div class="cell">
											<input type="hidden" id="hiddenBtmPID" name="btmPID" />
											<input type="text" id="txtBtmPName" readonly name="btmPName" class="input type2 f_left" style="width:80%; margin:3px 0; "/>
											<div class="icon-search f_right pointer" onclick="openWindow('','table');"></div> 
										</div>
									</td>
									<td>
										<div class="cell t_right">
											<input type="text" id="txtBtmSortOrder" name="btmSortOrder" class="input type2 f_left t_right" style="width:87%; margin:3px 0; "/>
										</div>
									</td>
									<td>
										<div class="cell t_center">
											<div class="combobox f_left"  id='cboUseYn' name="btmUseYn" ></div>
										</div>
									</td>
									<td>
										<div class="cell">
											<input type="text" id="txtBtmProgmID" name="btmProgmID" class="input type2 f_left" style="width:92%; margin:3px 0; "/>
										</div>
									</td>
									<td>
										<div class="cell">
											<div class="combobox f_left"  id='cboFolder' name="menuType"></div>
										</div>
									</td>
									<td>
										<div class="cell">
											<div class="combobox f_left"  id='cboCommon' name="btmCommonType" ></div>
										</div>
									</td>
									<td>
										<div class="cell">
											<div class="combobox f_left"  id='cboSource' name="btmSourceOwner" ></div>
										</div>
									</td>
									<td>
										<div class="cell">
											<input type="text" id="txtBtmCLink" name="btmCLink" class="input type2  f_left"  style="width:97%; margin:3px 0; "/>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="group_button f_right">
						<div class="button type2 f_left">
							<input type="button" value="초기화" id='btnReset' width="100%" height="100%" onclick="resetForm();" />
						</div>
						<div class="button type2 f_left">
							<input type="button" value="저장" id='btnSave' width="100%" height="100%" onclick="saveStartOne();" />
						</div>
						<div class="button type3 f_left">
							<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="deleteStart();" />
						</div>
					</div>
				</form>
				<form id="saveAllForm" name="saveAllForm" action="./insertArray">
					<input type="hidden" id="txtArrayCID" name="arrayCID" />
					<input type="hidden" id="txtArrayPID" name="arrayPID" />
					<input type="hidden" id="txtArraySortORder" name="arraySortOrder" />
	 			    <input type="hidden" id="txtArrayUseYn" name="arrayUseYn" />
	 			    <input type="hidden" id="txtArrayProgID" name="arrayProgID" />
	 			    <input type="hidden" id="txtArrayMenuType" name="arrayMenuType" />
	 			    <input type="hidden" id="txtArrayMenuOpenType" name="arrayMenuOpenType" />
	 			    <input type="hidden" id="txtArrayClink" name="arrayClink" />
	 			    <input type="hidden" id="txtArraySourceOwner" name="arraySourceOwner" />
				</form>
			</div>
		</div>          
	</div>
</body>
</html>