<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
    <meta charset="utf-8">
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
    <title>권한이력조회</title>
   		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
		 <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
		 <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
    <script type="text/javascript">
    	var roleArray=[];
    	var grpArray=[];
    	var userArray=[];
        $(document).ready(function() {
        	$("#jqxButtonSearch").jqxButton({ width: '',  theme:'blueish'}); 
        	$("#jqxButtonExcel").jqxButton({ width: '',  theme:'blueish'}); 
        	
            $("#jqxButtonRolesearch").jqxButton({ width: '',  theme:'blueish'}); 
            $("#jqxButtongroupsearch").jqxButton({ width: '',  theme:'blueish'}); 
            $("#jqxButtonUsersearch").jqxButton({ width: '',  theme:'blueish'}); 
             
         	$('#jqxTabs01').jqxTabs({ width: '100%', height: '100%', position: 'top', theme:'blueish', selectedItem: 0});
         	$("#roleSearch").keydown(function(event){ if(event.keyCode == 13) { return makeRoleList(); } });
         	$("#grpSearch").keydown(function(event){ if(event.keyCode == 13) { return makeGroupList(); } });
         	$("#userSearch").keydown(function(event){ if(event.keyCode == 13) { return makeUserList(); } });
         	
        	$("#dateInputStart").jqxInput({placeHolder: "", height: 23, width: 150, minLength: 1, theme:'blueish' });
        	$("#dateInputClose").jqxInput({placeHolder: "", height: 23, width: 150, minLength: 1, theme:'blueish' });
        	
            // 입력시작일, 입력종료일
            $('#dateInputStart, #dateInputClose').datepicker({
                //changeYear: true,
                //changeMonth: true,
                showOn: 'button',
                buttonImage: '../../resources/cmresource/image/icon-calendar.png',
                buttonImageOnly: true,
                dateFormat: 'yy-mm-dd' //2014-12-12
            });
            $('.ui-datepicker-trigger').each(function() {
    			$(this).css("vertical-align", "middle");
				$(this).css("padding-top", "4px");
				$(this).css("margin-left", "4px");
				$(this).css("float", "left");
    		});
            
            var nowDate = new Date();
    		var y = nowDate.getFullYear();
    		var m = nowDate.getMonth() + 1;
    		if(m<10) //1~9월
    			m = "0" + m;
    		var d = nowDate.getDate();
    		if(d<10)
    			d = "0" + d;
    		var calDate = y+"-"+m+"-"+d;
    		
    		$('#dateInputStart').val(calDate); 
    		$('#dateInputClose').val(calDate);
    		
    		$("#hiddenSdat").val(calDate);
    		$("#hiddenEdat").val(calDate);
    		
    		$('#dateInputStart').on('change', 
    				function (event) {
    				   var startDat = $('#dateInputStart').val(); 
    				   var endDat = $('#dateInputClose').val(); 
    				   if(startDat>endDat){
    					   $i.dialog.alert('SYSTEM', '시작일은 종료일보다 클 수 없습니다!'); 
    					  $('#dateInputStart').val(endDat); 
    				   }
    				}); 
    		
    		$('#dateInputClose').on('change', 
    				function (event) {
    				   var endDat = $('#dateInputClose').val(); 
    				   var startDat = $('#dateInputStart').val(); 
    				   if(startDat>endDat){
    					   $i.dialog.alert('SYSTEM', '시작일은 종료일보다 클 수 없습니다!'); 
    					   $('#dateInputClose').val(startDat); 
    				   }
    				}); 
    		
    		$("#nowSelectList").val("R");
    		searchStart();
        });
		function searchStart(){
			$("#hiddenSdat").val($("#dateInputStart").val());
    		$("#hiddenEdat").val($("#dateInputClose").val());
    		var type = $("#nowSelectList").val();
    		if(type=='R') makeRoleList();
    		if(type=='G') makeGroupList();
    		if(type=='U') makeUserList();
    		
    		var $jqxgrid1 = $('#jqxGrid01');
    		$jqxgrid1.jqxGrid('clearselection');
    		var $jqxgrid2 = $('#jqxGrid02');
    		$jqxgrid2.jqxGrid('clearselection');
    		var $jqxgrid3 = $('#jqxGrid03');
    		$jqxgrid3.jqxGrid('clearselection');
        }
        function makeRoleList(){ //롤 목록
        	$("#nowSelectList").val("R");
        	clearGrid();
    		$("#labelRole").html("");
    	//	$("#hiddenUnid").val("");
    	
        	var getList = $i.post("../../admin/menuAuthLog/getLeftList",{type:$("#nowSelectList").val(),search:$("#roleSearch").val(),startDat:$("#hiddenSdat").val(),endDat:$("#hiddenEdat").val()});
        	getList.done(function(data){
        		roleArray=data.returnArray;
	        	var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                        { name: 'ROLE_ID', type: 'string'},
	    					{ name: 'ROLE_NM', type: 'string'}
	                    ],
	                    localdata: data,
	                    sortcolumn: 'ROLE_ID',
	                    async: false,
	                    updaterow: function (rowid, rowdata, commit) {
	                        commit(true);
	                    }
	                };
	     			
	    			var detail = function (row, columnfield, value) {//그리드 선택시 하단 상세
						var newValue = $i.secure.scriptToText(value);
						var link = "<a href=\"javaScript:showRoleLog("+row+");\" style='color: #000000;text-decoration:underline;'>" + newValue + "</a>";
	
						return "<div style='text-align:center; margin:4px 0px 0px 0px;'>" + link + "</div>";
						
					};
	    			
	    				  
	                var dataAdapter = new $.jqx.dataAdapter(source);
	
	                $("#jqxGrid01").jqxGrid(
	                {
	                    width: '100%',
	                    height: '100%',
	    				altrows:true,
	    				pageable: true,
	    				pageSize: 100,
	    			    pageSizeOptions: ['100', '200', '300'],
	                    source: dataAdapter,
	    				theme:'blueish',
	    				columnsheight:25 ,
	    				rowsheight: 25,
	                    columnsresize: true,
	    				//autorowheight: true,
	    				sortable:true,
	                    columns: [
	                      { text: 'ROLE명', datafield: 'ROLE_NM', width: '100%', align:'center', cellsalign: 'center', cellsrenderer:detail}
	                    ]
	                });
	                showRoleLog('init');
               
        	});	
    	}
        function showRoleLog(row){
        	var $loggrid1 = $('#jqxGridRole');
    		$loggrid1.jqxGrid('clearselection');
        	var roleId='';
        	var roleNm='';
        	if(row!='init'){
        		roleId=roleArray[row].ROLE_ID;
        		roleNm=$i.secure.scriptToText(roleArray[row].ROLE_NM);
        	}
        	$("#labelRole").html(roleNm);
        	$("#hiddenRole").val(roleId);
        	var getLog = $i.post("../../admin/menuAuthLog/getLogList",{type:$("#nowSelectList").val(),id:roleId,startDat:$("#hiddenSdat").val(),endDat:$("#hiddenEdat").val()});
        	getLog.done(function(data){
        		var source =
	            {
	                datatype: "json",
	                datafields: [
	                	{ name: 'OBJ_TYP', type: 'string' },
	                	{ name: 'OBJ_TYP_NM', type: 'string' },
	                    { name: 'OBJ_ID', type: 'string' },
	                    { name: 'OBJ_NAME', type: 'string'},
	                    { name: 'ACTION_NM', type: 'string' },
	                    { name: 'TARGET_TYP', type: 'string' },
	                    { name: 'TARGET_TYP_NM', type: 'string' },
	                    { name: 'TARGET_ID', type: 'string' },
	                    { name: 'TARGET_NAME', type: 'string' },
	                    { name: 'INSERT_DAT', type: 'string' },
	                    { name: 'INSERT_EMP', type: 'string' }
	                ],
	                localdata: data.returnArray,
	                updaterow: function (rowid, rowdata, commit) {
	                    commit(true);
	                }
	            };
        		var noScript = function (row, columnfield, value) {//center
    				var newValue = $i.secure.scriptToText(value);
					return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
				}; 
				var actionCell = function (row, columnfield, value) {//center
					var newValue = value;
    				if(value=='매핑'){
    					newValue = "<a style='color:blue;font-weight:bold;'>"+value+"</a>";
    				}else if(value=='매핑해제'){
    					newValue = "<a style='color:red;font-weight:bold;'>"+value+"</a>";
    				}else{
    					newValue = "<a style='font-weight:bold;'>"+value+"</a>";
    				}
    				return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
				}; 
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#jqxGridRole").jqxGrid(
	            {
	            	width: '100%',
					height: '100%',
					altrows:true,
	                source: dataAdapter,
					theme:'blueish',
	                columnsresize: true,
	                sortable : true,
	                pageable: true,
					pagesizeoptions: ['100', '200', '300'],
					pagesize:100,
					pagermode: 'default',
	                columns: [
	                	{ text: '작업자', datafield:'INSERT_EMP', align:'center', cellsalign:'center', width:'8%'},
	                	{ text: '일시', datafield:'INSERT_DAT', align:'center', cellsalign:'center', width:'12%'},
						{ text: '구분', datafield:'OBJ_TYP_NM', align:'center', cellsalign:'center', width:'10%',columngroup:"A"},
						{ text: 'ID', datafield:'OBJ_ID', align:'center', cellsalign:'center', width:'10%',columngroup:"A"},
						{ text: '이름', datafield:'OBJ_NAME', align:'center', cellsalign:'center', width:'16%',cellsrenderer:noScript,columngroup:"A"},
	                	{ text: '액션' , datafield:'ACTION_NM', align:'center', cellsalign:'center', width: '8%',cellsrenderer:actionCell},
	                	{ text: '구분', datafield:'TARGET_TYP_NM', align:'center', cellsalign:'center', width:'10%',columngroup:"B"},
						{ text: 'ID', datafield:'TARGET_ID', align:'center', cellsalign:'center', width:'10%',columngroup:"B"},
						{ text: '이름', datafield:'TARGET_NAME', align:'center', cellsalign:'center', width:'16%',cellsrenderer:noScript,columngroup:"B"}
	                ],
	            	columngroups:[
						{ text : '대상', align:'center', name:'A'},
						{ text : '타겟', align:'center', name:'B'}
	                ]

	            });
        	});
        }
        
        
        
        function makeGroupList(){ //그룹 목록
        	$("#nowSelectList").val("G");
        	clearGrid();
    		$("#labelGrp").html("");
    	//	$("#hiddenUnid").val("");
    	
        	var getList = $i.post("../../admin/menuAuthLog/getLeftList",{type:$("#nowSelectList").val(),search:$("#grpSearch").val(),startDat:$("#hiddenSdat").val(),endDat:$("#hiddenEdat").val()});
        	getList.done(function(data){
        		grpArray=data.returnArray;
	        	var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                        { name: 'GRP_ID', type: 'string'},
	    					{ name: 'GRP_NM', type: 'string'}
	                    ],
	                    localdata: data,
	                    sortcolumn: 'GRP_ID',
	                    async: false,
	                    updaterow: function (rowid, rowdata, commit) {
	                        commit(true);
	                    }
	                };
	     			
	    			var detail = function (row, columnfield, value) {//그리드 선택시 하단 상세
						var newValue = $i.secure.scriptToText(value);
						var link = "<a href=\"javaScript:showGroupLog("+row+");\" style='color: #000000;text-decoration:underline;'>" + newValue + "</a>";
	
						return "<div style='text-align:center; margin:4px 0px 0px 0px;'>" + link + "</div>";
						
					};
	    			
	    				  
	                var dataAdapter = new $.jqx.dataAdapter(source);
	
	                $("#jqxGrid02").jqxGrid(
	                {
	                    width: '100%',
	                    height: '100%',
	    				altrows:true,
	    				pageable: true,
	    				pageSize: 100,
	    			    pageSizeOptions: ['100', '200', '300'],
	                    source: dataAdapter,
	    				theme:'blueish',
	    				columnsheight:25 ,
	    				rowsheight: 25,
	                    columnsresize: true,
	    				//autorowheight: true,
	    				sortable:true,
	                    columns: [
	                      { text: '그룹명', datafield: 'GRP_NM', width: '100%', align:'center', cellsalign: 'center', cellsrenderer:detail}
	                    ]
	                });
	                showGroupLog('init');
        	});	
    	}
        function showGroupLog(row){
    		var $loggrid2 = $('#jqxGridGroup');
    		$loggrid2.jqxGrid('clearselection');
        	var grpId='';
        	var grpNm='';
        	if(row!='init'){
        		grpId=grpArray[row].GRP_ID;
        		grpNm=$i.secure.scriptToText(grpArray[row].GRP_NM);
        	}
        	$("#labelGrp").html(grpNm);
        	$("#hiddenGroup").val(grpId);
        	var getLog = $i.post("../../admin/menuAuthLog/getLogList",{type:$("#nowSelectList").val(),id:grpId,startDat:$("#hiddenSdat").val(),endDat:$("#hiddenEdat").val()});
        	getLog.done(function(data){
        		var source =
	            {
	                datatype: "json",
	                datafields: [
	                	{ name: 'OBJ_TYP', type: 'string' },
	                	{ name: 'OBJ_TYP_NM', type: 'string' },
	                    { name: 'OBJ_ID', type: 'string' },
	                    { name: 'OBJ_NAME', type: 'string'},
	                    { name: 'ACTION_NM', type: 'string' },
	                    { name: 'TARGET_TYP', type: 'string' },
	                    { name: 'TARGET_TYP_NM', type: 'string' },
	                    { name: 'TARGET_ID', type: 'string' },
	                    { name: 'TARGET_NAME', type: 'string' },
	                    { name: 'INSERT_DAT', type: 'string' },
	                    { name: 'INSERT_EMP', type: 'string' }
	                ],
	                localdata: data.returnArray,
	                updaterow: function (rowid, rowdata, commit) {
	                    commit(true);
	                }
	            };
        		var noScript = function (row, columnfield, value) {//center
    				var newValue = $i.secure.scriptToText(value);
					return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
				}; 
				var actionCell = function (row, columnfield, value) {//center
					var newValue = value;
					if(value=='매핑'){
    					newValue = "<a style='color:blue;font-weight:bold;'>"+value+"</a>";
    				}else if(value=='매핑해제'){
    					newValue = "<a style='color:red;font-weight:bold;'>"+value+"</a>";
    				}else{
    					newValue = "<a style='font-weight:bold;'>"+value+"</a>";
    				}
    				return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
				}; 
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#jqxGridGroup").jqxGrid(
	            {
	            	width: '100%',
					height: '100%',
					altrows:true,
	                source: dataAdapter,
					theme:'blueish',
	                columnsresize: true,
	                sortable : true,
	                pageable: true,
					pagesizeoptions: ['100', '200', '300'],
					pagesize:100,
					pagermode: 'default',
	                columns: [
	                	{ text: '작업자', datafield:'INSERT_EMP', align:'center', cellsalign:'center', width:'8%'},
	                	{ text: '일시', datafield:'INSERT_DAT', align:'center', cellsalign:'center', width:'12%'},
						{ text: '구분', datafield:'OBJ_TYP_NM', align:'center', cellsalign:'center', width:'10%',columngroup:"A"},
						{ text: 'ID', datafield:'OBJ_ID', align:'center', cellsalign:'center', width:'10%',columngroup:"A"},
						{ text: '이름', datafield:'OBJ_NAME', align:'center', cellsalign:'center', width:'16%',cellsrenderer:noScript,columngroup:"A"},
	                	{ text: '액션' , datafield:'ACTION_NM', align:'center', cellsalign:'center', width: '8%',cellsrenderer:actionCell},
	                	{ text: '구분', datafield:'TARGET_TYP_NM', align:'center', cellsalign:'center', width:'10%',columngroup:"B"},
						{ text: 'ID', datafield:'TARGET_ID', align:'center', cellsalign:'center', width:'10%',columngroup:"B"},
						{ text: '이름', datafield:'TARGET_NAME', align:'center', cellsalign:'center', width:'16%',cellsrenderer:noScript,columngroup:"B"}
	                ],
	            	columngroups:[
						{ text : '대상', align:'center', name:'A'},
						{ text : '타겟', align:'center', name:'B'}
	                ]

	            });
        	});
        	
        }
        function clearGrid(){
        	var type=$("#nowSelectList").val();
        	if(type!='R'){
        		var $jqxgrid1 = $('#jqxGrid01');
        		$jqxgrid1.jqxGrid('clearselection');
        		var $loggrid1 = $('#jqxGridRole');
        		$loggrid1.jqxGrid('clearselection');
        		$loggrid1.jqxGrid('clear');
        		$("#roleSearch").val("");
        		$("#labelRole").html('');
        	}
        	if(type!='G'){
        		var $jqxgrid2 = $('#jqxGrid02');
        		$jqxgrid2.jqxGrid('clearselection');
        		var $loggrid2 = $('#jqxGridGroup');
        		$loggrid2.jqxGrid('clearselection');
        		$loggrid2.jqxGrid('clear');
             	$("#grpSearch").val("");
             	$("#labelGrp").html('');
        	}
        	if(type!='U'){
        		var $jqxgrid3 = $('#jqxGrid03');
        		$jqxgrid3.jqxGrid('clearselection');
        		var $loggrid3 = $('#jqxGridUser');
        		$loggrid3.jqxGrid('clearselection');
        		$loggrid3.jqxGrid('clear');
             	$("#userSearch").val("");
             	$("#labelUser").html('');
        	}
        	
        }
        function makeUserList(){ //사용자 목록
        	$("#nowSelectList").val("U");
        	clearGrid();
    		
        	var getList = $i.post("../../admin/menuAuthLog/getLeftList",{type:$("#nowSelectList").val(),search:$("#userSearch").val(),startDat:$("#hiddenSdat").val(),endDat:$("#hiddenEdat").val()});
        	getList.done(function(data){
        		userArray=data.returnArray;
	        	var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                        { name: 'USER_ID', type: 'string'},
	    					{ name: 'USER_NM', type: 'string'},
	    					{ name: 'GRP_ID', type: 'string'},
	    					{ name: 'GRP_NM', type: 'string'}
	                    ],
	                    localdata: data,
	                    sortcolumn: 'USER_ID',
	                    async: false,
	                    updaterow: function (rowid, rowdata, commit) {
	                        commit(true);
	                    }
	                };
	     			
	    			var detail = function (row, columnfield, value) {//그리드 선택시 하단 상세
						var newValue = $i.secure.scriptToText(value);
						var link = "<a href=\"javaScript:showUserLog("+row+");\" style='color: #000000;text-decoration:underline;'>" + newValue + "</a>";
	
						return "<div style='text-align:center; margin:4px 0px 0px 0px;'>" + link + "</div>";
						
					};
	    			
	    				  
	                var dataAdapter = new $.jqx.dataAdapter(source);
	
	                $("#jqxGrid03").jqxGrid(
	                {
	                    width: '100%',
	                    height: '100%',
	    				altrows:true,
	    				pageable: true,
	    				pageSize: 100,
	    			    pageSizeOptions: ['100', '200', '300'],
	                    source: dataAdapter,
	    				theme:'blueish',
	    				columnsheight:25 ,
	    				rowsheight: 25,
	                    columnsresize: true,
	    				//autorowheight: true,
	    				sortable:true,
	                    columns: [
	                    	{ text: '그룹', datafield: 'GRP_NM', width: '45%', align:'center', cellsalign: 'center'},
	                    	{ text: '사용자ID', datafield: 'USER_ID', width: '30%', align:'center', cellsalign: 'center', cellsrenderer:detail},
	                      	{ text: '사용자명', datafield: 'USER_NM', width: '25%', align:'center', cellsalign: 'center'}
	                    ]
	                });
	                showUserLog('init');
        	});	
    	}
        function showUserLog(row){
    		var $loggrid3 = $('#jqxGridUser');
    		$loggrid3.jqxGrid('clearselection');
        	var userId='';
        	var userNm='';
        	if(row!='init'){
        		userId=userArray[row].USER_ID;
        		userNm=$i.secure.scriptToText(userArray[row].USER_NM);
        		$("#labelUser").html(userId+' / '+userNm);
        	}else{
        		$("#labelUser").html('');
        	}
        	
        	$("#hiddenUser").val(userId);
        	var getLog = $i.post("../../admin/menuAuthLog/getLogList",{type:$("#nowSelectList").val(),id:userId,startDat:$("#hiddenSdat").val(),endDat:$("#hiddenEdat").val()});
        	getLog.done(function(data){
        	
        		var source =
	            {
	                datatype: "json",
	                datafields: [
	                	{ name: 'OBJ_TYP', type: 'string' },
	                	{ name: 'OBJ_TYP_NM', type: 'string' },
	                    { name: 'OBJ_ID', type: 'string' },
	                    { name: 'OBJ_NAME', type: 'string'},
	                    { name: 'ACTION_NM', type: 'string' },
	                    { name: 'TARGET_TYP', type: 'string' },
	                    { name: 'TARGET_TYP_NM', type: 'string' },
	                    { name: 'TARGET_ID', type: 'string' },
	                    { name: 'TARGET_NAME', type: 'string' },
	                    { name: 'INSERT_DAT', type: 'string' },
	                    { name: 'INSERT_EMP', type: 'string' }
	                ],
	                localdata: data.returnArray,
	                updaterow: function (rowid, rowdata, commit) {
	                    commit(true);
	                }
	            };
        		var noScript = function (row, columnfield, value) {//center
    				var newValue = $i.secure.scriptToText(value);
					return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
				}; 
				var actionCell = function (row, columnfield, value) {//center
					var newValue = value;
					if(value=='매핑'){
    					newValue = "<a style='color:blue;font-weight:bold;'>"+value+"</a>";
    				}else if(value=='매핑해제'){
    					newValue = "<a style='color:red;font-weight:bold;'>"+value+"</a>";
    				}else{
    					newValue = "<a style='font-weight:bold;'>"+value+"</a>";
    				}
    				return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
				}; 
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#jqxGridUser").jqxGrid(
	            {
	            	width: '100%',
					height: '100%',
					altrows:true,
	                source: dataAdapter,
					theme:'blueish',
	                columnsresize: true,
	                sortable : true,
	                pageable: true,
					pagesizeoptions: ['100', '200', '300'],
					pagesize:100,
					pagermode: 'default',
	                columns: [
	                	{ text: '작업자', datafield:'INSERT_EMP', align:'center', cellsalign:'center', width:'8%'},
	                	{ text: '일시', datafield:'INSERT_DAT', align:'center', cellsalign:'center', width:'12%'},
						{ text: '구분', datafield:'OBJ_TYP_NM', align:'center', cellsalign:'center', width:'10%',columngroup:"A"},
						{ text: 'ID', datafield:'OBJ_ID', align:'center', cellsalign:'center', width:'10%',columngroup:"A"},
						{ text: '이름', datafield:'OBJ_NAME', align:'center', cellsalign:'center', width:'16%',cellsrenderer:noScript,columngroup:"A"},
	                	{ text: '액션' , datafield:'ACTION_NM', align:'center', cellsalign:'center', width: '8%',cellsrenderer:actionCell},
	                	{ text: '구분', datafield:'TARGET_TYP_NM', align:'center', cellsalign:'center', width:'10%',columngroup:"B"},
						{ text: 'ID', datafield:'TARGET_ID', align:'center', cellsalign:'center', width:'10%',columngroup:"B"},
						{ text: '이름', datafield:'TARGET_NAME', align:'center', cellsalign:'center', width:'16%',cellsrenderer:noScript,columngroup:"B"}
	                ],
	            	columngroups:[
						{ text : '대상', align:'center', name:'A'},
						{ text : '타겟', align:'center', name:'B'}
	                ]

	            });
        	});
        	
        }
        function goExcel(){
        	var sDat = $("#hiddenSdat").val();
    		var eDat = $("#hiddenEdat").val();
    		var type = $("#nowSelectList").val();
    		var id = '';
    		if(type=='R') 
    			id=$("#hiddenRole").val();
    		else if(type=='G')
    			id=$("#hiddenGroup").val();
    		else if(type=='U')
    			id=$("#hiddenUser").val();
    		
    		if(id!=null && id!=''){
    			windowOpen('../../admin/menuAuthLog/excelDownByMenuAuthLog?type='+type+'&id='+id+'&startDat='+sDat+'&endDat='+eDat, "_self");
    		}else{
    			$i.dialog.error('SYSTEM','조회된 데이터가 없습니다.');
    		}
    		
    	}
    </script>

    </head>
    <body class='blueish'>
    <!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
    <div class="wrap" style="width:98%; min-width:1656px; margin:0 10px;">
     <div class="header f_left" style="width:100%; height:27px; margin:10px 0">
      	<input type="hidden" id="hiddenSdat"/>
        <input type="hidden" id="hiddenEdat"/>
        <div class="label type1 f_left">시작일 : </div>
        <input id="dateInputStart" type="text" class="f_left" style="margin-left:5px;" readOnly/>
         
      
        <div class="label type1 f_left">종료일 : </div>
        <input id="dateInputClose" type="text" class="f_left" style="margin-left:5px;" readOnly/>
    
        <div class="group_button f_right">
         <div class="button type1 f_left">
           <input type="button" value="조회" id='jqxButtonSearch' width="100%" height="100%" onclick="searchStart();"/>
         </div>
          <div class="button type1 f_left">
			<input type="button" value="Excel" id='jqxButtonExcel' width="100%" height="100%" onclick="goExcel();"/>
		  </div>
        </div>
        <!--group_button--> 
      <!--//header-->
		</div>
      <!--//header-->

		<div class="container  f_left" style="width:100%; margin:0 0 10px 0;">
		<div class="tabs f_left" style=" width:100%; height:635px; margin-top:0px;">
				<div id='jqxTabs01'>
				<ul>
				  <li style="margin-left: 0px;" onclick="makeRoleList();">Role</li>
				  <li onclick="makeGroupList();">그룹</li>
				  <li onclick="makeUserList();">사용자</li>
				</ul>
				<input type="hidden" id="nowSelectList"/>
				<div class="tabs_content" style="height:100%; overflow-y:auto; overflow-x:hidden;">
				  <div class="content f_left" style=" width:350px; margin:0; padding:0 15px;  background:#eee; height:100%;">
                    <div class="group f_left" style="margin:10px 0; width:100%;">
                      <div class="label type2 f_left">Role</div>
                          <input type="text" value="" id="roleSearch" class="input type1 f_left w50p"  style="padding:3px;width:100%; margin:0px 0px 0 5px; "/>
                          <div class="button type2 f_left" style="margin-left:5px;"><input type="button" value="검색" id='jqxButtonRolesearch' width="100%" height="100%" onclick="makeRoleList();"/></div>
                      </div>
                      <!--group-->
						<div class="f_left"  style="width:100%; height:520px; margin:0px 0; border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1;">
							<div id="jqxGrid01"></div>
				    	</div>
                        
                                          
                    </div>
					<!--//content-->
						<div class="content f_left" style=" width:77%;padding:10px 0;">
						<div class="group f_left  w100p m_b5">
						  <div class="label type2 f_left" style="margin-left:20px;margin-bottom:10px;">Role:</div>
                         	<span id="labelRole" style="line-height: 24px;font-weight: bold;margin-left: 5px;"></span>
                         	<input type="hidden" id="hiddenRole"/>
						</div>
						<!--group-->
                        <div class="blueish f_left" style="width:96%; height:520px; margin-left:20px;">
                        	<div id="jqxGridRole"></div>
                        <!--datatable_fixed--> 
                     	</div>
                     <!--datatable -->  
                        </div>
						<!--//content-->
					</div>
				<!--//tabs_content(Role)-->
				<div class="tabs_content" style="height:100%; overflow-y:auto; overflow-x:hidden;">
				   <div class="content f_left" style=" width:350px; margin:0; padding:0 15px;  background:#eee; height:100%;">
                    <div class="group f_left" style="margin:10px 0; width:100%;">
                      <div class="label type2 f_left">그룹</div>
                          <input type="text" value="" id="grpSearch" class="input type1 f_left w50p"  style="padding:3px;width:100%; margin:0px 0px 0 5px; "/>
                          <div class="button type2 f_left" style="margin-left:5px;"><input type="button" value="검색" id='jqxButtongroupsearch' width="100%" height="100%" onclick="makeGroupList();"/></div>
                      </div>
                      <!--group-->
						<div class="f_left"  style="width:100%; height:520px; margin:0px 0; border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1;">
							<div id="jqxGrid02"></div>
				    </div>
						<!--table -->  
                                          
                    </div>
					<!--//content-->
						<div class="content f_left" style=" width:77%;padding:10px 0;">
						<div class="group f_left  w100p m_b5">
						  <div class="label type2 f_left" style="margin-left:20px;margin-bottom:10px;">그룹:</div>
						  <span id="labelGrp" style="line-height: 24px;font-weight: bold;margin-left: 5px;"></span>
						  <input type="hidden" id="hiddenGroup"/>
						</div>
						<!--group-->
                        <div class="blueish f_left" style="width:96%; height:520px; margin-left:20px;">
                        	<div id="jqxGridGroup"></div>
                        <!--datatable_fixed--> 
                     	</div>
                     <!--datatable -->  
                        </div>
						<!--//content-->
					</div>
				<!--//tabs_content(그룹)-->
				<div class="tabs_content" style="height:100%; overflow-y:auto; overflow-x:hidden;">
				  <div class="content f_left" style=" width:350px; margin:0; padding:0 15px;  background:#eee; height:100%;">
                    <div class="group f_left" style="margin:10px 0; width:100%;">
                      <div class="label type2 f_left">사용자</div>
                          <input type="text" value="" id="userSearch" class="input type1 f_left w50p"  style="padding:3px;width:100%; margin:0px 0px 0 5px; "/>
                          <div class="button type2 f_left" style="margin-left:5px;"><input type="button" value="검색" id='jqxButtonUsersearch' width="100%" height="100%" onclick="makeUserList();"/></div>
                      </div>
                      <!--group-->
						<div class="blueish f_left"  style="width:100%; height:520px; margin:0px 0; border-left:1px solid #e1e1e1;border-right:1px solid #e1e1e1;">
							<div id="jqxGrid03"></div>
				    </div>
						<!--table -->  
                                          
                    </div>
					<!--//content-->
						<div class="content f_left" style=" width:77%;padding:10px 0;">
						<div class="group f_left  w100p m_b5">
						  <div class="label type2 f_left" style="margin-left:20px;margin-bottom:10px;">사용자:</div>
						  <span id="labelUser"  style="line-height: 24px;font-weight: bold;margin-left: 5px;"></span>
						  <input type="hidden" id="hiddenUser"/>
						</div>
						<!--group-->
                       <div class="blueish f_left" style="width:96%; height:520px; margin-left:20px;">
                        	<div id="jqxGridUser"></div>
                        </div>
                        <!--datatable_fixed--> 
                     </div>
                     <!--datatable -->  
                        </div>
						<!--//content-->
					</div>
				<!--//tabs_content(사용자)-->
			</div>
				<!--//jqxTabs-->
			</div>
		<!--//tabs-->
	</div>
		<!--//container-->
	</div>
<!--//wrap-->
</body>
</html>