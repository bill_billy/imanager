 <%@ page language="java" contentType="application/vnd.xls;charset=UTF-8" pageEncoding="utf-8"%>   -
<%@page import="java.net.URLEncoder"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%

	String header =request.getHeader("User-Agent");
	String browser="";  
	String docName = URLEncoder.encode("권한이력조회", "utf-8");  
	 
	System.out.println("header++++++++++++++++++++++++++++++"+header+":::::"+docName);
	 
	
    response.setHeader("Content-Disposition", "attachment; filename="+docName+".xls"); 
    response.setHeader("Content-Description", "JSP Generated Data");

%>   -
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>권한이력조회</title>
	
	</head>
 	<body>
		<table width="100%;" id="tableList" class="fix_rable" cellspacing="0" cellpadding="0" border="1">		
			<thead>			
				<tr> 
					<th rowspan=2 style="background-color:#eff0f0;" >작업자</th>			
					<th rowspan=2 style="background-color:#eff0f0;" >일시</th>
					<th colspan=3 style="background-color:#eff0f0;" >대상</th>	
					<th rowspan=2 style="background-color:#eff0f0;" >액션</th>						
					<th colspan=3 style="background-color:#eff0f0;" >타겟</th>
				</tr>
				<tr>
					<th style="background-color:#eff0f0;" >구분</th>		
					<th style="background-color:#eff0f0;" >ID</th>	
					<th style="background-color:#eff0f0;" >이름</th>	
					<th style="background-color:#eff0f0;" >구분</th>	
					<th style="background-color:#eff0f0;" >ID</th>	
					<th style="background-color:#eff0f0;" >이름</th>		
				</tr>
			</thead>
		    <tbody>
			<c:choose> 		
					<c:when test="${mainList != null && not empty mainList}"> 	
						<c:set var = "SQUOT">'</c:set>
						<c:set var = "DQUOT">"</c:set>
						<c:forEach items="${mainList}" var="rows" varStatus="loop">
							<tr>	
								<td align="center">${rows.insert_emp}</td>
								<td align="center">${rows.insert_dat}</td>
								<td align="center">${rows.obj_typ_nm}</td>
								<td align="center">${rows.obj_id}</td>
								<td align="center">${fn:replace(fn:replace(fn:replace(fn:replace(rows.obj_name,SQUOT,"&#39;"),DQUOT,"&quot;"),"<","&lt;"),">","&gt;")}</td>
								<c:choose> 
									<c:when test="${rows.action_nm=='매핑'}"> 
										<td align="center" style="width:100px;"><a style="font-weight:bold;color:blue;">${rows.action_nm}</a></td>
									</c:when>
									<c:when test="${rows.action_nm=='매핑해제'}"> 
										<td align="center" style="width:100px;"><a style="font-weight:bold;color:red;">${rows.action_nm}</a></td>
									</c:when>
									<c:otherwise>
										<td align="center" style="width:100px;"><a style="font-weight:bold;">${rows.action_nm}</a></td>
									</c:otherwise>
								</c:choose>
								<td align="center">${rows.target_typ_nm}</td>
								<td align="center">${rows.target_id}</td>
								<td align="center">${fn:replace(fn:replace(fn:replace(fn:replace(rows.target_name,SQUOT,"&#39;"),DQUOT,"&quot;"),"<","&lt;"),">","&gt;")}</td>
							</tr>
						</c:forEach> 		
					</c:when> 		
					<c:otherwise> 				
						<tr> 					
							<td class="gtd"  colspan="9">조회된 데이터가 없습니다.</td> 			
						</tr> 		
					</c:otherwise> 
				</c:choose>  
			</tbody> 
		</table>
	</body>
</html>