<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.net.URLEncoder"%>
<%
    request.setCharacterEncoding("UTF-8");
 %>

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Language" content="ko">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>엑셀이력관리</title>

<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	   	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish-tree/css/jqx.blueish-tree.css"/>
	   	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish-tree/css/style.blueish-tree.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	    

<script src="../../resources/cmresource/js/iplanbiz/web/ui.alert.js" charset="UTF-8"></script>

<script type="text/javascript">
	$(document).ready(function() {

		var nowDate = new Date();
		var y = nowDate.getFullYear();
		var m = nowDate.getMonth() + 1;
		if(m<10) //1~9월
			m = "0" + m;
		var d = nowDate.getDate();
		if(d<10)
			d = "0" + d;
		var calDate = y+"-"+m+"-"+d;
		
		$('#startDat').val(calDate); 
		$('#endDat').val(calDate);
		
		$('#jqxTabs').jqxTabs({
			width : '100%',
			height : '100%',
			position : 'top',
			theme : 'blueish',
			selectedItem : 0
		});
		$('#startDat').jqxInput({placeHolder: "", height: 23, width: 148, minLength: 1, theme:'blueish'});
		$('#endDat').jqxInput({placeHolder: "", height: 23, width: 148, minLength: 1, theme:'blueish'});
		
		
		$('#startDat').on('change', 
				function (event) {
				   var startDat = $('#startDat').val(); 
				   var endDat = $('#endDat').val(); 
				   if(startDat>endDat){
					   $i.dialog.alert('SYSTEM', '시작일은 종료일보다 클 수 없습니다!'); 
					  $('#startDat').val(endDat); 
				   }
				}); 
		
		$('#endDat').on('change', 
				function (event) {
				   var endDat = $('#endDat').val(); 
				   var startDat = $('#startDat').val(); 
				   if(startDat>endDat){
					   $i.dialog.alert('SYSTEM', '시작일은 종료일보다 클 수 없습니다!'); 
					   $('#endDat').val(startDat); 
				   }
				}); 
		
		
		
		$("#startDat").datepicker({
			showOn : "button",
			buttonImageOnly : true,
			dateFormat : "yy-mm-dd",
			buttonImage:"/resource/css/images/img_icons/calendarIcon.png"
		});
		$("#endDat").datepicker({
			showOn : "button",
			buttonImageOnly : true,
			dateFormat : "yy-mm-dd",
			buttonImage:"/resource/css/images/img_icons/calendarIcon.png"
		});
		$('.ui-datepicker-trigger').each(function() {
			$(this).css("vertical-align", "middle");
			$(this).css("padding-top", "4px");
			$(this).css("margin-left", "4px");
			$(this).css("float", "left");
		});

		$('#reportButton').jqxButton({
			width : '',
			theme : 'blueish'
		}).on('click', function() {
			goExcelCID();
		});
		$('#dateButton').jqxButton({
			width : '',
			theme : 'blueish'
		}).on('click', function() {
			goExcelDATE();
		});
		$('#dateSearchButton').jqxButton({
			width : '',
			theme : 'blueish'
		}).on('click', function() {
			formDateGrid();
		});
		viewPage('reportView');
		search();
		makeGridReport();
		//splitter
		$('#splitter').jqxSplitter({
			height : '100%',
			width : '100%',
			panels : [ {
				size : '300px'
			}, {
				size : '1280px'
			} ],
			theme : "blueish"
		});

		$("#feedExpander").jqxExpander({
			toggleMode : 'none',
			theme : "blueish",
			showArrow : false,
			width : "100%",
			height : "100%",
			initContent : function() {
				$('#treeMenuList').jqxTree({
					height : '100%',
					width : '300px',
					theme : "blueish-tree"
				});
			}
		});

	});
	function goExcelCID() {
		var rows=$('#reportExcel').jqxGrid('getrows');
		if(rows.length==0){
			 $i.dialog.alert('SYSTEM', '출력할 결과가 없습니다'); 
			return false;
		}
		var rpName= $("#reportExcel").jqxGrid('getrowdata', 0);
		
		var filename = '엑셀이력관리';
		var param = '?';
		param += '&cid=' + $('#cid').val();
		param += '&fileName=' + filename;
		location.href = "./downExcelLogReport" + param;
		
		return false;
	}
	function goExcelDATE() {
		var rows=$('#dateExcel').jqxGrid('getrows');
		if(rows.length==0){
			$i.dialog.alert('SYSTEM', '출력할 결과가 없습니다'); 
			return false;
		}
		var st = $('#start_dat').val();
		var ed = $('#end_dat').val();
		var filename = '엑셀이력관리';
		var param = '?';
		param += '&fileName=' + filename;
		param += '&start_dat=' + st;
		param += '&end_dat=' +  ed;
		
		location.href = "./downExcelLogDate" + param;
		
		return false;
	}
	function formDateGrid(){
		var st= $('#startDat').val();
		var ed=$('#endDat').val();
		
		var dat = $i.post("./getExcelLogDate", {start_dat:st,end_dat:ed});
		dat.done(function(data){
			$('#start_dat').val(st);
			$('#end_dat').val(ed);
		
			if (data.length != 0) {
				makeGridDate(data);
			} else {
				makeGridDate();
			}
				
			});
	}
	//조회
	function getIconImage(MENU_TYPE, MENU_TYPE, MENU_OPEN_TYPE) {
				var icon = '';
				
				if(MENU_TYPE!=null&&MENU_TYPE=="2") {
					icon = '../../resources/cmresource/image/icon-page.png';
					//파일
				} else {
					//폴더
					icon = '../../resources/cmresource/image/icon-foldernoopen.png';
				}
				return icon;
			}

	function search(){
		var dat = $i.post("/admin/help/helpManagerMenuList", {});
		dat.done(function(menu){
			$("#treeMenuList").jqxTreeGrid('clear');
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: 'C_ID'},
					{ name: 'P_ID'},
					{ name: 'C_NAME'},
					{ name: 'MENU_TYPE'},
					{ name: 'MENU_OPEN_TYPE'}
				],
				icons:true,
				pageable:false,  
				localdata: menu.returnArray,
				hierarchy:{
					keyDataField:{name:'C_ID'},
					parentDataField:{name:'P_ID'} 
				},
				id:'C_ID'
			};
			var dataAdapter = new $.jqx.dataAdapter(source, {
                loadComplete: function () {
                }
        	});
			$("#treeMenuList").jqxTreeGrid({source: dataAdapter, height: '100%', width: '100%',altRows: true,theme:"blueish-tree",
				icons:function(rowKey, rowdata) { return getIconImage(rowdata.MENU_TYPE, rowdata.MENU_TYPE, rowdata.MENU_OPEN_TYPE); },
				columns:[
					{ dataField:'C_ID',text:'C_ID',align:'center', hidden:true},
					{ dataField:'P_ID',text:'P_ID',align:'center', hidden:true},
					{ dataField:'C_NAME',text:'C_NAME', algin:'center'
						,cellsrenderer:function(row,datafield,value,rowdata){
							if(rowdata.records!=null){
								var cntlabel = "<span style='color: Blue;' data-role=='treefoldersize'> (" + rowdata.records.length + ")</span>";
								return "<a style='color:#000000;'>"+rowdata.C_NAME+cntlabel+"</a>";
							} else {
								if(rowdata.MENU_TYPE!=null&&rowdata.MENU_TYPE=="2") {
									return "<a href=\"javascript:viewDetail("+rowdata.C_ID+",'"+rowdata.C_NAME+"')\" style='color:#000000;'>"+rowdata.C_NAME+"</a>";
								}else{
									return "<a style='color:#000000;'>"+rowdata.C_NAME+"</a>";
								}
							}
						}
					}
				],
				ready:function(){
				}
			});
			$("#treeMenuList").jqxTreeGrid('expandAll');
		});
	}

	
	function makeGridReport(res) {
		var gridElement = $('#reportExcel');

		var source = {
			datatype : "json",
			updaterow : function(rowid, rowdata, commit) {
				commit(true);
			},
			datafields : [ {
				name : 'YYYYMM',
				type : 'string'
			}, {
				name : 'C_NAME',
				type : 'string'
			},{
				name : 'C_PATH',
				type : 'string'
			},{
				name : 'DEPT_NM',
				type : 'string'
			}, {
				name : 'EMP_ID',
				type : 'string'
			}, {
				name : 'EMP_NM',
				type : 'string'
			}, {
				name : 'IP_ADDR',
				type : 'string'
			}, {
				name : 'INSERT_DAT',
				type : 'string'
			}, {
				name : 'PARAM_NM',
				type : 'string'
			}, {
				name : 'BROWSER',
				type : 'string'
			}, {
				name : 'DEVICE_TYPE',
				type : 'string'
			} ],
			localdata : res
		};

		var dataAdapter = new $.jqx.dataAdapter(source);
		gridElement.jqxGrid({
			width : '100%',
			height : '600px',
			altrows : true,
			pageable : true,
			source : dataAdapter,
			sortable: true,
			theme : 'blueish',
			columnsheight : 28,
			pagesize : 100,
			pagesizeoptions : [ '100', '200', '300' ],
			rowsheight : 28,
			columnsresize : true,
			columns : [ {
				text : "년월",
				datafield : 'YYYYMM',
				width : '80px',
				align : 'center',
				cellsalign : 'center',
				pinned : true
			}, {
				text : "보고서명",
				datafield : 'C_NAME',
				width : '300px',
				align : 'center',
				cellsalign : 'left',
				pinned : true
			}, {
				text : "경로",
				datafield : 'C_PATH',
				width : '300px',
				align : 'center',
				cellsalign : 'left'
			}, {
				text : "부서",
				datafield : 'DEPT_NM',
				width : '300px',
				align : 'center',
				cellsalign : 'center'
			}, {
				text : "아이디",
				datafield : 'EMP_ID',
				width : '150px',
				align : 'center',
				cellsalign : 'center'
			}, {
				text : "이름",
				datafield : 'EMP_NM',
				width : '150px',
				align : 'center',
				cellsalign : 'center', cellsrenderer:function(a,b,c,d,e,f){
					d = d.replace(c,decodeURI(c));
					return d;
				}
			}, {
				text : "IP",
				datafield : 'IP_ADDR',
				width : '150px',
				align : 'center',
				cellsalign : 'center'
			}, {
				text : "일시",
				datafield : 'INSERT_DAT',
				width : '150px',
				align : 'center',
				cellsalign : 'center'
			}, {
				text : "조회조건",
				datafield : 'PARAM_NM',
				width : '300PX',
				align : 'center',
				cellsalign : 'left'
			}, {
				text : "브라우저",
				datafield : 'BROWSER',
				width : '90px',
				align : 'center',
				cellsalign : 'center'
			}, {
				text : "접속기기",
				datafield : 'DEVICE_TYPE',
				width : '90px',
				align : 'center',
				cellsalign : 'center'
			}  ]
		});
	}

	
	function makeGridDate(res) {
		var gridElement = $('#dateExcel');

		var source = {
			datatype : "json",
			updaterow : function(rowid, rowdata, commit) {
				commit(true);
			},
			datafields : [ {
				name : 'YYYYMM',
				type : 'string'
			}, {
				name : 'C_NAME',
				type : 'string'
			}, {
				name : 'C_PATH',
				type : 'string'
			}, {
				name : 'DEPT_NM',
				type : 'string'
			}, {
				name : 'EMP_ID',
				type : 'string'
			}, {
				name : 'EMP_NM',
				type : 'string'
			}, {
				name : 'IP_ADDR',
				type : 'string'
			}, {
				name : 'INSERT_DAT',
				type : 'string'
			}, {
				name : 'PARAM_NM',
				type : 'string'
			}, {
				name : 'BROWSER',
				type : 'string'
			}, {
				name : 'DEVICE_TYPE',
				type : 'string'
			} ],
			localdata : res
		};

		var dataAdapter = new $.jqx.dataAdapter(source);

		gridElement.jqxGrid({
			width : '100%',
			height : '600px',
			altrows : true,
			pageable : true,
			source : dataAdapter,
			sortable: true,
			theme : 'blueish',
			columnsheight : 28,
			pagesize : 100,
			pagesizeoptions : [ '100', '200', '300' ],
			rowsheight : 28,
			columnsresize : true,
			columns : [ {
				text : "년월",
				datafield : 'YYYYMM',
				width : '80px',
				align : 'center',
				cellsalign : 'center',
				pinned : true
			}, {
				text : "보고서명",
				datafield : 'C_NAME',
				width : '300px',
				align : 'center',
				cellsalign : 'left',
				pinned : true
			}, {
				text : "경로",
				datafield : 'C_PATH',
				width : '300px',
				align : 'center',
				cellsalign : 'left'
			}, {
				text : "부서",
				datafield : 'DEPT_NM',
				width : '300px',
				align : 'center',
				cellsalign : 'center'
			}, {
				text : "아이디",
				datafield : 'EMP_ID',
				width : '150px',
				align : 'center',
				cellsalign : 'center'
			}, {
				text : "이름",
				datafield : 'EMP_NM',
				width : '150px',
				align : 'center',
				cellsalign : 'center',
				cellsrenderer:function(a,b,c,d,e,f){
						d = d.replace(c,decodeURI(c));
						return d;
				}
			}, {
				text : "IP",
				datafield : 'IP_ADDR',
				width : '150px',
				align : 'center',
				cellsalign : 'center'
			}, {
				text : "일시",
				datafield : 'INSERT_DAT',
				width : '150px',
				align : 'center',
				cellsalign : 'center'
			}, {
				text : "조회조건",
				datafield : 'PARAM_NM',
				width : '300PX',
				align : 'center',
				cellsalign : 'left'
			}, {
				text : "브라우저",
				datafield : 'BROWSER',
				width : '90px',
				align : 'center',
				cellsalign : 'center'
			}, {
				text : "접속기기",
				datafield : 'DEVICE_TYPE',
				width : '90px',
				align : 'center',
				cellsalign : 'center'
			}  ]
		});
	}
	
	

	function resetForm() {
		$('#cid').val('');
		$('#formMenuName').text("");

	}
	function viewDetail(cId, name) {
		//init
		$('#cid').val('');
		$('#menuNavi').text("");
		var dat = $i.post("./getNavi", {cid:cId});
		dat.done(function(data){
			var navi = data.returnObject.navi;
			//set
			$('#cid').val(cId);
			if(navi){
				$('#menuNavi').text(navi+">"+name);
			}else{
				$('#menuNavi').text(name);
			}
		});
	
		var dat = $i.post("./getExcelLogReport", {cid:cId});
		dat.done(function(data){
				
				if (data.length != 0) {
					makeGridReport(data);

				} else {
					makeGridReport();
				}
				
			});

		$('#cid').val(cId);
		viewPage('menuNavi');

	}
	

	function viewPage(page) {
		if (page == 'dateView') {
			$('#reportView').hide();
			$('#dateView').show();
		} else {
			$('#dateView').hide();
			$('#reportView').show();
		}
	}

	function xmlParser(xml_url, query_string, op, callback) {
		var rtdata = null;
		var xmlRequest = $.ajax({
			url : xml_url,
			data : query_string,
			type : "get",
			async : false,
			cache : false
		});
		var cid = null;
		if (query_string.indexOf("storeId")) {
			cid = query_string.substring(
					query_string.indexOf("storeId") + "storeId".length).split(
					"&")[0].replace(/=/g, "").trim();
		}
		var tmp = new Array();
		xmlRequest.done(function(res) {

			if (typeof res == 'string') {
				window.location.reload();
			}
			$(res).find("menu").each(function() {
				/*
				 * id : 본인아이디
				 * pid : 부모아이디
				 * name : 출력명(보고서 이름)
				 * url : 타이틀 클릭시 액션(URL 링크)
				 * icon : 타이틀 앞 아이콘(보고서 종류에따라 아이콘 변경)
				 * copyImg : 내폴더로 아이콘
				 * copyUrl : 내폴더로 아이콘 클릭시 액션
				 */
				var pid = $(this).find("pid").text();
				var uid = $(this).find("uid").text();
				var name = $(this).find("name").text();
				var link = $(this).find("link").text();
				var icon = $(this).find("icon").text();
				var type = $(this).find("type").text();

				tmp.push({
					P_ID : pid,
					C_ID : uid,
					C_NAME : name,
					NAME : name,
					PID : pid,
					SORT_ORDER : link,
					cIcon : icon,
					cType : type
				});

			});

			// end menu loading 
			$(".loader").hide();

			if (callback != null)
				callback(tmp, cid);
			else
				rtdata = {
					res : tmp,
					pid : cid
				};
			//(tmp, op);

		});
		return rtdata;

	}

	function renderTreeNode(treeid, element, pmenu) {

		var icon = "";
		var afterLabel;

		if (element.hasItems) {
			//have a child
			if (element.isExpanded != null && element.isExpanded) {
				icon = 'folderopenIcon.png';
			} else
				icon = 'folderIcon.png';
			afterLabel = "";//자식노드 숫자"<span style='color: Blue;' data-role=='treefoldersize'> (" + $(element.subtreeElement).children().length + ")</span>";
		} else {
			//no have a child
			icon = 'pageIcon.png';
			afterLabel = "";//'&nbsp;<img onclick="setPopup(true);" src="/resource/css/images/img_icons/popupIcon.png" alt="팝업창" title="새창보기">';
		}

		//topmenu ctype이 널이 아니거나 topmenu가 없을 경우..= 코그너스 메뉴이거나 코그너스 탐색기능을 이용한 경우.
		if ((pmenu != null && pmenu.cType != null) || pmenu == null) {
			var menudata = Enumerable.From(curLeftMenu).Where(function(c) {
				return c.C_ID == element.id;
			}).FirstOrDefault();
			if (menudata.cIcon.indexOf("folder") != -1)
				afterLabel = "";
			var iconurl = '/resource/img' + "/dTree/"
					+ ((menudata != null) ? menudata.cIcon : "page.gif");
			if (menudata.cIcon != "loading.gif")
				label = "<img class='menutypeicon' style='float: left; margin-right: 5px;' src='"+iconurl + "'/><span item-title='true' style='width:100px;text-overflow:ellipsis;'  title='"
						+ (element.originalTitle == null ? element.label
								: element.originalTitle)
						+ "'  >"
						+ (element.originalTitle == null ? element.label
								: element.originalTitle)
						+ "</span>"
						+ afterLabel;
			else
				label = "<span item-title='true'  >"
						+ (element.originalTitle == null ? element.label
								: element.originalTitle) + "</span>";
		} else {
			if (icon != "loading.gif")
				label = "<img class='menutypeicon' style='float: left; margin-right: 5px;' src='"+$iv.envinfo.css+"/images/img_icons/" + icon + "'/><span item-title='true'  >"
						+ (element.originalTitle == null ? element.label
								: element.originalTitle)
						+ "</span>"
						+ afterLabel;
			else
				label = "<span item-title='true'  >"
						+ (element.originalTitle == null ? element.label
								: element.originalTitle) + "</span>";
		}
		$("#" + treeid).jqxTree('updateItem', element, {
			label : label
		});
	}
	
</script>
<style type="text/css">
.splitter_intab .jqx-widget-content-blueish-tree {
	border: 0;
}

.splitter_intab .tree .jqx-widget-content-blueish-tree,#contenttreeMenuList {
	background: #ecf2f8 !important;
}

.jqx-tree-grid-icon, .jqx-tree-grid-icon-size {
	width: 16px;
	height: 16px;
}
.jqx-hideborder .jqx-cell{
	padding:3px 4px !important;
}
#treeMenuList .jqx-widget-header-blueish-tree { 
	display : none !important; 
}
#verticalScrollBartreeMenuList{
	left:282px !important;
}
</style>
</head>
<body class='blueish i-DQ'>
	<div class="wrap" style="width: 98%; min-width:1160px; margin: 0 10px; overflow:scroll;">
		<div class="container  f_left" style="width: 100%; margin: 10px 0;">
			<div class="tabs f_left"
				style="width: 100%; height: 750px; margin-top: 0px;">
				<div id='jqxTabs'>
					<ul>
						<li style="margin-left: 0px;" onclick="viewPage('reportView');">보고서기준</li>
						<li onclick="formDateGrid();">날짜기준</li>
					</ul>
					<div class="tabs_content"
						style="height: 100%; overflow-y: auto; overflow-x: hidden; padding: 0;">
						<div class="splitter splitter_intab  f_left"
							style="width: 100%; height: 718px; margin: 0;">
							<div id="splitter" style="border: 0;">
								<div style="border: none;" id="feedExpander">
									<div class="jqx-hideborder" style="height: 20px;">보고서</div>
									<div class="jqx-hideborder jqx-hidescrollbars">
										<div class="tree jqx-hideborder" style="border: 0 !important;"
											id='treeMenuList'></div>
									</div>
								</div>
								<div class="content f_left"
									style="width: 100%; margin: 0px 0; padding: 10px 0;">
									<div class="content f_left" id="dateView" style="width: 100%;">
										<div class="group f_left "
											style="width: 100%; margin-left: 17px; margin-bottom: 7px; margin-top: 10px;">
											
										</div>
										
										<div style="width: 100%; margin: 10px auto !important;">
											<div class="grid f_left"
												style="float: left; width: 100%; height: 630px; margin-bottom: 20px;">
												<div id="helpgrid"></div>
											</div>
										</div>
									</div>
									<div style="overflow-y: auto;">
										<!--splitter Right-->
										<input type="hidden" id="cid" /> <input type="hidden"
											id='solutionList' name="solutionList" value="IVISIONPLUS" />
										<div class="content f_left" style="width: 96%; margin: 0 2%;">
											<div class="group f_left m_t10" style="width: 100%; margin: 10px 0%;">
											<div class="iwidget_label  header"
											style="margin: 0 !important; height: 27px;">
												<div class="label type1 f_left" style="margin-left:10px"><span id="menuNavi">보고서명</span>&nbsp;</div>
												<div class="group_button f_right">
													<div class="button type2 f_left" style="margin: 0 10px 0 0;">
														<input type="button" value="Excel" id='reportButton'
															width="100%" height="100%" />
													</div>
												</div>
											</div>
											</div>
											<div class="f_left"
												style="width: 100%; margin: 10px 0%;">
												<div id="reportExcel"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tabs_content"
						style="height: 100%; overflow-y: auto; overflow-x: hidden; padding: 10px 30px;">
						<div class="content f_left" style="width: 100%; margin: 0;">
							<div class="group f_left  m_t10" style="width: 100%; margin: 10px 0%;">
								<div>
									<form id="frm" name="frm" method="get" class="padding_top"
										style="margin-bottom: 10px;">
										<input type="hidden" id="pgmId" name="pgmId" /> 
										<input type="hidden" id="ac" name="ac" />

										<div class="iwidget_label  header"
											style="margin: 0 !important; height: 27px;">
											<div style="float: left; width: 100%;">
												<input type="hidden" id="start_dat" name="start_dat"/>
												<input type="hidden" id="end_dat" name="end_dat"/>
												
												<div class="label type1 f_left" style="line-height:21px !important;">시작일
													:</div>
												<input type="text" id="startDat" name="startDat"
													 style="float: left;margin-left:5px;" readOnly/>
												
												<div class="label type1 f_left"
													style="line-height: 21px !important;">종료일
													:</div>
												<input type="text" id="endDat" name="endDat"
													 style="float: left;margin-left:5px;" readOnly/>
											
												<div class="group_button f_right">
													<div class="button type2 f_left" style="margin: 0 10px 0 0;" >
														<input type="button" value="조회" id='dateSearchButton' width="100%" height="100%" style="margin-right:10px !important;" /> 
														<input type="button" value="Excel" id='dateButton' width="100%" height="100%" />
													</div>
												</div>

											</div>
										</div>
									</form>
								</div>
							</div>
							<div class="treegrid f_left"
								style="width: 100%; height: 630px; margin: 0 0;">
								<div id="dateExcel"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--wrap-->
</body>
</html>