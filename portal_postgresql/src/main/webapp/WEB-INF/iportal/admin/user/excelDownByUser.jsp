 <%@ page language="java" contentType="application/vnd.xls;charset=UTF-8" pageEncoding="utf-8"%>   -
<%@page import="java.net.URLEncoder"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%

	String header =request.getHeader("User-Agent");
	String browser="";  
	String docName = URLEncoder.encode("사용자", "utf-8");  
	 
	System.out.println("header++++++++++++++++++++++++++++++"+header+":::::"+docName);
	 
	
    response.setHeader("Content-Disposition", "attachment; filename="+docName+".xls"); 
    response.setHeader("Content-Description", "JSP Generated Data");

%>   -
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>사용자</title>
	
	</head>
 	<body>
		<table width="100%;" id="tableList" class="fix_rable" cellspacing="0" cellpadding="0" border="1">		
			<thead>			
				<tr> 			
					<th style="background-color:#eff0f0;"  >ID</th>
					<th style="background-color:#eff0f0;" >이름</th>								
					<th style="background-color:#eff0f0;" >직책</th>
					<th style="background-color:#eff0f0;" >Email</th>
					<th style="background-color:#eff0f0;" >관리자</th>
					<th style="background-color:#eff0f0;" >언어</th>
				</tr>
			</thead>
		    <tbody>
			<c:choose> 		
					<c:when test="${mainList != null && not empty mainList}"> 	
						<c:forEach items="${mainList}" var="rows" varStatus="loop">
							<tr>		
								<td align="center">${rows.USER_ID}</td>
								<td>${rows.USER_NAME}</td>
								<td align="center">${rows.USER_POSITION}</td>
								<td>${rows.USER_EMAIL}</td>
								<td>${rows.USER_ADMIN}</td>
								<td align="center">${rows.LOCALE}</td>
							</tr>
						</c:forEach> 		
					</c:when> 		
					<c:otherwise> 				
						<tr> 					
							<td class="gtd"  colspan="6">조회된 데이터가 없습니다.</td> 			
						</tr> 		
					</c:otherwise> 
				</c:choose>  
			</tbody> 
		</table>
	</body>
</html>