<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>사용자 관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <script src="../../resources/js/util/remoteInfo.js"></script>
        <script>
	        var remoteIP = '<%=request.getRemoteAddr() %>';
	    	var remoteOS = getOSInfoStr();
	    	var remoteBrowser = getBrowserName();
        	var localValue=new Array();
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		makeCombobox();
        		$("#btnNew").jqxButton({width:'', theme:'blueish'});
        		$("#btnSearch").jqxButton({width:'', theme:'blueish'});
        		$("#txtSearch").jqxInput({placeHolder: "", height: 23, width: 180, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return search(); } });
        		$("#hiddenRemoteIP").val(remoteIP);
        		$("#hiddenRemoteOS").val(remoteOS);
        		$("#hiddenRemoteBW").val(remoteBrowser);
        	}
        	//조회 
        	function search(){
        		var userList = $i.post("/user/getAdminUserList",{searchName:$("#cboText").val(), searchValue:$("#txtSearch").val()});
        		userList.done(function(userData){
        			localValue = userData.returnArray;
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'UNID', type: 'string' },
		                    { name: 'ACCT_ID', type: 'string' },
							{ name: 'DIVISION', type: 'string'},
							{ name: 'USER_ID', type: 'string'},
							{ name: 'USER_NAME', type: 'string'},
							{ name: 'USER_POSITION', type: 'string'},
							{ name: 'USER_LASTLOGIN', type: 'string'},
							{ name: 'USER_STATUS', type: 'string'},
							{ name: 'AUTHORITY', type: 'string'},
							{ name: 'FIRST_LOGIN_DATE', type:'string'},
							{ name: 'ADMIN_YN', type:'string'},
							{ name: 'LOCALE', type:'string'},
							{ name: 'EMP_STATUS', type:'string'},
							{ name: 'USER_REGDATE', type:'string'},
							{ name: 'USER_EMAIL', type:'string'}
		                ],
		                localdata: userData,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
        			var noScript = function (row, columnfield, value) {//left정렬
        				var newValue = $i.secure.scriptToText(value);
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
					}; 
        			var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var dateRender = function (row, columnfield, value) {//left정렬
						var newValue = value.slice(0,10);
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
					}; 
					var openButtonCellsrenderer = function(row,datafield,value, defaultHtml, propery, rowdata){
						var html = "";
						var status = rowdata.EMP_STATUS;
						var empId = rowdata.USER_ID;
						var unid = rowdata.UNID;
						if(status == "OPEN"){
							html = "정상";
						}else{
							html ="<a href=\"javaScript:lockButton('"+empId+"','"+unid+"');\" style='color:#000000;'><img src='../../resources/img/button/button_not_red.png'/></a>";
						}
						return "<div style='text-align:center; margin:4px 0px 0px 0px;'>" + html + "</div>";
					};
					var adminTextCellsenderer = function(row, datafield, value, defautlHtml, prorpery, rowdata){
						var html = "";
						if(value == "Y"){
							html = "예";
						}else {
							html = "아니오";
						}
						return "<div style='text-align:center; margin:4px 0px 0px 0px;'>" + html + "</div>";
					};
					var detailRow = function (row, columnfield, value, defaultHtml, property, rowdata) {//그리드 선택시 하단 상세
						var userID = rowdata.USER_ID;
						var unID = rowdata.UNID;
						var link = "<a href=\"javaScript:viewDetail('"+unID+"');\" style='color: #000000;text-decoration:underline;'>" + value.replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/'"'/g,'&quot;') + "</a>";
// 						var link = "<a href=\"javascript:getRowDetail('"+ifCod+"','"+ifCodNm+"','"+acctId+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:center; margin:4px 0px 0px 0px;'>" + link + "</div>";
						
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#grdUserList").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
		                source: dataAdapter,
		                columnsheight:28,
						theme:'blueish',
		                columnsresize: true,
		                pageable: true,
		                pagesize: 100,
						pagesizeoptions:['100', '200', '300'],
		                columns: [
		                   { text: 'No', datafield: 'UNID', width: '5%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: 'ID', datafield: 'USER_ID', width:  '15%', align:'center', cellsalign: 'center', cellsrenderer: noScript},
		                   { text: '이름', datafield: 'USER_NAME', width: '20%', align:'center', cellsalign: 'center', cellsrenderer: detailRow},
		                   { text: '직위', datafield: 'USER_POSITION', width: '15%', align:'center', cellsalign: 'center', cellsrenderer: noScript},
		                   { text: '사내 E-mail', datafield: 'USER_EMAIL', width: '15%', align:'center', cellsalign: 'center', cellsrenderer: noScript},
		                   { text: '상태', datafield: 'EMP_STATUS', width: '5%', align:'center', cellsalign: 'center', cellsrenderer: openButtonCellsrenderer},
		                   { text: '관리자', datafield: 'ADMIN_YN', width: '10%', align:'center', cellsalign: 'center', cellsrenderer: adminTextCellsenderer},
		                   { text: '가입일자', datafield: 'USER_REGDATE', width: '15%', align:'center', cellsalign: 'center', cellsrenderer: dateRender}
		                ]
		            });
        		});
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function viewDetail(unID){
        		var userID = "";
        		for(var i=0;i<localValue.length;i++){
        			if(localValue[i].UNID==unID){
        				userID = localValue[i].USER_ID;
        				break;
        			}
        		}
        		//location.replace("./detail?userID="+userID+"&remoteIP="+remoteIP+"&remoteOS="+remoteOS+"&remoteBrowser="+remoteBrowser);
        		location.replace("/user2/detail?userID="+userID+"&remoteIP="+remoteIP+"&remoteOS="+remoteOS+"&remoteBrowser="+remoteBrowser);
        	} 
        	function newPage(){
        		//location.replace("./detail");
        		location.replace("/user2/detail");
        	}
        	function makeCombobox(){
        		textCombobox();
        		search();
        	}
        	function textCombobox(){
        		var textData = [
						{ value:'id', name:'ID'},
						{ value:'name', name:'이름'},
						{ value:'email', name:'E-MAIL'}
        		];
        		var source = 
       			{
       				datatype:"json",
       				datafields:[
       					{ name : "value"},
       					{ name : "name"}
       				],
       				id:"id",
       				localdata:textData,
       				async : false
       			};
       			var dataAdapter = new $.jqx.dataAdapter(source);
       			$("#cboText").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "name", valueMember: "value", dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,  theme:'blueish'});
        	}
        	function changeYearCombo(){
        		var data = $i.service("listMmCombo",["${param.pEvaGbn}", $("#cboSearchYear").val(), "A"]);
           		data.done(function(c){ 
           			if(c.returnCode=="SUCCESS"){
           				$("#cboSearchMm").jqxComboBox("clear");
           				for(var i=0;i<c.returnArray.length;i++){
           					$("#cboSearchMm").jqxComboBox("insertAt", {label:c.returnArray[i].MM_NM, value:c.returnArray[i].MM_ID},i);
           				}
           				$("#cboSearchMm").jqxComboBox("selectIndex", 0);
           			}else{
           				$("#cboSearchMm").jqxComboBox("clear");
           				$("#cboSearchMm").jqxComboBox("insertAt", {label:"==선택==",value:""},0);
           			} 
           		});
        	}
        	function changeMmCombo(){
        		var data = $i.service("listKpiCombo",["${param.pEvaGbn}", $("#cboSearchYear").val()+$("#cboSearchMm").val()]);
        		data.done(function(c){
        			if(c.returnCode == "SUCCESS"){
        				$("#cboSearchKpiID").jqxComboBox("clear");
        				for(var i=0;i<c.returnArray.length;i++){
        					$("#cboSearchKpiID").jqxComboBox("insertAt",{label:c.returnArray[i].MT_NM, value:c.returnArray[i].MT_ID},i);
        				}
        				$("#cboSearchKpiID").jqxComboBox("selectIndex", 0);
        			}else{
        				$("#cboSearchKpiID").jqxComboBox("clear");
        				$("#cboSearchKpiID").jqxComboBox("insertAt", {label:"==선택==",value:""},0);
        			}
        		});
        	}
        	function changeKpiCombo(){
        		var dataScid = $i.service("listScidCombo",["${param.pEvaGbn}", "", $("#cboSearchYear").val()+$("#cboSearchMm").val(), $("#cboSearchKpiID").val()]);
           		dataScid.done(function(data){
           			if(data.returnCode == "SUCCESS"){
           				$("#cboSearchScID").jqxComboBox("clear");
           				for(var i=0;i<data.returnArray.length;i++){
           					$("#cboSearchScID").jqxComboBox("insertAt", {label:data.returnArray[i].SC_NM, value:data.returnArray[i].SC_ID},i);
           				}
           				$("#cboSearchScID").jqxComboBox("selectIndex", 0);
           			}else{
           				$("#cboSearchScID").jqxComboBox("clear");
           				$("#cboSearchScID").jqxComboBox("insertAt", {label:"==선택==", value:""},0);
           			}
           		});
        	}
        	function lockButton(empId, unid){//updateLock
				var remoteIp = '<%=request.getRemoteAddr() %>';
				var data = $i.post("/user/lockOpen", {empId:empId,unid:unid,gubn:"7",remoteIp:remoteIp,remoteOs:getOSInfoStr(),remoteBrowser:getBrowserName()});
				data.done(function(d1){
					if(d1.returnCode=='SUCCESS'){
						$i.dialog.alert('SYSTEM',"정상처리 되었습니다.",function(){
							search();
						});
					}else{
						$i.dialog.error('SYSTEM','오류가 발생했습니다');
					}
				});
			};
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
		</style>
		   
    </head>
    <body class='blueish'>
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%; min-width:1160px; margin:0 10px;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<form id="searchForm" name="searchForm" action="/user/list">
					<input type="hidden" id="hiddenRemoteIP" name="remoteIP" />
					<input type="hidden" id="hiddenRemoteOS" name="remoteOS" />
					<input type="hidden" id="hiddenRemoteBW" name="remoteBrowser" />
					<div class="label type1 f_left">&nbsp;</div>
					<div class="combobox f_left" id="cboText" name="cboText" style="margin-left:-4px;"></div>
					<input type="text" id="txtSearch" name="searchscnm" />
					<div class="group_button f_right">
						<div class="button type1 f_left" style="margin-right:10px !important;">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
						<div class="button type1 f_left" style="margin-right:10px !important;">
							<input type="button" value="신규" id='btnNew' width="100%" height="100%" onclick="newPage();" />
						</div>
					</div>
				</form>
			</div>
			
			<div class="container  f_left" style="width:100%; margin-top:10px !important;">
				<div class="content f_left" style=" width:100%;">
					<div class="grid f_left" style="width:100%; height:625px;">
						<div id="grdUserList"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>