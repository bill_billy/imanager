<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>사용자관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/js/util/remoteInfo.js"></script>
        <script src="../../resources/js/jquery/jquery.validate.js"></script>
        <link rel="stylesheet" href="../../resource/css/iplanbiz/table.css" type="text/css"/>
        <script>
        	var paramUserID = "${param.userID}";
        	var remoteIP = '<%=request.getRemoteAddr() %>';
        	var remoteOS = getOSInfoStr();
        	var remoteBrowser = getBrowserName();
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){   
        		$i.dialog.setAjaxEventPopup("SYSTEM","데이터 처리중..");
        		$("#btnList").jqxButton({width:'', theme:'blueish'});
        		$("#btnInsert").jqxButton({width:'', theme:'blueish'});
        		$("#btnBack").jqxButton({width:'', theme:'blueish'});
        		$("#btnRemove").jqxButton({width:'', theme:'blueish'});  
        		$("#btnReset").jqxButton({width:'', theme:'blueish'});
        		$("#cboAdminYn").jqxDropDownList({animationType: 'fade',dropDownHorizontalAlignment: 'right',dropDownWidth: 150,width: 150,height: 23,theme:'blueish'});
        		$("#cboLocale").jqxDropDownList({animationType: 'fade',dropDownHorizontalAlignment: 'right',dropDownWidth: 150,width: 150,height: 23,theme:'blueish'});
        		if(paramUserID == ""){
        			$('#saveForm').jqxValidator({
						rtl:true,  
						rules:[
						    { input: "#txtUserID", message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
		                    { input: '#txtUserName', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
		                    { input: '#txtUserEmail', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required'},
		                    { input: '#txtUserEmail', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'email'}
				        ]
					});
        		}else{
	        		$('#saveForm').jqxValidator({
						rtl:true,  
						rules:[
		                    { input: '#txtUserName', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
		                    { input: '#txtUserEmail', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required'},
		                    { input: '#txtUserEmail', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'email'}
				        ]
					});
        		}
        		init(); 
        	});
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		if(paramUserID == ""){
        			$("#userIDData").remove();
        			$("#noUserIDData").show();
        			$("#btnBack").hide();
	        		$("#btnRemove").hide();  
	        		$("#btnReset").hide();
	        		$("#btnBack,#btnRemove,#btnReset").parent().attr('style','margin-left:0px !important');
        		}else {
        			$("#userIDData").show();
        			$("#noUserIDData").remove();
        		}
        		makeListBox();
        		$("#hiddenRemoteIP").val(remoteIP);
        		$("#hiddenRemoteOS").val(remoteOS);
        		$("#hiddenRemoteBW").val(remoteBrowser);
        	}
        	//조회
        	function search(){
        		
        	}
        	function checkStringFormat(string) { 
	       	     //var stringRegx=/^[0-9a-zA-Z가-힝]*$/; 
	       	     var stringRegx = /[~!@\#$%<>^&*\-=+\’\'\"]/gi; 
	       	     var isValid = true; 
	       	     if(stringRegx.test(string)) { 
	       	       isValid = false; 
	       	     } 
	       	        
	       	     return isValid; 
	       }
        	//입력
        	function insert(){
        		if($("#saveForm").jqxValidator("validate") == true){
        			if(!checkStringFormat($("#txtUserID").val())){
        				$i.dialog.error('SYSTEM', 'ID에 특수문자를 입력할 수 없습니다!');
        				return false;
        			}
        			var txtID = $("#txtUserID").val();
        			if(txtID){//신규
	        			if(txtID.length>20){
	        				$i.dialog.error('SYSTEM', 'ID를 20자이내로 입력하세요.');
	        				return false;
	        			}
        			}
        			var position = $("#txtUserPosition").val();
        			if(position.length>20){
        				$i.dialog.error('SYSTEM', '직책을 20자이내로 입력하세요.');
        				return false;
        			}
        			
	        	//	$("#hiddenRemoteIP").val(remoteIP);
	        	//	$("#hiddenRemoteOS").val(remoteOS);
	        	//	$("#hiddenRemoteBW").val(remoteBrowser);
	        		var checkCount = $("#listBoxGroup").jqxListBox("getCheckedItems").length;
	        		var checkVal = "";
	        		for(var i=0;i<checkCount;i++){
	        			if(i == checkCount -1){
	        				checkVal += $("#listBoxGroup").jqxListBox("getCheckedItems")[i].value;
	        			}else{
	        				checkVal += $("#listBoxGroup").jqxListBox("getCheckedItems")[i].value+",";
	        			}
	        		}
	        		$("#hiddenCheckValue").val(checkVal);
	        		$i.insert("#saveForm").done(function(args){
	        			if(args.returnCode == "EXCEPTION"){
	        				$i.dialog.error("SYSTEM",args.returnMessage);
	        				console.log(args);
	        			}else{
	                		$i.dialog.alert("SYSTEM","저장 되었습니다.",function(){
		        				goList();
		        			});
	        			}
	        		}).fail(function(e){ 
	        			$i.dialog.error("SYSTEM",args.returnMessage);
	        		});
        		}
        	}
        	//삭제
        	function remove(){
        		$i.dialog.confirm("SYSTEM","삭제 하시겠습니까?",function(){
        			$i.remove("#saveForm").done(function(args){
	        			if(args.returnCode == "EXCEPTION"){
	        				$i.dialog.error("SYSTEM",args.returnMessage);
	        			}else{
	        				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
	        					goList();
	        				});
	        				
	        			}
	        		}).fail(function(e){ 
	        			$i.dialog.error("SYSTEM",args.returnMessage);
	        		});	
        		});
        		/*if(confirm("삭제 하시겠습니까?")){
        			$i.remove("#saveForm").done(function(args){
	        			if(args.returnCode == "EXCEPTION"){
	        				$i.dialog.error("SYSTEM",args.returnMessage);
	        			}else{
	        				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
	        					goList();
	        				});
	        				
	        			}
	        		}).fail(function(e){ 
	        			$i.dialog.error("SYSTEM",args.returnMessage);
	        		});	
        		}*/
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        		
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function checkUserID(){
        		if(!checkStringFormat($("#txtUserID").val())){
    				$i.dialog.error('SYSTEM', 'ID에 특수문자를 입력할 수 없습니다!');
    				$("#txtUserID").focus();
					$("#txtUserID").val("");
    				return false;
    			}
        		var txtID = $("#txtUserID").val();
    			if(txtID.length>20){
    				$i.dialog.error('SYSTEM', 'ID를 20자이내로 입력하세요.');
    				return false;
    			}
        		var userCheck = $i.post("/user/getCheckUserID", {userID:$("#txtUserID").val()})
    			userCheck.done(function(data){
    				if(data.returnCode == "EXCEPTION" ){
    					$i.dialog.error("SYSTEM", data.returnMessage);
    					$("#txtUserID").focus();
    					$("#txtUserID").val("");
    					return 
    				}else{
    					$i.dialog.alert("SYSTEM", data.returnMessage);
    				}
    			});
        	}
        	function resetPassword(){
        		if(confirm("비밀번호를 초기화 하시겠습니까?")){
        			var resetData = $i.post("/user/resetPasswordUser",{userID:$("#hiddenUserID").val()});
	        		resetData.done(function(reset){
	        			if(reset.returnCode == "EXCEPTION"){
	        				$i.dialog.error("SYSTEM", reset.returnMessage);
	        			}else{
	        				$i.dialog.alert("SYSTEM", reset.returnMessage,function(){
	        					changePage();
	        				});
	        			}
	        		});
        		}
        	}
        	function excelDown(){
        		windowOpen('/user/excelDownByUser?userID='+$("#hiddenUserID").val(), "_self");
        	}
        	function makeListBox(){
        		var roleData =  $i.post("/user/getRoleList", {});
        		roleData.done(function(role){
        			var source = {
							datatype:'json',
							datafields:[
									{name : 'GID'},
									{name : 'GROUP_NAME'}
							],
							id:'id',   
							localdata:role,
			                async: false
					};
					var dataAdapter = new $.jqx.dataAdapter(source);
				    // Create a jqxListBox
				    $("#listBoxGroup").jqxListBox({width: 250, source: dataAdapter, displayMember: "GROUP_NAME", valueMember: "GID",checkboxes: true,filterable: true, filterPlaceHolder: "검색",height: 150, theme:'blueish'});
				    var checkUserData = $i.post("/user/getRoleCheckUser", {userID:paramUserID});
				    checkUserData.done(function(user){
				    	if(user.returnCode == "EXCEPTION"){
				    		$i.dialog.alert("SYSTEM", user.returnMessage);
				    	}else{
				    		if(user.returnArray.length > 0){
				    			for(var i=0;i<user.returnArray.length;i++){
				    				$("#listBoxGroup").jqxListBox("checkItem", user.returnArray[i].GID);
				    			}
				    		}
				    	}
				    });
        		});
        	}
        	function changePage(){
        		location.replace("/user2/detail?userID="+$("#hiddenUserID").val());
        	}
        	function goList(){
        		location.replace("/user2/list");
        	}
        	function windowOpen(url, windowName, width, height, location, scrollbars){
				var screenWidth=screen.width;
				var screenHeight=screen.height;
			
				var x=(screenWidth/2)-(width/2);
				var y=(screenHeight/2)-(height/2);
				//2013-12-18장민수 수정
				//새창 객체를 리턴하도록.
				return window.open(url, windowName,"width=" + width + ", height=" + height + ",  location=" + location + ", toolbar=no, menubar=no, directories=no, scrollbars=" + scrollbars + ", resizable=no, left="+ x + ", top=" + y);
			}
        </script>
        <style  type="text/css">
        	.ui-datepicker-trigger{    
        		float:left !important;
        		margin-top:5px !important;
        	}
        	.jqx-validator-hint{
        		background-color : transparent !important;
        		border:0px !important;
        	}
        	.jqx-validator-hint-arrow{
        		display:none !important;
        	}
        </style>
    </head>
    <body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 10px;">
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:100%;">
					<div class="group f_left  w100p m_b5">
						<div class="group_button f_right">	
							<div class="button type2 f_left" style="margin-bottom:0;">
								<input type="button" value="목록" id='btnList' width="100%" height="100%" onclick="goList();"/>
							</div>
							<div class="button type2 f_left" style="margin-bottom:0;">
								<input type="button" value="저장" id='btnInsert' width="100%" height="100%" onclick="insert();" />
							</div>
							<div class="button type3 f_left" style="margin-bottom:0;">
								<input type="button" value="삭제" id='btnRemove' width="100%" height="100%" onclick="remove();"/>
							</div>
							<div class="button type2 f_left" style="margin-bottom:0;">
								<input type="button" value="백업" id='btnBack' width="100%" height="100%" onclick="excelDown();" />
							</div>
							<div class="button type2 f_left" style="margin-bottom:0;">
								<input type="button" value="초기화" id='btnReset' width="100%" height="100%" onclick="resetPassword();" />
							</div>
						</div>
					</div>
					<div class="table  f_left" style="width:100%; margin-top:5px;">
						<form id="saveForm" name="saveForm">
							<input type="hidden" id="hiddenRemoteIP" name="remoteIP" />
							<input type="hidden" id="hiddenRemoteOS" name="remoteOS" />
							<input type="hidden" id="hiddenRemoteBW" name="remoteBrowser" />
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<tbody>
									<tr id="userIDData">
										<th class="w15p"><div>ID<span class="th_must"></span></div></th>
										<td>
											<div class="cell">
												<input type="hidden" id="hiddenUnid" name="unID" value="${userInfo.UNID}" />
												<input type="hidden" id="hiddenUserID" name="userID" value="${userInfo.USER_ID}" />
												${userInfo.USER_ID}
											</div>
										</td>
									</tr>
									<tr id="noUserIDData">
										<th class="w15p"><div>ID<span class="th_must"></span></div></th>
										<td>
											<div class="cell">
												<input type="hidden" id="hiddenUnid" name="unID" value="${userInfo.UNID}" />
												<input type="text" value="${userInfo.USER_ID}" id="txtUserID" name="userID" class="input type2  f_left"  style="width:99%; margin:3px 0; " onChange="checkUserID();"/>
											</div>
										</td>
									</tr>
									<tr>
										<th><div>이름<span class="th_must"></span></div></th>
										<td>
											<div class="cell">
												<input type="text" value="${userInfo.USER_NAME}" id="txtUserName" name="userName" class="input type2 f_left" style="width:99%; margin:3px 0;" />
											</div>
										</td>
									</tr>
									<tr>
										<th><div>직책</div></th>
										<td>
											<div class="cell">
												<input type="text" value="${userInfo.USER_POSITION }" id="txtUserPosition" name="userPosition" class="input type2 f_left" style="width:99%; margin:3px 0;" />
											</div>
										</td>
									</tr>
									<tr>
										<th><div>사내Email<span class="th_must"></span></div></th>
										<td>
											<div class="cell">
												<input type="text" value="${userInfo.USER_EMAIL}" id="txtUserEmail" name="userEmail" class="input type2 f_left" style="width:99%; margin:3px 0;" />
											</div>
										</td>
									</tr> 
									<tr>
										<th><div>관리자</div></th>
										<td>
											<div class="cell">
												<div class="combobox">
													<select id="cboAdminYn" name="adminYn">
														<c:choose>
															<c:when test="${useYnCodeList != null && not empty useYnCodeList}">
																<c:forEach items="${useYnCodeList}" var ="code" varStatus="loop">
																	<option value="${code.COM_COD}" <c:if test="${code.COM_COD == userInfo.ADMIN_YN}"> selected </c:if>>${code.COM_NM}</option>
																</c:forEach>
															</c:when>
														</c:choose>
													</select>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<th><div>언어</div></th>
										<td>
											<div class="cell">
												<div class="combobox">
													<select id="cboLocale" name="locale">
														<c:choose>
															<c:when test="${localeCodeList != null && not empty localeCodeList}">
																<c:forEach items="${localeCodeList}" var ="code" varStatus="loop">
																	<option value="${code.COM_COD}" <c:if test="${code.COM_COD == userInfo.LOCALE}"> selected </c:if>>${code.COM_NM}</option>
																</c:forEach>
															</c:when>
														</c:choose>
													</select>
												</div>
											</div>
										</td>
									</tr> 
									<tr>
										<th><div>Role</div></th>
										<td>
											<div class="cell">
												<input type="hidden" id="hiddenCheckValue" name="checkValue" />
												<div id="listBoxGroup" name="group"></div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</form>
					</div>
					<div class="label-4" style="float:left;"><p class="iwidget_grid_tip">신규 생성 시 입력된 이메일로 임시 비밀번호가 전송됩니다.</p></div>
				</div>
			</div>
		</div>
	</body>
</html>