<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>메뉴얼관리</title>
	    
	    <link rel="stylesheet" href="../../resources/css/jqwidget3.5.0/styles/jqx.base.css" type="text/css" />
		<link rel="stylesheet" href="../../resources/css/jqwidget3.5.0/styles/iplanbiz.basic.css" type="text/css"/>
		<link rel="stylesheet" href="../../resources/css/jqwidget3.5.0/styles/jqx.SophisticatedLayout.css" type="text/css"/>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/thirdparty/LGPL/v2/smartEditor2/js/HuskyEZCreator.js" charset="UTF-8"></script>  
		<script type="text/javascript">
		var dummycount = 1;
		var curLeftMenu=null;
		var ajaxSignal = ".."; 
		var datafields = [
			{name:'C_ID', type: 'string'},
			{name:'PID', type: 'string'},
			{name:'NAME', type: 'string'},
			{name:'P_NAME', type: 'string'}, 
			{name:'PROG_ID', type: 'string'},
			{name:'SOURCE_OWNER', type: 'string'}
		];
		
		var oEditors = [];
		function initSmartEditor(id) {
			nhn.husky.EZCreator.createInIFrame({
				oAppRef : oEditors,
				elPlaceHolder : id,
				sSkinURI : "../../resources/cmresource/js/thirdparty/LGPL/v2/smartEditor2/SmartEditor2Skin.html",
				htParams : {
					bUseToolbar : true,
					fOnBeforeUnload : function() {
					}
				},
				fOnAppLoad : function() {
					$("textarea").next("iframe").attr('style','width:100% !important; height:560px !important');
					oEditors.getById['content'].setDefaultFont("돋움", 12);	
					//oEditors.getById["ir1"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
				},
				fCreator : "createSEditor2"
			});
		}    
		
		$(document).ready(function() {
			$('#menuName').jqxInput({placeHolder: "", height: 25, width: 180, minLength: 1}).on('keydown', function() { if(event.keyCode==13) { searchStart(); return false;}});
			$('#searchButton').jqxButton({ width: '',  theme:'SophisticatedLayout'}).on('click', function() { searchStart(); });
			$('#saveButton').jqxButton({ width: '',  theme:'SophisticatedLayout'}).on('click', function() { saveStart(); });
			$('#deleteButton').jqxButton({ width: '',  theme:'SophisticatedLayout'}).on('click', function() { deleteStart(); });
			$('#listButton').jqxButton({ width: '',  theme:'SophisticatedLayout'}).on('click', function() { viewPage('gridView'); });
			
			makeSearchTypeCombobox(); //검색타입 : id, name
			makeSolutionListCombobox(); //솔루션 리스트
			
			
			//splitter
			$('#splitter').jqxSplitter({ width:'', height: 700, panels: [{ min: 150 , size: 220 }, { min: 150}] });		
		//	initSmartEditor("content");
		});
		
		 function makeSolutionListCombobox() {
			$i.post("../../admin/menuContentManager/getSolutionListCbo").done(function(res){
							var pSource =
							{
								localdata: res.returnArray,
					    		dataType: 'json',
								dataFields: [
									{ name: 'com_id'},
									{ name: 'com_nm'}
								] 
							};
							  
							var pAdapter = new $.jqx.dataAdapter(pSource);			
							$("#solutionList").jqxComboBox({ selectedIndex: 0, source: pAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "com_nm", valueMember: "com_id", dropDownWidth: 120, dropDownHeight: 150, width: 120, height: 22});
							$('#solutionList').jqxComboBox({ selectedIndex: 0 }); 
							$('#solutionList').bind('change', function() {  
								getMenuList($(this).val());    
							});   
							getMenuList();
	       	});
		} 
		
	 	 function makeSearchTypeCombobox() {
			var res = [
			    {COM_ID:'C_ID', COM_NM:'메뉴명'},       
				{COM_ID:'P_ID', COM_NM:'상위메뉴명'}
			];
			
			makeCombobox($('#searchType'), res, 100);  
		}
		function makeCombobox(comboElement, res, width) {
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: 'COM_ID'},
					{ name: 'COM_NM'}
				],
				id : 'COM_ID',
				localdata: res
			};
			
			var dataAdapter = new $.jqx.dataAdapter(source);	
			comboElement.jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_ID", dropDownWidth: width, width: width, height: 25});	
		}  
		function getMenuList() {
				makeTree();
				gridContentList();
		}
		function makeTree() {
			$("#treeMenuContent").jqxTree("clear");     
			$i.post("../../admin/menuContentManager/getMenuContentList", {menualId:$("#solutionList").val(),searchType:$("#searchType").val(), searchValue:$("#menuName").val()}).done(function(res){   
					var source =
						{
			                datatype: "json",
			                datafields: [
			                    { name: 'c_id'},
			                    { name: 'p_id'},
			                    { name: 'prog_nm'}
			                ],   
			                id : 'c_id',      
			                localdata: res
			            };
			            var dataAdapter = new $.jqx.dataAdapter(source);
			            dataAdapter.dataBind();
			            
			            var records = dataAdapter.getRecordsHierarchy('c_id', 'p_id', 'items', [{ name: 'c_id', map: 'id'} ,{ name: 'prog_nm', map: 'label'},{ name: 'p_id', map: 'value'}]);   
			            $('#treeMenuContent').jqxTree({source: records, height: '100%', width: '100%', theme:'blueish' });             
			               
						// tree init
			            var items = $('#treeMenuContent').jqxTree('getItems');
			            var item = null;
			            var size = 0;
			            var img = '';
			            var label = '';
			            var afterLabel = '';
			            
						for(var i = 0; i < items.length; i++) {
							item = items[i];
							size = $(item.element).find('li').size();
							
							if(i == 0) {
								//root
								$("#treeMenuContent").jqxTree('expandItem', item);
								img = 'icon-foldernoopen.png';
								if(size > 0){
									afterLabel = "<span style='color: Blue;'> (" + size + ")</span>";
								}else{
									afterLabel = "";
								}
							} else {
								//children
								if(size > 0) {
									//have a child
									img = 'icon-foldernoopen.png';
									afterLabel = "<span style='color: Blue;'> (" + size + ")</span>";
								} else {
									//no have a child
									img = 'icon-page.png';
									//내부에서 사용하는 트리는 팝업 없음.
									afterLabel = "";
								}
							}
							
							label = "<img style='float: left; margin-right: 5px;' src='../../resources/cmresource/image/" + img + "'/><span item-title='truec style='vertical-align:middle;'>" + item.label + "</span>" + afterLabel;
							$('#treeMenuContent').jqxTree('updateItem', item, { label: label});
						}
			            //add event
			            $('#treeMenuContent')     
			            .on("expand", function(eve) {
			            	var args = eve.args;    
			            	var label = $('#treeMenuContent').jqxTree('getItem', args.element).label.replace('icon-foldernoopen', 'icon-folderopen');
			            	args.element.firstChild.className = args.element.firstChild.className.replace('jqx-tree-item-arrow-collapse', '').replace('jqx-icon-arrow-right', '');
			            	$('#treeMenuContent').jqxTree('updateItem', args.element, { label: label});
			            })
			            .on("collapse", function(eve) {
			            	var args = eve.args;
			            	var label = $('#treeMenuContent').jqxTree('getItem', args.element).label.replace('icon-folderopen', 'icon-foldernoopen');
			            	$('#treeMenuContent').jqxTree('updateItem', args.element, { label: label});         
			            })       
			            .on("select", function(eve) {   
			            	viewDetail($('#treeMenuContent').jqxTree('getItem', args.element).id); 
			            });
			            $("#treeMenuContent").jqxTree("expandAll");
			     });         
		}
			function gridContentList(){   
			$i.post("../../admin/menuContentManager/getMenuContenGridtList", {menualId:$("#solutionList").val(),searchType:$("#searchType").val(), searchValue:$("#menuName").val()}).done(function(data){
	            var source = 
	            {    
	                datatype: "json",
	                datafields: [
	                    { name: 'menual_id', type: 'string'},  
	                    { name: 'c_id', type: 'string'},
	                    { name: 'locale_cd', type: 'string'},  
	                    { name: 'p_prog_nm', type: 'string'},
	                    { name: 'prog_nm', type: 'string'},
	                    { name: 'p_id', type: 'string'}
	                ],
	                localdata: data,
	                updaterow: function (rowid, rowdata, commit) {
	                    commit(true);
	                }
	            };
	
				var alginLeft = function (row, columnfield, value) {//left정렬
	                       return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
								
	                  } 
				var alginRight = function (row, columnfield, value) {//right정렬
	                       return '<div id="userName-' + row + '"style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
								
	                  }	  
				var alginCenter = function (row, columnfield, value) {//right정렬
                    return '<div id="userName-' + row + '"style="text-align: center; margin:4px 0px 0px 0px;">' + value + '</div>';
							
               }
	            var dataAdapter = new $.jqx.dataAdapter(source);
	
	            var detail = function(row, columnfield, value, defaultHtml, columnproperties, rowdata){
	            		var cid = rowdata.c_id;
	            	     
						var link = "<a href=\"javascript:viewDetail('"+cid+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";            
				};
	            $("#gridMenuContent").jqxGrid(
	            {
	               width: '100%',
					height: '100%',
					altrows:true,
					pageable: true,
					source: dataAdapter,
					theme:'grid_SophisticatedLayout',
					columnsheight:28,
					pagesize: 100,
					pagesizeoptions:['100', '200', '300'],
					rowsheight: 28,
					columnsresize: true,
	                columns: [   
	                  { text: '상위메뉴ID', datafield: 'p_id', width: '25%', align:'center', cellsalign: 'center',cellsrenderer:alginCenter},   
	                  { text: '상위메뉴명', datafield: 'p_prog_nm', width: '25%', align:'center', cellsalign: 'center',cellsrenderer:alginCenter},    
	                  { text: '메뉴ID', datafield: 'c_id', width: '25%', align:'center', cellsalign: 'center',cellsrenderer:alginCenter},      
	                  { text: '메뉴명', datafield: 'prog_nm', width: '25%', align:'center', cellsalign: 'left', cellsrenderer: detail  }
	                ] 
	            });      
	            setPagerLayout("gridMenuContent");
	        });
		}
		
		function viewDetail(cid) {
			viewPage('tableView');	
			$('#content').val('');    
			$('#cid').val('');
			if($("iframe").length < 1){
				initSmartEditor("content");
			}
			$i.post("../../admin/menuContentManager/getMenuContentInfo",{cid:cid}).done(function(res) {
				res = res.returnArray;
				//set
				$('#cid').val(cid);
				if(res.length > 0) {
					$("#content").val(res[0].content_text);
					$('#progName').html(res[0].prog_nm);   
					$('#progNm').val(res[0].prog_nm);   
					$('#progId').val(res[0].prog_id);
					$('#localCd').val(res[0].locale_cd);
					$('#parentId').val(res[0].parent_id);
					if(res[0].content_text==null){
						oEditors.getById["content"].exec("SET_IR", [""]); //내용초기화 
					}else{
						oEditors.getById["content"].exec("SET_IR", [res[0].content_text]); //내용초기화 
					}
				}else{
					oEditors.getById["content"].exec("SET_IR", [""]); //내용초기화 
				}
				oEditors.getById["content"].exec("CHANGE_EDITING_MODE", ["WYSIWYG"]); //포커스얻기
				oEditors.getById["content"].exec("RESET_TOOLBAR"); 
			//	viewPage('tableView');		
			});
		}
		function searchStart() {
				gridContentList();
		}
		function saveStart() {       
			oEditors.getById["content"].exec("UPDATE_CONTENTS_FIELD", []);   
			var action = $i.post('./insert', '#saveForm');
			action.done(function(res) {
				if(res.returnCode == 'EXCEPTION') {
					$i.dialog.error('SYSTEM', res.returnMessage);
					return;
				}else{
					$i.dialog.alert('SYSTEM', '저장 되었습니다.');
				}
			})
			.fail(function(error){ 
				$i.dialog.error('SYSTEM', error.returnMessage);
			});
		}
		 function deleteStart() {
			$i.dialog.confirm("SYSTEM", "삭제하시겠습니까?", function(){
				var action2 = $i.post('./delete', '#saveForm');
				action2.done(function(res) {
					if(res.returnCode == 'EXCEPTION') {
						$i.dialog.error('SYSTEM', res.returnMessage);
						return;   
					}else{
						$i.dialog.alert("SYSTEM","삭제 되었습니다.",function(){
							viewPage('gridView');
						});	
					}	
				})
				.fail(function(error){ 
					$i.dialog.error('SYSTEM', error.returnMessage);
				});
			});
		} 
		function viewPage(page) {
			if(page == 'gridView') {
				$('#tableView').hide();		
				$('#gridView').show();
			} else {
				$('#gridView').hide();
				$('#tableView').show();
			//	oEditors.getById["content"].exec("CHANGE_EDITING_MODE", ["WYSIWYG"]); //포커스얻기
			//	oEditors.getById["content"].exec("RESET_TOOLBAR"); 
			}
		}
		
		
		function xmlParser(xml_url, query_string, op, callback) {
			var rtdata = null;
			var xmlRequest = $.ajax({
				url : xml_url,
				data : query_string,
				type : "get",
				async : false,
				cache : false
			});
			var cid = null;
			if (query_string.indexOf("storeId")) {
				cid = query_string.substring(
						query_string.indexOf("storeId") + "storeId".length).split(
						"&")[0].replace(/=/g, "").trim();
			}
			var tmp = new Array();
			xmlRequest.done(function(res) {
				
				if (typeof res == 'string') {
					window.location.reload();
				}
				$(res).find("menu").each(function() {
					/*
					 * id : 본인아이디
					 * pid : 부모아이디
					 * name : 출력명(보고서 이름)
					 * url : 타이틀 클릭시 액션(URL 링크)
					 * icon : 타이틀 앞 아이콘(보고서 종류에따라 아이콘 변경)
					 * copyImg : 내폴더로 아이콘
					 * copyUrl : 내폴더로 아이콘 클릭시 액션
					 */
					var pid = $(this).find("pid").text();
					var uid = $(this).find("uid").text();
					var name = $(this).find("name").text();
					var link = $(this).find("link").text();
					var icon = $(this).find("icon").text();
					var type = $(this).find("type").text();
	
					tmp.push({
						P_ID : pid,
						C_ID : uid,
						C_NAME : name,
						NAME:name,
						PID:pid,
						SORT_ORDER : link,
						cIcon : icon,
						cType : type
					});
	
				});
	
				// end menu loading 
				$(".loader").hide();
	
				if (callback != null)
					callback(tmp, cid);
				else
					rtdata = {
						res : tmp,
						pid : cid
					};
				//(tmp, op);
				
			
			});
			return rtdata;
	
		}
		
		function renderTreeNode(treeid, element, pmenu) {
	
			var icon = "";
			var afterLabel;
	
			if (element.hasItems) {
				//have a child
				if (element.isExpanded != null && element.isExpanded) {
					icon = 'folderopenIcon.png';
				} else
					icon = 'folderIcon.png';
				afterLabel = "";//자식노드 숫자"<span style='color: Blue;' data-role=='treefoldersize'> (" + $(element.subtreeElement).children().length + ")</span>";
			} else {
				//no have a child
				icon = 'pageIcon.png';
				afterLabel = "";//'&nbsp;<img onclick="setPopup(true);" src="${WWW.CSS}/images/img_icons/popupIcon.png" alt="팝업창" title="새창보기">';
			}
	
			//topmenu ctype이 널이 아니거나 topmenu가 없을 경우..= 코그너스 메뉴이거나 코그너스 탐색기능을 이용한 경우.
			if ((pmenu != null && pmenu.cType != null) || pmenu == null) {
				var menudata = Enumerable.From(curLeftMenu).Where(function(c) {
					return c.C_ID == element.id;
				}).FirstOrDefault(); 
				if(menudata.cIcon.indexOf("folder")!=-1) afterLabel = ""; 
				var iconurl = '${WEB.IMG}' + "/dTree/"
						+ ((menudata != null) ? menudata.cIcon : "page.gif"); 
				if (menudata.cIcon != "loading.gif")
					label = "<img class='menutypeicon' style='float: left; margin-right: 5px;' src='"+iconurl + "'/><span item-title='true' style='width:100px;text-overflow:ellipsis;'  title='"+(element.originalTitle == null ? element.label
							: element.originalTitle)+"'  >"
							+ (element.originalTitle == null ? element.label
									: element.originalTitle)
							+ "</span>"
							+ afterLabel;
				else
					label = "<span item-title='true'  >"
							+ (element.originalTitle == null ? element.label
									: element.originalTitle) + "</span>";
			} else {
				if (icon != "loading.gif")
					label = "<img class='menutypeicon' style='float: left; margin-right: 5px;' src='"+$iv.envinfo.css+"/images/img_icons/" + icon + "'/><span item-title='true'  >"
							+ (element.originalTitle == null ? element.label
									: element.originalTitle)
							+ "</span>"
							+ afterLabel;
				else
					label = "<span item-title='true'  >"
							+ (element.originalTitle == null ? element.label
									: element.originalTitle) + "</span>";
			}
			$("#" + treeid).jqxTree('updateItem', element, {
				label : label
			});
	
			
		} 
		function setPagerLayout(selector) {
			
			var pagesize = $('#'+selector).jqxGrid('pagesize');
			
			var w = 49; 
				
			if(pagesize<100) {
				w = 44;
			} else if(pagesize>99&&pagesize<1000) {
				w = 49;
			} else if(pagesize>999&&pagesize<10000) {
				w = 54;
			}
			
			//디폴트 셋팅
			$('#gridpagerlist'+selector).jqxDropDownList({ width: w+'px' });
			
			//체인지 이벤트 처리
			$('#'+selector).on("pagesizechanged", function (event) {
				var args = event.args;
				
				if(args.pagesize<100) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '44px' });
				} else if(args.pagesize>99 && args.pagesize<1000) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '49px' });
				} else if(args.pagesize>999 && args.pagesize<10000) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '54px' });
				} else {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: 'auto' });
				}
				
			});
		}
	</script>
<style>
</style>

</head>
<body class='default  i-DQ'>
<div class="wrap" style="width:96%; margin:0 auto;">
	<form id="saveForm" name="saveForm" method="post">
	<div class="headertop" style="width:100%; margin:10px auto !important; height:27px;">
		<div class="label-bg label-1" style="float:left;"><spring:message code="menuContentManager.title.solution" />：</div>
		<div  style='float: left; margin-right:10px;'>
			<div style='float: left; width:100px;' id='solutionList' name="solutionList" class="iwidget_combobox"></div>
		</div>	
	</div><!--headertop-->
	
	<div id="splitter" style="margin-top:20px;">
		<div>
			<div style="border: none; padding-top:0px;" id='treeMenuContent'></div>
		</div>
		
		<div id="ContentPanel">
			<div id="gridView">
				<div class="iwidget_label" style="width:98%; margin:10px auto !important; height:27px;  ">
					<div class="label-bg label-2" style="float:left; margin-right:0 !important; padding-right:0 !important;"><spring:message code="menuContentManager.title.search" />：</div>
					<div style='float: left; margin-right:10px;' ><input type="text" id="menuName"/></div>
					<div style='float: left; width:100px; margin-right:10px;' id='searchType' class="iwidget_combobox"></div>
					<div style="float:right;">
						<div class="buttonStyle" style="float:left; height:25px; margin-right:10px !important; ">
							<input type="button" value="<spring:message code="button.text._search" />" id='searchButton' width="100%" height="100%" style="margin-right:0px !important;" />
						</div>
					</div><!--headerbutton-->	 
				</div><!--iwidget_label--> 	
		
				 <div style="width:98%; margin:10px auto !important; ">
					<div class="jqx-grid-area" style=" float:left;width:100%;margin-bottom:20px; ">
						<div style="width:100% !important; height:600px !important; " id='gridMenuContent' ></div>
					</div> 
				</div>
			</div>
			<div id="tableView" style="display: none;">
				<input type="hidden" id="cid" name ="cid"/>
				<input type="hidden" id="progId" name ="progId"/>
				<input type="hidden" id="localCd" name ="localCd"/>
				<input type="hidden" id="parentId" name ="parentId"/>     
				<input type="hidden" id="progNm" name ="progNm"/>     
				          
				<div class="iwidget_label" style="width:98%; margin:10px auto !important; height:27px;  ">			
					<div class="label-bg label-2" style="float:left;">메뉴얼</div> 	    
					<div style="float:right;">
						<div class="buttonStyle" style="float:left; height:25px; margin-right:10px !important; ">
							<input type="button" value="<spring:message code="button.text._save" />" id='saveButton' width="100%" height="100%" style="margin-right:0px !important;" />
						</div>
						 <div class="buttonStyle" style="float:left; height:25px; margin-right:10px !important; ">
							<input type="button" value="<spring:message code="button.text._delete" />" id='deleteButton' width="100%" height="100%" style="margin-right:0px !important;" />
						</div> -
						<div class="buttonStyle" style="float:left; height:25px; margin-right:10px !important; ">
							<input type="button" value="<spring:message code="button.text._list" />" id='listButton' width="100%" height="100%" style="margin-right:0px !important;" />
						</div>   
					</div><!--headerbutton-->	 
				</div><!--iwidget_label-->
				
				<div class="table_Area" style="width:98%; margin:10px auto !important; ">	
					<table width="100%" id="jqxtab_table1" class="i-DQ-table" cellspacing="0" cellpadding="0" border="0" style="margin:10px 0 !important; float:left;">
						<tr>
							<th width="15%"><div class="text_left"><spring:message code="menuContentManager.list.menuName" /></div></th>
							<td colspan="3" class="td_side_left">
								<span id="progName" name="progName"></span>  
							</td>
						</tr> 
						<tr>
							<th width="15%"><div class="text_left"><spring:message code="menuContentManager.title.explain" /></div></th>
							<td colspan="3" >       
								<textarea id="content" name="content" class="textarea_df" style="width:100%; height:500px !important;"></textarea>   
							</td>
						</tr>
					</table>	
				</div><!--도움말 에디터창-->
			</div>
		</div>
	</div>
	</form>
</div><!--wrap-->		
</body>
</html>