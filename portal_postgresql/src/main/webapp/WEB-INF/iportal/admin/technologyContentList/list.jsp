<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>기술 백서</title>
	<link rel="stylesheet" type="text/css" href="../../resource/css/content/font-awesome.css" />   
	<link rel="stylesheet" type="text/css" href="../../resource/css/content/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../../resource/css/content/style.css">       
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>

	<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
    <script src="../../resources/cmresource/js/thirdparty/LGPL/v2/smartEditor2/js/HuskyEZCreator.js" charset="UTF-8"></script>  
	<script type="text/javascript">
	
	$(document).ready(function() {
	     
		treeTechnoList();
		searchContent();          
		
		$(".fa-search").click(function(){
			searchContent();
		});
		
	});

	function EnterCheck(){
	    if(event.keyCode == 13){
	         searchContent();  
	    }
	}
	
	function treeTechnoList() {
		$i.post("../../admin/technologyContentManager/getTechnoContentList").done(function(res){   
			res = res.returnArray;
				var source =
					{
		                datatype: "json",
		                datafields: [
		                    { name: 'c_id'},
		                    { name: 'p_id'},
		                    { name: 'prog_nm'}
		                ],   
		                id : 'c_id',      
		                localdata: res
		            };
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            dataAdapter.dataBind();
		            
		            var records = dataAdapter.getRecordsHierarchy('c_id', 'p_id', 'items', [{ name: 'c_id', map: 'id'} ,{ name: 'prog_nm', map: 'label'},{ name: 'p_id', map: 'value'}]);   
		            $('#treeTechnoContent').jqxTree({source: records, height: '100%', width: '100%', theme:'blueish' });            
		               
					// tree init
		            var items = $('#treeTechnoContent').jqxTree('getItems');
		            var item = null;
		            var size = 0;
		            var img = '';
		            var imgColor ='';
		            var label = '';
		            //var afterLabel = '';   
		            
					for(var i = 0; i < items.length; i++) {
						item = items[i];
						size = $(item.element).find('li').size();   
						if(i == 0) {
							//root
							$("#treeTechnoContent").jqxTree('expandItem', item);
							imgColor ="<a>";       
							if(size > 0){     
								img = 'fa fa-folder';     
								//afterLabel = "<span style='color: Blue;'> (" + size + ")</span>";
							}else{     
								img = 'fa fa-folder-open';     
								imgColor = "<a class='nav-list li a:hover, nav-list li a.active' >";      
								//afterLabel = "";
							}
						} else {
							//children
							if(size > 0) {
								if(items[i].parentElement != null){
									imgColor = "<a class='nav-list li a:hover nav-list li a.active' >";               
									img = 'fa fa-folder';
									//afterLabel = "<span style='color: Blue;'> (" + size + ")</span>";	
								}else{
 									imgColor ="<a>";
									img = 'fa fa-folder';     
									//afterLabel = "<span style='color: Blue;'> (" + size + ")</span>";     
								}
								//have a child
							} else {
								//no have a child
								imgColor = "<a class='nav-list li a:hover, nav-list li a.active' >";
								img = 'fa fa-file-text';   
								//내부에서 사용하는 트리는 팝업 없음.       
								//afterLabel = "";
							}
						}
						
						label = imgColor +"<div style='float: left; margin-right: 5px;' class='" + img + "'/><span item-title='truec style='vertical-align:middle;'>" + item.label + "</span></a>";
						$('#treeTechnoContent').jqxTree('updateItem', item, { label: label});    
					}
		            //add event
		            $('#treeTechnoContent')     
		            .on("expand", function(eve) {
		            	var args = eve.args;                        
		            	if($('#treeTechnoContent').jqxTree('getItem', args.element).label.indexOf("open") < 0){
		            		var label = $('#treeTechnoContent').jqxTree('getItem', args.element).label.replace('fa fa-folder', 'fa fa-folder-open');
			            	args.element.firstChild.className = args.element.firstChild.className.replace('jqx-tree-item-arrow-collapse', '').replace('jqx-icon-arrow-right', '');   
			            	$('#treeTechnoContent').jqxTree('updateItem', args.element, { label: label});                  
		            	}
		            })
		            .on("collapse", function(eve) {
		            	var args = eve.args;          
		            	var label = $('#treeTechnoContent').jqxTree('getItem', args.element).label.replace('fa fa-folder-open', 'fa fa-folder');               
		            	$('#treeTechnoContent').jqxTree('updateItem', args.element, { label: label});         
		            })       
		            .on("select", function(eve) {   
		            	viewDetail($('#treeTechnoContent').jqxTree('getItem', args.element).id);   
		            });
		            $("#treeTechnoContent").jqxTree("expandAll");
		            //$('#treeTechnoContent').jqxTree('selectItem', $("#treeTechnoContent").find('li:first')[0]);       

		     });         
	}
	function labelChange(cid){ 
		$("#topLabelShow").html("");          
			$i.post("../../admin/technologyContentManager/getLabelTechnoContent", {cid:cid}).done(function(res){  
				res = res.returnArray;
				var htmlShow = "";
        		for(var i=0;i<res.length;i++){   
     					if(i == 0){
     						htmlShow += res[i].prog_nm;                                                             
     					}else{                             
     						htmlShow += "<span class='label sublabel type2'>> "+res[i].prog_nm+"</span>";         
     					}     
     				}   
        		$("#topLabelShow").html(htmlShow);  
        	}); 
		}
	function viewDetail(cid) {
		labelChange(cid);
		$("#searchField").val("");
		$("#content").html("");
		
		$i.post("../../admin/technologyContentManager/getTechnoContentInfo",{cid:cid}).done(function(res) {
		res = res.returnArray;
		var content = res[0].content_text;	
		$("#content").append(content);                            
		
		});
	}
	function searchContent() { 
		$("#content").html("");
		$("#topLabelShow").html("검색 및 찾기");     
		$i.post("../../admin/technologyContentManager/getSearchTechnoContent", {content_text:$("#searchField").val()}).done(function(res){      
			res = res.returnArray;
			var len = 0;
			if(res!=null) len = res.length;
			var htmlShow = "";

			htmlShow +=  "<div class='searchMain'>검색 결과<span style='font-size:19px;'>("+len+"건)</span></div>";                                       
			for(var i=0;i<len;i++){   
				
				var contentText = res[i].content_text;
				if(contentText != null && contentText.length>200) {	                  
					contentText = contentText.substring(0, 600) + '...';         
				}
				
				if(i == 0){
 					htmlShow +=  "<div style='color:#222222; font-size:16px; font-weight:bold; padding-bottom:1%'> "+res[i].prog_nm+"</div><div style='height:100px; overflow:hidden !important'>"+contentText+"</div>";                      	                                                           
 				}else{                                 
 					htmlShow +=  "<div style='color:#222222; font-size:16px; border-top:1px solid #ddd; font-weight:bold;padding-top:1%; padding-bottom:1%'> "+res[i].prog_nm+"</div><div style='height:100px; overflow:hidden !important'>"+contentText+"</div>";                              	        
 				}     
					      
 			}   
			$("#content").html(htmlShow);  
        }); 
	}	
	
	</script>
</head>
<style>
	.searchMain {
		color: #000000;
		font-weight: normal;
		font-size: 32px;
		border-bottom: 1px solid #ddd;
		padding-bottom: 5px;
		margin-bottom:20px;
	}
	.main .container p{
		padding:0 !important;
	}
</style>
<body>
	<div id="header">
	 	<h1 class="logo"><img alt="로고" src="../../resource/css/content/logo.png" height="55"></h1>
	 	<div class="search-bar">
	    	<input type="text" class="search-field" id="searchField"  placeholder="Search..."  onkeydown="JavaScript:EnterCheck();">
	    	<div class="search-submit" > 
	      		<div title="검색" class="search-submit-icon fa fa-search"> </div>   
	      	</div>
	  	</div>
	</div>   
	<div id="contents">    
		<div class="nav">
    		<nav class="sidenav" id="mySidenav">
				<div  class="tree" style="border: none; padding-top:0px;" id="treeTechnoContent"></div>   
    		</nav>
    	</div>
    	<div class="main">
	    	<div class="breadcrumbsBox f_left" style="width:100%;">
		    	<a class="breadcrumbsBoxLink f_left" >                                    
		      		<div class="fa fa-home" title="홈"></div>         
		      	</a>  
		      	<span class="breadcrumbsBoxArrow f_left"> &gt; </span>                     
		      	<a class="breadcrumbsBoxLink f_left" ><apan class="breadcrumbs" id="topLabelShow">&nbsp;</apan></a>   
	    	</div>
	    	<div class="row padding-20" style="overflow-y:scroll; height:580px;">   
	    		<div class="container">
			  		<div  id="content" name="content"></div>                  
			 	</div>
		 	</div>    
		   	<footer id="myFooter">     
				<div class="footer-container theme-l2">
				  <p>COPYRIGHT© 2016 IPLANBIZ ALL RIGHTS RESERVED.</p>
				</div>
				<div class="footer-container theme-l1  padding-20">
				  	<h4><img src="../../resource/css/content/footer-logo.png" height="50"></h4>  
				</div>
		   	</footer>
		</div>
 	</div>    
</body>
</html>