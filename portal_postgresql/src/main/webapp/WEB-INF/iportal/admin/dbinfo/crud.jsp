<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>DB 접속 정보</title>
       <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <script>
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		$("#btnSearch").jqxButton({width:'', theme:'blueish'});
        		$("#btnNew").jqxButton({width:'', theme:'blueish'});
        		search();
        	}
        	//조회 
        	function search(){
        		var dbManagerData = $i.post("./getDbInfoList",{});
        		dbManagerData.done(function(dbData){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'DBNAME', type: 'string' },
                        	{ name: 'DBKEY', type: 'string' },
                        	{ name: 'VENDERCODE', type: 'string' },
                        	{ name: 'VENDERNAME', type: 'string' },
                        	{ name: 'SCHEMATYPE', type: 'string' },
                        	{ name: 'SCHEMATYPENAME', type: 'string' },
                        	{ name: 'IPADDRESS', type: 'string'},
                        	{ name: 'PORT', type:'string'},
                        	{ name: 'SCHEMANAME', type:'string'},
                        	{ name: 'USERID', type:'string' },
                        	{ name: 'MAXACTIVE', type: 'string'},
                        	{ name: 'MAXIDLE', type:'string'},
                        	{ name: 'MAXWAIT', type:'string'}

		                ],
		                localdata: dbData.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
        			 var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var noScript = function(row, columnfield, value){
	                	var newValue = $i.secure.scriptToText(value);
	                	return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
	                };
					
		            var detail = function(row,datafield,value, defaultHtml, property, rowdata){
		            	var comlCod = rowdata.DBKEY;
		            	var newValue = $i.secure.scriptToText(value);
	                	var link = "<a href=\"javaScript:goRow("+row+")\" style='color:black;text-decoration:underline;'>" + newValue + "</a>";
	                	
	                	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px;'>" + link + "</div>";
	                };
	                var buttonDetail = function(row,datafield,value, defaultHtml, property, rowdata){
	                	//var comlCod = rowdata.DBKEY;
	                	var link = "<a href=\"javaScript:getCheckCon("+row+");\"><img src='../../resources/img/button/button_CheckConnection.png' class='button' id='dbKey"+row+"' /></a><span style='color:red;display:none;' id='span"+row+"'>확인중</span>";
	                	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px;'>" + link + "</div>";
	                };
		            $("#gridDbList").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                pageable: false,
		                columns: [
							{ text: 'TEST', dataField: 'aa', width: 146, align:'center', cellsalign: 'center' , cellsrenderer: buttonDetail},
    				  		{ text: '이름', dataField: 'DBNAME', width: 146, align:'center', cellsalign: 'center',cellsrenderer: noScript },
                      		{ text: 'KEY', dataField: 'DBKEY', width: 146, align:'center', cellsalign: 'center', cellsrenderer:detail},
                      		{ text: '벤더',  dataField: 'VENDERNAME', width:146, align:'center',  cellsalign: 'center' },
	                      	{ text: 'DBType',  dataField: 'SCHEMATYPENAME', width:146, align:'center',  cellsalign: 'center' },
	                      	{ text: 'IP 주소',  editable: false, dataField: 'IPADDRESS', width: 146, cellsalign: 'center', align:'center' ,cellsrenderer: noScript},
	                      	{ text: '포트',  editable: false, dataField: 'PORT', width: 146, cellsalign: 'center', align:'center' },
	                      	{ text: 'Database',  editable: false, dataField: 'SCHEMANAME', width: 146, cellsalign: 'center', align:'center',cellsrenderer: noScript },
	                      	{ text: 'User ID',  editable: false, dataField: 'USERID', width: 146, cellsalign: 'center', align:'center',cellsrenderer: noScript },
                      		{ text: 'MAX Active',  editable: false, dataField: 'MAXACTIVE', width: 120, cellsalign: 'center', align:'center' },
                      		{ text: 'MAX IDLE',  editable: false, dataField: 'MAXIDLE', width: 120, cellsalign: 'center', align:'center' },
                      		{ text: 'MAX Wait',  editable: false, dataField: 'MAXWAIT', width: 120, cellsalign: 'center', align:'center', cellsrenderer:alginLeft }
						],
		            });
        		});
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function goRow(row){
        		var data = $('#gridDbList').jqxGrid('getrowdata', row);
        		var dbkey = data.DBKEY;
        		location.replace("./detail?dbKey="+dbkey);
        	}
        	function newDbInfo(){
        		location.replace("./detail");
        	}
        	function getCheckCon(row){
        		var data = $('#gridDbList').jqxGrid('getrowdata', row);
        		var dbkey = data.DBKEY;
        		var checkData = $i.post("./checkDB", {dbKey:dbkey});
        		checkData.done(function(data){
        			if(data.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM", data.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM", data.returnMessage,function(){
        					$i.post("/idashboard/source/init",{dbkey:dbkey});
        				});
        			}
        		});
// 	    		$("#dbKey"+dbKey).hide();
// 	    		$("#span"+dbKey)[0].style.display = "";
// 	    		$("#ac").val("check");
// 				$("#dbKey").val(dbKey);
// 				location.replace("${WWW.IPORTAL}/DbInfoSetting?ac=check&dbKey="+dbKey);
	    	}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
		</style>
		   
    </head>
    <body class='blueish'>
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%; min-width:1160px; margin:0 10px;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<form id="searchForm" name="searchForm" action="./list">
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
						<div class="button type1 f_left">
							<input type="button" value="신규" id='btnNew' width="100%" height="100%" onclick="newDbInfo();" />
						</div>
					</div>
				</form>
			</div>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style="width:100%; margin:0 0%;">
					<div class="grid f_left" style="width:100%; height:620px;">
						<div id="gridDbList"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>