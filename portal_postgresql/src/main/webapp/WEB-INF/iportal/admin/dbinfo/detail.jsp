<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<!-- 		<meta http-equiv="X-UA-Compatible" content="IE=8"/> -->
		<title>DB 접속 정보</title>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
		<script type="text/javascript">
		var errMessage = "${msg}";
		var dbTypeValue = "";
		$(document).ready(function(){
			init();
			$('#saveForm').jqxValidator({
				rtl:true,  
				rules:[
				    { input: "#txtDbName", message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtDbKey', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtIpAddress', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtPort', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtUserID', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtPwd', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtSchemaName', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtMaxActive', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtMaxIDLE', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtMaxWait', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' }
		        ]
			});
		});
		function init(){
			//목록버튼
			$("#btnList").jqxButton({ width : '' , theme : 'blueish' });
			//저장버튼
			$("#btnInsert").jqxButton({ width : '' , theme : 'blueish' });
			//삭제버튼
			$("#btnDelete").jqxButton({ width : '' , theme : 'blueish' });
			setData();
			makeCombobox();
		}
		function insert(){
			$i.insert("#saveForm").done(function(args){
       			if(args.returnCode == "EXCEPTION"){
       				$i.dialog.error("SYSTEM","저장 중 오류가 발생했습니다 : "+args.returnMessage);
       			}else{
       				$i.dialog.alert("SYSTEM","저장 되었습니다", function(){
       					goList();
       				});
       			}
       		});
       	}
       	function remove(){
       		$i.dialog.confirm("SYSTEM", "삭제하시겠습니까?",function(){
       			$i.remove("#saveForm").done(function(args){
      				if(args.returnCode == "EXCEPTION"){
	      				$i.dialog.error("SYSTEM",args.returnMessage);
      				}else{
	      				$i.dialog.alert("SYSTEM","삭제 되었습니다.", function(){
	      					goList();
	      				});
      				}
    			});
       		});
       	}
       	function checkDbKey(){
       		if($("#saveForm").jqxValidator("validate") == true){
       			if($("#hiddenInsertType").val() == "new"){
           			var checkData = $i.post("./checkDbKey", {dbKey:$("#txtDbKey").val()});
    				checkData.done(function(data){
    					if(data.returnCode == "EXCEPTION"){
    						$i.dialog.error("SYSTEM", data.returnMessage);
    					}else{
    						insert();
    					}
    				});
           		}else{
           			insert();
           		}
       		}
       	}
       	function setData(){
       		var dbKey = "${param.dbKey}";
       		if(dbKey == ""){
       			$("#hiddenInsertType").val("new");
       		}
       		var detailData = $i.post("./getDbInfoDetail", {dbKey:dbKey});
       		detailData.done(function(data){
       			if(data.returnCode == "EXCEPTION"){
       				$i.dialog.error("SYSTEM", data.returnMessage);
       			}else{
       				if(data.returnArray.length > 0){
       					$("#hiddenInsertType").val("update");
       					$("#txtDbName").val(data.returnArray[0].DBNAME);
       					$("#txtDbKey").val(data.returnArray[0].DBKEY);
       					$("#hiddenOrgDbKey").val(data.returnArray[0].DBKEY);
       					$("#cboVenderCode").val(data.returnArray[0].VENDERCODE);
       					$("#cboSchemaType").val(data.returnArray[0].SCHEMATYPE);
       					dbTypeValue=data.returnArray[0].SCHEMATYPE;
       					$("#txtIpAddress").val(data.returnArray[0].IPADDRESS);
       					$("#txtPort").val(data.returnArray[0].PORT);
       					$("#txtUserID").val(data.returnArray[0].USERID);
       					$("#txtPwd").val(data.returnArray[0].PWD);
       					$("#txtSchemaName").val(data.returnArray[0].SCHEMANAME);
       					$("#txtMaxActive").val(data.returnArray[0].MAXACTIVE);
       					$("#txtMaxIDLE").val(data.returnArray[0].MAXIDLE);
       					$("#txtMaxWait").val(data.returnArray[0].MAXWAIT);
       				}
       			}
       		});
		}
		function makeCombobox(){
			var venderData = $i.post("./getDbInfoCodeList", {comlCod:"DATA_VENDERCODE"});
			venderData.done(function(data){
				var source =
				{
					datatype: "json",
					datafields: [
						{ name: 'COM_COD'},
						{ name: 'COM_NM'}
					],
					id : 'value',
					localdata: data.returnArray
				};
				var dataAdapter = new $.jqx.dataAdapter(source);
				$("#cboVenderCode").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 100, dropDownHeight: 52, width: 100, height: 19,  theme:'blueish' });
				changeVender();
			});
		}
		function changeVender(){
			var comlCod = "";
			if($("#cboVenderCode").val() == "oracle"){
				comlCod = "DB_TYPE_ORACLE";
			}else{
				comlCod = "DB_TYPE_OTHER";
			}
			var venderData = $i.post("./getDbInfoCodeList", {comlCod:comlCod});
			venderData.done(function(data){
				var source =
				{
					datatype: "json",
					datafields: [
						{ name: 'COM_COD'},
						{ name: 'COM_NM'}
					],
					id : 'value',
					localdata: data.returnArray
				};
				var dataAdapter = new $.jqx.dataAdapter(source);
				$("#cboSchemaType").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 100, dropDownHeight: 52, width: 100, height: 19,  theme:'blueish' });
				if(dbTypeValue != ""){ 
					$("#cboSchemaType").val(dbTypeValue);
				}
			});
		};
		function goList() {
			location.replace("./crud");
		}
		</script>
		<style  type="text/css">
        	.jqx-validator-hint{
        		background-color : transparent !important;
        		border:0px !important;
        	}
        	.jqx-validator-hint-arrow{
        		display:none !important;
        	}
        </style>
	</head>
	<body class='blueish'>
		<div class="wrap" style="width:1160px; min-width:1160px; margin:0 10px;">
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:100%;">
					<div class="group f_left  w100p m_b5">
						<div class="group_button f_right">	
							<div class="button type2 f_left" style="margin-bottom:0;">
								<input type="button" value="목록" id='btnList' width="100%" height="100%" onclick="goList();" />
							</div>
							<div class="button type2 f_left" style="margin-bottom:0;">
								<input type="button" value="저장" id='btnInsert' width="100%" height="100%" onclick="checkDbKey();" />
							</div>
							<div class="button type3 f_left" style="margin-bottom:0;">
								<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();" />
							</div>
						</div><!--group_button-->
					</div>
					<form id="saveForm" name="saveForm">
						<input type="hidden" id="hiddenInsertType" name="insertType" />
						<div class="table  f_left" style="width:100%; margin:0; ">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<tbody>
									<tr>
										<th class="w15p">
											<div>이름<span class="th_must"></span></div>
										</th>
										<td class="w35p">
											<div class="cell">
												<input type="text" id="txtDbName" name="dbName" class="input type2" style="width:98.5%; margin:3px 0;" />
											</div>
										</td>
										<th class="w15p">
											<div>KEY<span class="th_must"></span></div>
										</th>
										<td class="w35p">
											<div class="cell">
												<input type="hidden" id="hiddenOrgDbKey" name="orgDbKey" />
												<input type="text" id="txtDbKey" name="dbKey" class="input type2" style="width:98.5%; margin:3px 0;" />
											</div>
										</td>
									</tr>
									<tr>
										<th class="w15p">
											<div>벤더<span class="th_must"></span></div>
										</th>
										<td class="w35p">
											<div class="cell">
												<div class="combobox" id="cboVenderCode" name="venderCode" onchange="changeVender();"></div>
											</div>
										</td>
										<th class="w15p">
											<div>DB Type<span class="th_must"></span></div>
										</th>
										<td class="w35p">
											<div class="cell">
												<div class="combobox" id="cboSchemaType" name="schemaType"></div>
											</div>
										</td>
									</tr>
									<tr>
										<th class="w15p">
											<div>IP 주소<span class="th_must"></span></div>
										</th>
										<td class="w35p">
											<div class="cell">
												<input type="text" id="txtIpAddress" name="ipAddress" class="input type2" style="width:98.5%; margin:3px 0;" />
											</div>
										</td>
										<th class="w15p">
											<div>포트<span class="th_must"></span></div>
										</th>
										<td class="w35p">
											<div class="cell">
												<input type="text" id="txtPort" name="port" class="input type2" style="width:98.5%; margin:3px 0;" />
											</div>
										</td>
									</tr>
									<tr>
										<th class="w15p">
											<div>User ID<span class="th_must"></span></div>
										</th>
										<td class="w35p">
											<div class="cell">
												<input type="text" id="txtUserID" name="userID" class="input type2" style="width:98.5%; margin:3px 0;" />
											</div>
										</td>
										<th class="w15p">
											<div>Password<span class="th_must"></span></div>
										</th>
										<td class="w35p">
											<div class="cell">
												<input type="password" id="txtPwd" name="pwd" class="input type2" style="width:98.5%; margin:3px 0;" />
											</div>
										</td>
									</tr>
									<tr>
										<th class="w15p">
											<div>DataBase<span class="th_must"></span></div>
										</th>
										<td class="w35p">
											<div class="cell">
												<input type="text" id="txtSchemaName" name="schemaName" class="input type2" style="width:98.5%; margin:3px 0;" />
											</div>
										</td>
										<th class="w15p">
											<div>Max Active<span class="th_must"></span></div>
										</th>
										<td class="w35p">
											<div class="cell">
												<input type="text" id="txtMaxActive" name="maxActive" class="input type2" style="width:98.5%; margin:3px 0;" />
											</div>
										</td>
									</tr>
									<tr>
										<th class="w15p">
											<div>Max IDLE<span class="th_must"></span></div>
										</th>
										<td class="w35p">
											<div class="cell">
												<input type="text" id="txtMaxIDLE" name="maxIDLE" class="input type2" style="width:98.5%; margin:3px 0;" />
											</div>
										</td>
										<th class="w15p">
											<div>Max Wait<span class="th_must"></span></div>
										</th>
										<td class="w35p">
											<div class="cell">
												<input type="text" id="txtMaxWait" name="maxWait" class="input type2" style="width:98.5%; margin:3px 0;" />
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>