<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=9"/>
<title id="Description">메뉴권한조회</title>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/css/iplanbiz/styles/table.css" />
<link rel="stylesheet" type="text/css" href="../../resources/css/iplanbiz/styles/jqx.simple-gray.css"/>
<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$i.dialog.setAjaxEventPopup("SYSTEM","데이터 조회 중 입니다.");
	// 탭
	$('#tabUserpermission').jqxTabs({ width: '100%', height: '100%', position: 'top', theme:'blueish', selectedItem: 0});
	
	// 콤보박스
	initCboSearchgb();
	
	
	initialize(); // jqx 초기화
	
	searchStart();
});
/********************************* 전역변수 셋팅 시작 *********************************/
// 소스
var srcuser  = null; // 사용자 그리드
var srcmenu  = null; // 메뉴 트리그리드
var srcgroup = null; // 그룹 그리드
var srcrole  = null; // 롤 그리드

//어뎁터
var adauser  = null; // 사용자 그리드
var adamenu  = null; // 메뉴 트리그리드
var adagroup = null; // 그룹 그리드
var adarole  = null; // 롤 그리드

var folderIcon = '../../resources/css/images/img_icons/folderIcon.png'; // 폴더 아이콘
var pageIcon   = '../../resources/css/images/img_icons/pageIcon.png';   // 페이지 아이콘
	
// jqx 셀 기본 렌더러
var defulatrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
	var margin = '';
	var cellsalign = columnproperties.cellsalign;
	
	if(cellsalign == 'center') {
		margin = 'margin:6px  0px 0px  0px;';
	} else if(cellsalign == 'left') {
		margin = 'margin:6px  0px 0px 10px;';
	} else if(cellsalign == 'right') {
		margin = 'margin:6px 10px 0px  0px;';
	}
	return '<div style="text-align:'+columnproperties.cellsalign+'; '+margin+'">'+value+'</div>';;
};

// jqx 셀 링크 렌더러
var userlinkrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
	/* 
	if (value.indexOf('#') != -1) {
		value = value.substring(0, value.indexOf('#'));
	}
	var format = { target: '"_self", href="javascript:viewSc();"' };
	var html = $.jqx.dataFormat.formatlink(value, format);
	*/
	var html = '<a href="javascript:viewUserpermission('+row+')">'+value+'</a>';
	
	var margin = '';
	var cellsalign = columnproperties.cellsalign;
	if(cellsalign == 'center') {
		margin = 'margin:6px  0px 0px  0px';
	} else if(cellsalign == 'left') {
		margin = 'margin:6px  0px 0px 10px';
	} else if(cellsalign == 'right') {
		margin = 'margin:6px 10px 0px  0px';
	}
	return '<div style="text-align:'+columnproperties.cellsalign+'; '+margin+'; text-decoration: underline;">'+html+'</div>';
};
/********************************* 전역변수 셋팅 종료 *********************************/
function initialize() {
	var dummy = [];
	
	// 사용자 그리드
	srcuser = {
		datatype: 'json',
		localdata: dummy,
		datafields: [
			{ name: 'EMP_ID', type: 'string'},
			{ name: 'EMP_NM', type: 'string'},
			{ name: 'USER_GRP_NM', type: 'string'}
		],
		updaterow: function (rowid, rowdata, commit) {
			commit(true);
		}
	};
	adauser = new $.jqx.dataAdapter(srcuser);
	
	$('#gridUser').jqxGrid({
		width: '100%',
		height:'100%',
		altrows:true,
		pageable: false,
		source: adauser,
		theme:'blueish',
		sortable: true,
		columnsresize: true,
		columns: [
			{ text: '사번', datafield: 'EMP_ID', width: '30%', align:'center', cellsalign: 'center', cellsrenderer: defulatrenderer},
			{ text: '이름', datafield: 'EMP_NM', width: '25%', align:'center', cellsalign: 'center', cellsrenderer: userlinkrenderer},
			{ text: '부서', datafield: 'USER_GRP_NM', width: '45%', align:'center', cellsalign: 'left', cellsrenderer: defulatrenderer}  
		]
	});
	
	
	// 메뉴 트리그리드 - 권한이 부여된 보고서
	$treegridUserpermission = $('#treegridUserpermission');
	srcmenu = {
		dataType: 'json',
		datafields: [
			{ name: 'C_ID'      , type: 'string' },
			{ name: 'P_ID'      , type: 'string' },
			{ name: 'C_NAME'    , type: 'string' },
			{ name: 'MENU_TYPE' , type: 'number' },
			{ name: 'PERM_ALL'  , type: 'number' },
			{ name: 'PERM_USER' , type: 'number' },
			{ name: 'PERM_GROUP', type: 'number' },
			{ name: 'PERM_ROLE' , type: 'number' }
		],
		hierarchy: {
			keyDataField: { name: 'C_ID' },
			parentDataField: { name: 'P_ID' }
		},
		id: 'C_ID',
		localData: dummy
	};
	var adamenu  = new $.jqx.dataAdapter(srcmenu, {
		beforeLoadComplete: function (records) {
			for(var i=0; i<records.length; i++) {
				var menutype = records[i].MENU_TYPE;
				var imgurl = '';
				
				if(menutype == 1) {
					imgurl = folderIcon;
				} else if(menutype == 2) {
					imgurl = pageIcon;	
				} 	
				
				records[i].icon = imgurl;
			}
			return records;
		}
	});

	$treegridUserpermission.jqxTreeGrid({
		width: '100%',
		height: '100%',
		source: adamenu,
		theme: 'simple-gray',
		pageable: false,
		columnsResize: true,
		autoRowHeight: false,
		selectionMode: 'none',
		icons: true,
		ready: function() {
		},
		columns: [
			{ text: '보고서명', dataField: 'C_NAME'    , align:'center', cellsalign: 'left'  }, // 가변 너비
			{ text: '전체공개', dataField: 'PERM_ALL'  , align:'center', cellsalign: 'center', width: 80 },
			{ text: '사용자'  , dataField: 'PERM_USER' , align:'center', cellsalign: 'center', width: 80, columnGroup: 'common' },
			{ text: '그룹'    , dataField: 'PERM_GROUP', align:'center', cellsalign: 'center', width: 80, columnGroup: 'common' },
			{ text: 'ROLE'    , dataField: 'PERM_ROLE' , align:'center', cellsalign: 'center', width: 80, columnGroup: 'common' }
		],
		columnGroups: [{ text: '권한공개', name: 'common', align: 'center' }]
	});
	
	// 그룹 그리드 - 사용자가 속한 그룹
	srcgroup = {
		datatype: 'json',
		localdata: dummy,
		datafields: [
			{ name: 'PID', type: 'string'},
			{ name: 'USER_GRP_NM', type: 'string'}
		],
		updaterow: function (rowid, rowdata, commit) {
			commit(true);
		}
	};
	var adagroup = new $.jqx.dataAdapter(srcgroup);
	
	$('#gridGroup').jqxGrid({
		width: '100%',
		height:'100%',
		altrows:true,
		pageable: false,
		source: adagroup,
		theme:'blueish',
		sortable: false,
		columnsresize: true,
		editable: true,
		columns: [
			{ text: '그룹명', datafield: 'USER_GRP_NM', width: '100%', align:'center', cellsalign: 'left', editable: false , cellsrenderer: defulatrenderer}
		]
	});		
	
	
	// 롤 그리드 - 사용자가 속한 롤
	srcrole = {
		datatype: 'json',
		localdata: dummy,
		datafields: [
			{ name: 'GID', type: 'string'},
			{ name: 'GROUP_NAME', type: 'string'}
		],
		updaterow: function (rowid, rowdata, commit) {
			commit(true);
		}
	};
	adarole = new $.jqx.dataAdapter(srcrole);
	
	$('#gridRole').jqxGrid({
		width: '100%',
		height:'100%',
		altrows:true,
		pageable: false,
		source: adarole,
		theme:'blueish',
		sortable: false,
		columnsresize: true,
		editable: true,
		columns: [
			{ text: 'Role명', datafield: 'GROUP_NAME', width: '100%', align:'center', cellsalign: 'left', editable: false , cellsrenderer: defulatrenderer}
		]
	});
	
}
function initCboSearchgb() {
	
	var data  = [
		{value:'name',text:'이름'},
		{value:'dept',text:'부서'}
	];
	
	var source = {
		datatype: 'json',
		datafields: [
			{ name: 'value' },
			{ name: 'text' }
		],
		id: 'id',
		localdata:data,
		async: false
	};
	var dataAdapter = new $.jqx.dataAdapter(source);

	$('#cboSearchgb').jqxComboBox({selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "text", valueMember: "value", dropDownWidth: 60, dropDownHeight: 80, width: 60, height: 19, theme:'blueish'});
}
function searchStart() {
	
	var empnm = '';
	var grpnm = '';
	
	if($('#cboSearchgb').val() == 'name'){
		empnm = $('#txtSearchval').val();
	}else{
		grpnm = $('#txtSearchval').val(); 
	}  
	
// 	$iv.progress.show("Information","화면이 생성 중 입니다.");
	var data = $i.post("./getMenuAuthroitySearchUserList", {empName : empnm, userGrpName : grpnm});
	data.done(function(datauser){
		srcuser.localdata = datauser.returnArray;
		adauser = new $.jqx.dataAdapter(srcuser);
		var $gridUser = $('#gridUser');
		$gridUser.jqxGrid('clearselection');
		$gridUser.jqxGrid({ source: adauser });

		$('#username').text('');
		$('#treegridUserpermission').jqxTreeGrid('clear');
		$('#gridGroup').jqxGrid('clear');
		$('#gridRole').jqxGrid('clear');					

// 		$iv.progress.hide();
	});
}
var isTab2Opend=false;
function tab2Search(){
	if(isTab2Opend==false){
		isTab2Opend=true;
		var data = $i.post("./getMenuAuthoritySearchMenuByReport", {});
		data.done(function(res){
			$element = $("#treeGridMenuList");
			makeTab2TreeGrid(res.returnArray, $element);
		});
	}
	
}
function makeTab2TreeGrid(res, $element){
	$element.jqxTreeGrid('clear');
	var source = {
		datatype: "json",
		datafields: [
			{ name: 'C_ID'},
			{ name: 'PID'},
			{ name: 'NAME'},
			{ name: 'MENU_TYPE'},
			{ name: 'MENU_OPEN_TYPE'}
		],
		icons:true,
		altrows:false,
		pageable:false,  
		localdata: res,
		hierarchy:{
			keyDataField:{name:'C_ID'},
			parentDataField:{name:'PID'} 
		},
		id:'C_ID'
	};
	var dataAdapter = new $.jqx.dataAdapter(source);
	
	$element.jqxTreeGrid({source: dataAdapter, height: '100%', width: '100%',theme:"blueish",
		icons:function(rowKey, rowdata) { return getIconImage(rowdata.MENU_TYPE, rowdata.MENU_TYPE, rowdata.MENU_OPEN_TYPE); },
		columns:[
			{ dataField:'C_ID',text:'C_ID',align:'center', hidden:true},
			{ dataField:'PID',text:'PID',align:'center', hidden:true},
			{ dataField:'NAME',text:'NAME', algin:'center',
				cellsrenderer:function(row,datafield,value,rowdata){
					if(rowdata.records!=null){
						var cntlabel = "<span style='color: Blue;' data-role=='treefoldersize'> (" + rowdata.records.length + ")</span>";
						return "<span onclick=\"reportMidGrid("+rowdata.C_ID+",'"+rowdata.NAME+"');\" style='color:#000000;'>"+rowdata.NAME+cntlabel+"</span>";
					} else {
						return "<span onclick=\"reportMidGrid("+rowdata.C_ID+",'"+rowdata.NAME+"');\" style='color:#000000;'>"+rowdata.NAME+"</span>";
					}
				}
			}
		],
		ready:function(){
		}
	});
	<%-- PID 가 사라져, ROOT 에 표시되는 메뉴 제거 --%>
// 				deleteNullMenu($element);
	$element.on('rowExpand', function (event){
	    var args = event.args;
	    var row = args.row;
	    var key = row.C_ID;
	    var icon;
	    icon = '../../resources/css/images/img_icons/permission/folderopenIcon.png';				    
	}); 
	$element.css('visibility', 'visible');
	reportMidGrid('','');
}
function reportMidGrid(cID, name){
	$("#reportName").html(name);
	var data = $i.post("./getMenuAuthoritySearchMenuUserByReport", { cID : cID});
	data.done(function(res){
		var usergridsource = {
			datatype: 'json',
			datafields: [
				{ name: 'EMP_ID', type: 'string'},
				{ name: 'EMP_NM', type: 'string'},
				{ name: 'USER_GRP_NM', type: 'string'},
				{ name: 'MENU_TYPE', type:'string'},
				{ name: 'P_TYPE', type:'string'},
				{ name: 'G_TYPE', type:'string'},
				{ name: 'U_TYPE', type:'string'}
			],
			localdata: res.returnArray,
			updaterow: function (rowid, rowdata, commit) {
				commit(true);
			}
		};
		var defulatrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
			var margin = '';
			var cellsalign = columnproperties.cellsalign;
			
			if(cellsalign == 'center') {
				margin = 'margin:6px  0px 0px  0px;';
			} else if(cellsalign == 'left') {
				margin = 'margin:6px  0px 0px 10px;';
			} else if(cellsalign == 'right') {
				margin = 'margin:6px 10px 0px  0px;';
			}
			return '<div style="text-align:'+columnproperties.cellsalign+'; '+margin+'">'+value+'</div>';
		};
		var detailRender = function (row, columnfield, value, defautHtml, property, rowdata){
			var link = "<a href=\"javascript:reportUserRoleList('"+rowdata.EMP_ID+"');\" style='color:#000000;'>"+value+"</a>";
			
			return '<div style="text-align:center;margin:6px; 0px 0px 0px;text-decoration: underline;">'+link+'</div>';
		};
		var dataAdapter = new $.jqx.dataAdapter(usergridsource);
		$("#gridReportByUser").jqxGrid(
		{
			width: '100%',
			height:'100%',
			altrows:true,
			pageable: false,
			source: dataAdapter,
			theme: 'simple-gray',
			sortable: true,
			columnsresize: true,
			editable: true,
			columns: [
				{ text: '사번', datafield: 'EMP_ID', width: '10%', align:'center', cellsalign: 'center', editable: false, cellsrenderer: defulatrenderer},
				{ text: '이름', datafield: 'EMP_NM', width: '20%', align:'center', cellsalign: 'center', editable: false, cellsrenderer: detailRender},
				{ text: '부서', datafield: 'USER_GRP_NM', width: '30%', align:'center', cellsalign: 'left',cellsrenderer: defulatrenderer, editable: false},
				{ text: '전체공개', datafield: 'MENU_TYPE', width: '10%', align:'center', cellsalign: 'center',cellsrenderer: defulatrenderer, editable: false},
				{ text: '사용자', datafield: 'U_TYPE', width: '10%', align:'center', cellsalign: 'center',cellsrenderer: defulatrenderer, editable: false, columnGroup: 'common' },
				{ text: '그룹', datafield: 'P_TYPE', width: '10%', align:'center', cellsalign: 'center',cellsrenderer: defulatrenderer, editable: false, columnGroup: 'common' },
				{ text: 'ROLE', datafield: 'G_TYPE', width: '10%', align:'center', cellsalign: 'center',cellsrenderer: defulatrenderer, editable: false, columnGroup: 'common' },
			],
			columnGroups: [{ text: '권한공개', name: 'common', align: 'center' }]
		});
		reportUserRoleList('!@#$');
	});
}
function getIconImage(MENU_TYPE, MENU_TYPE, MENU_OPEN_TYPE) {
	var icon = '';
	
	if(MENU_TYPE!=null&&MENU_TYPE=="2") {
		icon = '../../resources/css/images/img_icons/permission/pageIcon.png';
	} else {
		icon = '../../resources/css/images/img_icons/permission/folderIcon.png';
	}
	return icon;
}
function reportUserRoleList(empid){
	var groupData = $i.post("./getMenuAuthoritySearchGroupByEmp", { empID : empid });
	groupData.done(function(datagroup){
		var reportGroup = {
			datatype: 'json',
			localdata: datagroup,
			datafields: [
				{ name: 'PID', type: 'string'},
				{ name: 'USER_GRP_NM', type: 'string'}
			],
			updaterow: function (rowid, rowdata, commit) {
				commit(true);
			}
		};
		var adagroup = new $.jqx.dataAdapter(reportGroup);
		
		$('#gridReportGroup').jqxGrid({
			width: '100%',
			height:'100%',
			altrows:true,
			pageable: false,
			source: adagroup,
			theme:'blueish',
			sortable: false,
			columnsresize: true,
			editable: true,
			columns: [
				{ text: '그룹명', datafield: 'USER_GRP_NM', width: '100%', align:'center', cellsalign: 'left', editable: false , cellsrenderer: defulatrenderer}
			]
		});		
	});
	var roleData = $i.post("./getMenuAuthoritySearchRoleByEmp", { empID : empid });
	roleData.done(function(datarole){
		var reportRole = {
			datatype: 'json',
			localdata: datarole,
			datafields: [
				{ name: 'GID', type: 'string'},
				{ name: 'GROUP_NAME', type: 'string'}
			],
			updaterow: function (rowid, rowdata, commit) {
				commit(true);
			}
		};
		adarole = new $.jqx.dataAdapter(reportRole);
		
		$('#gridReportRole').jqxGrid({
			width: '100%',
			height:'100%',
			altrows:true,
			pageable: false,
			source: adarole,
			theme:'blueish',
			sortable: false,
			columnsresize: true,
			editable: true,
			columns: [
				{ text: 'Role명', datafield: 'GROUP_NAME', width: '100%', align:'center', cellsalign: 'left', editable: false , cellsrenderer: defulatrenderer}
			]
		});
	});
}
function viewUserpermission(row) {
	var rowdata = $('#gridUser').jqxGrid('getrowdata', row);
	var empid   = rowdata.EMP_ID;
	var empnm   = rowdata.EMP_NM;
	
	$('#username').text(empnm);
// 	getMenuAuthoritySearchList
	var data = $i.post("./getMenuAuthoritySearchList", { empID : empid });
	data.done(function(datamenu){
		srcmenu.localdata = datamenu.returnArray;
		var $treegridUserpermission = $('#treegridUserpermission');
		$treegridUserpermission.jqxTreeGrid('updateBoundData');
		
		<%--쿼리에서 처리하지 못한 데이터 삭제--%>
		var rows = $treegridUserpermission.jqxTreeGrid('getRows');
		var length = rows.length;
		for(var i=length-1; i>=0; i--) {
			var row = rows[i];
			if(row.level == 0 && row.P_ID != '5999') {
				var key = $treegridUserpermission.jqxTreeGrid('getKey', row);
				$treegridUserpermission.jqxTreeGrid('deleteRow', key);
			}
		}
		
		// 아이콘 누락 update
		var viewuser = $treegridUserpermission.jqxTreeGrid('getView');
		var displayView = function (records) {
			for (var i = 0; i < records.length; i++) {
				if(records[i].icon == '') {
					if(!records[i].icon && records[i].records) {
						records[i].icon = folderIcon;
					} else 	if(!records[i].icon && !records[i].records) {
						records[i].icon = pageIcon;
					}
				}				
				if (records[i].records) {
					displayView(records[i].records);
				}
			}
		};
		displayView(viewuser);
		$treegridUserpermission.jqxTreeGrid('expandAll');
		var groupData = $i.post("./getMenuAuthoritySearchGroupByEmp", { empID : empid });
		groupData.done(function(datagroup){
			srcgroup.localdata = datagroup.returnArray;
			adagroup = new $.jqx.dataAdapter(srcgroup);
			var $gridGroup = $('#gridGroup');
			$gridGroup.jqxGrid('clearselection');
			$gridGroup.jqxGrid({ source: adagroup });
			var roleData = $i.post("./getMenuAuthoritySearchRoleByEmp", { empID : empid });
			roleData.done(function(datarole){
				srcrole.localdata = datarole.returnArray;
				adarole = new $.jqx.dataAdapter(srcrole);
				var $gridRole = $('#gridRole');
				$gridRole.jqxGrid('clearselection');
				$gridRole.jqxGrid({ source: adarole });
			});
		});
	});
}
</script>
</script>
<style type="text/css">
#jqxTree01 .jqx-widget-content-blueish-system {
	background:#eee;
}
.jqx-icon-arrow-down-blueish-system, .jqx-icon-arrow-down-blueish-system {
	background: url(../../resources/css/images/img_icons/simple-blue-downarrow.png) center no-repeat !important;
}
.jqx-icon-arrow-up-blueish-system, .jqx-icon-arrow-up-blueish-system {
	background: url(../../resources/css/images/img_icons/simple-blue-uparrow.png) center no-repeat !important;
}
.jqx-grid-header-empty-style {
   	display : none !important;
}
#treeGridMenuList .jqx-grid-header-blueish {
	display : none !important;
}
/*추가*/
.i-DQ .label2-bg {
	background: url(../../resources/css/images/img_icons/icon_sqr_arrow_blue.png) left 7px no-repeat !important;
	font-size: 12px !important;
	padding-left: 15px;
	padding-top: 0px;
	line-height: 24px !important;
	color: #323232 !important;
	margin-right: 2px;
	font-weight: bold;
}
.i-DQ .label3-bg {
	background: none !important;
	font-size: 12px !important;
	padding-left: 5px;
	padding-top: 0px;
	line-height: 24px !important;
	color: #3972b4 !important;
	margin-right: 0px;
	font-weight: bold;
}
/*content > input */
.code_box_input {
	height: 19px;
	float: left;
	margin-right: 0px;
	margin-top: 0px;
	margin-left: 0px;
}
.code_box_input input.input_stA {
	width: 100%;
	background: #ffffff;
	border: 1px solid #4271ac;
	border-radius: 3px;
	-moz-border-radius: 3px;
	-ms-border-radius: 3px;
	-o-border-radius: 3px;
	-webkit-border-radius: 3px;
	text-indent: 5px;
	font-size: 12px;
	color: #303030;
	padding-bottom: 2px;
}
/*content > combobox*/
.con_combo {
	border-color: #4271ac !important;
}
.con_combo .jqx-combobox-state-normal {
	border: 1px solid #4271ac !important;
	border-radius: 3px;
	-ms-border-radius: 3px;
	-o-border-radius: 3px;
	-webkit-order-radius: 3px;
	-moz-border-radius: 3px;
	border-color: #43536A !important;
}
.con_combo .jqx-combobox-arrow-normal, .con_combo .jqx-action-button {
	background: #ffffff;
	border-color: #ffffff !important;
}
.con_combo .jqx-combobox-content {
	background: #ffffff;
	border-color: #ffffff !important;
}
.con_combo .jqx-combobox-input {
	color: #171819;
	font-size: 12px;
	text-shadow: none;
	background-color: #ffffff;
	background-image: none;
	text-indent: 5px;
	vertical-align: middle !important;
	margin-top: 0px !important;
}
.con_combo .jqx-icon-arrow-down {
	background: url(../../resources/css/images/img_icons/simple-arrow-down-icon.png) no-repeat center !important;
}
.jqx-tree-grid-icon-empty-style, .jqx-tree-grid-icon-size-empty-style {
	width: auto !important;
	height: auto !important;
	margin-left: 2px !important;
}
.treegrid .jqx-cell-empty-style {
	padding: 3px 4px !important;
}
</style>
</head>
<body class="default i-DQ">
	<div class="wrap" style="width: 98% !important; margin: 0 10px !important; min-width: 1040px;">
		<div class="contents" style="width: 100%; margin: 10px 0 !important;">
			<div class="tab_Area" style="width: 100%; height: 690px; float: left; margin-top: 0px;">
				<div id="jqxWidget" style="height: 100%;">
					<div id="tabUserpermission">
						<ul>
							<li style="margin-left: 0px;">사용자</li>
							<li onclick="tab2Search();">보고서</li>
						</ul>
						<div style="height: 650px !important; background: #fff; border-top: 1px solid #91A3B4; position: relative; overflow: hidden;">
							<div class="tab_con" style="width: 100%; height: 100%; margin: 0px 0%; background: #fff;">
								<div class="divi" style="float: left; width: 28%; margin-left: 1%;">
									<div class="iwidget_label" style="margin: 10px 0 0 0 !important; width: 100%; float: left;">
										<div class="label3-bg" style="float: left; margin: 0px; line-height: 19px;">구분</div>
										<div class="con_combo" style="float: left; margin: 0 10px; margin-left: 5px;" id="cboSearchgb"></div>
										<div class="code_box_input" style="float: left;">
											<input type="text" id="txtSearchval" name="txtSearchval" class="input_stA" style="width: 130px;" value="" onkeypress="if(event.keyCode==13) {searchStart(); return false;}"/>
											<a href="javascript:searchStart();"><img src="../../resources/css/images/img_icons/search_btn.png" alt="search" title="검색" style="margin: 0; padding: 0;" /></a>			
										</div>
									</div>
									<!--//iwidget_label-->
									<div class="iwidget_grid" style="float: left; margin: 10px 0 !important; padding: 0 !important; width: 100%; height: 597px;">
										<div id="gridUser"></div>
									</div>
									<!--//iwidget_grid -->
								</div>
								<!--//divi-->
								<div class="divi" style="float: right; width: 68%; padding: 0%; margin: 0px 1%; background: #fff;">
									<div class="iwidget_label" style="margin: 20px 0 0 0; width: 100%; padding: 5px 0; float: left; background: #f1f7ff; border: 1px solid #e5effb;">
										<div class="label-bg label-2" style="float: left; margin-left: 10px;">
											사용자:<span class="colorchange" id="username"></span>
										</div>
									</div>
									<!--//iwidget_label-->
									<div style="width: 100%; float: right; margin: 10px 0;">
										<div class="divi" style="float: left; width: 68%; background: #fff; padding: 0 1% 0 0;">
											<div class="iwidget_label" style="margin: 0 !important; width: 100%; height: 20px; float: left;">
												<div class="label2-bg" style="float: left; width: 100%;">권한이 부여된 보고서</div>
											</div>
											<!--//iwidget_label-->
											<div class="iwidget_grid" style="float: left; margin: 10px 0 !important; padding: 0 !important; width: 100%; height: 545px;">
												<div id="treegridUserpermission" class="treegrid"></div>
											</div>
											<!--//iwidget_grid -->
										</div>
										<!--//divi-->
										<div class="divi" style="float: right; width: 30%; margin: 0; background: #fff; padding: 0 0; margin-top: 10px;">
											<div class="divi" style="float: left; width: 100%; background: #fff; padding: 0px; margin-right: 2%;">
												<div class="iwidget_label" style="margin: 0 !important; width: 100%; height: 20px; float: left;">
													<div class="label2-bg" style="float: left; width: 100%;">사용자가 속한 그룹</div>
												</div>
												<!--//iwidget_label-->
												<div class="iwidget_grid" style="float: left; margin: 10px 0 !important; padding: 0 !important; width: 100%; height: 245px;">
													<div id="gridGroup"></div>
												</div>
												<!--//iwidget_grid -->
											</div>
											<!--//divi-->
											<div class="divi" style="float: right; width: 100%; background: #fff; padding: 0px;">
												<div class="iwidget_label" style="margin: 0 !important; width: 100%; height: 20px; float: left;">
													<div class="label2-bg" style="float: left; width: 100%;">사용자가 속한 Role</div>
												</div>
												<!--//iwidget_label-->
												<div class="iwidget_grid" style="float: left; margin: 10px 0 !important; padding: 0 !important; width: 100%; height: 245px;">
													<div id="gridRole"></div>
												</div>
												<!--//iwidget_grid -->
											</div>
											<!--//divi-->
										</div>
										<!--//divi-->
									</div>
								</div>
								<!--//divi-->
							</div>
							<!--//tab_con-->
						</div>
						<div style="height: 650px !important; background: #fff; border-top: 1px solid #91A3B4; position: relative; overflow: hidden;">
							<div class="divi" style="float: left; width: 28%; margin-left: 1%;">
								<!--//iwidget_label-->
								<div class="iwidget_grid" style="float: left; margin: 10px 0 !important; padding: 0 !important; width: 100%; height: 625px;">
									<div style=" padding-top:0px;background:#fff; width:100%; margin-left:0%; height:624px !important; overflow:auto;" id='treeGridMenuList' class='treegrid'></div>
								</div>
								<!--//iwidget_grid -->
							</div>
							<div class="divi" style="float: right; width: 68%; padding: 0%; margin: 0px 1%; background: #fff;">
								<div class="iwidget_label" style="margin: 20px 0 0 0; width: 100%; padding: 5px 0; float: left; background: #f1f7ff; border: 1px solid #e5effb;">
									<div class="label-bg label-2" style="float: left; margin-left: 10px;">
										보고서:<span class="colorchange" id="reportName"></span>
									</div>
								</div>
								<!--//iwidget_label-->
								<div style="width: 100%; float: right; margin: 10px 0;">
									<div class="divi" style="float: left; width: 68%; background: #fff; padding: 0 1% 0 0;">
										<div class="iwidget_label" style="margin: 0 !important; width: 100%; height: 20px; float: left;">
											<div class="label2-bg" style="float: left; width: 100%;">사용자</div>
										</div>   
										<!--//iwidget_label-->
<!-- 										<div class="iwidget_grid" style="float: left; margin: 10px 0 !important; padding: 0 !important; width: 100%; height: 545px;"> -->
<!-- 												<div id="treegridUserpermission" class="treegrid"></div> -->
<!-- 											</div> -->
										<div class="iwidget_grid" style="float: left; margin: 10px 0 !important; padding: 0 !important; width: 100%; height: 545px;">
											<div id="gridReportByUser" class="treegrid"></div>
										</div>
										<!--//iwidget_grid -->
									</div>
									<!--//divi-->
									<div class="divi" style="float: right; width: 30%; margin: 0; background: #fff; padding: 0 0; margin-top: 10px;">
										<div class="divi" style="float: left; width: 100%; background: #fff; padding: 0px; margin-right: 2%;">
											<div class="iwidget_label" style="margin: 0 !important; width: 100%; height: 20px; float: left;">
												<div class="label2-bg" style="float: left; width: 100%;">사용자가 속한 그룹</div>
											</div>
											<!--//iwidget_label-->
											<div class="iwidget_grid" style="float: left; margin: 10px 0 !important; padding: 0 !important; width: 100%; height: 245px;">
												<div id="gridReportGroup" calss="treegrid"></div>
											</div>
											<!--//iwidget_grid -->
										</div>
										<!--//divi-->
										<div class="divi" style="float: right; width: 100%; background: #fff; padding: 0px;">
											<div class="iwidget_label" style="margin: 0 !important; width: 100%; height: 20px; float: left;">
												<div class="label2-bg" style="float: left; width: 100%;">사용자가 속한 Role</div>
											</div>
											<!--//iwidget_label-->
											<div class="iwidget_grid" style="float: left; margin: 10px 0 !important; padding: 0 !important; width: 100%; height: 245px;">
												<div id="gridReportRole"></div>
											</div>
											<!--//iwidget_grid -->
										</div>
										<!--//divi-->
									</div>
									<!--//divi-->
								</div>
							</div>
						</div>
						<!--//tab1-->
					</div>
					<!--//jqxTabs-->
				</div>
				<!--//jqxWidget-->
			</div>
			<!--//tab_Area-->
		</div>
		<!--//contents-->
	</div>
	<!--//wrap-->
</body>
</html>