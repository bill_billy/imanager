<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>${title}</title>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/css/iplanbiz/styles/table.css" />
	    <link rel="stylesheet" type="text/css" href="../../resources/css/iplanbiz/styles/jqx.blueish-system.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/css/iplanbiz/styles/jqx.simple-gray.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/css/iplanbiz/styles/jqx.empty-style.css"/>
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_secure.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
	    <script src="../../resources/js/util/remoteInfo.js"></script>
		<script type="text/javascript">
			var _flagreportuser_  = true; // 보고서 > 사용자
			var _flagreportgroup_ = true; // 보고서 > 그룹
			var _flagreportrole_  = true; // 보고서 > 롤
			var _flaggroupuser_   = true; // 그룹 > 사용자
			var _flaggrouprole_   = true; // 그룹 > 선택롤
			var _flaggrouproleNo_ = true; // 그룹 > 전체롤
			var _flagrolegroup_   = true; // 롤 > 선택그룹
			var _flagrolegroupNo_ = true; // 롤 > 전체그룹
			var _flagusergroup_   = true; // 사용자 > 그룹
			var _flaguserrole_    = true; // 사용자 > 롤
			
			var _flagdatauser_    = true; // 데이타 > 사용자
			var _flagdatagroup_   = true; // 데이타 > 그룹
			var _flagdatarole_    = true; // 데이타 > 롤
			
			var remoteIP = '<%=request.getRemoteAddr() %>';
			var remoteOS = getOSInfoStr();
			var remoteBrowser = getBrowserName();
				
			// jqxgrid 렌더러
			var defulatrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
				var newValue = $i.secure.scriptToText(value);
				var margin = '';
				var cellsalign = columnproperties.cellsalign;
				
				if(cellsalign == 'center') {
					margin = 'margin:6px  0px 0px  0px;';
				} else if(cellsalign == 'left') {
					margin = 'margin:6px  0px 0px 10px;';
				} else if(cellsalign == 'right') {
					margin = 'margin:6px 10px 0px  0px;';
				}
				return '<div style="text-align:'+columnproperties.cellsalign+'; '+margin+'">'+newValue+'</div>';
			};
			// jqxgrid underline 렌더러
			var underlinerenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
				var newValue = $i.secure.scriptToText(value);
				var margin = '';
				var cellsalign = columnproperties.cellsalign;
				
				if(cellsalign == 'center') {
					margin = 'margin:6px  0px 0px  0px;';
				} else if(cellsalign == 'left') {
					margin = 'margin:6px  0px 0px 10px;';
				} else if(cellsalign == 'right') {
					margin = 'margin:6px 10px 0px  0px;';
				}
				return '<div style="text-align:'+columnproperties.cellsalign+'; '+margin+'; text-decoration: underline;">'+newValue+'</div>';
			};
			
			// jqxgrid 사용자 소스
			var usergridsource = {
				datatype: 'json',
				datafields: [
					{ name: 'EMP_ID', type: 'string'},
					{ name: 'EMP_NM', type: 'string'},
					{ name: 'USER_GRP_NM', type: 'string'}
				],
				localdata: {},
				updaterow: function (rowid, rowdata, commit) {
					commit(true);
				}
			};
			// jqxgrid 그룹 소스
			var groupgridsource = {
				datatype: 'json',
				datafields: [
					{ name: 'GID', type: 'string'},
					{ name: 'GROUP_NAME', type: 'string'},
					{ name: 'GROUP_CHECK', type: 'string'}
				],
				localdata: {},
				updaterow: function (rowid, rowdata, commit) {
					commit(true);
				}
			};
			// jqxgrid 롤 소스
			var rolegridsource = {
				datatype: 'json',
				datafields: [
					{ name: 'GID', type: 'string'},
					{ name: 'GROUP_NAME', type: 'string'},
					{ name: 'GID_CHECK', type: 'string'}
				],
				localdata: {},
				updaterow: function (rowid, rowdata, commit) {
					commit(true);
				}
			};
			var datagridsource = {
				datatype:'json',
				datafields:[
					{ name : 'BID'          , type : 'string'},
					{ name : 'BUSINESS_NAME', type : 'string'}
				],
				localdata : {},
				updaterow : function (rowid, rowdata, commit) {
					commit(true);
				}
			};
			$(document).ready(function() {
				init();
			});
			function init(){
				$i.dialog.setAjaxEventPopup("SYSTEM","데이터를 불러오고 있습니다.");
				$('#jqxTabs').jqxTabs({ width: '100%', height: '100%', position: 'top', theme:'blueish-system', selectedItem: 0});
				//Role 관련 버튼
				$("#btnRoleNameReset").jqxButton({ width: '',  theme:'simple-gray'});
				$("#btnRoleNameSave").jqxButton({ width: '',  theme:'simple-gray'});
				$("#btnRoleNameDelete").jqxButton({ width: '',  theme:'simple-gray'});
				//Data 관련 버튼
				$("#btnDataNameReset").jqxButton({ width: '',  theme:'simple-gray'});
				$("#btnDataNameSave").jqxButton({ width: '',  theme:'simple-gray'});
				$("#btnDataNameDelete").jqxButton({ width: '',  theme:'simple-gray'});
				
				$("#btnRoleInsert").jqxButton({ width: "", theme:"blueish-system"});
				$("#btnGroupInsert").jqxButton({ width : "", theme:"blueish-system"}); 
				$("#btnMenuInsert").jqxButton({ width : "", theme:"blueish-system"});
				$("#btnDataInsert").jqxButton({ width : "", theme:"blueish-system"});
				
				//권한적용 button
				$("[name='btnApply']").jqxButton({ width: '', theme :'blueish-system'});
				$("[name='btnCancel']").jqxButton({ width: '', theme :'blueish-system'});
				searchCombo();
			}
			function roleNameInsert(){
				if($("#txtAddRoleName").val().trim() == ""){
					$i.dialog.error("SYSTEM", "Role 명을 입력하세요");
				}else{
					$("#hiddenRoleID").val($("#hiddenAddRoleID").val());
					$("#hiddenRoleName").val($("#txtAddRoleName").val());
					$("#hiddenRoleNameOrg").val($("#hiddenAddRoleName").val());
					
					$("#hiddenRemoteIP01").val(remoteIP);
			        $("#hiddenRemoteOS01").val(remoteOS);
			        $("#hiddenRemoteBW01").val(remoteBrowser);
					$i.post("./insertRoleInfo","#roleInfoSaveForm").done(function(data){
						if(data.returnCode == "EXCEPTION"){
							$i.dialog.error("SYSTEM", data.returnMessage);
						}else{
							$i.dialog.alert("SYSTEM", data.returnMessage, function(){
								makeRoleData();
								roleNameReset(); 
							});
						}
					});	
				}
				
			}
			function roleNameDelete(){
				if($("#hiddenAddRoleID").val() == ""){
					$i.dialog.error("SYSTEM", "삭제할 Role을 선택하세요");
				}else{
					$("#hiddenRoleID").val($("#hiddenAddRoleID").val());
					$("#hiddenRoleName").val($("#hiddenAddRoleName").val());
					
					$("#hiddenRemoteIP01").val(remoteIP);
			        $("#hiddenRemoteOS01").val(remoteOS);
			        $("#hiddenRemoteBW01").val(remoteBrowser);
					$i.dialog.confirm("SYSTEM", "삭제하시겠습니까?", function(){
						$i.post("./deleteRoleInfo","#roleInfoSaveForm").done(function(data){
							if(data.returnCode == "EXCEPTION"){
								$i.dialog.error("SYSTEM", data.returnMessage);
							}else{
								$i.dialog.alert("SYSTEM", data.returnMessage, function(){
									makeRoleData();
									roleNameReset();
									
								});
							}
						});	
					});	
				}
			}
			function roleNameReset(){
				$("#hiddenRoleID").val("");
				$("#hiddenRoleName").val("");
				$("#hiddenAddRoleID").val("");
				$("#hiddenAddRoleName").val("");
				$("#txtAddRoleName").val("");
				$("#hiddenRoleNameOrg").val("");
			}
			function roleMappingInsert(){
				if($("#hiddenAddRoleID").val() == ""){
					$i.dialog.error("SYSTEM", "Role을 선택해주세요");
					return false;
				}
				var menuCheckValue = $("#treegridRoleMenuList").jqxTreeGrid("getCheckedRows");
				var userCheckValue = $("#gridRoleUserListSelect").jqxGrid("getRows");
				var groupCheckValue = $("#gridRoleUserGrpListSelect").jqxGrid("getRows");
				var menuInputBox = "";
				var userInputBox = "";
				var groupInputBox = "";
				
				for(var i=0;i<menuCheckValue.length;i++){
					menuInputBox += "<input type='hidden' name='menuCheckValue' value='"+menuCheckValue[i].C_ID+"' />";
					menuInputBox += "<input type='hidden' name='menuCheckName' value='"+menuCheckValue[i].C_NAME+"' />";
				}
				for(var j=0;j<userCheckValue.length;j++){
					menuInputBox += "<input type='hidden' name='userCheckValue' value='"+userCheckValue[j].EMP_ID+"' />";
					menuInputBox += "<input type='hidden' name='userCheckName' value='"+userCheckValue[j].EMP_NM+"' />";
				}
				for(var k=0;k<groupCheckValue.length;k++){
					menuInputBox += "<input type='hidden' name='groupCheckValue' value='"+groupCheckValue[k].USER_GRP_ID+"' />";
					menuInputBox += "<input type='hidden' name='groupCheckName' value='"+groupCheckValue[k].USER_GRP_NM+"' />";
				}
				$("#insertRoleMapping").append(menuInputBox);
				$("#mappingRoleID").val($("#hiddenAddRoleID").val()); 
				$("#mappingRoleName").val($("#hiddenAddRoleName").val()); 
				
				$("#hiddenRemoteIP02").val(remoteIP);
		        $("#hiddenRemoteOS02").val(remoteOS);
		        $("#hiddenRemoteBW02").val(remoteBrowser);
				$i.post("./insertMappingRole","#insertRoleMapping").done(function(data){
					if(data.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", data.returnMessage);
						$("#insertRoleMapping").find("[name='menuCheckValue']").remove();
						$("#insertRoleMapping").find("[name='userCheckValue']").remove();
						$("#insertRoleMapping").find("[name='groupCheckValue']").remove();
						$("#insertRoleMapping").find("[name='menuCheckName']").remove();
						$("#insertRoleMapping").find("[name='userCheckName']").remove();
						$("#insertRoleMapping").find("[name='groupCheckName']").remove();
					}else{
						$i.dialog.alert("SYSTEM", data.returnMessage, function(){
							$("#txtRoleUserSearchSelectNo").val("");
							$("#txtRoleUserSearchSelect").val("");
							$("#txtRoleGroupSearchSelectNo").val("");
							$("#txtRoleGroupSearchSelect").val("");
							roleNameReset();
							makeRoleData();
							$("#insertRoleMapping").find("[name='menuCheckValue']").remove();
							$("#insertRoleMapping").find("[name='userCheckValue']").remove();
							$("#insertRoleMapping").find("[name='groupCheckValue']").remove();
							$("#insertRoleMapping").find("[name='menuCheckName']").remove();
							$("#insertRoleMapping").find("[name='userCheckName']").remove();
							$("#insertRoleMapping").find("[name='groupCheckName']").remove();
							_flagrolegroup_   = true;
							_flagrolegroupNo_ = true;
						});
					}
				});
			}
			function groupMappingInsert(){
				if($("#hiddenSavePid").val() == ""){
					$i.dialog.error("SYSTEM", "그룹명을 선택하세요");
					return false;
				}
				var menuCheckValue = $("#treegridGroupMenuList").jqxTreeGrid("getCheckedRows");
				var roleCheckValue = $("#gridGroupRoleSelect").jqxGrid("getRows");
				
				var menuInputBox = "";
				for(var i=0;i<menuCheckValue.length;i++){
					menuInputBox += "<input type='hidden' name='menuCheckValue' value='"+menuCheckValue[i].C_ID+"' />";
					menuInputBox += "<input type='hidden' name='menuCheckName' value='"+menuCheckValue[i].C_NAME+"' />";
				}
				for(var k=0;k<roleCheckValue.length;k++){
					menuInputBox += "<input type='hidden' name='roleCheckValue' value='"+roleCheckValue[k].GID+"' />";
					menuInputBox += "<input type='hidden' name='roleCheckName' value='"+roleCheckValue[k].GROUP_NAME+"' />";
				}
				$("#insertGroupMapping").append(menuInputBox);
				$("#mappingGroupID").val($("#hiddenSavePid").val()); 
				$("#mappingGroupName").val($("#hiddenAddGroupName").val()); 
				
				$("#hiddenRemoteIP03").val(remoteIP);
		        $("#hiddenRemoteOS03").val(remoteOS);
		        $("#hiddenRemoteBW03").val(remoteBrowser);
				$i.post("./insertMappingGroup","#insertGroupMapping").done(function(data){
					if(data.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", data.returnMessage);
						$("#insertGroupMapping").find("[name='menuCheckValue']").remove();
						$("#insertGroupMapping").find("[name='roleCheckValue']").remove();
						$("#insertGroupMapping").find("[name='menuCheckName']").remove();
						$("#insertGroupMapping").find("[name='roleCheckName']").remove();
					}else{
						$i.dialog.alert("SYSTEM", data.returnMessage, function(){
							$("#txtGroupSearch").val("");
							$("#txtGroupUserSearch").val("");
							$("#txtGroupRoleSearchSelectNo").val("");
							$("#txtGroupRoleSearchSelect").val("");
							makeGroupData();
							$("#insertGroupMapping").find("[name='menuCheckValue']").remove();
							$("#insertGroupMapping").find("[name='roleCheckValue']").remove();
							$("#insertGroupMapping").find("[name='menuCheckName']").remove();
							$("#insertGroupMapping").find("[name='roleCheckName']").remove();
							_flaggrouprole_   = true;
							_flaggrouproleNo_ = true;
						});
					}
				});
			}
			function menuMappingInsert(){
				if($("#hiddenSaveCid").val() == ""){
					$i.dialog.error("SYSTEM", "보고서를 선택하세요");
					return false;
				}
				var userCheckValue = $("#gridMenuUserListSelect").jqxGrid("getRows");
				var groupCheckValue = $("#gridMenuGroupListSelect").jqxGrid("getRows");
				var roleCheckValue = $("#gridMenuRoleListSelect").jqxGrid("getRows");
				var menuInputBox = "";
				for(var i=0;i<userCheckValue.length;i++){
					menuInputBox += "<input type='hidden' name='userCheckValue' value='"+userCheckValue[i].EMP_ID+"' />";
					menuInputBox += "<input type='hidden' name='userCheckName' value='"+userCheckValue[i].EMP_NM+"' />";
				}
				for(var j=0;j<groupCheckValue.length;j++){
					menuInputBox += "<input type='hidden' name='groupCheckValue' value='"+groupCheckValue[j].USER_GRP_ID+"' />";
					menuInputBox += "<input type='hidden' name='groupCheckName' value='"+groupCheckValue[j].USER_GRP_NM+"' />";
				}
				for(var k=0;k<roleCheckValue.length;k++){
					menuInputBox += "<input type='hidden' name='roleCheckValue' value='"+roleCheckValue[k].GID+"' />";
					menuInputBox += "<input type='hidden' name='roleCheckName' value='"+roleCheckValue[k].GROUP_NAME+"' />";
				}
				$("#insertMenuMapping").append(menuInputBox);
				$("#mappingMenuID").val($("#hiddenSaveCid").val()); 
				$("#mappingMenuName").val($("#hiddenAddMenuName").val());
				
				$("#hiddenRemoteIP04").val(remoteIP);
		        $("#hiddenRemoteOS04").val(remoteOS);
		        $("#hiddenRemoteBW04").val(remoteBrowser);
				$i.post("./insertMappingMenu","#insertMenuMapping").done(function(data){
					if(data.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", data.returnMessage);
						$("#insertMenuMapping").find("[name='userCheckValue']").remove();
						$("#insertMenuMapping").find("[name='groupCheckValue']").remove();
						$("#insertMenuMapping").find("[name='roleCheckValue']").remove();
						$("#insertMenuMapping").find("[name='userCheckName']").remove();
						$("#insertMenuMapping").find("[name='groupCheckName']").remove();
						$("#insertMenuMapping").find("[name='roleCheckName']").remove();
					}else{
						$i.dialog.alert("SYSTEM", data.returnMessage, function(){
							$("#txtMenuUserSearchSelectNo").val("");
							$("#txtMenuUserSearchSelect").val("");
							$("#txtMenuGroupSearchNoSelect").val("");
							$("#txtMenuGroupSearchSelect").val("");
							$("#txtMenuRoleSearchSelect").val("");
							$("#txtMenuRoleSearchNoSelect").val("");
							makeMenuData();
							$("#insertMenuMapping").find("[name='userCheckValue']").remove();
							$("#insertMenuMapping").find("[name='groupCheckValue']").remove();
							$("#insertMenuMapping").find("[name='roleCheckValue']").remove();
							$("#insertMenuMapping").find("[name='userCheckName']").remove();
							$("#insertMenuMapping").find("[name='groupCheckName']").remove();
							$("#insertMenuMapping").find("[name='roleCheckName']").remove();
						});
					}
				});
			}
			function searchCombo(){
				var dat  = [
					{value:'1',text:'이름'},
					{value:'2',text:'사번'} 
				];
				var source = {
					datatype: "json",
					datafields: [
						{ name: 'value' },
						{ name: 'text' }
					],
					id: 'id',
					localdata:dat,
					async: false  
				};
				var dataAdapter = new $.jqx.dataAdapter(source);
				//Role 사용자 Combobox
				$("#cboRoleSearch").jqxComboBox({selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "text", valueMember: "value", dropDownWidth: 60, dropDownHeight: 80, width: 60, height: 19, theme:'blueish-system'});
				$("#cboRoleSearchSelect").jqxComboBox({selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "text", valueMember: "value", dropDownWidth: 60, dropDownHeight: 80, width: 60, height: 19, theme:'blueish-system'});
				
				//보고서 사용자 Combobox
				$("#cboMenuSearchNoSelect").jqxComboBox({selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "text", valueMember: "value", dropDownWidth: 60, dropDownHeight: 80, width: 60, height: 19, theme:'blueish-system'});
				$("#cboMenuSearchSelect").jqxComboBox({selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "text", valueMember: "value", dropDownWidth: 60, dropDownHeight: 80, width: 60, height: 19, theme:'blueish-system'});
				
				//데이터 사용자 Combobox
				$("#cboDataSearchNoSelect").jqxComboBox({selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "text", valueMember: "value", dropDownWidth: 60, dropDownHeight: 80, width: 60, height: 19, theme:'blueish-system'});
				$("#cboDataSearchSelect").jqxComboBox({selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "text", valueMember: "value", dropDownWidth: 60, dropDownHeight: 80, width: 60, height: 19, theme:'blueish-system'});
				
				$("#cboGroupSearch").jqxComboBox({selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "text", valueMember: "value", dropDownWidth: 60, dropDownHeight: 80, width: 60, height: 19, theme:'blueish-system'});
				var grpData = $i.post("./getGrpTypList",{});
				grpData.done(function(data){ 
					var cbosource = {
						datatype: "json",
						datafields: [
							{ name: 'GRP_TYP' },
							{ name: 'GRP_TYP_NM' }
						], 
						id: 'GRP_TYP', 
						localdata:data.returnObject.returnArray,
						async: false
					};
					var cbodataAdapter = new $.jqx.dataAdapter(cbosource);
					
					$("#cboGrpTyp").jqxComboBox({selectedIndex: 0, source: cbodataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "GRP_TYP_NM", valueMember: "GRP_TYP", dropDownWidth: 60, dropDownHeight: 80, width: 60, height: 19, theme:'blueish-system'});
					$("#cboGrpTypByReportNoSelect").jqxComboBox({selectedIndex: 0, source: cbodataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "GRP_TYP_NM", valueMember: "GRP_TYP", dropDownWidth: 60, dropDownHeight: 80, width: 60, height: 19, theme:'blueish-system'});
					$("#cboGrpTypByReportSelect").jqxComboBox({selectedIndex: 0, source: cbodataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "GRP_TYP_NM", valueMember: "GRP_TYP", dropDownWidth: 60, dropDownHeight: 80, width: 60, height: 19, theme:'blueish-system'});
					
					//Role Tab
					$("#cboGrpTypByRoleSelect").jqxComboBox({selectedIndex: 0, source: cbodataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "GRP_TYP_NM", valueMember: "GRP_TYP", dropDownWidth: 60, dropDownHeight: 80, width: 60, height: 19, theme:'blueish-system'});
					$("#cboGrpTypByRoleNoSelect").jqxComboBox({selectedIndex: 0, source: cbodataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "GRP_TYP_NM", valueMember: "GRP_TYP", dropDownWidth: 60, dropDownHeight: 80, width: 60, height: 19, theme:'blueish-system'});
					makeRoleData();
				});
			}
			<%-- 롤 탭 : 좌측 롤 리스트 --%>
			function makeRoleData(){
				$("#hiddenSaveGid").val('');
				var roleData=$i.post("./getRoleList",{groupName:$("#txtRoleSearch").val()});
				roleData.done(function(role){
					rolegridsource.localdata = role.returnArray;
					
					var dataAdapter = new $.jqx.dataAdapter(rolegridsource);
					
		            $("#gridRoleList").jqxGrid(
		            {
   	              	    width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'simple-gray',  
						sortable: false,
		                columnsresize: true,
		                columns: [
                            { text: 'Role명', datafield: 'GROUP_NAME', width: '100%', align:'center', cellsalign: 'left'
								, renderer: function (text, align, height) {
									return "<div style=' margin-top:4px; text-align:center;'><img width='16' height='16' style='' src='../../resources/css/images/img_icons/icon_role.png'/><span style='margin-left: 4px; '>Role명</span><div style='clear: both;'></div></div>";
								}, cellsrenderer: underlinerenderer
							}
						]
		            });
		            $('#gridRoleList').on('rowselect', function (event){
		            	var args = event.args;
					    var rowBoundIndex = args.rowindex;
					    var rowData = args.row;
					    _flagrolegroup_   = true; // 롤 > 그룹
					    _flagrolegroupNo_ = true;
						var gid = args.row.GID;
						$("#hiddenAddRoleID").val(gid);
						$("#hiddenAddRoleName").val(args.row.GROUP_NAME);
						$("#txtAddRoleName").val(args.row.GROUP_NAME);
						$("#labelRoleName").html($i.secure.scriptToText(args.row.GROUP_NAME));
						makeRoleDetailData(gid);
						makeRoleTabGrid(gid);
						$("#hiddenSaveGid").val(gid);
						
					});
		            $("#hiddenAddRoleID").val("");
		            $("#hiddenAddRoleName").val("");
		            $("#txtAddRoleName").val("");
		            $("#labelRoleName").html("");
		            $("#gridRoleList").jqxGrid('clearselection');
		            makeRoleDetailData('');
		            makeRoleTabGrid('');
				});
			}
			function makeRoleDetailData(fkey){
				var treegridRoleData = $i.post("./getMenuAuthorityTreeList", {fkey:'', type:'g'});
				treegridRoleData.done(function(data){
					if(data.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", data.returnMessage);
					}else{
						drawTreeTabByRole_User(data.returnArray,"treegridRoleMenuList",5999,fkey,'g');
					}
				});
			}
			<%-- 롤 탭 그룹 그리드 --%>
			function makeRoleTabGrid(gId){
				roleUserGrid(gId); 
				roleUserNoGrid(gId);
				roleGroupGrid(gId);
				roleGroupNoGrid(gId);
			}
			function roleUserNoGrid(gId){
				if(gId == ""){
					gId = $("#hiddenSaveGid").val();
				}
				var empID = "";
				var empNm = "";
				var isNull = "";
				if($("#txtRoleUserSearchSelectNo").val() == ""){
					isNull = "";
				}else{
					if($("#cboRoleSearch").val() == "1"){
						empNm = $("#txtRoleUserSearchSelectNo").val();
					}else{
						empID = $("#txtRoleUserSearchSelectNo").val();	
					}
					isNull = "NOTNULL";
				}
				$("#gridRoleUserListNoSelect").jqxGrid('unselectrow');
				var gridRoleUserListNoSelect = $i.post("./getMenuAuthorityUserListNoByRole",{empID:empID, empName:empNm, gID:gId, isNull:isNull});
				gridRoleUserListNoSelect.done(function(userData){        
					usergridsource.localdata = userData.returnArray;
			        var dataAdapter = new $.jqx.dataAdapter(usergridsource);
			            
		            $("#gridRoleUserListNoSelect").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish-system',
						sortable: true,
		                columnsresize: true,
		             	editable: true,
		             	selectionmode: 'multiplerows',
		                columns: [
						    { text: '사번', datafield: 'EMP_ID', width: 96, align:'center', cellsalign: 'center', editable: false ,cellsrenderer: defulatrenderer},
		                    { text: '이름', datafield: 'EMP_NM', width: 96, align:'center', cellsalign: 'center', editable: false ,cellsrenderer: defulatrenderer},
						    { text: '부서', datafield: 'USER_GRP_NM', align:'center', cellsalign: 'left', cellsrenderer: defulatrenderer, editable: false}
		                ]
		            });
		            $("#gridRoleUserListNoSelect").jqxGrid('clearselection');
				});
			}
			function roleUserGrid(gId){
				if(gId == ""){
					gId = $("#hiddenSaveGid").val();
				}
				var empID = "";
				var empNm = "";
				var isNull = "";
				if($("#cboRoleSearch").val() == "1"){
					empNm = $("#txtRoleUserSearchSelect").val();
				}else{
					empID = $("#txtRoleUserSearchSelect").val();
				}
				isNull = "NOTNULL";
// 				if($("#txtRoleUserSearchSelect").val() == ""){
// 					isNull = "";
// 				}else{
// 					isNull = "NOTNULL";
// 				}
				$("#gridRoleUserListSelect").jqxGrid('unselectrow');
				var gridRoleUserListSelect = $i.post("./getMenuAuthorityUserListByRole",{empID:empID, empName:empNm, gID:gId, isNull:isNull});
				gridRoleUserListSelect.done(function(userData){
					usergridsource.localdata = userData.returnArray;
			        var dataAdapter = new $.jqx.dataAdapter(usergridsource);
			            
		            $("#gridRoleUserListSelect").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish-system',
						sortable: true,
		                columnsresize: true,
		             	editable: true,
		             	selectionmode: 'multiplerows',
		                columns: [
						    { text: '사번', datafield: 'EMP_ID', width: 96, align:'center', cellsalign: 'center', editable: false ,cellsrenderer: defulatrenderer},
		                    { text: '이름', datafield: 'EMP_NM', width: 96, align:'center', cellsalign: 'center', editable: false ,cellsrenderer: defulatrenderer},
						    { text: '부서', datafield: 'USER_GRP_NM', align:'center', cellsalign: 'left', cellsrenderer: defulatrenderer, editable: false}
		                ]
		            });
		            $("#gridRoleUserListSelect").jqxGrid('clearselection');
				});
			}
			function roleGroupNoGrid(gId){
				if(gId == ""){
					gId = $("#hiddenSaveGid").val();
				}
				$("#gridRoleUserGrpListNoSelect").jqxGrid('unselectrow');
				if(_flagrolegroupNo_) {
					var gridRoleList = $i.post("./getGroupListNotInRole", {gID:gId, userGrpName:'', grpTyp:""});
					gridRoleList.done(function(roleData){
						var source = {
							datatype: 'json',
			                datafields: [
			                    { name: 'USER_GRP_ID', type: 'string'},
			                    { name: 'USER_GRP_NM', type: 'string'},
			                    { name: 'GRP_TYP', type: 'string'},
			                    { name: 'GRP_TYP_NM', type:'string'}
			                ],
			                localdata: roleData.returnArray,
			                updaterow: function (rowid, rowdata, commit) {
			                    commit(true);
			                }
		                };
						var dataAdapter = new $.jqx.dataAdapter(source);
						
			            $("#gridRoleUserGrpListNoSelect").jqxGrid({
			              	width: '100%',
			                height:'100%',
							altrows:true,
							pageable: false,
			                source: dataAdapter,
							theme:'blueish-system',
							sortable: false,
			                columnsresize: true,
							editable: true,
							selectionmode: 'multiplerows',
			                columns: [
						   	   { text: '구분', datafield : 'GRP_TYP_NM', width: 170, align: 'center', cellsalign: 'center', editable: false, cellsrenderer: defulatrenderer},
			                   { text: '그룹명', datafield: 'USER_GRP_NM', align:'center', cellsalign: 'left', editable: false, cellsrenderer: defulatrenderer}
			                ]
			            });
			            $("#gridRoleUserGrpListNoSelect").jqxGrid('clearselection');
					});
					_flagrolegroupNo_ = false;
				}
				setFilter($('#gridRoleUserGrpListNoSelect'), {datafield:'USER_GRP_NM', filtervalue: $('#txtRoleGroupSearchSelectNo').val()});
			}
			function roleGroupGrid(gId){
				if(gId == ""){
					gId = $("#hiddenSaveGid").val();
				}
				$("#gridRoleUserGrpListSelect").jqxGrid('unselectrow');
				if(_flagrolegroup_) {
					var gridRoleList = $i.post("./getMenuAuthorityGroupList", {gID:gId, userGrpName:'', grpTyp:$("#cboGrpTypByRoleSelect").val()});
					gridRoleList.done(function(roleData){
						var source = {
							datatype: 'json',
			                datafields: [
			                    { name: 'USER_GRP_ID', type: 'string'},
			                    { name: 'USER_GRP_NM', type: 'string'},
			                    { name: 'GRP_TYP', type: 'string'},
			                    { name: 'GRP_TYP_NM', type:'string'}
			                ],
			                localdata: roleData.returnArray,
			                updaterow: function (rowid, rowdata, commit) {
			                    commit(true);
			                }
		                };
						var dataAdapter = new $.jqx.dataAdapter(source);
						
			            $("#gridRoleUserGrpListSelect").jqxGrid({
			              	width: '100%',
			                height:'100%',
							altrows:true,
							pageable: false,
			                source: dataAdapter,
							theme:'blueish-system',
							sortable: false,
							selectionmode: 'multiplerows',
			                columnsresize: true,
							editable: true,
			                columns: [
						   	   { text: '구분', datafield : 'GRP_TYP_NM', width: 170, align: 'center', cellsalign: 'center', editable: false, cellsrenderer: defulatrenderer},
			                   { text: '그룹명', datafield: 'USER_GRP_NM', align:'center', cellsalign: 'left', editable: false, cellsrenderer: defulatrenderer}
			                ]
			            });
			            $("#gridRoleUserGrpListSelect").jqxGrid('clearselection');
					});
					_flagrolegroup_ = false;
				}
				setFilter($('#gridRoleUserGrpListSelect'), {datafield:'USER_GRP_NM', filtervalue: $('#txtRoleGroupSearch').val()});
			}
			function makeGroupData(){
				$("#hiddenSaveGid").val('');
				var groupData=$i.post("./getGroupList",{userGrpName:$("#txtGroupSearch").val(), grpTyp:$("#cboGrpTyp").val()});
				groupData.done(function(group){
					var source = {
		                datatype: "json",
		                datafields: [
		                    { name: 'USER_GRP_ID', type: 'string'},
		                    { name: 'USER_GRP_NM', type: 'string'},
		                    { name: 'GRP_TYP', type: 'string'},
		                    { name: 'GRP_TYP_NM', type: 'string'}
		                ],
		                localdata: group.returnArray,
		                updaterow: function (rowid, rowdata, commit) {  
		                    commit(true);
		                }
		            };
					var dataAdapter = new $.jqx.dataAdapter(source);
					
		            $("#gridGroupList").jqxGrid(
		            {
   	              	    width: '100%', 
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'simple-gray',
						sortable: false,
		                columnsresize: true, 
		                columns: [
							{ text: '그룹명', datafield: 'USER_GRP_NM', width: '100%', align:'center', cellsalign: 'left'
								, renderer: function (text, align, height) {
									return "<div style=' margin-top:4px; text-align:center;'><img width='16' height='16' style='' src='../../resources/css/images/img_icons/icon_role.png'/><span style='margin-left: 4px; '>그룹명</span><div style='clear: both;'></div></div>";
								}, cellsrenderer: underlinerenderer
							}
						]
		            });
		            $('#gridGroupList').on('rowselect', function (event){
		            	
		            	var args = event.args;
					    var rowBoundIndex = args.rowindex;
					    var rowData = args.row;
					    _flaggroupuser_   = true;
						_flaggrouprole_   = true;
						_flaggrouproleNo_ = true;
						
						var userGrpID = args.row.USER_GRP_ID;
						$("#c").html(args.row.USER_GRP_NM);
						$("#labelGroupName").html($i.secure.scriptToText(args.row.USER_GRP_NM));
						$("#hiddenAddGroupName").val(args.row.USER_GRP_NM);
						makeGroupDetailData(userGrpID);
						makeGroupTabGrid(userGrpID);   
						$("#hiddenSavePid").val(userGrpID);
					});
		            $("#labelGroupName").html("");
		            $("#hiddenAddGroupName").val("");
		            $("#gridGroupList").jqxGrid('clearselection');
		            makeGroupDetailData('0');
		            makeGroupTabGrid('0');
		            
				});
			}
			function makeGroupDetailData(fkey){
				var treegridRoleData = $i.post("./getMenuAuthorityTreeList", {fkey:'', type:'p'});
				treegridRoleData.done(function(data){
					if(data.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", data.returnMessage);
					}else{
						drawTreeTabByRole_User(data.returnArray,"treegridGroupMenuList",5999,fkey,'p');
					}
				});
			}
			function makeGroupTabGrid(gId){
				groupUserGrid(gId);
				groupRoleGrid(gId);
				groupRoleNoGrid(gId);
			}
			function groupUserGrid(pId){
				if(pId == ""){
					pId = $("#hiddenSavePid").val();
				}
				var empID = "";
				var empNm = "";
				if($("#cboGroupSearch").val() == "1"){
					empNm = $("#txtGroupUserSearch").val();
				}else{
					empID = $("#txtGroupUserSearch").val();
					
				}
				$("#gridGroupUserList").jqxGrid('unselectrow');
				var gridRoleUserList = $i.post("./getUsersInGroup",{empID:empID, empName:empNm, pID:pId});
				gridRoleUserList.done(function(userData){
					usergridsource.localdata = userData.returnArray;
			        var dataAdapter = new $.jqx.dataAdapter(usergridsource);
			            
		            $("#gridGroupUserList").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish-system',
						sortable: true,
		                columnsresize: true,
		             	editable: true,
		                columns: [
					   	    { text: '사번', datafield: 'EMP_ID', width: 143, align:'center', cellsalign: 'center', editable: false, cellsrenderer: defulatrenderer},
		                 	{ text: '이름', datafield: 'EMP_NM', width: 143, align:'center', cellsalign: 'center', editable: false, cellsrenderer: defulatrenderer},
							{ text: '부서', datafield: 'USER_GRP_NM', align:'center', cellsalign: 'left', cellsrenderer: defulatrenderer, editable: false}
		                ]
		            });
		            $("#gridGroupUserList").jqxGrid('clearselection');
				});
			}
			function groupRoleNoGrid(pId){
				if(pId == ""){
					pId = $("#hiddenSavePid").val();
				}
				$("#gridGroupRoleNoSelect").jqxGrid('unselectrow');
				if(_flaggrouproleNo_) {
					var gridRoleList = $i.post("./getRoleListNoByGroup", {fkey:pId, groupName:''});
					gridRoleList.done(function(roleData){
						rolegridsource.localdata = roleData.returnArray;
						var dataAdapter = new $.jqx.dataAdapter(rolegridsource);
						
			            $("#gridGroupRoleNoSelect").jqxGrid({
			              	width: '100%',
			                height:'100%',
							altrows:true,
							pageable: false,
			                source: dataAdapter,
							theme:'blueish-system',
							sortable: false,
			                columnsresize: true,
							editable: true,
							selectionmode: 'multiplerows',
			                columns: [
			                   { text: 'Role명', datafield: 'GROUP_NAME', width: '100%', align:'center', cellsalign: 'left', editable: false, cellsrenderer: defulatrenderer}
			                ]
			            });
			            $("#gridGroupRoleNoSelect").jqxGrid('clearselection');
					});
					_flaggrouproleNo_ = false;
				}
				setFilter($('#gridGroupRoleNoSelect'), {datafield:'USER_GRP_NM', filtervalue: $('#txtGroupRoleSearchSelectNo').val()});
			}
			function groupRoleGrid(pId){
				if(pId == ""){
					pId = $("#hiddenSavePid").val();
				}
				$("#gridGroupRoleSelect").jqxGrid('unselectrow');
				if(_flaggrouprole_) {
					var gridRoleList = $i.post("./getRoleListByGroup", {fkey:pId, groupName:''});
					gridRoleList.done(function(roleData){
						rolegridsource.localdata = roleData.returnArray;
						var dataAdapter = new $.jqx.dataAdapter(rolegridsource);
						
			            $("#gridGroupRoleSelect").jqxGrid({
			              	width: '100%',
			                height:'100%',
							altrows:true,
							pageable: false,
			                source: dataAdapter,
							theme:'blueish-system',
							sortable: false,
			                columnsresize: true,
							editable: true,
							selectionmode: 'multiplerows',
			                columns: [
			                   { text: 'Role명', datafield: 'GROUP_NAME', width: '100%', align:'center', cellsalign: 'left', editable: false, cellsrenderer: defulatrenderer}
			                ]
			            });
			            $("#gridGroupRoleSelect").jqxGrid('clearselection');
					});
					_flaggrouprole_ = false;
				}
				setFilter($('#gridGroupRoleSelect'), {datafield:'USER_GRP_NM', filtervalue: $('#txtGroupRoleSearchSelect').val()});
			}
			var addfilter = function () {
				$("#treegridMenuList").jqxTreeGrid('expandAll');
				 var searchWord = $('#txtReportSearch').val();
				 
			     var filtertype = 'stringfilter';
				  // create a new group of filters.
				 var filtergroup = new $.jqx.filter();
				 var filter_or_operator = 1;
				 var filtervalue = searchWord;
				 var filtercondition = 'contains';
				 var filter = filtergroup.createfilter(filtertype, filtervalue, filtercondition);
				 filtergroup.addfilter(filter_or_operator, filter);
				  // add the filters.
				 $("#treegridMenuList").jqxTreeGrid('addFilter', 'C_NAME', filtergroup);
				  // apply the filters.
				 $("#treegridMenuList").jqxTreeGrid('applyFilters');
			     
			 }
			function makeMenuData(){
				$("#hiddenSaveCid").val('');
				$("#labelMenuName").html('');
				$("#hiddenAddMenuName").val('');
				$("#txtReportSearch").val('');
				var menuData= $i.post("./getMenuAuthorityTreeList", {fkey:'', type:''});
				menuData.done(function(menu){
					$("#treegridMenuList").jqxTreeGrid('clear');
					var source =
					{
						datatype: "json",
						datafields: [
							{ name: 'C_ID'},
							{ name: 'P_ID'},
							{ name: 'C_NAME'},
							{ name: 'MENU_TYPE'},
							{ name: 'GROUP_CHECK'},
							{ name: 'MENU_OPEN_TYPE'}
						],
						icons:true,
						pageable:false,  
						filterable:true,
						localdata: menu.returnArray,
						hierarchy:{
							keyDataField:{name:'C_ID'},
							parentDataField:{name:'P_ID'} 
						},
						id:'C_ID'
					};
					var dataAdapter = new $.jqx.dataAdapter(source, {
		                loadComplete: function () {
		                }
	            	});
					$("#treegridMenuList").jqxTreeGrid({source: dataAdapter, height: '100%', width: '100%',altRows: true,theme:"simple-gray",
						icons:function(rowKey, rowdata) { return getIconImage(rowdata.MENU_TYPE, rowdata.MENU_TYPE, rowdata.MENU_OPEN_TYPE); },
						columns:[
							{ dataField:'C_ID',text:'C_ID',align:'center', hidden:true},
							{ dataField:'P_ID',text:'P_ID',align:'center', hidden:true},
							{ dataField:'C_NAME',text:'보고서명', algin:'center'
								, renderer: function (text, align, height) {
									return "<div style=' margin-top:4px; text-align:center;'><img width='16' height='16' style='' src='../../resources/css/images/img_icons/icon_role.png'/><span style='margin-left: 4px; '>보고서명</span><div style='clear: both;'></div></div>";
								}
								,cellsrenderer:function(row,datafield,value,rowdata){
									var newValue = $i.secure.scriptToText(rowdata.C_NAME);
									if(rowdata.records!=null){
										var cntlabel = "<span style='color: Blue;' data-role=='treefoldersize'> (" + rowdata.records.length + ")</span>";
										return "<a href=\"javascript:makeMenuTabGrid("+rowdata.C_ID+");\" style='color:#000000;'>"+newValue+cntlabel+"</a>";
									} else {
										return "<a href=\"javascript:makeMenuTabGrid("+rowdata.C_ID+");\" style='color:#000000;'>"+newValue+"</a>";
									}
								}
							}
						],
						ready:function(){
						}
					});
					$('#treegridMenuList').on('rowSelect', function (event){
						 var args = event.args;
						 var row = args.row;
						 var key = args.key;
						 var cname = row.C_NAME;
						 $("#hiddenAddMenuName").val(cname);
						 //$("#labelMenuName").html($i.secure.scriptToText(cname));
					});
					$("#treegridMenuList").jqxTreeGrid({ width: '100%',  height: '100%',  theme:'simple-gray' });
					
					makeMenuTabGrid('');
				});
			}
			function makeMenuTabGrid(cId){
				$("#hiddenSaveCid").val(cId);
				var cname =  $("#hiddenAddMenuName").val();
				$("#labelMenuName").html($i.secure.scriptToText(cname));
				menuUserNoGrid(cId);
				menuUserGrid(cId);
				menuGroupNoGrid(cId);
				menuGroupGrid(cId);
				menuRoleNoGrid(cId);
				menuRoleGrid(cId);
			}
			function menuUserNoGrid(cId){
				if(cId == ""){
					cId = $("#hiddenSaveCid").val();
				}
				var empID = "";
				var empNm = "";
				var gubn = "";
				if($("#txtMenuUserSearchSelectNo").val() == ""){
					gubn = "";
				}else{
					gubn = "NOTNULL";
					if($("#cboMenuSearchNoSelect").val() == "1"){
						empNm = $("#txtMenuUserSearchSelectNo").val();
					}else{
						empID = $("#txtMenuUserSearchSelectNo").val();						
					}	
				}
				$("#gridMenuUserListNoSelect").jqxGrid('unselectrow');
				if(empNm==""&&empID==""){
					cId=$i.cipher.aes256.encrypt("dummy");
					empID=$i.cipher.aes256.encrypt("dummy");
				}
				var gridMenuUserList = $i.post("./getMenuAuthorityUserListNoByMenu",{cID:cId, empID:empID, empNm:empNm, gubn:gubn});
				gridMenuUserList.done(function(userData){
					usergridsource.localdata = userData.returnArray;
			        var dataAdapter = new $.jqx.dataAdapter(usergridsource);
			            
		            $("#gridMenuUserListNoSelect").jqxGrid(
		             {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish-system',
						sortable: true,
		                columnsresize: true,
		             	editable: true,
		                columns: [
						    { text: '사번', datafield: 'EMP_ID', width: 95, align:'center', cellsalign: 'center', editable: false ,cellsrenderer: defulatrenderer},
		                    { text: '이름', datafield: 'EMP_NM', width: 90, align:'center', cellsalign: 'center', editable: false ,cellsrenderer: defulatrenderer},
						    { text: '부서', datafield: 'USER_GRP_NM', align:'center', cellsalign: 'left', cellsrenderer: defulatrenderer, editable: false}
		                ]
		            });
		            $("#gridMenuUserListNoSelect").jqxGrid('clearselection');
				});
			}
			function menuUserGrid(cId){
				if(cId == ""){
					cId = $("#hiddenSaveCid").val();
				}
				var empID = "";
				var empNm = "";
				if($("#cboMenuSearchSelect").val() == "1"){
					empID = $("#txtMenuUserSearchSelect").val();
				}else{
					empNm = $("#txtMenuUserSearchSelect").val();
				}
				$("#gridMenuUserListSelect").jqxGrid('unselectrow');
				var gridMenuUserList = $i.post("./getUsersInMenu",{cID:cId, empID:empID, empNm:empNm});
				gridMenuUserList.done(function(userData){
					usergridsource.localdata = userData.returnArray;
			        var dataAdapter = new $.jqx.dataAdapter(usergridsource);
			            
		            $("#gridMenuUserListSelect").jqxGrid(
		             {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish-system',
						sortable: true,
		                columnsresize: true,
		             	editable: true,
		                columns: [
						    { text: '사번', datafield: 'EMP_ID', width: 95, align:'center', cellsalign: 'center', editable: false ,cellsrenderer: defulatrenderer},
		                    { text: '이름', datafield: 'EMP_NM', width: 90, align:'center', cellsalign: 'center', editable: false ,cellsrenderer: defulatrenderer},
						    { text: '부서', datafield: 'USER_GRP_NM', align:'center', cellsalign: 'left', cellsrenderer: defulatrenderer, editable: false}
		                ]
		            });
		            $("#gridMenuUserListSelect").jqxGrid('clearselection');
				});
			}
			function menuGroupNoGrid(cId){
				if(cId == ""){
					cId = $("#hiddenSaveCid").val();
				}
				$("#gridMenuGroupListNoSelect").jqxGrid('unselectrow');
				var groupData = $i.post("./getMenuAuthorityGroupListNoSelectByMenu", {cID:cId, userGrpName:''});
				groupData.done(function(group){
					var source = {
						datatype: 'json',
		                datafields: [
		                	{ name: 'GRP_TYP',     type: 'string'},
		                	{ name: 'GRP_TYP_NM',  type: 'string'},
		                    { name: 'USER_GRP_ID', type: 'string'},
		                    { name: 'USER_GRP_NM', type: 'string'}
		                ],
		                localdata: group.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
	                };
					var dataAdapter = new $.jqx.dataAdapter(source);
					
		            $("#gridMenuGroupListNoSelect").jqxGrid({
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
						selectionmode: 'multiplerows',
		                source: dataAdapter,
						theme:'blueish-system',
						sortable: false,
		                columnsresize: true,
		                columns: [
					   	   { text: '구분', datafield: 'GRP_TYP_NM', width: 95, align:'center', cellsalign: 'center'},
		                   { text: '그룹명', datafield: 'USER_GRP_NM', align:'center', cellsalign: 'left', editable: false, cellsrenderer: defulatrenderer}
		                ]
		            });
		            $("#gridMenuGroupListNoSelect").jqxGrid('clearselection');
				});
				setFilter($('#gridMenuGroupListNoSelect'), {datafield:'USER_GRP_NM', filtervalue: $('#txtMenuGroupSearchNoSelect').val()});
			}
			function menuGroupGrid(cId){
				if(cId == ""){
					cId = $("#hiddenSaveCid").val();
				}
				$("#gridMenuGroupListSelect").jqxGrid('unselectrow');
				var groupData = $i.post("./getMenuAuthorityGroupListByMenu", {cID:cId, userGrpName:''});
				groupData.done(function(group){
					var source = {
						datatype: 'json',
		                datafields: [
		                	{ name: 'GRP_TYP',     type: 'string'},
		                	{ name: 'GRP_TYP_NM',  type: 'string'},
		                    { name: 'USER_GRP_ID', type: 'string'},
		                    { name: 'USER_GRP_NM', type: 'string'}
		                ],
		                localdata: group.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
	                };
					var dataAdapter = new $.jqx.dataAdapter(source);
					
		            $("#gridMenuGroupListSelect").jqxGrid({
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
		                selectionmode: 'multiplerows',
						theme:'blueish-system',
						sortable: false,
		                columnsresize: true,
		                columns: [
					   	   { text: '구분', datafield: 'GRP_TYP_NM', width: 95, align:'center', cellsalign: 'center'},
		                   { text: '그룹명', datafield: 'USER_GRP_NM', align:'center', cellsalign: 'left', editable: false, cellsrenderer: defulatrenderer}
		                ]
		            });
		            $("#gridMenuGroupListSelect").jqxGrid('clearselection');
				});
				setFilter($('#gridMenuGroupListSelect'), {datafield:'USER_GRP_NM', filtervalue: $('#txtMenuGroupSearchSelect').val()});
			}
			function menuRoleNoGrid(cId){
				if(cId == ""){
					cId = $("#hiddenSaveCid").val();
				}
				$("#gridMenuRoleListNoSelect").jqxGrid('unselectrow');
				var roleData = $i.post("./getMenuAuthorityRoleListNoSelectByMenu", {cID:cId, groupName:''});
				roleData.done(function(role){
					var source = {
						datatype: 'json',
		                datafields: [
		                    { name: 'GID', type: 'string'},
		                    { name: 'GROUP_NAME', type: 'string'},
		                    { name: 'GROUP_CHECK', type: 'string'}
		                ],
		                localdata: role.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
	                };
					var dataAdapter = new $.jqx.dataAdapter(source);
					
		            $("#gridMenuRoleListNoSelect").jqxGrid({
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
						selectionmode: 'multiplerows',
		                source: dataAdapter,
						theme:'blueish-system',
						sortable: false,
		                columnsresize: true, 
						editable: true,
		                columns: [
                  		   { text: 'Role명', datafield: 'GROUP_NAME', width: '100%', align:'center', cellsalign: 'center', editable: false , cellsrenderer: defulatrenderer}
		                ]
		            });
		            $("#gridMenuRoleListNoSelect").jqxGrid('clearselection');
				});
				setFilter($('#gridMenuRoleListNoSelect'), {datafield:'USER_GRP_NM', filtervalue: $('#txtMenuRoleSearchNoSelect').val()});
			}
			function menuRoleGrid(cId){
				if(cId == ""){
					cId = $("#hiddenSaveCid").val();
				}
				$("#gridMenuRoleListSelect").jqxGrid('unselectrow');
				var roleData = $i.post("./getMenuAuthorityRoleListByMenu", {cID:cId, groupName:''});
				roleData.done(function(role){
					var source = {
						datatype: 'json',
		                datafields: [
		                    { name: 'GID', type: 'string'},
		                    { name: 'GROUP_NAME', type: 'string'},
		                    { name: 'GROUP_CHECK', type: 'string'}
		                ],
		                localdata: role.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
	                };
					var dataAdapter = new $.jqx.dataAdapter(source);
					
		            $("#gridMenuRoleListSelect").jqxGrid({
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
						selectionmode: 'multiplerows',
		                source: dataAdapter,
						theme:'blueish-system',
						sortable: false,
		                columnsresize: true,
						editable: true,
		                columns: [
                  		   { text: 'Role명', datafield: 'GROUP_NAME', width: '100%', align:'center', cellsalign: 'center', editable: false , cellsrenderer: defulatrenderer}
		                ]
		            });
		            $("#gridMenuRoleListSelect").jqxGrid('clearselection');
				});
				setFilter($('#gridMenuRoleListSelect'), {datafield:'USER_GRP_NM', filtervalue: $('#txtMenuRoleSearchSelect').val()});
			}
			function drawTreeTabByMenu(res,id,fid,fkey,type){
				$("#"+id).jqxTreeGrid('clear');
				var source =
				{
					datatype: "json",
					datafields: [
						{ name: 'C_ID'},
						{ name: 'P_ID'},
						{ name: 'C_NAME'},
						{ name: 'MENU_TYPE'},
						{ name: 'GROUP_CHECK'},
						{ name: 'MENU_OPEN_TYPE'}
					],
					icons:true,
					pageable:false,  
					localdata: res,
					hierarchy:{
						keyDataField:{name:'C_ID'},
						parentDataField:{name:'P_ID'} 
					},
					id:'C_ID'
				};
				var dataAdapter = new $.jqx.dataAdapter(source, {
	                loadComplete: function () {
	                }
            	});
				$("#"+id).jqxTreeGrid({source: dataAdapter, height: '100%', width: '100%',altRows: true,theme:"empty-style",
					icons:function(rowKey, rowdata) { return getIconImage(rowdata.MENU_TYPE, rowdata.MENU_TYPE, rowdata.MENU_OPEN_TYPE); },
					columns:[
						{ dataField:'C_ID',text:'C_ID',align:'center', hidden:true},
						{ dataField:'P_ID',text:'P_ID',align:'center', hidden:true},
						{ dataField:'C_NAME',text:'C_NAME', algin:'center'
							,cellsrenderer:function(row,datafield,value,rowdata){
								return "<a href=\"javascript:treeCheckBox('"+rowdata.C_ID+"','"+rowdata.MENU_OPEN_TYPE+"','"+id+"');\" style='color:#000000;'>"+rowdata.C_NAME+"</a>";
					         }
						}
					],
					ready:function(){
					}
				});
				<%-- PID 가 사라져, ROOT 에 표시되는 메뉴 제거 --%>
// 				deleteNullMenu($("#"+id));
				var checkDataList = $i.post("./getMenuAuthorityList",{fkey:fkey,type:type});
				checkDataList.done(function(checkData){
					for(var i=0;i<checkData.returnArray.length;i++){
						$("#"+id).jqxTreeGrid("checkRow",checkData.returnArray[i].C_ID);
					}
				});
				$("#"+id).on('rowCheck', function (event) {
					if($("#"+id).jqxTreeGrid('getRow', event.args.row.C_ID).checked == true){
						if(event.args.row.MENU_OPEN_TYPE != 'common'){
							alert("["+event.args.row.NAME+"]" + "는 전체공개 메뉴 입니다");
							$("#"+id).jqxTreeGrid('uncheckRow', event.args.row.C_ID);
						}
					}else{
						return false;
					}
				});
				$("#"+id).jqxTreeGrid({ width: '100%',  height: 552,  theme:'empty-style' });
			}
			function drawTreeTabByRole_User(res,id,fid,fkey,type){
				$("#"+id).jqxTreeGrid('clear');
				var source =
				{
					datatype: "json",
					datafields: [
						{ name: 'C_ID'},
						{ name: 'P_ID'},
						{ name: 'C_NAME'},
						{ name: 'MENU_TYPE'},
						{ name: 'GROUP_CHECK'},
						{ name: 'MENU_OPEN_TYPE'}
					],
					icons:true,
					pageable:false,  
					localdata: res,
					hierarchy:{
						keyDataField:{name:'C_ID'},
						parentDataField:{name:'P_ID'} 
					},
					id:'C_ID'
				};
				var dataAdapter = new $.jqx.dataAdapter(source, {
	                loadComplete: function () {
	                }
            	});
				$("#"+id).jqxTreeGrid({source: dataAdapter, height: '100%', width: '100%',altRows: true,checkboxes: true,theme:"empty-style",
					icons:function(rowKey, rowdata) { return getIconImage(rowdata.MENU_TYPE, rowdata.MENU_TYPE, rowdata.MENU_OPEN_TYPE); },
					columns:[
						{ dataField:'C_ID',text:'C_ID',align:'center', hidden:true},
						{ dataField:'P_ID',text:'P_ID',align:'center', hidden:true},
						{ dataField:'C_NAME',text:'C_NAME', algin:'center'
							,cellsrenderer:function(row,datafield,value,rowdata){
								//var newValue = $i.secure.scriptToText(rowdata.C_NAME);
								var newValue = rowdata.C_NAME;
								return "<a href=\"javascript:treeCheckBox('"+rowdata.C_ID+"','"+rowdata.MENU_OPEN_TYPE+"','"+id+"');\" style='color:#000000;'>"+newValue+"</a>";
					         }
						}
					],
					ready:function(){
					}
				});
				<%-- PID 가 사라져, ROOT 에 표시되는 메뉴 제거 --%>
// 				deleteNullMenu($("#"+id));
				var checkDataList = $i.post("./getMenuAuthorityList",{fkey:fkey,type:type});
				checkDataList.done(function(checkData){
					for(var i=0;i<checkData.returnArray.length;i++){
						$("#"+id).jqxTreeGrid("checkRow",checkData.returnArray[i].C_ID);
						var row = $("#"+id).jqxTreeGrid("getRow",checkData.returnArray[i].C_ID);
						while(row!=null&&row.parent!=null){
							row = row.parent;
							var parentTitle = $("#"+id).jqxTreeGrid('getCellValue',row.C_ID, 'C_NAME');							
							$("#"+id).jqxTreeGrid('setCellValue',row.C_ID, 'C_NAME','<span style="color:blue !important;font-weight:bold;">'+parentTitle+'</span>');
						}
					}
				});
				$("#"+id).on('rowCheck', function (event) {
					if($("#"+id).jqxTreeGrid('getRow', event.args.row.C_ID).checked == true){
						if(event.args.row.MENU_OPEN_TYPE != 'common'){
							alert("["+event.args.row.NAME+"]" + "는 전체공개 메뉴 입니다");
							$("#"+id).jqxTreeGrid('uncheckRow', event.args.row.C_ID);
						}
					}else{
						return false;
					}
				});
				$("#"+id).jqxTreeGrid({ width: '100%',  height: 552,  theme:'blueish-system' });
			}
			
			function makeRoleData(){
				$("#hiddenSaveGid").val('');
				var roleData=$i.post("./getRoleList",{groupName:$("#txtRoleSearch").val()});
				roleData.done(function(role){
					rolegridsource.localdata = role.returnArray;
					
					var dataAdapter = new $.jqx.dataAdapter(rolegridsource);
					
		            $("#gridRoleList").jqxGrid(
		            {
   	              	    width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'simple-gray',  
						sortable: false,
		                columnsresize: true,
		                columns: [
                            { text: 'Role명', datafield: 'GROUP_NAME', width: '100%', align:'center', cellsalign: 'left'
								, renderer: function (text, align, height) {
									return "<div style=' margin-top:4px; text-align:center;'><img width='16' height='16' style='' src='../../resources/css/images/img_icons/icon_role.png'/><span style='margin-left: 4px; '>Role명</span><div style='clear: both;'></div></div>";
								}, cellsrenderer: underlinerenderer
							}
						]
		            });
		            $('#gridRoleList').on('rowselect', function (event){
		            	var args = event.args;
					    var rowBoundIndex = args.rowindex;
					    var rowData = args.row;
					    _flagrolegroup_   = true; // 롤 > 그룹
					    _flagrolegroupNo_ = true;
						var gid = args.row.GID;
						$("#hiddenAddRoleID").val(gid);
						$("#hiddenAddRoleName").val(args.row.GROUP_NAME);
						$("#txtAddRoleName").val(args.row.GROUP_NAME);
						$("#labelRoleName").html($i.secure.scriptToText(args.row.GROUP_NAME));
						makeRoleDetailData(gid);
						makeRoleTabGrid(gid);
						$("#hiddenSaveGid").val(gid);
						
					});
		            $("#hiddenAddRoleID").val("");
		            $("#hiddenAddRoleName").val("");
		            $("#txtAddRoleName").val("");
		            $("#labelRoleName").html("");
		            $("#gridRoleList").jqxGrid('clearselection');
		            makeRoleDetailData('');
		            makeRoleTabGrid('');
				});
			}
			function makeDataData(){
				$("#hiddenSaveBid").val('');
				var dataData=$i.post("./getBusinessList",{groupName:$("#txtRoleSearch").val()});
				dataData.done(function(data){
					datagridsource.localdata = data.returnArray;
					var dataAdapter = new $.jqx.dataAdapter(datagridsource); 
					$("#gridDataList").jqxGrid(
					{
						width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'simple-gray',  
						sortable: false,
		                columnsresize: true,
		                columns: [
                            { text: 'Data명', datafield: 'BUSINESS_NAME', width: '100%', align:'center', cellsalign: 'left'
								, renderer: function (text, align, height) {
									return "<div style=' margin-top:4px; text-align:center;'><img width='16' height='16' style='' src='../../resources/css/images/img_icons/icon_role.png'/><span style='margin-left: 4px; '>Data명</span><div style='clear: both;'></div></div>";
								}, cellsrenderer: underlinerenderer
							}
						]
		            });
		            $('#gridDataList').on('rowselect', function (event){
		            	var args = event.args;
					    var rowBoundIndex = args.rowindex;
					    var rowData = args.row;
// 					    _flagrolegroup_   = true; // 롤 > 그룹
// 					    _flagrolegroupNo_ = true;
						var bid = args.row.BID;
						$("#hiddenAddDataID").val(bid);
						$("#hiddenAddDataName").val(args.row.BUSINESS_NAME);
						$("#txtAddDataName").val(args.row.BUSINESS_NAME);
						$("#labelDataName").html($i.secure.scriptToText(args.row.BUSINESS_NAME));
						makeDataDetailData(bid);
// 						makeRoleTabGrid(gid);
						$("#hiddenSaveBid").val(bid);
						
					});
		            $("#hiddenAddDataID").val("");
		            $("#hiddenAddDataName").val("");
		            $("#txtAddDataName").val("");
		            $("#labelDataName").html("");
		            $("#gridDataList").jqxGrid('clearselection');
		            makeDataDetailData('');
				});
			}
			function makeDataDetailData(bID){
				dataUserNoGrid(bID);
				dataUserGrid(bID);
				dataGroupNoGrid(bID);
				dataGroupGrid(bID);
				dataRoleNoGrid(bID);
				dataRoleGrid(bID);
			}
			function dataUserNoGrid(bID){
				if(bID == ""){
					bID = $("#hiddenSaveBid").val();
				}
				var empID = "";
				var empNm = "";
				var gubn = "";
				if($("#txtDataUserSearchSelectNo").val() == ""){
					gubn = "";
				}else{
					gubn = "NOTNULL";
					if($("#cboDataSearchNoSelect").val() == "1"){
						empNm = $("#txtDataUserSearchSelectNo").val();
					}else{
						empID = $("#txtDataUserSearchSelectNo").val();
					}	
				}
				$("#gridDataUserListNoSelect").jqxGrid('unselectrow');
				var gridMenuUserList = $i.post("./getMenuAuthorityUserListNoByData",{bID:bID, empID:empID, empName:empNm, gubn:gubn});
				gridMenuUserList.done(function(userData){
					usergridsource.localdata = userData.returnArray;
			        var dataAdapter = new $.jqx.dataAdapter(usergridsource);
			            
		            $("#gridDataUserListNoSelect").jqxGrid(
		             {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish-system',
						sortable: true,
		                columnsresize: true,
		             	editable: true,
		                columns: [
						    { text: '사번', datafield: 'EMP_ID', width: 95, align:'center', cellsalign: 'center', editable: false ,cellsrenderer: defulatrenderer},
		                    { text: '이름', datafield: 'EMP_NM', width: 90, align:'center', cellsalign: 'center', editable: false ,cellsrenderer: defulatrenderer},
						    { text: '부서', datafield: 'USER_GRP_NM', align:'center', cellsalign: 'left', cellsrenderer: defulatrenderer, editable: false}
		                ]
		            });
		            $("#gridDataUserListNoSelect").jqxGrid('clearselection');
				});
			}
			function dataUserGrid(bID){
				if(bID == ""){
					bID = $("#hiddenSaveBid").val();
				}
				var empID = "";
				var empNm = "";
				var gubn = "";
				if($("#txtDataUserSearchSelect").val() != ""){
					gubn = "NOTNULL";
				}
				if($("#cboDataSearchSelect").val() == "1"){
					empID = $("#txtDataUserSearchSelect").val();
				}else{
					empNm = $("#txtDataUserSearchSelect").val();
				}
				$("#gridDataUserListSelect").jqxGrid('unselectrow');
				var gridMenuUserList = $i.post("./getMenuAuthorityUserListByData",{bID:bID, empID:empID, empName:empNm, gubn:gubn});
				gridMenuUserList.done(function(userData){
					usergridsource.localdata = userData.returnArray;
			        var dataAdapter = new $.jqx.dataAdapter(usergridsource);
			            
		            $("#gridDataUserListSelect").jqxGrid(
		             {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish-system',
						sortable: true,
		                columnsresize: true,
		             	editable: true,
		                columns: [
						    { text: '사번', datafield: 'EMP_ID', width: 95, align:'center', cellsalign: 'center', editable: false ,cellsrenderer: defulatrenderer},
		                    { text: '이름', datafield: 'EMP_NM', width: 90, align:'center', cellsalign: 'center', editable: false ,cellsrenderer: defulatrenderer},
						    { text: '부서', datafield: 'USER_GRP_NM', align:'center', cellsalign: 'left', cellsrenderer: defulatrenderer, editable: false}
		                ]
		            });
		            $("#gridDataUserListSelect").jqxGrid('clearselection');
				});
			}
			function dataGroupNoGrid(bID){
				if(bID == ""){
					bID = $("#hiddenSaveBid").val();
				}
				$("#gridDataGroupListNoSelect").jqxGrid('unselectrow');
				var groupData = $i.post("./getMenuAuthorityGroupListNoByData", {bID:bID, userGrpName:''});
				groupData.done(function(group){
					var source = {
						datatype: 'json',
		                datafields: [
		                	{ name: 'GRP_TYP',     type: 'string'},
		                	{ name: 'GRP_TYP_NM',  type: 'string'},
		                    { name: 'USER_GRP_ID', type: 'string'},
		                    { name: 'USER_GRP_NM', type: 'string'}
		                ],
		                localdata: group.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
	                };
					var dataAdapter = new $.jqx.dataAdapter(source);
					
		            $("#gridDataGroupListNoSelect").jqxGrid({
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
		                selectionmode: 'multiplerows',
						theme:'blueish-system',
						sortable: false,
		                columnsresize: true,
		                columns: [
					   	   { text: '구분', datafield: 'GRP_TYP_NM', width: 95, align:'center', cellsalign: 'center'},
		                   { text: '그룹명', datafield: 'USER_GRP_NM', align:'center', cellsalign: 'left', editable: false, cellsrenderer: defulatrenderer}
		                ]
		            });
		            $("#gridDataGroupListNoSelect").jqxGrid('clearselection');
				});
				setFilter($('#gridDataGroupListNoSelect'), {datafield:'USER_GRP_NM', filtervalue: $('#txtMenuGroupSearchNoSelect').val()});
			}
			function dataGroupGrid(bID){
				if(bID == ""){
					bID = $("#hiddenSaveBid").val();
				}
				$("#gridDataGroupListSelect").jqxGrid('unselectrow');
				var groupData = $i.post("./getMenuAuthorityGroupListByData", {bID:bID, userGrpName:''});
				groupData.done(function(group){
					var source = {
						datatype: 'json',
		                datafields: [
		                	{ name: 'GRP_TYP',     type: 'string'},
		                	{ name: 'GRP_TYP_NM',  type: 'string'},
		                    { name: 'USER_GRP_ID', type: 'string'},
		                    { name: 'USER_GRP_NM', type: 'string'}
		                ],
		                localdata: group.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
	                };
					var dataAdapter = new $.jqx.dataAdapter(source);
					
		            $("#gridDataGroupListSelect").jqxGrid({
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
		                selectionmode: 'multiplerows',
						theme:'blueish-system',
						sortable: false,
		                columnsresize: true,
		                columns: [
					   	   { text: '구분', datafield: 'GRP_TYP_NM', width: 95, align:'center', cellsalign: 'center'},
		                   { text: '그룹명', datafield: 'USER_GRP_NM', align:'center', cellsalign: 'left', editable: false, cellsrenderer: defulatrenderer}
		                ]
		            });
		            $("#gridDataGroupListSelect").jqxGrid('clearselection');
				});
				setFilter($('#gridDataGroupListSelect'), {datafield:'USER_GRP_NM', filtervalue: $('#txtMenuGroupSearchSelect').val()});
			}
			function dataRoleNoGrid(bID){
				if(bID == ""){
					bID = $("#hiddenSaveCid").val();
				}
				$("#gridDataRoleListNoSelect").jqxGrid('unselectrow');
				var roleData = $i.post("./getMenuAuthorityRoleListNoByData", {bID:bID, groupName:''});
				roleData.done(function(role){
					var source = {
						datatype: 'json',
		                datafields: [
		                    { name: 'GID', type: 'string'},
		                    { name: 'GROUP_NAME', type: 'string'}
		                ],
		                localdata: role.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
	                };
					var dataAdapter = new $.jqx.dataAdapter(source);
					
		            $("#gridDataRoleListNoSelect").jqxGrid({
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
		                selectionmode: 'multiplerows',
						theme:'blueish-system',
						sortable: false,
		                columnsresize: true, 
						editable: true,
		                columns: [
                  		   { text: 'Role명', datafield: 'GROUP_NAME', width: '100%', align:'center', cellsalign: 'center', editable: false , cellsrenderer: defulatrenderer}
		                ]
		            });
		            $("#gridDataRoleListNoSelect").jqxGrid('clearselection');
				});
				setFilter($('#gridDataRoleListNoSelect'), {datafield:'USER_GRP_NM', filtervalue: $('#txtDataRoleSearchNoSelect').val()});
			}
			function dataRoleGrid(bID){
				if(bID == ""){
					bID = $("#hiddenSaveCid").val();
				}
				$("#gridDataRoleListSelect").jqxGrid('unselectrow');
				var roleData = $i.post("./getMenuAuthorityRoleListByData", {bID:bID, GROUP_NAME:''});
				roleData.done(function(role){
					var source = {
						datatype: 'json',
		                datafields: [
		                    { name: 'GID', type: 'string'},
		                    { name: 'GROUP_NAME', type: 'string'}
		                ],
		                localdata: role.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
	                };
					var dataAdapter = new $.jqx.dataAdapter(source);
					
		            $("#gridDataRoleListSelect").jqxGrid({
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
		                selectionmode: 'multiplerows',
						theme:'blueish-system',
						sortable: false,
		                columnsresize: true,
						editable: true,
		                columns: [
                  		   { text: 'Role명', datafield: 'GROUP_NAME', width: '100%', align:'center', cellsalign: 'center', editable: false , cellsrenderer: defulatrenderer}
		                ]
		            });
		            $("#gridDataRoleListSelect").jqxGrid('clearselection');
				});
				setFilter($('#gridDataRoleListSelect'), {datafield:'USER_GRP_NM', filtervalue: $('#txtDataRoleSearchSelect').val()});
			}
			
			function dataNameReset(){
				$("#hiddenDataID").val("");
				$("#hiddenDataName").val("");
				$("#hiddenDataNameOrg").val("");
				$("#hiddenAddDataID").val("");
				$("#hiddenAddDataName").val("");
				$("#txtAddDataName").val("");
			}
			function dataNameInsert(){
				if($("#txtAddDataName").val().trim() == ""){
					$i.dialog.error("SYSTEM", "Data명을 입력하세요");
					return false;
				}
				$("#hiddenDataID").val($("#hiddenAddDataID").val());
				$("#hiddenDataName").val($("#txtAddDataName").val());
				$("#hiddenDataNameOrg").val($("#hiddenAddDataName").val());
				
				$("#hiddenRemoteIP05").val(remoteIP);
		        $("#hiddenRemoteOS05").val(remoteOS);
		        $("#hiddenRemoteBW05").val(remoteBrowser);
				$i.post("./insertDataInfo","#dataInfoSaveForm").done(function(data){
					if(data.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", data.returnMessage);
					}else{
						$i.dialog.alert("SYSTEM", data.returnMessage, function(){
							makeDataData();
							dataNameReset(); 
						});
					}
				});
			}
			function dataNameDelete(){
				$("#hiddenDataID").val($("#hiddenAddDataID").val());
				$("#hiddenDataName").val($("#hiddenAddDataName").val());
				
				$("#hiddenRemoteIP05").val(remoteIP);
		        $("#hiddenRemoteOS05").val(remoteOS);
		        $("#hiddenRemoteBW05").val(remoteBrowser);
				$i.dialog.confirm("SYSTEM", "삭제하시겠습니까?", function(){
					$i.post("./deleteDataInfo","#dataInfoSaveForm").done(function(data){
						if(data.returnCode == "EXCEPTION"){
							$i.dialog.error("SYSTEM", data.returnMessage);
						}else{
							$i.dialog.alert("SYSTEM", data.returnMessage, function(){
								makeDataData();
								dataNameReset();
								
							});
						}
					});	
				});
				
			}
			function dataMappingInsert(){
				if($("#hiddenSaveBid").val() == ""){
					$i.dialog.error("SYSTEM", "데이터를 선택하세요");
					return false;
				}
				var userCheckValue = $("#gridDataUserListSelect").jqxGrid("getRows");
				var groupCheckValue = $("#gridDataGroupListSelect").jqxGrid("getRows");
				var roleCheckValue = $("#gridDataRoleListSelect").jqxGrid("getRows");
				var menuInputBox = "";
				for(var i=0;i<userCheckValue.length;i++){
					menuInputBox += "<input type='hidden' name='userCheckValue' value='"+userCheckValue[i].EMP_ID+"' />";
					menuInputBox += "<input type='hidden' name='userCheckName' value='"+userCheckValue[i].EMP_NM+"' />";
				}
				for(var j=0;j<groupCheckValue.length;j++){
					menuInputBox += "<input type='hidden' name='groupCheckValue' value='"+groupCheckValue[j].USER_GRP_ID+"' />";
					menuInputBox += "<input type='hidden' name='groupCheckName' value='"+groupCheckValue[j].USER_GRP_NM+"' />";
				}
				for(var k=0;k<roleCheckValue.length;k++){
					menuInputBox += "<input type='hidden' name='roleCheckValue' value='"+roleCheckValue[k].GID+"' />";
					menuInputBox += "<input type='hidden' name='roleCheckName' value='"+roleCheckValue[k].GROUP_NAME+"' />";
				}
				$("#insertDataMapping").append(menuInputBox);
				$("#mappingDataID").val($("#hiddenSaveBid").val()); 
				$("#mappingDataName").val($("#hiddenAddDataName").val()); 
				
				$("#hiddenRemoteIP06").val(remoteIP);
		        $("#hiddenRemoteOS06").val(remoteOS);
		        $("#hiddenRemoteBW06").val(remoteBrowser);
				$i.post("./insertDataMapping","#insertDataMapping").done(function(data){
					if(data.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", data.returnMessage);
						$("#insertDataMapping").find("[name='userCheckValue']").remove();
						$("#insertDataMapping").find("[name='groupCheckValue']").remove();
						$("#insertDataMapping").find("[name='roleCheckValue']").remove();
						$("#insertDataMapping").find("[name='userCheckName']").remove();
						$("#insertDataMapping").find("[name='groupCheckName']").remove();
						$("#insertDataMapping").find("[name='roleCheckName']").remove();
					}else{
						$i.dialog.alert("SYSTEM", data.returnMessage, function(){
							$("#txtDataUserSearchSelectNo").val("")
							$("#txtDataUserSearchSelect").val("");
							$("#txtDataGroupSearchNoSelect").val("");
							$("#txtDataGroupSearchSelect").val("");
							$("#txtDataRoleSearchNoSelect").val("");
							$("#txtDataRoleSearchSelect").val("");
							$("#insertDataMapping").find("[name='userCheckValue']").remove();
							$("#insertDataMapping").find("[name='groupCheckValue']").remove();
							$("#insertDataMapping").find("[name='roleCheckValue']").remove();
							$("#insertDataMapping").find("[name='userCheckName']").remove();
							$("#insertDataMapping").find("[name='groupCheckName']").remove();
							$("#insertDataMapping").find("[name='roleCheckName']").remove();
						});
					}
				});
			}
			<%-- 필터 --%>
			function setFilter(gridelement, prop) {
				gridelement.jqxGrid('clearfilters');
				
				var filtervalue = prop.filtervalue;
				
				var datafield = prop.datafield;
				var filtergroup = new $.jqx.filter();
				var filter = filtergroup.createfilter('stringfilter', filtervalue, 'CONTAINS');
				filtergroup.addfilter(1, filter);
				gridelement.jqxGrid('addfilter', datafield, filtergroup);
				gridelement.jqxGrid('applyfilters');
			}
			<%-- 트리에 아이콘 --%>
			function getIconImage(MENU_TYPE, MENU_TYPE, MENU_OPEN_TYPE) {
				var icon = '';
				
				if(MENU_TYPE!=null&&MENU_TYPE=="2") {
					//파일
					if(MENU_OPEN_TYPE == 'common') {
						icon = '../../resources/css/images/img_icons/permission/pageIcon_lock.png';
					}else{
						icon = '../../resources/css/images/img_icons/permission/pageIcon.png';
					}
				} else {
					//폴더
					if(MENU_OPEN_TYPE == 'common') {
						icon = '../../resources/css/images/img_icons/permission/folderIcon_lock.png';
					}else {
						icon = '../../resources/css/images/img_icons/permission/folderIcon.png';
					}
				}
				return icon;
			}
			<%-- tree 에서 PID 가 사라져, ROOT 에 표시되는 메뉴 제거 --%>
			function deleteNullMenu(treegrid) {
				var rows = treegrid.jqxTreeGrid('getRows');
				var length = rows.length;
				for(var i=length-1; i>=0; i--) {
					var row = rows[i];
					if(row.level == 0 && row.PID != '5999') {
						var key = treegrid.jqxTreeGrid('getKey', row);
						treegrid.jqxTreeGrid('deleteRow', key);
					}
				}
			}
			<%-- 롤 관리 (저장, 삭제) --%>
			function replaceAll(str,orgStr,repStr){
			    return str.split(orgStr).join(repStr);
			}
			function moveApply(source,target){
				var sourceIndex = $('#' + source).jqxGrid('getselectedrowindexes');
				var sourceIndexRowData = new Array();
				var sourceDeleteIndexs = new Array();
				
				for(var i=0; i<sourceIndex.length; i++){
					var sourceItem = $('#' + source).jqxGrid('getrowdata', sourceIndex[i]);
					var id = $('#'+source).jqxGrid('getrowid', sourceIndex[i]);
					sourceIndexRowData.push(sourceItem);
					sourceDeleteIndexs.push(id);
				}
				
				if(sourceIndex == -1){
					alert("Role을 선택하세요.");
				}else{
					var commit = $('#'+source).jqxGrid('deleterow', sourceDeleteIndexs);
					$('#'+target).jqxGrid('addrow', null, sourceIndexRowData);
					$('#'+target).jqxGrid('refreshdata');
					$('#'+source).jqxGrid('refreshdata');
				}
			}
			function moveCancel(source){
				var sourceIndex = $('#' + source).jqxGrid('getselectedrowindexes');
				var sourceIndexRowData = new Array();
				var sourceDeleteIndexs = new Array();
				
				for(var i=0; i<sourceIndex.length; i++){
					var sourceItem = $('#' + source).jqxGrid('getrowdata', sourceIndex[i]);
					var id = $('#'+source).jqxGrid('getrowid', sourceIndex[i]);
					sourceIndexRowData.push(sourceItem);
					sourceDeleteIndexs.push(id);
				}
				
				if(sourceIndex == -1){
					alert("Role을 선택하세요.");
				}else{
					var commit = $('#'+source).jqxGrid('deleterow', sourceDeleteIndexs);
					$('#'+source).jqxGrid('refreshdata');
				}
			}
    </script>
    <style type="text/css">
	 	/*
	 	#treegridRoleMenuList .jqx-widget-header-blueish { 
	    		display : none !important; 
	 	}  
	 	#treegridGroupMenuList .jqx-widget-header-blueish { 
	    		display : none !important; 
	 	}  
	 	#treegridMenuList .jqx-widget-header-blueish { 
	    		display : none !important; 
	 	}*/
	 	.jqx-tree-item-li-blueish-system{ 
    		margin-top:0px;
	    }
	    .jqx-tree-item-blueish-system img{
			width:inherit;
			height:inherit;
		}
		.treegrid .jqx-cell-empty-style{
			padding:3px 4px !important;
		}
		.jqx-tree-item-blueish-system {
		    font-size: 11px;
		    padding: 2px !important;
		    vertical-align: middle;
		}
	    .jqx-grid-header-empty-style {
	    	display : none !important;
	    }
		.jqx-icon-arrow-down-blueish-system, .jqx-icon-arrow-down-blueish-system {
			background: url( ../../resources/css/images/img_icons/simple-blue-downarrow.png) center no-repeat !important;
		}
		.jqx-icon-arrow-up-blueish-system, .jqx-icon-arrow-up-blueish-system {
			background: url( ../../resources/css/images/img_icons/simple-blue-uparrow.png) center no-repeat !important;
		}
		/*추가*/
		.i-DQ .label2-bg {
			background:url(../../resources/css/images/img_icons/icon_sqr_arrow_blue.png) left 7px no-repeat !important;
			font-size: 12px !important;
			padding-left: 15px;
			padding-top: 0px;
			line-height: 24px !important;
			color: #323232!important;
			margin-right: 2px;
			font-weight: bold;
		}
		.i-DQ .label3-bg {
			background:none !important;
			font-size: 12px !important;
			padding-left: 5px;
			padding-top: 0px;
			line-height: 24px !important;
			color: #3972b4!important;
			margin-right: 0px;
			font-weight: bold;
		}
		.i-DQ .label4-bg {
			background:none !important;
			font-size: 12px !important;
			padding-left: 5px;
			padding-top: 0px;
			line-height: 24px !important;
			color: #000000 !important;
			margin-right: 0px;
			font-weight: bold;
		}
		/*content > input */
		.code_box_input {
			height:19px;
			float:left;
			margin-right:0px;
			margin-top:0px;
			margin-left:0px;
		}
		.code_box_input input.input_stA {
			width: 100%;
			background: #ffffff;
			border: 1px solid #aaa;
			border-radius: 3px;
			-moz-border-radius: 3px;
			-ms-border-radius: 3px;
			-o-border-radius: 3px;
			-webkit-border-radius: 3px;
			text-indent: 5px;
			font-size: 12px;
			color: #303030;
			padding-bottom: 2px;
		}
		/*content > combobox*/
		.con_combo {
			border-color:#aaa !important;
		}
		.con_combo .jqx-combobox-state-normal {
			border:1px solid #4271ac !important;
			border-radius:3px;
			-ms-border-radius:3px;
			-o-border-radius:3px;
			-webkit-order-radius:3px;
			-moz-border-radius:3px;
			border-color:#43536A !important;
		}
		.con_combo .jqx-combobox-arrow-normal, .con_combo .jqx-action-button {
			background:#ffffff;
			border-color:#ffffff !important;
		}
		.con_combo .jqx-combobox-content {
			background:#ffffff;
			border-color:#ffffff !important;
		}
		.con_combo .jqx-combobox-input {
			color: #171819;
			font-size:12px;
			text-shadow: none;
			background-color:#ffffff;
			background-image:none;
			text-indent:5px;
			vertical-align:middle !important;
			margin-top:0px !important;
		}
		.con_combo .jqx-icon-arrow-down {
			background:url(../../resources/css/images/img_icons/simple-arrow-down-icon.png) no-repeat center !important;
		}
		/*tree01*/
		#jqxTree01 .jqx-widget-content-blueish-system {
			background:#eee;
		}
		/*tree*/
		.jqx-widget-content-blueish-system {
			background:#f6f9fe;
		}
		/*groupbox*/
		.group_box_label .group_box_title {
			float:left;
			text-align:center;
			width:25%;
			margin-right:0%;
			font-size:12px;
			color:#303030;
			margin:10px 0;
			background:url(../../resources/css/images/img_icons/label-icon.png) left no-repeat;
		}
		.group_box_label .group_box_input {
			float:left;
			width:70%;
			margin:8px 0;
		}
		.group_box_label .group_box_input input.input_stA {
			width:100%;
			background:#f9f9f9;
			border:1px solid #9DA1A4;
			border-radius:3px;
			-moz-border-radius:3px;
			-ms-border-radius:3px;
			-o-border-radius:3px;
			-webkit-border-radius:3px;
			text-indent:5px;
			font-size:12px;
			color:#202020;
		}
		.jqx-grid-content .jqx-grid-content-empty-style .jqx-widget-content .jqx-widget-content-empty-style{
			height:100%;
		}
		.jqx-tree-grid-checkbox-empty-style {
		   	 float: none !important;
		   	 width: 13px; /*수정(기존 14px)*/
		   	 height: 13px; /*수정(기존 14px)*/
		     cursor: pointer;
		     margin-right: 2px !important;
		     margin-left: 2px !important;
		}
		.treegrid .jqx-cell-empty-style {
		   	padding: 4px 4px !important;
		}
	</style>
	</head>
	<body class='default i-DQ' style="overflow:auto !important;">
		<div class="wrap" style="width:98% !important; margin:0 10px !important; min-width:1656px;">
			<div class="content f_left" style=" width:100%; margin: 10px 0;">
				<input type="hidden" id="saveCid" name="saveCid" />
				<input type="hidden" id="savePid" name="savePid" />
				<input type="hidden" id="saveGid" name="saveGid" />
				<input type="hidden" id="saveEmpId" name="saveEmpId" />
				<input type="hidden" id="saveBID" name="saveBID" />
				<div class="iwidget_label" style=" float:right; padding:5px 0; position:absolute; right:0px; margin:0;">
					<div class="label-bg" style="float:left; margin-left:0; background:none;"><img src="../../resources/cmresource/image/pageIcon.png" alt="보고서" style="margin:0 2px;">&sbquo;<img src="../../resources/css/images/img_icons/folderIcon.png" alt="폴더" style="margin:0 2px;">:<span class="colorchange" style="font-weight:normal !important;">전체공개 메뉴는 권한부여를 할 수 없습니다.</span></div>
				</div>
				<div class="tab_Area" style="width:100%; height:100%; float:left; margin-top:0px;">
					<input type="hidden" id="hiddenSaveGid" name="saveGid" />
					<input type="hidden" id="hiddenSavePid" name="savePid" />
					<input type="hidden" id="hiddenSaveCid" name="savePid" />
					<input type="hidden" id="hiddenSaveBid" name="saveBid" />
					<div id='jqxTabs'> 
						<ul>
							<li style="margin-left: 0px;"  onclick="makeRoleData();">Role</li>
							<li onclick="makeGroupData();">그룹</li>
							<li onclick="makeMenuData();">보고서</li>
							<li onclick="makeDataData();">데이터</li>
						</ul>
						<!-- Role Tab -->
						<div style="height:650px !important;background:#fff;border-top:1px solid #91A3B4; position:relative;  overflow:hidden; ">
							<div class="tab_con"  style="width:100%; height:100%;  margin:0px 0%; background:#fff; ">
								<div class="divi" style="float:left; width:300px; margin:0; padding:0 10px;  background:#eee; height:660px;">
									<div class="iwidget_label" style="margin:10px 0 0 0 !important; width:100%;  float:left;">
										<div class="label4-bg" style='float:left; margin:0 5px; margin-left:0px;' >Role</div>
										<div class="code_box_input" style="float:left;">
											<input type="text" id="txtRoleSearch" class="input_stA"  style="width:140px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {makeRoleData(); return false;}"/>
											<a href="javascript:makeRoleData();"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a>
										</div>
									</div>
									<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:525px;">
										<div id="gridRoleList"></div>
									</div>
									<div class="group_box_label" >
										<p class="group_box_title"> Role명 </p>
										<p class="group_box_input">
											<input type="hidden" id="hiddenAddRoleID" />
											<input type="hidden" id="hiddenAddRoleName" />
											<input type="text" class="input_stA" id="txtAddRoleName" />
										</p>
										<div class="button-bottom" style="float:right;">
											<div class=" buttonStyle" style="float:left; margin-right:5px;">
												<input type="button" value="초기화" id='btnRoleNameReset' width="100%" height="100%" onclick="roleNameReset();" />
											</div>
											<div class=" buttonStyle" style="float:left; margin-right:5px; ">
												<input type="button" value="저장" id='btnRoleNameSave' width="100%" height="100%" onclick="roleNameInsert();"/>
											</div>
											<div class=" buttonStyle_deldete" style="float:left;  margin-right:5px;">
												<input type="button" value="삭제" id='btnRoleNameDelete' width="100%" height="100%" onclick="roleNameDelete();" />
											</div>
										</div>
									</div>
								</div>
								<div class="divi" style="float:left;width:80%; padding:0%; margin:0px 0%; background:#fff;">
									<div class="iwidget_label" style="margin:10px 0 0 0; width:100%; padding:5px 0; float:left; border-bottom:1px solid #ccc;">
										<div class="label-bg label-2" style="float:left; margin-left:2%;">Role:<span class="colorchange" id="labelRoleName"></span></div>
										<div class="button-bottom" style="float:right;margin-right:2%;">
											<div class=" buttonStyle" style="float:left; ">
												<input type="button" value="저장" id='btnRoleInsert' width="100%" height="100%" style="margin:0 !important;" onclick="roleMappingInsert();"/>
											</div>
										</div>
									</div>
									<div style="width:96%; float:right; margin:10px 2%;">
										<div class="divi" style="float:left;width:25%; background:#fff; padding:0px 0%;">
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:30px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">권한이 부여된 보고서</div>
											</div>
											<div style=" border:1px solid #a7b5c3;padding:0px 0;background:#f6f9fe; width:100%; margin-left:0%; height:552px !important; overflow:auto;" id='treegridRoleMenuList' class=''></div>
										</div>
										<div class="divi" style="float:left;width:36%; margin:0 2%; background:#fff; padding:0px 0%;">
											<!-- 전채 사용자 -->
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">전체 사용자<span style="font-size:11px !important; padding-left:20px; color:#606060; margin-left;20px; font-weight:normal;"><img src="../../../resources/cmresource/image/icon-Comment-blue.png" alt="search" title="검색" style="margin:0; padding:0;"/>조회시 리스트가 보입니다.</span></div>
												<div class="con_combo" style='float:left; margin:2px 10px 0px 5px;' id='cboRoleSearch' ></div>
												<div class="code_box_input" style="float:left;">
													<input type="text" id="txtRoleUserSearchSelectNo" class="input_stA"  style="width:130px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {roleUserNoGrid(''); return false;}"/>
													<a href="javascript:roleUserNoGrid('');"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a>
												</div>
												<div class="button-bottom" style="float:right; margin-right:2%;">
													<div class="buttonStyle" style="float:left;">
														<input type="button" value="권한적용" name="btnApply" width="100%" height="100%" style="margin:0 !important;" onclick="moveApply('gridRoleUserListNoSelect','gridRoleUserListSelect');"/>
													</div>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridRoleUserListNoSelect"></div>
											</div>
											<!-- 선택되 사용자 -->
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">Role에 속한 사용자</div>
												<div class="con_combo" style='float:left; margin:2px 10px 0px 5px;' id='cboRoleSearchSelect' ></div>
												<div class="code_box_input" style="float:left;">  
													<input type="text" id="txtRoleUserSearchSelect" class="input_stA"  style="width:130px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {roleUserGrid(''); return false;}"/>
													<a href="javascript:roleUserGrid('');"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a>
												</div>
												<div class="button-bottom" style="float:right;margin-right:2%;">
													<div class=" buttonStyle_deldete" style="float:left; ">
														<input type="button" value="권한해제" name='btnCancel' width="100%" height="100%" style="margin:0 !important;" onclick="moveApply('gridRoleUserListSelect','gridRoleUserListNoSelect');"/>
													</div>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridRoleUserListSelect"></div>
											</div>
											
										</div>
										<div class="divi" style="float:left;width:35%;  background:#fff; padding:0px 0%;">
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">전체 그룹<span style="font-size:11px !important; padding-left:20px; color:#606060; margin-left;20px; font-weight:normal;"><img src="../../../resources/cmresource/image/icon-Comment-blue.png" alt="search" title="검색" style="margin:0; padding:0;"/>조회시 리스트가 보입니다.</span></div>
												<div class="label3-bg" style='float:left; margin:0 10px; margin-left:10px;' ><img src="../../resources/css/images/img_icons/icon_group.png" alt="icon_role" height="20" style="margin-right:3px;">그룹</div>
												
												<div class="con_combo" style='float:left; margin:2px 10px 0px 5px;' id='cboGrpTypByRoleNoSelect' ></div>
												<div class="code_box_input" style="float:left;">
													<input type="text" id="txtRoleGroupSearchSelectNo" class="input_stA"  style="width:130px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {roleGroupNoGrid(''); return false;}"/>
													<a href="javascript:roleGroupNoGrid('');"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a>
												</div>
												<div class="button-bottom" style="float:right; margin-right:2%;">
													<div class="buttonStyle" style="float:left;">
														<input type="button" value="권한적용" name="btnApply" width="100%" height="100%" style="margin:0 !important;" onclick="moveApply('gridRoleUserGrpListNoSelect','gridRoleUserGrpListSelect');"/>
													</div>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridRoleUserGrpListNoSelect"></div>
											</div>
											
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">Role에 속한 그룹</div>
												<div class="label3-bg" style='float:left; margin:0 10px; margin-left:10px;' ><img src="../../resources/css/images/img_icons/icon_group.png" alt="icon_role" height="20" style="margin-right:3px;">그룹</div>
												<div class="con_combo" style='float:left; margin:2px 10px 0px 5px;' id='cboGrpTypByRoleSelect' ></div>
												<div class="code_box_input" style="float:left;">
													<input type="text" id="txtRoleGroupSearch" class="input_stA"  style="width:115px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {roleGroupGrid(); return false;}"/>
													<a href="javascript:roleGroupGrid();"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a>
												</div>
												<div class="button-bottom" style="float:right;margin-right:2%;">
													<div class=" buttonStyle_deldete" style="float:left; ">
														<input type="button" value="권한해제" name='btnCancel' width="100%" height="100%" style="margin:0 !important;" onclick="moveApply('gridRoleUserGrpListSelect','gridRoleUserGrpListNoSelect');"/>
													</div>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridRoleUserGrpListSelect"> </div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<!-- 그룹 TAB -->
						<div style="height:650px !important;background:#fff;border-top:1px solid #91A3B4; position:relative;  overflow:hidden; ">
							<div class="tab_con"  style="width:100%; height:100%;  margin:0px 0%; background:#fff; ">
								<div class="divi" style="float:left; width:300px; padding:0 10px; background:#eee; height:660px; ">
									<div class="iwidget_label" style="margin:10px 0 0 0 !important; width:100%;  float:left;">
										<div class="label4-bg" style='float:left; margin:0 5px; margin-left:0px;' >그룹</div>
										<div class="code_box_input" style="float:left;">
											<div class="combobox" style='float:left; margin:2px 5px 0px 0px;' id='cboGrpTyp' ></div>
											<input type="text" id="txtGroupSearch" class="input_stA"  style="width:140px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {makeGroupData(); return false;}"/>
											<a href="javascript:makeGroupData();"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a>
										</div>
									</div>
									<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:594px;">
										<div id="gridGroupList"></div>
									</div>
								</div>
								<div class="divi" style="float:left;width:80%; padding:0%; margin:0px 0%; background:#fff;">
									<div class="iwidget_label" style="margin:10px 0 0 0; width:100%; padding:5px 0; float:left; border-bottom:1px solid #ccc;">
										<div class="label-bg label-2" style="float:left; margin-left:2%;">그룹:<span class="colorchange" id="labelGroupName"></span></div>
										<input type="hidden" id="hiddenAddGroupName"/>
										<div class="button-bottom" style="float:right;margin-right:2%;">
											<div class=" buttonStyle" style="float:left; ">
												<input type="button" value="저장" id='btnGroupInsert' width="100%" height="100%" style="margin:0 !important;" onclick="groupMappingInsert();" />
											</div>
										</div>
									</div>
									<div style="width:96%; float:right; margin:10px 2%;">
										<div class="divi" style="float:left;width:25%; background:#fff; padding:0px 0%;">
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:30px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">권한이 부여된 보고서</div>
											</div>
											<div style=" border:1px solid #a7b5c3;padding:0px 0;background:#f6f9fe; width:100%; margin-left:0%; height:552px !important;overflow:auto;" id="treegridGroupMenuList" class='treegrid'></div>
										</div>
										<div class="divi" style="float:left;width:41%; margin:0 2%; background:#fff; padding:0px 0%;">
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">그룹에 속한 사용자</div>
<!-- 													<div class="label3-bg" style='float:left; margin:0px;' >구분</div> -->
												<div class="con_combo" style='float:left; margin:2px 10px 0px 5px;' id='cboGroupSearch' ></div>
												<div class="code_box_input" style="float:left;">
													<input type="text" id="txtGroupUserSearch" class="input_stA"  style="width:130px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {groupUserGrid(''); return false;}"/>
													<a href="javascript:groupUserGrid('');"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:525px;">
												<div id="gridGroupUserList"></div>
											</div>
										</div>
										<div class="divi" style="float:left;width:30%;  background:#fff; padding:0px 0%;">
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">전체 Role</div>
												<div class="label3-bg" style='float:left; margin:0 10px; margin-left:10px;' ><img src="../../resources/css/images/img_icons/icon_role.png" alt="icon_role" height="20" style="margin-right:3px;">Role</div>
												<div class="code_box_input" style="float:left;">
													<input type="text" id="txtGroupRoleSearchSelectNo" class="input_stA"  style="width:115px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {groupRoleNoGrid(); return false;}"/>
													<a href="javascript:groupRoleNoGrid();"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a>
												</div>
												<div class="button-bottom" style="float:right; margin-right:2%;">
													<div class="buttonStyle" style="float:left;">
														<input type="button" value="권한적용" name="btnApply" width="100%" height="100%" style="margin:0 !important;" onclick="moveApply('gridGroupRoleNoSelect','gridGroupRoleSelect');"/>
													</div>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridGroupRoleNoSelect"></div>
											</div>
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">그룹이 속한 Role</div>
												<div class="label3-bg" style='float:left; margin:0 10px; margin-left:10px;' ><img src="../../resources/css/images/img_icons/icon_role.png" alt="icon_role" height="20" style="margin-right:3px;">Role</div>
												<div class="code_box_input" style="float:left;">
													<input type="text" id="txtGroupRoleSearchSelect" class="input_stA"  style="width:115px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {groupRoleGrid(); return false;}"/>
													<a href="javascript:groupRoleGrid();"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a>
												</div>
												<div class="button-bottom" style="float:right;margin-right:2%;">
													<div class=" buttonStyle_deldete" style="float:left; ">
														<input type="button" value="권한해제" name='btnCancel' width="100%" height="100%" style="margin:0 !important;" onclick="moveCancel('gridGroupRoleSelect');"/>
													</div>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridGroupRoleSelect"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						
						<!--보고서 TAB-->
						<div style="height:650px !important;background:#fff;border-top:1px solid #91A3B4; position:relative;  overflow:hidden; ">
							<div class="tab_con"  style="width:100%; height:100%;  margin:0px 0%; background:#fff; ">
					<!--  -		<div class="divi" style="float:left; width:320px; margin-right:0%;height:100%;"> 
									<div style=" border:0;padding-top:0px;background:#fff; width:100%; margin-left:0%; height:660px !important; overflow:auto;" id='treegridMenuList' class='treegrid'></div>
            					</div>-->
            					<div class="divi" style="float:left; width:300px; padding:0 10px; background:#eee; height:660px; ">
									<div class="iwidget_label" style="margin:10px 0 0 0 !important; width:100%;  float:left;">
										<div class="label4-bg" style='float:left; margin:0 5px; margin-left:0px;' >보고서</div>
										<div class="code_box_input" style="float:left;">
											<div class="combobox" style='float:left; margin:2px 5px 0px 0px;' id='cboGrpTyp' ></div>
											<input type="text" id="txtReportSearch" class="input_stA"  style="width:140px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {addfilter(); return false;}"/>
											<a href="javascript:addfilter();"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a>
										</div>
									</div>
									<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:594px;">
										<div id="treegridMenuList" style="height:594px !important;"></div>
									</div>
								</div>
								<div class="divi" style="float:left;width:80%; padding:0%; margin:0px 0%; background:#fff;">
									<div class="iwidget_label" style="margin:10px 0 0 0; width:100%; padding:5px 0; float:left; border-bottom:1px solid #ccc;">
										<div class="label-bg label-2" style="float:left; margin-left:2%;">보고서&frasl;폴더:<span class="colorchange" id="labelMenuName" ></span></div>
										<input type='hidden' id="hiddenAddMenuName"/>
										<div class="button-bottom" style="float:right;margin-right:2%;">
											<div class=" buttonStyle" style="float:left;">
												<input type="button" value="저장" id='btnMenuInsert' width="100%" height="100%" style="margin:0 !important;" onclick="menuMappingInsert();" />
											</div>
										</div>
									</div>
									<div style="width:96%; float:right; margin:10px 2%;">
										<div class="divi" style="float:left;width:32%; background:#fff; padding:0px 0%;">
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">전체 사용자</div>
													<div class="combobox" style='float:left; margin:2px 10px 0px 5px;' id='cboMenuSearchNoSelect' ></div>
													<div class="code_box_input" style="float:left;">
														<input type="text" id="txtMenuUserSearchSelectNo" class="input_stA"  style="width:140px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {menuUserNoGrid(''); return false;}"/>
														<a href="javascript:menuUserNoGrid('');"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;" /></a> 
													</div>
													<div class="button-bottom" style="float:right; margin-right:2%;">
														<div class="buttonStyle" style="float:left;">
															<input type="button" value="권한적용" name="btnApply" width="100%" height="100%" style="margin:0 !important;" onclick="moveApply('gridMenuUserListNoSelect','gridMenuUserListSelect');"/>
														</div>
													</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridMenuUserListNoSelect"></div>
											</div>
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">권한이 부여된 사용자</div>
												<div class="combobox" style='float:left; margin:2px 10px 0px 5px;' id='cboMenuSearchSelect' ></div>
												<div class="code_box_input" style="float:left;">
													<input type="text" id="txtMenuUserSearchSelect" class="input_stA"  style="width:140px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {menuUserGrid(''); return false;}"/>
													<a href="javascript:menuUserGrid('');"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;" /></a> 
												</div>
												<div class="button-bottom" style="float:right;margin-right:2%;">
													<div class=" buttonStyle_deldete" style="float:left; ">
														<input type="button" value="권한해제" name='btnCancel' width="100%" height="100%" style="margin:0 !important;" onclick="moveApply('gridMenuUserListSelect','gridMenuUserListNoSelect');"/>
													</div>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridMenuUserListSelect"></div>
											</div>
										</div>  
										<div class="divi" style="float:left;width:34%; margin:0 2%; background:#fff; padding:0px 0%;">
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">전체 그룹</div>
												<div class="label3-bg" style='float:left; margin:0 10px; margin-left:5px;' ><img src="../../resources/css/images/img_icons/icon_group.png" alt="icon_group" height="20" style="margin-right:3px;">그룹</div>
												<div class="code_box_input" style="float:left;">
													<div class="combobox" style='float:left; margin:2px 10px 0px 5px;' id='cboGrpTypByReportNoSelect' ></div>
													<input type="text" id="txtMenuGroupSearchNoSelect" class="input_stA"  style="width:140px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {menuGroupNoGrid(''); return false;}"/>
													<a href="javascript:menuGroupNoGrid('');"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a> 
												</div>
												<div class="button-bottom" style="float:right; margin-right:2%;">
													<div class="buttonStyle" style="float:left;">
														<input type="button" value="권한적용" name="btnApply" width="100%" height="100%" style="margin:0 !important;" onclick="moveApply('gridMenuGroupListNoSelect','gridMenuGroupListSelect');"/>
													</div>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridMenuGroupListNoSelect"></div>
											</div>
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">권한이 부여된 그룹</div>
												<div class="label3-bg" style='float:left; margin:0 10px; margin-left:5px;' ><img src="../../resources/css/images/img_icons/icon_group.png" alt="icon_group" height="20" style="margin-right:3px;">그룹</div>
												<div class="code_box_input" style="float:left;">
													<div class="combobox" style='float:left; margin:2px 10px 0px 5px;' id='cboGrpTypByReportSelect' ></div>
													<input type="text" id="txtMenuGroupSearchSelect" class="input_stA"  style="width:140px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {menuGroupGrid(''); return false;}"/>
													<a href="javascript:menuGroupGrid('');"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a> 
												</div>
												<div class="button-bottom" style="float:right;margin-right:2%;">
													<div class=" buttonStyle_deldete" style="float:left; ">
														<input type="button" value="권한해제" name='btnCancel' width="100%" height="100%" style="margin:0 !important;" onclick="moveApply('gridMenuGroupListSelect','gridMenuGroupListNoSelect');"/>
													</div>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridMenuGroupListSelect"></div>
											</div>
										</div>
										<div class="divi" style="float:right;width:30%;  background:#fff; padding:0px 0%;">
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">전체 Role</div>
												<div class="label3-bg" style='float:left; margin:0 10px; margin-left:10px;' ><img src="../../resources/css/images/img_icons/icon_role.png" alt="icon_role" height="20" style="margin-right:3px;">Role</div>
												<div class="code_box_input" style="float:left;">
													<input type="text" id="txtMenuRoleSearchNoSelect" class="input_stA"  style="width:140px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {menuRoleNoGrid(); return false;}"/>
													<a href="javascript:menuRoleNoGrid();"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a>
												</div>
												<div class="button-bottom" style="float:right; margin-right:2%;">
													<div class="buttonStyle" style="float:left;">
														<input type="button" value="권한적용" name="btnApply" width="100%" height="100%" style="margin:0 !important;" onclick="moveApply('gridMenuRoleListNoSelect','gridMenuRoleListSelect');"/>
													</div>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridMenuRoleListNoSelect"></div>
											</div>
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">권한이 부여된 Role</div>
												<div class="label3-bg" style='float:left; margin:0 10px; margin-left:10px;' ><img src="../../resources/css/images/img_icons/icon_role.png" alt="icon_role" height="20" style="margin-right:3px;">Role</div>
												<div class="code_box_input" style="float:left;">
													<input type="text" id="txtMenuRoleSearchSelect" class="input_stA"  style="width:140px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {menuRoleGrid(); return false;}"/>
													<a href="javascript:menuRoleGrid();"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a>
												</div>
												<div class="button-bottom" style="float:right;margin-right:2%;">
													<div class=" buttonStyle_deldete" style="float:left; ">
														<input type="button" value="권한해제" name='btnCancel' width="100%" height="100%" style="margin:0 !important;" onclick="moveApply('gridMenuRoleListSelect','gridMenuRoleListNoSelect');"/>
													</div>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridMenuRoleListSelect"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<!--데이터-->
						<div style="height:650px !important;background:#fff;border-top:1px solid #91A3B4; position:relative;  overflow:hidden; ">
							<div class="tab_con"  style="width:100%; height:100%;  margin:0px 0%; background:#fff; ">
								<div class="divi" style="float:left; width:300px; margin:0; padding:0 10px;  background:#eee; height:660px;">
									<div class="iwidget_label" style="margin:10px 0 0 0 !important; width:100%;  float:left;">
										<div class="label4-bg" style='float:left; margin:0 5px; margin-left:0px;' >Data</div>
										<div class="code_box_input" style="float:left;">
											<input type="text" id="txtDataSearch" class="input_stA"  style="width:140px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {makeDataData(); return false;}"/>
											<a href="javascript:makeRoleData();"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a>
										</div>
									</div>
									<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:525px;">
										<div id="gridDataList"></div>
									</div>
									<div class="group_box_label" >
										<p class="group_box_title"> Data명 </p>
										<p class="group_box_input">
											<input type="hidden" id="hiddenAddDataID" />
											<input type="hidden" id="hiddenAddDataName" />
											<input type="text" class="input_stA" id="txtAddDataName" />
										</p>
										<div class="button-bottom" style="float:right;">
											<div class=" buttonStyle" style="float:left; margin-right:5px;">
												<input type="button" value="초기화" id='btnDataNameReset' width="100%" height="100%" onclick="dataNameReset();" />
											</div>
											<div class=" buttonStyle" style="float:left; margin-right:5px; ">
												<input type="button" value="저장" id='btnDataNameSave' width="100%" height="100%" onclick="dataNameInsert();"/>
											</div>
											<div class=" buttonStyle_deldete" style="float:left;  margin-right:5px;">
												<input type="button" value="삭제" id='btnDataNameDelete' width="100%" height="100%" onclick="dataNameDelete();" />
											</div>
										</div>
									</div>
								</div>
								<div class="divi" style="float:left;width:80%; padding:0%; margin:0px 0%; background:#fff;">
									<div class="iwidget_label" style="margin:10px 0 0 0; width:100%; padding:5px 0; float:left; border-bottom:1px solid #ccc;">
										<div class="label-bg label-2" style="float:left; margin-left:2%;">데이터:<span class="colorchange" id="labelDataName"></span></div>
										<div class="button-bottom" style="float:right;margin-right:2%;">
											<div class=" buttonStyle" style="float:left;">
												<input type="button" value="저장" id='btnDataInsert' width="100%" height="100%" style="margin:0 !important;" onclick="dataMappingInsert();" />
											</div>
										</div>
									</div>
									<div style="width:96%; float:right; margin:10px 2%;">
										<div class="divi" style="float:left;width:32%; background:#fff; padding:0px 0%;">
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">전체 사용자</div>
												<div class="combobox" style='float:left; margin:2px 10px 0px 5px;' id='cboDataSearchNoSelect' ></div>
												<div class="code_box_input" style="float:left;">
													<input type="text" id="txtDataUserSearchSelectNo" class="input_stA"  style="width:140px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {dataUserNoGrid(''); return false;}"/>
													<a href="javascript:dataUserNoGrid('');"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;" /></a> 
												</div>
												<div class="button-bottom" style="float:right; margin-right:2%;">
													<div class="buttonStyle" style="float:left;">
														<input type="button" value="권한적용" name="btnApply" width="100%" height="100%" style="margin:0 !important;" onclick="moveApply('gridDataUserListNoSelect','gridDataUserListSelect');"/>
													</div>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridDataUserListNoSelect"></div>
											</div>
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">권한이 부여된 사용자</div>
												<div class="combobox" style='float:left;margin:2px 10px 0px 5px;' id='cboDataSearchSelect' ></div>
												<div class="code_box_input" style="float:left;">
													<input type="text" id="txtDataUserSearchSelect" class="input_stA"  style="width:140px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {dataUserGrid(''); return false;}"/>
													<a href="javascript:menuUserGrid('');"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;" /></a> 
												</div>
												<div class="button-bottom" style="float:right;margin-right:2%;">
													<div class=" buttonStyle_deldete" style="float:left; ">
														<input type="button" value="권한해제" name='btnCancel' width="100%" height="100%" style="margin:0 !important;" onclick="moveApply('gridDataUserListSelect','gridDataUserListNoSelect');"/>
													</div>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridDataUserListSelect"></div>
											</div>
										</div>  
										<div class="divi" style="float:left;width:34%; margin:0 2%; background:#fff; padding:0px 0%;">
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">전체 그룹</div>
												<div class="label3-bg" style='float:left; margin:0 10px; margin-left:5px;' ><img src="../../resources/css/images/img_icons/icon_group.png" alt="icon_group" height="20" style="margin-right:3px;">그룹</div>
												<div class="code_box_input" style="float:left;">
													<div class="combobox" style='float:left; margin:0 10px; margin-left:15px;' id='cboGrpTypByReportNoSelect' ></div>
													<input type="text" id="txtDataGroupSearchNoSelect" class="input_stA"  style="width:140px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {dataGroupNoGrid(''); return false;}"/>
													<a href="javascript:dataGroupNoGrid('');"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a> 
												</div>
												<div class="button-bottom" style="float:right; margin-right:2%;">
													<div class="buttonStyle" style="float:left;">
														<input type="button" value="권한적용" name="btnApply" width="100%" height="100%" style="margin:0 !important;" onclick="moveApply('gridDataGroupListNoSelect','gridDataGroupListSelect');"/>
													</div>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridDataGroupListNoSelect"></div>
											</div>
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">권한이 부여된 그룹</div>
												<div class="label3-bg" style='float:left; margin:0 10px; margin-left:5px;' ><img src="../../resources/css/images/img_icons/icon_group.png" alt="icon_group" height="20" style="margin-right:3px;">그룹</div>
												<div class="code_box_input" style="float:left;">
													<div class="combobox" style='float:left; margin:0 10px; margin-left:15px;' id='cboGrpTypByReportSelect' ></div>
													<input type="text" id="txtDataGroupSearchSelect" class="input_stA"  style="width:140px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {dataGroupGrid(''); return false;}"/>
													<a href="javascript:dataGroupGrid('');"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a> 
												</div>
												<div class="button-bottom" style="float:right;margin-right:2%;">
													<div class=" buttonStyle_deldete" style="float:left; ">
														<input type="button" value="권한해제" name='btnCancel' width="100%" height="100%" style="margin:0 !important;" onclick="moveApply('gridDataGroupListSelect','gridDataGroupListNoSelect');"/>
													</div>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridDataGroupListSelect"></div>
											</div>
										</div>
										<div class="divi" style="float:right;width:30%;  background:#fff; padding:0px 0%;">
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">전체 Role</div>
												<div class="label3-bg" style='float:left; margin:0 10px; margin-left:10px;' ><img src="../../resources/css/images/img_icons/icon_role.png" alt="icon_role" height="20" style="margin-right:3px;">Role</div>
												<div class="code_box_input" style="float:left;">
													<input type="text" id="txtDataRoleSearchNoSelect" class="input_stA"  style="width:140px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {dataRoleNoGrid(); return false;}"/>
													<a href="javascript:dataRoleNoGrid();"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a>
												</div>
												<div class="button-bottom" style="float:right; margin-right:2%;">
													<div class="buttonStyle" style="float:left;">
														<input type="button" value="권한적용" name="btnApply" width="100%" height="100%" style="margin:0 !important;" onclick="moveApply('gridDataRoleListNoSelect','gridDataRoleListSelect');"/>
													</div>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridDataRoleListNoSelect"></div>
											</div>
											<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
												<div class="label2-bg" style="float:left; width:100%;">권한이 부여된 Role</div>
												<div class="label3-bg" style='float:left; margin:0 10px; margin-left:10px;' ><img src="../../resources/css/images/img_icons/icon_role.png" alt="icon_role" height="20" style="margin-right:3px;">Role</div>
												<div class="code_box_input" style="float:left;">
													<input type="text" id="txtDataRoleSearchSelect" class="input_stA"  style="width:140px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {dataRoleGrid(); return false;}"/>
													<a href="javascript:datRoleGrid();"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a>
												</div>
												<div class="button-bottom" style="float:right;margin-right:2%;">
													<div class=" buttonStyle_deldete" style="float:left; ">
														<input type="button" value="권한해제" name='btnCancel' width="100%" height="100%" style="margin:0 !important;" onclick="moveApply('gridDataRoleListSelect','gridDataRoleListNoSelect');"/>
													</div>
												</div>
											</div>
											<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:220px;">
												<div id="gridDataRoleListSelect"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<form id="roleInfoSaveForm" name="roleInfoSaveForm" action="./insertRoleInfo">
			<input type="hidden" id="hiddenRoleID" name="roleID" />
			<input type="hidden" id="hiddenRoleName" name="roleName" />
			<input type="hidden" id="hiddenRoleNameOrg" name="roleNameOrg" />
			
			<input type="hidden" id="hiddenRemoteIP01" name="remoteIP" />
			<input type="hidden" id="hiddenRemoteOS01" name="remoteOS" />
			<input type="hidden" id="hiddenRemoteBW01" name="remoteBrowser" />
			
		</form>
		<form id="insertRoleMapping" name="insertRoleMapping" action="./insertMappingRole">
			<input type="hidden" id="mappingRoleID" name="roleID" />
			<input type="hidden" id="mappingRoleName" name="roleName" />
			
			<input type="hidden" id="hiddenRemoteIP02" name="remoteIP" />
			<input type="hidden" id="hiddenRemoteOS02" name="remoteOS" />
			<input type="hidden" id="hiddenRemoteBW02" name="remoteBrowser" />
		</form>
		<form id="insertGroupMapping" name="insertGroupMapping" action="./insertMappingGroup">
			<input type="hidden" id="mappingGroupID" name="groupID" />
			<input type="hidden" id="mappingGroupName" name="groupName" />
			
			<input type="hidden" id="hiddenRemoteIP03" name="remoteIP" />
			<input type="hidden" id="hiddenRemoteOS03" name="remoteOS" />
			<input type="hidden" id="hiddenRemoteBW03" name="remoteBrowser" />
		</form>
		<form id="insertMenuMapping" name="insertMenuMapping" action="./insertMappingMenu">
			<input type="hidden" id="mappingMenuID" name="menuID" />
			<input type="hidden" id="mappingMenuName" name="menuName" />
			
			<input type="hidden" id="hiddenRemoteIP04" name="remoteIP" />
			<input type="hidden" id="hiddenRemoteOS04" name="remoteOS" />
			<input type="hidden" id="hiddenRemoteBW04" name="remoteBrowser" />
		</form>
		<form id="dataInfoSaveForm" name="dataInfoSaveForm" action="./insertDataInfo">
			<input type="hidden" id="hiddenDataID" name="dataID" />
			<input type="hidden" id="hiddenDataName" name="dataName" />
			<input type="hidden" id="hiddenDataNameOrg" name="dataNameOrg" />
			
			<input type="hidden" id="hiddenRemoteIP05" name="remoteIP" />
			<input type="hidden" id="hiddenRemoteOS05" name="remoteOS" />
			<input type="hidden" id="hiddenRemoteBW05" name="remoteBrowser" />
		</form>
		<form id="insertDataMapping" name="insertDataMapping" action="./insertMappingData">
			<input type="hidden" id="mappingDataID" name="dataID" />
			<input type="hidden" id="mappingDataName" name="dataName" />
			
			<input type="hidden" id="hiddenRemoteIP06" name="remoteIP" />
			<input type="hidden" id="hiddenRemoteOS06" name="remoteOS" />
			<input type="hidden" id="hiddenRemoteBW06" name="remoteBrowser" />
		</form>
	</body>
</html>