<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>도움말관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/thirdparty/LGPL/v2/smartEditor2/js/HuskyEZCreator.js" charset="UTF-8"></script>
        <script type="text/javascript">
        var dummycount = 1;
    	var curLeftMenu=null;
    	var ajaxSignal = ".."; 
    	var datafields = [
    		{name:'C_ID', type: 'string'},
    		{name:'P_ID', type: 'string'},
    		{name:'C_NAME', type: 'string'},
    		{name:'P_NAME', type: 'string'}, 
    		{name:'PROG_ID', type: 'string'},
    		{name:'SOURCE_OWNER', type: 'string'}
    	];
    	$(document).ready(function() {
	    		
    		$('#menuName').jqxInput({placeHolder: "", height: 25, width: 180, minLength: 1}).on('keydown', function() { if(event.keyCode==13) { searchStart(); return false;}});
    		$('#searchButton').jqxButton({ width: '',  theme:'blueish'}).on('click', function() { searchStart(); });
    		$('#saveButton').jqxButton({ width: '',  theme:'blueish'}).on('click', function() { insert(); });
    		$("#menuName").jqxInput({placeHolder: "", height: 25, width: 250, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return searchStart(); } });
    		$('#jqxTabs').jqxTabs({ width: '100%', height: '100%', position: 'top', theme:'blueish', selectedItem: 0});
    		$('#treeSaveButton').jqxButton({ width: '',  theme:'blueish'}).on('click', function() { saveInformation(); });
    		
    		makeSearchTypeCombobox(); //검색타입 : id, name   
    		
    		initSmartEditor("content");   
    		//splitter
    		$('#splitter').jqxSplitter({ height: '100%', width: '100%',  panels: [{ size: '23%' }, { size: '77%'}], theme:"blueish"  });
	    		
    		$("#feedExpander").jqxExpander({toggleMode: 'none', theme:"blueish" , showArrow: false, width: "100%", height: "100%", 
                 initContent: function () {
                     $('#treeHelpMange').jqxTree({ height: '100%', width: '100%', theme:"blueish"  });
                 }
             });
    		$("#updatePer").keyup(function(){
    			//숫자만 입력받기      
    			$(this).val( $(this).val().replace(/[^0-9]/g,"") );
    		});     

	    		
    	});
        
       	var oEditors = [];

		function initSmartEditor(id) {
			nhn.husky.EZCreator.createInIFrame({
				oAppRef : oEditors,
				elPlaceHolder : id,
				sSkinURI : "../../resources/cmresource/js/thirdparty/LGPL/v2/smartEditor2/SmartEditor2Skin.html",
				htParams : {
					bUseToolbar : true,
					fOnBeforeUnload : function() {
					}
				},
				fOnAppLoad : function() {
					//oEditors.getById["ir1"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
				},
				fCreator : "createSEditor2"
			});
		}
       	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
       	function makeSearchTypeCombobox() {
			var res = [
				{COM_ID:'C_NAME', COM_NM:'<spring:message code="helpManager.list.menuName" />'},
				{COM_ID:'P_NAME', COM_NM:'<spring:message code="helpManager.list.upperName" />'}
			];
			
			makeCombobox($('#searchType'), res, 100);
		}
		function makeCombobox(comboElement, res, width) {
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: 'COM_ID'},
					{ name: 'COM_NM'}
				],
				id : 'COM_ID',
				localdata: res
			};
			
			var dataAdapter = new $.jqx.dataAdapter(source);	
			comboElement.jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_ID", dropDownWidth: width, width: width, height: 25 ,theme:'blueish'});	
			getMenuList();   
		}
		function getMenuList() {
			var dat = $i.post("./helpManagerMenuListByUser", {});
			dat.done(function(menu){
				makeTree(menu);   
				makeGrid(menu);
			});
		}
		function makeTree(res) {
			var bifolders = Enumerable.From(res.returnArray).Where(function(c){return c.SOURCE_OWNER=="BIFOLDER";}).ToArray();
			   
			var foldersize = bifolders.length;   
			for(var i = 0; i<foldersize;i++){
				
				var dummy = {
						C_ID : 'dummy' + dummycount, 
						P_ID : bifolders[i].C_ID,
						PID :  bifolders[i].C_ID,
						C_NAME : '..',
						NAME : '..', 
						SOURCE_OWNER:null,
						PROG_ID:null,  
						cIcon : "loading.gif"
				};
				dummycount++;
				bifolders.push(dummy);
			}
			//res = obj.res.concat(res);
			res.returnArray = res.returnArray.concat(bifolders);
			
			curLeftMenu=res.returnArray;
			
			
			         
			
			var tree = $('#treeHelpMange');
			var source =
			{
				datatype: "json",
				datafields: datafields,
				id : 'C_ID',
				localdata: res.returnArray
			};
			var dataAdapter = new $.jqx.dataAdapter(source);
			dataAdapter.dataBind();
			
			var records = dataAdapter.getRecordsHierarchy('C_ID', 'P_ID', 'items', [{ name: 'C_ID', map: 'id'} ,{ name: 'C_NAME', map: 'label'}, { name: 'C_NAME', map : 'value'}]);
			tree.jqxTree({source: records, height: '100%', width: '100%', allowDrag:false, allowDrop:false});                     
			
		 	// tree init
			var items = tree.jqxTree('getItems');
			var item = null;
			var size = 0;
			var img = '';
			var label = '';
			var afterLabel = '';
			
			for(var i = 0; i < items.length; i++) {
				item = items[i];
			
				if(i == 0) {
					//root
					tree.jqxTree('expandItem', item);
					img = 'icon-foldernoopen.png';
					
					size = $(item.element).find('li').size();
					
					if(size > 0) {
						//have a child
						afterLabel = "<span style='color: Blue;'> (" + size + ")</span>";
					} else {
						afterLabel = "";	
					}	
					
				} else {
					//children
					size = $(item.element).find('li').size();
					
					if(size > 0) {
						//have a child
						img = 'icon-foldernoopen.png';
						afterLabel = "<span style='color: Blue;'> (" + size + ")</span>";
					} else {
						//no have a child
						img = 'icon-page.png';
						//내부에서 사용하는 트리는 팝업 없음.
						afterLabel = "";
					}
				}
				
				label = "<img style='float: left; margin-right: 5px;' src='../../resources/cmresource/image/" + img + "'/><span item-title='truec style='vertical-align:middle;'>" + item.label + "</span>" + afterLabel;
				tree.jqxTree('updateItem', item, { label: label});
			}
			
			//add event
			tree  
				.on("expand",  {tree:tree},function(event) {
					var args = event.args;
					var tree = event.data.tree;
					var label = tree.jqxTree('getItem', args.element).label.replace('icon-folderopen', 'icon-foldernoopen');
					args.element.firstChild.className = args.element.firstChild.className.replace('jqx-tree-item-arrow-collapse', '').replace('jqx-icon-arrow-right', '');
					tree.jqxTree('updateItem', args.element, { label: label});
					var item = $('#treeHelpMange').jqxTree('getItem', args.element);
					var curMenu = Enumerable.From(curLeftMenu).Where(function(c){return c.C_ID == item.id;}).FirstOrDefault();
					
					 
					if((curMenu.cType!=null&&(curMenu.cType=="cog"||curMenu.cType=="folder"))||(curMenu.SOURCE_OWNER=="BIFOLDER")){  
					 
						
						var label = item.label;
						$("#arrow" + item.id).attr("class", "jqx-icon-arrow-down");
						var tree = $('#treeHelpMange');
						var label = tree.jqxTree('getItem', event.args.element).label;
						var $element = $(event.args.element);
						var loader = false;
						var loaderItem = null;
						var children = $element.find('ul:first').children();
						
						$.each(children, function() {
							var item = tree.jqxTree('getItem', this);
							if (item && item.originalTitle == ajaxSignal) {
								loaderItem = item;
								loader = true;
								return false;
							}
							
						});
						if (loader) {  
							var pmenu = item;//item.id; 
							var reportid = curMenu.PROG_ID!=null?curMenu.PROG_ID:item.id; 
							menuServiceDwr.getMenuByJson(reportid, item.originalTitle, {
								callback : function(res) {
									//curLeftMenu = curLeftMenu.concat(res);
									var folders = Enumerable.From(res).Where(function(c) {
										return c.MENU_TYPE == "1"; 
									}).ToArray();
									for ( var i = 0; i < folders.length; i++) {
											var dummy = {
												C_ID : 'dummy' + dummycount,
												P_ID : folders[i].C_ID,
												C_NAME : ajaxSignal,
												NAME:ajaxSignal,
												cIcon : "loading.gif",
												cType : "dummy",
												SOURCE_OWNER:""
											};
											res.push(dummy); 
											dummycount++;
									}	 
									curLeftMenu = curLeftMenu.concat(res);
									
									var source = {
											datatype : "json",
											datafields : [ 
											    {name : 'C_ID'}, 
											    {name : 'PID' },
											    {name : 'C_NAME'}, 
												{name : 'C_LINK'},
												{name : 'cIcon'},
												{name : 'cType'},
												{name : 'NAME'},
												{name : 'PROG_ID'} ,
												{name : 'SOURCE_OWNER'} 
											],
											id : 'id',
											localdata : res
										};
									var dataAdapter = new $.jqx.dataAdapter(source);
									dataAdapter.dataBind();
									var records = dataAdapter.getRecordsHierarchy('C_ID', 'PID', 'items',
											[ {
												name : 'C_ID',
												map : 'id'
											}, {
												name : 'C_NAME',
												map : 'label'
											}  ]);
									var items = records;
									if(items!=null)
										tree.jqxTree('addTo', items, $element[0]);   
									//$("#"+loaderItem.element.id).empty(); 
									
									tree.jqxTree('removeItem', loaderItem.element);
									var cur = $("#treeHelpMange").jqxTree("getItem",$element[0]);
									pmenu = Enumerable.From(curLeftMenu).Where(function(c){ return c.C_ID == pmenu.id;}).FirstOrDefault();
									renderTreeNode('tree', cur, pmenu); 
									for ( var i = 0; i < res.length; i++) {
										var item = $("#treeHelpMange").jqxTree("getItem",$("#"+res[i].C_ID)[0]);
										renderTreeNode('tree', item, pmenu);                    
									}
								},
								errorHandler : function(msg) {
									alert("일시적 장애로 메뉴를 불러오지 못했습니다. 다시 시도해주세요");
								}
							});
							 
						}
					}
				})
				.on("collapse", function(eve) {
					var args = eve.args;
					var label = tree.jqxTree('getItem', args.element).label.replace('icon-folderopen', 'icon-foldernoopen');
					tree.jqxTree('updateItem', args.element, { label: label});
				})
				.on("select", function(eve) {
					var args = eve.args;
					var element = tree.jqxTree('getItem', args.element);
					resetForm();
					if(element.label.indexOf("folder") > 0){
						$("#saveButton").hide();
						$("#dept")[0].disabled = true;
						$("#confirmDept")[0].disabled = true;
						$("#confirmEmp")[0].disabled = true;
						$("#chargeDept")[0].disabled = true;
						$("#chargeEmp")[0].disabled = true;
						$("#updatePer")[0].disabled = true;
						if(element.label.indexOf("icon-folderopen") > 0){
							tree.jqxTree('collapseItem', $("#"+element.id)[0]);
						}else{
							tree.jqxTree('expandItem', $("#"+element.id)[0]);
						}
					}else{
						$("#saveButton").show();
						$("#dept")[0].disabled = false;
						$("#confirmDept")[0].disabled = false;
						$("#confirmEmp")[0].disabled = false;
						$("#chargeDept")[0].disabled = false;
						$("#chargeEmp")[0].disabled = false;
						$("#updatePer")[0].disabled = false;
						viewDetail(element.id,element.originalTitle);
					}
					
					  
				});
		}
		
		
		function makeGrid(res) {
			var girdElement = $('#helpgrid');
			var source =
			{
				datatype: "json",
				datafields: datafields,
				localdata: res
			};
			
			var dataAdapter = new $.jqx.dataAdapter(source);
		
			//columns
			var nameCellsrenderer = function(row, datafieId, value) {
				var cid = girdElement.jqxGrid("getrowdata", row)['C_ID'];
				var name = girdElement.jqxGrid("getrowdata", row)['NAME'];
				var link = "<a href=\"javaScript:viewDetail('"+cid+"', '"+name+"');\" style='text-decoration:underline;'>" + value + "</a>";
				return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px;'>" + link + "</div>";   
			};
			
			girdElement.jqxGrid({
				width: '100%',
				height: '100%',
				altrows:true,
				pageable: true,
				source: dataAdapter,
				theme:'blueish',
				columnsheight:28,
				pagesize: 100,
				pagesizeoptions:['100', '200', '300'],
				rowsheight: 28,
				columnsresize: true,
				columns: [
					{ text: "상위메뉴 ID", datafield: 'PID', width: '10%', align:'center', cellsalign: 'center'},
					{ text: "상위메뉴명", datafield: 'P_NAME', width: '40%', align:'center', cellsalign: 'center'},
					{ text: "메뉴 ID", datafield: 'C_ID', width: '10%', align:'center', cellsalign: 'center' },
					{ text: "메뉴명", datafield: 'NAME', width: '40%', align:'center', cellsalign: 'center' , cellsrenderer:nameCellsrenderer}
				]
			});
			
			//pager width
			$('#gridpagerlist' + girdElement.attr('id')).width('44px');
			$('#dropdownlistContentgridpagerlist' + girdElement.attr('id')).width('19px');
			viewPage('tableView');
			setPagerLayout("helpgrid");
		}		
		function setPagerLayout(selector) {
			
			var pagesize = $('#'+selector).jqxGrid('pagesize');
			
			var w = 49; 
				
			if(pagesize<100) {
				w = 44;
			} else if(pagesize>99&&pagesize<1000) {
				w = 49;
			} else if(pagesize>999&&pagesize<10000) {
				w = 54;
			}
			
			//디폴트 셋팅
			$('#gridpagerlist'+selector).jqxDropDownList({ width: w+'px' });
			
			//체인지 이벤트 처리
			$('#'+selector).on("pagesizechanged", function (event) {
				var args = event.args;
				
				if(args.pagesize<100) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '44px' });
				} else if(args.pagesize>99 && args.pagesize<1000) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '49px' });
				} else if(args.pagesize>999 && args.pagesize<10000) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '54px' });
				} else {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: 'auto' });
				}
				
			});
		}
		function resetForm(){
			$('#cid').val('');
			$('#formMenuName').text("");
			$('#content').val("");
			
			
			$("#dept").val("");
			$('#chargeDept').val("");
			$('#chargeEmp').val("");
			$("#confirmDept").val("");
			$("#confirmEmp").val("");
			$('#updatePer').val("");
		}
		function viewDetail(cid, name) {
			//init
			$('#cid').val('');
			$('#formMenuName').text("");
			$('#content').val("");
			
			
			$("#dept").val("");
			$('#chargeDept').val("");
			$('#chargeEmp').val("");
			$("#confirmDept").val("");
			$("#confirmEmp").val("");
			$('#updatePer').val("");
			//set
			$('#hiddenCID').val(cid);
			$('#formMenuName').text(name);             
			var naviData = $i.post("./getNaviByHelpManager", {cID:cid});
			naviData.done(function(data){
				var navi = "";
				for(var i = data.length-1; i>=0;i--){
					navi += data[i].C_NAME + ">";
				}
				$("#menuNavi").html(navi + "<span style='font-size:20px;'>"+name+"</span>");
			});
			var helpData = $i.post("./getHelpManagerMenuDetailByUser",{cID:cid});
			helpData.done(function(res){
				$('#cid').val(cid);
				if(res.returnArray.length > 0) {
					$('#content').val(res.returnArray[0].CONTENT);
					
					$('#deptCd').val(res.returnArray[0].DEPT_CD);
					$("#dept").val(res.returnArray[0].DEPT_NM);
					$('#chargeDept').val(res.returnArray[0].CHARGE_DEPT);
					$('#chargeEmp').val(res.returnArray[0].CHARGE_EMP);
					$("#confirmDept").val(res.returnArray[0].CONFIRM_DEPT);
					$("#confirmEmp").val(res.returnArray[0].CONFIRM_EMP);
					$('#updatePer').val(res.returnArray[0].UPDATE_PER);  
					
					oEditors.getById["content"].exec("LOAD_CONTENTS_FIELD");     
				}else{
					oEditors.getById["content"].exec("LOAD_CONTENTS_FIELD");     
				}	
				viewPage('tableView');		
			});
		}
		function searchStart() {
			groupManagerDwr.dwrGetMenuGridList($('#solutionList').val(), $('#menuName').val(), $('#searchType').val(), 'false', function(res) {
				makeGrid(res);
			});
		}
		function insert(){
			if($("#hiddenCID").val() == ""){
				$i.dialog.error("SYSTEM", "보고서를 선택하세요");
				return false;
			}
			if($("#hiddenCID").val() != ""){
				oEditors.getById["content"].exec("UPDATE_CONTENTS_FIELD", []);
				$i.post("./insertByUser", "#saveForm").done(function(args){
					if(args.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", args.returnMessage);
					}else{
						$i.dialog.alert("SYSTEM", args.returnMessage, function(){
							
						});
					}
				});	
			}else{
				$i.dialog.error("SYSTEM","보고서를 선택하세요");
			}
			
		}
		function viewPage(page) {
			if(page == 'gridView') {
				$('#tableView').hide();		
				$('#gridView').show();
			} else {
				$('#gridView').hide();
				$('#tableView').show();
			}
		}
		
		function xmlParser(xml_url, query_string, op, callback) {
			var rtdata = null;
			var xmlRequest = $.ajax({
				url : xml_url,
				data : query_string,
				type : "get",
				async : false,
				cache : false
			});
			var cid = null;
			if (query_string.indexOf("storeId")) {
				cid = query_string.substring(
						query_string.indexOf("storeId") + "storeId".length).split(
						"&")[0].replace(/=/g, "").trim();
			}
			var tmp = new Array();
			xmlRequest.done(function(res) {
				
				if (typeof res == 'string') {
					window.location.reload();
				}
				$(res).find("menu").each(function() {
					/*
					 * id : 본인아이디
					 * pid : 부모아이디
					 * name : 출력명(보고서 이름)
					 * url : 타이틀 클릭시 액션(URL 링크)
					 * icon : 타이틀 앞 아이콘(보고서 종류에따라 아이콘 변경)
					 * copyImg : 내폴더로 아이콘
					 * copyUrl : 내폴더로 아이콘 클릭시 액션
					 */
					var pid = $(this).find("pid").text();
					var uid = $(this).find("uid").text();
					var name = $(this).find("name").text();
					var link = $(this).find("link").text();
					var icon = $(this).find("icon").text();
					var type = $(this).find("type").text();
	
					tmp.push({
						P_ID : pid,
						C_ID : uid,
						C_NAME : name,
						NAME:name,
						PID:pid,
						SORT_ORDER : link,
						cIcon : icon,
						cType : type
					});
	
				});
	
				// end menu loading 
				$(".loader").hide();
	
				if (callback != null)
					callback(tmp, cid);
				else
					rtdata = {
						res : tmp,
						pid : cid
					};
				//(tmp, op);
				
			
			});
			return rtdata;
	
		}
		
		function renderTreeNode(treeid, element, pmenu) {
	
			var icon = "";
			var afterLabel;
	
			if (element.hasItems) {
				//have a child
				if (element.isExpanded != null && element.isExpanded) {
					icon = 'icon-foldernoopen.png';
				} else
					icon = 'icon-folderopen.png';
				afterLabel = "";//자식노드 숫자"<span style='color: Blue;' data-role=='treefoldersize'> (" + $(element.subtreeElement).children().length + ")</span>";
			} else {
				//no have a child
				icon = 'pageIcon.png';
				afterLabel = "";//'&nbsp;<img onclick="setPopup(true);" src="${WWW.CSS}/images/img_icons/popupIcon.png" alt="팝업창" title="새창보기">';
			}
	
			//topmenu ctype이 널이 아니거나 topmenu가 없을 경우..= 코그너스 메뉴이거나 코그너스 탐색기능을 이용한 경우.
			if ((pmenu != null && pmenu.cType != null) || pmenu == null) {
				var menudata = Enumerable.From(curLeftMenu).Where(function(c) {
					return c.C_ID == element.id;
				}).FirstOrDefault(); 
				if(menudata.cIcon.indexOf("folder")!=-1) afterLabel = ""; 
				var iconurl = '${WEB.IMG}' + "/dTree/"
						+ ((menudata != null) ? menudata.cIcon : "page.gif"); 
				if (menudata.cIcon != "loading.gif")
					label = "<img class='menutypeicon' style='float: left; margin-right: 5px;' src='"+iconurl + "'/><span item-title='true' style='width:100px;text-overflow:ellipsis;'  title='"+(element.originalTitle == null ? element.label
							: element.originalTitle)+"'  >"
							+ (element.originalTitle == null ? element.label
									: element.originalTitle)
							+ "</span>"
							+ afterLabel;
				else
					label = "<span item-title='true'  >"
							+ (element.originalTitle == null ? element.label
									: element.originalTitle) + "</span>";
			} else {
				if (icon != "loading.gif")
					label = "<img class='menutypeicon' style='float: left; margin-right: 5px;' src='"+$iv.envinfo.css+"/images/img_icons/" + icon + "'/><span item-title='true'  >"
							+ (element.originalTitle == null ? element.label
									: element.originalTitle)
							+ "</span>"
							+ afterLabel;
				else
					label = "<span item-title='true'  >"
							+ (element.originalTitle == null ? element.label
									: element.originalTitle) + "</span>";
			}
			$("#" + treeid).jqxTree('updateItem', element, {
				label : label
			});
		} 
		function treeInfromation() {
	  		$iv.mybatis("gettreeInfromationList", {ACCT_ID:$iv.userinfo.acctid(), USER_ID:$iv.userinfo.userid(), SOLUTION_NAME:$("#solutionList").val()}, {callback:function(data){
	
	            var source =
	             {     
	                 dataType: "json",
	                 dataFields: [			 
						  	{name:'C_ID', type: 'string'},      
							{name:'P_ID', type: 'string'},
							{name:'NAME', type: 'string'},
							{name:'P_NAME', type: 'string'}, 
							{name:'PROG_ID', type: 'string'},
							{name:'MENU_TYPE', type: 'string'},          
							{name:'SOURCE_OWNER', type: 'string'},
							{name:'CHARGE_DEPT_CD', type: 'string'},
							{name:'CHARGE_DEPT', type: 'string'},
							{name:'CHARGE_EMP', type: 'string'},
							{name:'UPDATE_PER', type: 'string'}
	                 ],
	                 hierarchy:
						{
							keyDataField: { name: 'C_ID' },      
	                 		parentDataField: { name: 'P_ID' }   
						},
						localData: data,             
						id:"id"        
	             };
	
	            var dataAdapter = new $.jqx.dataAdapter(source);
	
	            // create jqxTreeGrid.
	            $("#treegrdInformation").jqxTreeGrid(
	            {
					width:"100%",			
						height: "100%",				
	                	source: dataAdapter,
	                	altRows: true,
						icons: true,
	              		sortable: true,
				  		pageable:true ,
				   		pageSize: 100,
				   		pageSizeOptions: ['100', '200', '300'],
				   		pagerMode: "advanced" ,
				   		editable: true,
				   		editSettings: {
					 		saveOnPageChange: true,
					 		saveOnBlur: true,
					 		saveOnSelectionChange: true,
					 		cancelOnEsc: true,
					 		saveOnEnter: true,
					 		editSingleCell: true,
					 		editOnDoubleClick: true,
					 		editOnF2: true
				    	},
						enableHover: true ,
						columnsHeight: 40,
						theme:"blueish",
					icons:function(rowKey, rowdata){
			     			var icon = ""; 
			     			if(rowdata.MENU_TYPE!=null&&rowdata.MENU_TYPE=="2"){
			     				//파일
			     				icon = '../../resources/cmresource/image/icon-page.png';
			     			}
			     			else{
			     				//폴더
			     				icon = '../../resources/cmresource/image/icon-foldernoopen.png';
			     			}
			     			return icon;      
			     		}, 
					columns: [
	                  { text: '메뉴명', datafield: 'NAME', width: '55%', align:'center', cellsalign: 'left',
		                  	 cellsRenderer: function (rowKey, dataField, value, data) {
		                  		var rowValue = $("#treegrdInformation").jqxTreeGrid("getRow", rowKey);
		                  		var link = "<a href=\"javascript:getMenuDetail('"+rowValue.C_ID+"')\" style='' >" + value + "</a>";      
									return link; 	   	
		                  	} , editable: false		
	                  },
					  { text: '주관부서', datafield: 'CHARGE_DEPT', width: '15%', align:'center', cellsalign: 'center', 
	                 		 cellsRenderer: function (rowKey, dataField, value, data) {
	                 				var rowValue = $("#treegrdInformation").jqxTreeGrid('getRow', rowKey);
	                 				if(rowValue.CHARGE_DEPT == undefined){
	                 					chargeDept = "<input type='hidden' data-cid='"+rowValue.C_ID+"' data-rowkey='"+rowKey+"' name='daptCd'  value=' '/>" ; 
	                 				}else{
	                 					chargeDept = "<input type='hidden' data-cid='"+rowValue.C_ID+"' data-rowkey='"+rowKey+"' name='daptCd' value='"+rowValue.CHARGE_DEPT_CD+"' />" ;
	                 				}
	                 				if(rowValue.MENU_TYPE!=null&&rowValue.MENU_TYPE=="2"){   
	                 					hiddenIcon = "<img width='16' height='16' style='float: right;' src='../../resources/cmresource/image/icon-search.png' onclick=\"searchDept('"+rowValue.C_ID+"');\" />" ;
	                 				}else{
	                 					hiddenIcon ="";    
	                 				}	 
	                 				return "<div style='margin: 0px 5px;'>"+   
	                 				"<input type='hidden' data-cid='"+rowValue.C_ID+"' data-rowkey='"+rowKey+"' name='changeValue' />" + chargeDept  + hiddenIcon  +
	                 				"<span id='label"+rowValue.C_ID+"' style='margin-left: 4px; '>"+value+"</span><div style='clear: both;'></div></div>";
	                   		} , editable: false                        
					  },      
					  { text: '담당자', datafield: 'CHARGE_EMP', width: '15%', align:'center', cellsalign: 'center'},                         
					/*   담당자팝업
					{ text: '담당자', datafield: 'CHARGE_EMP', width: '15%', align:'center', cellsalign: 'center',   
				     		 cellsRenderer: function (rowKey, dataField, value, data) {
	                 				var rowValue = $("#treegrdInformation").jqxTreeGrid('getRow', rowKey);
	                 				if(rowValue.CHARGE_EMP == undefined){            
	                 					chargeEmp = "<input type='hidden' data-cid='"+rowValue.C_ID+"' data-rowkey='"+rowKey+"' name='chargeEmp' value=' '/>" ;      
	                 				}else{
	                 					chargeEmp = "<input type='hidden' data-cid='"+rowValue.C_ID+"' data-rowkey='"+rowKey+"' name='chargeEmp' value='"+rowValue.CHARGE_EMP+"' />" ;
	                 				}
	                 				return "<div style='margin: 0px 5px;'>"+   chargeEmp +
	                       			"<img width='16' height='16' style='float: right;' src='../../resources/cmresource/image/icon-search.png' onclick=\"searchEmp('"+rowValue.C_ID+"');\" /><span id='empLabel"+rowValue.C_ID+"' style='margin-left: 4px; '>"+value+"</span><div style='clear: both;'></div></div>";
	                   		} , editable: false       
					 },             */
					 { text: '갱신주기(일)', datafield: 'UPDATE_PER', width: '15%', align:'center', cellsalign: 'center' },                                       
	                ],
	            });
	            $("#treegrdInformation").jqxTreeGrid('expandRow', $("#treegrdInformation").jqxTreeGrid("getRows")[0].uid);          
			 }});
		}     
			function getMenuDetail(cid){
				$iv.mybatis("gettreeInfromationDetail", {ACCT_ID:$iv.userinfo.acctid(), C_ID:cid, SOLUTION_NAME:$("#solutionList").val()}, {callback:function(res){
					$("#deptCd").val(res[0].CHARGE_DEPT_CD);    
					$("#dept").val(res[0].CHARGE_DEPT);
	
					$("#updatePer").val(res[0].UPDATE_PER);   
				 }});
			}
			
			function saveInformation(){
				var arrayJson = [];
	
				//담당자
				var index = $("[name='changeValue']").length;
				for(var i=0;i<index;i++){
					var cID = $("[name='changeValue']").eq(i).data("cid");
					var rowKey = $("[name='changeValue']").eq(i).data("rowkey");
	
					var jsonData = null;
					
					jsonData = {
							    division : $iv.envinfo.division,
							    cid : cID,
							    chargeDept : $("[name='daptCd'][data-cid='"+cID+"']").val(),   
							    //chargeEmp : $("[name='chargeEmp'][data-cid='"+cID+"']").val(),     
							    chargeEmp :$("#treegrdInformation").jqxTreeGrid("getRow", rowKey).CHARGE_EMP,         
							    updatePer : $("#treegrdInformation").jqxTreeGrid("getRow", rowKey).UPDATE_PER  //갱신주기
						};  
						arrayJson.push(jsonData);    
					}	
				 
					 if(arrayJson.length > 0){
						groupManagerDwr.dwrInformationInfo(arrayJson, function(res) {     
							$i.dialog.alert('SYSTEM', '저장 되었습니다.');  
	
						});
					}else{
						$i.dialog.alert('SYSTEM', '변경된 내용이 없습니다.');    
					} 
					
			}
			//주관부서 팝업
			function searchDept(cid, gubn) {
				windowOpen('../../admin/help/popup?cid='+cid+"&gubn="+gubn, 'popup', '410', '450', 'no', 'yes');       
			}	
			//담당자팝업(미사용)
			function searchEmp(cid) {
				chargeDept = $('#label'+cid).html();
				
				windowOpen('../../admin/help/popupEmp?cid='+cid+"&chargeDept="+chargeDept, 'popup', '410', '450', 'no', 'yes');       
			}	
		</script>
		<style type="text/css">
			.splitter_intab .jqx-widget-content-blueish {
			   border:0;
			}
			.splitter_intab .tree .jqx-widget-content-blueish{
				background:#ecf2f8;
			}
			.jqx-tree-grid-icon, .jqx-tree-grid-icon-size {
					width: 16px;
					height: 16px;
			 }
			  .content .button{
			     margin: 0px 0px 0px 10px !important;
			 }
		</style>    
	</head>
	<body class='blueish' >   
		<div class="wrap" style="width:98%; min-width:1067px; margin:0 10px !important;">
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="tabs f_left" style=" width:100%; height:750px; margin-top:0px;">
					<div id='jqxTabs'>
						<ul>
							<li style="margin-left: 0px;" onclick="viewPage('tableView');">정보관리</li>                 
							<li onclick="treeInfromation();" style="display:none;">정보관리</li>   
						</ul>   
						<div class="tabs_content" style="height:100%; overflow-y:auto; overflow-x:hidden; padding:0;">
							<div class="splitter splitter_intab  f_left" style="width:100%; height:718px; margin:0;">
								<div id="splitter" style="border:0;">
									<div style="border: none;" id="feedExpander">
										<div class="jqx-hideborder" style="height:20px;">
												보고서
										</div>
										<div class="jqx-hideborder jqx-hidescrollbars" >
												<div  class="tree jqx-hideborder" style="border:0 !important;"  id='treeHelpMange'></div>
										</div>
									</div>     
									<div class="content f_left" style="width:100%; margin:0px 0; padding : 10px 0;">      
										<div class="content f_left" id="gridView" style="width:100%;">
											<div class="group f_left " style="width:98%; margin-left: 17px;margin-bottom: 7px;margin-top: 10px;">      
												<div class="label type2 f_left">검색：</div>
												<div style='float: left; margin-right:10px;' >
													<input type="text" id="menuName"/>
												</div>
												<div class="combobox f_left"  id='searchType' ></div>   
												<div style="float:right;">      
													<div class="button type2 f_right">  
														<input type="button" value="조회" id='searchButton' width="100%" height="100%" />
													</div>
												</div>   
											</div>
											<div style="width:98%; margin:10px auto !important; ">		
												<div class="grid f_left" style=" float:left;width:100%; height:630px; margin-bottom:20px; ">    
													<div id="helpgrid" ></div>                
												</div>    
											</div>	
										</div>
										<form id="saveForm" name="saveForm" action="./insert" >
										<div style="overflow-y:auto; "><!--splitter Right-->   
											<input type="hidden" id="hiddenCID" name="cID"/>
											<input type="hidden"  id='solutionList' name="solutionList" value="IVISIONPLUS"/>   
											<div class="content f_left" style=" width:96%; margin:0 2%;">
												<div class="group f_left  w100p m_t10">
													<div class="label type2 f_left" id="menuNavi">도움말</div>
													<div class="button type1 f_right">
														<div class="button type2 f_left">
															<input type="button" value="저장" id='saveButton' width="100%" height="100%" />
														</div>   
													</div>	 
												</div>
												<div class="table  f_left" style="width:100%; margin:10px 0%; ">	
													 <table width="100%" cellspacing="0" cellpadding="0" border="0" id="jqxtab_table1">				
														<tr>
															<th class="w15p"><div>주관부서</div></th>
															<td  class="w35p">
																<div class="cell">
																	<input type="hidden" id="deptCd" name="deptCd"/>
																	<input type="text" id="dept" name="dept" class="input type2  f_left"  style="width:50%; margin:3px 0; " readonly/>
																</div>
															</td>
															<th class="w15p"><div>갱신주기</div></th>
														 	<td  class="w35p">
														 		<div class="cell">
														 			<input type="text" id="updatePer" name="updatePer" class="input type2  f_left"  style="width:50%; margin:3px 0; " readonly="readonly" />
														 			<div style="padding-top:4px;">일</div>
														 		</div>
														 	</td>
														</tr> 
														<tr>      
															<th class="w15p"><div>확인부서</div></th>
															<td  class="w35p">
																<div class="cell">
																	<input type="text" id="confirmDept" name="confirmDept" class="input type2  f_left"  style="width:50%; margin:3px 0; " readonly="readonly" />        
																</div>
															</td> <!--  수정 -->  
														 	<th class="w15p"><div>확인자</div></th>
														 	<td  class="w35p">  
														 		<div class="cell">
														 			<input type="text" id="confirmEmp" name="confirmEmp" class="input type2  f_left"  style="width:50%; margin:3px 0; " />
														 		</div>
														 	</td> <!--  수정 -->
														</tr>
														<tr>
															<th class="w15p"><div>담당부서</div></th>
															<td  class="w35p">
																<div class="cell">
																	<input type="text" id="chargeDept" name="chargeDept" class="input type2  f_left"  style="width:50%; margin:3px 0; " readonly="readonly" />
																</div>
															</td> <!--  수정 -->  
														 	<th class="w15p"><div>담당자</div></th>
														 	<td  class="w35p">  
														 		<div class="cell">
														 			<input type="text" id="chargeEmp" name="chargeEmp" class="input type2  f_left"  style="width:50%; margin:3px 0; " />
														 		</div>
														 	</td> <!--  수정 -->
														</tr>
														<tr>
															<th><div>설명</div></th>
																<td colspan="3">                                    
																		<textarea id="content" name="content" class="textarea_df" style="width:100%; height:460px !important;"></textarea>
																</td>                  
														</tr>
													</table>	
												</div>
											</div>
										</div>
										</form>
									</div>
								</div>    
							</div>
						</div>
						<div  class="tabs_content" style="height:100%; overflow-y:auto; overflow-x:hidden; padding:10px 20px; ">
							<div class="content f_left" style=" width:100%; margin:0;">
								<div class="group f_left  w100p m_t10">
									<div class="button type1 f_right">
										<div class="button type2 f_left">
											<input type="button" value="저장" id='treeSaveButton' width="100%" height="100%"  />
										</div>   
									</div>	 
								</div>  
								<div class="treegrid f_left" style="width:100%; height:630px; margin:0 0;">
										<div id="treegrdInformation"></div>      
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!--wrap-->		
	</body>
</html>