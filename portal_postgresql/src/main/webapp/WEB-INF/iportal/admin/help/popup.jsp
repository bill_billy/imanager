<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>주관부서 검색</title>
	
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
    <script src="${WWW.JS}/iplanbiz/core/iplanbiz.web.include.jsp"></script>
    
	<script language="javascript" type="text/javascript">
	
	function searchStart(searchText){
		if(searchText == ""){
			searchText = "IPLANBIZNULL";
		}
		var data = $i.post("./getPopHelpManagerList",{userGrpNm:searchText});
		data.done(function(res){
			var source =  
			{
				datatype: "json",
				datafields: [
					{ name: 'USER_GRP_ID', type: 'string' },
					{ name: 'USER_GRP_NM', type: 'string' }
				],
				localdata: res
			};

			var dataAdapter = new $.jqx.dataAdapter(source);
			
			var cellsrenderer = function(row, datafieId, value) {
	           	var row = $("#gridUserSearch").jqxGrid("getrowdata", row);
	           	
	           	var link = "<a href=\"javaScript:throwParent( '"+row.USER_GRP_ID+"', '"+row.USER_GRP_NM+"');\">" + value + "</a>";
	           	
	           	return "<div style='text-align:left; padding-bottom:2px; margin-top:5px;margin-left:10px; text-decoration: underline;'>" + link + "</div>";
	        };
	           
			$("#gridUserSearch").jqxGrid({
				width: '100%',
					height:'100%',
					altrows:true,
					pageable: false,
					source: dataAdapter,
					theme:'blueish',
					sortable: true,   
					columnsresize: true,  
					columnsheight: 25,
					rowsheight: 25,
				columns: [
					{ text: '부서코드', datafield: 'USER_GRP_ID', width: '50%', align:'center', cellsalign: 'left' },               
					{ text: '부서명', datafield: 'USER_GRP_NM', width: '50%', align:'center', cellsalign: 'left', cellsrenderer:cellsrenderer}
				]
			});  
		});
	}  
		
	function throwParent(deptId, daptNm){
		
		var pCid = "${param.cid}";
		var gubn = "${param.gubn}";
		
		if(gubn == "dept"){
			$('#dept',opener.document).val(daptNm);
			$('#deptCd',opener.document).val(deptId);      
			
			          
// 			$("[name='daptCd'][data-cid='"+pCid+"']", opener.document).val(deptId);
// 			$('#label'+pCid, opener.document).html(daptNm);          
		}else if(gubn == "charge"){
			$('#chargeDept',opener.document).val(daptNm);
			$('#chargeDeptCd',opener.document).val(deptId);      
			
			          
// 			$("[name='daptCd'][data-cid='"+pCid+"']", opener.document).val(deptId);
// 			$('#label'+pCid, opener.document).html(daptNm);          
		}else if(gubn == "confirm"){
			$('#confirmDept',opener.document).val(daptNm);
			$('#confirmDeptCd',opener.document).val(deptId);      
			
			          
// 			$("[name='daptCd'][data-cid='"+pCid+"']", opener.document).val(deptId);
// 			$('#label'+pCid, opener.document).html(daptNm);          
		}
		self.close();
	}
	
	$(document).ready(function() {
		
		$("#searchText").jqxInput({placeHolder: "", height: 25, width: 180, minLength: 1,  theme:'blueish'}).on('keydown', function() { if(event.keyCode==13){searchStart($('#searchText').val()); return false;}}); 
		$("#btnSearch").jqxButton({ width: '',  theme:'blueish'}).on('click', function() {searchStart($('#searchText').val());});     
          
		//담당자조회
		searchStart("");  
	
	});
	</script>

</head>
<body class='blueish'>
	<div class="wrap" style="width:98%;min-height:300px; margin:0 1%;"><!--팝업창 사이즈 조정-->
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
					<div class="label type1 f_left">부서명 : </div>
					<input type="text" id="searchText" name="searchText" />
					<div class="group_button f_right">  
						<div class="button type1 f_left">      
							<input type="button" value="조회" id="btnSearch" width="100%" height="100%" />
						</div>
					</div>    
			</div>
			<div class="label type1 f_left" style="color:red;">※부서명을 입력하신 후 조회를 하셔야 부서가 보입니다.</div>
			<div class="container  f_left" style="width:100%; margin-top:5px;">    
				<div class="grid f_left" style="width:400px; height:300px;">         
					<div id="gridUserSearch"></div>  
				</div>   
			</div>
			
	</div><!--wrap-->
</body> 
</html> 