<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>${title}</title>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		init();
	});
	function init(){
		$("#btnSearch").jqxButton({ width : '', theme : 'blueish' });
		$("#txtSearch").jqxInput({placeHolder: "", height: 23, width: 250, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return makeGrid(); } });
		makeCombobox();
	}
	function makeCombobox(){
		 var gubn  = [
    		{value:'A',text:'영문명'},
    		{value:'B',text:'한글명'}
    	];
    	var source =
    	{
    		localdata:gubn,
    		dataType:'json',
    		dataField:[
    			{ name : 'value'},
    			{ name : 'text'}
    		]
    	};
    	var adapter = new $.jqx.dataAdapter(source);
    	$('#cboTableGubn').jqxComboBox({ selectedIndex: 0, source: adapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'text', valueMember: 'value', dropDownWidth: 100, dropDownHeight: 100, width: 100, height: 22, theme:'blueish'});
    	search();
	}
	function search(){
		var gridData = $i.post("./getDataInfoTableList",{searchValue:$("#txtSearch").val()});
		gridData.done(function(res){
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: 'OWNER', type: 'string' },
					{ name: 'TABLE_NAME', type: 'string' },
					{ name: 'TABLE_COMMENTS', type: 'string' }
				],
				localdata: res.returnArray
			};
			
			var dataAdapter = new $.jqx.dataAdapter(source);
			
			//columns
			var nameCellsrenderer = function(row, datafieId, value, defaultHtml, property, rowdata) {
				var userid = rowdata.OWNER;
				var name = rowdata.TABLE_NAME;
				var comment = rowdata.TABLE_COMMENTS;
				var link = "<a href=\"javaScript:setData('"+userid+"', '"+name+"','"+comment+"');\" style='color: #000000;'>" + value + "</a>";
			
				return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px;'>" + link + "</div>";
			};
	
			$('#gridTableList').jqxGrid({
				width: '100%',
				height: '100%',
				altrows:true,
				pageable: true,
				source: dataAdapter,
				theme:'blueish',
				columnsheight:28,
				rowsheight: 28,
				columnsresize: true,
				columns: [
					{ text: 'USER명', datafield: 'OWNER', width: '25%', align:'center', cellsalign: 'center'},
					{ text: 'TABLE(영문)명', datafield: 'TABLE_NAME', width: '25%', align:'center', cellsalign: 'center'  , cellsrenderer:nameCellsrenderer},
					{ text: 'TABLE(한글)명', datafield: 'TABLE_COMMENTS', width: '50%', align:'center', cellsalign: 'center' }
				]
			});
			
			//pager width
			$('#gridpagerlist' + $('#gridTableList').attr('id')).width('44px');
			$('#dropdownlistContentgridpagerlist' + $('#gridTableList').attr('id')).width('19px');
		});
	}
	function searchStart() {
	}
	function setData(owner, name, comment) {
		$('#hiddenUserID',opener.document).val(owner);
		$('#txtTableName',opener.document).val(name);
		$('#txtTableComment',opener.document).val(comment);
		window.close();
	}
	</script>
</head>
<body class='blueish'>
	<div class="wrap" style="width:96%; margin:0 auto;">
		<div class="header f_left" style="width:99%; height:97% !important; margin:3px 5px 5px !important;">
			<div class="label type1 f_left">USER명 : </div>
			<div class="input f_left">
				<input type="text" id="txtSearch" name="searchValue" />
			</div>
			<div class="label type1 f_left">Table명 : </div>
			<div class="combobox f_left" id="cboTableGubn" name="tableGubn"></div>
			<div class="group_button f_right">
				<div class="button type1 f_left">
					<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="makeGrid();" />
				</div>
			</div>
		</div>
		<div class="container  f_left" style="width:100%; margin:10px 0;">
			<div class="content f_left" style="width:100%; margin-right:1%;">
				<div class="grid f_left" style="width:99%; height:450px; margin:3px 5px 5px !important;">
					<div id="gridTableList"></div>
				</div>
			</div>
		</div>
	</div><!--wrap-->		
</body>
</html>
