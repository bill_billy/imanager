<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>정보공시 데이터 관리</title>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	<script type="text/javascript">
		var menudata = null;
		$(document).ready(function(){
			init();
		});
		function init(){
			useYnCombo();
			$('#dateUpStartDat').datepicker({
				changeYear: true,
				showOn: 'button',
				buttonImage: '../../resources/cmresource/image/icon-calendar.png',
				buttonImageOnly: true,
				dateFormat: "yy-mm-dd", //20141212
			});
			$('#dateUpEndDat').datepicker({
				changeYear: true,
				showOn: 'button',
				buttonImage: '../../resources/cmresource/image/icon-calendar.png',
				buttonImageOnly: true,
				dateFormat: "yy-mm-dd", //20141212
			});
			$("#btnSearch").jqxButton({ width : "", theme : "blueish"});
			$("#txtSearch").jqxInput({placeHolder: "", height: 23, width: 250, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return search(); } });
		}
		function search(){
			var gubn = $("#cboTableGubn").val();
			var tableComment = "";
			var tableName = "";
			if(gubn == "A"){
				tableName=$("#txtSearch").val();
			}else if(gubn == "B"){
				tableComment=$("#txtSearch").val();
			}
			var data = $i.post("./getInfoDataList", {searchComment:tableComment,searchTableName:tableName,yyyy:$("#cboInfoYyyy").val()+$("#cboInfoMm").val()});
			data.done(function(dataInfo){
				var source =
				{
					localdata: dataInfo.returnArray,
		    		dataType: 'json',
					dataFields: [
						{ name: 'IDX'},
						{ name: 'USERID'},
						{ name: 'TABLE_NAME'},
						{ name: 'TABLE_COMMENT'},
						{ name: 'DT_STARTDAT'} ,
						{ name: 'DT_ENDDAT'} ,
						{ name: 'UP_STARTDAT'} ,
						{ name: 'UP_ENDDAT'} ,
						{ name: 'BIGO'},
						{ name: 'USER_NAME'},
						{ name: 'INSERT_DAT'}
					]
				};
				var alginLeft = function (row, columnfield, value) {//right정렬
					return '<div style="text-align: left; margin:4px 10px 0px 10px;">' + value + '</div>';
				};
				var detailView = function (row, columnfield, value, defaultHtml, property, rowdata) {
					var idx = rowdata.IDX;
					var link = "<a href=\"javaScript:getDetailDataInfo('"+idx+"')\" style='color:black;text-decoration:underline;'>" + value + "</a>";
					return '<div style="text-align:left; margin:4px 10px 0px 10px;">' + link + '</div>';
				};
		
				var adapter = new $.jqx.dataAdapter(source);
				
				$("#gridInfoDataList").jqxGrid({
					width: '100%',
		         	height: '100%',
					altrows:true,
					pageable: false,
		        	source: adapter,
					theme:'blueish',
					//editable: true,
		         	columnsresize: true,
		         	columns: [
						{ text: '영문', datafield: 'TABLE_NAME', width: '10%', align:'center', cellsalign: 'center',  columngroup:'A' },
						{ text: '한글', datafield: 'TABLE_COMMENT', width: '24%', align:'center', cellsalign: 'center', cellsrenderer: detailView ,  columngroup:'A' },
						{ text: '시작년도', datafield: 'DT_STARTDAT', width: '6%', align:'center', cellsalign: 'center',  columngroup:'B' },
						{ text: '종료년도', datafield: 'DT_ENDDAT', width: '6%', align:'center', cellsalign: 'center', columngroup:'B' },
						{ text: '시작일', datafield: 'UP_STARTDAT', width: '7%', align:'center', cellsalign: 'center', columngroup:'C' },
						{ text: '종료일', datafield: 'UP_ENDDAT', width: '7%', align:'center', cellsalign: 'center', columngroup:'C' },
						{ text: '업데이트내용', datafield: 'BIGO', width: '40%', align:'center', cellsalign: 'left', cellsrenderer: alginLeft}
					],
					columngroups: 
		            [
		            	{ text: 'Table', align: 'center', name: 'A' },
						{ text: '데이터 년도', align: 'center', name: 'B' },
		            	{ text: '업데이트 기간', align: 'center', name: 'C' }
		
		        	]
				});
			});
		}
		function insert(){
			$i.insert("#saveForm").done(function(args){
				if(args.returnCode == "EXCEPTION"){
					$i.dialog.error("SYSTEM", args.returnMessage);
				}else{
					$i.dialog.alert("SYSTEM", args.returnMessage,function(){
						search();
						resetForm();
					});
				}
			});
		}
		function remove(){
			$i.dialog.confirm("SYSTEM","삭제하시겠습니까?",function(){
				$i.remove("#saveForm").done(function(args){
					if(args.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", args.returnMessage);
					}else{
						$i.dialog.alert("SYSTEM", args.returnMessage, function(){
							search();
							resetForm();
						});
					}
				});
			});
		}
		function resetForm(){
			$("#hiddenIdx").val("");
			$("#hiddenUserID").val("");
			$("#txtTableName").val("");
			$("#txtTableComment").val("");
			$("#dateDtStartDat").val("");
			$("#dateDtEndDat").val("");
			$("#dateUpStartDat").val("");
			$("#dateUpEndDat").val("");
			$("#txtBigo").val("");
			$("#txtUserID").val("");
			$("#txtInsertDat").val("");
		}
		function getDetailDataInfo(idx){
			var data = $i.post("./getInfoDataDetail", {idx:idx});
			data.done(function(detail){
				if(detail.returnCode == "EXCEPTION"){
					$i.dialog.error("SYSTEM", detail.retrunMessage);
				}else{
					if(detail.returnArray.length > 0){
						$("#hiddenIdx").val(detail.returnArray[0].IDX);
						$("#hiddenUserID").val(detail.returnArray[0].USERID);
						$("#txtTableName").val(detail.returnArray[0].TABLE_NAME);
						$("#txtTableComment").val(detail.returnArray[0].TABLE_COMMENT);
						$("#dateDtStartDat").val(detail.returnArray[0].DT_STARTDAT);
						$("#dateDtEndDat").val(detail.returnArray[0].DT_ENDDAT);
						$("#dateUpStartDat").val(detail.returnArray[0].UP_STARTDAT);
						$("#dateUpEndDat").val(detail.returnArray[0].UP_ENDDAT);
						$("#txtBigo").val(detail.returnArray[0].BIGO);
						$("#txtUserID").val(detail.returnArray[0].INSERT_EMP);
						$("#txtInsertDat").val(detail.returnArray[0].INSERT_DAT);
					}
				}
			});
		}
		function useYnCombo(){
			var codeData = $i.post("./getCodeList", {comlCod:"EVAL_YEAR"});
			codeData.done(function(data){
	            var source =
	            {
	                datatype: "json",
	                datafields: [
	                     { name: 'COM_COD' },
	                     { name: 'COM_NM' }
	                ],
	                id: 'id',
					localdata:data.returnArray,
	                async: false
	            };
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#cboInfoYyyy").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,theme:'blueish'});
	            var codeData = $i.post("./getCodeList", {comlCod:"INFO_MM"});
				codeData.done(function(data){
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                     { name: 'COM_COD' },
		                     { name: 'COM_NM' }
		                ],
		                id: 'id',
						localdata:data.returnArray,
		                async: false
		            };
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#cboInfoMm").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,theme:'blueish'});
		            var gubn  = [
			    		{value:'A',text:'영문명'},
			    		{value:'B',text:'한글명'}
			    	];
			    	var source =
			    	{
			    		localdata:gubn,
			    		dataType:'json',
			    		dataField:[
			    			{ name : 'value'},
			    			{ name : 'text'}
			    		]
			    	};
			    	var adapter = new $.jqx.dataAdapter(source);
			    	$('#cboTableGubn').jqxComboBox({ selectedIndex: 0, source: adapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'text', valueMember: 'value', dropDownWidth: 100, dropDownHeight: 100, width: 100, height: 22, theme:'blueish'});
			    	search();
				});
			});
		}
		function openWindow(){
			var url = "./popupMappingTable";
			
			var nWin = window.open(url,"_blank","left=0,top=0,width=1040,height=525,toolbar=no,scrollbars=0,status=yes,resizable=yes");
			nWin.focus();
		}		
	</script>
	<style type="text/css">
		.ui-datepicker-trigger{
			float:left; margin:5px;
		}
	</style>
</head>
<body class='blueish'>
	<div class="wrap" style="width:98%; min-width:1160px; margin:0 1%;">
		<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
			<div class="label type1 f_left">년도 : </div>
			<div class="combobox f_left" id="cboInfoYyyy" name="infoYyyy"> </div>
			<div class="label type1 f_left">월 : </div>
			<div class="combobox f_left" id="cboInfoMm" name="infoMm"> </div>
			<div class="label type1 f_left">테이블명 : </div>
			<div class="combobox f_left" id="cboTableGubn" name="tableGubn"></div>
			<div class="input f_left">
				<input type="text" id="txtSearch" name="searchscnm" />
			</div>
			<div class="group_button f_right">
				<div class="button type1 f_left">
					<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
				</div>
			</div>
		</div>
		<div class="container  f_left" style="width:100%; margin:10px 0;">
			<div class="content f_left" style=" width:100%; margin-right:1%;">
				<form id="saveForm" name="saveForm">
					<input type="hidden" id="hiddenLocale" name="saveLocale" />
					<input type="hidden" id="hiddenCName" name="saveCname" />
					<div class="grid f_left" style="width:100%; height:500px; margin:5px 0;">
						<div id="gridInfoDataList"></div>
					</div>
					<div class="blueish table f_left"  style="width:100%; height:; margin:10px 0;">
						<table  width="100%" cellspacing="0" cellpadding="0" border="0">
							<tbody>
								
								<tr>
									<th style="width:20%;">테이블(영문)</th>
									<td style="width:30%">
										<div class="cell">
											<input type="hidden" id="hiddenIdx" name="idx" />
											<input type="hidden" id="hiddenUserID" name="userID" />
											<input type="text" id="txtTableName" name="tableName" class="input type2  f_left  m_r10"  style="width:70%; margin:3px 0; " readonly/>
											<div class="icon-search f_right pointer" onclick="openWindow();"></div>
										</div>
									</td>
									<th style="width:20%;">테이블(한글)</th>
									<td style="width:30%">
										<div class="cell">
											<input type="text" id="txtTableComment" name="tableComment" class="input type2 f_left" style="width:80%; margin:3px 0; "/>
										</div>
									</td>
								</tr>
								<tr>
									<th style="width:20%;">데이터년도</th>
									<td>
										<div class="cell">
											<input type="text" id="dateDtStartDat" name="dtStartDat" class="input type2 f_left" style="width:40%; margin:3px 0; "/>
											<div style="float:left"> ~ </div>
											<input type="text" id="dateDtEndDat" name="dtEndDat" class="input type2 f_left" style="width:40%; margin:3px 0; "/>
										</div>
									</td>
									<th style="width:20%;">업데이트 기간</th>
									<td>
										<div class="cell">
											<input type="text" id="dateUpStartDat" name="upStartDat" class="input type2 f_left" style="width:40%; margin:3px 0; "/>
											<div style="float:left"> ~ </div>
											<input type="text" id="dateUpEndDat" name="upEndDat" class="input type2 f_left" style="width:40%; margin:3px 0; "/>
										</div>
									</td>
								</tr>
								<tr>
									<th style="width:20%;">업데이트 내용</th>
									<td colspan="3">
										<div class="cell">
											<input type="text" id="txtBigo" name="bigo" class="input type2 f_left" style="width:80%; margin:3px 0; "/>
										</div>
									</td>
								</tr>
								<tr>
									<th style="width:20%;">입력자</th>
									<td>
										<div class="cell">
											<input type="text" id="txtUserID" class="input type3 f_left" style="width:80%; margin:3px 0; " readonly="readonly"/>
										</div>
									</td>
									<th style="width:20%;">입력일시</th>
									<td>
										<div class="cell">
											<input type="text" id="txtInsertDat" class="input type1 f_left" style="width:80%; margin:3px 0; " readonly="readonly"/>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="group_button f_right">
						<div class="button type2 f_left">
							<input type="button" value="초기화" id='btnReset' width="100%" height="100%" onclick="resetForm();" />
						</div>
						<div class="button type2 f_left">
							<input type="button" value="저장" id='btnSave' width="100%" height="100%" onclick="insert();" />
						</div>
						<div class="button type3 f_left">
							<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();" />
						</div>
					</div>
				</form>
			</div>
		</div>          
	</div>
</body>
</html>