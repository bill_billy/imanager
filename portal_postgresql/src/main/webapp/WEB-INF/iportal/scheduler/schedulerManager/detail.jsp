<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<!-- 		<meta http-equiv="X-UA-Compatible" content="IE=8"/> -->
		<title>스케쥴러 관리</title>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
		<script type="text/javascript">            
		var errMessage = "${msg}";
		var jobData1,jobData2;
		$(document).ready(function(){
			$i.dialog.setAjaxEventPopup("SYSTEM","데이터 불러오는 중");
			init();
			
		});
		function init(){
			$("#cboUseYn").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 100, dropDownHeight: 52, width: 100, height: 19,  theme:'blueish' });
			$("#cboSchCyc").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right',  dropDownWidth: 100, dropDownHeight: 102, width: 100, height: 19,  theme:'blueish' });
			$("#txtFromTime").jqxNumberInput({ width:80, height: 19, decimalDigits: 0, min:0, max:23, inputMode: 'simple', spinButtons: true, symbol: '시', symbolPosition: 'right',  theme:'blueish' });
			$("#txtFromMinute").jqxNumberInput({ width: 80, height: 19, decimalDigits: 0, min:0, max:59, inputMode: 'simple', spinButtons: true, symbol: '분', symbolPosition: 'right',  theme:'blueish' });
			$("#txtFromTime1").jqxNumberInput({ width: 80, height: 19, decimalDigits: 0, min:0, max:59, inputMode: 'simple', spinButtons: true, symbol: '시', symbolPosition: 'right',  theme:'blueish' });
			$("#cboAType").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 80, dropDownHeight: 78, width: 80, height: 19,  theme:'blueish' });
			$("#cboJobDist").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 120, dropDownHeight: 100, width: 120, height: 19,  theme:'blueish' });
			$("#txtFromDat, #txtToDat").datepicker({
				showOn: "button",
				buttonImage: '../../resources/cmresource/image/icon-calendar.png',
				buttonImageOnly : true,
				dateFormat: "yymmdd"
			});
			$(".ui-datepicker-trigger").addClass("icon-calender f_left m_t7 pointer");
			detailList();
			makeCombobox();
			//목록버튼
			$("#btnList").jqxButton({ width : '' , theme : 'blueish' });
			//저장버튼
			$("#btnInsert").jqxButton({ width : '' , theme : 'blueish' });
			//삭제버튼
			$("#btnDelete").jqxButton({ width : '' , theme : 'blueish' });
			
			$("#txtSearchWord").on('keydown',function() {
					if(event.keyCode==13) {
						addfilter();
						return false;
					}
				}
			);
		}
		function insert(){
			$('#saveForm').jqxValidator('hide');
			
			var ruleinfo = null;
			ruleinfo = [
				{input: '#txtSchName', message:'이름을 입력하세요!', action:'keyup, blur', rule:'required'},
				{input: '#txtFromDat', message:'시작 날짜를 입력하세요!', action:'keyup, blur', rule:'required'},
				{input: '#txtToDat', message:'종료 날짜를 입력하세요!', action:'keyup, blur', rule:'required'},
				{input: '#txtATime', message:'숫자로 입력하세요!', action:'keyup, blur', rule:'number'},
				{input: '#txtCDate', message:'숫자로 입력하세요!', action:'keyup, blur', rule:'number'},
				{input: '#txtCWeek', message:'숫자로 입력하세요!', action:'keyup, blur', rule:'number'},
				{input: '#txtDDate', message:'숫자로 입력하세요!', action:'keyup, blur', rule:'number'},
				{input: '#txtDWeek', message:'숫자로 입력하세요!', action:'keyup, blur', rule:'number'}
			];
			$("#saveForm").jqxValidator({
				rules : ruleinfo
			});
			if($("#saveForm").jqxValidator('validate') == true) {
				if($("#cboSchCyc").val() == "A") { //매일
					if($("#txtATime").val()!=""){//반복시간입력시
						if($("#cboAType").val()==""){
							$i.dialog.alert('SYSTEM','시간간격을 선택하세요!');
							return false;
						}
					}
				} else if($("#cboSchCyc").val() == "B") { //매주
					if($("[name='bDay']:checked").length==0){
						$i.dialog.alert('SYSTEM','요일을 선택하세요!');
						return false;
					}
				} else if($("#cboSchCyc").val() == "C") { //매월
					if(($("#txtCWeek").val()=="")&&($("#txtCDate").val()=="")){
						$i.dialog.alert('SYSTEM','주기를 입력하세요!');
						return false;
					}
					if($("#txtCWeek").val()!=""){
						if($("#cboCDay").val()==""){
							$i.dialog.alert('SYSTEM','요일을 선택하세요!');
							return false;
						}
					}
				} else if($("#cboSchCyc").val() == "D") { //매년
					if($("#cboDMon").val()==""){
						$i.dialog.alert('SYSTEM','월을 선택하세요!');
						return false;
					}
					if(($("#txtDWeek").val()=="")&&($("#txtDDate").val()=="")){
						$i.dialog.alert('SYSTEM','주기를 입력하세요!');
						return false;
					}
					if($("#txtDWeek").val()!=""){
						if($("#cboDDay").val()==""){
							$i.dialog.alert('SYSTEM','요일을 선택하세요!');
							return false;
						}
					}
				} else {
					
				}
				function changeTime1(){
					$("div[name=numericInput3]").val(0);
					$("#txtATime").val("");
					$("#cboAType").jqxComboBox({ selectedIndex : 0 });
					
				}
				function changeTime3(){
					$("div[name=numericInput1]").val(0);
					$("div[name=numericInput2]").val(0);
				}
				function changeDate(){
					$("#txtCWeek").val("");
					$("#cboCDay").jqxComboBox({ selectedIndex : 0 });
					
				}
				function changeWeek(){
					$("#txtCDate").val("");
				}
				function changeDate2(){
					$("#txtDWeek").val("");
					$("#cboDDay").jqxComboBox({ selectedIndex : 0 });
					
				}
				function changeWeek2(){
					$("#txtDDate").val("");
				}
				
				
				$("#hiddenSchName").val($("#txtSchName").val());
				$("#hiddenDistYn").val($("[name='distYn']:checked").val());
				$("#hiddenUseYn").val($("#cboUseYn").val());
				$("#hiddenSchStatus").val("UNREG");
				$("#hiddenSchCyc").val($("#cboSchCyc").val());
				if($("#cboAType").val() == ""){
					$("#hiddenAType").val(null);
				}else{
					$("#hiddenAType").val($("#cboAType").val());	
				}
				$("#hiddenATime").val($("#txtATime").val());
				var bDay = "";
				for(var i=0; i<$("[name='bDay']:checked").length;i++){
					if(i==0){
						bDay += $("[name='bDay']:checked")[i].value;
					}else{
						bDay += "," + $("[name='bDay']:checked")[i].value;
					}
				}
				$("#hiddenBDay").val(bDay);
				$("#hiddenCDate").val($("#txtCDate").val());
				$("#hiddenCWeek").val($("#txtCWeek").val());
				$("#hiddenCDay").val($("#cboCDay").val());
				$("#hiddenDMon").val($("#cboDMon").val());
				$("#hiddenDDate").val($("#txtDDate").val());
				$("#hiddenDWeek").val($("#txtDWeek").val());        
				$("#hiddenDDay").val($("#cboDDay").val());
				$("#hiddenFromDat").val($("#txtFromDat").val());
				$("#hiddenToDat").val($("#txtToDat").val());
				if($("#cboAType").val() == ""){
					$("#hiddenFromTime").val($("#txtFromTime").val());
				}else{
					$("#hiddenFromTime").val($("#txtFromTime1").val());
				}
	// 			$("#hiddenFromTime").val($("#txtFromTime").val());
				$("#hiddenFromMinute").val($("#txtFromMinute").val());
				var selectGrid = $("#gridSelectList").jqxGrid("getRows");
				if(selectGrid.length > 0){
					var arrayJobData = "";
					for(var rows = 0; rows<selectGrid.length; rows++){
						var obj = $("<input type='hidden' name='jobID'/>").appendTo("#saveForm");
						obj.val(selectGrid[rows].JOB_ID);
						obj = $("<input type='hidden' name='dbKey'/>").appendTo("#saveForm");
						obj.val(selectGrid[rows].DB_KEY);
						obj = $("<input type='hidden' name='jobName'/>").appendTo("#saveForm");
						obj.val(selectGrid[rows].JOB_NM);
						obj = $("<input type='hidden' name='jobObjID'/>").appendTo("#saveForm");
						obj.val(selectGrid[rows].JOB_OBJ_ID);
						obj = $("<input type='hidden' name='jobTyp'/>").appendTo("#saveForm");
						obj.val(selectGrid[rows].JOB_TYP);
						obj = $("<input type='hidden' name='sortOrd'/>").appendTo("#saveForm");
						obj.val(selectGrid[rows].SORT_ORD);
					/*	싱글쿼테이션이 들어간 문자열 저장오류
						arrayJobData +="<input type='hidden' name='jobID' value='"+selectGrid[rows].JOB_ID+"'/>";
						arrayJobData += "<input type='hidden' name='dbKey' value='"+selectGrid[rows].DB_KEY+"'/>";
						arrayJobData += "<input type='hidden' name='jobName' value='"+selectGrid[rows].JOB_NM+"'/>";
						arrayJobData += "<input type='hidden' name='jobObjID' value='"+selectGrid[rows].JOB_OBJ_ID+"'/>";
						arrayJobData += "<input type='hidden' name='jobTyp' value='"+selectGrid[rows].JOB_TYP+"'/>";
						arrayJobData += "<input type='hidden' name='sortOrd' value='"+selectGrid[rows].SORT_ORD+"'/>";
						*/
					}
					//$("#saveForm").append(arrayJobData);
					$i.post("./insertBySchedulerManager", "#saveForm").done(function(data){
						if(data.returnCode == "EXCEPTION"){
							$i.dialog.error("SYSTEM", data.returnMessage);
						}else{
							$i.dialog.alert("SYSTEM", data.returnMessage,function(){
								goList();
							});
						}
					});
				}else{
					$i.dialog.error("SYSTEM", "선택된 스케쥴러가 없습니다");
				}
			}
		}
		function remove(){
			$i.dialog.confirm('SYSTEM','삭제하시겠습니까?',function(){
				$i.post("./deleteBySchedulerManager","#saveForm").done(function(data){
					if(data.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", data.returnMessage);
					}else{
						$i.dialog.alert("SYSTEM", data.returnMessage, function(){
							goList();
						});
					}
				});
			});
		}
		function resetInput(){
			$("[name='jobID']").remove();
			$("[name='dbKey']").remove();
			$("[name='jobName']").remove();
			$("[name='jobObjID']").remove();
			$("[name='jobType']").remove();
			$("[name='sortOrd']").remove();
		}
		function detailList() {
			if($("#cboSchCyc").val() == "A") { //매일
				$("#jobList").find("div[name=checkDayList]").hide();
				$("#jobList").find("div[name=selectDayList]").hide();
				$("#jobList").find("div[name=selectMonList]").hide();
				$("#detailTime").show();
			} else if($("#cboSchCyc").val() == "B") { //매주
				$("#jobList").find("div[name=checkDayList]").show();
				$("#jobList").find("div[name=selectDayList]").hide();
				$("#jobList").find("div[name=selectMonList]").hide();
				$("#detailTime").hide();
			} else if($("#cboSchCyc").val() == "C") { //매월
				$("#jobList").find("div[name=checkDayList]").hide();
				$("#jobList").find("div[name=selectDayList]").show();
				$("#jobList").find("div[name=selectMonList]").hide();
				$("#detailTime").hide();
				$("#cboCDay").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 80, dropDownHeight: 100, width: 80, height: 19,  theme:'blueish' });
			} else if($("#cboSchCyc").val() == "D") { //매년
				$("#jobList").find("div[name=checkDayList]").hide();
				$("#jobList").find("div[name=selectDayList]").hide();
				$("#jobList").find("div[name=selectMonList]").show();
				$("#detailTime").hide(); 
				$("#cboDMon").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 80, dropDownHeight: 100, width: 80, height: 19,  theme:'blueish' });
				$("#cboDDay").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 80, dropDownHeight: 100, width: 80, height: 19,  theme:'blueish' });
			} else {
				$("#jobList").find("div[name=checkDayList]").hide();
				$("#jobList").find("div[name=selectDayList]").hide();
				$("#jobList").find("div[name=selectMonList]").hide();
				$("input[name=bDay]").attr("checked", false);
				$("#detailTime").show();
			}
		}
		function makeCombobox(){
			var dbData = $i.post("./getDBinfoListByScheduler", {});
			dbData.done(function(res){
				res = Enumerable.From(res.returnArray).Where(function(c){ return c.DBKEY.toUpperCase().indexOf("V1")==-1;}).ToArray();
				var source = 
				{
					localdata : res,
					datatype : "json",
					datafields : [
						{ name : "DBKEY", type : 'string' },
						{ name : "DBNAME", type : 'string' } 
					]
				};
				var dataAdapter = new $.jqx.dataAdapter(source);
				$("#cboDbKey").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "DBNAME", valueMember: "DBKEY", dropDownWidth: 120, dropDownHeight: 100, width: 120, height: 19,  theme:'blueish' });
				getServerJobList();
				getJobList();
			});
		}
		function getJobList() {
			jobData2 = $i.post("./getJobMappingScheduler", {schID : "${mainList.SCH_ID}"});
			jobData2.done(function(res){
				var source = {
					localdata: res.returnArray,
					datatype: "json",
					datafields: [
						{ name: 'JOB_ID', type: 'String'},
						{ name: 'SCH_ID', type: 'String'},
						{ name: 'SCH_NM', type: 'String'},
						{ name: 'DB_KEY'  , type:'String'},
						{ name: 'JOB_NM', type: 'String'},
						{ name: 'JOB_OBJ_ID', type:'String'},
						{ name: 'JOB_TYP', type: 'String'},
						{ name: 'JOB_TYP_NM', type: 'String'},
						{ name: 'SORT_ORD', type: 'String'}
					]
				};
				var dataAdapter = new $.jqx.dataAdapter(source);
				var noScript = function(row,datafield,value) {
					var newValue = $i.secure.scriptToText(value);
                	return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 0px;">' + newValue + '</div>';
				};
				//JOB_DIST : 프로시저, 쉘 구분 코드
				var jobDistAdapter = getCommonCodeSource('JOB_DIST');
				$("#gridSelectList").jqxGrid({
					width: '100%',
					height:'100%',
					source: dataAdapter,
					sortable: false,
					theme:'blueish',
					columnsheight:30,
					rowsheight: 30,
					selectionmode: 'multiplerows',
					altrows:true,
					editable: true,
					ready: function () {
						$("#gridSelectList").jqxGrid();
					},
					columns: [
						{ text:'JOB_OBJ_ID',dataField:'JOB_OBJ_ID', hidden:true},
						{ text: '순서'    , dataField: 'SORT_ORD', width: 95.6, align:'center',  cellsalign: 'center'},
						{ text: '유저명'    , dataField: 'DB_KEY', displayfield: 'DB_KEY', width: 95.6, align:'center',  cellsalign: 'center', editable: false }, 
						{ text: '구분'    , dataField: 'JOB_TYP', displayfield: 'JOB_TYP_NM', width: 286.8, align:'center',  cellsalign: 'center', editable: false	},
						{ text: '이름'    , dataField: 'JOB_NM'    , align:'center',  cellsalign: 'left', editable: false, cellsrenderer:noScript}				
					]
				});
				var jobList = "";
				var step=[jobData1,jobData2];
				$.when.apply($,step).done(function(){
					var targetJobList = $("#gridSelectList").jqxGrid("getRows");
					var sourceJobList = $("#listJobList").jqxGrid("getRows");
					for(var i=0;i<targetJobList.length;i++){
						for(var j=0;j<sourceJobList.length;j++){
							if(targetJobList[i].JOB_NM == sourceJobList[j].NM){
								 $("#listJobList").jqxGrid('deleterow', sourceJobList[j].uid);
								 if(i != targetJobList.length-1){
									 i++;
									 j=-1;	 
								 }
							}					
						}   
					}
				
				});
			});
		}
		var addfilter = function () {
			
			 var jobDist = $('#cboJobDist').val();
			 var searchWord = $('#txtSearchWord').val();
			 var filtercondition = 'contains';
		     var filtergroup = new $.jqx.filter();
		     var filter_or_operator = 1;
		     var filtervalue = searchWord;
		     var filter1 = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
		     filtergroup.addfilter(filter_or_operator, filter1);
		     
		  	 // add the filters.
		     if(jobDist=="P"){
		    	 $("#listJobList").jqxGrid('addfilter', 'PROCEDURE_NAME', filtergroup);
			 }
			 else if(jobDist=="E"){
				 $("#listJobList").jqxGrid('addfilter', 'ETL_TITLE', filtergroup);
			 }else if(jobDist=="R"){
				 $("#listJobList").jqxGrid('addfilter', 'RULE_ID', filtergroup);
			 }else{
				 $("#listJobList").jqxGrid('addfilter', 'JOB_OBJ_NM', filtergroup);
			 }
		     // apply the filters.
		     $("#listJobList").jqxGrid('applyfilters');
		     
		 }
		function getServerJobList() {
			var jobDist = $('#cboJobDist').val();
			var searchWord = $('#txtSearchWord').val();
			if(jobDist=="P"){
				$("#cboDbKey").show();
			}
			else{
				$("#cboDbKey").hide();
			}
			jobData1 = $i.post("./getSeverJobList",{jobDist : $("#cboJobDist").val(), schID : "${mainList.SCH_ID}", searchName : '', dbKey : $("#cboDbKey").val()});
			jobData1.done(function(res){
				if($("#cboJobDist").val()=="D"){
					if(Enumerable.From($("#gridSelectList").jqxGrid("getrows")).Where(function(c){ return c.JOB_TYP=="D";}).Count()==0){
						res=[{
							JOB_OBJ_ID:"DATACENTER",
							USER:'IPLAN',
							JOB_OBJ_NM:'데이터배포',
							STATUS:'VALID'
						}];
					}else res=[];
				}
				var source = {};
				var columns = [];
				if($("#cboJobDist").val()=="E"){
					source = {
							localdata: res.returnArray,
							datatype: "json",
							datafields: [
							    { name:'JOB_OBJ_NM',type:'String'},
							    { name:'JOB_OBJ_ID',type:'String'},
							    { name:'ETL_TITLE',type:'String'},
							    { name:'TARGET_DBINFO' ,type:'String' },
								{ name:'TARGET_USER', type: 'String'},
								{ name:'TARGET_TABLE', type: 'String'},
								{ name:'SOURCE_DBINFO' ,type:'String' },
								{ name:'SOURCE_USER', type: 'String'},
								{ name:'SOURCE_TABLE', type: 'String'}
							]
						};
					var noScript = function(row,datafield,value) {
						var newValue = $i.secure.scriptToText(value);
	                	return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 0px;">' + newValue + '</div>';
					};
						columns = [
										{ text: 'JOB_OBJ_ID',  dataField: 'JOB_OBJ_ID',hidden:true, align:'center',  cellsalign: 'left'},
										{ text: '원본 DB',  dataField: 'SOURCE_DBINFO',hidden:true, align:'center',  cellsalign: 'left'},
										{ text: 'ETL제목',  dataField: 'ETL_TITLE', width: '46%', align:'center',  cellsalign: 'left',cellsrenderer: noScript},
										{ text: '원본 사용자',  dataField: 'SOURCE_USER', width: '15%', align:'center',  cellsalign: 'left',cellsrenderer: noScript},
										{ text: '원본 테이블명',  dataField: 'SOURCE_TABLE', width: '23%', align:'center',  cellsalign: 'left',cellsrenderer: noScript},
										{ text: '대상 DB',  dataField: 'TARGET_DBINFO',hidden:true, align:'center',  cellsalign: 'left'},
										{ text: '대상 사용자',  dataField: 'TARGET_USER', width: '15%', align:'center',  cellsalign: 'left',cellsrenderer: noScript},
										{ text: '대상 테이블명',  dataField: 'TARGET_TABLE', width: '23%', align:'center',  cellsalign: 'left',cellsrenderer: noScript}									
									];	
				}
				else if($("#cboJobDist").val()=="R"){
					source = {
							localdata: res.returnArray,
							datatype: "json",
							datafields: [
							    { name:'RULE_ID',type:'String'},
							    { name:'RULE_GROUP',type:'String'},
							    { name:'DESCRIPTION',type:'String'}							    
							]
						};
					var noScript = function(row,datafield,value) {
						var newValue = $i.secure.scriptToText(value);
	                	return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 5px;">' + newValue + '</div>';
					};
						columns = [ 
										{ text: 'RULE_ID',  dataField: 'RULE_ID', width: '30%', align:'center',  cellsalign: 'left',cellsrenderer: noScript},
										{ text: 'RULE_GROUP',  dataField: 'RULE_GROUP', width: '30%', align:'center',  cellsalign: 'left',cellsrenderer: noScript}, 
										{ text: 'DESCRIPTION',  dataField: 'DESCRIPTION', width: '40%', align:'center',  cellsalign: 'left',cellsrenderer: noScript}									
									];	
				}
				else if($("#cboJobDist").val()=="P"){
					source = {
							localdata: res.returnArray,
							datatype: "json",
							datafields: [
							    { name:'PROCEDURE_CAT',type:'String'},
							    { name:'PROCEDURE_SCHEM',type:'String'},
							    { name:'PROCEDURE_NAME',type:'String'},
							    { name:'REMARKS' ,type:'String' },
								{ name:'SPECIFIC_NAME', type: 'String'}
							]
						};
						columns = [
									{ text: '스키마',  dataField: 'PROCEDURE_SCHEM', width: '35%', align:'center', cellsalign: 'center'},
									{ text: '이름',  dataField: 'PROCEDURE_NAME', width: '65%', align:'center',  cellsalign: 'left'}									
								];
				}
				else{
					source = {
							localdata: res,
							datatype: "json",
							datafields: [
								{name:'JOB_OBJ_ID',type:'String'},
							    { name:'USER' ,type:'String' },
								{ name: 'JOB_OBJ_NM', type: 'String'},
								{ name: 'STATUS', type: 'String'}
							]
						}; 
						columns = [
										{ text: 'JOB_OBJ_ID',  dataField: 'JOB_OBJ_ID',hidden:true, align:'center',  cellsalign: 'left'},
										{ text: '이름',  dataField: 'JOB_OBJ_NM', width: '100%', align:'center',  cellsalign: 'left'} 
									];	
				}
				
				var dataAdapter = new $.jqx.dataAdapter(source);
				
				$("#listJobList").jqxGrid({
					width: '100%',
					height:'100%',
					source: dataAdapter,
					sortable: true,
					pageable: false, 
					filterable: true,
					showfiltermenuitems: false,
					showfiltercolumnbackground: false,
					pagesizeoptions: ['100', '200', '300'],
					theme:'blueish',
					columnsheight:30,
					rowsheight: 30,
					selectionmode: 'multiplerows',
					altrows:true,
					ready: function () {
						$("#listJobList").jqxGrid();
					},
					columns: columns
				});
				$('#listJobList').jqxGrid('clearfilters');
				
			});
		}
		function getCommonCodeSource(comlCd) {
			var rs = null;
			var codeData = $i.post("./getCodeListByScheduler", { comlCod : "JOB_DIST" });
			codeData.done(function(res){
				var source =
				{
					localdata: res,
					datatype: "json",
					datafields: [
						{ name: 'COM_COD', type: 'String'},
						{ name: 'COM_NM', type: 'String'}
					]
				};
				var dataAdapter = new $.jqx.dataAdapter(source, {
					autoBind: true
				});
				rs = dataAdapter;
			});
		}
		function add() {
			var src = "#listJobList";
			var target = "#gridSelectList";
			var rows = $(src).jqxGrid('getselectedrowindexes');
			var rowids=[];
			var datas=[];
			var rowseq = $(target).jqxGrid("getrows").length; //전체 행 갯수
			for(var i=0;i<rows.length;i++){
				var id = $(src).jqxGrid('getrowid',rows[i]);  
				rowids.push(id);
				
				var obj = $(src).jqxGrid('getrowdatabyid',id);
				if($("#cboJobDist").val() == "E"){
					datas.push({ORG_DATA:obj,SORT_ORD:++rowseq,DB_KEY:"", JOB_TYP:$('#cboJobDist').val(), JOB_TYP_NM:$("#cboJobDist").jqxComboBox("getSelectedItem").label, JOB_OBJ_ID:obj['JOB_OBJ_ID'],JOB_NM: obj['JOB_OBJ_NM'], JOB_PARAM:''});
				}else if($("#cboJobDist").val() == "P"){
					datas.push({ORG_DATA:obj,SORT_ORD:++rowseq,DB_KEY:$("#cboDbKey").val(), JOB_TYP:$('#cboJobDist').val(), JOB_TYP_NM:$("#cboJobDist").jqxComboBox("getSelectedItem").label, JOB_OBJ_ID:obj['PROCEDURE_SCHEM']+"."+obj['PROCEDURE_NAME'],JOB_NM: obj['PROCEDURE_SCHEM']+"."+obj['PROCEDURE_NAME'], JOB_PARAM:''});
				}else if($("#cboJobDist").val() == "R"){
					datas.push({ORG_DATA:obj,SORT_ORD:++rowseq,DB_KEY:"", JOB_TYP:$('#cboJobDist').val(), JOB_TYP_NM:$("#cboJobDist").jqxComboBox("getSelectedItem").label, JOB_OBJ_ID:obj['RULE_GROUP']+"."+obj['RULE_ID'],JOB_NM: obj['DESCRIPTION'], JOB_PARAM:''});
				}
				else{
					datas.push({ORG_DATA:obj,SORT_ORD:++rowseq,DB_KEY:$("#cboDbKey").val(), JOB_TYP:$('#cboJobDist').val(), JOB_TYP_NM:$("#cboJobDist").jqxComboBox("getSelectedItem").label, JOB_OBJ_ID:obj['JOB_OBJ_ID'],JOB_NM: obj['JOB_OBJ_NM'], JOB_PARAM:''});
				}
			}
			
			$(target).jqxGrid('addrow', null, datas);
			$(src).jqxGrid('deleterow',rowids);
			$(src).jqxGrid('clearselection');
		}
		function removeItem() {
			var src = "#gridSelectList";
			var target = "#listJobList";
			var rows = $(src).jqxGrid('getselectedrowindexes');
			var rowids=[];
			var datas=[];
			for(var i=0;i<rows.length;i++){
				datas=[];
				var id = $(src).jqxGrid('getrowid',rows[i]);
				rowids.push(id);
				
				var obj = $(src).jqxGrid('getrowdatabyid',id);
				
				var jobTyp = obj['JOB_TYP'];
				//datas.push({JOB_OBJ_NM:obj['JOB_NM']});	
				datas.push(obj.ORG_DATA);
				if(jobTyp == $('#cboJobDist').val()) {
					$(target).jqxGrid('addrow', null, datas);	
				}
			}
			
			$(src).jqxGrid('deleterow',rowids);
			$(src).jqxGrid('clearselection');
		
		}
		function addAll() {
			$('#listJobList').jqxGrid('selectallrows');
			add(); 
		}
		function removeAll() {
			$('#gridSelectList').jqxGrid('selectallrows');
			removeItem();
		}
		function checkValidator(){
			$("#saveForm").jqxValidator('validate');
		}
		function deyear(){
			if($("input[name=replayCheck]").is(":checked") ==true){
				$("#txtToDat").val("99999999");	
				$( "#txtToDat" ).datepicker( "destroy" );
			}else if($("input[name=replayCheck]").is(":checked") == false){
				$("#txtTodat").val("");
				$("#txtToDat").datepicker({
					showOn: "button",
					buttonImage: '../../resources/cmresource/image/icon-calendar.png',
					buttonImageOnly : true,
					dateFormat: "yymmdd"
				});
				$(".ui-datepicker-trigger").addClass("icon-calender f_left m_t7 pointer");
			}
		}
		function changeTime1(){
			$("div[name=numericInput3]").val(0);
			$("#txtATime").val("");
			$("#cboAType").jqxComboBox({ selectedIndex : 0 });
			
		}
		function changeTime3(){
			$("div[name=numericInput1]").val(0);
			$("div[name=numericInput2]").val(0);
		}
		function changeDate(){
			$("#txtCWeek").val("");
			$("#cboCDay").jqxComboBox({ selectedIndex : 0 });
			
		}
		function changeWeek(){
			$("#txtCDate").val("");
		}
		function changeDate2(){
			$("#txtDWeek").val("");
			$("#cboDDay").jqxComboBox({ selectedIndex : 0 });
			
		}
		function changeWeek2(){
			$("#txtDDate").val("");
		}
		function goList() {
			var param = '';
// 			param += '?tableGbn=${param.tableGbn}';
// 			param += '&txtSearch=${param.txtSearch}';
// 			param += '&etlYn=${param.etlYn}';
// 			param += '&etlGbn=${param.etlGbn}';
			location.replace("./list"+param);
		}
		</script>
		<style type="text/css">
		#gridmenulistJobList {
			display:none !important;
		}
		</style>
		<script>
		$("#columntablelistJobList").hover(function(){
			alert('gg');
		});
		</script>
	</head>
	<body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1160px; margin:0 10px;">
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:100%;">
					<div class="group f_left  w100p m_b5">
						<div class="group_button f_right">	
							<div class="button type2 f_left" style="margin-bottom:0;">
								<input type="button" value="목록" id='btnList' width="100%" height="100%" onclick="goList();" />
							</div>
							<div class="button type2 f_left" style="margin-bottom:0;">
								<input type="button" value="저장" id='btnInsert' width="100%" height="100%" onclick="insert();" />
							</div>
							<div class="button type3 f_left" style="margin-bottom:0;">
								<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();;" />
							</div>
						</div><!--group_button-->
					</div>
					<input type="hidden" id="txtAc" name="ac" />
					<div class="table  f_left" style="width:100%; margin:0; ">
						<table width="100%" cellspacing="0" cellpadding="0" border="0">
							<tbody>
								<tr>
									<th class="w10p"><div>스케줄명</div></th>
									<td colspan="3" class="w30p">
										<div class="cell">
											<input type="text" id="txtSchName" value="${mainList.SCH_NM}" class="input type2" style="width:98%; margin:3px 0; "/>
										</div>
									</td>
									<th rowspan="3" style="width:5%;"><div>설정</div></th>
									<th style="width:5%;"><div>주기</div></th>
									<td style="width:10%;">
										<div class="cell">
											<div class="combobox" onchange="detailList();">
												<select id="cboSchCyc" >
													<c:choose>
														<c:when test="${codeList != null && not empty codeList}">
															<c:forEach items="${codeList}" var="rows" varStatus="loop">
																<option value="${rows.COM_COD}" <c:if test="${rows.COM_COD == mainList.SCH_CYC}">selected</c:if>>${rows.COM_NM}</option>
															</c:forEach>
														</c:when>
													</c:choose>
												</select>
											</div>
										</div>
									</td>
									<td>
										<div id="jobList" name="jobList">
											<div style="display: none;">
												<span>비어있음</span>
											</div>
											<%-- 일~토 --%>
											<div id="checkDayList" name="checkDayList">
												<span>
													<label class="sideAlign right"><input type="checkbox" value="1" name="bDay" class="sideAlign" style="margin-bottom: 0px;" <c:if test="${fn:indexOf(mainList.B_DAY, '1') != -1}"> checked</c:if>>일요일</label>
													<label class="sideAlign right"><input type="checkbox" value="2" name="bDay" class="sideAlign" style="margin-bottom: 0px;" <c:if test="${fn:indexOf(mainList.B_DAY, '2') != -1}"> checked</c:if>>월요일</label>
													<label class="sideAlign right"><input type="checkbox" value="3" name="bDay" class="sideAlign" style="margin-bottom: 0px;" <c:if test="${fn:indexOf(mainList.B_DAY, '3') != -1}"> checked</c:if>>화요일</label>
													<label class="sideAlign right"><input type="checkbox" value="4" name="bDay" class="sideAlign" style="margin-bottom: 0px;" <c:if test="${fn:indexOf(mainList.B_DAY, '4') != -1}"> checked</c:if>>수요일</label>
													<label class="sideAlign right"><input type="checkbox" value="5" name="bDay" class="sideAlign" style="margin-bottom: 0px;" <c:if test="${fn:indexOf(mainList.B_DAY, '5') != -1}"> checked</c:if>>목요일</label>
													<label class="sideAlign right"><input type="checkbox" value="6" name="bDay" class="sideAlign" style="margin-bottom: 0px;" <c:if test="${fn:indexOf(mainList.B_DAY, '6') != -1}"> checked</c:if>>금요일</label>
													<label class="sideAlign right"><input type="checkbox" value="7" name="bDay" class="sideAlign" style="margin-bottom: 0px;" <c:if test="${fn:indexOf(mainList.B_DAY, '7') != -1}"> checked</c:if>>토요일</label>
												</span>
											</div>
											<div id="selectDayList" name="selectDayList">
												<div class="cell">
													<div class="group f_left">
														<input type="text" class="input type2" id="txtCDate" name="cDate" value="${mainList.C_DATE}" onchange="changeDate();"/>
														<label for="or"> 일 <span class="middelAlign">또는</span></label> <input type="text" id="txtCWeek" value="${mainList.C_WEEK}" class="input type2" onchange="changeWeek();"/><label for="nn"> 번째</label>
													</div>
													<div class="combobox f_left m_l10">
														<select id="cboCDay" name="cDay">
															<option value="" selected>=선택=</option>
															<option value="1" <c:if test="${mainList.C_DAY == '1'}">selected</c:if>>일요일</option>
															<option value="2" <c:if test="${mainList.C_DAY == '2'}">selected</c:if>>월요일</option>
															<option value="3" <c:if test="${mainList.C_DAY == '3'}">selected</c:if>>화요일</option>
															<option value="4" <c:if test="${mainList.C_DAY == '4'}">selected</c:if>>수요일</option>
															<option value="5" <c:if test="${mainList.C_DAY == '5'}">selected</c:if>>목요일</option>
															<option value="6" <c:if test="${mainList.C_DAY == '6'}">selected</c:if>>금요일</option>
															<option value="7" <c:if test="${mainList.C_DAY == '7'}">selected</c:if>>토요일</option>
														</select>
													</div>
												</div>
											</div>
											<div id="selectMonList" name="selectMonList">
												<span>
													<div class="combobox f_left m_r10">
														<select id="cboDMon" name="dMon">
															<option value="" selected>=선택=</option>
															<option value="1" <c:if test="${mainList.D_MON == '1'}">selected</c:if>>1월</option>
															<option value="2" <c:if test="${mainList.D_MON == '2'}">selected</c:if>>2월</option>
															<option value="3" <c:if test="${mainList.D_MON == '3'}">selected</c:if>>3월</option>
															<option value="4" <c:if test="${mainList.D_MON == '4'}">selected</c:if>>4월</option>
															<option value="5" <c:if test="${mainList.D_MON == '5'}">selected</c:if>>5월</option>
															<option value="6" <c:if test="${mainList.D_MON == '6'}">selected</c:if>>6월</option>
															<option value="7" <c:if test="${mainList.D_MON == '7'}">selected</c:if>>7월</option>
															<option value="8" <c:if test="${mainList.D_MON == '8'}">selected</c:if>>8월</option>
															<option value="9" <c:if test="${mainList.D_MON == '9'}">selected</c:if>>9월</option>
															<option value="10" <c:if test="${mainList.D_MON == '10'}">selected</c:if>>10월</option>
															<option value="11" <c:if test="${mainList.D_MON == '11'}">selected</c:if>>11월</option>
															<option value="12" <c:if test="${mainList.D_MON == '12'}">selected</c:if>>12월</option>
														</select>
													</div>
													<div class="group f_left">
														<input type="text" id="txtDDate" name="dDate" value="${mainList.D_DATE}" class="input type2" onchange="changeDate2();"/>
														<label for="or"> 일 <span class="middelAlign">또는</span></label>
														<input type="text" id="txtDWeek" name="dWeek" value="${mainList.D_WEEK}" class="input type2" onchange="changeWeek2();"/>
														<label for="nn"> 번째</label>
													</div>
													<div class="combobox f_left m_l10">
														<select id="cboDDay" name="dDay">
															<option value="" selected>=선택=</option>
															<option value="1" <c:if test="${mainList.D_DAY == '1'}">selected</c:if>>일요일</option>
															<option value="2" <c:if test="${mainList.D_DAY == '2'}">selected</c:if>>월요일</option>
															<option value="3" <c:if test="${mainList.D_DAY == '3'}">selected</c:if>>화요일</option>
															<option value="4" <c:if test="${mainList.D_DAY == '4'}">selected</c:if>>수요일</option>
															<option value="5" <c:if test="${mainList.D_DAY == '5'}">selected</c:if>>목요일</option>
															<option value="6" <c:if test="${mainList.D_DAY == '6'}">selected</c:if>>금요일</option>
															<option value="7" <c:if test="${mainList.D_DAY == '7'}">selected</c:if>>토요일</option>
														</select>
													</div>
												</span>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<th rowspan="2"><div>사용여부</div></th>
									<td rowspan="2">
										<div class="cell">
											<div class="combobox">
												<select id="cboUseYn" name="useYn">
													<option value="Y" <c:if test="${mainList.USE_YN == 'Y'}">selected</c:if>>사용</option>
													<option value="N" <c:if test="${mainList.USE_YN == 'N'}">selected</c:if>>미사용</option>
												</select>
											</div>
										</div>
									</td>
									<th rowspan="2"><div>분산처리</div></th>
									<td rowspan="2">
										<div  class="cell">
											<label for=""><input type="radio" name="distYn" value="Y" class="m_b2 m_r3" <c:if test="${mainList.DIST_YN == 'Y' || mainList.DIST_YN == null}">checked="checked"</c:if> />Yes</label>
											<label for=""><input type="radio" name="distYn" value="N" class="m_b2 m_r3 m_l5" <c:if test="${mainList.DIST_YN == 'N'}">checked="checked"</c:if> />No</label>
										</div>
									</td>
									<th><div>기간</div></th>
									<td colspan="2">
										<div  class="cell">
											<input type="text" id="txtFromDat" name="fromDat" class="input type1 t_center f_left" readonly style="width:100px; margin:3px 0;" value="${mainList.FROM_DAT}" />
											<div class="f_left m_l5 m_r5 m_t2">&sim;</div>
											<input type="text" id="txtToDat" name="toDat" class="input type1 t_center f_left" readonly style="width:100px; margin:3px 0;" value="${mainList.TO_DAT}" />
											<label for="" class="f_left m_t5 m_l10" ><input type="checkbox" value="replayCheck" id="replayCheck" name="replayCheck" onclick="deyear();" <c:if test="${mainList.TO_DAT == 99999999}">checked</c:if>>무한반복</label>
										</div>
									</td>
								</tr>
								<tr>
									<th><div>시간</div></th>
									<td colspan="2">
										<div class="cell">
											<c:if test="${mainList.A_TYPE == null || mainList.A_TYPE == ''}">
												<div id='txtFromTime' name="numericInput1" class="numericInputStyle" value="${mainList.FROM_TIME}" style="float: left;" onchange="changeTime1();"></div>
												<div id='txtFromMinute' name="numericInput2" class="numericInputStyle" value="${mainList.FROM_MINUTE}" style="float: left; margin-left: 5px !important;" onchange="changeTime1();"></div>
											</c:if>
											<c:if test="${mainList.A_TYPE != null || mainList.A_TYPE != ''}">
												<div id='txtFromTime' name="numericInput1" class="numericInputStyle" value="0" style="float: left;" onchange="changeTime1();"></div>
												<div id='txtFromMinute' name="numericInput2" class="numericInputStyle" value="0" style="float: left; margin-left: 5px !important;" onchange="changeTime1();"></div>
											</c:if>
											<div id="detailTime">
												<label for="" class="f_left m_t3 m_l5 m_r5" >또는</label>
												<c:if test="${mainList.A_TYPE == null || mainList.A_TYPE == ''}">
													<div id='txtFromTime1' name="numericInput3" class="numericInputStyle" style="float: left; margin-left: 5px !important;" onchange="changeTime3();"></div>
													<label for="" class="f_left m_t3 m_l5 m_r5" >부터</label>
													<label for="" class="f_left m_t3 m_l5 m_r5" >매</label>
													<input id="txtATime" name="aTime" class="f_left input type1" style="width:50px; margin:0px 5px;" onchange="changeTime3();" type="text" />
													<div class="combobox">
														<select id="cboAType">
															<option value="" selected>==선택==</option>
															<option value="1">시간</option>
															<option value="2">분</option>
														</select>
													</div>
												</c:if>
												<c:if test="${mainList.A_TYPE != null && mainList.A_TYPE != ''}">
													<div id='txtFromTime1' name="numericInput3" class="numericInputStyle" value="${mainList.FROM_TIME}" style="float: left; margin-left: 5px !important;" onchange="changeTime3();"></div>
													<label for="" class="f_left m_t3 m_l5 m_r5" >부터</label>
													<label for="" class="f_left m_t3 m_l5 m_r5" >매</label>
													<input id="txtATime" name="aTime" class="f_left input type1" style="width:50px; margin:0px 5px;" onchange="changeTime3();" type="text" value="${mainList.A_TIME}" />
													<div class="combobox" id='jqxCombobox03'>
														<select id="cboAType" name="aType">
															<option value="" selected>==선택==</option>
															<option value="1" <c:if test="${mainList.A_TYPE == '1'}">selected</c:if>>시간</option>
															<option value="2" <c:if test="${mainList.A_TYPE == '2'}">selected</c:if>>분</option>
														</select>
													</div>
												</c:if>
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="content content_style f_left" style=" width:30%; margin:20px 0; padding:10px 1%;">
						<div class="group w100p f_left" style="margin:0px 0;">
							<div class="group w100p f_left" style="margin:3px 0;">
								<div class="label type2 f_left">구분</div>
								<div class="combobox f_left" onchange="getServerJobList();">
									<select id="cboJobDist" name="jobDist">
										<c:choose>
											<c:when test="${jobDistList != null && not empty jobDistList}">
												<c:forEach items="${jobDistList}" var="rows" varStatus="loop">
													<option value="${rows.COM_COD}">${rows.COM_NM}</option>
												</c:forEach>
											</c:when>
										</c:choose>
									</select>
								</div> 
								<div class="combobox f_left" id='cboDbKey' name="dbKey" onchange="getServerJobList();"></div>
							</div>
							<div class="group w100p f_left" style="margin:3px 0;">
								<div class="label type2 f_left">검색</div>
								<input type="text" id="txtSearchWord" class="input type1 f_left w65p" style="width:100%; margin:0px 0px 0 5px; " onkeydown="if(event.keyCode == 13){addfilter();}" value="${mainList.JOB_NM}"/>
								<div class="button type2 f_left">
									<input type="button" value="검색" id='btnSearch' width="100%" height="100%" onclick="addfilter();"/>
								</div>
							</div>
						</div>
						<div class="grid f_left" style="width:100%; height:440px;"> 
							<div id='listJobList' class="f_left" style=" width:100%; margin:0px 0;"></div>
						</div>
					</div>
					<div class="button_arrow f_left" style="width: 9.8%; margin: 100px 0 0 0;">
						<ul>
							<li class="button_arrow_right">
								<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/btn-move-right.png" onmouseover="this.src='../../resources/cmresource/css/iplanbiz/theme/blueish/img/btn-hover-move-right.png';" onmouseout="this.src='../../resources/cmresource/css/iplanbiz/theme/blueish/img/btn-move-right.png';" onclick="add();" alt="오른쪽으로">
							</li>
							<li class="button_arrow_allright">
								<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/btn-move-all-right.png" onmouseover="this.src='../../resources/cmresource/css/iplanbiz/theme/blueish/img/btn-hover-move-all-right.png';" onmouseout="this.src='../../resources/cmresource/css/iplanbiz/theme/blueish/img/btn-move-all-right.png';" onclick="addAll();" alt="모두오른쪽으로">
							</li>
							<li class="button_arrow_left">
								<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/btn-move-left.png" onmouseover="this.src='../../resources/cmresource/css/iplanbiz/theme/blueish/img/btn-hover-move-left.png';" onmouseout="this.src='../../resources/cmresource/css/iplanbiz/theme/blueish/img/btn-move-left.png';" onclick="removeItem();" alt="왼쪽으로">
							</li>
							<li class="button_arrow_allleft">
								<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/btn-move-all-left.png" onmouseover="this.src='../../resources/cmresource/css/iplanbiz/theme/blueish/img/btn-hover-move-all-left.png';" onmouseout="this.src='../../resources/cmresource/css/iplanbiz/theme/blueish/img/btn-move-all-left.png';" onclick="removeAll();" alt="모두왼쪽으로">
							</li>
						</ul>
					</div>
					<div class="content f_left" style=" width:58%; margin:20px 0;">
						<div class="grid f_left" style="width:100%; height:530px;"> 
							<div id="gridSelectList"></div>
						</div>
					</div>
					<form id="saveForm" name="saveForm" method="post">
						<input type="hidden" id="hiddenSchID" name="schID" value="${mainList.SCH_ID}" />
						<input type="hidden" id="hiddenSchName" name="schName" />
						<input type="hidden" id="hiddenDistYn" name="distYn" />
						<input type="hidden" id="hiddenUseYn" name="useYn" />
						<input type="hidden" id="hiddenSchStatus" name="schStatus" />
						<input type="hidden" id="hiddenSchCyc" name="schCyc" />
						<input type="hidden" id="hiddenAType" name="aType"/>
						<input type="hidden" id="hiddenATime" name="aTime" />
						<input type="hidden" id="hiddenBDay" name="bDay" />
						<input type="hidden" id="hiddenCDate" name="cDate" />
						<input type="hidden" id="hiddenCWeek" name="cWeek" />
						<input type="hidden" id="hiddenCDay" name="cDay" />
						<input type="hidden" id="hiddenDMon" name="dMon" />
						<input type="hidden" id="hiddenDDate" name="dDate" />
						<input type="hidden" id="hiddenDWeek" name="dWeek" />
						<input type="hidden" id="hiddenDDay" name="dDay" />
						<input type="hidden" id="hiddenFromDat" name="fromDat" />
						<input type="hidden" id="hiddenToDat" name="toDat" />
						<input type="hidden" id="hiddenFromTime" name="fromTime" />
						<input type="hidden" id="hiddenFromMinute" name="fromMinute" />
					</form>
				</div>
			</div>
		</div>
	</body>
</html>