<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>스케쥴러 관리</title>
       <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <script>
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init(); 
        	}); 
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		$("#btnSearch").jqxButton({width:'', theme:'blueish'});
        		$("#btnNew").jqxButton({width:'', theme:'blueish'});
        		makeCombobox();
        	}
        	//조회 
        	function search(){
        		var schedulerData = $i.post("./getSchedulerManagerList", { useYn : $("#cboUseYn").val(), schStatus : $("#cboJobStatus").val()});
        		schedulerData.done(function(schData){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'SCH_ID', type: 'string' },
		                    { name: 'SCH_NM', type: 'string' },
		                    { name: 'SCH_STATUS', type: 'string'},
		                    { name: 'SCH_SNM', type: 'string'},
		                    { name: 'SCH_CNM', type: 'string'},
		                    { name: 'FROM_DAT', type: 'string'},
		                    { name: 'TO_DAT', type: 'string'},
		                    { name: 'TIME', type:'string'},
		                    { name: 'SCH_CYC_DETAIL', type:'string'}
		                ],
		                localdata: schData.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
        			 var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var detailRenderer = function(row, datafield, value, defaultHtml, property, rowdata){
						var schID = rowdata.SCH_ID;
						var schStatus = rowdata.SCH_STATUS;
						var newValue = $i.secure.scriptToText(value);
						var link = "<a href=\"javaScript:detailScheduler('"+schID+"','"+schStatus+"')\" style='color:black;'>" + newValue + "</a>";
		            	
		            	return "<div style='text-align:left;text-decoration:underline; padding-bottom:2px; margin:4px 0px 0px 10px; color:black;'>" + link + "</div>";
					};
					var buttonRenderer = function(row, datafield, value, defaultHtml, property, rowdata){
						var schID = rowdata.SCH_ID;
						var schStatus = rowdata.SCH_STATUS;
						var imglink = "";
						
						if(schStatus == "UNREG"){
							imglink = "<a href=\"javascript:setSchedulerData('"+schStatus+"','"+schID+"');\"'><img src='../../resources/img/button/button_Enrollment.png' class='button'/></a>";
						}else if(schStatus == "WAIT"){
							imglink = "<a href=\"javascript:setSchedulerData('"+schStatus+"','"+schID+"');\"'><img src='../../resources/img/button/button_notEnrollment.png' class='button'/></a>";
						}else if(schStatus == "RUN"){
							imglink = "<img src='../../resources/img/button/button_Default.png' class='button'/>";
						}
						return "<div style='text-align:center; padding-bottom:2px; margin-top:3px;margin-left:0px; color:red !important;'>" + imglink + "</div>"; 
					};
		            $("#gridSchedulerList").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                pageable: false,
		                columns: [
							{ text: '번호', datafield:'SCH_ID', width:'5%', align:'center', cellsalign:'center'},
							{ text: '스케쥴명', datafield: 'SCH_NM', width: '35%', align:'center', cellsalign: 'center', cellsrenderer: detailRenderer},
							{ text: '상태', datafield: 'SCH_SNM', width: '10%', align:'center', cellsalign: 'center' },
							{ text: '스케쥴등록', datafield: 'a', width: '10%', align:'center', cellsalign: 'center' ,columtype:'button',cellsrenderer: buttonRenderer },
							{ text: '주기', datafield: 'SCH_CYC_DETAIL', width: '10%', align:'center', cellsalign: 'center' },
							{ text: '시작일', datafield: 'FROM_DAT',  width: '10%', align:'center', cellsalign: 'center' },
							{ text: '종료일', datafield: 'TO_DAT', width:'10%',align:'center', cellsalign: 'center' },
							{ text: '시간', datafield:'TIME', width: '10%',align:'center', cellsalign:'center'}
						]
		            });
        		});
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function newScheduler(){
        		location.replace('./detail');
        	}
        	function detailScheduler(schID, schStatus){
        		if(schStatus == "WAIT"){
        			$i.dialog.error("SYSTEM", "이미 등록된 스케쥴러 입니다.\n 스케쥴러 등록을 해제 해주세요");
        		}else{
        			location.replace("./detail?schID="+schID+"&useYn="+$("#cboUseYn").val()+"&jobStatus="+$("#cboJobStatus").val());
        		}
        		//location.replace("./detail?schID="+schID+"&useYn="+$("#cboUseYn").val()+"&jobStatus="+$("#cboJobStatus").val());
        	}
        	function setSchedulerData(schStatus,schID){
        		var data = $i.post("./setSchedulerData", {schID:schID, schStatus:schStatus});
        		data.done(function(res){
        			if(res.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM", res.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM", res.returnMessage, function(){
        					search();
        				});
        			}
        		});
        	}
        	function makeCombobox(){
        		var dat = $i.post("./getCodeListByScheduler", { comlCod : "USE_YN" });
        		dat.done(function(res){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                     { name: 'COM_COD' },
		                     { name: 'COM_NM' }
		                ],
		                id: 'id',
						localdata:res.returnArray,
		                async: false
		            };
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#cboUseYn").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 110, dropDownHeight: 80, width: 110, height: 22,theme:'blueish'});
		            var dat = $i.post("./getCodeListByScheduler", { comlCod : "JOB_STATUS" });
	        		dat.done(function(res){
	        			var source =
			            {
			                datatype: "json",
			                datafields: [
			                     { name: 'COM_COD' },
			                     { name: 'COM_NM' }
			                ],
			                id: 'id',
							localdata:res.returnArray,
			                async: false
			            };
			            var dataAdapter = new $.jqx.dataAdapter(source);
			            $("#cboJobStatus").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 110, dropDownHeight: 80, width: 110, height: 22,theme:'blueish'});
			            $("#cboJobStatus").jqxComboBox("insertAt", {label:"==전체==",value:"Z"},0);
			            search();
	        		});
        		});
        	}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
		</style>
    </head>
    <body class='blueish'>
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%; min-width:1160px; margin:0 10px;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<form id="searchForm" name="searchForm" action="./list">
					<div class="label type1 f_left">사용여부 : </div>
					<div class="combobox f_left" id="cboUseYn"></div>
					<div class="label type1 f_left">상태 : </div>
					<div class="combobox f_left" id="cboJobStatus"></div>
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
						<div class="button type1 f_left">
							<input type="button" value="신규" id='btnNew' width="100%" height="100%" onclick="newScheduler();" />
						</div>
					</div>
				</form>
			</div>
			<div class="container  f_left" style="width:100%;margin-top:5px;">
				<div class="content f_left" style="width:100%;">
					<div class="grid f_left" style="width:100%; height:650px;">
						<div id="gridSchedulerList"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>