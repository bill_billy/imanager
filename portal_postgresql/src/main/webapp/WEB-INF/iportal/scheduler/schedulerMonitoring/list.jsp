<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>스케쥴러 모니터링</title>
       <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <script>
	        var pushonserver = function(info){
				var data = Enumerable.From($("#gridSchedulerList").jqxGrid("getrows")).Where(function(c){ return c.SCH_ID == info.SCH_ID;}).FirstOrDefault();
					
				data.SCH_STATUS = info.STATUS;
				if(info.STATUS=="RUN")
					data.SCH_SNM = "실행중";
				else if(info.STATUS=="WAIT")
					data.SCH_SNM = "대기중";
				else if(info.STATUS=="STOP"){
					data.SCH_SNM = "대기중";
					data.SCH_STATUS = "WAIT";
				}
				else if(info.STATUS=="UNREG")
					data.SCH_SNM = "미등록";
				else
					data.SCH_SNM = "알수없음";
				$("#gridSchedulerList").jqxGrid("refreshdata");
			};
			
			var executeJob = function(schId) {
				var data = Enumerable.From($("#gridSchedulerList").jqxGrid("getrows")).Where(function(c){ return c.SCH_ID == schId;}).FirstOrDefault();
				
				data.SCH_SNM = "실행중";
				data.SCH_STATUS = "RUN";
				$("#gridSchedulerList").jqxGrid("refreshdata");
				
				var runData = $i.post("./runJob", {schID:schId});
				runData.done(function(res){
					if(res.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", res.returnMessage,function(){
							search();
							$('#gridSchedulerList').jqxGrid('clearselection');
							$('#gridSchedulerList').jqxGrid('selectrow',0);
							goJobPro(schId,"TOTAL");
						});
					}else{
						$i.dialog.alert("SYSTEM", res.returnMessage, function(){
							search();
							$('#gridSchedulerList').jqxGrid('clearselection');
							$('#gridSchedulerList').jqxGrid('selectrow',0);
							goJobPro(schId,"TOTAL");
						});
					}
				});
			};
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init(); 
        	}); 
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		$("#btnSearch").jqxButton({width:'', theme:'blueish'});
        		makeCombobox();
        	}
        	//조회 
        	function search(){
        		var data = $i.post("./getSchedulerMonitorData", { jobStatus : $("#cboJobStatus").val()});
        		data.done(function(schData){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'SCH_ID', type: 'string' },
		                    { name: 'SCH_NM', type: 'string' },
		                    { name: 'START_DAT', type: 'string' },
		                    { name: 'END_DAT', type: 'string' },
		                    { name: 'LEAD_TIME', type: 'string' },
		                    { name: 'RESULT_NM', type: 'string' },
		                    { name: 'SCH_CYC', type: 'string' },
		                    { name: 'SCH_STATUS', type: 'string' },
		                    { name: 'SCH_SNM', type: 'string' },
		                    { name: 'SUCESS', type: 'string' },
		                    { name: 'ERROR', type: 'string' },
		                    { name: 'TOTAL', type: 'string'}
		                ],
		                localdata: schData.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
        			 var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var detail = function(row,datafieId,value,defaultHtml,property,rowdata){
						var schId = rowdata.SCH_ID;
						var pGubn = rowdata.SCH_STATUS;
			        	var schStatus = $("#statusCombo").val();
			        	
			        	var imglink = "";
			        	if(pGubn == "UNREG" || pGubn == "WAIT"){
			        		imglink = "<a href=\"javascript:executeJob(" + schId +");\"'><img src='../../resources/img/button/button_Execution.png' class='button'/></a>";
			        	}else if(pGubn == "RUN"){
			        		imglink = " - ";
			        	}
			        	return "<div style='text-align:center; padding-bottom:2px; margin-top:3px;margin-right:5px;'>" + imglink + "</div>";
			        };
			        var statuscolor = function(row,datafield,value){
						var color = "black";
						if(value!="대기중") color="red";
			        	var link = "<span style='color:"+color+";'>" + value + "</span>";
			        	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px;'>" + link + "</div>";
			        };
			        var totalSchList = function(row,datafieId,value,defaultHtml,property,rowdata){
			        	var schId = rowdata.SCH_ID;
			        	var schNm = rowdata.SCH_NM;
			        	var link = "<a href=\"javaScript:goJobPro('"+schId+"','TOTAL')\" style='color:black;text-decoration:underline;'>" + value + "</a>";
			        	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px;'>" + link + "</div>";
			        };
			        var sucessSchList = function(row,datafieId,value,defaultHtml,property,rowdata){
			        	var schId = rowdata.SCH_ID;
			        	var schNm = rowdata.SCH_NM;
			        	var link = "<a href=\"javaScript:goJobPro('"+schId+"','SUCESS')\" style='color:black;text-decoration:underline;'>" + value + "</a>";
			        	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px;'>" + link + "</div>";
			        };
			        var errorSchList = function(row,datafieId,value,defaultHtml,property,rowdata){
			        	var schId = rowdata.SCH_ID;
			        	var schNm = rowdata.SCH_NM;
			        	var link = "<a href=\"javaScript:goJobPro('"+schId+"','ERROR')\" style='color:black;text-decoration:underline;'>" + value + "</a>";
			        	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px;'>" + link + "</div>";
			        };
			        var color = function(row,datafieId,value,defaultHtml,property,rowdata){
			        	var resultNm = rowdata.RESULT_NM;
			        	var link = "";
			        	if(resultNm == "성공"){
			        		link = "<span style='color:blue;'>"+value+"</span>";   
			        	}else if(resultNm == "오류"){
			        		link = "<span style='color:red;'>"+value+"</span>";
			        	}
			        	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;'>" + link + "</div>";
			        };
			        var uRendererLeft = function (row, columnfield, value, defaulthtml, columnproperties, rowdata) {//왼쪽정렬
			        	var newValue = $i.secure.scriptToText(value);
						if (value < 20) {
							return '<span style="margin:7px 10px 7px 20px; float: ' + columnproperties.cellsalign + '; color: ;">' + newValue + '</span>';
						} else {
							return '<span style="margin: 7px 10px 7px 20px; float: ' + columnproperties.cellsalign + '; color: ;">' + newValue + '</span>';
						}
					}; 
		            $("#gridSchedulerList").jqxGrid(
		            {
		              	width: '100%',
		                height:'260px',
						altrows:true,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                pageable: false,
		                columns: [
							{ text: '스케줄 ID', dataField: 'SCH_ID', width: '5%', align:'center', cellsalign: 'center' },
							{ text: '스케줄명',  dataField: 'SCH_NM', width: '21%', align:'center',  cellsalign: 'left', cellsrenderer: uRendererLeft },
							{ text: '최근 실행 일시',  dataField: 'START_DAT', width: '10%', align:'center',  cellsalign: 'center'},
							{ text: '최근 종료 일시',  dataField: 'END_DAT', width: '10%', align:'center',  cellsalign: 'center'},
							{ text: '소요시간(초)',  dataField: 'LEAD_TIME', width: '8%', align:'center',  cellsalign: 'center'},
							{ text: '결과',  dataField: 'RESULT_NM', width: '5%', align:'center',  cellsalign: 'center', cellsrenderer:color},
							{ text: '주기',  editable: false, dataField: 'SCH_CYC', width: '10%', cellsalign: 'center', align:'center'  },
							{ text: '상태',  editable: false, dataField: 'SCH_SNM', width: '8%', cellsalign: 'center', align:'center' , cellsrenderer:statuscolor },
							{ text: '실행',  editable: false,  datafield: '실행', width: '5%', cellsalign: 'center', align:'center',columtype:'button',cellsrenderer:detail},
							{ text: '정상작동',  editable: false, dataField: 'SUCESS', width: '6%', cellsalign: 'center', align:'center', cellsrenderer:sucessSchList},
							{ text: '오류작동',  editable: false, dataField: 'ERROR', width: '6%', cellsalign: 'center', align:'center', cellsrenderer:errorSchList},
							{ text: 'TOTAL',  editable: false, dataField: 'TOTAL', width: '6%', cellsalign: 'center', align:'center', cellsrenderer:totalSchList}
						]
		            });
        		});
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function goJobPro(schID, gubn) {
				var selectedObj = $("#gridSchedulerList").jqxGrid("getrowdata", $('#gridSchedulerList').jqxGrid('selectedrowindex'));
		    	if(selectedObj != null && selectedObj != 'undefined') {
		    		$("#labelSchedulerID").html(selectedObj.SCH_ID);
		    		var newValue = $i.secure.scriptToText(selectedObj.SCH_NM);
		    		$("#labelSchedulerName").html(newValue);
		    		var schNm = newValue;
		    		$("#pJobNm").val(schNm);	
		    	}
		    	var detailData = $i.post("./getSchedulerMonitorDetailData", {schID:schID, resultGubn : gubn});
		    	detailData.done(function(data){
		    		var source =
		    		{
						localdata: data.returnArray,
						datafields:
		                [
		                    { name: 'IDX', type: 'string' },
		                    { name: 'SCH_ID', type: 'string' },
		                    { name: 'START_DAT', type: 'string' },
		                    { name: 'END_DAT', type: 'string' },
		                    { name: 'LEAD_TIME', type: 'string' },
							{ name: 'CMD_TYPE', type: 'string' },
							{ name: 'RESULT', type: 'string' }
		                ],
		                datatype: "json"
		            };
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            	
		            var errList = function(row,columnfield,value,defaulthtml,columnproperties,rowdata) {
		            	var schId = rowdata.SCH_ID;
		            	var idx = rowdata.IDX;
		            	var result = rowdata.RESULT;
			            //var schNm = $i.secure.scriptToText($("#pJobNm").val());
			            var link = "";
			            
			            if(result=="오류") {
			            	link = "<a href=\"javaScript:goErrorDetail('"+idx+"','"+schId+"')\" style='color:black;'>오류상세보기</a>";
			            }
			            
			            return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px;'>" + link + "</div>";
					};
					
					$("#jobDetail").jqxGrid(
					{
						width: '100%',
						height: '400px',
						source: dataAdapter,
						sortable: false,
						pageable:true,
						pagesize:100,
						pagesizeoptions:[100,200,300],
						theme:'blueish',
						columnsheight:30 ,
						rowsheight: 30,
						altrows:true,
						ready: function () {
							$("#jobDetail").jqxGrid();
						},
						columns: [
							{ text: '순번', dataField: 'IDX', width: '5%', align:'center', cellsalign: 'center' },
							{ text: '구분',  dataField: 'CMD_TYPE', width: '25%', align:'center',  cellsalign: 'center'},
							{ text: '실행일시',  dataField: 'START_DAT', width: '15%', align:'center',  cellsalign: 'center'},
							{ text: '종료일시',  dataField: 'END_DAT', width: '15%', align:'center',  cellsalign: 'center'},
							{ text: '소요시간(초)',  dataField: 'LEAD_TIME', width: '10%', align:'center',  cellsalign: 'center'},
							{ text: '작동결과',  editable: false, dataField: 'RESULT', width: '10%', cellsalign: 'center', align:'center'  },
							{ text: '비고',  editable: false, dataField: 'BIGO', width: '20%', cellsalign: 'center', align:'center' ,cellsrenderer:errList }
						]
					});
					$('#gridpagerlistjobDetail').css('width', '44px');//페이지 디폴트 선택 숫자 안보임.
					$('#dropdownlistContentgridpagerlistjobDetail').css('width', '19px');//페이지 디폴트 선택 숫자 안보임.
		    	});
		    }
        	function detailScheduler(schID){
        		location.replace("./detail?schID="+schID+"&useYn="+$("#cboUseYn").val()+"&jobStatus="+$("#cboJobStatus").val());
        	}
        	function setSchedulerData(schStatus,schID){
        		var data = $i.post("./setSchedulerData", {schID:schID, schStatus:schStatus});
        		data.done(function(res){
        			if(res.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM", res.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM", res.returnMessage, function(){
        					search();
        				});
        			}
        		});
        	}
        	function makeCombobox(){
       		 	var dat = $i.post("./getCodeListBySchedulerMonitoring", { comlCod : "JOB_STATUS" });
        		dat.done(function(res){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                     { name: 'COM_COD' },
		                     { name: 'COM_NM' }
		                ],
		                id: 'id',
						localdata:res.returnArray,
		                async: false
		            };
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#cboJobStatus").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 120, dropDownHeight: 150, width: 120, height: 22,theme:'blueish'});
		            $("#cboJobStatus").jqxComboBox("insertAt", {label:"==전체==",value:"Z"},0);
		            search();
		            goJobPro('','');
        		});
        	}
        	function goErrorDetail(idx, schId){
        		window.open("./errorDetail?pIdx="+idx+ "&schId=" + schId , "오류새창","width=1500, height=700");
        	}
        	function getJobDetail(schId, schNm, idx){ 
        		window.open("./jobDetail?schID=" + schId+ "&schName="+schNm+"&idx="+idx, "JOB 로그","width=1500, height=610");
        	}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
		</style>
    </head>
    <body class='blueish'>
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%; min-width:1160px; margin:0 10px;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<form id="searchForm" name="searchForm" action="./list">
					<div class="label type1 f_left">상태 : </div>
					<div class="combobox f_left" id="cboJobStatus"></div>
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
					</div>
				</form>
			</div>
			<div class="container  f_left" style="width:100%; margin:5px 0;">
				<div class="content f_left" style="width:100%;">
					<div class="grid f_left" style="width:100%; height:40%;">
						<div id="gridSchedulerList" style="height:260px !important;"></div>
					</div>
					<div class="grid f_left" style="width:100%;">
						<div class="label type2 f_left m_l10">상세이력 : (스케쥴 ID : <span class="colorchange" id="labelSchedulerID"></span> 스케쥴 명 : <span id="labelSchedulerName"></span> )</div>
						<input type="hidden" id="pJobNm"/>
						<div id="jobDetail" style="height:400px !important;"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>