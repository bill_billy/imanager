﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<title>오류 상세 보기</title>

<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/css/iplanbiz/styles/iplanbiz.basic.css"/>
<%--<link rel="stylesheet" type="text/css" href="../../resources/css/iplanbiz/styles/jqx.SophisticatedLayout.css"/> --%>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>

<script language="javascript" type="text/javascript">
	
	function goSqlDetail(idx, errIdx){
		var sqlData = $i.post("./getSchedulerSqlDetail", {errIDX:errIdx, idx:idx});
		sqlData.done(function(res){
			var errSql = res.returnArray[0].ERR_SQL;
			errSql = (errSql == null || errSql == 'undefined')?'':errSql;
			$("#errSql").val(errSql);
		});
	}

	function crateGrid() {
		var errorData = $i.post("./getSchedulerDetailError", {schID:"${param.schId}", idx:"${param.pIdx}"});
		errorData.done(function(res){
			var source =
			{
				localdata: res,
				datafields:
				[
					{ name: 'IDX', type:'string'},
					{ name: 'ERR_IDX', type:'string'},
					{ name: 'SCH_ID', type:'string'},
					{ name: 'JOB_NM', type:'string'},
					{ name: 'JOB_TYP', type:'string'},
					{ name: 'JOB_TYP_NM', type:'string'},
					{ name: 'START_DAT', type:'string'},
					{ name: 'TAB_NM', type:'string'},
					{ name: 'ERR_MESSAGE', type:'string'},
					{ name: 'ERR_SQL', type:'string'}
				],
				datatype: "json"
			};
			
			var dataAdapter = new $.jqx.dataAdapter(source);
			var detail = function(row,datafield,value) {
				var newValue = $i.secure.scriptToText(value);
				var idx = $("#jqxgrid1").jqxGrid("getrowdata", row).IDX;
				var errIdx = $("#jqxgrid1").jqxGrid("getrowdata", row).ERR_IDX;
				var link = "<a href=\"javaScript:goSqlDetail('"+idx+"','"+errIdx+"')\" style='color:black;'>"+newValue+"</a>";
				return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:5px;'>" + link + "</div>";
			};
			
			$("#jqxgrid1").jqxGrid(
			{
				width: '100%',
				height: '100%',
				source: dataAdapter,
				sortable: false,
				pageable: true,
				pagesize: 10,
				theme:'blueish',
				columnsheight:30 ,
				rowsheight: 30,
				altrows:true,
				ready: function () {
					$("#jqxgrid1").jqxGrid();
				},
				columns: [
					{ text: '순서', dataField: 'ERR_IDX', width: '7%', align:'center',  cellsalign: 'center'},
					{ text: '이름', dataField: 'JOB_NM', width: '15%', align:'center',  cellsalign: 'center' , cellsrenderer:detail},
					{ text: '구분', dataField: 'JOB_TYP_NM', width: '8%', align:'center',  cellsalign: 'center'},
					{ text: '수행일자', dataField: 'START_DAT', width: '15%', align:'center',  cellsalign: 'center'},
					{ text: '에러 위치', dataField: 'TAB_NM', width: '15%', align:'center',  cellsalign: 'center'},
					{ text: '에러 메세지', dataField: 'ERR_MESSAGE', width: '40%', cellsalign: 'left', align:'center'}
				]
			});
			
			$('#gridpagerlistjqxgrid1').css('width', '44px');//페이지 디폴트 선택 숫자 안보임.
			$('#dropdownlistContentgridpagerlistjqxgrid1').css('width', '19px');//페이지 디폴트 선택 숫자 안보임.
		});
	}
	$(document).ready(function() {
		
		crateGrid();
		
		$("#jqxButton1").jqxButton({ width: '66',  theme:'ui-grid_SophisticatedLayout'});
		
		var schnm = $("#pJobNm",opener.document).val();
		$("#schNm").html(schnm);
	});
</script>
</head>
<body class="default i-DQ">
	<div class="wrap" style="width:96%; margin:0 auto;">
		<div class="i-DQ-header" style="width:100%; margin-top:20px;">
			<div class="iwidget_label  headertop" style="width:100%; float:left; margin-bottom:10px;  ">
				<div class="label-bg label-1" style="float:left;">
					<span>에러 상세(스케줄 ID:${param.schId}, 스케줄 명 :</span><span id="schNm"></span><span>)</span></div>
				<div class="headerbutton  buttonStyle" style="position:absolute; height:25px; margin-right:10px; right:0;">
					<input type="button" value="닫기" id='jqxButton1' width="100%" height="100%" onclick="self.close();" />
				</div>
			</div>   
		</div>
		<div class="i-DQ-contents" style="width:100%; height:100%; float:left;  margin-top:5px;;">
			<div class="left_Area" style="width:100%; height:60%; float:left; margin-top:0px;">
				<div class="iwidget_grid" style= "float:right; width:100%; height:320px !important;">
					<div id="jqxgrid1" class="grid2" style=" width:100%; height:100%; text-align:center !important"> </div>
				</div>
				<div class="table_Area" style="width:100%">
					<div class="label-bg label-5" style="float:left;" id ="errJobId">에러 메세지 상세</div>	   
					<br/>
					<br/>
					<table width="100%" class="i-DQ-table i-DQ-table_Schedule register-table" cellspacing="0" cellpadding="0" border="0" style="margin-bottom:0 !important;">
						<thead>
							<tr style="display:none">
								<td></td>    
							</tr>
						</thead>   
						<tbody>	    
							<tr>
								<td>
									<textarea id="errSql" name="errSql" class="textarea textareaStyle" style="width:99%; height:100px !important;" ></textarea>
								</td>
							</tr>   
						</tbody>
					</table>    
				</div>
			</div>
		</div>
	</div>
</body>
</html>