﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<title>Job별 상세</title>

<link rel="stylesheet" href="${WWW.CSS}/jqwidget3.5.0/styles/jqx.base.css" type="text/css" />
<link rel="stylesheet" href="${WWW.CSS}/jqwidget3.5.0/styles/iplanbiz.basic.css" type="text/css"/>
<link rel="stylesheet" href="${WWW.CSS}/jqwidget3.5.0/styles/jqx.SophisticatedLayout.css" type="text/css"/>

<script src="${WWW.JS}/iplanbiz/core/iplanbiz.web.include.jsp"></script>
<script src="${WEB.JSDWR}/interface/schedulerService.js"></script>

<script language="javascript" type="text/javascript">
	var paramSchID = "${param.schID}";
	var paramIDX = "${param.idx}";
	function goSqlDetail(idx, errIdx){
		schedulerService.goSqlDetail(idx, errIdx, {callback:function(res) {
			var errSql = res.ERR_SQL;
			errSql = (errSql == null || errSql == 'undefined')?'':errSql;
			$("#errSql").val(errSql);
		}, async:false});
	}
	function makeData(){
		if(paramIDX != ""){
			$iv.mybatis("getJobRunDetailListByIDX", {ACCT_ID:$iv.userinfo.acctid(), SCH_ID:paramSchID, IDX:paramIDX}, {callback:function(res){
				createGrid(res);
			}});
		}else{
			$iv.mybatis("getJobRunDetailList", {ACCT_ID:$iv.userinfo.acctid(), SCH_ID:paramSchID}, {callback:function(res){
				createGrid(res);
			}});	
		}
	}
	function createGrid(res) {
		var source =
		{
			localdata: res,
			datafields:
			[
				{ name: 'IDX', type:'int'},
				{ name: 'SCH_ID', type:'string'},
				{ name: 'JOB_ID', type:'string'},
				{ name: 'JOB_NM', type:'string'},
				{ name: 'JOB_DIST_NM', type:'string'},
				{ name: 'START_DAT', type:'string'},
				{ name: 'END_DAT', type:'string'},
				{ name: 'STATUS', type:'string'},
				{ name: 'LEAD_TIME', type:'int'},
				{ name: 'ERR_IDX', type: 'string'}
			],
			datatype: "json"
		};
		
		var dataAdapter = new $.jqx.dataAdapter(source);
		var detail = function(row,datafield,value,defaultHtml,property,rowdata) {
			var status = rowdata.STATUS;
			var idx = rowdata.IDX;
			var errIDX = rowdata.ERR_IDX;
			var link = "";
			if(status == "SUCCESS"){
				link = "<span style='color:blue';>성공</span>";
			}else{
				link = "<a href=\"javaScript:goSqlDetail('"+idx+"','"+errIDX+"')\" style='color:red;text-decoration:underline'>오류</a>";
			}
			return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:5px;'>" + link + "</div>";
		};
		
		$("#jqxgrid1").jqxGrid(
		{
			width: '100%',
			height: '100%',
			source: dataAdapter,
			sortable: true,
			pageable: true,
			pagesize: 100,
			pagesizeoptions:[100,200,300],
			theme:'grid_SophisticatedLayout',
			columnsheight:30 ,
			rowsheight: 30,
			altrows:true,
			ready: function () {
				$("#jqxgrid1").jqxGrid();
			},
			columns: [
				{ text: '순서', dataField: 'JOB_ID', width: '7%', align:'center',  cellsalign: 'center'},
				{ text: '이름', dataField: 'JOB_NM', align:'center',  cellsalign: 'left'},
				{ text: '구분', dataField: 'JOB_DIST_NM', width: '8%', align:'center',  cellsalign: 'center'},
				{ text: '시작시간', dataField: 'START_DAT', width: '15%', align:'center',  cellsalign: 'center'},
				{ text: '종료시간', dataField: 'END_DAT', width: '15%', align:'center', cellsalign:'center'},
				{ text: '소요시간(초)', dataField: 'LEAD_TIME', width: '8%', align: 'center', cellsalign:'center'},
				{ text: '상태', dataField: 'STATUS', width:'8%', cellsalign: 'left', align:'center', cellsrenderer:detail}
			]
		});
		setPagerLayout("jqxgrid1");
// 		$('#gridpagerlistjqxgrid1').css('width', '44px');//페이지 디폴트 선택 숫자 안보임.
		$('#dropdownlistContentgridpagerlistjqxgrid1').css('width', '19px');//페이지 디폴트 선택 숫자 안보임.
	}
	function setPagerLayout(selector) {
		try{
			var pagesize = $('#'+selector).jqxGrid('pagesize');
			
			var w = 49; 
				
			if(pagesize<100) {
				w = 44;
			} else if(pagesize>99&&pagesize<1000) {
				w = 49;
			} else if(pagesize>999&&pagesize<10000) {
				w = 54;
			}
			
			//디폴트 셋팅
			$('#gridpagerlist'+selector).jqxDropDownList({ width: w+'px' });
			
			//체인지 이벤트 처리
			$('#'+selector).on("pagesizechanged", function (event) {
				var args = event.args;
				
				if(args.pagesize<100) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '44px' });
				} else if(args.pagesize>99 && args.pagesize<1000) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '49px' });
				} else if(args.pagesize>999 && args.pagesize<10000) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '54px' });
				} else {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: 'auto' });
				}
			});
		}catch(e){
			console.log(e);
		}
	}
	function excelDown(){
		var param = "?";  
		if(paramIDX != ""){
			param += "method=jobRunDetailListByIDXExcelData"
			param +="&IDX="+paramIDX;
		}else{
			param += "method=jobRunDetailListExceData"
		}
		param +="&fileName=" + "${param.schName}의 오류내역";
		param +="&ACCT_ID=" + $iv.userinfo.acctid();
		param +="&SCH_ID=" + paramSchID;
		
		location.href="${WWW.ROOT}/comm/downExcel"+param;
	}
	$(document).ready(function() {
		
		makeData();
		
		$("#jqxButton1").jqxButton({ width: '66',  theme:'ui-grid_SophisticatedLayout'});
		$("#jqxButton2").jqxButton({ width: '66',  theme:'ui-grid_SophisticatedLayout'});
	});
</script>
</head>
<body class="default i-DQ">
	<div class="wrap" style="width:96%; margin:0 auto;">
		<div class="i-DQ-header" style="width:100%; margin-top:30px;">
			<div class="iwidget_label  headertop" style="width:100%; float:left; margin-bottom:10px;  ">
				<div class="label-bg label-1" style="float:left;">실행 내역(스케줄 ID:${param.schID}, 스케줄 명 :${param.schName})</div>
				<div class="headerbutton  buttonStyle" style="position:absolute; height:25px; margin-right:10px; right:0;">
					<input type="button" value="닫기" id='jqxButton1' width="100%" height="100%" onclick="self.close();" />
					<input type="button" value="Excel" id='jqxButton2' width="100%" height="100%" onclick="excelDown();" />
				</div>
			</div>   
		</div>
		<div class="i-DQ-contents" style="width:100%; height:100%; float:left;  margin-top:5px;;">
			<div class="left_Area" style="width:100%; height:60%; float:left; margin-top:0px;">
				<div class="iwidget_grid" style= "float:right; width:100%; height:320px !important;">
					<div id="jqxgrid1" class="grid2" style=" width:100%; height:100%; text-align:center !important"> </div>
				</div>
				<div class="table_Area" style="width:100%">
					<div class="label-bg label-5" style="float:left;" id ="errJobId">에러 메세지 상세</div>	   
					<br/>
					<br/>
					<table width="100%" class="i-DQ-table i-DQ-table_Schedule register-table" cellspacing="0" cellpadding="0" border="0" style="margin-bottom:0 !important;">
						<thead>
							<tr style="display:none">
								<td></td>    
							</tr>
						</thead>   
						<tbody>	    
							<tr>
								<td>
									<textarea id="errSql" name="errSql" class="textarea textareaStyle" style="width:99%; height:100px !important;" ></textarea>
								</td>
							</tr>   
						</tbody>
					</table>    
				</div>
			</div>
		</div>
	</div>
</body>
</html>