<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>${title}</title>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/css/iplanbiz/styles/table.css" />
	    <link rel="stylesheet" type="text/css" href="../../resources/css/iplanbiz/styles/jqx.simple-gray.css"/>
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
		<script type="text/javascript">
			<%-- 전역변수 --%> 
// 			var _acctid_ = $iv.userinfo.acctid();
// 			var _division_ = $iv.envinfo.division;
// 			var _userid_ = $iv.userinfo.userid();
			
			var _flagreportuser_  = true; // 보고서 > 사용자
			var _flagreportgroup_ = true; // 보고서 > 그룹
			var _flagreportrole_  = true; // 보고서 > 롤 
			
			// jqxgrid 렌더러
			var defulatrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
				var margin = '';
				var cellsalign = columnproperties.cellsalign;
				var newValue = $i.secure.scriptToText(value);
				if(cellsalign == 'center') {
					margin = 'margin:6px  0px 0px  0px;';
				} else if(cellsalign == 'left') {
					margin = 'margin:6px  0px 0px 10px;';
				} else if(cellsalign == 'right') {
					margin = 'margin:6px 10px 0px  0px;';
				}
				return '<div style="text-align:'+columnproperties.cellsalign+'; '+margin+'">'+newValue+'</div>';
			};
			// jqxgrid underline 렌더러
			var underlinerenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
				var margin = '';
				var cellsalign = columnproperties.cellsalign;
				var newValue = $i.secure.scriptToText(value);
				if(cellsalign == 'center') {
					margin = 'margin:6px  0px 0px  0px;';
				} else if(cellsalign == 'left') {
					margin = 'margin:6px  0px 0px 10px;';
				} else if(cellsalign == 'right') {
					margin = 'margin:6px 10px 0px  0px;';
				}
				return '<div style="text-align:'+columnproperties.cellsalign+'; '+margin+'; text-decoration: underline;">'+newValue+'</div>';
			};
			
			// jqxgrid 사용자 소스
			var usergridsource = {
				datatype: 'json',
				datafields: [
					{ name: 'EMP_ID', type: 'string'},
					{ name: 'EMP_NM', type: 'string'},
					{ name: 'USER_GRP_NM', type: 'string'},
					{ name: 'EMP_CHECK', type: 'string'}
				],
				localdata: {},
				updaterow: function (rowid, rowdata, commit) {
					commit(true);
				}
			};
			// jqxgrid 그룹 소스
			var groupgridsource = {
				datatype: 'json',
				datafields: [
					{ name: 'GID', type: 'string'},
					{ name: 'GROUP_NAME', type: 'string'},
					{ name: 'GROUP_CHECK', type: 'string'}
				],
				localdata: {},
				updaterow: function (rowid, rowdata, commit) {
					commit(true);
				}
			};
			// jqxgrid 롤 소스
			var rolegridsource = {
				datatype: 'json',
				datafields: [
					{ name: 'GID', type: 'string'},
					{ name: 'GROUP_NAME', type: 'string'},
					{ name: 'GID_CHECK', type: 'string'}
				],
				localdata: {},
				updaterow: function (rowid, rowdata, commit) {
					commit(true);
				}
			};

			$(document).ready(function() {
				var dat  = [
					{value:'1',text:'이름'},
					{value:'2',text:'사번'} 
				];
				var source = {
					datatype: "json",
					datafields: [
						{ name: 'value' },
						{ name: 'text' }
					],
					id: 'id',
					localdata:dat,
					async: false
				};
				var dataAdapter = new $.jqx.dataAdapter(source);
				$("#cboGubnTyp").jqxComboBox({selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "text", valueMember: "value", dropDownWidth: 60, dropDownHeight: 80, width: 60, height: 19, theme:'blueish'});
				 
				$('#jqxTabs').jqxTabs({ width: '100%', height: '100%', position: 'top', theme:'blueish', selectedItem: 0});
				
				$("#jqxButtonSave01").jqxButton({ width: '',  theme:'blueish'});
				
				
				var grpData = $i.post("./getGrpTypList", {});
				grpData.done(function(data){
					var cbosource = {
						datatype: "json",
						datafields: [
							{ name: 'GRP_TYP' },
							{ name: 'GRP_TYP_NM' }
						],
						id: 'GRP_TYP', 
						localdata:data.returnObject,
						async: false
					};
					var cbodataAdapter = new $.jqx.dataAdapter(cbosource);
					
					$("#cboGrpTyp").jqxComboBox({selectedIndex: 0, source: cbodataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "GRP_TYP_NM", valueMember: "GRP_TYP", dropDownWidth: 60, dropDownHeight: 80, width: 60, height: 19, theme:'blueish'});	
					makeTab1();
				});
			});
			function makeTab1(){
				makeTree(5999);
				makeTab1Grid('','');
			}
			function allcheckheaderRenderer(a,b,c,d,e,f){
				return '<div><div style="margin-left: 10px; margin-top: 5px;"></div><div id="selectall" ></div></div>';
			}
			
			<%-- 보고서 탭 --%>
			function makeTree(fid) {
				// 사용중인 메뉴만 조회
// 				$iv.progress.show("Information","화면이 생성 중 입니다.");
				var treeData = $i.post("./getPortletAuthorityList", {});
				treeData.done(function(res){
					$element = $('#treegridPortletList');
					drawTree(res.returnArray, $element, fid);
// 					$iv.progress.hide();
				});
			}
			var addfilter = function () {
				$("#treegridPortletList").jqxTreeGrid('expandAll');
				 var searchWord = $('#txtPortletSearch').val();
				 console.log(searchWord);
				 
			     var filtertype = 'stringfilter';
				  // create a new group of filters.
				 var filtergroup = new $.jqx.filter();
				 var filter_or_operator = 1;
				 var filtervalue = searchWord;
				 var filtercondition = 'contains';
				 var filter = filtergroup.createfilter(filtertype, filtervalue, filtercondition);
				 filtergroup.addfilter(filter_or_operator, filter);
				  // add the filters.
				 $("#treegridPortletList").jqxTreeGrid('addFilter', 'PL_NAME', filtergroup);
				  // apply the filters.
				 $("#treegridPortletList").jqxTreeGrid('applyFilters');
			     
			 }
			<%-- 보고서 탭 : 좌측 트리 --%>
			function drawTree(res, $element, fid) {
				$element.jqxTreeGrid('clear');
				var source = {
					datatype: "json",
					datafields: [
						{ name: 'PLID'}, 
						{ name: 'PL_NAME'}
					],
					icons:true,
					altrows:false,
					pageable:false,  
					localdata: res,
					hierarchy:{         
						keyDataField:{name:'PLID'},
						parentDataField:{name:'PID'} 
					},
					id:'PLID'
				};
				var dataAdapter = new $.jqx.dataAdapter(source);
				
				$element.jqxTreeGrid({source: dataAdapter, height: '100%', width: '100%',theme:"simple-gray",
					icons:function(rowKey, rowdata) { return getIconImage(rowdata.MENU_TYPE, rowdata.MENU_OPEN_TYPE); },
					columns:[ 
						{ dataField:'PLID',text:'PLID',align:'center', hidden:true},
						{ dataField:'PL_NAME',text:'PL_NAME', algin:'center', 
							renderer: function (text, align, height) {
								return "<div style=' margin-top:4px; text-align:center;'><img width='16' height='16' style='' src='../../resources/css/images/img_icons/icon_role.png'/><span style='margin-left: 4px; '>화면명</span><div style='clear: both;'></div></div>";
							},
							cellsrenderer:function(row,datafield,value,rowdata){
								var newValue = $i.secure.scriptToText(rowdata.PL_NAME);
								if(rowdata.records!=null){
									var cntlabel = "<span style='color: Blue;' data-role=='treefoldersize'> (" + rowdata.records.length + ")</span>";
									return "<a href=\"javascript:makeTab1Grid("+rowdata.PLID+" );\" style='color:#000000;text-decoration:underline;'>"+newValue+cntlabel+"</a>";
								} else {
									return "<a href=\"javascript:makeTab1Grid("+rowdata.PLID+" );\" style='color:#000000;text-decoration:underline;'>"+newValue+"</a>";
								}
							}
						}
					],
					ready:function(){
					}
				});
				<%-- PID 가 사라져, ROOT 에 표시되는 메뉴 제거 --%>
// 				deleteNullMenu($element);
				
				$element.on('rowExpand', function (event){
				    var args = event.args;
				    var row = args.row;
				    var key = row.PLID;
				    var icon;
				    icon = '../../resources/css/images/img_icons/permission/folderopenIcon.png';				    
				});
				$element.jqxTreeGrid({ width: '100%',  height: 594,  theme:'simple-gray' });
				$element.css('visibility', 'visible');
		    }
			<%-- 보고서 탭 --%>
			function makeTab1Grid(cId,menuOpenType){
				_flagreportuser_ = true;
				_flagreportgroup_ = true;
				_flagreportrole_ = true;
				$('#tab1sText01').val('');
				$('#tab1sText02').val('');
				$('#tab1sText03').val('');
				
				var $parentName = $('#parentName');
				var temphtml = $parentName.html();
				$parentName.html('');
				
				if(cId != '') {
						var $jqxTree01 = $('#treegridPortletList');
						var row = $jqxTree01.jqxTreeGrid('getRow',cId);
						var level = row.level;
						
						var html = '';
						for(var i=level; i>=0; i--) {
							var newValue = $i.secure.scriptToText(row.PL_NAME);
							if(i==level) {
								html = '><span style="font-weight:bold !important;">'+newValue+'</span>';	
							} else {
								html = '>' + newValue + html;
							}
							row = row.parent;
						}
						$parentName.html(html.substring(1, html.length));
				}					
				$("#saveCid").val(cId);
				makejqxGrid01(cId);
				makejqxGrid02(cId);
				makejqxGrid03(cId);
			}
			<%-- 보고서 탭 : 사용자 --%>
			var updatingCheckState01= false;
			function makejqxGrid01(cId) {
				if(cId == ""){
					cId = $("#saveCid").val();
				}
				var empId = "";
				var empNm = "";
				if($("#cboGubnTyp").val() == "1"){
					empNm = $("#tab1sText01").val();
				}else{
					empId = $("#tab1sText01").val();
				}
				
// 				if(_flagreportuser_){
					<%-- ENP_ID, EMP_NM : '' 고정, 데이터 조건은 jqxGrid 필터를 이용하여 쳐리 --%>
					var userData = $i.post("./getPortletPermissionUserList", {plID:cId, empName:empNm, empID:empId});
					userData.done(function(data){
						usergridsource.localdata = data.returnArray;
			            var dataAdapter = new $.jqx.dataAdapter(usergridsource);
			            
			            $("#gridPermissionUserList").jqxGrid(
			            {
			              	width: '100%',
			                height:'100%',
							altrows:true,
							pageable: false,
			                source: dataAdapter,
							theme:'blueish',
							sortable: true,
			                columnsresize: true,
							editable: true,
			                columns: [
							{ text: '', datafield: 'EMP_CHECK', width: '10%', align:'center', cellsalign: 'center', columntype: 'checkbox', 
								  renderer:allcheckheaderRenderer,sortable:false,
								  rendered:function(element){
										var checkbox = $(element).last();
										$(checkbox).jqxCheckBox({ width: 25, height: 25, animationShowDelay: 0, animationHideDelay: 0 });
										columnCheckBox = $(checkbox);
										var rows = $('#gridPermissionUserList').jqxGrid('getdisplayrows');
										 if(rows <= 0){
										 $(checkbox).jqxCheckBox({ locked: true });
										 }
										$(checkbox).on('change', function (event) {
											var checked = event.args.checked;
											var pageinfo = $("#gridPermissionUserList").jqxGrid('getpaginginformation');
											var pagenum = pageinfo.pagenum;
											var pagesize = pageinfo.pagesize;
											if (checked == null || updatingCheckState01) return;
											$("#gridPermissionUserList").jqxGrid('beginupdate');

											// select all rows when the column's checkbox is checked.			
											 if (checked) {				 
												 $("#gridPermissionUserList").jqxGrid('setcellvalue', i, 'EMP_CHECK');
												for (var i=0; i<$('#gridPermissionUserList').jqxGrid('getRows').length; i++) {
													$('#gridPermissionUserList').jqxGrid('selectrow', i);
												} 
											}
											else if (checked == false) {
											 $("#gridPermissionUserList").jqxGrid('setcellvalue', i, 'EMP_CHECK'); 
												for (var i=0; i<$('#gridPermissionUserList').jqxGrid('getRows').length; i++) {
													if ($('#gridPermissionUserList').jqxGrid('unselectrow', i)) {

													}
												}
											
											}

										// update cells values.
										var startrow = 0;
										for (var i = startrow; i < $('#gridPermissionUserList').jqxGrid('getRows').length; i++) {
											var boundindex = $("#gridPermissionUserList").jqxGrid('getrowboundindex', i);
											$("#gridPermissionUserList").jqxGrid('setcellvalue', boundindex, 'EMP_CHECK', event.args.checked);
										}

											$("#gridPermissionUserList").jqxGrid('endupdate');
										});
											return true;  
								  }
							  },
			                  { text: '사번', datafield: 'EMP_ID', width: '20%', align:'center', cellsalign: 'center', editable: false, cellsrenderer: defulatrenderer},
			                  { text: '이름', datafield: 'EMP_NM', width: '25%', align:'center', cellsalign: 'center', editable: false, cellsrenderer: defulatrenderer},
							  { text: '부서', datafield: 'USER_GRP_NM', width: '45%', align:'center', cellsalign: 'left',cellsrenderer: defulatrenderer, editable: false}
			                ]
			            });
			            $("#gridPermissionUserList").jqxGrid('clearselection');
// 						_flagreportuser_ = false; 
					});
// 				}
// 				setFilter($('#jqxgrid01'), {datafield:(($('#combobox01').val()=='1')?'EMP_NM':'EMP_ID'), filtervalue: $('#tab1sText01').val()});
			}
			<%-- 보고서 탭 : 그룹 --%>
			function makejqxGrid02(cId){
				if(cId == ""){
					cId = $("#saveCid").val();
				}
				$("#gridPermissionGroupList").jqxGrid('unselectrow');
				
				var groupData = $i.post("./getPortletPermissionGroupList", {plID:cId, grpTyp:$("#cboGrpTyp").val(), groupName:$("#tab1sText02").val()});
				groupData.done(function(data){
					var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'USER_GRP_ID', type: 'string'},
		                    { name: 'USER_GRP_NM', type: 'string'},
		                    { name: 'USER_GRP_CHECK', type: 'string'},
		                    { name: "GRP_TYP",type:"string"} 
		                ],
		                localdata: data,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridPermissionGroupList").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
						sortable: true,
		                columnsresize: true,
						editable: true,
		                columns: [
					   	  	{ text: '', datafield: 'USER_GRP_CHECK', width: '20%', align:'center', cellsalign: 'center', 
								columntype: 'checkbox',
								renderer:allcheckheaderRenderer,sortable:false,
							    rendered:function(element){
								  var checkbox = $(element).last();
									$(checkbox).jqxCheckBox({ width: 25, height: 25, animationShowDelay: 0, animationHideDelay: 0 });
									columnCheckBox = $(checkbox);
									var rows = $('#gridPermissionGroupList').jqxGrid('getdisplayrows');
									 if(rows <= 0){
									 $(checkbox).jqxCheckBox({ locked: true });
									 }
									$(checkbox).on('change', function (event) {
										var checked = event.args.checked;
										var pageinfo = $("#gridPermissionGroupList").jqxGrid('getpaginginformation');
										var pagenum = pageinfo.pagenum;
										var pagesize = pageinfo.pagesize;
										if (checked == null || updatingCheckState01) return;
										$("#gridPermissionGroupList").jqxGrid('beginupdate');

										// select all rows when the column's checkbox is checked.			
										 if (checked) {				 
											 $("#gridPermissionGroupList").jqxGrid('setcellvalue', i, 'USER_GRP_CHECK');
											for (var i=0; i<$('#gridPermissionGroupList').jqxGrid('getRows').length; i++) {
												$('#gridPermissionGroupList').jqxGrid('selectrow', i);
											} 
										}
										else if (checked == false) {
										 $("#gridPermissionGroupList").jqxGrid('setcellvalue', i, 'USER_GRP_CHECK'); 
											for (var i=0; i<$('#gridPermissionGroupList').jqxGrid('getRows').length; i++) {
												if ($('#gridPermissionGroupList').jqxGrid('unselectrow', i)) {

												}
											}
										
										}

									// update cells values.
									var startrow = 0;
									for (var i = startrow; i < $('#gridPermissionGroupList').jqxGrid('getRows').length; i++) {
										var boundindex = $("#gridPermissionGroupList").jqxGrid('getrowboundindex', i);
										$("#gridPermissionGroupList").jqxGrid('setcellvalue', boundindex, 'USER_GRP_CHECK', event.args.checked);
									}

										$("#gridPermissionGroupList").jqxGrid('endupdate');
									});
										return true;  
							    }
							},
                  			{ text: '그룹명', datafield: 'USER_GRP_NM', width: '80%', align:'center', cellsalign: 'center', editable: false , cellsrenderer: defulatrenderer},
                  			{ text: '종류', datafield: 'GRP_TYP', hidden:true}
		                ]
		            });
		            $("#gridPermissionGroupList").jqxGrid('unselectrow');
		            _flagreportgroup_ = false;
				});
			}
			<%-- 보고서 탭 : 롤 --%>
			function makejqxGrid03(cId){
				if(cId == ""){
					cId = $("#saveCid").val();
				}
				$("#gridPermissionRoleList").jqxGrid('unselectrow');
				if(_flagreportrole_) {
					var roleData = $i.post("./getPortletPermissionRoleList", {plID:cId});
					roleData.done(function(data){
						var source =
						{
			                datatype: "json",
			                datafields: [
			                    { name: 'GID', type: 'string'},
			                    { name: 'GROUP_NAME', type: 'string'},
			                    { name: 'GROUP_CHECK', type: 'string'}
			                ],
			                localdata: data,
			                updaterow: function (rowid, rowdata, commit) {
			                    commit(true);
			                }
			            };  
			            var dataAdapter = new $.jqx.dataAdapter(source);
			            
			            $("#gridPermissionRoleList").jqxGrid(
			            {
			              	width: '100%',
			                height:'100%',
							altrows:true,
							pageable: false,
			                source: dataAdapter,
							theme:'blueish',
							sortable: true,
			                columnsresize: true,
							editable: true,
			                columns: [
						   	  	{ text: '', datafield: 'GROUP_CHECK', width: '20%', align:'center', cellsalign: 'center', columntype: 'checkbox',
								  renderer:allcheckheaderRenderer,sortable:false,
								  rendered:function(element){
										var checkbox = $(element).last();
										$(checkbox).jqxCheckBox({ width: 25, height: 25, animationShowDelay: 0, animationHideDelay: 0 });
										columnCheckBox = $(checkbox);
										var rows = $('#gridPermissionRoleList').jqxGrid('getdisplayrows');
										 if(rows <= 0){
										 $(checkbox).jqxCheckBox({ locked: true });
										 }
										$(checkbox).on('change', function (event) {
											var checked = event.args.checked;
											var pageinfo = $("#gridPermissionRoleList").jqxGrid('getpaginginformation');
											var pagenum = pageinfo.pagenum;
											var pagesize = pageinfo.pagesize;
											if (checked == null || updatingCheckState01) return;
											$("#gridPermissionRoleList").jqxGrid('beginupdate');

											// select all rows when the column's checkbox is checked.			
											 if (checked) {				 
												 $("#gridPermissionRoleList").jqxGrid('setcellvalue', i, 'GROUP_CHECK');
												for (var i=0; i<$('#gridPermissionRoleList').jqxGrid('getRows').length; i++) {
													$('#gridPermissionRoleList').jqxGrid('selectrow', i);
												} 
											}
											else if (checked == false) {
											 $("#gridPermissionRoleList").jqxGrid('setcellvalue', i, 'GROUP_CHECK'); 
												for (var i=0; i<$('#gridPermissionRoleList').jqxGrid('getRows').length; i++) {
													if ($('#gridPermissionRoleList').jqxGrid('unselectrow', i)) {

													}
												}
											
											}

										// update cells values.
										var startrow = 0;
										for (var i = startrow; i < $('#gridPermissionRoleList').jqxGrid('getRows').length; i++) {
											var boundindex = $("#gridPermissionRoleList").jqxGrid('getrowboundindex', i);
											$("#gridPermissionRoleList").jqxGrid('setcellvalue', boundindex, 'GROUP_CHECK', event.args.checked);
										}

											$("#gridPermissionRoleList").jqxGrid('endupdate');
										});
											return true;  
								  }
								
								},
	                  			{ text: 'Role명', datafield: 'GROUP_NAME', width: '80%', align:'center', cellsalign: 'center', editable: false , cellsrenderer: defulatrenderer}
			                ]
			            });
			            $("#gridPermissionRoleList").jqxGrid('clearselection');
			            _flagreportrole_ = false;
					});
				}
				setFilter($('#gridPermissionRoleList'), {datafield:'GROUP_NAME', filtervalue: $('#tab1sText03').val()});
			}
			  
			function treeCheckBox(cId, menuTypeOpen, id){
				console.log('id='+id);
				if(menuTypeOpen != 'common') {
					alert("전체공개 메뉴 입니다.");
				} else {
					if($("#"+id).jqxTreeGrid("getRow", cId).checked == true){
						$("#"+id).jqxTreeGrid('uncheckRow', cId);
					}else{
						$("#"+id).jqxTreeGrid('checkRow', cId);
					}
				}
			}
			
			<%-- 필터 --%>
			function setFilter(gridelement, prop) {
				gridelement.jqxGrid('clearfilters');
				
				var filtervalue = prop.filtervalue;
				
				var datafield = prop.datafield;
				var filtergroup = new $.jqx.filter();
				var filter = filtergroup.createfilter('stringfilter', filtervalue, 'CONTAINS');
				filtergroup.addfilter(1, filter);
				gridelement.jqxGrid('addfilter', datafield, filtergroup);
				gridelement.jqxGrid('applyfilters');
			}
			function setFilters(gridelement, props) {
				gridelement.jqxGrid('clearfilters');
				for(var i = 0; i < props.length;i++){
					var prop = props[i];
					var filtervalue = prop.filtervalue;
					
					var datafield = prop.datafield;
					var filtergroup = new $.jqx.filter();
					var filter = filtergroup.createfilter('stringfilter', filtervalue, 'CONTAINS');
					filtergroup.addfilter(i+1, filter);
					gridelement.jqxGrid('addfilter', datafield, filtergroup);
					
				}
				gridelement.jqxGrid('applyfilters'); 
			}
			<%-- 트리에 아이콘 --%>
			function getIconImage(MENU_TYPE, MENU_OPEN_TYPE) {
				var icon = '../../resources/css/images/img_icons/permission/pageIcon.png';
				return icon;
			}
			
			<%-- tree 에서 PID 가 사라져, ROOT 에 표시되는 메뉴 제거 --%>
			function deleteNullMenu(treegrid) {
				var rows = treegrid.jqxTreeGrid('getRows');
				var length = rows.length;
				for(var i=length-1; i>=0; i--) {
					var row = rows[i];
					if(row.level == 0 && row.PID != '5999') {
						var key = treegrid.jqxTreeGrid('getKey', row);
						treegrid.jqxTreeGrid('deleteRow', key);
					}
				}
			}
			
			<%-- 보고서 탭 저장 --%>	
			function saveStartTab1(){
				if($("#saveCid").val() == '') {
					alert("권한을 등록할 메뉴를 선택해주세요");
					return false;
				} else {
					setFilter($('#gridPermissionUserList'), {datafield:(($('#cboGubnTyp').val()=='1')?'EMP_NM':'EMP_ID'), filtervalue: ''});
					setFilter($('#gridPermissionGroupList'), {datafield:'USER_GRP_NM', filtervalue: ''});
					setFilter($('#gridPermissionRoleList'), {datafield:'GROUP_NAME', filtervalue: ''});
					
					var jqxGrid01 = $("#gridPermissionUserList").jqxGrid("getrows");
					var jqxGrid02 = $("#gridPermissionGroupList").jqxGrid("getrows");
					var jqxGrid03 = $("#gridPermissionRoleList").jqxGrid("getrows");
					
					var jqxGrid01Check = "";
					var jqxGrid02Check = "";
					var jqxGrid03Check = "";
					var html = "";
					for(var i=0;i<jqxGrid01.length;i++){
						if(jqxGrid01[i].EMP_CHECK != undefined && jqxGrid01[i].EMP_CHECK != false){
							html += '<input type="hidden" name="arrayUserData" value="'+jqxGrid01[i].EMP_ID+'" />';
						}
					}
					for(var j=0;j<jqxGrid02.length;j++){
						if(jqxGrid02[j].USER_GRP_CHECK != undefined && jqxGrid02[j].USER_GRP_CHECK != false){
							html += '<input type="hidden" name="arrayGroupData" value="'+jqxGrid02[j].USER_GRP_ID+'" />';
// 							jqxGrid02Check+= jqxGrid02[j].USER_GRP_ID + ",";
						}
					}
					for(var k=0;k<jqxGrid03.length;k++){
						if(jqxGrid03[k].GROUP_CHECK != undefined && jqxGrid03[k].GROUP_CHECK != false){
							html += '<input type="hidden" name="arrayRoleData" value="'+jqxGrid03[k].GID+'" />';
// 							jqxGrid03Check+= jqxGrid03[k].GID + ",";
						}
					}
					 $("#saveForm").append(html);
					var saveCid = $("#saveCid").val();   
					$("#plID").val(saveCid);
					
					$i.insert("#saveForm").done(function(args){
						if(args.returnCode == "EXCEPTION"){
							$i.dialog.error("SYSTEM", args.returnMessage);
						}else{ 
							$i.dialog.alert("SYSTEM", args.returnMessage,function(){
								makeTab1Grid(saveCid, 'common');
							});
						}
					});
// 					groupManagerDwr.dwrSetPageletPermission(saveCid, saveJqxGrid01, saveJqxGrid02, saveJqxGrid03, function(res){
// 						makeTab1Grid(saveCid,'common');
			            
// 						alert("권한이 저장되었습니다.");
// 					});
				}
			}
		   
    </script>
    <style>
    .jqx-tree-item-li-blueish-system{ 
    	margin-top:0px;
    }
    .jqx-tree-item-blueish-system img{
		width:inherit;
		height:inherit;
	}
	.treegrid .jqx-cell-empty-style{
		padding:3px 4px !important;
	}
	.jqx-tree-item-blueish-system {
	    font-size: 11px;
	    padding: 2px !important;
	    vertical-align: middle;
	}
    .jqx-grid-header-empty-style {
    	display : none !important;
    }
	.jqx-icon-arrow-down-blueish-system, .jqx-icon-arrow-down-blueish-system {
		background: url(../../resources/css/images/img_icons/simple-blue-downarrow.png) center no-repeat !important;
	}
	.jqx-icon-arrow-up-blueish-system, .jqx-icon-arrow-up-blueish-system {
		background: url(../../resources/css/images/img_icons/simple-blue-uparrow.png) center no-repeat !important;
	}
	/*추가*/
	.i-DQ .label2-bg {
		background:url(../../resources/css/images/img_icons/icon_sqr_arrow_blue.png) left 7px no-repeat !important;
		font-size: 12px !important;
		padding-left: 15px;
		padding-top: 0px;
		line-height: 24px !important;
		color: #323232!important;
		margin-right: 2px;
		font-weight: bold;
	}
	.i-DQ .label3-bg {
		background:none !important;
		font-size: 12px !important;
		padding-left: 5px;
		padding-top: 0px;
		line-height: 24px !important;
		color: #3972b4!important;
		margin-right: 0px;
		font-weight: bold;
	}
	.i-DQ .label4-bg {
		background:none !important;
		font-size: 12px !important;
		padding-left: 5px;
		padding-top: 0px;
		line-height: 24px !important;
		color: #000000 !important;
		margin-right: 0px;
		font-weight: bold;
	}
	/*content > input */
	.code_box_input {
		height:19px;
		float:left;
		margin-right:0px;
		margin-top:0px;
		margin-left:0px;
	}
	.code_box_input input.input_stA {
		width: 100%;
		background: #ffffff;
		border: 1px solid #aaa;
		border-radius: 3px;
		-moz-border-radius: 3px;
		-ms-border-radius: 3px;
		-o-border-radius: 3px;
		-webkit-border-radius: 3px;
		text-indent: 5px;
		font-size: 12px;
		color: #303030;
		padding-bottom: 2px;
	}
	/*content > combobox*/
	.con_combo {
		border-color:#aaa !important;
	}
	.con_combo .jqx-combobox-state-normal {
		border:1px solid #4271ac !important;
		border-radius:3px;
		-ms-border-radius:3px;
		-o-border-radius:3px;
		-webkit-order-radius:3px;
		-moz-border-radius:3px;
		border-color:#43536A !important;
	}
	.con_combo .jqx-combobox-arrow-normal, .con_combo .jqx-action-button {
		background:#ffffff;
		border-color:#ffffff !important;
	}
	.con_combo .jqx-combobox-content {
		background:#ffffff;
		border-color:#ffffff !important;
	}
	.con_combo .jqx-combobox-input {
		color: #171819;
		font-size:12px;
		text-shadow: none;
		background-color:#ffffff;
		background-image:none;
		text-indent:5px;
		vertical-align:middle !important;
		margin-top:0px !important;
	}
	.con_combo .jqx-icon-arrow-down {
		background:url(../../resources/css/images/img_icons/simple-arrow-down-icon.png) no-repeat center !important;
	}
	/*tree01*/
	#treegridPortletList .jqx-widget-content-blueish-system {
		background:#eee;
	}
	#treegridPortletList .jqx-widget-header-blueish { 
	    		display : none !important; 
 	}  
	/*tree*/
	.jqx-widget-content-blueish-system {
		background:#f6f9fe;
	}
	/*groupbox*/
	.group_box_label .group_box_title {
		float:left;
		text-align:center;
		width:25%;
		margin-right:0%;
		font-size:12px;
		color:#303030;
		margin:10px 0;
		background:url(../../resources/css/images/img_icons/label-icon.png) left no-repeat;
	}
	.group_box_label .group_box_input {
		float:left;
		width:70%;
		margin:8px 0;
	}
	.group_box_label .group_box_input input.input_stA {
		width:100%;
		background:#f9f9f9;
		border:1px solid #9DA1A4;
		border-radius:3px;
		-moz-border-radius:3px;
		-ms-border-radius:3px;
		-o-border-radius:3px;
		-webkit-border-radius:3px;
		text-indent:5px;
		font-size:12px;
		color:#202020;
	}
	.jqx-grid-content .jqx-grid-content-empty-style .jqx-widget-content .jqx-widget-content-empty-style{
		height:100%;
	}
	.jqx-tree-grid-checkbox-empty-style {
	   	 float: none !important;
	   	 width: 13px; /*수정(기존 14px)*/
	   	 height: 13px; /*수정(기존 14px)*/
	     cursor: pointer;
	     margin-right: 2px !important;
	     margin-left: 2px !important;
	}
	.treegrid .jqx-cell-empty-style {
	   	padding: 4px 4px !important;
	}
	.jqx-tabs-blueish{
		border-bottom:none !important;
	}
	</style>
	</head>
	<body class='default i-DQ blueish'>
		<div class="wrap" style="width:98% !important; margin:0 10px !important; min-width:1656px;">
			<div class="contents" style="width:100%; margin:10px 0 !important;">
				<input type="hidden" id="saveCid" name="saveCid" />
				<input type="hidden" id="savePid" name="savePid" />
				<input type="hidden" id="saveGid" name="saveGid" />
				<input type="hidden" id="saveEmpId" name="saveEmpId" />
				<!--전체공개메뉴는 권한부여...정보 추가-->
				<div class="iwidget_label" style=" float:right; padding:5px 0; position:absolute; right:0px; margin:0;">
					<div class="label-bg label-1" style="float:left;  ">권한이 없는 사용자는 [Main 화면관리]에서 설정 된 기본 페이지가 열립니다.</div>
				</div>
				
				<div class="tab_Area" style="width:100%; height:100%; float:left; margin-top:0px;">
					<div id='jqxWidget'>
						<div id='jqxTabs'>
							<ul>
								<li onclick="makeTab4();" style="display:none;">사용자</li> 
							</ul>
							<!--보고서 TAB-->
							<div style="height:650px !important;background:#fff;border-top:1px solid #91A3B4; position:relative;  overflow:hidden; ">
								<div class="tab_con"  style="width:100%; height:100%;  margin:0px 0%; background:#fff; ">
									<!--보고서 TAB Tree 영역 -->
									<div class="divi" style="float:left; width:300px; background:#eee;  margin:0; padding:0 10px;height:660px;"> 
										<div class="iwidget_label" style="margin:10px 0 0 0 !important; width:100%;  float:left;">
										<div class="label4-bg" style='float:left; margin:0 5px; margin-left:0px;' >화면</div>
										<div class="code_box_input" style="float:left;">
											<input type="text" id="txtPortletSearch" class="input_stA"  style="width:140px;margin-top:-1px !important;" onkeypress="if(event.keyCode==13) {addfilter(); return false;}"/>
											<a href="javascript:addfilter();"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;"/></a>
										</div>
										</div>
										<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:594px;">
											<div style="width:100%;height:594px !important; overflow:auto;" id='treegridPortletList' class='treegrid'></div>
										</div>
							<!--  		<div style=" border:0;padding-top:0px;background:#fff; width:100%; margin-left:0%; height:660px !important; overflow:auto;" id='treegridPortletList' class='treegrid'></div> -->
              						</div>
              						<!-- 보고서 TAB 우측 그리드 영역-->
									<div class="divi" style="float:left;width:80%; padding:0%; margin:0px 0%; background:#fff;">
										<div class="iwidget_label" style="margin:10px 0 0 0; width:100%; padding:5px 0; float:left; border-bottom:1px solid #ccc;">
											<div class="label-bg label-2" style="float:left; margin-left:2%;">화면명:<span class="colorchange" id="parentName" style="font-weight:normal !important;"></span></div>
											<div class="button-bottom" style="float:right;margin-right:2%;">
												<div class="button type2 f_left">
													<input type="button" value="저장" id='jqxButtonSave01' width="100%" height="100%" style="margin:0 !important;" onclick="saveStartTab1();" />
												</div>
											</div>
										</div>
										<div style="width:96%; float:right; margin:10px 2%;">
											<!-- 보고서 TAB 우측 사용자 그리드 영역-->
											<div class="divi" style="float:left;width:40%; background:#fff; padding:0px 0%;">
												<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
													<div class="label2-bg" style="float:left; width:100%;">권한이 부여된 사용자</div>
													<div class="combobox f_left" style='float:left;margin:2px 10px 0px 5px;' id='cboGubnTyp' ></div>
													<div class="code_box_input" style="float:left;">
														<input type="text" id="tab1sText01" class="input_stA"  style="width:150px;" onkeypress="if(event.keyCode==13) {makejqxGrid01(''); return false;}"/>
														<a href="javascript:makejqxGrid01('');"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin:0; padding:0;margin-top:3px !important;" /></a> 
													</div>
												</div>
												<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:525px;">
													<div id="gridPermissionUserList"></div>
												</div>
											</div>
											<!-- 보고서 TAB 우측 그룹 그리드 영역-->
											<div class="divi" style="float:left;width:28%; margin:0 2%; background:#fff; padding:0px 0%;">
												<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
													<div class="label2-bg" style="float:left; width:100%;">권한이 부여된 그룹</div>
													<div class="label3-bg" style='float:left; margin:0 10px; margin-left:10px;' ><img src="../../resources/css/images/img_icons/icon_group.png" alt="icon_group" height="20" style="margin-right:3px;">그룹</div>
													<div class="code_box_input" style="float:left;">
												 
														<div class="combobox f_left" style='float:left;margin:2px 10px 0px 5px;' id='cboGrpTyp' ></div>
														<input type="text" id="tab1sText02" class="input_stA"  style="width:135px;" onkeypress="if(event.keyCode==13) {makejqxGrid02(''); return false;}"/>
														<a href="javascript:makejqxGrid02('');"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin-top:3px !important;margin:0; padding:0;"/></a> 
													</div>
												</div>
												<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:525px;">
													<div id="gridPermissionGroupList"></div>
												</div>
											</div>
											<!-- 보고서 TAB 우측 Role 그리드 영역-->
											<div class="divi" style="float:right;width:28%;  background:#fff; padding:0px 0%;">
												<div class="iwidget_label" style="margin:0 !important; width:100%; height:45px; float:left;">
													<div class="label2-bg" style="float:left; width:100%;">권한이 부여된 Role</div>
													<div class="label3-bg" style='float:left; margin:0 10px; margin-left:10px;' ><img src="../../resources/css/images/img_icons/icon_role.png" alt="icon_role" height="20" style="margin-right:3px;">Role</div>
													<div class="code_box_input" style="float:left;">
														<input type="text" id="tab1sText03" class="input_stA"  style="width:135px;" onkeypress="if(event.keyCode==13) {makejqxGrid03(''); return false;}"/>
														<a href="javascript:makejqxGrid03('');"><img src="../../resources/css/images/img_icons/search_btn_gray.png" alt="search" title="검색" style="margin-top:3px !important; margin:0; padding:0;"/></a>
													</div>
												</div>
												<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:525px;">
													<div id="gridPermissionRoleList"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<form id="saveForm" name="saveForm" action="./insert">
					<input type="hidden" id="plID" name="plID" />
				</form>
			</div>
		</div>
	</body>
</html>