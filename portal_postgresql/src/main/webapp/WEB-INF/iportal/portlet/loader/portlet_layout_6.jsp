<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>jQuery UI Sortable - Portlets</title>
	<link rel="stylesheet" href="../../resource/css/jquery.ui.all_v2.css">
	<link rel="stylesheet" href="../../resource/css/common.css">
	<link rel="stylesheet" href="../../resources/css/portlet_theme/portlet_theme_1.css?a=${systemConfig.portlet_theme}">

	<script>
		var default_portlet_content_background_color = '${systemConfig.default_portlet_content_background_color}';
		var default_portlet_content_font_color = '${systemConfig.default_portlet_content_font_color}';
		var default_portlet_top_underline = '${systemConfig.default_portlet_top_underline}';
		var default_portlet_content_underline = '${systemConfig.default_portlet_content_underline}';
		var default_portlet_top_font_color = '${systemConfig.default_portlet_top_font_color}';
		var default_portlet_top_background_color = '${systemConfig.default_portlet_top_background_color}';
		var default_portlet_theme = "${systemConfig.portlet_theme}";
		var plid="${plid}";
		var pageType = '${portletUserType}';
		var uriContext = "${WEB.ROOT}";
	</script>
	
	<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>   
	<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	<script src="../../resources/cmresource/js/jquery/jquery-1.8.3.min.js"></script>
	<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	<script src="../../resources/js/portlet.js"></script>
	<script src="../../resources/js/izzyColor/izzyColor.js"></script>
	<style>
	body{
		margin-top:10px;
	}
	.column {}
	.layout_01_column1 { width: 100%; float: left;min-width:400px;white-space:nowrap; min-height:100px}
	.layout_01_column2 { width: 65%; float: left; padding-bottom: 50px;min-width:350px;white-space:nowrap; }
	.layout_01_column3 { width: 34%; float: left; padding-bottom: 50px;min-width:150px;white-space:nowrap; }
	
	.portlet { margin: 0 1em 1em 0; }
		
/* 테스트 작업 분량 */
	.portlet-setting {margin: 0.2em; padding-bottom: 4px; padding-left: 0.2em; font-size:12px;}
	.portlet-setting input{}
/* 테스트 작업 완료 */
	
	/* 포틀릿 영역 : 포틀릿 이동시에 보이는 점선 영역 */
	.ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
	.ui-sortable-placeholder * { visibility: hidden; }
	
	</style>
</head>
<body onload="loadPannel('${portletUserType}', '${plid}')" style="width:1035px;">
<div>
	<div class="column layout_01_column1" id="layout_01_column1"></div>
	<div class="column layout_01_column2" id="layout_01_column2"></div>
	<div class="column layout_01_column3" id="layout_01_column3"></div>
</div>
<input type="hidden" id="aaaa" value="layout6"></input>


</body>
</html>