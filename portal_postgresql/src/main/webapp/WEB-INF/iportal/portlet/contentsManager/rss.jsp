<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="crt" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<html>
	<head> 
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>RSS</title>
		<link rel="StyleSheet" href="../../resources/css/portlet_new.css" type="text/css" />
		<style>
			body{
				margin-top:10px;
			}
			a:link		{color:${systemConfig.default_portlet_content_font_color};}
			a:visited	{color:${systemConfig.default_portlet_content_font_color};}
			a:hover		{color:${systemConfig.default_portlet_content_font_color};}
			a:active	{color:${systemConfig.default_portlet_content_font_color};}
		</style>
		
		<script src="../../resources/js/jquery/jquery-1.7.2.js"></script>
		<script>
		$().ready(function() {
			var rssUrl='${rssUrl}';
			var content = '${content}';
			
			$.ajax({
				type:'post',
				dataType:'xml',
				url:'../../portlet/contentsManager/rss/read?url='+rssUrl,
				contentType: "text/xml; charset=UTF-8", 
				async:false,
				success:function(xml){
					// 블로그 제목 설정
					var title = xml.getElementsByTagName("title")[0].childNodes[0];
					var items = xml.getElementsByTagName("item");
					// 최신 글 목록 설정
					var contents = $('.form01 .form table td p');
				    for (var i=0; i<6; i++) {
					    var entry = items[i];
					    var title = $('<p/>').text(entry.getElementsByTagName("title")[0].childNodes[0].nodeValue);
					    var link = ($('<a/>').attr('href', entry.getElementsByTagName("link")[0].childNodes[0].nodeValue).attr("target", "_blank")).html(title);
					    var description = ""; // $('<td/>').html(entry.contentSnippet);
					    //var date = entry.publishedDate;
					    var listElement = $('<tr>').html($('<td/>').html(link)).append(description);
					    contents.append(listElement);
				    }
				}
			});
			/*
			var api = 'http://ajax.googleapis.com/ajax/services/feed/load?v=1.0';
			api = api + '&q=' + '${rssUrl}';
			api = api + '&num=' + 6;

			$.getJSON(api, function(resp) {
				if (resp.responseStatus == 200) {
					var feeds = resp.responseData.feed;
					// 블로그 제목 설정
					$('#rssTitle').text(feeds.title);

					// 최신 글 목록 설정
					var contents = $('.form01 .form table td p');
				     for (var i=0; i<feeds.entries.length; i++) {
				      var entry = feeds.entries[i];
				      var title = $('<p/>').text(entry.title);
				      var link = ($('<a/>').attr('href', entry.link).attr("target", "_blank")).html(title);
				      var description = ""; // $('<td/>').html(entry.contentSnippet);
				      //var date = entry.publishedDate;
				      var listElement = $('<tr>').html($('<td/>').html(link)).append(description);
				      contents.append(listElement);
				     }
				}
			});*/
		});
		</script>
		<script language="javascript" type="text/javascript">
		<!--
		function  goForm(){
			location.replace("${WEB.ROOT}/board/form?type=I");
		}
		function goList(){
			parent.parent.addTab("boardList", "게시판", "${WEB.ROOT}/board/list", 0,0);
		}
		function goGet(boardNo){
			parent.parent.addTab("board"+boardNo, "게시판", "${WEB.ROOT}/board/get?boardNo="+boardNo, 0,0);
			
			//addTab(cId, tmp_title, tmp_content, width_size, height_size);
			/*
			*	cId : 고유번호
			* tmp_title : Tab 이름
			* tmp_content : Tab 주소
			* size : size
			*/
			
		}
		// -->
		</script>
	</head>
	<body style="overflow:hidden;">
		<div class="form01" id="BOARD">
			<div class="form">
				<div class="table">
				<table style="width:100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<p style="margin-left:5px;"></p>
					</td>
				</tr>		
			</table>
		</div>
	</body>
</html> 