<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>컨텐츠 관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
<!--         <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script> -->
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	var paramPortletGrp = "${param.paramPortletGrp}";
        	$(document).ready(function(){
        		if("${param.poID}" != ""){
        			portlet("${param.poID}");
        		}
        		initAjaxForm();
        		init();
        		var pElement = $('.iwidget_label_contents').parent();
        		pElement.css('background-color','#ecf2f8');
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		
				$("#txtName").jqxInput({placeHolder: "", height: 25, width: 175, minLength: 1, theme:'blueish'});
				$("#txtHeight").jqxInput({placeHolder: "", height: 25, width: 50, minLength: 1, theme:'blueish'});
				$("#txtUrl").jqxInput({placeHolder: "", height: 25, width: 175, minLength: 1, theme:'blueish'});
				$("#txtMoreUrl").jqxInput({placeHolder: "", height: 25, width: 175, minLength: 1, theme:'blueish'});
        		$("#btnList").jqxButton({width:'', theme:'blueish'});
        		$("#btnSave").jqxButton({width:'', theme:'blueish'});
        		$("#btnDelete").jqxButton({width:'', theme:'blueish'});
        		$("#splitter").jqxSplitter({ width:'', height: 1000, panels: [{ min: 180 , size: 200 }, { min: 180}], theme:'blueish' });
        		$('#jqxContents').jqxTree({  height: '100%', width: '100%' , theme:'blueish'});
        		$("#radioButtonGroup").jqxButtonGroup({ mode: 'radio'});
        		if('${content.VIEW_HEADER}'=="true"){
	            	$("#radioButtonGroup").jqxButtonGroup("setSelection", 0);
	            }else if('${content.VIEW_HEADER}'=="false"){
	            	$("#radioButtonGroup").jqxButtonGroup("setSelection", 1);
	            }else{
	            	$("#radioButtonGroup").jqxButtonGroup("setSelection", 0);
	            }
        		$("#iconButtonGroup").jqxButtonGroup({ mode: 'radio'});
        		if('${content.VIEW_ICON}'=="true"){
					$("#iconButtonGroup").jqxButtonGroup("setSelection", 0);
				}else if('${content.VIEW_ICON}'=="false"){
					$("#iconButtonGroup").jqxButtonGroup("setSelection", 1);
				}else{
					$("#iconButtonGroup").jqxButtonGroup("setSelection", 0);
				}
        		$("#moreButtonGroup").jqxButtonGroup({ mode: 'radio'});
        		if('${content.MORE}'=="true"){
					$("#moreButtonGroup").jqxButtonGroup("setSelection", 0);
				}else if('${content.MORE}'=="false"){
					$("#moreButtonGroup").jqxButtonGroup("setSelection", 1);
				}else{
					$("#moreButtonGroup").jqxButtonGroup("setSelection", 0);
				}
        		$("#activeButtonGroup").jqxButtonGroup({ mode: 'radio'});
        		if('${content.ACTIVE}'=="사용"){
					$("#activeButtonGroup").jqxButtonGroup("setSelection", 0);
					$("#hiddenActive").val('1');
				}else if('${content.ACTIVE}'=="정지"){
					$("#activeButtonGroup").jqxButtonGroup("setSelection", 1);
					$("#hiddenActive").val('0');
				}else{
					$("#activeButtonGroup").jqxButtonGroup("setSelection", 0);
					$("#hiddenActive").val('1');
				}
        		$("#menuButtonGroup").jqxButtonGroup({ mode : 'radio'});
        		if('${content.MENU_OPEN_TYPE}'=="all"){
					$("#menuButtonGroup").jqxButtonGroup("setSelection", 0);
					$("#hiddenMenuOpenType").val("all");
				}else if('${content.MENU_OPEN_TYPE}'=="common"){
					$("#menuButtonGroup").jqxButtonGroup("setSelection", 1);
					$("#hiddenMenuOpenType").val("common");
				}
        		
        	}
        	//조회 
        	function search(){
        		var portletData = $i.post("./getPortletContentsList",{ portletGrp:"1"});
        		portletData.done(function(data){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'POID', type: 'string'},
							{ name: 'NAME', type: 'string'},
							{ name: 'HEIGHT', type: 'string'},
							{ name: 'AUTH', type: 'string'},
							{ name: 'TYPE', type: 'string'},
							{ name: 'PORTLET_GRP', type: 'string'},
							{ name: 'ACTIVE', type: 'string'}
		                ],
		                localdata: data,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var nameCellSrenderer = function (row, datafield, value, defaultHtml, propery, rowdata){
						var poID = rowdata.POID;
						var link = "<a href=\"javaScript:detailPage('"+poID+"');\" style='color: #000000;'>" + value + "</a>";
						return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px;'>" + link + "</div>"; 
					};
					var detailCellSrenderer = function (row, datafield, value, defaultHtml, propery, rowdata){
						var poID = rowdata.POID;
						var link = "<a href=\"javaScript:openWindow('"+poID+"');\" style='color: #000000;'>"+ '권한' + "</a>";
						return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px;'>" + link + "</div>";
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridContentsList").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                pageable: true,
		                pagesize: 100,
						pagesizeoptions:['100', '200', '300'],
		                columns: [
		                  	{ text: 'No', datafield: 'POID', width: '10%', align:'center', cellsalign: 'center'},
							{ text: '이름', datafield: 'NAME', width: '50%', align:'center', cellsalign: 'left', cellsrenderer :nameCellSrenderer},
							{ text: '권한', value: '권한', width: '10%', align:'center', cellsalign: 'center', cellsrenderer :detailCellSrenderer},
							{ text: '높이', datafield: 'HEIGHT', width: '10%', align:'center', cellsalign: 'center'},
							{ text: '형태', datafield: 'TYPE', width: '10%', align:'center', cellsalign: 'center'},
							{ text: '상태', datafield: 'ACTIVE', width: '10%', align:'center', cellsalign: 'center'}
		                ]
		            });
        		});
        	}
        	//입력
        	function insert(){
        		if($("#txtName").val() == ""||$("#txtName").val().trim()==""){
        			$i.dialog.error("SYSTEM", "이름을 입력하세요");
        			$("#txtName").val("");
        			return false;
        		}
        		if($("#txtHeight").val() == ""||$("#txtHeight").val().trim()==""){
        			$i.dialog.error("SYSTEM", "높이를 입력하세요");
        			$("#txtHeight").val("");
        			return false;
        		}
        		var typeValue = $("[name='type']:checked").val();
        		var portletGrpValue = $("[name='portletGrp']:checked").val();
        		if($("[name='type']:checked").length == 0){
        			$i.dialog.error("SYSTEM", "형태를 선택하세요");
        			return false;
        		}
        		if($("[name='portletGrp']:checked").length == 0){
        			$i.dialog.error("SYSTEM", "그룹분류를 선택하세요");
        			return false;
        		}
        		if(isNaN($("#txtHeight").val())){
        			$i.dialog.error("SYSTEM", "높이는 숫자로 입력하세요");
        			return false;
        		}
        		if(isNaN($("#txtHeight").val())){
        			$i.dialog.error("SYSTEM", "높이는 숫자로 입력하세요");
        			return false;
        		}
        		if(eval($("#txtHeight").val())>100000){
        			$i.dialog.error("SYSTEM", "높이를 확인하세요.");
        			$("#txtHeight").val("0");
        			return false;
        		}
        		if($("[name='portletImage']")[0].value == ""){
        			$("[name='portletImage']").remove();	
        		}
        		$("#hiddenTypeKeyValue").val(typeValue);
        		$("#hiddenPortletGrpKeyValue").val(portletGrpValue);
        		$("#saveForm").attr("action","./insert");
        		$("#saveForm").submit(); 
//         		goList();
//         		$i.post("./insert","#saveForm").done(function(data){
//         			if(data.returnCode == "EXCEPTION"){
//         				$i.dialog.error("SYSTEM", data.returnMessage);
//         			}else{
//         				$i.dialog.alert("SYSTEM", data.returnMessage, function(){
// //         					goList();
//         				});
//         			}
//         		});
        	}
        	function initAjaxForm(){
        		$("#saveForm").ajaxForm({
                    beforeSubmit: function (data,form,option) {
                    	if(form.attr("action").indexOf("insert")!=-1)
//         			 		$i.dialog.progress("SYSTEM","FILEUPLOAD","파일을 업로드 하였습니다.");  
                        return true;
                    },
                    success: function(response,status){
                    	var result = null;
                    	try{
                    		result = JSON.parse(response);
                    	}catch(e){
                    		$i.dialog.error("SYSTEM","오류가 발생 하였습니다.");	
                    		return; 
                    	}
                    	var alert = null;  
            			if(result.returnCode!="SUCCESS") {
            				$i.dialog.error("SYSTEM", result.returnMessage);
            			}
            			else {
            				$i.dialog.alert("SYSTEM", result.returnMessage, function(){
            					goList();
            				});
            			}  
            			//삭제 후 alert
            			if($("#saveForm").attr("action").indexOf("remove")!=-1 )
            				alert("SYSTEM", result.returnMessage);
                    },
                    error: function(){
        				
                    }                               
                });
        	}
        	//삭제
        	function remove(){
        		$i.dialog.confirm("SYSTEM", "삭제 하시겠습니까?", function(){
        			$i.post("./remove","#saveForm").done(function(data){
            			if(data.returnCode == "EXCEPTION"){
            				$i.dialog.error("SYSTEM", data.returnMessage);
            			}else{
            				$i.dialog.alert("SYSTEM", data.returnMessage, function(){
            					goList();
            				});
            			}
            		});
        		});
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	function goList(){
        		location.replace("./list?paramPortletGrp="+paramPortletGrp);
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function portlet(poID){
        		var contetns = "";
        		var data = $i.post("./getPortletContentsInfoData", { poID : poID});
        		data.done(function(res){
        			if(res.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM", res.returnMessage);
        			}else{
        				if(res.returnArray.length > 0){ 
        					if(res.returnArray[0].TYPE == 4){
        						content = "<iframe id='iframe_content' src='./imgLoader?poID="+poID+"' width='100%' style='overflow:hidden' frameborder=0 height='"+res.returnArray[0].HEIGHT+"'></iframe>";
        					}else if(res.returnArray[0].TYPE == 3){
        						content = "<iframe id='iframe_content' src='./cogLoader?storeId="+res.returnArray[0].URL+"' width='100%' style='overflow:hidden' frameborder=0 height='"+res.returnArray[0].HEIGHT+"'></iframe>";
        					}else if(res.returnArray[0].TYPE == 5){
        						content = "<iframe id='iframe_content' src='./spotLoader?guid="+res.returnArray[0].URL+"' width='100%' style='overflow:hidden' frameborder=0 height='"+res.returnArray[0].HEIGHT+"'></iframe>";
        					}else if(res.returnArray[0].TYPE == 2){
        						content = "<iframe src='"+res.returnArray[0].URL+"' width='100%' frameborder=0 height='"+res.returnArray[0].HEIGHT+"'  style='overflow:hidden'></iframe>";
        					}else if(res.returnArray[0].TYPE == 1){
        						content = "<iframe id='iframe_content' src='./rss?url="+res.returnArray[0].URL+"' width='100%' frameborder=0  style='overflow:hidden' height='"+res.returnArray[0].HEIGHT+"'></iframe>";
        					}else{
        						content = "<iframe id='iframe_content' src='."+res.returnArray[0].URL+"' width='100%' frameborder=0 style='overflow:hidden'></iframe>";
        					}
        				}
        				$("#portletContent").append(content);
        			}
        		});
        	}
        	function goValue( value ){
        		var selectValue = $("#"+value).jqxButtonGroup("getSelection");
        		if(selectValue == "0"){
        			if(value == "radioButtonGroup"){$("#hiddenViewHeaderKeyValue").val("true");}
        			else if(value == "iconButton"){$("#hiddenViewIconKeyValue").val("true");}
        			else if(value == "moreButtonGroup"){$("#hiddenMoreKeyValue").val("true");}
        			else if(value == "activeButtonGroup"){$("#hiddenActive").val("1");}
        			else if(value == "menuButtonGroup"){$("#hiddenMenuOpenType").val("all");};
        		}else{
        			if(value == "radioButtonGroup"){$("#hiddenViewHeaderKeyValue").val("false");}
        			else if(value == "iconButton"){$("#hiddenViewIconKeyValue").val("false");}
        			else if(value == "moreButtonGroup"){$("#hiddenMoreKeyValue").val("false");}
        			else if(value == "activeButtonGroup"){$("#hiddenActive").val("0");}
        			else if(value == "menuButtonGroup"){$("#hiddenMenuOpenType").val("common");};
        		}
        	}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
			.iwidget_label_contents h1{
				font-size:13px !important;
				color:#225E8E;
				text-align:left;
				padding-right:5px;
				padding-left:10px;
				padding-bottom:5px;
				border-bottom:1px solid #A1BAD9;
			}
			.label_contents{
				margin-left:5px;
				margin-bottom:10px;
			}
			.label_contents>div{
				margin-bottom:10px;
			}
			.label_contents .label{
				width:100% !important;
				padding-left:15px !important;
				font-size:13px !important;
			}
			.blueish .content .label.type2{
				line-height:30px !important;
			}
		</style>
		   
    </head>
    <body class='blueish' style="overflow:scroll !important;">
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%; hegiht:500px; margin:0 10px;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<form id="searchForm" name="searchForm" action="./list">
					<div class="group_button f_right">
						<div class="button type2 f_left">
							<input type="button" value="목록" id='btnList' width="100%" height="100%" onclick="goList();" />
						</div>
						<div class="button type2 f_left">
							<input type="button" value="저장" id='btnSave' width="100%" height="100%" onclick="insert();" />
						</div>
						<div class="button type3 f_left">
							<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();" />
						</div>
					</div>
				</form>
			</div>
			<div class="container  f_left" style="margin-top:10px;width:100%;">
				<div class="content f_left" style="width:100%; margin:0 0%;">
					<form action="./insert" method="post" name="saveForm" id="saveForm" style="width:100%;" enctype="multipart/form-data">   
					<div id="splitter">
						<div>
							<div style="border: none; padding-top:0px;" id='jqxContents'>
				            	<div  class="iwidget_label" style="width:30%; float:left;">
									<div class="iwidget_label_contents" style="padding:5px;background-color:#ECF2F8  !important;">
										<div class="label_contents" style="margin:10px 0;">
											<c:if test="${!empty content.POID}">
												<h1>No. <span>${content.POID}</span></h1>
												<input type="hidden" id="hiddenPoid" name="poID" value="${content.POID}" />
											</c:if>
											<c:if test="${empty content.POID}">
												<input type="hidden" name="poID" />
											</c:if>
										</div>
										<div class="label_contents" style="margin-left:5px;">
											<span class="label type2" style="width:100%;">이름</span>
											<p><input type="text" id="txtName" name="name" value="${content.NAME}" /></p>
										</div>
										<div class="label_contents">
											<span class="label type2" style="width:100%;">높이</span>
											<p>
											<input type="hidden" name="keyName" value="height"/>
											<input type="hidden" name="explain" value="Height"/>
											<input type="text" id="txtHeight" name="keyValue" value="${content.HEIGHT}" style="text-align:center; text-indent:0 !important;"> <label>px</label></p>
										</div>
										<div class="label_contents">
											<span class="label type2" style="width:100%;">형태</span>
											<p>
												<input type="hidden" name="keyName" value="type"/>
												<input type="hidden" name="explain" value="형태"/>
												<input type="hidden" id="hiddenTypeKeyValue" name="keyValue" />
												<input type="radio" style="margin:5px 5px;" name="type" value="1" <c:if test="${content.TYPE == '1' }">checked="checked" </c:if>/><span>RSS</span>
												<input type="radio" style="margin:5px 5px;" name="type" value="2" <c:if test="${content.TYPE == '2' }">checked="checked" </c:if>/><span>Iframe</span><br>
												<input type="radio" style="margin:0 5px;" name="type" value="3" <c:if test="${content.TYPE == '3' }">checked="checked" </c:if>/><span>Cognos</span>
												<input type="radio" style="margin:0 5px;" name="type" value="4" <c:if test="${content.TYPE == '4' }">checked="checked" </c:if>/><span>Image</span>
											</p>
										</div>
										<div class="label_contents">
											<span class="label type2" style="width:100%;">파일업로드</span>
											<p>
												<c:if test="${portletImgExist == 'true'}">
													<img src="../../resources/img/portletImage/${content.POID}.gif" width="200px" height="100px"><br>
												</c:if>
												<input type="file" id="file" name="portletImage" class="upload" />
											</p>
										</div>
										<div class="label_contents">   
											<span class="label type2" style="width:100%;">그룹분류</span>
											<p>  
												<input type="hidden" name="keyName" value="portlet_grp"/>
												<input type="hidden" name="explain" value="portlet grp"/>
												<input type="hidden" id="hiddenPortletGrpKeyValue" name="keyValue" />
												<c:choose>
													<c:when test="${portletGrpList != null && not empty portletGrpList}">
														<c:set var = "SQUOT">'</c:set>
														<c:set var = "DQUOT">"</c:set>
														<c:forEach items="${portletGrpList}" var="grpList" varStatus="loop">
															<c:if test="${loop.count!=0 && loop.count % 2 == 0}">
															<br>	
															</c:if>   
															<input type="radio" style="margin:5px 5px;" name="portletGrp" value="${grpList.COM_COD}" 
															 <c:if test="${grpList.COM_COD == content.PORTLET_GRP}">checked</c:if> 
															 <c:if test="${grpList.COM_COD == param.portlet_grp}">checked</c:if> 
															 />
															 ${fn:replace(fn:replace(fn:replace(fn:replace(grpList.COM_NM,SQUOT,"&#39;"),DQUOT,"&quot;"),"<","&lt;"),">","&gt;")}
														</c:forEach> 		
													</c:when> 			
													<c:otherwise>
														그룹 분류 오류	
													</c:otherwise>
												</c:choose>
											</p>
										</div>
										<div class="label_contents">
											<span class="label type2" style="width:100%;">URL</span>
											<p>
												<input type="hidden" name="keyName" value="url" />
												<input type="hidden" name="explain" vale="URL" />
												<input type="text" id="txtUrl" name="keyValue" value="${content.URL}" class="label_font_active" >
											</p>
										</div>
										<div class="label_contents jqxbuttonGroup">
											<span class="label type2" style="width:100%;">헤더뷰여부</span> 
											<p>
												<input type="hidden" name="keyName" value="view_header"/>
												<input type="hidden" name="explain" value="포틀릿 헤더"/>
												<input type="hidden" id="hiddenViewHeaderKeyValue" name="keyValue" /> 
												<div id='radioButtonGroup' style="margin-left:10px;" onClick="goValue('radioButtonGroup');">
													<button style="padding:4px 16px;cursor:pointer;" name="viewValue">보여짐</button>
													<button style="padding:4px 16px;cursor:pointer;" name="viewValue">숨김</button>
												</div><!--jqxbuttonGroup-->
											</p>
											<span class="label type2" style="width:100%;">아이콘뷰여부</span>
											<p>
												<input type="hidden" name="keyName" value="view_icon"/>
												<input type="hidden" name="explain" value="아이콘뷰여부"/>
												<input type="hidden" id="hiddenViewIconKeyValue" name="keyValue" />
												<div id='iconButtonGroup' style="margin-left:10px;" onClick="goValue('iconButtonGroup');">
													<button style="padding:4px 16px;cursor:pointer;" name="iconValue">보여짐</button>
													<button style="padding:4px 16px;cursor:pointer;" name="iconValue">숨김</button>
												</div><!--jqxbuttonGroup-->
											</p>
											<span class="label type2" style="width:100%;">more</span>
											<p>
												<input type="hidden" name="keyName" value="more"/>
												<input type="hidden" name="explain" value="more"/>
												<input type="hidden" id="hiddenMoreKeyValue" name="keyValue" />
												<div id='moreButtonGroup' style="margin-left:10px;" onClick="goValue('moreButtonGroup');">
													<button style="padding:4px 16px;cursor:pointer;" id="Left">사용</button>
													<button style="padding:4px 16px;cursor:pointer;" id="Right">미사용</button>
												</div><!--jqxbuttonGroup-->
											</p>
											<span class="label type2" style="width:100%;">more url</span>
											<p style="margin-bottom:10px;">
												<input type="hidden" name="keyName" value="more_url"/>
												<input type="hidden" name="explain" value="more url"/>
												<input type="text" id = "txtMoreUrl" name="keyValue" value="${content.MORE_URL}" class="label_font_active"  />
											</p>
											<span class="label type2" style="width:100%;">상태</span>
											<p>
												<input type="hidden" id="hiddenActive" name="active" />   
												<div id='activeButtonGroup' style="margin-left:10px;" onClick="goValue('activeButtonGroup');">
													<button style="padding:4px 16px;cursor:pointer;" id="Left">사용</button>
													<button style="padding:4px 16px;cursor:pointer;" id="Right">정지</button>
												</div><!--jqxbuttonGroup-->
											</p>
											 
											<span class="label type2" style="width:100%;display:none;">포틀릿 공개여부</span>
											<p>
												<input type="hidden" id="hiddenMenuOpenType" name="menuOpenType" />
												<div id='menuButtonGroup' style="margin-left:10px;display:none;" onClick="goValue('menuButtonGroup');">
													<button style="padding:4px 16px;cursor:pointer;" id="Left" >전체공개</button>
													<button style="padding:4px 16px;cursor:pointer;" id="Right">권한공개</button>
												</div>
											</p>
											
										</div>
									</div><!--iwidget_label_contents-->
								</div><!--left영역-->
							</div>
						</div>
						<div id="ContentPanel">
							<div class="contentsArea">
								<div class="con" id="previewBox">
									<div id="portletContent"></div>	
								</div>
							</div>	
						</div>
					</div>
				</form>
			</div>
		</div>
		</div>
	</body>
</html>