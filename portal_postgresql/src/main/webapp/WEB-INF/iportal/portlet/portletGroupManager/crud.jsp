<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>포틀릿 그룹 관리</title>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
	<script type="text/javascript">
		var menudata = null;
		$(document).ready(function(){
			init();
		});
		function init(){
			$("#btnSave").jqxButton({ width: '', theme: 'blueish' });
			$("#btnDelete").jqxButton({ width: '', theme: 'blueish' });
			$("#btnReset").jqxButton({ width: '', theme: 'blueish' });
			search();
		}
		function search(){
			var portletGrpData = $i.post("./getPortletGroupManagerList", {});
			portletGrpData.done(function(portletGrp){
				var source =
	            {
	                datatype: "json",
	                datafields: [
	                    { name: 'UNID', type: 'string'},
	                    { name: 'GRP_NAME', type: 'string'},
	                    { name: 'ETC', type: 'string'}
	                ],
	                localdata: portletGrp.returnArray,
	                updaterow: function (rowid, rowdata, commit) {
	                    commit(true);
	                }
	            };
	            var alginCenter = function (row, columnfield, value) {//left정렬
					return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
				}; 
				var alginLeft = function (row, columnfield, value) {//left정렬
					var newValue = $i.secure.scriptToText(value);
					return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + newValue + '</div>';
				}; 
				var alginRight = function (row, columnfield, value) {//right정렬
					return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
				};
				var detailrow = function(row,columnfield,value,defaultHtml, property, rowdata){
					var newValue = $i.secure.scriptToText(value);
					var unID = rowdata.UNID;

					var link = "<a href=\"javascript:detailPortletGrp('"+unID+"')\" style='color:black; text-decoration:underline;' >" + newValue + "</a>";
					return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>"; 
				};
	            var dataAdapter = new $.jqx.dataAdapter(source, {
	                downloadComplete: function (data, status, xhr) { },
	                loadComplete: function (data) { },
	                loadError: function (xhr, status, error) { }
	            });
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#gridPortletGrpList").jqxGrid(
	            {
	              	width: '100%',
	                height: '100%',
					altrows:true,
					pageable: true,
					sortable: true,
	                source: dataAdapter,
	                pagesize: 100,
					pagesizeoptions:['100', '200', '300'],
					theme:'blueish', 
	                columnsresize: true,
	                selectionmode:'checkbox',
	                columns: [
	                   { text: '그룹 ID', datafield: 'UNID', width:'10%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
					   { text: '그룹명', datafield: 'GRP_NAME', width: '20%', align:'center', cellsalign: 'center', cellsrenderer: detailrow},
					   { text: '설명', datafield: 'ETC', align:'center', cellsalign: 'center', cellsrenderer: alginLeft}
	                ]
	            });
			});
		}
		function insert(){
			if($("#txtGrpName").val() == ""){
				$i.dialog.error("SYSTEM", "그룹명을 입력하세요");
				return false;
			}
			$i.insert("#saveForm").done(function(args){
				if(args.returnCode == "EXCEPTION"){
					$i.dialog.error("SYSTEM", args.returnMessage);
				}else{
					$i.dialog.alert("SYSTEM", args.returnMessage, function(){
						search();
						resetForm();
					});
				}
			});
		}
		function remove(){
			var checkRows = $("#gridPortletGrpList").jqxGrid("getselectedrowindexes");
			var gridRows = $("#gridPortletGrpList").jqxGrid("getRows");
			if(checkRows.length> 0){
				var html = "";
				for(var i=0;i<checkRows.length;i++){
					html += "<input type='hidden' name='arrayDeleteUnID' value='"+gridRows[checkRows[i]].UNID+"' />";
				}
				$("#deleteForm").append(html);
			}else{
				$i.dialog.error("SYSTEM", "삭제할 그룹명을 선택하세요");
				return false;
			}
			$i.dialog.confirm("SYSTEM", "삭제하시겠습니까?", function(){
				$i.remove("#deleteForm").done(function(args){
					if(args.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", args.returnMessage);
					}else{
						$i.dialog.alert("SYSTEM", args.returnMessage, function(){
							search();
							resetForm();
						});
					}
				});
			});
			
		}
		function detailPortletGrp(unID){
			var detailData = $i.post("./getPortletGroupDataDetail", {unID:unID});
			detailData.done(function(data){
				if(data.returnCode == "EXCEPTION"){
					$i.dialog.error("SYSTEM", data.returnMessage);
				}else{
					if(data.returnArray.length > 0){
						$("#hiddenUnID").val(data.returnArray[0].UNID);
						$("#txtGrpName").val(data.returnArray[0].GRP_NAME);
						$("#txtEtc").val(data.returnArray[0].ETC);
					}
				}
			});
		}
		function resetForm(){
			$("#hiddenUnID").val("");
			$("#txtGrpName").val("");
			$("#txtEtc").val("");
		}
	</script>
</head>
<body class='blueish'>
	<div class="wrap" style="width:98%;margin:0 10px;">
		<div class="header f_left" style="width:100%; height:27px; margin-top:10px">
			<div class="group_button f_right">                                                                                                                                                                                                                                            
				<div class="button type1 f_left">
					<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();" />
				</div>
			</div>
		</div>
		<div class="container  f_left" style="width:100%; margin:10px 0;margin-top:20px !important;">
			<div class="content f_left" style=" width:100%; margin-right:1%;">
				<form id="saveForm" name="saveForm" action="./insert">   
					<div class="grid f_left" style="width:100%; height:450px;margin-bottom:20px;">	
						<div id="gridPortletGrpList"> </div>
					</div>
					<div class="blueish table f_left"  style="width:100%; margin:10px 0;margin-top:20px !important;">
						<table  width="100%" cellspacing="0" cellpadding="0" border="0">
							<thead>
								<tr>
									<th scope="col" style="width:30%; height:40px;">그룹명</th>
									<th scope="col" style="width:70%; height:40px;">설명</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<div class="cell">
											<input type="hidden" id="hiddenUnID" name="unID" />
											<input type="text" id="txtGrpName" name="grpName" class="input type2  f_left  m_r10"  style="width:95%; margin:3px 0; "/>
										</div>
									</td>
									<td>
										<div class="cell">
											<input type="text" id="txtEtc" name="etc" class="input type2 f_left" style="width:95%; margin:3px 0; "/>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="group_button f_right">
						<div class="button type2 f_left">
							<input type="button" value="초기화" id='btnReset' width="100%" height="100%" onclick="resetForm();" />
						</div>
						<div class="button type2 f_left">
							<input type="button" value="저장" id='btnSave' width="100%" height="100%" onclick="insert();" />
						</div>
					</div>
				</form>
				<form id="deleteForm" name="deleteForm">
				</form>
			</div>
		</div>          
	</div>
</body>
</html>