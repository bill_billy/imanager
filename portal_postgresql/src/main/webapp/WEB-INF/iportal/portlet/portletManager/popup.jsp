<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>포틀릿 설정</title>
	<link rel="stylesheet" href="../../resources/css/popup/setting.css">
	<link rel="styleSheet" href="../../resources/css/dtree.css" type="text/css">
    <style>
    	.strong_point{font-weight:bold;color:red}
    </style>
    <script src="../../resources/js/jquery/jquery-1.8.3.min.js"></script>
    <script src="../../resources/js/jquery/jquery-ui-1.10.3.custom.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
    <script>
    function accordionMenu(searchValue, res){

       	if(searchValue == ""){
       		alert("");
    		return;
    	}
    	
    	$(".strong_point").removeClass("strong_point");
    	
		for(var i=0;i<res.length;i++){
			if(searchValue == null){
				$(".content_menu").append("<h3 class='link' id='accord_"+res[i].COM_COD+"'>"+$i.secure.scriptToText(res[i].COM_NM)+" [<span class='thin_font' id='accord_"+res[i].COM_COD+"_count'></span>]</h3><div class='wrap_list' id='accord_"+res[i].COM_COD+"_div'><ul id='accord_"+res[i].COM_COD+"_div_ul'></ul></div>");
			}
			var COD = res[i].COM_COD;
// 			var listData = $i.post("./getPageletPortletList", {pID:res[i].COM_COD, searchValue:searchValue});
// 			listData.done(function(data){   
				
			$i.post("./getPageletPortletList", {pID:res[i].COM_COD, searchValue:searchValue}).done(function(data){
				count = 0;
				if(searchValue != null){
					for(var j=0;j<data.returnArray.length;j++){
						$("#poid_"+data.returnArray[j].POID).addClass("strong_point");
						count++;
					}
				}else{
					for(var j=0;j<data.returnArray.length;j++){
						$("#accord_"+data.returnArray[j].TYPE+"_div_ul").append("<li class='link' id='poid_"+data.returnArray[j].POID+"' onclick='prc("+data.returnArray[j].POID+")'>"+data.returnArray[j].NAME+"</li>");
						count++;
					}
				}
				if(count == 0){
					$("#accord_"+data.returnArray[j-1].TYPE).remove();
					$("#accord_"+data.returnArray[j-1].TYPE+"_div").remove();
				}else{
					$("#accord_"+data.returnArray[j-1].TYPE+"_count").text(count);
				}
			});
		}
   	    var icons = {
            header: "plus_square",
            activeHeader: "close_square"
        };
        $( ".content_menu" ).accordion({
        	icons:icons,
        	collapsible: true,
        	heightStyle: "content"
        });
    }
    
	$(document).ready(function(){
		$i.post("./getPortletCategory", {}).done(function(res){
			accordionMenu(null, res.returnArray);
		});
	});
	
	function viewHtml(target, title){
		$("#title_rgt").text(title);
		$("#right_content > div").hide();
		$("#"+target).show();
	}
	
	var gPoid = null;
	
	function makePortletCategory(searchValue, res){
		for(var i=0;i<res.length;i++){
			$("#span_"+res[i].COM_COD).remove();
			if(searchValue != null){
				displayStatus = "block";
			}else{
				displayStatus = "none";
			}
			$("#sidebar").append("<span id='span_"+res[i].COM_COD+"'><p class=\"bullet1\" id='poid_"+res[i].COM_COD+"' style='cursor:pointer' onclick='viewPortletList("+res[i].COM_COD+")'>"+$i.secure.scriptToText(res[i].COM_NM)+"</p><ul id='poid_"+res[i].COM_COD+"_ul' style='display:"+displayStatus+"'></ul></span>");
			gPoid=res[i].COM_COD;

			$i.post("./getPageletPortletList", {pID:res[i].COM_COD, searchValue:searchValue}).done(function(data){
				makePortletLiList(res.returnArray);
			});
		}
	}  
	
	function makePortletLiList(res){
		if(res.length < 1){
			$("#poid_"+gPoid).remove();
		}
		for(var i=0;i<res.length;i++){
			$("#poid_"+res[i].TYPE+"_ul").append("<li class=\"mem\" onclick='prc("+res[i].POID+")' style='cursor:pointer'>"+res[i].NAME+"</li>");
		}
		
	}
	
	function viewPortletList(typeId){
		$("#poid_"+typeId+"_ul").toggle();
	}
	
	function prc(poid){
		var gArr = new Array();
		
		gArr.push(poid);
		
		opener.portletIframe_admin.loadPortlet(gArr, false);
	}
	
	function searchPortlet(){
		$i.post("./getPortletCategory", {}).done(function(res){
			accordionMenu($("#search_value").attr("value"), res.returnArray);
		});
	}
	
    </script>

</head>

<body>
<!-- #wrap -->
<div id="wrap">
	<!-- #header -->
	<div id="header">
        <div class="header_title_rgt" style="width:100%"><span class="title_rgt" id='title_rgt'>포틀릿</span></div>
	</div><!-- // #header -->
	<!-- #container -->
	<div id="container">

        <div id="right_content">
			<!-- #content -->
	
			<div id="content">
				<div id="search">
					<input type="text" class="input_txt" id="search_value" />
	                <p id="btn">
						<a href="javascript:searchPortlet();" class="button button_white">
							<span>
								<img src="${WEB.ROOT}/resources/cmresource/image/icon-search.png"  alt="조회" style=" vertical-align:middle; padding-bottom:3px; padding-right:3px;"/>조회
							</span>
						</a>
						<a href="javascript:location.reload();" class="button button_white">
							<span>
								전체목록
							</span>
						</a>
					</p>	
	            </div><!-- // #search -->
				<div class="content_menu">
				</div><!-- // content_menu -->
			</div><!-- // #content -->
		</div>
	</div><!-- // container -->
</div><!-- // #wrap -->
</body>
</html>
