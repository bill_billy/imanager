<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Main 화면 관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <script>
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		$("#btnNew").jqxButton({width:'', theme:'blueish'});
        		$("#btnReset").jqxButton({width:'', theme:'blueish'});
        		search();
        	}
        	//조회 
        	function search(){
        		var portletData = $i.post("./getPortletManagerList",{});
        		portletData.done(function(data){
        			var source =
					{
						datatype: "json",
						datafields: [
							{ name: 'PLID', type: 'int'},
							{ name: 'PL_NAME', type: 'string'},
							{ name: 'PL_DEFAULT', type: 'string'},
						],
						localdata: data.returnArray,
						updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
					};
					
					var dataAdapter = new $.jqx.dataAdapter(source);
					var userNameRendererleft = function (row, columnfield, value, defaultHtml, property, rowdata) {//left정렬
						var newValue = $i.secure.scriptToText(value);
						var plId = rowdata.PLID;
						var link = "<a href=\"javaScript:goDetail('"+plId+"');\" style='color: #000000;'>" + newValue + "</a>";
	                    return '<div  id="userName-' + row + '"style="text-align: left; margin:7px 0px 0px 20px; cursor:pointer; font-weight:bold;">' + link + '</div>';
	               	};
	               	var radiobutton = function (row, columnfield, value, defaultHtml, property, rowdata) {//left정렬
	               		var plId = rowdata.PLID;
	               		var plDefault = rowdata.PL_DEFAULT;
	               		if(plDefault == '1'){
	               			return "<div style='text-align:center; margin-top:5px;'><input type='hidden' name='plId' value='"+plId+"' /><input type='radio' style='text-align:center;' name='test' onclick='changeDefaultPl("+plId+");' checked='checked' /></div>";	
	               		}else{
	               			return "<div style='text-align:center; margin-top:5px;'><input type='hidden' name='plId' value='"+plId+"' /><input type='radio' style='text-align:center;' name='test' onclick='changeDefaultPl("+plId+");' /></div>";
	               		}
	               	};
					$("#gridPortletList").jqxGrid({
						width: '100%',
						height: '100%',
						altrows:true,
						source: dataAdapter,
						theme:'blueish',  
						columnsheight:30 ,
						rowsheight: 30,
						editable: true,   
						selectionmode: 'singlerow',
						columnsresize: true,
						columns: [
							{ text: 'No', datafield: 'PLID', width: '5%', align:'center', cellsalign: 'center',editable:false},
							{ text: '화면명', datafield: 'PL_NAME', width: '75%', align:'center', cellsalign: 'left',editable:false, cellsrenderer:userNameRendererleft},
							{ text: '기본셋팅', value: 'c', width: '20%', align:'center', cellsalign: 'center',cellsrenderer:radiobutton}
						]
					});
        		});
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function changeDefaultPl ( plID ){
        		$i.post("./defalutPortletSetting", {plID : plID}).done(function(data){
        			if(data.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM", data.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM", data.returnMessage);
        			}
        		});
        	}
        	function setAllPageletDefault(){
        		$i.post("./defaultUserPortlet", {}).done(function(data){
        			if(data.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM", data.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM", data.returnMessage);
        			}
        		});
        	}
        	function goDetail( plID ){
        		location.href="./detail?plID="+plID;
        	}
        	function newPage(){
        		location.replace("./addPortlet");
        	}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
		</style>
		   
    </head>
    <body class='blueish'>
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%;  margin:0 10px;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0;">
				<form id="searchForm" name="searchForm" action="./list">
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="신규" id='btnNew' width="100%" height="100%" onclick="newPage();" />
						</div>
						<div class="button type1 f_left">
							<input type="button" value="전부초기화" id='btnReset' width="100%" height="100%" onclick="setAllPageletDefault();" />
						</div>
					</div>
				</form>
			</div>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:100%; margin:0%;">
					<div class="grid f_left" style="width:100%; height:620px !important;">
						<div id="gridPortletList"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>