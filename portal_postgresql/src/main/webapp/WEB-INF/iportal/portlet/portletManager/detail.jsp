<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Main 화면 관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/css/iplanbiz/styles/jqx.base.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../resources/css/iplanbiz/styles/iplanbiz.basic.css" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="../../resources/css/iplanbiz/styles/jqx.SophisticatedLayout.css" type="text/css"/>
        
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        	var portletTheme = 1;
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init();
        		var pElement = $('.iwidget_label_contents').parent();
        		pElement.css('background-color','#ecf2f8');
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		var countries = new Array("");
        		changePortletLayout('${plInfo[0].PL_LAYOUT}');
        		$("#btnList").jqxButton({width:'', theme:'blueish'});
        		$("#btnSave").jqxButton({width:'', theme:'blueish'});
        		$("#btnDelete").jqxButton({width:'', theme:'blueish'});
        		$("#btnContents").jqxButton({ width: '', theme:'blueish'});
        		$("#btnApply").jqxButton({ width : '' , theme : 'blueish'});
        		$("#pagelet_name").jqxInput({placeHolder: "", height: 25, width: 165, minLength: 1,  source: countries, theme : 'blueish' });
        		var source = [
	                { html: "<div style=''width:100%; height: 90px; float: left; vertical-align:middle; border-bottom:1px solid #cccccc;' value='1'><img width='89' height='88' style='float: left; margin-top: 2px;  margin-right: 5px;' src='../../resources/img/setting/portlet_thumnail_1.gif'/><span style='float: left; mar font-size: 13px; vertical-align:middle; line-height:89px; '>layout1</span></div>", title: 'layout1' ,value: '1'},
					{ html: "<div style=''width:100%; height: 90px; float: left; vertical-align:middle; border-bottom:1px solid #cccccc;' value='2'><img width='89' height='88' style='float: left; margin-top: 2px;  margin-right: 5px;' src='../../resources/img/setting/portlet_thumnail_3.gif'/><span style='float: left; mar font-size: 13px; vertical-align:middle; line-height:89px; '>layout2</span></div>", title: 'layout2' ,value: '2'},
					{ html: "<div style=''width:100%; height: 90px; float: left; vertical-align:middle; border-bottom:1px solid #cccccc;' value='3'><img width='89' height='88' style='float: left; margin-top: 2px;  margin-right: 5px;' src='../../resources/img/setting/portlet_thumnail_4.gif'/><span style='float: left; mar font-size: 13px; vertical-align:middle; line-height:89px; '>layout3</span></div>", title: 'layout3' ,value: '3'},
					{ html: "<div style=''width:100%; height: 90px; float: left; vertical-align:middle; border-bottom:1px solid #cccccc;' value='4'><img width='89' height='88' style='float: left; margin-top: 2px;  margin-right: 5px;' src='../../resources/img/setting/portlet_thumnail_5.gif'/><span style='float: left; mar font-size: 13px; vertical-align:middle; line-height:89px; '>layout4</span></div>", title: 'layout4' ,value: '4'},
					{ html: "<div style=''width:100%; height: 90px; float: left; vertical-align:middle; border-bottom:1px solid #cccccc;' value='5'><img width='89' height='88' style='float: left; margin-top: 2px;  margin-right: 5px;' src='../../resources/img/setting/portlet_thumnail_6.gif'/><span style='float: left; mar font-size: 13px; vertical-align:middle; line-height:89px; '>layout5</span></div>", title: 'layout5' ,value: '5'},
					{ html: "<div style=''width:100%; height: 90px; float: left; vertical-align:middle; border-bottom:1px solid #cccccc;' value='6'><img width='89' height='88' style='float: left; margin-top: 2px;  margin-right: 5px;' src='../../resources/img/setting/portlet_thumnail_7.gif'/><span style='float: left; mar font-size: 13px; vertical-align:middle; line-height:89px; '>layout6</span></div>", title: 'layout6' ,value: '6'},
					{ html: "<div style=''width:100%; height: 90px; float: left; vertical-align:middle; border-bottom:1px solid #cccccc;' value='7'><img width='89' height='88' style='float: left; margin-top: 2px;  margin-right: 5px;' src='../../resources/img/setting/portlet_thumnail_8.gif'/><span style='float: left; mar font-size: 13px; vertical-align:middle; line-height:89px; '>layout7</span></div>", title: 'layout7' ,value: '7'},
					{ html: "<div style=''width:100%; height: 90px; float: left; vertical-align:middle; border-bottom:1px solid #cccccc;' value='8'><img width='89' height='88' style='float: left; margin-top: 2px;  margin-right: 5px;' src='../../resources/img/setting/portlet_thumnail_9.gif'/><span style='float: left; mar font-size: 13px; vertical-align:middle; line-height:89px; '>layout8</span></div>", title: 'layout8' ,value: '8'},
			    ];
	           	// Create a jqxListBox
	           	$("#jqxListmenu").jqxListBox({ source: source, width: 175, height: 395, theme:'blueish'});
        		$("#splitter").jqxSplitter({ width:'', height: 1000, panels: [{ min: 200 , size: 200 }, { min: 200}], theme:'blueish' });
        		$('#jqxContents').jqxTree({  height: '100%', width: '100%' , theme:'blueish'});
        	}
        	//조회 
        	function search(){
        	}
        	//입력
        	function insert(){
        		var pageletName = $("#pagelet_name").val();
				var layoutNo = $("#portlet_layout").find(":checked").val();
				var plid = '${param.plID}';
				
				$i.post("./updatePageletInfo", {plName : pageletName, plLayout : layoutNo, plID : plid}).done(function(data){
					if(data.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", data.returnMessage);
					}else{
						portletIframe_admin.saveAdminPortlet(plid);
// 						$("#portletIframe_admin").contents().find(".portlet").each(function(){
// 							var portletId = $(this).attr("id");
// 							var portletTheme = $(this).attr("portletType");
							
// 							$i.post("./updatePageletTheme", {portletType: portletTheme, plID : plid, poID:portletId}).done(function(data){
								
// 		        			});
			
// 							groupManagerDwr.dwrSetPageletTheme(plid, portletId, portletTheme, function(res){
// 							});
// 						});
					}
				});
        	}
        	//삭제
        	function remove(){
        		$i.dialog.confirm("SYSTEM", "삭제하시겠습니까?",function(){
        			$i.post("./deletePageletInfo", {plID:"${param.plID}"}).done(function(data){
	        			if(data.returnCode == "EXCEPTION"){
	        				$i.dialog.error("SYSTEM", data.returnMessage);
	        			}else{
	        				$i.dialog.alert("SYSTEM", data.returnMessage, function(){
	        					goList();
	        				});
	        			}
	        		});
        		});
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	function goList(){
        		location.replace("./list");
        	}
        	function changePortletLayout(val){
        		$('#portlet_layout').find('input[value='+val+']').attr('checked', true);
				$(".iframe_content").attr("src", "./admin_l"+val+"?plID=${param.plID}");
        	}
        	function setPageletPortletLayout(){
				//portletTheme = $("input[name=portlet_theme]:checked").val();
				portletTheme = $("#jqxListmenu").val();
				switch(portletTheme){
					case "1":
						default_portlet_top_background_color = "#DDDDDD";
						default_portlet_top_font_color = "#000000";
						default_portlet_top_underline = "0";
					break;
					case "2":
						default_portlet_top_background_color = "#FFFFFF";
						default_portlet_top_font_color = "#5D5D5D";
						default_portlet_top_underline = "1";
					break;
					case "3":
						default_portlet_top_background_color = "#67799D";
						default_portlet_top_font_color = "#FFFFFF";
						default_portlet_top_underline = "0";
					break;
					case "4":
						default_portlet_top_background_color = "#FFFFFF";
						default_portlet_top_font_color = "#5D5D5D";
						default_portlet_top_underline = "0";
					break;
					default:
						default_portlet_top_background_color = "#DDDDDD";
						default_portlet_top_font_color = "#000000";
						default_portlet_top_underline = "0";
					break;
				}
				$("#portletIframe_admin").contents().find(".adminPortletCheckbox").each(function(){
					if($(this).is(":checked")){
						$(this).parent().parent().removeClass("ui-widget-content-1");
						$(this).parent().parent().removeClass("ui-widget-content-2");
						$(this).parent().parent().removeClass("ui-widget-content-3");
						$(this).parent().parent().removeClass("ui-widget-content-4");
						$(this).parent().parent().addClass("ui-widget-content-"+portletTheme);
						
						$(this).parent().parent().attr("portletType", portletTheme);
						$(this).parent().parent().parent().parent().parent().attr("portletType", portletTheme);
						$(this).parent().css("background-color", default_portlet_top_background_color);
						$(this).parent().css("color", default_portlet_top_font_color);
						
						$(this).parent().parent().parent().parent().find("input").each(function(){
							if($(this).attr("name") == "title_background_color"){
								$(this).attr("value", default_portlet_top_background_color);
							}
							if($(this).attr("name") == "title_font_color"){
								$(this).attr("value", default_portlet_top_font_color);
							}
						});
						
					}
				});
        	}
        	function portletManagerOpen(){
        		var temp = window.open("./popup","포틀릿관리3","height=400,width=500,scrollbars=yes");
				temp.focus();
			}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
			.iwidget_label_contents h1{
				margin:10px 0;
				margin-bottom:15px;
			}
			.label_contents span{
				font-size:13px !important;
			}
			.label_contents{
				margin-bottom:15px;
			}
		</style>
		   
    </head>
    <body class='blueish' style="overflow:scroll !important;">
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%;  margin:0 10px !important;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0;margin-bottom:20px !important;">
				<form id="searchForm" name="searchForm" action="./list">
					<div class="group_button f_right">
						<div class="button type2 f_left">
							<input type="button" value="목록" id='btnList' width="100%" height="100%" onclick="goList();" />
						</div>
						<div class="button type2 f_left">
							<input type="button" value="저장" id='btnSave' width="100%" height="100%" onclick="insert();" />
						</div>
						<div class="button type3 f_left">
							<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();" />
						</div>
					</div>
				</form>
			</div>
			<div id="splitter" >
				<div>
					<div style="border:none; padding-top:0px;" id="jqxContents">
						<div  class="iwidget_label" style="width:30%; float:left;">
							<div class="iwidget_label_contents content" style="padding:5px;background-color:#ECF2F8  !important;">
								<h1><span>Layout 설정</span></h1>
								<div class="label_contents">
									<span class="label type2">&nbsp;화면명</span>
									<p>
										<input type="text" id="pagelet_name" value="${plInfo[0].PL_NAME}">
									</p>
								</div>
								<div class="label_contents">
									<span class="label type2">&nbsp;컨텐츠 추가</span>
									<p>
										<input type="button" value="컨텐츠보기" id='btnContents' width="100%" height="100%" style=" margin-right:0px !important;" onclick="portletManagerOpen();" />
									</p>
								</div>
								<div class="label_contents">
									<span class="label type2">&nbsp;Portlet Layout</span>
									<p>
										<input type="hidden" id="portlet_theme" name="portlet_theme" />
										<div class="imageListbox" style="width:90%;margin:0 auto;">
											<div id='jqxListmenu' style="border:1px solid #cccccc !important;"> </div>
										</div>
									</p>
								</div>
								<div  style="width:100%; float:left; margin:20px 0 10px 0;"><!-- 하단버튼-->
									<p style=" text-align:center !important;">
										<span class="buttonStyle_5" style=" height:25px; margin:5px;">
											<input type="button" value="적용" id='btnApply' width="100%" height="100%" onclick="setPageletPortletLayout();"/>
										</span> 
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="ContentPanel">
					<div class="contentsArea">
						<div class="layout" style="width:100%;">
							<div class="layoutbox" id="portlet_layout">
								<div class=" layoutbox_check"> <img src="../../resources/imgs/layout/layout1.gif" alt="레이아웃1" onclick="$('#portlet_layout').find('input[value=1]').attr('checked', true);changePortletLayout(1);"/>
									<div id="check" class="check_radio">
										<input type="radio"name="chk_layout" value="1" onclick="changePortletLayout(1);"/>
									</div>
								</div><!--layoutbox_check-->
								<div class=" layoutbox_check"> <img src="../../resources/imgs/layout/layout2.gif" alt="레이아웃2" onclick="$('#portlet_layout').find('input[value=2]').attr('checked', true);changePortletLayout(2);"/>
									<div id="check" class="check_radio">
										<input type="radio"name="chk_layout" value="2" onclick="changePortletLayout(2);"/>
									</div>
								</div><!--layoutbox_check-->
								<div class=" layoutbox_check"> <img src="../../resources/imgs/layout/layout3.gif" alt="레이아웃3" onclick="$('#portlet_layout').find('input[value=3]').attr('checked', true);changePortletLayout(3);"/>
									<div id="check" class="check_radio">
										<input type="radio"name="chk_layout" value="3" onclick="changePortletLayout(3);"/>
									</div>
								</div><!--layoutbox_check-->
								<div class=" layoutbox_check"> <img src="../../resources/imgs/layout/layout4.gif" alt="레이아웃4" onclick="$('#portlet_layout').find('input[value=4]').attr('checked', true);changePortletLayout(4);"/>
									<div id="check" class="check_radio">
										<input type="radio"name="chk_layout" value="4" onclick="changePortletLayout(4);"/>
									</div>
								</div><!--layoutbox_check-->
								<div class=" layoutbox_check"> <img src="../../resources/imgs/layout/layout5.gif" alt="레이아웃5" onclick="$('#portlet_layout').find('input[value=5]').attr('checked', true);changePortletLayout(5);"/>
									<div id="check" class="check_radio">
										<input type="radio"name="chk_layout" value="5" onclick="changePortletLayout(5);"/>
									</div>
								</div><!--layoutbox_check-->
								<div class=" layoutbox_check"> <img src="../../resources/imgs/layout/layout6.gif" alt="레이아웃6" onclick="$('#portlet_layout').find('input[value=6]').attr('checked', true);changePortletLayout(6);"/>
									<div id="check" class="check_radio">
										<input type="radio"name="chk_layout" value="6" onclick="changePortletLayout(6);"/>
									</div>
								</div><!--layoutbox_check-->
								<div class=" layoutbox_check"> <img src="../../resources/imgs/layout/layout7.gif" alt="레이아웃7" onclick="$('#portlet_layout').find('input[value=7]').attr('checked', true);changePortletLayout(7);"/>
									<div id="check" class="check_radio">
										<input type="radio"name="chk_layout" value="7" onclick="changePortletLayout(7);"/>
									</div>
								</div><!--layoutbox_check-->   
								<div class=" layoutbox_check"> <span alt="레이아웃8" onclick="$('#portlet_layout').find('input[value=8]').attr('checked', true);changePortletLayout(8);">None </span>
									<div id="check" class="check_radio">
										<input type="radio"name="chk_layout" value="8" onclick="changePortletLayout(8);"/>
									</div>
								</div><!--layoutbox_check--> 
							</div><!--layoutbox-->
						</div>
						<div style="clear:both;">
							<div id="previewBox" style="background:#C3C7CC;">
 								<iframe name="portletIframe_admin" id="portletIframe_admin" class="iframe_content" frameborder="0" style="width:100%;height:900px;"></iframe>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</body>
</html>