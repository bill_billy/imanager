<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Main 화면 관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/css/portletManager/layout.css"/>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        	var portletTheme = 1;
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init();
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
				$("#pagelet_name").jqxInput({placeHolder: "", height: 25, width: 175, minLength: 1});
				changePortletLayout(1);
				$("#pageletListTable tr:even").addClass("odd");
				$(".iframe_content").css("width", "100%");
				$(".iframe_content").css("height", $("#previewBox").height());
        	}
        	//조회 
        	function search(){
        	}
        	//입력
        	function insert(){
        		var pageletName = $("#pagelet_name").val();
				var layoutNo = $("#portlet_layout").find(":checked").val();
				
				if(pageletName == ""){
					$i.dialog.error("SYSTEM", "화면명을 입력하세요");
					return;
				}
				if($("[name=chk_info]:checked").length==0){
					$i.dialog.error("SYSTEM", "레이아웃을 선택해 주세요");
					return;
				}
				if($("[name=portlet_theme]:checked").length==0){
					$i.dialog.error("SYSTEM", "테마를 선택해 주세요");
					return;
				}
				var plid = null;
				
				$i.post("./InsertPageletInfo", {plName : pageletName, plLayout : layoutNo}).done(function(data){
					if(data.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", data.returnMessage);
					}else{
						portletIframe_admin.saveAdminPortlet(data.returnObject.PLID);
					}
				});
        	}
        	//삭제
        	function remove(){    
        		$i.post("./delete","#saveForm").done(function(data){
        			if(data.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM", data.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM", data.returnMessage, function(){
        					goList();
        				});
        			}
        		});
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	function goList(){
        		location.replace("./list");
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function changePortletLayout(layoutNo){
        		$(".iframe_content").attr("src", "./admin_l"+layoutNo);
        	}
        	function portletManagerOpen(){
        		var temp = window.open("./popup","포틀릿관리2","height=400,width=500,scrollbars=yes");
				temp.focus();
        	}
        	function setPageletPortletLayout(){
				portletTheme = $("input[name=portlet_theme]:checked").val();

				switch(portletTheme){
					case "1":
						default_portlet_top_background_color = "#DDDDDD";
						default_portlet_top_font_color = "#000000";
						default_portlet_top_underline = "0";
					break;
					case "2":
						default_portlet_top_background_color = "#FFFFFF";
						default_portlet_top_font_color = "#5D5D5D";
						default_portlet_top_underline = "1";
					break;
					case "3":
						default_portlet_top_background_color = "#67799D";
						default_portlet_top_font_color = "#FFFFFF";
						default_portlet_top_underline = "0";
					break;
					case "4":
						default_portlet_top_background_color = "#FFFFFF";
						default_portlet_top_font_color = "#5D5D5D";
						default_portlet_top_underline = "0";
					break;
					default:
						default_portlet_top_background_color = "#DDDDDD";
						default_portlet_top_font_color = "#000000";
						default_portlet_top_underline = "0";
					break;
				}
				
				$("#portletIframe_admin").contents().find(".adminPortletCheckbox").each(function(){
					if($(this).attr("checked")){
						$(this).parent().parent().removeClass("ui-widget-content-1");
						$(this).parent().parent().removeClass("ui-widget-content-2");
						$(this).parent().parent().removeClass("ui-widget-content-3");
						$(this).parent().parent().removeClass("ui-widget-content-4");
						$(this).parent().parent().addClass("ui-widget-content-"+portletTheme);
						
						$(this).parent().parent().attr("portletType", portletTheme);
						$(this).parent().parent().parent().parent().parent().attr("portletType", portletTheme);
						
						$(this).parent().css("background-color", default_portlet_top_background_color);
						$(this).parent().css("color", default_portlet_top_font_color);
						
						$(this).parent().parent().find("input").each(function(){
							if($(this).attr("name") == "title_background_color"){
								$(this).attr("value", default_portlet_top_background_color);
							}
							if($(this).attr("name") == "title_font_color"){
								$(this).attr("value", default_portlet_top_font_color);
							}
						});
						
					}
				});
        	}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
		</style>
		   
    </head>
    <body class='blueish'>
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:99%;  margin:0 10px;">
			<div id="top_bar"></div>
			<div class="group_button f_right">
				
			</div>
			<div class="top_btn">
				<p id="btn">
					<a href="javascript:insert();" class="button button_gray">
						<span>
							저장
						</span>
					</a>
					<a href="javascript:location.href='./list'" class="button button_gray">
						<span>
							목록
						</span>
					</a>
				</p>	
			</div>
			<!--상단 바 끝-->
			
			<div id="content" style="width:100%;">
				<div class="left" style="width:34%; float:left;">
				<form action="./prc" method="post" name="con_left" id="layout" style="width:100%;">
					<h1><span>Layout 설정</span></h1>
		   			<p>
						<label for="frm_name">화면명</label>
						<input type="text" id="pagelet_name" name="pagelet_name">
					</p>
					<p>
						<label for="frm_add">컨텐츠 추가</label>
						<a href="javascript:portletManagerOpen();" class="button button_white">컨텐츠보기</a>
					</p>
					<p style="border-bottom:none;">
						<label for="frm_name">Portlet Layout</label>	
					</p>
					<div class="layout1" style="width:100%;  ">
					<div class="leftbox" style="width:100%; margin:0 auto;">
						<div class="layoutbox">
							<img src="../../resources/img/setting/portlet_thumnail_1.gif" alt="레이아웃1" onclick="$('#viewPortletTheme').find('input[value=1]').attr('checked', true)" />
							<div id="check"><input type="radio"name="portlet_theme" value="1" <c:if test="${systemConfig.portlet_theme == '1'}">checked</c:if> /></div>
						</div>
						<div class="layoutbox">
							<img src="../../resources/img/setting/portlet_thumnail_3.gif"  alt="레이아웃3"  onClick="$('#viewPortletTheme').find('input[value=3]').attr('checked', true)" />
							<div id="check"><input type="radio"name="portlet_theme" value="3" <c:if test="${systemConfig.portlet_theme == '3'}">checked</c:if> /></div>
						</div>
						<div class="layoutbox">
							<img src="../../resources/img/setting/portlet_thumnail_4.gif"  alt="레이아웃4"  onClick="$('#viewPortletTheme').find('input[value=4]').attr('checked', true)" />
							<div id="check"><input type="radio"name="portlet_theme" value="4" <c:if test="${systemConfig.portlet_theme == '4'}">checked</c:if> /></div>
						</div>
						<div class="layoutbox">
							<img src="../../resources/img/setting/portlet_thumnail_5.gif"  alt="레이아웃5"  onClick="$('#viewPortletTheme').find('input[value=5]').attr('checked', true)" />
							<div id="check"><input type="radio"name="portlet_theme" value="5" <c:if test="${systemConfig.portlet_theme == '5'}">checked</c:if> /></div>
						</div>
						<div class="layoutbox">
							<img src="../../resources/img/setting/portlet_thumnail_6.gif"  alt="레이아웃6"  onClick="$('#viewPortletTheme').find('input[value=6]').attr('checked', true)" />
							<div id="check"><input type="radio"name="portlet_theme" value="6" <c:if test="${systemConfig.portlet_theme == '6'}">checked</c:if> /></div>
						</div>
						<div class="layoutbox">
							<img src="../../resources/img/setting/portlet_thumnail_7.gif"  alt="레이아웃7"  onClick="$('#viewPortletTheme').find('input[value=7]').attr('checked', true)" />
							<div id="check"><input type="radio"name="portlet_theme" value="7" <c:if test="${systemConfig.portlet_theme == '7'}">checked</c:if> /></div>
						</div>
						<div class="layoutbox">
							<img src="../../resources/img/setting/portlet_thumnail_8.gif"  alt="레이아웃8"  onClick="$('#viewPortletTheme').find('input[value=8]').attr('checked', true)" />
							<div id="check"><input type="radio"name="portlet_theme" value="8" <c:if test="${systemConfig.portlet_theme == '8'}">checked</c:if> /></div>
						</div>
						<div class="layoutbox">
							<img src="../../resources/img/setting/portlet_thumnail_9.gif"  alt="레이아웃9"  onClick="$('#viewPortletTheme').find('input[value=9]').attr('checked', true)" />
							<div id="check"><input type="radio"name="portlet_theme" value="9" <c:if test="${systemConfig.portlet_theme == '9'}">checked</c:if> /></div>
						</div>
					</div><!--layout-->
					</div><!--leftbox-->
		     		<p id="btn" style="text-align:center;clear:both;border-bottom:none;display:block;">
						<a href="javascript:setPageletPortletLayout();" class="button button_gray">
							<span>
								적용
							</span>
						</a>
					</p>
			 </form>
			 </div><!--left영역-->
			 
			 <div class="right" style="width:66%; float:left;">
				<div class="layout2" style="width:100%;">
					<div class="rightbox" style="width:78%; margin:0 auto;" id="portlet_layout">
					<div class="layoutbox_right">
						<img src="../../resources/imgs/layout/layout1.gif" alt="레이아웃1" onclick="$('#portlet_layout').find('input[value=1]').attr('checked', true);changePortletLayout(1);"/>
						<div id="check"><input type="radio"name="chk_info" value="1" onclick="changePortletLayout(1);"/></div>
					</div>
					<div class="layoutbox_right">
						<img src="../../resources/imgs/layout/layout2.gif" alt="레이아웃2" onclick="$('#portlet_layout').find('input[value=2]').attr('checked', true);changePortletLayout(2);"/>
						<div id="check"><input type="radio"name="chk_info" value="2" onclick="changePortletLayout(2);"/></div>
					</div>
					<div class="layoutbox_right">
						<img src="../../resources/imgs/layout/layout3.gif" alt="레이아웃3" onclick="$('#portlet_layout').find('input[value=3]').attr('checked', true);changePortletLayout(3);"/>
						<div id="check"><input type="radio"name="chk_info" value="3" onclick="changePortletLayout(3);"/></div>
					</div>
					<div class="layoutbox_right">
						<img src="../../resources/imgs/layout/layout4.gif" alt="레이아웃4" onclick="$('#portlet_layout').find('input[value=4]').attr('checked', true);changePortletLayout(4);"/>
						<div id="check"><input type="radio"name="chk_info" value="4" onclick="changePortletLayout(4);" /></div>
					</div>
					<div class="layoutbox_right">
						<img src="../../resources/imgs/layout/layout5.gif" alt="레이아웃5" onclick="$('#portlet_layout').find('input[value=5]').attr('checked', true);changePortletLayout(5);"/>
						<div id="check"><input type="radio"name="chk_info" value="5" onclick="changePortletLayout(5);" /></div>
					</div>
					<div class="layoutbox_right">
						<img src="../../resources/imgs/layout/layout6.gif" alt="레이아웃6" onclick="$('#portlet_layout').find('input[value=6]').attr('checked', true);changePortletLayout(6);"/>
						<div id="check"><input type="radio"name="chk_info" value="6" onclick="changePortletLayout(6);" /></div>
					</div>
					<div class="layoutbox_right">
						<img src="../../resources/imgs/layout/layout7.gif" alt="레이아웃7" onclick="$('#portlet_layout').find('input[value=7]').attr('checked', true);changePortletLayout(7);"/>
						<div id="check"><input type="radio"name="chk_info" value="7" onclick="changePortletLayout(7);" /></div>
					</div>
					</div>
			
			 </div><!--layout2-->
			 
			 <!--미리보기화면영역-->
			 <div id="previewBox" style="background:#C3C7CC">
			 	<iframe name="portletIframe_admin" id="portletIframe_admin" class="iframe_content" frameborder="0"></iframe>
		 	</div>
		</div>
	</body>
</html>