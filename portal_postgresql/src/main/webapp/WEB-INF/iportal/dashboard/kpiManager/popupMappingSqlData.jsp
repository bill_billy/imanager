<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SQL</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$("#btnSend").jqxButton({width:'', theme:'blueish'});
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		search();
        	}
        	//조회
        	function search(){
        		var kpiGbn = "${param.kpiGbn}";
        		var kpiID = "${param.kpiID}";
        		
        		$("#txtareaSqlData").val($("#txtareaSql", opener.document).val());
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function sendParentData(){
				$("#txtareaSql", opener.document).val($("#txtareaSqlData").val()) ;
				self.close();
        	}
        </script>
    </head>
    <body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:600px; min-width:600px; height:610px; min-height:610px;  margin:0 1%;">
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="table f_left" style=" width:100%; margin-right:0;">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<thead>
							<th>SQL</th>
						</thead>
						<tbody>
							<td>
								<textarea id="txtareaSqlData" class="textarea" style="width:98%; height:500px; margin:3px 5px !important;"></textarea>
							</td>
						</tbody>
					</table>
				</div>
				<div class="group_button f_right">
					<div class="button type2 f_left" style="margin-bottom:0;">
						<input type="button" value="전송" id='btnSend' width="100%" height="100%" onclick="sendParentData();"/>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

