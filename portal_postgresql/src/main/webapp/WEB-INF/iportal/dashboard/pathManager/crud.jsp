<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>대시보드 경로 관리</title>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
	<script type="text/javascript">
		var menudata = null;
		$(document).ready(function(){
			init();
		});
		function init(){
			useYnCombo();
			$("#btnSave").jqxButton({ width: '', theme: 'blueish' });
			$("#btnDelete").jqxButton({ width: '', theme: 'blueish' });
			$("#btnReset").jqxButton({ width: '', theme: 'blueish' });
			$("#btnSearch").jqxButton({ width: '', theme: 'blueish' });
			search();
		}
		function search(){
			$('#gridDashboardPathList').jqxGrid('clearselection');
			var kpiListData = $i.post("./getDashboardPathList", {});
			kpiListData.done(function(data){   
				var source =
	            {
	                datatype: "json",
	                datafields: [
	                    { name: 'PATH_IDX', type: 'string'},
	                    { name: 'FULLPATH', type: 'string'},
	                    { name: 'PATH_DESC', type: 'string'},
	                    { name: 'BIGO', type: 'string'},
	                    { name: 'COM_UDC1', type: 'string'},
	                    { name: 'USE_YN', type: 'string'}
	                ],
	                localdata: data.returnArray,
	                updaterow: function (rowid, rowdata, commit) {
	                    commit(true);
	                }
	            };
	            var alginCenter = function (row, columnfield, value) {//left정렬
	            	var newValue = $i.secure.scriptToText(value);
					return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
				}; 
				var alginLeft = function (row, columnfield, value) {//left정렬
					return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
				}; 
				var alginRight = function (row, columnfield, value) {//right정렬
					return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
				};
				var detailrow = function(row,columnfield,value,defaultHtml, property, rowdata){
					var pathIdx = rowdata.PATH_IDX;
					var newValue = $i.secure.scriptToText(value);
					var link = "<a href=\"javascript:detailPath('"+pathIdx+"')\" style='color:black; text-decoration:underline;' >" + newValue + "</a>";
					return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>"; 
				}
	            var dataAdapter = new $.jqx.dataAdapter(source, {
	                downloadComplete: function (data, status, xhr) { },
	                loadComplete: function (data) { },
	                loadError: function (xhr, status, error) { }
	            });
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#gridDashboardPathList").jqxGrid(
	            {
	              	width: '100%',
	                height: '100%',
					altrows:true,
					pageable: true,
					sortable: true,
	                source: dataAdapter,
	                pagesize: 100,
					pagesizeoptions:['100', '200', '300'],
					theme:'blueish', 
	                columnsresize: true,
	                selectionmode:'checkbox',
	                columns: [
	                   { text: '경로', datafield: 'FULLPATH', align:'center', cellsalign: 'center', cellsrenderer: detailrow},
					   { text: '경로명', datafield: 'PATH_DESC', width: 200, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
					   { text: '설명', datafield: 'BIGO', width: 250, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
					   { text: '사용여부', datafield: 'USE_YN', width:100, align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
	                ]
	            });
			});
		}
		function insert(){
			if($("#txtFullpath").val() == ""){
				$i.dialog.error("SYSTEM", "경로를 입력하세요");
				return false;
			}
			$i.insert("#saveForm").done(function(args){
				if(args.returnCode == "EXCEPTION"){
					$i.dialog.error("SYSTEM", args.returnMessage);
				}else{
					$i.dialog.alert("SYSTEM", args.returnMessage, function(){
						search();
						resetForm();
					});
				}
			});
		}
		function remove(){
			var checkRows = $("#gridDashboardPathList").jqxGrid("getselectedrowindexes");
			var gridRows = $("#gridDashboardPathList").jqxGrid("getRows");
			if(checkRows.length == 0){
				$i.dialog.error("SYSTEM", "삭제할 경로를 선택하세요");
				return false;
			}
			if(checkRows.length> 0){
				var html = "";
				for(var i=0;i<checkRows.length;i++){
					html += "<input type='hidden' name='pathIdx' value='"+gridRows[checkRows[i]].PATH_IDX+"' />";
				}
				$("#deleteForm").append(html);
			}
			$i.dialog.confirm("SYSTEM", "삭제하시겠습니까?", function(){
				$i.remove("#deleteForm").done(function(args){
					if(args.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", args.returnMessage);
					}else{
						$i.dialog.alert("SYSTEM", args.returnMessage, function(){
							search();
							resetForm();
						});
					}
				});
			});
			
		}
		function useYnCombo(){
			var dat  = [
				{value:'Y',text:'사용'},
				{value:'N',text:'미사용'} 

			];
            var source =
            {
                datatype: "json",
                datafields: [
                     { name: 'value' },
                     { name: 'text' }
                ],
                id: 'id',
				localdata:dat,
                async: false
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#cboUseYn").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "text", valueMember: "value", dropDownWidth: 110, dropDownHeight: 80, width: 110, height: 22,theme:'blueish'});
		}
		function detailPath(pathIdx){
			var detailData = $i.post("./getDashboardPathDetail", {pathIdx:pathIdx});
			detailData.done(function(data){
				if(data.returnCode == "EXCEPTION"){
					$i.dialog.error("SYSTEM", data.returnMessage);
				}else{
					if(data.returnArray.length > 0){
						$("#hiddenPathIdx").val(data.returnArray[0].PATH_IDX);
						$("#txtFullpath").val(data.returnArray[0].FULLPATH);
						$("#txtPathDesc").val(data.returnArray[0].PATH_DESC);
						$("#txtBigo").val(data.returnArray[0].BIGO);
						$("#cboUseYn").val(data.returnArray[0].USE_YN);
					}
				}
			});
		}
		function resetForm(){
			$("#hiddenPathIdx").val("");
			$("#txtFullpath").val("");
			$("#txtPathDesc").val("");
			$("#txtBigo").val("");
			$("#cboUseYn").val("Y");
		}
	</script>
</head>
<body class='blueish'>
	<div class="wrap" style="width:98%; min-width:1160px; margin:0 10px;">
		<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
			<div class="group_button f_right">                                                                                                                                                                                                                                            
				<div class="button type1 f_left">
					<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
				</div>
				<div class="button type3 f_left">
					<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();" />
				</div>
			</div>
		</div>
		<div class="container  f_left" style="width:100%; margin:10px 0;">
			<div class="content f_left" style=" width:100%; margin-right:1%;">
				<form id="saveForm" name="saveForm" action="./insert">
					<div class="grid f_left" style="width:100%; height:500px;">	
						<div id="gridDashboardPathList"> </div>
					</div>
					<div class="blueish table f_left"  style="width:100%; height:; margin:10px 0;">
						<table  width="100%" cellspacing="0" cellpadding="0" border="0">
							<thead>
								<tr>
									<th scope="col" style="width:40%; height:40px;">경로</th>
									<th scope="col" style="width:20%; height:40px;">경로명</th>
									<th scope="col" style="width:30%; height:40px;">설명</th>
									<th scope="col" style="width:10%; height:40px;">사용유무</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<div class="cell">
											<input type="hidden" id="hiddenPathIdx" name="pathIdx" />
											<input type="text" id="txtFullpath" name="fullPath" class="input type2  f_left  m_r10"  style="width:95%; margin:3px 0; "/>
										</div>
									</td>
									<td>
										<div class="cell">
											<input type="text" id="txtPathDesc" name="pathDesc" class="input type2 f_left" style="width:95%; margin:3px 0; "/>
										</div>
									</td>
									<td>
										<div class="cell t_right">
											<input type="text" id="txtBigo" name="bigo" class="input type2 f_left t_left" style="width:95%; margin:3px 0; "/>
										</div>
									</td>
									<td>
										<div class="cell t_center">
											<div class="combobox f_left"  id='cboUseYn' name="useYn" ></div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="group_button f_right">
						<div class="button type2 f_left">
							<input type="button" value="초기화" id='btnReset' width="100%" height="100%" onclick="resetForm();" />
						</div>
						<div class="button type2 f_left">
							<input type="button" value="저장" id='btnSave' width="100%" height="100%" onclick="insert();" />
						</div>
					</div>
				</form>
				<form id="deleteForm" name="deleteForm">
				</form>
			</div>
		</div>          
	</div>
</body>
</html>