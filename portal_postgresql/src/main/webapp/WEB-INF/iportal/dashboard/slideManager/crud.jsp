<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>대시보드 슬라이드 관리</title>
		
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
		<script>
			$(document).ready(function(){
				init();
				
				$('#btnSearch').jqxButton({ width: '',  theme:'blueish'});
				$("#btnInsert").jqxButton({ width: '', theme : 'blueish'});
				$('#saveGroupButton').jqxButton({ width: '',  theme:'blueish'}).on('click', function() { insertDashGrp(); });
				$('#deleteButton').jqxButton({ width: '',  theme:'blueish'}).on('click', function() { remove(); });
				$("#txtDashGrpName").jqxInput({placeHolder: "", height: 25, width: 180, minLength: 1});
				$('#newGroupButton').jqxButton({ width: '',  theme:'blueish'}).on('click', function() { dashNmReset(); });
				
				
				//서버 갔다와서
				//결과값을
		/* 		var task = $i.post("./getSlideMaster");
				task.done(function(result){ 
					
					makeJqxGrid(result.returnObject.VALUE);
				}); */

				
				
			});
			function init(){
				var task = $i.post("./getSlideMaster");
				task.done(function(result){ 
					
					makeJqxGrid(result.returnObject.VALUE);
				});
			}
			function searchStart(){
				$("#txtDashGrpName").val("");
				
				
				
				
				if($("#hiddenDashGrpID").val() == null){
					init();
				}
				$i.post("./getRowSelectView","#dash_grp_id"); 
			}
			function search(){	
				
			}
			function insert(){ //저장버튼 눌러서 실행
				if($("[name=sortOrd]").filter(function(){return this.value==""; }).length!=0){
		 			alert("순번이 모두 입력되지 않았습니다.");
		 			return;
		 		}
		 		if($("[name=pathIDX]").filter(function(){return this.value==""; }).length!=0){
		 			alert("경로가 모두 입력되지 않았습니다.");
		 			return;
		 		}
		 		if($("[name=interval]").filter(function(){return this.value==""; }).length!=0){
		 			alert("간격이 모두 입력되지 않았습니다.");
		 			return;
		 		}
				if(confirm("저장하시겠습니까?")){
					$i.post("./deleteSlideMaster",{DASH_GRP_ID:$("#hiddenDashGrpID").val()},function(res){
						var dashGrpID = $("#hiddenDashGrpID").val();
						var arraySlideIDX = [];
						var arrayPathIDX = [];
						var arrayInterVal = [];
						var arrayEftType = [];
						var arrayApplyMonth = [];
						var arraySortOrd = [];
		 				for(var i = 0; i<$("tr[data-sidx]").length;i++){
			 				 var item = $($("tr[data-sidx]")[i]);
			 				 arraySlideIDX.push(i);
			 				 arrayPathIDX.push(item.find("[name='pathIDX']").val());
			 				 arrayInterVal.push(item.find("[name='interval']").val());
			 				 arrayEftType.push(item.find("[name='eftType']").val());
			 				 var applyMonth = "";
			 				 var checkboxes = item.find("[name=apply_month]:checked");
			 				 $.each(checkboxes,function(idx,cb){
			 					 applyMonth+=cb.value;
			 					 if(idx!=checkboxes.length-1) applyMonth+=",";
			 				 });
			 				 arrayApplyMonth.push(applyMonth);
			 				 arraySortOrd.push(item.find("[name='sortOrd']").val());
			 			 }
		 				$i.post("./insertSlideMaster",{dash_grp_id:dashGrpID,slideIDX:arraySlideIDX, pathIDX:arrayPathIDX, interval:arrayInterVal,
		 												eftType:arrayEftType, applyMonthValue:arrayApplyMonth, sortOrd:arraySortOrd},{callback:function(res){
		 					if(res == 0){
		 						alert("저장되었습니다.");
				 				getSlideData("");		
		 					}else{
		 						alert("오류가 발생했습니다");
		 					}
		 				}});
		 			 });
		 		 } 
				 for(var i=0;i<$("tr[data-sidx]").length;i++){
					var item = $($("tr[data-sidx]")[i]); //tr태그에 data-sidx의 갯수
					var applyMonth = "";
					var checkboxes = item.find("[name=apply_month]:checked");
	 				 $.each(checkboxes,function(idx,cb){
	 					 applyMonth+=cb.value;
	 					 if(idx!=checkboxes.length-1) 
	 						 applyMonth+=",";
	 				 });
	 				 /* item.find("[name='applyMonthValue']").val(applyMonth); */
				} 
					 item.find("[name='applyMonthValue']").val(applyMonth);
				/* if($("#hiddenDashGrpName").val() == $("#txtDashGrpName").val()){
					alert("이름동일"); }*/
				
					/* if($("#slidelist").v al()!=0){ */
						
					
				$i.insert("#slideSaveForm").done(function(args){ 
					//#savaForm 내용 action지정한 경로로 전송 
					if(args.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", args.returnMessage);
					}else{
						$i.dialog.alert("SYSTEM", args.returnMessage, function(){
							search();
						});
					}
				});
				/* 	}else{
					alert("변경된것이 없습니다.");
					}
					 */
					/* getSlideData(""); */
					
				/* $i.insert("#hiddenDashGrpID").done(function(args){  
					if(args.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", args.returnMessage);
					}else{
						$i.dialog.alert("SYSTEM", args.returnMessage, function(){
							search();
						});
					}
				}); */
			}
			function makeJqxGrid(dataSource){
		 		var subJectId="";

				var source = {
						datatype:"json",
						localdata:dataSource
				};
				if($("#gridDashGrp").html()!=""){
					$("#gridDashGrp").jqxGrid({
						source:source
					});
				}
				if($("#gridDashGrp").html()==""){
					var detail = function(row,datafield,value,defaultHtml, protperty, rowdata){
						var kpiID = rowdata.KPI_ID;
						var link="<a href=\"javascript:getKpiDetail('"+kpiId+"');\">" + value + "</a>";
						
						 return "<div style='text-align: left; padding-bottom: 2px; margin-top: 5px;margin-left:10px;'>" + link +"</div>";
					};
					$("#gridDashGrp").jqxGrid({
						width:'100%',
						height:'100%',
						source:source,
						theme:"blueish",
						pageable:false,
						altrows:true,
// 						editable:true,
						columnsresize:true,
						columns:[
							{text:'그룹명',cellsalign:'center',align:'center',datafield:'dash_grp_nm',width:"100%"}
						]
					});
				}
				$('#gridDashGrp').on('rowselect', function (event){
					
	            	var args = event.args;
				    var rowBoundIndex = args.rowindex;
				    var rowData = args.row;
					
				    var dashGrpID = rowData.dash_grp_id;
				  
				    /* $("#hiddenAcct_id").val(rowData.acct_id); */
				    $("#hiddenDashGrpID").val(rowData.dash_grp_id);
				    $("#hiddenDashGrpName").val(rowData.dash_grp_nm);
				   
				    $("#txtDashGrpID").val(rowData.dash_grp_id);
				    $("#txtDashGrpName").val(rowData.dash_grp_nm);
				    
				    
				   
				    
				    $("#dash_grp_id").val(rowData.dash_grp_id);
				  
				    $("#showDashLabel").html(rowData.dash_grp_nm);
				   
				    
				    
				   /*  $("#showDashLabel").html(rowData.dash_grp_nm); */
				    	 
				    //클릭시 오른쪽 화면 나와야함
				   getSlideData(dashGrpID);
				  
				   /* addrow(); */
				   
				  
				    /* $("slidelist").html(rowData.mainlist); */
				    
				   
				});
		 	}
			
			function getSlideData(dashGrpID){
				if(dashGrpID == ""){
		 			dashGrpID = $("#hiddenDashGrpID").val();
		 		}
				//alert("dashGrpID");
				//res = datasource
				//result를 뿌려줘야한다
				$i.post("./getRowSelectView",{dash_grp_id:dashGrpID}).done(function(result){ //select
					result = result.returnObject.result;
					
				$("#slidelist > tr").remove();
				 	
					
					var data="";
					for(var i=0;i<result.length;i++){
						data+="<tr data-sidx='"+result[i].SLIDE_IDX+"'>";
		 				data+="<td style='width:10%;'>";
		 				data+="<div class='cell'>";
		 				data+="<span name='path_desc' data-sidx='"+result[i].SLIDE_IDX+"'>"+result[i].PATH_DESC+"</span>";
		 				data+="</div>";
		 				data+="</td>";
		 				data+="<td style='width:20%;'>";
		 				data+="<div class='cell'>";
		 				data+="<input name='pathIDX' type='hidden' data-sidx='"+result[i].SLIDE_IDX+"' value='"+result[i].PATH_IDX+"' />";
		 				data+="<span class='f_left' name='fullpath' data-sidx='"+result[i].SLIDE_IDX+"'>"+result[i].FULLPATH+"</span>";
		 				data+="<div class='icon-search f_left m_l5 pointer' onclick=\"pathpopup('"+result[i].SLIDE_IDX+"');\" />";
		 				data+="</div>";
		 				data+="</td>";
		 				data+="<td style='width:10%;'>";
		 				data+="<div class='cell'>";
		 				data+="<input class='input type1 f_left' style='width:70%; margin:2px 0;' name='sortOrd' onkeypress='onlyNum(event);' data-sidx='"+result[i].SLIDE_IDX+"' value='"+result[i].SORT_ORD+"'>";
		 				data+="</div>";
		 				data+="</td>";
		 				data+="<td style='width:10%;'>";
		 				data+="<div class='cell'>";
		 				data+="<input class='input type2 f_left m_r3' style='width:50%; margin:2px 0;' name='interval' onkeypress='onlyNum(event);' data-sidx='"+result[i].SLIDE_IDX+"' value='"+result[i].INTERVAL+"'/><span style='line-height:29px; font-size: 11px !important; vertical-align: bottom;'> sec.</span>";
		 				data+="</div>";
		 				data+="</td>";
		 				data+="<td style='width:10%;'>";
		 				data+="<div class='cell t_center'>";
		 				data+="<div class='combobox'>";
		 				data+="<select name='eftType'><option>fade</option></select>";
		 				data+="</div>";
		 				data+="</div>";
		 				data+="</td>";
		 				data+="<td style='width:30%;'>";
		 				data+="<div class='cell t_center'>";
		 				data+="<div class='group f_left m_t3 m_b3'>";
		 				data+="<div class='group f_left m_r3'>";
		 				data+="<input name='apply_month' data-sidx='"+result[i].SLIDE_IDX+"' type='checkbox' value='01' class='f_left m_r3 m_t2'/>01";
		 				data+="</div>";
		 				data+="<div class='group f_left m_r3'>";
		 				data+="<input name='apply_month' data-sidx='"+result[i].SLIDE_IDX+"' type='checkbox' value='02' class='f_left m_r3 m_t2'/>02";
		 				data+="</div>";
		 				data+="<div class='group f_left m_r3'>";
		 				data+="<input name='apply_month' data-sidx='"+result[i].SLIDE_IDX+"' type='checkbox' value='03' class='f_left m_r3 m_t2'/>03";
		 				data+="</div>";
		 				data+="<div class='group f_left m_r3'>";
		 				data+="<input name='apply_month' data-sidx='"+result[i].SLIDE_IDX+"' type='checkbox' value='04' class='f_left m_r3 m_t2'/>04";
		 				data+="</div>";
		 				data+="<div class='group f_left m_r3'>";
		 				data+="<input name='apply_month' data-sidx='"+result[i].SLIDE_IDX+"' type='checkbox' value='05' class='f_left m_r3 m_t2'/>05";
		 				data+="</div>";
		 				data+="<div class='group f_left m_r3'>";
		 				data+="<input name='apply_month' data-sidx='"+result[i].SLIDE_IDX+"' type='checkbox' value='06' class='f_left m_r3 m_t2'/>06";
		 				data+="</div>";
		 				data+="</div>";
		 				data+="<div class='group f_left m_t3 m_b3'>";
		 				data+="<div class='group f_left m_r3'>";
		 				data+="<input name='apply_month' data-sidx='"+result[i].SLIDE_IDX+"' type='checkbox' value='07' class='f_left m_r3 m_t2'/>07";
		 				data+="</div>";
		 				data+="<div class='group f_left m_r3'>";
		 				data+="<input name='apply_month' data-sidx='"+result[i].SLIDE_IDX+"' type='checkbox' value='08' class='f_left m_r3 m_t2'/>08";
		 				data+="</div>";
		 				data+="<div class='group f_left m_r3'>";
		 				data+="<input name='apply_month' data-sidx='"+result[i].SLIDE_IDX+"' type='checkbox' value='09' class='f_left m_r3 m_t2'/>09";
		 				data+="</div>";
		 				data+="<div class='group f_left m_r3'>";
		 				data+="<input name='apply_month' data-sidx='"+result[i].SLIDE_IDX+"' type='checkbox' value='10' class='f_left m_r3 m_t2'/>10";
		 				data+="</div>";
		 				data+="<div class='group f_left m_r3'>";
		 				data+="<input name='apply_month' data-sidx='"+result[i].SLIDE_IDX+"' type='checkbox' value='11' class='f_left m_r3 m_t2'/>11";
		 				data+="</div>";
		 				data+="<div class='group f_left m_r3'>";
		 				data+="<input name='apply_month' data-sidx='"+result[i].SLIDE_IDX+"' type='checkbox' value='12' class='f_left m_r3 m_t2'/>12";
		 				data+="</div>";
		 				data+="</div>";
		 				data+="<div class='group f_left m_t3 m_b3'>";
		 				data+="<div class='group f_left m_r3'>";
		 				data+="<input class='f_left m_r3 m_t2' name='all_check' data-sidx='"+result[i].SLIDE_IDX+"' onclick=\"allcheck('"+result[i].SLIDE_IDX+"');\" type='checkbox'/>전체선택";
		 				data+="<input name='applyMonthValue' data-sidx='"+result[i].SLIDE_IDX+"' type='hidden' value='"+result[i].APPLY_MONTH+"'/>";
		 				data+="</div>";
		 				data+="</div>";
		 				data+="</div>";
		 				data+="</td>";
		 				data+="<td style='width:10%;'>";
		 				data+="<div class='cell t_center'>";
		 				data+="<div class='pointer'>";
		 				data+="<img src='../../resources/cmresource/image/icon-minus.png' alt='삭제' style='cursor:pointer;' onclick=\"removerow('"+result[i].SLIDE_IDX+"');\" >";
						data+="</div>";
		 				data+="</div>";
		 				data+="</td>";
		 				data+="</tr>";
					}
					$("#slidelist").append(data);
					/* for(var i=0;i<12;i++){
				
					$('input[name=apply_month]').val(i)
					} */
					
					for(var j=0;j<result.length;j++){
		 				var monthes = result[j].APPLY_MONTH.split(',');
		 				$.each(monthes,function(idx,item){
							$("input[data-sidx="+result[j].SLIDE_IDX+"][name=apply_month][value="+item+"]").attr("checked","checked");
						});
		 			} 
					
				});
				
			}
			
			
			function insertDashGrp(){
				
				
				if($("#txtDashGrpName").val().trim() == ""){
					$i.dialog.error("SYSTEM","Data명을 입력하세요");
				}else{
					
						
 					$i.post("./insertDashInfo","#dashInfoSaveForm").done(function(data){
 						
						if(data.returnCode == "EXCEPTION"){//??????????????????
							$i.dialog.error("SYSTEM", data.returnMessage);
						}else{
							$i.dialog.alert("SYSTEM", data.returnMessage, function(){
							
								
								var task = $i.post("./getSlideMaster");
								task.done(function(result){ 
									
									makeJqxGrid(result.returnObject.VALUE);
								});
								dashNmReset();
							});
						}
					});	
 					
				}
			}
			
					$("#hiddenRoleNameOrg").val($("#hiddenAddRoleName").val());
					$i.post("./insertRoleInfo","#roleInfoSaveForm").done(function(data){
						if(data.returnCode == "EXCEPTION"){
							$i.dialog.error("SYSTEM", data.returnMessage);
						}else{
							$i.dialog.alert("SYSTEM", data.returnMessage, function(){
								makeRoleData(); 
								roleNameReset(); 
							});
						}
					});	
				
			
					
			function remove(){
				
				$i.dialog.confirm("SYSTEM", "삭제하시겠습니까?",function(){
					/* $i.remove("#dashInfoSaveForm").done(function(args){
						if(args.returnCode == "EXCEPTION"){
							$i.dialog.error("SYSTEM", args.returnMessage);
						}else{
							$i.dialog.alert("SYSTEM", args.returnMessage, function(){
								$("#txtDashGrpName").val("");
								
							});
						}
					}); */
					$i.post("./remove","#dashInfoSaveForm").done(function(data){
						
						if(data.returnCode == "EXCEPTION"){//??????????????????
							$i.dialog.error("SYSTEM", data.returnMessage);
						}else{
							$i.dialog.alert("SYSTEM", data.returnMessage, function(){
							
								
								var task = $i.post("./getSlideMaster");
								task.done(function(result){ 
									
									makeJqxGrid(result.returnObject.VALUE);
								});
								dashNmReset();
								
							});
						}
					});	
				});
				
			}
			
			function dashNmReset(){
				$("#dash_grp_id").val("");
				$("#hiddenDashGrpID").val("");
				$("#hiddenDashGrpName").val("");
				$("#txtDashGrpName").val("");
				$("#txtDashGrpID").val("");
				$('#gridDashGrp').jqxGrid("clearselection");
			}
			function pathpopup(sidx){
		 		window.open("./popupMappingPath?sidx="+sidx,'_blank','width=800,height=500');
		 	}
			function removerow(sidx){
		 		$("tr[data-sidx="+sidx+"]").remove();	//tr에서 삭제됨
		 		 
		 	}
		 	function allcheck(sidx){
		 		if($("input[name=all_check][data-sidx="+sidx+"]").is(":checked")){
		 			//allcheck라는 이름중 data-sidx의 변수명이 받아온+sidx+인 것에 checked가 checked이면
		 			$("input[name=apply_month][data-sidx="+sidx+"]").prop("checked","checked");
		 			//applyMonth라는 이름중 data-sidx의 변수명이 받아온 +sidx+인것의 checked속성에 checked를 넣는다.
		 			//prop는 attr과 비슷
		 		}
		 		else{
		 			$("input[name=apply_month][data-sidx="+sidx+"]").removeAttr("checked");
		 			
		 		}
		 	}
		 	function addrow(){
		 		var nsidx = ($("tr[data-sidx]").length==0)?1:Enumerable.From($("tr[data-sidx]")).Max(function(c){ return $(c).data("sidx"); })+1;
		 		var tr="";
		 		tr+="<tr data-sidx='"+nsidx+"' class='tr_hover "+(nsidx%2==0?"":"odd")+"'>";
		 		tr+="<td style='width:10%;'>";
		 		tr+="<div class='cell t_center'>";
		 		tr+="<span name='path_desc' data-sidx='"+nsidx+"'></span>";
		 		tr+="</div>";
		 		tr+="</td>";
		 		
		 		tr+="<td style='width:20%;'>";
		 		tr+="<div class='cell t_center'>";
		 		tr+="<span name='fullpath' data-sidx='"+nsidx+"'></span>";
		 		tr+="<input name='pathIDX' type='hidden' data-sidx='"+nsidx+"' />";
		 		tr+="<img src='../../resources/cmresource/image/icon-search.png' style='cursor:pointer;' alt='경로검색'  onclick=\"pathpopup('"+nsidx+"');\" />";
		 		tr+="</div>";
		 		tr+="</td>";
		 		
		 		tr+="<td style='width:10%'>";
		 		tr+="<div class='cell t_center'>";
		 		tr+="<input name='sortOrd' class='input type2 f_left' style='width:50%; height:14px;' onkeypress='onlyNum(event);' data-sidx='"+nsidx+"'  />";
		 		tr+="</div>";
		 		tr+="</td>";
		 		
		 		tr+="<td style='width:10%'>";
		 		tr+="<div class='cell t_center'>";
		 		tr+="<input name='interval' class='input type2 f_left' style='width:50%; height:14px;' onkeypress='onlyNum(event)'; data-sidx='"+nsidx+"'  />";
		 		tr+="<span style='font-size:11px !important; vertical-align:bottom;'> sec.</span>";
		 		tr+="</div>";
		 		tr+="</td>";
		 		
		 		tr+="<td style='width:10%;'>";
		 		tr+="<div class='cell t_center'>";
		 		tr+="<select name='eftType' data-sidx='"+nsidx+"'>";
		 		tr+="<option value='fade'>fade</option>";
		 		tr+="</select>";
		 		tr+="</div>";
		 		tr+="</td>";
		 		
		 		tr+="<td style='width:30%;'>";
		 		tr+="<div class='cell t_center'>";
		 		tr+="		<p style='margin-top:5px;margin-bottom:5px;'>";
				tr+="			<input name = 'apply_month' data-sidx='"+nsidx+"' type='checkbox' value='01'/>01";
				tr+="			<input name = 'apply_month' data-sidx='"+nsidx+"' type='checkbox' value='02'/>02";
				tr+="			<input name = 'apply_month' data-sidx='"+nsidx+"' type='checkbox' value='03'/>03";
				tr+="			<input name = 'apply_month' data-sidx='"+nsidx+"' type='checkbox' value='04'/>04";
				tr+="			<input name = 'apply_month' data-sidx='"+nsidx+"' type='checkbox' value='05'/>05";
				tr+="			<input name = 'apply_month' data-sidx='"+nsidx+"' type='checkbox' value='06'/>06";
				tr+="		</p>";
				tr+="		<p style='margin-top:5px;margin-bottom:5px;'>";
				tr+="			<input name = 'apply_month' data-sidx='"+nsidx+"' type='checkbox' value='07'/>07";
				tr+="			<input name = 'apply_month' data-sidx='"+nsidx+"' type='checkbox' value='08'/>08";
				tr+="			<input name = 'apply_month' data-sidx='"+nsidx+"' type='checkbox' value='09'/>09";
				tr+="			<input name = 'apply_month' data-sidx='"+nsidx+"' type='checkbox' value='10'/>10";
				tr+="			<input name = 'apply_month' data-sidx='"+nsidx+"' type='checkbox' value='11'/>11";
				tr+="			<input name = 'apply_month' data-sidx='"+nsidx+"' type='checkbox' value='12'/>12";
				tr+="		</p>";
				tr+="		<p style='margin-top:5px;margin-bottom:5px;'>";
				tr+="			<input name = 'all_check' data-sidx='"+nsidx+"' onclick=\"allcheck('"+nsidx+"');\" type='checkbox'/>전체선택";
				tr+="			<input name = 'applyMonthValue' data-sidx='"+nsidx+"' type='hidden' />";
				tr+="		</p>";
				tr+="</div>";
				tr+="</td>";
				tr+="<td style='width:10%;'>";
				tr+="<div class='cell t_center'>";
				tr+="<img src='../../resources/cmresource/image/icon-minus.png' alt='삭제' style='cursor:pointer;' onclick=\"removerow('"+nsidx+"');\" >";
				tr+="</div>";
				tr+="</td>";
				tr+="</tr>";
				
		 		$("#slidelist").append(tr);
		 		$("input").css('imeMode','disabled');
		 	}
		</script>
	</head>
	<body class="blueish">
	
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 10px;">
				
				<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
					<!-- <div class="label type1 f_left">구분 : </div> -->
					<div class="combobox f_left"  id="cboSearchGubn" name="searchGubn"></div>
			<form id="saveForm" name="saveForm" action="./crud" method="get" class="padding_top" >
				
				<input type="hidden" id="hiddenDashGrpID" name="dashGrpID" />
				<input type="hidden" id="hiddenDashGrpName" name="dashGrpName" />
					<div class="group_button f_right">                                                                                                                                                                                                                                            
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="searchStart();" />
						</div>
						<div class="button type1 f_left">
							<input type="button" value="저장" id="btnInsert" width="100%" height="100%" onclick="insert();" />
						</div>
					</div>
			</form>
				</div>
			<div class="container  f_left" style="width:100%; margin:5px 0;"> 
				<div class="content f_left" style="width:24%; margin-right:1%; background:#ffffff; border:1px solid #b5caca; padding:10px 0;">
					<div class="grid f_left" style="width:92%; height:480px; margin:10px 4%;">
						<div id="gridDashGrp"></div>
					</div>
					<div class="f_left group  w90p" style="margin:10px 5%;">
						<div class="label type2 f_left">그룹</div>
						<form id="dashInfoSaveForm" name="dashInfoSaveForm" action="./insertDashInfo">
							
							<input type="hidden" id="txtDashGrpID" name="txtDashGrpID" />
							<input type="text" id="txtDashGrpName" name="txtDashGrpName" class="input type1  f_left"  style="width:200px; margin:3px 0px 3px 5px; "/>
						</form>
						
					</div>
					<div class="f_left group" style="margin:0; margin-left:40px;">
						<div class="button type2 f_left">
							<input type="button" value="신규" id='newGroupButton' width="100%" height="100%" />
						</div>
						<div class="button type2 f_left">
							<input type="button" value="저장" id='saveGroupButton' width="100%" height="100%" />
						</div>
						<div class="button type2 f_left">
							<input type="button" value="삭제" id='deleteButton' width="100%" height="100%" />
						</div>
					</div>
				</div>
				<div class="content f_right" style="width:74.5%">
					<div class="group f_left  w100p m_b5">
						<div class="label type2 f_left">DASH 그룹명<span class="label sublabel type2" id="showDashLabel"></span></div>
					</div>
					<div class="blueish datatable f_right " style="width:99.8%; height:672px; overflow:hidden;">
						<form id="slideSaveForm" name="slideSaveForm" action="./insert">
						
						<input type="hidden" id="dash_grp_id" name="dash_grp_id"/>
							<div class="datatable_fixed" style="width:100%;height:600px;">
								<div style=" height:647px !important; overflow-x:hidden;">
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<thead style="width:100%;">
											<tr>
												<th style="width:10%"><span style="color:red; font-size:11px;">＊</span>파일명</th>
												<th style="width:20%"><span style="color:red; font-size:11px;">＊</span>경로</th>
												<th style="width:10%"><span style="color:red; font-size:11px;">＊</span>순서</th>
												<th style="width:10%"><span style="color:red; font-size:11px;">＊</span>간격</th>
												<th style="width:10%"><span style="color:red; font-size:11px;">＊</span>효과</th>
												<th style="width:30%"><span style="color:red; font-size:11px;">＊</span>월지정</th>
												<th style="width:10%"><img src="../../resources/cmresource/image/icon-plus.png" alt="추가" style="cursor:pointer;" onclick="addrow();"></th>
												<th style="width:1%; min-width:17px;"></th>
											</tr>
										</thead>
										
										<tbody id="slidelist">
											<%-- <c:choose> 
												<c:when test="${mainList != null && not empty mainList}"> 
													<c:set var = "SQUOT">'</c:set>
													<c:set var = "DQUOT">"</c:set>
													<c:forEach items="${mainList}" var="rows" varStatus="loop">
													
														<tr data-sidx='${rows.SLIDE_IDX}' class='tr_hover <c:if test="${loop.count%2 == 0}"><c:out value="odd" /></c:if>'>
															<td style="width:10%;">
																<div class="cell t_center">
																	<input type="hidden" name="slideIDX" value="${rows.SLIDE_IDX}" />
																	<span name='path_desc' data-sidx='${rows.SLIDE_IDX}'>${fn:replace(fn:replace(fn:replace(fn:replace(rows.PATH_DESC,SQUOT,"&#39;"),DQUOT,"&quot;"),"<","&lt;"),">","&gt;")}</span>
																</div>
															</td>
															<td style="width:20%;">
																<div class="cell t_center">
																	<span name='fullpath' data-sidx='${rows.SLIDE_IDX}'>${fn:replace(fn:replace(fn:replace(fn:replace(rows.FULLPATH,SQUOT,"&#39;"),DQUOT,"&quot;"),"<","&lt;"),">","&gt;")}</span>
																	<input name='pathIDX' type='hidden' data-sidx='${rows.SLIDE_IDX}' value='${rows.PATH_IDX}' />
																	<img src="../../resources/cmresource/image/icon-search.png" style="cursor:pointer;" alt="경로검색"  onclick="pathpopup('${rows.SLIDE_IDX}');" />
																</div>
															</td>
															<td style="width:10%;">
																<div class="cell t_center">
																	<input name='sortOrd' class="input type2 f_left" style="width:50%; height:14px;" onkeypress='onlyNum(event);' data-sidx='${rows.SLIDE_IDX}' value="${rows.SORT_ORD}">
																</div>
															</td>
															<td style="width:10%;">
																<div class="cell t_center">
																	<input name='interval' class="input type2 f_left" style="width:50%;  height:14px;" onkeypress='onlyNum(event);' data-sidx='${rows.SLIDE_IDX}' value="${rows.INTERVAL}"/> 
																	<span style="font-size: 11px !important; vertical-align: bottom;"> sec.</span>
																</div>
															</td>
															<td style="width:10%;">
																<div class="cell t_center">
																	<select name=eftType><option>fade</option></select>
																</div>
															</td>
															<td style="width:30%;">
																<div class="cell t_center">
																	<p style='margin-top:5px;margin-bottom:5px;'>
																		<input name = 'applyMonth' data-sidx='${rows.SLIDE_IDX}' type='checkbox' value='01'/>01
																		<input name = 'applyMonth' data-sidx='${rows.SLIDE_IDX}' type='checkbox' value='02'/>02
																		<input name = 'applyMonth' data-sidx='${rows.SLIDE_IDX}' type='checkbox' value='03'/>03
																		<input name = 'applyMonth' data-sidx='${rows.SLIDE_IDX}' type='checkbox' value='04'/>04
																		<input name = 'applyMonth' data-sidx='${rows.SLIDE_IDX}' type='checkbox' value='05'/>05
																		<input name = 'applyMonth' data-sidx='${rows.SLIDE_IDX}' type='checkbox' value='06'/>06
																	</p>
																	<p style='margin-top:5px;margin-bottom:5px;'>
																		<input name = 'applyMonth' data-sidx='${rows.SLIDE_IDX}' type='checkbox' value='07'/>07
																		<input name = 'applyMonth' data-sidx='${rows.SLIDE_IDX}' type='checkbox' value='08'/>08
																		<input name = 'applyMonth' data-sidx='${rows.SLIDE_IDX}' type='checkbox' value='09'/>09
																		<input name = 'applyMonth' data-sidx='${rows.SLIDE_IDX}' type='checkbox' value='10'/>10
																		<input name = 'applyMonth' data-sidx='${rows.SLIDE_IDX}' type='checkbox' value='11'/>11
																		<input name = 'applyMonth' data-sidx='${rows.SLIDE_IDX}' type='checkbox' value='12'/>12
																	</p>	
																	<p style='margin-top:5px;margin-bottom:5px;'>
																		<input name = 'allCheck' data-sidx='${rows.SLIDE_IDX}' onclick="allcheck('${rows.SLIDE_IDX}');" type='checkbox'/>전체선택
																		<input name = 'applyMonthString' data-sidx='${rows.SLIDE_IDX}' type='hidden' value='${rows.APPLY_MONTH}'/>
																		<input name = "applyMonthValue" data-sidx="${rows.SLIDE_IDX}" type="hidden" value="${rows.APPLY_MONTH }" />
																	</p>
																	<script>
																		var monthes = '${rows.APPLY_MONTH}'.split(',');
																		$.each(monthes,function(idx,item){
																			$("input[data-sidx=${rows.SLIDE_IDX}][name=applyMonth][value="+item+"]").attr("checked","checked");
																		});
																	</script>
																</div>
															</td>   
															<td style="width:10%;">
																<div class="cell t_center">
																	<img src="../../resources/cmresource/image/icon-minus.png" alt="삭제" style="cursor:pointer;" onclick="removerow('${rows.SLIDE_IDX}');" >
																</div>
															</td>
														</tr>
													</c:forEach>
												</c:when>
											</c:choose> --%>
										</tbody>
									</table>
								</div>
							</div>
						</form>
						
					</div>
				</div>
			</div>
		</div>
	</body>
</html>