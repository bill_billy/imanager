<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html >
<html>
  <head>
  		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
		<link rel="stylesheet" href="../../resources/cmresource/css/thirdparty/MIT/animate.css/animate.css"/>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		
		
		<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
		<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
  
	
  <link rel="stylesheet" href="/resources/demos/style.css">
		<script>
		
			$.fn.extend({
				animateCss: function (animationName) {
					var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
					this.addClass('animated ' + animationName).one(animationEnd, function() {
						$(this).removeClass('animated ' + animationName);
					});
				}
			});     
			var cnt = 0;
			var slideon=true;
			var timer = null;
			var source = null;
			$(document).ready(function(){
				$(window).resize(function(){
					var popupSize = window.innerHeight;
					if(popupSize > 580){
						$("#remote")[0].style.top = "98.5%";
						$("#remote")[0].style.height = "10%";
					}else if(popupSize < 580 && popupSize > 365){
						$("#remote")[0].style.top = "98.5%";
						$("#remote")[0].style.height = "15%";
					}else if(popupSize < 365 && popupSize > 265){
						$("#remote")[0].style.top = "98.5%";
						$("#remote")[0].style.height = "20%";
					}else if(popupSize < 265 && popupSize > 200){
						$("#remote")[0].style.top = "98.5%";
						$("#remote")[0].style.height = "25%";
					}else if(popupSize < 200 && popupSize > 170){
						$("#remote")[0].style.top = "98.5%";
						$("#remote")[0].style.height = "30%";
					}else if(popupSize < 170 ){
						$("#remote")[0].style.top = "98.5%";
						$("#remote")[0].style.height = "35%";
					}
				});
				$("#div_pause").hide();
				var data = $i.post("./getDashboardSlidePopupData", {dash_grp_id:"<%=request.getParameter("dash_grp_id")%>"});
				data.done(function(res){
					source = res.returnArray;
					for(var i=0;i<source.length;i++){
			
							if(source.length!=0){
								$("#item").attr("src",source[0].FULLPATH);	
								cnt++;
								$("#item").load(function(){
									$("#body_div").animateCss("fadeIn");
									timer = setTimeout(function(){
										$("#body_div").animateCss("fadeOut");								
										setTimeout(function(){
											/* if(cnt>=source.length) cnt=0; */
											$("#item").attr("src",source[cnt].FULLPATH);
											cnt++;
											if(cnt>=source.length) cnt=0;
										},250);
									},(cnt==0?parseInt(source[source.length-1].INTERVAL):parseInt(source[cnt-1].INTERVAL))*1000);
								});
							}
			
					}
				});
				$("#remote").on("mouseenter",function(){
					$("#remote").animate({top:"-=50px",opacity:0.8},"slow");
					//$("#remote").animate({opacity:0.8},"slow");
				});
				$("#remote").on("mouseleave",function(){
					$("#remote").animate({top:"+=50px",opacity:0.3},"slow");
					//$("#remote").animate({opacity:0.3},"slow");
				});
				$(document).on("keypress",function(event){
					if(event.charCode=="32")
						slidetoggle();
				});
			});
			function slidetoggle(){
				
				if(slideon){
					$("#remote_atag").css("background","url(../../resources/css/images/showButton/showbutton_play.png) no-repeat");
					clearTimeout(timer);
					slideon = false;
					$("#div_pause").show();
					
				}	
				else{ 	
					$("#remote_atag").css("background","url(../../resources/css/images/showButton/showbutton_stop.png) no-repeat");
					slideon = true;
					$("#div_pause").hide();
					timer = setTimeout(function(){
						 
						//$("#body_div").fadeOut();
						$("#body_div").animateCss("fadeOut");
						setTimeout(function(){
							
							$("#item").attr("src",source[cnt].FULLPATH);
							cnt++;
							if(cnt>=source.length) cnt=0;
						},250);
					}
					,(cnt==0?parseInt(source[source.length-1].INTERVAL):parseInt(source[cnt-1].INTERVAL))*1000);
				}
			}

			function runEffect(){
				 
			}
			
		</script>
		<style type="text/css">
		html,body {
			weight:100%,
			height: 100%
		}
		</style>
  </head>
  <body style="padding:0px;margin:0;overflow-y:hidden;overflow-x:hidden;">


	


  <div id="body_div" style="border-width:0px;width:100%;height:1080px;overflow-y:hidden;overflow-x:hidden;">
  <iframe id="item" style="border-width:0px;width:100%;height:1080px;overflow-y:hidden;overflow-x:hidden;">
  </iframe>
 </div>
 
 	<div id="div_pause" style="position:absolute;top:10px;left:10px;color:#FF0000;">
 		일시정지
 	</div>

<div  style="position:absolute;top:98.5%;height:10%;background-color:#000000;opacity:0.3;width:100%" id="remote">
	<div  style="position:relative;top:15%;left:50%;">
		<a  class="btn_Play" onclick="slidetoggle();" id="remote_atag"
			style="cursor:pointer; position:relative; display:inline-block; width:41px;  height:40px; background:url(../../resources/css/images/showButton/showbutton_stop.png) no-repeat;"></a>
	
		
	</div>
</div>
  </body>
 </html>