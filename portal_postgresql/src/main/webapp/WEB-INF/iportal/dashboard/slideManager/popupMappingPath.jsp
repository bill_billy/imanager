<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>대시보드 경로</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <script>
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$("#btnConfirm").jqxButton({ width : '', theme : 'blueish' });
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		search();
        	}
        	//조회
        	function search(){
        		var gridData = $i.post("./getDashboardSlidePathList",{});
				gridData.done(function(res){
					var source =
					{
						datatype: "json",
						datafields: [
							{ name: 'PATH_IDX', type: 'string' },
							{ name: 'FULLPATH', type: 'string' },
							{ name: 'PATH_DESC', type: 'string' },
							{ name: 'USE_YN', type: 'string' },
							{ name: 'BIGO', type: 'string'}
						],
						localdata: res.returnArray
					};
					
					var dataAdapter = new $.jqx.dataAdapter(source);
					
// 					//columns
// 					var nameCellsrenderer = function(row, datafieId, value, defaultHtml, property, rowdata) {
// 						var univID = rowdata.UNIV_ID;
// 						var univName = rowdata.UNIV_NM;
// 						var link = "<a href=\"javaScript:setData('"+univID+"', '"+univName+"');\" style='color: #000000;'>" + value + "</a>";
					
// 						return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px;'>" + link + "</div>";
// 					};
					var noScript = function (row, columnfield, value) {//left정렬
		            	var newValue = $i.secure.scriptToText(value);
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
					}; 
					$('#gridDashboardPath').jqxGrid({
						width: '100%',
						height: '100%',
						altrows:true,
						pageable: true,
						source: dataAdapter,
						theme:'blueish',
						columnsheight:28,
						rowsheight: 28,
						columnsresize: true,
						columns: [
							{ text: '경로', datafield: 'FULLPATH', width:'40%',align:'center', cellsalign: 'center' ,cellsrenderer:noScript},
							{ text: '경로명', datafield: 'PATH_DESC', width: '20%', align:'center', cellsalign: 'center' ,cellsrenderer:noScript},
							{ text: '설명', datafield: 'BIGO', width: '30%', align:'center', cellsalign: 'center' ,cellsrenderer:noScript},
							{ text: '사용여부', datafield: 'USE_YN', width: '10%', align:'center', cellsalign: 'center'}
						]
					});
					//pager width
					$('#gridpagerlist' + $('#gridDashboardPath').attr('id')).width('44px');
					$('#dropdownlistContentgridpagerlist' + $('#gridDashboardPath').attr('id')).width('19px');
				});
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function dataConfirm(){
        		var row = $("#gridDashboardPath").jqxGrid("selectedrowindex");
        		var rowdata = $("#gridDashboardPath").jqxGrid("getrows")[row];
        		
        		var idx = rowdata.PATH_IDX;
        		var path = rowdata.FULLPATH;
        		var pathDesc = rowdata.PATH_DESC;
        		var bigo = rowdata.BIGO;
        		var useYn = rowdata.USE_YN;
        		
        		var pSlideID = "${param.sidx}";
        		$("input[name=pathIDX][data-sidx="+pSlideID+"]",opener.document).val(idx);
		 		$("span[name=fullpath][data-sidx="+pSlideID+"]",opener.document).html($i.secure.scriptToText(path));
		 		$("span[name=path_desc][data-sidx="+pSlideID+"]",opener.document).html($i.secure.scriptToText(pathDesc));
		 		self.close();
        	}
        </script>
    </head>
    <body class='blueish'>
    	<div class="wrap" style="width:96%; margin:0 auto">
    		<div class="header f_left" style="width:99%; height:97% !important; margin:20px 5px 5px !important;">
    			<div class="label type1 f_left">대시보드 경로</div>
    			<div class="group_button f_right">
	    			<div class="button type1 f_left">
	    				<input type="button" value="확정" id="btnConfirm" width="100%" height="100%" onclick="dataConfirm();" />
	    			</div>
	    		</div> 
    		</div>
    		<div class="container f_left" style="width:100%; margin:10px 0;">
    			<div class="content f_left" style="width:100%; margin-right:1%;">
    				<div class="grid f_left" style="width:99%; height:390px; margin:3px 5px 5px !important;">
    					<div id="gridDashboardPath"></div>
    				</div>
    			</div>
    		</div>
    	</div>
	</body>
</html>

