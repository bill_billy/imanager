<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>${title}</title>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		init();
	});
	function init(){
		$("#btnSearch").jqxButton({ width : '', theme : 'blueish' });
		$("#txtSearch").jqxInput({placeHolder: "", height: 23, width: 250, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return search(); } });
		search();
	}
	function search(){
		var gridData = $i.post("./getDashboardPopupUnivList",{univName:$("#txtSearch").val()});
		gridData.done(function(res){
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: 'UNIV_ID', type: 'string' },
					{ name: 'UNIV_NM', type: 'string' },
					{ name: 'UNIV_GRP', type: 'string' },
					{ name: 'UNIV_GRP1', type: 'string'}
				],
				localdata: res.returnArray
			};
			
			var dataAdapter = new $.jqx.dataAdapter(source);
			
			//columns
			var nameCellsrenderer = function(row, datafieId, value, defaultHtml, property, rowdata) {
				var univID = rowdata.UNIV_ID;
				var univName = rowdata.UNIV_NM;
				var link = "<a href=\"javaScript:setData('"+univID+"', '"+univName+"');\" style='color: #000000;'>" + value + "</a>";
			
				return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px;'>" + link + "</div>";
			};
	
			$('#gridUnivList').jqxGrid({
				width: '100%',
				height: '100%',
				altrows:true,
				pageable: true,
				source: dataAdapter,
				theme:'blueish',
				columnsheight:28,
				rowsheight: 28,
				columnsresize: true,
				columns: [
					{ text: '대학ID', datafield: 'UNIV_ID', width: '25%', align:'center', cellsalign: 'center'},
					{ text: '대학교명', datafield: 'UNIV_NM', width: '30%', align:'center', cellsalign: 'center'  , cellsrenderer:nameCellsrenderer},
					{ text: '학교설립', datafield: 'UNIV_GRP', width: '20%', align:'center', cellsalign: 'center' },
					{ text: '학교종류', datafield: 'UNIV_GPR1', width: '20%', align:'center', cellsalign: 'center' }
				]
			});
			
			//pager width
			$('#gridpagerlist' + $('#gridTableList').attr('id')).width('44px');
			$('#dropdownlistContentgridpagerlist' + $('#gridTableList').attr('id')).width('19px');
		});
	}
	function searchStart() {
	}
	function setData(univID, univName) {
		$('#hiddenUnivID',opener.document).val(univID);
		$("#labelUnivID", opener.document).html(univID);
		$("#labelUnivName", opener.document).html(univName);
		window.close();
	}
	</script>
</head>
<body class='blueish'>
	<div class="wrap" style="width:96%; margin:0 auto;">
		<div class="header f_left" style="width:99%; height:97% !important; margin:3px 5px 5px !important;">
			<div class="label type1 f_left">학교명 : </div>
			<div class="input f_left">
				<input type="text" id="txtSearch" name="searchValue" />
			</div>
			<div class="group_button f_right">
				<div class="button type1 f_left">
					<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
				</div>
			</div>
		</div>
		<div class="container  f_left" style="width:100%; margin:10px 0;">
			<div class="content f_left" style="width:100%; margin-right:1%;">
				<div class="grid f_left" style="width:99%; height:450px; margin:3px 5px 5px !important;">
					<div id="gridUnivList"></div>
				</div>
			</div>
		</div>
	</div><!--wrap-->		
</body>
</html>
