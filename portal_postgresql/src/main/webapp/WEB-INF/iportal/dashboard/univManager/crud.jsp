<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>대시보드 비교대학관리</title>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	<script type="text/javascript">
		var menudata = null;
		$(document).ready(function(){
			init();
		});
		function init(){
			$("#btnSave").jqxButton({ width: '', theme: 'blueish' });
			$("#btnDelete").jqxButton({ width: '', theme: 'blueish' });
			$("#btnReset").jqxButton({ width: '', theme: 'blueish' });
			$("#btnSearch").jqxButton({ width: '', theme: 'blueish' });
			$("#btnUnivSearch").jqxButton({ width: '', theme: 'blueish'});
			search();
		}
		function search(){
			$("#gridDashboardUnivList").jqxGrid('clearselection');
			var kpiListData = $i.post("./getDashboardUnivList", {scID:$("#hiddenScID").val()});
			kpiListData.done(function(data){
				var source =
	            {
	                datatype: "json",
	                datafields: [
	                    { name: 'UNIV_ID', type: 'string'},
	                    { name: 'UNIV_NM', type: 'string'},
	                    { name: 'UNIV_GRP1', type: 'string'},
	                    { name: 'SC_ID', type: 'string'}
	                ],
	                localdata: data.returnArray,
	                updaterow: function (rowid, rowdata, commit) {
	                    commit(true);
	                }
	            };
	            var alginCenter = function (row, columnfield, value) {//left정렬
					return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
				}; 
				var alginLeft = function (row, columnfield, value) {//left정렬
					return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
				}; 
				var alginRight = function (row, columnfield, value) {//right정렬
					return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
				};
	            var dataAdapter = new $.jqx.dataAdapter(source, {
	                downloadComplete: function (data, status, xhr) { },
	                loadComplete: function (data) { },
	                loadError: function (xhr, status, error) { }
	            });
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#gridDashboardUnivList").jqxGrid(
	            {
	              	width: '100%',
	                height: '100%',
					altrows:true,
					pageable: true,
					sortable: true,
	                source: dataAdapter,
	                pagesize: 100,
					pagesizeoptions:['100', '200', '300'],
					theme:'blueish', 
	                columnsresize: true,
	                selectionmode:'checkbox',
	                columns: [
	                   { text: '대학ID', datafield: 'UNIV_ID', width:'28%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
					   { text: '대학명', datafield: 'UNIV_NM', width:'70%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
	                ]
	            });
			});
		}
		function insert(){
			if($("#hiddenUnivID").val() == ""){
				$i.dialog.error("SYSTEM", "대학을 선택하세요");
				return false;
			}
			$i.insert("#saveForm").done(function(args){
				if(args.returnCode == "EXCEPTION"){
					$i.dialog.error("SYSTEM", args.returnMessage);
				}else{
					$i.dialog.alert("SYSTEM", args.returnMessage, function(){
						search();
						resetForm();
					});
				}
			});
		}
		function remove(){
			var checkRows = $("#gridDashboardUnivList").jqxGrid("getselectedrowindexes");
			var gridRows = $("#gridDashboardUnivList").jqxGrid("getRows");
			if(checkRows.length == 0){
				$i.dialog.error("SYSTEM", "삭제할 대학명을 선택하세요");
				return false;
			}
			if(checkRows.length> 0){
				var html = "";
				for(var i=0;i<checkRows.length;i++){
					html += "<input type='hidden' name='univID' value='"+gridRows[checkRows[i]].UNIV_ID+"' />";
				}
				$("#deleteForm").append(html);
			}
			$i.dialog.confirm("SYSTEM", "삭제하시겠습니까?", function(){
				$i.remove("#deleteForm").done(function(args){
					if(args.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", args.returnMessage);
					}else{
						$i.dialog.alert("SYSTEM", args.returnMessage, function(){
							search();
							resetForm();
							$("#deleteForm").find("[name='univID']").remove();
						});
					}
				});
			});
			
		}
		function univSearch(){
			window.open('./popupMappingDashboardUniv', 'popupMappingDashboardUniv','scrollbars=no, resizable=no, width=630, height=620');
		}
		function resetForm(){
			$("#hiddenUnivID").val("");
			$("#labelUnivID").html("");
			$("#labelUnivName").html("");
		}
	</script>
</head>
<body class='blueish'>
	<div class="wrap" style="width:98%; min-width:1160px; margin:0 10px;">
		<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
			<div class="label type1 f_left">평가구분 : ${scGubn.SC_NM}</div>
			<div class="group_button f_right">                                                                                                                                                                                                                                            
				<div class="button type1 f_left">
					<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="searchStart();" />
				</div>
				<div class="button type3 f_left">
					<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();" />
				</div>
			</div>
		</div>
		<div class="container  f_left" style="width:100%; margin:10px 0;">
			<div class="content f_left" style=" width:100%; margin-right:1%;">
				<form id="saveForm" name="saveForm" action="./insert">
					<input type="hidden" id="hiddenScID" name="scID" value="${scGubn.SC_ID}" />
					<input type="hidden" id="hiddenUnivGrp1" name="univGrp1" value="${scGubn.UNIV_GRP1}" />
					<div class="grid f_left" style="width:100%; height:500px;">	
						<div id="gridDashboardUnivList"> </div>
					</div>
					<div class="blueish table f_left"  style="width:100%; height:; margin:10px 0;">
						<table  width="100%" cellspacing="0" cellpadding="0" border="0">
							<thead>
								<tr>
									<th scope="col" style="width:40%; height:40px;">대학ID</th>
									<th scope="col" style="width:60%; height:40px;">대학명</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<div class="cell">
											<input type="hidden" id="hiddenUnivID" name="univID" class="input type2  f_left  m_r10"  style="width:95%; margin:3px 0; "/>
											<span id="labelUnivID" style="float:left; margin:5px 0 0 0 ;"></span>
										</div>
									</td>
									<td>
										<div class="cell">
											<span id="labelUnivName" style="float:left; margin:5px 0 0 0 ;"></span>
										</div>
										<div class="group_button f_right">
											<div class="button type2 f_left">
												<input type="button" value="검색" id='btnUnivSearch' width="100%" height="100%" onclick="univSearch();" />
											</div>
											<div class="button type2 f_left">
												<input type="button" value="초기화" id='btnReset' width="100%" height="100%" onclick="resetForm();" />
											</div>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="group_button f_right">
						<div class="button type2 f_left">
							<input type="button" value="저장" id='btnSave' width="100%" height="100%" onclick="insert();" />
						</div>
					</div>
				</form>
				<form id="deleteForm" name="deleteForm">
					<input type="hidden" name="scID" value="${scGubn.SC_ID}" />
				</form>
			</div>
		</div>          
	</div>
</body>
</html>