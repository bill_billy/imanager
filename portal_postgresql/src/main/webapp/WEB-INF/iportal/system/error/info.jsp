<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>시스템 페이지</title>
</head> 
<body>   
	<table style="text-align:left;font-size:12px;">  
		<tr> 
			<th colspan="2">예외적 요청이 발생 하였습니다. </th>
		</tr>
		<tr>
			<th colspan="2"><img src="../../resources/img/errpage_table_bar.png"/></th>
		</tr>
		<tr>
			<th>error code</th><td>${status_code}</td>
		</tr>
		<tr>
			<th>exception_type</th><td>${exception_type}</td>
		</tr>
		<tr>
			<th>message</th><td>${message}</td>
		</tr>
		<tr>
			<th>request_uri</th><td>${request_uri}</td>
		</tr>
		<tr>
			<th>exception</th><td>${exception}</td>
		</tr>
		<tr>
			<th>servlet_name</th><td>${servlet_name}</td>
		</tr> 
	
	</table>
</body>
</html>