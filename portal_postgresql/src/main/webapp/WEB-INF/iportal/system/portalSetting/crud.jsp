<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="java.util.Date" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<title>Portal Setting</title>
<link rel="stylesheet" href="../../resources/css/popup/setting_portal.css">
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
<script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
    <script>
    var uriContext = "${WEB.ROOT}";
    </script>
	<script>
	function setSystem(keyNames){
		var arrKeyName = keyNames.split(',');
		var arrKeyValue = new Array();
		
		for(var i = 0; i < arrKeyName.length; i++){
			arrKeyValue[i] = document.getElementById(arrKeyName[i]).value;
		}

		var keyValues = arrKeyValue.join(',');

		
		var upDate = $i.post("./update", { keyValues :$i.secure.scriptToText(keyValues), keyNames :$i.secure.scriptToText(keyNames)});
		upDate.done(function(data){
			if(data.returnMessage=='success'){
				setSystemFile('imagefrm');
			}else{
				$i.dialog.error('SYSTEM','적용 중 오류가 발생하였습니다.');
			}
		});
		
	}
 // 파일 업로드
	function setSystemFile(frmName){
		eval("document."+frmName+".target='hidden_frame'");
		eval("document."+frmName+".submit()");
		
		$("#file_img_upload").find("img").each(function(){
			var img_url = $(this).attr("src");
			$(this).attr("src", img_url+"?"+new Date().getTime());
		});
		$i.dialog.alert('SYSTEM','<spring:message code="_alert.complete._apply" javaScriptEscape="true" />',function(){
			top.document.location.reload(true);
		});
	}
 	function fileCheck(obj){
 		var file_kind = obj.value.lastIndexOf('.');
 		 var file_name = obj.value.substring(file_kind+1,obj.length);
 		 var file_type = file_name.toLowerCase();
 		 var check_file_type=new Array();
 		 check_file_type=['jpg','gif','png','jpeg','bmp'];
 		 
 		 if(check_file_type.indexOf(file_type)==-1){
 		 $i.dialog.alert('SYSTEM','이미지 파일만 선택할 수 있습니다.');
 		 var parent_Obj=obj.parentNode;
 		 var node=parent_Obj.replaceChild(obj.cloneNode(true),obj);
 		 $("#file").val("");
 		 return false;
 		 }
 	}
	//문서 로딩이 완료되면 처음 수행되는 로직
	$(document).ready(function(){
		init(); 
	}); 
	
	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
	function init(){
		$("#btnSave").jqxButton({width:'', theme:'blueish'});
		$("#system_name").jqxInput({placeHolder: "", height: 23, width: 150, minLength: 1, theme:'blueish' });
		
		var source = ["1","2","3","4","5","6","7","8","9","10"];
		$("#tab_countDiv").jqxComboBox({ selectedIndex: 0, source: source, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "name", valueMember: "value", dropDownWidth: 60, dropDownHeight: 120, width: 60, height: 22,theme:'blueish'});
		var count = "${systemConfig.tab_count}";
		$("#tab_countDiv").jqxComboBox('selectItem', count );
		$('#tab_countDiv').on('select', function (event) {
		       var args = event.args;
		       if (args) {                       
		           var item = args.item;
		           var value = item.value;
		           $("#tab_count").val(value);
		       }
		});
	}
	</script>
</head>
<body class='blueish'>
<!-- #wrap -->
<div class="wrap" style="width:1100px;  margin:0 10px;">
	<!-- #header -->
	<div class="header f_left" style="width:100%; height:27px; margin-bottom:10px;">
		<div class="label type1 f_left">포탈 환경설정</div>
		<div class="group_button f_right">
			<div class="button type1 f_left">
				<input type="button" value="저장" id='btnSave' width="100%" height="100%" onclick="setSystem('system_name,tab_count');" />
			</div>
		</div>
	</div>
	<!-- #container -->
	<div id="container f_left" style="width:100%; margin:10px 0;">
        <div class="content f_left" style="width:100%; margin:0 10px;">
        	<div id="viewGeneral">
        		<div class="wrap_set" style="height:150px;">
	        		<div class="label type2 f_left" style="margin-bottom:10px;">System</div>
					<!-- #viewSystemName -->
					<div class="body_set">
						<table class="table_set">
							<colgroup>
								<col style="width:79px"><col>
							</colgroup>
							<tr>	
								<th>
									<div class="label type4 f_left" style="width:100%;padding-left:5px;margin-left:5px;"><spring:message code="title.text.system_name" /></div>
								</th>
								<td>
						    	 <input type="text" id="system_name" class="text_box left" value="${systemConfig.system_name}" style="width:350px;" />
						    	 <script>
									var systemNm = "${systemConfig.system_name}";
									$("#system_name").val($i.secure.scriptToText(systemNm));
								</script>
						    	</td>
						    </tr>
							<tr>
								<th>
									<div class="label type4 f_left" style="width:100%;padding-left:5px;margin-left:5px;"><spring:message code="title.text.tab_count" /></div>
								</th>
								<td class="bottom10">
									<div id="tab_countDiv" style="margin-top:10px;"></div>
									<input type="hidden" name="tab_count" id="tab_count" value="${systemConfig.tab_count}"/>
						    	</td>
						    </tr>
						    </table>
					</div>
						    
				</div>
			</div>
			<div id="viewImage">
				<div class="wrap_set" style="height:150px;">
					<div style="display:none">
						<iframe id="hidden_frame" name="hidden_frame" src=""></iframe>
					</div>
					<form name="imagefrm" action="${WEB.ROOT}/system/portalSetting/file" method="post" enctype="multipart/form-data">
						<div id="file_img_upload">
							<div class="label type2 f_left"><spring:message code="portalSetting.text.main_page" /></div>
							<!-- body_set -->
							<div class="body_set" id="mainPageImage">
								<table class="table_set">
									<tr>
										<td width="50px" rowspan="4" class="bg_white border_thin">
											<img src="../../resources/img/n/logo_sch.gif" style="height:28px;width:100px;">
										</td>
										<td class="text_left">
								    		<div class="label type4 f_left" style="width:80px;margin-top:15px;">Main Logo&nbsp;</div>
								    	</td>
								    </tr>
								    <tr>
								    	<td class="text_left">
								    		<input type="hidden" name="file_src" id="file_src" value="1"/>
								    		<input type="hidden" name="file_name" id="file_name" value="logo_sch.gif"/>
								    		<input type="file" name="file" id="file" class="text_file" accept="image/gif,image/jpeg,image/png" onchange="fileCheck(this)">
								    	</td>
								    </tr>
								    <tr>
								    	<td class="explain text_left"><spring:message code="title.text._size" />: 240 x 56</td>
								    </tr>
								    <tr>
								    	<td class="explain text_left">
								    	<spring:message code="title.text.explanation" /> : <spring:message code="portalSetting.text._explanation.main_logo" /><br /><br />
								    	</td>
								    </tr>
								
									
  								    <c:if test="${WEB.MAIN_PAGE_IMAGE != 'false'}">
									    <tr>
									    	<td style="height:10px;"></td>
									    </tr>
  								    </c:if>
								</table>
							</div>

						
						</div> <!-- file_upload_div -->
					</form>
				</div> <!-- wrap_set -->
			</div><!-- // #viewImageMain -->
		</div>
	</div>
</div>
</body>
</html>