<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>설치 정보</title> 
</head>
<body>
<table> 
<tr><th>WAS 정보</th><td>${WAS}</td></tr>
<tr><th>서블릿 버전</th><td>${ServletMajorVersion}.${ServletMinorVersion}</td></tr>
<tr><th>App 설치 경로</th><td>${AppPath}</td></tr>
<c:forEach items="${props}" var="rows" varStatus="loop">
<tr>
<th>${rows.key}</th><td>${rows.val}</td>
</tr>
</c:forEach>  
</table>
</body>
</html>