<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Session Check</title>
<script>
var $i={};  
</script>  
<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
<script src="../../resources/cmresource/js/thirdparty/MIT/JSON-js-master/json2.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/secure/cipher.js"></script>  
  
<%
	String purl = request.getParameter("purl");
	String key = request.getParameter("key"); 
	String link = request.getParameter("link");
	if(purl==null) purl = "";
	if(key ==null) key = "";
	if(link==null) link = "";
%> 
<script>
var purl = ""; 
var key = "";
var link = "";
$(document).ready(function(){
	purl = "<%=purl%>";
	key = "<%=key%>";
	link = "<%=link%>";
	 
	
	$.ajax({
	    type: 'post',
	    dataType: 'jsonp', 
	    async:false,
	    contentType: "text/plain; charset=utf-8",
	    url: (purl + '/session/currentusersessioninfo.jsp'),
	    success: function (data) {
	    	//console.log(data);
	    },
	    error: function (request, status, error) {
	       	//console.log("error" + error); 
	    } 
	});	
}); 
function sessioncheck(json){ 
	
	var jsonobj = null;
	if(json!=null&&json!="") jsonobj = JSON.parse($i.cipher.base64.decrypt(json) );
	else jsonobj = { isLogin:false }; 
	if(jsonobj.isLogin){  
		$("#key").val(json);
		$("#link").val(link);
		$("#frm").submit();  
	}
	else{
		document.location.href = purl+"/noSSOLogin?key="+key;
	}
} 
</script>
</head>
<body>
connecting..
<form id="frm" action = "./create" method = "POST">
	<input id="key" name='key' type="hidden" value=""/>
	<input id="link" name='link' type="hidden" value=""/>
</form>
</body>
</html>