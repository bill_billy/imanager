<%@page import="org.apache.commons.codec.binary.Base64"%>
<%@page import="com.iplanbiz.core.session.LoginSessionInfo"%>
<%@page import="javax.servlet.http.HttpSession"%>
<%@page import="javax.servlet.RequestDispatcher"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="org.json.simple.JSONValue"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Session Check</title>
<script>
var $i={};  
</script>
<script src="../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
<script src="../resources/cmresource/js/iplanbiz/secure/cipher.js"></script>  

<script>
$(document).ready(function(){
 
}) ; 
</script>
</head>
<body> 
<pre>
<%
	//iframe에서 뜰때 세션이 자꾸 초기화 되는 문제 
	response.setHeader("P3P","CP='CAO PSA CONi OTR OUR DEM ONL'");
	Logger logger = LoggerFactory.getLogger(this.getClass()); 
	String purl = request.getParameter("purl"); 
	String key = request.getParameter("key");
	String link = request.getParameter("link");
	LoginSessionInfo loginSessionInfo = (LoginSessionInfo)request.getSession().getAttribute("loginSessionInfo"); 
	if (loginSessionInfo == null) {
		HttpSession cursession = request.getSession(false);
		if(cursession!=null){
			cursession.invalidate();	
		}
		cursession  = request.getSession(true);  
		 
		loginSessionInfo = new LoginSessionInfo();  
		request.getSession().setAttribute("loginSessionInfo",loginSessionInfo);  
		loginSessionInfo = (LoginSessionInfo)request.getSession().getAttribute("loginSessionInfo");   
	}
	if(purl==null) purl = "";
	if(key ==null) key = "";   
	if(link ==null) link = ""; 
%>
encode key => <%=key %>
isLogin => <%=loginSessionInfo.isLogin() %>
<%
	key = new String(Base64.decodeBase64(key));	
	link = new String(Base64.decodeBase64(link));
%>  
decode key => <%=key %> 
<%
	//JSONObject jsonobj = new JSONObject(key);
	JSONObject jsonobj = (JSONObject)JSONValue.parse(key);  
	
	boolean pIsLogin = (Boolean)jsonobj.get("isLogin");   
	if(pIsLogin){ 
		
		out.println("세션 동기화 진행중.  장시간 동작이 없으면 재 로그인 하시기 바랍니다."); 
		logger.info("포털 세션 동기화..");  
		loginSessionInfo.setLogin(pIsLogin);
		loginSessionInfo.setAcctID(jsonobj.get("acctID").toString());
		loginSessionInfo.setUserId(jsonobj.get("userId").toString()); 
		loginSessionInfo.setUserName(jsonobj.get("userName").toString());  
		loginSessionInfo.setIsAdmin((Boolean) jsonobj.get("isAdmin")); 
		//out.println("#################  세션 포털 동기화 성공  ####################");
		//out.println("## is login : "+ loginSessionInfo.isLogin() );  
		//out.println("## session key : "+ request.getSession().getId() ); 
		//out.println("## user id : "+ loginSessionInfo.getUserId() );
		//out.println("## user id : "+ loginSessionInfo.getUserName() );
		//out.println("## acct id : "+ loginSessionInfo.getAcctID() );
		//out.println("## admin yn : "+ loginSessionInfo.getIsAdmin() );
		//out.println("## link : "+ link );
		//out.println("###########################################################"); 
		//1)
		response.sendRedirect(link.replaceAll("\r\n",""));    
		//2)
		//RequestDispatcher rd = request.getRequestDispatcher(link);
		//rd.forward(request, response);  
		//3) 
		//pageContext.forward(link);
		//4)  javascript location.href, location.replace
		%> 
		<script>
			//("P3P","CP='CAO PSA CONi OTR OUR DEM ONL'"); 때문에 사용 안하지만 혹시 몰라 남겨둠 
			var link = "<%=link%>";
			/**
			$("body").append("<iframe id='ifrm' src='"+link+"'></iframe>");
			$("#ifrm").on("load", function(event){
				location.replace(link);  
			}); 
			**/
			var pop = window.open("./sessionpop.jsp?link="+link,"세션동기화중");  
		</script>
		<%
	} 
	else{ 
		out.println("세션이 만료되었습니다. 로그아웃 후에 재 로그인 하시기 바랍니다.");  
	}
%>


  </pre>
</body>
</html>