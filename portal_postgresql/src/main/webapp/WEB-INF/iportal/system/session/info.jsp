<%@page import="java.net.URLEncoder"%>
<%@page import="org.apache.commons.codec.binary.Base64"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.iplanbiz.core.session.LoginSessionInfo"%>
<%@page import="org.json.simple.JSONArray"%>
<%@page import="org.json.simple.JSONObject"%>
<%@page import="java.util.List"%>
<%@page import="java.util.LinkedHashMap"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>  
<%@ page import="java.io.*" %>
<%@ page import="org.springframework.web.context.WebApplicationContext"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ page import="java.lang.Exception"%> 
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="com.iplanbiz.iportal.config.WebConfig"%>
<%@ page import="org.springframework.beans.factory.ObjectFactory"%> 
<%@ page language="java" import="java.net.InetAddress" %> 
<% 
	Logger logger = LoggerFactory.getLogger(this.getClass()); 
 	
 	LoginSessionInfo loginSessionInfo = (LoginSessionInfo)request.getSession().getAttribute("loginSessionInfo"); 
 	
	logger.info("session is null = {}",loginSessionInfo==null);
	JSONObject json = new JSONObject();
	String jsonString = "";
	if(loginSessionInfo==null){
		json.put("isLogin",false);
	}
	else{
		try{
			String userId = loginSessionInfo.getUserId();
			String ipaddress = loginSessionInfo.getIpAddress();
			String acctID = loginSessionInfo.getAcctID();
			boolean isLogin = loginSessionInfo.isLogin();
			String userName = URLEncoder.encode(loginSessionInfo.getUserName(), "UTF-8");
			
			//String userName = loginSessionInfo.getUserName();
			String passport = loginSessionInfo.getPassport();
			boolean isAdmin = loginSessionInfo.getIsAdmin();
			String CAMID = loginSessionInfo.getCAMID();
			String myFolderStoreId = loginSessionInfo.getMyFolderStoreId();
			String userBrowserVersion = loginSessionInfo.getUserBrowserVersion();
			String userBrowserType = loginSessionInfo.getUserBrowserType();
			//String division = loginSessionInfo.getDivision(); 
			
			logger.info("userId:{}",userId);
			logger.info("ipaddress:{}",ipaddress);
			logger.info("acctID:{}",acctID);
			logger.info("isLogin:{}",isLogin);
			logger.info("userName:{}",userName);
			logger.info("passport:{}",passport);
			logger.info("isAdmin:{}",isAdmin);
			logger.info("CAMID:{}",CAMID);
			logger.info("myFolderStoreId:{}",myFolderStoreId);
			logger.info("userBrowserVersion:{}",userBrowserVersion);
			logger.info("userBrowserType:{}",userBrowserType); 
			
			json.put("userId",userId);  
			json.put("ipaddress",ipaddress);
			json.put("acctID",acctID);
			json.put("isLogin",isLogin);
			json.put("userName",userName);
			json.put("passport",passport);
			json.put("isAdmin",isAdmin);
			json.put("CAMID",CAMID); 
			json.put("myFolderStoreId",myFolderStoreId);
			json.put("userBrowserVersion",userBrowserVersion);
			json.put("userBrowserType",userBrowserType); 
			jsonString = Base64.encodeBase64String(json.toJSONString().getBytes("utf-8"));
		}catch(Exception e){
			json.put("isLogin",false);
		}
	}
	logger.info("info:{}", jsonString);
%>sessioncheck('<%=jsonString%>'); 