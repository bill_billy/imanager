<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="crt" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
	<head> 
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>게시판</title>
		<link rel="StyleSheet" href="../../resource/css/portlet_new.css" type="text/css" />
		<style>
			body{
				margin-top:10px;
			}
		</style>
		<script src="../../resource/js/jquery/jquery-1.7.2.js"></script>
		<script src="../../resource/js/util/common.js"></script>
		<script src="../../dwr/engine.js"></script>
		<script src="../../dwr/interface/groupManagerDwr.js"></script>
		<script language="javascript" type="text/javascript">
		function  goForm(){
			location.replace("../../cms/boardByYear/form?type=I");
		}
		function goList(){
			parent.parent.addTab("boardList", "<spring:message code='tab.text.board' javaScriptEscape='true' />", "../../cms/boardByYear/list", 0,0);
		}
		function goGet(boardNo, boardType){
			var url = "";
			url = "../../cms/boardByYear/get?boardNo="+boardNo+"&boardType="+ boardType;
// 			url = "/board/get?boardNo="+boardNo+"&boardType="+ boardType+"&gubn="+gubn+"&pType="+boardType;
		//	parent.parent.addTab(10014, "<spring:message code='tab.text.board' javaScriptEscape='true' />",url, 0,0);
			location.replace(url);
			//addTab(cId, tmp_title, tmp_content, width_size, height_size);
			/*
			*	cId : 고유번호
			* tmp_title : Tab 이름
			* tmp_content : Tab 주소
			* size : size
			*/
			
		}
		
		$(document).ready(function(){
			groupManagerDwr.dwrGetPortletContentBackgroundColor(function(res){
				$("body").css("background-color", res);
			});
			groupManagerDwr.dwrGetPortletContentFontColor(function(res){
				$("body").find("p").css("color", res);
			});
		});
		</script>
		
		<style>
			*{margin-right:0px}
		</style>
	</head>
	<body style="overflow:hidden;">
		<div class="form01" id="BOARD">
			<div class="form">
				<div class="table">
				<table style="width:100%" border="0" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th>제목</th><th>이름</th><th>작성일자</th>
					</tr>
				</thead>		
				<colgroup>
					<col width="50%" />
					<col width="25%" />
					<col width="25%" />
				</colgroup>
				
				<c:choose>
					<c:when test="${boardList != null && not empty boardList}">
					<c:forEach items="${boardList}" var="board" end="4" varStatus="loop">
					<tr>
						<c:if test="${board.answer_level > 0}">
							<td style="text-align:left; margin-left: 2px; vertical-align:middle;">
								<c:forEach begin="1" end="${board.answer_level}">
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								</c:forEach>
								<img src="../resource/webInput_img/boardButton/re.gif"  />
								<a href="#" onclick="goGet('${board.boardno}', '${board.board_type}')" ><c:out value="${board.title}" /></a></td>
						</c:if>
						<c:if test="${board.answer_level == 0}">
						<td>
								<a href="javascript:goGet('${board.boardno}', '${board.board_type}');" >
									<p style="margin-left:10px;"><c:out value="${board.title}" /></p>
								</a>
						</td>
						</c:if>
						<td><p>${board.name}</p></td>
						<td><p style="text-align:center;">${board.createdate}</p></td>
					</tr>
					</c:forEach>
					</c:when> 			 
					<c:otherwise>
						<tr>			
							<td colspan="3" style="font-size:12px;"><spring:message code="search.noData" /></td>
						</tr> 			
					</c:otherwise> 
				</c:choose>
				</table>
		</div>
	</body>
</html> 