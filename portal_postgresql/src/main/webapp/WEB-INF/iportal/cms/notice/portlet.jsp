<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="crt" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
	<head> 
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>공지사항</title>
		<link rel="StyleSheet" href="../../resource/css/portlet_new.css" type="text/css" />
		<style>
			body{
				margin-top:10px;
				font-size: 0.9em !important;
			}			}
		</style>
		<script src="../../resource/js/jquery/jquery-1.7.2.js"></script>
		<script src="../../resource/js/util/common.js"></script>
		<script src="../../dwr/engine.js"></script>
		<script src="../../dwr/interface/groupManagerDwr.js"></script>
		<script language="javascript" type="text/javascript">
		function  goForm(){
			location.replace("../../cms/notice/form?type=I");
		}
		function goList(){
			parent.parent.addTab("noticeList", '<spring:message code="tab.text.notice" javaScriptEscape="true" />', "../../cms/notice/list", 0,0);
		}
		function goGet(boardNo){
			var url="";
			url="../../cms/notice/get?boardNo="+boardNo;
			//parent.parent.addTab(boardNo, '<spring:message code="tab.text.notice" javaScriptEscape="true" />', url, 0,0);
			location.replace(url);
			//addTab(cId, tmp_title, tmp_content, width_size, height_size);
			/*
			*	cId : 고유번호
			* tmp_title : Tab 이름
			* tmp_content : Tab 주소
			* size : size
			*/
			
		}
		
		$(document).ready(function(){
			groupManagerDwr.dwrGetPortletContentBackgroundColor(function(res){
				$("body").css("background-color", res);
			});
			groupManagerDwr.dwrGetPortletContentFontColor(function(res){
				$("body").css("color", res);
			});
		});
		</script>
		
		<style>
			*{margin-right:0px}
		</style>
	</head>
	<body style="overflow:hidden;">
		<div class="form01" id="BOARD">
			<div class="form">
				<div class="table">
				<table style="width:100%;" border="0" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th>제목</th><th>작성일자</th>
					</tr>
				</thead>		
					<colgroup>
						<col width="60%" />
						<col width="*" />
					</colgroup>
<%-- 					<c:forEach items="${informList}" var="inform" varStatus="loop"><c:set var="informSize" value="${loop.count}"/> --%>
<!-- 					<tr> -->
<%-- 						<td class="inform"><a href="#" onclick="goGet('${inform.BOARDNO}')" ><p><c:out value="${inform.TITLE}" /></p></a></td> --%>
<%-- 						<td class="date">${inform.CREATEDATE}</td> --%>
<!-- 					</tr> -->
<%-- 					</c:forEach> --%>
					<c:choose>
						<c:when test="${noticeList != null && not empty noticeList}">
							<c:forEach items="${noticeList}" var="notice" end="${5-informSize }" varStatus="loop">
								<tr>
									<td><a href="#" onclick="goGet('${notice.boardno}')" ><p style="margin-left:10px;"><c:out value="${notice.title}" /></p></a></td>
									<td><p style="text-align:center;">${notice.createdate}</p></td>
								</tr>
							</c:forEach>
						</c:when> 			 
						<c:otherwise>
							<tr>			
								<td colspan="3" style="font-size:12px;"><spring:message code="search.noData" /></td>
							</tr> 			
						</c:otherwise> 
					</c:choose>
				</table>
		</div>
	</body>
</html> 