<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html> 
	<head> 
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>공지사항 등록</title>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" href="../../resource/css/iplanbiz/table.css" type="text/css"/>
		<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
		<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
		<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>

		<link rel="stylesheet" href="../../resource/css/iplanbiz/table.css" type="text/css" />
		<script src="../../resources/cmresource/js/iplanbiz/core/iplanbiz.web.include.jsp"></script>
		<script src="../../resources/cmresource/js/thirdparty/LGPL/v2/smartEditor2/js/HuskyEZCreator.js" charset="UTF-8"></script>
		
		<script language="javascript" type="text/javascript">
		var oEditors = [];
		
		function initSmartEditor(id) {
			nhn.husky.EZCreator.createInIFrame({
				oAppRef: oEditors,
				elPlaceHolder: id,
				sSkinURI : "../../resources/cmresource/js/thirdparty/LGPL/v2/smartEditor2/SmartEditor2Skin.html",
				htParams : {bUseToolbar : true,
					fOnBeforeUnload : function(){
					}
				},
				fOnAppLoad : function(){
					//oEditors.getById["ir1"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
				},
				fCreator: "createSEditor2"
			});
		}
		function fRemoveHtmlTag(string) { 
			   var objReplace = new RegExp();
			   var objnbsp = new RegExp();
			   objReplace = /[<][^>]*[>]/gi; 
			   objnbsp = /&nbsp;/gi;
			   return string.replace(objReplace, "").replace(objnbsp,"").replace(/(\s*)/g, ""); 
			} 
		function saveFrm(){
			oEditors.getById["content"].exec("UPDATE_CONTENTS_FIELD", []);
			if($("#title").val() == null || $("#title").val() == ""){
				$i.dialog.warning('SYSTEM','<spring:message code="board.alert.title" javaScriptEscape="true" />');
				return false;
			}
			var smartCheck = oEditors.getById["content"].getIR();
			if(byteCheck(fRemoveHtmlTag(smartCheck)) > 60000){
				$i.dialog.warning('SYSTEM',"1~20000자(한글 기준)의 한글/영문/특수문자 혼용만 가능합니다.");
				return false;
			}
// 			if($(":radio[name='popupYn']:checked").val() == "y"){
// 				if($("#beginDay").val() == "" || $("#endDay").val() == ""){
// 					alert("팝업 시작일/종료일을 선택하세요");
// 					return false;
// 				}
// 			}
			
			document.getElementById("notice").submit();
		}
		function checkDay(){
			if($("#beginDay").val() > $("#endDay").val()){
				$i.dialog.warning('SYSTEM',"팝업종료일이 팝업시작일 보다 과거입니다.");
				$("#endDay").val("");
			}
		}
		function goList(){
			location.replace("../../cms/notice/list");
		}
		function byteCheck(code) {
			var size = 0;
			for (i = 0; i < code.length; i++) {
				var temp = code.charAt(i);
			 	if (escape(temp) == '%0D')
			 		continue;
				if (escape(temp).indexOf("%u") != -1) {
			   		size += 3;
			  	} else {
			   		size++;
			 	}
		 	}
			return size;
		};
		function checkSize(idx){
			var fileLength = $("input[name=file]").length;
			var fileUpload = $("[name='fileUpload']").length;
			if($("[name='fileUpload']").length >= 5){
				$i.dialog.warning('SYSTEM',"파일업로드는 5개 이상 할 수 없습니다.");
			}else{
				if($("input[name='file']")[idx].value != ""){
					var FileFilter = /\.(txt|zip|jpg|png|xls|xlsx|ppt|pptx|doc|docx|hwp|pdf|mp4)$/i;
					var fileValue = $("input[name='file']")[idx].value.toLowerCase();
					var fileCheck = false;
					var fileNameSize = false;
					var fileSize = $("input[name='file']")[idx].files[0].size;
					var fileName = $("input[name='file']")[idx].files[0].name;
					var defaultSize = 10000;
					if(fileValue.match(FileFilter)){
						fileCheck = true;
					}
					if(byteCheck(fileName)< 50){
						fileNameSize = true;
					}
					if(fileCheck == true){
						if(fileNameSize == true){
							/*
							if(parseInt(fileSize) > parseInt(defaultSize)){
								alert("업로드 용량이 기본 용량보다 큽니다.");
								$("#fileName"+idx).val("");
								$("input[name='file']")[idx].value = "";
							}else{
								$("#fileName"+idx).val($("input[name='file']")[idx].value);
							}	*/
							$("#fileName"+idx).val($("input[name='file']")[idx].value);
						}else{
							var length = parseInt((50 / 3));
							$i.dialog.warning('SYSTEM', length + "자(한글기준)의 한글/영문/특수문자/숫자 혼용만 가능합니다.");
		    				$("input[name='file']")[idx].focus();
						}
					}else{
						$i.dialog.warning('SYSTEM',"업로드가 가능한 확장자가 아닙니다.");
					}
				}
			}
		}
		function addFile(){
			var fileLength = $("input[name=file]").length;
			var fileUpload = $("[name='fileUpload']").length;
			var fileNo = fileLength;
			var fileName = "fileName" + fileNo;
			var fileName1 = "fileName1" + fileNo;
			if(fileLength >= 5 || $("[name='fileUpload']").length >= 5 || parseInt(fileLength + fileUpload) >= 5){
				$i.dialog.warning('SYSTEM',"파일업로드는 5개 이상 할 수 없습니다.");
			}else{
				var html = "";
				html += '<div id="'+fileName1+'" >';
				html += '<div  style="float:left; margin-bottom:5px;">';
				html += '<input type="text" id="'+fileName+'" class="file_input_wide_textbox" readonly style="width:680px;">';
				html += '<div class="file_input_div">';
				html += '<input type="button" value="찾아보기" class="file_input_button" />';
				html += '<input type="file" name="file" class="file_input_hidden" accept=".txt,.zip,.jpg,.png,.xls,.xlsx,.ppt,.pptx,.doc,.docx,.hwp,.pdf" onchange="checkSize('+fileNo+');" />';
				html += '</div>';
				html += '</div>';
				html += '<span id="fileMinusButton" style="cursor:pointer;float:right;margin:0!important; padding-right:20px;"> <img src="../../resource/css/images/img_icons/icon-minus-small.png" height="15" alt="삭제버튼" class="button_minus_minus"  onclick="deleteFIle(\''+fileName1+'\'); "> </span> <br>';
				html += '</div>';
				
				$("#inputFile").append(html);
			}
		}
		function deleteFIle(inputName){
			$("#"+inputName).remove();
		}
		function checkInput(id, size, gubn){
			oEditors.getById["content"].exec("UPDATE_CONTENTS_FIELD", []);
    		if(byteCheck($("#"+id).val()) > size){
    			var length = "";
    			if(gubn == "en"){
    				length = parseInt((size / 1));
    				$i.dialog.warning('SYSTEM', length + "자(영문 기준)의 영문/_ 혼용만 가능합니다.");
    			}else if(gubn == "all"){
    				length = parseInt((size / 3));
    				$i.dialog.warning('SYSTEM', length + "자(한글 기준)의 한글/영문/특수문자 혼용만 가능합니다.");
    			}else if(gubn =="num"){
    				length = parseInt((size / 1));
    				$i.dialog.warning('SYSTEM', length + "자의 숫자만 가능합니다.");
    			}else if(gubn =="kor"){
    				length = parseInt((size / 3));
    				$i.dialog.warning('SYSTEM', length + "자(한글 기준)의 한글만 가능합니다.");
    			}
    			$("#"+id).focus();
    			return false;
    		}
    	}
	 	function byteCheck(code) {
			var size = 0;
			for (i = 0; i < code.length; i++) {
				var temp = code.charAt(i);
				if (escape(temp) == '%0D')
			   		continue;
			  	if (escape(temp).indexOf("%u") != -1) {
			   		size += 3;
			  	} else {
			   		size++;
			 	}
		 	}
		 	return size;
		}
		$(function() {
			$( "#beginDay" ).datepicker({
				changeYear: false,
				showOn: "button",
				buttonImage: "../../resource/img/calendar.gif",
				buttonImageOnly: true,
				dateFormat: "yy-mm-dd"
			});
// 			$( "#startDay1" ).datepicker( "option", $.datepicker.regional["${WEB.LOCALE}"]);
			
			$( "#endDay" ).datepicker({
				changeYear: false,
				showOn: "button",
				buttonImage: "../../resource/img/calendar.gif",
				buttonImageOnly: true,
				dateFormat: "yy-mm-dd"
			});
// 			$( "#endDay" ).datepicker( "option", $.datepicker.regional["${WEB.LOCALE}"]);
			$("#jqxButton1").jqxButton({ width: '',  theme:'blueish'});	
			$("#jqxButton2").jqxButton({ width: '',  theme:'blueish'});
			initSmartEditor('content'); 
			var fileUpload = $("[name='fileUpload']").length;
			if(fileUpload >= 5){
				$("#fileUploadTr").hide();
			}else{
				$("#fileUploadTr").show();
			}
		});
		</script>
		<style>
		 .padding_table{padding-top:10px;padding-bottom:10px;}
		</style>
	</head> 
	<body class='blueish  i-DQ'>
		<div class="wrap" style="width:98% !important; min-width:1160px; margin:0 10px!important; padding:0 !important; ">
			<div class="i-DQ-contents content" style="width:100%; height:100%; float:left;">
				<div class="label type2 f_left">작성하기</div>
				<div class="table_Area" style="width:100%; height:100%; float:left; margin-top:0px; margin-bottom:5px; ">
					<form:form action="save" method="post" commandName="notice" enctype="multipart/form-data"> 	
						<input type="hidden" id="name" name="name" value="${sessionScope.loginSessionInfo.userName}"/>
						<input type="hidden" id="userId" name="userId" value="${notice.userid }"/>
						<input type="hidden" id="type" name="type" value="${type }"/>
						<input type="hidden" id="boardNo" name="boardNo" value="${notice.boardno}"/>
						 <table width="100%" class="i-DQ-table" cellspacing="0" cellpadding="0" border="0" style="margin-bottom:0 !important;">
						 	<tr>
								<th class="required_icon"  width="15%"><span style="color:red; font-size:12px; vertical-align:-2px;">＊</span>제목</th>
								<td colspan="3" class="activetable">
									<input type="text" value="${fn:escapeXml(notice.title)}" id="title" name="title" class="txt_style_txt" style="height:17px; width:100%;  margin:5px 0;padding-bottom:3px; " onblur="checkInput('title','255','all');" />
								</td>
							</tr>
							<tr>
								<th width="15%">작성자</th>
								<td colspan="3">
									<input type="text" value="${sessionScope.loginSessionInfo.userName}" id="i-user" class="inputNormal" style="height:19px; width:100%; margin:5px 0; padding:0; padding-bottom:3px; " disabled/>
								</td>
							</tr>
							<tr>
								<th width="15%" >팝업여부</th>
								<td>
									<label for="ranking"  style="vertical-align: middle;">
										<input type="radio" value="y" name="popupYn" style="vertical-align: middle; margin-top:2px;" <c:if test="${notice.popup == 'y' }">checked</c:if> /><spring:message code="button.text._yes" />
									</label>
									<label for="ranking" style="vertical-align: middle;">
										<input type="radio" value="n" name="popupYn" style="vertical-align: middle;margin-top:2px; width:5%;" <c:if test="${notice.popup == 'n'|| notice.popup == null}">checked</c:if> /><spring:message code="button.text._no" />
									</label>
								</td>
								<th width="15%">알림여부</th>
								<td>
									<label for="ranking"  style="vertical-align: middle;">
										<input type="radio" value="y" name="informYn" style="vertical-align: middle; margin-top:2px;" <c:if test="${notice.inform_yn == 'y'}">checked</c:if> /><spring:message code="button.text._yes" />
									</label>
									<label for="ranking" style="vertical-align: middle;">
										<input type="radio" value="n" name="informYn" style="vertical-align: middle;margin-top:2px; width:5%;" <c:if test="${notice.inform_yn == 'n'}">checked</c:if> /><spring:message code="button.text._no" />
									</label>
								</td>
							</tr>
							<tr style="display: none;">
								<th>팝업시작일</th>
								<td>
									<input type="text" id="beginDay" name="beginDay" value="${notice.beginday}" readonly="readonly"/>
								</td>
								<th>팝업종료일</th>
								<td>
									<input type="text" id="endDay" name="endDay" value="${notice.endday}" readonly="readonly" onchange="checkDay();"/>
								</td>
							</tr>
							<tr>
								<th class="th_height2 required_icon"><span style="color:red; font-size:12px; vertical-align:-2px;">＊</span>내용</th>
								<td colspan="3" style=" padding:5px !important">
									<textarea id="content" name="content" rows="20" cols="102" class="textarea_df" style="border:0px;width:100%;" onchange="checkInput('content','60000','all');" >${notice.content}</textarea>
								</td>
							</tr>
							<c:if test = "${(fileList != null)&&(fn:length(fileList) > 0)}">
								<tr>
									<th>파일목록</th>
									<td colspan="3">
										<div style="margin:5px 0; width:100%; height:80px; overflow-y:scroll;">
											<c:forEach var="file" items="${fileList}">
												<p name="fileUpload">
													${file.file_org_name }<a href="../../cms/notice/deleteFile?fileId=${file.file_id}&boardNo=${notice.boardno}&type=${type}"><img src="../../resource/css/images/img_icons/cancel.png" border="0" alt="${file.file_org_name}" /></a>
												</p>
											</c:forEach>
										</div>
									</td>
								</tr>
							</c:if>
							<tr id="fileUploadTr">
								<th>파일첨부</th>
								<td colspan="3">
									<div id="inputFile" style="margin:5px 0; width:100%; height:80px; overflow-y:scroll;">
										<div id="fileName10">
											<div  style="float:left; margin-bottom:5px; ">
												<input type="text" id="fileName0" class="file_input_wide_textbox" readonly style="width:680px;">
												<div class="file_input_div">
													<input type="button" value="찾아보기" class="file_input_button" />
													<input type="file" name="file" accept=".txt,.zip,.jpg,.png,.xls,.xlsx,.ppt,.pptx,.doc,.docx,.hwp,.pdf" class="file_input_hidden" onchange="checkSize('0');" />
												</div>
											</div>
											<span id="filePlusButton" style="cursor:pointer;float:right;margin:0 !important; padding-right:10px;"> <img src='../../resource/css/images/img_icons/icon-plus-small.png' alt="추가버튼" class="button_plus_minus" onclick="addFile();" height=15 > </span> <br>
										</div>
									</div>
								</td>
							</tr>
						 </table>
					</form:form>
				</div>
				<div class="group_button f_right" style="margin-top:10px !important;">
				<div class="button type2 f_left" style="margin-left:0px !important;">
					<input type="button" value="목록" id='jqxButton1' width="100%"
						height="100%" onclick="goList();" />
				</div>
				<div class="button type2 f_left" style="margin-left:10px !important;">
					<input type="button" value="저장" id='jqxButton2' width="100%"
						height="100%" onclick="saveFrm();" />
				</div>
			</div>
			</div>
		</div>
	</body>
</html> 