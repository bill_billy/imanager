<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head> 
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>공지사항</title>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
		<script src="${WEB.JSUTIL}/common.js"></script>
		
		<script language="javascript" type="text/javascript">
		var msgValue = "${param.msg}";
		if(msgValue != ""){
			if(msgValue == "1"){
				alert("저장되었습니다.");
			}else if(msgValue == "0"){
				alert("저장 중 에러가 발생하였습니다.");
			}else if(msgValue == "3"){
				alert("삭제 되었습니다.");
			}else if(msgValue == "4"){
				alert("삭제 중 에러가 발생했습니다.");
			}
		}
		function  goForm(){
			location.replace("../../cms/notice/form?type=I");
		}
		function goDetail(boardNo){
			location.replace("../../cms/notice/get?boardNo="+boardNo);
		}
		function formSearch(){
			$("#search").submit();		
		}
		function dayCheck(){
			if($("#endDay1").val() < $("#startDay1").val()){
				alert("종료일자가 시작일자보다 과거입니다.");
				$("#endDay1").val("");
			}else{
				searchStart();
			}
		}
		function searchStart(){
    		var division = "";
			var title = "";
			var content = "";
			var name = "";
			var gubn = "";
			var startDay = "";
			var endDay = "";
			if($("#startDay1").val() == "" && $("#endDay1").val() == ""){
				gubn = "NULL";
				startDay = "0000-00-00";
				endDay = "9999-99-99";
			}else{
				gubn = "NOT";
				startDay = $("#startDay1").val();
				endDay = $("#endDay1").val();
			}
			if($("#pSelect").val() == 'title'){
				title = $("#input").val();
			}else if($("#pSelect").val() == 'content'){
				content = $("#input").val();
			}else if($("#pSelect").val() == 'name'){
				name = $("#input").val();
			}
    		
    		var boardData = $i.post("./getNoticeList", { TITLE : title, CONTENT : content,
    			NAME : name, GUBN : gubn,START_DAT : startDay, END_DAT : endDay});
    		boardData.done(function(bData){
    			console.log(bData);
    			var source =
	            {
	                datatype: "json",
	                datafields: [
	                	{ name: 'RNUM', type: 'string' },
	                    { name: 'BOARDNO', type: 'string' },
	                    { name: 'TITLE', type: 'string' },
	                    { name: 'CONTENT', type: 'string' },
	                    { name: 'USERID', type: 'string' },
	                    { name: 'NAME', type: 'string' },
	                    { name: 'DIVISION', type: 'string' },
	                    { name: 'HITCOUNT', type: 'string' },
	                    { name: 'CREATEDATE', type: 'string' },
	                    { name: 'INFORM_YN', type: 'string' },
	                    { name: 'FILECOUNT', type: 'string' }
					],
	                localdata: bData.returnArray,
	                updaterow: function (rowid, rowdata, commit) {
	                    commit(true);
	                }
	            };
    			 var dataAdapter = new $.jqx.dataAdapter(source, {
	                downloadComplete: function (data, status, xhr) { },
	                loadComplete: function (data) { },
	                loadError: function (xhr, status, error) { }
	            });
    			 var dataAdapter = new $.jqx.dataAdapter(source);
 	            var alginRight = function (row, columnfield, value) {//right정렬
 	                return '<div id="userName-' + row + '"style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
 	            };
 				var alginLeft = function (row, columnfield, value) {//left정렬
 	                return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
 	            };
 	            var infoRender = function(row,datafield,value){
 	            	var informYn = $("#gridNoticeList").jqxGrid("getrowdata",row).INFORM_YN;
 	            	if(informYn == "Y"){
 	            		return "<div style='text-align:center; font-weight:bold; margin:4px 0px 0px 10px;'>" + value + "</div>";
 	            	}else{
 	            		return "<div style='text-align:center; margin:4px 0px 0px 10px;'>" + value + "</div>";
 	            	}
 	            };
 	            var detail = function(row,datafieId,value){
 	            	var newValue = $i.secure.scriptToText(value);
 	            	var boardNo = $("#gridNoticeList").jqxGrid("getrowdata", row).BOARDNO;     
 	            	var informYn = $("#gridNoticeList").jqxGrid("getrowdata",row).INFORM_YN;
 	            	var link = "<a href=\"javaScript:goDetail('"+boardNo+"')\" style='color:black;text-decoration:underline !important;'>" + newValue + "</a>";
 	            	if(informYn == "Y"){
 	            		return "<div style='text-align:left; font-weight:bold; margin:4px 0px 0px 10px;'>" + link + "</div>";
 	            	}else{
 	            		return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
 	            	}
 	            };
				var dataAdapter = new $.jqx.dataAdapter(source); 
				$("#gridNoticeList").jqxGrid(
			            {
			                width: '100%',
			                height: '100%',
							altrows:true,
							pageable: true,
							pagesize: 100,
							pagesizeoptions:['100', '200', '300'],
			                source: dataAdapter,
							theme:'blueish',
							columnsheight:30 ,
							rowsheight: 30,
			                columnsresize: true,
		     				columns:[
		     					{ text: 'NO', datafield: 'RNUM', width: '5%', align:'center', cellsalign: 'center', cellsrenderer: infoRender},
		  	                  	{ text: '제목', datafield: 'TITLE', width: '65%', align:'center', cellsalign: 'left', cellsrenderer:detail},
		  					  	{ text: '작성자', datafield: 'NAME', width: '10%', align:'center', cellsalign: 'center', cellsrenderer: infoRender},
		  	                  	{ text: '첨부', datafield: 'FILECOUNT', width: '5%', align:'center', cellsalign: 'center', cellsrenderer: infoRender},
		  	                  	{ text: '등록일', datafield: 'CREATEDATE', width: '10%', align:'center', cellsalign: 'center', cellsrenderer: infoRender},
		  	                  	{ text: '조회', datafield: 'HITCOUNT', width: '5%', align:'center', cellsalign: 'center', cellsrenderer: infoRender}
		     				]
					});
    		});
    		
    	}
		function checkInput(id, size, gubn){
    		if(byteCheck($("#"+id).val()) > size){
    			var length = "";
    			if(gubn == "en"){
    				length = parseInt((size / 1));
    				alert( length + "자(영문 기준)의 영문/_ 혼용만 가능합니다.");
    			}else if(gubn == "all"){
    				length = parseInt((size / 3));
    				alert( length + "자(한글 기준)의 한글/영문/특수문자 혼용만 가능합니다.");
    			}else if(gubn =="num"){
    				length = parseInt((size / 1));
    				alert( length + "자의 숫자만 가능합니다.");
    			}else if(gubn =="kor"){
    				length = parseInt((size / 3));
    				alert( length + "자(한글 기준)의 한글만 가능합니다.");
    			}
    			$("#"+id).focus();
    			return false;
    		}
    	}
	 	function byteCheck(code) {
			var size = 0;
			for (i = 0; i < code.length; i++) {
				var temp = code.charAt(i);
				if (escape(temp) == '%0D')
			   		continue;
			  	if (escape(temp).indexOf("%u") != -1) {
			   		size += 3;
			  	} else {
			   		size++;
			 	}
		 	}
		 	return size;
		}
		function setPagerLayout(selector) {
			
			var pagesize = $('#'+selector).jqxGrid('pagesize');
			
			var w = 49; 
				
			if(pagesize<100) {
				w = 44;
			} else if(pagesize>99&&pagesize<1000) {
				w = 49;
			} else if(pagesize>999&&pagesize<10000) {
				w = 54;
			}
			
			//디폴트 셋팅
			$('#gridpagerlist'+selector).jqxDropDownList({ width: w+'px' });
			
			//체인지 이벤트 처리
			$('#'+selector).on("pagesizechanged", function (event) {
				var args = event.args;
				
				if(args.pagesize<100) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '44px' });
				} else if(args.pagesize>99 && args.pagesize<1000) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '49px' });
				} else if(args.pagesize>999 && args.pagesize<10000) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '54px' });
				} else {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: 'auto' });
				}
				
			});
		}
		$(function() {
			$("#newButton").jqxButton({width:'', theme:'blueish'});
    		$( "#startDay1" ).datepicker({
				changeYear: false,
				showOn: "button",
				buttonImage: "../../resources/img/calendar.gif",
				buttonImageOnly: true,
				dateFormat: "yy-mm-dd"
			});
    		$( "#endDay1" ).datepicker({
    			changeYear: false,
				showOn: "button",
				buttonImage: "../../resources/img/calendar.gif",
				buttonImageOnly: true,
				dateFormat: "yy-mm-dd"
			});
    		var dat  = [
				{value:'title',text:'제목'},
				{value:'content',text:'내용'},
				{value:'name',text:'작성자'},
				
			];
			// prepare the data
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: 'value' },
					{ name: 'text' }
				],
				id: 'id',
				localdata:dat,
			//	url: url,
				async: false
			};
			var dataAdapter = new $.jqx.dataAdapter(source);

			// Create a jqxComboBox
			$("#pSelect").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "text", valueMember: "value", dropDownWidth: 150, dropDownHeight: 80, width: 150, height: 21, theme:'blueish'});
			$("#input").jqxInput({placeHolder: "", height: 21, width: 250, minLength: 1, theme:'blueish' });
			$("#startDay1").jqxInput({placeHolder: "", height: 21, width: 120, minLength: 1, theme:'blueish' });
			$("#endDay1").jqxInput({placeHolder: "", height: 21, width: 120, minLength: 1, theme:'blueish' });
		
			$("#input").keydown(function(event){
				if(event.keyCode == 13){
					return searchStart();
				}
	        		
        	});

			searchStart();
		});
		</script>
		<style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
			.ui-datepicker-trigger{
				float:left;
				margin-top:5px;
				margin-left:5px;
			}
			
		</style>
		<style>
			.border_top{border-top:1px solid #dedede}
		</style>
	</head>
	<body class='blueish'>
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%; min-width:1160px; margin:0 10px;">
			<div class="header f_left" style="width:100%; height:58px; margin:10px 0">
				<form id="searchForm" name="searchForm" action="./list">
					<div style=" float:left;width:100%;">
						<div class="label type1 f_left">등록일</div>
						<div style="margin:0 10px;" class="f_left">
							<input type="text"  id="startDay1" class="f_left"  style="height:16px; width:120px; text-align:center; "/>
							<span style="float:left; vertical-align:middle; padding:7px 20px 0px 20px;">~</span>
							<input type="text"  id="endDay1" class="f_left"  style="height:16px; width:120px; text-align:center; " onChange="searchStart();" />
						</div>
						<div class="f_right">
						<div class="button type1 f_left">
							<input type="button" value="신규" id='newButton' width="100%" height="100%" onclick="goForm();" />
						</div>
					</div>
					</div>
					<div style=" float:left;width:100%; margin-top:10px;">
						<div class="label type1 f_left" style="margin-right:12px;">검색</div>
						<div style='float:left; margin:0 10px;' id='pSelect' ></div>
						<div  style='float: left; margin-left:10px; margin-right:0px;' >
							<input type="text" id="input" name="input" />
						</div>
						<div cass="image-button" style="float:left;margin:0; padding:0;"><img src="../../resources/img/img_icons/search_icon.png" alt="조회" style="cursor:pointer;" onclick="searchStart();"></div>
					</div>
				</form>
			</div>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style="width:100%; margin:0 0%;">
					<div class="grid f_left" style="width:100%; height:500px;">
						<div id="gridNoticeList"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html> 