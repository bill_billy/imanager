<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<% String contextPath = request.getContextPath(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>공지사항</title>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" href="../../resource/css/iplanbiz/table.css" type="text/css"/>
		<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
		<script language="javascript" type="text/javascript">
		function  goForm(){
			location.replace("../../cms/notice/form?type=M&boardNo=${notice.boardno}");
		}
			function deleteBoard() {
				$i.dialog.confirm('SYSTEM','<spring:message code="_alert._delete" javaScriptEscape="true" />',function(){
					location.replace("../../cms/notice/delete?boardNo=${notice.boardno}");
				});
			}
			function goList() {
				location.replace("../../cms/notice/list");
			}
			$(document).ready(function() {
				$("#jqxButton1").jqxButton({ width: '',  theme:'blueish'}).on('click', function() {goForm();}); //수정;	
				$("#jqxButton2").jqxButton({ width: '',  theme:'blueish'}).on('click', function() {deleteBoard();}); //삭제
				$("#jqxButton3").jqxButton({ width: '',  theme:'blueish'}).on('click', function() {goList();}); //리스트

			});
		</script>
		<style>
	 		.border_bottom{border-bottom:1px solid #dedede;}
	 		.text_left{padding-left:10px;text-align:left;}
		</style>
	</head>
	<body class='blueish i-DQ'>
		<div class="wrap" style="width:98% !important; min-width:1160px; margin:0 10px!important; ">
			<div class="i-DQ-contents" style="width:100%; height:100%; float:left;margin:10px 0">
				<div class="iwidget_label" style="margin:0 !important;">
					<div class="button-bottom" style="float:right; margin-bottom:10px;">
						<c:if test="${sessionScope.loginSessionInfo.userId == 'admin' || sessionScope.loginSessionInfo.userId == notice.userid}">
							<div class="button type2 f_left"  style="margin-left:0px !important;">
									<input type="button" value="수정" id='jqxButton1' width="100%" height="100%" onclick="goForm();"/>
								</div>
						</c:if>
						<div class="button type2 f_left"  style="margin-left:10px !important;">
								<input type="button" value="목록" id='jqxButton3' width="100%" height="100%" onclick="goList();" />
							</div>
						<c:if test="${sessionScope.loginSessionInfo.userId == 'admin' || sessionScope.loginSessionInfo.userId == notice.userid}">
							<div class="button type3 f_left"  style="margin-left:10px !important;">
									<input type="button" value="삭제" id='jqxButton2' width="100%" height="100%" onclick="deleteBoard();" />
								</div>
						</c:if>
						
					</div>
					
				</div>
				<div class="table_Area" style="width:100%; height:100%; float:left; margin-top:0px; margin-bottom:10px; ">
					<table width="100%" class="i-DQ-table" cellspacing="0" cellpadding="0" border="0" style="margin-bottom:0 !important; border-left:0 !important; border-right:0 !important; border-bottom:0 !important;">
						<tr>
							<th colspan="7" style="text-align:left; padding-left:10px; height:35px; border-right: 0 !important; font-size:13px !important; font-weight:bold !important;"><div id="NoticeTitle">${fn:escapeXml(notice.title)}</div></th>
							<script>
							function replaceAll(str,orgStr,repStr){
								    return str.split(orgStr).join(repStr);
							}
							var title = $("#NoticeTitle")[0].innerHTML;
							$("#NoticeTitle").html(replaceAll(replaceAll(title,"<","&lt;"),">","&gt;"));
							</script>
						</tr>
						<tr>
							<th width="15%" style="background:#EDF5F8;border-left:0 !important; border-right:0 !important; border-bottom-color:#E1E1E1 !important;">작성자</th>
							<td colspan="3" style="border-left:0 !important; border-right:0 !important;"><input type="text" value="${notice.name}" id="i-user" class="tab_table_box" style=" width:100%;  margin:5px; padding:0; text-indent:0 !important;" disabled/></td>
							<th width="10%" style="background:#EDF5F8;border-left:0 !important; border-right:0 !important; border-bottom-color:#E1E1E1 !important;">소속</th>
							<td colspan="2"  style="border-left:0 !important; border-right:0 !important;"><input type="text" value="${notBu}" id="i-user" class="tab_table_box" style=" width:100%; margin:5px; padding:0;text-indent:0 !important;" disabled/></td>
						</tr>
						<tr>
							<th width="15%" style="background:#EDF5F8; border-left:0 !important; border-right:0 !important; border-bottom-color:#E1E1E1 !important;">등록일</th>
							<td colspan="3" style="border-left:0 !important; border-right:0 !important;"><input type="text" value="${notice.createdate}" id="i-user" class="tab_table_box" style=" width:100%;  margin:0px; padding:0;text-indent:0 !important;text-align:center !important;" disabled/></td>
							<th width="10%"  style="background:#EDF5F8; border-left:0 !important; border-right:0 !important; border-bottom-color:#E1E1E1 !important;">조회수</th>
							<td colspan="2" width="15%" style="border-left:0 !important; border-right:0 !important;"><input type="text" value="${notice.hitcount}" id="i-user" class="tab_table_box" style=" width:100%;  margin:0px; padding:0;text-indent:0 !important; text-align:center !important;text-indent:0 !important;" disabled/></td>
						</tr>
						<tr>
							<th width="15%" style="background:#EDF5F8; border-left:0 !important; border-right:0 !important;border-bottom-color:#E1E1E1 !important;">내용</th>
							<td colspan="5" style=" padding:5px !important"><div class="dhtml_area" style=" margin:0px 0 0px 0px !important; border:1px solid #eeeeee; height:250px;overflow-y:scroll;">${notice.content}</div></td>
							
						</tr>
						<c:forEach var="file" items="${fileList }"  varStatus="loop">
							<tr>
								<th width="15%" style="background:#EDF5F8;border-left:0 !important; border-right:0 !important; border-bottom-color:#E1E1E1 !important;">첨부 ${loop.count}</th>
								<td  colspan="5"  style="border-left:0 !important; border-right:0 !important;">
									<span style="font-weight: bold; padding-right: 10px;"></SPAN>${file.file_org_name }<a href="../../cms/board/download?fileId=${file.file_id}"><img src="../../resource/css/images/img_icons/down.png" border="0" alt="${file.file_org_name}" /></a>
								</td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
		</div>
	</body>
</html> 