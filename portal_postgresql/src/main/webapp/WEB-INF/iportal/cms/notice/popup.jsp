<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<% String contextPath = request.getContextPath(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>공지사항</title>
		<link rel="StyleSheet" href="../../resource/css/layout_common/common.css" type="text/css" />
		<script src="../../resources/js/jquery/jquery-1.8.3.min.js"></script>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<%--		<script type="text/javascript" src="${WEB.JS}/common.js"></script>--%>
		<script type="text/javascript">
		function popupClose(boardNo){
			if(parent!=null) {
				if($('#cookie_checkbox').is(":checked")){
					if(parent.jqxpopupclose!=null){
						parent.setCookie('notice_popup_'+boardNo,'n',7,'/');
					}
				}
				if(parent.jqxpopupclose!=null)
					parent.jqxpopupclose("notice_popup_"+boardNo);
				else window.close();
			}  
			else window.close(); 
		}
		</script>
		<style>
			a:hover{cursor:pointer !important;}
		</style>
	</head>
<body style="width:100%;">
	<!-- #cognus_result -->
<div id="cognus_result">
<table class="table_form" style="width:100%">
<colgroup>
<col style="width:15%" /><col style="width:35%" /><col style="width:15%" /><col style="width:35%" />
</colgroup>
<tr>
	<th class="th_form">제목</th>
	<td class="td_form" colspan="3">${notice.title}</td>
	</td>
</tr>
<c:choose>
<c:when test="${fileList[0]!=null}">
<tr style="height:340px;">
	<th class="th_form" style="height:340px;">내용</th>
	<td class="td_form" colspan="3" style="height:340px;"><div style="width:670px; height:340px; overflow:auto">${notice.content}</div></td>
</tr>
<tr style="height:50px;">
	<th class="th_form">첨부파일</th>
	<td colspan="3" class="td_form" style="width:670px;">
	<div style="width:670px; height:50px; overflow:auto">
	 <c:forEach var="file" items="${fileList}" varStatus="loop">
		<SPAN style="font-weight: bold; padding-right: 10px;"></SPAN>${file.file_org_name }<a href="../../cms/board/download?fileId=${file.file_id}"><img src="../../resource/css/images/img_icons/down.png" border="0" alt="${file.file_org_name}" /></a>
		<br>
	</c:forEach>
	</div> 
	</td>
</tr>
</c:when>
<c:otherwise>
<tr style="height:390px;">
	<th class="th_form" style="height:420px;">내용</th>
	<td class="td_form" colspan="3" style="height:420px;"><div style="width:670px; height:420px; overflow:auto">${notice.content}</div></td>
</tr>
</c:otherwise>
</c:choose>
</table>
</div><!-- // #cognus_result -->
<div style="text-align:right;padding-top: 3px;position:absolute;bottom:10px;right:10px;"><span><input id="cookie_checkbox" type="checkbox" value="y" style="vertical-align: middle;" onchange="popupClose(${notice.boardno});"/> 일주일 동안 보지 않기 <a onclick="popupClose(${notice.boardno});">[확인]</a></span></div>
</body>
</html> 