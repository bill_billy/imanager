<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head> 
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>게시판</title>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/ktoto/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/ktoto/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
		<script src="${WEB.JSUTIL}/common.js"></script>
		
		<script language="javascript" type="text/javascript">
		var msg = "${param.msg}";
		var boardType = "${param.boardType}";
		var loginID = "${sessionScope.loginSessionInfo.userId}";
		var page = "";
		var countPage = "";
		var totPage = "";
		$(document).ready(function(){
    		init(); 
    	}); 
		function init(){
			$("#startDat").jqxInput({placeHolder: "", height: 21, width: 120, minLength: 1, theme:'blueish' });
			$("#endDat").jqxInput({placeHolder: "", height: 21, width: 120, minLength: 1, theme:'blueish' });
			$( "#startDat" ).datepicker({
				changeYear: false,
				showOn: "button",
				buttonImage: "../../resources/img/calendar.gif",
				buttonImageOnly: true,
				dateFormat: "yy-mm-dd",
				onSelect:function(dateText){
					if($("#endDat").val() != ""){
						if($("#endDat").val() < $("#startDat").val()){
							alert("종료일자가 시작일자보다 과거입니다.");
							$("#endDat").val("");
						}else{
							search();
						}	
					}
				}
			});
    		$( "#endDat" ).datepicker({
    			changeYear: false,
				showOn: "button",
				buttonImage: "../../resources/img/calendar.gif",
				buttonImageOnly: true,
				dateFormat: "yy-mm-dd",
				onSelect:function(dateText){
					if($("#endDat").val() < $("#startDat").val() || $("#startDat").val() == ""){
						alert("종료일자가 시작일자보다 과거입니다.");
						$("#endDat").val("");
					}else{
						search();
					}	
				}
			});
			
			$("#btnSearch").jqxButton({ width: '',  theme:'blueish'}); 
            $("#btnNew").jqxButton({ width: '',  theme:'blueish'}); 
			$("#btnModi").jqxButton({ width: '',  theme:'blueish'});   
			$("#searchText").jqxInput({placeHolder: "", height: 22, width: 250, minLength: 1, theme:'blueish' });
			$("#searchText").keydown(function(event){
				if(event.keyCode == 13){
					return search();
				}
        	});
			if(msg != ""){
				$i.dialog.alert('SYSTEM', decodeURIComponent(msg));
			}
			makeGrid();
			//search();
		}
		function makeGrid(){
			$i.post("./getBoardList", {boardType:boardType, titleName:$("#searchText").val(), startDat:$("#startDat").val(), endDat:$("#endDat").val()}).done(function(data){
				var source =
	            {
	                datatype: "json",
	                datafields: [
	                	{ name: 'BOARD_TYPE', type: 'string'},
	                    { name: 'BOARDNO', type: 'string'},
	                    { name: 'BOARD_GROUP', type: 'string'},
	                    { name: 'TITLE', type: 'string'},
	                    { name: 'USERID', type: 'string'},
	                    { name: 'NAME', type: 'string'},
	                    { name: 'CREATEDATE', type: 'string'},
	                    { name: 'HITCOUNT', type: 'string'},
	                    { name: 'FILE_CNT', type: 'string'}
	                ],
	                localdata: data,
	                updaterow: function (rowid, rowdata, commit) {
	                    commit(true);
	                }
	            };
				var noScript = function(row,columnfield,value){
					var newValue = $i.secure.scriptToText(value);
					return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + newValue + '</div>';
				};
				var alginRight = function (row, columnfield, value) {//right정렬
					return '<div id="userName-' + row + '"style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
				};
				var alginLeft = function (row, columnfield, value) {//left정렬
					return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
				}; 
				var detailRow = function (row, columnfield, value, defaultHtml, property, rowdata ) {
					var newValue = $i.secure.scriptToText(value);
					var boardNO = rowdata.BOARDNO;
					var boardTyp = rowdata.BOARD_TYPE
					var link = "<a href=\"javaScript:detail('"+boardTyp+"','"+boardNO+"')\" style='color:black;text-decoration:underline;'>" + newValue + "</a>";
	            	
	            	return "<div style='text-align:left; margin:4px 0px 0px 10px; color:black;'>" + link + "</div>";
				}
	            var dataAdapter = new $.jqx.dataAdapter(source, {
	                downloadComplete: function (data, status, xhr) { },
	                loadComplete: function (data) { },
	                loadError: function (xhr, status, error) { }
	            });
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#gridBoardList").jqxGrid(
	            {
	                 width: '100%',
	                height: '100%',
					altrows:true,
					pageable: true,
					pageSize: 100,
				    pageSizeOptions: ['100', '200', '300'],
	                source: dataAdapter,
					theme:'blueish',
					columnsheight:25 ,
					rowsheight: 25,
	                columnsresize: true,
	                sortable:true,
	                columns: [
	                  { text: 'No', datafield: 'BOARDNO', width: '5%', align:'center', cellsalign: 'center'},
	                  { text: '제목', datafield: 'TITLE', width: '67%', align:'center', cellsalign: 'left' , cellsrenderer: detailRow},
					  { text: '등록일', datafield: 'CREATEDATE', width: '7%', align:'center', cellsalign: 'center'},
					  { text: '작성자', datafield: 'NAME', width: '7%', align:'center', cellsalign: 'center'},
					  { text: '첨부', datafield: 'FILE_CNT', width: '7%', align:'center', cellsalign: 'center'},
					  { text: '조회수', datafield: 'HITCOUNT', width: '7%', align:'center', cellsalign: 'center'}
	                ]
	            });
			});
		}
		function search(){
			$('#gridBoardList').jqxGrid("clearselection");
			$i.post("./getBoardList", {boardType:boardType, titleName:$("#searchText").val(), startDat:$("#startDat").val(), endDat:$("#endDat").val()}).done(function(data){
				var $jqxGrid = $('#gridBoardList');
				$jqxGrid.jqxGrid('source')._source.localdata = data;
				$jqxGrid.jqxGrid('updatebounddata');
			});
		}
		function detail( boardTyp, boardNo){
			$("#hiddenBoardNO").val(boardNo);
			$("#hiddenBoardTyp").val(boardTyp);
			$i.post("./setBoardHitCount",{boardNo:boardNo, boardType:boardTyp}).done(function(res){
				$i.post("./getBoardDetail",{boardType:boardTyp, boardNO:boardNo}).done(function(data){
					if(data.returnCode == "SUCCESS"){
						$("#boardName").html($i.secure.scriptToText(data.returnObject.TITLE));
						$("#txtAreaContent").html($i.secure.scriptToText(data.returnObject.CONTENT));
						$("#createDate").html(data.returnObject.CREATEDATE);
						if(data.returnObject.USERID == loginID){
							$("#btnModi").show();
						}else{
							$("#btnModi").hide();
						}
					}
				});
				$i.post("./getBoardFileList",{tableNM:boardTyp, boardNO:boardNo}).done(function(data){
					if(data.returnCode == "SUCCESS"){
						$("#fileList > div").remove();
						var html = "";
						for(var i=0; i<data.returnArray.length; i++){
							html += "<div class='cell'><a href=\"javaScript:filedownLoad('"+data.returnArray[i].FILE_ID+"')\"><span style='font-weight: bold; padding-right: 10px;'>"+data.returnArray[i].FILE_ORG_NAME+"</SPAN><img src='../../resources/cmresource/css/iplanbiz/theme/ktoto/img/btn_save.png' border='0' alt='' /></a></div>";
						}
						$("#fileList").append(html);
					}
				});
				boardReplyData('1');
			});
		}
		function boardReplyData(clickNum){
			if(clickNum != ""){
				$("#hiddenClickNum").val(clickNum);
			}
			var countPage = "3";
			var startNum = parseInt(parseInt($("#hiddenClickNum").val()-1) * countPage + 1);
			var endNum = parseInt($("#hiddenClickNum").val()*countPage);
			$i.post("./getBoardReplyList",{boardType:$("#hiddenBoardTyp").val(), boardNo:$("#hiddenBoardNO").val(), startNum:startNum, endNum:endNum}).done(function(data){
				var html = "";
				$("#boardReply > div").remove();
				if(data.returnArray.length > 0){
					totPage = data.returnArray[0].TOT_CNT;
					html += "<div class='blockComment_t'>댓글 <span class='comment_num'>("+data.returnArray[0].TOTAL+")</span></div>";
					html += "<div class='Comment_write'>";
					html += "<table summary='등록테이블'>";
					html += "<caption></caption>";
					html += "<tr>";
					html += "<td><textarea id='txtAreaReply' title='입력창' cols='' rows='3' ></textarea></td>";
					html += "<td width='74' class='textRight'><a class='button_re' href=\"javascript:insertReply('1');\"><span>저장</span></a></td>";
					html += "</tr>";
					html += "</table>";
// 					html += "<div class='Comment_write_num'>0/150</div>";
					html += "</div>";
					for(var i=0; i<data.returnArray.length;i++){
						if(i==0){
							html += "<div class='blockComment_c' id='reply"+data.returnArray[i].P_STEP+"' style='width:100%;float:left;padding-top:20px;'>";
						}else{
							html += "<div class='blockComment_c' id='reply"+data.returnArray[i].P_STEP+"' style='width:100%;float:left;'>";	
						}
						html += "<div class='blockCommentInfo'>";
						html += "<span class='blockCommentInfo_name' ><a href='#'>"+data.returnArray[i].NAME+"</a></span>";
						html += "<span >"+data.returnArray[i].DEPT+" </span>"
						html += "<span >"+data.returnArray[i].CREATEDATE+" </span>";
						if(data.returnArray[i].USERID == loginID){
							html += "<span class='rebtn' style='float:right;'>"
							html += "<a class='ic_modi' href=\"javascript:updateReply('"+data.returnArray[i].P_STEP+"','"+clickNum+"');\" title='수정'><img src='../../resources/cmresource/css/iplanbiz/theme/ktoto/img/btn_modi.gif' alt='수정'></a>"
							html += "<a class='ic_delete' href=\"javascript:deleteReply('"+data.returnArray[i].P_STEP+"','"+clickNum+"');\" title='삭제'><img src='../../resources/cmresource/css/iplanbiz/theme/ktoto/img/btn_del.gif' alt='삭제'></a>";
							html += "</span>";	
						}
						html += "</div>";
						html += "<p id='content"+data.returnArray[i].P_STEP+"' style='float:left;'>"+data.returnArray[i].CONTENT+"</p>";
						html += "</div>";	
					}
// 					html += "<div class='pageNum'>";
// 					html += "<ul>";
// 					html += "<li><a class='btn_page_first' href=\"javascript:allBackNum();\" ><span>첫페이지</span></a></li>";
// 					html += "<li><a class='btn_page_pre' href=\"javascript:backNum();\"><span>이전페이지</span></a></li>";
// 					for(var j=0; j<data.returnArray[0].TOT_CNT; j++){
// 						if(j==0){
// 							if(j == parseInt(clickNum-1)){
// 								html += "<li><span>"+parseInt(j+1)+"</span></li>";	
// 							}else{
// 								html += "<li><span class='liFirst'><a href=\"javascript:boardReplyData('"+parseInt(j+1)+"');\">"+parseInt(j+1)+"</a></span></li>";
// 							}
// 						} else if(j==parseInt(data.returnArray[0].TOT_CNT-1)){
// 							if(j == parseInt(clickNum-1)){
// 								html += "<li><span>"+parseInt(j+1)+"</span></li>";	
// 							}else{
// 								html += "<li><span class='liLast'><a href=\"javascript:boardReplyData('"+parseInt(j+1)+"');\">"+parseInt(j+1)+"</a></span></li>";	
// 							}
// 						}else{
// 							if(j == parseInt(clickNum-1)){
// 								html += "<li><span>"+parseInt(j+1)+"</span></li>";	
// 							}else{
// 								html += "<li><span><a href=\"javascript:boardReplyData('"+parseInt(j+1)+"');\">"+parseInt(j+1)+"</a></span></li>";
// 							}
// 						}
// 					}
// 					html += "<li><a class='btn_page_next' href=\"javascript:nextNum();\"><span>다음페이지</span></a></li>";
// 					html += "<li><a class='btn_page_last' href=\"javascript:allNextNum();\"><span>마지막페이지</span></a></li>";
// 					html += "</ul>";
// 					html += "</div>";
				}else{
					html += "<div class='blockComment_t'>댓글 <span class='comment_num'>(0)</span></div>";
					html += "<div class='Comment_write'>";
					html += "<table summary='등록테이블'>";
					html += "<caption></caption>";
					html += "<tr>";
					html += "<td><textarea id='txtAreaReply' title='입력창' cols='' rows='3' ></textarea></td>";
					html += "<td width='74' class='textRight'><a class='button_re' href=\"javascript:insertReply('1');\"><span>저장</span></a></td>";
					html += "</tr>";
					html += "</table>";
// 					html += "<div class='Comment_write_num'>0/150</div>";
					html += "</div>";
				}
				
				$("#boardReply").append(html);
				$("#gridBoardList").jqxGrid("refresh");
			});
		}
		function nextNum(){
			var nowNum = $("#hiddenClickNum").val();
			if(nowNum >= totPage){
				$i.dialog.error("SYSTEM", "마지막 페이지 입니다.");
			}else{
				boardReplyData(nowNum+1);	
			}
		}
		function allNextNum(){
			var nowNum = $("#hiddenClickNum").val();
			if(nowNum >= totPage){
				$i.dialog.error("SYSTEM", "마지막 페이지 입니다.");
			}else{
				boardReplyData(totPage);
			}
		}
		function backNum(){
			var nowNum = $("#hiddenClickNum").val();
			if(nowNum <= "1"){
				$i.dialog.error("SYSTEM", "처음 페이지 입니다.");
			}else{
				boardReplyData(nowNum-1);
			}
		}
		function allBackNum(){
			var nowNum = $("#hiddenClickNum").val();
			if(nowNum <= "1"){
				$i.dialog.error("SYSTEM", "처음 페이지 입니다.");
			}else{
				boardReplyData("1");	
			}
		}
		function updateReply(pStep, clickNum){
			$("#txtAreaReply").val($("#content"+pStep).html());
			$("#hiddenReplyNo").val(pStep);
			$("#hiddenClickNum").val(clickNum);
		}
		function insertReply(clickNum){
			if($("#hiddenReplyNo").val() != ""){
				$i.post("./updateReply", {boardType:$("#hiddenBoardTyp").val(), boardNo:$("#hiddenBoardNO").val(),pStep:$("#hiddenReplyNo").val(),content:$("#txtAreaReply").val()}).done(function(data){
					if(data.returnCode == "SUCCESS"){
						$i.dialog.alert("SYSTEM", data.returnMessage);
						boardReplyData($("#hiddenClickNum").val());
						$("#hiddenReplyNo").val("");
					}else{
						$i.dialog.alert("SYSTEM", data.returnMessage);
						$("#txtAreaReply").val("");
					}
				});
			}else{
				$i.post("./insertReply", {boardType:$("#hiddenBoardTyp").val(), boardNo:$("#hiddenBoardNO").val(),content:$("#txtAreaReply").val()}).done(function(data){
					if(data.returnCode == "SUCCESS"){
						$i.dialog.alert("SYSTEM", data.returnMessage);
						boardReplyData(clickNum);
					}else{
						$i.dialog.alert("SYSTEM", data.returnMessage);
						$("#txtAreaReply").val("");
					}
				});
			}
		}
		function deleteReply(pStep, clickNum){
			$("#hiddenReplyNo").val(pStep);
			$i.post("./removeReply", {boardType:$("#hiddenBoardTyp").val(), boardNo:$("#hiddenBoardNO").val(),pStep:pStep}).done(function(data){
				if(data.returnCode == "SUCCESS"){
					$i.dialog.alert("SYSTEM", data.returnMessage);
					boardReplyData(clickNum);
				}else{
					$i.dialog.alert("SYSTEM", data.returnMessage);
				}
			});
		}
		function filedownLoad(fileID){
			location.replace("../../cms/board/download?fileId="+fileID);
		}
		function  goForm(){
			location.replace("../../cms/boardv2/crud?type=N&boardType="+boardType);
		}
		function goDetail(){
			location.replace("../../cms/boardv2/crud?type=U&boardNO="+$("#hiddenBoardNO").val()+"&boardType="+boardType);
		}
		function formSearch(){
			$("#search").submit();		
		}
		function dayCheck(){
			if($("#endDay1").val() < $("#startDay1").val()){
				alert("종료일자가 시작일자보다 과거입니다.");
				$("#endDay1").val("");
			}else{
				searchStart();
			}
		}
		function checkInput(id, size, gubn){
    		if(byteCheck($("#"+id).val()) > size){
    			var length = "";
    			if(gubn == "en"){
    				length = parseInt((size / 1));
    				alert( length + "자(영문 기준)의 영문/_ 혼용만 가능합니다.");
    			}else if(gubn == "all"){
    				length = parseInt((size / 3));
    				alert( length + "자(한글 기준)의 한글/영문/특수문자 혼용만 가능합니다.");
    			}else if(gubn =="num"){
    				length = parseInt((size / 1));
    				alert( length + "자의 숫자만 가능합니다.");
    			}else if(gubn =="kor"){
    				length = parseInt((size / 3));
    				alert( length + "자(한글 기준)의 한글만 가능합니다.");
    			}
    			$("#"+id).focus();
    			return false;
    		}
    	}
	 	function byteCheck(code) {
			var size = 0;
			for (i = 0; i < code.length; i++) {
				var temp = code.charAt(i);
				if (escape(temp) == '%0D')
			   		continue;
			  	if (escape(temp).indexOf("%u") != -1) {
			   		size += 3;
			  	} else {
			   		size++;
			 	}
		 	}
		 	return size;
		}
		function setPagerLayout(selector) {
			
			var pagesize = $('#'+selector).jqxGrid('pagesize');
			
			var w = 49; 
				
			if(pagesize<100) {
				w = 44;
			} else if(pagesize>99&&pagesize<1000) {
				w = 49;
			} else if(pagesize>999&&pagesize<10000) {
				w = 54;
			}
			
			//디폴트 셋팅
			$('#gridpagerlist'+selector).jqxDropDownList({ width: w+'px' });
			
			//체인지 이벤트 처리
			$('#'+selector).on("pagesizechanged", function (event) {
				var args = event.args;
				
				if(args.pagesize<100) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '44px' });
				} else if(args.pagesize>99 && args.pagesize<1000) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '49px' });
				} else if(args.pagesize>999 && args.pagesize<10000) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '54px' });
				} else {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: 'auto' });
				}
				
			});
		}
		</script>
		<style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
			.ui-datepicker-trigger{
				float:left;
				margin-top:5px;
				margin-left:5px;
			}
			
		</style>
		<style>
			.border_top{border-top:1px solid #dedede}
		</style>
	</head>
	<body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1020px; margin:0 1%;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<div class="label type1 f_left">작성일</div>
				<div class="cell">
					<input id="startDat" type="text" class="input type3 t_center f_left" style="width:100px; margin:3px 0;" />
					<div class="f_left m_l5 m_r5 m_t2">&sim;</div>
					<input id="endDat" type="text" class="input type3 t_center f_left" style="width:100px; margin:3px 0;" />
				</div>    
				<div class="label type1 f_left">제목:</div>
				<div class="input f_left">
					<input type="text" id="searchText" style="margin:2px 5px;"/>
				</div>
				<div class="group_button f_right">
					<div class="button type1 f_left">
						<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();"/>
					</div>
					<div class="button type1 f_left">
						<input type="button" value="신규" id='btnNew' width="100%" height="100%" onclick="goForm();"/>
					</div>
				</div>
			</div>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:100%; margin:0;">
					<div class="grid f_left" style="width:100%; height:287px;">
						<div id="gridBoardList"></div>
					</div>
				</div>
				<div class="content f_left" style=" width:100%; margin:10px 0;">
					<div class="group f_left  w100p m_b5">
						<div class="label type2 f_left">게시판 상세</div>
						<div class="group_button f_right">
							<input type="hidden" id="hiddenBoardNO" />
							<input type="hidden" id="hiddenBoardTyp" />
							<input type="hidden" id="hiddenReplyNo" />
							<input type="hidden" id="hiddenClickNum" />
							<div class="button type2 f_left" style="margin-bottom:0;">
								<input type="button" value="수정" id='btnModi' width="100%" height="100%" onclick="goDetail();" />
							</div>
						</div>
					</div>
					<div class="table  f_left" style="width:100%; margin:0; border-top:2px solid #30638d; border-left:0; border-right:0;">
						<table width="100%" cellspacing="0" cellpadding="0" border="0">
							<tbody>
								<tr>
									<td style="width:5%;height:20px; background:#5a8cb6; color:#ddeeff; font-size:1.0em; font-weight:bold; text-indent:10px; padding:3px 0 6px 0; border-right:0; text-align:center;">제목</td>
									<td style="width:85%;height:20px; background:#5a8cb6; color:#ffffff; font-size:1.1em; font-weight:bold; text-indent:10px; padding:3px 0 6px 0; text-align:left;" id="boardName"></td>
									<td style="width:10%; height:20px; background:#e7f1f9; color:#30638d; font-size:1.0em; text-indent:10px; padding:3px 0 6px 0; text-align:center;" id="createDate"></td>
								</tr>
								<tr>
									<td colspan="3">
										<div class="dhtml_area" style=" padding:5px !important; height:220px;overflow-y:scroll;" id="txtAreaContent"></div>
									</td>
								</tr>
								<tr>
									<td colspan="2" style=" padding:10px !important;" id="fileList"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="content f_left" style=" width:100%; margin:10px 0;">
					<div class="blockComment" id="boardReply">
						
					</div>
				</div>
			</div>
		</div>
	</body>
</html> 