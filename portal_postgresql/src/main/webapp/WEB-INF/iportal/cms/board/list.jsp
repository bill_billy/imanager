<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>게시판</title>
        
       <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	    
        <script>
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init(); 
        	}); 
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		$("#newButton").jqxButton({width:'', theme:'blueish'});
        		$( "#startDay1" ).datepicker({
    				changeYear: false,
    				showOn: "button",
    				buttonImage: "../../resources/img/img_icons/calendarIcon.png",
    				buttonImageOnly: true,
    				dateFormat: "yy-mm-dd"
    			});
        		$( "#endDay1" ).datepicker({
        			changeYear: false,
    				showOn: "button",
    				buttonImage: "../../resources/img/img_icons/calendarIcon.png",
    				buttonImageOnly: true,
    				dateFormat: "yy-mm-dd"
    			});
        		var dat  = [
					{value:'title',text:'제목'},
					{value:'content',text:'내용'},
					{value:'name',text:'작성자'},
					
				];
				// prepare the data
				var source =
				{
					datatype: "json",
					datafields: [
						{ name: 'value' },
						{ name: 'text' }
					],
					id: 'id',
					localdata:dat,
				//	url: url,
					async: false
				};
				var dataAdapter = new $.jqx.dataAdapter(source);

				// Create a jqxComboBox
				$("#pSelect").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "text", valueMember: "value", dropDownWidth: 150, dropDownHeight: 80, width: 150, height: 21, theme:'blueish'});
				$("#input").jqxInput({placeHolder: "", height: 21, width: 250, minLength: 1, theme:'blueish' });
				$("#startDay1").jqxInput({placeHolder: "", height: 21, width: 120, minLength: 1, theme:'blueish' });
				$("#endDay1").jqxInput({placeHolder: "", height: 21, width: 120, minLength: 1, theme:'blueish' });
				
				//searchStart();
				$("#input").keydown(function(event){
					if(event.keyCode == 13){
						return searchStart();
					}
		        		
	        	});

				searchStart();
        	}
        	function  goForm(){
    			location.replace("../../cms/board/form?type=I&boardType=${param.boardType}");
    			return false;
    		}
    		function goDetail(boardNo, boardType){
    			location.replace("../../cms/board/get?boardNo="+boardNo+"&boardType=" + boardType);
    		}
        	function searchStart(){
        		var tableNm = "${param.boardType}";
        		var division = "";
    			var title = "";
    			var content = "";
    			var name = "";
    			var gubn = "";
    			var startDay = "";
    			var endDay = "";
    			if($("#startDay1").val() == "" && $("#endDay1").val() == ""){
    				gubn = "NULL";
    				startDay = "0000-00-00";
    				endDay = "9999-99-99";
    			}else{
    				gubn = "NOT";
    				startDay = $("#startDay1").val();
    				endDay = $("#endDay1").val();
    			}
    			if($("#pSelect").val() == 'title'){
    				title = $("#input").val();
    			}else if($("#pSelect").val() == 'content'){
    				content = $("#input").val();
    			}else if($("#pSelect").val() == 'name'){
    				name = $("#input").val();
    			}
        		
        		var boardData = $i.post("./getBoardList", { TABLE_NM : tableNm, TITLE : title, CONTENT : content,
        			NAME : name, GUBN : gubn,START_DAT : startDay, END_DAT : endDay});
        		boardData.done(function(bData){
        			console.log(bData);
        			var source =
		            {
		                datatype: "json",
		                datafields: [
							{ name: 'RNUM', type: 'string' },
							{ name: 'BOARD_TYPE', type: 'string' },
							{ name: 'BOARDNO', type: 'string' },
							{ name: 'P_BOARDNO', type: 'string' },
							{ name: 'BOARD_GROUP', type: 'string' },
							{ name: 'ANSWER_LEVEL', type: 'string' },
							{ name: 'TITLE', type: 'string' },
							{ name: 'CONTENT', type: 'string' },
							{ name: 'USERID', type: 'string' },
							{ name: 'NAME', type: 'string' },
							{ name: 'FILECOUNT', type: 'number' },
							{ name: 'HITCOUNT', type: 'string' },
							{ name: 'NOTICE_YN', type: 'string' },
							{ name: 'COUNT', type:'number'},
							{ name: 'CREATEDATE', type: 'string' }
						],
		                localdata: bData.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
        			 var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var infoRender = function(row,datafield,value){
	 	            	var informYn = $("#gridBoardList").jqxGrid("getrowdata",row).NOTICE_YN;
	 	            	if(informYn == "Y"){
	 	            		return "<div style='text-align:center; font-weight:bold; margin:4px 0px 0px 10px;'>" + value + "</div>";
	 	            	}else{
	 	            		return "<div style='text-align:center; margin:4px 0px 0px 10px;'>" + value + "</div>";
	 	            	}
	 	            };
					var detailView = function (row, columnfield, value, rowdata) {//left정렬
						var boardNo = $("#gridBoardList").jqxGrid("getrowdata", row).BOARDNO;
						var boardType = $("#gridBoardList").jqxGrid("getrowdata", row).BOARD_TYPE;
						var count = $("#gridBoardList").jqxGrid("getrowdata", row).COUNT;
						var informYn = $("#gridBoardList").jqxGrid("getrowdata",row).NOTICE_YN;
						if(count > 0){
							if(informYn == "Y"){
		            			var link = "<a href=\"javaScript:goDetail('"+boardNo+"','"+boardType+"')\" style='font-weight:bold;color:black;text-decoration:underline !important;'>" + value.replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/'"'/g,'&quot;') +"["+count+"] </a>";
							}else{
		            			var link = "<a href=\"javaScript:goDetail('"+boardNo+"','"+boardType+"')\" style='color:black;text-decoration:underline !important;'>" + value.replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/'"'/g,'&quot;') +"["+count+"] </a>";

							}
						}else{
							if(informYn == "Y"){
		            			var link = "<a href=\"javaScript:goDetail('"+boardNo+"','"+boardType+"')\" style='font-weight:bold;color:black;text-decoration:underline !important;'>" + value.replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/'"'/g,'&quot;') + "</a>";	
		            		}else{
		            			var link = "<a href=\"javaScript:goDetail('"+boardNo+"','"+boardType+"')\" style='color:black;text-decoration:underline !important;'>" + value.replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/'"'/g,'&quot;') + "</a>";	

		            		}
						}
		            	return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + link + '</div>';
		            };
					var dataAdapter = new $.jqx.dataAdapter(source); 
					$("#gridBoardList").jqxGrid(
				            {
				                width: '100%',
				                height: '100%',
								altrows:true,
								pageable: true,
								pagesize: 100,
								pagesizeoptions:['100', '200', '300'],
				                source: dataAdapter,
								theme:'blueish',
								columnsheight:30 ,
								rowsheight: 30,
				                columnsresize: true,
			     				columns:[
												{ text: 'No', datafield: 'BOARDNO', width: '7%', align:'center', cellsalign: 'center',cellsrenderer:infoRender},
												{ text: '제목', datafield: 'TITLE', width: '58%', align:'center', cellsalign: 'left',cellsrenderer:detailView},
												{ text: '작성자', datafield: 'NAME', width: '10%', align:'center', cellsalign: 'center' ,cellsrenderer:infoRender},
												{ text: '첨부', datafield: 'FILECOUNT', width: '7%', align:'center', cellsalign: 'center',cellsrenderer:infoRender},
												{ text: '등록일', datafield: 'CREATEDATE', width: '10%', align:'center', cellsalign: 'center',cellsrenderer:infoRender},
												{ text: '조회수', datafield: 'HITCOUNT', width: '8%', align:'center', cellsalign: 'right',cellsrenderer:infoRender}
			     				               ]
						});
        		});
        		
        	}
        </script>
        <style type="text/css">
			
			.ui-datepicker-trigger{
				float:left;
				margin-top:5px;
				margin-left:5px;
			}
			
		</style>
    </head>
    <body class='blueish'>
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%; min-width:1160px; margin:0 10px;">
			<div class="header f_left" style="width:100%; height:58px; margin:10px 0">
				<form id="searchForm" name="searchForm" action="./list">
					<div style=" float:left;width:100%;">
						<div class="label type1 f_left">등록일</div>
						<div style="margin:0 10px;" class="f_left">
							<input type="text"  id="startDay1" class="f_left"  style="height:16px; width:120px; text-align:center; "/>
							<span style="float:left; vertical-align:middle; padding:7px 20px 0px 20px;">~</span>
							<input type="text"  id="endDay1" class="f_left"  style="height:16px; width:120px; text-align:center; " onChange="searchStart();" />
						</div>
						<div class="f_right">
						<div class="button type1 f_left">
							<input type="button" value="신규" id='newButton' width="100%" height="100%" onclick="goForm();" />
						</div>
					</div>
					</div>
					<div style=" float:left;width:100%; margin-top:10px;">
						<div class="label type1 f_left" style="margin-right:12px;">검색</div>
						<div style='float:left; margin:0 10px;' id='pSelect' ></div>
						<div  style='float: left; margin-left:10px; margin-right:0px;' >
							<input type="text" id="input" name="input" />
						</div>
						<div cass="image-button" style="float:left;margin:0; padding:0;"><img src="../../resources/img/img_icons/search_icon.png" alt="조회" style="cursor:pointer;" onclick="searchStart();"></div>
					</div>
				</form>
			</div>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style="width:100%; margin:0 0%;">
					<div class="grid f_left" style="width:100%; height:500px;">
						<div id="gridBoardList"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>