<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>게시판 등록</title>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
<link rel="stylesheet" href="../../resource/css/iplanbiz/table.css" type="text/css"/>
<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>

<link rel="stylesheet" href="../../resource/css/iplanbiz/table.css" type="text/css" />
<script src="../../resources/cmresource/js/iplanbiz/core/iplanbiz.web.include.jsp"></script>
<script src="../../resources/cmresource/js/thirdparty/LGPL/v2/smartEditor2/js/HuskyEZCreator.js" charset="UTF-8"></script>
<script language="javascript" type="text/javascript">
var oEditors = [];
function initSmartEditor(id) {
	nhn.husky.EZCreator.createInIFrame({
		oAppRef : oEditors,
		elPlaceHolder : id,
		sSkinURI : "../../resources/cmresource/js/thirdparty/LGPL/v2/smartEditor2/SmartEditor2Skin.html",
		htParams : {
			bUseToolbar : true,
			fOnBeforeUnload : function() {
			}
		},
		fOnAppLoad : function() {
			//oEditors.getById["ir1"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
		},
		fCreator : "createSEditor2"
	});
}
function checkInput(id, size, gubn) {
	if (byteCheck($("#" + id).val()) > size) {
		var length = "";
		if (gubn == "en") {
			length = parseInt((size / 1) - 5);
			$i.dialog.warning('SYSTEM',length + "자의 영문/_ 혼용만 가능합니다.");
		} else if (gubn == "all") {
			length = parseInt((size / 3) - 5);
			$i.dialog.warning('SYSTEM',length + "자의 한글/영문/특수문자 혼용만 가능합니다.");
		} else if (gubn == "num") {
			length = parseInt((size / 1) - 2);
			$i.dialog.warning('SYSTEM',length + "자의 숫자만 가능합니다.");
		} else if (gubn == "kor") {
			length = parseInt((size / 3) - 5);
			$i.dialog.warning('SYSTEM',length + "자의 한글만 가능합니다.");
		}
		$("#" + id).focus();
		return false;
	}
}
function deleteFIle(inputName) {
	$("#" + inputName).remove();
}
function byteCheck(code) {
	var size = 0;
	for (i = 0; i < code.length; i++) {
		var temp = code.charAt(i);
		if (escape(temp) == '%0D')
			continue;
		if (escape(temp).indexOf("%u") != -1) {
			size += 3;
		} else {
			size++;
		}
	}
	return size;
}
function checkSize(idx) {
	var fileLength = $("input[name=file]").length;
	var fileUpload = $("[name='fileUpload']").length;
	if ($("[name='fileUpload']").length >= 5) {
		$i.dialog.warning('SYSTEM',"파일업로드는 5개 이상 할 수 없습니다.");
	} else {
		if ($("input[name='file']")[idx].value != "") {
			var FileFilter = /\.(txt|zip|jpg|png|xls|xlsx|ppt|pptx|doc|docx|hwp|pdf)$/i;
			var fileValue = $("input[name='file']")[idx].value
					.toLowerCase();
			var fileCheck = false;
			var fileSize = $("input[name='file']")[idx].files[0].size;
			var defaultSize =1000;
			if (fileValue.match(FileFilter)) {
				fileCheck = true;
			}
			if (fileCheck == true) {
				/*if (parseInt(fileSize) > parseInt(defaultSize)) {
					alert("업로드 용량이 기본 용량보다 큽니다.");
					$("#fileName" + idx).val("");
					$("input[name='file']")[idx].value = "";
				} else {
					$("#fileName" + idx).val(
							$("input[name='file']")[idx].value);
				}*/
				$("#fileName" + idx).val(
						$("input[name='file']")[idx].value);
			} else {
				$i.dialog.warning('SYSTEM',"업로드가 가능한 확장자가 아닙니다.");
			}
		}
	}
}
function addFile() {
	var fileLength = $("input[name=file]").length;
	var fileUpload = $("[name='fileUpload']").length;
	var fileNo = fileLength;
	var fileName = "fileName" + fileNo;
	var fileName1 = "fileName1" + fileNo;
	if (fileLength >= 5 || $("[name='fileUpload']").length >= 5
			|| parseInt(fileLength + fileUpload) >= 5) {
		$i.dialog.warning('SYSTEM',"파일업로드는 5개 이상 할 수 없습니다..");
	} else {
		var html = "";
		html += '<div id="'+fileName1+'" >';
		html += '<div  style="float:left; margin-bottom:5px;">';
		html += '<input type="text" id="'+fileName+'" class="file_input_wide_textbox" readonly style="width:720px;">';
		html += '<div class="file_input_div">';
		html += '<input type="button" value="찾아보기" class="file_input_button" />';
		html += '<input type="file" name="file" class="file_input_hidden" accept=".zip,.jpg,.png,.xls,.xlsx,.ppt,.pptx,.doc,.docx,.hwp,.pdf" onchange="checkSize('
				+ fileNo + ');" />';
		html += '</div>';
		html += '</div>';
		html += '<span id="fileMinusButton" style="cursor:pointer;float:right;margin:0!important; padding-right:20px;"> <img src="../../resource/css/images/img_icons/icon-minus-small.png" height="15" alt="삭제버튼" class="button_minus_minus"  onclick="deleteFIle(\''
				+ fileName1 + '\'); "> </span> <br>';
		html += '</div>';

		$("#inputFile").append(html);
	}
}
function saveFrm() {
	oEditors.getById["content"].exec("UPDATE_CONTENTS_FIELD", []);
	if ($("#title").val() == null || $("#title").val() == "") {
		$i.dialog.warning('SYSTEM','<spring:message code="board.alert.title" javaScriptEscape="true" />');
		return false;
	}
	if (byteCheck($("#content").val()) > 4000) {
		$i.dialog.warning('SYSTEM','<spring:message code="board.alert._maxByte" javaScriptEscape="true" arguments="4000" />');
		return false;
	}
	if (byteCheck($("#title").val()) > 255) {
		$i.dialog.warning('SYSTEM','<spring:message code="board.alert._maxByte" javaScriptEscape="true" arguments="255" />');
		return false;
	}
	
	
	
	document.getElementById("board").submit();
}
$(document).ready(function() {
	
	$("#jqxButton1").jqxButton({
		width : '',
		theme : 'blueish'
	});
	$("#jqxButton2").jqxButton({
		width : '',
		theme : 'blueish'
	});
	initSmartEditor('content');
	var fileUpload = $("[name='fileUpload']").length;
	if (fileUpload >= 5) {
		$("#fileUploadTr").hide();
	} else {
		$("#fileUploadTr").show();
	}

});
function goList() {
	location.replace("../../cms/board/list?boardType=${board.board_type}");
}
</script>
<style>
.padding_table {
	padding-top: 10px;
	padding-bottom: 10px;
}
</style>
</head>
<body class='blueish i-DQ'>
	<div class="wrap" style="width:98% !important; min-width:1160px; margin:0 10px;">
		<div class="i-DQ-contents content"
			style="width: 100%; height: 100%; float: left;">
			<div class="label type2 f_left">작성하기</div>
			<div class="table_Area"
				style="width: 100%; height: 100%; float: left; margin-top: 0px; margin-bottom: 5px;">
				<form:form action="save" method="post" commandName="board"
					enctype="multipart/form-data">
					<input type="hidden" id="name" name="name"
						value="${sessionScope.loginSessionInfo.userName}" />
					<input type="hidden" id="userId" name="userId"
						value="${board.userid }" />
					<input type="hidden" id="type" name="type" value="${type}" />
					<input type="hidden" id="boardNo" name="boardNo"
						value="${board.boardno}" />
					<input type="hidden" id="boardType" name="boardType"
						value="${board.board_type }" />
					<input type="hidden" id="pBoardno" name="pBoardno"
						value="${board.p_boardno}" />
					<input type="hidden" id="answerLevel" name="answerLevel"
						value="${board.answer_level}" />
					<input type="hidden" id="boardGroup" name="boardGroup"
						value="${board.board_group }" />
					<table width="100%" class="i-DQ-table" cellspacing="0"
						cellpadding="0" border="0" style="margin-bottom: 0 !important;">
						<tr>
							<th class="required_icon" width="15%"><span
								style="color: red; font-size: 12px; vertical-align: -2px;">＊</span>제목</th>
							<td colspan="3" class="activetable"><input type="text"
								value="${fn:escapeXml(board.title)}" id="title" name="title"
								class="txt_style_txt"
								style="height: 17px; width: 95%; margin: 5px 0; padding-bottom: 3px;"
								onblur="checkInput('title','255','all');" /></td>
						</tr>
						<tr>
							<th>작성자</th>
							<td width="60%"><input type="text" value="${boardName}"
								id="i-user" class="inputNormal"
								style="height: 19px; width: 100%; margin: 5px 0; padding: 0; padding-bottom: 3px;"
								disabled /></td>
							<th>우선순위</th>
							<td><c:if test="${sessionScope.loginSessionInfo.isAdmin}">
									<label for="ranking" style="vertical-align: middle;"> <input
										type="radio" value="Y" name="noticeYn" id="noticeYn"
										style="vertical-align: middle; margin-top: 2px;"
										<c:if test="${board.notice_yn == 'Y'}">checked</c:if> />
									<spring:message code="board.text.priority.yes" />
									</label>
									<label for="ranking" style="vertical-align: middle;"> <input
										type="radio" value="N" name="noticeYn" id="noticeYn"
										style="vertical-align: middle; margin-top: 2px;"
										<c:if test="${board.notice_yn == 'N'}">checked</c:if> />
										<spring:message code="board.text.priority.no" />
									</label>
								</c:if></td>
						</tr>
						<tr>
							<th class="th_height2 required_icon"><span
								style="color: red; font-size: 12px; vertical-align: -2px;">＊</span>내용</th>
							<td colspan="3" style="padding: 5px !important"><textarea
									id="content" name="content" rows="20" cols="102"
									class="textarea_df" style="border: 0px; width: 100%;"
									onblur="checkInput('content','4000','all');">${board.content}</textarea>
							</td>
						</tr>
						<c:if test="${fileList != null}">
							<tr>
								<th>파일목록</th>
								<td colspan="3">
									<div
										style="margin: 5px 0; width: 100%; height: 80px; overflow-y: scroll;">
										<c:forEach var="file" items="${fileList}">
											<p name="fileUpload">
												${file.file_org_name }<a
													href="../../cms/board/deleteFile?fileId=${file.file_id}&boardNo=${board.boardno}&type=${type}&boardType=${board.board_type}"><img
													src="../../resource/css/images/img_icons/cancel.png" border="0"
													alt="${file.file_org_name}"/></a>
											</p>
										</c:forEach>
									</div>
								</td>
							</tr>
						</c:if>
						<tr id="fileUploadTr">
							<th>파일첨부</th>
							<td colspan="3">
								<div id="inputFile"
									style="margin: 5px 0; width: 100%; height: 80px; overflow-y: scroll;">
									<div id="fileName10">
										<div style="float: left; margin-bottom: 5px;">
											<input type="text" id="fileName0"
												class="file_input_wide_textbox" readonly
												style="width: 720px;">
											<div class="file_input_div">
												<input type="button" value="찾아보기" class="file_input_button" />
												<input type="file" name="file"
													accept=".zip,.jpg,.png,.xls,.xlsx,.ppt,.pptx,.doc,.docx,.hwp,.pdf"
													class="file_input_hidden" onchange="checkSize('0');" />
												<!-- 													<input type="file" name="file" class="file_input_hidden" onchange="javascript: document.getElementById('fileName1').value = this.value" /> -->
											</div>
										</div>
										<span id="filePlusButton"
											style="cursor: pointer; float: right; margin: 0 !important; padding-right: 10px;">
											<img
											src='../../resource/css/images/img_icons/icon-plus-small.png'
											alt="추가버튼" class="button_plus_minus" onclick="addFile();"
											height=15>
										</span> <br>
									</div>
								</div>
							</td>
						</tr>
					</table>
				</form:form>
			</div>
			<div class="group_button f_right" style="margin-top:10px !important;">
				<div class="button type2 f_left" style="margin-left:0px !important;">
					<input type="button" value="목록" id='jqxButton1' width="100%"
						height="100%" onclick="goList();" />
				</div>
				<div class="button type2 f_left" style="margin-left:10px !important;">
					<input type="button" value="저장" id='jqxButton2' width="100%"
						height="100%" onclick="saveFrm();" />
				</div>
			</div>
			
		</div>
	</div>
</body>
</html>
