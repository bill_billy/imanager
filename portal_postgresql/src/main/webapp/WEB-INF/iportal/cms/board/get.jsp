<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<% String contextPath = request.getContextPath(); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>공지사항</title>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" href="../../resource/css/iplanbiz/table.css" type="text/css"/>
		<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    
	    

		
		<script language="javascript" type="text/javascript">
		
		function  goForm(){
			location.replace("../../cms/board/form?type=M&boardNo=${board.boardno}&boardType=${board.board_type}");
		}
		function  deleteBoard(){
			$i.dialog.confirm('SYSTEM','<spring:message code="_alert._delete" javaScriptEscape="true" />',function(){
				location.replace("../../cms/board/delete?boardNo=${board.boardno}&boardType=${board.board_type}");
			});
			return false;
		}
		function goAnswer(){
			location.replace("../../cms/board/form?type=I&pBoardno=${board.boardno}&answerLevel=${board.answer_level}&boardType=${board.board_type}&boardGroup=${board.board_group}");
		}
		function goList(){
			location.replace("../../cms/board/list?boardType=${board.board_type}");
		}
		
				
		function saveReply(){
			if ($("#replyTxt").val() == '' || $("#replyTxt") == null) {
				$i.dialog.warning('SYSTEM','<spring:message code="board.alert.comments" javaScriptEscape="true" />');
				return; 
			}
			var reply=$("#replyTxt").val();
			reply="<xmp>"+reply+"</xmp>";
				//$("#replyTxt").val(encodeURIComponent(reply));
				$("#ac").val("save");
				$("#saveForm").submit();	
		}
		
		function deleteReply(boardNo,pStep,pLevel) {
			$i.dialog.confirm('SYSTEM','<spring:message code="_alert._delete" javaScriptEscape="true" />',function(){
				$("#ac1").val("del");
				$("#boardNo").val(boardNo);
				$("#pStep").val(pStep);
				$("#pLevel").val(pLevel);
				$("#deleteForm").submit();
			});
	  		return false;
		}
		
		$(document).ready(function(){
			$("#jqxButton1").jqxButton({ width: '',  theme:'blueish'});	
			$("#jqxButton2").jqxButton({ width: '',  theme:'blueish'});
			$("#jqxButton3").jqxButton({ width: '',  theme:'blueish'});	
// 			$("#jqxButton4").jqxButton({ width: '',  theme:'blueish-system'});
			/* getUserName(); */	
			//$('#replyTxt').elastic();
		});
		
		</script>
		<style>
	 		.border_bottom{border-bottom:1px solid #dedede;}
	 		.text_left{padding-left:10px;text-align:left;}
	 		.reply_table{margin-top:3px;background:#f0f0f0;vertical-align:middle;}
	 		.textarea {border: 5px solid #eee; padding: 5px; width: 91%;}
		</style>
	</head>
		<body class='blueish i-DQ'>
			<div class="wrap" style="width:98% !important; min-width:1160px; margin:0 10px!important; ">
				<div class="i-DQ-contents" style="width:100%; height:100%; float:left; margin:10px 0;">
					<div class="iwidget_label" style="margin:0 !important;">
						<div class="group_button f_right" style="margin-bottom:10px !important;">
							<c:if test="${board.userid == sessionScope.loginSessionInfo.userId || sessionScope.loginSessionInfo.userId == 'admin'}">
								<div class="button type2 f_left"  style="margin-left:0px !important;">
									<input type="button" value="수정" id='jqxButton1' width="100%" height="100%" onclick="goForm();"/>
								</div>
							</c:if>
							<div class="button type2 f_left"  style="margin-left:10px !important;">
								<input type="button" value="목록" id='jqxButton3' width="100%" height="100%" onclick="goList();" />
							</div>
<%-- 							<c:if test="${board.USERID == sessionScope.loginSessionInfo.userId || sessionScope.loginSessionInfo.userId == 'admin_339'}"> --%>
							<c:if test="${board.userid == sessionScope.loginSessionInfo.userId || sessionScope.loginSessionInfo.userId == 'admin'}">
								<div class="button type3 f_left"  style="margin-left:10px !important;">
									<input type="button" value="삭제" id='jqxButton2' width="100%" height="100%" onclick="deleteBoard();" />
								</div>
							</c:if>
						</div>
						
					</div>
					<div class="table_Area" style="width:100%; height:100%; float:left; margin-top:0px; margin-bottom:10px; ">
						<table width="100%" class="i-DQ-table" cellspacing="0" cellpadding="0" border="0" style="margin-bottom:0 !important; border-left:0 !important; border-right:0 !important; border-bottom:0 !important;">
						<tr>
							<th colspan="7" style="text-align:left; padding-left:10px; height:35px; border-right: 0 !important; font-size:13px !important; font-weight:bold !important;"><div id="BoardTitle">${fn:escapeXml(board.title)}</div></th>
							<script>
							function replaceAll(str,orgStr,repStr){
								    return str.split(orgStr).join(repStr);
							}
							var title = $("#BoardTitle")[0].innerHTML;
							$("#BoardTitle").html(replaceAll(replaceAll(title,"<","&lt;"),">","&gt;"));
							</script>
						</tr>
						<tr>
							<th width="15%" style="background:#EDF5F8;border-left:0 !important; border-right:0 !important; border-bottom-color:#E1E1E1 !important;">작성자</th>
							<td colspan="3" style="border-left:0 !important; border-right:0 !important;"><input type="text" value="${board.name}" id="i-user" class="tab_table_box" style=" width:100%;  margin:5px; padding:0; text-indent:0 !important;" disabled/></td>
							<th width="10%" style="background:#EDF5F8;border-left:0 !important; border-right:0 !important; border-bottom-color:#E1E1E1 !important;">소속</th>
							<td colspan="2"  style="border-left:0 !important; border-right:0 !important;"><input type="text" value="${borBu.user_grp_nm}" id="i-user" class="tab_table_box" style=" width:100%; margin:5px; padding:0;text-indent:0 !important;" disabled/></td>
						</tr>
						<tr>
							<th width="10%" style="background:#EDF5F8; border-left:0 !important; border-right:0 !important; border-bottom-color:#E1E1E1 !important;">등록일</th>
							<td width="15%" style="border-left:0 !important; border-right:0 !important;"><input type="text" value="${board.createdate}" id="i-user" class="tab_table_box" style=" width:100%;  margin:0px; padding:0;text-indent:0 !important;text-align:center !important;" disabled/></td>
							<th width="10%"  style="background:#EDF5F8; border-left:0 !important; border-right:0 !important; border-bottom-color:#E1E1E1 !important;">조회수</th>
							<td width="15%" style="border-left:0 !important; border-right:0 !important;"><input type="text" value="${board.hitcount}" id="i-user" class="tab_table_box" style=" width:100%;  margin:0px; padding:0;text-indent:0 !important; text-align:center !important;text-indent:0 !important;" disabled/></td>
							<th width="10%" style="background:#EDF5F8; border-left:0 !important; border-right:0 !important; border-bottom-color:#E1E1E1 !important;">우선순위</th>
							<td style="border-left:0 !important; border-right:0 !important;">
							<input type="text" value="<spring:message code="${board.notice_yn_nm}" />" id="i-user" class="tab_table_box" style="   width:100%; margin:0px !important; padding:0 !important; text-align:center !important;text-indent:0 !important;" disabled/>
						</tr>
						<tr>
							<th width="15%" style="background:#EDF5F8; border-left:0 !important; border-right:0 !important;border-bottom-color:#E1E1E1 !important;">내용</th>
							<td colspan="5" style=" padding:5px !important"><div class="dhtml_area" style=" margin:0px 0 0px 0px !important; border:1px solid #eeeeee; height:250px;overflow-y:scroll;">${board.content}</div></td>
						</tr>
						<c:forEach var="file" items="${fileList }"  varStatus="loop">
							<tr>
								<th width="15%" style="background:#EDF5F8;border-left:0 !important; border-right:0 !important; border-bottom-color:#E1E1E1 !important;"><spring:message code="board.text.file" /> ${loop.count}</th>
								<td  colspan="5"  style="border-left:0 !important; border-right:0 !important;">
									<span style="font-weight: bold; padding-right: 10px;"></SPAN>${file.file_org_name }<a href="../../cms/board/download?fileId=${file.file_id}"><img src="../../resource/css/images/img_icons/down.png" border="0" alt="${file.file_org_name}" /></a>
								</td>
							</tr>
						</c:forEach>
					</table>
					<form id="deleteForm" name="deleteForm" action="../../cms/board/get" enctype="multipart/form-data">
						<input type="hidden" id="ac1" name="ac"/>
						<input type="hidden" id="boardNo" name="boardNo" value="${board.boardno}"/>
						<input type="hidden" id="boardType" name="boardType" value="${board.board_type}"/>
						<input type="hidden" id="pStep" name="pStep" value="${board.p_step}"/>
						<input type="hidden" id="pLevel" name="pLevel" value="${board.p_level}"/>
						<c:choose>
							<c:when test="${replyTxt != null && not empty replyTxt}" >
							<table width="100%" class="i-DQ-table" cellspacing="0" cellpadding="0" border="0" style="margin-bottom:0 !important; border-left:0 !important; border-right:0 !important; border-top:0 !important;">
							</c:when>
						</c:choose>
						<c:choose>
							<c:when test="${replyTxt != null && not empty replyTxt}">
								<c:forEach items="${replyTxt}" var="reply" varStatus="loop">
									<tr>
										<th width="15%" style="background:#ffffff; border-left:0 !important; border-right-color:#E1E1E1 !important; border-bottom-color:#E1E1E1 !important;"><img src="../../resource/css/images/img_icons/comment_icon.png" alt="comment">${reply.name}</th>
										<td  style=" padding:5px !important; height:20px; color:#505050; border-right:0 !important;">${reply.content}</td>
										<td width="15%"  style="border-right:0 !important;">
											<span style=" float:right; padding-right:10px; vertical-align:middle;">${reply.createdate}
												<c:if test="${sessionScope.loginSessionInfo.isAdmin || sessionScope.loginSessionInfo.userId == reply.userid}">
													<span><img src="../../resource/css/images/img_icons/comment_close_icon.png" alt="comment_close" style="vertical-align:middle; padding-bottom:3px; padding-left:3px; cursor:pointer;" title="삭제" onclick="deleteReply('${reply.pid}','${reply.p_step}','${reply.p_level}');" ></span>
												</c:if>
											</span>
										</td>
									</tr>
								</c:forEach>
							</c:when>
						</c:choose>
						<c:choose>
							<c:when test="${replyTxt != null && not empty replyTxt}">
								</table>
							</c:when>
						</c:choose>
					</form>
				</div>
				<form id="saveForm" name="saveForm" action="../../cms/board/get" enctype="multipart/form-data">
					<input type="hidden" id="name" name="name" value="${sessionScope.loginSessionInfo.userName}"/>
					<input type="hidden" id="userId" name="userId" value="${sessionScope.loginSessionInfo.userId}"/>
					<input type="hidden" id="ac" name="ac"/>
					<input type="hidden" id="boardNo" name="boardNo" value="${board.boardno}"/>
					<input type="hidden" id="boardType" name="boardType" value="${board.board_type}"/>
					<div class="table_Area" style="width:100%; height:100%; float:left; margin-top:10px; margin-bottom:10px; ">
						<table width="100%"  cellspacing="0" cellpadding="0" border="0" style="margin-bottom:0 !important; border-left:0 !important; border-right:0 !important;border-top:2px solid #30638D; background:#ffffff; padding:10px;">
							<tr>
								<th width="15%" style=" font-size:17px !important; color:#30638D;">comment</th>
								<td width="75%"><div><textarea class="txt_style_txt" id="replyTxt" name="replyTxt" style="height:80px; width:96%; margin:0 2%;"></textarea></div></td>
								<td width="10%"><button type="button" style="height:80px; width:100%; background:url(../../resource/css/images/background/button_save_bg.png) repeat-x; border:1px solid #E1E1E1; cursor:pointer;" onclick="saveReply();" >저장</button></td>
							</tr>
						</table>
					</div>
				</form>
			</div>
		</body>
</html> 