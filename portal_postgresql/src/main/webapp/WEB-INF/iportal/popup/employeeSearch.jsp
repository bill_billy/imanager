<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<title>교직원 검색</title>

<link rel="stylesheet" type="text/css"
	href="../../resources/cmresource/css/iplanbiz/style/style.common.css" />
<link rel="stylesheet" type="text/css"
	href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css" />
<link rel="stylesheet" type="text/css"
	href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css" />
<link rel="stylesheet" type="text/css"
	href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css" />
<link rel="stylesheet" type="text/css"
	href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css"
	type="text/css">
<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
<script
	src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
<script
	src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<script
	src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
<script language="javascript" type="text/javascript">
	function searchStart(scId2, searchText2) {
		var listData = $i.post("/employeeSearch/getUserList",{scId:scId2,searchText:searchText2});
		listData.done(function(res){
								var source = {
									datatype : "json",
									datafields : [ {
										name : 'DEPTCD',
										type : 'string'
									}, {
										name : 'DEPT_NM',
										type : 'string'
									}, {
										name : 'NAMEHAN',
										type : 'string'
									}, {
										name : 'EMPNO',
										type : 'string'
									}, {
										name : 'WKGD_NAME',
										type : 'string'
									}, {
										name : 'JIKWI_NM',
										type : 'string'
									}, {
										name : 'BOJIC_NM',
										type : 'string'
									}, {
										name : 'DEPTCD',
										type : 'string'
									}, {
										name : 'ROWNUM',
										type : 'int'
									} ],
									localdata : res
								};

								var dataAdapter = new $.jqx.dataAdapter(source);

								var cellsrenderer = function(row, datafieId,
										value) {
									var row = $("#jqxgrid2").jqxGrid(
											"getrowdata", row);

									var link = "<a href=\"javaScript:throwParent('"
											+ row.EMPNO
											+ "', '"
											+ row.NAMEHAN
											+ "', '"
											+ "${param.employeeGbn}"
											+ "', '"
											+ row.DEPT_NM
											+ "', '"
											+ row.WKGD_NAME
											+ "', '"
											+ row.JIKWI_NM
											+ "');\">"
											+ value
											+ "</a>";

									return "<div style='text-align:left; padding-bottom:2px; margin-top:5px;margin-left:10px; text-decoration: underline;'>"
											+ link + "</div>";
								};

								$("#jqxgrid2").jqxGrid({
									width : '100%',
									height : '100%',
									altrows : true,
									pageable : true,
									source : dataAdapter,
									theme : 'blueish',
									pagesize : 100,
									pagesizeoptions : [ '100', '200', '300' ],
									//columnsheight:30 ,
									//rowsheight: 30,
									columnsresize : true,
									columns : [ {
										text : '조직 ID',
										datafield : 'DEPTCD',
										width : '17%',
										align : 'center',
										cellsalign : 'center'
									}, {
										text : '조직',
										datafield : 'DEPT_NM',
										width : '17%',
										align : 'center',
										cellsalign : 'center'
									}, {
										text : '이름',
										datafield : 'NAMEHAN',
										width : '17%',
										align : 'center',
										cellsalign : 'center',
										cellsrenderer : cellsrenderer
									}, {
										text : '사번',
										datafield : 'EMPNO',
										width : '17%',
										align : 'center',
										cellsalign : 'center'
									}, {
										text : '직급',
										datafield : 'WKGD_NAME',
										width : '16%',
										align : 'center',
										cellsalign : 'center'
									}, {
										text : '직위',
										datafield : 'JIKWI_NM',
										width : '16%',
										align : 'center',
										cellsalign : 'center'
									} ]
								});
								//페이지 디폴스 선택 숫자 안보임...
								$('#gridpagerlistjqxgrid2').jqxDropDownList({
									width : '49px'
								});
								//체인지 이벤트 처리
								$('#jqxgrid2').on(
										"pagesizechanged",
										function(event) {
											$('#gridpagerlistjqxgrid2')
													.jqxDropDownList({
														width : '49px'
													});
										});

							});
	}

	function throwParent(empNo, nameHan, employeeGbn, deptNm, wkgdName, jikwiNm) {

		if (employeeGbn == 'excelPgmDQView') {
<%-- excelPgmDQView.jsp : 엑셀 프로그램 관리(DQ 연동) - 관리자 검색 팝업 --%>
	$('#pgmEmp', opener.document).val(empNo);
			$('#pgmEmpNm', opener.document).val(nameHan);
		}

		self.close();
	}
	function getScList() {
		var listData = $i.post("/employeeSearch/getScList",{scId:''});
		listData.done(function(res){
								var source = {
									datatype : "json",
									datafields : [ {
										name : 'SC_ID',
										type : 'string'
									}, {
										name : 'SC_NM',
										type : 'string'
									} ],
									localdata : res
								};

								var dataAdapter = new $.jqx.dataAdapter(source);

								var cellsrenderer = function(row, datafieId,
										value) {
									var scId = $("#jqxgrid1").jqxGrid(
											"getrowdata", row)['SC_ID'];
									var link = "<a href=\"javaScript:searchStart('"
											+ scId
											+ "', '');\">"
											+ value
											+ "</a>";
									return "<div style='text-align:left; padding-bottom:2px; margin-top:5px;margin-left:10px; text-decoration: underline;'>"
											+ link + "</div>";
								};

								$("#jqxgrid1").jqxGrid({
									width : '100%',
									height : '100%',
									altrows : true,
									pageable : false,
									source : dataAdapter,
									theme : 'blueish',
									//columnsheight:30 ,
									//rowsheight: 30,
									columnsresize : true,
									columns : [ {
										text : 'ID',
										datafield : 'SC_ID',
										width : '30%',
										align : 'center',
										cellsalign : 'center'
									}, {
										text : '조직명',
										datafield : 'SC_NM',
										width : '70%',
										align : 'center',
										cellsalign : 'center',
										cellsrenderer : cellsrenderer
									} ]
								});
							});
	}

	$(document).ready(function() {

		$("#searchText").jqxInput({
			placeHolder : "",
			height : 25,
			width : 180,
			minLength : 1,
			theme : 'blueish'
		}).on('keydown', function() {
			if (event.keyCode == 13) {
				searchStart('', $('#searchText').val());
				return false;
			}
		});
		$("#jqxButton1").jqxButton({
			width : '',
			theme : 'blueish'
		}).on('click', function() {
			searchStart('', $('#searchText').val());
		});

		//조직 리스트
		getScList();

		//교직원 리스트 조회
		searchStart('', '');

	});
</script>

</head>
<body class='blueish'>
	<div class="wrap" style="width: 96%; margin: 0 auto !important;">
		<div id="content">
			<div class="header" style="width: 100%; margin: 10px auto !important;">
				<div class="iwidget_label"
					style="width: 100%; float: left; margin-bottom: 10px;">
					<form id="employeeSearchList" name="employeeSearchList"
						action="${WWW.IPORTAL}/employeeSearch" method="get">
						<div class="label type1 f_left">이름 :</div>
						<input type="text" id="searchText" name="searchText" value="" />


						<div class="headerbutton" style="float: right;">
							<div class="button type1"
								style="float: left; height: 25px; margin-right: 10px;">
								<input type="button" value="조회" id="jqxButton1" width="100%"
									height="100%" />
							</div>
						</div>
					</form>
				</div>
			</div>


			<div class="jqx-grid-area"
				style="clear: both; width: 98%; margin: 0px auto !important; border-left: 1px solid #AAAAAA;">
				<div style="width: 30%; float: left;">
					<div id="jqxgrid1"
						style="float: left; border-left: none !important; height: 370px !important;"></div>
				</div>
				<div style="width: 69%; float: left;">
					<div id="jqxgrid2"
						style="float: left; border-left: none !important; height: 370px !important; margin-left: 10px;"></div>
				</div>
			</div>
			<!--jqx-grid-area-->
		</div>
	</div>
	<!--wrap-->
</body>
</html>
