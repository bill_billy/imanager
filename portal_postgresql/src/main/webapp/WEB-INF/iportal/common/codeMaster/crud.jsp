<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta http-equiv="Content-Language" content="ko">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>공통코드관리</title>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
<link rel="stylesheet" href="../../resource/css/iplanbiz/table.css" type="text/css"/>
<script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
<script type="text/javascript">
$(this).ready(function() {
	$('#searchButton').jqxButton({ width : '', theme : 'blueish' });
	
	$('#newButton01').jqxButton({ width : '', theme : 'blueish' });
	$('#saveButton01').jqxButton({ width : '', theme : 'blueish' });
	$('#delButton01').jqxButton({ width : '', theme : 'blueish' });
	
	$('#newButton02').jqxButton({ width : '', theme : 'blueish' });
	$('#saveButton02').jqxButton({ width : '', theme : 'blueish' });
	$('#delButton02').jqxButton({ width : '', theme : 'blueish' });
	
	$("#sText").jqxInput({height: 23, width:250 , minLength: 1, theme:'blueish' });
	
	$('#searchButton').click(function() { searchStart(); });
	
	$('#newButton01').click(function() { clearForm(); });
	$('#saveButton01').click(function() { saveStart01(); });
	$('#delButton01').click(function() { delStart01(); });
	
	$('#saveButton02').click(function() { saveStart02(); });
	$('#delButton02').click(function() { delStart02(); });
	$('#newButton02').click(function() { newCode(); });
	
	$('#sText').on('keydown',
		function() {
			if(event.keyCode==13) {
				searchStart();
				return false;
			}
		}
	);
	
	//init
	createGrid1();
	createGrid2();
	
	searchStart();
	
	
	$('#ac').val("INSERT");
	$('#cCd').attr('readonly', false);
	
	
});
function createGrid1() {
	var source = {
		localdata: [],
		dataType: 'json',
		dataFields: [
			{ name: 'COM_GRP_CD'},
			{ name: 'COM_GRP_CD_NM'},
			{ name: 'COM_CD_NUM'}
		]
	};
	var alginCenter = function (row, columnfield, value) {//right정렬
		var newValue = $i.secure.scriptToText(value);
		return '<div style="text-align: center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
	};
	var alginRight = function (row, columnfield, value) {//right정렬
		return '<div style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
	};
	var detailView = function (row, columnfield, value) {
		var newValue = $i.secure.scriptToText(value);
		var link = "<a href=\"javaScript:detailView('"+row+"')\" style='color:black;'>" + newValue + "</a>";
		return '<div style="text-align:left; margin:4px 0px 0px 10px; text-decoration: underline;">' + link + '</div>';
	};

	var adapter = new $.jqx.dataAdapter(source);

	$('#jqxgrid1').jqxGrid({
		width: '100%',
     	height: '100%',
		altrows:true,
		pageable: false,
		sortable:true,
    	source: adapter,
		theme:'blueish',
     	columnsresize: true,
     	columns: [
			{ text: '그룹코드', datafield: 'COM_GRP_CD', width: '30%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
			{ text: '그룹코드명', datafield: 'COM_GRP_CD_NM', width: '50%', align:'center', cellsalign: 'left', cellsrenderer: detailView },
			{ text: '코드개수', datafield: 'COM_CD_NUM', width: '20%', align:'center', cellsalign: 'center'},
		]
	});
}
function createGrid2() {
	var source = {
		localdata: [],
		dataType: 'json',
		dataFields: [
			{ name: 'COM_GRP_CD'},
			{ name: 'COM_CD'},
			{ name: 'COM_CD_NM'},
			{ name: 'SORT_SEQ'},
			{ name: 'USE_YN'},
			{ name: 'COM_CD_DESC'}
		]
	};
	var alginCenter = function (row, columnfield, value) {//right정렬
		var newValue = $i.secure.scriptToText(value);
		return '<div style="text-align: center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
	};
	var alginLeft = function (row, columnfield, value) {//right정렬
		var newValue = $i.secure.scriptToText(value);
		return '<div style="text-align: left; margin:4px 0px 0px 10px;">' + newValue + '</div>';
	};
	var alginRight = function (row, columnfield, value) {//right정렬
		return '<div style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
	};
	var detailChildView = function (row, columnfield, value) {
		var newValue = $i.secure.scriptToText(value);
		var link = "<a href=\"javaScript:detailChildView('"+row+"')\" style='color:black;'>" + newValue + "</a>";
		return '<div style="text-align:left; margin:4px 0px 0px 10px; text-decoration: underline;">' + link + '</div>';
	};
	var adapter = new $.jqx.dataAdapter(source);
	
	$('#jqxgrid2').jqxGrid({
		width: '100%',
     	height:'100%',
		altrows:true,
		pageable: false,
		sortable:true,
    	source: adapter,
		theme:'blueish',
     	columnsresize: true,
     	columns: [
			{ text: '코드', datafield: 'COM_CD', width: '20%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
			{ text: '코드명', datafield: 'COM_CD_NM', width: '25%', align:'center', cellsalign: 'left', cellsrenderer: detailChildView },
			{ text: '사용여부', datafield: 'USE_YN', width: '10%', align:'center', cellsalign: 'center'},
			{ text: '정렬순서', datafield: 'SORT_SEQ', width: '10%', align:'center', cellsalign: 'center'},
			{ text: '비고', datafield: 'COM_CD_DESC', width: '35%', align:'center', cellsalign: 'center', cellsrenderer: alginLeft}
		]
	});
}
function searchStart(info) {
	if(!info)
		clearForm();
	var listData = $i.post("../../common/codeMaster/getGroupCodeList", {searchText:$('#sText').val()});
	listData.done(function(res){
		var $jqxgrid1 = $('#jqxgrid1');
		$jqxgrid1.jqxGrid('source')._source.localdata = res;
		$jqxgrid1.jqxGrid('updatebounddata');
		//detailView(0); //중분류코드
	});
}
function clearForm(info) {
	var $jqxgrid1 = $('#jqxgrid1');
	var $jqxgrid2 = $('#jqxgrid2');
	
	$jqxgrid1.jqxGrid('clearselection');
	$jqxgrid2.jqxGrid('clear');
	$jqxgrid2.jqxGrid('clearselection');
	
	
	$('#labelColId').text('그룹코드 : ');
		//$('#labelColNm').text('그룹코드명 : ');
	newGroupCode();	

	newCode();
}
function detailView(row) {
	
	$('#jqxgrid2').jqxGrid('clearselection');
	
	$('#jqxgrid1').jqxGrid('selectrow', row);
	var rowdata = $("#jqxgrid1").jqxGrid("getrowdata", row);
	var gCd = rowdata['COM_GRP_CD'];
	var gCdNm = rowdata['COM_GRP_CD_NM'];
	
	$("#gCd").attr('readonly', true);
	
	$("#ac1").val("UPDATE");
	$('#labelColId').text('그룹코드 : ' + gCd + ' / ' + gCdNm);
	//$('#labelColNm').text('그룹코드명 : ' + gCdNm);
	
	$('#hiddenGcode').val(gCd);
	$('#gCd').val(gCd);
	$('#gCdNm').val(gCdNm);
	
	getChild(gCd);	
}
function newGroupCode() {
	$('#saveForm01').jqxValidator('hide');
	
	$("#gCd").attr('readonly', false);
	$('#ac1').val('INSERT');
	
	$('#jqxgrid1').jqxGrid('clearselection');
	$('#jqxgrid2').jqxGrid('clearselection');
	$('#hiddenGcode').val('');
	document.saveForm01.reset();
}
function newCode() {
	$('#saveForm02').jqxValidator('hide');
	
	$("#cCd").attr('readonly', false);
	$('#ac2').val('INSERT');
	
	$('#jqxgrid2').jqxGrid('clearselection');
	$('#hiddenCcode').val('');
	document.saveForm02.reset();
}
function getChild(gCd2) {
	var listData = $i.post("../../common/codeMaster/getCodeList", {gCd:gCd2});
	listData.done(function(res){
		var $jqxgrid2 = $('#jqxgrid2');
		$jqxgrid2.jqxGrid('source')._source.localdata = res;
		$jqxgrid2.jqxGrid('updatebounddata');
	});
}
function detailChildView(row) {
	$('#saveForm02').jqxValidator('hide');
	var rowdata = $("#jqxgrid2").jqxGrid("getrowdata", row);

	$("#ac2").val("UPDATE");
	$("#cCd").attr('readonly', true);
	
	$('#hiddenCcode').val(rowdata['COM_CD']);
	
	$('#cCd').val(rowdata['COM_CD']);
	$('#cNm').val(rowdata['COM_CD_NM']);
	$('#useYn').val(rowdata['USE_YN']);
	$('#sortSeq').val(rowdata['SORT_SEQ']);
	$('#cDesc').val(rowdata['COM_CD_DESC']);
}
function saveStart01() {
	$('#saveForm01').jqxValidator('hide');
	$('#saveForm02').jqxValidator('hide');
	
	
	var ruleinfo = null;
	ruleinfo = [
		{input: '#gCd', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action:'keyup, blur', rule:'required'},
		{input: '#gCd', message:"<img src='../../resources/img/icon/icon_munst_red.png'>", action:'keyup, blur', rule:'maxLength=20'},
		{input: '#gCdNm', message:"<img src='../../resources/img/icon/icon_munst_red.png'>", action:'keyup, blur', rule:'required'},
	];
	
	$("#saveForm01").jqxValidator({
		rtl:true,  
		rules : ruleinfo,
		position: 'left'
	});
	
	if($("#saveForm01").jqxValidator('validate') == true) {
		var method = $("#saveForm01").attr("method");
		var action = $("#saveForm01").attr("action");
		var data = $("#saveForm01").serialize();
		
		$.ajax({
			type:method,
			url:action,
			data:data,
			async:false,
			success:function(res){
				if(res.returnCode=='EXCEPTION'){
					$i.dialog.error('SYSTEM',res.returnMessage);
				}else{
					$i.dialog.alert('SYSTEM', res.returnMessage);
					searchStart();
				}
			},error:function(jqXHR,textStatus,errorMessage){
				$i.dialog.error('SYSTEM', '저장에 실패하였습니다.');
			}
		});
	}	
	
}
function saveStart02() {
	$('#saveForm02').jqxValidator('hide');
	if($('#hiddenGcode').val() == '') {
		$i.dialog.error('SYSTEM', '그룹코드를 먼저 선택하세요.');
		return;
	}
	
	var ruleinfo = null;
	ruleinfo = [
		{input: '#cCd', message:"<img src='../../resources/img/icon/icon_munst_red.png'>", action:'keyup, blur', rule:'required'},
		{input: '#cCd', message:"<img src='../../resources/img/icon/icon_munst_red.png'>", action:'keyup, blur', rule:'maxLength=20'},
		{input: '#cNm', message:"<img src='../../resources/img/icon/icon_munst_red.png'>", action:'keyup, blur', rule:'required'},
		{input: '#sortSeq', message:"<img src='../../resources/img/icon/icon_munst_red.png'>", action:'keyup, blur', rule:'number'}
		/* ,
		{input: '#sortOrder', message:'정렬순서를 입력하세요!', action:'keyup, blur', rule:'required'},
		{input: '#sortOrder', message:'정렬순서는 숫자로 입력하세요!', action:'keyup, blur', rule:'number'}
		*/
	];
	
	$("#saveForm02").jqxValidator({
		rtl:true,  
		rules : ruleinfo,
		position: 'left'
	});
	
	if($("#saveForm02").jqxValidator('validate') == true) {
		
		
		var method = $("#saveForm02").attr("method");
		var action = $("#saveForm02").attr("action");
		var data = $("#saveForm02").serialize();
		
		
		$.ajax({
			type:method,
			url:action,
			data:data,
			async:false,
			success:function(res){
				if(res.returnCode=='EXCEPTION'){
					$i.dialog.error('SYSTEM', res.returnMessage);
				}else{
					$i.dialog.alert('SYSTEM', res.returnMessage);
					searchStart("info");
					newCode();
					getChild($('#hiddenGcode').val());
				}
			},error:function(jqXHR,textStatus,errorMessage){
				$i.dialog.error('SYSTEM', '저장에 실패하였습니다.');
			}
		});	
	}	
	
}
function delStart01() {
	if($('#hiddenGcode').val() == '') {
		$i.dialog.error('SYSTEM', '그룹 코드를 선택하세요');
		return;
	}
	$i.dialog.confirm('SYSTEM','그룹 코드 삭제 시 하위 코드도 삭제됩니다. 삭제하시겠습니까?',function(){
		$("#ac1").val("DELETE");
		
		var del = $i.post("/common/codeMaster/deleteGroupCode", {gCd:$('#hiddenGcode').val()});
		del.done(function(res){
			searchStart();
		//	var $jqxgrid1 = $('#jqxgrid1');
		//	$jqxgrid1.jqxGrid('source')._source.localdata = res;
		//	$jqxgrid1.jqxGrid('updatebounddata');
			//detailView(0); //중분류코드
			
		});	
	});
}
function delStart02() {
	if($('#hiddenCcode').val() == '') {
		$i.dialog.error('SYSTEM', '코드를 선택하세요');
		return;
	}
	$i.dialog.confirm('SYSTEM','코드를 삭제하시겠습니까?',function(){

		$("#ac2").val("DELETE");
		
		var del = $i.post("/common/codeMaster/deleteCommonCode", {gCd:$('#hiddenGcode').val(),cCd:$('#cCd').val()});
		del.done(function(res){
			searchStart("info");
			newCode();
			getChild($('#hiddenGcode').val());
		//	var $jqxgrid1 = $('#jqxgrid1');
		//	$jqxgrid1.jqxGrid('source')._source.localdata = res;
		//	$jqxgrid1.jqxGrid('updatebounddata');
			//detailView(0); //중분류코드
			
		});	
	});
}
function checkStringFormat(string) { 
    //var stringRegx=/^[0-9a-zA-Z가-힝]*$/; 
    var stringRegx = /[~!@\#$%<>^&*\-=+_\’\'\"]/gi; 
    var isValid = true; 
    if(stringRegx.test(string)) { 
      isValid = false; 
    } 
       
    return isValid; 
}
</script>
 <style  type="text/css">
	.jqx-validator-hint{
		background-color : transparent !important;
		border:0px !important;
	}
	.jqx-validator-hint-arrow{
		display:none !important;
	}
	/*groupbox*/
		.group_box_label .group_box_title {
			float:left;
			width:11%;
			font-weight:bold;
			text-align:left;
			margin-left:10px;
			margin-top:3px;
			margin-right:20px;
			padding-left:20px;
			font-size:12px;
			color:#303030;
			margin-bottom:15px;
			background:url(../../resources/css/images/img_icons/label-icon.png) left no-repeat;
		}
		.group_box_label .group_box_input {
			float:right;
			width:79%;
			margin-bottom:12px;
		}
		.group_box_label .group_box_input input.input_stA {
			width:94%;
			background:#f9f9f9;
			border:1px solid #9DA1A4;
			border-radius:3px;
			-moz-border-radius:3px;
			-ms-border-radius:3px;
			-o-border-radius:3px;
			-webkit-border-radius:3px;
			text-indent:5px;
			font-size:12px;
			color:#202020;
			padding:3px;
			margin-left:10px;
			float:right;
		}
	</style>
</head>
<body class="blueish">
	<div class="wrap" style="width:98%; min-width:1160px; margin:0 10px;">
		 	<div class="iwidget_label header" style="width:100%; margin:10px 0; height:27px;">
					<div class="label type1 f_left">그룹코드명 :</div>
					<input type="text" id="sText" name="sText" style="margin-left:5px;"/>
					
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='searchButton' width="100%" height="100%" />
						</div>
						<!--buttonStyle--> 
					</div>
		  	</div>
		  	<div class="i-DQ-contents content" style="width:100%; height:100%; float:left;margin-top:10px;">
			  	<div class="left_Area" style="width:33%; height:100%; float:left; margin-right:2%;">
			  		<input type="hidden" id="goColId" name="goColId" />
			  		<input type="hidden" id="goColNm" name="goColNm" />
					<div class="iwidget_grid" style= "float:right; width:100%; height:505px !important;margin-bottom:20px !important;">
						<div id="jqxgrid1" class="grid2" style=" width:100%; height:100%; text-align:center !important"> </div>
					</div>
					<div style="width:100%; height:100%; float:right; margin-top:0px; ">
						<form id="saveForm01" name="saveForm01" action="${WWW.IPORTAL}/common/codeMaster/insertGroupCode" method="post" style="margin-top:10px">
						<input type="hidden" id="ac1" name="ac" value="INSERT"/>
						<!--  
						<table style="width:100%;" cellspacing="0" cellpadding="0" border="0">
							<colgroup>    
								<col width="35%" />
								<col width="65%" />
							</colgroup> 
							<tr>
								<th><span style="color:red; font-size:12px; vertical-align:-2px;margin-left:10px;">＊</span>그룹 코드 :</th>
								<td><input type="text" id="gCd" name="gCd" class="inputtable" style="width:93%;" /></td>
							</tr>
							<tr>
								<th><span style="color:red; font-size:12px; vertical-align:-2px;margin-left:10px;">＊</span>그룹 코드명 :</th>
								<td><input type="text" id="gCdNm" name="gCdNm" class="inputtable" style="width:93%;" /></td>
							</tr>
						</table>
						-->
						
						<div class="group_box_label" >
							<p class="group_box_title" > 그룹코드 </p>
							<p class="group_box_input" style="font-size:12px; vertical-align:-2px;"><b>:</b>
								<input type="text" id="gCd" name="gCd" class="input_stA"/>
							</p>	
							<p class="group_box_title" > 그룹코드명 </p>
							<p class="group_box_input"><b>:</b>
								<input type="text" id="gCdNm" name="gCdNm" class="input_stA"/>
							</p>	
						</div>
					
						</form>
						<div class="button-bottom" style="float:right; margin-top: 5px;margin-right:5px;">
							<div class="button type2" style="float:left; height:25px; ">
								<input type="button" value="신규" id='newButton01' width="100%" height="100%" style="margin-left: 0px !important;"/>
							</div>
							<div class="button type2" style="float:left; height:25px; ">
								<input type="button" value="저장" id='saveButton01' width="100%" height="100%" style="margin-left: 0px !important;"/>
							</div>
							<div class="button type3" style="float:left; height:25px; ">
								<input type="button" value="삭제" id='delButton01' width="100%" height="100%" style="margin-left: 0px !important;"/>
							</div>
						</div>
					</div>
				</div>
				<div class="right_Area" style="width:65%; height:100%; float:left;">
					<div class="iwidget_label" style="width:100%; float:left; margin-bottom:10px;  ">
						<div class="label type2 f_left">
							<label id="labelColId" name="labelColId">그룹코드 : </label>
							
							<input type="hidden" id="hiddenCcode"/>
						</div>
					</div>
					<div class="iwidget_grid" style= "float:right; width:100%; height:470px;margin-bottom:20px !important;">
						<input type="hidden" id="colId1" name="colId1" />
						<div id="jqxgrid2" class="grid2" style="width:100%; height:100%;" > </div>
					</div>
					<div class="table_Area" style="width:100%; height:100%; float:left; margin-top:0px; ">
						<form id="saveForm02" name="saveForm02" action="${WWW.IPORTAL}/common/codeMaster/insertCommonCode" method="post" style="margin-top:10px">
							<input type="hidden" id="ac2" name="ac" value="INSERT"/>
							<input type="hidden" id="hiddenGcode" name="gCd"/>
							<table width="100%" class="i-DQ-table" cellspacing="0" cellpadding="0" border="0">
									<colgroup>    
										<col width="20%" />
										<col width="25%" />
										<col width="10%" />
										<col width="10%" />
										<col width="35%" />
									</colgroup> 	
									<thead>
										<tr style="height:30px !important;"> 		
											<th style="font-weight:bold !important;"><span style="color:red; font-size:12px; vertical-align:-2px;">＊</span>코드</th> 		
											<th style="font-weight:bold !important;"><span style="color:red; font-size:12px; vertical-align:-2px;">＊</span>코드명</th> 		
											<th style="font-weight:bold !important;"><span style="color:red; font-size:12px; vertical-align:-2px;">＊</span>사용여부</th>
											<th style="font-weight:bold !important;">정렬순서</th> 	
											<th style="font-weight:bold !important;">비고</th> 	
										</tr>
									</thead>
								<tbody> 	
									<tr>
										<td><input type="text" id="cCd" name="cCd" class="inputtable" style="width:93%;" /></td> 		
										<td><input type="text" id="cNm" name="cNm" class="inputtable" style="width:94%;" /></td>
										<td style="text-align: center;">
											<select class="styled_select" id="useYn" name="useYn" style="width:90%;">
												<option value="Y">Y</option>	
												<option value="N">N</option>	
											</select>
										</td>
										<td><input type="text" id="sortSeq" name="sortSeq" class="inputtable" style="width:80%; text-align: right; padding-right: 5px;" /></td>
										<td><input type="text" id="cDesc" name="cDesc" class="inputtable" style="width:90%; padding-right: 5px;" /></td>
									</tr> 			
								</tbody>
							</table>   
							<div class="label-4" style="float:left;margin-top:10px;"></div>
								<div class="button-bottom" style="float:right; margin-top: 10px;margin-right:5px;">
									<div class="button type2" style="float:left; height:25px; ">
										<input type="button" value="신규" id='newButton02' width="100%" height="100%" style="margin-left: 0px !important;"/>
									</div>
									<div class="button type2" style="float:left; height:25px; ">
										<input type="button" value="저장" id='saveButton02' width="100%" height="100%" style="margin-left: 0px !important;"/>
									</div>
									<div class="button type3" style="float:left; height:25px; ">
										<input type="button" value="삭제" id='delButton02' width="100%" height="100%" style="margin-left: 0px !important;"/>
									</div>
									
								</div>
						</form>
					</div>
				</div>
			</div>
	</div>
</body>
</html>