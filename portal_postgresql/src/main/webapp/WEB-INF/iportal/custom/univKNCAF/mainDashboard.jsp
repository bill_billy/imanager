<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>농수산대학교 대시보드(메인)</title>
		<link rel="stylesheet"  href="../../resource/custom/css/dashboard.basic_portal.css" type="text/css"/>
		<link rel="stylesheet"  href="../../resource/custom/css/style_portal_kyungdong.css" type="text/css"/>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<!-- 		<script type="text/javascript" src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script> -->
<!-- 		<script type="text/javascript" src="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxcore.js"></script> -->
<!-- 		<script type="text/javascript" src="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdraw.js"></script> -->
<!-- 		<script type="text/javascript" src="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxchart.core.js"></script> -->
<!-- 		<script type="text/javascript" src="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdata.js"></script> -->
		<script type="text/javascript">
			var colorset = [{columns:['#c4aeba','#d44c95'], line:'#b72700'},
							{columns:['#898e98','#5471ab'], line:'#b72700'},
							{columns:['#ceb498','#e99c4a'], line:'#b72700'},
							{columns:['#a6b093','#8dc12a'], line:'#b72700'},
							{columns:['#a5c7c7','#20acae'], line:'#b72700'},
							{columns:['#bcccd6','#4aabe9'], line:'#b72700'},
							{columns:['#bfbcd4','#786ccf'], line:'#b72700'},
							{columns:['#b7a8a4','#8b7066'], line:'#b72700'},
							{columns:['#849aa8','#4681a7'], line:'#b72700'},
							{columns:['#8fb2b8','#9087d7'], line:'#b72700'},
							{columns:['#b0c88f','#6da3da'], line:'#b72700'},
							{columns:['#85c5c7','#5981a4'], line:'#b72700'},
							{columns:['#b5b88f','#dba741'], line:'#b72700'},
							{columns:['#a6adbc','#278fb6'], line:'#b72700'}];
			
			//이미지 순서
			var cnt=1;
			//차트 순서
			var chartCnt=0;
			
			$(document).ready(function(){
				var kpiData = $i.post("./getKpiList",{});
				kpiData.done(function(data){
					for(var i = 0; i < data.returnArray.length; i++){
						var info = data.returnArray[i];
						setTitle(info);
						setBody(info);
					}
				});
			});
			function comma(str) {
				str = String(str);
				return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
			}
			function go(cid,title,prog_link){
				try{
					parent.parent.addTab(cid,title,prog_link);
				}catch(e){}
			}
			function setTitle(info){ 
				var html = "<div class='container_title'>"+ 
							   "<h3>"+info.COM_NM+"</h3>"+
							 //  "<div class='date'><span>2017</span>년 <span>8</span>월 <span>17</span>일 기준</div>"+
						   "</div>";
				$("#container").append(html);
			}
			
			function setBody(info){
				var sc_id = info.COM_COD;
				var kpiTarget = $i.post("./getMainDashboardKpiTarget",{scID:sc_id});
				kpiTarget.done(function(data){
					for(var i = 0; i < data.returnArray.length; i++){
						var kpi = data.returnArray[i];
						var islast = i%2!=0;
						appendKpi(kpi,islast);
					}
					if(data.length%2!=0){
						appendImage();
						cnt++;
					}
				});
				// prepare chart data as an array
			}
			function appendImage(){
				var html = '<div class="content visual">'+
							'	<div class="title">'+
							'		<h2></h2>'+
							'	</div>'+
							'	<div class="con">'+
							'		<img src="../../resource/custom/img/background/bg_visual'+cnt+'.png" alt="visual01">'+
							'	</div>'+
							'</div>';
				$("#container").append(html);	
			}
			function go(cid,title,prog_link){
				if(prog_link==""){
					alert("링크가 등록되지 않았습니다.");return;
				}
				try{
					parent.parent.addTab(cid,title,prog_link);
				}catch(e){}
			}
	
			function appendKpi(kpi, islast){
				//console.log(kpi);
				var html = '<div class="content '+(islast?"last":"")+'" id="S'+kpi.KPI_GBN+'M'+kpi.KPI_ID+'" data-unit_nm="'+kpi.UNIT_NM+'">'+
								'<div class="title">'+
								'<h2 style="cursor:pointer;" onclick="javascript:go(\''+kpi.C_ID+'\',\''+kpi.PROG_NM+'\',\''+kpi.PROG_LINK+'\');"><a class="link_txt" >'+kpi.KPI_NM+'</a></h2>'+
								'	<input type="hidden" name="COL1_NM" value="'+kpi.KPI_UDC1+'"/>'+
								'	<input type="hidden" name="COL2_NM" value="'+kpi.KPI_UDC2+'"/>'+
								'	<input type="hidden" name="COL3_NM" value="'+kpi.KPI_UDC3+'"/>'+
								'</div>'+
								'<div class="con">'+
								'	<div class="con_left">'+
								'		<div class="date"><p> 대학알리미 기준</p></div>'+
								'		<ul class="count" style="min-width:217px;">'+
								
								'			<li>'+kpi.KPI_UDC1+'<span name="COL1">COL1</span></li>'+
								'			<li>'+kpi.KPI_UDC2+'<span name="COL2">COL2</span></li>'+
								'			<li  class="bold">'+kpi.KPI_UDC3+'<span name="VAL">VAL</span></li>'+
								'			<li>달성률<span name="RATE"></span></li>'+
								'		</ul>'+
								'	</div>'+
								'	<div class="con_right">'+
								'		<div class="formula"><p>'+kpi.ETC_DESC+'</p></div>'+
								'		<div class="years">'+
								'			<dl>'+
								'				<dt name="YYYY2_NM">&nbsp;</dt> '+
								'				<dd name="YYYY2_VAL"><span>&nbsp;</span></dd>'+
								'			</dl>'+
								'			<dl>'+
								'				<dt name="YYYY1_NM">&nbsp;</dt>'+
								'				<dd name="YYYY1_VAL">&nbsp;</dd>'+
								'			</dl>'+
								'			<dl>'+
								'				<dt class="bold" name="YYYY0_NM">&nbsp;</dt>'+
								'				<dd class="bold" name="YYYY0_VAL">&nbsp;</span></dd>'+
								'			</dl>'+
								'			<dl>'+
								'				<dt>B-A</dt>'+
								'				<dd name="YYYY_COMP"></dd>'+
								'			</dl>'+
								'		</div>'+
								'		<div class="graph">'+
								'			<div class="chart" style=" float:left; width:100%; height:210px; margin:0 0%; padding: 0;">'+
								'				<div control="chart" style="width:100%; height:100%;"></div> '+
								'			</div>'+
								
								'		</div>'+
								'	</div>'+
								'</div>'+
							'</div>';
				$("#container").append(html);	
	 
				// prepare chart data as an array
				var kpiTarget = $i.post("./getMainDashbaordSqlDATA",{scID:kpi.KPI_GBN, kpiID:kpi.KPI_ID});
				kpiTarget.done(function(res){
					var data = res.returnArray;
					if(data.length!=0){ 
						 
						var area = $('#S'+kpi.KPI_GBN+'M'+kpi.KPI_ID);
						var insertdt = data[0].INSERT_DT;
						var col1 = data[0].COL1==undefined?"-":comma(data[0].COL1);
						var col2 = data[0].COL2==undefined?"-":comma(data[0].COL2);
						var val = data[0].VAL==undefined?"-":comma(data[0].VAL);
						var rate = data[0].RATE!=null&&data[0].RATE!=""?data[0].RATE+"%":"-";
						area.find("[name=INSERT_DT]").html(insertdt);
						area.find("[name=COL1]").html(col1);
						area.find("[name=COL2]").html(col2);
						area.find("[name=VAL]").html(val);
						area.find("[name=RATE]").html(rate);
					}
					var clone = [];
					var stack = {};
					for(var i = 0; i < data.length;i++){
						if(stack[data[i].YYYY]!=undefined) continue;
						stack[data[i].YYYY] = true;
						clone.push(data[i]);
					}
					
					for(var i = 0; i < clone.length;i++){
						var area = $('#S'+kpi.KPI_GBN+'M'+kpi.KPI_ID);
						var val = clone[i].VAL==undefined||clone[i].VAL==""?"-":clone[i].VAL;
						area.find("[name=YYYY"+i+"_NM]").html(clone[i].YYYY+(i==0?"(B)":(i==1?"(A)":""))); 
						area.find("[name=YYYY"+i+"_VAL]").html(val+"<span>"+area.data("unit_nm")+"</span>");
					}
					if(clone.length>=2&&clone[0].VAL!=null&&clone[0].VAL!=""){
						area.find("[name=YYYY_COMP]").html("<span>"+(parseFloat(clone[0].VAL)-parseFloat(clone[1].VAL)).toFixed(2)+area.data("unit_nm")+"</span>");
					}else{
						area.find("[name=YYYY_COMP]").html("<span>-</span>");
					}
					
					if(clone.length!=0)
						renderChart(area,clone);
				});
			}
	        var toolTipCustomFormatFn = function (value, itemIndex, serie, group, categoryValue, categoryAxis) {
				  
				  return '<DIV style="text-align:left"><b>년도: ' + categoryValue +
					  '</b><br />'+serie.displayText+': ' + comma(value) +"</DIV>";
			  };
			function renderChart(area, data) {
				var linechartMin = 999999;
				for(var i = 0; i < data.length;i++){
					if(data[i].VAL!=null){
						if(linechartMin>parseFloat(data[i].VAL))
							linechartMin = parseFloat(data[i].VAL);
					}
						
				}
				linechartMin = linechartMin-(linechartMin*0.1);
	            // prepare jqxChart settings
	            var settings = { 
					 title: "",
	                description: "",
	              //  title: "jqxchart 01",
	                //description: "과정별 추이",
					enableAnimations: true,
	                showLegend: true,
	                padding: { left: 10, top: 20, right: 10, bottom: 10 },
	                titlePadding: { left: 0, top: 0, right: 0, bottom: 10 },
					background:'none',
					backgroundImage:'none',
					borderLineColor :'none', 
	                source: data,
	                xAxis:
	                    {
	                        dataField: 'YYYY',
	                        showGridLines: false,
							gridLinesColor: '#dadada',
	                    },
	               // colorScheme: 'color03',
	                seriesGroups:
	                    [
	                        {
	                            type: 'column',
								useGradientColors: false,
	                            columnsGapPercent: 50,
	                            seriesGapPercent: 5,
								showLabels: false, 
								opacity:1,
								toolTipFormatFunction: toolTipCustomFormatFn,
	                            valueAxis:
	                                { 
	                                    title: { text: '' },
	                                    position: 'left',
										visible:false,
										gridLinesColor: '#ccc',
										displayValueAxis: false, 
	                   				    flip: false
	                                },
	                            series: [
	                                    { dataField: 'COL1',  formatFunction:function(value){ return comma(value);} , labels:{ visible:true,verticalAlignment: 'bottom',offset:{x:0,y:0}}
										  ,displayText: area.find("[name=COL1_NM]").val(), stroke:"0", fillColor:colorset[chartCnt].columns[0], lineColor:colorset[chartCnt].columns[0] },
	                                    { dataField: 'COL2',  formatFunction:function(value){ return comma(value);} , labels:{ visible:true,verticalAlignment: 'bottom',offset:{x:0,y:0}} 
										  , displayText: area.find("[name=COL2_NM]").val(), stroke:"0", fillColor:colorset[chartCnt].columns[1], lineColor:colorset[chartCnt].columns[1] }
	                                ]
	                        },
							 {
	                            type: 'line',
	                            columnsGapPercent: 100,
	                            seriesGapPercent: 5,
								showToolTips: true,
								toolTipBackground:'#ffffff',			
								toolTipFormatFunction: toolTipCustomFormatFn,				
	                            valueAxis:
	                            {
									visible:false, 
	                                description: '',
	                                axisSize: 'auto',
									minValue:linechartMin,
									 showTickMarks: false,
									 valuesOnTicks: false,
	                                tickMarksColor: '#ccc',
									gridLinesColor: '#ccc',
									alternatingBackgroundColor: 'none',
									alternatingBackgroundColor2: 'none',
									alternatingBackgroundOpacity: 0,
									horizontalTextAlignment: 'right'
	                            },
	                            series: [ //------------------------------------line 색상 및 스타일
								 { dataField: 'VAL', displayText: area.find("[name=COL3_NM]").val(), opacity: 1, lineWidth: 3, symbolType: 'circle', symbolSize:10, symbolSizeSelected:10,   
	     lineColor:colorset[chartCnt].line, lineColorSelected:colorset[chartCnt].line, fillColor:colorset[chartCnt].line, fillColorSymbol:'#ffffff',fillColorSymbolSelected: '#ffffff',  lineColorSymbolSelected:'#b72700', showLabels: true,
		 							labels: {
	                                            visible: false,
	                                            verticalAlignment: 'top',
	                                            offset: { x: 0, y: 0 }
	                                        }, 
										}
	                                ]
	                        }
	                    ]
	            };
				 // setup the chart
				area.find("[control=chart]").jqxChart(settings); 
				chartCnt++;
	        }
	    </script> 
	</head>
	<body class='default kyungdong_portal' style="overflow:hidden;">
		<div id="wrap" class="wrap_style">
			<div id="header" class="header_style">
<!-- 				<h1><img src="css/images/background/header_title01.png" alt="경동대학교"></h1> -->
				<h2><img src="../../resource/custom/img/background/header_title02.png" alt="지표현황"></h2>
			</div>
			<div id="container"  class="container_style"></div>
		</div>
	</body>
</html>
