<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <title>사용자이력관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" hrlef="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <script>
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$i.dialog.setAjaxEventPopup("SYSTEM","데이터 로딩중 입니다.");
        		$("#jqxButtonExcel").jqxButton({width:'', theme:'blueish'});
        		$("#jqxButtonSearch").jqxButton({width:'', theme:'blueish'});
        //		$("#btnSearchUserID").jqxButton({width:'', theme:'blueish'});
        		$("#txtSearchUser").jqxInput({placeHolder: "", height: 23, width: 250, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return makeUserList(); } });
        		init();
        		
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		makeUserList();
        		detailLog();
        		var getCode1 = $i.post("../../custom/ktotoUser/getCodeList", {com_grp_cd:'USER_TEAM'})//부서
            	getCode1.done(function(data){
         			data.returnArray.unshift({COM_CD:'', COM_CD_NM:'전체'});
         			var source =
                    {
                        datatype: "json",
                        datafields: [
                            { name: 'COM_CD' },
                            { name: 'COM_CD_NM' }
                        ],
    					localdata:data,
                        async: false
                    };
    	            var dataAdapter = new $.jqx.dataAdapter(source);
    	            $("#topTeamCombo").jqxComboBox({ 
    				selectedIndex: 0, 
    				source: dataAdapter, 
    				animationType: 'fade', 
    				dropDownHorizontalAlignment: 'right', 
    				displayMember: "COM_CD_NM", 
    				valueMember: "COM_CD", 
    				dropDownWidth: 150, 
    				dropDownHeight: 100, 
    				width: 150, 
    				height: 22,  
    				theme:'blueish'});
         		});
        	}
        
        	function makeUserList(){ //사용자 목록
        		var $jqxgrid1 = $('#jqxGrid01');
        		var $jqxgrid2 = $('#jqxGrid02');
        		$jqxgrid1.jqxGrid('clearselection');
        		$jqxgrid2.jqxGrid('clear');
        		$jqxgrid2.jqxGrid('clearselection');
        		$("#labelID").text(" ");
        		$("#hiddenUnid").val("");
        	
            	var getList = $i.post("../../custom/ktotoUserLog/getUserList",{team:$("#topTeamCombo").val(),userNm:$("#txtSearchUser").val()});
            	getList.done(function(data){
    	        	var source =
    	                {
    	                    datatype: "json",
    	                    datafields: [
    	                        { name: 'UNID', type: 'string'},
    	    					{ name: 'USER_ID', type: 'string'},
    	    					{ name: 'USER_NAME', type: 'string'},
    	    					{ name: 'K_TEAM', type: 'string'},
    	    					{ name: 'K_TEAM_NM', type: 'string'}
    	                    ],
    	                    localdata: data,
    	                    sortcolumn: 'USER_ID',
    	                    async: false,
    	                    updaterow: function (rowid, rowdata, commit) {
    	                        commit(true);
    	                    }
    	                };
    	     			var noScript = function (row, columnfield, value) {//center
    	    				var newValue = $i.secure.scriptToText(value);
    						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
    					}; 
    					
    	    			var alginLeft = function (row, columnfield, value) {//left정렬
    	                    return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
    	    			}; 
    	    			var alginRight = function (row, columnfield, value) {//right정렬
    	                    return '<div id="userName-' + row + '"style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
    	    			};
    	    			
    	    			var detail = function (row, columnfield, value) {//그리드 선택시 하단 상세
    						var newValue = $i.secure.scriptToText(value);
    						var link = "<a href=\"javaScript:detailLog("+row+");\" style='color: #000000;text-decoration:underline;'>" + newValue + "</a>";
    	
    						return "<div style='text-align:center; margin:4px 0px 0px 0px;'>" + link + "</div>";
    						
    					};
    	    			
    	    				  
    	                var dataAdapter = new $.jqx.dataAdapter(source);
    	
    	                $("#jqxGrid01").jqxGrid(
    	                {
    	                    width: '100%',
    	                    height: '100%',
    	    				altrows:true,
    	    				pageable: true,
    	    				pageSize: 100,
    	    			    pageSizeOptions: ['100', '200', '300'],
    	                    source: dataAdapter,
    	    				theme:'blueish',
    	    				columnsheight:25 ,
    	    				rowsheight: 25,
    	                    columnsresize: true,
    	    				//autorowheight: true,
    	    				sortable:true,
    	                    columns: [
    	                      { text: '팀', datafield: 'K_TEAM_NM', width: '45%', align:'center', cellsalign: 'center'},
    	                      { text: 'ID', datafield: 'USER_ID', width: '30%', align:'center', cellsalign: 'center',cellsrenderer:detail},
    	                      { text: '이름', datafield: 'USER_NAME', width: '25%', align:'center', cellsalign: 'center'}
    	                    ]
    	                });
                   
            	});	
        	}
        	
        	function detailLog(row){
        		var unid=0, userId;
        		if(row!=null){
        			unid = $("#jqxGrid01").jqxGrid("getrowdata", row).UNID;
    				userId = $("#jqxGrid01").jqxGrid("getrowdata", row).USER_ID;
    				$("#hiddenUnid").val(unid);
    				$("#labelID").text(userId);
        		}
        		var getLog = $i.post("../../custom/ktotoUserLog/getUserLogList",{unid:unid});
        		getLog.done(function(data){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                	{ name: 'UNID', type: 'string' },
		                	{ name: 'IDX', type: 'string' },
		                    { name: 'STATUS_GUBN', type: 'string' },
		                    { name: 'USER_ID', type: 'string'},
		                    { name: 'USER_NAME', type: 'string' },
		                    { name: 'USER_EMAIL', type: 'string' },
		                    { name: 'K_OLAPID', type: 'string' },
		                    { name: 'K_PHONE', type: 'string' },
		                    { name: 'K_STATUS', type: 'string' },
		                    { name: 'K_COMPANY', type: 'string' },
		                    { name: 'K_DEPT', type: 'string' },
		                    { name: 'K_TEAM', type: 'string' },
		                    { name: 'K_STATUS_NM', type: 'string' },
		                    { name: 'K_COMPANY_NM', type: 'string' },
		                    { name: 'K_DEPT_NM', type: 'string' },
		                    { name: 'K_TEAM_NM', type: 'string' },
		                    { name: 'K_IP_IN', type: 'string' },
		                    { name: 'K_IP_OUT', type: 'string' },
		                    { name: 'K_SYS', type: 'string' },
		                    { name: 'K_FAILCOUNT', type: 'string' },
		                    { name: 'REMOTE_IP', type: 'string' },
		                    { name: 'REMOTE_OS', type: 'string' },
		                    { name: 'REMOTE_BROWSER', type: 'string' },
		                    { name: 'UPDATE_DAT', type: 'string' },
		                    { name: 'UPDATE_EMP', type: 'string' },
		                    { name: 'UPDATE_EMP_NM', type: 'string' }
		                ],
		                localdata: data.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
        			var index = function (row, columnfield, value) {//그리드 선택시 하단 상세
						var newValue = eval(value)+1;
						return "<div style='text-align:center; margin:4px 0px 0px 0px;'>" + newValue + "</div>";
						
					};
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#jqxGrid02").jqxGrid(
		            {
		            	width: '100%',
						height: '625px',
						altrows:true,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                sortable : true,
		                pageable: true,
						pagesizeoptions: ['100', '200', '300'],
						pagesize:100,
						pagermode: 'default',
		                columns: [
		                	{ text: 'No', datafield:'IDX', align:'center', cellsalign:'center', width:'4%',cellsrenderer:index},
		                	{ text: '구분', datafield:'STATUS_GUBN', align:'center', cellsalign:'center', width:'8%'},
							{ text: '이름', datafield:'USER_NAME', align:'center', cellsalign:'center', width:'8%'},
							{ text: 'OLAP ID', datafield:'K_OLAPID', align:'center', cellsalign:'center', width:'8%'},
		                	{ text: '휴대전화' , datafield:'K_PHONE', align:'center', cellsalign:'center', width: '8%'},
		                    { text: 'E-MAIL' , datafield:'USER_EMAIL', align:'center', cellsalign:'center', width: '12%'},
		                    { text: '소속회사' , datafield:'K_COMPANY_NM', align:'center', cellsalign:'center', width: '8%'},
		                    { text: '부서' , datafield:'K_DEPT_NM', align:'center', cellsalign:'center', width: '8%'}, 
						 	{ text: '팀' , datafield:'K_TEAM_NM', align:'center', cellsalign:'center', width: '8%'},
						 	{ text: '상태' , datafield:'K_STATUS_NM', align:'center', cellsalign:'center', width: '8%'},
						 	{ text: '내부IP' , datafield:'K_IP_IN', align:'center', cellsalign:'center', width: '8%'},
						 	{ text: '외부IP' , datafield:'K_IP_OUT', align:'center', cellsalign:'center', width: '8%'},
						 	{ text: '수정일' , datafield:'UPDATE_DAT', align:'center', cellsalign:'center', width: '12%'},
						 	{ text: '수정자' , datafield:'UPDATE_EMP', align:'center', cellsalign:'center', width: '8%'},
						 	{ text: 'IP' , datafield:'REMOTE_IP', align:'center', cellsalign:'center', width: '8%'},
						 	{ text: 'OS' , datafield:'REMOTE_OS', align:'center', cellsalign:'center', width: '8%'},
						 	{ text: 'BROWSER' , datafield:'REMOTE_BROWSER', align:'center', cellsalign:'center', width: '8%'} 
		                ]
		            });
        		});
        	}
        	function goExcel(){
        		var unid = $("#hiddenUnid").val();
        		if(unid!=null && unid!=''){
        			windowOpen('../../custom/ktotoUserLog/excelDownByUserLog?unid='+unid, "_self");
        		}else{
        			$i.dialog.error('SYSTEM','조회된 데이터가 없습니다.');
        		}
        		
        	}

       
	    </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
			#gridKpilist .jqx-cell {
			  padding: 3px 0px 3px 5px     
			}
		</style>
    </head>
    <body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 10px;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
					<div class="label type1 f_left">팀:</div>
					<div class="combobox f_left"  id='topTeamCombo'></div>
					<div class="label type1 f_left">사용자 이름:</div>
					<input type="text" id="txtSearchUser" style="float:left;margin-left:5px;"/>
					<div class="group_button f_right">
					     <div class="button type1 f_left">
					       <input type="button" value="조회" id='jqxButtonSearch' width="100%" height="100%" onclick="makeUserList();"/>
					     </div>
					     <div class="button type1 f_left">
					       <input type="button" value="Excel" id='jqxButtonExcel' width="100%" height="100%" onclick="goExcel();"/>
					     </div>
				    </div>
			</div>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:24%; margin-right:1%;">
					<div class="tabs f_left" style=" width:100%; height:630px; margin-top:0px;">
					<!-- 
						<div  class="tabs_content" style=" height:100%; ">
								<div class="group  w90p" style="margin:10px auto;">
									<div class="label type2 f_left">검색</div>
									<input type="text" id="txtSearchUserID" name="searchUserID" class="input type1  f_left  w50p"  style="width:100%; margin:3px 5px; "/>
    								<div class="button type2">            
  										<input type="button" value="검색" id='btnSearchUserID' width="100px" height="100%" style="margin-top:2px!important;"" onclick="makeUserList();" />
									</div>   
  								</div>
								<div class=" w90p" style="margin:0px auto;height:550px;">             
									<div id='jqxGrid01' class="f_left" style=" width:100%; margin:2px 0;"> </div>
								</div>
						</div>
						-->
						<div class=" w100p" style="margin:0px auto;height:650px;">             
									<div id='jqxGrid01' class="f_left" style=" width:100%; margin:2px 0;"> </div>
								</div>
					</div>
				</div>
				<div class="content f_right" style=" width:75%;">
					<div class="label type2 f_left">ID&nbsp;:&nbsp;<span id="labelID"></span></div>
					<input type="hidden" id="hiddenUnid" name="hiddenUnid"/>
					<div class="tabs f_left" style=" width:100%; height:550px; margin-top:0px;">
						<div id='jqxGrid02'>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

