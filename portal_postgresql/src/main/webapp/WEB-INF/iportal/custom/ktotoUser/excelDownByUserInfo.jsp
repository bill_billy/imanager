 <%@ page language="java" contentType="application/vnd.xls;charset=UTF-8" pageEncoding="utf-8"%>   -
<%@page import="java.net.URLEncoder"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%

	String header =request.getHeader("User-Agent");
	String browser="";  
	String docName = URLEncoder.encode("사용자 정보", "utf-8");  
	 
	System.out.println("header++++++++++++++++++++++++++++++"+header+":::::"+docName);
	 
	
    response.setHeader("Content-Disposition", "attachment; filename="+docName+".xls"); 
    response.setHeader("Content-Description", "JSP Generated Data");

%>   -
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>사용자 정보</title>
	
	</head>
 	<body>
		<table width="100%;" id="tableList" class="fix_rable" cellspacing="0" cellpadding="0" border="1">		
			<thead>			
				<tr> 
					<th style="background-color:#eff0f0;" >번호</th>			
					<th style="background-color:#eff0f0;" >아이디(ID)</th>							
					<th style="background-color:#eff0f0;" >이름</th>
					<th style="background-color:#eff0f0;" >소속회사</th>
					<th style="background-color:#eff0f0;" >부서</th>
					<th style="background-color:#eff0f0;" >팀</th>
					<th style="background-color:#eff0f0;" >상태</th>
					<th style="background-color:#eff0f0;" >수정일</th>
				</tr>
			</thead>
		    <tbody>
			<c:choose> 		
					<c:when test="${mainList != null && not empty mainList}"> 	
						<c:set var = "SQUOT">'</c:set>
						<c:set var = "DQUOT">"</c:set>
						<c:forEach items="${mainList}" var="rows" varStatus="loop">
							<tr>	
								<td align="center">${rows.unid}</td>	
								<td align="center">${rows.user_id}</td>
								<td align="center">${fn:replace(fn:replace(fn:replace(fn:replace(rows.user_name,SQUOT,"&#39;"),DQUOT,"&quot;"),"<","&lt;"),">","&gt;")}</td>
			
								<td align="center">${rows.k_company_nm}</td>
								<td align="center">${rows.k_dept_nm}</td>
								<td align="center">${rows.k_team_nm}</td>
								<td align="center">${rows.k_status_nm}</td>
								<td align="center">${rows.update_dat}</td>
							</tr>
						</c:forEach> 		
					</c:when> 		
					<c:otherwise> 				
						<tr> 					
							<td class="gtd"  colspan="8">조회된 데이터가 없습니다.</td> 			
						</tr> 		
					</c:otherwise> 
				</c:choose>  
			</tbody> 
		</table>
	</body>
</html>