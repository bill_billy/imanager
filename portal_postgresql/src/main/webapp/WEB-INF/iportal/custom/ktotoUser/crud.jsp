<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Language" content="ko">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title id='Description'>사용자관리</title>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/ktoto/css/style.blueish.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/ktoto/css/jqx.blueish.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
	<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
    <script src="../../resources/js/util/remoteInfo.js"></script>
    <script src="../../resources/js/jquery/jquery.validate.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>


    <script type="text/javascript">// button
	    var remoteIP = '<%=request.getRemoteAddr() %>';
		var remoteOS = getOSInfoStr();
		var remoteBrowser = getBrowserName();
		var getCode1,getCode2,getCode3;
		var loadingMsg = '데이터 로딩중 입니다.';
        $(this).ready(function () {  
        	
        	init();
        	
		});
        function showMsg(){
        	$i.dialog.showLoader('SYSTEM',loadingMsg); 
        }
        function init(){
        	$(window).ajaxStart(showMsg);
    		$(window).ajaxStop(function(){
    			$i.dialog.hideLoader();
    		});
        	$("#jqxButtonSearch").jqxButton({ width: '',  theme:'blueish'}); 
            $("#jqxButtonExcel").jqxButton({ width: '',  theme:'blueish'}); 
			$("#jqxButtonNew").jqxButton({ width: '',  theme:'blueish'});  
			$("#jqxButtonSave").jqxButton({ width: '',  theme:'blueish'});  
			$("#jqxButtonDelete").jqxButton({ width: '',  theme:'blueish'});  
			
	        $("#hiddenRemoteIP").val(remoteIP);
	        $("#hiddenRemoteOS").val(remoteOS);
	        $("#hiddenRemoteBW").val(remoteBrowser);
	        
	        makeTopCombo();
	       	makeGridCombo();
	        search();
	     //   makeGrid();
        }
        
        
        function makeTopCombo(){ //조회조건 콤보박스
        	getCode1 = $i.post("../../custom/ktotoUser/getCodeList", {com_grp_cd:'USER_DEPT'})//부서
        	getCode1.done(function(data){
     			data.returnArray.unshift({COM_CD:'', COM_CD_NM:'전체'});
     			var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'COM_CD' },
                        { name: 'COM_CD_NM' }
                    ],
					localdata:data,
                    async: false
                };
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#topDeptCombo").jqxComboBox({ 
				selectedIndex: 0, 
				source: dataAdapter, 
				animationType: 'fade', 
				dropDownHorizontalAlignment: 'right', 
				displayMember: "COM_CD_NM", 
				valueMember: "COM_CD", 
				dropDownWidth: 150, 
				dropDownHeight: 100, 
				width: 150, 
				height: 22,  
				theme:'blueish'});
     		});
        	getCode2 = $i.post("../../custom/ktotoUser/getCodeList", {com_grp_cd:'USER_TEAM'})//팀
        	getCode2.done(function(data){
        		data.returnArray.unshift({COM_CD:'', COM_CD_NM:'전체'});
     			var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'COM_CD' },
                        { name: 'COM_CD_NM' }
                    ],
					localdata:data,
                    async: false
                };
                var dataAdapter = new $.jqx.dataAdapter(source);
                $("#topTeamCombo").jqxComboBox({ 
    				selectedIndex: 0, 
    				source: dataAdapter, 
    				animationType: 'fade', 
    				dropDownHorizontalAlignment: 'right', 
    				displayMember: "COM_CD_NM", 
    				valueMember: "COM_CD", 
    				dropDownWidth: 100, 
    				dropDownHeight: 100, 
    				width: 100, 
    				height: 22,  
    				theme:'blueish'});
     		});
        	getCode3 = $i.post("../../custom/ktotoUser/getCodeList", {com_grp_cd:'USER_STATUS'})//상태
        	getCode3.done(function(data){
        		data.returnArray.unshift({COM_CD:'', COM_CD_NM:'전체'});
     			var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'COM_CD' },
                        { name: 'COM_CD_NM' }
                    ],
					localdata:data,
                    async: false
                };
                var dataAdapter = new $.jqx.dataAdapter(source);
                $("#topStatCombo").jqxComboBox({ 
				selectedIndex: 0, 
				source: dataAdapter, 
				animationType: 'fade', 
				dropDownHorizontalAlignment: 'right', 
				displayMember: "COM_CD_NM", 
				valueMember: "COM_CD", 
				dropDownWidth: 100, 
				dropDownHeight: 100, 
				width: 100, 
				height: 22,  
				theme:'blueish'});
     		});
        }
        
        function makeGridCombo(){ //사용자 정보 콤보박스
        	var getcode = $i.post("../../custom/ktotoUser/getCodeList", {com_grp_cd:'USER_COMPANY'})
     		getcode.done(function(data){
     			var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'COM_CD' },
                        { name: 'COM_CD_NM' }
                    ],
					localdata:data,
                    async: false
            	};
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#gridCpCombo").jqxComboBox({ 
				
				selectedIndex: 0, 
				source: dataAdapter, 
				animationType: 'fade', 
				dropDownHorizontalAlignment: 'right', 
				displayMember: "COM_CD_NM", 
				valueMember: "COM_CD", 
				dropDownWidth: 100, 
				dropDownHeight: 100, 
				width: 100, 
				height: 22,  
				theme:'blueish'});
	     	});
        	
        	var getcode = $i.post("../../custom/ktotoUser/getCodeList", {com_grp_cd:'USER_DEPT'})
     		getcode.done(function(data){
	     		var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                    	 { name: 'COM_CD' },
	                         { name: 'COM_CD_NM' }
	                    ],
						localdata:data,
	                    async: false
	            };
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#gridDpCombo").jqxComboBox({ 
				
				selectedIndex: 0, 
				source: dataAdapter, 
				animationType: 'fade', 
				dropDownHorizontalAlignment: 'right', 
				displayMember: "COM_CD_NM", 
				valueMember: "COM_CD", 
				dropDownWidth: 150, 
				dropDownHeight: 100, 
				width: 150, 
				height: 22,  
				theme:'blueish'});
	     	});
        	
        	var getcode = $i.post("../../custom/ktotoUser/getCodeList", {com_grp_cd:'USER_TEAM'})
     		getcode.done(function(data){
	     		var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                    	{ name: 'COM_CD' },
	                        { name: 'COM_CD_NM' }
	                    ],
						localdata:data,
	                    async: false
	            };
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#gridTeamCombo").jqxComboBox({ 
				
				selectedIndex: 0, 
				source: dataAdapter, 
				animationType: 'fade', 
				dropDownHorizontalAlignment: 'right', 
				displayMember: "COM_CD_NM", 
				valueMember: "COM_CD", 
				dropDownWidth: 100, 
				dropDownHeight: 100, 
				width: 100, 
				height: 22,  
				theme:'blueish'});
     		});
        	
        	var getcode = $i.post("../../custom/ktotoUser/getCodeList", {com_grp_cd:'USER_STATUS'})
     		getcode.done(function(data){
	     		var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                    	{ name: 'COM_CD' },
	                        { name: 'COM_CD_NM' }
	                    ],
						localdata:data,
	                    async: false
	            };
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#gridStatCombo").jqxComboBox({ 
				selectedIndex: 0, 
				source: dataAdapter, 
				animationType: 'fade', 
				dropDownHorizontalAlignment: 'right', 
				displayMember: "COM_CD_NM", 
				valueMember: "COM_CD", 
				dropDownWidth: 100, 
				dropDownHeight: 100, 
				width: 100, 
				height: 22,  
				theme:'blueish'});
	     	});
        }
        
        function makeGrid(){ //사용자 목록
        	var getList = $i.post("../../custom/ktotoUser/getUserList", {dept:$("#topDeptCombo").val(),team:$("#topTeamCombo").val(),status:$("#topStatCombo").val()})
        	getList.done(function(data){
	        	var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                        { name: 'UNID', type: 'string'},
	                        { name: 'ACCT_ID', type: 'string'},
	    					{ name: 'USER_ID', type: 'string'},
	    					{ name: 'USER_NAME', type: 'string'},
	    					{ name: 'USER_EMAIL', type: 'string'},
	    					{ name: 'USER_REGDATE', type: 'string'},
	    					{ name: 'K_OLAPID', type: 'string'},
	    					{ name: 'K_PHONE', type: 'string'},
	    					{ name: 'K_STATUS', type: 'string'},
	    					{ name: 'K_COMPANY', type: 'string'},
	    					{ name: 'K_DEPT', type: 'string'},
	    					{ name: 'K_TEAM', type: 'string'},
	    					{ name: 'K_STATUS_NM', type: 'string'},
	    					{ name: 'K_COMPANY_NM', type: 'string'},
	    					{ name: 'K_DEPT_NM', type: 'string'},
	    					{ name: 'K_TEAM_NM', type: 'string'},
	    					{ name: 'K_FAILCOUNT', type: 'string'},
	    					{ name: 'K_IP_IN', type: 'string'},
	    					{ name: 'K_IP_OUT', type: 'string'},
	    					{ name: 'K_SYS', type: 'string'},
	    					{ name: 'UPDATE_DAT', type: 'string'}
	                    ],
	                    localdata: data,
	                    sortcolumn: 'USER_ID',
	                    async: false,
	                    updaterow: function (rowid, rowdata, commit) {
	                        commit(true);
	                    }
	                };
	     			var noScript = function (row, columnfield, value) {//center
	    				var newValue = $i.secure.scriptToText(value);
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
					}; 
					
	    			var alginLeft = function (row, columnfield, value) {//left정렬
	                    return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
	    			}; 
	    			var alginRight = function (row, columnfield, value) {//right정렬
	                    return '<div id="userName-' + row + '"style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
	    			};
	    			
	    			var detail = function (row, columnfield, value) {//그리드 선택시 하단 상세
						var newValue = $i.secure.scriptToText(value);
						var link = "<a href=\"javaScript:detailUser("+row+");\" style='color: #000000;text-decoration:underline;'>" + newValue + "</a>";
	
						return "<div style='text-align:center; margin:4px 0px 0px 0px;'>" + link + "</div>";
						
					};
	    			
	    				  
	                var dataAdapter = new $.jqx.dataAdapter(source);
	
	                $("#jqxGrid01").jqxGrid(
	                {
	                    width: '100%',
	                    height: '100%',
	    				altrows:true,
	    				pageable: true,
	    				pageSize: 100,
	    			    pageSizeOptions: ['100', '200', '300'],
	                    source: dataAdapter,
	    				theme:'blueish',
	    				columnsheight:25 ,
	    				rowsheight: 25,
	                    columnsresize: true,
	    				//autorowheight: true,
	    				sortable:true,
	                    columns: [
	                      { text: '번호', datafield: 'UNID', width: '12%', align:'center', cellsalign: 'center'},
	                      { text: '아이디', datafield: 'USER_ID', width: '12%', align:'center', cellsalign: 'center',cellsrenderer:detail},
	    				  { text: '이름', datafield: 'USER_NAME', width: '12%', align:'center', cellsalign: 'center',cellsrenderer:noScript},
	    				  { text: '소속회사', datafield: 'K_COMPANY_NM', width: '14%', align:'center', cellsalign: 'center',cellsrenderer:noScript},
	    				  { text: '부서', datafield: 'K_DEPT_NM', width: '14%', align:'center', cellsalign: 'center',cellsrenderer:noScript},
	    				  { text: '팀', datafield: 'K_TEAM_NM', width: '12%', align:'center', cellsalign: 'center',cellsrenderer:noScript},
	    				  { text: '상태', datafield: 'K_STATUS_NM', width: '12%', align:'center', cellsalign: 'center'},
	    				  { text: '수정일', datafield: 'UPDATE_DAT', width: '12%', align:'center', cellsalign: 'center'}
	                    ]
	                });
	    			//$("#jqxGrid01").jqxGrid('selectrow', 0);
               
        	});	
        }
        function detailUser(row){
        	var unid = $("#jqxGrid01").jqxGrid("getrowdata", row).UNID;
        	var userId = $("#jqxGrid01").jqxGrid("getrowdata", row).USER_ID;
        	var olapId = $("#jqxGrid01").jqxGrid("getrowdata", row).K_OLAPID;
        	var userName = $("#jqxGrid01").jqxGrid("getrowdata", row).USER_NAME;
        	var kPhone = $("#jqxGrid01").jqxGrid("getrowdata", row).K_PHONE;
        	var userEmail = $("#jqxGrid01").jqxGrid("getrowdata", row).USER_EMAIL;
        	var kStatus = $("#jqxGrid01").jqxGrid("getrowdata", row).K_STATUS;
        	var kCompany = $("#jqxGrid01").jqxGrid("getrowdata", row).K_COMPANY;
        	var kDept = $("#jqxGrid01").jqxGrid("getrowdata", row).K_DEPT;
        	var kTeam = $("#jqxGrid01").jqxGrid("getrowdata", row).K_TEAM;
        	var kFailCount = $("#jqxGrid01").jqxGrid("getrowdata", row).K_FAILCOUNT;
        	var kIpin = $("#jqxGrid01").jqxGrid("getrowdata", row).K_IP_IN;
        	var kIpout = $("#jqxGrid01").jqxGrid("getrowdata", row).K_IP_OUT;
        	var kSys = $("#jqxGrid01").jqxGrid("getrowdata", row).K_SYS;
        	
        	$("#hiddenUnid").val(unid);
        	$("#txtUserID").val(userId);
        	$("#txtOlapID").val(olapId);
        	$("#txtUserName").val(userName);
        	$("#txtKphone").val(kPhone);
        	$("#hiddenKphone").val(kPhone);
        	$("#txtUserEmail").val(userEmail);
        	$("#gridStatCombo").val(kStatus);
        	$("#gridCpCombo").val(kCompany);
        	$("#gridDpCombo").val(kDept);
        	$("#gridTeamCombo").val(kTeam);
        	$("#txtFailCount").val(kFailCount);
        	$("#txtSys").val(kSys);
        	
        	$("#txtKipin").val(kIpin);
        	var splitIp1 = kIpin.split('.');
        	if(splitIp1!='' && splitIp1!='...'){
        		for(var i=1;i<splitIp1.length+1;i++){
        			$("#txtKipin0"+i).val(splitIp1[i-1]);
        		}
        	}else{
        		for(var i=1;i<5;i++){
        			$("#txtKipin0"+i).val('');
        		}
        	}
        	$("#txtKipout").val(kIpout);
        	var splitIp2 = kIpout.split('.');
			if(splitIp2!='' && splitIp2!='...'){
				for(var i=1;i<splitIp2.length+1;i++){
        			$("#txtKipout0"+i).val(splitIp2[i-1]);
        		}
        	}else{
        		for(var i=1;i<5;i++){
        			$("#txtKipout0"+i).val('');
        		}
        	}
        	$( "#txtFailCount" ).attr( 'readonly', false );
        	$( "#txtUserID" ).attr( 'readonly', true );
        }
        
        function search(){
    		var ajaxArray = [getCode1,getCode2,getCode3];
    		$.when.apply($,ajaxArray).done(function(){
    			makeGrid();
    			$('#jqxGrid01').jqxGrid('clearselection');
    			newUser();
    			$("#hiddenDept").val($("#topDeptCombo").val());
    			$("#hiddenTeam").val($("#topTeamCombo").val());
    			$("#hiddenStat").val($("#topStatCombo").val());
    		});
    	}
      	function goExcel(){
        	windowOpen('./excelDownByUserInfo?dept='+$("#hiddenDept").val()+'&team='+$("#hiddenTeam").val()+'&status='+$("#hiddenStat").val(), "_self");
      	}
    	function checkUserID(){
            		
            var txtID = $("#txtUserID").val();
        	if(txtID.length>20){
        		$i.dialog.error('SYSTEM', 'ID를 20자이내로 입력하세요.');
        		return false;
        	}
        	if(txtID.trim()==''){
        		$i.dialog.error('SYSTEM', 'ID를 입력하세요.');
        		return false;
        	}
            var userCheck = $i.post("../../custom/ktotoUser/getCheckUserID", {userID:$("#txtUserID").val()});
        	userCheck.done(function(data){
    	    	if(data.returnCode == "EXCEPTION" ){
    	    		$i.dialog.error("SYSTEM", data.returnMessage);
    	    		$("#txtUserID").focus();
    	    		$("#txtUserID").val("");
    	    		return;
    	    	}else{
    	    		$i.dialog.alert("SYSTEM", data.returnMessage,function(){
    	    			
    	    		});
    	    	}
        	});
         }
    	function checkOlapID(){
    		
            var txtID = $("#txtOlapID").val();
        	if(txtID.length>20){
        		$i.dialog.error('SYSTEM', 'ID를 20자이내로 입력하세요.');
        		return false;
        	}
        	if(txtID.trim()==''){
        		$i.dialog.error('SYSTEM', 'ID를 입력하세요.');
        		return false;
        	}
            var idCheck = $i.post("../../custom/ktotoUser/getCheckOlapID", {olapID:$("#txtOlapID").val()});
            idCheck.done(function(data){
    	    	if(data.returnCode == "EXCEPTION" ){
    	    		$i.dialog.error("SYSTEM", data.returnMessage);
    	    		$("#txtOlapID").focus();
    	    		$("#txtOlapID").val("");
    	    		return;
    	    	}else{
    	    		$i.dialog.alert("SYSTEM", data.returnMessage,function(){
    	    			
    	    		});
    	    	}
        	});
         }
      	function newUser(){
      		$('#jqxGrid01').jqxGrid('clearselection');
      		$('#saveForm').jqxValidator('hide');
      		$("#saveForm")[0].reset();
      		$("#hiddenUnid").val("");
      		$("#gridStatCombo").jqxComboBox('selectIndex', 0 ); 
      		$("#gridCpCombo").jqxComboBox('selectIndex', 0 ); 
      		$("#gridDpCombo").jqxComboBox('selectIndex', 0 ); 
      		$("#gridTeamCombo").jqxComboBox('selectIndex', 0 ); 
      		
      		$( "#txtFailCount" ).attr( 'readonly', true );
      		$( "#txtUserID" ).attr( 'readonly', false );
      	}
    	function insert(){
    		$('#saveForm').jqxValidator('hide');
    		$('#saveForm').jqxValidator({
    			rtl:true,  
    			rules:[
    			    { input: "#txtUserID", message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
    			    { input: "#txtUserID", message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'length=1,20' },
    			    { input: "#txtOlapID", message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
    			    { input: "#txtOlapID", message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'length=1,20' },
                    { input: '#txtUserName', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtUserName', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'length=1,20' },
                    { input: "#txtKphone", message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required'},
                    { input: "#txtKphone", message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: function (input, commit) {
                    	var regx = /^(01[016789]{1})-?[0-9]{3,4}-?[0-9]{4}$/;
                        var result=true;
                        var phoneNum = $("#txtKphone").val();
                        result = regx.test(phoneNum);
                        return result;
                        }
                    },
                    { input: '#txtUserEmail', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required'},
                    { input: '#txtUserEmail', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'email'},
                    { input: '#txtKipin01', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: function (input, commit) {
                    	var regx = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
                    	var result=true;
                    	var ipin = $("#txtKipin01").val()+"."+$("#txtKipin02").val()+"."+$("#txtKipin03").val()+"."+$("#txtKipin04").val();
                    	$("#txtKipin").val(ipin);
                    	result = regx.test(ipin);
                    
                    	return result;
                    	}
                    },
                    { input: '#txtKipout01', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: function (input, commit) {
                    	var regx = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
                    	var result=true;
                    	var ipout = $("#txtKipout01").val()+"."+$("#txtKipout02").val()+"."+$("#txtKipout03").val()+"."+$("#txtKipout04").val();
                    	$("#txtKipout").val(ipout);
                    	result = regx.test(ipout);
                    	
                    	return result;
                    	}
                    },
                    { input: '#txtFailCount', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'number'}
                ]
    		});
    		/*
      		var checkCount = $("#listBoxGroup").jqxListBox("getCheckedItems").length;
      		var checkVal = "";
      		for(var i=0;i<checkCount;i++){
      			if(i == checkCount -1){
      				checkVal += $("#listBoxGroup").jqxListBox("getCheckedItems")[i].value;
      			}else{
      				checkVal += $("#listBoxGroup").jqxListBox("getCheckedItems")[i].value+",";
      			}
      		}
      		$("#hiddenCheckValue").val(checkVal);*/
      		if($("#saveForm").jqxValidator("validate") == true){
      			if($("#hiddenUnid").val()==''){//신규
      				loadingMsg='임시비밀번호를 이메일로 전송중입니다.';
      			}
    	  		$i.insert("#saveForm").done(function(args){
    	  			loadingMsg='데이터 로딩중 입니다.';
    	  			if(args.returnCode == "EXCEPTION"){
    	  				if(args.returnMessage == "MAILERROR"){//임시비밀번호 전송실패, 사용자등록완료
    						$i.dialog.error("SYSTEM","임시 비밀번호 전송이 실패했습니다");
    						search();
    					}else{
    						$i.dialog.error("SYSTEM",args.returnMessage);
    					}
    	  			}else{
    	          		$i.dialog.alert("SYSTEM","저장 되었습니다.",function(){
    	          			search();
    	      			});
    	  			}
    	  		}).fail(function(e){
    	  			loadingMsg='데이터 로딩중 입니다.';
    	  			$i.dialog.error("SYSTEM",args.returnMessage);
    	  		});
      		}
    	}
    	function remove(){
    		if($("#hiddenUnid").val()==''){
    			$i.dialog.error("SYSTEM",'삭제할 사용자를 선택하세요.');
    			return;
    		}
    		$i.dialog.confirm("SYSTEM","삭제 하시겠습니까?",function(){
    			$i.remove("#saveForm").done(function(args){
        			if(args.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM",args.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
        					search();
        				});
        			}
        		}).fail(function(e){ 
        			$i.dialog.error("SYSTEM",args.returnMessage);
        		});	
    		});
    	}
    	function sendPwd(){
    		if($("#hiddenUnid").val()==''){
    			$i.dialog.error("SYSTEM",'초기화할 사용자를 선택하세요.');
    			return;
    		}else{
    			$i.dialog.confirm("전송 방식 선택","<div style='float:right;'><div style='margin-bottom:8px;'>아래에서 선택해 주세요</div><div>"+
    		    	"<input type='radio' name='sendType' checked value='email'/>Email 전송 <input type='radio' name='sendType' value='sms' style='margin-left:5px;'/>SMS 전송</div></div>",function(data){
    		    		var sendTyp = data.sendType;
    		    		
    		    		if(sendTyp == 'email'){
    		    			loadingMsg='임시비밀번호를 이메일로 전송중입니다.';
    		    			var resetPwd = $i.post("../../custom/ktotoUser/resetPassword", {type:sendTyp,unid:$("#hiddenUnid").val(),userId:$("#txtUserID").val()
        		    			,remoteIP:remoteIP,remoteOS:remoteOS,remoteBrowser:remoteBrowser});
    		    			resetPwd.done(function(args){    	
        		    			loadingMsg='데이터 로딩중 입니다.';
        		    			if(args.returnCode == "EXCEPTION"){
        	    	  				if(args.returnMessage == "MAILERROR"){//임시비밀번호 전송실패, 사용자등록완료
        	    						$i.dialog.error("SYSTEM","임시 비밀번호 전송이 실패했습니다");
        	    						search();
        	    					}else{
        	    						$i.dialog.error("SYSTEM",args.returnMessage);
        	    					}
        	    	  			}else{
        	    	          		$i.dialog.alert("SYSTEM",args.returnMessage,function(){
        	    	          			search();
        	    	      			});
        	    	  			}
        		    		}).fail(function(e){
        	    	  			loadingMsg='데이터 로딩중 입니다.';
        	    	  			$i.dialog.error("SYSTEM",args.returnMessage);
        	    	  		});
    		    		} 
    		    		else if(sendTyp == 'sms'){
    		    			//$i.dialog.error("SYSTEM","준비중입니다.");
    		    			
    		    			loadingMsg='SMS를 전송중입니다.';
    		    			var resetPwd = $i.post("../../custom/ktotoUser/resetPassword", {type:sendTyp,unid:$("#hiddenUnid").val(),userId:$("#txtUserID").val()
        		    			,remoteIP:remoteIP,remoteOS:remoteOS,remoteBrowser:remoteBrowser});
    		    			resetPwd.done(function(args){    	
        		    			loadingMsg='데이터 로딩중 입니다.';
        		    			if(args.returnCode == "EXCEPTION"){
        	    	  				if(args.returnMessage == "SMSERROR"){//임시비밀번호 전송실패, 사용자등록완료
        	    						$i.dialog.error("SYSTEM","임시 비밀번호 전송이 실패했습니다");
        	    						search();
        	    					}else{
        	    						$i.dialog.error("SYSTEM",args.returnMessage);
        	    					}
        	    	  			}else{
        	    	          		$i.dialog.alert("SYSTEM",args.returnMessage,function(){
        	    	          			search();
        	    	      			});
        	    	  			}
        		    		}).fail(function(e){
        	    	  			loadingMsg='데이터 로딩중 입니다.';
        	    	  			$i.dialog.error("SYSTEM",args.returnMessage);
        	    	  		});
    		    		}
    		    });
    			
    		}
    	}
        
    </script>


  <style  type="text/css">
	.jqx-validator-hint{
		background-color : transparent !important;
		border:0px !important;
	}
	.jqx-validator-hint-arrow{
		display:none !important;
	}
	</style>
</head>
<body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
<div class="wrap" style="width:98%; min-width:1040px; margin:0 10px;">
  <div class="header f_left" style="width:100%; height:27px; margin:10px 0">
	<div class="label type1 f_left">부서:</div>
	<div class="combobox f_left"  id='topDeptCombo' style="margin:2px 5px;"></div>
	<div class="label type1 f_left">팀:</div>
	<div class="combobox f_left"  id='topTeamCombo' style="margin:2px 5px;"></div>
	<div class="label type1 f_left">상태:</div>
	<div class="combobox f_left"  id='topStatCombo' style="margin:2px 5px;"></div>
	<input type="hidden" id="hiddenDept" value=""/>
	<input type="hidden" id="hiddenTeam" value=""/>
	<input type="hidden" id="hiddenStat" value=""/>
    <div class="group_button f_right">
     <div class="button type1 f_left">
       <input type="button" value="조회" id='jqxButtonSearch' width="100%" height="100%" onclick="search();"/>
     </div>
     <div class="button type1 f_left">
       <input type="button" value="Excel" id='jqxButtonExcel' width="100%" height="100%" onclick="goExcel();"/>
     </div>
    </div>
    <!--group_button--> 
  </div>
      <!--//header-->
  <div class="container  f_left" style="width:100%; margin:10px 0;">
    <div class="content f_left" style=" width:100%; margin:0;">
    	 <div class="grid f_left" style="width:100%; height:450px;">
            <div id="jqxGrid01">
            </div>
         </div>
         <!--grid -->
  	</div>
    <!--//content-->    
	<div class="content f_left" style=" width:100%; margin:10px 0;">
			<div class="group f_left  w100p m_b5">
				<div class="label type2 f_left">사용자 상세</div>
				<div class="f_right" style="color:#e72822; font-size:13px; margin-top:5px;font-weight:bold;">* 신규사용자 입력시 입력된 이메일로 임시비밀번호가 전송됩니다.</div>
				<!--group_button-->
			</div>
			<!--group-->
			<div class="table  f_left" style="width:100%; margin:0; ">
			<form id="saveForm" name="saveForm">
				<input type="hidden" id="hiddenRemoteIP" name="remoteIP" />
				<input type="hidden" id="hiddenRemoteOS" name="remoteOS" />
				<input type="hidden" id="hiddenRemoteBW" name="remoteBrowser" />
			  <table width="100%" cellspacing="0" cellpadding="0" border="0">
                <tbody>
                  <tr>
                    <th class="w15p"><div> 아이디(ID)<span class="th_must"></span> </div></th>
                    <td><div class="cell">
                      <input type="hidden" id="hiddenUnid" name="unID" value="" />
                      <input type="text" value="" id="txtUserID" name="userID" class="input type2  f_left"  style="width:130px; margin:4px 0; " onChange="checkUserID();"/>
                    </div></td>
                    <th class="w15p"><div> OLAP ID<span class="th_must"></span> </div></th>
                    <td><div class="cell">
                      <input type="text" value="" id="txtOlapID" name="olapID" class="input type2  f_left"  style="width:130px; margin:4px 0; " onChange="checkOlapID();"/>
                    </div></td>
                    <th><div> 이름<span class="th_must"></span> </div></th>
                    <td><div  class="cell">
                      <input type="text" value="" id="txtUserName" name="userName" class="input type2  f_left"  style="width:130px; margin:4px 0; "/>
                    </div></td>
                  <tr>
                    <th><div>휴대전화<span class="th_must"></span> </div></th>
                    <td><div  class="cell">
                      <input type="text" value="" id="txtKphone" name="kPhone" class="input type2  f_left"  style="width:130px; margin:4px 0; "/>
                      <input type="hidden" id="hiddenKphone"/>
                    </div></td>
                    <th><div> E-mail<span class="th_must"></span> </div></th>
                    <td><div class="cell">
                      <input type="text" value="" id="txtUserEmail" name="userEmail" class="input type2  f_left"  style="width:96%; margin:4px 0; "/>
                    </div></td>
                     <th><div> 상태<span class="th_must"></span> </div></th>
                      <td><div class="combobox f_left"  id='gridStatCombo' name="kStatus" style="margin:4px 5px;"></div></td>
                  </tr>
                  <tr>
                    <th class="w15p"><div> 소속회사<span class="th_must"></span> </div></th>
                    <td><div class="combobox f_left"  id='gridCpCombo' name="kCompany" style="margin:4px 5px;"></div></td>
                    <th class="w15p"><div> 부서<span class="th_must"></span> </div></th>
                    <td><div class="combobox f_left"  id='gridDpCombo' name="kDept" style="margin:4px 5px;"></div></td>
                    <th class="w15p"><div> 팀<span class="th_must"></span> </div></th>
                    <td><div class="combobox f_left"  id='gridTeamCombo' name="kTeam" style="margin:4px 5px;"></div></td>
                  </tr>
                  <tr>
                    <th><div> Login 시도횟수</div></th>
                    <td><div  class="cell">
                      <input id="txtFailCount" name="kFailCount" type="text" class="input type2 t_center f_left" readonly style="width:130px; margin:3px 0;" value=""/>
                    </div></td>
                    <th><div> 내부IP<span class="th_must"></span></div></th>
                    <td valign=bottom><div  class="cell">
                      <input id="txtKipin" name="kIpin" type="hidden" class="input type2 t_center f_left" style="width:130px; margin:3px 0;" value=""/>
                      <input id="txtKipin01" type="text" class="input type2 t_center f_left" style="width:30px; margin:3px 0;" value=""/><div style="float:left;margin-top:7px;font-weight:bold;">.</div>
                      <input id="txtKipin02" type="text" class="input type2 t_center f_left" style="width:30px; margin:3px 0;" value=""/><div style="float:left;margin-top:7px;font-weight:bold;">.</div>
                      <input id="txtKipin03" type="text" class="input type2 t_center f_left" style="width:30px; margin:3px 0;" value=""/><div style="float:left;margin-top:7px;font-weight:bold;">.</div>
                      <input id="txtKipin04" type="text" class="input type2 t_center f_left" style="width:30px; margin:3px 0;" value=""/>
                    </div></td>
                    <th><div> 외부IP<span class="th_must"></span></div></th>
                    <td valign=bottom><div  class="cell">
                      <input id="txtKipout" name="kIpout" type="hidden" class="input type2 t_center f_left" style="width:130px; margin:3px 0;" value=""/>
                      <input id="txtKipout01" type="text" class="input type2 t_center f_left" style="width:30px; margin:3px 0;" value=""/><div style="float:left;margin-top:7px;font-weight:bold;">.</div>
                      <input id="txtKipout02" type="text" class="input type2 t_center f_left" style="width:30px; margin:3px 0;" value=""/><div style="float:left;margin-top:7px;font-weight:bold;">.</div>
                      <input id="txtKipout03" type="text" class="input type2 t_center f_left" style="width:30px; margin:3px 0;" value=""/><div style="float:left;margin-top:7px;font-weight:bold;">.</div>
                      <input id="txtKipout04" type="text" class="input type2 t_center f_left" style="width:30px; margin:3px 0;" value=""/>
                    </div></td>
                  </tr>
                  <tr>
                    <th><div> 비고</div></th>
                    <td colspan="5">
                    <div class="cell">
                        <textarea id="txtSys" name="kSys" class="input type2"  style="width:99%; margin:4px 0;"></textarea>
                    </div></td>
                  </tr>
                </tbody>
              </table>
			</form>
			</div>
    <!--table-->
        <div class="group_button f_right" style="margin-top:10px;">
        	 <div class="button type2 f_left">
            <input type="button" value="비밀번호 초기화" id='jqxButtonReset' width="100%" height="100%" onclick="sendPwd();"/>
        	</div>
            <div class="button type2 f_left">
            <input type="button" value="신규" id='jqxButtonNew' width="100%" height="100%" onclick="newUser();"/>
        	</div>
            <div class="button type2 f_left">
            <input type="button" value="저장" id='jqxButtonSave' width="100%" height="100%" onclick="insert();" />
       		</div>
            <div class="button type3 f_left">
            <input type="button" value="삭제" id='jqxButtonDelete' width="100%" height="100%" onclick="remove();"/>
        	</div>
        </div>
    <!--group_button-->
		</div>
    <!--//content-->    
  </div>
  <!--//container--> 
</div>
<!--//wrap-->
</body>
</html>