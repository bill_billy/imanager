 <%@ page language="java" contentType="application/vnd.xls;charset=UTF-8" pageEncoding="utf-8"%>   -
<%@page import="java.net.URLEncoder"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%

	String header =request.getHeader("User-Agent");
	String browser="";  
	String docName = URLEncoder.encode("이메일발송이력", "utf-8");  
	 
	System.out.println("header++++++++++++++++++++++++++++++"+header+":::::"+docName);
	 
	
    response.setHeader("Content-Disposition", "attachment; filename="+docName+".xls"); 
    response.setHeader("Content-Description", "JSP Generated Data");

%>   -
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>이메일발송이력</title>
	
	</head>
 	<body>
		<table width="100%;" id="tableList" class="fix_rable" cellspacing="0" cellpadding="0" border="1">		
			<thead>			
				<tr> 
					<th rowspan=2 style="background-color:#eff0f0;" >일시</th>			
					<th rowspan=2 style="background-color:#eff0f0;" >구분</th>
					<th colspan=3 style="background-color:#eff0f0;" >송신자</th>							
					<th colspan=4 style="background-color:#eff0f0;" >수신자</th>
					<th rowspan=2 style="background-color:#eff0f0;" >IP</th>
					<th rowspan=2 style="background-color:#eff0f0;" >OS</th>
					<th rowspan=2 style="background-color:#eff0f0;" >BROWSER</th>
				</tr>
				<tr>
					<th style="background-color:#eff0f0;" >팀</th>	
					<th style="background-color:#eff0f0;" >ID</th>		
					<th style="background-color:#eff0f0;" >이름</th>	
					<th style="background-color:#eff0f0;" >팀</th>	
					<th style="background-color:#eff0f0;" >ID</th>	
					<th style="background-color:#eff0f0;" >이름</th>	
					<th style="background-color:#eff0f0;" >EMAIL</th>		
				</tr>
			</thead>
		    <tbody>
			<c:choose> 		
					<c:when test="${mainList != null && not empty mainList}"> 	
						<c:set var = "SQUOT">'</c:set>
						<c:set var = "DQUOT">"</c:set>
						<c:forEach items="${mainList}" var="rows" varStatus="loop">
							<tr>	
								<td align="center">${rows.insert_dat}</td>
								<td align="center">${rows.send_typ_nm}</td>
								<td align="center">${fn:replace(fn:replace(fn:replace(fn:replace(rows.send_dept_nm,SQUOT,"&#39;"),DQUOT,"&quot;"),"<","&lt;"),">","&gt;")}</td>
								<td align="center">${rows.send_id}</td>
								<td align="center">${rows.send_name}</td>
								
								<td align="center">${fn:replace(fn:replace(fn:replace(fn:replace(rows.recv_dept_nm,SQUOT,"&#39;"),DQUOT,"&quot;"),"<","&lt;"),">","&gt;")}</td>
								<td align="center">${rows.recv_id}</td>
								<td align="center">${rows.recv_name}</td>
								<td align="center">${rows.recv_email}</td>
								<td align="center">${rows.remote_ip}</td>
								<td align="center">${rows.remote_os}</td>
								<td align="center">${rows.remote_browser}</td>
							</tr>
						</c:forEach> 		
					</c:when> 		
					<c:otherwise> 				
						<tr> 					
							<td class="gtd"  colspan="12">조회된 데이터가 없습니다.</td> 			
						</tr> 		
					</c:otherwise> 
				</c:choose>  
			</tbody> 
		</table>
	</body>
</html>