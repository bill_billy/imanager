<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
    <meta charset="utf-8">
     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
    <title>이메일이력조회</title>
   		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/ktoto/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/ktoto/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
    <script type="text/javascript">
    	var roleArray=[];
    	var grpArray=[];
    	var userArray=[];
        $(document).ready(function() {
        	$("#jqxButtonSearch").jqxButton({ width: '',  theme:'blueish'}); 
        	$("#jqxButtonExcel").jqxButton({ width: '',  theme:'blueish'}); 
        	
        	$("#dateInputStart").jqxInput({placeHolder: "", height: 23, width: 150, minLength: 1, theme:'blueish' });
        	$("#dateInputClose").jqxInput({placeHolder: "", height: 23, width: 150, minLength: 1, theme:'blueish' });
        	
            // 입력시작일, 입력종료일
            $('#dateInputStart, #dateInputClose').datepicker({
                //changeYear: true,
                //changeMonth: true,
                showOn: 'button',
                buttonImage: '../../resources/cmresource/image/icon-calendar.png',
                buttonImageOnly: true,
                dateFormat: 'yy-mm-dd' //2014-12-12
            });
            $('.ui-datepicker-trigger').each(function() {
    			$(this).css("vertical-align", "middle");
				$(this).css("padding-top", "4px");
				$(this).css("margin-left", "4px");
				$(this).css("float", "left");
    		});
            
            var nowDate = new Date();
    		var y = nowDate.getFullYear();
    		var m = nowDate.getMonth() + 1;
    		if(m<10) //1~9월
    			m = "0" + m;
    		var d = nowDate.getDate();
    		if(d<10)
    			d = "0" + d;
    		var calDate = y+"-"+m+"-"+d;
    		
    		$('#dateInputStart').val(calDate); 
    		$('#dateInputClose').val(calDate);
    		
    		$("#hiddenSdat").val(calDate);
    		$("#hiddenEdat").val(calDate);
    		
    		$('#dateInputStart').on('change', 
    				function (event) {
    				   var startDat = $('#dateInputStart').val(); 
    				   var endDat = $('#dateInputClose').val(); 
    				   if(startDat>endDat){
    					   $i.dialog.alert('SYSTEM', '시작일은 종료일보다 클 수 없습니다!'); 
    					  $('#dateInputStart').val(endDat); 
    				   }
    				}); 
    		
    		$('#dateInputClose').on('change', 
    				function (event) {
    				   var endDat = $('#dateInputClose').val(); 
    				   var startDat = $('#dateInputStart').val(); 
    				   if(startDat>endDat){
    					   $i.dialog.alert('SYSTEM', '시작일은 종료일보다 클 수 없습니다!'); 
    					   $('#dateInputClose').val(startDat); 
    				   }
    				}); 
    		
    		searchStart();
        });
		function searchStart(){
			$("#hiddenSdat").val($("#dateInputStart").val());
    		$("#hiddenEdat").val($("#dateInputClose").val());
    		
    		makeLogList();
    		
        }
		function makeLogList(){ //이메일 이력 리스트
			var $jqxgrid1 = $('#jqxGrid01');
    		$jqxgrid1.jqxGrid('clearselection');
    	
        	var getList = $i.post("../../custom/ktotoEmailLog/getEmailLogList",{startDat:$("#hiddenSdat").val(),endDat:$("#hiddenEdat").val()});
        	getList.done(function(data){
	        	var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                        { name: 'SEND_ID', type: 'string'},
	    					{ name: 'SEND_NAME', type: 'string'},
	    					{ name: 'SEND_DEPT_NM', type: 'string'},
	    					{ name: 'SEND_TYP_NM', type: 'string'},
	    					{ name: 'INSERT_DAT', type: 'string'},
	    					{ name: 'RECV_ID', type: 'string'},
	    					{ name: 'RECV_NAME', type: 'string'},
	    					{ name: 'RECV_DEPT_NM', type: 'string'},
	    					{ name: 'RECV_EMAIL', type: 'string'},
	    					{ name: 'REMOTE_IP', type: 'string'},
	    					{ name: 'REMOTE_OS', type: 'string'},
	    					{ name: 'REMOTE_BROWSER', type: 'string'}
	                    ],
	                    localdata: data,
	                    async: false,
	                    updaterow: function (rowid, rowdata, commit) {
	                        commit(true);
	                    }
	                };
	     			var noScript = function (row, columnfield, value) {//center
	    				var newValue = $i.secure.scriptToText(value);
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
					}; 
					
	    			var alginLeft = function (row, columnfield, value) {//left정렬
	                    return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
	    			}; 
	    			var alginRight = function (row, columnfield, value) {//right정렬
	                    return '<div id="userName-' + row + '"style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
	    			};
	    			
	    			var detail = function (row, columnfield, value) {//그리드 선택시 하단 상세
						var newValue = $i.secure.scriptToText(value);
						var link = "<a href=\"javaScript:detailLog("+row+");\" style='color: #000000;text-decoration:underline;'>" + newValue + "</a>";
	
						return "<div style='text-align:center; margin:4px 0px 0px 0px;'>" + link + "</div>";
						
					};
	    			
	    				  
	                var dataAdapter = new $.jqx.dataAdapter(source);
	
	                $("#jqxGrid01").jqxGrid(
	                {
	                    width: '100%',
	                    height: '100%',
	    				altrows:true,
	    				pageable: true,
	    				pageSize: 100,
	    			    pageSizeOptions: ['100', '200', '300'],
	                    source: dataAdapter,
	    				theme:'blueish',
	    				columnsheight:25 ,
	    				rowsheight: 25,
	                    columnsresize: true,
	    				//autorowheight: true,
	    				sortable:true,
	                    columns: [
	                      { text: '일시', datafield: 'INSERT_DAT', width: '10%', align:'center', cellsalign: 'center'},
		                  { text: '구분', datafield: 'SEND_TYP_NM', width: '10%', align:'center', cellsalign: 'center'},
		                  { text: '팀', datafield: 'SEND_DEPT_NM', width: '10%', align:'center', cellsalign: 'center',columngroup:"A"},
	                      { text: 'ID', datafield: 'SEND_ID', width: '10%', align:'center', cellsalign: 'center',columngroup:"A"},
	                      { text: '이름', datafield: 'SEND_NAME', width: '10%', align:'center', cellsalign: 'center',columngroup:"A"},
	                      { text: '팀', datafield: 'RECV_DEPT_NM', width: '10%', align:'center', cellsalign: 'center',columngroup:"B"},
	                      { text: 'ID', datafield: 'RECV_ID', width: '10%', align:'center', cellsalign: 'center',columngroup:"B"},
	                      { text: '이름', datafield: 'RECV_NAME', width: '10%', align:'center', cellsalign: 'center',columngroup:"B"},
	                      { text: 'EMAIL', datafield: 'RECV_EMAIL', width: '12%', align:'center', cellsalign: 'center',columngroup:"B"},
	                      { text: 'IP' , datafield:'REMOTE_IP', align:'center', cellsalign:'center', width: '10%'},
						  { text: 'OS' , datafield:'REMOTE_OS', align:'center', cellsalign:'center', width: '10%'},
						  { text: 'BROWSER' , datafield:'REMOTE_BROWSER', align:'center', cellsalign:'center', width: '10%'}
	                      
	                    ],
	                    columngroups:[
							{ text : '송신자', align:'center', name:'A'},
							{ text : '수신자', align:'center', name:'B'}
		                ]
	                });
               
        	});	
    	}
		function goExcel(){
    		var sDat = $("#hiddenSdat").val();
    		var eDat = $("#hiddenEdat").val();
    		
    		windowOpen('../../custom/ktotoEmailLog/excelDownByEmailLog?startDat='+sDat+'&endDat='+eDat, "_self");
    		
    	}
    </script>

    </head>
    <body class='blueish'>
    <!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
    <div class="wrap" style="width:98%; min-width:1280px; margin:0 10PX;">
      <div class="header f_left" style="width:100%; height:27px; margin:10px 0">
      	<input type="hidden" id="hiddenSdat"/>
        <input type="hidden" id="hiddenEdat"/>
        <div class="label type1 f_left">시작일 : </div>
        <input id="dateInputStart" type="text" class="f_left" style="margin-left:5px;" readOnly/>
         
      
        <div class="label type1 f_left">종료일 : </div>
        <input id="dateInputClose" type="text" class="f_left" style="margin-left:5px;" readOnly/>
    
        <div class="group_button f_right">
         <div class="button type1 f_left">
           <input type="button" value="조회" id='jqxButtonSearch' width="100%" height="100%" onclick="searchStart();"/>
         </div>
          <div class="button type1 f_left">
			<input type="button" value="Excel" id='jqxButtonExcel' width="100%" height="100%" onclick="goExcel();"/>
		  </div>
        </div>
        <!--group_button--> 
      <!--//header-->
		</div>
		<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:100%;">
					<div class="tabs f_left" style=" width:100%; height:625px; margin-top:0px;">
						<div id='jqxGrid01'>
						</div>
					</div>
				</div>
		</div>
		<!--//container-->
	</div>
<!--//wrap-->
</body>
</html>