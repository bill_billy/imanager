
<%@page import="java.util.AbstractMap"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.HashMap"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 

<html> 
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>  
		<link rel="stylesheet" type="text/css" href="../../resources/css/theme/07/default.css" />
		<link rel="stylesheet" type="text/css" href="../../resources/css/theme/07/font-awesome.css" />
		<link rel="stylesheet" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css" type="text/css" />    
		
		<link rel="stylesheet" type="text/css" href="../../resources/css/theme/07/iplanbiz.control.button.css" />
		<link rel="stylesheet" type="text/css" href="../../resources/css/theme/07/iplanbiz.control.popup.css" />
		<link rel="stylesheet" href="../../resources/css/iplanbiz/styles/jqx.ui-redmond_lotte.css" type="text/css" />
		<link rel="stylesheet" href="../../resources/css/iplanbiz/styles/jqx.Lotte-depart.css" type="text/css" />
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>   
		<script src="../../resources/js/util/remoteInfo.js"></script>
 
		<style>
			html,body{
				height:100%;
			}
		</style>	
		<title>${reportNm }</title>
		<script type="text/javascript">
		
		
		
		var ajaxsubmit=function(selector,fnsucess,fnerror){
			var method = ($(selector).attr("method")!=null&&$(selector).attr("method")!="")?$(selector).attr("method"):"get";
			var action = $(selector).attr("action");
			var data = $(selector).serialize();
			
			var option = {
					type:method,
					url:action,
					data:data,
					async:false,
					success:fnsucess,
					error:fnerror
			};
			
			$.ajax(option);
		};
		var browserInfo=null;
		function goMyfolder(){
			var p = parent!=undefined&&this!=parent?parent:opener;
			
			if((parent==this?opener:p).isRegistFavoriteMenu('${realCid}')){ 
				$i.dialog.error("SYSTEM", "이미 등록된 보고서 입니다");
			}
			
			else{
				<%
					String attrMenuNm = null;
					if(request.getAttribute("menuNm")!=null)
						attrMenuNm = java.net.URLDecoder.decode(request.getAttribute("menuNm").toString(),"UTF-8");
					else attrMenuNm = request.getParameter("menuNm");
					String attrSummaryContent = "";
					if( request.getAttribute("summaryContent")!=null  )
						attrSummaryContent = java.net.URLDecoder.decode(request.getAttribute("summaryContent").toString(),"UTF-8");
					
					String attrChargeDept = "";
					if( request.getAttribute("chargeDept")!=null&& request.getAttribute("chargeDept").toString().equals("")==false  ){
						String temp = java.net.URLDecoder.decode(request.getAttribute("chargeDept").toString(),"UTF-8");
						temp = temp.replaceAll("\'","&#39;").replaceAll("\"","&quot;").replaceAll("<","&lt;").replaceAll(">","&gt;");
						attrChargeDept ="  <span style='font-weight:bold;'>·담당자</span> : " + temp +  " / ";
						
					}
					String attrChargeEmp = "";
					if( request.getAttribute("chargeEmp")!=null  ){
						String temp = java.net.URLDecoder.decode(request.getAttribute("chargeEmp").toString(),"UTF-8");
						temp = temp.replaceAll("\'","&#39;").replaceAll("\"","&quot;").replaceAll("<","&lt;").replaceAll(">","&gt;");
						attrChargeEmp = temp;
					}
					String attrConfirmDept = "";
					if( request.getAttribute("confirmDept")!=null && request.getAttribute("confirmDept").toString().equals("")==false ){
						String temp = java.net.URLDecoder.decode(request.getAttribute("confirmDept").toString(),"UTF-8");
						temp = temp.replaceAll("\'","&#39;").replaceAll("\"","&quot;").replaceAll("<","&lt;").replaceAll(">","&gt;");
						attrConfirmDept ="<span style='font-weight:bold;'>·확인자</span> : " + temp + " / " ;
					}
					String attrConfirmEmp = "";
					if( request.getAttribute("confirmEmp")!=null  ){
						String temp = java.net.URLDecoder.decode(request.getAttribute("confirmEmp").toString(),"UTF-8");
						temp = temp.replaceAll("\'","&#39;").replaceAll("\"","&quot;").replaceAll("<","&lt;").replaceAll(">","&gt;");
						attrConfirmEmp = temp;
					}
					
				%>
				(parent==this?this:p).addFavoriteMenu("<%=attrMenuNm%>",function(data){
					//alert(data.fList);
					var winType = "${param.winType}";
					if(winType == "newWin"){
						$("input[name='paramCid']").val("${param.cid}");
						$("input[name='pMenuNm']").val("${param.menuNm}");
						$("input[name='pType']").val(winType);
					}else{
						var title = "${param.menuNm}";
						var storeId = "${param.storeId}";
						if($("input[name='paramCid']").val() == ""){
							$("input[name='paramCid']").val(storeId);
						}
						$("input[name='pMenuNm']").val(title);
					}
					console.log(data.fList);
					$("input[name=pid]").val(p.$("#fList").val());
					
					$i.post("./insertBookReport", {cID:$("input[name='paramCid']").val(), menuName:$("input[name='pMenuNm']").val(),menuType:$("input[name='pType']").val(),pID:$("input[name='pid']").val()}).done(function(data){
						if(data.returnCode == "EXCEPTION"){
							$i.dialog.error("SYSTEM", data.returnMessage);
						}else{
							$i.dialog.alert("SYSTEM", data.returnMessage, function(){
								if(p.makeBookmark!=undefined){
									//setTimeout(function(){parent.makeBookmark();},2000);
									p.makeBookmark();
								}
							});
						}
					});
					
// // 					insertBookReport
// 					document.frm.target="ifrm";
// 					document.frm.action="${WEB.ROOT}/goMyfolderNoCog";//?searchPath="+searchPath;
// 					document.frm.submit();	
					
					//즐겨찾기 후 오른쪽 즐겨찾기 리스트 refresh 
					
				}); 
			}
			
		}
		function jqxpopupclose(id){
			$('#'+id).jqxWindow('close');
			$('#'+id).remove();  
		}
		function addFavoriteMenu(cname,callback){
			
			var menuList = Enumerable.From(opener.favoriteMenuList).Where(function(c) {
				return c.MENU_TYPE == "1";
			}).ToArray();
			var selecthtml = "<select id='fList' name='fList' style='height:20px;width:250px;padding-top:3px;'>";
			selecthtml+="<option value=5999>=폴더선택=</option>";
			$.each(menuList,function(idx,item){
				selecthtml+="<option value='"+item.C_ID+"'>"+item.C_NAME+"</option>";
			});
			selecthtml+="</select>";
			
			var gomyhtml="<table style='margin-top:5px;margin-left:10px;width:80%;float:right;border:0px;border-style:solid;border-color:gray;' cellpadding=0 cellspacing=0>"+
			"<tr style='height:25px;margin:2px;'>"+
			"<th style='width:100px;'>폴더</th><td style='padding:5px;'>"+selecthtml+"</td>"+ 
			"</tr>"+
			"<tr style='height:25px;margin:2px;'>"+
			"<th>메뉴명</th><td style = 'color:black;padding:5px;'>"+cname+"</td>"+
			"</tr>"+
			"</table>";
			$i.dialog.confirm("즐겨찾기",gomyhtml,function(data){
				callback(data);
			});
		} 
		function MsgShow(title,contents){
			var p = parent!=undefined&&this!=parent?parent:opener;
			p.messageLotte(title,contents);
		}
		function resizeTabsWidth(){ 
			//$("#iframe_cognos").height($(parent.window).height()-116); //122
			//$("#iframe_cognos").trigger("resize");
			
		}
		
		function resizeIframe(height_size){
			//$("#iframe_cognos").css("height", height_size);
		}
	 
		
		function setLog(targetName){
			
			var cid='';
	
			if('${type}' == 'audit' || '${type}' == 'cog'){
				cid = "${param.storeId}";
				targetTypeNo = 2006; 
			}else{
				targetTypeNo = 1002;
				cid = '${cid}';
			} 
			//log 기록
			var os = $i.client.os();
			var browser = $i.client.browser();
			$i.post("../../login/action/insertUserLog", {actionTypeNo : '1' , 
														 targetTypeNo: targetTypeNo, 
														 targetValue:decodeURIComponent(targetName),
														 targetComment:"",
														 cID:cid, 
														 //osInfo:getOSInfoStr(),
														 //browserInfo:getBrowserName(), 
														 //browserVersion:getBrowserVer(),
														 osInfo:os.name+"_"+os.version+"_"+os.platform,
														 browserInfo:browser.accessName, 
														 browserVersion:browser.version,														 
														 targetPath:'${path}'}
			).done(function(res){
			});
		}
		
	 	$(document).ready(function(){
	 		//parent.close();
			var c_id = '${realCid}';
			if(c_id=='') c_id = "${param.cid}";
			
	 	 	var winType = "${param.winType}";
	 	 	if(winType =="book"){
	 	 		$("#myfolder").hide();
	 	 	} 
			 
			setLog('${menuNm}');  
			if(self.opener) {
				window.focus();
				var agent = navigator.userAgent.toLowerCase();

				if('${type}' == 'audit' || '${type}' == 'cog'){
					if (agent.indexOf("chrome") != -1) {
						location.replace("${url}");
					}
				}
				
			
			}
			resizeTabsWidth(); 
			
			$("#helper").on("click",function(){
				var c_id = "${param.cid}";
				openHelpWin(c_id);	
			}); 
			$("#btnToggle").on("click", function(event){
				var bol = parent.toggleLeftTab();
				if(bol)
					$("#btnToggle").attr("src","../../resources/img/theme/07/left_hide_btn.gif"); 
				else
					$("#btnToggle").attr("src","../../resources/img/theme/07/right_hide_btn.gif");
			}); 
			$("#btnsch").on("click",popupCogSearch); 
			window.onresize = resizeTabsWidth; 
			if(top !=null && top.resetSessionTimer!=null) { 
	 			top.resetSessionTimer(); // 세션 시간 타이머를 초기화한다.
	 			//console.log("top reset session timer");

	 			$("#iframe_cognos").on("load",function(e){
	 				try{
		 				$("#iframe_cognos")[0].contentWindow.document.addEventListener("click",function(event){
			 				//console.log("Event top reset session timer");
			 				top.resetSessionTimer();
			 			},true);
	 				}catch(e){
	 					//console.log("unfired click event");
	 				}
	 			});  
	 		}
	 		if(opener!=null && opener.resetSessionTimer!=null){
	 			opener.resetSessionTimer();
	 			//console.log("opener reset session timer");

	 			$("#iframe_cognos").on("load",function(e){
	 				try{
			 			$("#iframe_cognos")[0].contentWindow.document.addEventListener("click",function(event){
			 				//console.log("Event opener reset session timer");
			 				opener.resetSessionTimer();
			 			},true);
	 				}catch(e){
	 					//console.log("unfired click event");
	 				}
	 			}); 
	 		} 
		});
	 	
	 	
	 	function addTab(a,b,c,d,e){
			var p = parent!=undefined?parent:opener;
			if(p!=null&&p.addTab!=undefined)
				p.addTab(a,b,c,d,e);
	 	}
		function popupCogSearch(){
			var p = parent!=undefined?parent:opener;
			 
			if(p!=null&&p.newWinPopUp!=undefined){
				p.popupCogSearch();//("/cogSearch","보고서검색",1034,500); 
			}
			else{
				newWinPopUp("/cogSearch","보고서검색",1034,500); 
			}
		} 
		var load_img_cnt = 0;
		function hidden_img(){
	    	$(".loading_box").hide();
		}
		
		function openHelpWin(cid){
			var path ="${path}";
			
			var menuName = "${menuNm}";
			if(cid.toString().indexOf("i")!=-1)
				menuName= encodeURI(menuName);
			if(parent!=this)
				parent.openHelpWin(cid,path,menuName);
			else{
				var popid = "help_"+cid;
				if($("#"+popid).length!=0) { 
					$("#"+popid).remove();
				}
					var popup = "<div id='"+popid+"' style='position:absolute;z-index:999999;'>"+
						"<div>도움말</div>"+
						"<div>"+ 
							"<iframe style='width:880px !important;height:530px !important;' frameborder='0' src='../../admin/help/helpPopup?cID="+cid + "&path=" + encodeURIComponent(path) + "&menuNm=" + encodeURIComponent(menuName) +"'></iframe>"+
						"</div>"+
					"</div>";
					$(document).find("#wrap").prepend(popup);
					var offset = {left:0,top:0};
					var size = ($("body").width()-820)/2; 
					$("#"+popid).jqxWindow({
						
						theme:'Lotte-depart',
			            position: { x: offset.left + size, y: offset.top + 150} ,
			            maxHeight: 600, maxWidth: 966, minHeight: 580, minWidth: 800, height: 580, width: 820,
			            resizable: true, isModal: true, modalOpacity: 0.7,
			            initContent: function (a,b,c,d,e) {
			            	$("#"+popid).find("iframe").css({width:"100%",height:"100%"}); 
			
			            }
			        }); 
					$("#"+popid).find("iframe").css({width:"100%",height:"100%"}); 
				 
			}
				
		}
		</script>
		<style>
		img{
			vertical-align:middle;
		}
		.content_header .p_path{
			background:url(${WEB.IMG }/main_icon.png) left no-repeat !important;
		}
		.body.loading, div, h1, h2, h3, h4, h5, h6, p, span {
			padding: 0;
			margin: 0;
			font-family:"굴림", "맑은 고딕", arial;
			line-height:1.5em;
		}
		.loading .loading_box {
			text-align:center;
			width:110px;
			height:100%;
			margin:0 auto;
			overflow:hidden;
			position:relative;
		}
		.loading .loading_box img {
			vertical-align:middle;
			border:0;
			text-align:center;
			padding:250% 0;
		}
		.loading .loading_box .image {
			position:relative;
		}
		.loading .loading_box .txt {
		    position: absolute;
		    z-index: 9999;
		    top: 340px;
		    left: 20px;
		}
		.loading .loading_box h1 {
		    font-size: 11px;
		    color: #0b2d44;
		    letter-spacing: -0.5px;
		    font-weight: normal;
		}
		.loading .loading_box h2 {
			font-size:12px;
			color:#777777;
			letter-spacing:-1px;
			font-weight:normal;
		}
		</style> 
		
	</head>
	<body class="loading" style="overflow:hidden;border-width:0px;">    
		<div class="loading_box">
			<div class="txt"><h1>데이터 조회중..</h1></div><div class="image"><img src="../../resources/img/loading.gif" border="0" /></div>
		</div>
		<div id="wrap" style="height:100%;">	 
			<div class="sitemap_Area">
					<div class="btn_toggle"> 
					 <img id='btnToggle' src='../../resources/img/theme/07/left_hide_btn.gif'/>
					</div>  
					<div class="sitemap"> 
					<%
						String pathstring = "";
						String type = request.getParameter("type");
						String cogcid = request.getParameter("cogcid"); 
						if(type==null) type="";
						if(cogcid==null) cogcid = "";
						//if(type.equals("cog") && cogcid.equals("")) pathstring = "Home > "+ ((request.getAttribute("path")!=null&&request.getAttribute("path").toString().trim().equals("")==false)?request.getAttribute("path").toString().replace(">","&nbsp;>&nbsp;"):("") )+" > " +attrMenuNm;
						if(type.equals("cog") && cogcid.equals("")) pathstring = "Home > "+ request.getAttribute("path")+" > " +attrMenuNm;
						else pathstring = request.getAttribute("path")==null?"":request.getAttribute("path").toString(); 
					%>
					<%=pathstring %>
 
					</div>								  
					
						
					<div class="btn_Area" id="btn_Area"  >
						<img id = "helper" src="../../resources/img/theme/07/btn_help.png" title="도움말"/>
						<img src="../../resources/img/theme/07/btn_fav.png" onclick="goMyfolder();" title="즐겨찾기"/>
						<!-- <img src="${WWW.IMG}/theme/07/btn_sch.png" id="btnsch" title="보고서찾기"/> --> 
					</div>
			
					<div style="width:1000px; float:right;margin-top:2px;margin-right:5px;"> 
						<div style="float:right;">
						<pre class="help_txt03"  style="float:left;valign:middle;"><%=attrConfirmDept.replaceAll("\n", "<br>")%></pre>
						<pre class="help_txt03"  style="float:left;valign:middle;"><%=attrConfirmEmp.replaceAll("\n", "<br>")%></pre>     
						<pre class="help_txt03"  style="float:left;valign:middle;"><%=attrChargeDept.replaceAll("\n", "<br>")%></pre>
						<pre class="help_txt03"  style="float:left;valign:middle;"><%=attrChargeEmp.replaceAll("\n", "<br>")%></pre>       
						<pre class="help_txt03"  style="float:right;valign:middle;"title="<%=attrSummaryContent.replace("\"","\'\'")%>"><%=attrSummaryContent.replaceAll("\n", "<br>")%></pre>
						</div>
					</div>   
					
					
			</div> 
			
			<form method="get" name="frm" id='frm'>
				<input type="hidden" name="searchPath"/>
				<input type="hidden" name="paramCid" value="${param.cid}" />
				<input type="hidden" name="pid" value="" />
				<input type="hidden" name="pMenuNm" value="${param.title }" />
				<input type="hidden" name="pType" value="${param.pType }" />
				<input type="hidden" name="gubn" value="${param.gubn}" />
				<iframe name="ifrm" style="border-width:0px;marginwidth="0" marginheight="0" width="0" height="0" frameboarder="0"></iframe>
	
			</form> 
			<iframe  frameborder="0" id="iframe_cognos" style="overflow:auto;width:100%;height:calc(100% - 31px);" src="${url}<c:if test="${fn:indexOf(url, '?') != -1}">&</c:if><c:if test="${fn:indexOf(url, '?') == -1}">?</c:if>ippRcid=${realCid}" onload="resizeTabsWidth(); hidden_img();"></iframe> 
			
		</div>
	</body>
</html>