<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="crt" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<html>
	<head> 
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>Image</title>
		<script src="${WEB.ROOT}/resource/js/jquery/jquery-1.7.2.js"></script>
		<script>
		
		
			$(document).ready(function(){
				var height 	= $(window).height()-20;
				var width 	= $(window).width()-20; 
				
				$("#portletImage").css("height", height);
				$("#portletImage").css("width", width);
			});
		</script>
	</head>
	<body style="overflow:hidden;">
		<img src="${WEB.ROOT}/resource/img/portletImage/${poid}.gif" id="portletImage" />
	</body>
</html> 
