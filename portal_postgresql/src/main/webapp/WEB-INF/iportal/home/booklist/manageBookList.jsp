<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Language" content="ko">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
    <title id='Description'>즐겨찾기관리</title>
 <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/pop/css/pop_simple.css"/>
 <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
 <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/fresh/css/jqx.fresh.css"/>
 <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
 <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
 <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
 <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
 <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
 <script src="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/jqxdragdrop.js"></script>
<style>
	.jqx-panel{
		border:none;
	}
</style>
 <script type="text/javascript">//tree
 
 	var treeData=[];
        $(this).ready(function () {
             makeBookmark();
           //트리접기/펴기 동작 시 폴더아이콘변경 & <script>동작을 막기
             $('#bookNavi').on("expand",  {tree:$('#bookNavi')},function(event) {
     			var args = event.args;
     			var tree = event.data.tree;
     			var label = tree.jqxTree('getItem', args.element).label.replace('icon-folder', 'icon-folderopen');
     			var sPoint = label.indexOf("<span>")+6;
     			var ePoint = label.indexOf("</span>");
     			label = label.slice(0,sPoint)+$i.secure.scriptToText(label.slice(sPoint,ePoint))+label.slice(ePoint);
     			tree.jqxTree('updateItem', args.element, { label: label});
     		}).on("collapse", {tree:$('#bookNavi')}, function(eve) {
     			var args = eve.args;
     			var tree = eve.data.tree;
     			var label = tree.jqxTree('getItem', args.element).label.replace('icon-folderopen', 'icon-folder');
     			var sPoint = label.indexOf("<span>")+6;
     			var ePoint = label.indexOf("</span>");
     			label = label.slice(0,sPoint)+$i.secure.scriptToText(label.slice(sPoint,ePoint))+label.slice(ePoint);
     			tree.jqxTree('updateItem', args.element, { label: label});
     		}).on("select", {tree:$('#bookNavi')}, function(eve) {
     			var args = eve.args;
     			var tree = eve.data.tree;
     			var element =  tree.jqxTree('getItem', args.element);
     			var realCid="";
     			var cName="";
     			var cId=element.id;
     			for(var i=0;i<treeData.length;i++){
     				if(treeData[i].C_ID==cId){
     					cName=treeData[i].C_NAME;
     					realCid=treeData[i].REAL_C_ID;
     				}
     			}
     			$("#C_ID").val(cId);
     			$("#C_NAME").val(cName);
     			$("#REAL_C_ID").val(realCid);
     		}).on('dragEnd', function (dragItem, dropItem) //drop된 위치로 pid변경
     		{
     			if(dragItem.args.owner.selectedItem!=null){
     				var id = dragItem.args.owner.selectedItem.id;
         			var pid = dragItem.args.owner.selectedItem.parentId;
         			if(pid==null)	pid="5999";
         			var originPid = "5999";
         			for(var i=0;i<treeData.length;i++){
         				if(treeData[i].C_ID==id){
         					originPid = treeData[i].P_ID;
         					break;
         				}
         			}
         			if(pid==originPid){//부모노드 동일
         				console.log('sort변경');
         			}else{
         				$i.post("../../home/layout/moveMyfolderNoCog", {cId : id, pId : pid}).done(function(res){
            				if(res.returnCode == "EXCEPTION"){
            					$i.dialog.error("SYSTEM", res.returnMessage);
            				}else{
            					parent.makeBookmark();
            					parent.bookReset();
            					makeBookmark();
            					bookReset();
            				}
            			});	
         			}
     			}
     		}).on('dragStart', {tree:$('#bookNavi')},function(eve) {
     			var args = eve.args;
     			var element = args.owner._dragItem.element;
     			var width = element.firstElementChild.offsetWidth; //div의 width
     			$(".jqx-draggable-dragging").css("width",width+10);
     			
     		});
        });
 	function addFolder(){ //새폴더
 		$i.dialog.confirm("즐겨찾기 새폴더","<table style='margin-top:5px;float:right;'><tr><th style='color:black;width:100px;' align='center'>폴더명</th><td><input type='text' name='folderName' style='width:250px;height:20px;padding-top:3px;'/></td></tr></table>",function(data){
			var cname = $("[name='folderName']").val();
			$i.post("../../home/layout/insertMyfolderNoCog", {cName : cname}).done(function(res){
				if(res.returnCode == "EXCEPTION"){
					$i.dialog.error("SYSTEM", res.returnMessage);
				}else{
					$i.dialog.alert("SYSTEM", res.returnMessage, function(){
						parent.makeBookmark();
						parent.bookReset();
						makeBookmark();
						bookReset();
					});
				}
			});
		});
 	}
 	function renameFolder(){ //이름바꾸기
		var realCid = $("#REAL_C_ID").val();
		var cId = $("#C_ID").val();
		var cName = $("#C_NAME").val();
		if(cId==""){
			$i.dialog.alert('SYSTEM','변경할 대상을 선택하세요.');
			return;
		}
	/*	if(realCid!=null&&realCid!="0"){//page
			$i.dialog.alert('SYSTEM','페이지는 이름을 변경할 수 없습니다.');
			return;
		}*/
		$i.dialog.confirm("즐겨찾기 이름변경","<table style='margin-top:5px;float:right;'><tr><th style='color:black;width:100px;' align='center'>폴더명</th><td><input type='text' name='folderName' value='"+cName+"'style='width:250px;height:20px;padding-top:3px;'/></td></tr></table>",function(data){
			var cname = $("[name='folderName']").val();
			$i.post("../../home/layout/renameMyfolderNoCog", {cId : cId, cName : cname}).done(function(res){
				if(res.returnCode == "EXCEPTION"){
					$i.dialog.error("SYSTEM", res.returnMessage);
				}else{
					$i.dialog.alert("SYSTEM", res.returnMessage, function(){
						parent.makeBookmark();
						parent.bookReset();
						makeBookmark();
						bookReset();
					});
				}
			});
		});	
	}
	function remove(){ //삭제
		//tree에서 먼저 제거
		var realCid = $("#REAL_C_ID").val();
		var cId = $("#C_ID").val();
		var cName = $("#C_NAME").val();
    	var selectedItem = $('#bookNavi').jqxTree('selectedItem');
	    if (cId!="") {
			var isfolder = false;
			if(realCid!=null&&realCid!="0"){
				$i.dialog.confirm("즐겨찾기 삭제", "즐겨찾기가 삭제됩니다.",function(){
					$i.post("../../home/layout/deleteBookUserData", {cID:cId}).done(function(data){
						if(data.returnCode == "EXCEPTION"){
							$i.dialog.error("SYSTEM", data.returnMessage);
						}else{
							$i.dialog.alert("SYSTEM", data.returnMessage, function(){
								parent.makeBookmark();
	        					parent.bookReset();
	        					$('#bookNavi').jqxTree('removeItem', selectedItem.element, false);
	        					$('#bookNavi').jqxTree('render');
	        					makeBookmark();
	        					bookReset();
							});
						}
					});
				});
			}else{
				$i.dialog.confirm("즐겨찾기", "폴더 안의 즐겨찾기도 함께 삭제됩니다",function(){
					$i.post("../../home/layout/deleteBookUserData", {cID:cId}).done(function(data){
						if(data.returnCode == "EXCEPTION"){
							$i.dialog.error("SYSTEM", data.returnMessage);
						}else{
							$i.dialog.alert("SYSTEM", data.returnMessage, function(){
								parent.makeBookmark();
	        					parent.bookReset();
	        					
	        					$('#bookNavi').jqxTree('removeItem', selectedItem.element, false);
	        					$('#bookNavi').jqxTree('render');
	        					makeBookmark();
	        					bookReset();
							});
						}
					});
				});
			} 
	    }else{
	    	$i.dialog.alert('SYSTEM','삭제할 대상을 선택하세요.');
			return;
	    }
	}
	function closePopup(){ //닫기
		if(parent.jqxpopupclose!=null)
			parent.jqxpopupclose("booklist");
		else window.close();
	}
	
 	function makeBookmark(){
		//bookReset();
		$i.post("../../home/layout/getBookList", {}).done(function(data){
			treeData=data.returnArray;
			makeLeftFavoriteMenu(treeData);
	        $('#bookNavi').css('visibility', 'visible');
		}); 
	}
	function bookReset(){
		$("#C_ID").val('');
		$("#C_NAME").val('');
		$("#REAL_C_NAME").val('');
	}
	//LEFT MENU GENERATE
	var favoriteMenuList = [];
	function makeLeftFavoriteMenu(resData) {
		for(var i=0;i<resData.length;i++){
			if(resData[i].REAL_C_ID=="0"){
				label = "<img style='float: left; margin-right: 5px;' src='../../resources/cmresource/image/icon-folder.png'/><span>" +  $i.secure.scriptToText(resData[i].C_NAME)+ "</span>";
				resData[i].LABEL=label;
			}else{
				label = "<img style='float: left; margin-right: 5px;' src='../../resources/cmresource/image/icon-page.png'/><span>" +  $i.secure.scriptToText(resData[i].C_NAME) + "</span>";
				resData[i].LABEL=label;
			}
		} //label setting
		   // prepare the data
        var source =
        {
            datatype: "json",
            datafields: [
                { name: 'C_ID' },
                { name: 'C_LINK' },
                { name: 'C_NAME' },
                { name: 'MENU_TYPE' },
                { name: 'P_ID' },
                { name: 'REAL_C_ID' },
                { name: 'SORT_ORDER' },
                { name: 'SOURCE_OWNER' },
                { name: 'USER_ID' },
                { name: 'LABEL'}
            ],
            id: 'C_ID',
            localdata: resData
        };
        // create data adapter.
        var dataAdapter = new $.jqx.dataAdapter(source);
        dataAdapter.dataBind();
        var records = dataAdapter.getRecordsHierarchy('C_ID', 'P_ID', 'items', [{ name: 'LABEL', map: 'label'},{ name: 'C_ID', map: 'id'},{ name: 'C_NAME', map: 'value'}]);
        $('#bookNavi').jqxTree({ 
        	source: records
        	, width: '100%'
        	,height: 420
        	,theme:'fresh'
        	,allowDrag: true
        	,allowDrop: true
        	,dragEnd: function (dragItem, dropItem, args, dropPosition, tree) {
        		var dropId = dropItem.id;
    			var droplabel = dropItem.label;
        		var dragId = dragItem.id;
        		var draglabel = dragItem.label;
        		var isPage = false;
        		if(dropPosition=='inside'){
        			var realcid = '';
        			for(var i = 0; i<treeData.length;i++){
        				if(dropId==treeData[i].C_ID){
        					realcid = treeData[i].REAL_C_ID;
        					console.log(realcid);
        					break;
        				}
        			}
        			
        			//if(droplabel.indexOf("icon-page")>0){//페이지
        			if(realcid!=0 && realcid!=null){
        				$i.dialog.error('SYSTEM','페이지의 하위단계로 이동할 수 없습니다.');
            			return false;
        			}else{
        				var sort = 0;
            			for(var i = 0; i<treeData.length;i++){
            				if(dropId==treeData[i].C_ID){
            					sort = treeData[i].SORT_ORDER;
            					break;
            				}
            			}
            			$i.post("../../home/layout/sortMyfolderNoCog", {cId : dragId, targetId : dropId, position:dropPosition, sortOrder:sort}).done(function(res){
            				if(res.returnCode == "EXCEPTION"){
            					$i.dialog.error("SYSTEM", res.returnMessage);
            				}else{
            					parent.makeBookmark();
            					parent.bookReset();
            					makeBookmark();
            					bookReset();
            				}
            			});
        			}    
        		}else{//sort
        			if(dropPosition!=undefined){
        				var sort = 0;
            			for(var i = 0; i<treeData.length;i++){
            				if(dropId==treeData[i].C_ID){
            					sort = treeData[i].SORT_ORDER;
            					break;
            				}
            			}
            			
            			$i.post("../../home/layout/sortMyfolderNoCog", {cId : dragId, targetId : dropId, position:dropPosition, sortOrder:sort}).done(function(res){
            				if(res.returnCode == "EXCEPTION"){
            					$i.dialog.error("SYSTEM", res.returnMessage);
            				}else{
            					parent.makeBookmark();
            					parent.bookReset();
            					makeBookmark();
            					bookReset();
            				}
            			});	
        			}
        		}
            }	
        });
        if(resData!=null &&resData.length>0){
	        $("#bookNavi li div").jqxDragDrop({initFeedback: function (feedback) { //drag할때 script동작막기
	        	var label = feedback.html();
	 			var sPoint = label.indexOf("<span>")+6;
	 			var ePoint = label.indexOf("</span>");
	 			label = label.slice(0,sPoint)+$i.secure.scriptToText(label.slice(sPoint,ePoint))+label.slice(ePoint);
			    feedback.html(label);        
			    feedback.css("width","70px");
			    feedback.css("color","#fff");
			    feedback.css("text-shadow","none");
			    feedback.css("border-color","#5a89af !important");
			    feedback.css("background-color","#5a89af");
			    feedback.css(" -moz-background-clip","padding");
			    feedback.css("background-clip","padding-box");
			    feedback.css("-webkit-background-clip","padding-box");
			    feedback.css("font-family","'맑은 고딕', arial");
			}});
	        $("#bookNavi").jqxTree('expandAll');
        }
        
		
	}  
 </script>
 </head>
 <body class='pop_simple'>
<!--팝업-->
<div class="pop" style="width:570px; min-width:570px; height:544px; margin:0 0%;border:none;">
	<!-- 
	<div class="top f_left" style="width:100%; margin:0px 0">
		<div class="label type1 f_left">
			<h1>즐겨찾기 관리 <span class="close"><img src="../../resources/cmresource/css/iplanbiz/theme/pop/img/btn_cross.png" alt="닫기"  title="닫기" style="cursor:pointer;"></span></h1>
		</div>
	</div>-->
	<!--//top-->
	<div class="container" style=" float:left; width:100%; margin:15px 0;">
		<div class="content" style=" width:93%; margin:0 3%;">
		<input type="hidden" id="C_ID"/>
		<input type="hidden" id="C_NAME"/>
		<input type="hidden" id="REAL_C_ID"/>
			<div  class="tree" style=""  id='bookNavi'>
			</div>
			<!--//jqxTree-->
			<div class="group_button" style=" width:77%; margin:10px auto;margin-left:110px;">
				<div class="button type1" style="float:left; margin-right:10px;">
					<button type="button" value="새폴더(N)" id='jqxButtonNewfolder' onclick="addFolder();">새폴더(N)</button>
				</div>
				<div class="button type1" style="float:left; margin-right:10px;">
					<button type="button" value="이름바꾸기(R)" id='jqxButtonChangename' onclick="renameFolder();">이름바꾸기(R)</button>
				</div>
				<div class="button type1" style="float:left;">
					<button type="button" value="삭제(D)" id='jqxButtonDelete' onclick="remove();" >삭제(D)</button>
				</div>
			</div>
			<!--group_button-->
		</div>
		<!--//content-->
	</div>
	<!--//container-->
	<div class="bottom" style=" float:left; width:100%; position:absolute; bottom:0;">
		<div class="label type1" style="float:left; ">
			<p>이동은 선택 후 마우스 Drag&amp;Drop</p>
		</div>
		<div class="group_button" style=" float:right; margin:10px 0;">
			<div class="button type2" style="float:left; margin-right:10px;">
				<button type="button" value="닫기" id='jqxButtonClose' onclick="closePopup();">닫기</button>
			</div>
		</div>
		<!--group_button-->
	</div>
	<!--//bottom-->
</div>
<!--//pop-->

</body>
</html>