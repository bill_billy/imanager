<%@page import="com.iplanbiz.core.session.LoginSessionInfo"%>
<%-- <%@page import="org.apache.commons.lang3.StringUtils"%> --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="java.util.Date"%>
<%@ page import="com.iplanbiz.iportal.comm.message.LocaleUtil"%>
<%-- <%@ page import="com.iplanbiz.iportal.comm.util.MenuUtil"%> --%>
<%-- <%@ page import="com.iplanbiz.iportal.comm.util.CognosInfo"%> --%>
<%@ page import="com.iplanbiz.iportal.comm.util.CognosInfo"%>
<%@ page import="com.iplanbiz.iportal.config.WebConfig"%>
<%@ page import="com.iplanbiz.iportal.comm.cognos.MenuUtil"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head> 
 
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>   
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>${systemConfig.system_name}</title> 

<link rel="shortcut icon" href="../../resources/css/login/favicon.ico" />
<link rel="stylesheet" type="text/css" href="../../resources/css/theme/07/default.css" />
<link rel="stylesheet" type="text/css" href="../../resources/css/theme/07/font-awesome.css" />

<link rel="stylesheet" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css" type="text/css" />      
<link rel="stylesheet" href="../../resources/css/iplanbiz/styles/jqx.ui-redmond_lotte.css" type="text/css" />
<link rel="stylesheet" href="../../resources/css/iplanbiz/styles/jqx.Lotte-depart.css" type="text/css" />
 
<link rel="stylesheet" type="text/css" href="../../resources/css/theme/07/iplanbiz.control.button.css" />
<link rel="stylesheet" type="text/css" href="../../resources/css/theme/07/iplanbiz.control.popup.css" />

<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>   
<script src="../../resources/cmresource/js/iplanbiz/secure/cipher.js"></script>  
<script src="../../resources/js/theme/07/mainFrame.js"></script> 
<link rel="stylesheet" data-role='custom_theme' data-idx='${sessionScope.loginSessionInfo.acctID}' type="text/css" href="../../resources/css/theme/07/${sessionScope.loginSessionInfo.acctID}.css" />
<link rel="stylesheet" data-role='custom_theme' data-idx='${sessionScope.loginSessionInfo.acctID}' type="text/css" href="../../resources/css/theme/07/font-awesome.${sessionScope.loginSessionInfo.acctID}.css" />

<%-- <script src="${WWW.JS}/cognoslaunch.js"></script> --%>
<!-- <script src="${WWW.JS}/jquery/jquery.corner.js"></script>--> 
<script> 
<%
String storeID = "";
String url = "";    
String cognosConnection = (CognosInfo.getConnect()==null)?"":CognosInfo.getConnect().trim(); 
     
if(CognosInfo.getConnect()!=null&&CognosInfo.getConnect().trim().equals("")==false){  
	try{  
		url = CognosInfo.getConnect().trim()+"?b_action=xts.run&m=portal/cc.xts&m_tab=";
		storeID = MenuUtil.getMyFolderStoreID((LoginSessionInfo)(session.getAttribute("loginSessionInfo")));
		url+=storeID;
	}catch(Exception e){
		e.printStackTrace();  
	}
}
LoginSessionInfo loginSessionInfo = (LoginSessionInfo)request.getSession().getAttribute("loginSessionInfo"); 
boolean checkPwdUpdate = (Boolean)loginSessionInfo.getExternalInfo("checkPwdUpdate");
//메인화면 정형보고서... 대시보드일경우 아래 mainurl, postfix를 바꿔줍니다.
// String mainurl = cognosConnection.equals("")?"":cognosConnection+CognosInfo.getCognosViewer();
// String postfix = cognosConnection.equals("")?"":CognosInfo.getCognosViewerLast();

// String biaFolderName = cognosConnection.equals("")?"":CognosInfo.getBIAMenuName();
String popupTopMenuNames = WebConfig.getProperty("system.option.popupTopMenuNames");

String mainurl = ""; 
String postfix = "";

String biaFolderName = "";
%>
var maxTabCnt = "${systemConfig.tab_count}";
var biafolder = "<%=popupTopMenuNames%>";  
var mainurl = "${portletLayout}";   
var cogConnection = "<%=cognosConnection%>";
var cogMyfolder = "<%=storeID%>";    
var rootContext = "../../"; 
var solutionDomain = "${systemConfig.root_domain}";
//var isadmin = "true";    
var coglogincnt = 0;
function cogloginif(){
	coglogincnt ++;
	if(coglogincnt>1) return;
	
	document.getElementById("coglogin").src="<%=CognosInfo.getConnect()%>?m_passportID=${sessionScope.loginSessionInfo.passport}";
}
function popupClose(boardNo){
	if(parent!=null) {
		if($('#cookie_checkbox').is(":checked")){
			if(parent.jqxpopupclose!=null){
				parent.setCookie('notice_popup_'+boardNo,'n',9999,'/');
			}
		}
		if(parent.jqxpopupclose!=null)
			parent.jqxpopupclose("notice_popup_"+boardNo);
		else window.close();
	}  
	else window.close(); 
}

</script>
<style>
html, body{
	height:100%;
}
.jqx-tree-grid-indent{
	width:5px;
}
.jqx-grid-cell-ui-redmond:hover{
	font-weight:normal !important;
}
@font-face {
  font-family: 'FontAwesome';
  src: url('../../resources/font/fontawesome-webfont.eot?v=4.5.0');
  src: url('../../resources/font/fontawesome-webfont.eot?#iefix&v=4.5.0') format('embedded-opentype'), url('../../resources/font/fontawesome-webfont.woff2?v=4.5.0') format('woff2'), url('../../resources/font/fontawesome-webfont.woff?v=4.5.0') format('woff'), url('../../resources/font/fontawesome-webfont.ttf?v=4.5.0') format('truetype'), url('../../resources/font/fontawesome-webfont.svg?v=4.5.0#fontawesomeregular') format('svg');
  font-weight: normal;
  font-style: normal;
}
/* 케이토토 공지사항 관련 */
.popup_box {margin:0 auto; margin-top:0px; padding:0px; border:4px solid #003664; background:#fff; margin-left:0px;} 
/* .popup_box_contents {margin:0; width:100%; padding:5px;position:relative;overflow:auto;} */
/* .popup_box_contents {margin:0; width:100%; } */
.popup_box_contents {margin:0; width:100%; padding:5px;position:relative;overflow:auto;}
.popup_box_bottom {color:#000; background:#ecedf4; font-size:13px; letter-spacing:-1px; margin:0; text-align:right; 
					border-top:1px solid #d5d7e0; padding:5px 15px 8px 0; vertical-align: middle;
					position:absolute;bottom:0;width:98%;}
.jqx-window-header-Lotte-depart{
	background: #003664 !important;
    height: 35px !important;
    margin: 0 !important;
}
.jqx-window-header-Lotte-depart > div:first-child{
width: 85%;
    line-height: 30px; 
    float: left;
    padding-left: 27px;
    background: url(../../resources/img/ktoto/title_ico.png) 10px 10px no-repeat;
    font-size: 15px;
    color: #FFF;
    letter-spacing: -1px;
    font-weight: bold;
}
.jqx-window-close-button-Lotte-depart{
	background-image: url(../../resources/img/ktoto/btn_close.png);
	cursor:pointer;
	margin-right:3px;
	margin-top:3px;
}
.jqx-window-close-button-background-Lotte-depart{
	margin-right:15px !important;
	margin-top:5px;
}
#bookNavi .jqx-tree-item-li:hover{
	background:none !important;
}
#bookNavi li a:hover{
	background:#719bc7; 
}

#bookNavi {
	overflow:hidden;
}
#bookNavi > ul{
	width:100%;
	overflow:hidden;
}
#bookNavi .jqx-tree-item-li{
	max-width:210px;
	overflow:hidden;
}
#bookNavi .jqx-tree-item-li > div{
	max-width:150px;
	overflow:hidden;
}


</style>
</head>

<body> 

<!-- UI Object -->
<div id="wrap" style = "height:100%;">	
	<!-- header -->
	<div id="header">
    	<div class="logo"><h1> 
    	<img src="../../resources/img/n/logo_sch.gif" alt="로고" /> 
    	</h1></div>   
    	<div class="navi_gnb">  
            <div class="navi">
                <div class="navi_cont" id = "topMenu"></div>
            </div> 
            <div class="btn_area">
                <ul>
                    <li class="user"><span class="name">${sessionScope.loginSessionInfo.userName}</span> 님</li>  
                    
                    <c:if test="${cognosConnection ne null||cognosConnection ne ''}">
                    <!--   
                    	<li class="ico"><i class="fa fa-folder-open" title="그룹폴더" onclick="window.open('<%=url%>','그룹폴더');"></i></li>
 						<li class="ico"><i class="fa fa-cube" title="비정형분석" onclick="cogpopup('_blank','<%=cognosConnection%>','BUA_standalone');"></i></li>
 					-->
 					</c:if>  
                    <li class="ico"><i class="fa fa-remove" title="탭 전체닫기" id ="allclosebutton"></i></li>
                    <c:set var="checkPwdUpdate" value="<%=checkPwdUpdate %>"/>
                    <c:if test="${checkPwdUpdate==true}">
                    	<li class="ico"><i class="fa fa-key" title="비밀번호 변경" id ="changepwd"></i></li>
                    </c:if>
					<li class="ico"><i class="fa fa-power-off" title="로그아웃" data-path="<%=WebConfig.getLogoutPath()%>" id = "btnLogOut"></i></li>
                </ul>
            </div>
        </div>
	</div>
	<!-- //header -->
	<!-- container -->
	<div id="container" style="height:calc(100% - 50px);">
		<!-- snb -->
		<div class="snb" id="leftTabs">     <!-- style="display:none;" 원복..--> 
        <!-- search -->
            <div class="snb_Tab_area">
                <div class="snb_Tab_contents ui-top-corner" id="leftMenuTitle" data-role="tabheader" data-idx="0"></div>
				<div class="snb_Fav_contents ui-top-corner"  data-role="tabheader" data-idx="1"><img title="즐겨찾기" src="../../resources/img/theme/07/bookmarks.png"/></div>
            </div>
            <div data-role="tabcontents">
            <!--//search -->
                <div class="snb_navi"  id="jqxTree" style="top:-1px;overflow-x:hidden;overflow-y:auto;width:100%;"> 
					 
                </div>
            </div>
            <div data-role="tabcontents" style="display:none;">
            	<div class="folderNew" style="margin:6px;float:right;"><button class='btnlotte' onclick="addFolder();">폴더관리</button></div>
            	<div id="bookNavi" class="snb_navi">
            	
            	</div>
            </div>
                 
        </div> 
		<!-- //snb -->
		<!-- content -->  
		<div id="content" style="margin-left:211px;height:100%;">
			<ul  class="Tab_Area" id='tabHeader' >
				<li class="first"><a href="#content-1" id='mainTabTitle'>메인</a></li>
			</ul>
			 
			<div id="content-1"> 
				<div class="sitemap_Area">
					<div class="btn_toggle">
						<img id='btnToggle' src='../../resources/img/theme/07/left_hide_btn.gif'/> 
					</div>
					<div class="sitemap">Home</div>
					<div class="btn_Area">   
						<!-- <img src="${WWW.IMG}/theme/07/btn_sch.png" id="btnsch" title="보고서찾기"/> --> 
					</div>
				</div> 
				<!-- mainTabContents width 2000px do not change because of cognos scroll bug -->
				<iframe id='mainTabContents' name='mainTabContents' frameborder = "0" style ="width:100%;height:1024px;border-width:0px;border-style:none;overflow:hidden !important;"></iframe>
        	</div>
        </div>
		<!-- //content -->
	</div> 
	<!-- //container -->
	  
	 
	 
		
</div>
<%
if(CognosInfo.getConnect()!=null&&CognosInfo.getConnect().trim().equals("")==false){
	%>
	<iframe id="coglogin" src="<%=CognosInfo.getConnect()%>?b_action=xts.run&m=portal/logoff.xts&h_CAM_action=logoff"  scrolling="no" frameborder="0" marginwidth="0" marginheight="0" style="display:none"  width="500" height="500" onload="cogloginif()" />
	<%	
}
%>
<!-- //UI Object -->
<iframe name='ifav_' style='display:none;'></iframe>
</body>
</html>
