﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko">
<head>
	<meta content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
	<meta name="msapplication-tap-highlight" content="no" />

	<title>${systemConfig.system_name}</title>
	<link rel="stylesheet" href="${WWW.CSS}/jqwidget3.9.1/styles/jquery.mobile-1.4.3.min.css" />
	<link rel="stylesheet" href="${WWW.CSS}/jqwidget3.9.1/styles/jqx.base.css" type="text/css" />
	<link rel="stylesheet" href="${WWW.CSS}/login/mobile/common_mobile.css" type="text/css" />
	<link rel="stylesheet" href="${WWW.CSS}/login/mobile/jqx.i_mobile.css" type="text/css" />
	 
	<link rel="stylesheet" href="${WWW.CSS}/login/mobile/styles.css" />
	
	<script type="text/javascript" src="${WEB.JS}/jquery/simulator.js"></script>
	<script type="text/javascript" src="${WEB.JS}/jquery/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="${WEB.JS}/jqwidget3.9.1/jqwidgets/jqxcore.js"></script>
	<script type="text/javascript" src="${WEB.JS}/jqwidget3.9.1/jqwidgets/jqxdata.js"></script>
	<script type="text/javascript" src="${WEB.JS}/jqwidget3.9.1/jqwidgets/jqxbuttons.js"></script>
	<script type="text/javascript" src="${WEB.JS}/jqwidget3.9.1/jqwidgets/jqxscrollbar.js"></script>
	<script type="text/javascript" src="${WEB.JS}/jqwidget3.9.1/jqwidgets/jqxdatetimeinput.js"></script>
	<script type="text/javascript" src="${WEB.JS}/jqwidget3.9.1/jqwidgets/jqxcalendar.js"></script>
	<script type="text/javascript" src="${WEB.JS}/jqwidget3.9.1/jqwidgets/jqxtooltip.js"></script>
	<script type="text/javascript" src="${WEB.JS}/jqwidget3.9.1/jqwidgets/globalization/globalize.js"></script>
	<script type="text/javascript" src="${WEB.JS}/jqwidget3.9.1/jqwidgets/jqxchart.js"></script>
	<link rel="stylesheet" href="${WWW.CSS}/iplanbiz/util/pivottable/pivot.css" type="text/css" />
	<link rel="stylesheet" href="${WWW.CSS}/jquery.ui.all.css" type="text/css" />
	<script src="${WWW.JS}/iplanbiz/core/iplanbiz.web.include.jsp" charset="UTF-8"></script>
	<script type="text/javascript" src="${WEB.JS}/jqwidget3.9.1/jqwidgets/jqxpopover.js"></script> 
	<!-- table widget  의 align를 강제로 left로 설정하는 구문이 wpt에 포함되어있음.. 현재 사용하지 않는 css파일이어서 주석처리함. -->
	<link href="${WWW.JS}/webpivottable2.5.0/brightsea/wpt/wpt.css" rel="stylesheet" />
	<script type="text/javascript"  src="${WEB.JS}/jquery/jquery.mobile-1.4.3.min.js"></script>
	<script>
		var uriContext = "${WEB.ROOT}";
		var solutionDomain = "${systemConfig.root_domain}";
		function goLeftMenu(cid, cName){
			location.href="${WEB.ROOT}" + "/mobile/LeftMenu?pCid="+cid+"&cName="+cName;
		}
		function backMenu(){
			if($("#topId").val() == "5999"){
				$( "#left-panel" ).panel( "close" );
				$("#menu_List")[0].style.display="none";
				$("#left-panel")[0].style.display="block";
				$("#mainTopLayout")[0].style.display="block";
				$("#mainLayout")[0].style.display="block";
			}else{
				makeLeftMenu($("#topId").val(),$("#pName").val());
			}
		}
// 		function goLeftMenu(cId, cName){
// 			$("#left-panel" ).panel("close");
// 			$("#left-panel")[0].style.display="none";
// 			if(cId == ""){
// 				cId = "${param.pCid}";
// 				cName = "${param.cName}";
// 			}
// 			if(window.sessionStorage.getItem(cId) == null || window.sessionStorage.getItem(cId) == "[]"){
// 				$iv.mybatis("getMobileTopMenuList",{USER_ID:$iv.userinfo.userid(),ACCT_ID:$iv.userinfo.acctid(),DIVISION:$iv.envinfo.division,LOCALE:"ko",P_ID:cId},{callback:function(res){
// 					var menuList = [];
// 					for(var i=0;i<res.length;i++){
// 						menuList.push({"C_ID":res[i].C_ID,"C_NAME":res[i].C_NAME,"P_ID":res[i].P_ID,"P_NAME":res[i].P_NAME,"TOP_ID":res[i].TOP_ID, "MENU_TYPE":res[i].MENU_TYPE,"C_LINK":res[i].C_LINK});
// 					}
// 					window.sessionStorage.setItem(res[0].P_ID,JSON.stringify(menuList));
// 					makeLeftMenu(cId, cName);
// 				}});
// 			}else{
// 				makeLeftMenu(cId, cName);
// 			}
// 		}
		function makeLeftMenu(cId, cName){
			$("#menu_List")[0].style.display="block";
			$("#mainTopLayout")[0].style.display="none";
			$("#mainLayout")[0].style.display="none";
// 			var list = "";
// 			for(var i=0;i<res.length;i++){
// 				list += "<li><a href=\"javascript:goLeftMenu("+res[i].C_ID+",'"+res[i].C_NAME+"');\" class='navbarMenu'>"+res[i].C_NAME+"</a></li>";
// 			}
// 			$("#leftMenuList").append(list);
            $("#MenuLeftList").empty();
			var res = JSON.parse(window.sessionStorage.getItem(cId));
			var list = "";
			var pId = res[0].P_ID;
			var pName = res[0].P_NAME;
			var topId = res[0].TOP_ID;
			for(var i=0;i<res.length;i++){
				if(res[i].MENU_TYPE == "2"){
					list += "<li class='list'><a href=\"javascript:detailList('"+res[i].C_LINK+"','"+res[i].C_NAME+"');\" class='menu_title'><span class='ic-drive-file icon'></span>"+res[i].C_NAME+"</a></li>";
				}else{
					list += "<li class='list'><a href=\"javascript:goLeftMenu("+res[i].C_ID+",'"+res[i].C_NAME+"');\" class='menu_title'><span class='ic-folder icon'></span>"+res[i].C_NAME+"</a></li>";	
				}
			}
			$("#pId").val(pId);
			$("#pName").val(pName);
			$("#topId").val(topId);
			$("#MenuLeftList").append(list);
			$("#showCname").html(pName);     	
			hidden_img();
		}
		function logout(){
			sessionStorage.clear();
			location.replace("${WEB.ROOT}" + "/mobileLogout");
		}
		function hidden_img(){
	    	$(".loader").hide();
		}
		function dataMenu(cId, cName){
			if(cId == ""){
				cId = "${param.pCid}";
				cName = "${param.cName}";
			}
			if(window.sessionStorage.getItem(cId) == null || window.sessionStorage.getItem(cId) == "[]"){
				$iv.mybatis("getMobileTopMenuList",{USER_ID:$iv.userinfo.userid(),ACCT_ID:$iv.userinfo.acctid(),DIVISION:$iv.envinfo.division,LOCALE:"ko",P_ID:cId},{callback:function(res){
					var menuList = [];
					for(var i=0;i<res.length;i++){
						menuList.push({"C_ID":res[i].C_ID,"C_NAME":res[i].C_NAME,"P_ID":res[i].P_ID,"P_NAME":res[i].P_NAME,"TOP_ID":res[i].TOP_ID, "MENU_TYPE":res[i].MENU_TYPE,"C_LINK":res[i].C_LINK});
					}
					window.sessionStorage.setItem(res[0].P_ID,JSON.stringify(menuList));
					makeMenuList(cId, cName);
				}});
			}else{
				makeMenuList(cId, cName);
			}
		}
		function makeMenuList(cId, cName){
			$("#MenuLeftList").empty();
			var res = JSON.parse(window.sessionStorage.getItem(cId));
			var list = "";
			var pId = res[0].P_ID;
			var pName = res[0].P_NAME;
			var topId = res[0].TOP_ID;
			for(var i=0;i<res.length;i++){
				if(res[i].MENU_TYPE == "2"){
					list += "<li class='list'><a href=\"javascript:detailList('"+res[i].C_LINK+"','"+res[i].C_NAME+"');\" class='menu_title'><span class='ic-drive-file icon'></span>"+res[i].C_NAME+"</a></li>";
				}else{
					list += "<li class='list'><a href=\"javascript:goLeftMenu("+res[i].C_ID+",'"+res[i].C_NAME+"');\" class='menu_title'><span class='ic-folder icon'></span>"+res[i].C_NAME+"</a></li>";	
				}
			}
			$("#pId").val(pId);
			$("#pName").val(pName);
			$("#topId").val(topId);
			$("#MenuLeftList").append(list);
			$("#showCname").html(pName);
		}
		function leftMenuListMake(){
// 			window.localStorage.getItem("5999")
//          window.localStorage.setItem("11181",JSON.stringify([{"A":"a"},{"A":"b"}]))
			var res = JSON.parse(window.sessionStorage.getItem("5999"));
			var list = "";
			for(var i=0;i<res.length;i++){
				list += "<li><a href=\"javascript:goLeftMenu("+res[i].C_ID+",'"+res[i].C_NAME+"');\" class='navbarMenu'>"+res[i].C_NAME+"</a></li>";
			}
			$("#leftMenuList").append(list);
			hidden_img();
			
		}
		function goPage(gubn){
			if(gubn == "notice"){
 				location.href="${WEB.ROOT}" + "/notice/list_mobile";
// 				location.href="app://notice";
			}
		}
		function makePorver(id){
			var theme = prepareSimulator("popover");
			var data = new Array();
			var share = ["공유"];
	        var zoom = ["확대보기"];
			var remove = ["삭제하기"];
			var choice = $("#choice"+id);
			var row = {};
			row["share" , "zoom", "remove"]
			var table = '<table style="min-width: 120px;"><tr><td><span class="ic-share-alt"><a href="#" class="ai-txt">' + share + '</a></span></td></tr><tr><td><span class="ic-search"><a href="detail1_4.html" class="ai-txt">' + zoom + '</a></span></td></tr><tr><td><span class="ic-delete"><a href="#" class="ai-txt">' + remove + '</a></span></td></tr></table>';
			choice[0].innerHTML += table;
	        $("#popbutton"+id).jqxButton({ theme: "i_mobile",  template: "inverse" });
	        $("#popover"+id).jqxPopover({theme: "i_mobile", offset: {left: -55, top:0}, arrowOffsetValue: -55, title: "", showCloseButton: false, selector: $("#popbutton"+id) });	
			$(".jqx-window-close-button-i_mobile").addClass('ai-cancel');
	        if (theme) {
	            $(".jqx-window-close-button-i_mobile ").addClass('ai-cancel-' + theme);
	        }
	        initSimulator("#popover"+id);
		}
		function makeMain(){
			var html = "";
			var makeList = [];
			$iv.mybatis("mobileMain", {}, {callback:function(data){
				for(var i=0;i<data.length;i++){
					if(i == 0 || data[i].EXPLAIN != data[i-1].EXPLAIN){
						if(i!=0){
							html += '</div>';
							html += '</a>';
							html += '</div>';	
						}
						if(i != 0){
							$("#mainPutData").append(html);
							for(var j=0;j<makeList.length;j++){
								if(makeList[j].VISIBLE == "N"){
									$(document).data("builderinfo",{filepath:makeList[j].TARGET});
									$("#"+makeList[j].TARGET).data("builderinfo",JSON.parse(makeList[j].INFO));
									$iv.uibuilder.build("#"+makeList[j].TARGET);
									$("#"+makeList[j].TARGET).hide();
								}else{
									$(document).data("builderinfo",{filepath:makeList[j].TARGET});
									$("#"+makeList[j].TARGET).data("builderinfo",JSON.parse(makeList[j].INFO));
									$iv.uibuilder.build("#"+makeList[j].TARGET);
									$("#"+makeList[j].TARGET)[0].style.top = "";
									$("#"+makeList[j].TARGET)[0].style.right = "";
									$("#"+makeList[j].TARGET)[0].style.left = "";
									$("#"+makeList[j].TARGET)[0].style.bottom = "";
									$("#"+makeList[j].TARGET)[0].style.width = "100%";
									$("#"+makeList[j].TARGET)[0].style.height = "100px";
									$("#"+makeList[j].TARGET)[0].style.position = "";
									makePorver(makeList[j].TARGET);
								}
							}
							html = "";	
							makeList = [];
						}
						html += '<div class="con_Box">';
						html += '<div class="listbox_btn" >';
						html += '<div id="popover'+data[data[i].CNT-1].PATH+'"><div id="choice'+data[data[i].CNT-1].PATH+'"></div></div>';
						html += '<div><button id="popbutton'+data[data[i].CNT-1].PATH+'"><span class="ic-more-vert" style="padding:0;"></span></button></div>';
						html += '</div>';
						html += '<a href="detail1_3.html">';
						html += '<div data-role="header" style="border:0;border-bottom:1px solid #dddddd !important; margin:10px 0; background:none; text-align:left;">';
						html += '<h1 class="table_title">'+JSON.parse(data[data[i].CNT-1].BUILDERINFO).attribute.name+'</h1>';
						html += '</div>';
						html += '<div class="chart_style" style="padding: 0px; height:250px; width:100%;">';
						makeList.push({"TARGET":data[i].PATH,"VISIBLE":data[i].VISIBLE,"INFO":data[i].BUILDERINFO});
						html += data[i].HTML;
					}else if(i==0 || data[i].EXPLAIN == data[i-1].EXPLAIN){
						makeList.push({"TARGET":data[i].PATH,"VISIBLE":data[i].VISIBLE,"INFO":data[i].BUILDERINFO});
						html += data[i].HTML;
					}
					if(i == data.length-1){
						$("#mainPutData").append(html);
						for(var j=0;j<makeList.length;j++){
							if(makeList[j].VISIBLE == "N"){
								$(document).data("builderinfo",{filepath:makeList[j].TARGET});
								$("#"+makeList[j].TARGET).data("builderinfo",JSON.parse(makeList[j].INFO));
								$iv.uibuilder.build("#"+makeList[j].TARGET);
								$("#"+makeList[j].TARGET).hide();
							}else{
								$(document).data("builderinfo",{filepath:makeList[j].TARGET});
								var jsonArray = JSON.parse(makeList[j].INFO);
								jsonArray.attribute.height = "200";
								$("#"+makeList[j].TARGET).data("builderinfo",jsonArray);
								$iv.uibuilder.build("#"+makeList[j].TARGET);
								$("#"+makeList[j].TARGET)[0].style.top = "";
								$("#"+makeList[j].TARGET)[0].style.right = "";
								$("#"+makeList[j].TARGET)[0].style.left = "";
								$("#"+makeList[j].TARGET)[0].style.bottom = "";
								$("#"+makeList[j].TARGET)[0].style.width = "100%";
								$("#"+makeList[j].TARGET)[0].style.height = "100px";
								$("#"+makeList[j].TARGET)[0].style.position = "";
								makePorver(makeList[j].TARGET);
							}
						}
						html = "";	
						makeList = [];
					}
				}
			}});
		}
		$(function(){
			$( document ).on( "pagecreate", "#demo-page", function() {
				$( document ).on( "swipeleft ", "#demo-page", function( e ) {
					// We check if there is no open panel on the page because otherwise
					// a swipe to close the left panel would also open the right panel (and v.v.).
					// We do this by checking the data that the framework stores on the page element (panel: open).
					if ( $( ".ui-page-active" ).jqmData( "panel" ) !== "open" ) {
						if ( e.type === "swipeleft" ) {
							$( "#right-panel" ).panel( "open" );
						//} else if ( e.type === "swiperight" ) {
							//$( "#left-panel" ).panel( "open" );
						}
					}
			
				});
			});
			makeMain();
			sessionStorage.clear();
           if(window.sessionStorage.getItem("5999") == null || window.sessionStorage.getItem("5999") == "[]"){
				$iv.mybatis("getMobileTopMenuList",{USER_ID:$iv.userinfo.userid(),ACCT_ID:$iv.userinfo.acctid(),DIVISION:$iv.envinfo.division,LOCALE:"ko",P_ID:"5999"},{callback:function(res){
					var menuList = [];
					for(var i=0;i<res.length;i++){
						menuList.push({"C_ID":res[i].C_ID,"C_NAME":res[i].C_NAME});
					}
					window.sessionStorage.setItem(res[0].P_ID,JSON.stringify(menuList));
					leftMenuListMake();
				}});
			}else{
				leftMenuListMake();
			}
		});
	</script>
	<style>
		#demo-page :not(INPUT):not(TEXTAREA) {
		 -webkit-user-select: none;
		 -moz-user-select: none;
		 -ms-user-select: none;
		 -o-user-select: none;
		 user-select: none;
		}
		.ui-overlay-a, .ui-page-theme-a, .ui-page-theme-a .ui-panel-wrapper {
			text-shadow:none !important;
		}
		.ui-body-a, .ui-page-theme-a .ui-body-inherit, html .ui-bar-a .ui-body-inherit, html .ui-body-a .ui-body-inherit, html body .ui-group-theme-a .ui-body-inherit, html .ui-panel-page-container-a {
			background:none !important;
			border:0 !important;
			color: inherit !important;
			text-shadow:none !important;
			box-shadow:none !important;
			margin:0 !important;
		}
		.ui-page-theme-a .ui-btn, html .ui-bar-a .ui-btn, html .ui-body-a .ui-btn, html body .ui-group-theme-a .ui-btn, html head + body .ui-btn-a.ui-btn, .ui-page-theme-a .ui-btn:visited, html .ui-bar-a .ui-btn:visited, html .ui-body-a .ui-btn:visited, html body .ui-group-theme-a .ui-btn:visited, html head + body .ui-btn-a.ui-btn:visited {
			background: none !important;
			border: 0 !important;
			color:inherit !important;
			text-shadow: 0px 1px 0px #f3f3f3;
			box-shadow:none !important;
		}
		.ui-page-theme-b .ui-btn, html .ui-bar-b .ui-btn, html .ui-body-b .ui-btn, html body .ui-group-theme-b .ui-btn, html head + body .ui-btn-b.ui-btn, .ui-page-theme-b .ui-btn:visited, html .ui-bar-b .ui-btn:visited, html .ui-body-b .ui-btn:visited, html body .ui-group-theme-b .ui-btn:visited, html head + body .ui-btn-b.ui-btn:visited {
			background: none !important;
			border: 0 !important;
			color:inherit !important;
			text-shadow:none !important;
			box-shadow:none !important;
		}
		.ui-bar-a, .ui-page-theme-a .ui-bar-inherit, html .ui-bar-a .ui-bar-inherit, html .ui-body-a .ui-bar-inherit, html body .ui-group-theme-a .ui-bar-inherit {
			background: none !important;
			border: 0 !important;
			color:inherit !important;
			text-shadow:none !important;
			box-shadow:none !important;
			font-weight: normal !important;
		}
		.ui-panel-wrapper {
			background:none !important;/*	z-index:0 !important;*/
		}
		.ui-body-b, .ui-page-theme-b .ui-body-inherit, html .ui-bar-b .ui-body-inherit, html .ui-body-b .ui-body-inherit, html body .ui-group-theme-b .ui-body-inherit, html .ui-panel-page-container-b {
			text-shadow:none !important;
		}
		.ui-content {
			background:#dddddd !important;
			padding:0 !important;
		}
		body, input, select, textarea, button, .ui-btn {
			line-height: 1.231 !important;
		}
	</style>
</head>
<body class='default'>
<div class="loader" style="top:50%;left:50%;position:absolute;vertical-align: middle;text-align: center" ><img src="${WEB.ROOT}/resource/img/ajax-loader.gif" border="0" /></div>
<div data-role="page" id="demo-page">
  <div data-role="header" class="panel-header" data-theme="b" id="mainTopLayout">
    <h1><!--IPLANBIZ Mobile--></h1>
    <a href="#left-panel"  style="margin-top:-6px; left:0;">
    <div class="icon ic-menu"></div>
    </a> 
    <!-- <a class="ui-nodisc-icon ai-list" href="#left-panel" data-icon="carat-r" data-iconpos="notext" data-iconshadow="false" data-shadow="false" >Open left panel</a><a href="#right-panel" data-icon="carat-l" data-iconpos="notext" data-shadow="false" data-iconshadow="false" class="ui-nodisc-icon">Open right panel</a>-->
    <div class="info" >
      <div class="info_count">
        <div class="notification" >
          <div id="noticejqxNotification" >
            <div id="notificationContent"><span class="icon ic-check-circle-blank"></span></div>
            <!--notificationContent--> 
          </div>
          <!--noticejqxNotification--> 
        </div>
        <!--notification--> 
        <a href="javascript:goPage('notice');" class="btn_icon" title="공지사항"> <span class="icon ic-notifications"></span></a> </div>
<!--         <a href="app://notice" class="btn_icon" title="공지사항"> <span class="icon ic-notifications"></span></a> </div> -->
      <!--info_count--> 
    </div>
    <!--info--> 
  </div>
  <!--panel-header -->
  
  <div class="ui-content" role="main"> 
    <!--<dl>
          <dt>Swipe <span>verb</span></dt>
          <dd><b>1.</b> to strike or move with a sweeping motion</dd>
        </dl>--> 
    <!--차트영역-->
    <div id="mainLayout">
      <div class="contents cons">
        <div style="margin-top:10px;" id="mainPutData">
        </div>
      </div>
      <!--contents--> 
    </div>
    <div id="menu_List" class="container" style="bottom:0;top:0px;display:none;">
    	<input type="hidden" id="pId" name="pId" />
	  	<input type="hidden" id="pName" name="pName" />
	  	<input type="hidden" id="topId" name="topId" />
    	<span id="title"  style=" position:absolute !important;">
    		<span class=" navBar_head_back"> 
    			<a href="javascript:backMenu();"  title="뒤로"> 
    				<span class="ic-arrow-back" ></span>
    			</a>
    		</span><!--navBar_head_back--> 
    		<span class="navBar_title" id="showCname"></span> 
    		<span class="navBar_head_icon"> 
    			<a href="main.html" title="메인" >
    				<span class="ic-home  icon_hover" style=" font-size:25px; color:#C0C0C0; text-shadow:none; padding-right:5px;"></span>
    			</a> 
    		</span><!--navBar_head_icon--> 
       	</span><!--title-->
       <div class="contents" style=" position:absolute; top:42px;">
       	  <div id="menuLeft">
		      <ul id="MenuLeftList">
		      </ul>
	      </div>
       </div>
       <!--contents--> 
    </div>
    </div>
    <!--차트영역--> 
    <div data-role="panel" id="left-panel" class="left-panel" data-theme="b" style="background:#212633;"> 
    <!--<p>Left reveal panel.</p>-->
    <div class="left-panel-header"><a href="#" data-rel="close" class="close_btn"> <span class="ic-close" ></span></a>
     <p style="text-align:left; visibility:hidden;"><a href="javascript:logout();" class="logout">Logout</a></p>
      <p style="margin:10px 0 25px 0 !important;text-align:center;"><span>${sessionScope.loginSessionInfo.userName}</span></p>
      <!--title-->
      <div style=" line-height:normal !important; margin:0px auto !important; padding:0 !important; ">
<%--         <p style=" text-align:center;"><a href="javascript:logout();" title="로그아웃"><span class="ic-exit-to-app"></span></a> <a href="${WEB.ROOT}/notice/list_mobile" title="게시판"><span class="ic-notifications"></span></a> <a href="system.html" title="설정"><span class="ic-settings"></span></a> </p> --%>
        <p style=" text-align:center;"><a href="javascript:logout();" title="로그아웃"><span class="ic-exit-to-app"></span></a> <a href="javascript:goPage('notice');" title="게시판"><span class="ic-notifications"></span></a> <a href="system.html" title="설정"><span class="ic-settings"></span></a> </p>
      </div>
      <!--icon_links--> 
    </div>
    <!--left-panel-header-->
    <div class="left-panel-menu">
    	<div id="menu">
	      <ul id="leftMenuList">
	      </ul>
      </div>
    </div>
  </div>
  </div>
  <!-- ui-content -->
</div>
</body>
</html>

