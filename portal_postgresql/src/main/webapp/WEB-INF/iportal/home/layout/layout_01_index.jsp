<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ko"> 
<head> 
<meta name="viewport" content="width=device-width, initial-scale=1, minimun-scale=1.0, maximun-scale=5.0,user-scalable=yes, width=1280,height=1024" />
<meta http-equiv="X-UA-Compatible" content="IE=8"/>
<c:if test="${systemDivision == 'IPORTAL' }">
</c:if>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${systemConfig.system_name}</title>
<link rel="stylesheet" type="text/css" href="${WEB.CSS}/layout_01/common.css">
<link rel="StyleSheet" href="${WEB.CSS}/dtree.css" type="text/css">
<link rel="stylesheet" href="${WEB.CSS}/jquery.ui.all.css">
<link rel="stylesheet" href="${WEB.CSS}/treeview/jquery.treeview.css" />
<style>
.ui-tabs-paging-prev a,
.ui-tabs-paging-next a {
    display: block; 
    position: relative; 
    top: 1px; 
    border: 0;
    z-index: 2; 
    padding: 0;
    /* color: #444; */ 
    text-decoration: none;
	background: transparent !important; 
	cursor: pointer;
	width:50px;
}
/* test A*/
/*그라디언트스타일_예제(크로스브라우징코드)*/
.gradient{
width:50px;
height:54px;
background:#999;/* for non-css3 browsers */
filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='${systemConfig.top_low_color1}', endColorstr='${systemConfig.top_low_color2}'); /* for IE */
background: -webkit-gradient(linear, left top, left bottom, from(${systemConfig.top_low_color1}), to(${systemConfig.top_low_color2})); /* for webkit browsers */
background: -moz-linear-gradient(top,  ${systemConfig.top_low_color1},  ${systemConfig.top_low_color2}); /* for firefox 3.6+ */ 
}

.gradient_left_top{
width:200px;
height:36px;
background:#999;/* for non-css3 browsers */
filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='${systemConfig.left_title_color1}', endColorstr='${systemConfig.left_title_color2}'); /* for IE */
background: -webkit-gradient(linear, left top, left bottom, from(${systemConfig.left_title_color1}), to(${systemConfig.left_title_color2})); /* for webkit browsers */
background: -moz-linear-gradient(top,  ${systemConfig.left_title_color1},  ${systemConfig.left_title_color2}); /* for firefox 3.6+ */ 
}

.iframe_content{width:100%;height:100%}

/*상단 메뉴 버튼 클릭시 반전되는 색의 영역 크기 변경 및 글자 위치 조정 */
button.btn_gnb_on {height : 27px;}
#gnb table.table_gnb td {padding-top: 14px;}

</style>
	<script>
		// 최초 실행인지 여부를 알려주는 값
		var gFirstLoadPage = true;
		
		var gTabsActiveColor = "${systemConfig.tab_active_color}";
		var gTabsDeactiveColor = "${systemConfig.tab_default_color}";
		var gTabsLineColor = "${systemConfig.tab_line_color}";
		var gTabsFontColor = "${systemConfig.tab_font_color}";
		
		var gTopMenuDeactiveColor = "${systemConfig.top_menu_deactive_color}";
		var gTopMenuActiveColor = "${systemConfig.top_menu_active_color}";
		var solutionDomain = "${systemConfig.root_domain}";
		var uriContext = "${WEB.ROOT}";
		var isAdmin = "${sessionScope.loginSessionInfo.isAdmin}";
		
	</script>
	<script src="${WEB.JQUERY}/jquery-1.8.3.min.js"></script>
	<script src="${WEB.JQUERY}/jquery.cookie.js"></script>
	<script src="${WEB.JQUERY}/jquery-ui-1.9.1.custom.js"></script>
	<script src="${WEB.JQUERY}/jquery.i18n.properties-min-1.0.9.js"></script>
	<script src="${WEB.JS}/i18n.js"></script>
 	<script src="${WEB.JQUERY}/jquery-ui-tabs-paging.js"></script> 
	<script src="${WEB.JQUERY}/jquery-corner.js"></script>
	
	<link rel="stylesheet" href="${WEB.ROOT}/resource/jqwidgets/styles/jqx.base.css" type="text/css" />
	<link type="text/css" rel="stylesheet" href="${WEB.CSS}/button.css" />
    <script type="text/javascript" src="${WEB.ROOT}/resource/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="${WEB.ROOT}/resource/jqwidgets/jqxbuttons.js"></script>
    <script type="text/javascript" src="${WEB.ROOT}/resource/jqwidgets/jqxwindow.js"></script>
    <script type="text/javascript" src="${WEB.ROOT}/resource/jqwidgets/jqxscrollbar.js"></script>
    <script type="text/javascript" src="${WEB.ROOT}/resource/jqwidgets/jqxpanel.js"></script>
    
	<script type="text/javascript" src="${WEB.JSDWR}/engine.js"></script>
	<script type="text/javascript" src="${WEB.JSDWR}/interface/menuService.js"></script>
	<script type="text/javascript" src="${WEB.JSDWR}/interface/menuServiceDwr.js"></script>
	<script type="text/javascript" src="${WEB.JSDWR}/interface/noticeServiceDwr.js"></script>
	<script type="text/javascript" src="${WEB.JSDWR}/interface/groupManagerDwr.js"></script>
	<script type="text/javascript" src="${WEB.JS}/dTree/dtree.js"></script>
	<script src="${WEB.JQUERY}/jquery.treeview.js" type="text/javascript"></script>
	<script src="${WEB.JQUERY}/jquery.treeview.edit.js" type="text/javascript"></script>
	<script src="${WEB.JQUERY}/jquery.treeview.async.js" type="text/javascript"></script>
	<script>
		// js 파일에서 EL(Expression Language)을 사용할수 없어서 이전에 전역변수로 지정해준다.
		var gMaxTabCount= 5;
		gMaxTabCount = "${systemConfig.tab_count}";
		var userId="${sessionScope.loginSessionInfo.userId}";
		var cookieSwitch = "${systemConfig.cookie_use}";
		var viewSystemMenu = "${viewSystemMenu}";
		var themeLayout = "${systemConfig.theme_layout}";
		
		var gTopSnbMenuHeight = 92;
		var gTopIframeContent = 102;
		var gInnerIframeHeight = 0;
		var leftMenuWidth = 210;
	</script>
	<script type="text/javascript" src="${WEB.JS}/common.js"></script>
	<script type="text/javascript" src="${WEB.JS}/mainFrame.js"></script>
	<script>
		
		function resizeTabsWidth(){
//			$("#snb_menu").css("height", $(window).height()-92+gInnerIframeHeight);
// 			var tmp = $(window).height()-gTopSnbMenuHeight;
// 			var tmp2 = $(window).height()-gTopIframeContent;
			
// 			$("#snb_menu").css("height", tmp+"px");
// 			$(".iframe_content").css("height", tmp2+"px");
			
			if($.browser.msie){
				$("#gnb").css("width", $(window).width()-210);
				$("#gnb").css("min-width", "1056px");
				$("#wrap_content").css("width", $(window).width()-leftMenuWidth);
				$("#wrap_content").css("min-width", "1056px");
				$("#top_menu").css("width", "1250px");
// 				$("#header").css("min-width", "1290px");
				//$("#header").css("width", $(window).width()-200);
			}else{
				$("#gnb").css("width", $(window).width()-210);
				$("#gnb").css("min-width", "1060px");
				$("#wrap_content").css("min-width", "1060px");
				$("#wrap_content").css("width", $(window).width()-leftMenuWidth);
				$("#top_menu").css("width", "1270px");
// 				$("#header").css("min-width", "1280px");
// 				$("#header").css("width", "100%");
			}
			$("#snb_menu").css("height", $(window).height()-91+gInnerIframeHeight);
			
			if("${WEB.LODER}" != 'false'){
				$(".iframe_content").css("height", $(window).height()-120+gInnerIframeHeight);
			}else {
				$(".iframe_content").css("height", $(window).height()-90+gInnerIframeHeight);
			}	
		}
	 
	 	$(document).ready(function(){
			window.onresize = resizeTabsWidth;
			var backgroundColor= '${topColor}';
			$("#top_view").css("background-color", backgroundColor);
			getTopMenu();
			resizeTabsWidth();
			setPageSetting();
			makeTopSelectMenu();
			
			gFirstLoadPage = false;
			// initialize tabs with default behaviors
	
			jQuery('#tabs').tabs({ cache: true, activeOnAdd: true });
			jQuery('#tabs').tabs( "option", "active", 21);
			// initialize paging
			function init() {
				jQuery('#tabs').tabs('paging', { cycle: false, follow: false, followOnActive: false, activeOnAdd: false });
			}
 			init();
 			$("#tabs").find(".ui-state-default").css("background-color", gTabsDeactiveColor);
 			$("#tabs").find(".ui-state-active").css("background-color", gTabsActiveColor);
 			$("#tabs").find(".ui-state-default").css("border", "1px solid "+gTabsLineColor);
 			$("#tabs").find(".ui-state-active").css("border", gTabsLineColor);
 			$("#tabs").find(".ui-tabs-anchor").css("color", gTabsFontColor);
 			
 			
 			// 팝업 리스트
 			getNoticePopUpList();
		});
	 	
	 	function readjust_portlet_width(){
	 		if($.cookie("fullScreenMode") == "true"){
	 			if("${WEB.HEADER}" == 'false'){ 
	 			}else if ("${WEB.LEFT_MENU}" == 'false'){
	 			}else {
	 				toggleHeader();
	 				toggleLeftMenu();
	 			}
	 			$.cookie("fullScreenMode", "", {expire:-1});
 			}
	 	}
		
		window.onresize = resizeTabsWidth;
			
	
	     // go Search (코그너스 보고서 검색)
		 function goSearch(){
			var keyword = document.getElementById("search").value;
			document.getElementById("search").value = "";
			if(keyword == ""){
				alert("검색어를 입력하세요.");
				document.getElementById("search").focus();
				return;
			}else{
				addTab("search_"+keyword, "보고서 검색("+ keyword +")", "${WEB.ROOT}" + "/cogSearch?searchKeyWord="+encodeURIComponent(keyword), 0,0);
			}
		 }
	     //logout (코그너스 로그아웃)
	     function logout(){
	    	 var msg = window.confirm("로그아웃 하시겠습니까?");
	    	 if(msg == true){
	    	 	location.replace("${WEB.ROOT}" + "/cogLogout");
	    	 }
	     }
	
	     // 코그너스 내 폴더 가기
	     function myFolderMng(){
	    	 addTab(1, "내 폴더 구성하기","${WEB.ROOT}/myFolderMng", 0,0);
	     }
	     function setPage(){
	    	 
	    	 viewCookieTab();
	
	    	 getPortalMenu('${param.uid}');
	    	 //xmlParser("${WEB.ROOT}/cogMenu", "ac=left", 1);
	    	 
	     }
	     
	     function getTopMenu(){
	    	 
	    	 menuService.dwrGetSystemDivision({callback:function(res){
	    		solutionType = res;	 
	    	 }, async:false});
			
	    	 switch(solutionType){
	    	 	case "IPORTAL":
	    	 		setPage();
	    	 		break;
	    	 	case "IVISION":
	    	 		iVision_init();
	    	 		break;
	    	 	case "IMANAGER":
	    	 		iManager_init();
	    	 		break;
	    	 	default:
	    	 		iPortal_init();		// js/mainFrame.js : iPortal_init() 호출
	    	 	break;
	    	 }
	     }
	     
	     
	     
	     function toggleLeftMenu(){
	    	 $("#snb").toggle();
	    	 if($("#snb").css("width") == "210px"){
	    		 $("#wrap_content").css("width", $("#wrap_content").width()+210);
	    		 if("${WEB.ACCT_ID}" != "lotteFood"){
                     $(".iframe_content").contents().find("body").css("width", 1239);
             	 }
	    		 $("#snb").css("width", "0px");
	    		 $("#wrap_content").css("left", "0px");
	    		 leftMenuWidth = 0;
	    	 }else{
	    		 $("#wrap_content").css("width", $("#wrap_content").width()-210);
	    		 if("${WEB.ACCT_ID}" != "lotteFood"){
	    		 	$(".iframe_content").contents().find("body").css("width", 1039);
	    		 }
	    		 $("#snb").css("width", "210px");
	    		 $("#wrap_content").css("left", "210px");
	    		 leftMenuWidth = 210;
	    	 }
	    	 $(".btn_snb_close").toggle();
	    	 $(".btn_snb_open").toggle();
	     }
	     
		function setPageSetting(){
			$("#logo").css("background-color", "${systemConfig.top_logo_color}");
			$("#snb").css("background-color", "${systemConfig.left_menu_background}");
			$("#wrap_tab_menu").css("background-color", "${systemConfig.top_tab_menu_background}");
			
			//$("#header").css("background-color", "${systemConfig.top_high_color}");
		}
		function openSettingPage(){
			window.open("${WEB.ROOT}/popup/portalSetting","_blank","height=500,width=700,scrollbars=yes");
		}
		
		function openChangePasswd(){
			 
			window.open("${WEB.ROOT}/changePasswd","_blank","height=280,width=400,no, auto");
		}

		function topSelectMenu(val){
			$(".topSelectMenu option:eq(0)").attr("selected", "selected");
			if(val=="admin"){
				openSettingPage();
			}else if(val=="changePasswd"){
				openChangePasswd();	
			}else{
				eval(tmpArr[val]);
			}
		}
		var tmpArr = new Array();
	    function toggleHeader(){
	    	$("#header").toggle();
	    	$("#header_on").toggle();
	    	$("#header_off").toggle();
			
	    	 if($("#container").css("top") == "55px"){
	    		 $("#container").css("top", "0px");
	    		 gTopSnbMenuHeight = gTopSnbMenuHeight - 155;
	    		 gTopIframeContent = gTopIframeContent - 155;
	    		 
	    		 tmp = $(".iframe_content").contents().find("#iframe_cognos").height()+55;
	    		 $(".iframe_content").contents().find("#iframe_cognos").css("height", "500px");
	    		 gInnerIframeHeight = 55;
	    	 }else{
	    		 $("#container").css("top", "55px");
	    		 gTopSnbMenuHeight = gTopSnbMenuHeight + 155;
	    		 gTopIframeContent = gTopIframeContent + 155;
	    		 
	    		 $(".iframe_content").each(function(){
	    			 now = $(this).contents().find("#iframe_cognos").height()-55;
	    			 $(this).contents().find("#iframe_cognos").css("height", now+"px");
	    		 });
	    		 gInnerIframeHeight = 0;
	    	 }
 	    	 resizeTabsWidth();
	    	 
	    	 
	     }
		function makeTopSelectMenu(){
			groupManagerDwr.dwrGetTopSelectMenu("Y", function(res){
				for(var i=0;i<res.length;i++){
					$(".topSelectMenu").append("<option value='"+res[i].UNID+"'>"+res[i].NAME);
					if(res[i].URL_TYPE == 1){
						tmpArr[res[i].UNID] = res[i].URL;
					}else if(res[i].URL_TYPE == 2){
						tmpArr[res[i].UNID] = "window.open('"+res[i].URL+"', '_blank')";
					}
				}
			});
			
		}
		
	
	</script>
</head> 

<body> 
<!-- 세션종료 알림 -->
<c:import url="../../session/warn-timeout-modal.jsp" />
<!-- #wrap -->
<div id="wrap">
	<!-- #header -->
	<div id="header">
		<div id="logo" class="link" onclick="moveTab(0)"><img src="${WEB.IMG}/n/logo_sch.gif" style="cursor:pointer"></div>
		<!-- #gnb -->
        <div id="gnb" class="gradient">
        	<!-- Ul, Li Tag 를 사용하게 되면 화면 사이즈가 줄어들었을경우 메뉴가 떨어진다. 그 현상을 막기 위해 Table Tag 를 사용. -->
			<table class="table_gnb" >
	            <tr id="topMenu">
	            </tr>
            </table>
		</div><!-- // #gnb -->
        <!-- #top_menu -->
		<div id="top_menu">
			<select onchange="topSelectMenu(this.value);" class="topSelectMenu">
				<option value="--">- <spring:message code="menu.customMenus" /> -
<%-- 				<c:if test="${sessionScope.loginSessionInfo.userId == 'admin'}"> --%>
<!--      			<option value="admin">Portal Setting -->
<%-- 				</c:if> --%>
			<option value="changePasswd"><spring:message code="menu.changePassword" />
			<c:if test="${sessionScope.loginSessionInfo.isAdmin}">
     				<option value="admin">Portal Setting
			</c:if>
			</select> 
			<c:if test="${systemDivision == 'IPORTAL' }">
			<div class="box_search">
				<label for="search" id="search_label" style="position:absolute; visibility:visible;padding-top:4px;color:#999999">검색</label>
				<input type="text" id="search" name="search" onkeypress="if(event.keyCode == 13) goSearch();" onfocus="$('#search_label').css('visibility','hidden');" onblur="$('#search_label').css('visibility','visible');" class="top_search" />
				<img alt="검색" src="${WEB.IMG}/n/bg_search.gif" onclick="goSearch();" style="cursor:pointer">
			</div>
			</c:if>
			<div class="my_menu"><span class="bold">${sessionScope.loginSessionInfo.userName}</span>[${sessionScope.loginSessionInfo.userId}] 님 | 
			<span class="my" onclick="logout();">로그아웃</span>
			</div>

		</div><!-- // #top_menu -->
	</div><!-- // #header -->
	<!-- #container -->
	<span style="position:absolute;top:0px;left:1200px;z-index:2;display:none;" onclick="toggleHeader()" id="header_on">
		<img src="${WEB.IMG}/n/btn_header_on.gif">
	</span>
	<span style="position:absolute;top:45px;left:1200px;z-index:2" onclick="toggleHeader()" id="header_off">
		<img src="${WEB.IMG}/n/btn_header_off.gif">
	</span>
	<div id="container" class="style1">
		<!-- #snb -->
		<div id="snb" style=";">
			<!-- .wrap_snb_title -->
			<div class="wrap_snb_title gradient_left_top">
				<div class="snb_title"><span id="logoimg"><!--  내 폴더  --></span></div>
				<div class="btn_snb_close" onclick="toggleLeftMenu();"><img src="${WEB.IMG}/n/btn_snb_close.gif" alt="사이드네비 닫기"></div>
			</div><!-- // wrap_snb_title -->
			<!-- #snb_menu -->            
			<div id="snb_menu" style="white-space:nowrap">
            	<!-- .snb_menu_pad -->
				<div class="snb_menu_pad">
					<div class="loader" style="position:absolute;margin-top: 100px;margin-left:90px;display:none;z-index:10;vertical-align: middle;text-align: center" ><img src="resource/img/ajax-loader.gif" border="0" /></div>
					<div id="trees_id" class="filetree">
					</div>
				</div><!-- // .snb_menu_pad -->            
			</div><!-- // #snb_menu -->
		</div><!-- // #snb -->
		<!-- #wrap_content -->
		<div id="wrap_content"  style="overflow:hidden;">
			<!-- #wrap_tab_menu -->
			<div id="wrap_tab_menu">
				<div class="btn_snb_open" style="display:none;position:absolute;cursor:pointer;z-index:100" onclick="toggleLeftMenu();">
					<img src="${WEB.IMG}/n/btn_snb_open.gif" alt="사이드네비 열기">
				</div>
<!-- 				<div class="tab_control"> -->
<%-- 					<span onclick="leftClick();" style="cursor:pointer;"><img src="${WEB.IMG}/n/btn_tab_prev.png" alt="이전"></span><span onclick="rightClick();" style="cursor:pointer;"><img src="${WEB.IMG}/n/btn_tab_next.png" alt="다음"></span><span onclick="closeClick();" style="cursor:pointer;"><img src="${WEB.IMG}/n/btn_tab_del.png" alt="삭제"></span> --%>
<!-- 				</div> -->

 				<div id="tabs" style="top:7px;position:relative;">
				  
					<ul class="tab" id="uiTab" style="margin-left:12px;">
						<li style="width:142px;"><a href="#tabs-1">Main</a><span class="ui-icon ui-icon-gear" onclick="javascript:portletManagerOpen();" style="zoom:1.5"></span></li>
					</ul>
					<div style="margin-top: 12px; margin-bottom: 2px; margin-left:10px;margin-right:0px"></div>
	
					<div id="tabs-1" class="content">
						<iframe src="${portletLayout}" name="portletIframe" class="iframe_content" frameborder="0"   onload="readjust_portlet_width()"></iframe>
					</div>
				</div>
			</div>
 		</div>
	</div><!-- // container -->
</div><!-- // wrap --> 
</body> 
</html>