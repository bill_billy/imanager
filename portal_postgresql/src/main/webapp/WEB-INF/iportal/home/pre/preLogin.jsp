<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=9"/>
		<title></title>
			<script>
				var userId="${sessionScope.loginSessionInfo.userId}";
			</script>
			<script src="${WEB.ROOT}/resource/js/jquery/jquery-1.8.3.min.js"></script>
			<script src="${WEB.JQUERY}/jquery.cookie.js"></script>
			<script src="${WEB.ROOT}/resource/js/util/remoteInfo.js"></script>
			<script type="text/javascript" src="${WEB.ROOT}/dwr/engine.js"></script>
			<script type="text/javascript" src="${WEB.ROOT}/dwr/interface/groupManagerDwr.js"></script>
			<script type="text/javascript">
			
				function goMain(){
					setLog();
					
					if('${systemConfig.fullscreen_mode}' == '1'){
						$.cookie("fullScreenMode", "true");
						
						var browserLocationBarSetting = "";
						
						if( window.navigator.userAgent.indexOf("MSIE 7") != -1){
							browserLocationBarSetting = "Internet Explorer 7.location=no";
						}else{
							browserLocationBarSetting = "location=no";
						}
						
						//window.open('${WEB.ROOT}/main', "_blank", "height=1024,width=1280,menubar=0,resizable=yes,"+browserLocationBarSetting+",toolbar=0");
						//window.open('${WEB.ROOT}/main', "_blank", "width=" + screen.width + ",height=" + screen.height + ", menubar=0, resizable=yes,"+browserLocationBarSetting+",toolbar=0");
						
						window.open('${WEB.ROOT}/main', "_blank", "width=" + screen.width + ",height=" + screen.height + ", menubar=0, resizable=yes,status=no"+browserLocationBarSetting+",toolbar=0");
						
						window.opener='nothing';
						window.open('','_parent','');
						window.close();
					}else{
						$.cookie("fullScreenMode", "false");
						location.href = '${WEB.ROOT}/main';
					}
					return;
				}
				
				
				var cnt = 0;
				function login(){
					cnt++;
					if(cnt > 1) return false;
					
					document.getElementById("loginIfr").onload= function(){
						goMain();
					};
					document.getElementById("loginIfr").src="${cognosUrl}?m_passportID=${sessionScope.loginSessionInfo.passport}";
					
				}
				function setLog(){
					getOSInfoStr();
					// dwrSetUserLog( ActionType, targetType, targetValue, targetId );
					groupManagerDwr.dwrSetUserLog(3, 1000, '', '', '<%=request.getRemoteAddr() %>', getOSInfoStr(), getBrowserName(), getBrowserVer(), '',{callback:function(){
					}, async:false});
				}
				
			</script>
	</head>
<body>
<div class="loader" style="top:50%;left:50%;position:absolute;" valign="middle" align="center" ><img src="${WEB.ROOT}/resource/img/ajax-loader.gif" border="0"/></div>
<iframe id="loginIfr" src="${cognosUrl}?b_action=xts.run&m=portal/logoff.xts&h_CAM_action=logoff"  scrolling="no" frameborder="0" marginwidth="0" marginheight="0" style="display:none"  width="500" height="500" onload="login()" />
</body>
</html>
