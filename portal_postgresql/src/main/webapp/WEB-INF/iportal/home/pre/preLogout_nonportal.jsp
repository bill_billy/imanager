<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title></title>
			<script>
				var userId="${sessionScope.loginSessionInfo.userId}";
			</script>
			<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>     
			<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>

			<script src="../../resources/js/util/remoteInfo.js"></script>
			<script type="text/javascript">
				function goMain(){
					//removeCookie();
					setLog();
					//location.href = '${systemConfig.root_domain}${solutionDomainUrl}?key=${sessionKey}';
					var isSSO = <%=session.getAttribute("isSSO")%>;
					
					if(isSSO == true){
						location.href = '${WEB.ROOT}/sso/logout.jsp';
					}else{
// 						$iv.mybatis("dwrGetExtRootDomain", {}, function(res){
// 							var division = $iv.envinfo.division;
// 							var mainRootDomain = "${systemConfig.root_domain}";
// 							var arrRootDomain = "";
// 	 						if(res.length > 0){
// 	 							for(var i=0;i<res.length;i++){
//  									if(i==0 || i == res.length-1){
//  	 									arrRootDomain += res[i].EXT_DIVISION_ROOTDOMAIN;
//  	 								}else{
//  	 									arrRootDomain += res[i].EXT_DIVISION_ROOTDOMAIN + ";";
//  	 								}	
// 	 							}
// 	 						}
// 	 						for(var i=0;i<arrRootDomain.split(";").length;i++){
// 								var rootdomain = arrRootDomain.split(";")[i];
// // 								var src=rootdomain+'/sessionCreateJson?key=${sessionKey}';
// // 								$(document).find("body").append("<iframe src='"+src+"' style='display:none;'></iframe>");
// 								$.ajax({
// 									url : rootdomain+'/logout', 
// 									dataType : "jsonp",
// 									jsonp:"sessioncheck",
// 									success : function(d){
// 										//console.log("success"+d);
// 									},
// 									complete:function(data){
// 										//console.log("complete:function"+data);
// 									},
// 									error:function(err){
// 										if(err.status!="200") {
// 											var logouturl = '${systemConfig.root_domain}${solutionDomainUrl}';
// 											if(logouturl.indexOf("logout")==-1) logouturl +="/logout";
// 											location.href = logouturl;    
// 										}
// 										//console.log("error:function"+err);
// 									}
// 								});
// 							} 
// 							$.ajax({
// 									url : '/imanager/system/session/logout', 
									 
// 									success : function(d){
// 										 location.href = mainRootDomain + '/logout';
// 									},
// 									complete:function(data){
// 										 location.href = mainRootDomain + '/logout';
// 									},
// 									error:function(err){
// 										 location.href = mainRootDomain + '/logout';
// 									}
// 								}); 
// 						});
						location.href = '/logout';
// 						var root = "${systemConfig.root_domain}";
						//로그아웃 세션처리를 위해 logout 으로 이동.
					}
				}

				$(document).ready(function(){
//					alert("http://192.168.12.25${solutionDomainUrl}?key=${sessionKey}");
					goMain();
				});
				
				function setLog(){
					getOSInfoStr();
					// dwrSetUserLog( ActionType, targetType, targetValue, targetId );
					userIp = '<%=request.getRemoteAddr() %>';
					$i.post("/login/action/insertUserLog", {actionTypeNo : 4, targetTypeNo: 1000, targetValue:"",cID:"",remoteIP:userIp, osInfo:getOSInfoStr(), browserInfo:getBrowserName(), browserVersion:getBrowserVer(),targetPath:""}).done(function(res){});
				}
				
			</script>
	</head>
<body>
<!-- <iframe src="http://192.168.12.25${solutionDomainUrl}?key=${sessionKey}" name="2" scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="0" height="0" nosize onload="goMain()" /> -->
</body>
</html>
