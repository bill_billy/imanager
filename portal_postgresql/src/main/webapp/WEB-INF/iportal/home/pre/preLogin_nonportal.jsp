<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<title></title>
<script src="${WWW.JS}/iplanbiz/core/iplanbiz.web.include.jsp"></script>
<script src="${WEB.ROOT}/resource/js/util/remoteInfo.js"></script>
<script type="text/javascript" src="${WEB.ROOT}/dwr/interface/groupManagerDwr.js"></script>
<script type="text/javascript">
	var userId="${sessionScope.loginSessionInfo.userId}";
	var accesInfo = "${accesInfo}";
	
	/* 프레임 로드 체크 */
	var totcnt = 0;
	var totalRes = 0;
	function checkFrameLoad() {
		totcnt++;
		
		if(totcnt == totalRes) {
			location.href = '${WWW.ROOT}';
		}
		/*
		오산대 신규임용관리 커스텀.. 지원자일경우 지원자 포털. 지원자가 아닐경우 기존 포털로 리다이렉트.
		if(totcnt == totalRes) {
			$iv.mybatis("customer271_empCheck", {ACCT_ID: $iv.userinfo.acctid(), DIVISION: $iv.envinfo.division, USER_ID: $iv.userinfo.userid()}, function(res){
				if(res[0].CNT >= 1){
					location.href = 'http://211.221.231.120:8095/customer/layout_c0271empl_index';
				}
				else{
					
					location.href = '/${WWW.ROOT}'; 
				}
			});
		} 
		*/
	}
	$(document).ready(function() {
		setLog();
		
		if(accesInfo != ""){
			$.cookie("fullScreenMode", "false");
			location.href = $iv.envinfo.root;
		}else{
			$iv.mybatis("dwrGetExtRootDomain", {}, function(res){
				// var division = $iv.envinfo.division;
				// var mainRootDomain = $iv.envinfo.root;
				
				var mainRootDomain = "${systemConfig.root_domain}";
				var arrRootDomain = "";
				
				if(mainRootDomain == '') {
					location.href = '/${WWW.ROOT}';
				} else {
					arrRootDomain = mainRootDomain;
				}
				
				for(var i=0;i<res.length;i++) {
					arrRootDomain += ';'+res[i].EXT_DIVISION_ROOTDOMAIN;
				}
				
				<%--
				full screen mode 삭제함 - 2016.03.21 : 박대열
				1. full screen mode 는 기존 2.0 버전부터 있던 기능임.
				2. 확장 솔루션 개념이 들어오면서, 바뀐 소스가 확장 솔류션만 팝업하는 형태로 변경 됨(기존 소스 및 아래 소스 참고)
				3. 2016.03.21 배재대 로그인 관련 부분 수정 중 full screen mode 부분은 스킵함. 
				
				if('${systemConfig.fullscreen_mode}' == '1') {
					$.cookie("fullScreenMode", "true");
					totalRes = arrRootDomain.split(";").length;
					for(var i=0;i<arrRootDomain.split(";").length;i++) {
						var rootdomain = arrRootDomain.split(";")[i];
						if(rootdomain.trim()!="") {
							window.open(rootdomain+'${solutionDomainUrl}?key=${sessionKey}', "_blank", "fullscreen=1,height=1024,width=1280,menubar=0,location=0,toolbar=0");
							window.opener='nothing';
							window.open('','_parent','');
							window.close();
						}
					}
				}
				--%>
				
				<%-- 솔루션 별 iframe 로 로그인 --%>
				$.cookie("fullScreenMode", "false");
				totalRes = arrRootDomain.split(";").length;
				if(totalRes == 1){
					location.href = '${systemConfig.root_domain}${solutionDomainUrl}?key=${sessionKey}';
				}else{
					for(var i=0;i<arrRootDomain.split(";").length;i++) {
						var rootdomain = arrRootDomain.split(";")[i];
						var src = '../../login/action/sessionCreateJson?key=${sessionKey}';
						var iframe = $("<iframe src='"+src+"' style='width:0px; height:0px; border:0;' name='frameDomain' onload='checkFrameLoad();'></iframe>");
						iframe.appendTo($(document).find("body"));
						iframe.on("load",checkFrameLoad);
						//$(document).find("body").append("<iframe src='"+src+"' style='width:0px; height:0px; border:0;' name='frameDomain' onload='checkFrameLoad();'></iframe>");
					}
				}
				
			});
		}
	});
	function setLog() {
		getOSInfoStr();
		groupManagerDwr.dwrSetUserLog(3, 1000, '', '', '<%=request.getRemoteAddr() %>', getOSInfoStr(), getBrowserName(), getBrowserVer(), '',function() {
		});
	}
</script>
</head>
<body>
</body>
</html>
