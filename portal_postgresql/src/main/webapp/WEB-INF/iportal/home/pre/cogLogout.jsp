<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title></title>
			<script src="${WEB.ROOT}/resource/js/jquery/jquery-1.8.3.min.js"></script>
			<script type="text/javascript" src="${WEB.ROOT}/dwr/engine.js"></script>
			<script src="${WEB.ROOT}/resource/js/util/remoteInfo.js"></script>
			<script src="${WWW.JS}/iplanbiz/core/iplanbiz.web.include.jsp"></script>
			<script type="text/javascript" src="${WEB.ROOT}/dwr/interface/groupManagerDwr.js"></script>
			<script type="text/javascript">
				setTimeout("goLogout()",100);
				function goLogout(){
					setLog();
					var isSSO = <%=session.getAttribute("isSSO")%>;
					 
						$iv.mybatis("dwrGetExtRootDomain", {}, function(res){
							var division = $iv.envinfo.division;
	 						var arrRootDomain = "${systemConfig.root_domain}"+";";
	 						if(res.length > 0){
	 							for(var i=0;i<res.length;i++){
	 								if(i == res.length-1){
	 									arrRootDomain += res[i].EXT_DIVISION_ROOTDOMAIN;
	 								}else{
	 									arrRootDomain += res[i].EXT_DIVISION_ROOTDOMAIN + ";";
	 								}
	 							}
	 						}
	 						for(var i=0;i<arrRootDomain.split(";").length;i++){
								var rootdomain = arrRootDomain.split(";")[i];
								$.ajax({
									url : rootdomain+'/logout', 
								dataType : "jsonp",
								jsonp:"sessioncheck",
								success : function(d){
									//console.log("success"+d);
								},
								complete:function(data){
									//console.log("complete:function"+data);
								},
								error:function(err){
									if(err.status!="200") location.href = '${systemConfig.root_domain}${solutionDomainUrl}';    
									//console.log("error:function"+err);
								}
								});
							}
	 						if(isSSO == true){
	 							location.href = '${WEB.ROOT}/sso/logout.jsp';
	 						}
	 						else{
	 							location.href = '${WEB.ROOT}/logout';
	 						}
						});
// 						var root = "${systemConfig.root_domain}";
						//로그아웃 세션처리를 위해 logout 으로 이동.
					  
				}
				
				function setLog(){
					// dwrSetUserLog( ActionType, targetType, targetValue, targetId );
					userIp = '<%=request.getRemoteAddr() %>';
					$i.post("/login/action/insertUserLog", {actionTypeNo : 4, targetTypeNo: 1000, targetValue:"",cID:"",remoteIP:userIp, osInfo:getOSInfoStr(), browserInfo:getBrowserName(), browserVersion:getBrowserVer(),targetPath:""}).done(function(res){});
				}
			</script>
	</head>
<body>
<iframe src="${cognosUrl}?b_action=xts.run&m=portal/logoff.xts&h_CAM_action=logoff"  scrolling="no" frameborder="0" marginwidth="0" marginheight="0" width="0" height="0"></iframe>
</body>
</html>
