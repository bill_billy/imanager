<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Language" content="ko">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
    <title id='Description'>비밀번호변경</title>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/ktoto/css/style.blueish.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/ktoto/css/jqx.blueish.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
	<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
    <script src="../../resources/js/util/remoteInfo.js"></script>
	<script>
	var remoteIP = '<%=request.getRemoteAddr() %>';
	var remoteOS = getOSInfoStr();
	var remoteBrowser = getBrowserName();
		function checkPassword(){
			var p0 = $("#passwd0").val();
			var p1 = $("#passwd1").val();
			var p2 = $("#passwd2").val();
			var num = p1.search(/[0-9]/g);
			var eng = p1.search(/[a-z]/ig);
			var spe = p1.search(/[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi);
			if(p1!=p2){
				$i.dialog.error('SYSTEM','새 비밀번호와 새 비밀번호 확인이 일치하지 않습니다. 다시입력해주세요',function(){
					$("#passwd1, #passwd2").val("");
					$("#passwd1").focus();
				});
				return false;
			}else{
				//길이
				if(p1.length < 9 || p1.length > 20){
					$i.dialog.error('SYSTEM','비밀번호는 9~20자리로 입력해주세요.',function(){
						$("#passwd1").focus();
					});
					return false;//';drop view iportal.ivfe1002;select 1 from dual where ''='
				}
				/*
				if(p1.search(/\s/g) != -1){
					$i.dialog.error('SYSTEM','비밀번호는 공백없이 입력해주세요.',function(){
						$("#passwd1, #passwd2").val("");
						$("#passwd1").focus();
					});
					return false;
				}
				//유효하지 않는 문자
				if(p1.search(/[^0-9a-zA-Z!@#$%^&*+=-]/gi) != -1){
					$i.dialog.error('SYSTEM','비밀번호는 영문,숫자,특수문자(!@#$%^&*+=-)만 입력가능합니다.',function(){
						$("#passwd1, #passwd2").val("");
						$("#passwd1").focus();
					});
					return false;
				}*/
				//영문,숫자,특수문자 3가지 포함
				if(num < 0 || eng < 0 || spe < 0){
					$i.dialog.error('SYSTEM','영문, 숫자, 특수문자를 각 1자리 이상 입력해주세요.',function(){
						$("#passwd1").focus();
					});
					return false;
				}
			}
			var data =  $i.post("../../home/pwd/update", {newpwd:p1,orgpwd:p0,remoteIP:remoteIP,remoteOS:remoteOS,remoteBrowser:remoteBrowser});
    		data.done(function(pdata){
    			var code = pdata.returnCode;
    			var msg = pdata.returnMessage;
    			if(code=='SUCCESS'){
    				$i.dialog.alert('SYSTEM',msg,function(){
    					var type = "${type}";
    					if(type=='welcome'){
    						location.href='../../home/layout/list';	
    					}else{
    						top.document.location.reload();
    					}
    					
    				});
    				
    				
    			}else{
    				$i.dialog.error('SYSTEM',msg,function(){
    					$("#passwd0").focus();
    				});
    			}
    		});
			return true;
		}
		function cancleBtn(){
			var type = "${type}";
			if(type=='welcome'){
				//$i.dialog.confirm("SYSTEM","로그아웃 하시겠습니까?",function(){
					location.replace("../../login/action/cogLogout");
				//});
			}
			else{
				top.document.location.reload();
			}
		}
		$(document).ready(function(){
			$('#passwd2').on('keydown',function() {
				if(event.keyCode==13) {
					$('#passwd2').blur();
					checkPassword();
					return false;
				}
			});
    	});
	</script>
</head>
<body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
<div class="wrap" style="width:98%; min-width:600px; margin:0 1%;">
  <div class="container f_left" style="width:100%; margin:10px 0;">
    <div class="pw_change">
  	 <h2>비밀번호변경</h2>
       
       <div class="pw_change_box" style="height:500px !important;">
  	   <div class="confirm2">
  	   		<c:if test="${type=='welcome'}">
  	   			<b>${sessionScope.loginSessionInfo.userName}</b>님</br>
  	   		</c:if>
  	   <span class="color_point">비밀번호변경</span>을 통해<br>
  	     개인정보를 안전하게 보호하세요.<br>
         <span class="txt">비밀번호가 타인에게 노출되지 않도록 항상 주의해주세요.</span>       
       </div>
       
		<div id="login_box" class="logbox">
          <input name="" type="hidden" id="" value = ""/>
          <p class="pwd_new">
            <input name="passwd0" type="password" id="passwd0" style="padding:3px;padding-left:5px;" onKeyPress="" placeholder="현재 비밀번호" />
          </p>
		  <p class="pwd_new">
            <input name="passwd1" type="password" id="passwd1" style="padding:3px;padding-left:5px;" onKeyPress="" placeholder="새 비밀번호" />
          </p>
		  <p class="pwd_new">
            <input name="passwd2" type="password" id="passwd2" style="padding:3px;padding-left:5px;" onKeyPress="" placeholder="새 비밀번호 확인" />
          </p>
		  <p class="chk2">
           * 비밀번호는 9~20자 이내로 영문, 숫자, 특수문자가 각 1자리 이상 포함되어야 합니다.</p>
		  <div class="btn">
            <p id="btn" class="btn1">
		    <input type="button" value="취소" onClick="cancleBtn();">
		    </p>
		    <p id="btn" class="btn2">
		    <input type="button" value="변경" onClick="checkPassword();">
		    </p>
	     </div>
		<!--//content-->
    
      </div>

    </div>
    <!--//content-->
  </div>
  <!--//container--> 
</div>
</div>
<!--//wrap-->
</body>
</html>