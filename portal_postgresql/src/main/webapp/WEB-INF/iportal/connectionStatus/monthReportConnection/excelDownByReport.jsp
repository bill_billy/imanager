 <%@ page language="java" contentType="application/vnd.xls;charset=UTF-8" pageEncoding="utf-8"%>   -
<%@page import="java.net.URLEncoder"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%

	String header =request.getHeader("User-Agent");
	String browser="";  
	String docName = URLEncoder.encode("월별 보고서 접속 현황", "utf-8");  
	 
	System.out.println("header++++++++++++++++++++++++++++++"+header+":::::"+docName);
	 
	
    response.setHeader("Content-Disposition", "attachment; filename="+docName+".xls"); 
    response.setHeader("Content-Description", "JSP Generated Data");

%>   -
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>월별 보고서 접속 현황</title>
	
	</head>
 	<body>
		<table width="100%;" id="tableList" class="fix_rable" cellspacing="0" cellpadding="0" border="1">		
			<thead>			
				<tr> 			
					<th style="background-color:#eff0f0;" >순번</th>
					<th style="background-color:#eff0f0;" >보고서 명</th>
					<th style="background-color:#eff0f0;" >보고서 경로</th>		
					<th style="background-color:#eff0f0;" >01월</th>
					<th style="background-color:#eff0f0;" >02월</th>
					<th style="background-color:#eff0f0;" >03월</th>
					<th style="background-color:#eff0f0;" >04월</th>
					<th style="background-color:#eff0f0;" >05월</th>
					<th style="background-color:#eff0f0;" >06월</th>
					<th style="background-color:#eff0f0;" >07월</th>
					<th style="background-color:#eff0f0;" >08월</th>
					<th style="background-color:#eff0f0;" >09월</th>
					<th style="background-color:#eff0f0;" >10월</th>
					<th style="background-color:#eff0f0;" >11월</th>
					<th style="background-color:#eff0f0;" >12월</th>
					<th style="background-color:#eff0f0;" >합계</th>
				</tr>
			</thead>
		    <tbody>
			<c:choose> 		
					<c:when test="${mainList != null && not empty mainList}"> 	
						<c:set var = "SQUOT">'</c:set>
						<c:set var = "DQUOT">"</c:set>
						<c:forEach items="${mainList}" var="rows" varStatus="loop">
							<tr>		
								<td align="center">${rows.RNUM}</td>
								<td align="center">${fn:replace(fn:replace(fn:replace(fn:replace(rows.REPORT_NAME,SQUOT,"&#39;"),DQUOT,"&quot;"),"<","&lt;"),">","&gt;")}</td>
								<td align="center">${fn:replace(fn:replace(fn:replace(fn:replace(rows.REPORT_PATH,SQUOT,"&#39;"),DQUOT,"&quot;"),"<","&lt;"),">","&gt;")}</td>
								<td align="right">${rows.JAN}</td>
								<td align="right">${rows.FEB}</td>
								<td align="right">${rows.MAR}</td>
								<td align="right">${rows.APR}</td>
								<td align="right">${rows.MAY}</td>
								<td align="right">${rows.JUN}</td>
								<td align="right">${rows.JUL}</td>
								<td align="right">${rows.AUG}</td>
								<td align="right">${rows.SEP}</td>
								<td align="right">${rows.OCT}</td>
								<td align="right">${rows.NOV}</td>
								<td align="right">${rows.DEC}</td>
								<td align="right">${rows.TOTAL}</td>
							</tr>
						</c:forEach> 		
					</c:when> 		
					<c:otherwise> 				
						<tr> 					
							<td class="gtd"  colspan="6">조회된 데이터가 없습니다.</td> 			
						</tr> 		
					</c:otherwise> 
				</c:choose>  
			</tbody> 
		</table>
	</body>
</html>