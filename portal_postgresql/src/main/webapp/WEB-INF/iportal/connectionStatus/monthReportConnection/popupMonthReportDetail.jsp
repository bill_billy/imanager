<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>${title}</title>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		init();
	});
	function init(){
		$("#btnExcel").jqxButton({ width : '', theme : 'blueish' });
		search();
	}
	function search(){
		var year = "${param.pYear}";
		var targetID = "${param.pTargetID}";
		var gridData = $i.post("./getMonthReportConnectionDetail",{year:year, targetID:targetID});
		gridData.done(function(res){
			console.log(res.returnArray[0]);
			$("#labelTargetValue").html($i.secure.scriptToText(res.returnArray[0].TARGET_VALUE));
			$("#labelTargetPath").html($i.secure.scriptToText(res.returnArray[0].TARGET_PATH));
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: "EMP_ID", type:"string"},
					{ name: "EMP_NM", type:"string"},
					{ name: "USER_GRP_NM", type: "string"},
					{ name: "JAN", type: "number"},
					{ name: "FEB", type: "number"},
					{ name: "MAR", type: "number"},
					{ name: "APR", type: "number"},
					{ name: "MAY", type: "number"},
					{ name: "JUN", type: "number"},
					{ name: "JUL", type: "number"},
					{ name: "AUG", type: "number"},
					{ name: "SEP", type: "number"},
					{ name: "OCT", type: "number"},
					{ name: "NOV", type: "number"},
					{ name: "DEC", type: "number"},
					{ name: "TOTAL", type: "number"}
				],
				localdata: res.returnArray
			};
			var alginLeft = function (row, columnfield, value) {//left정렬
				var newValue=$i.secure.scriptToText(value);
				return '<div id="userName-' + row + '"style="text-align:left; margin:6px 0px 0px 4px;">' + newValue + '</div>';
			}; 
			var alginRight = function (row, columnfield, value) {//right정렬
				return '<div id="userName-' + row + '"style="text-align:right; margin:6px 4px 0px 0px;">' + value + '</div>';
			};
			var dataAdapter = new $.jqx.dataAdapter(source);
	
			$('#gridUnivList').jqxGrid({
				width: '100%',
				height: '100%',
				altrows:true,
				pageable: true,
				source: dataAdapter,
				theme:'blueish',
				columnsheight:28,
				rowsheight: 28,
				columnsresize: true,
				columns: [
					{ text: 'ID', datafield: 'EMP_ID', width: 130, align:'center', cellsrenderer:alginLeft},
					{ text: '이름', datafield: 'EMP_NM', width: 130, align:'center', cellsrenderer:alginLeft},
					{ text: '부서', datafield: 'USER_GRP_NM',width: 260, align:'center', cellsrenderer:alginLeft},
					{ text: '01월', datafield: 'JAN', width: 55, align:'center', cellsrenderer:alginRight},
					{ text: '02월', datafield: 'FEB', width: 55, align:'center', cellsrenderer:alginRight},
					{ text: '03월', datafield: 'MAR', width: 55, align:'center', cellsrenderer:alginRight},
					{ text: '04월', datafield: 'APR', width: 55, align:'center', cellsrenderer:alginRight},
					{ text: '05월', datafield: 'MAY', width: 55, align:'center', cellsrenderer:alginRight},
					{ text: '06월', datafield: 'JUN', width: 55, align:'center', cellsrenderer:alginRight},
					{ text: '07월', datafield: 'JUL', width: 55, align:'center', cellsrenderer:alginRight},
					{ text: '08월', datafield: 'AUG', width: 55, align:'center', cellsrenderer:alginRight},
					{ text: '09월', datafield: 'SEP', width: 55, align:'center', cellsrenderer:alginRight},
					{ text: '10월', datafield: 'OCT', width: 55, align:'center', cellsrenderer:alginRight},
					{ text: '11월', datafield: 'NOV', width: 55, align:'center', cellsrenderer:alginRight},
					{ text: '12월', datafield: 'DEC', width: 55, align:'center', cellsrenderer:alginRight},
					{ text: '합계', datafield: 'TOTAL', width: 55, align:'center', cellsrenderer:alginRight}
				]
			});
			
			//pager width
			$('#gridpagerlist' + $('#gridTableList').attr('id')).width('44px');
			$('#dropdownlistContentgridpagerlist' + $('#gridTableList').attr('id')).width('19px');
		});
	}
	function goExcel(){
		var year = "${param.pYear}";
		var targetID = "${param.pTargetID}";
		windowOpen('./popupMonthReportExcel?year='+year+"&targetID="+targetID, "_self");
	}
	</script>
</head>
<body class='blueish'>
	<div class="wrap" style="width:98%; margin:10px auto;">
		<div class="header f_left" style="width:99%; height:97% !important; margin:3px 5px 5px !important;">
			<div class="label type1 f_left">보고서 명: <span id="labelTargetValue"></span></div>
			<div class="label type1 f_left">경로 : <span id="labelTargetPath"></span></div>
			<div class="group_button f_right">
				<div class="button type1 f_left">
					<input type="button" value="Excel" id='btnExcel' width="100%" height="100%" onclick="goExcel();" />
				</div>
			</div>
		</div>
		<div class="container  f_left" style="width:100%; margin:10px 0;">
			<div class="content f_left" style="width:100%; margin-right:1%;">
				<div class="grid f_left" style="width:99%; height:450px; margin:3px 5px 5px !important;">
					<div id="gridUnivList"></div>
				</div>
			</div>
		</div>
	</div><!--wrap-->		
</body>
</html>
