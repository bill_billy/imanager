<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>사용자 관리</title>
       <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<!-- 	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script> -->
<!-- 	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script> -->
<!-- 	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script> -->
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
        <script>
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		$("#btnSearch").jqxButton({width:'', theme:'blueish'});
        		$("#dateStartDate").jqxInput({placeHolder: "", height: 23, width: 150, minLength: 1, theme:'blueish' });
        		$("#dateEndDate").jqxInput({placeHolder: "", height: 23, width:150, minLength: 1, theme:'blueish' });
        		$('#dateStartDate').datepicker({
					changeYear: true,
					showOn: 'button',
					buttonImage: '../../resources/cmresource/image/icon-calendar.png',
					buttonImageOnly: true,
					dateFormat: "yy-mm-dd", //20141212
				});
        		$('#dateEndDate').datepicker({
					changeYear: true,
					showOn: 'button',
					buttonImage: '../../resources/cmresource/image/icon-calendar.png',
					buttonImageOnly: true,
					dateFormat: "yy-mm-dd", //20141212
				});
        		$('.ui-datepicker-trigger').each(function() {
        			$(this).css("vertical-align", "middle");
					$(this).css("padding-top", "4px");
					$(this).css("margin-left", "4px");
					$(this).css("float", "left");
        		});

        		var date = new Date();
        		var year = date.getFullYear();
        		var month = date.getMonth()+1;
        		var day = date.getDate();
        		if((""+month).length == 1){
        			month = "0"+month;
        		}
        		if((""+day).length == 1){
        			day = "0"+day;
        		}
        		$("#dateStartDate").val(year + "-" + month + "-" + day);
        		$("#dateEndDate").val(year + "-" + month + "-" + day);
        		search();
        	}
        	//조회 
        	function search(){
        		var dayConnection = $i.post("./getDayConnectionUserList",{startDat:$("#dateStartDate").val(), endDat:$("#dateEndDate").val()});
        		dayConnection.done(function(dayData){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'EMP_ID', type: 'string' },
		                    { name: 'EMP_NM', type: 'string' },
		                    { name: 'USER_GRP_NM', type: 'string'},
							{ name: 'COUNT', type: 'number'}
		                ],
		                localdata: dayData.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var detailRow = function (row, columnfield, value, defaultHtml, property, rowdata) {//그리드 선택시 하단 상세
						var userID = rowdata.EMP_ID;
						var userName = rowdata.EMP_NM;
						var link = "<a href=\"javaScript:detailReport('"+userID+"','"+userName+"');\" style='color: #000000;text-decoration:underline;'>" + value.replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/'"'/g,'&quot;') + "</a>";
// 						var link = "<a href=\"javascript:getRowDetail('"+ifCod+"','"+ifCodNm+"','"+acctId+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridUserCountList").jqxGrid(
		            {
		            	width : '100%',
						height : '625px',
						altrows:true,
		                source: dataAdapter,
						theme:'blueish',
						sortable : true,
		                columnsresize: true,
		                pageable: true,
		                pagesize: 100,
						pagesizeoptions:['100', '200', '300'],
		                columns: [
		                   { text: 'ID', datafield: 'EMP_ID', width: 150, align:'center', cellsalign: 'center', cellsrenderer: detailRow},
		                   { text: '이름', datafield: 'EMP_NM', width: 150, align:'center', cellsalign: 'center', cellsrenderer: detailRow},
		                   { text: '부서', datafield: 'USER_GRP_NM', align:'center', cellsalign: 'center', cellsrenderer: alginLeft},
		                   { text: '접속횟수', datafield: 'COUNT', align:'center',width:150, cellsalign: 'center', cellsrenderer: alginRight}
		                ]
		            });
		            detailReport('', '');
        		});
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function detailReport(userID, userName){
        		$("#labelUserID").html(userID);
        		$("#labelUserName").html(userName);
        		var dayConnection = $i.post("./getDayConnectionReportList",{startDat:$("#dateStartDate").val(), endDat:$("#dateEndDate").val(), userID:userID});
        		dayConnection.done(function(dayData){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'INSERT_TIME', type: 'string' },
		                    { name: 'TARGET_VALUE', type: 'string' },
		                    { name: 'TARGET_PATH', type: 'string'}
		                ],
		                localdata: dayData.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var detailRow = function (row, columnfield, value, defaultHtml, property, rowdata) {//그리드 선택시 하단 상세
						var userID = rowdata.USER_ID;
						var unID = rowdata.UNID;
						var link = "<a href=\"javaScript:viewDetail('"+userID+"','"+unID+"');\" style='color: #000000;text-decoration:underline;'>" + value.replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/'"'/g,'&quot;') + "</a>";
// 						var link = "<a href=\"javascript:getRowDetail('"+ifCod+"','"+ifCodNm+"','"+acctId+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:center; margin:4px 0px 0px 0px;'>" + link + "</div>";
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridUserDetailList").jqxGrid(
		            {
		            	width : '100%',
						height : '590px',
						altrows:true,
		                source: dataAdapter,
						theme:'blueish',
						sortable : true,
		                columnsresize: true,
		                pageable: true,
		                pagesize: 100,
						pagesizeoptions:['100', '200', '300'],
		                columns: [
		                   { text: '시간', datafield: 'INSERT_TIME', width: 150, align:'center', cellsalign: 'center', cellsrenderer: alginLeft},
		                   { text: '보고서명', datafield: 'TARGET_VALUE', width: 300, align:'center', cellsalign: 'center', cellsrenderer: alginLeft},
		                   { text: '경로', datafield: 'TARGET_PATH', align:'center', cellsalign: 'center', cellsrenderer: alginLeft}
		                ]
		            });
        		});
        	}
        	function viewDetail(userID, unID){
        		location.replace("./detail?userID="+userID);
        	}
        	function newPage(){
        		location.replace("./detail");
        	}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
		</style>
		   
    </head>
    <body class='blueish'>
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98% ;min-width:1160px; margin:0 10px;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<form id="searchForm" name="searchForm" action="./list">
					<div class="label type1 f_left">시작일 : </div>
					<input type="text" class="f_left" style="margin-left:5px;" id="dateStartDate"/>
					<div class="label type1 f_left">종료일 : </div>
					<input type="text" class="f_left" style="margin-left:5px;" id="dateEndDate"/>
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
					</div>
				</form>
			</div>
			<div class="container  f_left" style="width:100%; height: 625px; margin-top:10px;/*margin-top:10px;*/">
				<div class="content f_left" style="width: 49%; height: 625px;float:left;">
					<div class="grid f_left" style="width:100%; height: 100%;">
						<div id="gridUserCountList"></div>
					</div>
				</div>
				<div class="content f_right" style="width: 49%;height: 590px;float:right;">
					<div style="width:100%; float:left; margin-bottom:10px;">
						<div class="label type2 f_left" style="padding-right:20px;">ID : <span id="labelUserID"></span></div>
						<div class="label type2 f_left m_l10" style="padding-left:10px;">이름 : <span id="labelUserName"></span></div>
					</div>
					<div class="grid f_left" style="width:100%; height: 100%;">
						<div id="gridUserDetailList"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>