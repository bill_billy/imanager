 <%@ page language="java" contentType="application/vnd.xls;charset=UTF-8" pageEncoding="utf-8"%>   -
<%@page import="java.net.URLEncoder"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%

	String header =request.getHeader("User-Agent");
	String browser="";  
	String docName = URLEncoder.encode("리포트 사용률 분석", "utf-8");  
	 
	System.out.println("header++++++++++++++++++++++++++++++"+header+":::::"+docName);
	 
	
    response.setHeader("Content-Disposition", "attachment; filename="+docName+".xls"); 
    response.setHeader("Content-Description", "JSP Generated Data");

%>   -
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>리포트 사용률 분석</title>
	
	</head>
 	<body>
		<table width="100%;" id="tableList" class="fix_rable" cellspacing="0" cellpadding="0" border="1">		
			<thead>			
				<tr> 			
					<th rowspan=2 style="background-color:#eff0f0;" >보고서ID</th>
					<th rowspan=2 style="background-color:#eff0f0;" >보고서명</th>
					<th rowspan=2 style="background-color:#eff0f0;" >경로</th>								
					<th rowspan=2 style="background-color:#eff0f0;" >최종사용일</th>
					<th colspan=5 style="background-color:#eff0f0;" >사용횟수</th>
				</tr>
				<tr> 				
					<th style="background-color:#eff0f0;" >3개월 합계</th>
					<th style="background-color:#eff0f0;" >6개월 합계</th>
					<th style="background-color:#eff0f0;" >9개월 합계</th>								
					<th style="background-color:#eff0f0;" >12개월 합계</th>
					<th style="background-color:#eff0f0;" >12개월 이전</th>
				</tr>
			</thead>
		    <tbody>
			<c:choose> 		
					<c:when test="${mainList != null && not empty mainList}"> 	
						<c:set var = "SQUOT">'</c:set>
						<c:set var = "DQUOT">"</c:set>
						<c:forEach items="${mainList}" var="rows" varStatus="loop">
							<tr>		
								<td align="center">${rows.C_ID}</td>
								<td align="center">${fn:replace(fn:replace(fn:replace(fn:replace(rows.REPORT_NAME,SQUOT,"&#39;"),DQUOT,"&quot;"),"<","&lt;"),">","&gt;")}</td>
								<td align="center">${fn:replace(fn:replace(fn:replace(fn:replace(rows.REPORT_PATH,SQUOT,"&#39;"),DQUOT,"&quot;"),"<","&lt;"),">","&gt;")}</td>
								<td align="right">${rows.LAST_USE_DAY}</td>
								<td align="right">${rows.FIRST_DATE}</td>
								<td align="right">${rows.SECOND_DATE}</td>
								<td align="right">${rows.THIRD_DATE}</td>
								<td align="right">${rows.FOURTH_DATE}</td>
								<td align="right">${rows.BEFORE_DATE}</td>
							</tr>
						</c:forEach> 		
					</c:when> 		
					<c:otherwise> 				
						<tr> 					
							<td class="gtd"  colspan="6">조회된 데이터가 없습니다.</td> 			
						</tr> 		
					</c:otherwise> 
				</c:choose>  
			</tbody> 
		</table>
	</body>
</html>