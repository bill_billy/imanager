 <%@ page language="java" contentType="application/vnd.xls;charset=UTF-8" pageEncoding="utf-8"%>   -
<%@page import="java.net.URLEncoder"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%

	String header =request.getHeader("User-Agent");
	String browser="";  
	String docName = URLEncoder.encode("사용자 로그인 분석", "utf-8");  
	 
	System.out.println("header++++++++++++++++++++++++++++++"+header+":::::"+docName);
	 
	
    response.setHeader("Content-Disposition", "attachment; filename="+docName+".xls"); 
    response.setHeader("Content-Description", "JSP Generated Data");

%>   -
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>사용자 로그인 분석</title>
	
	</head>
 	<body>
		<table width="100%;" id="tableList" class="fix_rable" cellspacing="0" cellpadding="0" border="1">		
			<thead>			
				<tr> 			
					<th style="background-color:#eff0f0;" >ID</th>
					<th style="background-color:#eff0f0;" >이름</th>		
					<th style="background-color:#eff0f0;" >부서</th>							
					<th style="background-color:#eff0f0;" >최종로그인</th>
					<th style="background-color:#eff0f0;" >3개월 합계</th>
					<th style="background-color:#eff0f0;" >3개월 주기</th>
					<th style="background-color:#eff0f0;" >6개월 합계</th>
					<th style="background-color:#eff0f0;" >6개월 주기</th>
					<th style="background-color:#eff0f0;" >9개월 합계</th>
					<th style="background-color:#eff0f0;" >9개월 주기</th>
					<th style="background-color:#eff0f0;" >12개월 합계</th>
					<th style="background-color:#eff0f0;" >12개월 주기</th>
				</tr>
			</thead>
		    <tbody>
			<c:choose> 		
					<c:when test="${mainList != null && not empty mainList}"> 
						<c:set var = "SQUOT">'</c:set>
						<c:set var = "DQUOT">"</c:set>	
						<c:forEach items="${mainList}" var="rows" varStatus="loop">
							<tr>		
								<td align="center">${rows.EMP_ID}</td>
								<td align="center">${fn:replace(fn:replace(fn:replace(fn:replace(rows.EMP_NM,SQUOT,"&#39;"),DQUOT,"&quot;"),"<","&lt;"),">","&gt;")}</td>
								<td align="center">${fn:replace(fn:replace(fn:replace(fn:replace(rows.USER_GRP_NM,SQUOT,"&#39;"),DQUOT,"&quot;"),"<","&lt;"),">","&gt;")}</td>
								<td align="right">${rows.LAST_USER_DAY}</td>
								<td align="right">${rows.FIRST_DATE}</td>
								<td align="right">${rows.FIRST_VALUE}</td>
								<td align="right">${rows.SECOND_DATE}</td>
								<td align="right">${rows.SECOND_VALUE}</td>
								<td align="right">${rows.THIRD_DATE}</td>
								<td align="right">${rows.THIRD_VALUE}</td>
								<td align="right">${rows.FOURTH_DATE}</td>
								<td align="right">${rows.FOURTH_VALUE}</td>
							</tr>
						</c:forEach> 		
					</c:when> 		
					<c:otherwise> 				
						<tr> 					
							<td class="gtd"  colspan="6">조회된 데이터가 없습니다.</td> 			
						</tr> 		
					</c:otherwise> 
				</c:choose>  
			</tbody> 
		</table>
	</body>
</html>