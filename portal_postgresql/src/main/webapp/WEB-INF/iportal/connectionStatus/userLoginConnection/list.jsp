<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>사용자 로그인 분석</title>
       <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <script>
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		$("#btnSearch").jqxButton({width:'', theme:'blueish'});
        		$("#btnExcel").jqxButton({width:'', theme:'blueish'});
        		makeCombobox();
        	}
        	//조회 
        	function search(){
        		$("#cboYear").val($("#cboYear2").val());
        		var dayUserConnection = $i.post("./getUserLoginConnectionList",{yyyymm:$("#cboYear").val()});
        		dayUserConnection.done(function(dayData){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'EMP_ID', type: 'string' },
		                    { name: 'EMP_NM', type: 'string' },
		                    { name: 'USER_GRP_NM', type: 'string'},
		                    { name: 'LAST_USER_DAY', type: 'string'},
		                    { name: 'FIRST_DATE', type: 'number'},
		                    { name: 'FIRST_VALUE', type: 'number'},
		                    { name: 'SECOND_DATE', type: 'number'},
		                    { name: 'SECOND_VALUE', type: 'number'},
		                    { name: 'THIRD_DATE', type: 'number'},
		                    { name: 'THIRD_VALUE', type: 'number'},
		                    { name: 'FOURTH_DATE', type: 'number'},
		                    { name: 'FOURTH_VALUE', type: 'number'}
		                ],
		                localdata: dayData.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
        			var detailView = function (row, columnfield, value, defaultHtml, property, rowdata) {
        				var empID = rowdata.EMP_ID;
						var link = "<a href=\"javaScript:openWindow('"+empID+"')\" style='color:black;text-decoration:underline;'>" + value + "</a>";
						return '<div style="text-align:left; margin:4px 10px 0px 10px;">' + link + '</div>';
					};
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						var newValue = $i.secure.scriptToText(value);
						return '<div id="userName-' + row + '"style="text-align:left; margin:6px 0px 0px 10px;">' + newValue + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridMonthUserList").jqxGrid(
		            {
		            	width: '100%',
						height: '625px',
						columnsheight:30,
						rowsheight: 30,
						altrows:true,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                pageable: true,
		                sortable : true,
						pagesizeoptions : [ '100', '200', '300' ],
						pagesize: 100,
						pagermode: 'default',
		                columns: [
							{ text: 'ID', datafield:'EMP_ID', align:'center', width:130, cellsrenderer:alginLeft},
							{ text: '이름', datafield:'EMP_NM', align:'center', width:130, cellsrenderer:alginLeft},
							{ text: '부서', datafield:'USER_GRP_NM', align:'center', cellsrenderer:alginLeft},
		                    { text: '최종로그인' , datafield:'LAST_USER_DAY', align:'center', cellsalign:'center', width: 160},
						 	{ text: '3개월' , datafield:'FIRST_DATE', align:'center', cellsalign:'right', width: 81, columngroup:"A"}, 
						 	{ text: '3개월 주기' , datafield:'FIRST_VALUE', align:'center', cellsalign:'right', width: 81 , columngroup:"A"},
						 	{ text: '6개월' , datafield:'SECOND_DATE', align:'center', cellsalign:'right', width: 81, columngroup:"B"},
						 	{ text: '6개월 주기' , datafield:'SECOND_VALUE', align:'center', cellsalign:'right', width: 81, columngroup:"B"},
						 	{ text: '9개월' , datafield:'THIRD_DATE', align:'center', cellsalign:'right', width: 81, columngroup:"C"},
						 	{ text: '9개월 주기' , datafield:'THIRD_VALUE', align:'center', cellsalign:'right', width: 81, columngroup:"C"},
						 	{ text: '12개월' , datafield:'FOURTH_DATE', align:'center', cellsalign:'right', width: 81, columngroup:"D"},
						 	{ text: '12개월 주기' , datafield:'FOURTH_VALUE', align:'center', cellsalign:'right', width: 81, columngroup:"D"}
		                ],
		               	columngroups:[
							{ text : '3개월 평균', align:'center', name:'A'},
							{ text : '6개월 평균', align:'center', name:'B'},
							{ text : '9개월 평균', align:'center', name:'C'},
							{ text : '12개월 평균', align:'center',name:'D'}
		                ]
		            });
        		});
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function openWindow(targetID){
				var url = "./popupMonthUserDetail?pYear="+$("#cboYear").val()+"&pTargetID="+targetID;
				
				var nWin = window.open(url,"_blank","left=0,top=0,width=1040,height=525,toolbar=no,scrollbars=0,status=yes,resizable=yes");
				nWin.focus();
			}
        	function makeCombobox(){
        		var yearData = $i.post("./getUserLoginConnectionYyyymm", {});
        		yearData.done(function(data){
        			 var source =
		            {
		                datatype: "json",
		                datafields: [
		                     { name: 'COM_COD' },
		                     { name: 'COM_NM' }
		                ],
		                id: 'id',
						localdata:data.returnArray,
		                async: false
		            };
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#cboYear2").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 120, dropDownHeight: 150, width: 120, height: 22,theme:'blueish'});
		            search();
        		});
        	}
        	function excelDown(){
        		windowOpen('./excelDownByUserLoginConnection?yyyymm='+$("#cboYear").val(), "_self");
        	}
        	function windowOpen(url, windowName, width, height, location, scrollbars){
				var screenWidth=screen.width;
				var screenHeight=screen.height;
			
				var x=(screenWidth/2)-(width/2);
				var y=(screenHeight/2)-(height/2);
				//2013-12-18장민수 수정
				//새창 객체를 리턴하도록.
				return window.open(url, windowName,"width=" + width + ", height=" + height + ",  location=" + location + ", toolbar=no, menubar=no, directories=no, scrollbars=" + scrollbars + ", resizable=no, left="+ x + ", top=" + y);
			}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
		</style>
		   
    </head>
    <body class='blueish'>
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%;min-width:1160px; margin:0 10px;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0;">
				<form id="searchForm" name="searchForm" action="./list">
					<div class="label type1 f_left">년월 :</div>
					<div class="combobox f_left" id="cboYear2"></div>
					<input type="hidden" id="cboYear"/>
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="Excel" id='btnExcel' width="100%" height="100%" onclick="excelDown();" />
						</div>
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
					</div>
				</form>
			</div>
			<div class="container  f_left" style="width:100%; margin-top:10px; height: 600px;">
				<div class="content f_left" style="width:100%; margin:0 0%;">
					<div class="grid f_left" style="width:100%; height:100%;">
						<div id="gridMonthUserList"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>