<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>로그인 정보</title>
       <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <script>
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		$("#btnSearch").jqxButton({width:'', theme:'blueish'});
        		$("#btnExcel").jqxButton({width:'', theme:'blueish'});
        		$("#dateStartDate2").jqxInput({placeHolder: "", height: 23, width: 150, minLength: 1, theme:'blueish' });
        		$('#dateStartDate2').datepicker({
					changeYear: true,
					showOn: 'button',
					buttonImage: '../../resources/cmresource/image/icon-calendar.png',
					buttonImageOnly: true,
					dateFormat: "yy-mm", //20141212
				});
        		$('.ui-datepicker-trigger').each(function(){
					$(this).css("vertical-align", "middle");
					$(this).css("padding-top", "4px");
					$(this).css("margin-left", "4px");
					$(this).css("float", "left");
				});
        		
        		var date = new Date();
				var yyyy = date.getFullYear();
				var mm = date.getMonth() + 1;
				if((""+mm).length == 1){
					mm = "0"+mm;
        		}
				if($("#dateStartDate2").val() == ""){
					$("#dateStartDate2").val( yyyy +'-'+ mm);
				}

				
        		makeCombobox();
        	}
        	//조회 
        	function search(){
        		$("#cboGubn").val($("#cboGubn2").val());
        		$("#dateStartDate").val($("#dateStartDate2").val());
        		var dayUserConnection = $i.post("./getUserLoginInfoList",{yyyymm:$("#dateStartDate").val(), gubn:$("#cboGubn").val()});
        		dayUserConnection.done(function(dayData){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'USER_ID', type: 'string' },
		                    { name: 'USER_NM', type: 'string'},
		                    { name: 'LOG_INFO', type: 'string' },
		                    { name: 'TIME', type: 'string'},
		                    { name: 'REMOTE_IP', type: 'string'},
		                    { name: 'REMOTE_OS', type: 'string'},
		                    { name: 'REMOTE_BROWSER', type: 'string'},
		                    { name: 'TARGET_USER', type: 'string'},
		                    { name: 'TARGET_NM', type: 'string'}
		                ],
		                localdata: dayData.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
		            	var newValue = $i.secure.scriptToText(value);
						return '<div id="userName-' + row + '"style="text-align:center; margin:6px 0px 0px 0px;">' + newValue + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridUserLoginInfoList").jqxGrid(
		            {
		            	width: '100%',
						height: '625px',
						altrows:true,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                sortable : true,
		                pageable: true,
						pagesizeoptions: ['100', '200', '300'],
						pagesize:100,
						pagermode: 'default',
		                columns: [
							{ text: '접속ID', datafield:'USER_ID', align:'center', cellsalign:'center', width:'11%',cellsrenderer:alginCenter},
							{ text: '이름', datafield:'USER_NM', align:'center', cellsalign:'center', width:'12%',cellsrenderer:alginCenter},
							{ text: '유형', datafield:'LOG_INFO', align:'center', cellsalign:'center', width:'12%'},
		                    { text: '일시' , datafield:'TIME', align:'center', cellsalign:'center', width: '15%'},
						 	{ text: 'IP' , datafield:'REMOTE_IP', align:'center', cellsalign:'center', width: '10%'}, 
						 	{ text: 'OS' , datafield:'REMOTE_OS', align:'center', cellsalign:'center', width: '15%'},
						 	{ text: 'BROWSER' , datafield:'REMOTE_BROWSER', align:'center', cellsalign:'center', width: '15%'},
						 	{ text: '대상유저' , datafield:'TARGET_USER', align:'center', cellsalign:'center'}
		                ],
		            });
        		});
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function makeCombobox(){
        		var yearData = $i.post("./getUserLoinInfoGubn", {});
        		yearData.done(function(data){
        			 var source =
		            {
		                datatype: "json",
		                datafields: [
		                     { name: 'COM_COD' },
		                     { name: 'COM_NM' }
		                ],
		                id: 'id',
						localdata:data.returnArray,
		                async: false
		            };
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#cboGubn2").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 150, dropDownHeight: 80, width: 150, height: 22,theme:'blueish'});
		            search();
        		});
        	}
        	function excelDown(){
        		windowOpen('./excelDownByUserLoginInfo?yyyymm='+$("#dateStartDate").val()+"&gubn="+$("#cboGubn").val(), "_self");
        	}
        	function windowOpen(url, windowName, width, height, location, scrollbars){
				var screenWidth=screen.width;
				var screenHeight=screen.height;
			
				var x=(screenWidth/2)-(width/2);
				var y=(screenHeight/2)-(height/2);
				//2013-12-18장민수 수정
				//새창 객체를 리턴하도록.
				return window.open(url, windowName,"width=" + width + ", height=" + height + ",  location=" + location + ", toolbar=no, menubar=no, directories=no, scrollbars=" + scrollbars + ", resizable=no, left="+ x + ", top=" + y);
			}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
		</style>
		   
    </head>
    <body class='blueish'>
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%;min-width:1160px; margin:0 10px;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0;">
				<form id="searchForm" name="searchForm" action="./list">
					<div class="label type1 f_left">시작일 : </div>
					<input type="text" class="f_left" id="dateStartDate2" style="margin-left:5px;"/>
					<div class="label type1 f_left">유형 : </div>
					<div class="combobox f_left" id="cboGubn2"></div>
					<input type="hidden" id="dateStartDate"/>
					<input type="hidden" id="cboGubn"/>
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
						<div class="button type1 f_left">
							<input type="button" value="ExcelDown" id='btnExcel' width="100%" height="100%" onclick="excelDown();" />
						</div>
					</div>
				</form>
			</div>
			<div class="container  f_left" style="width:100%;margin-top:10px; z-index:0; height: 600px;">
				<div class="content f_left" style="width:100%; margin:0 0%;">
					<div class="grid f_left" style="width:100%; height:600px;">
						<div id="gridUserLoginInfoList"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>