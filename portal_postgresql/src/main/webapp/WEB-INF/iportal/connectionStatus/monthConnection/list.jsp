<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>월별 접속 추이</title>
       <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <script>
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		$("#btnSearch").jqxButton({width:'', theme:'blueish'});
        		$("#btnExcel").jqxButton({width:'', theme:'blueish'});
        		makeCombobox();
        	}
        	//조회 
        	function search(){
        		$("#cboYear").val($("#cboYear2").val());
        		var dayConnection = $i.post("./getMonthUserConnectionTotal",{year:$("#cboYear").val()});
        		dayConnection.done(function(dayData){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'JAN', type: 'number' },
		                    { name: 'FEB', type: 'number' },
		                    { name: 'MAR', type: 'number'},
		                    { name: 'APR', type: 'number'},
		                    { name: 'MAY', type: 'number'},
		                    { name: 'JUN', type: 'number'},
		                    { name: 'JUL', type: 'number'},
		                    { name: 'AUG', type: 'number'},
		                    { name: 'SEP', type: 'number'},
		                    { name: 'OCT', type: 'number'},
		                    { name: 'NOV', type: 'number'},
		                    { name: 'DEC', type: 'number'},
							{ name: 'TOTAL', type: 'number'}
		                ],
		                localdata: dayData.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridMonthTotalList").jqxGrid(
		            {
		            	width: '100%',
						height: '51px',
						altrows:true,
		                source: dataAdapter,
						theme:'blueish',
						columnsheight:25,
						rowsheight: 25,
		                columnsresize: false,
		                pageable: false,
		                columns: [
		                   { text: '1월' , datafield:'JAN', align:'center', cellsalign:'right', width: 125},
						 	{ text: '2월' , datafield:'FEB', align:'center', cellsalign:'right', width: 125}, 
						 	{ text: '3월' , datafield:'MAR', align:'center', cellsalign:'right', width: 125},
						 	{ text: '4월' , datafield:'APR', align:'center', cellsalign:'right', width: 125},
						 	{ text: '5월' , datafield:'MAY', align:'center', cellsalign:'right', width: 125},
						 	{ text: '6월' , datafield:'JUN', align:'center', cellsalign:'right', width: 125},
						 	{ text: '7월' , datafield:'JUL', align:'center', cellsalign:'right', width: 125},
						 	{ text: '8월' , datafield:'AUG', align:'center', cellsalign:'right', width: 125},
						 	{ text: '9월' , datafield:'SEP', align:'center', cellsalign:'right', width: 125},
						 	{ text: '10월' , datafield:'OCT', align:'center', cellsalign:'right', width: 125},
						 	{ text: '11월' , datafield:'NOV', align:'center', cellsalign:'right', width: 125},
						 	{ text: '12월' , datafield:'DEC', align:'center', cellsalign:'right', width: 125},
						 	{ text: '합계' , datafield:'TOTAL', align:'center', cellsalign:'right'}
		                ]
		            });
        		});
        		var dayUserConnection = $i.post("./getMonthUserConnectionList",{year:$("#cboYear").val()});
        		dayUserConnection.done(function(dayData){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'JAN', type: 'number' },
		                    { name: 'FEB', type: 'number' },
		                    { name: 'MAR', type: 'number'},
		                    { name: 'APR', type: 'number'},
		                    { name: 'MAY', type: 'number'},
		                    { name: 'JUN', type: 'number'},
		                    { name: 'JUL', type: 'number'},
		                    { name: 'AUG', type: 'number'},
		                    { name: 'SEP', type: 'number'},
		                    { name: 'OCT', type: 'number'},
		                    { name: 'NOV', type: 'number'},
		                    { name: 'DEC', type: 'number'},
							{ name: 'TOTAL', type: 'number'},
							{ name: 'EMP_ID', type: 'string' },
							{ name: 'EMP_NM', type: 'string' },
							{ name: 'USER_GRP_NM', type: 'string' }
		                ],
		                localdata: dayData.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
		            	var newValue = $i.secure.scriptToText(value);
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						var newValue = $i.secure.scriptToText(value);
						return '<div id="userName-' + row + '"style="text-align:left; margin:6px 0px 0px 10px;">' + newValue + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridMonthUserList").jqxGrid(
		            {
		            	width: '100%',
						height: '560px',  
						altrows:true,
		                source: dataAdapter,
						theme:'blueish',
						sortable : true,
						pageable : true,
						pagesizeoptions : [ '100', '200', '300' ],
						pagesize : 100,
		                columnsresize: true,
		                columnsheight:30,
						rowsheight: 30,
		                columns: [
							{ text: 'ID', datafield:'EMP_ID', align:'center', width:130,cellsrenderer:alginLeft},
							{ text: '이름', datafield:'EMP_NM', align:'center', width:130,cellsrenderer:alginLeft},
							{ text: '부서', datafield:'USER_GRP_NM', align:'center',cellsrenderer:alginLeft},
		                    { text: '1월' , datafield:'JAN', align:'center', cellsalign:'right', width: 62},
						 	{ text: '2월' , datafield:'FEB', align:'center', cellsalign:'right', width: 62}, 
						 	{ text: '3월' , datafield:'MAR', align:'center', cellsalign:'right', width: 62},
						 	{ text: '4월' , datafield:'APR', align:'center', cellsalign:'right', width: 62},
						 	{ text: '5월' , datafield:'MAY', align:'center', cellsalign:'right', width: 62},
						 	{ text: '6월' , datafield:'JUN', align:'center', cellsalign:'right', width: 62},
						 	{ text: '7월' , datafield:'JUL', align:'center', cellsalign:'right', width: 62},
						 	{ text: '8월' , datafield:'AUG', align:'center', cellsalign:'right', width: 62},
						 	{ text: '9월' , datafield:'SEP', align:'center', cellsalign:'right', width: 62},
						 	{ text: '10월' , datafield:'OCT', align:'center', cellsalign:'right', width: 62},
						 	{ text: '11월' , datafield:'NOV', align:'center', cellsalign:'right', width: 62},
						 	{ text: '12월' , datafield:'DEC', align:'center', cellsalign:'right', width: 62},
						 	{ text: '합계' , datafield:'TOTAL', align:'center', cellsalign:'right',width: 80}
		                ]
		            });
        		});
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function makeCombobox(){
        		var yearData = $i.post("./getMonthUserYear", {});
        		yearData.done(function(data){
        			 var source =
		            {
		                datatype: "json",
		                datafields: [
		                     { name: 'COM_COD' },
		                     { name: 'COM_NM' }
		                ],
		                id: 'id',
						localdata:data.returnArray,
		                async: false
		            };
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#cboYear2").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 120, dropDownHeight: 150, width: 120, height: 22,theme:'blueish'});
		  			search();
        		});
        	}
        	function excelDown(){
        		windowOpen('./excelDownByMonth?year='+$("#cboYear").val(), "_self");
        	}
        	function windowOpen(url, windowName, width, height, location, scrollbars){
				var screenWidth=screen.width;
				var screenHeight=screen.height;
			
				var x=(screenWidth/2)-(width/2);
				var y=(screenHeight/2)-(height/2);
				//2013-12-18장민수 수정
				//새창 객체를 리턴하도록.
				return window.open(url, windowName,"width=" + width + ", height=" + height + ",  location=" + location + ", toolbar=no, menubar=no, directories=no, scrollbars=" + scrollbars + ", resizable=no, left="+ x + ", top=" + y);
			}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
		</style>
		   
    </head>
    <body class='blueish'>
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98% ;min-width:1680px !important;margin:0 10px ;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0;">
				<form id="searchForm" name="searchForm" action="./list">
					<div class="label type1 f_left">년 :</div>
					<div class="combobox f_left" id="cboYear2"></div>
					<input type="hidden" id="cboYear" />
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="Excel" id='btnExcel' width="100%" height="100%" onclick="excelDown();" />
						</div>
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
					</div>
				</form>
			</div>
			<div class="container  f_left" style="width:100%;height:625px; margin-top:10px;">
				<div class="content f_left" style="width:100%; margin:0 0%;">
					<div class="grid f_left" style="width:100%;margin-bottom:10px;">
						<div id="gridMonthTotalList"></div>
					</div>
					<div class="grid f_left" style="width:100%;height: 100%;">
						<div id="gridMonthUserList"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>