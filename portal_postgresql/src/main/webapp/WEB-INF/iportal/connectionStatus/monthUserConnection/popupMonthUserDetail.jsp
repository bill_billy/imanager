<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>${title}</title>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
		init();
	});
	function init(){
		$("#btnExcel").jqxButton({ width : '', theme : 'blueish' });
		search();
	}
	function search(){
		var year = "${param.pYear}";
		var targetID = "${param.pTargetID}";
		var targetNM = "${param.pTargetNM}";
		$("#documentid").html(targetID);
		$("#documentnm").html(targetNM);
		//$("#documentgrp").html();
		var gridData = $i.post("./getMonthUserConnectionPopupList",{year:year, userID:targetID});
		gridData.done(function(res){
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: "C_ID", type:"string"},
					{ name: "REPORT_NAME", type:"string"},
					{ name: "REPORT_PATH", type: "string"},
					{ name: "JAN", type: "number"},
					{ name: "FEB", type: "number"},
					{ name: "MAR", type: "number"},
					{ name: "APR", type: "number"},
					{ name: "MAY", type: "number"},
					{ name: "JUN", type: "number"},
					{ name: "JUL", type: "number"},
					{ name: "AUG", type: "number"},
					{ name: "SEP", type: "number"},
					{ name: "OCT", type: "number"},
					{ name: "NOV", type: "number"},
					{ name: "DEC", type: "number"},
					{ name: "TOTAL", type: "number"}
				],
				localdata: res.returnArray
			};
			var alginLeft = function (row, columnfield, value) {//left정렬
				var newValue = $i.secure.scriptToText(value);
				return '<div id="userName-' + row + '"style="text-align:left; margin:6px 0px 0px 10px;">' + newValue + '</div>';
			};
			var alginRight = function (row, columnfield, value) {//right정렬
				return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
			};
			var dataAdapter = new $.jqx.dataAdapter(source);
	
			$('#gridUnivList').jqxGrid({
				width: '100%',
				height: '100%',
				altrows:true,
				pageable: true,
				source: dataAdapter,
				theme:'blueish',
				columnsheight:28,
				rowsheight: 28,
				pagesizeoptions : [ '100', '200', '300' ],
				pagesize: 100,
				columnsresize: true,
				columns: [
					{ text: '보고서명', datafield: 'REPORT_NAME', width: 140, align:'center', cellsrenderer:alginLeft},
					{ text: '보고서경로', datafield: 'REPORT_PATH', align:'center', cellsrenderer:alginLeft},
					{ text: '01월', datafield: 'JAN', width: 54, align:'center', cellsalign: 'right'},
					{ text: '02월', datafield: 'FEB', width: 54, align:'center', cellsalign: 'right'},
					{ text: '03월', datafield: 'MAR', width: 54, align:'center', cellsalign: 'right'},
					{ text: '04월', datafield: 'APR', width: 54, align:'center', cellsalign: 'right'},
					{ text: '05월', datafield: 'MAY', width: 54, align:'center', cellsalign: 'right'},
					{ text: '06월', datafield: 'JUN', width: 54, align:'center', cellsalign: 'right'},
					{ text: '07월', datafield: 'JUL', width: 54, align:'center', cellsalign: 'right'},
					{ text: '08월', datafield: 'AUG', width: 54, align:'center', cellsalign: 'right'},
					{ text: '09월', datafield: 'SEP', width: 54, align:'center', cellsalign: 'right'},
					{ text: '10월', datafield: 'OCT', width: 54, align:'center', cellsalign: 'right'},
					{ text: '11월', datafield: 'NOV', width: 54, align:'center', cellsalign: 'right'},
					{ text: '12월', datafield: 'DEC', width: 54, align:'center', cellsalign: 'right'},
					{ text: '합계', datafield: 'TOTAL', width: 54, align:'center', cellsalign: 'right'}
				]
			});
			
			//pager width
			$('#gridpagerlist' + $('#gridTableList').attr('id')).width('44px');
			$('#dropdownlistContentgridpagerlist' + $('#gridTableList').attr('id')).width('19px');
		});
	}
	function excelAllDown(){
		var year = "${param.pYear}";
		var targetID = "${param.pTargetID}";
     	windowOpen('./excelDownByUserReport?year='+year+"&userID="+targetID, "_self");
    }
    function windowOpen(url, windowName, width, height, location, scrollbars){
		var screenWidth=screen.width;
		var screenHeight=screen.height;
	
		var x=(screenWidth/2)-(width/2);
		var y=(screenHeight/2)-(height/2);
		//2013-12-18장민수 수정
		//새창 객체를 리턴하도록.
		return window.open(url, windowName,"width=" + width + ", height=" + height + ",  location=" + location + ", toolbar=no, menubar=no, directories=no, scrollbars=" + scrollbars + ", resizable=no, left="+ x + ", top=" + y);
	 }
	</script>
</head>
<body class='blueish'>
	<div class="wrap" style="width:98%; margin:0 auto;">
		<div class="header f_left" style="width:99%; height:97% !important; margin:10px 5px 5px !important;">
			<div class="label type1 f_left">ID：<span id='documentid'></span></div>
		 	<div class="label type1 f_left" style="margin-left: 20px;">이름：<span id='documentnm'></span></div>
		<!--	<div class="label type1 f_left" style="margin-left: 20px;">부서：<span id='documentgrp'></span></div>
			-->
			<div class="group_button f_right">
				<div class="button type1 f_left">
					<input type="button" value="Excel" id='btnExcel' width="100%" height="100%" onclick="excelAllDown();" />
				</div>
			</div>
		</div>
		<div class="container  f_left" style="width:100%; margin:10px 0;">
			<div class="content f_left" style="width:100%; margin-right:1%;">
				<div class="grid f_left" style="width:99%; height:450px; margin:3px 5px 5px !important;">
					<div id="gridUnivList"></div>
				</div>
			</div>
		</div>
	</div><!--wrap-->		
</body>
</html>
