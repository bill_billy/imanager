<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title id='Description'>(4월,10월) 통계 마감 작업</title>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
 
 
<script type="text/javascript">//combobox
	var statCodeList = null;
	var stack = {};
	$(document).ready(function(){
		printEmptyLog();
		$("#btnSearch").jqxButton({ width: '',  theme:'blueish'}).on("click",search);  
		//Year Combo 
		var dat = $i.post("../../ExcelPgmDQ/getCommonCode", {comlcd:"EXCEL_YEAR"});
		dat.done(function(res){
			var pSource =
			{
				localdata: res,
	    		dataType: 'json',
				dataFields: [
					{ name: 'COM_COD'},
					{ name: 'COM_NM'}
				]
			};
			
			var pAdapter = new $.jqx.dataAdapter(pSource);
			
			$("#cboYear").jqxComboBox({ selectedIndex: 0,  source: pAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 80, dropDownHeight: 100, width: 80, height: 22,  theme:'blueish'});
			$("#cboYear").jqxComboBox({ selectedIndex: 0});
			var data = $i.post("../../ExcelPgmDQ/getCommonCode", {comlcd:"CLOSEEDU_STATUS"});
			data.done(function(res){
				statCodeList = res.returnArray;			 
				search();
			});  
		});  
		
		$("#saveForm").ajaxForm({
                    beforeSubmit: function (data,form,option) {
                    	//todo
                    },
                    success: function(response,status){
                    	 //todo
						 $i.dialog.alert("SYSTEM","저장 되었습니다.");
						 search();
						 searchLog();
                    },
                    error: function(e){
        				//todo
						$i.dialog.error("SYSTEM","저장 중 오류가 발생 했습니다."+e);
                    }                               
         });
	});
	function searchLog(yyyy, mm){ 
		if(yyyy==undefined)
			yyyy = stack.YYYY;
			
		if(mm==undefined)
			mm = stack.MM;		
		
		
		var dat = $i.post("../../excelPgm/stepCloseEduStats/getStepHistoryForCloseEduStat",{yyyy:yyyy, mm:mm});
		dat.done(function(result){
					var logtbl = $("#logList");
					logtbl.empty();
					var res = result.returnArray;
					for(var i = 0; i < res.length; i++){
						var log = res[i];
						var appendLog = "";
						
							appendLog+=			'<tr>'+
												'	<td style="width:10%;"><div class="cell t_center">'+i+'</div></td>'+
												'	<td style="width:10%;"><div class="cell t_center">'+log.YYYY+'</div></td>'+
												'	<td style="width:10%;"><div class="cell t_center">'+log.MM_NM+'</div></td>'+
												'	<td style="width:30%;"><div class="cell t_center">'+log.TXT+'</div></td>'+
												'	<td style="width:15%;"><div class="cell t_center">'+log.EMP_NM+'</div></td>'+
												'	<td style="width:22%;"><div class="cell t_center">'+log.UPDATE_DAT+'</div></td>'+
												'</tr>';
							logtbl.append(appendLog);												
					}
					if(res.length==0){
						printEmptyLog();
					}
					
			}).fail(function(msg){
				
			});		
	}
	function printEmptyLog(){
		var logtbl = $("#logList"); 
		logtbl.empty();
		logtbl.append("<tr><td colspan = '6' align='center' style='height:274px;'> 조회된 이력이 없습니다. </td></tr>");
	}
	function search(){
		var year = $("#cboYear").val();
		var dat = $i.post("../../excelPgm/stepCloseEduStats/getStepListForCloseEduStat", {yyyy:year});
		dat.done(function(result){
				var tbl = $("#mainList");
				tbl.empty();
				var res = result.returnArray;
				for(var i = 0; i<res.length;i++){
					var node = res[i];
					var appendHtml = "";
						appendHtml+='<tr data-mm="'+node.MM+'"data-yyyy="'+node.YYYY+'">'+
									'	<td style="width:10%;" ><div class="cell t_center">'+node.MM_NM+'</div></td>'+
									'	<td style="width:40%; "><div class="cell t_center">';
						for(var j = 0; j<statCodeList.length;j++){
							appendHtml+='		<label for="">'+
									'				<input type="radio" name="status_'+node.MM+'" value="'+statCodeList[j].COM_COD+'" class="m_b2 m_r3" '+(statCodeList[j].COM_COD==node.STATUS?"checked":"")+'>'+statCodeList[j].COM_NM+'</label>';
						}						
						
						appendHtml+='		</div></td>'+
									'	<td style="width:15%; "><div class="cell t_center">'+
									'			<div class="button type2" style="margin:3px 0;">'+
									'				<input type="button" data-role="btnStatusSave" value="저장" width="100%" height="100%" style="padding:2px 10px 3px 10px !important;" />'+
									'			</div>'+
									'		</div></td>'+
									'	<td style="width:17%; "><div class="cell t_center">'+(node.UPDATE_DAT==undefined?"":node.UPDATE_DAT)+'</div></td>'+
									'	<td style="width:15%; "><div class="cell t_center t_underline pointer">'+
									'			<a href="javascript:searchLog(\''+node.YYYY+'\',\''+node.MM+'\');">보기</a>'+
									'		</div></td>'+
									'</tr> ';
					$(appendHtml).appendTo(tbl);
				}
				//tbl.find("[data-role=btnStatusSave]").jqxButton({ width: '',  theme:'blueish'});
				tbl.find("[data-role=btnStatusSave]").on("click", function(event){
					var tr = $(this).parents("tr");
					var yyyy = tr.data("yyyy");
					var mm = tr.data("mm");
					var status = tr.find("[name=status_"+mm+"]:checked").val();
					if(status==undefined) {
						alert("진행 상태를 선택 해 주세요");return ;
					}
					var form = $("#saveForm");
					form.find("[name=yyyy]").val(yyyy);
					form.find("[name=mm]").val(mm);
					form.find("[name=status]").val(status);
					stack.YYYY = yyyy;
					stack.MM = mm;
					form.submit();
					
				});
				printEmptyLog();
			})
			.fail(function(msg){
				alert(msg);
			});
	}
</script>
</head>
<body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
<div class="wrap" style="width:98%; min-width:1067px; margin:0 1%;">
	<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
		<div class="label type1 f_left">
			학년도:
		</div>
		<div class="combobox f_left"  id='cboYear' >
		</div>
		<div class="group_button f_right">
			<div class="button type1 f_left">
				<input type="button" value="조회" id='btnSearch' width="100%" height="100%" />
			</div>
		</div>
		<!--group_button-->
	</div>
	<!--//header-->
	<div class="container  f_left" style="width:100%; margin:10px 0;">
		<div class="content f_left" style=" width:100%; margin:0%;">
			<div class="group f_left  w100p m_b5">
				<div class="label type2 f_left">
					진행현황
				</div>
			</div>
			<!--group-->
			<div class="blueish datatable f_left" style="width:100%; height:170px; margin:0px 0; overflow:hidden;">
				<div class="datatable_fixed" style="width:100%; height:170px; float:left;">
					<div style=" height:145px !important; overflow-x:hidden;">
						<!--datatable과 datatable_fixed 높이 - 헤더높이(기본25px) (ex: 600px - 25px= 550px)-->
						<table summary="진행현황" style="width:100%;"  class="none_hover">
							<thead style="width:100%;">
								<tr>
									<th scope="col" style="width:10%;">월</th>
									<th scope="col" style="width:40%;">진행상태</th>
									<th scope="col" style="width:15%;">저장</th>
									<th scope="col" style="width:17%;">저장일자</th>
									<th scope="col" style="width:15%;">이력</th>
									<th style="width:1%; min-width:17px;"></th>
								</tr>
							</thead>
							<tbody id="mainList">
								
							</tbody>
						</table>
					</div>
				</div>
				<form id='saveForm' method="post" action="../../excelPgm/stepCloseEduStats/stepCloseEduStatsSave">
					<input type='hidden' name='yyyy'/>
					<input type='hidden' name='mm'/>
					<input type='hidden' name='status'/>
				</form>
				<!--datatable_fixed-->
			</div>
			<!--datatable -->
		</div>
		<!--//content-->
		<div class="content f_left" style=" width:100%; margin:10px 0%;">
			<div class="group f_left  w100p m_b5">
				<div class="label type2 f_left">
					이력보기
				</div>
			</div>
			<!--group-->
			<div class="blueish datatable f_left" style="width:100%; height:300px; margin:0px 0; overflow:hidden;">
				<div class="datatable_fixed" style="width:100%; height:300px; float:left;">
					<div style=" height:275px !important; overflow-x:hidden;">
						<!--datatable과 datatable_fixed 높이 - 헤더높이(기본25px) (ex: 600px - 25px= 550px)-->
						<table summary="이력보기" style="width:100%;"  class="none_hover">
							<thead style="width:100%;">
								<tr>
									<th scope="col" style="width:10%;">번호</th>
									<th scope="col" style="width:10%;">학년도</th>
									<th scope="col" style="width:10%;">월</th>
									<th scope="col" style="width:30%;">단계</th>
									<th scope="col" style="width:15%;">작업자</th>
									<th scope="col" style="width:22%;">작업일시</th>
									<th style="width:1%; min-width:17px;"></th>
								</tr>
							</thead>
							<tbody id="logList">
								 
							</tbody>
						</table>
					</div>
				</div>
				<!--datatable_fixed-->
			</div>
			<!--datatable -->
		</div>
		<!--//content-->
	</div>
	<!--//container-->
</div>
<!--//wrap-->
</body>
</html>