<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>엑셀 업로드</title>
		<link rel="stylesheet" href="../../resources/css/jqwidget3.5.0/styles/jqx.base.css" type="text/css" />
		<link rel="stylesheet" href="../../resources/css/jqwidget3.5.0/styles/iplanbiz.basic.css" type="text/css"/>
		<link rel="stylesheet" href="../../resources/css/jqwidget3.5.0/styles/jqx.SophisticatedLayout_portalV2.0.css" type="text/css"/>
		<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
		<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
		<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>

		<script language="javascript" type="text/javascript">
		
		var errMessage = "${errMessage}";
		
		function formClear(form){
			document.getElementById("oldPgmId").value = "";
			document.getElementById("saveFormAc").value = "save";
			resetForm(form);
		}
		function saveStart(){
			
			
			
			if($('input[name="upFile"]').val() == '') {
				alert('업로드할 파일을 선택하세요.');
				return;
			}else{
				var a = $('input[name="upFile"]').val(); 
				var extname = a.substring(a.lastIndexOf(".")+1);
				if(extname.toUpperCase()!="XLS"){
					alert("Microsft Excel 97-2003 워크시트 문서(xls)만 업로드 가능합니다.");
					return;
				}
			}
			
			if($("#yymmdd").val() == ""){
				alert("기준일자를 입력하세요.");
				return;
			}
			
			$("#uploadForm").submit();
			layerPopupFull.style.display = "";
		}
		
		$(function (){   
			
// 			var pgmNm = "${pgmNm}";
// 			if(pgmNm!="") {
// 				$("#pgmNm").val(decodeURIComponent(pgmNm));	
// 			}
			
			if("${errMessage}" == "정상처리하였습니다."){
				
				window.close();
			}
// 			if("${param.yyyymm}".trim()==""){
// 				window.close();
// 			}
			$("#jqxButton1").jqxButton({ width: '',  theme:'SophisticatedLayout'});
			$("#jqxButton2").jqxButton({ width: '',  theme:'SophisticatedLayout'});
		});
	
		</script>
	</head> 
	<body class='default i-DQ'>
		<div id="layerPopupFull" style="position:absolute; z-index:9;width:100%; height:100%; left:0px; top:0px; display:none; background-color:white;filter:alpha(opacity=60);opacity:.6;">
			<img src="${WEB.ROOT}/resource/img/ajax-loader.gif" style="left:250px; top:150px;position: absolute;"/><!-- 페이지전체영역입니다. -->
		</div>
		<div class="wrap" style="width:96%; margin:0 auto;">
			<div class="i-DQ-header" style="width:100%; margin-top:30px;">
				<div class="iwidget_label" style="width:100%; float:right; margin-bottom:10px;  ">
					<div class="label-bg label-1" style="float:left;">엑셀 업로드</div>
				</div>
			</div>
		</div>
		<div class="i-DQ-contents" style="width:100%; margin:10px auto !important; margin-top:20px !important;">
			<div class="table_Area table-style" style="width:100%; height:100%; float:left; margin-top:0px; ">
				<form id="uploadForm" name="uploadForm" method="post" action="../../excelPgm/closeEduStats/upload" enctype="multipart/form-data">
					<input type="hidden" name="ac" id="uploadFormAc" value="upload" />
					<input type="hidden" id="dbkey" name="dbkey" value="${param.dbkey}" /> 
					<input type="hidden" id="year" name="year" value="${param.year}" />
					<input type="hidden" id="month" name="month" value="${param.month}" />
					<table width="100%" class="i-DQ-table2 table_box" cellspacing="0" cellpadding="0" border="0" style="margin-bottom:20px !important;">
<!-- 						<tr> -->
<!-- 							<th class="tableTitle form">프로그램명</th> -->
<%-- 							<td class="tableContent_form"><div style="margin-left: 5px;" id="pgmNm"><%=java.net.URLDecoder.decode(request.getAttribute("pgmNm").toString(),"UTF-8")%></div></td>	 --%>
<!-- 						</tr> -->
<!-- 						<tr> -->
<!-- 							<th class="tableTitle form">프로그램ID</th> -->
<%-- 							<td class="tableContent_form"><div style="margin-left: 5px;">${pgmId}</div></td>	 --%>
<!-- 						</tr> -->
<!-- 						<tr> -->
<!-- 							<th class="tableTitle form">기준일자<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th> -->
<%-- 							<td class="tableContent_form"><input type="text" id="yymmdd" name="yymmdd" class="inputtable" value="${param.yyyymm }" readonly="readonly"/></td>	 --%>
<!-- 						</tr> -->
						<tr>
							<th class="tableTitle form">파일</th>
							<td class="tableContent_form"><div style="margin-left: 5px;"><input type="file" accept="application/vnd.ms-excel" name="upFile" value="${upFile}"/></div></td>
						</tr>
						<tr>
							<td colspan="2">
								
								<div style="float:right;margin-top: 3px;margin-right:0px;margin-bottom:3px;">
									<div class="buttonStyle" style="float:left; height:25px;margin-right:10px; ">
										<input type="button" value="Excel업로드" id='jqxButton1' width="100%" height="100%" onclick="saveStart();" />
									</div>
									<div class="buttonStyle" style="float:left; height:25px;margin-right:10px; ">
										<input type="button" value="닫기" id='jqxButton2' width="100%" height="100%" onclick="self.close();" />
									</div>
								</div>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</body> 
</html> 