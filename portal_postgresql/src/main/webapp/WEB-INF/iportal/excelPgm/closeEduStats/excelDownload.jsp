 <%@ page language="java" contentType="application/vnd.xls;charset=UTF-8" pageEncoding="utf-8"%>   -
<%@page import="java.net.URLEncoder"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%

	String header =request.getHeader("User-Agent");
	String browser="";  
	String docName = URLEncoder.encode("standard", "utf-8");  
	 
	System.out.println("header++++++++++++++++++++++++++++++"+header+":::::"+docName);
	 
	
    response.setHeader("Content-Disposition", "attachment; filename="+docName+".xls"); 
    response.setHeader("Content-Description", "JSP Generated Data");

%>   -
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>standard</title>
	
	</head>
 	<body>
		<table width="100%;" id="tableList" class="fix_rable" cellspacing="0" cellpadding="0" border="1">		
			<thead>			
				<tr> 
					<th style="background-color:#eff0f0;" >년도</th>
					<th style="background-color:#eff0f0;" >월</th>
					<th style="background-color:#eff0f0;" >대학대학원구분</th>
					<th style="background-color:#eff0f0;" >학과코드</th>
					<th style="background-color:#eff0f0;" >학과명</th>
					<th style="background-color:#eff0f0;" >단과대학명</th>
					<th style="background-color:#eff0f0;" >주야</th>
					<th style="background-color:#eff0f0;" >학과특성</th>
					<th style="background-color:#eff0f0;" >교육부계열_대계열명</th>
					<th style="background-color:#eff0f0;" >교육부계열_중계열명</th>
				</tr>
			</thead>
		    <tbody>
				<c:if test="${data!=null}">
				<c:forEach items="${data}" var="rows" varStatus="loop">
					<tr>	
						<td align="center">${rows.YEAR}</td>
						<td align="center">${rows.MONTH}</td>
						<td align="center">${rows.GUBUN}</td>
						<td align="center">${rows.SUST_ID}</td>
						<td align="center">${rows.SUST_NM}</td>
						<td align="center">${rows.COLL_NM}</td>
						<td align="center">${rows.DAN_NM}</td>
						<td align="center">${rows.SUST_SPE_NM}</td>
						<td align="center">${rows.PART_NM}</td>
						<td align="center">${rows.PART_DTL_NM}</td>
					</tr>
				</c:forEach>
				</c:if>
			</tbody> 
		</table>
	</body>
</html>