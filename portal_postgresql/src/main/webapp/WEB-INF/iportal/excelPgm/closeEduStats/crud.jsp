<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title id='Description'>고등교육통계 마감</title>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
<script>
	var curProgramID = null;   
	var curProgramIDX = null;
	var loadingMsg = '데이터 로딩중 입니다.';
	var stat1='',stat2='',stat3='';
	function showMsg(){
     	$i.dialog.showLoader('SYSTEM',loadingMsg); 
    }
	$(document).ready(function(){
		$(window).ajaxStart(showMsg);
		$(window).ajaxStop(function(){
			$i.dialog.hideLoader();
		});
		//Year Combo
		var dat = $i.post("../../ExcelPgmDQ/getCommonCode", {comlcd:"EXCEL_YEAR"});
		dat.done(function(res){
			var pSource =
			{
				localdata: res,
	    		dataType: 'json',
				dataFields: [
					{ name: 'COM_COD'},
					{ name: 'COM_NM'}
				]
			};
			var pAdapter = new $.jqx.dataAdapter(pSource);
			
			$("#cboYear").jqxComboBox({ selectedIndex: 0,  source: pAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 80, dropDownHeight: 100, width: 80, height: 22,  theme:'blueish'});
			$("#cboYear").jqxComboBox({ selectedIndex: 0});
			var data = $i.post("../../excelPgm/closeEduStats/getCommonCodeMmOrder", {comlcd:"EXCEL_MM"});
			data.done(function(res){
				var pSource =
				{
					localdata: res,
		    		dataType: 'json',
					dataFields: [
						{ name: 'COM_COD'},
						{ name: 'COM_NM'}
					]
				};
				
				var pAdapter = new $.jqx.dataAdapter(pSource);			
				
				$("#cboMonth").jqxComboBox({ selectedIndex: 0, source: pAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 80, dropDownHeight: 150, width: 80, height: 22, theme:'blueish'});
				search();
			});
		}); 
		//월 콤보
		
		$("#btnSearch").jqxButton({ width: '',  theme:'blueish'}); 
		$("#btnExecute").jqxButton({ width: '',  theme:'blueish'});  
		$("#btnDownload").jqxButton({ width: '',  theme:'blueish'}); 
		$("#btnUpload").jqxButton({ width: '',  theme:'blueish'}); 
// 		$("#btnConfirm1").jqxButton({ width: '',  theme:'blueish'});
// 		$("#btnConfirm2").jqxButton({ width: '',  theme:'blueish'});
		;
		
	}); 
	
	function getStepCloseEduStat(){
		var dat = $i.post("../../excelPgm/closeEduStats/getStepOneForCloseEduStat",{yyyy:$("#cboYear").val(),mm:$("#cboMonth").val()});
		dat.done(function(result){
			var res = result.returnArray;
			if(res.length==0||res[0].STATUS!="WORKING"){
							
				$("#btnUpload").hide();
				$("#btnExecute").hide();
				$("div[data-role='checkForStep']").show();
			}
			else{
				$("#btnUpload").show();
				$("#btnExecute").show();
				$("div[data-role='checkForStep']").hide();
			}
				
		}).fail(function(msg){
			alert(msg);
		});
		
	}
	function search(curProgramID){
// 		getStandardList();
		stat1 = $i.post("../../excelPgm/closeEduStats/closeEduStatsGetProgramID");
		stat1.done(function(res){
			var data = res.returnArray;
			var customsortfunc = function (column, direction) {
	            var sortdata = new Array();

	            if (direction == 'ascending') direction = true;
	            if (direction == 'descending') direction = false;

	            if (direction != null) { 
	                for (i = 0; i < data.length; i++) {
	                    sortdata.push(data[i]);
	                }
	            }
	            else sortdata = data;
	            source.localdata = sortdata;
	            $("#grdProgram").jqxGrid('pincolumn', 'columndatafield');
	            Object.prototype.toString = tmpToString;
	        };

	        var source =
	        {
	            localdata: data,
	            sort: customsortfunc,
	            datafields:
	            [
					{ name: 'GUBN', type: 'string' },
					{ name: 'GUBN_NAME', type: 'string' },
					{ name: 'END_TIME', type: 'string' },
					{ name: 'SUCCESS_YN', type: 'string' }
	            ],
	            id:"GUBN", 
	            datatype: "array"
	        };
	        var dataAdapter = new $.jqx.dataAdapter(source);
	        $("#grdProgram").jqxGrid(
	        { 
	        	width: '100%',
                height:'100%',
 				altrows:true,
 				pageable: false,
                source: dataAdapter,
 				theme:'blueish',
 				columnsheight:25 ,
 				rowsheight: 25,
                columnsresize: true,
				
	            ready: function () {
	                $("#grdProgram").jqxGrid(); 
	            },    

	            columns: [  
	              { text: '구분', dataField: 'GUBN_NAME', width: '50%', align:'center', cellsalign: 'left'  },
	              { text: '최종 집계시간',  dataField: 'END_TIME', width: '30%', cellsalign: 'center', align:'center',  formatter: 'date',formatoptions: { newformat: "y/m/d H:i:s" }},
	              { text: '완료여부',  dataField: 'SUCCESS_YN',   width:'20%', align:'center',  cellsalign: 'center'}
	            ]
	        });
	        $("#grdProgram").on("rowselect",function(event){ 
	        	$("#gubnName").html(event.args.row.GUBN_NAME);  
	       
	        	if(event.args.row.GUBN=="ZZZZZZ"){
	        		$("[data-role=contents]").eq(1).show();
	        		$("[data-role=contents]").eq(0).hide();
	        		getStandardList(); 
	        	}else{
	        		$("[data-role=contents]").eq(0).show();
	        		$("[data-role=contents]").eq(1).hide();
	        		$('#grdProgramDetail').jqxGrid('clearselection');
	        		detail(event.args.row.GUBN);  
		        	error(event.args.row.GUBN);
	        	}
	        	getStepCloseEduStat();
	        }); 
	        if(curProgramID == null){
	        	$("#grdProgram").jqxGrid('selectrow', 0);
	        }
	         
	        
	        
	        getStepCloseEduStat();
	    			
		});
	}
	
	function detail(pgmid){
// 		$iv.mybatis("getExcelPgmDAS_Tab1_Top", {GUBN: pgmid, ACCT_ID:$iv.userinfo.acctid()}, {callback:function(data){
		stat2 = $i.post("../../excelPgm/closeEduStats/getProceduerDetailByClose", {gubn:pgmid});
		stat2.done(function(res){
			var data = res.returnArray;
			var customsortfunc = function (column, direction) {
	            var sortdata = new Array();

	            if (direction == 'ascending') direction = true;
	            if (direction == 'descending') direction = false;

	            if (direction != null) {
	                for (i = 0; i < data.length; i++) {
	                    sortdata.push(data[i]);
	                }
	            }
	            else sortdata = data;
	            source.localdata = sortdata;
	            $("#jqxgrid1").jqxGrid('pincolumn', 'columndatafield');
	            Object.prototype.toString = tmpToString;
	        }; 
	        var source =
	        {
	            localdata: data,
	            sort: customsortfunc,
	            datafields:
	            [
					{ name: 'GUBN', type: 'string' },
					{ name: 'IDX', type: 'string' },
					{ name: 'PROCEDURE_ID', type: 'string' },
					{ name: 'PROCEDURE_NM', type: 'string' },
					{ name: 'DBKEY', type: 'string' },
					{ name: 'END_TIME', type: 'string' },
					{ name: 'SUCCESS_YN', type: 'string' },
					{ name: 'EXPLAIN', type: 'string' },
					{ name: 'SCH_ID', type: 'string'},
					{ name: 'SCH_NM', type: 'string'},
					{ name: 'RESULT_NM', type: 'string'},
					{ name: 'END_DAT', type: 'string'},
					{ name: 'RESULT', type: 'string'},
					{ name: 'SCH_IDX', type:' string'}
	            ],
	            datatype: "array"
	        };
	        var dataAdapter = new $.jqx.dataAdapter(source);
	        
	        var alginRight = function (row, columnfield, value) {//right정렬
				return '<div id="userName-' + row + '"style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
			};
			 var errList = function(row,columnfield,value,defaulthtml,columnproperties,rowdata) {
            	var schId = rowdata.SCH_ID;
            	var idx = rowdata.SCH_IDX;
            	var result = rowdata.RESULT;
            	var resultName = rowdata.RESULT_NM;
	            var schNm = rowdata.SCH_NM;
	            var link = "";
	            
	            if(result == "Y"){
					link = "<span style='color:blue;'>"+resultName+"</span>";
	            }else if(result == "N"){
	            	link = "<a href=\"javaScript:goErrorDetail('"+idx+"','"+schId+"','"+schNm+"')\" style='color:red;''>"+resultName+"</a>";
	            }else{
	            	link = "";
	            }
	            return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:5px;'>" + link + "</div>";
			};
	         
	        $("#grdProgramDetail").jqxGrid(
	                {
	                    width: '100%',
	                    height:'100%',
	    				altrows:true,
	    				pageable: false,
	                    source: dataAdapter,
	    				theme:'blueish',
	    				scrollmode: 'default',
	    				scrollbarsize: 17,
	    				//columnsheight:30 ,
	    				//rowsheight: 35,
	                    columnsresize: true,
	    				selectionmode: 'checkbox',
	    				//editable: true, 
	                    columns: [ 
	                      { text: 'DB키', datafield: 'DBKEY',hidden:true},
	                      { text: '구분', datafield: 'PROCEDURE_NM', width:90, align:'center', cellsalign: 'center'},
	                      { text: '스케쥴명', datafield: 'SCH_NM', width: 140, align: 'center', cellsalign:'left', cellsrenderer: alginLeft },
// 	                      { text: '최근 스케쥴 시간', datafield: 'END_DAT', width: 140, align: 'center', cellsalign:'center'},
	                      { text: '실행결과', datafield: 'RESULT_NM', width: 60, align: 'center', cellsalign:'center', cellsrenderer:errList},
	                      { text: '프로시져', datafield: 'PROCEDURE_ID', width: 90, align:'center', cellsalign: 'left', cellsrenderer: alginLeft  },
	                      { text: '최종 집계시간',  dataField: 'END_TIME', width: 120, cellsalign: 'center', align:'center',  formatter: 'date',formatoptions: { newformat: "y/m/d H:i:s" }},
	    				  { text: '성공여부', datafield: 'SUCCESS_YN', width: 60, align:'center', cellsalign: 'center' }, 
	    				  { text: '설명', datafield: 'EXPLAIN', align:'center',  cellsalign: 'right', cellsrenderer: alginLeft } 
	                    ]
	                });
		});
		 
	}
	function goErrorDetail(idx, schId, schNm){
		window.open("${WWW.IPORTAL}/scheduler/errorDetail?pIdx="+idx+ "&schId=" + schId+ "&schNm="+schNm, "오류새창","width=1500, height=700");
	}
	function error(progid){ 
// 			$iv.mybatis("getExcelPgmDAS_Tab1_Btm", {GUBN: progid}, {callback:function(data2){
			stat3 = $i.post("../../excelPgm/closeEduStats/getProcedureErrorByClose", {gubn:progid});
			stat3.done(function(data2){
				var data = data2.returnArray;
		    	var customsortfunc = function (column, direction) {
		            var sortdata = new Array();

		            if (direction == 'ascending') direction = true;
		            if (direction == 'descending') direction = false;

		            if (direction != null) {
		                for (i = 0; i < data.length; i++) {
		                    sortdata.push(data[i]);
		                }
		            }
		            else sortdata = data;
		            source.localdata = sortdata;
		            $("#jqxgrid2").jqxGrid('pincolumn', 'columndatafield');
		            Object.prototype.toString = tmpToString;
		        };

		        var source =
		        {
		            localdata: data,
		            sort: customsortfunc,
		            datafields:
		            [
						{ name: 'SP_NAME', type: 'string' },
						{ name: 'USER_ID', type: 'string' },
						{ name: 'DTINSERT', type: 'string' },
						{ name: 'ERRORCOMMENT', type: 'string' },
						{ name: 'PARAMS', type: 'string'}
		            ],
		            datatype: "array"
		        };
		        var dataAdapter = new $.jqx.dataAdapter(source);
		        
		        $("#grdErrorList").jqxGrid(
		                {
		                    width:'100%' ,
		                    height: '100%',
		    				altrows:true,
		    				pageable: false,
		                    source: dataAdapter,
		    				theme:'blueish',
		    				columnsheight:25 , 
		    				rowsheight: 25, 
		                    columnsresize: true,
		                    columns: [
		                      { text: '프로시져명', datafield: 'SP_NAME', width:100,   align:'center', cellsalign: 'left',  cellsrenderer: alginLeft},
		                      { text: '집계사용자', datafield: 'USER_ID', width: 100, align:'center', cellsalign: 'left',  cellsrenderer: alginLeft },
		                      { text: '오류발생시간', datafield: 'DTINSERT', width: 130, align:'center', cellsalign: 'center' },
		    				  { text: '파라미터', datafield: 'PARAMS', width: 85, align:'center', cellsalign: 'center' },
		    				  { text: '오류메세지', datafield: 'ERRORCOMMENT',  align:'center', cellsalign: 'center'}
		                 
		                    ]
		                });
		        
			});
	}
	
	function getStandardList(){
		var dat = $i.post("../../excelPgm/closeEduStats/closeEduStatsExcelList",{year:$("#cboYear").val(), month:$("#cboMonth").val()});
		dat.done(function(data) {
			var source =
			{
				datatype: "json",
				datafields: [
				             
					{ name: 'YEAR', type: 'string' },
					{ name: 'MONTH', type: 'string' },
					{ name: 'SUST_NM', type: 'string' },
					{ name: 'COLL_NM', type: 'string' },
					{ name: 'DAN_NM', type: 'string' },
					{ name: 'SUST_SPE_NM', type: 'string' },
					{ name: 'PART_NM', type: 'string' },
					{ name: 'PART_DTL_NM', type: 'string' }
				],
				localdata: data,
				updaterow: function (rowid, rowdata, commit) {
					commit(true);
				}
			};
			var alginRight = function (row, columnfield, value) {//right정렬
				return '<div id="userName-' + row + '"style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
			};
			var alginLeft = function (row, columnfield, value) {//left정렬
				return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
			}; 
			var dataAdapter = new $.jqx.dataAdapter(source, {
				downloadComplete: function (data, status, xhr) { },
				loadComplete: function (data) { },
				loadError: function (xhr, status, error) { }
			});
			var dataAdapter = new $.jqx.dataAdapter(source); 
			$("#grdStandardList").jqxGrid(
			{
				width:'100%' ,
				height: '100%',
				altrows:true,
				pageable: false,
				source: dataAdapter,
				theme:'blueish',
				columnsheight:25,
				rowsheight: 25, 
				columnsresize: true,
				columns: [
					{ text: '년도', datafield: 'YEAR', width: 60, align:'center', cellsalign: 'center'},
					{ text: '월', datafield: 'MONTH', width: 50, align:'center', cellsalign: 'left',  cellsrenderer: alginLeft },
					{ text: '학과명', datafield: 'SUST_NM',  align:'center', cellsalign: 'center' }, 
					{ text: '단과대학명', datafield: 'COLL_NM', width: 120, align:'center', cellsalign: 'center'},
					{ text: '주야', datafield: 'DAN_NM', width: 80, align:'center', cellsalign: 'center'}, 
					{ text: '학과특성', datafield: 'SUST_SPE_NM', width: 80, align:'center', cellsalign: 'center'},
					{ text: '대계열명', datafield: 'PART_NM', width: 100, align:'center', cellsalign: 'center', columngroup:"part"},
					{ text: '중계열명', datafield: 'PART_DTL_NM', width: 96, align:'center', cellsalign: 'center',columngroup:"part"}           
				],
				columngroups:[
					{name:"part",text:"교육부계열", align:"center"}  
				]
			});
			$("#grdStandardList").jqxGrid('selectrow', 0);
		});
	}
	
	function progressShow(){
		if(confirm("확인 버튼을 누르면 집계가 진행됩니다.")){ 
			procedureStart();     
	   	}
	}

	function procedureStart(){ //집계
		var row = $("#grdProgram").jqxGrid("getrowdatabyid",$("#grdProgram").jqxGrid("getrowid" ,$('#grdProgram').jqxGrid('selectedrowindex') )); 
		curProgramID = row.GUBN; 
		curProgramIDX = $('#grdProgram').jqxGrid('selectedrowindex') ;
		var indexs = $('#grdProgramDetail').jqxGrid('getselectedrowindexes');
		var rows = $('#grdProgramDetail').jqxGrid('getrows');
		var procedureId = [];
		var procedureNm = [];
		var schID = [];
		var schName = [];
		var dbkey = [];
		var j=0;
		if(indexs.length == 0){
			$i.dialog.alert("SYSTEM","집계할 프로시져를 선택해주세요.");
			return false;
		}else{
			var makeInputBox = "";
			$("[data-role=execItem]").remove();
			for(var i=0; i<indexs.length; i++){
				if(rows[indexs[i]].SCH_ID){
					schID[i] = rows[indexs[i]].SCH_ID;
					schName[i] = rows[indexs[j]].SCH_NM;
				}else{
					schID[i] = "";
					schName[i] = "";
				}
				procedureId[i] = rows[indexs[i]].PROCEDURE_ID;
				procedureNm[i] = rows[indexs[i]].PROCEDURE_NM;
				dbkey[i] = rows[indexs[i]].DBKEY;
				console.log(dbkey[i]);
				makeInputBox += "<input type='hidden' data-role='execItem' name='dbKey' value='"+dbkey[i]+"' />";
				makeInputBox += "<input type='hidden' data-role='execItem' name='procedureId' value='"+procedureId[i]+"' />";
				makeInputBox += "<input type='hidden' data-role='execItem' name='procedureNm' value='"+procedureNm[i]+"' />";
				makeInputBox += "<input type='hidden' data-role='execItem' name='schID' value='"+schID[i]+"' />";
				makeInputBox += "<input type='hidden' data-role='execItem' name='schName' value='"+schID[i]+"' />";
				
			} //IPLANBS_NEW
			$("#runningProcedure").append(makeInputBox);
			$("#procYYYY").val($("#cboYear").val());
			$("#procMM").val($("#cboMonth").val());
			$("#procGubn").val(curProgramID);
			
		} 
// 		if(schID.length > 0){
// 			for(var k=0;k<schID.length;k++){
// 				quartsService.executeJob(schID[k],{callback:function(res){}, async:false});
// 			}
// 		}
		loadingMsg='집계중입니다.';
		console.log(dbkey);
		var dat = $i.post("../../excelPgm/closeEduStats/procedureStartYyyymmddBySch","#runningProcedure");
		dat.done(function(data){
			
			if(data.returnCode!="EXCEPTION"){
				search(curProgramID);
				detail(curProgramID);               
				error(curProgramID);
				var ajaxArray = [stat1,stat2,stat3];
				$.when.apply($,ajaxArray).done(function(){
					loadingMsg='데이터 로딩중 입니다.';
					$i.dialog.alert("SYSTEM","완료되었습니다.");
				});
			}else{
				$i.dialog.error("SYSTEM",data.returnMessage+" ( 관련 테이블 : iportal_custom.IECT3002, iportal_custom.IECT3005 ) ");
			}
		}).fail(function(){
			loadingMsg='데이터 로딩중 입니다.';
		});   
		
	}
	function download(){
		$("[data-role=download]").remove(); 
		var iframe = $("<iframe data-role='download' style='display:none;'></iframe>").appendTo("body");
		
		iframe.attr("src","../../excelPgm/closeEduStats/download?year="+$("#cboYear").val()+"&month="+$("#cboMonth").val()+"&dbkey="+"LOGIN_ORACLE");
		//iframe.attr("src","./closeEduStats/download?year=2016&month=04&dbkey="+($iv.param("dbkey")==null?"IPLANBS":$iv.param("dbkey") )   ); 
	}
	
	var alginLeft = function (row, columnfield, value) {//left정렬
         return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
 				
    } ;
 	var alginRight = function (row, columnfield, value) {//right정렬
         return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
 				
    } ;	   
    function upStart() {
	
	var year = $("#cboYear").val();
	var month = $("#cboMonth").val();
	var dbKey = "IPLANBS";
	var url = "../../excelPgm/closeEduStats/upload?dbkey="+dbKey+"&year="+year+"&month="+month;
	windowOpen(url, 'ExcelUpload', 500,300,'no','auto');
}
</script>
    
</head>
	<body class='blueish'>
		<div class="wrap" style="width:1250px; min-width:1040px; margin:0 10px;">
			<div class="header f_left" style="width:1250px; height:27px; margin:10px 0px">
				<div class="label type1 f_left">년월:</div>
				<div class="combobox f_left"  id='cboYear' ></div>
				<div class="combobox f_left"  id='cboMonth' onchange="search();"></div>
				<div class="group_button f_right"> 
					<div class="button type1 f_left">
						<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();"/>
					</div>
				</div>
			</div>
			<div class="container  f_left" style="width:1250px; margin:10px 0px;">
				<div class="content f_left" style="width:380px; margin-right:15px;">
					<div class="grid f_left" style="width:380px; height:451px;">
						<div id="grdProgram"></div>
					</div>
				</div>
				<div class="content f_right"  data-role='contents'  style="width:855px;">
					<div class="group f_left  w100p">
						<div class="label type2 f_left">구분:<span class="label sublabel" id="gubnName"></span></div>
						<div class="group_button f_right">
							<div class="button type2 f_left" style='color:red;' data-role='checkForStep'>
								<span>마감 상태를 확인 해 주세요.</span>
							</div>
							<div class="button type2 f_left">
								<input type="button" value="집계" id='btnExecute' width="100%" height="100%" onclick="progressShow();" />
							</div>
<!-- 							<div class="button type4 f_left"> -->
<!-- 								<input type="button" value="완료여부" id='btnConfirm1' width="100%" height="100%" /> -->
<!-- 							</div> -->
						</div>
					</div>
					<div class="grid f_left" style="width:855px; height:285px;">
						<div id="grdProgramDetail"></div> 
					</div>
					<div class="group f_left  w100p">
						<div class="label type2 f_left m_r10">집계오류</div>
					</div>
					<div class="grid f_left" style="width:100%; height:101px;">
						<div id="grdErrorList"></div>
					</div>
				</div> 
				<div class="content f_right" data-role='contents' style="width:855px;display:none;"> 
					<div class="group f_left  w100p">
						<div class="label type2 f_left">구분:<span class="label sublabel">표준분류</span></div>
						<div class="group_button f_right">
							<div class="button type2 f_left" style='color:red;' data-role='checkForStep'>
								<span>마감 상태를 확인 해 주세요.</span>
							</div>
							<div class="button type2 f_left">
								<input type="button" value="다운로드" id='btnDownload' width="100%" height="100%" onclick="download();"/> 
							</div>
							<div class="button type2 f_left">
								<input type="button" value="업로드" id='btnUpload' width="100%" height="100%" onclick="upStart();" />
							</div>
<!-- 							<div class="button type4 f_left"> -->
<!-- 								<input type="button" value="완료여부" id='btnConfirm2' width="100%" height="100%" /> -->
<!-- 							</div> -->
						</div> 
					</div>
					<div class="grid f_left" style="width:855px; height:420px;">
						<div id="grdStandardList"></div>
					</div>
					<div class="group f_left  w100p">
						<div class="label type3 f_left">다운로드 기능을 통해서 정해진 Excel 양식으로 업로드 해야합니다.</div>
					</div>
				</div>
				<form id="runningProcedure" name="runningProcedure" action="../../excelPgm/closeEduStats/procedureStartYyyymmddBySch">
					<input type="hidden" name="gubn" id="procGubn"/>
					<input type="hidden" name="yyyy" id="procYYYY"/>
					<input type="hidden" name="mm" id="procMM"/>
				</form>
			</div>
		</div>
	</body>
</html>