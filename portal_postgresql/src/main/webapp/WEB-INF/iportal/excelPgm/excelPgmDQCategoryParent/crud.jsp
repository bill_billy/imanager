<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<title>엑셀프로그램 대분류 관리</title>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
<link rel="stylesheet" href="../../resource/css/iplanbiz/table.css" type="text/css"/>
<script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
<script type="text/javascript">
function detailView(row) {
	$('#saveForm').jqxValidator('hide');
	var rowdata = $("#jqxgrid1").jqxGrid("getrowdata", row);
	$("#ac").val("UPDATE");
	$("#pCd").attr('readonly', true);
	
	$('#pCd').val(rowdata['COML_COD']);
	$('#pNm').val(rowdata['COML_NM']);
	$('#useYn').val(rowdata['USE_YN']);
	$('#sortOrder').val(rowdata['SORT_ORDER']);
	
}
function searchStart() {
	resetStart();
	$('#jqxgrid1').jqxGrid('clearselection');
	
	var listData = $i.post("./getExcelPgmDQCategory", { category:'P',pCd:'',cCd:'',useYn:$('#useCombo').val(),searchText:$('#sText').val()});
											// param : 대/중분류(P/C), 대분류코드, 중분류코드, 사용여부, 검색어
	listData.done(function(res){
		var source =
		{
			localdata: res,
    		dataType: 'json',
			dataFields: [
				{ name: 'COML_COD'},
				{ name: 'COML_NM'},
				{ name: 'SORT_ORDER'},
				{ name: 'BIGO'},
				{ name: 'USE_YN'} 
			]
		};

		var alginRight = function (row, columnfield, value) {//right정렬
			return '<div style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
		};
		var detailView = function (row, columnfield, value) {
			var newValue = $i.secure.scriptToText(value);
			var link = "<a href=\"javaScript:detailView('"+row+"')\" style='color:black;'>" + newValue + "</a>";
			return '<div style="text-align:left; margin:4px 0px 0px 10px; text-decoration: underline;">' + link + '</div>';
		};

		var adapter = new $.jqx.dataAdapter(source);
		
		$("#jqxgrid1").jqxGrid({
			width: '100%',
         	height: '100%',
			altrows:true,
			pageable: false,
        	source: adapter,
			theme:'blueish',
			//editable: true,
         	columnsresize: true,
         	columns: [
				{ text: '대분류 코드', datafield: 'COML_COD', width: '20%', align:'center', cellsalign: 'center'},
				{ text: '대분류 명', datafield: 'COML_NM', width: '40%', align:'center', cellsalign: 'left', cellsrenderer: detailView },
				{ text: '사용여부', datafield: 'USE_YN', width: '20%', align:'center', cellsalign: 'center'},
				{ text: '정렬순서', datafield: 'SORT_ORDER', width: '20%', align:'center', cellsalign: 'right', cellsrenderer: alginRight}
			]
		});
		
		
	});
}
function saveStart() {
	$('#saveForm').jqxValidator('hide');
	var ruleinfo = null;
	ruleinfo = [
		{input: '#pCd', message:'대분류 코드를 입력하세요!', action:'keyup, blur', rule:'required'},
		{input: '#pCd', message:'10자이내로 입력하세요!', action:'keyup, blur', rule:'maxLength=10'},
		{input: '#pNm', message:'대분류 명을 입력하세요!', action:'keyup, blur', rule:'required'},
		{input: '#sortOrder', message:'정렬순서는 숫자로 입력하세요!', action:'keyup, blur', rule:'number'}
		/* ,
		{input: '#sortOrder', message:'정렬순서를 입력하세요!', action:'keyup, blur', rule:'required'},
		{input: '#sortOrder', message:'정렬순서는 숫자로 입력하세요!', action:'keyup, blur', rule:'number'}
		*/
	];
	
	$("#saveForm").jqxValidator({
		rules : ruleinfo,
		position: 'bottom'
	});
	if($("#saveForm").jqxValidator('validate') == true) {
		
		$("#category").val("P");
		
		if($("#sortOrder").val() == '') {
			$("#sortOrder").val(0);
		}
		
		if(!checkStringFormat($("#pCd").val())){
			$i.dialog.warning('SYSTEM', '대분류 코드에 특수문자를 입력할 수 없습니다!');
			return false;
		}
		/*else if(!checkStringFormat($("#pNm").val())){
			$i.dialog.warning('SYSTEM', '대분류 명에 특수문자를 입력할 수 없습니다!');
			return false;
		}*/
		var method = $("#saveForm").attr("method");
		var action = $("#saveForm").attr("action");
		var data = $("#saveForm").serialize();
		
		$.ajax({
			type:method,
			url:action,
			data:data,
			async:false,
			success:function(res){
				$i.dialog.alert('SYSTEM', '저장되었습니다');
				resetStart();
	    		searchStart();
			},error:function(jqXHR,textStatus,errorMessage){
				$i.dialog.error('SYSTEM', '저장에 실패하였습니다.');
			}
		});	
	}	
}
function checkStringFormat(string) { 
    //var stringRegx=/^[0-9a-zA-Z가-힝]*$/; 
    var stringRegx = /[~!@\#$%<>^&*\-=+_\’\'\"]/gi; 
    var isValid = true; 
    if(stringRegx.test(string)) { 
      isValid = false; 
    } 
       
    return isValid; 
}
function delStart() {
	
	if($('#pCd').val() == '') {
		$i.dialog.alert('SYSTEM', '대분류를 선택하세요');
		return;
	}
	
	$i.dialog.confirm('SYSTEM', '대분류 코드를 삭제하시겠습니까?',function(){
		$("#category").val("P");
		$("#ac").val("DELETE");
		
		var method = $("#saveForm").attr("method");
		var action = $("#saveForm").attr("action");
		var data = $("#saveForm").serialize();
		
		$.ajax({
			type:method,
			url:action,
			data:data,
			async:false,
			success:function(res){
				$i.dialog.alert('SYSTEM', '삭제되었습니다',function(){
					resetStart();
		    		searchStart();
				});
			},error:function(jqXHR,textStatus,errorMessage){
			}
		});
	});
		
}
function resetStart() {
	$('#saveForm').jqxValidator('hide');
	$("#ac").val("INSERT");
	$("#pCd").attr('readonly', false);
	
	document.saveForm.reset();
}
function setCommonCodeCombobox(objid, comlcod) {
	var comboData = $i.post("./getCommonCode", { comlcd : comlcod });
	comboData.done(function(res){
		res.returnArray.unshift({"COM_NM":"전체","COM_COD":""});
		console.log(res);
		var source =
		{
			localdata: res,
    		dataType: 'json',
			dataFields: [
				{ name: 'COM_COD'},
				{ name: 'COM_NM'}				
			]
		};
		
		var adapter = new $.jqx.dataAdapter(source);
		
		$('#'+objid).jqxComboBox({ selectedIndex: 0, source: adapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'COM_NM', valueMember: 'COM_COD', dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22, theme:'blueish'});
		
	});
}
$(this).ready(function() {
	$('#searchButton').jqxButton({ width : '', theme : 'blueish' });
	$('#saveButton').jqxButton({ width : '', theme : 'blueish' });
	$('#delButton').jqxButton({ width : '', theme : 'blueish' });
	$('#resetButton').jqxButton({ width : '', theme : 'blueish' });
	
	$("#sText").jqxInput({height: 23, width:250 , minLength: 1, theme:'blueish'});
	setCommonCodeCombobox('useCombo', 'USE_YN'); //사용여부 - 검색조건
	
	
	
	
	$('#searchButton').click(function() { searchStart(); });
	$('#saveButton').click(function() { saveStart(); });
	$('#delButton').click(function() { delStart(); });
	$('#resetButton').click(function() { resetStart(); });
	
	$('#sText').on('keydown',
		function() {
			if(event.keyCode==13) {
				searchStart();
				return false;
			}
		}
	);
	
	//init
	searchStart();
	
	$("#ac").val("INSERT");
	$("#pCd").attr('readonly', false);
});
</script>
</head>
<body class="blueish">
	<div class="wrap" style="width:98%; min-width:1160px; margin:0 10px;">
		 	<div class="header" style="width:100%; margin:10px 0;">
				<div class="iwidget_label" style="width:100%; float:left;">
					<div class="label type1 f_left">사용여부 :</div>
					<div class="combobox f_left" id='useCombo'></div>
					<div class="label type1 f_left">프로그램명 :</div>
					<input type="text" id="sText" name="sText" style="float:left;margin-left:5px;"/>
					
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='searchButton' width="100%" height="100%" />
						</div>
						<!--buttonStyle--> 
					</div>
				</div>
		  	</div>
		  <div class="content" style="width:100%; height:100%; float:left;">
		  	<div class="left_Area" style="width:100%; height:100%; float:left; margin-top:10px;">
				<div class="iwidget_grid" style= "float:right; width:100%; height:380px !important; margin-bottom:20px !important;">
					<div id="jqxgrid1" class="grid2" style=" width:100%; height:100%; text-align:center !important"> </div>
				</div>
			</div>
			<div class="table_Area" style="width:100%; height:100%; float:left; margin-top:0px; ">
				<form id="saveForm" name="saveForm" action="${WWW.IPORTAL}/ExcelPgmDQ/pgmCategory" method="post" style="margin-top:10px">
				<input type="hidden" id="category" name="category" />
				<input type="hidden" id="ac" name="ac" />
					<table width="100%" class="i-DQ-table" cellspacing="0" cellpadding="0" border="0">
						<tbody> 	
							<colgroup>
								<col width="20%" />
								<col width="40%" />
								<col width="20%" />
								<col width="20%" />
							</colgroup>
							<thead>
								<tr> 		
									<th><span style="color:red; font-size:12px; vertical-align:-2px;">＊</span>대분류 코드</th> 		
									<th><span style="color:red; font-size:12px; vertical-align:-2px;">＊</span>대분류 명</th> 		
									<th><span style="color:red; font-size:12px; vertical-align:-2px;">＊</span>사용여부</th>
									<th>정렬순서</th> 		
								</tr>
							</thead>
						<tbody> 	
							<tr>
								<td><div class="cell t_center">
								<input type="text" id="pCd" name="pCd" style="width:94%;" class="input type2  f_left" /></div></td>
								<td><div class="cell t_center">
								<input type="text" id="pNm" name="pNm" style="width:95%;" class="input type2  f_left" /></div></td>
								<td style="text-align: center;"><div class="cell t_center">
									<select class="input type2  f_left" id="useYn" name="useYn" style="width:80%;">
										<option value="Y">Y</option>	
										<option value="N">N</option>	
									</select>
									</div>
								</td>
								<td><div class="cell t_center">
								<input type="text" id="sortOrder" name="sortOrder" style="width:90%; padding-right: 5px; text-align: right;" class="input type2  f_left" /></div></td>
							</tr> 			
						</tbody>
					</table>
					<div class="label-4" style="float:left;margin-top:10px;"><p class="iwidget_grid_tip">셀에 마우스 클릭시 편집 및 선택할 수 있습니다.</p></div>
					<div class="button-bottom" style="float:right; margin-top: 10px;">
						<div class="button type2" style="float:left; height:25px; ">
							<input type="button" value="저장" id='saveButton' width="100%" height="100%" style="margin-left: 0px !important;"/>
						</div>
						<div class="button type2" style="float:left; height:25px; ">
							<input type="button" value="삭제" id='delButton' width="100%" height="100%" style="margin-left: 0px !important;"/>
						</div>
						<div class="button type2" style="float:left; height:25px;margin-right:10px;">
							<input type="button" value="초기화" id='resetButton' width="100%" height="100%" style="margin-left: 0px !important;"/>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>