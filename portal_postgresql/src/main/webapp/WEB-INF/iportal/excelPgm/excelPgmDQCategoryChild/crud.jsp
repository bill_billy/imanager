<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<title>엑셀 프로그램 중분류 관리</title>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
<link rel="stylesheet" href="../../resource/css/iplanbiz/table.css" type="text/css"/>
<script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
<script type="text/javascript">
$(this).ready(function() {
	$('#searchButton').jqxButton({ width : '', theme : 'blueish' });
	$('#saveButton').jqxButton({ width : '', theme : 'blueish' });
	$('#delButton').jqxButton({ width : '', theme : 'blueish' });
	$('#resetButton').jqxButton({ width : '', theme : 'blueish' });
	
	$("#sText").jqxInput({height: 23, width:250 , minLength: 1, theme:'blueish' });
	
	setCommonCodeCombobox('combobox1', 'USE_YN'); //사용여부 - 검색조건
	

	
	$('#searchButton').click(function() { searchStart(); });
	$('#saveButton').click(function() { saveStart(); });
	$('#delButton').click(function() { delStart(); });
	$('#resetButton').click(function() { resetStart(); });
	
	$('#sText').on('keydown',
		function() {
			if(event.keyCode==13) {
				searchStart();
				return false;
			}
		}
	);
	
	//init
	createGrid1();
	createGrid2();
	
	searchStart();
	
	
	$('#ac').val("INSERT");
	$('#cCd').attr('readonly', false);
	
	
});
function setCommonCodeCombobox(objid, comlcod) {
	var comboData = $i.post("./getCommonCode", { comlcd : comlcod });
	comboData.done(function(res){
		var source =
		{
			localdata: res,
    		dataType: 'json',
			dataFields: [
				{ name: 'COM_COD'},
				{ name: 'COM_NM'}				
			]
		};
		var adapter = new $.jqx.dataAdapter(source);
		
		$('#'+objid).jqxComboBox({ selectedIndex: 0, source: adapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'COM_NM', valueMember: 'COM_COD', dropDownWidth: 150, dropDownHeight: 70, width: 150, height: 22, theme:'blueish'});
		
	});
}
function createGrid1() {
	var source = {
		localdata: [],
		dataType: 'json',
		dataFields: [
			{ name: 'COML_COD'},
			{ name: 'COML_NM'},
			{ name: 'SORT_ORDER'},
			{ name: 'BIGO'},
			{ name: 'USE_YN'} 
		]
	};

	var alginRight = function (row, columnfield, value) {//right정렬
		return '<div style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
	};
	var detailView = function (row, columnfield, value) {
		var newValue = $i.secure.scriptToText(value);
		var link = "<a href=\"javaScript:detailView('"+row+"')\" style='color:black;'>" + newValue + "</a>";
		return '<div style="text-align:left; margin:4px 0px 0px 10px; text-decoration: underline;">' + link + '</div>';
	};

	var adapter = new $.jqx.dataAdapter(source);

	$('#jqxgrid1').jqxGrid({
		width: '100%',
     	height: '100%',
		altrows:true,
		pageable: false,
    	source: adapter,
		theme:'blueish',
     	columnsresize: true,
     	columns: [
			{ text: '대분류 코드', datafield: 'COML_COD', width: '30%', align:'center', cellsalign: 'center'},
			{ text: '대분류 명', datafield: 'COML_NM', width: '40%', align:'center', cellsalign: 'left', cellsrenderer: detailView },
			{ text: '정렬순서', datafield: 'SORT_ORDER', width: '30%', align:'center', cellsalign: 'right', cellsrenderer: alginRight}
		]
	});
}
function createGrid2() {
	var source = {
		localdata: [],
		dataType: 'json',
		dataFields: [
			{ name: 'COML_COD'},
			{ name: 'COM_COD'},
			{ name: 'COM_NM'},
			{ name: 'SORT_ORDER'},
			{ name: 'BIGO'},
			{ name: 'USE_YN'} 
		]
	};

	var alginRight = function (row, columnfield, value) {//right정렬
		return '<div style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
	};
	var detailChildView = function (row, columnfield, value) {
		var newValue = $i.secure.scriptToText(value);
		var link = "<a href=\"javaScript:detailChildView('"+row+"')\" style='color:black;'>" + newValue + "</a>";
		return '<div style="text-align:left; margin:4px 0px 0px 10px; text-decoration: underline;">' + link + '</div>';
	};

	var adapter = new $.jqx.dataAdapter(source);
	
	$('#jqxgrid2').jqxGrid({
		width: '100%',
     	height: '100%',
		altrows:true,
		pageable: false,
    	source: adapter,
		theme:'blueish',
     	columnsresize: true,
     	columns: [
			{ text: '중분류 코드', datafield: 'COM_COD', width: '20%', align:'center', cellsalign: 'center'},
			{ text: '중분류 명', datafield: 'COM_NM', width: '40%', align:'center', cellsalign: 'left', cellsrenderer: detailChildView },
			{ text: '사용여부', datafield: 'USE_YN', width: '20%', align:'center', cellsalign: 'center'},
			{ text: '정렬순서', datafield: 'SORT_ORDER', width: '20%', align:'center', cellsalign: 'right', cellsrenderer: alginRight}
		]
	});
}
function searchStart() {
	
	clearForm();
	var listData = $i.post("./getExcelPgmDQCategory", { category:'P',pCd:'',cCd:'',useYn:'Y',searchText:$('#sText').val()});
	// param : 대/중분류(P/C), 대분류코드, 중분류코드, 사용여부, 검색어
	listData.done(function(res){
		var $jqxgrid1 = $('#jqxgrid1');
		$jqxgrid1.jqxGrid('source')._source.localdata = res;
		$jqxgrid1.jqxGrid('updatebounddata');
		detailView(0); //중분류코드
	});
}
function clearForm() {
	var $jqxgrid1 = $('#jqxgrid1');
	var $jqxgrid2 = $('#jqxgrid2');
	
	$jqxgrid1.jqxGrid('clearselection');
	$jqxgrid2.jqxGrid('clear');
	$jqxgrid2.jqxGrid('clearselection');
	
	
	$('#labelColId').text('대분류 코드 : ');
	$('#labelColNm').text('대분류 명 : ');
	$('#pCd').val('');
	
	resetStart();
}
function detailView(row) {
	resetStart();
	$('#jqxgrid2').jqxGrid('clearselection');
	
	$('#jqxgrid1').jqxGrid('selectrow', row);
	var rowdata = $("#jqxgrid1").jqxGrid("getrowdata", row);
	var pCd = rowdata['COML_COD'];
	var pNm = rowdata['COML_NM'];
	
	$('#labelColId').text('대분류 코드 : ' + pCd);
	$('#labelColNm').text('대분류 명 : ' + pNm);
	$('#pCd').val(pCd);
	
	getChild(pCd);	
}
function resetStart() {
	$('#saveForm').jqxValidator('hide');
	$("#ac").val("INSERT");
	$("#cCd").attr('readonly', false);
	$('#cCd').val('');
	
	$('#jqxgrid2').jqxGrid('clearselection');
	
	document.saveForm.reset();
}
function getChild(pCd2) {
	var listData = $i.post("./getExcelPgmDQCategory", { category:'C', pCd:pCd2, cCd:'', useYn:'', searchText:''});
	// param : 대/중분류(P/C), 대분류코드, 중분류코드, 사용여부, 검색어
	listData.done(function(res){
		var $jqxgrid2 = $('#jqxgrid2');
		$jqxgrid2.jqxGrid('source')._source.localdata = res;
		$jqxgrid2.jqxGrid('updatebounddata');
	});
}
function detailChildView(row) {
	$('#saveForm').jqxValidator('hide');
	var rowdata = $("#jqxgrid2").jqxGrid("getrowdata", row);

	$("#ac").val("UPDATE");
	$("#cCd").attr('readonly', true);
	
	$('#cCd').val(rowdata['COM_COD']);
	$('#cNm').val(rowdata['COM_NM']);
	$('#useYn').val(rowdata['USE_YN']);
	$('#sortOrder').val(rowdata['SORT_ORDER']);
}
function saveStart() {
	$('#saveForm').jqxValidator('hide');
	if($('#pCd').val() == '') {
		$i.dialog.alert('SYSTEM', '대분류를 먼저 선택하세요.');
		return;
	}
	
	var ruleinfo = null;
	ruleinfo = [
		{input: '#cCd', message:'중분류 코드를 입력하세요!', action:'keyup, blur', rule:'required'},
		{input: '#cCd', message:'10자이내로 입력하세요!', action:'keyup, blur', rule:'maxLength=10'},
		{input: '#cNm', message:'중분류 명을 입력하세요!', action:'keyup, blur', rule:'required'},
		{input: '#sortOrder', message:'정렬순서는 숫자로 입력하세요!', action:'keyup, blur', rule:'number'}
		/* ,
		{input: '#sortOrder', message:'정렬순서를 입력하세요!', action:'keyup, blur', rule:'required'},
		{input: '#sortOrder', message:'정렬순서는 숫자로 입력하세요!', action:'keyup, blur', rule:'number'}
		*/
	];
	
	$("#saveForm").jqxValidator({
		rules : ruleinfo,
		position: 'bottom'
	});
	
	if($("#saveForm").jqxValidator('validate') == true) {
		
		$("#category").val("C");
		
		if($("#sortOrder").val() == '') {
			$("#sortOrder").val(0);
		}
		
		if(!checkStringFormat($("#cCd").val())){
			$i.dialog.warning('SYSTEM', '중분류 코드에 특수문자를 입력할 수 없습니다!');
			return false;
		}
		/*else if(!checkStringFormat($("#cNm").val())){
			$i.dialog.warning('SYSTEM', '중분류 명에 특수문자를 입력할 수 없습니다!');
			return false;
		}*/
		var method = $("#saveForm").attr("method");
		var action = $("#saveForm").attr("action");
		var data = $("#saveForm").serialize();
		
		$.ajax({
			type:method,
			url:action,
			data:data,
			async:false,
			success:function(res){
				$i.dialog.alert('SYSTEM', '저장되었습니다.');
				resetStart();
				getChild($('#pCd').val());
			},error:function(jqXHR,textStatus,errorMessage){
				$i.dialog.error('SYSTEM', '저장에 실패하였습니다.');
			}
		});	
	}	
	
}
function checkStringFormat(string) { 
	     //var stringRegx=/^[0-9a-zA-Z가-힝]*$/; 
	     var stringRegx = /[~!@\#$%<>^&*\-=+_\’\'\"]/gi; 
	     var isValid = true; 
	     if(stringRegx.test(string)) { 
	       isValid = false; 
	     } 
	        
	     return isValid; 
}
function delStart() {
	if($('#cCd').val() == '') {
		$i.dialog.alert('SYSTEM', '중분류를 선택하세요');
		return;
	}
	$i.dialog.confirm('SYSTEM','중분류 코드를 삭제하시겠습니까?',function(){
		$("#category").val("C");
		$("#ac").val("DELETE");
		
		var method = $("#saveForm").attr("method");
		var action = $("#saveForm").attr("action");
		var data = $("#saveForm").serialize();
		
		$.ajax({
			type:method,
			url:action,
			data:data,
			async:false,
			success:function(res){
				$i.dialog.alert('SYSTEM', '삭제되었습니다.',function(){
					resetStart();
					getChild($('#pCd').val());
				});
			},error:function(jqXHR,textStatus,errorMessage){
			}
		});
	});
}
</script>
</head>
<body class="blueish">
	<div class="wrap" style="width:98%; min-width:1160px; margin:0 10px;">
		 	<div class="iwidget_label header" style="width:100%; margin:10px 0; height:27px;">
					<div class="label type1 f_left">프로그램명 :</div>
					<input type="text" id="sText" name="sText" style="margin-left:5px;"/>
					
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='searchButton' width="100%" height="100%" />
						</div>
						<!--buttonStyle--> 
					</div>
		  	</div>
		  	<div class="i-DQ-contents content" style="width:100%; height:100%; float:left;margin-top:10px;">
			  	<div class="left_Area" style="width:30%; height:100%; float:left; margin-right:2%;">
			  		<input type="hidden" id="goColId" name="goColId" />
			  		<input type="hidden" id="goColNm" name="goColNm" />
					<div class="iwidget_grid" style= "float:right; width:100%; height:505px !important;">
						<div id="jqxgrid1" class="grid2" style=" width:100%; height:100%; text-align:center !important"> </div>
					</div>
				</div>
				<div class="right_Area" style="width:68%; height:100%; float:left;">
					<div class="iwidget_label" style="width:100%; float:left; margin-bottom:10px;  ">
						<div class="label type2 f_left">
							<label id="labelColId" name="labelColId">대분류 코드 : </label></div>
						<div class="label type2 f_left" style="margin-left:10px;">
							<label id="labelColNm" name="labelColNm">대분류 명 : </label></div>
					</div>
					<div class="iwidget_grid" style= "float:right; width:100%; height:380px;margin-bottom:20px !important;">
						<input type="hidden" id="colId1" name="colId1" />
						<div id="jqxgrid2" class="grid2" style="width:100%; height:100%;" > </div>
					</div>
					<div class="table_Area" style="width:100%; height:100%; float:left; margin-top:0px; ">
						<form id="saveForm" name="saveForm" action="${WWW.IPORTAL}/ExcelPgmDQ/pgmCategory" method="post" style="margin-top:10px">
							<input type="hidden" id="category" name="category" />
							<input type="hidden" id="ac" name="ac" />
							<input type="hidden" id="pCd" name="pCd" />
							
							<table width="100%" class="i-DQ-table" cellspacing="0" cellpadding="0" border="0">
								<tbody>
									<colgroup>    
										<col width="20%" />
										<col width="40%" />
										<col width="20%" />
										<col width="20%" />
									</colgroup> 	
									<thead>
										<tr> 		
											<th><span style="color:red; font-size:12px; vertical-align:-2px;">＊</span>중분류 코드</th> 		
											<th><span style="color:red; font-size:12px; vertical-align:-2px;">＊</span>중분류 명</th> 		
											<th><span style="color:red; font-size:12px; vertical-align:-2px;">＊</span>사용여부</th>
											<th>정렬순서</th> 		
										</tr>
									</thead>
								<tbody> 	
									<tr>
										<td><input type="text" id="cCd" name="cCd" class="inputtable" style="width:93%;" /></td> 		
										<td><input type="text" id="cNm" name="cNm" class="inputtable" style="width:94%;" /></td>
										<td style="text-align: center;">
											<select class="styled_select" id="useYn" name="useYn">
												<option value="Y">Y</option>	
												<option value="N">N</option>	
											</select>
										</td>
										<td><input type="text" id="sortOrder" name="sortOrder" class="inputtable" style="width:90%; text-align: right; padding-right: 5px;" /></td>
									</tr> 			
								</tbody>
							</table>   
							<div class="label-4" style="float:left;margin-top:10px;"><p class="iwidget_grid_tip">셀에 마우스 클릭시 편집 및 선택할 수 있습니다.</p></div>
								<div class="button-bottom" style="float:right; margin-top: 10px;margin-right:5px;">
									<div class="button type2" style="float:left; height:25px; ">
										<input type="button" value="저장" id='saveButton' width="100%" height="100%" style="margin-left: 0px !important;"/>
									</div>
									<div class="button type2" style="float:left; height:25px; ">
										<input type="button" value="삭제" id='delButton' width="100%" height="100%" style="margin-left: 0px !important;"/>
									</div>
									<div class="button type2" style="float:left; height:25px; ">
										<input type="button" value="초기화" id='resetButton' width="100%" height="100%" style="margin-left: 0px !important;"/>
									</div>
								</div>
						</form>
					</div>
				</div>
			</div>
	</div>
</body>
</html>