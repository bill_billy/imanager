<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.iplanbiz.iportal.config.WebConfig"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<title>엑셀 자료 조회</title>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquercy/styles/jquery-ui-1.10.4.custom.css" type="text/css">
<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
<script type="text/javascript">
<%
String idqUrl = WebConfig.getIDQUrl();
String acctId = WebConfig.getAcctID();
%>
var step1,step2;
var IDQURL = "<%=idqUrl%>";
var ACCTID = "<%=acctId%>";
function setParentCombobox(value) {
	step1 = $i.post("/ExcelPgmDQ/getParentCombobox");
	step1.done(function(pRes){
		var pSource =
		{
			localdata:pRes,
    		dataType: 'json',
			dataFields: [
				{ name: 'P_CD'},
				{ name: 'P_NM'}
			]
		};
		
		var pAdapter = new $.jqx.dataAdapter(pSource);
		
		$('#parentCombobox').jqxComboBox({source: pAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'P_NM', valueMember: 'P_CD', dropDownWidth: 150, dropDownHeight: 150, width: 150, height: 22,  theme:'blueish' });
 
		if(value != '') {
			var item = $('#parentCombobox').jqxComboBox('getItemByValue', value);
			$("#parentCombobox").jqxComboBox('selectItem',item);
		}else{
			$("#parentCombobox").jqxComboBox('selectIndex', 0 );
		}
	});
}
function setChildCombobox(value) {
	step2 = $i.post("/ExcelPgmDQ/getChildCombobox",{pCd:$('#parentCombobox').val()});
	step2.done(function(cRes){
		var cSource =
		{
			localdata: cRes,
    		dataType: 'json',
			dataFields: [
				{ name: 'C_CD'},
				{ name: 'C_NM'}
			]
		};
		var cAdapter = new $.jqx.dataAdapter(cSource);
		
		$('#childCombobox').jqxComboBox({source: cAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'C_NM', valueMember: 'C_CD', dropDownWidth: 150, dropDownHeight: 150, width: 150, height: 22,  theme:'blueish' }); 
		if(value != '') {
			var item = $('#childCombobox').jqxComboBox('getItemByValue', value);
			$("#childCombobox").jqxComboBox('selectItem',item);
		}else{
			$("#childCombobox").jqxComboBox('selectIndex', 0 );
		}
		
	});
}
function allErrList(){
	var list = $i.post("/ExcelPgmDQData/getExcelPgmDQData",{pCd:$('#parentCombobox').val(),cCd:$('#parentCombobox').val(),pgmId:$('#parentCombobox').val()});
	list.done(function(res){

		var item = res['column'];
		
		var rescolumns = [];
		
		var dataFields = [{ name: 'IPLANBIZ_DATAROW_ID'}];
		var columns = [{ text: 'ROW_ID', datafield:'IPLANBIZ_DATAROW_ID'}];
/*		
		for(var i = 0; i < item.length; i++) {
			rescolumns.push(item[i]);
		}  
     	
		  
		dataFields.push({ name: 'IPLANBIZ_DATAROW_ID'});
		columns.push({ text: 'ROW_ID', datafield:'IPLANBIZ_DATAROW_ID' , hidden:true}); 
		if(res.column.length==0){
	 		dataFields.push({ name: 'ERRCHECK'});
	 		columns.push({ text: '오류현황', datafield:'a', align:'center', cellsalign: 'center'});			
		}else{
			for(var i = 0; i < rescolumns.length; i++) {
				dataFields.push({ name: rescolumns[i]});
				columns.push({ text: rescolumns[i], datafield:rescolumns[i], align:'center', cellsalign: 'center'});
			}
		}
		var source =
		{
			localdata: res.data,
    		dataType: 'json',
			dataFields: dataFields
		};
		*/
		var source =
		{
			localdata: res,
    		dataType: 'json',
			dataFields: dataFields
		};
		var dataAdapter = new $.jqx.dataAdapter(source);
		
		
		$("#allErrGrid").jqxGrid({
			width: '100%',
			height: '100%',
			altrows:true,
			pageable: true,
			source: dataAdapter,
			theme:'blueish',
			pagesize:100,
			pagesizeoptions:['100','200','300'],
			//editable: true,
			columnsresize: true,
			columns: columns
		});
		
		//페이지 디폴스 선택 숫자 안보임...
		setPagerLayout("allErrGrid");
		
	});
}
function searchStart() {
	
	$("#jqxTabs").jqxTabs("select",0);
	var pgmid = $("#pgmId").val();
	
	$('#listGrid').jqxGrid('clearselection');
	$('#detailGrid').jqxGrid('clearselection');
	$('#allErrGrid').jqxGrid('clearselection');
	
	
	if($("#systemid").val() != ""){
		var systemid = $("#systemid").val();
		$.ajax({
			url : IDQURL+"/Scheduler/IQFE1000?acct_id="+ACCTID+"&systemid="+systemid,
			dataType : "jsonp",
			jsonp : "callback",
			success : function(d){
			},
			complete:function(data){
			},
			error:function(err){
			}
		});
	}else{
		callbackList(0);
	}
}
function callbackList(errdata){   
	var source = 
	{ 
			localdata:errdata.MAINLIST,
			datatype:"json",
			datafields:[
			     { name: 'brid'},
			     { name: 'brnm'},
			     { name: 'errcnt'},
			     { name: 'brdef'}
			]
			
	};
	var dataAdapter = new $.jqx.dataAdapter(source);
	var detail = function(row,datafieId,value){
    	var brid = $("#listGrid").jqxGrid("getrowdata", row).brid;     
    	var brNm = $("#listGrid").jqxGrid("getrowdata", row).brnm;
    	var link = "<a href=\"javaScript:detailErr('"+brid+"','"+brNm+"')\" style='color:black;'>" + value + "</a>";
    	return "<div style='text-align:left; padding-bottom:2px; margin-top:5px;margin-left:10px; color:black; text-decoration: underline;'>" + link + "</div>";
    };
    var rightAlign = function(row,datafield,value){
    	return "<div style='text-align:left; padding-bottom:2px; margin-top:5px;margin-left:10px; color:black;'>" + value + "</div>";
    }
	$("#listGrid").jqxGrid({
		width: '100%',
		height: '100%',
		altrows:true,
		pageable: true,
		source: dataAdapter,
		pagesize:100,
		pagesizeoptions:['100','200','300'],
		theme:'blueish',
		//editable: true,
		columnsresize: true,
		columns: [
                  { text: '업무규칙명', dataField: 'brnm', width: '30%', align:'center', cellsalign: 'center' ,cellsrenderer:detail},
                  { text: '오류개수',  dataField: 'errcnt', width: '20%', align:'center',  cellsalign: 'right'},
                  { text: '설명',  dataField: 'brdef', width: '50%', align:'center',  cellsalign: 'center', cellsrenderer : rightAlign}
				  
                ],
	});
	setPagerLayout("listGrid");
	callbackRowId(0);
}
function detailErr(brId, brNm){
	$("#upmuNm").html(brNm);
	$.ajax({ 
		url : IDQURL+"/Scheduler/IQFE1001?acct_id="+ ACCTID +"&brid="+brId,
		dataType : "jsonp",
		jsonp : "callback",
		success : function(d){

		},
		complete:function(data){  
			//alert(data);
		},
		error:function(err){
			//alert(err);
		}
	});
}
function callbackRowId(errdata){
	var inputRowId = "";
	
	var $listgrid = $('#listGrid');
	var selectedrowindex = $listgrid.jqxGrid('selectedrowindex');
	var rowdata = $listgrid.jqxGrid('getrowdata', 0);
	
	if(errdata == 0 && (selectedrowindex == -1 || selectedrowindex == 0) && rowdata != undefined) {
		detailErr(rowdata.brid, rowdata.brnm);
		$('#listGrid').jqxGrid('selectedrowindex', 0);
		return false;
	}
	
	if(errdata == "0"){
		inputRowId = 0;
	}else{
		var data = errdata.ROWID.length;
		for(var i=0;i<data;i++){
			if(i == data-1){
				inputRowId += "(1,'"+errdata.ROWID[i].rowid + "')";	
			}else{
				inputRowId += "(1,'"+errdata.ROWID[i].rowid + "'),";
			}
		}	
	}
	var list = $i.post("/ExcelPgmDQData/getExcelPgmDQDataRowId",{pCd:$('#parentCombobox').val(),cCd:$('#childCombobox').val(),rowId:inputRowId});
	list.done(function(res){
		
		
		var item = res['column'];
		console.log(item);
		var rescolumns = [];
		
		var dataFields = [{ name: 'IPLANBIZ_DATAROW_ID'}];
		var columns = [{ text: 'ROW_ID', datafield:'IPLANBIZ_DATAROW_ID' }];
		/*
		for(var i = 0; i < item.length; i++) {
			rescolumns.push(item[i]);
		}  
		dataFields.push({ name: 'IPLANBIZ_DATAROW_ID'});
		columns.push({ text: 'ROW_ID', datafield:'IPLANBIZ_DATAROW_ID' , hidden:true});
		if(inputRowId == 0){
			dataFields.push({ name: 'ERRCHECK'});
			columns.push({ text: '오류현황', datafield:'a', align:'center', cellsalign: 'center'});
		}else{
			for(var i = 0; i < rescolumns.length; i++) {
				dataFields.push({ name: rescolumns[i]});
				columns.push({ text: rescolumns[i], datafield:rescolumns[i], align:'center', cellsalign: 'center'});
			}	
		}
		var source =
		{
			localdata: res.data,
    		dataType: 'json',
			dataFields: dataFields
		};*/
		var source =
		{
			localdata: res,
    		dataType: 'json',
			dataFields: dataFields
		};
		var dataAdapter = new $.jqx.dataAdapter(source);
		
		
		$("#detailGrid").jqxGrid({
			width: '100%',
			height: '100%',
			altrows:true,
			pageable: true,
			source: dataAdapter,
			pagesize:100,
			pagesizeoptions:['100','200','300'],
			theme:'blueish',
			//editable: true,
			columnsresize: true,
			columns: columns
		});
		//setPagerLayout("detailGrid"); <%--체인지 이벤트가 안되서 트리거로 처리함--%>
		$('#detailGrid').on('change', function() {
			setPagerLayout("detailGrid");
		});
		$('#detailGrid').trigger('change');
		
	});
}
function errcheckcell(row,datafield,value,defaulthtml,columnproperty,rowdata){ 
	if(value==true){
		return defaulthtml.replace("true","").replace("</div>","")+"<img src='${WEB.IMG}/icon/icon_munst_red.png'/></div>"; 
	}
	else
		return defaulthtml; 
}
function downStart() {//다운로드
	
	var pgmId = $('#pgmId').val();
	var pgmNm = $('#pgmNm').val();
	
	if(pgmId == '') {
		$i.dialog.alert('SYSTEM', '프로그램 ID가 존재하지 않습니다.\n프로그램을 먼저 선택하세요.');
		return;
	}
	
	var url = "${WWW.ROOT}/ExcelPgmDQ/download?pgmId="+pgmId+"&pgmNm="+pgmNm+"&acctid="+ACCTID;
	window.open(url, 'ExcelDownload_postgresql', 'width=500,height=300, resizable=no, scrollbars=auto, left=0, top=0');
	
}
function upStart() {//업로드
	
	var pgmId = $('#pgmId').val();
	var pgmNm = $('#pgmNm').val();
	
	if(pgmId == '') {
		$i.dialog.alert('SYSTEM', '프로그램 ID가 존재하지 않습니다.\n프로그램을 먼저 선택하세요.');
		return;
	}
	
	var url = "${WWW.ROOT}/ExcelPgmDQ/upload?pgmId="+pgmId+"&pgmNm="+pgmNm+"&acctid="+ACCTID;
	window.open(url, 'ExcelUpload_postgresql', 'width=500,height=300, resizable=no, scrollbars=auto, left=0, top=0');
}
function checkStart() {//오류체크
	var pgmid = $("#pgmId").val();
	var systemid = $("#systemid").val();
	if(pgmid!=""&&systemid!=""){
		//$iv.progress.show('Information', '오류체크 중...');
		// hide 는 callback 함수에서 실행 
		$.ajax({ 
			url : IDQURL+"/Scheduler/BR_STANDALONE_EXECUTE?acct_id="+ACCTID+"&systemid="+systemid,
			dataType : "jsonp",
			jsonp : "callback",
			success : function(d){
			},
			complete:function(data){
			},
			error:function(err){
			}
		});
	}else{
		$i.dialog.alert('SYSTEM', 'i-DQ와 연동된 프로그램을 선택해 주십시오.');
	}
	 
}
function callback(errdata){
	$iv.progress.hide();
		
	searchStart();
	$i.dialog.alert('SYSTEM', "에러 데이터가 총 "+errdata.ERRDATA.length+"개 검출 되었습니다.");
	/*
	var resultdata=[];
	if(errdata.ERRDATA!=null){
		 
		var griddata = Enumerable.From($("#allErrGrid").jqxGrid("getrows")).Select(function(c){return c.IPLANBIZ_DATAROW_ID;}).ToArray();
		var errdata = Enumerable.From(errdata.ERRDATA).Select(function(c){return c.rowid;}).ToArray();
		resultdata = intersect(griddata,errdata);
		var erridx = 0;
		searchStart();
	}
	alert("에러 데이터가 총 "+resultdata.length+"개 검출 되었습니다.");
	searchStart();
	*/
}
function intersect(a, b) {
	  var tmp={}, res=[];
	  for(var i=0;i<a.length;i++) tmp[a[i]]=1;
	  for(var i=0;i<b.length;i++) if(tmp[b[i]]) res.push(b[i]);
	  return res;
	}
function runStart() {//적용
	var pgmId = $('#pgmId').val();
	var pgmNm = $('#pgmNm').val();
	
	if(pgmId == '') {
		$i.dialog.alert('SYSTEM', "프로그램 ID가 존재하지 않습니다.\n프로그램을 먼저 선택하세요.");
		return;
	}
	
	var url = "${WWW.ROOT}/ExcelPgmDQ/run?pgmId="+pgmId+"&pgmNm="+pgmNm;
	window.open(url, 'ExcelRun_postgresql', 'width=400, height=160,resizable=no,scrollbars=auto, left=0, top=0');
	
}
function searchProgram() {
	var pCombo = $('#parentCombobox').jqxComboBox('getSelectedItem');
	var cCombo = $('#childCombobox').jqxComboBox('getSelectedItem');
	var param = '?';
	param += 'pCd=' + pCombo.value;
	param += '&pNm=' + pCombo.label;
	param += '&cCd=' + cCombo.value;
	param += '&cNm=' + cCombo.label;
	
	var url = '${WWW.ROOT}/ExcelPgmDQData/excelPgmDQSearchProgram'+param;
	window.open(url, 'SearchExcelProgram_postgresql', 'width=630,height=420, resizable=no, scrollbars=auto, left=0, top=0');
}
function setPagerLayout(selector) {
	
	var pagesize = $('#'+selector).jqxGrid('pagesize');
	
	var w = 49; 
		
	if(pagesize<100) {
		w = 44;
	} else if(pagesize>99&&pagesize<1000) {
		w = 49;
	} else if(pagesize>999&&pagesize<10000) {
		w = 54;
	}
	
	//디폴트 셋팅
	$('#gridpagerlist'+selector).jqxDropDownList({ width: w+'px' });
	
	//체인지 이벤트 처리
	$('#'+selector).on("pagesizechanged", function (event) {
		var args = event.args;
		
		if(args.pagesize<100) {
			$('#gridpagerlist'+selector).jqxDropDownList({ width: '44px' });
		} else if(args.pagesize>99 && args.pagesize<1000) {
			$('#gridpagerlist'+selector).jqxDropDownList({ width: '49px' });
		} else if(args.pagesize>999 && args.pagesize<10000) {
			$('#gridpagerlist'+selector).jqxDropDownList({ width: '54px' });
		} else {
			$('#gridpagerlist'+selector).jqxDropDownList({ width: 'auto' });
		}
		
	});
}
function progmReset(){
	$("#pgmId").val('');
	$("#pgmNm").val('');
}
$(this).ready(function() {
	//button
	$('#jqxTabs').jqxTabs({ width: '100%', height: '100%', position: 'top',theme : 'blueish'});
	$("#searchButton").jqxButton({ width : '', theme : 'blueish' });
	$("#downButton").jqxButton({ width : '', theme : 'blueish' });
	$("#upButton").jqxButton({ width : '', theme : 'blueish' });
	$("#checkButton").jqxButton({ width : '', theme : 'blueish' });
	$("#runButton").jqxButton({ width : '', theme : 'blueish' });

	//set combobox
	setParentCombobox(''); //대분류
	$.when(step1).done(function(){
		setChildCombobox(''); //중분류
	});
	
	//set input
	$("#pgmId").jqxInput({height: 22, width:100 , minLength: 1, theme:'blueish' });
	$("#pgmNm").jqxInput({height: 21, width: 250, minLength: 1, theme:'blueish' });	
	
	//bind event
	$('#searchButton').click(function() { searchStart(); });
	$('#downButton').click(function() { downStart(); });
	$('#upButton').click(function() { upStart(); });
	$('#checkButton').click(function() { checkStart(); });
	$('#runButton').click(function() { runStart(); });
	
	$('#parentCombobox').bind('select', function(e) {
		if(e.args) {
			setChildCombobox('');	
		}
	});
	
	//init
	searchStart();
	
});
</script>
<style type="text/css">
.jqx-tabs-content-element{
	overflow:hidden !important;
}
.jqx-tabs-content-blueish{
	background:none !important;
	border-top:1px solid #91a3b4 !important;
}
.jqx-tabs-blueish{
	border-bottom:none !important;
}
.jqx-tabs-titleContentWrapper {
	padding-left:2px !important;
}
</style>
</head>
<body class='blueish i-DQ'>
<div class="wrap" style="width:98%;min-width:1160px; margin:0 10px;">
		<div class="iwidget_label header" style="margin:10px 0 !important; height:27px;">
			<div class="label type1 f_left">대분류 :</div>
			<div class="combobox f_left" id='parentCombobox'></div>
			<div class="label type1 f_left">중분류 :</div>
			<div class="combobox f_left" id='childCombobox' onchange="progmReset();"></div>
			<input id="systemid" type="hidden"/>
			<div class="label type1 f_left">프로그램 :</div>
			<input type="text" class="f_left" id="pgmId" readonly="readonly" style="margin:0 5px;"/>
			<input type="text" class="f_left" id="pgmNm" readonly="readonly"/>
			<div class="image-button f_left"> <a onclick="javascrit:searchProgram();" style="cursor: pointer;"><img src="../../resources/img/img_icons/search_icon.png" alt="조회"></a> </div>
			<div class="group_button f_right">
				<div class="button type1 f_left">
					<input type="button" value="조회" id='searchButton' width="100%" height="100%" />
				</div>
					<!--buttonStyle--> 
			</div>
			<!--headerbutton-->
		</div>
	<div class="content" style="width:100%; height:100%; float:left;">
		<!--iwidget_label headertop-->
		<div style="width:100%;">
				<div class="button-bottom" style="float:right;">
				<div class=" button type2" style="float:left; height:25px;">
						<input type="button" value="다운로드" id='downButton' width="100%" height="100%" />
					</div>
				<div class=" button type2" style="float:left; height:25px; ">
						<input type="button" value="업로드" id='upButton' width="100%" height="100%" />
					</div>
				<div class=" button type2" style="float:left; height:25px;">
						<input type="button" value="오류체크" id='checkButton' width="100%" height="100%" />
					</div>
				<div class=" button type2" style="float:left; height:25px;">
						<input type="button" value="적용" id='runButton' width="100%" height="100%" />
					</div>
			</div>
				<!--bottuon-bootom-->
			<div id='jqxWidget'>
				<div id='jqxTabs'>
					 <ul>
		                <li style="margin-left: 0px;padding-left:2px;width:30px;display:inline-block;text-align: center;">오류</li>
		                <li style="margin-left: 0px;width:30px;" onclick="allErrList();">전체</li>
		            </ul>
		            <div style="margin-top: 10px; float:left; width:100%;">
						<div class="iwidget_grid" style="float:left;margin:0px auto !important; margin-bottom:10px !important;  width:100%; height:150px;">
							<div id="listGrid" style=" width:100%; height:100%; "> </div>
						</div>
						<div class="iwidget_label" style="margin:0 !important; ">
							<div class="label type2 f_left">업무규칙 : <span id="upmuNm"></span></div>
						</div>
					<!--iwidget_label-->
						<div class="iwidget_grid" style="float:left;margin:0px auto !important; margin-bottom:10px !important;  width:100%; height:300px;">
							<div id="detailGrid" style=" width:100%; height:100%"> </div>
						</div>
					</div>
					<div style="margin-top: 10px;">
						<div class="iwidget_grid" style="float:left;margin:0px auto !important; margin-bottom:10px !important;  width:100%; height:500px;">
							<div id="allErrGrid" style=" width:100%; height:100%;"> </div>
						</div>
					</div>
				</div>
				<!--iwidget_grid --> 
			</div>
		</div>
		<!--grid area--> 
	</div>
		<!--contents--> 
	</div>
<!--wrap-->
</body>

</html>