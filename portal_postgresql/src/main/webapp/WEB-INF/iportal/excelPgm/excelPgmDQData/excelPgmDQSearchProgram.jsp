<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<title>엑셀 자료 조회</title>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
<script type="text/javascript">
function throwParent(pgmId, pgmNm,systemid) {
	
	$('#pgmId',opener.document).val(pgmId);
	$('#pgmNm',opener.document).val(pgmNm);
	$("#systemid",opener.document).val(systemid); 
	opener.searchStart();
	//opener.allErrList();
	self.close();
	
}
function searchStart() {
	var list = $i.post("/ExcelPgmDQData/getProgramList",{pCd:'${param.pCd}',cCd:'${param.cCd}',searchText:$('#searchText').val()});
	list.done(function(res){
		// prepare the data
		var source =
		{
			datatype: "json",
			datafields: [
				{ name: 'PGM_ID', type: 'string' },
				{ name: 'PGM_NM', type: 'string' },
				{ name: 'PGM_EMP', type: 'string' },
				{ name: 'PGM_EMP_NM', type: 'string' },
				{name:'SYSTEM_ID',type:'string'}
			],
			localdata: res,
			updaterow: function (rowid, rowdata, commit) {
				commit(true);
			}
		};
		
		var cellsrenderer = function(row, datafield, value, defaulthtml, columnproperties, rowdata) {
 
           	var link = "<a href=\"javaScript:throwParent('"+rowdata.PGM_ID+"', '"+rowdata.PGM_NM+"','"+rowdata.SYSTEM_ID+"');\">" + value + "</a>";
           	return "<div style='text-align:left; padding-bottom:2px; margin-top:5px;margin-left:10px; text-decoration: underline;'>" + link + "</div>";
        };
		
		var dataAdapter = new $.jqx.dataAdapter(source);
		
		$("#listGrid").jqxGrid({
			width: '100%',
			height: '100%',
			altrows:true,
			pageable: true,
			source: dataAdapter,
			theme:'blueish',
			pagesize:100,
			pagesizeoptions:['100','200','300'],
			columnsresize: true,
			columns: [
				{ text: 'SYSTEM_ID', datafield: 'SYSTEM_ID', hidden:true },
				{ text: 'ID', datafield: 'PGM_ID', width: '15%', align:'center', cellsalign: 'center' },
				{ text: '프로그램명', datafield: 'PGM_NM', width: '60%', align:'center', cellsalign: 'left', cellsrenderer: cellsrenderer },
				{ text: '담당자', datafield: 'PGM_EMP_NM', width: '25%', align:'center', cellsalign: 'center' }
			]
		});
		
		
	});
}
$(this).ready(function() {
	//button
	$("#searchButton").jqxButton({ width : '', theme : 'blueish' });
	
	//input
	$("#searchText").jqxInput({height: 21, width:450 , minLength: 1, theme:'blueish' });
	
	
	//bind event
	$('#searchButton').click(function() { searchStart(); });
	$('#searchText').on('keydown',
		function() {
			if(event.keyCode==13) {
				searchStart();
				return false;
			}
		}
	);
	//init
	$('#pCd').text('${param.pNm}');
	$('#cCd').text('${param.cNm}');

	searchStart();
});
</script>
</head>
<body class='blueish i-DQ'>
<div class="wrap" style="width:600px !important; margin:0 !important; padding:0 !important; ">
	<div class="content" style="width:100%; height:100%; float:left; padding:10px; ">
		<div class="iwidget_label  header" style="margin:0 !important; width:100%; height:60px;  ">
			<div style=" float:left;width:100%; margin-bottom:10px;">
				<div class="label type1 f_left" style="float:left; line-height:21px !important;">대분류: <span class="colorchange" id="pCd">학사</span></div>
				<div class="label type1 f_left" style="float:left; line-height:21px !important;">중분류: <span class="colorchange" id="cCd">학적</span></div>
			</div>
			<div style="margin-top:10px;width:100%;">
				<div class="label type1 f_left" style="line-height:21px !important;">프로그램</div>
				<div  style="float: left; margin-left:5px; margin-right:5px;width:70% !important;" >
					<input type="text" id="searchText"/>
				</div>
				<div class="headerbutton" style="float:right;">
					<div class="button type1" style="float:left; height:25px; margin-right:5px;">
						<input type="button" value="조회" id='searchButton' width="100%" height="100%" />
					</div>
					<!--buttonStyle--> 
				</div>
				<!--headerbutton--> 
			</div>
		</div>
		<!--iwidget_label headertop-->
	
		<div  style=" clear:both;width:100%; margin:10px auto !important; ">
			<div class="iwidget_grid" style="float:left;margin:10px auto !important; margin-bottom:10px !important;  width:100%; height:280px;">
				<div id="listGrid" style=" width:100%; height:100%; "></div>
			</div>
		<!--iwidget_grid --> 
		</div>
		<!--grid area--> 
	</div>
	<!--contents--> 
</div>
<!--wrap-->
</body>
</html>