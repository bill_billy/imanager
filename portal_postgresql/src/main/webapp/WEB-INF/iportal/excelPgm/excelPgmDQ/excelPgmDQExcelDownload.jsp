<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>엑셀 다운로드</title>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
		<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
		<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
		
		<link rel="stylesheet" href="../../resource/css/iplanbiz/table.css" type="text/css" />
		<script language="javascript" type="text/javascript">
		var resultMessage = "${errMessage}";
		function formClear(form){
			document.getElementById("oldPgmId").value = "";
			document.getElementById("saveFormAc").value = "save";
			resetForm(form);
		}
		function downloadStart(){
			if($("#yymmdd").val().replace(/ /ig, '') != ""){
				document.downloadForm.submit();
			}else{
				$i.dialog.alert('SYSTEM', "기준일자를 입력하세요."); 
			}
		}
		$(window).ready(function(){ 
			if(resultMessage!=""){
				$i.dialog.error('SYSTEM', resultMessage); 
			}
			$("#pgmNmLabel").html(decodeURI("${param.pgmNm}"));
			$("#jqxButton1").jqxButton({ width: '',  theme:'blueish'});
			$("#jqxButton2").jqxButton({ width: '',  theme:'blueish'});
		});
		</script>
	</head> 
	<body class='blueish i-DQ' style="height:100%;">
		<div class="wrap" style="width:96%; margin:0 auto;">
			<div class="header" style="width:100%; margin-top:30px;">
				<div class="iwidget_label" style="width:100%; float:right; margin-bottom:10px;">
					<div class="label type1 f_left">엑셀 다운로드</div>
				</div>
			</div>
		</div>
		<div class="content" style="width:100%; margin:10px auto !important; margin-top:20px !important;">
			<div class="table_Area table-style" style="width:100%; height:100%; float:left; margin-top:0px; ">
				<form id="downloadForm" name="downloadForm" method="post" action="${WWW.ROOT}/ExcelPgmDQ/download" enctype="multipart/form-data" onsubmit="return false;">
					<input type="hidden" name="ac" id="ac" value="download" />
					<input type="hidden" name="acctid" id="acctid" value="${param.acctid}" />  
					<input type="hidden" name="pgmNm" value="${param.pgmNm}" />
					<input type="hidden" id="pgmId" name="pgmId" value="${param.pgmId}" />
					<table width="100%" class="i-DQ-table2 table_box" cellspacing="0" cellpadding="0" border="0" style="margin-bottom:20px !important;">
						<tr>
							<th class="tableTitle form">프로그램명</th>
							<td class="tableContent_form"><div style="margin-left: 5px;" id="pgmNmLabel">${param.pgmNm}</div></td>	
						</tr>
						<tr>
							<th class="tableTitle form">프로그램ID</th>
							<td class="tableContent_form"><div style="margin-left: 5px;">${param.pgmId}</div></td>	
						</tr>
						<tr>
							<th class="tableTitle form">기준일자<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th>
							<td class="tableContent_form"><input type="text" class="inputtable" id="yymmdd" name="yymmdd" value="${param.yymmdd}" onkeydown="if(event.keyCode == 13){downloadStart();}"/></td>	
						</tr>
						<tr>
							<td colspan="2">
							<span style="float:left; color:red; margin-top:3px;">* 기준일자는 YYYYMMDD 형식으로 작성해주세요.</span>
								<div style="float:right;margin-right:0px;">
									<div class="button type2" style="float:left; height:25px;margin-right:10px; ">
										<input type="button" value="Excel다운로드" id='jqxButton1' width="100%" height="100%" onclick="downloadStart();" />
									</div>
									<div class="button type2" style="float:left; height:25px;margin-right:10px; ">
										<input type="button" value="닫기" id='jqxButton2' width="100%" height="100%" onclick="self.close();" />
									</div>
								</div>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</body> 
</html> 