<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>엑셀 적용</title>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
		<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
		<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
		<link rel="stylesheet" href="../../resource/css/iplanbiz/table.css" type="text/css" />

		<script language="javascript" type="text/javascript">
		var resultMessage = "${errMessage}";
		function formClear(form){
			document.getElementById("oldPgmId").value = "";
			document.getElementById("saveFormAc").value = "save";
			resetForm(form);
		}
		function runStart(gb){
			
			if($('#yymmdd').val().replace(/ /ig, '') == '') {
				$i.dialog.alert('SYSTEM', "기준일자를 입력하세요.");
				return;
			}
			
			if(gb == 1) {
				$('#uploadFormAc').val('DeleteAndInsert');
			} else if(gb == 2) {
				$('#uploadFormAc').val('Insert');
			}
			
			$("#uploadForm").submit();
			layerPopupFull.style.display = "";
		}
		$(window).ready(function(){ 
			if(resultMessage == "정상 처리 되었습니다."){
				$i.dialog.alert('SYSTEM', resultMessage,function(){
					window.close();
				}); 
			}else if(resultMessage!=""){
				$i.dialog.error('SYSTEM', resultMessage); 
			}
			$("#jqxButton1").jqxButton({ width: '',  theme:'blueish'});
			$("#jqxButton2").jqxButton({ width: '',  theme:'blueish'});
			$("#jqxButton3").jqxButton({ width: '',  theme:'blueish'});
		});
		</script>
	</head> 
	<body class='blueish i-DQ'>
		<div class="wrap" style="width:400px !important; height:95px;  margin:0 !important; padding:0 !important; "><!-- 창 사이즈입니다.-->
			<div class="pop" style="width:100%; height:100%;">
				<form id="uploadForm" name="uploadForm" method="post" action="${WWW.ROOT}/ExcelPgmDQ/run">
					<input type="hidden" name="ac" id="uploadFormAc" value="run" />
					<input type="hidden" name="pgmNm" value="${pgmNm}" />
					<input type="hidden" id="pgmId" name="pgmId" value="${pgmId}" />
					<div class="popup_contents"  style="width:100%; height: 40px;margin:0; padding:0;">
						<ul style="margin:0;padding:15px;">
							<li><span><img src="../../resource/css/images/img_icons/popup_icon.png" alt="알림"></span></li>
							<li>적용 하시겠습니까?</li>
						</ul>
					</div>
					<div class="table_Area table-style" style="width:100%; height:100%; float:left; margin-top:0px; ">
						<table width="100%" class="i-DQ-table2 table_box" cellspacing="0" cellpadding="0" border="0" style="margin-bottom:20px !important;">
							<tr>
								<th class="tableTitle form">기준일자<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th>
								<td class="tableContent_form"><input type="text" class="inputtable" id="yymmdd" name="yymmdd" value="${param.yymmdd}"onkeydown="if(event.keyCode == 13){return false;}"/></td>	
							</tr>
							<tr>
								<td colspan="2">
									<span style="float:left; color:red; margin-top:3px;">* 기준일자는 YYYYMMDD 형식으로 작성해주세요.</span>
								</td>
							</tr>
						</table>
					</div>
					<div  style="width:100%; clear:both; margin:5px 0 0 0;"><!-- 하단버튼-->
						<div style="width:243px; margin:0 auto;">
							<div class="button type2" style="float:left; height:25px;margin-right:10px;">
								<input type="button" value="삭제 후 저장" id='jqxButton1' width="100%" height="100%" onclick="runStart(1);" />
							</div>
							<div class="button type2" style="float:left; height:25px;margin-right:10px;">
								<input type="button" value="추가" id='jqxButton2' width="100%" height="100%" onclick="runStart(2);" />
							</div>
							<div class="button type2" style="float:left; height:25px;">
								<input type="button" value="취소" id='jqxButton3' width="100%" height="100%" onclick="self.close();" />
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body> 
</html> 