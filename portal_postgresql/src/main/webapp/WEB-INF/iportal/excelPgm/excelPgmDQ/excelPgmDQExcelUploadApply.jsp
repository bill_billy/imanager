<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>엑셀 업로드</title>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
		<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
		<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
		<link rel="stylesheet" href="../../resource/css/iplanbiz/table.css" type="text/css" />
		<script language="javascript" type="text/javascript">

		var resultMessage = "${errMessage}";
		function formClear(form){
			document.getElementById("oldPgmId").value = "";
			document.getElementById("saveFormAc").value = "save";
			resetForm(form);
		}
		function saveStart(){
			if($('input[name="upFile"]').val() == '') {
				$i.dialog.alert('SYSTEM', '업로드할 파일을 선택하세요.'); 
				return;
			}
			if($("#yymmdd").val().replace(/ /ig, '') == ""){
				$i.dialog.alert('SYSTEM', "기준일자를 입력하세요."); 
				return;
			}
			 
			$("#uploadForm").submit();
			layerPopupFull.style.display = "";
		}
		
		$(window).ready(function(){ 
			if(resultMessage == "정상 처리 되었습니다."){
				$i.dialog.alert('SYSTEM', resultMessage,function(){
					if(opener!=null&&opener.searchStart!=null)
						opener.searchStart(); <%--excelPgmDQ/list.jsp 에 영향 --%>
					window.close();
				}); 
			}else if(resultMessage!=""){
				$i.dialog.error('SYSTEM', resultMessage); 
			}
			
			$("#jqxButton1").jqxButton({ width: '',  theme:'blueish'});
			$("#jqxButton2").jqxButton({ width: '',  theme:'blueish'});
		});
		</script>
		<%
		String attrProgramName = "";
		if(request.getAttribute("pgmNm")!=null)
			attrProgramName = java.net.URLDecoder.decode(request.getAttribute("pgmNm").toString(),"UTF-8");
		else{
			if(request.getParameter("pgmNm")!=null) 
				attrProgramName = request.getParameter("pgmNm"); 
			else
				attrProgramName = "TEST";  
		}   
		%>
	</head> 
	<body class='blueish i-DQ'>
		<div id="layerPopupFull" style="position:absolute; z-index:9;width:100%; height:100%; left:0px; top:0px; display:none; background-color:white;filter:alpha(opacity=60);opacity:.6;">
			<img src="../../resources/img/ajax-loader.gif" style="left:250px; top:150px;position: absolute;"/><!-- 페이지전체영역입니다. -->
		</div>
		<div class="wrap" style="width:96%; margin:0 auto;">
			<div class="header" style="width:100%; margin-top:30px;">
				<div class="iwidget_label" style="width:100%; float:right; margin-bottom:10px;">
					<div class="label type1 f_left" style="float:left;">엑셀 업로드</div>
				</div>
			</div>
		</div>
		<div class="content" style="width:100%; margin:10px auto !important; margin-top:20px !important;">
			<div class="table_Area table-style" style="width:100%; height:100%; float:left; margin-top:0px; ">
				<form id="uploadForm" name="uploadForm" method="post" action="${WWW.ROOT}/ExcelPgmDQ/uploadApply" enctype="multipart/form-data">
					<input type="hidden" name="ac" id="uploadFormAc" value="upload" />
					<input type="hidden" name="acctid" id="acctid" value="${param.acctid}" />  
					<input type="hidden" name="pgmNm" value="${pgmNm}" />
					<input type="hidden" id="pgmId" name="pgmId" value="${pgmId}" />
					<input type="text" value="" style="display: none;"/>
					
					<table width="100%" class="i-DQ-table2 table_box" cellspacing="0" cellpadding="0" border="0" style="margin-bottom:20px !important;">
						<tr>
							<th class="tableTitle form">프로그램명</th>
							<td class="tableContent_form"><div style="margin-left: 5px;"><%=attrProgramName%></div></td>	
						</tr>
						<tr>
							<th class="tableTitle form">프로그램ID</th>
							<td class="tableContent_form"><div style="margin-left: 5px;">${pgmId}</div></td>	
						</tr>
						<tr>
							<th class="tableTitle form">기준일자<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th>
							<td class="tableContent_form"><input type="text" id="yymmdd" name="yymmdd" class="inputtable" value="${yymmdd}" onkeydown="if(event.keyCode == 13){saveStart();};"/></td>	
						</tr>
						<tr>
							<th class="tableTitle form">파일</th>
							<td class="tableContent_form"><div style="margin-left: 5px;"><input type="file" name="upFile" value="${upFile}"/></div></td>
						</tr>
						<tr>
							<td colspan="2">
								<span style="float:left; color:red; margin-top:7px;">* 기준일자는 YYYYMMDD 형식으로 작성해주세요.</span>
								<div style="float:right;margin-top: 3px;margin-right:0px;margin-bottom:3px;">
									<div class="button type2" style="float:left; height:25px;margin-right:10px; ">
										<input type="button" value="Excel업로드" id='jqxButton1' width="100%" height="100%" onclick="saveStart();" />
									</div>
									<div class="button type2" style="float:left; height:25px;margin-right:10px; ">
										<input type="button" value="닫기" id='jqxButton2' width="100%" height="100%" onclick="self.close();" />
									</div>
								</div>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</body> 
</html> 