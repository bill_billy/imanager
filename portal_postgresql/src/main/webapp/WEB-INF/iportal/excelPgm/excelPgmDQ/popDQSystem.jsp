<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.iplanbiz.iportal.config.WebConfig"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<title>진단 시스템 리스트</title>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
<link rel="stylesheet" href="../../resource/css/iplanbiz/table.css" type="text/css" />

<script type="text/javascript">
<%
String idqUrl = WebConfig.getIDQUrl();
String acctId = WebConfig.getAcctID();
%>
var IDQURL = "<%=idqUrl%>";
var ACCTID = "<%=acctId%>";
function callbackMajor(pRes){
	var pSource =
	{ 
		localdata: [{comlcod:'',comlnm:'==선택=='}].concat(pRes.iqfe0010),
		dataType: 'json',
		dataFields: [
			{ name: 'comlcod'},
			{ name: 'comlnm'}
		]
	};
	 
	var pAdapter = new $.jqx.dataAdapter(pSource);
	
	$('#cboMajor').jqxComboBox({ selectedIndex: 0, source: pAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'comlnm', valueMember: 'comlcod', dropDownWidth: 150, dropDownHeight: 150, width: 150, height: 22,  theme:'blueish' });
	/**
	if(value != '') {
		var item = $('#cboMajor').jqxComboBox('getItemByValue', value);
		$("#cboMajor").jqxComboBox({selectedIndex: item.index });
	}
	**/
	$('#cboMajor').on("change",setChildCombobox);
}
   
function setParentCombobox() {
	$.ajax({
		url : IDQURL+"/Scheduler/IQFE0010?acct_id="+ACCTID, 
		dataType : "jsonp", 
		jsonp : "callbackMajor",
		success : function(d){
			
		},
		complete:function(data){
			//alert(data);
		},
		error:function(err){
			//alert(err);
		}
	});
}
function callbackMinor(cRes){
	var cSource =
	{ 
		localdata: cRes.iqfe0011,
		dataType: 'json',
		dataFields: [
			{ name: 'comcod'},
			{ name: 'comnm'}
		]
	};
	var cAdapter = new $.jqx.dataAdapter(cSource);
	 
	$('#cboMinor').jqxComboBox({ selectedIndex: 0, source: cAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'comnm', valueMember: 'comcod', dropDownWidth: 150, dropDownHeight: 150, width: 150, height: 22,  theme:'blueish' });
	$("#cboMinor").on("change",function(){
		searchStart(); 
	});
}
function setChildCombobox() {
	var comlcod = $("#cboMajor").val();
	$.ajax({
		url : IDQURL+"/Scheduler/IQFE0011?acct_id="+ ACCTID+"&major="+comlcod, 
		dataType : "jsonp", 
		jsonp : "callbackMinor",
		success : function(d){
			
		},
		complete:function(data){
			//alert(data);
		},
		error:function(err){
			//alert(err);
		}
	});	
}
function detailView(pgmId) {
	location.href = '${WEB.ROOT}/ExcelPgmDQ/view?pgmId='+pgmId;
}
function downexcel(row) {
	var rowdata = $("#listGrid").jqxGrid("getrowdata", row);
	var pgmId = rowdata.PGM_ID;
	var pgmNm = rowdata.PGM_NM;
	
	if(pgmId == '') {
		alert("프로그램 ID가 존재하지 않습니다.\n프로그램을 먼저 선택하세요.");
		return;
	}
	
	var url = "${WWW.ROOT}/ExcelPgmDQ/download?pgmId="+pgmId+"&pgmNm="+pgmNm;
	windowOpen(url, 'ExcelDownload','width=500,height=300, resizable=no, scrollbars=auto, left=0, top=0');
}
function upexcel(row) {
	var rowdata = $("#listGrid").jqxGrid("getrowdata", row);
	var pgmId = rowdata.PGM_ID;
	var pgmNm = rowdata.PGM_NM;
	
	if(pgmId == '') {
		alert("프로그램 ID가 존재하지 않습니다.\n프로그램을 먼저 선택하세요.");
		return;
	}
	
	var url = "${WWW.ROOT}/ExcelPgmDQ/upload?pgmId="+pgmId+"&pgmNm="+pgmNm;
	windowOpen(url, 'ExcelUpload', 'width=500,height=300, resizable=no, scrollbars=auto, left=0, top=0');
}
function sendparent(systemid,systemnm){
	//console.log("test"+systemid); 
	$("#systemid",opener.document).val(systemid);
	$("#systemnm",opener.document).val(systemnm);
	self.close();
} 
function callback(res){
	var source =
	{
		localdata: res.iqfe0003,
		dataType: 'json',
		dataFields: [
			{ name: 'systemid'},
			{ name: 'systemnm'} 
		]
	}; 
	
	var adapter = new $.jqx.dataAdapter(source);

	var dataAdapter = new $.jqx.dataAdapter(source);
	var detailView = function(row, datafield, value, defaulthtml, columnproperties, rowdata) {
		var systemid = rowdata.systemid;
		var link = "<a href=\"javaScript:sendparent('"+systemid+"','"+rowdata.systemnm+"');\" style='color:black;'>" + value + "</a>";
		return '<div style="text-align:left; margin:4px 0px 0px 10px; text-decoration: underline;">' + link + '</div>';
	};
	$("#listGrid").jqxGrid({
		width: '480px',
     	height: '500px',   
		altrows:true, 
		pageable: true,
		pagesize:100,
		pagesizeoptions:['100','200','300'],
		selectionmode:'singlerow',
    	source: dataAdapter,
		theme:'blueish',
		//editable: true,
     	columnsresize: true,
     	columns: [
			{ text: 'ID', datafield: 'systemid', width: '10%', align:'center', cellsalign: 'center'},
			{ text: '시스템명', datafield: 'systemnm', width: '90%', align:'center', cellsalign: 'left',cellsrenderer:detailView}
		]
	});
	
	$('#listGrid').on('change', function() {
		setPagerLayout("listGrid");
	});
	$('#listGrid').trigger('change');
}  
function searchStart() {
	var comlcod = $("#cboMajor").val();
	var comcod = $("#cboMinor").val();
	$.ajax({
		url : IDQURL+"/Scheduler/IQFE0003?acct_id="+ ACCTID +"&major="+comlcod+"&minor="+comcod, 
		dataType : "jsonp", 
		jsonp : "callback",
		success : function(d){
			
		},
		complete:function(data){
			//alert(data);
		},
		error:function(err){
			//alert(err);
		}
	});
}

$(this).ready(function() {
	//var $searchButton = $("#searchButton");
	
	//button
	//$("#searchButton").jqxButton({ width : '', theme : 'SophisticatedLayout' });

	//set combobox
	setParentCombobox('${param.pCd}'); //대분류
	setChildCombobox('${param.cCd}'); //중분류
	
	searchStart();

	//bind event
	$('#cboMajor').bind('select', function(e) {
		if(e.args) {
			setChildCombobox();	
			$('#cboMinor').jqxComboBox({ selectedIndex: 0});
		}
	});
	
	//$("#searchButton").bind('click', function(e) {
	//	searchStart();
	//});
	

});
</script>
</head>
<body class='blueish i-DQ'>
<div class="wrap" style="width:480px !important; margin:0 !important; padding:0 !important; ">
	<div class="content" style="width:100%; height:100%; float:left; padding:10px; ">
		<div class="header" style="margin:0 !important; height:27px;  ">
			<div class="iwidget_label" style=" float:left;width:100%;">
				<div class="label type1 f_left" style="line-height:21px !important;">대분류</div>
				<div style='float:left; margin:0 10px;' id='cboMajor'  class="table_combobox"></div>
				<div class="label type1 f_left" style="line-height:21px !important;">중분류</div>
				<div style='float:left; margin:0 10px;' id='cboMinor'  class="table_combobox"></div>
				<div class="headerbutton" style="float:right;">
					<div class="button type1" style="float: left; height: 25px; margin-right: 10px;">
						<!-- <input type="button" value="조회" id='searchButton' width="100%" height="100%" />-->
					</div>
 
						<!--buttonStyle--> 
				</div>
				<!--headerbutton--> 
			</div>
		</div>
		<!--iwidget_label headertop-->
		
		<div  style=" clear:both;width:100%; margin:10px auto !important; ">
			<div class="iwidget_grid" style="float:left;margin:10px auto !important; margin-bottom:10px !important;  width:100%; height:300px;">
				<div id="listGrid" style=" width:480px; height:500px; "> </div> 
			</div>
				<!--iwidget_grid --> 
		</div>
		<!--grid area--> 
	</div>
	<!--contents--> 
</div>
<!--wrap-->
</body>
</html>