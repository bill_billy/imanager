<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.iplanbiz.iportal.config.WebConfig"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<title>엑셀 프로그램 관리</title>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>

<script type="text/javascript">
<%
String idqUrl = WebConfig.getIDQUrl();
String acctId = WebConfig.getAcctID();
%>
var step1,step2;
var IDQURL = "<%=idqUrl%>";
IDQURL = "";
var ACCTID = "<%=acctId%>";
function setParentCombobox(value) {
	step1 = $i.post("/ExcelPgmDQ/getParentCombobox");
	step1.done(function(pRes){
		var pSource =
		{
			localdata: pRes,
    		dataType: 'json',
			dataFields: [
				{ name: 'P_CD'},
				{ name: 'P_NM'}
			]
		};
		
		var pAdapter = new $.jqx.dataAdapter(pSource);
		
		$('#parentCombobox').jqxComboBox({source: pAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'P_NM', valueMember: 'P_CD', dropDownWidth: 150, dropDownHeight: 150, width: 150, height: 22,  theme:'blueish' }); 
		if(value != '') {
			var item = $('#parentCombobox').jqxComboBox('getItemByValue', value);
			$("#parentCombobox").jqxComboBox('selectItem',item);
		}else{
			$("#parentCombobox").jqxComboBox('selectIndex', 0 );
		}
		
	});
}
function setChildCombobox(value) {
	step2 = $i.post("/ExcelPgmDQ/getChildCombobox",{pCd:$('#parentCombobox').val()});
	step2.done(function(cRes){
		console.log(cRes);
		var cSource =
		{
			localdata: cRes,
    		dataType: 'json',
			dataFields: [
				{ name: 'C_CD'},
				{ name: 'C_NM'}
			]
		};
		var cAdapter = new $.jqx.dataAdapter(cSource);
		
		$('#childCombobox').jqxComboBox({source: cAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'C_NM', valueMember: 'C_CD', dropDownWidth: 150, dropDownHeight: 150, width: 150, height: 22,  theme:'blueish' });
		if(value != '') {
			var item = $('#childCombobox').jqxComboBox('getItemByValue', value);
			$("#childCombobox").jqxComboBox('selectItem',item);
		}else{
			$("#childCombobox").jqxComboBox('selectIndex', 0 ); 
		}
	});
}
function detailView(acctid,pgmId) { 
	location.href = '${WEB.ROOT}/ExcelPgmDQ/view?acctid='+acctid+'&pgmId='+pgmId+'&pCd='+$("#parentCombobox").val()+'&cCd='+$("#childCombobox").val();
}
function downexcel(acctid,row) {
	var rowdata = $("#listGrid").jqxGrid("getrowdata", row);
	var pgmId = rowdata.PGM_ID;
	var pgmNm = rowdata.PGM_NM;
	
	if(pgmId == '') {
		$i.dialog.alert('SYSTEM', "프로그램 ID가 존재하지 않습니다.\n프로그램을 먼저 선택하세요."); 
		return;
	}
	
	var url = "${WWW.IPORTAL}/ExcelPgmDQ/download?acctid="+acctid+"&pgmId="+pgmId+"&pgmNm="+encodeURI(encodeURIComponent(pgmNm));
	window.open(url, 'ExcelDownload_postgresql', 'height=300,width=500, resizable=no, scrollbars=auto, left=0, top=0');
}
function upexcel(acctid,row) {
	var rowdata = $("#listGrid").jqxGrid("getrowdata", row);
	var pgmId = rowdata.PGM_ID;
	var pgmNm = rowdata.PGM_NM; 
	
	if(pgmId == '') {
		$i.dialog.alert('SYSTEM', "프로그램 ID가 존재하지 않습니다.\n프로그램을 먼저 선택하세요."); 
		return;
	}
	
	var url = "${WWW.ROOT}/ExcelPgmDQ/uploadApply?acctid="+acctid+"&pgmId="+pgmId+"&pgmNm="+encodeURI(encodeURIComponent(pgmNm));
	window.open(url, 'ExcelUploadApply_postgresql', 'height=300,width=500, resizable=no, scrollbars=auto, left=0, top=0');
}
function searchStart() {
	var listData = $i.post("/ExcelPgmDQ/getExcelPgmDQList",{pCd:$('#parentCombobox').val(),cCd:$('#childCombobox').val()});
	listData.done(function(res){
		var source =
		{
			localdata: res,
    		dataType: 'json',
			dataFields: [
			    { name:'ACCT_ID' },
				{ name: 'PGM_ID'},
				{ name: 'PGM_NM'},
				{ name: 'P_CD'},
				{ name: 'P_NM'},
				{ name: 'C_CD'},
				{ name: 'C_NM'},
				{ name: 'JOB_CYC'},
				{ name: 'JOB_CYC_NM'},
				{ name: 'SYSTEM_ID'},
				{ name: 'SYSTEM_NM'},
				{ name: 'PGM_EMP'},
				{ name: 'PGM_EMP_NM'},
				{ name: 'USE_YN'},
				{ name: 'SORT_ORDER'},
				{ name: 'EXPLAIN'},
				{ name: 'PGM_DEF'}, 
				{ name: 'DOWN_SQL'},
				{ name: 'UP_SQL'}						
			]
		};
		
		var adapter = new $.jqx.dataAdapter(source);

		var alginRight = function (row, columnfield, value) {//right정렬
			return '<div style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
		};
		var alginLeft = function (row, columnfield, value) {//left정렬
			return '<div style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
		};
		var noScript = function(row, columnfield, value){
        	var newValue = $i.secure.scriptToText(value);
        	return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
        };
		var detailView = function (row, columnfield, value, html, a, b, c, d) {
			var pgmId = b.PGM_ID;
			var newValue = $i.secure.scriptToText(value);
			var link = "<a href=\"javaScript:detailView('"+b.ACCT_ID+"',  '"+pgmId+"')\" style='color: #000000; text-decoration: underline;'>" + newValue + "</a>";
			return '<div style="text-align:left; margin:4px 0px 0px 10px;">' + link + '</div>';
		};
		
		var dataAdapter = new $.jqx.dataAdapter(source);
		var customCol=[];
		if(IDQURL==""){
			customCol = [
				{ text: 'ID', datafield: 'PGM_ID', width: '10%', align:'center', cellsalign: 'center'/* , cellsrenderer: alginLeft   */},
				{ text: '프로그램명', datafield: 'PGM_NM', width: '40%', align:'center', cellsalign: 'left', cellsrenderer: detailView },
				{ text: '주기', datafield: 'JOB_CYC_NM', width: '15%', align:'center', cellsalign: 'center'},
				{ text: '담당자', datafield: 'PGM_EMP_NM', width: '15%', align:'center', cellsalign: 'center'/* , cellsrenderer:alginRight  */},
				{ text: '작업', width: '20%', align:'center', cellsalign: 'center',  columnType: 'none', editable: false, sortable: false, dataField: null,
					cellsRenderer: function (row, column, value,html, a,b,c,d,e) {
						// render custom column.
						return "<div style='width:100%;height:100%;position:relative;'><div style='margin: auto;width:130px;height:100%;position:absolute; top: 0; left: 0; bottom: 0; right: 0;'><button style='margin:0 !important;margin-top:3px !important;' class='editButton' onclick='javascript:downexcel(\""+b.ACCT_ID+"\"  ,\""+row+"\");'>다운로드</button><button style='' class='editButton' onclick='javascript:upexcel(\""+b.ACCT_ID+"\", \""+row+"\");'>업로드</button></div></div>";
					}
				}
			];
		}else{
			customCol = [
				{ text: 'ID', datafield: 'PGM_ID', width: '20%', align:'center', cellsalign: 'center'/* , cellsrenderer: alginLeft   */},
				{ text: '프로그램명', datafield: 'PGM_NM', width: '45%', align:'center', cellsalign: 'left', cellsrenderer: detailView },
				{ text: '주기', datafield: 'JOB_CYC_NM', width: '15%', align:'center', cellsalign: 'center'},
				{ text: '담당자', datafield: 'PGM_EMP_NM', width: '20%', align:'center', cellsalign: 'center'/* , cellsrenderer:alginRight  */}
			];
		}
		$("#listGrid").jqxGrid({
			width: '100%',
         	height: '100%',
			altrows:true,
			pageable: true,
			pagesize:100,
			pagesizeoptions:['100','200','300'],
			source: dataAdapter,
			theme:'blueish',
			//editable: true,
         	columnsresize: true,
         	columns: customCol
		});
		//setPagerLayout("listGrid"); <%--체인지 이벤트가 안되서 트리거로 처리함--%>
		$('#listGrid').on('change', function() {
			setPagerLayout("listGrid");
		});
		$('#listGrid').trigger('change');
	});
}
function setPagerLayout(selector) {
	var pagesize = $('#'+selector).jqxGrid('pagesize');
	var w = 49; 
	if(pagesize<100) {
		w = 44;
	} else if(pagesize>99&&pagesize<1000) {
		w = 49;
	} else if(pagesize>999&&pagesize<10000) {
		w = 54;
	}
	
	//디폴트 셋팅
	$('#gridpagerlist'+selector).jqxDropDownList({ width: w+'px' });
	//체인지 이벤트 처리
	$('#'+selector).on("pagesizechanged", function (event) {
		var args = event.args;
		if(args.pagesize<100) {
			$('#gridpagerlist'+selector).jqxDropDownList({ width: '44px' });
		} else if(args.pagesize>99 && args.pagesize<1000) {
			$('#gridpagerlist'+selector).jqxDropDownList({ width: '49px' });
		} else if(args.pagesize>999 && args.pagesize<10000) {
			$('#gridpagerlist'+selector).jqxDropDownList({ width: '54px' });
		} else {
			$('#gridpagerlist'+selector).jqxDropDownList({ width: 'auto' });
		}
	});
}
function goNew() {
	location.href = '${WEB.ROOT}/ExcelPgmDQ/view?pCd='+$('#parentCombobox').val() + '&cCd=' + $('#childCombobox').val();
}
$(this).ready(function() {
 
	$("#searchButton").jqxButton({ width : '', theme : 'blueish' });
	$("#newButton").jqxButton({ width : '', theme : 'blueish' });

	//set combobox
	setParentCombobox('${param.pCd}'); //대분류 ->step1
	$.when(step1).done(function(){
		setChildCombobox('${param.cCd}'); //중분류 ->step2
		$.when(step2).done(function(){
			searchStart();
		});
	});

	//bind event
	$('#parentCombobox').bind('select', function(e) {
		if(e.args) {
			setChildCombobox('');	
		}
	});
	
	$("#searchButton").bind('click', function(e) {
		searchStart();
	});
	
	$('#newButton').click(function(e) {
		goNew();
	});
});
</script>
<style>
.editButton {
	margin: 0 0 0 10px !important;
	background:#cccccc url(../../resources/css/images/background/jqxNavigationBar_header_bg.gif) 50% 50% repeat-x !important;
	color:#225886 !important;
	border-top:1px solid #729AC4 !important;
	border-left:1px solid #729AC4 !important;
	border-right:1px solid #729AC4 !important;
	border-bottom:1px solid #729AC4 !important;
	font-weight:bold !important;
	font-size:11px !important;
	cursor:pointer !important;
	border-radius:4px !important;
	padding:2px 10px !important;
	margin-top:3px !important;
	position:relative !important;
	text-indent:0 !important;
}
.editButton:hover {
	margin: 0 0 0 10px !important;
	background:#cccccc url(../../resources/css/images/background/jqxNavigationBar_button_hover_bg.png) 50% 50% repeat-x !important;
	color:#225886 !important;
	border-top:1px solid #729AC4 !important;
	border-left:1px solid #729AC4 !important;
	border-right:1px solid #729AC4 !important;
	border-bottom:1px solid #729AC4 !important;
	font-weight:bold !important;
	font-size:11px !important;
	cursor:pointer !important;
	border-radius:4px !important;
	padding:2px 10px !important;
	margin-top:3px !important;
	position:relative !important;
}
</style>
</head>
<body class='blueish'>
<div class="wrap" style="width:98%;min-width:1160px; margin:0 10px;">
	
		<div class="header" style="width:100%; margin:10px 0;">
				<div class="iwidget_label" style="width:100%; float:left;">
					<div class="label type1 f_left">대분류： </div>
					<div id='parentCombobox'  class="table_combobox f_left"></div>
					<div class="label type1 f_left">중분류： </div>
					<div id='childCombobox'  class="table_combobox f_left"></div>

					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='searchButton' width="100%" height="100%" />
						</div>
						<div class="button type1 f_left">
							<input type="button" value="신규" id='newButton' width="100%" height="100%" />
						</div>
						<!--buttonStyle--> 
					</div>
				</div>
		  	</div>
		<!--iwidget_label headertop-->
	<div class="content" style="width:100%; height:100%; float:left;">
		<div style="width:100%;">
			<div class="iwidget_grid" style="float:left; width:100%; height:620px;margin:10px 0;">
				<div id="listGrid" style=" width:100%; height:100%; "> </div>
			</div>
				<!--iwidget_grid --> 
		</div>
		<!--grid area--> 
	</div>
	<!--contents--> 
</div>
<!--wrap-->
</body>
</html>