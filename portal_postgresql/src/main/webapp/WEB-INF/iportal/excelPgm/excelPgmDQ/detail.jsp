<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<title>엑셀 프로그램 관리</title>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
<link rel="stylesheet" href="../../resource/css/iplanbiz/table.css" type="text/css" />
<script type="text/javascript">
var step1,step2,step3,step4;
var init=true;
function setParentCombobox(value) {
	step1 = $i.post("/ExcelPgmDQ/getParentCombobox");
	step1.done(function(pRes){
		var pSource =
		{
			localdata:pRes,
    		dataType: 'json',
			dataFields: [
				{ name: 'P_CD'},
				{ name: 'P_NM'}
			]
		};
		
		var pAdapter = new $.jqx.dataAdapter(pSource);
		
		$('#parentCombobox').jqxComboBox({source: pAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'P_NM', valueMember: 'P_CD', dropDownWidth: 150, dropDownHeight: 150, width: 150, height: 22,  theme:'blueish' });
 
		if(value != '') {
			var item = $('#parentCombobox').jqxComboBox('getItemByValue', value);
			$("#parentCombobox").jqxComboBox('selectItem',item);
		}else{
			$("#parentCombobox").jqxComboBox('selectIndex', 0 );
		}
	});
}
function setChildCombobox(value) {
	step2 = $i.post("/ExcelPgmDQ/getChildCombobox",{pCd:$('#parentCombobox').val()});
	step2.done(function(cRes){
		var cSource =
		{
			localdata: cRes,
    		dataType: 'json',
			dataFields: [
				{ name: 'C_CD'},
				{ name: 'C_NM'}
			]
		};
		var cAdapter = new $.jqx.dataAdapter(cSource);
		
		$('#childCombobox').jqxComboBox({source: cAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'C_NM', valueMember: 'C_CD', dropDownWidth: 150, dropDownHeight: 150, width: 150, height: 22,  theme:'blueish' }); 
		if(value != '') {
			var item = $('#childCombobox').jqxComboBox('getItemByValue', value);
			$("#childCombobox").jqxComboBox('selectItem',item);
		}else{
			$("#childCombobox").jqxComboBox('selectIndex', 0 );
		}
		
	});
}
function setDBCombobox(value) {
	step3 = $i.post("../../admin/dbinfo/getDbInfoList");
	step3.done(function(res){
		var dSource =
		{
			localdata: res,
    		dataType: 'json',
			dataFields: [
				{ name: 'DBKEY'},
				{ name: 'DBNAME'}
			]
		};
		var dAdapter = new $.jqx.dataAdapter(dSource);
		
		$('#dbkeyCombobox').jqxComboBox({source: dAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'DBNAME', valueMember: 'DBKEY', dropDownWidth: 150, dropDownHeight: 150, width: 150, height: 22,  theme:'blueish' }); 
		if(value != '') {
			var item = $('#dbkeyCombobox').jqxComboBox('getItemByValue', value);
			$("#dbkeyCombobox").jqxComboBox('selectItem',item);
		}else{
			$("#dbkeyCombobox").jqxComboBox('selectIndex', 0 );
		}
		
	});
	/*
	var dbkeysource =
	{
		localdata: $iv.array($iv.envinfo.dbinfo),
		dataType: 'json',
		dataFields: [
			{ name: 'KEY'},
			{ name: 'DB_COMMENT'}				
		]
	};
	var adapter = new $.jqx.dataAdapter(dbkeysource);
	
	$('#dbkeyCombobox').jqxComboBox({ selectedIndex: 0, source: adapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'DB_COMMENT', valueMember: 'KEY', dropDownWidth: 150, dropDownHeight: 150, width: 150, height: 22,  theme:'blueish' });
	*/
}
function setCommonCodeCombobox(objid, comlcod) {
	step4 = $i.post("./getCommonCode", { comlcd : comlcod });
	step4.done(function(res){
		var source =
		{
			localdata: res,
    		dataType: 'json',
			dataFields: [
				{ name: 'COM_COD'},
				{ name: 'COM_NM'}				
			]
		};
		var adapter = new $.jqx.dataAdapter(source);
		
		$('#'+objid).jqxComboBox({ selectedIndex: 0, source: adapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'COM_NM', valueMember: 'COM_COD', dropDownWidth: 150, dropDownHeight: 70, width: 150, height: 22, theme:'blueish'});
		
	});
}



function deleteStart() {
	$i.dialog.confirm('SYSTEM', '프로그램을 삭제하시겠습니까?',function(){
		$("#ac").val('delete');
		$("#pgmId").jqxInput({ disabled: false });
		
		$.ajax({
			type : $("#saveForm").attr("method"),
			url  : $("#saveForm").attr("action"),
			data : $("#saveForm").serialize(),
			async: false,
			success:function(res) {
				$i.dialog.alert('SYSTEM', "삭제가 완료 되었습니다.",function(){
					goList();
				}); 
			},
			error:function(jqXHR,textStatus,errorMessage) {
				$i.dialog.error('SYSTEM', "삭제에 실패 하였습니다.");
			}
		});
	});
}
function saveStart() {
	$('#saveForm').jqxValidator('hide');
	var ruleinfo = null;
	ruleinfo = [
		{input: '#pgmNm', message:'프로그램명을 입력하세요!', action:'keyup, blur', rule:'required'},
		{input: '#pgmId', message:'프로그램 ID를 입력하세요!', action:'keyup, blur', rule:'required'},
		//{input: '#systemnm', message:'업무규칙을 입력하세요!', action:'keyup, blur', rule:'required'},
		//{input: '#pgmEmpNm', message:'담당자를 입력하세요!', action:'keyup, blur', rule:'required'},
		{input: '#sortOrder', message:'정렬순서를 입력하세요!', action:'keyup, blur', rule:'required'},
		{input: '#sortOrder', message:'정렬순서는 숫자로 입력하세요!', action:'keyup, blur', rule:'number'},
		{input: '#pgmDef', message:'설명을 입력하세요!', action:'keyup, blur', rule:'required'},
		{input: '#downSql', message:'다운로드 쿼리를 작성하세요!', action:'keyup, blur', rule:'required'},
		{input: '#upSql', message:'업로드 쿼리를 작성하세요!', action:'keyup, blur', rule:'required'},
		{input: '#delSql', message:'업로드 전 삭제 쿼리를 작성하세요!', action:'keyup, blur', rule:'required'}
	];
	
	$("#saveForm").jqxValidator({
		rules : ruleinfo
	});
	if(!checkStringFormat($('#pgmId').val())){
		$i.dialog.warning('SYSTEM', '프로그램 ID에 특수문자를 입력할 수 없습니다!');
		return false;
	}
	if($("#saveForm").jqxValidator('validate') == true) {
		$("#ac").val($("#ac").val() == 'update'?'update':'save');
		
		$('#comlCod').val($('#parentCombobox').val());
		$('#comCod').val($('#childCombobox').val());
		$('#jobCyc').val($('#jobCycCombobox').val());
		$('#useYn').val($('#useYnCombobox').val());
		
		//$("#pgmNm").jqxInput({ disabled: false });
		$("#pgmId").jqxInput({ disabled: false });
		
		var type = $("#saveForm").attr("method");
		var url =  $("#saveForm").attr("action");
		var data = $("#saveForm").serialize();
		
		$.ajax({
			type : type,
			url  : url,
			data : data,
			async: false,
			success:function(res) {
				console.log(res);
				
				if(res == 'ERROR_DUPLICATION') {
					$i.dialog.error('SYSTEM', "사용중인 프로그램 ID 입니다.\n다른 프로그램 ID를 입력하세요."); 
					
				} else {
					//alert("저장이 완료 되었습니다.");
					$i.dialog.alert('SYSTEM', "저장이 완료 되었습니다.",function(){
						goList2($('#comlCod').val(), $('#comCod').val());
					});
					//goList2($('#comlCod').val(), $('#comCod').val());
				}
			},
			error:function(jqXHR,textStatus,errorMessage) {
				$i.dialog.error('SYSTEM', "저장에 실패 하였습니다.");
				//alert("저장에 실패 하였습니다.");
			}
		});
	}
}
function checkStringFormat(string) { 
    //var stringRegx=/^[0-9a-zA-Z가-힝]*$/; 
    var stringRegx = /[~!@\#$%<>^&*\-=+_\’\'\"]/gi; 
    console.log(stringRegx);
    var isValid = true; 
    if(stringRegx.test(string)) { 
      isValid = false; 
    } 
       
    return isValid; 
}

function goList() {
	location.href = '${WEB.ROOT}/ExcelPgmDQ?pCd=${param.pCd}&cCd=${param.cCd}';
}
function goList2(pCd, cCd) {
	location.href = '${WEB.ROOT}/ExcelPgmDQ?pCd='+pCd+'&cCd=' + cCd;
}
function searchSystem(){
	window.open('${WEB.ROOT}/popDQSystem','system_postgresql','height=590,width=500, resizable=no, scrollbars=yes, left=0, top=0');
}
function searchEmp() {
	window.open('${WEB.ROOT}/employeeSearch?employeeGbn=excelPgmDQView', 'employeeSearch_postgresql', 'height=590,width=900, resizable=no, scrollbars=yes, left=0, top=0');
}
/*
function callback(res){
	if(res!=null&&res.iqfe0003.length!=0){
		$("#systemnm").val(res.iqfe0003[0].systemnm);
	}
	else{
		$("#systemnm").val("진단시스템 매핑 오류");
	}
}
*/
function initExcelProgram(pgmId, acctid) {
		$('#parentCombobox').val('${excelDQ.P_CD}');
		$('#childCombobox').val('${excelDQ.C_CD}');
		$('#jobCycCombobox').val('${excelDQ.JOB_CYC}');
		$('#useYnCombobox').val('${excelDQ.USE_YN}');
		$("#dbkeyCombobox").val('${excelDQ.DBKEY}');
	//	$("#pgmNm").jqxInput({ disabled: true });
		$("#pgmId").jqxInput({ disabled: true });
	//	$('#parentCombobox').jqxComboBox({ disabled: true });
	//	$('#childCombobox').jqxComboBox({ disabled: true });
		
		$('#ac').val('update'); //저장시 update 구분
		
		//진단 시스템 명 가져오기
		/*$.ajax({
			url :$iv.envinfo.idqurl+"/Scheduler/IQFE0003?acct_id="+$iv.userinfo.acctid()+"&systemid="+res.SYSTEM_ID, 
			dataType : "jsonp", 
			jsonp : "callback",
			success : function(d){
				
			},
			complete:function(data){
				//alert(data);
			},
			error:function(err){
				//alert(err);
			}
		});*/
		
	
}
$(this).ready(function() {
	//button
	$('#delButton').jqxButton({ width: '',  theme:'blueish'});
	$('#saveButton').jqxButton({ width: '',  theme:'blueish'});
	$('#listButton').jqxButton({ width: '',  theme:'blueish'});
// 	$("#systemsearch").on("click",function(event){
// // 		windowOpen('${WEB.ROOT}/popDQSystem','진단시스템', '500', '590', 'no', 'yes');
//  		window.Open('${WEB.ROOT}/popDQSystem','진단시스템',"height=590,width=500, resizable=no, scrollbars=yes, left=0, top=0");
// 	});
	//set combobox
	setParentCombobox('${param.pCd}'); //대분류
	$.when(step1).done(function(){
		setChildCombobox('${param.cCd}'); //중분류
		if('${param.pgmId}'=='')	init=false; //신규
		$('#parentCombobox').bind('select', function(e) {
			console.log(init+$('#parentCombobox').val());
			if(e.args) {
				if(!init){
					setChildCombobox('');
				}else{
					init=false;
				}
			}
		});
	});
	setDBCombobox('${excelDQ.DBKEY}');
	setCommonCodeCombobox('jobCycCombobox', 'JOB_CYC'); //주기
	$.when(step4).done(function(){
		setCommonCodeCombobox('useYnCombobox', 'USE_YN'); //사용여부
	});

	
	//bind event
	$('#delButton').click(function() { deleteStart(); });
	$('#saveButton').click(function() { saveStart(); });
	$('#listButton').click(function() { goList(); });
	
	
	//init
	if('${param.pgmId}' != '') {
		var ajaxArray = [step1,step2,step3,step4];
		$.when.apply($,ajaxArray).done(function(){
			initExcelProgram('${param.pgmId}','${param.acctid}'); 
		});
	} else {
		$('#delButton').jqxButton('destroy');
	}
});
</script>
</head>
<body class='blueish'>
<div class="wrap" style="width:98%; margin:0 10px;">
	<div class="content f_left" style="width:100%; height:100%; margin:10px 0;">
		<div class="iwidget_label" style="margin:0 !important; height:27px;  ">
			<div class="label type2 f_left" style="float:left;">기본정보</div>
				<div class="group_button f_right" style="margin-right:10px;">
						<div class="button type3 f_left" >
							<input type="button" value="삭제" id="delButton" width="100%" height="100%" />
						</div>
						<div class="button type2 f_left">
							<input type="button" value="목록" id="listButton" width="100%" height="100%" />
						</div>
						<div class="button type2 f_left" >
							<input type="button" value="저장" id="saveButton" width="100%" height="100%" />
						</div>
			
						<!--buttonStyle--> 
				</div>
		<!--iwidget_label-->
		</div>
		<div class="table_Area" style="width:100%; height:100%; float:left; margin-top:10px; ">
			<form id="saveForm" name="saveForm" action="${WWW.IPORTAL}/ExcelPgmDQ/view" method="post">
				<input type="hidden" id="ac" name="ac" />
				<input type="hidden" id="comlCod" name="comlCod" />
				<input type="hidden" id="comCod" name="comCod" />
				<input type="hidden" id="jobCyc" name="jobCyc" />
				<input type="hidden" id="useYn" name="useYn" />
				
				<table width="100%" class="i-DQ-table" cellspacing="0" cellpadding="0" border="0" style="margin-bottom:0 !important;">
					<tr>
						<th class="required_icon" style="width:15%;text-align:right;  padding-right:1%;">프로그램명<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th>
						<td colspan="2" class="activetable" style="width:30%; padding:0 !important;">
							<div style='float: left; margin-left:7px; margin-right:5px;'><input type="text" id="pgmNm" name="pgmNm" class="inputEditStyle" style="width: 300px;" value="${excelDQ.PGM_NM}"/></div>
						</td>
						<th class="required_icon" style="text-align:right; padding-right:1%;width:15%;">프로그램 ID<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th>
						<td colspan="2"  style="padding:0 !important;">
							<div  style='float: left; margin-left:5px; margin-right:5px;'><input type="text" id="pgmId" name="pgmId" class="inputEditStyle" style="width: 245px;" value="${excelDQ.PGM_ID}"/></div>
						</td>
					</tr>
					<tr>
						<th class="required_icon" style="width:15%;text-align:right;  padding-right:1%;">대분류<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th>
						<td colspan="2" class="activetable" style="padding:0 !important;">
							<div  style='float: left; margin: 0!important'><div style='float:left; margin:0 7px;' id='parentCombobox'  class="table_combobox"></div></div>
						</td>
						<th class="required_icon" style="text-align:right;  padding-right:1%;">중분류<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th>
						<td colspan="2" style="padding:0 !important;" >
							<div  style='float: left; margin: 0 !important;' ><div style='float:left; margin:0 7px;' id='childCombobox'  class="table_combobox"></div></div>
						</td>
					</tr>
					<tr>
					<th class="required_icon" style="width:15%;text-align:right;  padding-right:1%;">주기<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th>
						<td colspan="2" class="activetable" style="padding:0 7px;">
							<div style='float:left; margin:0 !important;' id='jobCycCombobox' class="table_combobox"></div>
						</td>
						<th class="required_icon" style="text-align:right;  padding-right:1%;">진단시스템&nbsp;</th>
						<td colspan="2" style="width:30%;">
							<div  style='float: left; margin-left:5px; margin-right:5px;' >
								<input type="text" class="inputEditStyle" value="" style="width:245px; float:left;" id="systemnm" name="systemnm" readonly="readonly">
								<input type="hidden" id="systemid" name="systemid" value="${excelDQ.SYSTEM_ID}" >
							</div>
							<div style="float: left; text-align:right;">
								<div class="image-button" style="float:left;margin:0; padding:0;"> <a href="javascript:searchSystem();"><img id='systemsearch' src="../../resources/img/img_icons/search_icon.png" alt="검색"></a> </div>
							</div>
						</td>
					</tr>
					<tr>
						<th class="required_icon" style="width:15%;text-align:right;  padding-right:1%;">담당자&nbsp;</th>
						<td colspan="2" class="activetable" style="padding:0 !important;">
							<div  style='float: left; margin-left:5px; margin-right:5px;'>
								<input type="text" class="inputEditStyle" value="${excelDQ.PGM_EMP_NM}" style="width:300px; float:left;" id="pgmEmpNm" name="pgmEmpNm" readonly="readonly"/>
								<input type="hidden" id="pgmEmp" name="pgmEmp" value="${excelDQ.PGM_EMP}"/>
							</div>
							<div style="float: left; text-align:right;">
								<div class="image-button" style="float:left;margin:0; padding:0;"> <a href="javascript:searchEmp();"><img src="../../resources/img/img_icons/search_icon.png" alt="검색"></a> </div>
							</div>
						</td>
						<th class="required_icon" style="text-align:right;  padding-right:1%;">업로드 대상DB</th>
						<td colspan="2" style="width:30%;">
							<div style='float:left; margin:0 7px;' id='dbkeyCombobox' name = "dbkey" class="table_combobox"></div>
						</td>
					</tr>
					<tr>
						<th class="required_icon" style="width:15%;text-align:right;  padding-right:1%;">사용여부<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th>
						<td colspan="2" class="activetable" style="padding:0 !important;">
							<div style='float:left; margin:0 7px;' id='useYnCombobox' class="table_combobox"></div>
						</td>
						<th class="required_icon" style="text-align:right;  padding-right:1%;">정렬순서<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th>
						<td colspan="2" style="width:30%;">
							<div  style='float: left; margin-left:5px; margin-right:5px;' ><input type="text" class="inputEditStyle" value="${excelDQ.SORT_ORDER}" style="width:145px; float:left; text-align: right !important; padding-right: 5px !important;" id='sortOrder' name='sortOrder'></div>
						</td>
					</tr>
					<tr>
						<th class="th_height2 required_icon" style="text-align:right;  padding-right:1%;">설명<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th>
						<td colspan="5"><textarea class="textarea textareaStyle2" value="" style="width:99%; height:60px !important;" id='pgmDef' name='pgmDef' >${excelDQ.PGM_DEF}</textarea></td>
					</tr>
					<tr>
						<th class="th_height2 required_icon" style="text-align:right;  padding-right:1%;">다운로드 쿼리<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th>
						<td colspan="5"><textarea class="textarea textareaStyle2" value="" style="width:99%; height:120px !important; " id='downSql' name='downSql'>${excelDQ.DOWN_SQL}</textarea></td>
					</tr>
					<tr>
						<th class="th_height2 required_icon" style="text-align:right;  padding-right:1%;">업로드 적용 쿼리(insert)</th>
						<td colspan="5"><textarea class="textarea textareaStyle2" value="" style="width:99%; height:80px !important; " id='insertSql' name='insertSql'>${excelDQ.INSERT_SQL}</textarea></td>
					</tr>
					<tr>
						<th class="th_height2 required_icon" style="text-align:right;  padding-right:1%;">업로드 적용 쿼리(select)<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th>
						<td colspan="5"><textarea class="textarea textareaStyle2" value="" style="width:99%; height:80px !important; " id='upSql' name='upSql'>${excelDQ.UP_SQL}</textarea></td>
					</tr>
					<tr>
						<th class="th_height2 required_icon" style="text-align:right;  padding-right:1%;">업로드 적용 전 <br/>삭제 쿼리<span style="color:red; font-size:12px; vertical-align:-2px;">＊</span></th>
						<td colspan="5"><textarea class="textarea textareaStyle2" value="" style="width:99%; height:120px !important; " id='delSql' name='delSql'>${excelDQ.DEL_SQL}</textarea></td>
					</tr>
				</table>
				<div class="label-4" style="float:left;">
					<p class="iwidget_grid_tip" style="margin-top: 5px;">
						<b>@YYMMDD</b> : 다운로드 쿼리, 업로드 적용 전 삭제 쿼리에 사용 가능한 파라미터.<br/>
						ex) SELECT * FROM IECT7033 WHERE UDC1 >= <b>'@YYMMDD'</b>
					</p>
				</div>
			</form>
		</div>
	<!--table_Area--><!--★table_Area★--> 
	
	</div>
	<!--contents--> 
</div>
<!--wrap-->
</body>
</html>