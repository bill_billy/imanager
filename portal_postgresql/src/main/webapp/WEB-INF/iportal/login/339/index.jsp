<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="java.util.Map"%>
<%@page import="com.iplanbiz.iportal.config.WebConfig"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<link rel="shortcut icon" href="../../resources/css/login/favicon.ico" />
<link rel="stylesheet" type="text/css" href="../../resources/css/login/idas_Ktoto/layout_idas_Ktoto.css"/>
<script src="../../resources/js/jquery/jquery-1.7.2.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
<link rel="stylesheet" href="../../resources/css/iplanbiz/styles/jqx.Lotte-depart.css" type="text/css" />

<link rel="stylesheet" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css" type="text/css" />      

<script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
<script src="../../resources/js/util/remoteInfo.js"></script>   
<script src="../../resources/js/jquery/jquery.cookie.js"></script>
<title>경영정보시스템</title>
<script type="text/javascript"> 
	var noticeList = "";
	$(document).ready(function(){
		
<%-- 		var msg = "<%=msg%>";     --%>
		
		if($.cookie("IPLANBIZUSERID")!=null){
			var userID = $.cookie("IPLANBIZUSERID");
			$("#userID").val(userID);
			$("#checkID").attr("checked","checked");
		}
		$("#userID").focus();
		
		if("${msg != null && msg != ''}" == "true"){
			alert("${msg}");
		}
		var ndata = $i.post("/ktoto/notice/getKtotoNoticeInformList", {num:5});
		ndata.done(function(res){
			var $parent = $("#txtlist");
			var $child,$li;
			var array = res.returnArray;
			noticeList = array;
			for(var i=0;i<array.length;i++){
				var title = array[i].TITLE;
				var no = array[i].BOARDNO;
				
				if(i==0||i%2==0){//0과 짝수
					$li = $("<li></li>")
					$child = $("<p><a id='"+no+"' onclick='popupNotice(this);' style='cursor:pointer;' title='"+$i.secure.scriptToText(title)+"'>"+$i.secure.scriptToText(title)+"</a></p>");
					$li.append($child);
					if(i==array.length-1){
						$parent.append($li);
					}
				}else{//홀수
					$child = $("<p><a id='"+no+"' onclick='popupNotice(this);' style='cursor:pointer;' title='"+$i.secure.scriptToText(title)+"'>"+$i.secure.scriptToText(title)+"</a></p>");
					$li.append($child);
					$parent.append($li);
				}
			}
			startSlide();
		});
	});
	function startSlide(){
		//변수설정
	    var list = $("#txtlist"); //리스트몸체
	    var listN = $("#txtlist li").length; //리스트자식갯수
	    var timer; //타이머변수
	    var nowNum = null; //현재인덱스
	    var speed = 5000; //타이머속도
	    
	    //초기설정
	    $("#txtlist li:eq(0)").addClass('active');
	    $("#txtlist").addClass('ing'); //진행중임을 암시하는 클래스

	    //이벤트설정
	    list.bind({
	        mouseenter:function(){
	            if($(this).hasClass('ing')){
	                clearInterval(timer);
	                timer =0;
	            }else{
	                
	            }
	        },
	        mouseleave:function(){
	            if($(this).hasClass('ing')){
	                 timer = setInterval(auto, speed);  
	            }else{
	                
	            } 
	        }
	    });
	    //함수설정
	    function slide(idx){
	    	//console.log(JSON.stringify({top:-idx * 22}));
	      //  list.animate({top:-idx * 22});
	        var cur = list.find(">li.active");
	        cur.fadeOut(300,function(){
	        	 cur.removeClass('active');
	     	     list.find(">li").eq(idx).addClass('active');
	     	     cur.appendTo("#txtlist");
	     	     cur.fadeIn();
	        });
	        
	    }

	    function auto(){
	        var Num = $("#txtlist li.active").index();
	        if(Num >= listN-1){
	            Num = 0;
	        }else{
	            Num ++;         
	        }
	        slide(Num);
	    }
	    
	    timer = setInterval(auto, speed);
		
	}
	function popupNotice(e){
		var boardNo = e.getAttribute('id');
		var title, content;
		for(var i=0;i<noticeList.length;i++){
			var no = noticeList[i].BOARDNO;
			if(no==boardNo){
				title=$i.secure.scriptToText(noticeList[i].TITLE);
				content=noticeList[i].CONTENT;
				break;
			}
		}
		var uriContext = "../..";

		var popid = "notice_popup_"+boardNo;
		var popup = "<div id='"+popid+"' style='position:absolute;z-index:999999;border:4px solid #003664;'>"+
						"<div>"+title+"</div>"+
						"<div>"+
							"<div class='popup_box_contents' style='height:500px;'>"+content+
								 
							"</div>"+
							"<div class='popup_box_bottom'>"+
						      "<a style='cursor:pointer;' onclick='jqxpopupclose("+boardNo+");'>"+
						       "[확인]"+
						      "</a>"+
						    "</div>"+  
							//"<iframe style='width:890px !important;height:530px !important;' frameborder='0' src='"+uriContext + '/cms/notice/get?type=popup&boardNo='+boardNo+"'></iframe>"+
						"</div>"+
					"</div>";
		$(document).find("body").append(popup);
			 
		var offset = $("body").offset();
		var size = ($("body").width()-820)/2; 
		var modalColor=1;		
		$("#"+popid).jqxWindow({
					
			theme:'Lotte-depart',
	        position: { x: offset.left + size, y: offset.top + 150} ,
	        maxHeight: 600, maxWidth: 966, minHeight: 580, minWidth: 800, height: 580, width: 823,
	        resizable: true, isModal: true, modalOpacity: ((modalColor==1)?0.7:0), 
	        initContent: function (a,b,c,d,e) {
	        $("#"+popid).find(".popup_box_contents").css({width:"100%",height:"490px"}); 
	    	}
	    });
				 
		$("#"+popid).find(".popup_box_contents").css({width:"100%",height:"490px"});
	}
	function jqxpopupclose(id){
		$('#notice_popup_'+id).jqxWindow('close');
		$('#notice_popup_'+id).remove();  
	}
	function login(){
		if($("#userID").val() == ""){
			return;
		}else if($("#passwd").val() == "")
		{
			return;
		}
		if($("#checkID").is(":checked")){
			$.cookie("IPLANBIZUSERID",$("#userID").val(),{expires:365});
		}
		else{
			$.cookie("IPLANBIZUSERID",null);
		}
		$("#pIp").val('<%=request.getRemoteAddr() %>');
		$("#osVer").val(getOSInfoStr());
		$("#browserNm").val(getBrowserName());
		$("#browserVer").val(getBrowserVer());
		$("#loginForm").submit();
		
	}
	</script> 
	<style>
	.notice .list p{
		text-decoration:underline;
		width:300px;
		display: inline-block !important;
		overflow: hidden; 
  		text-overflow: ellipsis;
  		white-space: nowrap; 
  		height: 20px;
	}
	.twostep{
		width: 800px;
    	display: inline-block;
    	height: 23px;
    	line-height:20px;
    	overflow: hidden;
    	vertical-align:middle;
	}
	ul,li {list-style:none;}
	.twostep > ul {position:relative;top:0;left:0;}

.popup_box_contents {margin:5px; width:100%; position:relative;overflow:auto;}
.popup_box_bottom {color:#000; background:#ecedf4; font-size:13px; letter-spacing:-1px; margin:0; text-align:right; 
					border-top:1px solid #d5d7e0; padding:5px 15px 8px 0; vertical-align: middle;
					position:absolute;bottom:0;width:99%;margin-left:-5px;font-family: "dotum","돋움", Tahoma, Gulim, AppleGothic, sans-serif;}

.jqx-window-header-Lotte-depart{
	background: #003664 !important;
    height: 35px !important;
    margin: 0 !important;
    border-radius: 0px;
    font-family: "dotum","돋움", Tahoma, Gulim, AppleGothic, sans-serif;
}
.jqx-window-header-Lotte-depart > div:first-child{
width: 85%;
    line-height: 30px;
    float: left;
    padding-left: 27px;
    background: url(../../resources/img/ktoto/title_ico.png) 10px 10px no-repeat;
    font-size: 15px;
    color: #FFF;
    letter-spacing: -1px;
    font-weight: bold;
}
.jqx-window-close-button-Lotte-depart{
	background-image: url(../../resources/img/ktoto/btn_close.png);
	cursor:pointer;
	margin-right:3px;
	margin-top:3px;
}
.jqx-window-close-button-background-Lotte-depart{
	margin-right:15px !important;
	margin-top:5px;
}
</style>
</head>
<body>
<div id="wrap" class="wrap">
  <div id="header" class="header">
    <h1><img src="../../resources/img/login/idas_Ktoto/login_logo.png" alt="Ktoto" title=""></h1>
    <h2>체육진흥투표권 D/W 및 MIS 통합시스템</h2>
  </div>
  <!--//header-->
  <!--container-->
  <div id="container"  class="container">
    <div class="content01">
      <div class="visual"> <img src="../../resources/img/login/idas_Ktoto/login_visual.png" alt="배경이미지"> </div>
      <!--visual-->
    </div>
    <!--//content-->
    <div class="content02">
      <div class="info">
        <form id="loginForm" action="" method="post" commandName="user" class="form">
        	<input type="hidden" id="pIp" name="pIp" />
			<input type="hidden" id="osVer" name="osVer" />
			<input type="hidden" id="browserNm" name="browserNm" />
			<input type="hidden" id="browserVer" name="browserVer" />
          <fieldset>
            <legend> login</legend>
            <h1><img src="../../resources/img/login/idas_Ktoto/login_caption.png" alt="Login"></h1>
            <div id="login_box" class="logbox">
              	<input type="hidden" name="CSRFTOKENNAME" value="${CSRFTOKENNAME}"/>
				<input type='hidden' name="ACCT_NM" id="ACCT_NM" value = ""/> 
              <p class="uid">
                <label for="userID" >아이디</label>
                <input  name="userID" id="userID"/>
              </p>
              <p class="pwd">
                <label for="passwd">비밀번호</label>
                <input type="password" name="passwd" id="passwd" onKeyPress="if(event.keyCode==13){ login(); return false;}" />
              </p>
              <p class="chk">
                <input type="checkbox" id="checkID" name="checkID">
                <label for="check">아이디 저장</label>
              </p>
              <p id="btn" class="btn">
                <input type="button" value="로그인" onclick="login();">
              </p>
            </div>
          </fieldset>
        </form>
      </div>
      <!--info-->
    </div>
    <!--content2-->

    <!--notice-->
    <div class="notice">
      <div class="list">
	      <h2>케이토토 소식</h2>
	      <div class="twostep">
	      	<ul id="txtlist">
		       <!--  <li><p>케이토토 평창동계패럴림픽 서포터즈 패럴림픽을 가다!</p>
		        	<p>이상화, 박승희 선수 등 스포츠토토 빙상단, 케이토토 본사...</p></li>
		        <li><p>test1</p>
		        	<p>test2</p></li>
		        <li><p>test3</p>
		        	<p>test4</p></li>
		        <li><p>test5</p>
		        	<p>test6</p></li> -->
		    </ul>
	      </div>
      </div>
    </div>
    <!--//notice-->

    <div class="footer">
      <p><img src="../../resources/img/login/idas_Ktoto/login_coyright.png" alt="copyright"></p>
    </div>
    <!--footer-->
  </div>
  <!--//container-->
</div>
<!--wrap-->
</body>
</html>
