<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="java.util.Map"%>
<%@page import="com.iplanbiz.iportal.config.WebConfig"%>
<%@ page import="org.slf4j.Logger"%>
<%@ page import="org.slf4j.LoggerFactory"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<link rel="shortcut icon" href="../../resources/css/login/favicon.ico" />
  <link rel="stylesheet" href="../../resources/css/login/layout.css" type="text/css" media="screen">
  <link rel="stylesheet" href="../../resources/css/jquery.ui.all.css">
  <script src="../../resources/js/jquery/jquery-1.7.2.js"></script>
  <script src="../../resources/js/jquery/jquery.cookie.js"></script>
  <script src="../../resources/js/util/remoteInfo.js"></script>   
<title>경영정보시스템</title>  
	<script type="text/javascript"> 
	
	
	$(document).ready(function(){
		
<%-- 		var msg = "<%=msg%>";     --%>
		
		if($.cookie("IPLANBIZUSERID")!=null){
			var userID = $.cookie("IPLANBIZUSERID");
			$("#userID").val(userID);
			$("#checkID").attr("checked","checked");
		}
		$("#userID").focus();
		
		if("${msg != null && msg != ''}" == "true"){
			alert("${msg}");
		}
		
// 		requestcsrfvalidation();
// 		$("#userID").on("keypress",requestcsrfvalidation);
// 		$("#passwd").on("keypress",requestcsrfvalidation);
		
	});
	function login(){
		if($("#userID").val() == ""){
			return;
		}else if($("#passwd").val() == "")
		{
			return;
		}
		if($("#checkID").is(":checked")){
			$.cookie("IPLANBIZUSERID",$("#userID").val(),{expires:365});
		}
		else{
			$.cookie("IPLANBIZUSERID",null);
		}
		$("#pIp").val('<%=request.getRemoteAddr() %>');
		$("#osVer").val(getOSInfoStr());
		$("#browserNm").val(getBrowserName());
		$("#browserVer").val(getBrowserVer());
		$("#loginForm").submit();
		
		/*
		var pdata = $i.post("/duplicationCheck", {id : $("#userID").val(), pwd : $("#passwd").val(), acctName: $("#ACCT_NM").val()});
		pdata.done(function(res){
			var result = res.returnMessage;
			if(result!=null&&result=='on'){
				$i.dialog.confirm('SYSTEM','중복된 로그인 정보가 존재합니다. 계속하시겠습니까?',function(){
					$("#loginForm").submit();
				});
			}else{
				$("#loginForm").submit();
			}
		});*/
	}
	</script> 
	<style type="text/css">
.msgbox {
	z-index:99999;
	position: absolute;
	top: 50%;
	left: 50%;
	margin: -82.5px 0px 0px -175px;
	width:350px;
	height:165px;
	border: 1px solid #bec2c3;
	/*	left: 40%;
	top: 30%;
	position: absolute;*/
	background:#ffffff url(../../resource/img/msgbox/sessioncheck/bg_box.png) no-repeat 100%;
	padding:10px;
	-webkit-border-radius:4px;
	-moz-border-radius:4px;
	border-radius:4px;
	-webkit-box-shadow: 1px 1px 12px -2px rgba(68, 68, 68, 1);
	-moz-box-shadow: 1px 1px 12px -2px rgba(68, 68, 68, 1);
	box-shadow: 1px 1px 12px -2px rgba(68, 68, 68, 1);
	font-family:"맑은고딕", "Malgun Gothic", Arial;
	letter-spacing:0px;
	font-size:12px;
}
.boxtitle {
	float:left;
	width:100%;
	padding:5px 0;
	border-bottom: 1px solid #e9e9e9;
	margin-bottom:10px;
}
.boxtitle h1 {
	float:left;
	font-size:17px;
	font-weight:normal;
	color:#666;
	letter-spacing:-1px;
	margin:0;
	padding:0;
}
.boxclose {
	float:right;
	margin:0;
	padding:0;
}
.boxbtn {
	float:right;
	border-top: 1px solid #e9e9e9;
	padding-top:10px;
	width:100%;
	text-align:right;
	margin-top:10px;
}
.boxbtn .btn {
	margin:0;
	padding:0;
	padding:2px 10px;
	font-size:12px;
	font-weight:normal;
	color:#fff;
	background:#747474 url(../../resource/img/msgbox/sessioncheck/bg_btn.png) repeat-x;
	border-left:1px solid #6a6a6a;
	border-right:1px solid #6a6a6a;
	border-top:1px solid #717171;
	borde-bottom:1px solid #4a4a4a;
	border-radius:4px;
	text-shadow:none;
	cursor:pointer;
}
.boxbtn p {
	text-align:left;
	margin:0;
	padding:0;
	font-size:11px;
	letter-spacing:-1px;
	color:#555;
	float:left;
}
.boxtable {
	padding:10px;
}
.boxtable table th, .boxtable table td {
	margin:0;
	padding:0;
	padding:5px 0;
	font-size:13px;
	word-break:break-all;
}
.boxtable table th {
	color:#1c62a5;
	width:30%;
	font-weight:normal;
	text-align:left;
	padding-left:20px;
}
.boxtable table td {
	color:#000;
	width:70%;
	text-align:left;
	padding-left:10px;
}
</style>
</head>
<body id="login" >
	<div id="wrap">
		<div id="logo">
			<h1><a href="#"><img src="../../resources/img/login/logo.png" alt="logo"></a></h1>
		</div><!-- logo끝-->
		
		<div id="box">
		<form id="loginForm" action="" method="post" commandName="user">
			<input type="hidden" id="pIp" name="pIp" />
			<input type="hidden" id="osVer" name="osVer" />
			<input type="hidden" id="browserNm" name="browserNm" />
			<input type="hidden" id="browserVer" name="browserVer" />
		<fieldset>
    		<legend> login</legend>
        	<div id="login_box">
				<h1><span>ADMIN LOGIN</span></h1>
 
				 <input type="hidden" name="CSRFTOKENNAME" value="${CSRFTOKENNAME}"/>
				<input type='hidden' name="ACCT_NM" id="ACCT_NM" value = ""/> 
				<p>
					<label for="userID"  class="labelStyle">아이디</label>
					<input name="userID" id="userID"/>
				</p>
				<p>
					<label for="passwd" class="labelStyle">비밀번호</label>
					<input type="password" name="passwd" id="passwd" onKeyPress="if(event.keyCode==13){ login(); return false;}" />
				</p>
				<p style="padding-left:70px; vertical-align:middle;">
					<input type="checkbox" id="checkID" name="checkID"><label for="check" style="color:#333333; font-weight:normal; padding-left:10px; vertical-align:top;">아이디 저장</label>
				</p>
				<p id="btn">
						<input type="button" class="btn_login" value="LOGIN" onclick="login();">     
				</p>
			</div>
		</fieldset>
		</form>
		</div>
		
		<div id="bar">
			<!--남색띠 -->
		</div>
		<div id="bottom">
			<p>COPYRIGHT (C)2013 IPLANBIZ SYSTEM ALL RIGHTS RESERVED ONLINE SERVICE</p>
		</div>
	</div>	
</body>
</html>
