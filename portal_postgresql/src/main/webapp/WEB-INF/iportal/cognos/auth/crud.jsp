<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>${title}</title>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/css/iplanbiz/styles/jqx.blueish-system.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/css/iplanbiz/styles/jqx.simple-gray.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/css/iplanbiz/styles/jqx.empty-style.css"/>
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script> 
		<script type="text/javascript">// button
		var ajaxSignal = "..";
		var curLeftMenu=null;
		$(this).ready(function () {               
		
			init();
		});
		function init(){
			getMenuListData();
			$(".add_layout").hide();
			$("#jqxButtonSave").jqxButton({ width: '',  theme:'blueish'}); 
			$("#jqxButtonAdd").jqxButton({ width: '',  theme:'blueish'}); 
			$("#jqxButtonDelete").jqxButton({ width: '',  theme:'blueish'}); 
			
			$("#jqxButtonFind").jqxButton({ width: '',  theme:'blueish'}); 
			$("#jqxButtonApply").jqxButton({ width: '',  theme:'blueish'}); 
			$("#jqxButtonClose").jqxButton({ width: '',  theme:'blueish'});
			$("#readCheckO").click(function(){
				var id = $("[name='permissionCheck']:checked");
				$("#readCheckN").prop("checked","");
				if(id.length > 0){
					for(var i=0;i<id.length;i++){
						$("[data-id='"+id[i].value+"'] td").eq(4).find("img").remove();
						if($("#readCheckO")[0].checked == true){
							$("[data-id='"+id[i].value+"'] td").eq(4).html("<div class='cell t_center'>"+getImg('0','ok')+"</div>");	
						}	
					}
				}
			});
			$("#readCheckN").click(function(){
				var id = $("[name='permissionCheck']:checked");
				$("#readCheckO").prop("checked","");
				if(id.length > 0){
					for(var i=0;i<id.length;i++){
						$("[data-id='"+id[i].value+"'] td").eq(4).find("img").remove();
						if($("#readCheckN")[0].checked == true){
							$("[data-id='"+id[i].value+"'] td").eq(4).html("<div class='cell t_center'>"+getImg('0','no')+"</div>");	
						}
					}
				}
			});
			$("#writeCheckO").click(function(){
				var id = $("[name='permissionCheck']:checked");
				$("#writeCheckN").prop("checked", "");
				if(id.length > 0){
					for(var i=0;i<id.length;i++){
						$("[data-id='"+id[i].value+"'] td").eq(5).find("img").remove();
						if($("#writeCheckO")[0].checked == true){
							$("[data-id='"+id[i].value+"'] td").eq(5).html("<div class='cell t_center'>"+getImg('1','ok')+"</div>");	
						}
					}
				}
			});
			$("#writeCheckN").click(function(){
				var id = $("[name='permissionCheck']:checked");
				$("#writeCheckO").prop("checked","");
				if(id.length > 0){
					for(var i=0;i<id.length;i++){
						$("[data-id='"+id[i].value+"'] td").eq(5).find("img").remove();
						if($("#writeCheckN")[0].checked == true){
							$("[data-id='"+id[i].value+"'] td").eq(5).html("<div class='cell t_center'>"+getImg('1','no')+"</div>");	
						}
					}
				}
			});
			$("#executeCheckO").click(function(){
				var id = $("[name='permissionCheck']:checked");
				$("#executeCheckN").prop("checked","");
				if(id.length > 0){
					for(var i=0;i<id.length;i++){
						$("[data-id='"+id[i].value+"'] td").eq(6).find("img").remove();
						if($("#executeCheckO")[0].checked == true){
							$("[data-id='"+id[i].value+"'] td").eq(6).html("<div class='cell t_center'>"+getImg('2','ok')+"</div>");	
						}
					}
				}
			});
			$("#executeCheckN").click(function(){
				var id = $("[name='permissionCheck']:checked");
				$("#executeCheckO").prop("checked","");
				if(id.length > 0){
					for(var i=0;i<id.length;i++){
						$("[data-id='"+id[i].value+"'] td").eq(6).find("img").remove();
						if($("#executeCheckN")[0].checked == true){
							$("[data-id='"+id[i].value+"'] td").eq(6).html("<div class='cell t_center'>"+getImg('2','no')+"</div>");	
						}
					}
				}
			});
			$("#traverseCheckO").click(function(){
				var id = $("[name='permissionCheck']:checked");
				$("#traverseCheckN").prop("checked","");
				if(id.length > 0){
					for(var i=0;i<id.length;i++){
						$("[data-id='"+id[i].value+"'] td").eq(8).find("img").remove();
						if($("#traverseCheckO")[0].checked == true){
							$("[data-id='"+id[i].value+"'] td").eq(8).html("<div class='cell t_center'>"+getImg('3','ok')+"</div>");	
						}
					}
				}
			});
			$("#traverseCheckN").click(function(){
				var id = $("[name='permissionCheck']:checked");
				$("#traverseCheckO").prop("checked","");
				if(id.length > 0){
					for(var i=0;i<id.length;i++){
						$("[data-id='"+id[i].value+"'] td").eq(8).find("img").remove();
						if($("#traverseCheckN")[0].checked == true){
							$("[data-id='"+id[i].value+"'] td").eq(8).html("<div class='cell t_center'>"+getImg('3','no')+"</div>");	
						}
					}
				}
			});
			$("#policyCheckO").click(function(){
				var id = $("[name='permissionCheck']:checked");
				$("#policyCheckN").prop("checked","");
				if(id.length > 0){
					for(var i=0;i<id.length;i++){
						$("[data-id='"+id[i].value+"'] td").eq(7).find("img").remove();
						if($("#policyCheckO")[0].checked == true){
							$("[data-id='"+id[i].value+"'] td").eq(7).html("<div class='cell t_center'>"+getImg('4','ok')+"</div>");	
						}
					}
				}
			});
			$("#policyCheckN").click(function(){
				var id = $("[name='permissionCheck']:checked");
				$("#policyCheckO").prop("checked","");
				if(id.length > 0){
					for(var i=0;i<id.length;i++){
						$("[data-id='"+id[i].value+"'] td").eq(7).find("img").remove();
						if($("#policyCheckN")[0].checked == true){
							$("[data-id='"+id[i].value+"'] td").eq(7).html("<div class='cell t_center'>"+getImg('4','no')+"</div>");	
						}
					}
				}
			});
			$("#checkYAuthority").click(function(){
				var id = $("[name='permissionCheck']:checked");
				if(id.length > 0){
					for(var i=0;i<id.length;i++){
						$("[data-id='"+id[i].value+"'] td").eq(4).find("img").remove();
						$("[data-id='"+id[i].value+"'] td").eq(5).find("img").remove();
						$("[data-id='"+id[i].value+"'] td").eq(6).find("img").remove();
						$("[data-id='"+id[i].value+"'] td").eq(7).find("img").remove();
						$("[data-id='"+id[i].value+"'] td").eq(8).find("img").remove();
						if($("#checkYAuthority")[0].checked == true){
							$("[data-id='"+id[i].value+"'] td").eq(4).html("<div class='cell t_center'>"+getImg('0','ok')+"</div>");
							$("[data-id='"+id[i].value+"'] td").eq(5).html("<div class='cell t_center'>"+getImg('1','ok')+"</div>");
							$("[data-id='"+id[i].value+"'] td").eq(6).html("<div class='cell t_center'>"+getImg('2','ok')+"</div>");
							$("[data-id='"+id[i].value+"'] td").eq(7).html("<div class='cell t_center'>"+getImg('3','ok')+"</div>");
							$("[data-id='"+id[i].value+"'] td").eq(8).html("<div class='cell t_center'>"+getImg('4','ok')+"</div>");
						}
					}
				}
			});
			$("#checkNAuthority").click(function(){
				var id = $("[name='permissionCheck']:checked");
				if(id.length > 0){
					for(var i=0;i<id.length;i++){
						$("[data-id='"+id[i].value+"'] td").eq(4).find("img").remove();
						$("[data-id='"+id[i].value+"'] td").eq(5).find("img").remove();
						$("[data-id='"+id[i].value+"'] td").eq(6).find("img").remove();
						$("[data-id='"+id[i].value+"'] td").eq(7).find("img").remove();
						$("[data-id='"+id[i].value+"'] td").eq(8).find("img").remove();
						if($("#checkNAuthority")[0].checked == true){
							$("[data-id='"+id[i].value+"'] td").eq(4).html("<div class='cell t_center'>"+getImg('0','no')+"</div>");
							$("[data-id='"+id[i].value+"'] td").eq(5).html("<div class='cell t_center'>"+getImg('1','no')+"</div>");
							$("[data-id='"+id[i].value+"'] td").eq(6).html("<div class='cell t_center'>"+getImg('2','no')+"</div>");
							$("[data-id='"+id[i].value+"'] td").eq(7).html("<div class='cell t_center'>"+getImg('3','no')+"</div>");
							$("[data-id='"+id[i].value+"'] td").eq(8).html("<div class='cell t_center'>"+getImg('4','no')+"</div>");
						}
					}
				}
			});
			$("#permissionAllCheck").click(function(){
				if($("#permissionAllCheck")[0].checked == true){
					$("[name='permissionCheck']").prop("checked","checked");
					setPermission($("[name='permissionCheck']:checked").eq(0).val(), $("[name='permissionCheck']:checked").eq(0).data("nm"));
				}else{
					$("[name='permissionCheck']").prop("checked","");
				}
				
			});
			$("[name='permissionCheck']").click(function(res){
				alert("응");
				setPermission($("[name='permissionCheck']:checked").eq(0).val(), $("[name='permissionCheck']:checked").eq(0).data("nm"));
			});
			$("#noSelectTableCheckAll").click(function(){
				if($("#noSelectTableCheckAll")[0].checked == true){
					$("#userData [name='noSelectCheck']").prop("checked","checked");
				}else{
					$("#userData [name='noSelectCheck']").prop("checked","");
				}
			});
			$("#selectTableCheckAll").click(function(){
				if($("#selectTableCheckAll")[0].checked == true){
					$("#selectUserData [name='noSelectCheck']").prop("checked","checked");
				}else{
					$("#selectUserData [name='noSelectCheck']").prop("checked","");
				}
			});
		}
		function search(){
			
		}
		function insert(){
			var userPermission 		= getTableInfo('permissionUser',';',',');
			if($("#hiddenCID").val() == ""){
				alert("메뉴를 선택하세요");
				return;
			}
			if($("#permissionUser > tbody > tr").length > 0){
				if(userPermission != ""){
					$i.post("./insert", {cID:$("#hiddenCID").val(), userPermission:userPermission, ac:$("#ac").val()}).done(function(res){
						if($("#ac").val() == 'del'){
							if(res.returnCode == "EXCEPTION"){
								$i.dialog.error("SYSTEM", res.returnMessage);
							}else{
								$i.dialog.alert("SYSTEM", res.returnMessage, function(){
									resetForm();
								});
							}
							$("#ac").val("");
						}else{
							if(res.returnCode == "EXCEPTION"){
								$i.dialog.error("SYSTEM", res.returnMessage);
							}else{
								$i.dialog.alert("SYSTEM", res.returnMessage, function(){
									resetForm();
								});
							}
						}
					});
				}else{
					alert("권한이 맵핑 되지 않은 사용자/그룹/Role이 존재합니다");
				}	
			}else{
				$i.post("./insert", {cID:$("#hiddewnCID").val(), userPermission:userPermission, ac:$("#ac").val()}).done(function(res){
					if($("#ac").val() == 'del'){
						if(res.returnMessage ="EXCEPTION"){
							$i.dialog.error("SYSTEM", res.returnMessage);
						}else{
							$i.dialog.alert("SYSTEM", res.returnMessage, function(){
								resetForm();
							});
						}
						$("#ac").val("");
					}else{
						if(res.returnMessage ="EXCEPTION"){
							$i.dialog.error("SYSTEM", res.returnMessage);
						}else{
							$i.dialog.alert("SYSTEM", res.returnMessage, function(){
								resetForm();
							});
						}
					}
				});
			}
		}
		function getTableInfo(tableId, tr_glue, td_glue){	
			var tr_values = new Array();
			var finalCheck = "true";
					
			$("#"+tableId+" tr:gt(0)").each(function (i){
				var checkData = "";
				var td_values = new Array();
				td_values[0] = $(this).attr("id");
				td_values[1] = $(this).children().next().children().children().attr("name");
				$(this).children(':gt(3)').children().each(function (j){
					
					if($(this).children().length > 0){
						if($(this).children()[0].src.indexOf("_ok.gif") > 0){
							td_values[j+2] ="ok";
							checkData += "true";
						}else if($(this).children()[0].src.indexOf("_no.gif") > 0){
							td_values[j+2] ="no";
							checkData += "true";
						}else{
							td_values[j+2] ="";
							checkData += "false";
						}
					}else{
						td_values[j+2] ="";
						checkData += "false";
					}
				});
				if(checkData.indexOf("true") < 0){
					finalCheck = "false";
				}
				tr_values[i] = td_values.join(td_glue);
			});
			if(finalCheck == "true"){
				return tr_values.join(tr_glue);
			}else{
				return "";
			}
		}
		function getMenuListData(){
			var menuData = $i.post("./getMenuListByCognos",{});
			menuData.done(function(data){
				makeTree(data);
			});
		}
		function makeTree(res){
			var bifolders = Enumerable.From(res.returnArray).Where(function(c){return c.SOURCE_OWNER=="BIFOLDER";}).ToArray();
			for(var i = 0; i < bifolders.length;i++){
				bifolders[i].C_ID=bifolders[i].C_LINK.toUpperCase().replace("STOREID(","").replace("&QUOT;","").replace(")","").replace("&QUOT;","");
				if(bifolders[i].C_ID[0]=="I"){
					bifolders[i].C_ID=bifolders[i].C_ID.replace("I","i");
				}
			}   
			var foldersize = bifolders.length;    
			var data=[];
			for(var i = 0; i<foldersize;i++){
				var menuData = $i.sync.post("./getCognosChildListByArray", {cID:bifolders[i].C_ID, cName:bifolders[i].NAME, clink:$i.secure.TextToScript(bifolders[i].C_LINK), ac:"left"});
				data = data.concat(menuData.returnArray);
			}
			
				for(var j=0; j<data.length;j++){
					var dummy = {
						C_ID : data[j].C_ID, 
						P_ID : data[j].P_ID,
						PID :  data[j].P_ID,
						C_NAME : data[j].C_NAME,
						NAME : data[j].C_NAME,
						SOURCE_OWNER:null,
						PROG_ID:null,  
						cIcon : data[j].cIcon,
						REPORTTYPE : data[j].reportType
					};
// 					dummycount++;
					bifolders.push(dummy);	
				}
				res.returnArray = res.returnArray.concat(bifolders);
				
				curLeftMenu=res.returnArray;
				var tree = $('#treeMenuList');
				var source =
				{
					datatype: "json",
					datafields: [
						{name:'C_ID', type: 'string'},
						{name:'PID', type: 'string'},
						{name:'NAME', type: 'string'},
						{name:'C_NAME', type: 'string'}, 
						{name:'P_NAME', type: 'string'}, 
						{name:'PROG_ID', type: 'string'},
						{name:'SOURCE_OWNER', type: 'string'},
						{name:'REPORTTYPE',  type:'string'}
					],
					id : 'C_ID',
					localdata: res.returnArray
				};
				var dataAdapter = new $.jqx.dataAdapter(source);
				dataAdapter.dataBind();
				
				var records = dataAdapter.getRecordsHierarchy('C_ID', 'PID', 'items', [{ name: 'C_ID', map: 'id'} ,{ name: 'NAME', map: 'label'}, { name: 'REPORTTYPE', map : 'value'}]);
				tree.jqxTree({source: records, height: '100%', width: '100%', allowDrag:false, allowDrop:false, theme:"blueish"});
				
			 	// tree init
				var items = tree.jqxTree('getItems');
				var item = null;
				var size = 0;
				var img = '';
				var label = '';
				var afterLabel = '';
				
				for(var i = 0; i < items.length; i++) {
					item = items[i];
				
					if(i == 0) {
						//root
		// 				tree.jqxTree('expandItem', item);
						img = 'folderIcon.png';
						
						size = $(item.element).find('li').size();
						
						if(size > 0) {
							//have a child
							afterLabel = "<span style='color: Blue;'> (" + size + ")</span>";
						} else {
							afterLabel = "";	
						}	
						
					} else {
						//children
						size = $(item.element).find('li').size();
						
						if(size > 0) {
							//have a child
							img = 'folderIcon.png';
							afterLabel = "<span style='color: Blue;'> (" + size + ")</span>";
						} else {
							//no have a child
							img = 'pageIcon.png';
							//내부에서 사용하는 트리는 팝업 없음.
							afterLabel = "";
						}
					}
					
					label = "<img style='float: left; margin-right: 5px;' src='../../resources/css/images/img_icons/permission/" + img + "'/><span item-title='truec style='vertical-align:middle;'>" + item.label + "</span>" + afterLabel;
					tree.jqxTree('updateItem', item, { label: label});
				}
				
				//add event
				tree
					.on("expand",  {tree:tree},function(event) {
						var args = event.args;
						var tree = event.data.tree;
						var label = tree.jqxTree('getItem', args.element).label.replace('folderIcon', 'folderopenIcon');
						args.element.firstChild.className = args.element.firstChild.className.replace('jqx-tree-item-arrow-collapse', '').replace('jqx-icon-arrow-right', '');
						tree.jqxTree('updateItem', args.element, { label: label});
						var item = $('#treeMenuList').jqxTree('getItem', args.element);
						var curMenu = Enumerable.From(curLeftMenu).Where(function(c){return c.C_ID == item.id;}).FirstOrDefault();
						
						 
						if((curMenu.cType!=null&&(curMenu.cType=="cog"||curMenu.cType=="folder"))||(curMenu.SOURCE_OWNER=="BIFOLDER")){  
						 
							
							var label = item.label;
							$("#arrow" + item.id).attr("class", "jqx-icon-arrow-down");
							var tree = $('#treeMenuList');
							var label = tree.jqxTree('getItem', event.args.element).label;
							var $element = $(event.args.element);
							var loader = false;
							var loaderItem = null;
							var children = $element.find('ul:first').children();
							
							$.each(children, function() {
								var item = tree.jqxTree('getItem', this);
								if (item && item.originalTitle == ajaxSignal) {
									loaderItem = item;
									loader = true;
									return false;
								}
								
							});
							if (loader) {  
								var pmenu = item;//item.id; 
								var reportid = curMenu.PROG_ID!=null?curMenu.PROG_ID:item.id; 
								var menuJSONData = $i.post("./getMenuByJSON", {storeID:reportid, menuName:pmenu});
								menuJSONData.done(function(res){
										var folders = Enumerable.From(res.returnArray).Where(function(c) {
											return c.MENU_TYPE == "1"; 
										}).ToArray();
										for ( var i = 0; i < folders.length; i++) {
												var dummy = {
													C_ID : 'dummy' + dummycount,
													P_ID : folders[i].C_ID,
													C_NAME : ajaxSignal,
													NAME:ajaxSignal,
													cIcon : "loading.gif",
													cType : "dummy",
													SOURCE_OWNER:""
												};
												res.push(dummy); 
												dummycount++;
										}	 
										curLeftMenu = curLeftMenu.concat(res.returnArray);
										
										var source = {
												datatype : "json",
												datafields : [ 
												    {name : 'C_ID'}, 
												    {name : 'PID' },
												    {name : 'C_NAME'}, 
													{name : 'C_LINK'},
													{name : 'cIcon'},
													{name : 'cType'},
													{name : 'NAME'},
													{name : 'PROG_ID'} ,
													{name : 'SOURCE_OWNER'} 
												],
												id : 'id',
												localdata : res
											};
										var dataAdapter = new $.jqx.dataAdapter(source);
										dataAdapter.dataBind();
										var records = dataAdapter.getRecordsHierarchy('C_ID', 'PID','C_LINK',  'items',
												[ {
													name : 'C_ID',
													map : 'id'
												}, {
													name : 'C_NAME',
													map : 'label'
												}, {
													name:'C_LINK',
													map : 'value'
												}  ]);
										var items = records;
										if(items!=null)
											tree.jqxTree('addTo', items, $element[0]);   
										//$("#"+loaderItem.element.id).empty(); 
										
										tree.jqxTree('removeItem', loaderItem.element);
										var cur = $("#treeMenuList").jqxTree("getItem",$element[0]);
										pmenu = Enumerable.From(curLeftMenu).Where(function(c){ return c.C_ID == pmenu.id;}).FirstOrDefault();
										renderTreeNode('tree', cur, pmenu); 
										for ( var i = 0; i < res.length; i++) {
											var item = $("#treeMenuList").jqxTree("getItem",$("#"+res[i].C_ID)[0]);
											renderTreeNode('tree', item, pmenu);                    
										}
								});
							}
						}
					})
					.on("collapse", function(eve) {
						var args = eve.args;
						var label = tree.jqxTree('getItem', args.element).label.replace('folderopenIcon', 'folderIcon');
						tree.jqxTree('updateItem', args.element, { label: label});
					})
					.on("select", function(eve) {
						var args = eve.args;
						var element = tree.jqxTree('getItem', args.element);
						if(element.level == 0){
							$("#hiddenCID").val(element.id);
						}else{
							$("#hiddenCID").val(element.id);
						}
						if(element.label.indexOf("folder") > 0){
							makeDataTable(element.id, element.originalTitle);
							tree.jqxTree('expandItem', $("#"+element.id)[0]);
							if(element.label.indexOf("folderopenIcon") > 0){
								tree.jqxTree('collapseItem', $("#"+element.id)[0]);	
							}
						}else{
							makeDataTable(element.id, element.originalTitle);
						}
					});
			 
		}
		function makeDataTable(cid, cname){
	// 		$("#labelID").html("");
			$(".nCheck").prop("checked","");
			$(".yCheck").prop("checked","");
			$("#checkYAuthority").prop("checked","");
			$("#checkNAuthority").prop("checked","");
			var searchData = $i.post("./getSearchPathUseStoreID", {storeID:cid});
			searchData.done(function(res){
				var pathData = $i.post("./getReportPath", {searchPath:res.returnObject.returnValue});
				pathData.done(function(data){
					var pathArray = data.returnObject.returnValue.split(">");
					var labelHtml = "보고서/폴더 : ";
					for(var i=0; i<pathArray.length; i++){
						if(pathArray[i] != ""){
							if(i==0){
								labelHtml += "<span class='label sublabel type2' style='background:none;'>"+pathArray[i].replace(" ","")+"</span>";	
							}else{
								labelHtml += "<span class='label sublabel type2' >"+pathArray[i].replace(" ","")+"</span>";
							}	
						}
					}
					if(pathArray[0] != ""){
						labelHtml += "<span class='label sublabel type2' >"+cname+"</span>";	
					}else{
						labelHtml += "<span class='label sublabel type2' style='background:none;'>"+cname+"</span>";
					}
					$("#labelPath").html(labelHtml);	
				});
			});
			$("#dataList > tr").remove();
			var dataMenu = $i.post("./getDataMenuPermissionByCognos", {cID : cid});
			dataMenu.done(function(data){
				var html = "";
				for(var i=0;i<data.returnArray.length;i++){
					html += "<tr id='"+data.returnArray[i].ID+"' data-id='"+data.returnArray[i].ID+"' >";
					html += "<td style='width:8%;'>";
					html += "<div class='cell t_center'>";
					html += "<input type='checkbox' name='permissionCheck' value='"+data.returnArray[i].ID+"' data-nm='"+data.returnArray[i].NM+"' onclick=\"setPermission('"+data.returnArray[i].ID+"','"+data.returnArray[i].NM+"');\" />";
					html += "</div>";
					html += "</td>";
					html += "<td style='width:17%;'>"; 
					html += "<div class='cell t_center'>";
					html += getUserIMGCheck(data.returnArray[i].TYPE);
					html += "</div>";
					html += "</td>";
					html += "<td style='width:23%;'>";
					html += "<div class='cell'>";
					html += data.returnArray[i].ID;			
					html += "</div>";
					html += "</td>";
					html += "<td style='width:23%;'>";
					html += "<div class='cell'>";
					html += data.returnArray[i].NM;
					html += "</div>";
					html += "</td>";
					html += "<td style='width:5.8%; border:0px;'>";
					html += "<div class='cell t_center'>";
					html += getImg(0, data.returnArray[i].READ);
					html += "</div>";
					html += "</td>";
					html += "<td style='width:5.8%; border:0px;'>";
					html += "<div class='cell t_center'>";
					html += getImg(1, data.returnArray[i].WRITE);
					html += "</div>";
					html += "</td>";
					html += "<td style='width:5.8%; border:0px;'>";
					html += "<div class='cell t_center'>";
					html += getImg(2, data.returnArray[i].EXECUTE);
					html += "</div>";
					html += "</td>";
					html += "<td style='width:5.8%; border:0px;'>";
					html += "<div class='cell t_center'>";
					html += getImg(3, data.returnArray[i].POLICY);
					html += "</div>";
					html += "</td>";
					html += "<td style='width:5.8%; border:0px;'>";
					html += "<div class='cell t_center'>";
					html += getImg(4, data.returnArray[i].TRAVERSE);
					html += "</div>";
					html += "</td>";
					html += "</tr>";
				}
				$("#dataList").append(html);
			});
		}
		function setPermission(id, name){
			$("#hiddenDataID").val(id);
			$("#checkYAuthority")[0].checked = false
			$("#checkNAuthority")[0].checked = false
			$(".yCheck").prop("checked","");
			$(".nCheck").prop("checked","");
			if($("[data-id='"+id+"'] td").eq(4).find("img").length > 0){
				if($("[data-id='"+id+"'] td").eq(4).find("img")[0].src.indexOf("_ok.gif") > 0){
					$("#readCheckO").prop("checked",true);
				}else{
					$("#readCheckN").prop("checked",true);
				}
			}
			if($("[data-id='"+id+"'] td").eq(5).find("img").length > 0){
				if($("[data-id='"+id+"'] td").eq(5).find("img")[0].src.indexOf("_ok.gif") > 0){
					$("#writeCheckO").prop("checked",true);
				}else{
					$("#writeCheckN").prop("checked",true);
				}
			}
			if($("[data-id='"+id+"'] td").eq(6).find("img").length > 0){
				if($("[data-id='"+id+"'] td").eq(6).find("img")[0].src.indexOf("_ok.gif") > 0){
					$("#executeCheckO").prop("checked",true);
				}else{
					$("#executeCheckN").prop("checked",true);
				}
			}
			if($("[data-id='"+id+"'] td").eq(7).find("img").length > 0){
				if($("[data-id='"+id+"'] td").eq(7).find("img")[0].src.indexOf("_ok.gif") > 0){
					$("#policyCheckO").prop("checked",true);
				}else{
					$("#policyCheckN").prop("checked",true);
				}
			}
			if($("[data-id='"+id+"'] td").eq(8).find("img").length > 0){
				if($("[data-id='"+id+"'] td").eq(8).find("img")[0].src.indexOf("_ok.gif") > 0){
					$("#traverseCheckO").prop("checked",true);
				}else{
					$("#traverseCheckN").prop("checked",true);
				}
			}
		}
		function moveOtherTarget(source, target){
			$('#'+source+' tr:gt(0)').each(function(){
				var html = "";
				html += "<tr id='"+$(this).children().eq(2).text()+"' data-id='"+$(this).children().eq(2).text()+"' >";
				html += "<td style='width:8%;'>";
				html += "<div class='cell t_center'>"; 
				html += "<input type='checkbox' name='permissionCheck' value='"+$(this).children().eq(2).text()+"' data-nm='"+$(this).children().eq(3).text()+"' onclick=\"setPermission('"+$(this).children().eq(2).text()+"','"+$(this).children().eq(3).text()+"');\" />";
				html += "</div>";
				html += "</td>";
				html += "<td style='width:17%;'>";
				html += $(this).children().eq(1).html();
				html += "</td>";
				html += "<td style='width:23%;'>";
				html += "<div class='cell'>";
				html += $(this).children().eq(2).text();
				html += "</div>";
				html += "</td>";
				html += "<td style='width:23%;'>";
				html += "<div class='cell'>";
				html += $(this).children().eq(3).text();
				html += "</div>";
				html += "</td>";
				html += "<td style='width:5.8%; border:0px;'>";
				html += "<div class='cell t_center'>";
				html += "";
				html += "</div>";
				html += "</td>";
				html += "<td style='width:5.8%; border:0px;'>";
				html += "<div class='cell t_center'>";
				html += "";
				html += "</div>";
				html += "</td>";
				html += "<td style='width:5.8%; border:0px;'>";
				html += "<div class='cell t_center'>";
				html += "";
				html += "</div>";
				html += "</td>";
				html += "<td style='width:5.8%; border:0px;'>";
				html += "<div class='cell t_center'>";
				html += "";
				html += "</div>";
				html += "</td>";
				html += "<td style='width:5.8%; border:0px;'>";
				html += "<div class='cell t_center'>";
				html += "";
				html += "</div>";
				html += "</td>";
				html += "</tr>";
	
				$("#"+target).append(html);
			});
			$('#'+source+' tr:gt(0)').remove();
		}
		function getUserIMGCheck(type){
			if(type == 'u'){
				return "<img src='../../resources/cmresource/image/permission/icon_user.gif' name='u' alt='User' />";
			}else if(type == 'p'){
				return "<img src='../../resources/cmresource/image/permission/icon_user_group.gif' name='p' alt='Group' />";
			}else if(type == 'g'){
				return "<img src='../../resources/cmresource/image/permission/icon_user_role.gif' name='g' alt='Role' />";
			}
		}
		function getCheck(value){
			if(value == '1' ){
				return  'ok';
			}else if(value == '0' ){
				return 'no';
			}else{
				return '';
			}
		}
		function getImg(key, value){
			var check = value
			if(check == '' ){
				return '';
			}
			
			if(key == 0){
				return "<img src='../../resources/cmresource/image/permission/ico_read_"+check+".gif' alt='read' />";
			}else if(key == 1){
				return "<img src='../../resources/cmresource/image/permission/ico_right_"+check+".gif' alt='write' />";
			}else if(key == 2){
				return "<img src='../../resources/cmresource/image/permission/ico_done_"+check+".gif' alt='execute' />";
			}else if(key == 3){
				return "<img src='../../resources/cmresource/image/permission/ico_policy_"+check+".gif' alt='policy' />";
			}else if(key == 4){
				return "<img src='../../resources/cmresource/image/permission/ico_traverse_"+check+".gif' alt='traverse' />";
			}
			return ;
		}
		function showPermissionData(){
			if($("#hiddenCID").val() != ""){
				$(".add_layout").show();
				$("[name='r_chk']").eq(0).prop("checked","true");
				makeUserData();	
			}else{
				alert("메뉴를 선택해주세요");
			}
			
		}
		function closePermissionData(){
			$(".add_layout").hide();
		}
		function applyPermissionData(){
			moveOtherTarget('selectTable','dataList');
			$(".add_layout").hide();
		}
		function removePermission(){
			if($("#hiddenCID").val() == ""){
				alert("메뉴를 선택하지 않으셨습니다!");
				return;
			}
			$("input[name=permissionCheck]:checkbox:checked").each(function(index){
				$(this).parent().parent().parent().remove();
			});
			$("#ac").val("del");
			
		}
		function makeSearchData(){
			if($("[name='r_chk']:checked")[0].value == "u"){
				makeUserData();
			}else if($("[name='r_chk']:checked")[0].value == "p"){
				makeGroupData();
			}else if($("[name='r_chk']:checked")[0].value == "g"){
				makeRoleData();
			}
			
		}
		function makeUserData(){
			$("#userData > tr").remove();
			var userData = "";
			var resData = $i.post("./getUserDataByCognos", {cID : $("#hiddenCID").val()});
			resData.done(function(data){
				for(var i=0;i<data.returnArray.length;i++){
					userData += "<tr>";
					userData += "<td style='width:8%;'>";
					userData += "<div class='cell t_center'>";
					userData += "<input type='checkbox' name='noSelectCheck'>"; 
					userData += "</div>";
					userData += "</td>";
					userData += "<td style='width:22%;'>";
					userData += "<div class='cell t_center'>";
					userData += "<img src='../../resources/cmresource/image/permission/icon_user.gif' name='u' alt='User' />";
					userData += "</div>";
					userData += "</td>";
					userData += "<td style='width:35%;'>";
					userData += "<div class='cell'>";
					userData += data.returnArray[i].ID;
					userData += "</div>";
					userData += "</td>";
					userData += "<td style='width:35%;'>";
					userData += "<div class='cell'>";
					userData += data.returnArray[i].NM;
					userData += "</div>";
					userData += "</td>";
					userData += "</tr>";
				}
				$("#userData").append(userData);
			});
		}
		function makeGroupData(cId){
			$("#userData > tr").remove();
			var groupData = "";
			var data = $i.post("./getGroupDataByCognos", {cID:$("#hiddenCID").val()});
			data.done(function (res){
				for(var i=0;i<res.returnArray.length; i++){
					groupData += "<tr>";
					groupData += "<td style='width:8%;'>";
					groupData += "<div class='cell t_center'>";
					groupData += "<input type='checkbox' name='noSelectCheck'>"; 
					groupData += "</div>";
					groupData += "</td>";
					groupData += "<td style='width:22%;'>";
					groupData += "<div class='cell t_center'>";
					groupData += "<img src='../../resources/cmresource/image/permission/icon_user_group.gif' name='p' alt='Group' />";
					groupData += "</div>";
					groupData += "</td>";
					groupData += "<td style='width:35%;'>";
					groupData += "<div class='cell'>";
					groupData += res.returnArray[i].ID;
					groupData += "</div>";
					groupData += "</td>";
					groupData += "<td style='width:35%;'>";
					groupData += "<div class='cell'>";
					groupData += res.returnArray[i].NM;
					groupData += "</div>";
					groupData += "</td>";
					groupData += "</tr>";
				}
			});
		}
		function makeRoleData(){
			$("#userData > tr").remove();
			var roleData = "";
			var data = $i.post("./getRoleDataByCognos", {cID:$("#hiddenCID").val()});
			data.done(function(res){
				for(var i=0;i<res.returnArray.length;i++){
					roleData += "<tr>";
					roleData += "<td style='width:8%;'>";
					roleData += "<div class='cell t_center'>";
					roleData += "<input type='checkbox' name='noSelectCheck'>"; 
					roleData += "</div>";
					roleData += "</td>";
					roleData += "<td style='width:22%;'>";
					roleData += "<div class='cell t_center'>";
					roleData += "<img src='../../resources/cmresource/image/permission/icon_user_role.gif' name='g' alt='Role' />";
					roleData += "</div>";
					roleData += "</td>";
					roleData += "<td style='width:35%;'>";
					roleData += "<div class='cell'>";
					roleData += res.returnArray[i].ID;
					roleData += "</div>";
					roleData += "</td>";
					roleData += "<td style='width:35%;'>";
					roleData += "<div class='cell'>";
					roleData += res.returnArray[i].NM;
					roleData += "</div>";
					roleData += "</td>";
					roleData += "</tr>";
				}
				$("#userData").append(roleData);
			});
		}
		function authorityCheck(gubn){
			if(gubn == "Y"){
				$(".nCheck").prop("checked","");
				$("#checkNAuthority")[0].checked = false
				if($("#checkYAuthority")[0].checked == true){
					$(".yCheck").prop("checked","true");	
				}else{
					$(".yCheck").prop("checked","");
				}
			}else{
				$("#checkYAuthority")[0].checked = false
				$(".yCheck").prop("checked","");
				if($("#checkNAuthority")[0].checked == true){
					$(".nCheck").prop("checked","true");	
				}else{
					$(".nCheck").prop("checked","");
				}
			}
		}
		function resetForm(){
			console.log("resetForm");
			$("#permissionAllCheck").prop("checked", "");
			$(".yCheck").prop("checked","");
			$(".nCheck").prop("checked","");
		}
		function moveAll(source, target){
			
			var obj = $("#"+source+" tr:gt(0)");
			
			obj.clone(true).appendTo("#"+target);
			obj.remove();
			$("#"+target+" tr").removeClass("odd");
			$("#"+target+" tr:even").addClass("odd");
		}
		function moveSelected(source, target){
			
			if($("#"+source+" tr:gt(0) input:checked").size() == 0){
				alert('옮길 대상이 없습니다.');
			}
			var obj = $("#"+source+" tr:gt(0) input:checked").parent().parent().parent();
			
			obj.clone(true).appendTo("#"+target);
			obj.remove();
			$("#"+source+" tr:gt(0)").removeClass("odd");
			$("#"+source+" tr:odd").addClass("odd");
			$("#"+target+" tr:gt(0)").removeClass("odd");
			$("#"+target+" tr:odd").addClass("odd");
			
			$("#"+source+" tr input:checked").attr("checked", false);
			$("#"+target+" tr input:checked").attr("checked", false);
		}
		function setDownExcelData(val){
			var map = new HashMap();
	
	
// 			if(val == "permisstion") downloadExcel(1365, map);
// 			else if(val == "target") downloadExcel(1366, map);
		}
		function permissionBatchProcess() {
			if( $("#uploadFile").val() != "" ){
				var ext = $('#uploadFile').val().split('.').pop().toLowerCase();
				
				if($.inArray(ext, ['xls']) == -1) {
					alert("<spring:message code='_alert.upload.possibility.ext' javaScriptEscape='true' arguments='xls' />");
					return;
				}else{
					var file = dwr.util.getValue('uploadFile');
					dwr.util.useLoadingMessage();
					permissionDwr.permissionBatchProcess(file, function(res){
						alert(res);
					});						
				}
			}else alert("<spring:message code='permissionManager.alert.file' javaScriptEscape='true' arguments='xls' />");
		}
	</script>
	<script type="text/javascript">//tab
	        $(this).ready(function () {
				$('#jqxTabs01').jqxTabs({ width: '100%', height: '100%', position: 'top', theme:'blueish', selectedItem: 0});
	        });
	    </script>
	<script type="text/javascript">
	    $(document).ready(function () { // Create jqxSplitter 
	        $('#jqxSplitter').jqxSplitter({ height: '100%', width: '100%',  panels: [{ size: '23%' }, { size: '77%'}], theme:"blueish"  });
	    });
	</script>
	<style type="text/css">
	.splitter_intab .jqx-widget-content-blueish {
		border:0;
	}
	.splitter_intab .tree .jqx-widget-content-blueish {
		background:#ecf2f8;
	}
	.jqx-tree-grid-icon, .jqx-tree-grid-icon-size {
		width: 16px;
		height: 16px;
	}
	</style>
	</head>
	<body class='blueish'>
	<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%; min-width:1067px; margin:0 1%;">
			<input type="hidden" id="hiddenCID" name="cID" />
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="tabs f_left" id="tableMask" style=" width:100%; height:750px; margin-top:0px;">
					<div id='jqxTabs01'>
						<ul>
							<li style="margin-left: 0px;">Cognos</li>
						</ul>
						<div class="tabs_content" style="height:100%; overflow-y:auto; overflow-x:hidden; padding:0;">
							<div class="content f_left" style="width:100%;">
								<div class="splitter splitter_intab  f_left" style="width:100%; height:718px; margin:0;">
									<div id="jqxSplitter" style="border:0;">
										<div>
											<div  class="tree jqx-hideborder" style="border:0 !important;"  id='treeMenuList'></div>
										</div>
										<div style="overflow-y:auto;">
											<!--splitter Right-->
											<div class="content f_left" style=" width:96%; margin:0 2%;">
												<div class="group f_left  w100p m_t10 m_b10">
													<div class="label type2 f_left" id="labelPath"></div>
													<div class="group_button f_right">
														<div class="button type2 f_left">
															<input type="button" value="저장" id='jqxButtonSave' width="100%" height="100%" onclick="insert();"/>
														</div>
													</div>
													<!--group_button-->
												</div>
												<!--group-->
												<div class="content f_left" style=" width:57%; margin-right:3%;">
													<div class="blueish datatable f_left" style="width:100%; height:274px; margin:0 0 10px 0; overflow:hidden;">
														<!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 헤더가 50px일경우, 300px - 50px= 250px)-->
														<div class="datatable_fixed" style="width:100%; height:274px; float:left; padding:25px 0;">
															<!--group으로 헤더높이가 길어질 경우 padding  (기본높이는 25px)줘야 함-->
															<div style=" height:250px !important; overflow-x:hidden;">
																<table summary="가산점" style="width:100%;" id="permissionUser">
																	<thead style="width:100%;">
																		<tr>
																			<th  scope="col"  style="width:8%; "><input type="checkbox" id="permissionAllCheck"></th>
																			<th  scope="col" style="width:17%;">구분</th>
																			<th  scope="col" style="width:23%;">ID</th>
																			<th scope="col" style="width:23%;">이름</th>
																			<th scope="col" style="width:29%;" colspan="5">적용권한</th>
																			<th style="width:17px; min-width:17px; border-bottom:0;"></th>
																		</tr>
																	</thead>
																	<tbody id="dataList">
																	</tbody>
																</table>
															</div>
														</div>
														<!--datatable_fixed-->
													</div>
													<!--datatable -->
													<div class="group_button f_right">
														<div class="button type5 f_left">
															<input type="button" value="추가" id='jqxButtonAdd' width="100%" height="100%" style="font-size:12px !important;" onclick="showPermissionData();"/>
														</div>
														<div class="button type5 f_left">
															<input type="button" value="삭제" id='jqxButtonDelete' width="100%" height="100%" style="font-size:12px !important;" onclick="removePermission();" />
														</div>
													</div>
													<!--group_button-->
												</div>
												<!--conetent-->
												<div class="content f_right" style=" width:40%;">
	<!-- 												<div class="label type2 f_left">이름 : <span class='label sublabel type2' style="background:none;" id="labelID"></span></div> -->
													<div class="blueish datatable f_left" style="width:100%; height:274px; margin:0 0 10px 0; overflow:hidden;">
														<input type="hidden" id="hiddenDataID" />
														<div class="datatable_fixed" style="width:100%; height:274px; float:left; padding:25px 0;">
															<div style=" height:250px !important; overflow-x:hidden;">
																<table summary="가산점" style="width:100%;">
																	<thead style="width:100%;">
																		<tr>
																			<th scope="col" style="width:50%;">권한</th>
																			<th scope="col" style="width:25%;">
																				<input type="checkbox" id="checkYAuthority" class="m_r5" onclick="authorityCheck('Y')">허용
																			</th>
																			<th scope="col" style="width:25%;">
																				<input type="checkbox" id="checkNAuthority" class="m_r5" onclick="authorityCheck('N');">거부
																			</th>
																			<th style="width:17px; min-width:17px; border-bottom:0;"></th>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<td style="width:50%;">
																				<div class="cell">
																					<img src="../../resources/cmresource/image/permission/ico_read.gif" alt="읽기" class="m_l10 m_r3" /> Read
																				</div>
																			</td>
																			<td style="width:25%;">
																				<div class="cell t_center">
																					<input type="checkbox" class="yCheck" id="readCheckO" name="readCheck" value='1' />
																				</div>
																			</td>
																			<td style="width:25%;">
																				<div class="cel t_center">
																					<input type="checkbox" class="nCheck" id="readCheckN" name="readCheck" value="0" />
																				</div>
																			</td>
																		</tr>
																		<tr class="clicked">
																			<td style="width:50%;">
																				<div class="cell">
																					<img src="../../resources/cmresource/image/permission/ico_right.gif" alt="쓰기" class="m_l10 m_r3" /> Write
																				</div>
																			</td>
																			<td style="width:25%;">
																				<div class="cell t_center">
																					<input type="checkbox" class="yCheck" id="writeCheckO" name="writeCheck" value="1" />
																				</div>
																			</td>
																			<td style="width:25%;">
																				<div class="cel t_center">
																					<input type="checkbox" class="nCheck" id="writeCheckN" name="writeCheck" value="0" />
																				</div>
																			</td>
																		</tr>
																		<tr>
																			<td style="width:50%;">
																				<div class="cell">
																					<img src="../../resources/cmresource/image/permission/ico_done.gif" alt="실행" class="m_l10 m_r3"/> Execute
																				</div>
																			</td>
																			<td style="width:25%;">
																				<div class="cell t_center">
																					<input type="checkbox" class="yCheck" id="executeCheckO" name="executeCheck" value="1" />
																				</div>
																			</td>
																			<td style="width:25%;">
																				<div class="cel t_center">
																					<input type="checkbox" class="nCheck" id="executeCheckN" name="executeCheck" value="0" />
																				</div>
																			</td>
																		</tr>
																		<tr>
																			<td style="width:50%;">
																				<div class="cell">
																					<img src="../../resources/cmresource/image/permission/ico_policy.gif" alt="정책설정" class="m_l10 m_r3" /> Policy
																				</div></td>
																			<td style="width:25%;">
																				<div class="cell t_center">
																					<input type="checkbox" class="yCheck" id="policyCheckO" name="policyCheck" value="1" />
																				</div>
																			</td>
																			<td style="width:25%;">
																				<div class="cel t_center">
																					<input type="checkbox" class="nCheck" id="policyCheckN" name="policyCheck" value="0" />
																				</div>
																			</td>
																		</tr>
																		<tr>
																			<td style="width:50%;">
																				<div class="cell">
																					<img src="../../resources/cmresource/image/permission/ico_traverse.gif" alt="트래버스" class="m_l10 m_r3"/> Traverse
																				</div>
																			</td>
																			<td style="width:25%;">
																				<div class="cell t_center">
																					<input type="checkbox" class="yCheck" id="traverseCheckO" name="traverseCheck" value="1" />
																				</div>
																			</td>
																			<td style="width:25%;">
																				<div class="cel t_center">
																					<input type="checkbox" class="nCheck" id="traverseCheckN" name="traverseCheck" value="0" />
																				</div>
																			</td>
																		</tr>
																	</tbody>
																</table>
															</div>
														</div>
														<!--datatable_fixed-->
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</br>
				</br>
			</div>
		</div>
		<div class="content f_left add_layout" id="popupMask" style="width:100%; height:100%; background:url(../../resources/img/opa_bg.png) 100%; margin-top:0px;margin-left:0%; margin-bottom:0px; position: absolute;">
			<div class="add_layer_in" style="position:relative; margin:4% 0% 0% 25%; width:850px; height:650px; background-color: #FFFFFF; border: 2px solid #707070;">
				<div style="margin:2% 2% 2% 2%;">
					<div class="group f_left  w100p m_t10 m_b20">
						<div class="group  w100p">
							<div class="label type2 f_left">ID</div>
							<input type="text" id="txtSearchValue" class="input type1  f_left"  style="width:200px; margin:3px 5px;" onkeydown="if(event.keyCode == 13){makeSearchData();}"/>
							<div class="button type2 f_left">
								<input type="button" value="검색" id='jqxButtonFind' width="100%" height="100%" onclick="makeSearchData();"/>
							</div>
						</div>
						<div class="group f_left m_t5 m_l20">
							<label for=""><input type="radio" name="r_chk" value="u" class="m_b2 m_r3" onclick="makeUserData();">User</label>
							<label for=""><input type="radio" name="r_chk" value="p" class="m_b2 m_r3 m_l5" onclick="makeGroupData();">Group</label>
							<label for=""><input type="radio" name="r_chk" value="g" class="m_b2 m_r3 m_l5" onclick="makeRoleData();">Role</label>
						</div>
						<div class="group_button f_right">
							<div class="button type2 f_left">
								<input type="button" value="적용" id='jqxButtonApply' width="100%" height="100%" onclick="applyPermissionData();" />
							</div>
							<div class="button type2 f_left">
								<input type="button" value="닫기" id='jqxButtonClose' width="100%" height="100%" onclick="closePermissionData();" />
							</div>
						</div>
					</div>
					<div class="content f_left" style=" width:45%; margin-right:0%;">
						<div class="blueish datatable f_left" style="width:100%; height:550px; margin:0 0 10px 0; overflow:hidden;">
							<div class="datatable_fixed" style="width:100%; height:550px; float:left; padding:25px 0;">
								<div style=" height:525px !important; overflow-x:hidden;">
									<table summary="가산점" style="width:100%;" id="noSelectTable">
										<thead style="width:100%;">
											<tr>
												<th  scope="col"  style="width:8%; "><input type="checkbox" id="noSelectTableCheckAll"></th>
												<th  scope="col" style="width:22%;">구분</th>
												<th  scope="col" style="width:35%;">ID</th>
												<th scope="col" style="width:35%;">이름</th>
												<th style="width:17px; min-width:17px; border-bottom:0;"></th>
											</tr>
										</thead>
										<tbody id="userData">
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="group_button f_left" style=" width:4%;  margin:25% 3%;"><!--이동버튼-->
						<div class="group">
							<div class="pointer m_b10" style=" width:100%;">
								<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/btn-hover-move-all-right.png" alt="모두선택" onclick="moveAll('noSelectTable','selectTable');" >
							</div>
							<div class="pointer m_b10 " style=" width:100%;">
								<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/btn-hover-move-all-left.png" alt="모두선택해제" onclick="moveAll('selectTable','noSelectTable');" >
							</div>
							<div class="pointer m_b10" style=" width:100%;">
								<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/btn-hover-move-right.png" alt="오른쪽으로" onclick="moveSelected('noSelectTable','selectTable');">
							</div>	
							<div class="pointer" style=" width:100%;">
								<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/btn-hover-move-left.png" alt="왼쪽으로" onclick="moveSelected('selectTable','noSelectTable');">
							</div>		
						</div>
					</div>
					<div class="content f_right" style=" width:45%;">
						<div class="blueish datatable f_left" style="width:100%; height:550px; margin:0 0 10px 0; overflow:hidden;">
							<div class="datatable_fixed" style="width:100%; height:550px; float:left; padding:25px 0;">
								<div style=" height:525px !important; overflow-x:hidden;">
									<table summary="가산점" style="width:100%;" id="selectTable">
										<thead style="width:100%;">
											<tr>
												<th  scope="col"  style="width:8%; "><input type="checkbox" id="selectTableCheckAll"></th>
												<th  scope="col" style="width:22%;">구분</th>
												<th  scope="col" style="width:35%;">ID</th>
												<th scope="col" style="width:35%;">이름</th>
												<th style="width:17px; min-width:17px; border-bottom:0;"></th>
											</tr>
										</thead>
										<tbody id="selectUserData"></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>