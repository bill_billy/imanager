<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="com.iplanbiz.iportal.config.WebConfig"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head> 
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>공지사항</title>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
	    <script src="../../resources/js/util/remoteInfo.js"></script>
	    <script src="../../resources/cmresource/js/thirdparty/LGPL/v2/smartEditor2/js/HuskyEZCreator.js" charset="UTF-8"></script>
		<script src="${WEB.JSUTIL}/common.js"></script>
		
		<script language="javascript" type="text/javascript">
		<%
		String uploadFileSizeLabel = String.valueOf((WebConfig.getDefaultMaxUpSize()/1024)/1024);
		String uploadFileSize = String.valueOf(WebConfig.getDefaultMaxUpSize());
		
		%>
		var uploadFileSize = "<%=uploadFileSize%>";
		var uploadFileSizeLabel = "<%=uploadFileSizeLabel%>";
		var msg = "${msg}";
		var oEditors = [];
		
		var remoteIP = '<%=request.getRemoteAddr() %>';
		var remoteOS = getOSInfoStr();
		var remoteBrowser = getBrowserName();
		
		function initSmartEditor(id) {
			nhn.husky.EZCreator.createInIFrame({
				oAppRef: oEditors,
				elPlaceHolder: id,
				sSkinURI : "../../resources/cmresource/js/thirdparty/LGPL/v2/smartEditor2/SmartEditor2Skin.html",
				htParams : {bUseToolbar : true,
					fOnBeforeUnload : function(){
					}
				},
				fOnAppLoad : function(){
					//oEditors.getById["ir1"].exec("PASTE_HTML", ["로딩이 완료된 후에 본문에 삽입되는 text입니다."]);
				},
				fCreator: "createSEditor2"
			});
		}
		$(document).ready(function(){
    		init(); 
//     		initAjaxForm();
    	}); 
		function checkSize(idx){
			if($("input[name='file']")[idx].value != ""){
				var FileFilter = /\.(xls|xlsx|ppt|pptx|doc|docx|hwp|pdf)$/i;
				var fileValue = $("input[name='file']")[idx].value.toLowerCase();
				var fileCheck = false;
				var fileNameSize = false;
				var fileSizeCheck = false;
				var fileSize = $("input[name='file']")[idx].files[0].size;
				var fileName = $("input[name='file']")[idx].files[0].name;
				var defaultSize = "3145728";
				var fileList = $("input[name='file']");
				var totalFileSize = "0";
				var totalFileCheck = false;
				for(var i=0;i<fileList.length;i++){
					totalFileSize = (parseInt(totalFileSize) + parseInt($("#file"+idx)[0].files[0].size));
				}
				if(parseInt(totalFileSize) < parseInt(uploadFileSize)){
					totalFileCheck = true;
				}
				if(parseInt(fileSize) < parseInt(defaultSize)){
					fileSizeCheck = true;
				}
				if(fileValue.match(FileFilter)){
					fileCheck = true;
				}
				if(byteCheck(fileName)< 50){
					fileNameSize = true;
				}
				if(totalFileCheck == true){
					if(fileSizeCheck == true){
						if(fileCheck == true){
							if(fileNameSize == true){
								$("#fileName"+idx).val($("input[name='file']")[idx].value);
							}else{
								var length = parseInt((50 / 3));
								$i.dialog.warning('SYSTEM', length + "자(한글기준)의 한글/영문/특수문자/숫자 혼용만 가능합니다.");
			    				$("input[name='file']")[idx].focus();
							}
						}else{
							$i.dialog.warning('SYSTEM',"업로드가 가능한 확장자가 아닙니다.");
						}
					}else{
						$i.dialog.warning('SYSTEM',"파일 용량이 초과하였습니다.");
					}
				}else{
					$i.dialog.warning("SYSTEM", "전체 파일 용량이 "+uploadFileSizeLabel+"MB을 초과하였습니다.");
				}
			}
		}
		function init(){
			$("#labelFileSize").html("(※전체 업로드 용량은 최대 "+uploadFileSizeLabel+"MB가 입니다.)");
			$("#dateBeginDay").jqxInput({placeHolder: "", height: 21, width: 120, minLength: 1, theme:'blueish' });
			$("#dateEndDay").jqxInput({placeHolder: "", height: 21, width: 120, minLength: 1, theme:'blueish' });
			$( "#dateBeginDay" ).datepicker({
				changeYear: false,
				showOn: "button",
				buttonImage: "../../resources/img/calendar.gif",
				buttonImageOnly: true,
				dateFormat: "yy-mm-dd",
				onSelect:function(dateText){
					if($("#dateEndDay").val() != ""){
						if($("#dateEndDay").val() < $("#dateBeginDay").val()){
							alert("종료일자가 시작일자보다 과거입니다.");
							$("#dateEndDay").val("");
						}else{
							search();
						}	
					}
				}
			});
    		$( "#dateEndDay" ).datepicker({
    			changeYear: false,
				showOn: "button",
				buttonImage: "../../resources/img/calendar.gif",
				buttonImageOnly: true,
				dateFormat: "yy-mm-dd",
				onSelect:function(dateText){
					if($("#dateEndDay").val() < $("#dateBeginDay").val() || $("#dateBeginDay").val() == ""){
						alert("종료일자가 시작일자보다 과거입니다.");
						$("#dateEndDay").val("");
					}else{
						search();
					}	
				}
				
			});
			$("#btnList").jqxButton({ width: '',  theme:'blueish'}); 
            $("#btnInsert").jqxButton({ width: '',  theme:'blueish'}); 
			$("#btnDelete").jqxButton({ width: '',  theme:'blueish'});
			$('#saveForm').jqxValidator({
				rtl:true,  
				rules:[
				    { input: "#txtTitle", message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtUserName', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#dateBeginDay', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required'},
                    { input: '#dateEndDay', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required'}
		        ]
			});
			initSmartEditor('txtAreaContext');
			search("${param.boardNO}");	
			getAuthorityData();
			
			$("#hiddenRemoteIP").val(remoteIP);
	        $("#hiddenRemoteOS").val(remoteOS);
	        $("#hiddenRemoteBW").val(remoteBrowser);
		}
		function userPopup(){
			window.open('../../ktoto/notice/popupUserList', 'popupUserList','scrollbars=no, resizable=no, width=630, height=580');	
		}
		function search(boardNo){
			$i.post("./getKtotoNoticeDetail",{boardNO:boardNo}).done(function(data){
				if(data.returnCode == "SUCCESS"){
					$("#hiddenBoardNO").val(data.returnObject.BOARDNO);
					$("#txtTitle").val(data.returnObject.TITLE);
					$("#hiddenUserID").val(data.returnObject.USERID);
					$("[name='informYn'][value='"+data.returnObject.INFORM_YN+"']").prop("checked", "checked");
					$("[name='popup'][value='"+data.returnObject.POPUP+"']").prop("checked", "checked");
					$("#txtUserName").val(data.returnObject.NAME);
					$("#dateBeginDay").val(data.returnObject.BEGINDAY);
					$("#dateEndDay").val(data.returnObject.ENDDAY);
					$("#txtAreaContext").val(data.returnObject.CONTENT);
				}
			});
			$i.post("./getNoticeDtailFileInfo",{boardNO:boardNo}).done(function(data){
				$("#fileList > div").remove();
				var html = "";
				if(data.returnCode == "SUCCESS"){
					for(var i=0; i<data.returnArray.length; i++){
						html += "<div class='group' style='width:93%;'><a href=\"javaScript:fileRemove('"+data.returnArray[i].FILE_ID+"','"+boardNo+"')\"><span style='font-weight: bold; padding-right: 10px;'>"+data.returnArray[i].FILE_ORG_NAME+"</SPAN><img src='../../resource/css/images/img_icons/cancel.png' border='0' alt='' /></a></div>";
					}
				}
				html += "<div class='group' style='width:90%;'>";
                html += "<div class='file_input'  style='width:100%;'>";
                html += "<input type='text' id='fileName0' name='fileName' class='file_input_textbox' readonly style='width:76%;'>"; 
                html += "<div class='button'>";
                html += "<input type='button' value='찾아보기' class='file_input_button' />";
                html += "<input type='file' id='file0' name='file' class='file_input_hidden' onChange=\"javascript:checkSize(0);\" accept='application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, .pdf, .hwp'/>";
                html += "</div>";
                html += "</div>";
                html += "<div class='button_action'>";
                html += "<span class='pointer f_right p_r20' style='margin:0;'>";
                html += "<img src='../../resources/cmresource/css/iplanbiz/theme/style01/img/icon-cir-plus.png' alt='추가' onClick='addFile();' height=15>";
                html += "</span>";
                html += "<br>";
                html += "</div>";
                html += "<span style='color:red;'>※파일당 업로드 용량은 최대 3MB가 입니다.</span>"
            	html += "</div>";
				$("#fileList").append(html);
			});
		}
		function addFile(){
			var index = $("[name='fileName']").length;
			if(index > 4){
				$i.dialog.error("SYSTEM", "첨부파일 최대 갯수는 5개 입니다.");
			}else{
				var html = "";
				html += "<div class='group' style='width:90%;' id='fileDiv"+index+"'>";
				html += "<div class='file_input'  style='width:100%;'>";
				html += "<input type='text' id='fileName"+index+"' name='fileName' class='file_input_textbox' readonly style='width:76%;'>";
				html += "<div class='button'>";
				html += "<input type='button' value='찾아보기' class='file_input_button' />";
				html += "<input type='file' id='file"+index+"' name='file' class='file_input_hidden' onChange=\"javascript:checkSize("+index+");\" accept='application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, .pdf, .hwp'/>";
				html += "</div>";
				html += "</div>";
				html += "<div class='button_action'>";
				html += "<span class='pointer f_right p_r20' style='margin:0;'>";
				html += "<img src='../../resources/cmresource/css/iplanbiz/theme/style01/img/icon-cir-minus.png' alt='빼기'  onclick='removeFile("+index+");' height=15>";
				html += "</span>";
				html += "<br>";
				html += "</div>";
				html += "<span style='color:red;'>※파일당 업로드 용량은 최대 3MB가 입니다.</span>"
				html += "</div>";
				$("#fileList").append(html);
			}
			
		}
		function removeFile(index){
			$("#fileDiv"+index).remove();
		}
		function fileRemove(fileID, boardNO){
			location.replace("../../ktoto/notice/deleteFile?fileId="+fileID+"&boardNo="+boardNO);
		}
		function filedownLoad(fileID){
			location.replace("../../cms/board/download?fileId="+fileID);
		}
		function  goList(){
			location.replace("../../ktoto/notice/list");
		}
		function getAuthorityData(){
			var treeData = $i.post("./getKtotoNoticeAuthList", {});
			treeData.done(function(data){
				makeTree(data.returnArray);
			});
		}
		function makeTree(data){
			$("#treeGridAuthorityList").jqxTreeGrid('clear');
			var source = {
				datatype: "json",
				datafields: [
					{ name: 'C_ID'},
					{ name: 'PID'},
					{ name: 'NAME'},
					{ name: 'TYPE'}, 
					{ name: 'C_VALUE'}
				],
				icons:true,
				altrows:false,
				pageable:false,  
				localdata: data,
				hierarchy:{         
					keyDataField:{name:'C_ID'},
					parentDataField:{name:'PID'} 
				},
				id:'C_ID'
			};
			var dataAdapter = new $.jqx.dataAdapter(source);
			
			$("#treeGridAuthorityList").jqxTreeGrid({
				source: dataAdapter,
				height: '100%',
				width: '100%',
				altRows: true,
				icons: true,
          		sortable: true,
          		editable: true,
				editSettings: {
			 		saveOnPageChange: true,
			 		saveOnBlur: true,
			 		saveOnSelectionChange: true,
			 		cancelOnEsc: true,
			 		saveOnEnter: true,
			 		editSingleCell: true,
			 		editOnDoubleClick: true,
			 		editOnF2: true
		    	},
		    	columnsHeight:40,
		    	enableHover: true ,
		    	theme:"blueish",
		    	checkboxes: true,
				icons:function(rowKey, rowdata) { return getIconImage(rowdata.TYPE); },
				columns:[ 
					{ dataField:'C_ID',text:'C_ID',align:'center', hidden:true},
					{ dataField:'NAME',text:'NAME', algin:'center'
						, renderer:function(text,align,height){
							return "<div style=' margin-top:4px; text-align:center;'><img width='16' height='16' style='' src='../../../resources/cmresource/image/permission/icon_user_role.gif'/><span style='margin-left: 4px; '>보고서명</span><div style='clear: both;'></div></div>";
						}
						,cellsrenderer:function(row,datafield,value,rowdata){
							var newValue = rowdata.NAME;
							if(rowdata.records!=null){
								var cntlabel = "<span style='color: Blue;' data-role=='treefoldersize'> (" + rowdata.records.length + ")</span>";
								return "<a href=\"javascript:makeTab1Grid("+rowdata.C_ID+" );\" style='color:#000000;text-decoration:underline;'>"+newValue+cntlabel+"</a>";
							} else {
								return "<a href=\"javascript:makeTab1Grid("+rowdata.C_ID+" );\" style='color:#000000;text-decoration:underline;'>"+newValue+"</a>";
							}
						}
					}
				],
				ready:function(){
				}
			});
			if("#{param.type}" != "N"){
				var checkData = $i.post("./getKtotoNoticeSelectAuthData", {boardNO:"${param.boardNO}"});
				checkData.done(function(data){
					for(var i=0;i<data.returnArray.length;i++){
						$("#treeGridAuthorityList").jqxTreeGrid("checkRow",data.returnArray[i].CHECKFKEY);
					}
				});	
			}
			$("#treeGridAuthorityList").jqxTreeGrid({ width: '100%',  height: 200,  theme:'simple-gray' });
			$("#treeGridAuthorityList").css('visibility', 'visible');
		}
		function getIconImage(type) {
			if(type == 'u'){
				return "../../resources/cmresource/image/permission/icon_user.gif";
			}else if(type == 'p'){
				return "../../resources/cmresource/image/permission/icon_user_group.gif";
			}else if(type == 'g'){
				return "../../resources/cmresource/image/permission/icon_user_role.gif";
			}
		}
		function insert(){
			if($("#saveForm").jqxValidator("validate") == true){
				oEditors.getById["txtAreaContext"].exec("UPDATE_CONTENTS_FIELD", []);
				if($("#txtTitle").val() == null || $("#txtTitle").val() == ""){
					$i.dialog.warning('SYSTEM','<spring:message code="board.alert.title" javaScriptEscape="true" />');
					return false;
				}
				var smartCheck = oEditors.getById["txtAreaContext"].getIR();
				if(byteCheck(fRemoveHtmlTag(smartCheck)) > 60000){
					$i.dialog.warning('SYSTEM',"1~20000자(한글 기준)의 한글/영문/특수문자 혼용만 가능합니다.");
					return false;
				}
				var checkItem = $("#treeGridAuthorityList").jqxTreeGrid("getCheckedRows");
				if($("[name='popup']:checked").val() != "N"){
					if(checkItem.length <= 0){
						$i.dialog.warning('SYSTEM',"권한을 선택해주세요.");
						return false;
					}
				}
				var html = "";
				if(checkItem.length > 0){
					for(var i=0; i<checkItem.length;i++){
						html +="<input type='hidden' name='checkfkey' value='"+checkItem[i].C_ID+"'/>";
						html +="<input type='hidden' name='fkey' value='"+checkItem[i].C_VALUE+"'/>";
						html +="<input type='hidden' name='type' value='"+checkItem[i].TYPE+"'/>";
					}	
				}else{
					html +="<input type='hidden' name='checkfkey' value=''/>";
					html +="<input type='hidden' name='fkey' value=''/>";
					html +="<input type='hidden' name='type' value=''/>";
				}
				$("#saveForm").append(html);
				document.getElementById("saveForm").submit();
			}
		}
		function remove(){
			$("#saveForm").attr("action","./remove");
			document.getElementById("saveForm").submit();
		}
		function initAjaxForm(){
    		$("#saveForm").ajaxForm({
                beforeSubmit: function (data,form,option) {
                	if(form.attr("action").indexOf("insert")!=-1)
//     			 		$i.dialog.progress("SYSTEM","FILEUPLOAD","파일을 업로드 하였습니다.");  
                    return true;
                },
                success: function(response,status){
                	var result = null;
                	try{
                		result = JSON.parse(response);
                	}catch(e){
                		$i.dialog.error("SYSTEM","오류가 발생 하였습니다.");	
                		return; 
                	}
                	var alert = null;  
        			if(result.returnCode!="SUCCESS") {
        				$i.dialog.error("SYSTEM", result.returnMessage);
        			}
        			else {
        				$i.dialog.alert("SYSTEM", result.returnMessage, function(){
        					goList();
        				});
        			}  
        			//삭제 후 alert
        			if($("#saveForm").attr("action").indexOf("remove")!=-1 )
        				alert("SYSTEM", result.returnMessage);
                },
                error: function(){
    				
                }                               
            });
    	}
		function fRemoveHtmlTag(string) { 
		   var objReplace = new RegExp();
		   var objnbsp = new RegExp();
		   objReplace = /[<][^>]*[>]/gi; 
		   objnbsp = /&nbsp;/gi;
		   return string.replace(objReplace, "").replace(objnbsp,"").replace(/(\s*)/g, ""); 
		} 
		function dayCheck(){
			if($("#endDat").val() < $("#startDat").val()){
				alert("종료일자가 시작일자보다 과거입니다.");
				$("#endDat").val("");
			}else{
				searchStart();
			}
		}
		function checkInput(id, size, gubn){
    		if(byteCheck($("#"+id).val()) > size){
    			var length = "";
    			if(gubn == "en"){
    				length = parseInt((size / 1));
    				alert( length + "자(영문 기준)의 영문/_ 혼용만 가능합니다.");
    			}else if(gubn == "all"){
    				length = parseInt((size / 3));
    				alert( length + "자(한글 기준)의 한글/영문/특수문자 혼용만 가능합니다.");
    			}else if(gubn =="num"){
    				length = parseInt((size / 1));
    				alert( length + "자의 숫자만 가능합니다.");
    			}else if(gubn =="kor"){
    				length = parseInt((size / 3));
    				alert( length + "자(한글 기준)의 한글만 가능합니다.");
    			}
    			$("#"+id).focus();
    			return false;
    		}
    	}
	 	function byteCheck(code) {
			var size = 0;
			for (i = 0; i < code.length; i++) {
				var temp = code.charAt(i);
				if (escape(temp) == '%0D')
			   		continue;
			  	if (escape(temp).indexOf("%u") != -1) {
			   		size += 3;
			  	} else {
			   		size++;
			 	}
		 	}
		 	return size;
		}
		function setPagerLayout(selector) {
			
			var pagesize = $('#'+selector).jqxGrid('pagesize');
			
			var w = 49; 
				
			if(pagesize<100) {
				w = 44;
			} else if(pagesize>99&&pagesize<1000) {
				w = 49;
			} else if(pagesize>999&&pagesize<10000) {
				w = 54;
			}
			
			//디폴트 셋팅
			$('#gridpagerlist'+selector).jqxDropDownList({ width: w+'px' });
			
			//체인지 이벤트 처리
			$('#'+selector).on("pagesizechanged", function (event) {
				var args = event.args;
				
				if(args.pagesize<100) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '44px' });
				} else if(args.pagesize>99 && args.pagesize<1000) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '49px' });
				} else if(args.pagesize>999 && args.pagesize<10000) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '54px' });
				} else {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: 'auto' });
				}
				
			});
		}
		</script>
		<style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
			.blueish .table table th{
				height:35px !important;
			}
			.ui-datepicker-trigger{
				float:left;
				margin-top:5px;
				margin-left:5px;
			}
			.jqx-validator-hint{
        		background-color : transparent !important;
        		border:0px !important;
        	}
        	.jqx-validator-hint-arrow{
        		display:none !important;
        	}
        	#treeGridAuthorityList .jqx-widget-content-blueish {
				background:#ffffff;
			}
			#treeGridAuthorityList .jqx-widget-header-blueish { 
	    		display : none !important; 
		 	}
		 	#treeGridAuthorityList .jqx-item-blueish {
		 		border-color : #ffffff !important;  
	    		 
		 	}
		 	.jqx-tree-grid-checkbox-empty-style {
			   	 float: none !important;
			   	 width: 13px; /*수정(기존 14px)*/
			   	 height: 13px; /*수정(기존 14px)*/
			     cursor: pointer;
			     margin-right: 2px !important;
			     margin-left: 2px !important;
			}
			.treegrid .jqx-widget-blueish .jqx-grid-column-header-blueish {
				border-color : #ffffff !important;
			}
			.jqx-grid-column-header-blueish {
				border-color : #ffffff !important;
			}
		</style>
		<style>
			.border_top{border-top:1px solid #dedede}
		</style>
	</head>
	<body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1067px; margin:0 1%;">
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:100%;">
					<div class="group f_left  w100p m_b5">
						<div class="label type2 f_left">
							공지사항 등록
						</div>
						<div class="group_button f_right">
							<div class="button type2 f_left" style="margin-bottom:0;">
								<input type="button" value="목록" id='btnList' width="100%" height="100%" onclick="goList();"/>
							</div>
							<div class="button type2 f_left" style="margin-bottom:0;">
								<input type="button" value="저장" id='btnInsert' width="100%" height="100%" onclick="insert();"/>
							</div>
							<div class="button type3 f_left" style="margin-bottom:0;">
								<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();"/>
							</div>
						</div>
						<!--group_button-->
					</div>
					<!--group-->
					<div class="table  f_left" style="width:100%; margin:0; ">
						<form:form action="../../ktoto/notice/save" id="saveForm" name="saveForm" method="post" commandName="notice" enctype="multipart/form-data">
						<input type="hidden" name="saveGubn" value="${param.type}" />
						<input type="hidden" id="hiddenRemoteIP" name="remoteIP" />
						<input type="hidden" id="hiddenRemoteOS" name="remoteOS" />
						<input type="hidden" id="hiddenRemoteBW" name="remoteBrowser" />
						<table width="100%" cellspacing="0" cellpadding="0" border="0">
							<tbody>
								<tr>
									<th class="w15p"><div>
											제목<span class="th_must"></span></div></th>
									<td colspan="3">
										<div class="cell activetable">
											<input type="hidden" id="hiddenBoardNO" name="boardNO" />
											<input type="text" id="txtTitle" name="title" class="input type2" style="height: 17px; width: 95%; margin: 5px 0; padding-bottom: 3px;" onblur="checkInput('title','255','all');" />
										</div>
									</td>
								</tr>
								<tr>
									<th>
										<div>알림</div>
									</th>
									<td class="w35p">
										<div  class="cell">
		                              		<span style="margin-right:10px;">
		                              			<input name="popup" type="radio" value="N" checked="checked"> 알림 없음
		                              		</span>
		                              		<span>
		                              			<input name="popup" type="radio" value="E"> 메일
		                              		</span>
		                              		<span>
		                              			<input name="popup" type="radio" value="P"> 팝업
		                              		</span>
		                             	</div>
		                            </td>
									<th class="w15p"><div>로그인 시 알림</div></th>
									<td class="w35p">
										<div class="cell">
		                              		<span style="margin-right:10px;">
		                              			<input name="informYn" type="radio" value="Y"> Y
		                              		</span>
		                              		<span>
		                              			<input name="informYn" type="radio" value="N" checked="checked"> N
		                              		</span>
										</div>
									</td>
								</tr>
								<tr>
									<th><div>
											작성자</div></th>
									<td class="w35p">
										<div  class="cell">
											<input type="hidden" id="hiddenUserID" name="userID" />
											<input type="text" id="txtUserName" name="userName" class="input type2  f_left"  style="width:150px; margin:3px 0; " readonly="readonly"/>
											<div class="icon-search f_left m_t7 pointer" onclick="userPopup();"></div>
										</div>
									</td>
									<th class="w15p"><div>
											공고기간</div></th>
									<td class="w35p">
										<div  class="cell">
											<input id="dateBeginDay" name="beginDay" type="text" class="input type2 t_center f_left" style="width:100px; margin:5px 0;" readonly="readonly"/>
											<div class="f_left m_l5 m_r5 m_t2">&sim;</div>
											<input id="dateEndDay" name="endDay" type="text" class="input type2 t_center f_left" style="width:100px; margin:5px 0;" readonly="readonly"/>
										</div>
									</td>
								</tr>
								<tr>
									<th><div>내용</div></th>
									<td colspan="3"  class="activetable">
		                            	<div  class="cell" >
											<textarea id="txtAreaContext" name="context" class="textarea type1"  value="" style=" width:99.5%; height:300px; margin:3px 0;" ></textarea>
										</div>
		                            </td>
								</tr>
								<tr>
									<th><div>권한</div></th>
									<td>
		                            <div  class="cell">
		                            <div class="splitter  f_left" style="width:100%; margin:7px 0;">
		                            	<div style="width:100%;height:200px !important; overflow:auto;" id='treeGridAuthorityList' class='treegrid'></div>
		                            </div>
		                            <!--splitter-->
		                            </div>
		                            </td>
									<th><div>첨부파일</div><span style="color:red;" id="labelFileSize"></span></th>
		                            <td>
		                            	<div  class="cell">
		                                    <div class="fileupload" id="fileList" style=" width:100%; margin:5px 0; height:200px; overflow-y:scroll;">
<!-- 		                                        <div class="group" style="width:93%;"> -->
<!-- 		                                            <div class="file_input"  style="width:100%;"> -->
<!-- 		                                                <input type="text" id="fileName1" class="file_input_textbox" readonly style="width:86%;"> -->
<!-- 		                                                <div class="button"> -->
<!-- 		                                                    <input type="button" value="찾아보기" class="file_input_button" /> -->
<!-- 		                                                    <input type="file" name="mb_icon" class="file_input_hidden" onChange="javascript: document.getElementById('fileName1').value = this.value" /> -->
<!-- 		                                                </div> -->
<!-- 		                                            </div> -->
<!-- 		                                            <div class="button_action"> -->
<!-- 		                                                <span class="pointer f_right p_r20" style="margin:0;"> -->
<!-- 		                                                    <img src='../../resources/cmresource/css/iplanbiz/theme/style01/img/icon-cir-plus.png' alt="추가" onClick="" height=15> -->
<!-- 		                                                </span> -->
<!-- 		                                                <br> -->
<!-- 		                                            </div> -->
<!-- 		                                        </div> -->
		                                        <!--group-->
<!-- 		                                        <div class="group" style="width:93%;"> -->
<!-- 		                                            <div class="file_input"  style="width:100%;"> -->
<!-- 		                                                <input type="text" id="fileName2" class="file_input_textbox" readonly style="width:86%;"> -->
<!-- 		                                                <div class="button"> -->
<!-- 		                                                    <input type="button" value="찾아보기" class="file_input_button" /> -->
<!-- 		                                                    <input type="file" name="mb_icon" class="file_input_hidden" onChange="javascript: document.getElementById('fileName2').value = this.value" /> -->
<!-- 		                                                </div> -->
<!-- 		                                            </div> -->
<!-- 		                                            file_input -->
<!-- 		                                            <div class="button_action"> -->
<!-- 		                                                <span class="pointer f_right p_r20" style="margin:0;"> -->
<!-- 		                                                <img src='../../resources/cmresource/css/iplanbiz/theme/style01/img/icon-cir-minus.png' alt="빼기"  onclick="" height=15> -->
<!-- 		                                                </span> -->
<!-- 		                                                <br> -->
<!-- 		                                            </div> -->
<!-- 		                                        </div> -->
		                                    </div>
		                                </div>
		                            </td>
								</tr>
								<tr>
									
								</tr>
							</tbody>
						</table>
						</form:form>
					</div>
					<!--table-->
				</div>
				<!--//content-->
			</div>
			<!--//container-->
		</div>
	<!--//wrap-->
	</body>
</html> 