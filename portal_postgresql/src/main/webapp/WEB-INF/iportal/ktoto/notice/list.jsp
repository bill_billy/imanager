<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head> 
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>공지사항</title>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
		<script src="${WEB.JSUTIL}/common.js"></script>
		
		<script language="javascript" type="text/javascript">
		var msg = "${param.msg}";
		var loginID = "${sessionScope.loginSessionInfo.userId}";
		$(document).ready(function(){
    		init(); 
    	}); 
		function init(){
			$("#startDat").jqxInput({placeHolder: "", height: 21, width: 120, minLength: 1, theme:'blueish' });
			$("#endDat").jqxInput({placeHolder: "", height: 21, width: 120, minLength: 1, theme:'blueish' });
			$( "#startDat" ).datepicker({
				changeYear: false,
				showOn: "button",
				buttonImage: "../../resources/img/calendar.gif",
				buttonImageOnly: true,
				dateFormat: "yy-mm-dd",
				onSelect:function(dateText){
					if($("#endDat").val() != ""){
						if($("#endDat").val() < $("#startDat").val()){
							alert("종료일자가 시작일자보다 과거입니다.");
							$("#endDat").val("");
						}else{
							search();
						}	
					}
				}
			});
    		$( "#endDat" ).datepicker({
    			changeYear: false,
				showOn: "button",
				buttonImage: "../../resources/img/calendar.gif",
				buttonImageOnly: true,
				dateFormat: "yy-mm-dd",
				onSelect:function(dateText){
					if($("#endDat").val() < $("#startDat").val() || $("#startDat").val() == ""){
						alert("종료일자가 시작일자보다 과거입니다.");
						$("#endDat").val("");
					}else{
						search();
					}	
				}
			});
    		var dat  = [
				{value:'begin',text:'시작일'},
				{value:'end',text:'종료일'}
			];
			// prepare the data
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: 'value' },
					{ name: 'text' }
				],
				id: 'id',
				localdata:dat,
			//	url: url,
				async: false
			};
			var dataAdapter = new $.jqx.dataAdapter(source);
			// Create a jqxComboBox
			$("#comboGubn").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "text", valueMember: "value", dropDownWidth: 150, dropDownHeight: 80, width: 150, height: 21, theme:'blueish'});
			
			$("#btnSearch").jqxButton({ width: '',  theme:'blueish'}); 
            $("#btnNew").jqxButton({ width: '',  theme:'blueish'}); 
			$("#btnModi").jqxButton({ width: '',  theme:'blueish'});   
			$("#searchText").jqxInput({placeHolder: "", height: 22, width: 250, minLength: 1, theme:'blueish' });
			$("#searchText").keydown(function(event){
				if(event.keyCode == 13){
					return search();
				}
        	});
			$("#endDat").datepicker({ });
			if(msg != ""){
				$i.dialog.alert('SYSTEM', decodeURIComponent(msg));
			}
			search();
		}
		function search(){
			var beginStartData = "";
			var beginEndData = "";
			var endStartData = "";
			var endEndData = "";
			if($("#comboGubn").val() == "begin"){
				beginStartData = $("#startDat").val();
				beginEndData = $("#endDat").val();
			}else{
				endStartData = $("#startDat").val();
				endEndData = $("#endDat").val();
			}
			$i.post("./getKtotoNoticeList", {name:$("#searchText").val(), beginStartDat:beginStartData, beginEndDat:beginEndData, endStartDat:endStartData, endEndDat:endEndData}).done(function(data){
				var source =
	            {
	                datatype: "json",
	                datafields: [
	                    { name: 'BOARDNO', type: 'string'},
	                    { name: 'TITLE', type: 'string'},
	                    { name: 'INFORM_YN', type: 'string'},
	                    { name: 'USERID', type:'string'},
	                    { name: 'BEGINDAY', type: 'string'},
	                    { name: 'DATE', type: 'string'},
						{ name: 'ENDDAY', type: 'string'},
	                    { name: 'NAME', type: 'string'},
	                    { name: 'HITCOUNT', type: 'string'},
	                    { name: 'FILE_CNT', type: 'string'}
	                ],
	                localdata: data,
	                updaterow: function (rowid, rowdata, commit) {
	                    commit(true);
	                }
	            };
				var alginRight = function (row, columnfield, value) {//right정렬
					return '<div id="userName-' + row + '"style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
				};
				var alginLeft = function (row, columnfield, value) {//left정렬
					return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
				}; 
				var detailRow = function (row, columnfield, value, defaultHtml, property, rowdata ) {
					var boardNO = rowdata.BOARDNO;
					var userID = rowdata.USERID;
					var link = "<a href=\"javaScript:detail('"+userID+"','"+boardNO+"')\" style='color:black;text-decoration:underline;'>" + replaceAll(replaceAll(value,"<","&lt;"),">","&gt;") + "</a>";
	            	
	            	return "<div style='text-align:left; margin:4px 0px 0px 10px; color:black;'>" + link + "</div>";
				}
	            var dataAdapter = new $.jqx.dataAdapter(source, {
	                downloadComplete: function (data, status, xhr) { },
	                loadComplete: function (data) { },
	                loadError: function (xhr, status, error) { }
	            });
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#gridNoticeList").jqxGrid(
	            {
	                 width: '100%',
	                height: '100%',
					altrows:true,
					pageable: true,
					pageSize: 100,
				    pageSizeOptions: ['100', '200', '300'],
	                source: dataAdapter,
					theme:'blueish',
					columnsheight:25 ,
					rowsheight: 25,
	                columnsresize: true,
	                sortable:true,
	                columns: [
	                  { text: 'No', datafield: 'BOARDNO', width: '5%', align:'center', cellsalign: 'center'},
	                  { text: '제목', datafield: 'TITLE', width: '55%', align:'center', cellsalign: 'left' , cellsrenderer: detailRow},
					  { text: '로그인표시', datafield: 'INFORM_YN', width: '7%', align:'center', cellsalign: 'center' },
					  { text: '공고기간', datafield: 'DATE', width: '12%', align:'center', cellsalign: 'center'},
					  { text: '작성자', datafield: 'NAME', width: '7%', align:'center', cellsalign: 'center'},
					  { text: '첨부', datafield: 'FILE_CNT', width: '7%', align:'center', cellsalign: 'center'},
					  { text: '조회수', datafield: 'HITCOUNT', width: '7%', align:'center', cellsalign: 'center'}
	                ]
	            });
			});
		}
		function detail(userID, boardNo){
			$("#hiddenBoardNO").val(boardNo);
			$i.post("./getKtotoNoticeDetail",{boardNO:boardNo}).done(function(data){
				if(data.returnCode == "SUCCESS"){
					if(userID == loginID){
						$("#btnModi").show();
					}else{
						$("#btnModi").hide();
					}
					$("#boardName").html(replaceAll(replaceAll(data.returnObject.TITLE,"<","&lt;"),">","&gt;"));
					$("#txtAreaContent").html(data.returnObject.CONTENT);
					$("#createDate").html(data.returnObject.CREATEDATE);
					$i.post("./setKtotoNoticeHitCount",{boardNo:boardNo}).done(function(res){
					});
				}
			});
			$i.post("./getNoticeDtailFileInfo",{boardNO:boardNo}).done(function(data){
				if(data.returnCode == "SUCCESS"){
					$("#fileList > div").remove();
					var html = "";
					for(var i=0; i<data.returnArray.length; i++){
						html += "<div class='cell'><a href=\"javaScript:filedownLoad('"+data.returnArray[i].FILE_ID+"')\"><span style='font-weight: bold; padding-right: 10px;'>"+data.returnArray[i].FILE_ORG_NAME+"</SPAN><img src='../../resources/cmresource/css/iplanbiz/theme/ktoto/img/btn_save.png' border='0' alt='' /></a></div>";
					}
					$("#fileList").append(html);
				}
			});
		}
		function replaceAll(str,orgStr,repStr){
		    return str.split(orgStr).join(repStr);
		}
		function filedownLoad(fileID){
			location.replace("../../cms/board/download?fileId="+fileID);
		}
		function  goForm(){
			location.replace("../../ktoto/notice/crud?type=N");
		}
		function goDetail(){
			location.replace("../../ktoto/notice/crud?type=U&boardNO="+$("#hiddenBoardNO").val());
		}
		function formSearch(){
			$("#search").submit();		
		}
		function dayCheck(){
			if($("#endDat").val() != "" && $("#startDat").val() != ""){
				if($("#endDat").val() < $("#startDat").val()){
					alert("종료일자가 시작일자보다 과거입니다.");
					$("#endDat").val("");
				}else{
					search();
				}	
			}
		}
		function checkInput(id, size, gubn){
    		if(byteCheck($("#"+id).val()) > size){
    			var length = "";
    			if(gubn == "en"){
    				length = parseInt((size / 1));
    				alert( length + "자(영문 기준)의 영문/_ 혼용만 가능합니다.");
    			}else if(gubn == "all"){
    				length = parseInt((size / 3));
    				alert( length + "자(한글 기준)의 한글/영문/특수문자 혼용만 가능합니다.");
    			}else if(gubn =="num"){
    				length = parseInt((size / 1));
    				alert( length + "자의 숫자만 가능합니다.");
    			}else if(gubn =="kor"){
    				length = parseInt((size / 3));
    				alert( length + "자(한글 기준)의 한글만 가능합니다.");
    			}
    			$("#"+id).focus();
    			return false;
    		}
    	}
	 	function byteCheck(code) {
			var size = 0;
			for (i = 0; i < code.length; i++) {
				var temp = code.charAt(i);
				if (escape(temp) == '%0D')
			   		continue;
			  	if (escape(temp).indexOf("%u") != -1) {
			   		size += 3;
			  	} else {
			   		size++;
			 	}
		 	}
		 	return size;
		}
		function setPagerLayout(selector) {
			
			var pagesize = $('#'+selector).jqxGrid('pagesize');
			
			var w = 49; 
				
			if(pagesize<100) {
				w = 44;
			} else if(pagesize>99&&pagesize<1000) {
				w = 49;
			} else if(pagesize>999&&pagesize<10000) {
				w = 54;
			}
			
			//디폴트 셋팅
			$('#gridpagerlist'+selector).jqxDropDownList({ width: w+'px' });
			
			//체인지 이벤트 처리
			$('#'+selector).on("pagesizechanged", function (event) {
				var args = event.args;
				
				if(args.pagesize<100) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '44px' });
				} else if(args.pagesize>99 && args.pagesize<1000) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '49px' });
				} else if(args.pagesize>999 && args.pagesize<10000) {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: '54px' });
				} else {
					$('#gridpagerlist'+selector).jqxDropDownList({ width: 'auto' });
				}
				
			});
		}
		</script>
		<style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
			.ui-datepicker-trigger{
				float:left;
				margin-top:5px;
				margin-left:5px;
			}
			
		</style>
		<style>
			.border_top{border-top:1px solid #dedede}
		</style>
	</head>
	<body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 1%;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<div class="label type1 f_left">일자:</div>
				<div class="combobox f_left" id='comboGubn' style="margin:2px 5px;"></div>
				<div class="cell">
					<input id="startDat" type="text" class="input type3 t_center f_left date" style="width:100px; margin:3px 0;" />
					<div class="f_left m_l5 m_r5 m_t2">&sim;</div>
					<input id="endDat" type="text" class="input type3 t_center f_left date" style="width:100px; margin:3px 0;" />
				</div>    
				<div class="label type1 f_left">제목:</div>
				<div class="input f_left">
					<input type="text" id="searchText" style="margin:2px 5px;"/>
				</div>
				<div class="group_button f_right">
					<div class="button type1 f_left">
						<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();"/>
					</div>
					<div class="button type1 f_left">
						<input type="button" value="신규" id='btnNew' width="100%" height="100%" onclick="goForm();"/>
					</div>
				</div>
			</div>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:100%; margin:0;">
					<div class="grid f_left" style="width:100%; height:287px;">
						<div id="gridNoticeList"></div>
					</div>
				</div>
				<div class="content f_left" style=" width:100%; margin:10px 0;">
					<div class="group f_left  w100p m_b5">
						<div class="label type2 f_left">공지사항 상세</div>
						<div class="group_button f_right">
							<input type="hidden" id="hiddenBoardNO" />
							<div class="button type2 f_left" style="margin-bottom:0;">
								<input type="button" value="수정" id='btnModi' width="100%" height="100%" onclick="goDetail();" />
							</div>
						</div>
					</div>
					<div class="table  f_left" style="width:100%; margin:0; border-top:2px solid #30638d; border-left:0; border-right:0;">
						<table width="100%" cellspacing="0" cellpadding="0" border="0">
							<tbody>
								<tr>
									<td style="width:5%;height:20px; background:#5a8cb6; color:#ddeeff; font-size:1.0em; font-weight:bold; text-indent:10px; padding:3px 0 6px 0; border-right:0; text-align:center;">제목</td>
									<td style="width:85%;height:20px; background:#5a8cb6; color:#ffffff; font-size:1.1em; font-weight:bold; text-indent:10px; padding:3px 0 6px 0; text-align:left;" id="boardName"></td>
									<td style="width:10%; height:20px; background:#e7f1f9; color:#30638d; font-size:1.0em; text-indent:10px; padding:3px 0 6px 0; text-align:center;" id="createDate"></td>
								</tr>
								<tr>
									<td colspan="3">
										<div class="dhtml_area" style=" padding:5px !important; height:220px;overflow-y:scroll;" id="txtAreaContent"></div>
									</td>
								</tr>
								<tr>
									<td colspan="2" style=" padding:10px !important;" id="fileList"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html> 