<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ETL</title>
       <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_secure.js"></script>
        <script>
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init(); 
        	}); 
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		$("#btnSearch").jqxButton({width:'', theme:'blueish'});
        		$("#txtSearch").jqxInput({placeHolder: "", height: 23, width: 250, minLength: 1, theme:'blueish' });
        		search();
        		makeGridError();
        	}
        	//조회 
        	function search(){
        		var etlResultData = $i.post("./getEtlResultList",{sourceTable:$("#txtSearch").val(),targetTable:$("#txtSearch").val()});
        		etlResultData.done(function(etlData){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'CREATE_TABLE', type: 'string' },
		                    { name: 'SOURCE_USER', type: 'string' },
		                    { name: 'SOURCE_TABLE', type: 'string'},
		                    { name: 'TARGET_USER', type: 'string'},
		                    { name: 'TARGET_TABLE', type: 'string'},
		                    { name: 'ETL_CNT', type: 'string'},
		                    { name: 'RESULT_TIME', type: 'string'}
		                ],
		                localdata: etlData.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
        			 var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var noScript = function(row, columnfield, value){
	                	var newValue = $i.secure.scriptToText(value);
	                	return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
	                };
		            $("#gridEtlResultList").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                pageable: true,
						pageSize: 100,
					    pageSizeOptions: ['100', '200', '300'],
					    columnsheight:25 ,
						rowsheight: 25,
		                columns: [
							{ text: '원본유저', datafield:'SOURCE_USER', width:'15%', align:'center', cellsalign:'center',cellsrenderer:noScript},
							{ text: '원본 테이블', datafield: 'SOURCE_TABLE', width: '15%', align:'center', cellsalign: 'center',cellsrenderer:noScript },
							{ text: '대상 유저', datafield: 'TARGET_USER', width: '10%', align:'center', cellsalign: 'center',cellsrenderer:noScript },
							{ text: '대상 테이블', datafield: 'TARGET_TABLE', width: '15%', align:'center', cellsalign: 'center' ,cellsrenderer:noScript},
							{ text: '테이터 수', datafield: 'ETL_CNT', width: '15%', align:'center', cellsalign: 'center' },
							{ text: '소요 시간', datafield: 'RESULT_TIME',  width: '10%', align:'center', cellsalign: 'center' },
							{ text: 'ETL 시작시간', datafield: 'CREATE_TABLE', width: '20%',align:'center', cellsalign: 'center' }
						]
		            });
        		});
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function makeGridError(){
        		var etlErrorData = $i.post("./getEtlResultErrorList",{});
        		etlErrorData.done(function(etlData){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'CREATE_TABLE', type: 'string' },
		                    { name: 'SOURCE_USER', type: 'string' },
		                    { name: 'SOURCE_TABLE', type: 'string'},
		                    { name: 'TARGET_USER', type: 'string'},
		                    { name: 'TARGET_TABLE', type: 'string'},
		                    { name: 'DSCMESSAGE', type: 'string'}
		                ],
		                localdata: etlData.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
        			 var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
		           
		            $("#gridEtlResultErrorList").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                pageable: true,
						pageSize: 100,
					    pageSizeOptions: ['100', '200', '300'],
					    columnsheight:25 ,
						rowsheight: 25,
		                columns: [
							{ text: '원본유저', datafield:'SOURCE_USER', width:'10%', align:'center', cellsalign:'center'},
							{ text: '원본 테이블', datafield: 'SOURCE_TABLE', width: '10%', align:'center', cellsalign: 'center' },
							{ text: '대상 유저', datafield: 'TARGET_USER', width: '10%', align:'center', cellsalign: 'center' },
							{ text: '대상 테이블', datafield: 'TARGET_TABLE', width: '15%', align:'center', cellsalign: 'center' },
							{ text: '오류메세지', datafield: 'DSCMESSAGE',width: '40%',align:'center', cellsalign: 'center', cellsrenderer:alginLeft },
							{ text: '오류시간', datafield: 'CREATE_TABLE', width:'15%',align:'center', cellsalign: 'center' }
							
						],
		            });
        		});
        	}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
		</style>
		   
    </head>
    <body class='blueish'>
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%; min-width:1160px; margin:0 10px;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<form id="searchForm" name="searchForm" action="./list">
					<div class="label type1 f_left" style="margin-right:5px;">테이블명 :</div>
					<input type="text" id="txtSearch" style="float:left;"/>
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
					</div>
				</form>
			</div>
			<div class="container  content f_left" style="width:100%;">
				<div class="group f_left  w100p m_t5 m_l10">
					<div class="label type2 f_left">ETL 실행 결과</div>
				</div>
				<div class="content f_left" style="width:100%; margin:0 0%;">
					<div class="grid f_left" style="width:100%; height:300px;">
						<div id="gridEtlResultList"></div>
					</div>
				</div>
				<div class="group f_left  w100p m_t5 m_l10">
					<div class="label type2 f_left">ETL 에러 결과</div>
				</div>
				<div class="content f_left" style="width:100%; margin:0 0%;">
					<div class="grid f_left" style="width:100%; height:300px;">
						<div id="gridEtlResultErrorList"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>