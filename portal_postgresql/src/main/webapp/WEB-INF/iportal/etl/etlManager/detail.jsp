<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<!-- 		<meta http-equiv="X-UA-Compatible" content="IE=8"/> -->
		<title>${title}</title>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
		<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
		<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
		<script src="../../resources/js/jquery/jquery.validate.js"></script>
		<script type="text/javascript">
		var errMessage = "${msg}";
		$(document).ready(function(){
			init();
			$('#saveForm').jqxValidator({
				rtl:true,  
				rules:[
				    { input: "#txtGbn1", message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtGbn2', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtSourceUser', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtSourceTable', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtSourceTableName', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtTargetUser', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtTargetTable', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtTargetTableName', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' },
                    { input: '#txtEtlTitle', message: "<img src='../../resources/img/icon/icon_munst_red.png'>", action: 'change, blur', rule: 'required' }
		        ]
			});
		});
		function init(){
			//ETL 방법Combo
			etlGubn('${detailList.ETL_MTHD}');
			//ETL 대상Combo
			etlYnCombo('${detailList.ETL_YN}');
			//Source DBInfo Combo
			var dbData = $i.post("./getEtlManagerDBInfoList", {});
			dbData.done(function(data){
				var dbkeysource =
				{
					localdata: data.returnArray,
					dataType: 'json',
					dataFields: [
						{ name: 'DBKEY'},
						{ name: 'DBNAME'}				
					]
				};
				var adapter = new $.jqx.dataAdapter(dbkeysource);
				$('#cboSourceDbInfo').jqxComboBox({ selectedIndex: 0, source: adapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "DBNAME", valueMember: "DBKEY", dropDownWidth: 150, dropDownHeight: 150, width: 150, height: 19,  theme:'blueish' });
				if("${detailList.SOURCE_DBINFO}" != ""){
					$("#cboSourceDbInfo").val("${detailList.SOURCE_DBINFO}");
				}
				//TargetDbInfo Combo
				var dbkeysource =
				{
					localdata: data.returnArray,
					dataType: 'json',
					dataFields: [
						{ name: 'DBKEY'},
						{ name: 'DBNAME'}				
					]
				};
				var adapter = new $.jqx.dataAdapter(dbkeysource);
				
				$('#cboTargetDbInfo').jqxComboBox({ selectedIndex: 0, source: adapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "DBNAME", valueMember: "DBKEY", dropDownWidth: 150, dropDownHeight: 150, width: 150, height: 19,  theme:'blueish' });
				if("${detailList.TARGET_DBINFO}" != ""){
					$("#cboTargetDbInfo").val("${detailList.TARGET_DBINFO}");
				}
			});
			//목록버튼
			$("#btnList").jqxButton({ width : '' , theme : 'blueish' });
			//저장버튼
			$("#btnInsert").jqxButton({ width : '' , theme : 'blueish' });
			//삭제버튼
			$("#btnDelete").jqxButton({ width : '' , theme : 'blueish' });
			if("${param.etlID}" == ""){
				$("#btnDelete").hide();
			}
		}
		function insert(){
			if($("#saveForm").jqxValidator("validate") == true){
				$i.post("./insertEtlManager", "#saveForm").done(function(data){
					if(data.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", data.returnMessage);
					}else{
						$i.dialog.alert("SYSTEM", data.returnMessage,function(){
							goList();
						});
					}
				});
			}
		}
		function remove(){
			$i.dialog.confirm('SYSTEM','삭제하시겠습니까?',function(){
				$i.post("./removeEtlManager","#saveForm").done(function(data){
					if(data.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", data.returnMessage);
					}else{
						$i.dialog.alert("SYSTEM", data.returnMessage, function(){
							goList();
						});
					}
				});
			});
		}
		function etlGubn(defaultValue) {
			var res = [
				{value:'1', name:'초기적재'},
				{value:'2', name:'변경적재'}
			];
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: 'value'},
					{ name: 'name'}
				],
				id : 'value',
				localdata: res
			};
			var dataAdapter = new $.jqx.dataAdapter(source);
			$("#cboEtlMthd").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "name", valueMember: "value", dropDownWidth: 100, dropDownHeight: 52, width: 100, height: 19,  theme:'blueish' });
			if(defaultValue != '') {
				$("#cboEtlMthd").val(defaultValue);
			}
		}
		function etlYnCombo(defaultValue){
			var res = [
				{value:'Y', name:'Y'},
				{value:'N', name:'N'}
			];
			var source =
			{
				datatype: "json",
				datafields: [
					{ name: 'value'},
					{ name: 'name'}
				],
				id : 'value',
				localdata: res
			};
			var dataAdapter = new $.jqx.dataAdapter(source);
			$("#cboEtlYn").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "name", valueMember: "value", dropDownWidth: 100, dropDownHeight: 52, width: 100, height: 19,  theme:'blueish' });
			if(defaultValue != '') {
				$("#cboEtlYn").val(defaultValue);
			}
		}
		function checkValidator(){
			$("#saveForm").jqxValidator('validate');
		}
		function goList() {
			var param = '';
			param += '?tableGbn=${param.tableGbn}';
			param += '&txtSearch=${param.txtSearch}';
			param += '&etlYn=${param.etlYn}';
			param += '&etlGbn=${param.etlGbn}';
			location.replace("./crud"+param);
		}
// 			$(document).ready(function() {
// 				etlGubn('${mainList.ETL_MTHD}');
// 				etlYnCombo('${mainList.ETL_YN}');
				
				
// 				$('#saveForm').jqxValidator({
// 					onSuccess:function(){
// 						$("#saveForm").jqxValidator("hide");
// 						saveStart();
// 					},
// 					rtl:true,
// 					rules:[
// 	                    { input: '#gbn1', message: "<img src='${WWW.IMG}/icon/icon_munst_red.png'>", action: 'keyup, blur', rule: 'required' },
// 	                    { input: '#gbn2', message: "<img src='${WWW.IMG}/icon/icon_munst_red.png'>", action: 'keyup, blur', rule: 'required' },
// 	                    { input: '#tUser', message: "<img src='${WWW.IMG}/icon/icon_munst_red.png'>", action: 'keyup, blur', rule: 'required' },
// 	                    { input: '#tTable', message: "<img src='${WWW.IMG}/icon/icon_munst_red.png'>", action: 'keyup, blur', rule: 'required' },
// 	                    { input: '#tTableNm', message: "<img src='${WWW.IMG}/icon/icon_munst_red.png'>", action: 'keyup', rule: 'required' },
// 	                    { input: '#sUser', message: "<img src='${WWW.IMG}/icon/icon_munst_red.png'>", action: 'keyup, blur', rule: 'required' },
// 	                    { input: '#sTable', message: "<img src='${WWW.IMG}/icon/icon_munst_red.png'>", action: 'keyup, blur', rule: 'required' },
// 	                    { input: '#sTableNm', message: "<img src='${WWW.IMG}/icon/icon_munst_red.png'>", action: 'keyup', rule: 'required' },
// 			        ]
// 				});
// 			});
		</script>
		<style  type="text/css">
        	.jqx-validator-hint{
        		background-color : transparent !important;
        		border:0px !important;
        	}
        	.jqx-validator-hint-arrow{
        		display:none !important;
        	}
        </style>
	</head>
	<body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 10px;">
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:100%;">
					<div class="group f_left  w100p m_b5">
						<div class="group_button f_right">	
							<div class="button type2 f_left" style="margin-bottom:0;">
								<input type="button" value="목록" id='btnList' width="100%" height="100%" onclick="goList();" />
							</div>
							<div class="button type2 f_left" style="margin-bottom:0;">
								<input type="button" value="저장" id='btnInsert' width="100%" height="100%" onclick="insert();" />
							</div>
							<div class="button type3 f_left" style="margin-bottom:0;">
								<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();;" />
							</div>
						</div><!--group_button-->
					</div>
					<form id="saveForm" name="saveForm" method="post">
						<input type="hidden" id="txtAc" name="ac" />
						<div class="table  f_left" style="width:100%; margin:0; ">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<tbody>
									<tr>
										<th class="w15p">
											<div>업무대분류명<span class="th_must"></span></div>
										</th>
										<td class="w15p">
											<div class="cell">
												<input type="text" id="txtGbn1" name="gbn1" class="input type2" style="width:98.5%; margin:3px 0;" value="${detailList.GBN1}" />
											</div>
										</td>
										<th class="w15p">
											<div>ETL방법<span class="th_must"></span></div>
										</th>
										<td class="w15p">
											<input type="hidden" id="hiddenEtlID" name="etlID" value="${detailList.ETL_ID}"/>
											<div class="cell">
												<div class="combobox" id="cboEtlMthd" name="etlMthd"></div>
											</div>
										</td>
									</tr>
									<tr>
										<th class="w15p">
											<div>업무명<span class="th_must"></span></div>
										</th>
										<td class="w15p">
											<div class="cell">
												<input type="text" id="txtGbn2" name="gbn2" class="input type2"  style="width:98.5%; margin:3px 0;" value="${detailList.GBN2}"/>
											</div>
										</td>
										<th class="w15p">
											<div>ETL대상<span class="th_must"></span></div>
										</th>
										<td class="w15p">
											<div class="cell">
												<div class="combobox" id='cboEtlYn' name="etlYn"></div>
											</div>
										</td>
									</tr>
									<tr>
										<th class="w15p">
										<!-- 
											<div>ETL주기</div>
											-->
											<div>ETL제목<span class="th_must"></span></div>
										</th>
										<td colspan="3">
											<div class="cell">
												<input type="hidden" id="txtEtlCycle" name="etlCycle" class="input type2" style="width:99%; margin:3px 0;" value="${detailList.ETL_CYCLE}"/>
												<input type="text" id="txtEtlTitle" name="etlTitle" class="input type2" style="width:99%; margin:3px 0;" value="${detailList.ETL_TITLE}"/>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="content f_left" style=" width:49%; margin-right:1%;">
							<div class="group f_left  w100p m_t10">
								<div class="label type2 f_left">Source</div>
							</div>
							<div class="table  f_left" style="width:100%; margin:0; ">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<th class="w20p">
												<div>DB 정보&frasl;User<span class="th_must"></span></div>
											</th>
											<td colspan="3" class="w80p">
												<div class="cell">
													<div class="combobox f_left m_r10 m_t3" id='cboSourceDbInfo' name="sourceDbInfo"></div>
													<input type="text" id="txtSourceUser" name="sourceUser" class="input type2 f_left" style="width:225px; margin:3px 0;" value="${detailList.SOURCE_USER}" />
												</div>
											</td>
										</tr>
										<tr>
											<th class="w20p">
												<div>Table<span class="th_must"></span></div>
											</th>
											<td colspan="3">
												<div class="cell">
													<input type="text" id="txtSourceTable" name="sourceTable" class="input type2 f_left m_r10"  style="width:150px; margin:3px 0;" value="${detailList.SOURCE_TABLE}" />
													<input type="text" id="txtSourceTableName" name="sourceTableName" class="input type2 f_left"  style="width:220px; margin:3px 0;" value="${detailList.SOURCE_TABLE_NAME}" />
												</div>
											</td>
										</tr>	
										<tr>
											<th>
												<div>Source Select  Query<br><span class="th_view normal">(생략 가능 : 기본값 SELECT * FROM 유저명.테이블명)</span></div>
											</th>
											<td colspan="3">
												<div class="cell">
												<textarea id="txtAreaSourceSelectQuery" name="sourceSelectQuery" class="textarea" style=" width:100%; height:308px; margin:3px 0;">${detailList.SOURCE_SELECT_QUERY}</textarea>
												</div>
											</td>
										</tr>
										<tr>
											<th><div>비고</div></th>
											<td colspan="3">
												<div class="cell">
													<textarea id="txtAreaSourceRemk" name="sourceRemk" class="textarea" style="width:100%; height:60px; margin:3px 0;">${detailList.SOURCE_REMK}</textarea>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="content f_left" style=" width:49%; margin-left:1%;">
							<div class="group f_left m_t10">
								<div class="label type2">Target</div>
							</div>
							<div class="table  f_left" style="width:100%; margin:0; ">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<th class="w20p">
												<div>DB 정보&frasl;User<span class="th_must"></span></div>
											</th>
											<td colspan="3" class="w80p">
												<div class="cell">
													<div class="combobox f_left m_r10 m_t3" id='cboTargetDbInfo' name="targetDbInfo"></div>
													<input type="text" id="txtTargetUser" name="targetUser" class="input type2 f_left" style="width:225px; margin:3px 0;" value="${detailList.TARGET_USER}" />
												</div>
											</td>
										</tr>
										<tr>
											<th class="w20p">
												<div>Table<span class="th_must"></span></div>
											</th>
											<td colspan="3">
												<div class="cell">
													<input type="text" id="txtTargetTable" name="targetTable" class="input type2 f_left m_r10" style="width:150px; margin:3px 0;" value="${detailList.TARGET_TABLE}" />
													<input type="text" id="txtTargetTableName" name="targetTableName" class="input type2 f_left" style="width:220px; margin:3px 0;" value="${detailList.TARGET_TABLE_NAME}" />
												</div>
											</td>
										</tr>	
										<tr>
											<th><div>Target Delete Query<br><span class="th_view normal">(생략 가능 : 기본값 DELETE FROM 유저명.테이블명)</span></div></th>
											<td colspan="3">
												<div class="cell">
													<textarea id="txtAreaTargetDeleteQuery" name="targetDeleteQuery" class="textarea" style="width:100%; height:150px; margin:3px 0;">${detailList.TARGET_DELETE_QUERY}</textarea>
												</div>
											</td>
										</tr>
										<tr>
											<th><div>Target Insert Query<br><span class="th_view normal">(생략 가능 : SELECT QUERY의 결과값이 그대로 대상테이블에 적재 됩니다.) </span></div></th>
											<td colspan="3">
												<div class="cell">
													<textarea id="txtAreaTargetInsertQuery" name="targetInsertQuery" class="textarea" style="width:100%; height:150px; margin:3px 0;">${detailList.TARGET_INSERT_QUERY}</textarea>
												</div>
											</td>
										</tr>
										<tr>
											<th><div>비고</div></th>
											<td colspan="3">
												<div class="cell">
													<textarea id="txtTargetRemk" name="targetRemk" class="textarea" style="width:100%; height:60px; margin:3px 0;">${detailList.TARGET_REMK}</textarea>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</form>
					</div>
					<div class="group f_left  w100p">
						<div class="label type3 f_left">where 조건 예제:<span class="sublabel">where emp_no = 'A0001'</span></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>