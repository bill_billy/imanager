 <%@ page language="java" contentType="application/vnd.xls;charset=UTF-8" pageEncoding="utf-8"%>   -
<%@page import="java.net.URLEncoder"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
	String fileName = request.getParameter("fileName");
	String header =request.getHeader("User-Agent");
	String browser="";  
	String docName = URLEncoder.encode(fileName, "utf-8");  
	 
	System.out.println("header++++++++++++++++++++++++++++++"+header+":::::"+docName);
	 
	
    response.setHeader("Content-Disposition", "attachment; filename="+docName+".xls"); 
    response.setHeader("Content-Description", "JSP Generated Data");

%>   -
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>ETL 리스트</title>
	
	</head>
 	<body>
		<table width="100%;" id="tableList" class="fix_rable" cellspacing="0" cellpadding="0" border="1">		
			<thead>			
				<tr> 			
					<th style="background-color:#eff0f0;" >ETL번호</th>
					<th style="background-color:#eff0f0;" >업무대분류</th>
					<th style="background-color:#eff0f0;" >업무명</th>								
					<th style="background-color:#eff0f0;" >ETL제목</th>
					<th style="background-color:#eff0f0;" >SOURCE DB 정보</th>
					<th style="background-color:#eff0f0;" >SOURCE USER</th>
					<th style="background-color:#eff0f0;" >SOURCE TALBE</th>
					<th style="background-color:#eff0f0;" >SOURCE TABLE 한글명</th>
					<th style="background-color:#eff0f0;" >TARGET DB 정보</th>
					<th style="background-color:#eff0f0;" >TARGET USER</th>
					<th style="background-color:#eff0f0;" >TARGET TABLE</th>
					<th style="background-color:#eff0f0;" >TARGET TABLE 한글명</th>
					<th style="background-color:#eff0f0;" >ETL 대상</th>
					<th style="background-color:#eff0f0;" >ETL 방법</th>
				</tr>
			</thead>
		    <tbody>
			<c:choose> 		
					<c:when test="${mainList != null && not empty mainList}"> 	
						<c:forEach items="${mainList}" var="rows" varStatus="loop">
							<tr>		
								<td align="center">${rows.etl_id}</td>
								<td align="center">${rows.gbn1}</td>
								<td align="center">${rows.gbn2}</td>
								<td align="left">${rows.etl_title}</td>
								<td align="center">${rows.source_dbinfo}</td>
								<td align="center">${rows.source_user}</td>
								<td align="left">${rows.source_table}</td>
								<td align="left">${rows.source_table_name}</td>
								<td align="center">${rows.target_dbinfo}</td>
								<td align="center">${rows.target_user}</td>
								<td align="left">${rows.target_table}</td>
								<td align="left">${rows.target_table_name}</td>
								<td align="center">${rows.etl_yn}</td>
								<td align="center">${rows.etl_mthd}</td>
							</tr>
						</c:forEach> 		
					</c:when> 		
					<c:otherwise> 				
						<tr> 					
							<td class="gtd"  colspan="6">조회된 데이터가 없습니다.</td> 			
						</tr> 		
					</c:otherwise> 
				</c:choose>  
			</tbody> 
		</table>
	</body>
</html>