<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ETL</title>
       <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	    <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <script>
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		$("#btnSearch").jqxButton({width:'', theme:'blueish'});
        		$("#btnNew").jqxButton({width:'', theme:'blueish'});
        		$("#btnExcel").jqxButton({width:'', theme:'blueish'});
        		$("#txtSearch").jqxInput({placeHolder: "", height: 23, width: 150, minLength: 1, theme:'blueish' });
        		$('#txtSearch').on('keydown',function() {
						if(event.keyCode==13) {
							search();
							return false;
						}
					}
				);
        		makeCombobox();
        	}
        	//조회 
        	function search(){
        		var etlManagerData = $i.post("./getEtlManagerList",{gbn:$("#cboEtlGbn").val(),searchKey:$("#cboTableGbn").val(), searchValue:$("#txtSearch").val(),etlYn:$("#cboEtlYn").val()});
        		etlManagerData.done(function(etlData){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'ETL_ID', type: 'string' },
		                    { name: 'ETL_TITLE', type: 'string' },
		                    { name: 'TARGET_DBINFO', type: 'string' },
		                    { name: 'TARGET_USER', type: 'string'},
		                    { name: 'TARGET_TABLE', type: 'string'},
		                    { name: 'TARGET_TABLE_NAME', type: 'string'},
		                    { name: 'SOURCE_DBINFO', type: 'string'},
		                    { name: 'SOURCE_USER', type: 'string'},
		                    { name: 'SOURCE_TABLE', type: 'string'},
		                    { name: 'SOURCE_TABLE_NAME', type: 'string'},
		                    { name: 'DBLINK_NAME', type: 'string'},
		                    { name: 'GBN1', type: 'string'},
		                    { name: 'GBN2', type: 'string'},
		                    { name: 'ETL_YN', type: 'string'},
		                    { name: 'ETL_MTHD', type: 'string'},
		                    { name: 'ROW_CNT', type: 'string'},
		                    { name: 'ETL_CYCLE', type: 'string'}
		                ],
		                localdata: etlData.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
        			 var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
        			var leftRenderer = function (row, columnfield, value) {//left정렬
        				var newValue = $i.secure.scriptToText(value);
	                    return '<div id="userName-' + row + '"style="text-align: left; margin:4px 0px 0px 10px;">' + newValue + '</div>';
								
	                };
        			var tableRender = function (row, columnfield, value,defaultHtml,property, rowdata) {
	                	var etlId = rowdata.ETL_ID;
	                	var tableName = rowdata.TARGET_TABLE;
	                	var tDbinfo = rowdata.TARGET_DBINFO;
	                	var sDbinfo = rowdata.SOURCE_DBINFO;
		            	var link = "<a href=\"javaScript:execStart('"+etlId+"')\" style='color:black;'>" + value + "</a>";
		            	
		            	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:9px; color:black;'>" + link + "</div>";
	                };
	                var detaildbInfo = function (row, columnfield, value,defaultHtml,property, rowdata) {
	                	var valDb = rowdata.TARGET_DBINFO;
	                	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:9px; color:black;'>" + value+ "</div>"; 
	                };
	                var detailsdbInfo = function(row,datafield,value,defautHtml, property,rowdata){
	                	var valDb = rowdata.SOURCE_DBINFO; 
	                	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:9px; color:black;'>" + value + "</div>"; 
	                };
	                var noScript = function(row, columnfield, value){
	                	var newValue = $i.secure.scriptToText(value);
	                	return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
	                };
	                var detailRender = function(row,datafield,value,defaultHtml, property, rowdata){
	                	var etlId = rowdata.ETL_ID;
	                	var tTable = rowdata.TARGET_TABLE;
	                	var sTable = rowdata.SOURCE_TABLE;
	                	var tUser = rowdata.TARGET_USER;
	                	var sUser = rowdata.SOURCE_USER;
	                	var tDbinfo = rowdata.TARGET_DBINFO;
	                	var sDbinfo = rowdata.SOURCE_DBINFO;
	                	var newValue = $i.secure.scriptToText(value);
						var link = "<a href=\"javaScript:viewDetail('"+etlId+"')\" style='color:black;'>" + newValue + "</a>";
		            	
		            	return "<div style='text-align:left;text-decoration:underline; padding-bottom:2px; margin:4px 0px 0px 10px; color:black;'>" + link + "</div>";
	                };
		           
		            $("#gridEtlManagerList").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                pageable: true,
		                pagesize: 100,
						pagesizeoptions:['100', '200', '300'],
		                columns: [
							{ text: 'ETLID', datafield:'ETL_ID', width:'6%', align:'center', cellsalign:'center'},
							{ text: '업무대분류', datafield: 'GBN1', width: '7%', align:'center', cellsalign: 'center',cellsrenderer:noScript},
							{ text: '업무명', datafield: 'GBN2', width: '8%', align:'center', cellsalign: 'center',cellsrenderer:noScript},
							{ text: 'ETL제목', datafield: 'ETL_TITLE', width: '10%', align:'center', cellsalign: 'center', cellsrenderer: detailRender},
							{ text: 'DB정보', datafield: 'SOURCE_DBINFO',  width: '7%', align:'center', cellsalign: 'center', columngroup:'B' ,cellsrenderer: detailsdbInfo},
							{ text: 'Table', datafield: 'SOURCE_TABLE', width: '9%', align:'center', cellsalign: 'center', columngroup:'B', cellsrenderer: leftRenderer  },
							{ text: 'Table Name', datafield: 'SOURCE_TABLE_NAME',  width: '12%', align:'center', cellsalign: 'center', columngroup:'B', cellsrenderer: leftRenderer  },
							{ text: '조건 여부', datafield: 'SOURCE_WHERE', width: '5%', align:'center', cellsalign: 'center', columngroup:'B'  },
							{ text: 'DB정보', datafield: 'TARGET_DBINFO', width: '7%', align:'center', cellsalign: 'center', columngroup:'A' , cellsrenderer: detaildbInfo },
							{ text: 'Table', datafield: 'TARGET_TABLE', width: '9%', align:'center', cellsalign: 'left', columngroup:'A', cellsrenderer: leftRenderer  },
							{ text: 'Table Name', datafield: 'TARGET_TABLE_NAME', width: '12%', align:'center', cellsalign: 'center', columngroup:'A', cellsrenderer: leftRenderer},
							{ text: '조건 여부', datafield: 'TARGET_WHERE', width: '5%', align:'center', cellsalign: 'center',  columngroup:'A' },
							{ text: '실행', datafield: '실행', width: '3%', align:'center', cellsalign: 'center', cellsrenderer: function (row, columnfield, value, defaultHtml, property, rowdata) {
								var etlId = rowdata.ETL_ID;
								var tableName = rowdata.TARGET_TABLE;
								var tDbinfo = rowdata.TARGET_DBINFO;
			                	var sDbinfo = rowdata.SOURCE_DBINFO;
								
				            	var link = "<img src='../../resources/img/icon/icon_cell_button_play.png' onClick=\"execStart('"+etlId+"')\" />";
				            	
				            	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px; color:black;'>" + link + "</div>";
								}, 
							}
						],
						columngroups: 
				        [
				        	{ text: 'Target DB', align: 'center', name: 'A' },
							{ text: 'Source DB', align: 'center', name: 'B' },
				        ]
		            });
        		});
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function execStart(etlID){
        		$i.post("./callEtl", {etlID : etlID}).done(function(data){
        			if(data.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM", data.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM", data.returnMessage, function(){
        					search();
        				});
        			}
        		});
        	}
        	function makeCombobox(){
        		var tableGubnData = [
					{ value : "source", name: "source"},
					{ value : "target", name: "target" }
				];
        		var source =
	            {
	                datatype: "json",
	                datafields: [
	                     { name: 'value' },
	                     { name: 'name' }
	                ],
	                id: 'id',
					localdata:tableGubnData,
	                async: false
	            };
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#cboTableGbn").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "name", valueMember: "value", dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,theme:'blueish'});
	            if("${param.tableGbn}" != ""){
	            	$("#cboTableGbn").val("${param.tableGbn}");
	            }
	            var tableGubnData = [
					{ value : "Y", name: "사용"},
					{ value : "N", name: "미사용" }
				];
        		var source =
	            {
	                datatype: "json",
	                datafields: [
	                     { name: 'value' },
	                     { name: 'name' }
	                ],
	                id: 'id',
					localdata:tableGubnData,
	                async: false
	            };
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#cboEtlYn").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "name", valueMember: "value", dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,theme:'blueish'});
	            if("${param.etlYn}" != ""){
	            	$("#cboEtlYn").val("${param.etlYn}");
	            }
	            var gbnData = $i.post("./getEtlManagerGbnList",{});
	            gbnData.done(function(data){
	            	var source =
		            {
		                datatype: "json",
		                datafields: [
		                     { name: 'COM_COD' },
		                     { name: 'COM_NM' }
		                ],
		                id: 'id',
						localdata:data.returnArray,
		                async: false
		            };
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#cboEtlGbn").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,theme:'blueish'});
		            if("${param.etlGbn}" != ""){
		            	$("#cboEtlGbn").val("${param.etlGbn}");
		            }
		            $("#txtSearch").val("${param.txtSearch}");
		            search();
	            });
        	}
        	function viewDetail(etlID){
        		location.replace("./detail?etlID="+etlID+"&tableGbn=" + $("#cboTableGbn").val()+"&txtSearch="+$("#txtSearch").val()+"&etlYn="+$("#cboEtlYn").val()+"&etlGbn="+$("#cboEtlGbn").val());
        	}
        	function goNewPage(){
        		location.replace("./detail?tableGbn=" + $("#cboTableGbn").val()+"&txtSearch="+$("#txtSearch").val()+"&etlYn="+$("#cboEtlYn").val()+"&etlGbn="+$("#cboEtlGbn").val());
        	}
        	function excelDown(){
        		var filename = 'ETL리스트';
        		var param = '?';
				param += 'fileName=' + filename;
				param += '&gbn=' + $("#cboEtlGbn").val();
				param += '&searchKey=' + $("#cboTableGbn").val();
				param += '&searchValue=' + $("#txtSearch").val();
				param += '&etlYn=' + $("#cboEtlYn").val();
				windowOpen('./excelDownByEtl'+param, "_self");
				
				return false;
        	}
        	function windowOpen(url, windowName, width, height, location, scrollbars){
				var screenWidth=screen.width;
				var screenHeight=screen.height;
			
				var x=(screenWidth/2)-(width/2);
				var y=(screenHeight/2)-(height/2);
				//2013-12-18장민수 수정
				//새창 객체를 리턴하도록.
				return window.open(url, windowName,"width=" + width + ", height=" + height + ",  location=" + location + ", toolbar=no, menubar=no, directories=no, scrollbars=" + scrollbars + ", resizable=no, left="+ x + ", top=" + y);
			}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
		</style>
		   
    </head>
    <body class='blueish'>
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%; min-width:1160px; margin:0 10px;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<form id="searchForm" name="searchForm" action="./list">
					<div class="label type1 f_left">테이블명 :</div>
					<div class="combobox f_left" id="cboTableGbn"></div>
					<input type="text" id="txtSearch" style="float:left;"/>
					<div class="label type1 f_left">ETL 대상여부 :</div> 
					<div class="combobox f_left" id="cboEtlYn"></div>
					<div class="label type1 f_left">업무분류별 :</div>
					<div class="combobox f_left" id="cboEtlGbn"></div>
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
						<div class="button type1 f_left">
							<input type="button" value="신규" id='btnNew' width="100%" height="100%" onclick="goNewPage();" />
						</div>
						<div class="button type1 f_left">
							<input type="button" value="Excel" id='btnExcel' width="100%" height="100%" onclick="excelDown();" />
						</div>
					</div>
				</form>
			</div>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style="width:100%; margin:0 0%;">
					<div class="grid f_left" style="width:100%; height:620px;">
						<div id="gridEtlManagerList"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>