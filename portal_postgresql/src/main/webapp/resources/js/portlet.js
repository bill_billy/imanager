	/*$(".button_right img.ui-icon_off").live('click', function(){
		$(this).hide();
		$(this).parent().children('.ui-icon').not('.ui-icon-gear').show();
		$(this).parent().children('.ui-icon_on').show();
	});

	$(".button_right img.ui-icon_on").live('click', function(){
		$(this).hide();
		$(this).parent().children('.ui-icon').hide();
		$(this).parent().children('.ui-icon_off').show();
	});*/
$(document).on("click",".button_right img.ui-icon_off", function() {
	$(this).hide();
	$(this).parent().children('.ui-icon').not('.ui-icon-gear').show();
	$(this).parent().children('.ui-icon_on').show();
});
$(document).on("click",".button_right img.ui-icon_on", function() {
	$(this).hide();
	$(this).parent().children('.ui-icon').hide();
	$(this).parent().children('.ui-icon_off').show();
});
	
	// 포틀릿의 목록을 id 값으로 가져온다.
	function getPortletList(){
		var obj = $("div.column");
		var tmp = "";
		for(i=0;i<obj.length;i++){
			var tmp_obj = $("div."+obj[i].id+" > div");
			for(k=0;k<tmp_obj.length;k++){
				tmp += i+":"+k+":"+tmp_obj[k].id+":::";
			}
		}
		return tmp;
	}
	
	// 레이아웃의 설정을 저장한다.
	function saveLayout(layoutNo){
		groupManagerDwr.dwrSetPortletLayout(layoutNo, function(){
			parent.location.reload();
		});
	}

	// 현재 오픈되어져 있는 포틀릿에 poid 가 있는지 확인하는 함수.
	function countPortletList(poid){
		var obj = $("div.column");
		var tmp = "";
		for(i=0;i<obj.length;i++){
			var tmp_obj = $("div."+obj[i].id+" > div");
			for(k=0;k<tmp_obj.length;k++){
				if(tmp_obj[k].id == poid){
					return true;
				}
			}
		}
		return false;
	}
	
	function changeStyle(uniqueId, setKey, setValue){
		
		// izzyColor 특성상 onchange 속성이 적용되지 않는다. 그래서 기능 보류.
		
		if(setKey == "title_background_color"){
			css_name = "background-color";
			target = "title";
			$("#"+uniqueId+" .portlet-header").css(css_name, setValue);
		}else if(setKey == "title_font_color"){
			css_name = "color";
			target = "title";
			$("#"+uniqueId+" .portlet-header").css(css_name, setValue);
		}else if(setKey == "title_font_size"){
			css_name = "font-size";
			target = "title";
			$("#"+uniqueId+" .portlet-header").css(css_name, setValue+"px");
		}else if(setKey == "content_background_color"){
			css_name = "background-color";
			target = "content";
		}else if(setKey == "content_font_color"){
			css_name = "color";
			target = "content";
		}else if(setKey == "content_font_size"){
			css_name = "font-size";
			target = "content";
		}

	}
	
	function setPageletDefault(){
		$i.post("../../portlet/loader/setPortletDefault", {}, function(res){
			
		});
//		groupManagerDwr.dwrSetPageletDefault(plid,{callback:function(){
//		
//		}, async:false});
	}
	
	function setStyle(objId){

		var p = new Object();
		p.poid = objId;
		$("#"+objId).find("input").each(function(){
			if($(this).attr("value")){
				value = $(this).attr("value");
			}else{
				value = "#FF0000";
			}
			eval_tmp = "p."+$(this).attr("name")+"='"+value+"';";
			changeStyle(objId, $(this).attr("name"), value);

			eval(eval_tmp);
		});		
		if(pageType=='admin'){
			
		}else{
			p.plid=plid;
			groupManagerDwr.dwrUpdatePortletPersonal(p, null);
		}
		$("#set_"+objId).hide();
	
	}
	
	function cancelStyle(objId){
		$("#set_"+objId).hide();
	}
	
	// Portlet 생성 스크립트
	function addPannel(layoutTarget, viewHeader, viewIcon, pannelTarget, uniqueId, pannelTitle, pannelContent, more, moreUrl){

		if(pannelTarget > $("div.column").length) pannelTarget = 1;
		
		if(countPortletList(uniqueId)){
			return;
		}
		var portletCss = new Object();
		var passPortletCss=false;
		var ajaxStatus = '';
		if('admin' == pageType){
			ajaxStatus=$i.post("../../portlet/loader/getPageletPortlet", {poID : uniqueId, plID : gPlid});
			ajaxStatus.done(function(res){
				if(res.returnArray.length>0){
					portletCss = res.returnArray[0];
					console.log(portletCss);
					passPortletCss = true;
				}
			});
		}else{
			ajaxStatus=$i.post("../../portlet/loader/getPageletPortletByUser", {poID : uniqueId, plID : gPlid});
			ajaxStatus.done(function(res){
				if(res.returnArray.length>0){
					portletCss = res.returnArray[0];
					console.log(portletCss);
					passPortletCss = true;
				}else{
					$i.post("../../portlet/loader/getPageletPortletDefault", {}).done(function(res){
						portletCss.PORTLET_TYPE = res.returnArray[0].KEY_VALUE;
					});
				};
			});
		}
		ajaxStatus.done(function(){
			if(!passPortletCss){
				portletCss.TITLE_BACKGROUND_COLOR = false;
				portletCss.TITLE_FONT_COLOR = false;
				portletCss.TITLE_FONT_SIZE = false;
				portletCss.CONTENT_BACKGROUND_COLOR = false;
				portletCss.CONTENT_FONT_COLOR = false;
				portletCss.CONTENT_FONT_SIZE = false;
			}
			console.log(portletCss);
			if(portletCss.TITLE_BACKGROUND_COLOR != 0){
				title_background_color = portletCss.TITLE_BACKGROUND_COLOR;
			}else{
				title_background_color = default_portlet_top_background_color;
			}
			
			if(portletCss.TITLE_FONT_COLOR != 0){
				title_font_color = portletCss.TITLE_FONT_COLOR;
			}else{
				title_font_color = default_portlet_top_font_color;
			}
			
			if(portletCss.TITLE_FONT_SIZE != 0){
				title_font_size = portletCss.TITLE_FONT_SIZE;
			}else{
				title_font_size = "12";
			}
	
			if(portletCss.CONTENT_BACKGROUND_COLOR != 0){
				content_background_color = portletCss.CONTENT_BACKGROUND_COLOR;
			}else{
				content_background_color = default_portlet_content_background_color;
			}
			
			if(portletCss.CONTENT_FONT_COLOR != 0){
				content_font_color = portletCss.CONTENT_FONT_COLOR;
			}else{
				content_font_color = default_portlet_content_font_color;
			}
	
			if(portletCss.CONTENT_FONT_SIZE != 0){
				content_font_size = portletCss.CONTENT_FONT_SIZE;
			}else{
				content_font_size = "12";
			}
			
			var portlet_type="";
			
			if(portletCss.PORTLET_TYPE != 0 && typeof portletCss.PORTLET_TYPE != "undefined"){
				portlet_type = portletCss.PORTLET_TYPE;
			}else{
				portlet_type = default_portlet_theme;
			}
			
	
			var pannel = "";
			var moreButton = ""; 
			
			if(more == "true"){
				//addTab(cId, tmp_title, tmp_content, width_size, height_size, type)
				moreButton = "&nbsp;&nbsp;>&nbsp;<span onclick='parent.addTab(\""+uniqueId+"\", \""+pannelTitle+"\", \""+uriContext+moreUrl+"\")' style='cursor:pointer'>more</span>";
			}else{
				moreButton = "";
			}
			
			if("admin" == pageType){
				chkboxForAdminPortlet = "<input type='checkbox' value='"+uniqueId+"' class='adminPortletCheckbox' />";
			}else{
				chkboxForAdminPortlet = "";
			}
			
			
			var portal_setting_html = "";
			portal_setting_html  = "<table>";
			portal_setting_html += "<tr>";
			portal_setting_html += "<td colspan=\"3\" style='color:#000000;'> 타이틀 </td>";
			portal_setting_html += "</tr>";
			portal_setting_html += "<tr>";
			portal_setting_html += "<td style='color:#000000;'> 배경색 변경</td>";
			portal_setting_html += "<td style='color:#000000;'> 폰트 색상 변경</td>";
			portal_setting_html += "<td style='color:#000000;'> 폰트 사이즈 변경</td>";
			portal_setting_html += "</tr>";
			portal_setting_html += "<tr>";
			portal_setting_html += "<td><input type='text' class='izzyColor' id='color_t_b_"+uniqueId+"' value='"+title_background_color+"' name='title_background_color' style='width:50px'></td>";
			portal_setting_html += "<td><input type='text' class='izzyColor' id='color_t_f_"+uniqueId+"' value='"+title_font_color+"' name='title_font_color'  style='width:50px'></td>";
			portal_setting_html += "<td><input type='text' name='title_font_size' value='"+title_font_size+"'  style='width:50px'>px</td>";
			portal_setting_html += "</tr>";
			portal_setting_html += "<tr style='display:none'>";
			portal_setting_html += "<td colspan=\"3\"> 컨텐츠</td>";
			portal_setting_html += "</tr>";
			portal_setting_html += "<tr style='display:none'>";
			portal_setting_html += "<td> 배경색 변경</td>";
			portal_setting_html += "<td> 폰트 색상 변경</td>";
			portal_setting_html += "<td> 폰트 사이즈 변경</td>";
			portal_setting_html += "</tr>";
			portal_setting_html += "<tr style='display:none'>";
			portal_setting_html += "<td><input type='text' class='izzyColor' id='color_c_b_"+uniqueId+"' value='"+content_background_color+"' name='content_background_color'></td>";
			portal_setting_html += "<td><input type='text' class='izzyColor' id='color_c_f_"+uniqueId+"' value='"+content_font_color+"' name='content_font_color'></td>";
			portal_setting_html += "<td><input type='text' name='content_font_size' value='"+content_font_size+"'>px</td>";
			portal_setting_html += "</tr>";
			portal_setting_html += "<tr>";
			portal_setting_html += "<td colspan=3>";
			portal_setting_html += "<span class='button'><button type='button' onclick='setStyle(\""+uniqueId+"\");'>설정</button></span>";
			portal_setting_html += "<span class='button margin'><button type='button' onclick='cancelStyle(\""+uniqueId+"\")'>취소</button></span>";
			portal_setting_html += "</td>";
			portal_setting_html += "</tr>";
			portal_setting_html += "</table>";
			
			pannel += "<div class='portlet' id='"+uniqueId+"' portletType='"+portlet_type+"'>";
			pannel += "<div class='portlet_"+portlet_type+"'>";
	
			if(portlet_type >= 5){
				header = "<div class='portlet-header-new portlet-header-"+portlet_type+"' style=''>";
			}else{
				header = "<div class='portlet-header-new portlet-header-"+portlet_type+"' style='padding-top:7px; background-color:"+title_background_color+";color:"+title_font_color+";font-size;"+title_font_size+"px;'>";
			}
			header += "<div class='tit_right'><div class='tit_left'>"+chkboxForAdminPortlet+pannelTitle+moreButton+"</div></div>";
			header += "</div>";
	
			
			if(viewHeader == "true" || "admin" == pageType){
				pannel += header;
			}
			
			pannel += "<div class='portlet-setting' id='set_"+uniqueId+"' style='display:none'>";
			pannel += portal_setting_html;
			pannel += "</div>";
			pannel += "<div class='portlet-content portlet-content-"+portlet_type+"' style='overflow:hidden'>"+pannelContent+"</div>";
			pannel += "</div>";
	
			$("#"+layoutTarget+"_column"+pannelTarget).append($(pannel));
			if(viewIcon == "true"  || "admin" == pageType){
				
				var portlet_button_html = "";
				portlet_button_html += "<div class='button_right'>";
				if(portlet_type >= 5){
					portlet_button_html += "<img class='ui-icon_off' src='../../resources/img/portletThemeImage/portlet_header_button_"+portlet_type+".png' >";
					portlet_button_html += "<img class='ui-icon_on' src='../../resources/img/portletThemeImage/portlet_header_button_"+portlet_type+"_1.png' >";
				}
				portlet_button_html += "<span class='ui-icon ui-icon-gear'></span>"; 
				portlet_button_html += "<span class='ui-icon ui-icon-closethick'></span>"; 
				portlet_button_html += "<span class='ui-icon ui-icon-minusthick'></span>";
				portlet_button_html += "<span class='ui-icon ui-icon-arrowrefresh-1-e'></span>";
				portlet_button_html += "</div>";
				
				$( "#"+uniqueId ).addClass( "ui-widget ui-widget-content-"+portlet_type+" ui-helper-clearfix" )
					.find( ".portlet-header-new")
						.addClass( "ui-widget-header" )
						.prepend(portlet_button_html)
						.end()
					.find( ".portlet-content" );
				
				$(".button_right img").parent().children(".ui-icon").hide();
				$(".button_right img").parent().children(".ui-icon_on").hide();
				
			}
			$( "#"+uniqueId).find(".ui-icon-minusthick").click(function() {
				$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
				$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
			});
	
			// Fade out Script : 1초동안 Fade out
			$( "#"+uniqueId).find(".ui-icon-closethick").click(function() {
				$( this ).parents( ".portlet:first" ).effect( "fade", {}, 1000, removeSortable($( this ).parents( ".portlet:first" )) );
				
			});
			
			// Page Reload
			$( "#"+uniqueId ).find(".ui-icon-arrowrefresh-1-e").click(function() {
				$( this ).parents( ".portlet" ).find("iframe").each(function(){
					$(this).attr('src', $(this).attr('src'));
				});
			});
	
			// Portlet 설정보여주기
			$( "#"+uniqueId ).find(".ui-icon-gear").click(function() {
				$( this ).parents( ".portlet" ).find(".portlet-setting").toggle();
			});
				
			
			// Portlet 설정 적용
	//		$( "#set_"+uniqueId+" .button").click(function (){
	//			alert($(this).parents(".portlet").children(".portlet-setting").attr("id"));
	//			$( this ).parents( ".portlet" ).children(".portlet-setting").hide();
	//		});
			var screenheight = document.body.scrollHeight;
			
			jQuery(function(){izzyColor();});
		});
	}
	
	// Portlet 제거
	function removeSortable(obj){

		setTimeout(function() {
			// Error 발생 : jQuery Version Up 이후
			//obj.sortable("destroy").remove();
			obj.remove();
			savePersonalPortlet();
			
		}, 1000 );
	}

	// 포틀릿 개인화 함수 : 포틀릿이 이동할때마다 DB에 모든 Row 를 저장한다. 
	function savePersonalPortlet(){
		
		groupManagerDwr.dwrDelPortletPersonalList(plid,
		{callback:
		function(){
			tmpArr = getPortletList().split(":::");
			for(var i=0;i<tmpArr.length;i++){
				tmpMap = tmpArr[i].split(":");
				var cssInfo = new Object();
				$("#set_"+tmpMap[2]).find("input").each(function(){
					
					if($(this).attr("name") == "title_background_color"){
						cssInfo.title_background_color = $(this).attr("value");
					}
					if($(this).attr("name") == "title_font_color"){
						cssInfo.title_font_color = $(this).attr("value");
					}
					if($(this).attr("name") == "title_font_size"){
						cssInfo.title_font_size = $(this).attr("value");
					}
					if($(this).attr("name") == "content_background_color"){
						cssInfo.content_background_color = $(this).attr("value");
					}
					if($(this).attr("name") == "content_font_color"){
						cssInfo.content_font_color = $(this).attr("value");
					}
					if($(this).attr("name") == "content_font_size"){
						cssInfo.content_font_size = $(this).attr("value");
					}
				});

				if(tmpMap[2] && tmpMap[0] && tmpMap[1]){
					
					groupManagerDwr.dwrGetPortletPersonalList(plid,tmpMap[2], tmpMap[0], tmpMap[1], cssInfo, $("#"+tmpMap[2]).attr("portlettype"), function(){});
				}
			}
		},async:false}
		
		);
		var screenheight = document.body.scrollHeight;
		
	}
	
	var gTmpPoid = null;
	
	/* 포틀릿을 로드 한다. Type 에 따라서 다르게 로드 한다.
	 * TYPE 1 : RSS 형식으로 RSS 로더에 포틀릿 로딩 주소를 붙인다.
	 * TYPE 2 : Iframe 형식. 
	 * TYPE 3 : Cognos 형식으로 Cognos 로더에 StoreId 를 붙여서 Report 를 로딩 한다.
	 * TYPE 4 : Image 형식.
	 */ 

	function loadPortlet(poid, excuteSave){
		
		 
		for(var i=0;i<poid.length;i++){
			$i.post("../../portlet/loader/getPortletInfoData", {poID : poid[i], gx : gX}).done(function(res){
				if(res.returnArray[0]!=null){  
					if(res.returnArray[0].XAXIS){
						gX = res.XAXIS+1; 
					}else{
						gX = 1;
					}
					if(res.returnArray[0].TYPE == 4){
						addPannel("layout_01", res.returnArray[0].VIEW_HEADER, res.returnArray[0].VIEW_ICON,  gX ,res.returnArray[0].POID ,res.returnArray[0].NAME, "<iframe src='../../portlet/contentsManager/imgLoader?poid="+poid[i]+"' width='100%' style='overflow:hidden' frameborder=0 height='"+res.returnArray[0].HEIGHT+"'></iframe>", res.returnArray[0].MORE, res.returnArray[0].MORE_URL);
					}else if(res.returnArray[0].TYPE == 3){
						addPannel("layout_01", res.returnArray[0].VIEW_HEADER, res.returnArray[0].VIEW_ICON,  gX ,res.returnArray[0].POID ,res.returnArray[0].NAME, "<iframe src='../../portlet/contentsManager/cogLoader?storeId="+res.returnArray[0].URL+"' width='100%' style='overflow:hidden' frameborder=0 height='"+res.returnArray[0].HEIGHT+"'></iframe>", res.returnArray[0].MORE, res.returnArray[0].MORE_URL);
					}else if(res.returnArray[0].TYPE == 2){
						var url = (res.returnArray[0].URL.indexOf("http")==-1)?uriContext+res.returnArray[0].URL:res.returnArray[0].URL
						addPannel("layout_01", res.returnArray[0].VIEW_HEADER, res.returnArray[0].VIEW_ICON,  gX ,res.returnArray[0].POID ,res.returnArray[0].NAME, "<iframe src='"+url+"' width='100%' frameborder=0 height='"+res.returnArray[0].HEIGHT+"'  style='overflow:hidden'></iframe>", res.returnArray[0].MORE, res.returnArray[0].MORE_URL);
					}else if(res.returnArray[0].TYPE == 1){
						addPannel("layout_01", res.returnArray[0].VIEW_HEADER, res.returnArray[0].VIEW_ICON,  gX ,res.returnArray[0].POID ,res.returnArray[0].NAME, "<iframe src='../../portlet/contentsManager/rss?url="+res.returnArray[0].URL+"' width='100%' frameborder=0  style='overflow:hidden' height='"+res.returnArray[0].HEIGHT+"'></iframe>", res.returnArray[0].MORE, res.returnArray[0].MORE_URL);
					}else{
						var url = (res.returnArray[0].URL.indexOf("http")==-1)?uriContext+res.returnArray[0].URL:res.returnArray[0].URL
						addPannel("layout_01", res.returnArray[0].VIEW_HEADER, res.returnArray[0].VIEW_ICON, gX ,res.returnArray[0].POID ,res.returnArray[0].NAME, "<iframe src='"+url+"' width='100%' frameborder=0 style='overflow:hidden'></iframe>", res.returnArray[0].MORE, res.returnArray[0].MORE_URL);
					}
				}
//					makeUserPortlet(data);
			});
//			groupManagerDwr.dwrGetPortletInfo(poid[i],'', {callback:function(res){
//				 
//				
//			}, async:true});
			
		}
		if(excuteSave == false){
			
		}else{
			savePersonalPortlet();
		}
	}
	
	var gX = null;
	var gY = null;
	
	//user의 poid 가져옴
	function preMakeUserPortlet(res){

		if(res.length == 0){
			setPageletDefault();
			
			if(pageType != "admin"){
//				location.reload();
			}
		}

		for(var i=0;i<res.length;i++){
			gX = parseInt(res[i].XAXIS)+1;
			
			// Portlet 의 정보를 가져온다.
			// Name, Url, Height, Type, More, More_url
			$i.post("../../portlet/loader/getPortletInfoData", {poID : res[i].POID, gx : gX}).done(function(data){
				makeUserPortlet(data.returnArray[0]);
			});
//			groupManagerDwr.dwrGetPortletInfo(res[i].POID, gX, {callback:makeUserPortlet, async:false});
		}
		
		jQuery(function(){izzyColor();});
	}
	
	//user의 poid 가져옴
	function preMakeUserPortletAdmin(res){
		if(res!=null){ 
			for(var i=0;i<res.length;i++){
				gX = parseInt(res[i].XAXIS)+1;
				
				// Portlet 의 정보를 가져온다.
				// Name, Url, Height, Type, More, More_url
				$i.post("../../portlet/loader/getPortletInfoData", {poID : res[i].POID, gx : gX}).done(function(data){
					makeUserPortlet(data.returnArray[0]);
				});
//				groupManagerDwr.dwrGetPortletInfo(res[i].POID, gX, {callback:makeUserPortlet, async:false});
			}
			jQuery(function(){izzyColor();});
		}
	}
	
	
	function makeUserPortlet(res){
		if(res!=null){
			if(res.gX){
				gX = res.gX;
			}else{
				gX = 1;
			}
			var iframeID = "iframe"+res.POID;
			if(res.TYPE == 4){
				addPannel("layout_01", res.VIEW_HEADER, res.VIEW_ICON,  gX ,res.POID ,res.NAME, "<iframe class='portletInnerFrame' id='"+ iframeID + "' src='../../portlet/contentsManager/imgLoader?poid="+res.POID+"' width='100%' style='overflow:hidden' frameborder=0 height='"+res.HEIGHT+"'></iframe>", res.MORE, res.MORE_URL);
			}else if(res.TYPE == 3){
				addPannel("layout_01", res.VIEW_HEADER, res.VIEW_ICON,  gX ,res.POID ,res.NAME, "<iframe class='portletInnerFrame' id='"+ iframeID + "' src='../../home/loader/cogLoader?storeId="+res.URL+"' style='overflow:hidden' name='iframe_"+res.POID+"' width='100%' frameborder=0 height='"+res.HEIGHT+"'></iframe>", res.MORE, res.MORE_URL);
			}else if(res.TYPE == 2){
				var url = (res.URL.indexOf("http")==-1)?uriContext+res.URL:res.URL
				addPannel("layout_01", res.VIEW_HEADER, res.VIEW_ICON,  gX ,res.POID ,res.NAME, "<iframe class='portletInnerFrame' id='"+ iframeID + "' src='"+url+"' name='iframe_"+res.POID+"' style='overflow:hidden' width='100%' frameborder=0 height='"+res.HEIGHT+"'></iframe>", res.MORE, res.MORE_URL);
			}else if(res.TYPE == 1){
				addPannel("layout_01", res.VIEW_HEADER, res.VIEW_ICON,  gX ,res.POID ,res.NAME, "<iframe class='portletInnerFrame' id='"+ iframeID + "' src='../../portlet/contentsManager/rss?url="+res.URL+"' name='iframe_"+res.POID+"' style='overflow:hidden' width='100%' frameborder=0 height='"+res.HEIGHT+"'></iframe>", res.MORE, res.MORE_URL);
			}
			//alert($("#"+iframeID).contents().find("html").html());
		}
	}
	 
	// 최초 페이지 로딩 : Portlet 로딩
	gPlid = null;
	noReload = false;
	function loadPannel(userType, plid){
		$( ".column" ).sortable({
			connectWith: ".column"
		});
		
		gPlid = plid;
		
		$( ".column" ).disableSelection();

		// 사용자의 Portlet 의 위치와 List 를 가져온다. 
		// POID, XAXIS, YAXIS
		notReload = true;
		if(userType == 'admin'){
			$i.post("../../portlet/loader/getPageletPortletInfo", {plID: plid}).done(function(data){
				preMakeUserPortletAdmin(data.returnArray);
			});
		}else if(userType == 'normal'){
			$i.post("../../portlet/loader/getPagetletPortletInfoByUser", {plID: plid}).done(function(data){
				preMakeUserPortlet(data.returnArray);
			});
		}else{
			$i.post("../../portlet/loader/getPagetletPortletInfoByUser", {plID: plid}).done(function(data){
				preMakeUserPortlet(data.returnArray);
			});
		}
		
		
	}
	
	function saveAdminPortlet(plid){
		$i.post("../../portlet/portletManager/deletePageletPortlet", {plID : plid}).done(function(data){
			if(data.returnCode == "EXCEPTION"){
				$i.dialog.error("SYSTEM", data.returnMessage);
			}else{
				tmpArr = getPortletList().split(":::");
				for(var i=0;i<tmpArr.length;i++){
					tmpMap = tmpArr[i].split(":");
					var cssInfo = new Object();
					cssInfo.portletType=$("#set_"+tmpMap[2]).parent().parent().attr("portletType");
					$("#set_"+tmpMap[2]).find("input").each(function(){
						if($(this).attr("name") == "title_background_color"){
							cssInfo.title_background_color = $(this).attr("value");
						}
						if($(this).attr("name") == "title_font_color"){
							cssInfo.title_font_color = $(this).attr("value");
						}
						if($(this).attr("name") == "title_font_size"){
							cssInfo.title_font_size = $(this).attr("value");
						}
						if($(this).attr("name") == "content_background_color"){
							cssInfo.content_background_color = $(this).attr("value");
						}
						if($(this).attr("name") == "content_font_color"){
							cssInfo.content_font_color = $(this).attr("value");
						}
						if($(this).attr("name") == "content_font_size"){
							cssInfo.content_font_size = $(this).attr("value");
						}
					});
					if(tmpMap[2] && tmpMap[0] && tmpMap[1]){ //delete 후 insert
						//$i.post("../../portlet/portletManager/insertPageletPortlet", {plID : plid, poID : tmpMap[2], xAxis : tmpMap[0], yAxis : tmpMap[1], cssInfo_ : JSON.stringify(cssInfo), cssInfo : cssInfo}).done(function(data){
						$i.post("../../portlet/portletManager/insertPageletPortlet", {plID : plid, poID : tmpMap[2], xAxis : tmpMap[0], yAxis : tmpMap[1], cssInfo : JSON.stringify(cssInfo)}).done(function(data){
							if(data.returnCode == "EXCEPTION"){
								$i.dialog.error("SYSTEM", data.returnMessage);
							}else{
								//$("#portletIframe_admin").contents().find(".portlet").each(function(){
									//var portletId = $(this).attr("id");
									//var portletTheme = $(this).attr("portletType");
									//console.log(portletTheme);
									//$i.post("/porlet/portletManager/updatePageletTheme", {portletType: portletTheme, plID : plid, poID:portletId}).done(function(data){});
								//});
								$i.dialog.alert("SYSTEM", data.returnMessage, function(){
									parent.location.href='/portlet/portletManager/list';
								});
							}
						});
					}
				}
			}
		});
	}
	