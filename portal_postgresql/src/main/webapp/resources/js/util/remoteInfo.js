try{
	$["browser"]= (function() {
		  var s = navigator.userAgent.toLowerCase();
		  var match = /(webkit)[ \/](\w.]+)/.exec(s) ||
		              /(opera)(?:.*version)?[ \/](\w.]+)/.exec(s) ||
		              /(msie) ([\w.]+)/.exec(s) ||               
		              /(mozilla)(?:.*? rv:([\w.]+))?/.exec(s) ||
		             [];
		  return { name: match[1] || "", version: match[2] || "0" };
		}());
	$.browser[$.browser.name] = true;
}catch(e){}
function getBrowserName(){
	var s = navigator.userAgent.toLowerCase();
	  if(s.indexOf("trident") != -1){
		  browserInfo = "IE";
	  }else if(s.indexOf("opr") != -1){
		  browserInfo = "opera";
	  }else if(s.indexOf("chrome") != -1){
		  browserInfo = "chrome";
	  }else if(s.indexOf("firefox") != -1){
		  browserInfo = "firefox";
	  }else if(s.indexOf("chrome") < 0 && s.indexOf("safari") != -1){
		  browserInfo = "safari";
	  }else{
		  browserInfo = "mozilla";
	  }
	return browserInfo;
}

function getBrowserVersion(){
	
	return getBrowserName()+" "+$.browser.version;
}

function getBrowserVer(){
	return parseInt($.browser.version);
}
 
function getOSInfoStr(){
    var ua = navigator.userAgent;
    if(ua.indexOf("NT 6.0") != -1) return "Windows Vista/Server 2008";
    else if(ua.indexOf("NT 6.2") != -1) return "Windows 8";
    else if(ua.indexOf("NT 6.1") != -1) return "Windows 7";
    else if(ua.indexOf("NT 5.2") != -1) return "Windows Server 2003";
    else if(ua.indexOf("NT 5.1") != -1) return "Windows XP";
    else if(ua.indexOf("NT 5.0") != -1) return "Windows 2000";
    else if(ua.indexOf("NT") != -1) return "Windows NT";
    else if(ua.indexOf("9x 4.90") != -1) return "Windows Me";
    else if(ua.indexOf("98") != -1) return "Windows 98";
    else if(ua.indexOf("95") != -1) return "Windows 95";
    else if(ua.indexOf("Win16") != -1) return "Windows 3.x";
    else if(ua.indexOf("Windows") != -1) return "Windows";
    else if(ua.indexOf("Linux") != -1){
    	if(ua.indexOf("Android") != -1){
    		return getAndroidDevName();
    	}else if(ua.indexOf("BlackBerry9000") != -1){
    		return "BlackBerry9000"; 
    	}else if(ua.indexOf("BlackBerry9300") != -1){
    		return "BlackBerry9300";
    	}else if(ua.indexOf("PDA") != -1){
    		return "PDA";
    	}else if(ua.indexOf("PLAYSTATION3") != -1){
    		return "PLAYSTATION3";
    	}else if(ua.indexOf("BlackBerry9700") != -1){
    		return "BlackBerry9700";
    	}else if(ua.indexOf("BlackBerry9780") != -1){
    		return "BlackBerry9780";
    	}else if(ua.indexOf("BlackBerry9900") != -1){
    		return "BlackBerry9900";
    	}else if(ua.indexOf("BlackBerry;Opera Mini") != -1){
    		return "BlackBerry;Opera Mini";
    	}else{
    		return "Linux";
    	}
    }
    else if(ua.indexOf("Mac") != -1){
    	if(ua.indexOf("iPhone") != -1){
    		return "iPhone OS";
    	}else if(ua.indexOf("iPad") != -1){
    		return "BlackBerry9700";
    	}else if(ua.indexOf("MacOSX10_1") != -1 || ua.indexOf("MacOsX10.1") != -1){
    		return "Mac OS X Puma";
    	}else if(ua.indexOf("MacOSX10_2") != -1 || ua.indexOf("MacOsX10.2") != -1){
    		return "Mac OS X Jaguar";
    	}else if(ua.indexOf("MacOSX10_3") != -1 || ua.indexOf("MacOsX10.3") != -1){
    		return "Mac OS X Panther";
    	}else if(ua.indexOf("MacOSX10_4") != -1 || ua.indexOf("MacOsX10.4") != -1){
    		return "Mac OS X Tiger";
    	}else if(ua.indexOf("MacOSX10_5") != -1 || ua.indexOf("MacOsX10.5") != -1){
    		return "Mac OS X Leopard";
    	}else if(ua.indexOf("MacOSX10_6") != -1 || ua.indexOf("MacOsX10.6") != -1){
    		return "Mac OS X Snow Leopard";
    	}else if(ua.indexOf("MacOSX10_7") != -1 || ua.indexOf("MacOsX10.7") != -1){
    		return "Mac OS X Lion";
    	}else if(ua.indexOf("MacOSX10_8") != -1 || ua.indexOf("MacOsX10.8") != -1){
    		return "Mac OS X Mountain Lion";
    	}else if(ua.indexOf("MacOSX10_9") != -1 || ua.indexOf("MacOsX10.9") != -1){
    		return "Mac OS X Mavericks";
    	}else if(ua.indexOf("MacOSX10_10") != -1 || ua.indexOf("MacOsX10.10") != -1){
    		return "Mac OS X Yosemite";
    	}else{
    		return "MacOS";
    	}
    }
    else if(ua.indexOf("Macintosh") != -1) return "Macintosh";
    else return "";
}
function getAndroidDevName() {
	var uaAdata = navigator.userAgent;
	var regex = /Android (.*);.*;\s*(.*)\sBuild/;
	var match = regex.exec(uaAdata);
		if(match) {
			var ver = match[1];
			var dev_name = match[2];
			return "Android " + ver + " " + dev_name;
		}
	return "Android OS";
	}