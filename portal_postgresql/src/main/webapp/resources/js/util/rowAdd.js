var delRowCnt = 0;
function fnAddRow(tbName) {
    try{
        // 테이블을 찾아서 로우 추가
        var tbody = $('#' + tbName + ' tbody');
        var rows = tbody.find('tr').length;
        var newRow = tbody.find('tr:last').clone(true).appendTo(tbody);
        
        fnControlInit(newRow, rows);

    }catch(e) {
        alert(e.Message);
    }
}

function fnRemove(obj, min) {
    try{
        var rows = $(obj).parent().parent().parent().parent("tbody").find('> tr');
        var table = $(obj).parent().parent().parent().parent().parent();
        
        if(rows.length > min) $(obj).parent().parent().parent().remove();
        delRowCnt++;
	}catch(e){
		alert(e.Message);
	}
}
/**
 * IVPG3001에서 사용
 * @param obj
 * @param min
 */
function fnRemove2(obj) {
	var rowIdx = $(obj).closest("tr").prevAll().length;//몇번째 tr인지...
    
	if(rowIdx == 0) return;
    try{
        var rows = $(obj).parent().parent().parent().parent().parent("tbody").find('> tr');
        var table = $(obj).parent().parent().parent().parent().parent().parent();
        $(obj).parent().parent().parent().parent().remove();
	}catch(e){
		alert(e.Message);
	}
}

function fnControlInit(jRowobj, rowCnt){
    // input tag를 찾아서 value 지움
    jRowobj.find(':input').val('').each(function () {
        var id = this.id;

        if(id){
            this.id = this.id.split('_')[0] + '_' + Number(rowCnt+delRowCnt+1);
        }
    });
}	

function fnResetRow(obj){
	 $(obj).parent().parent().parent().find(':input').val('');
}