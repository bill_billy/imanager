$["browser"]= (function() {
	  var s = navigator.userAgent.toLowerCase();
	  var match = /(webkit)[ \/](\w.]+)/.exec(s) ||
	              /(opera)(?:.*version)?[ \/](\w.]+)/.exec(s) ||
	              /(msie) ([\w.]+)/.exec(s) ||               
	              /(mozilla)(?:.*? rv:([\w.]+))?/.exec(s) ||
	             [];
	  return { name: match[1] || "", version: match[2] || "0" };
	}());
$.browser[$.browser.name] = true;

function isEmpty(val){
	if("" == val || null == val ){ // || typeof val != "undefined"
		return true;
	}
	return false;
}

function pageMove(url){
	document.location.href=url;
}

$(document).ready(function(){       
	
	
	$("input:checkbox").click(function(){
		if($(this).attr("name") == undefined){
			var target = $(this).parent().parent().parent().parent().parent().parent().parent().find("table:eq(1)");
			
			//클릭한 체크박스가 체크상태인지 체크해제상태인지 판단   
			if($(this).attr("checked")=="checked"){
				$(target).find("input:[type=checkbox]").attr("checked", true);
			}else{
				$(target).find("input:[type=checkbox]").attr("checked", false);
			}
		}
	 });
	

	//숫자만 받기
	$('.num_only').css('imeMode','disabled').keypress(function(event) {
       if(event.which && (event.which < 48 || event.which > 57) ) {
           event.preventDefault();
       }
    }).keyup(function(){
        if( $(this).val() != null && $(this).val() != '' ) {
            $(this).val( $(this).val().replace(/[^0-9]/g, '') );
        }
    });
	
	//메세지 출력
	if(typeof errMessage != "undefined" && !isEmpty(errMessage)) alert(errMessage);
	if(typeof errCode != "undefined" && !isEmpty(errCode) && errCode > 0){callBackMsg(errCode);}
	
	if(typeof dwr != "undefined") dwr.engine.setErrorHandler(dwrErrorHandler);
	
});

function dwrErrorHandler(){
	//TODO : errorHandlering
}

function callBackMsg(res){
	commonCodeManagerService.getErrorMessage(res, alertErrMsg);
}

function alertErrMsg(res){
	alert(res);
}
/**
 * 평가마감 버튼 활성, 비활성
 * button id=closeBtn
 * 비교: evalStep_년도 해당 값....
 * displayYn : Y(마감버튼 활성화 일경우...)
 *             N(마감버튼 비활성화 일경우...)
 * 마감관리 : 평가중, 평가마감인 년도가 출력되고 평가마감시 마감버튼 비활성
 * 시뮬레이션 : 기타가 아닌 년도가 출력되고 시뮬레이션(C)만 마감버튼 활성
 * 핵심지표 분석   : 마감된 년도만 출력
 * 
 * 평가구간 설정 : 기타가 아닌 년도가 출력되고 평가마감시 저장, 초기화버튼 비활성
 * 지표 목표입력 : 기타가 아닌 년도가 출력되고 평가마감시 저장, 초기화버튼 비활성
 * 가중치 관리 : 기타가 아닌 년도가 출력되고 평가마감시 저장, 초기화버튼 비활성
 * 평가대상학교 설정 : 기타가 아닌 년도가 출력되고 평가마감시 저장, 초기화버튼 비활성  
 */
function alreadyClose(displayYn){
	var yyyy = $("select[name=yyyy]").attr("selected",true).val();
	if(yyyy == undefined) yyyy = $("select[name=pYyyy]").attr("selected",true).val();
	if(yyyy == undefined) yyyy = $("select[name=summaryYear]").attr("selected",true).val();
	if(yyyy == undefined) yyyy = $("select[name=selectYear]").attr("selected",true).val();
	
	var evalStep = $("input[name=evalStep_" + yyyy + "]").val();

	switch(displayYn){
		case 1:
			if(evalStep == "C"){
				$("#closeBtn").css("display", "");
				$("#defaultDataBtn").css("display", "");
				$("#saveBtn").css("display", "");
				$("#createDataBtn").css("display", "");
			}else{
				$("#closeBtn").css("display", "none");
				$("#defaultDataBtn").css("display", "none");
				$("#saveBtn").css("display", "none");
				$("#createDataBtn").css("display", "none");
			}
		break;
		
		case 2:
			if(evalStep == "B"){
				$("#closeBtn").css("display", "none");
				$("#defaultDataBtn").css("display", "none");
				$("#saveBtn").css("display", "none");
				$("#createDataBtn").css("display", "none");
			}else{
				$("#closeBtn").css("display", "");
				$("#defaultDataBtn").css("display", "");
				$("#saveBtn").css("display", "");
				$("#createDataBtn").css("display", "");
			}
		break;
		
		default:
			$("#closeBtn").css("display", "none");	
			$("#defaultDataBtn").css("display", "none");
			$("#saveBtn").css("display", "none");
			$("#createDataBtn").css("display", "none");
			
	}
}

function windowOpen(url, windowName, width, height, location, scrollbars){
	var screenWidth=screen.width;
	var screenHeight=screen.height;

	var x=(screenWidth/2)-(width/2);
	var y=(screenHeight/2)-(height/2);
	//2013-12-18장민수 수정
	//새창 객체를 리턴하도록.
	return window.open(url, windowName,"width=" + width + ", height=" + height + ",  location=" + location + ", toolbar=no, menubar=no, directories=no, scrollbars=" + scrollbars + ", resizable=no, left="+ x + ", top=" + y);
}

function arrJoin(glue, arr){
	tmp_str = "";
	for(var i=0; i<arr.length; i++){
		if(i==0){
			tmp_str = arr[i].value;
		}else{
			tmp_str += glue+arr[i].value;
		}
	}
	return tmp_str;
}

function resetForm(obj){
	for (var i=0;i<obj.elements.length ;i++ ){
		if (obj.elements[i].type == "select-one"){
			obj.elements[i][0].selected = true;
		}else if(obj.elements[i].type == "text"){
			obj.elements[i].value = "";
		}else if(obj.elements[i].type == "textarea"){
				obj.elements[i].value = "";
		}else if(obj.elements[i].type == "file"){
			obj.elements[i].select();
			document.selection.clear();			
		}
	}
	for (var i=0;i<obj.elements.length ;i++ ){
		if(obj.elements[i].type == "text" || obj.elements[i].type == "select-one"){
			obj.elements[i].select();
			break;
		}
	}
}

function goPage(pageNum){

	if(Number($("#pageNum").val()) > Number($("#totalPage").val())){
		alert("잘못된 페이지번호 입니다.");
		return;
	}
	if(Number(pageNum) == 0){
		alert("첫 페이지 입니다.");
		return;
	}
	if(Number(pageNum) > Number($("#totalPage").val())){
		alert("마지막 페이지 입니다.");
		return;	
	}

	document.location.href="?pageNum=" + pageNum + "&pageSize=" + $("#pageSize").val() + "&" + $("#pageParam").val();				
}

//좌측 공백제거
function ltrim(str){
	var i,j = 0;
	var objstr;
	for(i=0; i< str.length; i++){
		if(str.charAt(i) == ' ')
			j=j + 1;
		else 
			break;
	}
	return str.substring(j, str.length - j + 1);
}

//우측 공백제거
function rtrim(str){
	var i,j = 0;
	for(i = str.length-1;i>=0; i--){
		if(str.charAt(i) == ' ')
			j=j + 1;
		else 
			break;
	}
	return str.substring(0, str.length - j);
}

//좌우 공백제거
function trim(str){
	var i,j = 0;
	var objstr;
	for(i=0; i< str.length; i++){
		if(str.charAt(i) == ' ')
			j=j + 1;
		else 
			break;
	}
	str = str.substring(j, str.length - j + 1);
 
	i,j = 0;
	for(i = str.length-1;i>=0; i--){
		if(str.charAt(i) == ' ')
			j=j + 1;
		else 
			break;
	}
	return str.substring(0, str.length - j);
}

//값이 배열에 존재하는가
function indexOfArr(key, arr){
	if(typeof(arr) != "object") return false;
	
	for(var i=0;i<arr.length;i++){
		if(arr[i][0] == key){
			return i;
		}
	}
	return -1;
}

/**
 * 작성자 : 신운식
 * 반올림(소수점 자리수 표현)
 * @param val(값)
 * @param precision(소수점 자리수)
 * @returns {Number}
 */
function fncRoundPrecision(val, precision){
	var p = Math.pow(10, precision);
	return Math.round(val * p) / p;
}

/**
 * 작성자: 신운식
 * DB에서 출력한 세로형태의 데이터를 가로형태로 변환
 * DATA_CNT: 반복되어질 data의 group count 
 * @param res (세로형태의 데이터: dwr return value)
 * @param xKey (x축 key name)
 * @param yKey (y축 key name)
 * @param yValue (y축 value name)
 * @returns 리스트 형태의 배열 List<Array>
 */
function changeDataPosition(res, xKey, yKey, yValue, isValueNum){
	if(res == null) return;
	
	var data = new Array();
	if(res.length > 0){
		isValueNum = (isValueNum == undefined) ? true : isValueNum;  
		for(var i=0; i<res.length; i++){
			var tempArray = new Array();
			tempArray[xKey] = res[i][xKey];
//			console.log(res[i][xKey])
			var dataCnt = (res[i].DATA_CNT == "") ? 0 : res[i].DATA_CNT;
			for(var j=0; j<dataCnt; j++){
				if(isValueNum) tempArray[res[i][yKey]] = Number(res[i][yValue]);
				else tempArray[res[i][yKey]] = res[i][yValue];
				
//				console.log(res[i][yKey] + " : " + res[i][yValue]);
				if(j != dataCnt-1) i++;
			}
			data.push(tempArray);
		}
	}else{
		data.push(new Array("[{" + xKey + ":'', " + yKey + ":''}]"));
	}
	return data;
}

/**
 * 작성자: 신운식
 * 배열내 맥스값 알아오기
 * @param res (dwr return value)
 * @param key (비교할 값의 key)
 * @returns
 * TODO: 98.5인경우 99가 반환되어 이상(?)함 100이나 110값으로 변환을 해야하나 아직 정책 결정이 되지 않아 보류...
 * 현재 사용하지 않음
 */
function getMaxValue(res, key){
	var max = 0 ;
	var tempArray = new Array();
	for(var i=0; i<res.length; i++){
		tempArray[i] = res[i][key];
	}
	
	for (var i = 0 ; i < tempArray.length  ; i++ ){
	    if(max < tempArray[i]) max = tempArray[i] ;
	}
//		var min=100;
//		for (var i = 0 ; i < su.length  ; i++ ){
//		    if(min > su[i]) min = su[i] ;
//		}
	return Math.round(max);
}

/**
 * 3자리마다 콤마(,)
 */
function number_format(num){
    var num_str = num.toString();
    var result = "";
 
    for(var i=0; i<num_str.length; i++){
        var tmp = num_str.length - (i+1);
 
        if(((i%3)==0) && (i!=0))    result = ',' + result;
        result = num_str.charAt(tmp) + result;
    }
 
    return result;
}

//배열 인덱스 return
function arrayIndexOf(arr, str)
{
    var pos = -1;
    for(var i=0; i<arr.length; i++)
    {
        if(arr[i]==str)
        {
            pos = i;
            break;
        }
    }
    return pos;
}

function downloadExcel(idx, map){
	
	var formHtml = "";
	$("#downExcelForm").remove();
	formHtml += "<form id=\"downExcelForm\" name=\"downExcelForm\" action=\"/comm/downloadExcel\" method=\"post\">";
	formHtml += "<input type=\"hidden\" id=\"idx\" name=\"idx\" value=\"" + idx + "\" />";
	formHtml += "<input type=\"hidden\" id=\"fileName\" name=\"fileName\" value=\"" + $(document).attr("title") + "\" />";
	
	for(var i=0; i<map.size(); i++){
		formHtml += "<input type=\"hidden\" id=\"" + map.getKey(i) + "\" name=\"" + map.getKey(i) + "\" value=\"" + map.get(map.getKey(i)) + "\" />";
	}			
	
	formHtml += "</form>";

	$("body").append(formHtml);
	$("#downExcelForm").submit();
}
function downloadExcelWithProcedure(idx,map){
	var formHtml = "";
	$("#downExcelForm").remove();
	formHtml += "<form id=\"downExcelForm\" name=\"downExcelForm\" action=\"/comm/downloadExcelWithProcedure\" method=\"post\">";
	formHtml += "<input type=\"hidden\" id=\"idx\" name=\"idx\" value=\"" + idx + "\" />";
	formHtml += "<input type=\"hidden\" id=\"fileName\" name=\"fileName\" value=\"" + $(document).attr("title") + "\" />";
	
	for(var i=0; i<map.size(); i++){
		formHtml += "<input type=\"hidden\" id=\"" + map.getKey(i) + "\" name=\"" + map.getKey(i) + "\" value=\"" + map.get(map.getKey(i)) + "\" />";
	}			
	
	formHtml += "</form>";

	$("body").append(formHtml);
	$("#downExcelForm").submit();
}

/**
 * 작성자 장민수
 * @param title 제목
 * @param msg   메시지
 * @param imagePath  로딩 이미지.. 생략가능
 */
function showProgressBar(title,msg,imagePath){
	if(imagePath==null || imagePath==""){
		imagePath="/resource/img";
	}
	if($("#iVisionPlusProgressBar").length==0){
		$(document.body).append("<div id='iVisionPlusProgressBar' style='position:absolute;top:50%;left:50%;width:380px;height:110px;margin-top:-55px;margin-left:-190px;border:1px solid #eeeeee;background:#ffffff;' >"+
				"<h1 id='iVisionPlusProgressBar_title' style='font-size:100%;color:#2c3945;font-weight:bold;height:20px;background:#eff3fa;padding-left:10px;padding-top:10px;border-bottom:1px solid #c7d9e6;display:block;'>"+title+"</h1><!--상단 타이틀-->"+
				"<div class='progressbar' style = 'width:220px;height:19px;position:relative;margin:20px auto;margin-bottom:7px;'>"+
				"		<span style='position:absolute;display:block;width:220px;height:19px;background:url("+imagePath+"/loading_bar.gif) no-repeat 0 0;top:0px;left:0px;overflow:hidden;text-indent:-8000px;'><em style='left:220px;position:absolute;display:block;height:25px;top:0;'>100%</em></span>"+
				"</div>"+
				"<p id='iVisionPlusProgressBar_msg' style='font-size:90%;color:#738695;text-align:center;'>"+msg+"</p><!--아래 멘트-->"+
			"</div>"
		);
	}
	else{
		$("#iVisionPlusProgressBar").show();
		$("#iVisionPlusProgressBar_title").html(title);
		$("#iVisionPlusProgressBar_msg").html(msg);
	}	
}
function comboBoxClear(id)
{
	$(id+" option").remove();
	$("<option></option>").text("===선택===").attr("value", "").appendTo(id);
}
function hideProgressBar(){
	$("#iVisionPlusProgressBar").hide();
}
function onlyNumber() {
	if((event.keyCode > 31) && (event.keyCode < 45) || (event.keyCode > 57)) {
	event.returnValue = false;
	}
}
