 	//cogurl : 'http://192.168.12.110/ibmcognos/cgi-bin/cognosisapi.dll'
	//type : AnalysisStudio
	function cogpopup(target,cogurl, type){
		//cognosLaunchInWindow(target, '','ui.gateway', cogurl, 'ui.tool', type, 'ui.object', '', 'ui.folder', '', 'ui.action', 'new');
		window.open('/cognosbia.jsp?type='+type+'+&url='+cogurl);   
		//cognosLaunch(target,'','ui.gateway',cogurl,'ui.tool','ReportStudio','ui.profile',type,'','','','') ;    

	} 
	var uriContext = "../..";
	var iCostDomain = ''; // icost 주소
	var locale = "ko";
	 
	//Document  Init 
	var curTopMenu = null; 
	var curLeftMenu = null;//현재 열려있는 메뉴 정보 //메인메뉴 제외 
	var solutionType = "IVISIONPLUS";
//	var solutionType = $iv.envinfo.division;
	var treeFlag = false;
	
	
	var first_status = true;
	function toggleLeftTab(flag){
		if(flag!=null){  
			if(flag){ 
				$("#leftTabs").hide();$("#content").css("margin-left","0px");
			}else{
				$("#leftTabs").show();$("#content").css("margin-left","211px");
			}
			$(window).trigger("resize");
			return $("#leftTabs").is(":visible");
		}else{
			if($("#leftTabs").is(":visible")){ 
				$("#leftTabs").hide();$("#content").css("margin-left","0px");
			}else{
				$("#leftTabs").show();$("#content").css("margin-left","211px");
			}
			$(window).trigger("resize");
			return $("#leftTabs").is(":visible");
		}
		
	}
	function sessionCheck(){
		$.ajax({
			url : '/imanager/system/session/logout',
			success : function(d){},
			complete:function(data){},
			error:function(err){}
		});
		$.ajax({
			url : '/ibiztask/system/session/logout',
			success : function(d){},
			complete:function(data){},
			error:function(err){}
		});
		$.ajax({
			url : '/ivision/system/session/logout',
			success : function(d){},
			complete:function(data){},
			error:function(err){}
		});
		$.ajax({
			url : '/idashboard/system/session/logout',
			success : function(d){},
			complete:function(data){},
			error:function(err){}
		});
		
	}
	$(document).ready(function(){ 
		//i-Cost 도메인 주소
		sessionCheck();
		//IECT7002에 DIVISION 사라짐으로써 현재로는 쓰지 않는 기능
//		$iv.mybatis("dwrGetSystemUrl",{ACCT_ID:$iv.userinfo.acctid(),DIVISION:$iv.envinfo.division,GUBN:'ICOST'},function(res){
//			iCostDomain = res[0].KEY_VALUE;
//		});
		
		//메인화면 [대시보드 ]불러오기
		makeMainContents(mainurl);     
		//공지사항불러오기
		getNoticePopUpList();
		///탑메뉴 불러오기 
		makeTopMenu();
		//메인 탭 
		makeMainTap();
		//왼쪽 중메뉴,즐겨찾기 탭
		makeLeftTap();
		//즐겨찾기 기능 
		makeBookmark();
		//윈도우 이벤트
		setWindowEvent();
		//버튼 이벤트
		setButtonEvent();
		
		//롤별,그룹별 시작페이지 load
		//loadPageletByPermission();
	});	 
	
	
	
	function setWindowEvent(){  
		window.onresize = function(event){ 
			var tot_height = $(window).height();
			var tot_width = $(window).width();
			$("#mainTabContents").height(tot_height- 121);    
			var left = $("#leftTabs").width()+1;
			if($("#leftTabs").is(":visible")==false) left = 0; 
			//$("[data-role=tabcontentsifm]").height(tot_height - 145);     
			$("#jqxTree").height(tot_height- 80);    
		};
	}
	
	function loadPageletByPermission(){
		$i.post('../../home/layout/getPageletByUserID').done(function(result){ 
			$.each(result.returnArray,function(idx,item){
				if(idx==0){
					$("#mainTabTitle").html(item.PL_NAME);
					$("#mainTabContents").attr("src","../../portlet/loader/l"+item.PL_LAYOUT+"?plID="+item.PLID);  
					$("#mainTabContents").on("load",function(){ 
						$("#mainTabContents").height($(window).height()- 121); 
					});   
				}					
				else{
					addTab($.guid++,item.PL_NAME,""+"../../portlet/loader/l"+item.PL_LAYOUT+"?plID="+item.PLID);
				}
				
		})});
	}
	
	function makeMainContents(url){
		loadPageletByPermission();
	}
	function jqxpopupclose(id){
		$('#'+id).jqxWindow('close');
		$('#'+id).remove();  
	}
	function jqxpopupopen(id,options){
		var popid = id;
		if($("#"+popid).length!=0) $("#"+popid).remove(); 
		var opts={
			framewidth:890,	
			frameheight:530,  
			width:823,
			height:580,
			src:"",
			title:"제목없음",
		};
		opts = $.extend(opts, options);
		var popup = "<div id='"+popid+"' style='position:absolute;z-index:999999;'>"+
			"<div>"+opts.title+"</div>"+
			"<div>"+ 
				"<iframe style='width:"+opts.framewidth+"px !important;height:"+opts.frameheight+"px !important;' frameborder='0' src='"+uriContext + opts.src+"'></iframe>"+
			"</div>"+
		"</div>";
		$(document).find("body").append(popup);
		 
		var offset = $("body").offset();
		var size = ($("body").width()-opts.width)/2; 
		
		$("#"+popid).jqxWindow({
			
			theme:'Lotte-depart',
            position: { x: offset.left + size, y: offset.top + 150} ,
            maxHeight: opts.height, maxWidth:  opts.width, minHeight: opts.height, minWidth:  opts.width, height: opts.height, width: opts.width,
            resizable: true, isModal: true, modalOpacity: 0.7,
            initContent: function (a,b,c,d,e) {
            	$("#"+popid).find("iframe").css({width:"100%",height:"100%"}); 

            }
        });
		 
		//$("#"+popid).find("iframe").css("width","100%").css("height","100%"); 
		$("#"+popid).find("iframe").css({width:"100%",height:"100%"});
	}
	function getNoticePopUpList(){
		var boardArray = [];
		var boardData = $i.post("../../ktoto/notice/getKtotoNoticePopupList", {});
		boardData.done(function(bdata){
			res = bdata.returnArray;
			var modalColor = 0;
			for(var i = 0; i< res.length; i++){
				boardArray.push(res[i].BOARDNO);
				boardNo = res[i].BOARDNO;
				title = res[i].TITLE;
				content = res[i].CONTENT;
				popup_yn = getCookie('notice_popup_'+boardNo);
				if(popup_yn != 'n'){
					modalColor+=1;
					var popid = "notice_popup_"+boardNo;
					var popup = "<div id='"+popid+"' class='popup_box' >"+
									"<div class='tit'>"+title+"</div>"+
									"<div>"+
										"<div class='popup_box_contents' style='height:450px; overflow:auto;'>"+content+"</div>"+
										"<div class='popup_box_contents' style='height:50px; overflow:auto;' id='fileList"+boardNo+"'></div>"+
										"<div class='popup_box_bottom'>"+
											"<span><input id='cookie_checkbox' type='checkbox' value='y' style='vertical-align:middle;' onchange=\"popupClose('"+boardNo+"');\"/>그만 보기 <a onclick=\"popupClose('"+boardNo+"');\">[확인]</a></span>"
									    "</div>"+  
									"</div>"+
								"</div>";
					$(document).find("body").append(popup);
					 
					var offset = $("body").offset();
					var size = ($("body").width()-820)/2; 
					
					$("#"+popid).jqxWindow({
						
						theme:'Lotte-depart',
		                position: { x: offset.left + size, y: offset.top + 150} ,
		                height:"100%",
		                width:"100%",
		                maxHeight: 600, maxWidth: 966, minHeight: 580, minWidth: 800, height: 580, width: 823,
		                resizable: true,
		                isModal: true, 
		                modalOpacity: ((modalColor==1)?0.7:0), 
		                initContent: function (a,b,c,d,e) {

		                }
		            });
				}
			}
			for(var k=0;k<boardArray.length;k++){
				var fileData = $i.post("../../ktoto/notice/getNoticeDtailFileInfo", {boardNO:boardArray[k]});
				fileData.done(function(data){
					if(data.returnArray.length > 0){
						var fileList = "";
						for(var j=0;j<data.returnArray.length; j++){
							fileList += "▶다운로드 <span style='font-weight: bold; padding-right: 10px;'>"+data.returnArray[j].FILE_ORG_NAME+"</span><a href='../../cms/board/download?fileId="+data.returnArray[j].FILE_ID+"'><img src='../../resources/cmresource/css/iplanbiz/theme/ktoto/img/btn_save.png' border='0' /></a><br>"
//							fileList += "<span>"+data.returnArray[j].FILE_ORG_NAME+"</span><a href='../../cms/board/download?fileId="+data.returnArray[j].FILE_ID+"'><img src='../../resource/css/images/img_icons/down.png' border='0' /></a><br>"
						}
						$(document).find("#fileList"+data.returnArray[0].BOARD_NO).append(fileList);
					}
				});
			}
		});
	}
	function openHelpWin(cid, path, menuName){  
		var popid = "help_"+cid;
		if($("#"+popid).length!=0) { 
			$("#"+popid).remove();
		}
			var popup = "<div id='"+popid+"' style='position:absolute;z-index:999999;'>"+
				"<div>도움말</div>"+
				"<div>"+ 
					"<iframe style='width:880px !important;height:530px !important;' frameborder='0' src='../../admin/help/helpPopup?cID="+cid + "&path=" + encodeURIComponent(path) + "&menuNm=" + encodeURIComponent(menuName) +"'></iframe>"+
				"</div>"+
			"</div>";
			$(document).find("#wrap").prepend(popup);
			var offset = {left:0,top:0};
			var size = ($("body").width()-820)/2; 
			$("#"+popid).jqxWindow({
				
				theme:'Lotte-depart',
	            position: { x: offset.left + size, y: offset.top + 150} ,
	            maxHeight: 600, maxWidth: 966, minHeight: 580, minWidth: 800, height: 580, width: 820,
	            resizable: true, isModal: true, modalOpacity: 0.7,
	            initContent: function (a,b,c,d,e) {
	            	$("#"+popid).find("iframe").css({width:"100%",height:"100%"}); 
	
	            }
	        }); 
			$("#"+popid).find("iframe").css({width:"100%",height:"100%"}); 
		 
	}
	
	function popupCogSearch(){  
		var popid = "cogsearchpop";
		if($("#"+popid).length!=0) {
			$("#"+popid).remove();
		}
		
		//"/cogSearch","보고서검색",1034,500
			var popup = "<div id='"+popid+"' style='position:absolute;z-index:999999;'>"+
				"<div>보고서찾기</div>"+
				"<div>"+
					"<iframe style='width:880px !important;height:530px !important;' frameborder='0' src='"+uriContext+"/cogSearch'></iframe>"+
				"</div>"+
			"</div>";
			$(document).find("#wrap").prepend(popup);
			var offset = $("body").offset();
			var size = ($("body").width()-820)/2; 
			
			$("#"+popid).jqxWindow({ 
				
				theme:'Lotte-depart',
	            position: { x: offset.left + size, y: offset.top + 150} ,
	            maxHeight: 600, maxWidth: 966, minHeight: 580, minWidth: 800, height: 580, width: 820,
	            resizable: true, isModal: true, modalOpacity: 0.7,
	            initContent: function (a,b,c,d,e) {
	            	$("#"+popid).find("iframe").css({width:"100%",height:"100%"}); 
	
	            }
	        });
			 
			//$("#"+popid).find("iframe").css("width","100%").css("height","100%"); 
			$("#"+popid).find("iframe").css({width:"100%",height:"100%"});
			//newWinPopUp(uriContext + '/cms/notice/get?type=popup&boardNo='+boardNo,"notice_popup_"+boardNo,866,500);
		 
	}
	// 쿠키 관련 스크립트 추가
	function getCookieVal (offset) {
		var endstr = document.cookie.indexOf (";", offset); 
		if (endstr == -1) endstr = document.cookie.length; 
		return unescape(document.cookie.substring(offset, endstr)); 
	}
	function getCookie (name) { 
		var arg = name + "="; 
		var alen = arg.length; 
		var clen = document.cookie.length; 
		var i = 0; 
		while (i < clen) { 
			//while open 
			var j = i + alen; 
			if (document.cookie.substring(i, j) == arg) return getCookieVal (j); 
			i = document.cookie.indexOf(" ", i) + 1; 
			if (i == 0) break; 
		} 
		//while close 
		return null; 
	}
	
	
	function setCookie (name, value) { 
		var argv = setCookie.arguments;
		var argc = setCookie.arguments.length;
		
		var expires = (2 < argc) ? argv[2] : null;
		var toDate = new Date();
		toDate.setDate(toDate.getDate() + expires);
		var path = (3 < argc) ? argv[3] : null;
		var domain = (4 < argc) ? argv[4] : null;
		var secure = (5 < argc) ? argv[5] : false; 
		document.cookie = name + "=" + escape (value) + ((expires == null) ? "" : ("; expires=" + toDate.toGMTString())) + ((path == null) ? "" : ("; path=" + path)) + ((domain == null) ? "" : ("; domain=" + domain)) + ((secure == true) ? "; secure" : ""); 
	}

	
	function makeLeftTap(){
		$("#leftTabs div[data-role=tabheader]").on("click", function (event){
			var selectidx = $(this).data("idx");
			
			
			selectLeftTab(  selectidx );
			
			
		});
	}
	var isfavorite = false;
	function selectLeftTab(selectidx){
		var selectimg = rootContext+"/resources/img/theme/07/left_tab"+selectidx+"_select";
		var custom = $("link[data-role=custom_theme]");
		if(custom.length!=0){
			if(custom.data("idx")!=null)
				selectimg+="-"+custom.data("idx");
		}
		var unselectidx = selectidx==1?0:1;
		
		if(selectidx==1) isfavorite = true;
		else isfavorite = false;
		
		var unselectimg = rootContext+"/resources/img/theme/07/left_tab"+unselectidx+"_unselect";
		$("#leftTabs div[data-role=tabheader][data-idx="+selectidx+"]").css({
			"background":"url("+selectimg+".png)",
			"font-weight":'bold',
			color:'#ffffff'
			});
		$("#leftTabs div[data-role=tabheader][data-idx="+unselectidx+"]")
		.css({
			"background" : 'url('+unselectimg+'.png)', 
			"font-weight":'none',
			color:'#53687f'
				});
		
		$("#leftTabs div[data-role=tabcontents]").hide();
		$("#leftTabs div[data-role=tabcontents]").eq(selectidx).show();
	}
	function setButtonEvent(){
		$("#allclosebutton").on("click", closeAllTabs);
		$("#btnLogOut").on("click",logout);
		$("#changepwd").on("click",changePwd);
		//메인탭 토글버튼
		$("#btnToggle").on("click", function(event){
			var bol = toggleLeftTab();
			if(bol)
				$("#btnToggle").attr("src",rootContext+"/resources/img/theme/07/left_hide_btn.gif");
			else
				$("#btnToggle").attr("src",rootContext+"/resources/img/theme/07/right_hide_btn.gif"); 
			 
			
			//왼쪽트리 기본 탑메뉴 첫번째 메뉴로 보여줄것. 
			if($("#jqxTree").html().trim()==""){
				$("#topMenu li").eq(0).trigger("click");
			} 
			
		});
		
		//$("#btnsch").on("click",popupCogSearch);
		
	}
	
 
	var curTop = "&ISNOTYETLOAD";
	function makeTopMenu() {
		var rootCID = $i.post("../../home/layout/getRootCID",{});
		rootCID.done(function(rootData){
			if(rootData.returnCode == "EXCEPTION"){
				$i.dialog.alert("SYSTEM", rootData.returnMessage);
			}else{
				var pidstr = rootData.returnArray[0].C_ID;
				if(pidstr == null) pidstr = 5999;
				var topMenuList = $i.post("../../home/layout/getTopMenuList",{pID:pidstr});
				topMenuList.done(function(topMenuData){
					if(topMenuData.returnCode == "EXCEPTION"){
						$i.dialog.alert("SYSTEM", topMenuData.returnMessage);
					}else{
						makeTopMenuCallback(topMenuData.returnArray);
					}
				});
			}
		});
	} 
	function makeTopMenuCallback(res){  
//		for(var i=0;i<res.length;i++){
//			if(res[i].SOURCE_OWNER == "BIFOLDER"){
//				res[i].C_ID = cogMyfolder;
//			}
//		}
		if(solutionType == "IPORTAL"){
			var obj = xmlParser("./cogMenu", "ac=top", 2);
			res = obj.res.concat(res);
		}
		curTopMenu = res;
		$("#topMenu").empty();
		$("#topMenu").append("<ul></ul>");
		$.each(res,function(idx,item){
			if(item.SOURCE_OWNER == "BIFOLDER"){
				var id = item.C_ID;
				item.C_ID = cogMyfolder;
				$("#topMenu ul").append("<li id='"+id+"'><a><span class='off'>"+item.C_NAME+"</span></a></li>");
				$("#"+id).data("json",item);
			}else{
				var id = item.C_ID;
				$("#topMenu ul").append("<li id='"+ item.C_ID+"'><a><span class='off'>"+item.C_NAME+"</span></a></li>");
				$("#"+id).data("json",item);
			}
			
		});
		
		$("#topMenu li").on("click",function(event){ 
			// get the clicked LI element.
			toggleLeftTab(false);
			selectLeftTab(0);
			var json  = $(this).data("json");
			var c_nm = json.C_NAME;
			curTop = c_nm;
			var c_id = json.C_ID;
			var treeTitle = "";
			if(c_nm.length > 7){
				treeTitle = c_nm.substring(0,7) + "..";
			}else{
				treeTitle = c_nm;
			}
			$("#leftMenuTitle").html(treeTitle);
			//$('#tabs').jqxTabs('select', 0);
			
			
			//코그너스 보고서가 아니면 IECT7001에서 조회
			var item = Enumerable.From(curTopMenu).Where(function(c) {
				return c.C_ID == c_id;
			}).FirstOrDefault();
			if (item != null&&item.SOURCE_OWNER!="BIFOLDER" && item.cType == null) {
				var menuData = $i.post("../../home/layout/getMenuList", {pID:c_id});
				menuData.done(function(menuList){
					if(menuList.returnCode == "EXCEPTION"){
						$i.dialog.alert("SYSTEM", menuList.returnMessage);
					}else{
						makeLeftMenu(menuList.returnArray, c_id);
					}
				});
			}
			//코그너스 보고서면 API
			else {
//				var result = xmlParser("./cogMenu", "ac=left&op=&storeId="+c_id, 1,makeLeftMenu);
				
				$i.post("../../admin/menu/getMenuBiSolutionPathDeepScan",{cID:c_id,cName:json.C_NAME,cLink:json.C_LINK,ac:"left"}).done(function(data){
					makeLeftMenu(data.returnArray,c_id);
				});
 
			}
		});
		//첫번째 메뉴 클릭 후 펼치기.
		$("#topMenu li").eq(0).trigger("click"); 
		//toggleLeftTab(true);
	}
	function makeMainTap(){
		var tabs=$("#content").tabs({
			activate:function(event,ui){
				//if(ui.newTab.hasClass("ui-tabs-paging-next")==false&&ui.newTab.hasClass("ui-tabs-paging-prev")==false){
				if(ui.newTab.hasClass("ui-tabs-paging-next")==false){
					
					ui.oldTab.removeClass("select");
					ui.newTab.addClass("select");
					ui.oldTab.find("i").removeClass("fa_tab_sel");
					ui.newTab.find("i").addClass("fa_tab_sel");
				//
					//ui.newPanel.find("iframe").height( $("body").height()-350);  
					
					
					//ui.newPanel.find("[data-role=tabcontentsifm]").height($(window).height() - 145);
					
					
				}
					  
 
			},
			beforeActivate:function(event,ui){
				if(ui.newTab.hasClass("ui-tabs-paging-next")){ 
					return false; 
				}
			}
		});   
		tabs.tabs(
				'paging', 
				{   /*
					cycle: false,
					follow: false,
					followOnActive: false,
					activeOnAdd: false
					*/ 
					/*follow: true, followOnActive: true*/ 
					/*cache:true, follow: true, followOnActive: true,activeOnAdd: true*/   
				} 
		);
		tabs.find(".ui-tabs-nav").sortable({
			axis:"x",
			stop:function(){
				tabs.tabs("refresh");
			}
		});
		 
		tabs.on( "click", "i.ui-icon-close", function() {
		      var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
		      //$( "#" + panelId ).removeData();
		      $( "#" + panelId ).remove();
		      tabs.tabs( "refresh" );
		      $(".snb_navi").find("div[data-role=treeview]").jqxTree("selectItem",null);
		      
		});
		//$('#content').jqxTabs('hideCloseButtonAt', 0);
	}
	function newWinPopUp(url, title,width,height){
		var defwidth=500;
		var defheight = 460;
		if(width==null) width = defwidth;
		if(height==null) height = defheight;
		
		
		var winHeight = document.body.clientHeight;	// 현재창의 높이
		var winWidth = document.body.clientWidth;	// 현재창의 너비
		var winX = window.screenLeft;	// 현재창의 x좌표
		var winY = window.screenTop;	// 현재창의 y좌표

		var popX = winX + (winWidth - width)/2;
		var popY = 130;
		
		var nWin = window.open(url,title,""); 
		try{
			nWin.focus();
		}catch(e){
			alert("팝업 차단을 해제해주세요.");
		}
	}
	function xmlParser(xml_url, query_string, op, callback) {
		var rtdata = null;
		var xmlRequest = $.ajax({
			url : xml_url,
			data : query_string,
			type : "get",
			async : false,
			cache : false
		});
		var cid = null;
		if (query_string.indexOf("storeId")) {
			cid = query_string.substring(
					query_string.indexOf("storeId") + "storeId".length).split(
					"&")[0].replace(/=/g, "").trim();
		}
		var tmp = new Array();
		xmlRequest.done(function(res) {
			if (typeof res == 'string') {
				//window.location.href="/cogLogout";
				res=[];
			}
			$(res).find("menu").each(function() {
				/*
				 * id : 본인아이디
				 * pid : 부모아이디
				 * name : 출력명(보고서 이름)
				 * url : 타이틀 클릭시 액션(URL 링크)
				 * icon : 타이틀 앞 아이콘(보고서 종류에따라 아이콘 변경)
				 * copyImg : 내폴더로 아이콘
				 * copyUrl : 내폴더로 아이콘 클릭시 액션
				 */
				var pid = $(this).find("pid").text();
				var uid = $(this).find("uid").text();
				var name = $(this).find("name").text();
				var link = $(this).find("link").text();
				var icon = $(this).find("icon").text();
				var type = $(this).find("type").text();

				tmp.push({
					P_ID : pid,
					C_ID : uid,
					C_NAME : name,
					SORT_ORDER : link,
					cIcon : icon,
					cType : type
				});

			});

			// end menu loading 
			//$(".loader").hide();
			if(tmp==null){   
				window.location.href="../../login/action/cogLogout";
			} 

			if (callback != null)
				callback(tmp, cid);
			else
				rtdata = {
					res : tmp,
					pid : cid
				};
			//(tmp, op);
		});
 
		return rtdata; 

	}
	
	

	// go Search (코그너스 보고서 검색)
	function goSearch() {

		var keyword = $("#search").val();
		
		$("#tree_title").html("<span>검색결과(" + keyword + ")</span>");
		menuServiceDwr.getReportList(keyword, function(res) {
			makeLeftMenu(res, "");
		});
	}
	function bookTreeRender(treeid, element, type){
		var icon = "";
		if(type == 1){
			if (element.isExpanded != null && element.isExpanded) {
				icon = 'folderopenIcon.png';
			} else
				icon = 'folderIcon.png';
			label = "<img class='menutypeicon' style='float: left; margin-right: 5px;' data-label='"+element.label+"' src='"+$iv.envinfo.css+"/images/img_icons/" + icon + "'/><span item-title='true'  >"
			+ (element.originalTitle == null ? element.label
					: element.originalTitle)
			+ "</span>";
		}else{
			icon = 'pageIcon.png';
			label = "<img class='menutypeicon' style='float: left; margin-right: 5px;' data-type='"+type+"' data-label='"+element.label+"'  src='"+$iv.envinfo.css+"/images/img_icons/" + icon + "'/><span item-title='true'  >"
				+ (element.originalTitle == null ? element.label
						: element.originalTitle) + "</span>";
		}
		$("#" + treeid).jqxTree('updateItem', element, {
			label : label
		});
	}
	var dummycount = 0;
	//LEFT MENU GENERATE
	function makeLeftMenu(resData, c_id) {
		console.log(resData);
		selectLeftTab(0);
		
		curLeftMenu = resData;
		var pmenu = Enumerable.From(resData).Where(function(c) {
			return c.C_ID == c_id;
		}).ToArray();
		var source = {
			datatype : "json",
			datafields : [ {
				name : 'C_ID'
			}, {
				name : 'P_ID'
			}, {
				name : 'C_NAME'
			}, {
				name : 'C_LINK'
			}, {
				name : 'cIcon'
			}, {
				name : 'SOURCE_OWNER'
			}, {
				name : 'items'
			} ],
			id : 'id',
			localdata : resData
		};
		// create data adapter.
		var dataAdapter = new $.jqx.dataAdapter(source);
		// perform Data Binding.
		dataAdapter.dataBind();
	
		// get the tree items. The first parameter is the item's id. The second parameter is the parent item's id. The 'items' parameter represents 
		// the sub items collection name. Each jqxTree item has a 'label' property, but in the JSON data, we have a 'text' field. The last parameter 
		// specifies the mapping between the 'text' and 'label' fields.  
		var records = dataAdapter.getRecordsHierarchy('C_ID', 'P_ID', 'items',
				[ {
					name : 'C_ID',
					map : 'id'
				}, {
					name : 'C_NAME',
					map : 'label'
				} ]);
		if ((pmenu != null && pmenu.cType == null)
				&& Enumerable.From(records).Where(function(c) {
					return c.C_ID == 5999;
				}).FirstOrDefault() != null) {
			records = Enumerable.From(
					Enumerable.From(records).Where(function(c) {
						return c.C_ID == 5999;
					}).FirstOrDefault().items).Where(function(c) {
				return c.C_ID == c_id;  
			}).ToArray();
		}
		/*
		if ((pmenu != null && pmenu.cType == null && pmenu.SOURCE_OWNER!="BIFOLDER")
				&& Enumerable.From(records).Where(function(c) {
					return c.C_ID == 5999;
				}).FirstOrDefault() == null) {
			records = Enumerable.From(Enumerable.From(records)).Where(
					function(c) {
						return c.C_ID == c_id;
					}).ToArray();
		}
		**/ 
		var rcd = null;
		if(pmenu == null){
			rcd = records;
		} else {
			if(records != null){
				if(records[0].C_ID.indexOf("i") > -1){
					 rcd = records;	
				}else{
					rcd = (pmenu.cType==null && records.length == 1 && records[0].items != null ) ? records[0].items  
							: records;
				}
			}
		}
		$("#jqxTree").empty();
		$("#jqxTree").append("<ul></ul>");
		if(rcd == null){
			$("#jqxTree > ul > li").remove();
		}
		$.each(rcd, function(idx, item){
			var id = item.C_ID;
			$("#jqxTree ul").append("<li id='"+item.C_ID+"'><a   title='"+item.C_NAME+"'>"+item.C_NAME+"</a></li>");
			$("#"+id).data("json",item);
		}); 
		$(".jqx-tree-dropdown-root > li").css({"float":"left"});
		$(".jqx-tree-dropdown > li > span").css({ "float" : "left"});
		$(".jqx-tree-dropdown > li > div").css({ "float" : "left"});
		$(".jqx-tree-dropdown > li > ul").css({ "float" : "left"});
 
		
		$("#jqxTree li").on("click", function (event){
			event.stopPropagation();
			event.stopImmediatePropagation();
			
			var isopen = $(this).hasClass("open");
			$("#jqxTree div[data-role=treeview]").hide();
			$("#jqxTree li").removeClass("open");
			
			if(isopen==false){
				$(this).addClass("open");
				var self = $(this);
				var json = self.data("json");
				console.log(json);
				
				var iscognos = (String(json.C_ID).indexOf("i")>=0);
				if(json.items!=undefined && json.items!=null&& json.items.length!=0){				
					if(self.find("div[data-role=treeview]").length==0){
						self.append("<div data-role='treeview' style='float:0px;' class='menu'></div>");
						var selfdiv = self.find("div[data-role=treeview]");  
							var source =
				             {
				                 dataType: "json",
				                 dataFields: [
				                      { name: "C_ID", type: "string" },
				                      { name: "C_NAME", type: "string" },
				                      { name: "C_LINK", type: "string" },
				                      { name: "SOURCE_OWNER",type:"string"},  
				                      { name: "items", type: "array" }
				                 ],
				                 hierarchy:
				                     {
				                         root: "items"
				                     },
				                 localData: json.items,
				                 id: "C_ID"
				             };
				            var dataAdapter = new $.jqx.dataAdapter(source, {
				                loadComplete: function () {
				                }
				            });
				            dataAdapter.dataBind();				            
				            var records = dataAdapter.getRecordsHierarchy('C_ID','P_ID','items',[{ name: 'C_NAME', map: 'label'},{ name: 'C_LINK', map: 'link'},{ name: 'C_ID', map: 'value'}]);
							selfdiv.jqxTree({width:300,source:records,theme:'ui-redmond'});
							$(".jqx-tree-dropdown-root > li").css({"float":"left"});
							$(".jqx-tree-dropdown > li > span").css({ "float" : "left"});
							$(".jqx-tree-dropdown > li > div").css({ "float" : "left"});
							$(".jqx-tree-dropdown > li > ul").css({ "float" : "left"});
							
							selfdiv.on("click", function(event){
									
									var treenode = $(this).jqxTree('getSelectedItem');
									var cid = treenode.value;
									if(cid==null)cid = treenode.id;
									var row = Enumerable.From(curLeftMenu).Where(function(c){ return c.C_ID==cid;}).FirstOrDefault(); 
									event.stopPropagation();
									event.stopImmediatePropagation();
									
									if(treenode.hasItems==true){
										if(treenode.isExpanded){
											selfdiv.jqxTree('collapseItem',treenode);
											$("#"+cid).children("span").removeClass("jqx-icon-arrow-down-ui-redmond");
										}
										
										else
											selfdiv.jqxTree('expandItem',treenode); 
									}  
									else{	
										var owner = row.SOURCE_OWNER==null?"IPORTAL":row.SOURCE_OWNER;
										var innerurl = "";
										if(owner=="SOLUTION"||owner=="LINK"){
											innerurl = row.C_LINK;
										}
										else if(owner=="ICOST") {
											innerurl = iCostDomain+row.C_LINK;
										}
										else{
											innerurl = uriContext+ row.C_LINK;
										}  
										if(row.C_ID.toString().toUpperCase().indexOf("I")!=-1){
											cogtype = "cog";
											addTab(row.C_ID, row.C_NAME,innerurl,owner,cogtype);
										}
										else{
											addTab(row.C_ID, row.C_NAME,innerurl,owner);
										}
	//									addTab(row.C_ID, row.C_NAME,innerurl,owner);  
									}
									event.preventDefault();
									return false;
							 
								});
						
					}else{
						self.find("div[data-role=treeview]").show();
					}
				}
				else{
					
					if((json.cIcon!=undefined && json.cIcon.indexOf("folder")!=-1)==false){
						
						var owner = json.SOURCE_OWNER==null?"IPORTAL":json.SOURCE_OWNER;
						var innerurl = "";
						if(owner=="SOLUTION"||owner=="LINK"){
							innerurl = json.C_LINK;
						}
						else{
							innerurl = uriContext+ json.C_LINK;
						}  
						if(json.C_ID.toString().toUpperCase().indexOf("I")!=-1){
							cogtype = "cog";
							addTab(json.C_ID, json.C_NAME,innerurl,owner,cogtype);
						}else if(innerurl.indexOf("views") != -1){
							cogtype = "tableau"
							addTab(json.C_ID, json.C_NAME,innerurl,owner,cogtype);
						}else{
							addTab(json.C_ID, json.C_NAME,innerurl,owner);
						}
					}
				}
			}
		});
		
	}  
	//LEFT MENU GENERATE
	var favoriteMenuList = [];
	function makeLeftFavoriteMenu(resData) {
		console.log(resData);
		favoriteMenuList = resData;
		
		var pmenu = Enumerable.From(resData).Where(function(c) {
			return c.P_ID == 5999;
		}).ToArray();
		console.log(pmenu);
		var source = {
			datatype : "json",
			datafields : [ {
				name : 'C_ID'
			}, {
				name : 'P_ID'
			}, {
				name : 'C_NAME'
			}, {
				name : 'C_LINK'
			}, {
				name : 'REAL_C_ID'
			}, {
				name : 'SOURCE_OWNER'
			}, {
				name : 'items'
			} ],
			id : 'id',
			localdata : resData
		};
		// create data adapter.
		var dataAdapter = new $.jqx.dataAdapter(source);
		// perform Data Binding.
		dataAdapter.dataBind();
	
		// get the tree items. The first parameter is the item's id. The second parameter is the parent item's id. The 'items' parameter represents 
		// the sub items collection name. Each jqxTree item has a 'label' property, but in the JSON data, we have a 'text' field. The last parameter 
		// specifies the mapping between the 'text' and 'label' fields.  
		var records = dataAdapter.getRecordsHierarchy('C_ID', 'P_ID', 'items',
				[ {
					name : 'C_ID',
					map : 'id'
				}, {
					name : 'C_NAME',
					map : 'label'
				} ]);
		
		if ((pmenu != null && pmenu.cType == null)
				&& Enumerable.From(records).Where(function(c) {
					return c.C_ID == 5999;
				}).FirstOrDefault() != null) {
			records = Enumerable.From(
					Enumerable.From(records).Where(function(c) {
						return c.C_ID == 5999;
					}).FirstOrDefault().items).Where(function(c) {
				return c.C_ID == 5999;  
			}).ToArray();
		}
		/*
		if ((pmenu != null && pmenu.cType == null && pmenu.SOURCE_OWNER!="BIFOLDER")
				&& Enumerable.From(records).Where(function(c) {
					return c.C_ID == 5999;
				}).FirstOrDefault() == null) {
			records = Enumerable.From(Enumerable.From(records)).Where(
					function(c) {
						return c.C_ID == c_id;
					}).ToArray();
		}
		**/ 
		var rcd = null;
		/*if(pmenu == null){
			rcd = records;
		}else{
			rcd = (pmenu.cType==null && records.length == 1 && records[0].items != null) ? records[0].items
					: records;
		}*/
		rcd = records;
		console.log(rcd);
		$("#bookNavi").empty();
		if(rcd!=null){
			$("#bookNavi").append("<ul></ul>");
			$.each(rcd, function(idx, item){
				var id = item.C_ID;
				$("#bookNavi ul").append("<li id='"+item.C_ID+"'><a title='"+item.C_NAME+"'>"+item.C_NAME+"</a></li>");
				$("#"+id).data("json",item);
			}); 
			$(".jqx-tree-dropdown-root > li").css({"float":"left"});
			$(".jqx-tree-dropdown > li > span").css({ "float" : "left"});
			$(".jqx-tree-dropdown > li > div").css({ "float" : "left"});
			$(".jqx-tree-dropdown > li > ul").css({ "float" : "left"});
			
			
			$("#bookNavi li").on("click", function (event){
				event.stopPropagation();
				event.stopImmediatePropagation();
				
				var isopen = $(this).hasClass("open");
				$("#bookNavi div[data-role=treeview]").hide();
				$("#bookNavi li").removeClass("open");
				
				if(isopen==false){
					$(this).addClass("open");
					var self = $(this);
					var json = self.data("json");
					console.log(json);
					var iscognos = (String(json.C_ID).indexOf("i")>=0);
					if(json.items!=undefined && json.items!=null&& json.items.length!=0){				
						if(self.find("div[data-role=treeview]").length==0){
							self.append("<div data-role='treeview' style='float:0px;' class='menu'></div>");
							var selfdiv = self.find("div[data-role=treeview]");  
								var source =
					             {
					                 dataType: "json",
					                 dataFields: [
					                      { name: "C_ID", type: "string" },
					                      { name: "C_NAME", type: "string" },
					                      { name: "C_LINK", type: "string" },
					                      { name: "SOURCE_OWNER",type:"string"},  
					                      { name: "items", type: "array" }
					                 ],
					                 hierarchy:
					                     {
					                         root: "items"
					                     },
					                 localData: json.items,
					                 id: "C_ID"
					             };
					            var dataAdapter = new $.jqx.dataAdapter(source, {
					                loadComplete: function () {
					                }
					            });
					            dataAdapter.dataBind();				            
					            var records = dataAdapter.getRecordsHierarchy('C_ID','P_ID','items',[{ name: 'C_NAME', map: 'label'},{ name: 'C_LINK', map: 'link'},{ name: 'C_ID', map: 'value'}]);
								selfdiv.jqxTree({width:300,source:records,theme:'ui-redmond'});
								$(".jqx-tree-dropdown-root > li").css({"float":"left"});
								$(".jqx-tree-dropdown > li > span").css({ "float" : "left"});
								$(".jqx-tree-dropdown > li > div").css({ "float" : "left"});
								$(".jqx-tree-dropdown > li > ul").css({ "float" : "left"});
								selfdiv.on("click", function(event){
										
										var treenode = $(this).jqxTree('getSelectedItem');
										var cid = treenode.value;
										if(cid==null)cid = treenode.id;
										var row = Enumerable.From(favoriteMenuList).Where(function(c){ return c.C_ID==cid;}).FirstOrDefault(); 
										event.stopPropagation();
										event.stopImmediatePropagation();
										
										if(treenode.hasItems==true){
											if(treenode.isExpanded){
												selfdiv.jqxTree('collapseItem',treenode);
												$("#"+cid).children("span").removeClass("jqx-icon-arrow-down-ui-redmond");
											}
											
											else
												selfdiv.jqxTree('expandItem',treenode); 
										}  
										else{	
											if(row.C_LINK!=null){	
												var owner = row.SOURCE_OWNER==null?"IPORTAL":row.SOURCE_OWNER;
												var innerurl = "";
												if(owner=="SOLUTION"||owner=="LINK"){
													innerurl = row.C_LINK;
												}
												 
												else if(owner=="ICOST") {
													innerurl = iCostDomain+row.C_LINK;
												}
												else{
													innerurl = uriContext+ row.C_LINK;
												}  
												if(row.C_ID.toString().toUpperCase().indexOf("I")!=-1){
													cogtype = "cog";
													addTab(row.REAL_C_ID, row.C_NAME,innerurl,owner,cogtype);
												}
												else{
													addTab(row.REAL_C_ID, row.C_NAME,innerurl,owner);
												}
			//									addTab(row.C_ID, row.C_NAME,innerurl,owner);  
											}
										}
										event.preventDefault();
										return false;
								 
									});
							
						}else{
							self.find("div[data-role=treeview]").show();
						}
					}else{
						
						//if((json.cIcon!=undefined && json.cIcon.indexOf("folder")!=-1)==false){
						if(json.C_LINK!=null){	
							var owner = json.SOURCE_OWNER==null?"IPORTAL":json.SOURCE_OWNER;
							var innerurl = "";
							if(owner=="SOLUTION"||owner=="LINK"){
								innerurl = json.C_LINK;
							}
							else{
								innerurl = uriContext+ json.C_LINK;
							}  
							if(json.C_ID.toString().toUpperCase().indexOf("I")!=-1){
								cogtype = "cog";
								addTab(json.REAL_C_ID, json.C_NAME,innerurl,owner,cogtype);
							}else if(innerurl.indexOf("views") != -1){
								cogtype = "tableau"
								addTab(json.REAL_C_ID, json.C_NAME,innerurl,owner,cogtype);
							}else{
								addTab(json.REAL_C_ID, json.C_NAME,innerurl,owner);
							}
						}
					}
				}
			});
		}
		/*
		var rootList = Enumerable.From(resData).Where(function(c) {
			return c.P_ID == "5999";
		}).ToArray();
		
		
		$("#bookNavi").empty();
		$("#bookNavi").append("<ul></ul>");
		$.each(rootList,function(idx, item){ 
			var id = item.C_ID;

			$("#bookNavi ul").append("<li id='"+item.C_ID+"' data-json='"+item.REAL_C_ID+"'><a style='width:170px;text-overflow: ellipsis;-o-text-overflow:ellipsis;overflow:hidden;display:inline-block;white-space:nowrap;word-wrap:normal !important;' title='"+item.C_NAME+"'>"+item.C_NAME+"</a><i class='fa fa-close' style='float:right;margin:6px;cursor:pointer;' onclick=\"deleteFavoriteMenu('"+item.C_ID+"',"+(item.MENU_TYPE==1?"true":"false")+");\"></i></li>");
			$("#"+id).data("json",item);
		});
		
		$("#bookNavi li").on("click", function (event){
			$("#bookNavi div[data-role=treeview]").hide();
			$("#bookNavi li").removeClass("open");
			$(this).addClass("open");
			var cid = $(this).attr("id");
			var self = $(this);
			if(self.find("div[data-role=treeview]").length==0){
			   var childList = Enumerable.From(favoriteMenuList).Where(function(c){return c.P_ID == cid;}).ToArray();
			   var source =
	           {
	                dataType: "json",
	                dataFields: [
	                     { name: "C_ID", type: "string" },
	                     { name: "REAL_C_ID", type: "string" },
	                     { name: "C_NAME", type: "string" },
	                     { name: "C_LINK", type: "string" },
	                     { name: "SOURCE_OWNER", type: "string" },
	                     { name: "P_ID", type: "string" }
	                ],
	                hierarchy:
	                    {
		                	 keyDataField: { name: 'C_ID' },
		                     parentDataField: { name: 'P_ID' }
	                    },
	                localData: childList,
	                id: "C_ID"
	           };
	           var dataAdapter = new $.jqx.dataAdapter(source, {
	               loadComplete: function () {
	               }
	           }); 
	           dataAdapter.dataBind();
	           var json  = $(self).data("json");
	           if(json.MENU_TYPE=="1"){
		           if(childList.length!=0){		
		        	   if(self.find("div[data-role=treeview]").length==0){
		        		   self.append("<div data-role='treeview' class='menu'></div>");
		        	   }
			           var selfdiv = self.find("div[data-role=treeview]");
			           var records = dataAdapter.getRecordsHierarchy('C_ID','P_ID','items',[{ name: 'C_NAME', map: 'label'},{ name: 'C_LINK', map: 'link'},{ name: 'C_ID', map: 'value'}]);
			           selfdiv.jqxTree({width:210,source:records,theme:'ui-redmond' });
			            
			           selfdiv.on("click",{json:json,childList:childList} , function(event){
			        	    var curFav = $(this).jqxTree('getSelectedItem');
			        	    
			        	   	var cid = curFav.value;
							var dt = Enumerable.From(event.data.childList).Where(function(c){ return c.C_ID == cid;}).FirstOrDefault();
							event.stopPropagation(); 
							//addTab(dt.REAL_C_ID, dt.C_NAME,dt.C_LINK,null,'cog');
							var rcid = dt.REAL_C_ID!=null?dt.REAL_C_ID:dt.C_ID;
							var cogtype = undefined;
							if(rcid.toString().indexOf("i")!=-1) cogtype="cog";
							addTab(rcid, dt.C_NAME,dt.C_LINK,dt.MENU_TYPE,cogtype);
			           }); 
			            
		           }
	           }
	           else{
	        	   var owner = json.SOURCE_OWNER==null?"IPORTAL":json.SOURCE_OWNER;
        			var innerurl = "";
        			if(owner=="SOLUTION"){
        				innerurl = json.C_LINK;
        			}
        			else{
        				innerurl = uriContext+ json.C_LINK; 
        			} 
        			var cogtype = null;
        			if(json.REAL_C_ID!=null&&json.REAL_C_ID.toString().toUpperCase().indexOf("I")!=-1) cogtype = "cog";
        			if(innerurl.indexOf("views") != -1) cogtype = "tableau";
        			var real_cid = json.REAL_C_ID;
        			if(real_cid==null) {
        				real_cid = json.C_ID;
        			}
        			
        			addTab(real_cid, json.C_NAME,innerurl,owner,cogtype);  
	        	    //addTab(json.REAL_C_ID, json.C_NAME,innerurl,owner,cogtype);  
	           }
			}
			else{
				self.find("div[data-role=treeview]").show();
			}
		});*/
	}  
	
	
	//메뉴 ID,제목,열릴 링크 순으로 인자 나열됨.
	function addTab(cid, title, clink, udc1, udc2, udc3) {
		
		var cogcid = "";
		if(clink!=null&&clink.indexOf("i")==0) {
			cogcid = cid;
			udc2="cog";cid=clink; 
		} 
		
		var curidx = $("#content").find("li[data-cid="+cid+"]").length; 
		
		if (curidx != 0) {
			tabclick(cid);
			return; 
		} 
		var url = "";
		var curitem = Enumerable.From(curLeftMenu).Where(function(c) { 
			return c.C_ID == cid;
		}).FirstOrDefault();//.cType != null
		if(udc2 == "cog"){
			url = uriContext + "/home/loader/get?type=cog&cid="+cid+"&storeId=" + cid + "&menuNm=" + encodeURI(title) +"&pType=" + udc1+"&gubn=cog"+"&winType="+udc3+"&cogcid="+cogcid;
		}else if(udc2 == "tableau"){
			var href = clink.substring(clink.indexOf("/views"));
			$i.post( clink.substring(0,clink.indexOf("/views"))+"/trusted", { username:$iv.userinfo.userid()})
			  .done(function( data ) {
				url = uriContext + "/home/loader/get?cid=" + cid + "&src=" + encodeURIComponent(clink.substring(0,clink.indexOf("/views"))+"/trusted/"+data+href) + "&menuNm=" + encodeURI(encodeURIComponent(title)) +"&title="+encodeURIComponent(title) + "&pType=" + udc1+"&gubn=nomal"+"&winType="+udc3+"&cogcid="+cogcid;
			  });
		}else{
			if (curLeftMenu != null&&curitem!=null&&curitem.cType!=null	){
				url = uriContext + "/home/loader/get?type=cog&cid="+cid+"&storeId=" + cid + "&menuNm=" + encodeURI(title) +"&pType=" + udc1+"&gubn=cog"+"&winType="+udc3+"&cogcid="+cogcid;
			}
			else
//	 			alert("@@CID = " + cid + "###TITLE = " + title + "%%%%TYPE=" + udc1);
				url = uriContext + "/home/loader/get?cid=" + cid + "&src=" + $i.cipher.base64.encrypt(encodeURIComponent(clink)) + "&menuNm=" + encodeURI(encodeURIComponent(title)) +"&title="+encodeURIComponent(title) + "&pType=" + udc1+"&gubn=nomal"+"&winType="+udc3+"&cogcid="+cogcid;
		}
		
		if(biafolder.indexOf(curTop) > -1&&isfavorite==false){ 
			newWinPopUp(url,"BIA",1280,1024);
			
			return;
		}
		 
		 
		var tabidx = cid;//$iv.guid(); 
		var element = "<div style='width:100%;height:calc(100% - 36px);overflow-x:hidden;overflow-y:hidden;' ><iframe id='tabframe"
				+ tabidx
				+ "' src='about:blank' data-role='tabcontentsifm'  tabindex='-1' frameborder='0' style='border-width:0;width:100%;height:100%;overflow-x:hidden;overflow-y:hidden;'></iframe></div>";

		var length =  $("#content .Tab_Area li").length;
		var maxcnt = parseInt((maxTabCnt== "" ? "100"
				: maxTabCnt)) + 1;
		if (length >= maxcnt+2) { 
			$i.dialog.error("탭 설정","최대 창 개수(" + (maxcnt - 1) + ")를 초과 할 수 없습니다.")
			return;
		}
 
		jQueryAddTab(cid,title,element);
 
		$("#tabframe" + tabidx).attr("src", url);
		$("#content").on('tabclick', function (event){ 
			resetSessionTimer(); 
		});  
	}
	function jQueryAddTab(cid, title, content) {
		var tabs = $("#content");
		var idx = $("#content .Tab_Area li").length+1;
		var tabTemplate = "<li data-cid='"+cid+"'><a style='float:left;'href='#{href}'>#{label}</a> <i class='ui-icon ui-icon-close fa_tab fa-remove' role='presentation'></i></li>",
	        label = title || "Tab " + idx,
	        id = "content-" + cid, 
	        li = $( tabTemplate.replace( /#\{href\}/g, "#" + id ).replace( /#\{label\}/g, label ) ),
	        tabContentHtml = content || "Tab " + idx + " content.";
	    
	      tabs.find( ".ui-tabs-nav" ).append( li );
	      tabs.append( "<div id='" + id + "' style='height:100%;'><p>" + tabContentHtml + "</p></div>" );
	      //tabs.append( tabContentHtml ); 
	      tabs.tabs( "refresh" );
	      tabs.find( ".ui-tabs-nav" ).find("[href=#"+id+"]").trigger("click");
	      
	      activeTab(cid);
	      //tabs.tabs("load",idx);
	      
	}
	
	//cid 탭을 찾아 active
	function activeTab(cid){
		if($("li[data-cid='"+cid+"']:visible").length==0){
			if($("#next_button:visible").length!=0){
				$("#next_button:visible a").trigger("click");
			}
			else{
				$("#prev_button:visible a").trigger("click");
			}    
	    } 
	}
	
	function tabclick(cid){
		var tabs = $("#content");
		tabs.find("li[data-cid="+cid+"]").find("a").trigger("click");
		activeTab(cid); 
	}
	var popupflag = false;
 
	//팝업으로 열기
	function newWin(cid, url, type, name, winType) {
 
			
		if(type == "bookType"){
// 			url = url;
			url = '/home/loader/get?winType=' + winType + '&type=' + type + '&src='
			+ url + '&menuNm=' + name + "&cid=" + cid, "";	
		}else if(type == "cogPopup"){
			url = uriContext + "/home/loader/get?type=cog&storeId=" + cid
			+ "&menuNm=" + encodeURI(encodeURIComponent(name)) + "&gubn=cogPopup"+"&winType="+winType;
		}
		else{
			url = uriContext + "/home/loader/get?type=cog&storeId=" + cid
			+ "&menuNm=" + encodeURI(encodeURIComponent(name)) + "&gubn=cog"+"&winType="+winType;	
		}
		
		
		var curitem = Enumerable.From(curLeftMenu).Where(function(c) {
			return c.C_ID == cid;
		}).FirstOrDefault();//.cType != null
		
		if (curLeftMenu != null&&curitem!=null&&curitem.cType!=null	){
			if(curitem.cIcon.indexOf("_a") != -1){
				url = uriContext + "/home/loader/get?type=cog&storeId=" + cid + "&menuNm=" + encodeURI(encodeURIComponent(name) + "&gubn=cogPopup"+"&winType="+winType);	
			}else if(curitem.cIcon.indexOf("_q") != -1){
				url = uriContext + "/home/loader/get?type=cog&storeId=" + cid + "&menuNm=" + encodeURI(encodeURIComponent(name) + "&gubn=cogPopup"+"&winType="+winType);
			}else{
				url = uriContext + "/home/loader/get?type=cog&storeId=" + cid + "&menuNm=" + encodeURI(encodeURIComponent(name) + "&gubn=cog"+"&winType="+winType);
			}
			 
		}  
		
		
		var nWin = window.open(	url);//,'',	"width="+ ($(window).width() - 5)+ ",height="+ ($(window).height() )+ ", directories=no,toolbar=no,scrollbars=0,status=yes,resizable=yes");

		try{
			nWin.focus();
		}catch(e){
			alert("팝업 차단을 해제해주세요.");
		}
	}
	function makeBookmark(){
		bookReset();
		$i.post("../../home/layout/getBookList", {}).done(function(data){
			makeLeftFavoriteMenu(data.returnArray);
		}); 
	}
	function bookReset(){
		$("#bookCid").val('');
		$("#nameInput").val('');
		$("#locationInput").html('');;
		$("#realCid").val('');
		$("#sortOrder").val('');
		$("#bookPid").val('');
		$("#dragLabel").val('');
	}
	function deleteBook(cid, isfolder){
		 
			
	}
	  
	function isRegistFavoriteMenu(storeid){
		var cnt = Enumerable.From(favoriteMenuList).Where(function(c) {
			return c.REAL_C_ID == storeid;
		}).Count();
		return cnt!=0;
	} 
	function deleteFavoriteMenu(cid, isfolder, event){
		//if(event!=null) event.stopPropagation();
		if(isfolder){
			$i.dialog.confirm("즐겨찾기", "폴더 안의 즐겨찾기도 함께 삭제됩니다",function(){
				$i.post("../../home/layout/deleteBookUserData", {cID:cid}).done(function(data){
					if(data.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", data.returnMessage);
					}else{
						$i.dialog.alert("SYSTEM", data.returnMessage, function(){
							makeBookmark();
						});
					}
				});
			});
		}else{
			$i.dialog.confirm("즐겨찾기", "즐겨찾기가 삭제됩니다.",function(){
				$i.post("../../home/layout/deleteBookUserData", {cID:cid}).done(function(data){
					if(data.returnCode == "EXCEPTION"){
						$i.dialog.error("SYSTEM", data.returnMessage);
					}else{
						$i.dialog.alert("SYSTEM", data.returnMessage, function(){
							makeBookmark();
						});
					}
				});
			});
		} 
	}
	function addFavoriteMenu(cname,callback){
		
		var menuList = Enumerable.From(favoriteMenuList).Where(function(c) {
			return c.MENU_TYPE == "1";
		}).ToArray();
		var selecthtml = "<select id='fList' name='fList' style='height:20px;width:250px;padding-top:3px;'>";
		selecthtml+="<option value=5999>=폴더선택=</option>";
		$.each(menuList,function(idx,item){
			selecthtml+="<option value='"+item.C_ID+"'>"+item.C_NAME+"</option>";
		});
		selecthtml+="</select>";
		
		var gomyhtml="<table style='margin-top:5px;margin-left:10px;width:80%;float:right;border:0px;border-style:solid;border-color:gray;' cellpadding=0 cellspacing=0>"+
		"<tr style='height:25px;margin:2px;'>"+
		"<th style='width:100px;'>폴더</th><td style='padding:5px;'>"+selecthtml+"</td>"+ 
		"</tr>"+
		"<tr style='height:25px;margin:2px;'>"+
		"<th>메뉴명</th><td style = 'color:black;padding:5px;'>"+cname+"</td>"+
		"</tr>"+
		"</table>";
		$i.dialog.confirm("즐겨찾기",gomyhtml,function(data){
			callback(data);
		});
	} 
	function addFolder(){ 
		var popid = 'booklist';
		if($("#"+popid).length!=0) $("#"+popid).remove(); 
		
		var popup = "<div id='"+popid+"' style='position:absolute;z-index:999999;'>"+
			"<div>즐겨찾기관리</div>"+
			"<div style='padding:0;width:570px !important;height:590px !important;'>"+ 
				"<iframe style='width:570px !important;height:544px !important;' frameborder='0' src='../../home/layout/manageBookList'></iframe>"+
			"</div>"+
		"</div>";
		$(document).find("body").append(popup);
		 
		var offset = $("body").offset();
		var size = ($("body").width()-570)/2; 
		
		$("#"+popid).jqxWindow({
			
			theme:'Lotte-depart',
            position: { x: offset.left + size, y: offset.top + 150} ,
            maxHeight: 590, maxWidth:  570, minHeight:590, minWidth:  570, height: 590, width: 570,
            resizable: true, isModal: true, modalOpacity: 0.7,
            initContent: function (a,b,c,d,e) {
            	$("#"+popid).find("iframe").css({width:"100%",height:"100%"}); 

            }
        });
		 
		$("#"+popid).find("iframe").css({width:"100%",height:"100%"});
		
		/*
		$i.dialog.confirm("즐겨찾기","<table style='margin-top:5px;float:right;'><tr><th style='color:black;width:100px;' align='center'>폴더명</th><td><input type='text' name='folderName' style='width:250px;height:20px;padding-top:3px;'/></td></tr></table>",function(data){
			var cname = $("[name='folderName']").val();
			$i.post("../../home/layout/insertMyfolderNoCog", {cName : cname}).done(function(res){
				if(res.returnCode == "EXCEPTION"){
					$i.dialog.error("SYSTEM", res.returnMessage);
				}else{
					$i.dialog.alert("SYSTEM", res.returnMessage, function(){
						makeBookmark();
						bookReset();
					});
				}
			});
		});*/

	}   
	function closeAllTabs() {
		$i.dialog.confirm("확인","전체 화면을 닫으시겠습니까?",function(data){
			$("#content").find("i.ui-icon-close").trigger("click");
			//toggleLeftTab(true); 
		}); 
	}  
	function openSettingPage() {
		window.open("${WEB.ROOT}/popup/portalSetting", "_blank",
				"height=500,width=700,scrollbars=yes");
	}

	//logout (코그너스 로그아웃)
	function logout() {
		var msg = ""; 
		$i.dialog.confirm("SYSTEM","로그아웃 하시겠습니까?",function(){
			var btn = $("#btnLogOut");
			var path = "../../login/action/cogLogout";
			if(btn.length!=0&&btn.data("path")!=null&&btn.data("path").trim()!=""){
				path = btn.data("path"); 
			}
			location.replace(path);
		});
	}
	function changePwd() {
		//var tabidx = cid;//$iv.guid(); 
		var url = '../../home/pwd/changePassword';
		var title = '비밀번호변경'
		var tabidx = '999998';
		
		addTab(tabidx, title,url);
	}

	function renderTreeNode(treeid, element, pmenu) {

		var icon = "";
		var afterLabel;

		if (element.hasItems) {
			//have a child
			if (element.isExpanded != null && element.isExpanded) {
				icon = 'folderopenIcon.png';
			} else
				icon = 'folderIcon.png';
			afterLabel = "";//자식노드 숫자"<span style='color: Blue;' data-role=='treefoldersize'> (" + $(element.subtreeElement).children().length + ")</span>";
		} else {
			//no have a child
			icon = 'pageIcon.png';
			afterLabel = "";//'&nbsp;<img onclick="setPopup(true);" src="${WWW.CSS}/images/img_icons/popupIcon.png" alt="팝업창" title="새창보기">';
		}

		//topmenu ctype이 널이 아니거나 topmenu가 없을 경우..= 코그너스 메뉴이거나 코그너스 탐색기능을 이용한 경우.
		if ((pmenu != null && pmenu.cType != null) || pmenu == null) {
			var menudata = Enumerable.From(curLeftMenu).Where(function(c) {
				return c.C_ID == element.id;
			}).FirstOrDefault(); 
			if(menudata.cIcon.indexOf("folder")!=-1) afterLabel = ""; 
			var iconurl = '${WEB.IMG}' + "/dTree/"
					+ ((menudata != null) ? menudata.cIcon : "page.gif");
			if (menudata.cIcon != "loading.gif")
				label = "<img class='menutypeicon' style='float: left; margin-right: 5px;' src='"+iconurl + "'/><span item-title='true' >"
						+ (element.originalTitle == null ? element.label
								: element.originalTitle)
						+ "</span>"
						+ afterLabel;
			else
				label = "<span item-title='true' >"
						+ (element.originalTitle == null ? element.label
								: element.originalTitle) + "</span>";
		} else {
			if (icon != "loading.gif")
				label = "<img class='menutypeicon' style='float: left; margin-right: 5px;' src='"+$iv.envinfo.css+"/images/img_icons/" + icon + "'/><span item-title='true'  >"
						+ (element.originalTitle == null ? element.label
								: element.originalTitle)
						+ "</span>"
						+ afterLabel;
			else
				label = "<span item-title='true'  >"
						+ (element.originalTitle == null ? element.label
								: element.originalTitle) + "</span>";
		}
		$("#" + treeid).jqxTree('updateItem', element, {
			label : label
		});

	}
	function setPopup(val) {
		popupflag = val;
	}
 
	var tmpArr = new Array();