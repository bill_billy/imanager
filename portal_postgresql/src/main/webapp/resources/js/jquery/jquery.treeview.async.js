/*
 * Async Treeview 0.1 - Lazy-loading extension for Treeview
 * 
 * http://bassistance.de/jquery-plugins/jquery-plugin-treeview/
 *
 * Copyright (c) 2007 Jörn Zaefferer
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Revision: $Id$
 *
 */

;(function($) {

function load(settings, root, child, container) {
	function createNode(parent) {
		var current;
		if(this.reportType == "folder"){//set newWin
			current = $("<li/>").attr("id", this.id || "").html("<span style=\"cursor:pointer\">&nbsp;" + this.text + "</span>").appendTo(parent);
		}else{
			current = $("<li/>").attr("id", this.id || "").html("<span style=\"cursor:pointer;padding-top:3px\">&nbsp;<a onclick=\"javascript:addTab( '"+this.id+"', '"+this.text+"', '', '', '', 'cog')\">" + this.text + "</a></span>").appendTo(parent);
		}
		current.children("span").addClass(this.reportType);
		
		if (this.classes) {
			current.children("span").addClass(this.classes);
		}
		if (this.expanded) {
			current.addClass("open");
		}
		if (this.hasChildren || this.children && this.children.length) {
			var branch = $("<ul/>").appendTo(current);
			if (this.hasChildren) {
				
				current.addClass("hasChildren");
//				createNode.call({
//					classes: "placeholder",
//					text: "&nbsp;",
//					children:[]
//				}, branch);
			}
			if (this.children && this.children.length) {
				$.each(this.children, createNode, [branch]);
			}
		}
		if(this.reportType != "folder"){//set newWin
			current.children().append("<img src=\"resource/img/dTree/ico_newwin.gif\" onclick=\"newWinReport('" + this.id + "', '" + this.text + "', 'newWin');\" style=\"margin-left:5px;cursor:pointer;vertical-align:middle\" alt=\"새창열기\" />");
		}
		
			/*
			 * layout01
			if(Number($(".snb_menu_pad").css("height").replace("px", "")) > Number($("#snb_menu").css("height").replace("px", "")))
	            $("#snb_menu").css("overflow-y", "scroll");
			else
				$("#snb_menu").css("overflow-y", "hidden");
			
			if(current.css("width") == "194px")
	            $("#snb_menu").css("overflow-x", "scroll");
		    */		
	}
	
	if(root != "source") {
		dwr.engine.setAsync(false);
		menuServiceDwr.getMenuByJson(root, $("#" + root).text(), function(res){
			settings = res;
		});
	}
	//	$.ajax($.extend(true, {
//		url: settings.url,
//		dataType: "json",
//		data: {
//			root: root
//		},
//		success: function(response) {
	
			child.empty();
//			$.each(response, createNode, [child]);
			$.each(settings, createNode, [child]);
	        $(container).treeview({add: child});

//	    }
//	}, settings.ajax));
}

var proxied = $.fn.treeview;
$.fn.treeview = function(settings) {
//	if (!settings.url) {
//		return proxied.apply(this, arguments);
//	}
	var container = this;
	if (!container.children().size()){
		load(settings, "source", this, container);
	}
	var userToggle = settings.toggle;
	return proxied.call(this, $.extend({}, settings, {
		collapsed: true,
		toggle: function() {
			var $this = $(this);
			if ($this.hasClass("hasChildren")) {
				var childList = $this.removeClass("hasChildren").find("ul");
				load(settings, this.id, childList, container);
			}
			if (userToggle) {
				userToggle.apply(this, arguments);
			}
		}
		
	}));
};

})(jQuery);