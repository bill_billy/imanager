![iPlanBiz][iplanbiz-logo]

프로그램 사용권 명시
------

i-Portal is a portal system for BI.
i-Portal was developed using several open source programs and application programs. Below we list the programs used for development by license.

# Open Source

## [Apache License Version 2.0]
- Sheetjs
## [LGPL v2]
- SmartEditor2
## [MIT]
- jQuery
- c3
- clipboard.js
- dtree
- Fixed-Header-Table-master
- imcore
- jqcloud 
- Vue
- showdown
## PUBLIC DOMAIN
- JSON-js
## [BSD 2-Clause License]
- liquidFillGauge
## [BSD 3-Clause License]
- d3
## [Microsoft Public License]
- linq

# Commercial license
- JQWidgets (Enterprise License)


[iplanbiz-logo]:http://www.iplanbiz.co.kr/main_images/iplanbiz_logo.png
[Apache License Version 2.0]:https://opensource.org/licenses/Apache-2.0
[LGPL v2]: https://opensource.org/licenses/lgpl-2.1.php
[BSD 2-Clause License]:https://opensource.org/licenses/BSD-2-Clause
[BSD 3-Clause License]:https://opensource.org/licenses/BSD-3-Clause
[MIT]:https://opensource.org/licenses/MIT
[Microsoft Public License]:https://opensource.org/licenses/ms-pl.html