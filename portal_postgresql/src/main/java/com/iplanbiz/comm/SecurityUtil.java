/**
 * Copyright(c) 2012 IPLANBIZ All Rights Reserved
 */
package com.iplanbiz.comm;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author : woon sik Shin(sky3473@iplanbiz.co.kr or gmail.com)
 * @date   : 2014. 4. 3.
 * @desc   : 
 */
public class SecurityUtil {
	public static String CSRFTOKENNAME="CSRFTOKENNAME";
	private static Logger logger = LoggerFactory.getLogger(SecurityUtil.class);
	private static Pattern pattern1 = Pattern.compile("\'|\"|<script>|</script>|<object>|</object>", Pattern.CASE_INSENSITIVE);
	//단순 표기용 라이센스 만기일.
	public static String expireDate = "";
	
	/**
	* SQL Injection 을 막기위해 알파벳과 숫자를 제외한 나머지 문자들과([^\\p{Alnum}])과
	* SQL 예약어들을 ""(Empty String)로 치환하는 역할을 수행
	* @param str : 사용자 입력값
	* @param length : 체크할 문자열 길이
	* @return : SQL Injection 체크가 완료된 안전한 문자열
	*/
	public static String makeSecureString(String str, int length){
	
		Pattern unsecuredCharPattern;
		String UNSECURED_CHAR_REGULAR_EXPRESSION = "[^\\p{Alnum}]|select|delete|update|insert|create|alter|drop";
		//[^\\p{Alnum}] => 알파벳과 숫자를 제외한 나머지 문자들을 의미
		
		unsecuredCharPattern = Pattern.compile(UNSECURED_CHAR_REGULAR_EXPRESSION, Pattern.CASE_INSENSITIVE);
		
		String secureStr = str.substring(0, length);
		Matcher matcher = unsecuredCharPattern.matcher(secureStr);
		return matcher.replaceAll("");
	}
	
	/**
	* XSS(Cross Site Scripting)공격을 막기위해 JavaScript 및 html과 관련된 특정 문자를 escape한다.
	* 예) "&"=>"&amp;", "<"=>"&lt;", ">"=>"&gt;", """=>"&quot;", "\'"=>"&#039;", "&"=>"&amp;" 
	* @param str : 사용자자 입력한 문자열
	*/
	public static String escape(String str) {
	
		if ( str == null ){
			return null;
		}
		
		StringBuffer escapedStr = new StringBuffer();
		char[] ch = str.toCharArray();
		int charSize = ch.length;
		
		for ( int i=0; i < charSize; i++) {
			if ( ch[i] == '&' )
				escapedStr.append("&amp;");
			else if ( ch[i] == '<' )
				escapedStr.append("&lt;");
			else if ( ch[i] == '>' )
				escapedStr.append("&gt;");
			else if ( ch[i] == '"' )
				escapedStr.append("&quot;");
			else if ( ch[i] == '\'')
				escapedStr.append("&#39;");
			else
				escapedStr.append(ch[i]);
		}
		
		return escapedStr.toString();
	}
	
	/**
	* escape와 반대의 기능을 한다.
	* 예) "&"<="&amp;", "<"<="&lt;", ">"<="&gt;", """<="&quot;", "\'"<="&#039;", "&"<="&amp;" 
	* @param str : DB에서 조회된 문자열(화면에 전달하기 전 WAS에서 필터링해야 하는 문자열) 
	*/
	public static String unEscape(String str) {
	
		if ( str == null ){
			return null;
		}
		
		str = str.replace("&amp;",  "&");
		str = str.replace("&lt;",   "<");
		str = str.replace("&gt;",   ">");
		str = str.replace("&quot;", "\"");
		str = str.replace("&#39;", "'");
		str = str.replace("&#039;", "'");
		
		return str;
	}
	
	/**
	* Quotation 문자를 escape한다.
	* 예) """ => "&quot;", "'" => "&#039;" 
	* @param str : 사용자 입력값
	*/
	public static String escapeQuot(String str) {
	
		if ( str == null ){
			return null;
		}
		
		StringBuffer escapedStr = new StringBuffer();
		char[] ch = str.toCharArray();
		int charSize = ch.length;
		
		for ( int i=0; i < charSize; i++) {
			if ( ch[i] == '"' )
				escapedStr.append("&quot;");
			else if ( ch[i] == '\'')
				escapedStr.append("&#039;");
			else
				escapedStr.append(ch[i]);
		}
		
		return escapedStr.toString();
	}
	
	/**
	* escapeQuot와 반대의 기능을 한다.
	* 예) """ <= "&quot;", "'" <= "&#039;" 
	* @param str : DB에서 조회된 문자열(화면에 전달하기 전 WAS에서 필터링해야 하는 문자열) 
	*/
	public static String unEscapeQuot(String str) {
	
		if ( str == null ){
			return null;
		}
		
		str = str.replace("&quot;", "\"");
		str = str.replace("&#039;", "'");
		
		return str;
	}
	
	/**
	* 파일 다운로드 요청 파라미터에서 상위 경로를 참조하는지 여부를 확인한다.
	* @param fileStr 
	 */
	public static String checkFileParam(String fileStr) throws Exception {
	
	if (fileStr == null) {
		logger.error("SecurityUtil : 파일경로 값이 없습니다.");
		throw new Exception("SecurityUtil : 파일경로 값이 없습니다.");
	}
	
	if ( (fileStr.indexOf("../") != -1) || (fileStr.indexOf(".\\./") != -1) || (fileStr.indexOf("..\\") != -1) ) {
		logger.error("SecurityUtil : 파일경로에 사용 불가능한 문자가 있습니다. : {}", Utils.getString(fileStr, "").replaceAll("[\r\n]",""));
		throw new Exception("SecurityUtil : 파일경로에 사용 불가능한 문자가 있습니다.");
	}
	
	return fileStr;
	}
	
	/**
	* 보안에 문제를 발생시키는 문자열을 대체시킨다.
	* 
	* <script></script> => 제거
	* <object></object> => 제거
	* ' => ''
	* " => ""
	* 
	* @param str : 사용자 입력값(게시판의 게시글 내용 포함) 
	* @return : 안전하게 변환된 문자열
	*/
	public static String replaceForSecurity(String str){
		if ( str == null ){
			return null;
		}
		
		Matcher m = pattern1.matcher(str);
		StringBuffer sb = new StringBuffer();
		String replaceStr = "";
		while (m.find()) {
		
			if(m.group().toLowerCase().equals("\'")) replaceStr = "\'\'";
			//else if(m.group().toLowerCase().equals("\"")) replaceStr = "\"\"";
			else replaceStr = "";
				logger.error("SecurityUtil : Invalid String [{}] was replaced " + "[{}]", Utils.getString(m.group(), "").replaceAll("[\r\n]",""), Utils.getString(replaceStr, "").replaceAll("[\r\n]",""));
			
			m.appendReplacement(sb, replaceStr);
		}
		
		m.appendTail(sb);
		
		return sb.toString();
	}
	public static String getCSRFToken(){
		return java.util.UUID.randomUUID().toString();
	}
} 

