/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.comm;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

/**
 * @author : woon sik Shin(sky3473@iplanbiz.co.kr or gmail.com)
 * @date   : 2012. 6. 14.
 * @desc   : 
 */
public class Utils {
	
	public static String clobToString(oracle.sql.CLOB clob){
		 
		StringBuffer buffer = new StringBuffer();
		
		int y;           
		char ac[] = new char[4096];           
		java.io.Reader reader;
		if(clob!=null){
			try {
				reader = clob.getCharacterStream();
				while ((y = reader.read(ac, 0, 4096)) != -1) {
					 String buff = new String(ac,0,y);
					 buffer.append(buff);
					}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}           
			
		}
		return buffer.toString();
		
	}
	/**
	 * 문자열을 숫자로 바꿔주는 함수. 문자열이 null 또는 공백일 경우 default 값을 리턴 한다.
	 * @param sValue 문자열
	 * @param nDefaultValue 기본 값
	 * @return 수
	 */
	public static int getValue(String sValue, int nDefaultValue) {

		try {
			if (sValue != null && sValue.trim().length() != 0)
				return Integer.parseInt(sValue);
		}
		catch (Exception e) {
		}
		
		return nDefaultValue;
	}

	public static long getValue(String sValue, long nDefaultValue) {

		try {
			if (sValue != null && sValue.trim().length() != 0)
				return Long.parseLong(sValue);
		}
		catch (Exception e) {
		}
		
		return nDefaultValue;
	}
	
	public static int getValue(Object oValue, int nDefaultValue){
		try{
			if(oValue != null)
				return Integer.parseInt((String) oValue);
		}catch(Exception e){
			e.printStackTrace();
		}
		return nDefaultValue;
	}

	public static String getString(String sValue, String sDefaultValue) {

		if (sValue == null || sValue.trim().length() == 0)
			return sDefaultValue;
		if ("null".equals(sValue.toLowerCase())){
			return sDefaultValue;
		}
		return sValue;
	}

	public static String getString(String sValue, String sDefaultValue, int len) {
		return Utils.getString(sValue, sDefaultValue, len, false);
	}

	public static String getString(String sValue, String sDefaultValue, int len, boolean flag) {

		if (sValue == null || sValue.trim().length() == 0)
			return sDefaultValue;
		
		if (sValue.length() > len)
			return sValue.substring(0, len) + (flag ? " ..." : "");

		return sValue;
	}
	
	/**
	 * 특정 길이만큼 문자열을 잘라 낸다.flag 값에 따라 문자열의 끝에 "..."을 추가 한다.
	 * @param sValue
	 * @param sDefaultValue
	 * @param len
	 * @param flag
	 * @return
	 */
	public static String getStringEx(String sValue, String sDefaultValue, int len, boolean flag) {

		if (sValue == null || sValue.trim().length() == 0)
			return sDefaultValue;

		int nCount = 0;
		for (int i = 0; i < sValue.length(); i++) {
			if (sValue.charAt(i) > 255)
				nCount += 2;
			
			if (nCount >= len) {
				sValue = sValue.substring(0, i);
				break;
			}
		}
		
		return sValue + (flag ? " ..." : "");
	}
	
	public static boolean getBoolean(String sValue, boolean bDefaultValue) {

		try {
			if (sValue != null && sValue.trim().length() != 0)
				return Boolean.parseBoolean(sValue);
		}
		catch (Exception e) {
		}
		
		return bDefaultValue;
	}

	/**
	 * comma(,)로 구분되는 숫자로 이루어진 문자열을 숫자 배열로 바꿔주는 함수
	 * 숫자로 변환하지 못할 경우 그곳에서 변환을 멈추고 변환 된 값들만 배열로 리턴 한다.
	 * 예) "1,2,3,4" => {1,2,3,4}
	 * @param sValues
	 * @return
	 */
	public static ArrayList<Integer> getIntArray(String sValues) {
		
		ArrayList<Integer> intArray = new ArrayList<Integer>();
		if (sValues != null && sValues.trim().length() != 0) {
			String [] s = sValues.split(",");

			try {
				for (int i = 0; i < s.length; i++) {
					intArray.add((Integer.valueOf(s[i])));
				}
			}
			catch (Exception e) {
			}
		}
		
		return intArray;
	}
	
	/**
	 * 문자열 숫자에 3개 마다 콤마(,)를 추가 한다.
	 * @param s
	 * @return
	 */
	public static String addComma(String s) {
		StringBuffer sb = new StringBuffer(s);
		sb.reverse();

		StringBuffer rb = new StringBuffer();
		for (int i = 0; i < sb.length(); i++) {
			rb.append(sb.charAt(i));
			if ((i+1) % 3 == 0 && i != sb.length() - 1)
				rb.append(',');
		}
		rb.reverse();
		return rb.toString();
	}

	/**
	 * 숫자에 3개 마다 콤마(,)를 추가 한다.
	 * @param value
	 * @return
	 */
	public static String addComma(long value) {
		java.text.DecimalFormat df = new java.text.DecimalFormat("###,###");
		return df.format(value);		
		
		/*
		StringBuffer sb = new StringBuffer(String.valueOf(value));
		sb.reverse();

		StringBuffer rb = new StringBuffer();
		for (int i = 0; i < sb.length(); i++) {
			rb.append(sb.charAt(i));
			if ((i+1) % 3 == 0 && i != sb.length() - 1)
				rb.append(',');
		}
		rb.reverse();
		//System.out.println(rb.toString());
		return rb.toString();
		*/
	}
	
	/**
	 * 현재 시간을 YYYY/MM/DD 형식의 문자열로 변환 한다.
	 * @return
	 */
	public static String today(String str) {
		return String.format("%1$tY" + str + "%1$tm" + str + "%1$td", Calendar.getInstance());		
	}

	/**
	 * 시간을 YYYY/MM/DD 형식의 문자열로 변환하여 준다. 
	 * @param mtime milisecond
	 * @return String
	 */
	public static String date(long mtime) {
	    DateFormat df = new SimpleDateFormat("yyyy/MM/dd"); 
		return df.format(mtime);		
	}
	
	/**
	 * 시간을 지정한 패턴의 문자열로 치환해준다.
	 * @param format
	 * @param time millisecond
	 * @return
	 */
	public static String date(String format, long time) {
	    DateFormat df = new SimpleDateFormat(format); 
		return df.format(time);		
	}
	
	public static boolean isZero(String s) {
		boolean b = false;
		try {
			b = s == null || s.length() == 0 || Integer.parseInt(s) == 0;
		}
		catch (Exception e) {}
		
		return b;   
	}
	
	public static String arrayToString(String[] arr) {
		StringBuffer sb = new StringBuffer();
		for (String s : arr) {
			if (sb.length() > 0) {
				sb.append(",");
			}
			sb.append( s );
		}
		return sb.toString();
	}
	
	public static String getDecodeValue(String src){
		try {
			return URLDecoder.decode(src, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getEncodingValue(String src){
		try{
			return URLEncoder.encode(src, "UTF-8");
		}catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static String setGlue(String glue, ArrayList<String> arr){
		StringBuffer tmpString=new StringBuffer("");
		
		for(int i=0; i<arr.size();i++){
			if(i == 0){
				tmpString.append(arr.get(i));
			}else{
				tmpString.append(glue + arr.get(i));
			}
		}
		
		return tmpString.toString();
	}
	
	public static String setGlue(String glue, boolean addComma, String value){
		String tmpString = "";
			tmpString += glue+value+glue;
			if(addComma) tmpString += ", ";
		
		return tmpString;
	}
	
	public static String setGlue(String beginGlue, String glue, String endGlue, boolean addComma, String[] arrValue){
		StringBuffer result = new StringBuffer();
		//String tmpString = "";
			/**
			for(int i=0; i < arrValue.length; i++){
				if(i == 0) tmpString += beginGlue;
				
				tmpString += glue + arrValue[i] + glue;
				if(addComma && i != arrValue.length-1) tmpString += ", ";
				
				if(i == arrValue.length-1) tmpString +=endGlue;
			}
			**/
			for(int i=0; i < arrValue.length; i++){
				if(i == 0) result.append(beginGlue);
				
				result.append(glue);
				result.append(arrValue[i]);
				result.append(glue); 
				
				if(addComma && i != arrValue.length-1) result.append(", ");
				
				if(i == arrValue.length-1) result.append(endGlue); 
			}
		
		return result.toString();
	}
	
	/**
	 * String 배열에 해당문자가 존재하는지 여부
	 * @param arrayValue
	 * @param needle
	 * @return
	 */
	public static boolean inArray(String[] arrayValue, String needle) {
	    for(int i=0;i<arrayValue.length;i++) {
	        if(arrayValue[i].equals(needle)) {
	            return true;
	        }
	    }
	    return false;
	}
	
	/**
	 * 알파벳과 기타문자로 혼합된 문자를 알파벳만 추출하여 중복제거하고 정렬하여 리턴함
	 * @param source
	 * @return List<String>
	 */
	public static List<String> getAlphabet(String source, int op){
		
		String pattern  =  "";  
		ArrayList<String> arrList1 = new ArrayList<String>();
		
		   
		switch(op){
			case 1: pattern=  "^[a-zA-Z]$"; break;
		}
		
		
		for(int i=0; i<source.length(); i++){
			
			Pattern  p  =  Pattern.compile(pattern);
			Matcher  m  =  p.matcher(String.valueOf(source.charAt(i)));
			
			if(m.find()) arrList1.add(String.valueOf(source.charAt(i)));
			
		}
		
		HashSet<String> hashSet = new HashSet<String>(arrList1);
		ArrayList<String> arrList2 = new ArrayList<String>(hashSet) ; 
		Collections.sort(arrList2);
		
		return arrList2;
	}

	/**
	 * 특수문자를 간접표현식으로 변환
	 * @param content
	 * @return
	 */
	public static String htmlspecialchars(String content){
		
		StringBuffer sb = new StringBuffer();
		
		for(int i=0; i<content.length(); i++) {
			char c = content.charAt(i);
			
			switch (c) {
				case '<' : 
					sb.append("&lt;");
					break;
				case '>' : 
					sb.append("&gt;");
					break;
				case '&' :
					sb.append("&amp;");
					break;
				case '"' :
					sb.append("&quot;");
					break;
				case '\'' :
					sb.append("&apos;");
					break;
				default:
					sb.append(c);
			}
		}
		return sb.toString();
	}
	
	public static String getClientIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
     
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
     
        return ip;
    } 
	
	public static String replaceString(String sValue){
		return sValue.replace("'", "''");
	}
	 
	//배열을 문자열로 치환 glue를 넣어서
		public static String join(String glue, String array[], int length) {
			StringBuffer result = new StringBuffer("");

		    for (int i = 0; i < length; i++) {
		      result.append(array[i]);
		      if (i < array.length - 1) {
		    	  result.append(glue);
		    	 
		      }
		      if(i==array.length-1)  break;
		      
		    }
		    return result.toString();
		  }
}
