package com.iplanbiz.iportal.config;

import java.util.Iterator;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.iplanbiz.comm.Utils;
 
public class WebConfig {
    
	

	private static Logger logger = LoggerFactory.getLogger(WebConfig.class);
	private static Properties props = null;  
	private static String acctid="";
	    
	static{
		String propsResource = "";
			
	    props = new Properties();
	    	
		try{
			propsResource = "config/webConfig.properties";
		    props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(propsResource));
		
		    StringBuffer logStr = new StringBuffer();
		    Iterator iter = props.keySet().iterator();
		    logger.info("====>>> {} Open!!", propsResource);
		    	
			while(iter.hasNext()){
				Object key = iter.next();
			    Object val = props.getProperty(String.valueOf(key));
			    //logger.info("====>>> * {}  = {}", Utils.getString(String.valueOf(key), "").replaceAll("[\r\n]",""),Utils.getString(String.valueOf(val), "").replaceAll("[\r\n]",""));
			}
			    
			logger.debug("====>>> {} Close!!", propsResource);
		 }catch(Exception e){
			 logger.error("===>>>> Load {} Fail: {}", e);
		 }	
	}
	public static String getIDQUrl(){
		String idqurl = props.getProperty("system.idqUrl"); 
		return (idqurl==null)?"":idqurl;
	}
	public static String getSerialKey(){
		return props.getProperty("system.serialNo");
	}
	public static String getProperty(String property){
		return props.getProperty(property);
	}
	public static int getSystemRdbms(){
		return Integer.parseInt(props.getProperty("system.rdbms"));
	}
	public static String getAcctID(){
		return props.getProperty("system.acctID");
	}
 
	public static String getDefaultSaveDir(){
		return props.getProperty("system.upload.SaveDir");
	}
	public static String getDefaultServerRoot(){
		return props.getProperty("system.serverRootDIR");
	}
	public static String getMaxUploadSize(){
		return props.getProperty("system.upload.maxUploadSize");
	}
	public static String getSystemPortalUrl(){
		return props.getProperty("system.portalUrl");
	}
	public static String getDefaultWebDir(){
		return props.getProperty("default.uploadWebDir");
	}
	public static String getPortalPort(){
		return props.getProperty("system.portalPort");
	}
	public static long getDefaultMaxUpSize(){
		return Utils.getValue(props.getProperty("system.upload.maxUploadSize"), 0);
	}
	public static String getLogoutPath(){
		String logoutPath = props.getProperty("system.path.logout");
		if(logoutPath==null||logoutPath.trim().equals("")) {
			logoutPath = "/login/action/logout";			
		}
		return logoutPath;
	}
	public static int getLockedCount(){
		int returnValue = 0;
		String lockedCount = props.getProperty("system.locked.count");
		if(!lockedCount.equals("")){
			returnValue = Integer.parseInt(lockedCount);
		}
		return returnValue;
	}
	public static int getPasswordCycle(){
		return Utils.getValue(props.getProperty("system.password.cycle"), 3);//기본 3개월주기
	}
	public static String getAdminTableName(String tableName) {
		
		String schema_admin = props.getProperty("system.schema.admin"); 
		if(schema_admin!=null&&schema_admin.trim().equals("")==false) 
			schema_admin=schema_admin.trim()+".";
		else 
			schema_admin="";
		 
		return schema_admin+tableName;
	} 
	public static String getCustomTableName(String tableName) { 
		String schema_customer = props.getProperty("system.schema.customer");
 
		if(schema_customer!=null&&schema_customer.trim().equals("")==false)
			schema_customer=schema_customer.trim()+".";
		else
			schema_customer="";
		
		return schema_customer+tableName;
	}	
	public static String getSmtpHost(){
		return props.getProperty("smtp.host");
	}
	public static String getSmtpPort(){
		return props.getProperty("smtp.port");
	}
	public static String getSmtpAdminId(){
		return props.getProperty("smtp.admin.id");
	}
	public static String getSmtpAdminPwd(){
		return props.getProperty("smtp.admin.pwd");
	}
	public static String getSmtpAdminEmail(){
		return props.getProperty("smtp.admin.email");
	}
	
}