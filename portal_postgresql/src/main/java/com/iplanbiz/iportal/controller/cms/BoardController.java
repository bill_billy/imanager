package com.iplanbiz.iportal.controller.cms;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dto.Board;
import com.iplanbiz.iportal.dto.File;
import com.iplanbiz.iportal.service.cms.BoardService;
import com.iplanbiz.iportal.service.file.FileService;

@Controller
public class BoardController {

	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired BoardService boardService;
	@Autowired FileService fileService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	 @RequestMapping(value="/cms/board/portlet", method = RequestMethod.GET)
	    public ModelAndView portlet(@ModelAttribute Board board) {
	    	ModelAndView modelAndView = new ModelAndView();
	    	if(board.getBoardType()==null)
	    		board.setBoardType("1");
	    	board.setGubn("NULL");
	    	board.setStartDay1("0000-00-00");
	    	board.setEndDay1("9999-99-99");
	    	
			modelAndView.addObject("boardList",  boardService.getBoardList(board));
	    	modelAndView.setViewName("/cms/board/portlet");
	    	return modelAndView;
	    }
	
	@RequestMapping(value="/cms/board/list", method={RequestMethod.GET})
	public ModelAndView list () throws Exception {
		ModelAndView modelAndView = new ModelAndView("/cms/board/list");
		loginSessionInfoFactory.getObject();
		return modelAndView;
	}
	
	@RequestMapping(value="/cms/board/getBoardList", method={RequestMethod.POST})
	public void getBoardList ( HttpServletResponse response , HttpServletRequest request 
																   ,@RequestParam(required=false) String TABLE_NM
																   ,@RequestParam(required=false) String TITLE
																   ,@RequestParam(required=false) String CONTENT
																   ,@RequestParam(required=false) String NAME
																   ,@RequestParam(required=false) String GUBN
																   ,@RequestParam(required=false) String START_DAT
																   ,@RequestParam(required=false) String END_DAT) throws Exception {
		
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = boardService.getBoardList(TABLE_NM, TITLE,CONTENT,NAME,GUBN,START_DAT,END_DAT);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Scheduler Manager List Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/cms/board/form", method = RequestMethod.GET)         
    public String form(@RequestParam(required = false, value = "boardNo") String boardNo, 
    					@RequestParam(required = false) String boardType, 
    					@RequestParam(required = false) String pBoardno,
    					@RequestParam(required = false) String answerLevel, 		
    					String type, Model model) throws Exception {        

    	LoginSessionInfo loginInfo = loginSessionInfoFactory.getObject();
    	model.addAttribute("boardName", loginInfo.getUserName());
    	if (boardNo != null && boardNo.length() > 0) {	// 수정
        	Map<String,String> boardMap =  boardService.getBoardNew(new Board(boardType, Integer.parseInt(boardNo))).get(0);
        	
        	File file = new File();
        	file.setBoardNo(Integer.parseInt(boardMap.get("boardno")));
    		file.setTableNm(boardType);
    		
    		model.addAttribute("board", boardMap);
    		model.addAttribute("fileList", fileService.getFileList(file));
    	} else {	// 신규
    		model.addAttribute("board",boardService.getNewBoard(boardType, pBoardno, answerLevel)); 
    	}
    	model.addAttribute("type", type);
    	return "/cms/board/form";
    }
	@RequestMapping(value="/cms/board/save", method = RequestMethod.POST)
    public String save(Board board, String type, Model model) throws Exception {             
    	String result = "";
    	String returnValue = "";
    	if(board.getFile().length > 0){
    		for(CommonsMultipartFile cmfile : board.getFile()){
    			String FileFilter = "txt,zip,jpg,png,xls,xlsx,ppt,pptx,doc,docx,hwp,pdf";
    			String ext = (cmfile.getOriginalFilename().substring(cmfile.getOriginalFilename().lastIndexOf(".")+1, cmfile.getOriginalFilename().length())).toLowerCase();
    			if(FileFilter.indexOf(ext)!=-1){
    				returnValue = "Y";
    			}else{
    				returnValue = "N";
    			}
    		}
    	}else{
    		returnValue = "Y";
    	}
    	if ("M".equalsIgnoreCase(type)) {	// 수정
    		if(returnValue == "N"){
    			result = Utils.getString(WebConfig.getSystemPortalUrl() + ":" + WebConfig.getPortalPort()+"/accessRight", "");
    		}else{
    			boardService.modifyBoard(board);
        		
        		Map<String,String> boardMap = new HashMap<String,String>();
            	boardMap = boardService.getBoardNew(board).get(0);
//            	boardMap.put("CONTENT", boardMap.get("CONTENT").replaceAll(" ", "&nbsp;").replaceAll("\n", "<BR>").replaceAll("<", "&lt;").replaceAll(">", "&gt;"));
        		model.addAttribute("board", boardMap);  
        		
        		File file = new File();
            	file.setBoardNo(Integer.parseInt(boardMap.get("boardno")));
            	file.setTableNm(String.valueOf(board.getBoardType()));
        		model.addAttribute("fileList", fileService.getFileList(file));
        		
        		result = "redirect:/cms/board/get?boardNo=" + String.valueOf(board.getBoardNo()) + "&boardType=" + board.getBoardType();
        		
    		}
    	} else if ("I".equalsIgnoreCase(type)) {	// 신규                  
    		if(returnValue == "Y"){
    			boardService.createBoard(board);
        		result = "redirect:/cms/board/list?boardType=" + board.getBoardType();
    		}else{
    			result = Utils.getString(WebConfig.getSystemPortalUrl() + ":" + WebConfig.getPortalPort()+"/accessRight", "");
    		}
    	} 
    	return result;         
    }          
	
	
	@RequestMapping(value = "/cms/board/get", method = {RequestMethod.GET})         
    public ModelAndView get(@RequestParam(required = false, value = "boardNo") int boardNo ,
    		                @ModelAttribute Board board1,
    		                @RequestParam(required=false) String gubn
    		) throws Exception {
    	ModelAndView modelAndView = new ModelAndView();
    	Board board = new Board(board1.getBoardType(),  boardNo);
    	if(board1.getBoardType()!=null)
    		board.setBoardType(board1.getBoardType());
    	else 
    		board.setBoardType(String.valueOf(boardNo));
    	board.setTableNm(board1.getTableNm());
    	if("save".equals(Utils.getString(String.valueOf(board1.getAc()),""))){ //댓글저장
    		board1.setpLevel(0);
    		board1.setpStep(Integer.parseInt(String.valueOf(boardService.getPstepMax(board1).get(0).get("max"))));
    		boardService.insertReplyTxt(board1);
    	}else if("del".equals(Utils.getString(String.valueOf(board1.getAc()),""))){//댓글삭제
    		boardService.deleteReplyTxt(board1);
        }else {
    		boardService.modifyHitCount(board);	// 조회수 + 1
    	}
    	modelAndView.addObject("board", boardService.getBoardNew(board).get(0));
    	System.out.println(board);
    	modelAndView.addObject("borBu", boardService.getBoardBuser(board).get(0));
    	//modelAndView.addObject("boardNext", boardService.getBoardNext(board));
    	modelAndView.addObject("replyTxt", boardService.getReplyTxt(board));
    	
    	File file = new File();
    	file.setBoardNo(boardNo);
    	file.setTableNm(String.valueOf(board.getBoardType()));
		modelAndView.addObject("fileList", fileService.getFileList(file));
		
    	modelAndView.setViewName("/cms/board/get");
    	return modelAndView;
    }      
	@RequestMapping(value="/cms/board/download", method={RequestMethod.GET})
	public ModelAndView download(@RequestParam int fileId) throws SQLException{
		
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("downloadFile", fileService.getFile(fileId));
		modelAndView.setViewName("downloadView");
		
		return modelAndView;
	}
	
	 @RequestMapping(value="/cms/board/delete", method = RequestMethod.GET)        
	    public String delete(@RequestParam String boardType, int boardNo) throws Exception {
	    	Board board = new Board(boardType,  boardNo);
	    	boardService.removeBoard(board);
	    	//cms/boardService.removeBoardReplyTxt(board);
	    	return "redirect:/cms/board/list?boardType=" + boardType;
	    }
	 
	 @RequestMapping(value="/cms/board/deleteFile", method = RequestMethod.GET)
	    public String deleteFile(@RequestParam(required = false, value = "boardNo") String boardNo, 
	    						 @RequestParam(required = false) String boardType, 
	    						 @RequestParam(required = false) String pBoardno,
	    						 @RequestParam(required = false) String answerLevel,
	    						 int fileId, String type, Model model) throws Exception {
	    	logger.debug("boardNo {} fileId {}", Utils.getString(boardNo, "").replaceAll("[\r\n]",""), fileId);
	    	logger.debug("type {} model {}", Utils.getString(type, "").replaceAll("[\r\n]",""), Utils.getString(model.toString(), "").replaceAll("[\r\n]",""));
	    	fileService.removeFile(fileId);
	    	return form(boardNo, boardType, pBoardno, answerLevel, type, model);
	    }
}
