package com.iplanbiz.iportal.controller.cms;

import java.net.URLEncoder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dto.BoardV2;
import com.iplanbiz.iportal.service.cms.BoardV2Service;
import com.iplanbiz.iportal.service.file.FileService;

@Controller
public class BoardV2Controller {

	@Autowired
	BoardV2Service boardService;
	@Autowired
	FileService fileService;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/cms/boardv2/list", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView boardList () throws Exception {
		ModelAndView modelAndView = new ModelAndView("/cms/boardv2/list");
		
		return modelAndView;
	}
	@RequestMapping(value="/cms/boardv2/crud", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView boardCrud() throws Exception {
		ModelAndView modelAndView = new ModelAndView("/cms/boardv2/crud");
		
		return modelAndView;
	}
	@RequestMapping(value="/cms/boardv2/getBoardList", method={RequestMethod.POST, RequestMethod.GET})
	public void getBoardList ( HttpServletResponse response, HttpServletRequest request
			                        , @RequestParam(required=false) String boardType
			                        , @RequestParam(required=false) String startDat
			                        , @RequestParam(required=false) String endDat
			                        , @RequestParam(required=false) String titleName ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = boardService.getBoardList(boardType, startDat, endDat, titleName);
			msg.setSuccessMessage(returnValue);
		} catch ( Exception e ) {
			logger.error("ERROR > Get  Board List Data Error : {}", e.getMessage());
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/cms/boardv2/getBoardDetail", method={RequestMethod.POST, RequestMethod.GET})
	public void getBoardDetail ( HttpServletResponse response, HttpServletRequest request
			                          , @RequestParam(required=false) String boardType
			                          , @RequestParam(required=false) String boardNO ) throws Exception {
		JSONObject returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = boardService.getBoardDetail(boardType, boardNO);
			msg.setSuccessMessage(returnValue);
		} catch ( Exception e ) {
			logger.error("ERROR > Get  Board Detail Data Error : {} " ,e.getMessage());
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	} 
	@RequestMapping(value="/cms/boardv2/getBoardFileList", method={RequestMethod.POST, RequestMethod.GET})
	public void getBoardFileList ( HttpServletResponse response, HttpServletRequest request 
			                            , @RequestParam(required=false) String tableNM
			                            , @RequestParam(required=false) String boardNO ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = boardService.getBoardFileList(tableNM, boardNO);
			msg.setSuccessMessage(returnValue);
		} catch ( Exception e ) {
			logger.error("ERROR > Get  Board File List Data Error : {}", e.getMessage());
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/cms/boardv2/setBoardHitCount", method=RequestMethod.POST)
	public void setBoardHitCount ( HttpServletRequest request, HttpServletResponse response
			                            , @RequestParam(required=false) String boardNo
			                            , @RequestParam(required=false) String boardType ) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		try {
			boardService.setBoardHitCount(boardNo, boardType);
			msg.setSuccessText("성공");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Set  Board Hit Count Error : {}" ,e.getMessage());
		}
		msg.send(response);
		
	}
	@RequestMapping(value="/cms/boardv2/save", method={RequestMethod.POST})
	public String insertBoard ( @ModelAttribute BoardV2 board, Model model, @RequestParam(required=false) String saveGubn ) throws Exception {
		String result = "";
		String returnValue = "Y";
		AjaxMessage msg = new AjaxMessage();
		logger.info(" Notice Save Start");
		logger.info("Save GUBN = " + saveGubn);
		logger.info("Get FIle = " + board.getFile());
		if(board.getFile()!=null && board.getFile().length > 0) {
			for (CommonsMultipartFile cmfile : board.getFile()) {
//				String FileFilter = "txt,zip,jpg,png,xls,xlsx,ppt,pptx,doc,docx,hwp,pdf,mp4";
				String FileFilter = "xls,xlsx,ppt,pptx,doc,docx,hwp,pdf";
				String ext = (cmfile.getOriginalFilename().substring(cmfile.getOriginalFilename().lastIndexOf(".") + 1, cmfile.getOriginalFilename().length())).toLowerCase();
				if (FileFilter.indexOf(ext) != -1) {
					returnValue = "Y";
				} else {
					returnValue = "N";
				} 
			}
		} else {   
			returnValue = "Y";
		}
		if("N".equalsIgnoreCase(saveGubn)) {
			if(returnValue.equals("N")) {
				result = Utils.getString(WebConfig.getSystemPortalUrl() + ":" + WebConfig.getPortalPort() + "/accessRight","");
			} else {
				boardService.insertBoard(board);
				model.addAttribute("msg", URLEncoder.encode("저장되었습니다", "UTF-8"));
				result = "redirect:/cms/boardv2/list?boardType="+board.getBoardType();
			}
		}else if("U".equalsIgnoreCase(saveGubn)) {
			logger.info("Notice Udate Start");
			if(returnValue.equals("N")) {
				result = Utils.getString(WebConfig.getSystemPortalUrl() + ":" + WebConfig.getPortalPort() + "/accessRight","");
			} else {
				logger.info("Notice Udate  Query Start");
				model.addAttribute("msg", URLEncoder.encode("수정되었습니다.", "UTF-8"));
				boardService.updateBoard(board);
				result = "redirect:/cms/boardv2/list?boardType="+board.getBoardType();
			}
		}
//		msg.send(response);
		return result;
	}
	@RequestMapping(value="/cms/boardv2/remove", method=RequestMethod.POST)
	public String noticeDelete ( @RequestParam(required=false) String boardNO
			                     , @RequestParam(required=false) String boardType
			                     ,  Model model) throws Exception {
		String result = "";
		try {
			boardService.removeBoard(boardType, boardNO);
			model.addAttribute("msg", URLEncoder.encode("삭제되었습니다.", "UTF-8"));
			result = "redirect:/cms/boardv2/list?boardType="+boardType;
		} catch ( Exception e ) {
			result = Utils.getString(WebConfig.getSystemPortalUrl() + ":" + WebConfig.getPortalPort() + "/accessRight","");
			e.printStackTrace();
			logger.error("ERROR > Delete Notice Data Error : {} ", e.getMessage());
		}
		return result;
	}
	@RequestMapping(value="/cms/boardv2/deleteFile", method = RequestMethod.GET)
    public String deleteFile(@RequestParam(required = false, value = "boardNo") String boardNo, 
    						 @RequestParam(required = false) String boardType, 
    						 @RequestParam(required = false) String pBoardno,
    						 @RequestParam(required = false) String answerLevel,
    						 int fileId, String type, Model model) throws Exception {
    	logger.debug("boardNo {} fileId {}", Utils.getString(boardNo, "").replaceAll("[\r\n]",""), fileId);
    	logger.debug("type {} model {}", Utils.getString(type, "").replaceAll("[\r\n]",""), Utils.getString(model.toString(), "").replaceAll("[\r\n]",""));
    	fileService.removeFile(fileId);
    	String result = "redirect:/cms/boardv2/list?boardType="+boardType;
    	return result;
    }
	@RequestMapping(value="/cms/boardv2/getBoardUserName", method={RequestMethod.POST, RequestMethod.POST})
	public void getBoardUserName ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		JSONObject returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
//			returnValue = boardService.getKtotoBoardUserName();
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get  Board User Name Data Error : {}", e.getMessage());
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/cms/boardv2/getBoardReplyList", method={RequestMethod.GET, RequestMethod.POST})
	public void getBoardReplyList ( HttpServletResponse response, HttpServletRequest request
			                             , @RequestParam(required=false) String boardType
			                             , @RequestParam(required=false) String boardNo
			                             , @RequestParam(required=false) String startNum
			                             , @RequestParam(required=false) String endNum) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = boardService.getBoardReplyList(boardType, boardNo, startNum, endNum);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get  Board Reply List Data Error : {}", e.getMessage());
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/cms/boardv2/insertReply", method=RequestMethod.POST)
	public void insertReply ( HttpServletResponse response, HttpServletRequest request
			                  , @RequestParam(required=false) String boardType
			                  , @RequestParam(required=false) String boardNo 
			                  , @RequestParam(required=false) String content) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		try {
			boardService.insertReply(boardNo, boardType, content);
			msg.setSuccessText("저장 되었습니다");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR >  Insert Board Reply Data Error : {}", e.getMessage());
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/cms/boardv2/updateReply", method=RequestMethod.POST)
	public void updateReply ( HttpServletResponse response, HttpServletRequest request
			                  , @RequestParam(required=false) String boardType
			                  , @RequestParam(required=false) String boardNo
			                  , @RequestParam(required=false) String pStep
			                  , @RequestParam(required=false) String content ) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		try {
			boardService.updateReply(boardNo, boardType, pStep, content);
			msg.setSuccessText("수정 되었습니다");
		} catch (Exception e) {
			logger.error("ERROR >  Update Board Reply Data Error : {} ", e.getMessage());
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/cms/boardv2/removeReply", method=RequestMethod.POST)
	public void removeReply ( HttpServletResponse response, HttpServletRequest request 
			                  , @RequestParam(required=false) String boardType
			                  , @RequestParam(required=false) String boardNo
			                  , @RequestParam(required=false) String pStep ) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		try {
			boardService.removeReply(boardNo, boardType, pStep);
			msg.setSuccessText("삭제 되었습니다");
		} catch (Exception e) {
			logger.error("ERROR >  Remove Board Reply Data Error : {}" ,e.getMessage());
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
}
