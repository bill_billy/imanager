package com.iplanbiz.iportal.controller.excelPgm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.message.AjaxCode;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dto.ExcelDQDTO;
import com.iplanbiz.iportal.dto.ExcelPgmDQ;
import com.iplanbiz.iportal.service.excelPgm.ExcelPgmDQService;

@Controller
public class ExcelPgmDQController {
	@Autowired ExcelPgmDQService excelPgmDQService;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
		 
	/**
	 * 엑셀 자료관리(DQ)
	 */
	
	
	@RequestMapping(value="/ExcelPgmDQ", method={RequestMethod.GET})
	public ModelAndView ExcelPgmDQList(@RequestParam(required=false) String pCd, @RequestParam(required=false) String cCd) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("excelPgm/excelPgmDQ/list");
		modelAndView.addObject("pCd", pCd);
		modelAndView.addObject("cCd", cCd);
		
		return modelAndView;
	} 
	@RequestMapping(value="/ExcelPgmDQ/view", method=RequestMethod.GET)
	public ModelAndView ExcelPgmDQView(@ModelAttribute ExcelPgmDQ excelPgmDq, HttpServletResponse response)  {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("excelPgm/excelPgmDQ/detail");
		
		String ac = Utils.getString(excelPgmDq.getAc(), "");
		
		int result = 0;                                                                        
		
		if(ac.equals("save")) { //신규
			result = excelPgmDQService.saveExcelPgmDQ(excelPgmDq);
			// result : -1 = 중복 프로그램 아이디
			
			if(result == -1) {
				try {
					response.getWriter().print("ERROR_DUPLICATION");
					return null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else if(ac.equals("update")) { //수정
			result = excelPgmDQService.updateExcelPgmDQ(excelPgmDq);
		} else if(ac.equals("delete")) { // 삭제
			result = excelPgmDQService.deleteExcelPgmDQ(excelPgmDq);
		}else {
			String pgmId = Utils.getString(excelPgmDq.getPgmId(), "");
			String acctid =loginSessionInfoFactory.getObject().getAcctID();
			if(!pgmId.equals("")) { // 상세
				mav.addObject("excelDQ", excelPgmDQService.getExcelPgmDQView2(acctid, pgmId));
			}
		}

		return mav;
	}
	@RequestMapping(value="/ExcelPgmDQ/view", method=RequestMethod.POST)
	public ModelAndView ExcelPgmDQView2(@ModelAttribute ExcelPgmDQ excelPgmDq, HttpServletResponse response)  {
		
		return ExcelPgmDQView(excelPgmDq,response);
	}
	
	
	@RequestMapping(value="/ExcelPgmDQ/getParentCombobox", method={RequestMethod.POST})
	public void getParentCombobox ( HttpServletResponse response , HttpServletRequest request ) throws Exception {
		
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = excelPgmDQService.getParentCombobox();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getParentCombobox Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/ExcelPgmDQ/getChildCombobox", method={RequestMethod.POST})
	public void getChildCombobox ( HttpServletResponse response , HttpServletRequest request 
																   ,@RequestParam(required=false) String pCd) throws Exception {
		
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = excelPgmDQService.getChildCombobox(pCd);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getChildCombobox Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/ExcelPgmDQ/getExcelPgmDQList", method={RequestMethod.POST})
	public void getExcelPgmDQList ( HttpServletResponse response , HttpServletRequest request 
																   ,@RequestParam(required=false) String pCd
																   ,@RequestParam(required=false) String cCd) throws Exception {
		
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = excelPgmDQService.getExcelPgmDQList(pCd,cCd);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getExcelPgmDQList Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	/**
	 * 엑셀 관리 프로그램(DQ연동)<br/>
	 *  - 리스트
	 */
	@RequestMapping(value="/popDQSystem", method={RequestMethod.GET})
	public ModelAndView popDQSystem( ) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("excelPgm/excelPgmDQ/popDQSystem");
		
		return modelAndView;
	}
	
	
	
	
	
	/**
	 * 교직원 검색
	 */
	@RequestMapping(value="/employeeSearch", method=RequestMethod.GET)
	public ModelAndView empSearch(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("popup/employeeSearch");
		
		return modelAndView;
	}
	@RequestMapping(value="/employeeSearch/getScList", method= RequestMethod.POST)
	public void getScList( HttpServletResponse response , HttpServletRequest request 
			   ,@RequestParam(required=false) String scId) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = excelPgmDQService.getScList(scId);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getScList Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/employeeSearch/getUserList", method=RequestMethod.POST)
	public void getUserList( HttpServletResponse response , HttpServletRequest request 
			   ,@RequestParam(required=false) String scId
			   ,@RequestParam(required=false) String searchText) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = excelPgmDQService.getUserList(scId,searchText);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getUserList Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	
	/**
	 * 엑셀다운로드(DQ 연동)
	 */
	@RequestMapping(value="/ExcelPgmDQ/download", method=RequestMethod.POST)
	public ModelAndView ExcelPgmDQDownload(@RequestParam(required=false) String acctid,@RequestParam(required=false) String pgmId, @RequestParam(required=false) String ac, @RequestParam(required=false) String yymmdd){
		//ExcelPgmDownload
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("excelPgm/excelPgmDQ/excelPgmDQExcelDownload");
		
		ac = Utils.getString(ac, "");
		
		if(ac.equals("download")) {
			
			ExcelDQDTO program = excelPgmDQService.getExcelPgmDQView(acctid,pgmId);//(pgm_id, yymmdd);		// 쿼리 가져오기 (조건포함)
			
			String query = program.getDOWN_SQL();
			System.out.println(query);
			String yymm;
			String dbKey = program.getDBKEY();
			if ("all".equals(Utils.getString(yymmdd, ""))){
				yymm = "" ;
			}else{
				yymm = yymmdd;
			} 
			query = query.replace("@ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
			query = query.replace("@YYMMDD", yymm); // 쿼리에 조건 값 추가
			/*
			List<String> columnNm = excelPgmDQService.getDownloadExcelColumnNm(query);			// 쿼리에서 컬럼 가져오기
			if(columnNm == null){
				modelAndView.addObject("errMessage","데이터 다운로드 중 에러가 발생했습니다.(다운로드쿼리 오류)");
				return modelAndView;
			}
			
			List<Map<String, String>> data = excelPgmDQService.getDownloadExcelData(query);					// 데이터 가져오기
			if(data == null){
				modelAndView.addObject("errMessage","데이터 다운로드 중 에러가 발생했습니다.(다운로드쿼리 오류)");
				return modelAndView;
			}
			*/
			
			Map<String,Object> data2= excelPgmDQService.getDownloadExcelData(dbKey,query);
			
			String result = (String) data2.get("result");
			System.out.println(result);
			if(result==null) {
				modelAndView.addObject("errMessage","엑셀 파일 다운로드 중 오류가 발생했습니다.(null)");
				return modelAndView;
			}
			else if(result.equals("ConnectionException")) {
				modelAndView.addObject("errMessage","엑셀 파일 다운로드 중 오류가 발생했습니다.(Proxy서버 연결 오류)");
				return modelAndView;
			}else if(result.equals("complete")) {
				modelAndView.addObject("data", data2.get("data"));
				modelAndView.addObject("columnNm", data2.get("columnNm"));
				modelAndView.addObject("fileName", pgmId+"_"+yymmdd+".xls");
				modelAndView.setViewName("excelDownload");
			}else {
				modelAndView.addObject("errMessage","엑셀 파일 다운로드 중 오류가 발생했습니다.("+result+")");
				return modelAndView;
			}
		}

		return modelAndView;
	}
	@RequestMapping(value="/ExcelPgmDQ/download", method=RequestMethod.GET)
	public ModelAndView ExcelPgmDQDownload2(@RequestParam(required=false) String acctid,@RequestParam(required=false) String pgmId,
			@RequestParam(required=false) String ac, @RequestParam(required=false) String yymmdd){
		
		return ExcelPgmDQDownload(acctid,pgmId,ac,yymmdd);
	}
	/**
	 * 엑셀 프로그램 조회(DQ)
	 *  - 적용 : 업로드 된 데이터 적용
	 */
//업로드결과
	@RequestMapping(value="/ExcelPgmDQ/upload", method= RequestMethod.POST)
	public ModelAndView ExcelPgmDQUpload(@RequestParam(required=false) CommonsMultipartFile upFile,@RequestParam(required=false) String acctid, @RequestParam(required=false) String pgmId,@RequestParam(required=false) String pgmNm,@RequestParam(required=false) String yymmdd,@RequestParam(required=false) String ac){
		ModelAndView modelAndView = new ModelAndView();
		
		modelAndView.addObject("pgmId", pgmId);
		modelAndView.addObject("pgmNm", pgmNm);
		modelAndView.addObject("yymmdd", yymmdd);
		AjaxMessage msg = null;
		if("upload".equals(Utils.getString(String.valueOf(ac), ""))){
			
			try{ 
				msg = excelPgmDQService.uploadExcelDQByAcctID(upFile,acctid, pgmId, yymmdd);
				if(msg.getReturnCode().equals(AjaxCode.ReturnCodeMaster.SUCCESS.toString())){
					modelAndView.addObject("errMessage","정상 처리 되었습니다.");
				}else if(msg.getReturnText().equals("FileTypeError")){//업로드파일형식이 엑셀이 아닐 경우
					modelAndView.addObject("errMessage","엑셀 파일 업로드 중 오류가 발생했습니다.(업로드 파일 타입 오류)");
				}else if(msg.getReturnText().equals("FileUploadError")){
					modelAndView.addObject("errMessage","엑셀 파일 업로드 중 오류가 발생했습니다.(업로드 파일 오류)");
				}else if(msg.getReturnText().equals("FileException")){
					modelAndView.addObject("errMessage","엑셀 파일 업로드 중 오류가 발생했습니다.(Proxy서버 오류)");
				}else if(msg.getReturnText().equals("ConnectionException")) {
					modelAndView.addObject("errMessage","엑셀 파일 업로드 중 오류가 발생했습니다.(Proxy서버 연결 오류)");
				}else {
					modelAndView.addObject("errMessage","엑셀 파일 업로드 중 오류가 발생했습니다.");
				}
			}catch(Exception e){
				logger.error("error",e);
				modelAndView.addObject("errMessage","엑셀 파일 업로드 중 오류가 발생했습니다.");
			} 
		}
		modelAndView.setViewName("excelPgm/excelPgmDQ/excelPgmDQExcelUpload");
		
		return modelAndView;
	}
	@RequestMapping(value="/ExcelPgmDQ/upload", method=RequestMethod.GET)
	public ModelAndView ExcelPgmDQUpload2(@RequestParam(required=false) CommonsMultipartFile upFile,@RequestParam(required=false) String acctid, @RequestParam(required=false) String pgmId,@RequestParam(required=false) String pgmNm,@RequestParam(required=false) String yymmdd,@RequestParam(required=false) String ac){
		return ExcelPgmDQUpload(upFile,acctid,pgmId,pgmNm,yymmdd,ac);
	}
	
	/**
	 * 엑셀 프로그램 관리(DQ)
	 *  - 적용 : 업로드 + 적용
	 */
	@RequestMapping(value="/ExcelPgmDQ/uploadApply", method= RequestMethod.POST)
	public ModelAndView ExcelPgmDQUploadApply(@RequestParam(required=false) CommonsMultipartFile upFile,@RequestParam(required=false) String acctid, @RequestParam(required=false) String pgmId,@RequestParam(required=false) String pgmNm,@RequestParam(required=false) String yymmdd,@RequestParam(required=false) String ac){
		ModelAndView modelAndView = new ModelAndView();
		
		modelAndView.addObject("pgmId", pgmId);
		modelAndView.addObject("pgmNm", pgmNm);
		modelAndView.addObject("yymmdd", yymmdd); 
		
		if("upload".equals(Utils.getString(String.valueOf(ac), ""))){
			
			AjaxMessage upMsg = null;
			AjaxMessage runMsg = null;	
			try{
				upMsg = excelPgmDQService.uploadExcelDQByAcctID(upFile,acctid, pgmId, yymmdd);
			
				if(upMsg.getReturnCode().equals(AjaxCode.ReturnCodeMaster.SUCCESS.toString())){//업로드 성공
					runMsg = excelPgmDQService.executeExcelPgmDQRun("DeleteAndInsert", pgmId, yymmdd);
					if(runMsg.getReturnCode().equals(AjaxCode.ReturnCodeMaster.SUCCESS.toString())){
						modelAndView.addObject("errMessage","정상 처리 되었습니다.");
					}
					else if(runMsg.getReturnText().equals("delSql_Error")){
						modelAndView.addObject("errMessage","엑셀 파일 적용 중 오류가 발생했습니다.(삭제 쿼리 오류)");
					}else if(runMsg.getReturnText().equals("upSql_Error")){
						modelAndView.addObject("errMessage","엑셀 파일 적용 중 오류가 발생했습니다.(업로드 쿼리/업로드 데이터 오류)");
					}else if(runMsg.getReturnText().equals("dbkey_Error")){
						modelAndView.addObject("errMessage","엑셀 파일 적용 중 오류가 발생했습니다.(DB KEY 오류)");
					}else if(runMsg.getReturnText().equals("ConnectionException")) {
						modelAndView.addObject("errMessage","엑셀 파일 적용 중 오류가 발생했습니다.(Proxy서버 연결 오류)");
					}else{
						modelAndView.addObject("errMessage","엑셀 파일 적용 중 오류가 발생했습니다.");
					}//적용결과
				}else if(upMsg.getReturnText().equals("FileTypeError")){//업로드파일형식이 엑셀이 아닐 경우
					modelAndView.addObject("errMessage","엑셀 파일 업로드 중 오류가 발생했습니다.(업로드 파일 타입 오류)");
				}else if(upMsg.getReturnText().equals("FileUploadError")){
					modelAndView.addObject("errMessage","엑셀 파일 업로드 중 오류가 발생했습니다.(업로드 파일 오류)");
				}else if(upMsg.getReturnText().equals("FileException")){
					modelAndView.addObject("errMessage","엑셀 파일 업로드 중 오류가 발생했습니다.(Proxy서버 오류)");
				}else if(upMsg.getReturnText().equals("ConnectionException")) {
					modelAndView.addObject("errMessage","엑셀 파일 업로드 중 오류가 발생했습니다.(Proxy서버 연결 오류)");
				}else{
					modelAndView.addObject("errMessage","엑셀 파일 업로드 중 오류가 발생했습니다.");
				}//업로드결과
				
			}catch(Exception e){
				logger.error("error",e);
				modelAndView.addObject("errMessage","엑셀 파일 업로드 중 오류가 발생했습니다.");
			}
		}
		modelAndView.setViewName("excelPgm/excelPgmDQ/excelPgmDQExcelUploadApply");
		
		return modelAndView;
	}
	@RequestMapping(value="/ExcelPgmDQ/uploadApply", method= RequestMethod.GET)
	public ModelAndView ExcelPgmDQUploadApply2(@RequestParam(required=false) CommonsMultipartFile upFile,@RequestParam(required=false) String acctid
			, @RequestParam(required=false) String pgmId,@RequestParam(required=false) String pgmNm
			,@RequestParam(required=false) String yymmdd,@RequestParam(required=false) String ac){
		return ExcelPgmDQUploadApply(upFile,acctid,pgmId,pgmNm,yymmdd,ac);
	}
	
	
	/**
	 * 엑셀 프로그램 관리(DQ)
	 *  - 적용 : 업로드 된 데이터 적용
	 */
	@RequestMapping(value="/ExcelPgmDQ/run", method= RequestMethod.POST)
	public ModelAndView ExcelPgmDQRun(@RequestParam(required=false) String pgmId
										,@RequestParam(required=false) String pgmNm
										,@RequestParam(required=false) String ac
										,@RequestParam(required=false) String yymmdd){
		ModelAndView modelAndView = new ModelAndView();
		
		modelAndView.addObject("pgmId", pgmId);
		modelAndView.addObject("pgmNm", pgmNm);
		
		ac = Utils.getString(ac, "");
		
		AjaxMessage msg = null;
		
		if(ac.equals("DeleteAndInsert") || ac.equals("Insert")) { 
			try{ 
				msg = excelPgmDQService.executeExcelPgmDQRun(ac, pgmId, yymmdd);
				if(msg.getReturnCode().equals(AjaxCode.ReturnCodeMaster.SUCCESS.toString())){
					modelAndView.addObject("errMessage","정상 처리 되었습니다.");
				}else if(msg.getReturnText().equals("delSql_Error")){
					modelAndView.addObject("errMessage","엑셀 파일 적용 중 오류가 발생했습니다.(삭제 쿼리 오류)");
				}else if(msg.getReturnText().equals("upSql_Error")){
					modelAndView.addObject("errMessage","엑셀 파일 적용 중 오류가 발생했습니다.(업로드 쿼리/업로드 데이터 오류)");
				}else if(msg.getReturnText().equals("dbkey_Error")){
					modelAndView.addObject("errMessage","엑셀 파일 적용 중 오류가 발생했습니다.(DB KEY 오류)");
				}else if(msg.getReturnText().equals("ConnectionException")) {
					modelAndView.addObject("errMessage","엑셀 파일 적용 중 오류가 발생했습니다.(Proxy서버 연결 오류)");
				}else {
					modelAndView.addObject("errMessage","엑셀 파일 적용 중 오류가 발생했습니다.");
				}
			}catch(Exception e){
				logger.error("error",e);
				modelAndView.addObject("errMessage","엑셀 파일 적용 중 오류가 발생했습니다.");
			} 
			
			
		}
		
		modelAndView.setViewName("excelPgm/excelPgmDQ/excelPgmDQExcelRun");
		
		return modelAndView;
	}
	@RequestMapping(value="/ExcelPgmDQ/run", method= RequestMethod.GET)
	public ModelAndView ExcelPgmDQRun2(@RequestParam(required=false) String pgmId
										,@RequestParam(required=false) String pgmNm
										,@RequestParam(required=false) String ac
										,@RequestParam(required=false) String yymmdd){
		return ExcelPgmDQRun(pgmId,pgmNm,ac,yymmdd);
	}
	
	
}
