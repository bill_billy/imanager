package com.iplanbiz.iportal.controller.custom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.custom.UnivKNCAFCustomService;

@Controller
public class UnivKNCAFCustomController {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	UnivKNCAFCustomService kncafService;
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	@RequestMapping(value="/univKNCAF/mainDashboard", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView mainDashboard ( HttpServletResponse response, HttpServletRequest request) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/custom/univKNCAF/mainDashboard");
		
		return modelAndView;
	}
	@RequestMapping(value="/univKNCAF/getKpiList", method={RequestMethod.POST, RequestMethod.GET})
	public void getKpiList ( HttpServletResponse response, HttpServletRequest request) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		JSONArray returnValue = null;
		
		try {
			returnValue = kncafService.getMainDashboardKpiList();
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/univKNCAF/getMainDashboardKpiTarget", method={RequestMethod.POST, RequestMethod.GET})
	public void getMainDashboardKpiTarget ( HttpServletResponse response, HttpServletRequest request
			                                , @RequestParam(required=false) String scID ) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		JSONArray returnValue = null;
		
		try {
			returnValue = kncafService.getMainDashboardKpiTarget(scID);
			msg.setSuccessMessage(returnValue);
			logger.info("Controller Data = " + returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/univKNCAF/getMainDashbaordSqlDATA", method={RequestMethod.POST, RequestMethod.GET})
	public void getMainDashbaordSqlDATA ( HttpServletResponse response, HttpServletRequest request
			                              , @RequestParam(required=false) String scID
			                              , @RequestParam(required=false) String kpiID ) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		JSONArray returnValue = null;
		
		try {
			returnValue = kncafService.getMainDashbaordSqlDATA(scID, kpiID);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setExceptionText("오류가 있습니다" + e.getMessage());
		}
		msg.send(response);
	}
}
