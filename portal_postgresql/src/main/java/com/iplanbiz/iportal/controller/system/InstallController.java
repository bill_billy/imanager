package com.iplanbiz.iportal.controller.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView; 

@Controller
public class InstallController { 
	
	private Logger logger = LoggerFactory.getLogger(getClass());	

	@RequestMapping(value="/system/install/info", method={RequestMethod.GET})
	public ModelAndView admin_portelt (HttpServletRequest request, HttpServletResponse response ) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/system/install/info");
		  
		modelAndView.addObject("WAS",request.getSession().getServletContext().getServerInfo());
		modelAndView.addObject("ServletMajorVersion",request.getSession().getServletContext().getMajorVersion());
		modelAndView.addObject("ServletMinorVersion",request.getSession().getServletContext().getMinorVersion());
		
		modelAndView.addObject("AppPath",request.getSession().getServletContext().getRealPath("/"));
		
		String propsResource = "config/webConfig.properties";
		Properties props = new Properties();
	    props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(propsResource));
	 
	    Iterator iter = props.keySet().iterator();
	    	
	    ArrayList<HashMap<String,String>> propList = new ArrayList<HashMap<String,String>>();
		while(iter.hasNext()){
			Object key = iter.next();
		    Object val = props.getProperty(String.valueOf(key));
		    //logger.info("====>>> * {}  = {}",key,val);
		    HashMap<String, String> arg0 = new HashMap();
		    arg0.put("key", key.toString().trim());
		    arg0.put("val", val.toString().trim());
			propList.add(arg0);
		}
		modelAndView.addObject("props",propList);
		
		return modelAndView;
	}
}
