package com.iplanbiz.iportal.controller.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.CodeService;
import com.iplanbiz.iportal.service.admin.DataCenterService;

@Controller
public class DataCenterController {

	@Autowired DataCenterService dataCenterService;
	@Autowired CodeService codeService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *  
	 */
	@RequestMapping(value="/admin/datacenter/crud", method=RequestMethod.GET)
	public ModelAndView crud () {
		ModelAndView modelAndView = new ModelAndView("admin/datacenter/crud");
		
		return modelAndView;
	}
	@RequestMapping(value="/admin/datacenter/popupMappingTable", method=RequestMethod.GET)
	public ModelAndView popupMappingTable() {
		ModelAndView modelAndView = new ModelAndView("admin/datacenter/popupMappingTable");
		
		return modelAndView;
	}
	@RequestMapping(value="/admin/datacenter/getCodeList", method=RequestMethod.POST)
	public void getCodeList( HttpServletResponse response, HttpServletRequest request
			                           ,@RequestParam(required=false) String comlCod) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = codeService.getCodeListByAsc(comlCod);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Code Data Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다");
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/datacenter/getInfoDataList", method=RequestMethod.POST)
	public void getInfoDataList( HttpServletResponse response, HttpServletRequest request
											,@RequestParam(required=false, defaultValue = "") String searchComment
											,@RequestParam(required=false, defaultValue = "") String searchTableName
											,@RequestParam(required=false) String yyyy) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dataCenterService.getInfoDataList(searchComment, searchTableName, yyyy);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Info Data Error : {}", e);
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/datacenter/getDataInfoTableList", method=RequestMethod.POST)
	public void getDataInfoTableList( HttpServletResponse response, HttpServletRequest request
													,@RequestParam(required=false, defaultValue="") String searchOwner
													,@RequestParam(required=false, defaultValue="") String searchTableName
													,@RequestParam(required=false, defaultValue="") String searchComment) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dataCenterService.getDataInfoTableList(searchOwner, searchTableName, searchComment);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Info Data Table List Error : {}", e);
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/datacenter/getInfoDataDetail", method=RequestMethod.POST)
	public void getInfoDataDetail ( HttpServletResponse response , HttpServletRequest request
												,@RequestParam(required=false) String idx)  throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dataCenterService.getInfoDataDetail(idx);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Info Data Detail Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/datacenter/insert", method=RequestMethod.POST)
	public void insertInfoData( HttpServletResponse response , HttpServletRequest request
										  , @RequestParam(required=false, defaultValue="") String idx
										  , @RequestParam(required=false) String userID
										  , @RequestParam(required=false) String tableName
										  , @RequestParam(required=false) String tableComment
										  , @RequestParam(required=false) String dtStartDat
										  , @RequestParam(required=false) String dtEndDat
										  , @RequestParam(required=false) String upStartDat
										  , @RequestParam(required=false) String upEndDat
										  , @RequestParam(required=false) String bigo) throws Exception{
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		String text = "";
		try{
			returnValue = dataCenterService.insertInfoData(idx, userID, tableName, tableComment, dtStartDat, dtEndDat, upStartDat, upEndDat, bigo);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else if(returnValue == 1){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
			
		}catch(Exception e){
			logger.error("Insert Info Data Error : {}" , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/datacenter/remove", method=RequestMethod.POST)
	public void removeInfoData( HttpServletResponse response, HttpServletRequest request
											,@RequestParam(required=false) String idx) throws Exception{
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		String text = "";
		try{
			returnValue = dataCenterService.removeInfoData(idx);
			if(returnValue == 0){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Remove Info Data Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
}
