/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.iportal.controller.excelPgm;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.service.excelPgm.StepCloseEduStatsService;


@Controller
public class StepCloseEduStatsController {
	
	@Autowired StepCloseEduStatsService stepCloseEduStatService;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
		 
	/**
	 * 고등교육통계 > 통계마감처리
	 * -고등통계 진행관리
	 */
	@RequestMapping(value="/excelPgm/stepCloseEduStats/crud", method={RequestMethod.GET})
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("excelPgm/stepCloseEduStats/crud");		 
		return modelAndView; 
	}  
	@RequestMapping(value="/excelPgm/stepCloseEduStats/getStepListForCloseEduStat", method={RequestMethod.POST})
	public void getStepListForCloseEduStat(HttpServletResponse response , HttpServletRequest request 
			   ,@RequestParam(required=false) String yyyy){
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = stepCloseEduStatService.getStepListForCloseEduStat(yyyy);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getCommonCode Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}  
	@RequestMapping(value="/excelPgm/stepCloseEduStats/getStepHistoryForCloseEduStat", method={RequestMethod.POST})
	public void getStepHistoryForCloseEduStat(HttpServletResponse response , HttpServletRequest request 
			   ,@RequestParam(required=false) String yyyy,@RequestParam(required=false) String mm){
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = stepCloseEduStatService.getStepHistoryForCloseEduStat(yyyy,mm);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getCommonCode Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}  
	@RequestMapping(value="/excelPgm/stepCloseEduStats/stepCloseEduStatsSave", method={RequestMethod.POST})
	public void stepCloseEduStatsSave(HttpServletResponse response , HttpServletRequest request
			,@RequestParam(required=false) String yyyy, @RequestParam(required=false) String mm, @RequestParam(required=false) String status){
		AjaxMessage msg = new AjaxMessage();
		try {
			
			stepCloseEduStatService.stepCloseEduStatsSave(yyyy, mm, status);
			msg.setReturnText("정상 처리 되었습니다.");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			msg.setExceptionText("처리 중 오류가 발생 하였습니다."+e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);		
	}
}
