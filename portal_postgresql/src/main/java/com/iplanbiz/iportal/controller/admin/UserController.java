package com.iplanbiz.iportal.controller.admin;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.dao.admin.UserDAO;
import com.iplanbiz.iportal.dto.KtotoUser;
import com.iplanbiz.iportal.service.CodeService;
import com.iplanbiz.iportal.service.RoleService;
import com.iplanbiz.iportal.service.admin.UserService;
import com.iplanbiz.core.secure.GeneratePassword;
@Controller
public class UserController {
	
	@Autowired
	UserService userService;
	@Autowired
	CodeService codeService;
	@Autowired
	RoleService roleService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/admin/user/list", method=RequestMethod.GET)
	public ModelAndView list(){
		ModelAndView modelAndView = new ModelAndView("/admin/user/list");
		
		return modelAndView;
	}
	@RequestMapping(value="/admin/user/detail", method=RequestMethod.GET)
	public ModelAndView detail(@RequestParam(required=false, defaultValue="") String userID
								,@RequestParam(required=false) String remoteIP
					            ,@RequestParam(required=false) String remoteOS
					            ,@RequestParam(required=false) String remoteBrowser) throws Exception{
		ModelAndView modelAndView = new ModelAndView("/admin/user/detail");
		try{
			modelAndView.addObject("useYnCodeList"			, codeService.getCodeListByDesc("USE_YN"));
			modelAndView.addObject("localeCodeList"			, codeService.getCodeListByAsc("LOCALE"));
			if(!userID.equals("")){
				modelAndView.addObject("userInfo"				, userService.getDetailUserInfo(userID,remoteIP,remoteOS,remoteBrowser).get(0));
			}
		}catch(Exception e){
			logger.error("ERROR : {}", e);
			e.printStackTrace();
			throw e;
		}
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/user/insert", method=RequestMethod.POST)
	public void insert( HttpServletResponse response, HttpServletRequest request
			                  ,@RequestParam(required=false) String unID
			                  ,@RequestParam(required=false) String userID
			                  ,@RequestParam(required=false) String userName
			                  ,@RequestParam(required=false) String userPassword
			                  ,@RequestParam(required=false) String userPosition
			                  ,@RequestParam(required=false) String userEmail
			                  ,@RequestParam(required=false) String adminYn
			                  ,@RequestParam(required=false) String locale
			                  ,@RequestParam(required=false) String checkValue
			                  ,@RequestParam(required=false) String remoteIP
			                  ,@RequestParam(required=false) String remoteOS
			                  ,@RequestParam(required=false) String remoteBrowser
			                  ,@RequestParam(required=false) String orgPassword) throws Exception{
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = userService.userSave(unID, userID, userName, userPassword, userPosition, userEmail, adminYn, locale, checkValue, remoteIP, remoteOS, remoteBrowser, orgPassword);
			if(resultValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else if(resultValue == 1){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("에러가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("에러가 발생했습니다 " + e.getMessage());
			logger.error("ERROR : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/admin/user/remove", method=RequestMethod.POST)
	public void remove( HttpServletResponse response, HttpServletRequest request
			                     ,@RequestParam(required=false) String userID
			                     ,@RequestParam(required=false) String unID
			                     ,@RequestParam(required=false) String remoteIP
			                     ,@RequestParam(required=false) String remoteOS
			                     ,@RequestParam(required=false) String remoteBrowser) throws Exception{
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = userService.userDelete(unID, userID, remoteIP, remoteOS, remoteBrowser);
			if(resultValue == 0){
				msg.setSuccessText("삭제되었습니다");
			}else{
				msg.setExceptionText("에러가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("에러 : " + e.getMessage());
			logger.error("User Delete Error : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/admin/user/getAdminUserList", method=RequestMethod.POST)
	public void getAdminUserList( HttpServletResponse response, HttpServletRequest request
											   ,@RequestParam(required=false) String searchName
											   ,@RequestParam(required=false) String searchValue){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = userService.getAdminUserList(searchName, searchValue);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			msg.setExceptionText("데이터 불러오는 중 에러 : " + e.getMessage());
			logger.error("ERROR : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/user/getRoleList", method= RequestMethod.POST)
	public void getRoleList( HttpServletResponse response, HttpServletRequest request){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = roleService.getRoleList("");
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("ERROR : {}", e);
			e.printStackTrace();
			msg.setExceptionText("데이터 로딩중 Error" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/user/getRoleCheckUser", method=RequestMethod.POST)
	public void getRoleCheckUser( HttpServletResponse response, HttpServletRequest request
			                                    ,@RequestParam(required=false) String userID) throws Exception{
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = roleService.getRoleCheckUser(userID);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("ERROR : {}", e);
			msg.setExceptionText("데이터 로딩중 Error" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/user/resetPasswordUser", method=RequestMethod.POST)
	public void resetPasswordUser( HttpServletResponse response, HttpServletRequest requeset
                                                 ,@RequestParam(required=false) String userID) throws Exception{
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = userService.resetPassworedUser(userID);
			if(resultValue == 0){
				msg.setSuccessText("비밀번호가 초기화 되었습니다");
			}else{
				msg.setExceptionText("초기화 중 에러가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("초기화 중 에러가 발생했습니다 \n" + e.getMessage());
			logger.error("ERROR : {}",e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/user/excelDownByUser", method=RequestMethod.GET)
	public ModelAndView excelDownByUser(@RequestParam(required=false) String userID) throws Exception{
		ModelAndView modelAndView = new ModelAndView();
		
		modelAndView.addObject("mainList", userService.excelDownByUserBackData(userID));
		
		modelAndView.setViewName("/admin/user/excelDownByUser");
		return modelAndView;
	}
	@RequestMapping(value="/admin/user/getCheckUserID", method=RequestMethod.POST)
	public void getCheckUserID( HttpServletResponse response, HttpServletRequest request
			                    ,@RequestParam(required=false) String userID ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = userService.getCheckUserID(userID);
			if(returnValue > 0) {
				msg.setExceptionText("중복 ID가 있습니다");
			} else if(returnValue == 0) {
				msg.setSuccessText("가능한 ID 입니다.");
			} else {
				msg.setExceptionText("오류가 발생했습니다");
			}
		} catch (Exception e) {
			logger.error("ERROR : Get Check User ID Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/user/lockOpen", method=RequestMethod.POST)
	public void lockOpen( HttpServletResponse response, HttpServletRequest request
			,@RequestParam(required=false) String empId,@RequestParam(required=false) String unid 
			,@RequestParam(required=false) String gubn,@RequestParam(required=false) String remoteIp
			,@RequestParam(required=false) String remoteOs,@RequestParam(required=false) String remoteBrowser) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = userService.adminExpenses(unid, gubn, remoteIp, remoteOs, remoteBrowser);
			if(returnValue > 0) {//성공
				//lock해제
				returnValue = userService.updateLock(empId);
				if(returnValue > 0)  
					msg.setSuccessText("성공");
				else 
					msg.setExceptionText("오류가 발생했습니다");
			}else {
				msg.setExceptionText("오류가 발생했습니다");
			}
		} catch (Exception e) {
			logger.error("ERROR : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
}