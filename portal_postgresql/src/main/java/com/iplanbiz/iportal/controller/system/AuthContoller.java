package com.iplanbiz.iportal.controller.system;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.service.system.AuthService;

@Controller
public class AuthContoller {

	@Autowired
	private AuthService authService;
	 
	 
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	@RequestMapping(value="/system/auth/show", method= RequestMethod.POST)
	public void show(HttpServletRequest request, HttpServletResponse response,			
			@RequestParam(required=false) String command,
			@RequestParam(required=false) String param,
			@RequestParam(required=false) String value
			){
		AjaxMessage msg = new AjaxMessage();
		try {
			JSONObject returnObject = new JSONObject();
			
			if(command.equals("u")) {
				if(param.equals("name"))
					returnObject.put("value", authService.getEmpViewListByName(value));
			}
			else if(command.equals("p")) {
				returnObject.put("value", authService.getGroupViewList());
			}
			else if(command.equals("g")) { 
				returnObject.put("value", authService.getRoleViewList());
			}
			msg.setSuccessMessage(returnObject);
			msg.setSuccessText("complete");
		} catch (Exception e) {
			msg.setExceptionText(e.getMessage());
			logger.error("error",e);
			
		}		
		msg.send(response);
  
	}
	
	@RequestMapping(value="/system/auth/count", method= RequestMethod.POST)
	public void count(HttpServletRequest request, HttpServletResponse response,	 
			@RequestParam(required=false) String studioID,
			@RequestParam(required=false) String command,
			@RequestParam(required=false) String cID,
			@RequestParam(required=false) String pID
			){
		AjaxMessage msg = new AjaxMessage();
		try {
			JSONArray returnObject = null;
			
			if(studioID.equals("olap")) {
				returnObject = authService.isSaveAbleDocument(cID,pID);
			}
			else if(studioID.equals("report")) {
				returnObject = authService.isSaveAbleDocument(cID,pID);
			}
			msg.setSuccessMessage(returnObject);
			msg.setSuccessText("complete");
		} catch (Exception e) {
			msg.setExceptionText(e.getMessage());
			logger.error("error",e);
		}		
		msg.send(response);
  
	}
	
	
	@RequestMapping(value="/system/auth/my", method= RequestMethod.POST)
	public void my(HttpServletRequest request, HttpServletResponse response,			
			@RequestParam(required=false) String command
			){
		AjaxMessage msg = new AjaxMessage();
		try {
			JSONObject returnObject = new JSONObject();
			String userID = loginSessionInfoFactory.getObject().getUserId();
			logger.info("request.getLocalAddr()"+request.getLocalAddr());
			logger.info("request.getRemoteAddr()"+request.getRemoteAddr());
			if(request.getLocalAddr().equals(request.getRemoteAddr())==false) {
				if(userID==null||userID.trim().equals("")) {
					msg.setExceptionText("로그인 정보가 없습니다.");
					msg.send(response);;
				}
			}
			
			if(command.equals("u")) {
				returnObject.put("value", authService.getEmpViewListByUserID(userID));
			}
			else if(command.equals("p")) {
				returnObject.put("value", authService.getGroupViewListByUserID(userID));
			}
			else if(command.equals("g")) {
				returnObject.put("value", authService.getRoleViewListByUserID(userID));
			}
			msg.setSuccessMessage(returnObject);
			msg.setSuccessText("complete");
		} catch (Exception e) {
			msg.setExceptionText(e.getMessage());
			logger.error("error",e);
		}		
		msg.send(response);
  
	}
	
	@RequestMapping(value="/system/auth/find", method= RequestMethod.POST)
	public void find(HttpServletRequest request, HttpServletResponse response,			
			@RequestParam(required=false) String command,
			@RequestParam(required=false) String search
			){
		AjaxMessage msg = new AjaxMessage();
		try {
			JSONObject returnObject = new JSONObject(); 
			logger.info("request.getLocalAddr()"+request.getLocalAddr());
			logger.info("request.getRemoteAddr()"+request.getRemoteAddr());
			if(request.getLocalAddr().equals(request.getRemoteAddr())==false) {
				msg.setExceptionText("권한이 없습니다.");
				msg.send(response);;
			}
			
			if(command.equals("u")) {
				returnObject.put("value", authService.getEmpViewListByUserID(search,WebConfig.getAcctID()));
			}
			else if(command.equals("p")) {
				returnObject.put("value", authService.getGroupViewListByUserID(search,WebConfig.getAcctID()));
			}
			else if(command.equals("g")) {
				returnObject.put("value", authService.getRoleViewListByUserID(search,WebConfig.getAcctID()));
			}
			msg.setSuccessMessage(returnObject);
			msg.setSuccessText("complete");
		} catch (Exception e) {
			msg.setExceptionText(e.getMessage());
			logger.error("error",e);
		}		
		msg.send(response);
  
	}
	
	
	@RequestMapping(value="/system/auth/checkDataPermission", method= RequestMethod.POST)
	public void checkDataPermission(HttpServletRequest request, HttpServletResponse response,			
			@RequestParam(required=false) String dataName,@RequestParam(required=false) String userID,@RequestParam(required=false) String acctID
			){
		AjaxMessage msg = new AjaxMessage();
		try {
			JSONObject returnObject = new JSONObject(); 
			logger.info("request.getLocalAddr()"+request.getLocalAddr());
			logger.info("request.getRemoteAddr()"+request.getRemoteAddr());
			if(request.getLocalAddr().equals(request.getRemoteAddr())==false) {
				msg.setExceptionText("권한이 없습니다.");
				msg.send(response);;
			}
			
			returnObject.put("value", authService.myDataPermission(dataName,userID,acctID));
			
			msg.setSuccessMessage(returnObject);
			msg.setSuccessText("complete");
		} catch (Exception e) {
			msg.setExceptionText(e.getMessage());
			logger.error("error",e);
		}		
		msg.send(response);
  
	}
	
}
