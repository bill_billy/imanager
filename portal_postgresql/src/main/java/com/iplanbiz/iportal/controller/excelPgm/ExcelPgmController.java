/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.iportal.controller.excelPgm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.message.AjaxCode;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.service.cms.BoardService;
import com.iplanbiz.iportal.service.excelPgm.ExcelPgmService;


@Controller
public class ExcelPgmController {
	
	@Autowired ExcelPgmService excelPgmService;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
		 
	/**
	 * 엑셀 프로그램 관리(DQ)
	 *  - 대분류 / 중분류
	 */
	@RequestMapping(value="/ExcelPgmDQ/pgmCategory", method=RequestMethod.POST)
	public ModelAndView category(@RequestParam(required=false) String category
			, @RequestParam(required=false) String ac
			, @RequestParam(required=false) String pCd
			, @RequestParam(required=false) String pNm
			, @RequestParam(required=false) String cCd
			, @RequestParam(required=false) String cNm
			, @RequestParam(required=false) String useYn
			, @RequestParam(required=false) String sortOrder
			) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("excelPgm/excelPgmDQCategoryParent/crud");
		
		category = Utils.getString(category, "");
		ac = Utils.getString(ac, "");
		
		
		if(category.equals("P") || category.equals("")) {
			modelAndView.setViewName("excelPgm/excelPgmDQCategoryParent/crud");
					
			if(ac.equals("INSERT")) {
				excelPgmService.insertExcelPgmCategoryParent(pCd, pNm, useYn, sortOrder);
			} else if(ac.equals("UPDATE")) {
				excelPgmService.updateExcelPgmCategoryParent(pCd, pNm, useYn, sortOrder);
			} else if(ac.equals("DELETE")) {
				excelPgmService.deleteExcelPgmCategoryParent(pCd);
			}
		} else if(category.equals("C")) {
			modelAndView.setViewName("excelPgm/excelPgmDQCategoryChild/crud");
			
			if(ac.equals("INSERT")) {
				excelPgmService.insertExcelPgmCategoryChild(pCd, cCd, cNm, useYn, sortOrder);
			} else if(ac.equals("UPDATE")) {
				excelPgmService.updateExcelPgmCategoryChild(pCd, cCd, cNm, useYn, sortOrder);
			} else if(ac.equals("DELETE")) {
				excelPgmService.deleteExcelPgmCategoryChild(pCd, cCd);
			}
			
		}
		
		
		return modelAndView;
	}
	@RequestMapping(value="/ExcelPgmDQ/pgmCategory", method=RequestMethod.GET)
	public ModelAndView category2(@RequestParam(required=false) String category
			, @RequestParam(required=false) String ac
			, @RequestParam(required=false) String pCd
			, @RequestParam(required=false) String pNm
			, @RequestParam(required=false) String cCd
			, @RequestParam(required=false) String cNm
			, @RequestParam(required=false) String useYn
			, @RequestParam(required=false) String sortOrder
			) {
		return category(category,ac,pCd,pNm,cCd,cNm,useYn,sortOrder);
	}
	@RequestMapping(value="/ExcelPgmDQ/getCommonCode", method={RequestMethod.POST})
	public void getCommonCode ( HttpServletResponse response , HttpServletRequest request 
																   ,@RequestParam(required=false) String comlcd) throws Exception {
		
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = excelPgmService.getCommonCode(comlcd);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getCommonCode Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/ExcelPgmDQ/getExcelPgmDQCategory", method={RequestMethod.POST})
	public void getExcelPgmDQCategory ( HttpServletResponse response , HttpServletRequest request 
																   ,@RequestParam(required=false) String category
																   ,@RequestParam(required=false) String pCd
																   ,@RequestParam(required=false) String cCd
																   ,@RequestParam(required=false) String useYn
																   ,@RequestParam(required=false) String searchText) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = excelPgmService.getExcelPgmDQCategory(category,pCd,cCd,useYn,searchText);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getExcelPgmDQCategory Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	
}
