package com.iplanbiz.iportal.controller.portlet;

import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.CodeService;
import com.iplanbiz.iportal.service.portlet.PortletAuthorityManagerService;

@Controller
public class PortletAuthorityManagerController {

	@Autowired PortletAuthorityManagerService portletAuthorityManagerService;
	@Autowired CodeService codeService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	
	@RequestMapping(value="/portlet/portletAuthorityManager/crud", method= RequestMethod.GET)
	public ModelAndView crud ( ) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/portlet/portletAuthorityManager/crud");
		
		return modelAndView;
	}
	@RequestMapping(value="/portlet/portletAuthorityManager/getGrpTypList", method=RequestMethod.POST)
	public void getGrpTypListByPortlet ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		List<LinkedHashMap<String,Object>> list = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			JSONObject returnObject = new JSONObject();
			
			list = this.portletAuthorityManagerService.getGrpTypList();
			returnObject.put("returnArray", list);
			msg.setSuccessMessage(returnObject);
		}catch(Exception e){
			logger.error("ERROR > Get Grp Typ List Data By Portlet Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletAuthorityManager/getPortletAuthorityList", method=RequestMethod.POST)
	public void getPortletAuthorityList ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletAuthorityManagerService.getPortletAuthorityList();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Get Portlet Authrority List Data Error : {}", e);
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletAuthorityManager/getPortletPermissionUserList", method=RequestMethod.POST)
	public void getPortletPermissionUserList ( HttpServletResponse response, HttpServletRequest request 
															  ,@RequestParam(required=false) String plID
															  ,@RequestParam(required=false, defaultValue="") String empName
															  ,@RequestParam(required=false, defaultValue="") String empID) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletAuthorityManagerService.getPortletPermissionUserList(plID, empName, empID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Get Portlet Authority Permission User List Data Error : {} ", e);
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletAuthorityManager/getPortletPermissionGroupList", method=RequestMethod.POST)
	public void getPortletPermissionGroupList ( HttpServletResponse response, HttpServletRequest request
																 ,@RequestParam(required=false) String plID
																 ,@RequestParam(required=false) String grpTyp
																 ,@RequestParam(required=false) String groupName) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletAuthorityManagerService.getPortletPermissionGroupList(plID, grpTyp, groupName);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("ERROR > Get Portlet Permission Group List Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletAuthorityManager/getPortletPermissionRoleList", method=RequestMethod.POST)
	public void getPortletPermissionRoleList ( HttpServletResponse response, HttpServletRequest request 
															   ,@RequestParam(required=false) String plID) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletAuthorityManagerService.getPortletPermissionRoleList(plID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("ERROR > Get Portlet Permission Role List Data Error {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletAuthorityManager/insert", method={RequestMethod.POST})
	public void insertPortletPermission ( HttpServletResponse response, HttpServletRequest request
			                                           ,@RequestParam(required=false) String plID
			                                           ,@RequestParam(required=false) String[] arrayUserData
			                                           ,@RequestParam(required=false) String[] arrayGroupData
			                                           ,@RequestParam(required=false) String[] arrayRoleData) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletAuthorityManagerService.insertPortletPermission(plID, arrayUserData, arrayGroupData, arrayRoleData);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("ERROR > Insert Portlet Authority Data Insert Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 :" + e.getMessage());
		}
		msg.send(response);
	}
}
