package com.iplanbiz.iportal.controller.admin;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.SecurityUtil;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.dto.Menu; 
import com.iplanbiz.iportal.service.admin.MenuService;
import com.iplanbiz.iportal.service.system.SystemService;

@Controller
public class MenuController {
	
	@Autowired
	MenuService menuService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/admin/menu/crud", method=RequestMethod.GET)
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView("/admin/menu/crud");
		
		return modelAndView;
	}
	@RequestMapping(value="/admin/menu/popupMappingMenu", method=RequestMethod.GET)
	public ModelAndView popupMappingMenu(){
		ModelAndView modelAndView = new ModelAndView("/admin/menu/popupMappingMenu");
		
		return modelAndView;
	}
	@RequestMapping(value="/admin/menu/insertOne", method=RequestMethod.POST)
	public void insertOne( HttpServletResponse response, HttpServletRequest request
						            ,@ModelAttribute Menu menu) throws Exception{
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuService.insertMenuInfo(menu);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else if(returnValue == 1){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("에러가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Menu Info Insert/Update Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("에러가 발생했습니다\n" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/menu/insertArray", method=RequestMethod.POST)
	public void insertArray( HttpServletResponse response, HttpServletRequest request
			                         ,@ModelAttribute Menu menu) throws Exception{
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuService.insertMenuInfoArray(menu);
			if(returnValue == 0){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("에러가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Error : {}", e);
			msg.setExceptionText("에러가 발생했습니다\n" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/menu/remove", method=RequestMethod.POST)
	public void delete( HttpServletResponse response, HttpServletRequest request
			                   ,@ModelAttribute Menu menu) throws Exception{
		AjaxMessage msg = new AjaxMessage();
		int returnValue = 0;
		try{
			returnValue = menuService.deleteMenuInfo(menu);
			if(returnValue == 0){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("에러가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Error : {}", e);
			msg.setExceptionText("에러가 발생했습니다\n" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/menu/getTreeGridMenuList", method=RequestMethod.POST)
	public void getTreeGridMenuList( HttpServletResponse response, HttpServletRequest request) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuService.getTreeGridMenuList();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			msg.setExceptionText("데이터 로딩 중 에러 발생 \n" + e.getMessage());
			logger.error("ERROR {}" ,e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/menu/getMenuDomain", method= RequestMethod.POST)
	public void getMenuDomain( HttpServletResponse response, HttpServletRequest request) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuService.getMenuDomain();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			msg.setExceptionText("데이터 로딩 중 에러 발생 \n" + e.getMessage());
			logger.error("ERROR {}" ,e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/menu/getMenuInfoDetail", method=RequestMethod.POST)
	public void getMenuInfoDetail( HttpServletResponse response, HttpServletRequest request
			                                     ,@RequestParam(required=false) String cID) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuService.getMenuInfoDetail(cID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR : {}", e);
			msg.setExceptionText("데이터 로딩 중 에러 발생 \n" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/menu/getPopupMenuTreeData", method=RequestMethod.POST)
	public void getPopupMenuTreeData( HttpServletResponse response, HttpServletRequest requeset
			                                             ,@RequestParam(required=false) String searchValue){
		AjaxMessage msg = new AjaxMessage();
		JSONArray returnValue = null;
		try{
			returnValue = menuService.getPopupMenuTreeData(searchValue);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			msg.setExceptionText("데이러 로딩 중 에러 발생 \n" + e.getMessage());
			logger.error("ERROR : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/menu/getMenuBiSolutionPathDeepScan", method=RequestMethod.POST)
	public void getMenuBiSolutionPathDeepScan ( HttpServletResponse response, HttpServletRequest request
																	  ,@RequestParam(required=false) String cID
																	  ,@RequestParam(required=false) String cName
																	  ,@RequestParam(required=false) String cLink
																	  ,@RequestParam(required=false) String ac ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		cLink = SecurityUtil.unEscape(cLink);
		try{
			returnValue = menuService.getMenuBiSolutionPathDeepScan(cID, cName, cLink, ac);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Get Menu BiSolution Path Deep Scan Data Error : {}" ,e);
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/menu/manageMenuList", method=RequestMethod.GET)
	public ModelAndView manageMenuList() throws Exception{
		ModelAndView mview = new ModelAndView();
		mview.setViewName("/admin/menu/manageMenuList");
		return mview;
	}
	@RequestMapping(value="/admin/menu/moveMenuPosition", method=RequestMethod.POST)
	public void moveMyfolderNoCog ( HttpServletResponse response, HttpServletRequest request
			,@RequestParam(required=false) String cId ,@RequestParam(required=false) String pId ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			if(pId==null||pId.equals("")) pId="5999";
			returnValue = menuService.moveMenuPosition(cId,pId);
			if(returnValue == 0){
				msg.setSuccessText("이동 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("ERROR > moveMenuPosition Error : {}" , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/menu/sortMenuPosition", method=RequestMethod.POST)
	public void sortMenuPosition ( HttpServletResponse response, HttpServletRequest request
			,@RequestParam(required=false) String cId ,@RequestParam(required=false) String targetId ,@RequestParam(required=false) String position,@RequestParam(required=false) String sortOrder) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = menuService.sortMenuPosition(cId,targetId,position,sortOrder);
			if(returnValue == 0){
				msg.setSuccessText("이동 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("ERROR > sortMenuPosition Error : {}" , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
}
