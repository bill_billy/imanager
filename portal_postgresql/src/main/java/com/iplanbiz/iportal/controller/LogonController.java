package com.iplanbiz.iportal.controller;

import java.io.File;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.SecurityUtil;
import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.secure.Crypto;
import com.iplanbiz.core.secure.SHA256;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.comm.message.LocaleUtil;
import com.iplanbiz.iportal.comm.message.Prop;
import com.iplanbiz.iportal.comm.session.SessionManager;
//import com.iplanbiz.iportal.comm.session.SessionManager;
import com.iplanbiz.iportal.comm.util.CognosInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.auth.EmpView;
import com.iplanbiz.iportal.dto.User;
import com.iplanbiz.iportal.service.CognosService;
import com.iplanbiz.iportal.service.PasswordService;
import com.iplanbiz.iportal.service.UserLoginConnectionService;
import com.iplanbiz.iportal.service.admin.MenuService;
import com.iplanbiz.iportal.service.admin.UserService;
import com.iplanbiz.iportal.service.custom.KtotoUserService;
import com.iplanbiz.iportal.service.system.SystemService;


@Controller
public class LogonController {
	
	@Autowired private UserService userService;
	@Autowired private SystemService systemService;
	@Autowired private PasswordService passwordService;
	@Autowired private UserLoginConnectionService userLoginConnectionService;
	@Autowired private CognosService cognosService;
	@Autowired private EmpView empView;
	@Autowired private KtotoUserService ktotouserService;
	@Autowired
	MenuService menuService;
	
	@Resource(name="sessionManager") SessionManager sessionManager;
	@Resource(name="loginSessionInfoFactory") 	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/**
	 * 로그인 화면 이동
	 * @param 
	 * @return ModelAndView
	 */
	@RequestMapping(value = {"/login/page/index"},  method=RequestMethod.GET)
	public ModelAndView LoginView(HttpServletRequest request, HttpServletResponse response
													,@RequestParam(required=false, defaultValue ="") String key){
		  
		ModelAndView modelAndView = new ModelAndView();
		//이미 세션에 로그인 정보가 있다면 메인 페이지로..
		if(loginSessionInfoFactory.getObject().getUserId()!=null){
			return new ModelAndView("redirect:/");
		}
		else{  
			User user = new User();   
			String csrfToken = SecurityUtil.getCSRFToken();
			loginSessionInfoFactory.getObject().setCsrfToken(csrfToken);
			modelAndView.addObject(SecurityUtil.CSRFTOKENNAME, csrfToken);
			modelAndView.addObject("user", user);
			modelAndView.setViewName(this.getLoginView());
		}
		
		
		return modelAndView;
	}
	@RequestMapping(value = {"/login/action/csrfvalidation"},  method=RequestMethod.GET)
	public ModelAndView csrfValidation(HttpServletRequest request, String csrftoken){ 
		ModelAndView modelAndView = new ModelAndView(); 
		
		String rtdata = "false";
		if(csrftoken!=null&&this.loginSessionInfoFactory.getObject().getCsrfToken().equals(csrftoken)){
			rtdata="true";
		}
		modelAndView.addObject("result",rtdata);
		modelAndView.setViewName("login/csrfvalidation");
		return modelAndView;
	}
	public String getLoginView(){
		String filepath = Utils.getString(WebConfig.getDefaultServerRoot(),"")+"/WEB-INF/iportal/login/"+WebConfig.getAcctID()+"/"; 
		String filename = "index.jsp";
		File loginFile = new File(filepath,FilenameUtils.getName(filename));
		logger.debug("{} LoginPage: {}", Utils.getString(WebConfig.getAcctID(), "").replaceAll("[\r\n]",""), (filepath).replaceAll("[\r\n]",""));
//		logger.info("Is {} LoginPage: {}", WebConfig.getAcctID(), loginFile.isFile());
		if(loginFile.isFile())
			return "login/"+ Integer.parseInt(WebConfig.getAcctID())+"/index";
		else
			return "login/page/index";
	}
	@RequestMapping(value = {"/login/action/noSSOScript"}, method=RequestMethod.GET)
	public ModelAndView noSSOScript(){
		return new ModelAndView("login/action/noSSOScript");
	}
	
	/**
	 * 로그인 처리(로그인 완료 되면 preLogin 으로 먼저 이동하여 코그너스 웹페이지 로그인 처리)
	 * @param 
	 * @return ModelAndView
	 * @throws Exception 
	 * SecurityUtil 적용 by 신운식 2014.04.03
	 * GS인증용 CSRF방어 장민수 20151001
	 */
	@RequestMapping(value = {"/login/page/index"}, method=RequestMethod.POST)
	public ModelAndView Login(@ModelAttribute("user") User user, HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		
		ModelAndView modelAndView = new ModelAndView();
		request.setCharacterEncoding("UTF-8"); 
		
		String serverCSRFToken = loginSessionInfoFactory.getObject().getCsrfToken();
		String clientCSRFToken = "";
		if(request.getParameter(SecurityUtil.CSRFTOKENNAME)!=null)
			clientCSRFToken = request.getParameter(SecurityUtil.CSRFTOKENNAME);
		else
			clientCSRFToken = serverCSRFToken;
		
	//	logger.info("####SERVER CSRF TOKEN#### :"+serverCSRFToken.replaceAll("[\r\n]",""));
	//	logger.info("####CLIENT CSRF TOKEN#### :"+clientCSRFToken.replaceAll("[\r\n]",""));
		
		//CSRF 토큰 비교 
		/**
		if(serverCSRFToken.equals(clientCSRFToken)==false){
					String csrfToken = SecurityUtil.getCSRFToken();
					loginSessionInfoFactory.getObject().setCsrfToken(csrfToken);
					modelAndView.addObject(SecurityUtil.CSRFTOKENNAME, csrfToken);
					modelAndView.addObject("msg", Prop.propFormat("_alert.csrftokeninvalid", LocaleUtil.getLocale(request),  "[Message]","java"));
					modelAndView.setViewName(this.getLoginView());
					return modelAndView;
		}
		**/
		String acctName= (request.getParameter("ACCT_NM")==null)?"":request.getParameter("ACCT_NM");
		acctName = acctName.trim();
		String acctID = userService.getAcctIDByAcctName(acctName);
		logger.debug("GET ACCT NAME="+Utils.getString(acctName, "").replaceAll("[\r\n]",""));		
		logger.debug("WebConfig.getACCTID=" + Utils.getString(WebConfig.getAcctID(), "").replaceAll("[\r\n]",""));
		logger.debug("User Service Get ACCT ID = " + Utils.getString(acctID, "").replaceAll("[\r\n]",""));
		
		if(acctID == null||acctID.trim().equals("")) acctID = WebConfig.getAcctID();
		
		String userID = SecurityUtil.escapeQuot(user.getUserID());
		//이미 로그인한 User인지 체크
		logger.info("SessionManager.isLogOn("+userID+")=>"+sessionManager.isDuplicateLogOn(userID, acctID,request));
		
		loginSessionInfoFactory.getObject().setAcctID(acctID);		
		loginSessionInfoFactory.getObject().setIpAddress(sessionManager.getClientIP(request));
		logger.debug("userID:{},acctID:{}",userID,acctID);
		User dbUser = userService.getUserInfo(userID, acctID);
		logger.debug("DB 정보");
		//잘못된 사용자 로그인시 로그인 화면으로!
		if(dbUser == null) {
			logger.debug("사용자 정보 없음:{}",userID);
//			String csrfToken = SecurityUtil.getCSRFToken();
//			loginSessionInfoFactory.getObject().setCsrfToken(csrfToken);
//			modelAndView.addObject(SecurityUtil.CSRFTOKENNAME, csrfToken);
			modelAndView.setViewName(this.getLoginView());
			userService.setLoginFailInfo(user, request);
			modelAndView.addObject("msg", "ID와 PASSWORD를 확인주세요!");
			return modelAndView;
		}
		
		user.setUserLocale(dbUser.getUserLocale());
		user.setUserType(dbUser.getUserType());
		
		logger.info("계정상태확인");
		//계정 상태확인
		int returnVar = 0;
		
		Map<String, Object> iect7003 = userService.getAdminUserInfo(userID);
		if(iect7003 != null) { //iect7003에 있고
			String unid = iect7003.get("unid").toString();
			Map<String, Object> userIp = null;
			try {
				userIp = ktotouserService.getIpAddress(unid);
			}catch(Exception e) { logger.debug("not use ktt lib");}
			if(userIp!=null) {
				String loginip = loginSessionInfoFactory.getObject().getIpAddress();
				String ipIn = userIp.get("k_ip_in").toString();
				String ipOut = userIp.get("k_ip_out").toString();
				
				logger.info("현재 로그인 IP:"+loginip);
				logger.info("내부 IP:"+ipIn);
				logger.info("외부 IP:"+ipOut);
				if(!loginip.equals(ipIn) && !loginip.equals(ipOut)) {
					if(WebConfig.getLockedCount() > 0){
						// IP접근제한으로 로그인실패 증가 및 실패이력 INSERT
						userService.setLoginFailCountAdd(user);
						userService.setLoginFailInfo(user, request);
					}
					returnVar = -4;
				}
			}
		}
		
		if(WebConfig.getLockedCount() > 0 && returnVar!=-4){
			if(userService.isExistUser(user)){
				
				int loginFailCnt = Integer.parseInt(String.valueOf(userService.getUserStatusAndFailCount(user).get("fail_cnt")));
				if(loginFailCnt >= WebConfig.getLockedCount()){
					returnVar = -3;
				}
			}
		}
		
		logger.info("계정상태확인returnVar="+returnVar);
		user.setPasswd(SecurityUtil.unEscape(user.getPasswd()));
		if(returnVar == 0){
			String pwd = user.getPasswd().replace("'", "''");
			
			System.out.println("CognosINFO::"+CognosInfo.getConnect());
			if(CognosInfo.getConnect()!=null){
				returnVar = cognosService.logon(user);
			}
			String cryptoPassword = "";
			if("IPLAN".equals(dbUser.getUserType())){ // IECT7003 
				cryptoPassword = SHA256.getSHA256(user.getPasswd());
			}
			else if("S256B64".equals(dbUser.getUserType())){
				cryptoPassword = SHA256.getSHA256ByBASE64(user.getPasswd());
			}
			else if("FUNCTION".equals(dbUser.getUserType())){
				 
				cryptoPassword = empView.decodePassword(user.getUserID(), pwd, acctID);
			}
			else if("OTHER".equals(dbUser.getUserType())){ //고객사 DB사용
				cryptoPassword = user.getPasswd();
			}else {
				cryptoPassword = SHA256.getSHA256(user.getPasswd());
			}
			logger.info("emp_typ:{}",dbUser.getUserType());
			logger.info("cryptoPassword:{}",cryptoPassword);
			logger.info("user.getPasswd():{}",user.getPasswd());
			logger.info("dbUser.getPasswd():{}",dbUser.getPasswd());
			
			if(cryptoPassword!=null&&cryptoPassword.equals(dbUser.getPasswd()))
				returnVar = 1;
			else
				returnVar = 0;
			 
		}
		
		if(returnVar > 0){
			Crypto cs = new Crypto();
	    	
			cs.setSecretKey("ioneiplan");
			cs.setAlgorithm("BlowFish");
			
			InetAddress local = InetAddress.getLocalHost();
			String svIp = local.getHostAddress();
			
			
			long timeStamp = System.currentTimeMillis();
		
			//계정상태 초기화
			userService.resetUserStatus(user);
			
			loginSessionInfoFactory.getObject().setLogin(true);
			loginSessionInfoFactory.getObject().setUserId(userID);
			loginSessionInfoFactory.getObject().setUserName(userService.getUserInfo(userID, acctID).getUserName());
			loginSessionInfoFactory.getObject().setState(LoginSessionInfo.State.NORMAL);
			loginSessionInfoFactory.getObject().setLocale(user.getUserLocale());
			if(userService.getAdminUserInfo(userID)!=null) {
				loginSessionInfoFactory.getObject().setExternalInfo("checkPwdUpdate", true);
				String pwdUpdateDate = passwordService.passwordUpdateDat(userID);
				loginSessionInfoFactory.getObject().setExternalInfo("passwordUpdateDat", pwdUpdateDate);
			}else{
				loginSessionInfoFactory.getObject().setExternalInfo("checkPwdUpdate", false);
			}
			
			modelAndView.addObject("systemConfig",systemService.getSystemConfig());
			modelAndView.addObject("solutionDomainUrl", "/sessionCreate");
			modelAndView.setViewName("/login/action/preLogin_nonportal");
			try {
				if("Y".equals(userService.getAdminUserInfo(userID).get("admin_yn")) || "admin".equals(userID)){
					loginSessionInfoFactory.getObject().setIsAdmin(true);
				}	
			} catch (Exception e) {
				if("admin".equals(userID))
					loginSessionInfoFactory.getObject().setIsAdmin(true);
				else
					loginSessionInfoFactory.getObject().setIsAdmin(false);
			}
			modelAndView.addObject("isLogin",sessionManager.isDuplicateLogOn(userID, acctID,request));
		}else{
			logger.info("LOCKEDCOUNT login:{}", WebConfig.getLockedCount());
			logger.info("LOCKEDCOUNT loginReturnVal:{}", returnVar);
			if(returnVar == -2 || returnVar == 0){
				logger.info("LOCKEDCOUNT :{}", WebConfig.getLockedCount());
				if(WebConfig.getLockedCount() > 0){
					// ID or PW 실패로 로그인실패 증가 및 실패이력 INSERT
					userService.setLoginFailCountAdd(user);
					
				}
				modelAndView.addObject("msg", "ID와 PASSWORD를 확인주세요!");
			}else if(returnVar == -3){
				modelAndView.addObject("msg", "계정이 잠겼습니다. 관리자에게 문의해주세요!");
			}else if(returnVar == -4){
				modelAndView.addObject("msg", "현재 IP에서는 접속할 수 없는 계정입니다. 관리자에게 문의해주세요!");
			}
			else{
				modelAndView.addObject("msg", "ID와 PASSWORD를 확인주세요!");	
			}
			userService.setLoginFailInfo(user, request);
//			String csrfToken = SecurityUtil.getCSRFToken();
//			loginSessionInfoFactory.getObject().setCsrfToken(csrfToken);
//			modelAndView.addObject(SecurityUtil.CSRFTOKENNAME, csrfToken);
			modelAndView.setViewName(this.getLoginView());
		}
		return modelAndView;
	}
	@RequestMapping(value="/login/action/addLoginSessionInfo", method={RequestMethod.POST})
	public void addLoginSessionInfo ( HttpServletRequest request, HttpServletResponse response) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		boolean returnValue = false;
		try{
			sessionManager.addLoginSessionInfo(loginSessionInfoFactory.getObject().getUserId(),
					loginSessionInfoFactory.getObject().getAcctID(),request,loginSessionInfoFactory.getObject());
			returnValue = true;
			
		}catch(Exception e){
			returnValue = false;			
		}
		JSONObject result = new JSONObject();
		result.put("RESULT", returnValue);
		msg.setSuccessMessage(result);
		msg.send(response);
	}
	@RequestMapping(value="/login/action/authresult", method={RequestMethod.POST})
	public void authresult ( HttpServletRequest request, HttpServletResponse response
										  ,@RequestParam(required=false) String userID
										  ,@RequestParam(required=false) String passwd 
										  ,@RequestParam(required=false) String CSRFTOKENNAME) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		boolean returnValue = false;
		 
		try{
			returnValue = loginprocess(userID, passwd, WebConfig.getAcctID());
			
		}catch(Exception e){
			returnValue = false;			
		}
		JSONObject result = new JSONObject();
		result.put("RESULT", returnValue);
		msg.setSuccessMessage(result);
		msg.send(response);
	}
	public boolean loginprocess(String empID, String pwd, String acctID) throws Exception {  
		boolean result = false; 
		
		String userID = SecurityUtil.escapeQuot(empID);		
		
		User dbUser = userService.getUserInfo(userID, acctID); 
		//사용자 테이블에 유저 정보가 없을 경우.
		if(dbUser == null) {			
			return false;
		}   
		pwd = pwd.replace("'", "''");
		  
		String cryptoPassword = "";
		if("IPLAN".equals(dbUser.getUserType())){ // IECT7003 
			cryptoPassword = SHA256.getSHA256(pwd);
		}
		else if("S256B64".equals(dbUser.getUserType())){
			cryptoPassword = SHA256.getSHA256ByBASE64(pwd);
		}
		else if("FUNCTION".equals(dbUser.getUserType())){
			 
			cryptoPassword = empView.decodePassword(empID, pwd, acctID);
		}
		else if("OTHER".equals(dbUser.getUserType())){ //고객사 DB사용
			cryptoPassword = pwd;
		}else {
			cryptoPassword = SHA256.getSHA256(pwd);
		} 
		
		if(cryptoPassword.equals(dbUser.getPasswd()))
			result = true;
		else
			result = false;
		
		return result;
	}
	
	
	@RequestMapping(value="/login/action/duplicationwarning", method=RequestMethod.GET)
	public ModelAndView duplicationWarning(HttpServletRequest request){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/login/action/duplicationwarning");
		return modelAndView;
	}  
	@RequestMapping(value="/login/action/duplicationwarningPost", method=RequestMethod.GET)
	public void duplicationWarningPost(HttpServletRequest request, HttpServletResponse response){
		
		AjaxMessage msg = new AjaxMessage();
		msg.setExceptionText("duplication");
		msg.send(response);
	}  
	/**
	 * 코그너스 로그아웃 처리
	 * @param 
	 * @return ModelAndView
	 * @throws Exception 
	 */
	@RequestMapping(value = {"/login/action/cogLogout"}, method=RequestMethod.GET)
	public ModelAndView cogLogout() throws Exception {
		
		ModelAndView modelAndView = new ModelAndView();
		if(CognosInfo.getConnect()!=null){
				cognosService.logout();
			}
			modelAndView.addObject("systemConfig", systemService.getSystemConfig());
			modelAndView.addObject("solutionDomainUrl", "/logout");
			modelAndView.setViewName("/login/action/preLogout_nonportal");
		return modelAndView;
	}
	
	@RequestMapping(value = {"/login/action/cogLogout"}, method=RequestMethod.POST)
	public ModelAndView cogLogout2() throws Exception {		
		return cogLogout();
	}
	
	@RequestMapping(value="/login/action/insertUserLog", method=RequestMethod.POST)
	public void insertUserLog ( HttpServletRequest request, HttpServletResponse response
										  ,@RequestParam(required=false) String actionTypeNo
										  ,@RequestParam(required=false) String targetTypeNo
										  ,@RequestParam(required=false) String targetValue
										  ,@RequestParam(required=false) String targetComment
										  ,@RequestParam(required=false) String cID 
										  ,@RequestParam(required=false) String osInfo
										  ,@RequestParam(required=false) String browserInfo
										  ,@RequestParam(required=false) String browserVersion
										  ,@RequestParam(required=false) String targetPath) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		int returnValue = 0;
		String remoteIP = Utils.getClientIpAddr(request);	
		if(targetPath==null||targetPath.equals(""))
			targetPath = menuService.getNavi(cID);
		if(targetValue==null||targetValue.equals("")) {
			JSONArray list = menuService.getMenuInfoDetail(cID);
			if(list.size()!=0)
				targetValue = ((JSONObject)list.get(0)).get("C_NAME").toString();
		}
		if(targetComment!=null&&targetComment.equals("")==false)
			targetValue+="_"+targetComment;
		try{
			returnValue = userLoginConnectionService.insertUserLog(actionTypeNo, targetTypeNo, targetValue, cID, remoteIP, osInfo, browserInfo, browserVersion, targetPath);
			if(returnValue == 0){
				logger.info("User Log Insert Success");
			}else{
				logger.info("User Log Insert Fail");
			}
		}catch(Exception e){
			logger.error("User Log Insert Error : {} " , e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	
	@RequestMapping(value={"/login/action/logout"}, method=RequestMethod.GET)
	public String logout(HttpSession session,HttpServletRequest request,HttpServletResponse response) {
		String userId = loginSessionInfoFactory.getObject().getUserId();
		String acctId = loginSessionInfoFactory.getObject().getAcctID();
		String duplication = request.getParameter("dp");
		
		logger.info("duplication logout");
		loginSessionInfoFactory.getObject().setLogin(false);
		session.invalidate();
		
		//session.invalidate();
		//SSO 연동 Interface가 구현되어 있다면 SSO로그아웃
		//Reflection 객체 생성
		String path = WebConfig.getAcctID();
		
		return "redirect:/";
	}
	@RequestMapping(value={"/login/action/logout"}, method=RequestMethod.POST)
	public String logout2(HttpSession session,HttpServletRequest request,HttpServletResponse response) {
		return logout(session,request,response);
	}

}