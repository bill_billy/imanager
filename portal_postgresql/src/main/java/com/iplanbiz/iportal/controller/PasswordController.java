package com.iplanbiz.iportal.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.secure.SHA256;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.service.PasswordService;
import com.iplanbiz.iportal.service.admin.UserService;

@Controller
public class PasswordController {

	@Autowired
	PasswordService passwordService;
	
	@Autowired
	UserService userService;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	@RequestMapping(value="/home/pwd/changePassword", method=RequestMethod.GET)
	public ModelAndView changePassword(String type) {
		ModelAndView m = new ModelAndView("home/pwd/changePassword");
		m.addObject("type", type);
		return m;
		
	}
	
	@RequestMapping(value="/home/pwd/update", method=RequestMethod.POST)
	public void update( HttpServletResponse response, HttpServletRequest request
            ,@RequestParam(required=true) String newpwd,@RequestParam(required=true) String orgpwd
            ,@RequestParam(required=false) String remoteIP
			,@RequestParam(required=false) String remoteOS
			,@RequestParam(required=false) String remoteBrowser) {
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			if(passwordService.equalOrgpwd(orgpwd) == 1) {
				resultValue = passwordService.update(newpwd,orgpwd,remoteIP,remoteOS,remoteBrowser);
				if(resultValue == 1){
					msg.setSuccessText("비밀번호가 변경되었습니다");
				}else if(resultValue == -1){
					msg.setExceptionText("기존 비밀번호와 일치합니다. 다시 입력해주세요.");
				}else{
					msg.setExceptionText("에러가 발생했습니다");
				}
			}else {
				msg.setExceptionText("기존 비밀번호가 일치하지 않습니다.");
			}
		}catch(Exception e){
			msg.setExceptionText("에러가 발생했습니다 " + e.getMessage());
			logger.error("ERROR : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
}
