package com.iplanbiz.iportal.controller.ktoto;

import java.beans.Encoder;
import java.net.URLEncoder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dto.ktoto.KtotoBoard;
import com.iplanbiz.iportal.dto.ktoto.KtotoNotice;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.cms.NoticeService;
import com.iplanbiz.iportal.service.file.FileService;
import com.iplanbiz.iportal.service.ktoto.KtotoBoardService;
import com.iplanbiz.iportal.service.ktoto.KtotoNoticeService;

@Controller
public class KtotoBoardController {

	@Autowired
	KtotoBoardService ktotoService;
	@Autowired
	FileService fileService;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	�삵븘�엳 以��닔�빐 二쇱꽭�슂.�븳 而⑦듃濡ㅼ뿉 �븘�옒 硫붿냼�뱶�뒗 �븯�굹�뵫留� �엳�뼱�빞 �븯硫�
	 *	insert 濡쒖쭅�씠 異붽��릺�뼱�빞 �븷 寃쎌슦 而⑦듃濡ㅼ쓣 異붽� �벑濡� �빀�땲�떎.
	 *
	 *	而⑦듃濡ㅼ쓣 �깮�꽦�븯�떊 遺꾩� �씠 硫붿떆吏�瑜� �룞�씪�븯寃� �꽔�뼱二쇱꽭�슂.
	 *	
	 *  援щ텇 | 硫붿냼�뱶	 | 		硫붿냼�뱶紐�
		�깮�꽦 : POST 			/insert
		�닔�젙 : POST 			/update
		議고쉶 : GET,POST 		/list
		議고쉶 : GET,POST 		/select(ajax) 
		�궘�젣 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/ktoto/board/list", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView ktotoBoardList () throws Exception {
		ModelAndView modelAndView = new ModelAndView("/ktoto/board/list");
		
		return modelAndView;
	}
	@RequestMapping(value="/ktoto/board/crud", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView ktotoBoardCrud() throws Exception {
		ModelAndView modelAndView = new ModelAndView("/ktoto/board/crud");
		
		return modelAndView;
	}
	@RequestMapping(value="/ktoto/board/getKtotoBoardList", method={RequestMethod.POST, RequestMethod.GET})
	public void getKtotoBoardList ( HttpServletResponse response, HttpServletRequest request
			                        , @RequestParam(required=false) String boardType
			                        , @RequestParam(required=false) String startDat
			                        , @RequestParam(required=false) String endDat
			                        , @RequestParam(required=false) String titleName ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = ktotoService.getKtotoBoardList(boardType, startDat, endDat, titleName);
			msg.setSuccessMessage(returnValue);
		} catch ( Exception e ) {
			logger.error("ERROR > Get Ktoto Board List Data Error : {}", e.getMessage());
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/ktoto/board/getKtotoBoardDetail", method={RequestMethod.POST, RequestMethod.GET})
	public void getKtotoBoardDetail ( HttpServletResponse response, HttpServletRequest request
			                          , @RequestParam(required=false) String boardType
			                          , @RequestParam(required=false) String boardNO ) throws Exception {
		JSONObject returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = ktotoService.getKtotoBoardDetail(boardType, boardNO);
			msg.setSuccessMessage(returnValue);
		} catch ( Exception e ) {
			logger.error("ERROR > Get Ktoto Board Detail Data Error : {} " ,e.getMessage());
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	} 
	@RequestMapping(value="/ktoto/board/getKtotoBoardFileList", method={RequestMethod.POST, RequestMethod.GET})
	public void getKtotoBoardFileList ( HttpServletResponse response, HttpServletRequest request 
			                            , @RequestParam(required=false) String tableNM
			                            , @RequestParam(required=false) String boardNO ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = ktotoService.getKtotoBoardFileList(tableNM, boardNO);
			msg.setSuccessMessage(returnValue);
		} catch ( Exception e ) {
			logger.error("ERROR > Get Ktoto Board File List Data Error : {}", e.getMessage());
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/ktoto/board/setKtotoBoardHitCount", method=RequestMethod.POST)
	public void setKtotoBoardHitCount ( HttpServletRequest request, HttpServletResponse response
			                            , @RequestParam(required=false) String boardNo
			                            , @RequestParam(required=false) String boardType ) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		try {
			ktotoService.setKtotoBoardHitCount(boardNo, boardType);
			msg.setSuccessText("성공");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Set Ktoto Board Hit Count Error : {}" ,e.getMessage());
		}
		msg.send(response);
		
	}
	@RequestMapping(value="/ktoto/board/save", method={RequestMethod.POST})
	public String insertBoard ( @ModelAttribute KtotoBoard board, Model model, @RequestParam(required=false) String saveGubn ) throws Exception {
		String result = "";
		String returnValue = "Y";
		AjaxMessage msg = new AjaxMessage();
		logger.info("Ktoto Notice Save Start");
		logger.info("Save GUBN = " + saveGubn);
		logger.info("Get FIle = " + board.getFile());
		if(board.getFile().length > 0) {
			for (CommonsMultipartFile cmfile : board.getFile()) {
//				String FileFilter = "txt,zip,jpg,png,xls,xlsx,ppt,pptx,doc,docx,hwp,pdf,mp4";
				String FileFilter = "xls,xlsx,ppt,pptx,doc,docx,hwp,pdf";
				String ext = (cmfile.getOriginalFilename().substring(cmfile.getOriginalFilename().lastIndexOf(".") + 1, cmfile.getOriginalFilename().length())).toLowerCase();
				if (FileFilter.indexOf(ext) != -1) {
					returnValue = "Y";
				} else {
					returnValue = "N";
				} 
			}
		} else {   
			returnValue = "Y";
		}
		if("N".equalsIgnoreCase(saveGubn)) {
			if(returnValue.equals("N")) {
				result = Utils.getString(WebConfig.getSystemPortalUrl() + ":" + WebConfig.getPortalPort() + "/accessRight","");
			} else {
				ktotoService.insertBoard(board);
				model.addAttribute("msg", URLEncoder.encode("저장되었습니다", "UTF-8"));
				result = "redirect:/ktoto/board/list?boardType="+board.getBoardType();
			}
		}else if("U".equalsIgnoreCase(saveGubn)) {
			logger.info("Notice Udate Start");
			if(returnValue.equals("N")) {
				result = Utils.getString(WebConfig.getSystemPortalUrl() + ":" + WebConfig.getPortalPort() + "/accessRight","");
			} else {
				logger.info("Notice Udate  Query Start");
				model.addAttribute("msg", URLEncoder.encode("수정되었습니다.", "UTF-8"));
				ktotoService.updateBoard(board);
				result = "redirect:/ktoto/board/list?boardType="+board.getBoardType();
			}
		}
//		msg.send(response);
		return result;
	}
	@RequestMapping(value="/ktoto/board/remove", method=RequestMethod.POST)
	public String noticeDelete ( @RequestParam(required=false) String boardNO
			                     , @RequestParam(required=false) String boardType
			                     ,  Model model) throws Exception {
		String result = "";
		try {
			ktotoService.removeBoard(boardType, boardNO);
			model.addAttribute("msg", URLEncoder.encode("삭제되었습니다.", "UTF-8"));
			result = "redirect:/ktoto/board/list?boardType="+boardType;
		} catch ( Exception e ) {
			result = Utils.getString(WebConfig.getSystemPortalUrl() + ":" + WebConfig.getPortalPort() + "/accessRight","");
			e.printStackTrace();
			logger.error("ERROR > Delete Notice Data Error : {} ", e.getMessage());
		}
		return result;
	}
	@RequestMapping(value="/ktoto/board/deleteFile", method = RequestMethod.GET)
    public String deleteFile(@RequestParam(required = false, value = "boardNo") String boardNo, 
    						 @RequestParam(required = false) String boardType, 
    						 @RequestParam(required = false) String pBoardno,
    						 @RequestParam(required = false) String answerLevel,
    						 int fileId, String type, Model model) throws Exception {
    	logger.debug("boardNo {} fileId {}", Utils.getString(boardNo, "").replaceAll("[\r\n]",""), fileId);
    	logger.debug("type {} model {}", Utils.getString(type, "").replaceAll("[\r\n]",""), Utils.getString(model.toString(), "").replaceAll("[\r\n]",""));
    	fileService.removeFile(fileId);
    	String result = "redirect:/ktoto/board/list?boardType="+boardType;
    	return result;
    }
	@RequestMapping(value="/ktoto/board/getKtotoBoardUserName", method={RequestMethod.POST, RequestMethod.POST})
	public void getKtotoBoardUserName ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		JSONObject returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
//			returnValue = ktotoService.getKtotoBoardUserName();
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Ktoto Board User Name Data Error : {}", e.getMessage());
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/ktoto/board/getKtotoBoardReplyList", method={RequestMethod.GET, RequestMethod.POST})
	public void getKtotoBoardReplyList ( HttpServletResponse response, HttpServletRequest request
			                             , @RequestParam(required=false) String boardType
			                             , @RequestParam(required=false) String boardNo
			                             , @RequestParam(required=false) String startNum
			                             , @RequestParam(required=false) String endNum) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = ktotoService.getKtotoBoardReplyList(boardType, boardNo, startNum, endNum);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Ktoto Board Reply List Data Error : {}", e.getMessage());
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/ktoto/board/insertReply", method=RequestMethod.POST)
	public void insertReply ( HttpServletResponse response, HttpServletRequest request
			                  , @RequestParam(required=false) String boardType
			                  , @RequestParam(required=false) String boardNo 
			                  , @RequestParam(required=false) String content) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		try {
			ktotoService.insertReply(boardNo, boardType, content);
			msg.setSuccessText("저장 되었습니다");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Ktoto Insert Board Reply Data Error : {}", e.getMessage());
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/ktoto/board/updateReply", method=RequestMethod.POST)
	public void updateReply ( HttpServletResponse response, HttpServletRequest request
			                  , @RequestParam(required=false) String boardType
			                  , @RequestParam(required=false) String boardNo
			                  , @RequestParam(required=false) String pStep
			                  , @RequestParam(required=false) String content ) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		try {
			ktotoService.updateReply(boardNo, boardType, pStep, content);
			msg.setSuccessText("수정 되었습니다");
		} catch (Exception e) {
			logger.error("ERROR > Ktoto Update Board Reply Data Error : {} ", e.getMessage());
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/ktoto/board/removeReply", method=RequestMethod.POST)
	public void removeReply ( HttpServletResponse response, HttpServletRequest request 
			                  , @RequestParam(required=false) String boardType
			                  , @RequestParam(required=false) String boardNo
			                  , @RequestParam(required=false) String pStep ) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		try {
			ktotoService.removeReply(boardNo, boardType, pStep);
			msg.setSuccessText("삭제 되었습니다");
		} catch (Exception e) {
			logger.error("ERROR > Ktoto Remove Board Reply Data Error : {}" ,e.getMessage());
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
}
