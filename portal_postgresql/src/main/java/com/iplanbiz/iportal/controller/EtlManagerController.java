package com.iplanbiz.iportal.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.dto.Etl;
import com.iplanbiz.iportal.service.EtlManagerService;

@Controller
public class EtlManagerController {

	@Autowired EtlManagerService etlManagerService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/etl/etlManager/crud", method= RequestMethod.GET)
	public ModelAndView list () throws Exception {
		ModelAndView modelAndView = new ModelAndView("etl/etlManager/crud");
		
		return modelAndView;
	}
	@RequestMapping(value="/etl/etlManager/detail", method= RequestMethod.GET)
	public ModelAndView detail (@RequestParam(required=false, defaultValue="") String etlID) throws Exception {
		ModelAndView modelAndView = new ModelAndView("etl/etlManager/detail");
		if(!etlID.equals("")){
			modelAndView.addObject("detailList", etlManagerService.getEtlManagerDetail(etlID).get(0));
		}
		return modelAndView;
	}
	@RequestMapping(value="/etl/etlManager/getEtlManagerGbnList", method=RequestMethod.POST)
	public void getEtlManagerGbnList ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = etlManagerService.getEtlManagerGbnList();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Etl Manager Gbn List Data Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/etl/etlManager/getEtlManagerList",method=RequestMethod.POST)
	public void getEtlManagerList ( HttpServletResponse response, HttpServletRequest request
												,@RequestParam(required=false, defaultValue="") String gbn
												,@RequestParam(required=false, defaultValue="") String searchKey
												,@RequestParam(required=false, defaultValue= "") String searchValue
												,@RequestParam(required=false, defaultValue= "") String etlYn) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = etlManagerService.getEtlManagerList(gbn, searchKey, searchValue, etlYn);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Etl Manager List Data Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/etl/etlManager/getEtlManagerDetail", method=RequestMethod.POST)
	public void getEtlManagerDetail ( HttpServletResponse response, HttpServletRequest request
													,@RequestParam(required=false) String etlID ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = etlManagerService.getEtlManagerDetail(etlID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Etl Manager Detail Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/etl/etlManager/getEtlManagerDBInfoList", method=RequestMethod.POST)
	public void getDBInfoJsonArray ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = etlManagerService.getEtlManagerDBInfoList();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get DB Info List JSON Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/etl/etlManager/insertEtlManager", method=RequestMethod.POST)
	public void insertEtlManager ( HttpServletResponse response, HttpServletRequest request
											   ,@ModelAttribute Etl etl ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = etlManagerService.insertEtlManager(etl);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else if(returnValue == 1){
				msg.setSuccessText("수정 되었습니다");
			}else if(returnValue == 2){
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Insert / Update ETL Manager Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/etl/etlManager/removeEtlManager", method=RequestMethod.POST)
	public void removeEtlManager ( HttpServletResponse response, HttpServletRequest request
												 ,@ModelAttribute Etl etl ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = etlManagerService.removeEtlManager(etl);
			if(returnValue == 0){
				msg.setSuccessText("삭제 되었습니다");
			}else if(returnValue == 1){
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Remove ETL Manager Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
			
		}
		msg.send(response);
	}
	@RequestMapping(value="/etl/etlManager/callEtl", method={RequestMethod.POST})
	public void callEtl ( HttpServletResponse response, HttpServletRequest request 
								,@RequestParam(required=false) String etlID ) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		try{
			msg = etlManagerService.callEtl(etlID);
			if(msg.getReturnCode().equals("EXCEPTION"))
				msg.setExceptionText("ETL 작업 중 에러가 발생했습니다 : " + msg.getReturnText());
			else 
				msg.setSuccessText("ETL 작업이 완료되었습니다");
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Call Etl Error : {} " ,e);
			msg.setExceptionText("ETL 작업 중 에러가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/etl/etlManager/excelDownByEtl", method=RequestMethod.GET)
	public ModelAndView excelDownByEtl (@RequestParam(required=false, defaultValue="") String fileName
										,@RequestParam(required=false, defaultValue="") String gbn
										,@RequestParam(required=false, defaultValue="") String searchKey
										,@RequestParam(required=false, defaultValue= "") String searchValue
										,@RequestParam(required=false, defaultValue= "") String etlYn) throws Exception{
		ModelAndView modelAndView = new ModelAndView("/etl/etlManager/excelDownByEtl");
		modelAndView.addObject("fileName",fileName);
		modelAndView.addObject("mainList", etlManagerService.excelDownByEtl(gbn,searchKey,searchValue,etlYn));
		
		return modelAndView;
	}
}
