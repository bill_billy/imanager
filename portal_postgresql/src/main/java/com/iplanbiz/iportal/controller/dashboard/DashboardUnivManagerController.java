package com.iplanbiz.iportal.controller.dashboard;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.dashboard.DashboardUnivManagerService;

@Controller
public class DashboardUnivManagerController {

	@Autowired DashboardUnivManagerService univManagerService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/dashboard/univManager/crud", method=RequestMethod.GET)
	public ModelAndView crud () throws Exception{
		ModelAndView modelAndView = new ModelAndView("/dashboard/univManager/crud");
		modelAndView.addObject("scGubn", univManagerService.getDashboardUnivScGubn());
		return modelAndView;
	}
	@RequestMapping(value="/dashboard/univManager/popupMappingDashboardUniv", method=RequestMethod.GET)
	public ModelAndView popupMappingDashboardUniv() throws Exception{
		ModelAndView modelAndView = new ModelAndView("/dashboard/univManager/popupMappingDashboardUniv");
		
		return modelAndView;
	}
	@RequestMapping(value="/dashboard/univManager/getDashboardUnivList", method=RequestMethod.POST)
	public void getDashboardUnivList ( HttpServletResponse response, HttpServletRequest request
												  ,@RequestParam(required=false) String scID) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = univManagerService.getDashboardUnivList(scID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Dash board Univ Data Error : {}" , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/dashboard/univManager/getDashboardPopupUnivList", method=RequestMethod.POST)
	public void getDashboardPopupUnivList ( HttpServletResponse response, HttpServletRequest request
																,@RequestParam(required=false) String univName) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = univManagerService.getDashboardPopupUnivList(univName);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Dash board Popup Univ List Data Error : {}", e);
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/dashboard/univManager/insert", method=RequestMethod.POST)
	public void insertDashboardUniv ( HttpServletResponse response, HttpServletRequest request
													,@RequestParam(required=false) String scID
													,@RequestParam(required=false) String univID
													,@RequestParam(required=false) String univGrp1) throws Exception{
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = univManagerService.insertDashboardUniv(scID, univID, univGrp1);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Insert Dash board Univ Data Error : {}" , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/dashboard/univManager/remove", method=RequestMethod.POST)
	public void removeDashboardUniv ( HttpServletResponse response, HttpServletRequest request
													,@RequestParam(required=false) String scID
													,@RequestParam(required=false) String[] univID) throws Exception{
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = univManagerService.removeDashboardUniv(scID, univID);
			if(returnValue == 0){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Delete Dash board Univ Data Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
}
