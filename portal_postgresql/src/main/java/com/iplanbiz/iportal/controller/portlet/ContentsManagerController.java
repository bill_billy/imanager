package com.iplanbiz.iportal.controller.portlet;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 


import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dto.contentsManager;
import com.iplanbiz.iportal.service.portlet.ContentsManagerService;
import com.iplanbiz.iportal.service.admin.MenuService;
import com.iplanbiz.iportal.service.system.SystemService;

@Controller
public class ContentsManagerController {

	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired ContentsManagerService contentsManagerService;
	@Autowired MenuService menuService;
	@Autowired SystemService systemService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	@RequestMapping(value="/portlet/contentsManager/list", method=RequestMethod.GET)
	public ModelAndView list () throws Exception {
		ModelAndView modelAndView = new ModelAndView("/portlet/contentsManager/list");
		
		return modelAndView;
	}
	@RequestMapping(value="/portlet/contentsManager/detail", method=RequestMethod.GET)
	public ModelAndView detail ( @RequestParam(required=false, defaultValue="") String poID ) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/portlet/contentsManager/detail");
		
		if(!poID.equals("")){ 
			modelAndView.addObject("content", contentsManagerService.getPortletContentsDetail(poID).get(0));
		}else {
			poID="-1";
		}
		File fp = new File(WebConfig.getDefaultServerRoot() + "/resources/img/portletImage/"+Integer.valueOf(poID)+".gif");
		String imgExist = "false";
		if(fp.exists()) {
			imgExist = "true";
		} else {
			imgExist = "false";
		}
		modelAndView.addObject("portletImgExist", imgExist);
		modelAndView.addObject("portletGrpList" , contentsManagerService.getPortletGrpList());
		
		return modelAndView;
	}
	@RequestMapping(value="/portlet/contentsManager/getPortletContentsList", method=RequestMethod.POST)
	public void getPortletContentsList ( HttpServletResponse response, HttpServletRequest request 
													  ,@RequestParam(required=false) String portletGrp ) throws Exception {
		JSONArray returnValue = new JSONArray();
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = contentsManagerService.getPortletContentsList(portletGrp);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
			logger.error("Error : Get Portlet Contents List Data Error : {} " ,e);
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/contentsManager/getPortletGrpList", method=RequestMethod.POST)
	public void getPortletGrpList ( HttpServletResponse response , HttpServletRequest request ) throws Exception {
		JSONArray returnValue = new JSONArray();
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = contentsManagerService.getPortletGrpList();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR == > Get Portlet Grp List Data Error : {} " ,e);
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/contentsManager/getPortletContentsInfoData", method=RequestMethod.POST)
	public void getPortletContentsInfoData ( HttpServletResponse response, HttpServletRequest requset
			             									 ,@RequestParam(required=false) String poID ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = contentsManagerService.getPortletContentsInfoData(poID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("ERROR > Get Portlet Contents Info Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/contentsManager/insert", method=RequestMethod.POST)
	public void insert ( HttpServletResponse response, HttpServletRequest request
			                    , @ModelAttribute contentsManager contents) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = contentsManagerService.insert(contents);
			
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else if(returnValue == 1){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("ERROR > Insert & Update Contents Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		};
		msg.send(response);
	}
	@RequestMapping(value="/portlet/contentsManager/remove", method=RequestMethod.POST)
	public void deleteByContents ( HttpServletResponse response, HttpServletRequest request
												, @ModelAttribute contentsManager contents ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = contentsManagerService.deleteByContents(contents);
			if(returnValue == 0){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Delete Portlet Manager Contents Error : {} " , e);
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/contentsManager/imgLoader", method=RequestMethod.GET)
	public ModelAndView imgLoader ( @RequestParam(required=false) String poID ) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/portlet/contentsManager/imgLoader");
		
		modelAndView.addObject("poID", poID);
		
		return modelAndView;
	}
	@RequestMapping(value="/portlet/contentsManager/cogLoader", method=RequestMethod.GET)
	public ModelAndView cogLoader ( @RequestParam(required=false) String storeID ) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/portlet/contentsManager/cogLoader");
		
		LoginSessionInfo loginInfo = loginSessionInfoFactory.getObject();
		String browserType = loginInfo.getUserBrowserType();
		int browserVersion = Integer.parseInt(loginInfo.getUserBrowserVersion());
		
		boolean isIE8 = false;
		if((browserType.equals("IE") && browserVersion <= 8) || (browserType.equals("IE") && browserVersion == 10)){
			isIE8 = true;
		}
		boolean isView = false;
		if(browserType.equals("safari") || browserType.equals("chrome")){
			isView = true;
		}
		
		String cogReportUrl = menuService.getCognosReportURL(storeID, isView, isIE8);
		
		modelAndView.addObject("cogUrl", cogReportUrl);
		
		return modelAndView;
	}
	@RequestMapping(value="/portlet/contentsManager/rss", method= RequestMethod.GET)
	public ModelAndView rss ( HttpServletResponse response, HttpServletRequest request,@RequestParam(required=false) String url ) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/portlet/contentsManager/rss");
		try{
			modelAndView.addObject("rssUrl", url); 
			Map<String, Object> systemConfigMap = systemService.getSystemConfig(); 
			modelAndView.addObject("systemConfig", systemConfigMap);
		}catch(Exception e){
			e.printStackTrace();
		}
		return modelAndView;
	}
	
	@RequestMapping(value="/portlet/contentsManager/rss/read", method=RequestMethod.POST)
	public void getXmlfromUrl ( HttpServletResponse response, HttpServletRequest request, String url ) throws Exception {
		String contents = IOUtils.toString(new URL(url), Charset.forName("UTF-8"));
		String key = "encoding";
		String charset = "UTF-8";
		int index = contents.indexOf(key);
		if(index>-1) {//encoding이 존재하면
			String encoding = contents.substring(index+key.length()+2);
			int endIndex = encoding.indexOf("\"");
			if(endIndex>-1)
				charset = encoding.substring(0,endIndex);
		}
		if(!charset.equals("UTF-8")) {//정해진 charset으로 xml다시읽어오기
			contents = IOUtils.toString(new URL(url), Charset.forName(charset));
			byte[] contentB = contents.getBytes("UTF-8");
			contents = new String(contentB);
		}
		try {
			response.setContentType("text/xml;charset=UTF-8");
			response.getWriter().write(contents);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} 
	

}
