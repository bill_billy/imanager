package com.iplanbiz.iportal.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.DayConnectionService;

@Controller
public class DayConnectionController {

	@Autowired
	DayConnectionService dayService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	@RequestMapping(value="/connectionStatus/dayConnection/list", method=RequestMethod.GET)
	public ModelAndView list () throws Exception{
		ModelAndView modelAndView = new ModelAndView("/connectionStatus/dayConnection/list");
		
		return modelAndView;
	}
	@RequestMapping(value="/connectionStatus/dayConnection/getDayConnectionUserList", method=RequestMethod.POST)
	public void getDayConnectionUserList ( HttpServletResponse response, HttpServletRequest request
															 ,@RequestParam(required=false) String startDat
															 ,@RequestParam(required=false) String endDat) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dayService.getDayConnectionUserList(startDat, endDat);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Day Connection Detail Data Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/connectionStatus/dayConnection/getDayConnectionReportList", method=RequestMethod.POST)
	public void getDayConnectionReportList ( HttpServletResponse response, HttpServletRequest request
													  			,@RequestParam(required=false) String startDat
													  			,@RequestParam(required=false) String endDat
													  			,@RequestParam(required=false) String userID) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dayService.getDayConnectionReportList(startDat, endDat, userID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Day Connection Data Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다");
		}
		msg.send(response);
	}
	

}
