package com.iplanbiz.iportal.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.MonthUserConnectionService;

@Controller
public class MonthUserConnectionController {

	@Autowired MonthUserConnectionService monthUserService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	@RequestMapping(value="/connectionStatus/monthUserConnection/list", method= RequestMethod.GET)
	public ModelAndView list () throws Exception{
		ModelAndView modelAndView = new ModelAndView("/connectionStatus/monthUserConnection/list");
		
		return modelAndView;
	}
	@RequestMapping(value="/connectionStatus/monthUserConnection/popupMonthUserDetail", method=RequestMethod.GET)
	public ModelAndView popupMonthUserDetail () throws Exception {
		ModelAndView modelAndView = new ModelAndView("/connectionStatus/monthUserConnection/popupMonthUserDetail");
		
		return modelAndView;
	}
	@RequestMapping(value="/connectionStatus/monthUserConnection/excelDownByUser", method=RequestMethod.GET)
	public ModelAndView excelDownByUser( @RequestParam(required=false) String year) throws Exception{
		ModelAndView modelAndView = new ModelAndView("/connectionStatus/monthUserConnection/excelDownByUser");
		
		modelAndView.addObject("mainList", monthUserService.getMonthUserConnectionListByExcel(year));
		
		return modelAndView;
	}
	@RequestMapping(value="/connectionStatus/monthUserConnection/excelDownByUserReport", method= RequestMethod.GET)
	public ModelAndView excelDownByUserReport( @RequestParam(required=false) String year
			                                                             ,@RequestParam(required=false) String userID) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/connectionStatus/monthUserConnection/excelDownByUserReport");
		
		modelAndView.addObject("mainList", monthUserService.getMonthUserConnectionPopupListByExcel(year, userID));
		
		return modelAndView;
	}
	@RequestMapping(value="/connectionStatus/monthUserConnection/getMonthUserYearList", method=RequestMethod.POST)
	public void getMonthUserYearList ( HttpServletResponse response, HttpServletRequest requeset) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = monthUserService.getMonthUserYearList();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Month User Data Error : {} ", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/connectionStatus/monthUserConnection/getMonthUserConnectionTotalList", method=RequestMethod.POST)
	public void getMonthUserConnectionTotalList ( HttpServletResponse response, HttpServletRequest request
																  ,@RequestParam(required=false) String year) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = monthUserService.getMonthUserConnectionTotalList(year);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Month User Connection Total Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/connectionStatus/monthUserConnection/getMonthUserConnectionListByUser", method=RequestMethod.POST)
	public void getMonthUserConnectionListByUser ( HttpServletResponse response , HttpServletRequest request
																,@RequestParam(required=false) String year) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = monthUserService.getMonthUserConnectionListByUser(year);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Month User Connection List Data Error : {}" , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/connectionStatus/monthUserConnection/getMonthUserConnectionPopupList", method=RequestMethod.POST)
	public void getMonthUserConnectionPopupList ( HttpServletResponse response, HttpServletRequest request
																		, @RequestParam(required=false) String year
																		, @RequestParam(required=false) String userID) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = monthUserService.getMonthUserConnectionPopupList(year, userID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Month User Connection Popup List Data Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
}
