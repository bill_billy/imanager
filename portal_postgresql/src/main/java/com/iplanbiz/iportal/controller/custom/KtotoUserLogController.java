package com.iplanbiz.iportal.controller.custom;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.dao.admin.UserDAO;
import com.iplanbiz.iportal.dto.KtotoUser;
import com.iplanbiz.iportal.service.CodeService;
import com.iplanbiz.iportal.service.RoleService;
import com.iplanbiz.iportal.service.admin.UserService;
import com.iplanbiz.iportal.service.custom.KtotoUserLogService;
import com.iplanbiz.iportal.service.custom.KtotoUserService;
import com.iplanbiz.core.secure.GeneratePassword;
@Controller
public class KtotoUserLogController {
	
	@Autowired
	KtotoUserLogService ktotoUserLogService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/custom/ktotoUserLog/list", method=RequestMethod.GET)
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView("/custom/ktotoUserLog/list");
		return modelAndView;
	}
	@RequestMapping(value="/custom/ktotoUserLog/getUserList", method=RequestMethod.POST)
	public void getUserList(HttpServletResponse response, HttpServletRequest request
			,@RequestParam(required=false) String team,@RequestParam(required=false) String userNm){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = ktotoUserLogService.getUserList(team,userNm);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("ERROR : {}", e);
			msg.setExceptionText("데이터 로딩중 Error" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/custom/ktotoUserLog/getUserLogList", method=RequestMethod.POST)
	public void getUserLogList(HttpServletResponse response, HttpServletRequest request,@RequestParam(required=false) String unid){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = ktotoUserLogService.getUserLogList(unid);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("ERROR : {}", e);
			msg.setExceptionText("데이터 로딩중 Error" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/custom/ktotoUserLog/excelDownByUserLog", method= RequestMethod.GET)
	public ModelAndView excelDownByUserInfo ( @RequestParam(required=false) String unid) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/custom/ktotoUserLog/excelDownByUserLog");
		
		modelAndView.addObject("mainList", ktotoUserLogService.getUserLogListForExcel(unid));
		
		return modelAndView;
	}
	
}