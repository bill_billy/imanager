package com.iplanbiz.iportal.controller.portlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage; 
import com.iplanbiz.iportal.service.portlet.PortletGroupManagerService;

@Controller
public class PortletGroupManagerController {

	@Autowired PortletGroupManagerService portletGroupManagerService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	@RequestMapping(value="/portlet/portletGroupManager/crud", method=RequestMethod.GET)
	public ModelAndView crud () throws Exception {
		ModelAndView modelAndView = new ModelAndView("/portlet/portletGroupManager/crud");
		
		return modelAndView;
	}
	@RequestMapping(value="/portlet/portletGroupManager/getPortletGroupManagerList", method=RequestMethod.POST)
	public void getPortletGroupManagerList ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletGroupManagerService.getPortletGroupManagerList();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Error => Get Portlet Group Manager List Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletGroupManager/getPortletGroupDataDetail", method=RequestMethod.POST)
	public void getPortletGroupDataDetail ( HttpServletResponse response, HttpServletRequest request
															,@RequestParam(required=false) String unID ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletGroupManagerService.getPortletGroupDataDetail(unID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Error => Get Portlet Group Detail Data Error : {} ", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletGroupManager/insert", method=RequestMethod.POST)
	public void insertPortletGroup ( HttpServletResponse response, HttpServletRequest request
											  ,@RequestParam(required=false, defaultValue = "") String unID
											  ,@RequestParam(required=false, defaultValue = "") String grpName
											  ,@RequestParam(required=false, defaultValue = "") String etc ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletGroupManagerService.insertPortletGroup(unID, grpName, etc);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else if(returnValue == 1){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Error => Inset/Update Portlet Group Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/portletGroupManager/remove", method={RequestMethod.POST})
	public void deletePortletGroup ( HttpServletResponse response, HttpServletRequest request 
												  ,@RequestParam(required=false, defaultValue = "") String[] arrayDeleteUnID ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletGroupManagerService.deletePortletGroup(arrayDeleteUnID);
			if(returnValue == 0){
				msg.setSuccessText("삭제 되었습니다");
			}else if(returnValue == 1){
				msg.setExceptionText("삭제 할 항목이 없습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("ERROR => Delete Portlet Group Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
}
