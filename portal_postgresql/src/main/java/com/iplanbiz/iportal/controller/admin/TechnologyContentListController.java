package com.iplanbiz.iportal.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.service.admin.MenuContentManagerService;
import com.iplanbiz.iportal.service.admin.TechnologyContentListService;

@Controller
public class TechnologyContentListController {
	@Autowired TechnologyContentListService technologyService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	//기술 백서
	@RequestMapping(value="/admin/technologyContentList/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView modelAndView = new ModelAndView("/admin/technologyContentList/list");
	   	return modelAndView;
	}
	@RequestMapping(value="/admin/technologyContentManager/getTechnoContentList", method = RequestMethod.POST)
	public void getTechnoContentList( HttpServletResponse response, HttpServletRequest request) throws Exception{
		AjaxMessage msg = new AjaxMessage();
		JSONArray returnValue = null;
		try{
			returnValue = technologyService.getTechnoContentList();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("ERROR:{}", e);
			e.printStackTrace();
			msg.setExceptionText("에러가 발생했습니다 \n" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/technologyContentManager/getLabelTechnoContent", method = RequestMethod.POST)
	public void getLabelTechnoContent( HttpServletResponse response, HttpServletRequest request
			,@RequestParam(required=false) String cid) throws Exception{
		AjaxMessage msg = new AjaxMessage();
		JSONArray returnValue = null;
		try{
			returnValue = technologyService.getLabelTechnoContent(cid);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("ERROR:{}", e);
			e.printStackTrace();
			msg.setExceptionText("에러가 발생했습니다 \n" + e.getMessage());
		}
		msg.send(response);
	}	
	@RequestMapping(value="/admin/technologyContentManager/getTechnoContentInfo", method = RequestMethod.POST)
	public void getTechnoContentInfo( HttpServletResponse response, HttpServletRequest request
			,@RequestParam(required=false) String cid) throws Exception{
		AjaxMessage msg = new AjaxMessage();
		JSONArray returnValue = null;
		try{
			returnValue = technologyService.getTechnoContentInfo(cid);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("ERROR:{}", e);
			e.printStackTrace();
			msg.setExceptionText("에러가 발생했습니다 \n" + e.getMessage());
		}
		msg.send(response);
	}		
	@RequestMapping(value="/admin/technologyContentManager/getSearchTechnoContent", method = RequestMethod.POST)
	public void getSearchTechnoContent( HttpServletResponse response, HttpServletRequest request
			,@RequestParam(required=false) String content_text) throws Exception{
		AjaxMessage msg = new AjaxMessage();
		JSONArray returnValue = null;
		try{
			returnValue = technologyService.getSearchTechnoContent(content_text);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("ERROR:{}", e);
			e.printStackTrace();
			msg.setExceptionText("에러가 발생했습니다 \n" + e.getMessage());
		}
		msg.send(response);
	}	
}
