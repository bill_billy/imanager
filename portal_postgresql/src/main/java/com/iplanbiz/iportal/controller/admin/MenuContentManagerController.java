package com.iplanbiz.iportal.controller.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.service.admin.MenuContentManagerService;

@Controller
public class MenuContentManagerController {
	@Autowired MenuContentManagerService menuContentManagerService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	//메뉴얼 관리
		@RequestMapping(value="/admin/menuContentManager/list", method = RequestMethod.GET)
	    public ModelAndView list() {
			ModelAndView modelAndView = new ModelAndView("/admin/menuContentManager/list");
	    	return modelAndView;
	    }
		
		@RequestMapping(value="/admin/menuContentManager/getSolutionListCbo", method=RequestMethod.POST)
		public void getSolutionListCbo( HttpServletResponse response, HttpServletRequest request) throws Exception{
			AjaxMessage msg = new AjaxMessage();
			JSONArray returnValue = null;
			try{
				returnValue = menuContentManagerService.getSolutionListCbo();
				msg.setSuccessMessage(returnValue);
			}catch(Exception e){
				logger.error("ERROR:{}", e);
				e.printStackTrace();
				msg.setExceptionText("에러가 발생했습니다 \n" + e.getMessage());
			}
			msg.send(response);
		}
		@RequestMapping(value="/admin/menuContentManager/getMenuContentList", method=RequestMethod.POST)
		public void getMenuContentList( HttpServletResponse response, HttpServletRequest request
				,@RequestParam(required=false) String menualId
				,@RequestParam(required=false) String searchType
				,@RequestParam(required=false) String searchValue) throws Exception{
			AjaxMessage msg = new AjaxMessage();
			JSONArray returnValue = null;
			try{
				returnValue = menuContentManagerService.getMenuContentList(menualId,searchType,searchValue);
				msg.setSuccessMessage(returnValue);
			}catch(Exception e){
				logger.error("ERROR:{}", e);
				e.printStackTrace();
				msg.setExceptionText("에러가 발생했습니다 \n" + e.getMessage());
			}
			msg.send(response);
		}
		@RequestMapping(value="/admin/menuContentManager/getMenuContenGridtList", method=RequestMethod.POST)
		public void getMenuContenGridtList( HttpServletResponse response, HttpServletRequest request
				,@RequestParam(required=false) String menualId
				,@RequestParam(required=false) String searchType
				,@RequestParam(required=false) String searchValue) throws Exception{
			AjaxMessage msg = new AjaxMessage();
			JSONArray returnValue = null;
			try{
				returnValue = menuContentManagerService.getMenuContenGridtList(menualId,searchType,searchValue);
				msg.setSuccessMessage(returnValue);
			}catch(Exception e){
				logger.error("ERROR:{}", e);
				e.printStackTrace();
				msg.setExceptionText("에러가 발생했습니다 \n" + e.getMessage());
			}
			msg.send(response);
		}
		@RequestMapping(value="/admin/menuContentManager/getMenuContentInfo", method=RequestMethod.POST)
		public void getMenuContentInfo( HttpServletResponse response, HttpServletRequest request
				,@RequestParam(required=false) String cid) throws Exception{
			AjaxMessage msg = new AjaxMessage();
			JSONArray returnValue = null;
			try{
				returnValue = menuContentManagerService.getMenuContentInfo(cid);
				msg.setSuccessMessage(returnValue);
			}catch(Exception e){
				logger.error("ERROR:{}", e);
				e.printStackTrace();
				msg.setExceptionText("에러가 발생했습니다 \n" + e.getMessage());
			}
			msg.send(response);
		}
		@RequestMapping(value="/admin/menuContentManager/insert",  method=RequestMethod.POST)
		public void insert(HttpServletResponse response, HttpServletRequest request
														,@RequestParam(required=false) String solutionList
				                                        ,@RequestParam(required=false) String progNm
														 ,@RequestParam(required=false) String progId 
														 ,@RequestParam(required=false) String localCd 
														 ,@RequestParam(required=false) String parentId 
														 ,@RequestParam(required=false) String content )  throws Exception{
			AjaxMessage msg = new AjaxMessage();
			int result = 0;
			try {
				result = menuContentManagerService.insertMenuContent(solutionList, progNm, progId, localCd, parentId, content);
				if(result == 1){
					msg.setSuccessText("저장 되었습니다.");
				}else if(result == 2){
					msg.setSuccessText("수정 되었습니다.");
				}else{
					msg.setExceptionText("저장 중 오류가 발생했습니다");
				}
			} catch (Exception e) {
				msg.setExceptionText("저장 중 오류가 발생했습니다 : "+e.getMessage());
				logger.error("error : {}",e);
			}
			msg.send(response);
			
	    }
		
		@RequestMapping(value="/admin/menuContentManager/delete",  method=RequestMethod.POST)
		public void delete(HttpServletResponse response, HttpServletRequest request
														 ,@RequestParam(required=false) String solutionList
														 ,@RequestParam(required=false) String progId 
														 ,@RequestParam(required=false) String parentId )  throws Exception{
			AjaxMessage msg = new AjaxMessage();
			int result = 0;
			try{
					result = menuContentManagerService.deleteMenuContent(solutionList, progId, parentId);
			}catch(Exception e){
				msg.setExceptionText("삭제 중 오류가 발생했습니다 : " + e.toString());
				logger.error("error : {}",e);
			}
			msg.send(response);
	    }	

}
