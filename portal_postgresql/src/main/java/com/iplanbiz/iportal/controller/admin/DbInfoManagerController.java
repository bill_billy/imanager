package com.iplanbiz.iportal.controller.admin;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.io.butler.Proxy;
import com.iplanbiz.core.io.butler.ProxyPool;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.dto.DbInfo;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.CodeService;
import com.iplanbiz.iportal.service.admin.DbInfoManagerService;

@Controller
public class DbInfoManagerController {

	@Autowired DbInfoManagerService dbInfoService;
	@Autowired CodeService codeService;
	@Resource(name="proxyPool") ProxyPool proxyPool;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	@RequestMapping(value="/admin/dbinfo/crud", method= RequestMethod.GET)
	public ModelAndView crud () throws Exception {
		ModelAndView modelAndView = new ModelAndView("/admin/dbinfo/crud");
		
		return modelAndView;
	}
	@RequestMapping(value="/admin/dbinfo/detail", method=RequestMethod.GET)
	public ModelAndView detail ( @RequestParam(required=false, defaultValue="") String dbKey ) throws Exception{
		ModelAndView modelAndView = new ModelAndView("/admin/dbinfo/detail");
		if(!dbKey.equals("")){
			modelAndView.addObject("detailData", dbInfoService.getDbInfoDetail(dbKey).get(0));
		}
		return modelAndView;
	}
	@RequestMapping(value="/admin/dbinfo/getDbInfoList", method=RequestMethod.POST)
	public void getDbInfoList ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dbInfoService.getDbInfoList();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Db Info List Data Error : {} " ,e);
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/dbinfo/getDbInfoDetail", method=RequestMethod.POST)
	public void getDbInfoDetail ( HttpServletResponse response, HttpServletRequest request 
			                                  ,@RequestParam(required=false) String dbKey) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dbInfoService.getDbInfoDetail(dbKey);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Get DB Info Detail Data Error : {} " ,e);
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/dbinfo/getDbInfoCodeList", method=RequestMethod.POST)
	public void getDbInfoCodeList ( HttpServletResponse response, HttpServletRequest request
												 ,@RequestParam(required=false) String comlCod ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = codeService.getCodeListByAsc(comlCod);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Db Info Code List Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/dbinfo/insert", method=RequestMethod.POST)
	public void insertDbInfo ( HttpServletResponse response, HttpServletRequest request
										, @ModelAttribute DbInfo dbinfo) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dbInfoService.insertDbInfo(dbinfo);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else if(returnValue == 1){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("DB Info Insert/Update Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/dbinfo/remove", method=RequestMethod.POST)
	public void removeDbInfo ( HttpServletResponse response, HttpServletRequest request 
											,@ModelAttribute DbInfo dbinfo ) throws Exception{
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dbInfoService.removeDbInfo(dbinfo);
			if(returnValue == 0){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("DB Info Delete Error : {} ", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/dbinfo/checkDbKey", method=RequestMethod.POST)
	public void checkDbKey ( HttpServletResponse response, HttpServletRequest request
			                            ,@RequestParam(required=false, defaultValue="") String dbKey ) throws Exception {
		String returnValue = "";
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = dbInfoService.checkDbKey(dbKey);
			if(returnValue.equals("true")){
				msg.setSuccessText("해당 DB Key가 존재 하지 않습니다");
			}else{
				msg.setExceptionText("해당 DB Key 가 존재합니다");
			}
		}catch(Exception e){
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
			logger.error("ERROR > DB Key Check Error : {} " ,e);
			
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/dbinfo/checkDB", method=RequestMethod.POST)
	public void checkDB ( HttpServletResponse response, HttpServletRequest request
			                       ,@RequestParam(required=false) String dbKey ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dbInfoService.checkDbInfo(dbKey);
			if(returnValue == 0){
				msg.setSuccessText("접속 확인되었습니다");
				JSONObject param = new JSONObject();
				param.put("DBKEY", dbKey);
				try {
					Proxy proxy = proxyPool.createProxy();
					proxy.start();
					proxy.requestRule("dbmsMaster",  "Register", param);
					proxy.stop();
				}catch(Exception e1) {
					logger.error("butler접속 오류:",e1);
				}
			}else {
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
			logger.error("ERROR : Check DB Error : {}", e);
		}
		msg.send(response);
	}
	
}
