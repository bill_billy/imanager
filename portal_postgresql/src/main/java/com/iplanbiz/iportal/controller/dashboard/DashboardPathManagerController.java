package com.iplanbiz.iportal.controller.dashboard;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.dashboard.DashboardPathManagerService;

@Controller
public class DashboardPathManagerController {

	@Autowired DashboardPathManagerService pathService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/dashboard/pathManager/crud", method=RequestMethod.GET)
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView("/dashboard/pathManager/crud");
		
		return modelAndView;
	}
	@RequestMapping(value="/dashboard/pathManager/getDashboardPathList", method=RequestMethod.POST)
	public void getDashboardPathList ( HttpServletResponse response, HttpServletRequest request) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = pathService.getDashboardPathList();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get DashBoard Path Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/dashboard/pathManager/getDashboardPathDetail", method=RequestMethod.POST)
	public void getDashboardPathDetail ( HttpServletResponse response, HttpServletRequest request
														 , @RequestParam(required=false) String pathIdx) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = pathService.getDashboardPathDetail(pathIdx);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Dashboard Path Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/dashboard/pathManager/insert", method=RequestMethod.POST)
	public void insertDashboardPath ( HttpServletResponse response, HttpServletRequest request
													,@RequestParam(required=false) String pathIdx
													,@RequestParam(required=false) String fullPath
													,@RequestParam(required=false) String pathDesc
													,@RequestParam(required=false) String bigo
													,@RequestParam(required=false) String useYn) throws Exception{
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = pathService.insertDashboardPath(pathIdx, fullPath, pathDesc, bigo, useYn);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else if(returnValue == 1){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Insert & Update Dash board Path Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다");
		}
		msg.send(response);
	}
	@RequestMapping(value="/dashboard/pathManager/remove", method=RequestMethod.POST)
	public void removeDashboardPath ( HttpServletResponse response, HttpServletRequest request
														,@RequestParam(required=false) String[] pathIdx) throws Exception{
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = pathService.removeDashboardPath(pathIdx);
			if(returnValue == 0){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Remove Dash board Path Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
}
