package com.iplanbiz.iportal.controller.system;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.dto.IECT7002;
import com.iplanbiz.iportal.service.system.PortalSettingService;
import com.iplanbiz.iportal.service.system.SystemService;



@Controller
public class PortalSettingController {


@Autowired
SystemService systemService;

@Autowired
PortalSettingService portalService;
/*
 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
 *
 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
 *	
 *  구분 | 메소드	 | 		메소드명
	생성 : POST 			/insert
	수정 : POST 			/update
	조회 : GET,POST 		/list
	조회 : GET,POST 		/select(ajax) 
	삭제 : POST 			/remove
 *
 */
	
	@RequestMapping(value="/system/portalSetting/crud", method={RequestMethod.GET})
	public ModelAndView create(HttpServletResponse response) throws Exception{
		ModelAndView mview = new ModelAndView(); 
		
		Map<String, Object> systemConfigMap = systemService.getSystemConfig();
		
		mview.addObject("systemConfig", systemConfigMap);
		
		mview.setViewName("system/portalSetting/crud");
		
		return mview;
	} 
	@RequestMapping(value="/system/portalSetting/update", method={RequestMethod.POST})
	public void update(HttpServletResponse response , HttpServletRequest request,
			@RequestParam(required=false)String keyValues, @RequestParam(required=false) String keyNames) throws Exception {
		
		AjaxMessage msg = new AjaxMessage();
		
		try{
			systemService.setSystem(keyValues, keyNames);
			systemService.getSystemConfig(true);
			msg.setSuccessText("success");
		}catch(Exception e){
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/system/portalSetting/file", method=RequestMethod.POST)
	public String portalSettingFile(@ModelAttribute IECT7002 iect7002) throws IOException{
		int result = portalService.savePortalSettingFile(iect7002);
		return null;
	}

	 
}
