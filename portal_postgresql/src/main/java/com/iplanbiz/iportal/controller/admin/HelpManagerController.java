package com.iplanbiz.iportal.controller.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.admin.HelpManagerService;

@Controller
public class HelpManagerController {

	@Autowired HelpManagerService helpManagerService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/admin/help/crud", method=RequestMethod.GET)
	public ModelAndView crud (){
		ModelAndView modelAndView = new ModelAndView("/admin/help/crud");
		
		return modelAndView;
	}
	@RequestMapping(value="/admin/help/crudByUser", method=RequestMethod.GET)
	public ModelAndView crudByUser(){
		ModelAndView modelAndView = new ModelAndView("/admin/help/crudByUser");
		
		return modelAndView;
	}
	@RequestMapping(value="/admin/help/helpManagerMenuList", method=RequestMethod.POST)
	public void helpManagerMenuList( HttpServletResponse response, HttpServletRequest request) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = helpManagerService.helpManagerMenuList();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Help Manager Menu Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/help/admin/helpMenuListByUser", method=RequestMethod.POST)
	public void helpManagerMenuListByUser ( HttpServletResponse response, HttpServletRequest request) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = helpManagerService.helpManagerMenuListByUser();
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("Get Help Manager Menu Data By User Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/help/getHelpManagerMenuGrid", method=RequestMethod.POST)
	public void getHelpManagerMenuGrid( HttpServletResponse response, HttpServletRequest request
															,@RequestParam(required=false, defaultValue = "") String searchType
															,@RequestParam(required=false, defaultValue = "") String searchValue) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = helpManagerService.getHelpManagerMenuGrid(searchType, searchValue);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get HelpManager Menu Grid Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/help/getHelpManagerMenuDetail", method=RequestMethod.POST)
	public void getHelpManagerMenuDetail(HttpServletResponse response, HttpServletRequest request
															 , @RequestParam(required=false) String cID) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = helpManagerService.getHelpManagerMenuDetail(cID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Help Manager Menu Detail Data Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/help/getHelpManagerMenuDetailByUser", method=RequestMethod.POST)
	public void getHelpManagerMenuDetailByUser ( HttpServletResponse response, HttpServletRequest request
			                                     , @RequestParam(required=false) String cID ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = helpManagerService.getHelpManagerMenuDetailByUser(cID);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("Get Help Manager Menu Detail By User Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/help/insert", method=RequestMethod.POST)
	public void insertHelpManager( HttpServletResponse response, HttpServletRequest request
												 , @RequestParam(required=false) String cID
												 , @RequestParam(required=false, defaultValue = "") String summaryContent
												 , @RequestParam(required=false, defaultValue = "") String content
												 , @RequestParam(required=false, defaultValue = "") String etcContent
												 , @RequestParam(required=false, defaultValue = "") String deptCd
												 , @RequestParam(required=false, defaultValue = "") String dept
												 , @RequestParam(required=false, defaultValue = "") String chargeDept
												 , @RequestParam(required=false, defaultValue = "") String chargeEmp
												 , @RequestParam(required=false, defaultValue = "") String confirmDept
												 , @RequestParam(required=false, defaultValue = "") String confirmEmp
												 , @RequestParam(required=false, defaultValue = "") String updatePer) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = helpManagerService.insertHelpManager(cID, summaryContent, content, etcContent, dept, deptCd, chargeDept, chargeEmp, confirmDept, confirmEmp, updatePer);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else if(returnValue == 1){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Insert Help Manager Menu Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/help/insertByUser", method={RequestMethod.POST})
	public void insertHelpManagerByUser ( HttpServletResponse response, HttpServletRequest request
			                              , @RequestParam(required=false) String cID
			                              , @RequestParam(required=false, defaultValue="") String content 
			                              , @RequestParam(required=false, defaultValue="") String chargeEmp
			                              , @RequestParam(required=false, defaultValue="") String confirmEmp ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = helpManagerService.insertHelpManagerByUser(cID, content, chargeEmp, confirmEmp);
			if(returnValue == 0) {
				msg.setSuccessText("저장 되었습니다");
			}else {
				msg.setExceptionText("오류가 발생했습니다");
			}
		} catch (Exception e) {
			logger.error("Insert Help Manager Menu Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/help/remove", method=RequestMethod.POST)
	public void deleteHelpManagerMenu ( HttpServletResponse response, HttpServletRequest request
														   ,@RequestParam(required=false) String cID) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = helpManagerService.deleteHelpManagerMenu(cID);
			if(returnValue == 0){
				msg.setSuccessText("삭제되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");;
			}
		}catch(Exception e){
			logger.error("Delete Help Manager Menu Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/help/helpPopup", method=RequestMethod.GET)
	public ModelAndView helpPopup ( HttpServletResponse response, HttpServletRequest request
													 ,@RequestParam(required=false) String cID ) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/admin/help/helpPopup");
		
		modelAndView.addObject("help", helpManagerService.getHelpPopup(cID));
		
		return modelAndView;
	}
	@RequestMapping(value="/admin/help/getNaviByHelpManager", method=RequestMethod.POST)
	public void getNaviByHelpManager ( HttpServletResponse response, HttpServletRequest request
									   ,@RequestParam(required=false) String cID ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = helpManagerService.getNaviByHelpManager(cID);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Navi By Help Manager Data Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/help/popup", method= RequestMethod.GET)
	public ModelAndView popup ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/admin/help/popup");
		
		return modelAndView;
	}
	@RequestMapping(value="/admin/help/getPopHelpManagerList", method=RequestMethod.POST)
	public void getPopHelpManagerList ( HttpServletResponse response, HttpServletRequest request
									    ,@RequestParam(required=false) String userGrpNm ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = helpManagerService.getPopHelpManagerList(userGrpNm);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Popup Help Manage List Data Error : {}", e);
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/help/popupEmp", method=RequestMethod.GET)
	public ModelAndView popupEmp ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/admin/help/popupEmp");
		
		return modelAndView;
	}
}
