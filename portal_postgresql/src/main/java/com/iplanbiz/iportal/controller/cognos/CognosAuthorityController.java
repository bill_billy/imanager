package com.iplanbiz.iportal.controller.cognos;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.SecurityUtil;
import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.comm.cognos.CRNConnect;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.cognos.CognosAuthorityService;

@Controller
public class CognosAuthorityController {

	@Autowired
	CognosAuthorityService cognosService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	@RequestMapping(value="/cognos/auth/crud", method=RequestMethod.GET)
	public ModelAndView list ( HttpServletResponse response, HttpServletRequest request) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/cognos/auth/crud");
		
		return modelAndView;
	}
	@RequestMapping(value="/cognos/auth/getMenuListByCognos", method=RequestMethod.POST)
	public void getMenuListByCognos ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		String userID = loginSessionInfoFactory.getObject().getUserId();
		try {
			returnValue = cognosService.getMenuListByCognos(userID);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Menu List By Cognos Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 "  + e.getMessage());
		}
		msg.send(response);
	}
	
 
	@RequestMapping(value="/cognos/auth/getCognosChildListByArray", method=RequestMethod.POST)
	public void getCognosChildListByArray ( HttpServletResponse response, HttpServletRequest request
			                                , @RequestParam(required=false) String cID
			                                , @RequestParam(required=false) String cName
			                                , @RequestParam(required=false) String clink
			                                , @RequestParam(required=false) String ac ) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		JSONArray returnValue =null;
		CRNConnect connect = (CRNConnect) loginSessionInfoFactory.getObject().getExternalInfo("connect");
		String userID = loginSessionInfoFactory.getObject().getUserId();
		if(connect!=null) {
			logger.info("Connect = " + Utils.getString(connect.toString(), "").replaceAll("[\r\n]",""));
			logger.info("connect LoginSessionInfoFactory = " + Utils.getString(loginSessionInfoFactory.getObject().getExternalInfo("connect").toString(), "").replaceAll("[\r\n]",""));
			clink = SecurityUtil.unEscape(clink);
			logger.info("cLink:{}",clink);
			try {
				
				returnValue = cognosService.getCognosChildListByArray(cID, clink, connect, ac);
				msg.setSuccessMessage(returnValue);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		msg.send(response);
	}
	@RequestMapping(value="/cognos/auth/getSearchPathUseStoreID", method=RequestMethod.POST)
	public void getSearchPathUseStoreID ( HttpServletResponse reponse, HttpServletRequest request , @RequestParam(required=true) String storeID) throws Exception {
		String returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			JSONObject value = new JSONObject();
			returnValue = cognosService.getSearchPathUseStoreID(storeID);
			value.put("returnValue", returnValue);
			msg.setSuccessMessage(value);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(reponse);
	}
	@RequestMapping(value="/cognos/auth/getReportPath", method=RequestMethod.POST)
	public void getReportPath ( HttpServletResponse response, HttpServletRequest request
			                          , @RequestParam(required=false) String searchPath ) throws Exception {
		JSONObject returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		 
		try {		
			searchPath = SecurityUtil.unEscape(searchPath);
			logger.info("aft searchPath:{}",searchPath);
			returnValue = cognosService.getReportPath(searchPath);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/cognos/auth/getMenuByJSON", method=RequestMethod.POST)
	public void getMenuByJSON ( HttpServletResponse response, HttpServletRequest request
			                         , @RequestParam(required=false) String storeID
			                         , @RequestParam(required=false) String menuName ) throws Exception {
		JSONObject returnValue = new JSONObject();
		AjaxMessage msg = new AjaxMessage();
		List<Map<String, Object>> list = null;
		try {
			list = cognosService.getMenuByJSON(storeID, menuName);
			returnValue.put("VALUE", list);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/cognos/auth/getDataMenuPermissionByCognos", method=RequestMethod.POST)
	public void getDataMenuPermissionByCognos ( HttpServletResponse response, HttpServletRequest request
			                                    , @RequestParam(required=false) String cID ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = cognosService.getDataMenuPermissionByCognos(cID);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Data Menu Permission By Cognos Error : {} ", e);
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/cognos/auth/getUserDataByCognos", method=RequestMethod.POST)
	public void getUserDataByCognos ( HttpServletResponse response, HttpServletRequest request
			                          , @RequestParam(required=false) String cID ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = cognosService.getUserDataByCognos(cID);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get User Data By Cognos Error : {}", e);
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/cognos/auth/getGroupDataByCognos", method=RequestMethod.POST)
	public void getGroupDataByCognos ( HttpServletResponse response, HttpServletRequest request
			                           , @RequestParam(required=false) String cID ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = cognosService.getGroupDataByCognos(cID);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Group Data By Cognos Error : {}", e);
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/cognos/auth/getRoleDataByCognos", method=RequestMethod.POST)
	public void getRoleDataByCognos ( HttpServletResponse response, HttpServletRequest request
			                          , @RequestParam(required=false) String cID ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = cognosService.getRoleDataByCognos(cID);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Role Data By Cognos Error : {}", e);
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/cognos/auth/insert", method=RequestMethod.POST)
	public void insert ( HttpServletResponse response, HttpServletRequest request
			             , @RequestParam(required=false) String cID
			             , @RequestParam(required=false) String userPermission
			             , @RequestParam(required=false) String ac ) throws Exception {
		boolean returnValue = false;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = cognosService.insert(cID, userPermission, ac);
			if(returnValue == true) {
				msg.setSuccessText("권한이 적용되었습니다");
			} else {
				msg.setExceptionText("에러가 발생했습니다");
			}
		} catch (Exception e) {
			logger.error("ERROR > Cognos Authroity Error : {}", e);
			msg.setExceptionText("에러가 발생했습니다" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	
	}
}
