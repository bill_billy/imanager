package com.iplanbiz.iportal.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.CodeService;
import com.iplanbiz.iportal.service.SchedulerMonitoringService;

@Controller
public class SchedulerMonitoringController {

	@Autowired SchedulerMonitoringService schedulerMonitoringService;
	@Autowired CodeService codeService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	@RequestMapping(value="/scheduler/schedulerMonitoring/list", method= RequestMethod.GET)
	public ModelAndView schedulerMonitoringList () throws Exception {
		ModelAndView modelAndView = new ModelAndView("/scheduler/schedulerMonitoring/list");
		
		return modelAndView;
	}
	@RequestMapping(value="/scheduler/schedulerMonitoring/errorDetail", method=RequestMethod.GET)
	public ModelAndView errorDetail() throws Exception {
		ModelAndView modelAndView = new ModelAndView("/scheduler/schedulerMonitoring/errorDetail");
		
		return modelAndView;
	}
	@RequestMapping(value="/scheduler/schedulerMonitoring/jobDetail", method=RequestMethod.GET)
	public ModelAndView jobDetail() throws Exception {
		ModelAndView modelAndView = new ModelAndView("/scheduler/schedulerMonitoring/jobDetail");
		
		return modelAndView;
	}
	@RequestMapping(value="/scheduler/schedulerMonitoring/getSchedulerMonitorData", method=RequestMethod.POST)
	public void getSchedulerMonitorData ( HttpServletResponse response, HttpServletRequest request
														   ,@RequestParam(required=false) String jobStatus ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = schedulerMonitoringService.getSchedulerMonitorData(jobStatus);
			logger.info("getSchedulerMonitorData ReturnValue = " + returnValue.size());
			msg.setSuccessMessage(returnValue); 
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Get Scheduelr Monitor Data Error :{}" ,e);
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/scheduler/schedulerMonitoring/getSchedulerMonitorDetailData", method=RequestMethod.POST)
	public void getSchedulerMonitorDetailData ( HttpServletResponse response, HttpServletRequest request
																   ,@RequestParam(required=false) String schID
																   ,@RequestParam(required=false) String resultGubn ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = schedulerMonitoringService.getSchedulerMonitorDetailData(schID, resultGubn);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("ERROR > Get Scheduler Monitor Detail Data Error :{} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/scheduler/schedulerMonitoring/getCodeListBySchedulerMonitoring", method=RequestMethod.POST)
	public void getCodeListBySchedulerMonitoring ( HttpServletResponse response, HttpServletRequest request
																        ,@RequestParam(required=false) String comlCod ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = codeService.getCodeListByAsc(comlCod);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Get Code Data By Scheduler Monitoring Error : {}" ,e);
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/scheduler/schedulerMonitoring/runJob", method={RequestMethod.POST})
	public void runJob ( HttpServletResponse response, HttpServletRequest request 
								 ,@RequestParam(required=false) String schID ) throws Exception {
		JSONObject returnValue = new JSONObject();
		AjaxMessage msg = new AjaxMessage();
		
		try{
			msg = schedulerMonitoringService.runJob(schID);
			if(msg.getReturnCode().equals("EXCEPTION"))
				msg.setExceptionText("스케줄러 실행 중 에러가 발생했습니다 : "+msg.getReturnText());
			else
				msg.setReturnText("정상 실행되었습니다.");
			/*if(returnValue.get("result").equals("sucess")){
				msg.setSuccessText("정상 실행되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}*/
		}catch(Exception e){
			logger.error("ERROR > Run JOB Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("스케줄러 실행 중 에러가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/scheduler/schedulerMonitoring/getSchedulerDetailError", method=RequestMethod.POST)
	public void getSchedulerDetailError ( HttpServletResponse response, HttpServletRequest request 
			                              , @RequestParam(required=false) String schID
			                              , @RequestParam(required=false) String idx ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = schedulerMonitoringService.getSchedulerDetailError(schID, idx);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Scheduler Detail Error Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/scheduler/schedulerMonitoring/getSchedulerSqlDetail", method=RequestMethod.POST)
	public void getSchedulerSqlDetail ( HttpServletResponse response, HttpServletRequest request
			                            , @RequestParam(required=false) String errIDX
			                            , @RequestParam(required=false) String idx ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = schedulerMonitoringService.getSchedulerSqlDetail(errIDX, idx);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Scheduler Sql Detail Data Error : {}", e);
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
			
		}
		msg.send(response);
	}
}
