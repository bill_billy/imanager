package com.iplanbiz.iportal.controller.dashboard;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cognos.developer.schemas.bibus._3.Document;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.dto.DashboardSlideManager;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.dashboard.DashBoardSlideManagerService;

@Controller
public class DashboardSlideManagerController {
	
	static ModelAndView HTML = new ModelAndView();								// Web Page 기본 구성 요소
	
	@Autowired DashBoardSlideManagerService dashboardService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/dashboard/slideManager/crud", method=RequestMethod.GET)
	public ModelAndView crud (HttpServletRequest request
            , @RequestParam(required=false, defaultValue="") String dashGrp
            , @RequestParam(required=false, defaultValue="") String searchDashGrpName) throws Exception{
		/*logger.info("PARAM DASH GRP ID: {} ", request.getParameter("dash_grp"));*/
		List<HashMap<String, String>> dashGrpList = dashboardService.getDashGrpList(dashGrp, searchDashGrpName);
		
		/*if(dashGrp.equals("")){
			if(dashGrpList.size() > 0){
				dashGrp = dashboardService.getDashGrpList().get(0).get("dashGrp");
				searchDashGrpName = dashboardService.getDashGrpList().get(0).get("searchDashGrpName");
			}
		}*/
		
		HTML.addObject("dashGrpList", dashGrpList);
		HTML.addObject("mainList", dashboardService.getRowSelectView(dashGrp));
		HTML.addObject("dashGrpName", searchDashGrpName);
//		HTML.addObject("mainList", topSelectMenuManagerService.getQueryResult(1402, map1398));
    	HTML.setViewName("/dashboard/slideManager/crud");
    	return HTML;
		
		/*ModelAndView modelAndView = new ModelAndView("/dashboard/slideManager/crud");
		
		modelAndView.addObject("mainList", dashboardService.getDashboardSlideManagerList());
		
		return modelAndView;*/
	}
	
	
	@RequestMapping(value="/dashboard/slideManager/popupMappingPath", method=RequestMethod.GET)
	public ModelAndView popupPath () throws Exception {
		ModelAndView modelAndView = new ModelAndView("/dashboard/slideManager/popupMappingPath");
		
		return modelAndView;
	}
	@RequestMapping(value="/dashboard/slideManager/getDashboardSlidePathList", method=RequestMethod.POST)
	public void getDashboardSlidePathList ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dashboardService.getDashboardSlidePathList();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Dash Board Path List Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/dashboard/slideManager/getSlideMaster", method=RequestMethod.POST)
	public void getSlideMaster( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		JSONObject returnValue = new JSONObject();
		List<HashMap<String, Object>> result=null;
		AjaxMessage msg = new AjaxMessage();
		try{
			result = dashboardService.getSlideMaster();
			returnValue.put("VALUE", result);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Dash Board Path List Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/dashboard/slideManager/getRowSelectView", method=RequestMethod.POST)
	public void getRowSelectView( HttpServletResponse response, HttpServletRequest request 
											,String dash_grp_id) throws Exception {
		JSONObject returnValue = new JSONObject();
		List<HashMap<String, Object>> result=null;
		AjaxMessage msg = new AjaxMessage();
		try{
			result = dashboardService.getRowSelectView(dash_grp_id);
			returnValue.put("result", result);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Dash Board Path List Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/dashboard/slideManager/insertDashInfo", method=RequestMethod.POST)
	public void insertDashInfo( HttpServletResponse response, HttpServletRequest request
			                              ,@RequestParam(required=false) String txtDashGrpName
			                              ,@RequestParam(required=false, defaultValue="") String txtDashGrpID ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try {
			
			msg.setSuccessText("controller에서 service타기전");
			returnValue = dashboardService.insertDashInfo(txtDashGrpName, txtDashGrpID);
			if(returnValue ==0) {
				msg.setSuccessText("저장되었습니다");
			}else if(returnValue == 1) {
				msg.setSuccessText("수정되었습니다");
			}else {
				msg.setExceptionText("에러가 발생했습니다2");
				
			}
		}catch(Exception e){
			logger.error("Error : {}",e);
			e.printStackTrace();
			msg.setExceptionText("에러가 발생했습니다3. \n" + e.getMessage());
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/dashboard/slideManager/deleteSlideMaster", method=RequestMethod.POST)
	public void deleteSlideMaster( HttpServletResponse response, HttpServletRequest request
									,@RequestParam(required=false, defaultValue="") String DASH_GRP_ID) throws Exception{
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try {
			resultValue = dashboardService.deleteSlideMaster(DASH_GRP_ID);
			if(resultValue == 0) {
				msg.setSuccessText("삭제되었습니다.");
			}else {
				msg.setExceptionText("에러가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("에러 : " + e.getMessage());
		}
		msg.send(response);
	}
	
	@RequestMapping(value="dashboard/slideManager/insertSlideMaster", method=RequestMethod.POST)
	public void insertSlideMaster(HttpServletResponse response, HttpServletRequest request
									,@RequestParam(required=false, defaultValue="") String dash_grp_id
									,String slideIDX, String pathIDX, String interval, String eftType, String applyMonthValue, String sortOrd) throws Exception{
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try {
			resultValue = dashboardService.insertSlideMaster(dash_grp_id, slideIDX, pathIDX, interval, eftType, applyMonthValue, sortOrd);
			if(resultValue==0) {
				msg.setSuccessText("삽입되었습니다.");
			}else {
				msg.setExceptionText("에러가 발생했습니다.");
			}
		}catch(Exception e) {
			msg.setExceptionText("에러:" +e.getMessage());
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/dashboard/slideManager/remove", method=RequestMethod.POST)
	public void remove( HttpServletResponse response, HttpServletRequest request
			                     ,@RequestParam(required=false, defaultValue="") String txtDashGrpID
			                     ,@RequestParam(required=false) String txtDashGrpName) throws Exception{
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			
			resultValue = dashboardService.deleteDashInfo(txtDashGrpID, txtDashGrpName);
			/*logger.info("service타기후txtDashGrpName="+dashGrpName+ "   dashID="+dashGrpID);*/
			if(resultValue == 0){
				msg.setSuccessText("삭제되었습니다");
			}else{
				msg.setExceptionText("에러가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("User Delete Error : {}", e);
			msg.setExceptionText("에러 : " + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	
	
	/*@RequestMapping(value="/dashboard/slideManager/insert", method=RequestMethod.POST)//그룹명 아이디 값따와서 조인해주기
	public void insertDashboardSlide ( HttpServletResponse response, HttpServletRequest request
			                                         ,String dash_grp_id, String path_idx, String sortord, 
			                                          String interval, String eft_type, String apply_month
			                                         ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			logger.info(slide.getDash_grp_id());
			logger.info("서비스 타기전");
			logger.info(slide.getPathIDX()[0]);
			
			logger.info("서비스 타기전");
			System.out.println("배열값");
			
			System.out.println(dash_grp_id);
			System.out.println(path_idx);
			System.out.println(sortord);
			System.out.println(interval);
			System.out.println(eft_type);
			System.out.println(apply_month);
			
			returnValue = dashboardService.insertDashboardSlide(dash_grp_id, path_idx, sortord, interval, eft_type, apply_month);
			
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Insert/Update Dash Board Slide Manager Data Error : {}" , e);
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}*/
	
	//DTO(slide)로 값 넘겨줘서 저장하는 방식
	@RequestMapping(value="/dashboard/slideManager/insert", method=RequestMethod.POST)//그룹명 아이디 값따와서 조인해주기
	public void insertDashboardSlide ( HttpServletResponse response, HttpServletRequest request
			                                         ,@ModelAttribute DashboardSlideManager slide
			                                         ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			logger.info(slide.getDash_grp_id());
			logger.info("서비스 타기전");
			
			
			logger.info("서비스 타기전");
			System.out.println("배열값");
			
			System.out.println(slide.getDash_grp_id());
			
			System.out.println(Arrays.toString(slide.getSlideIDX()));
			System.out.println(Arrays.toString(slide.getPathIDX()));
			System.out.println(Arrays.toString(slide.getSortOrd()));
			System.out.println(Arrays.toString(slide.getInterval()));
			System.out.println(Arrays.toString(slide.getEftType()));
			System.out.println(Arrays.toString(slide.getApplyMonthValue()));
			returnValue = dashboardService.insertDashboardSlide(slide);
			
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Insert/Update Dash Board Slide Manager Data Error : {}" , e);
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	
	
	@RequestMapping(value="/dashboard/slideManager/popupSlideShow", method=RequestMethod.GET)
	public ModelAndView popupSlideShow () throws Exception {
		ModelAndView modelAndView = new ModelAndView("/dashboard/slideManager/popupSlideShow");
		
		return modelAndView;
	}
	@RequestMapping(value="/dashboard/slideManager/dashboardSlideShow", method=RequestMethod.GET)
	public ModelAndView dashboardSlideManagerShow () throws Exception {
		ModelAndView modelAndView = new ModelAndView("/dashboard/slideManager/dashboardSlideShow");
		
		return modelAndView;
	}
	@RequestMapping(value="/dashboard/slideManager/getDashboardSlidePopupData", method={RequestMethod.POST, RequestMethod.POST})
	public void getDashboardSlidePopupData ( HttpServletResponse response, HttpServletRequest request ,String dash_grp_id) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dashboardService.getDashboardSlidePopupData(dash_grp_id);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Dashboard Slide Popup Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/dashboard/slideManager/dashIdForm", method=RequestMethod.POST)
	public void dashIdForm( HttpServletResponse response, HttpServletRequest request 
											,String dash_grp_id) throws Exception {
		JSONObject returnValue = new JSONObject();
		List<HashMap<String, Object>> result=null;
		AjaxMessage msg = new AjaxMessage();
		try{
			result = dashboardService.dashIdForm(dash_grp_id);
			returnValue.put("result", result);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Dash Board Path List Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
}
