package com.iplanbiz.iportal.controller.home;

import java.io.IOException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mortbay.jetty.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.secure.Base64Util;
import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.comm.util.CognosInfo;
import com.iplanbiz.iportal.config.WebConfig; 
import com.iplanbiz.iportal.service.admin.HelpManagerService;
import com.iplanbiz.iportal.service.home.HomeService;
import com.iplanbiz.iportal.service.admin.MenuService;
import com.iplanbiz.iportal.service.PasswordService;
import com.iplanbiz.iportal.service.portlet.PortletService;
import com.iplanbiz.iportal.service.admin.UserService;
import com.iplanbiz.iportal.service.system.SystemService;

@Controller
public class HomeController {
	
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired
	HomeService homeService;
	
	@Autowired
	SystemService systemService;
	
	@Autowired
	PortletService portletService;
	
	@Autowired
	MenuService menuService;
	
	@Autowired
	HelpManagerService helpManagerService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/", method=RequestMethod.GET)
	public ModelAndView home(HttpServletResponse response) throws Exception{
		ModelAndView mview = new ModelAndView("home"); 
		return mview;
	}
	@RequestMapping(value="/home/layout/list", method=RequestMethod.GET)
	public ModelAndView main(HttpServletResponse response) throws Exception{
		ModelAndView mview = new ModelAndView();
		
		Map<String, Object> systemConfigMap = systemService.getSystemConfig();
		
		
		int userPortletCount = portletService.getUserPortletInfo().size();
		if(userPortletCount == 0){
			portletService.defaultUserPortlet();
		}
		int layoutNo = 1;
		try{
			layoutNo = portletService.getUserPortletLayoutNo();
		}catch(Exception e){
			layoutNo = 1;
			logger.error("ERROR", e);
		}
		
		mview.addObject("portletLayout", "/portlet/l"+layoutNo);
		mview.addObject("systemConfig", systemConfigMap);
		mview.addObject("systemDivision", "IVISIONPLUS");
		//mview.addObject("homeList", homeService.getHomeList());
		int layoutno = 7;
		if(systemConfigMap.get("theme_layout")!=null)
			layoutno = Integer.valueOf(systemConfigMap.get("theme_layout").toString());
		mview.setViewName("/home/layout/layout_0"+layoutno+"_index");
		return mview;
	}
	@RequestMapping(value="/home/loader/get", method= RequestMethod.GET)
	public ModelAndView getLoader ( HttpServletRequest request
													 ,@RequestParam(required=false, defaultValue="") String type
			                                         ,@RequestParam(required=false, defaultValue="") String storeId
			                                         ,@RequestParam(required=false, defaultValue="") String src
			                                         ,@RequestParam(required=false, defaultValue="") String cid
			                                         ,@RequestParam(required=false, defaultValue="") String menuNm
			                                         ,@RequestParam(required=false, defaultValue="") String winType) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		
		LoginSessionInfo loginInfo = loginSessionInfoFactory.getObject();
		
		String url = "";
		String path = "";
		String cID = "";
		String realCid = "";
		String menuName = URLDecoder.decode(menuNm, "UTF-8");
		
		if(type.equals("audit") || type.equals("cog")){
			logger.info("Cognos Report Open");
			String storeID = storeId;
			String browserType = loginInfo.getUserBrowserType();
			int browserVersion = Integer.parseInt(loginInfo.getUserBrowserVersion());
			realCid = storeID;
			boolean isIE8 = false;
			if((browserType.equals("IE") && browserVersion <= 8) || (browserType.equals("IE") && browserVersion == 10)){
				isIE8 = true;
			}
			boolean isView = false;
			if(browserType.equals("safari") || browserType.equals("chrome")){
				isView = true;
			}
			if(type.equals("audit")){
				storeID = menuService.getAuditStoreID(request);
			}
			url = menuService.getCogReportUrl(storeID, isView, isIE8);
			cid = storeId;
			
			modelAndView.addObject("helperCount", helpManagerService.getHelpManagerMenuDetail(cid));
			String searchPath = menuService.getCogSearchPath(storeID);
			modelAndView.addObject("searchPath", searchPath);
			path = menuService.getReportPath(searchPath);
			 
		}else{
			logger.info("Portal Open");
			try {
				url = URLDecoder.decode(Base64Util.decode(src), "UTF-8");
			}catch(Exception e) {
				throw new Exception("잘못된 접근 입니다.");
			}
			path = menuService.getNavi(cid);
			realCid = cid;
			modelAndView.addObject("helperCount", helpManagerService.getHelpManagerMenuDetail(cid));
		}
		boolean isEdge = true;
		if(url.toLowerCase().indexOf("analysisstudio") != -1 || url.toLowerCase().indexOf("querystudio") != -1){
			isEdge = false;
		}
		List<HashMap<String, Object>> summaryContentList = helpManagerService.getSummaryContentData(cid);
		String summaryContent = "";
		String chargeEmp = "";
		String chargeDept = "";
		String confirmDept = "";
		String confirmEmp = "";
		if(summaryContentList.size() > 0 && summaryContentList.get(0) != null){
			summaryContent = (summaryContentList.get(0).get("summarycontent").toString() != null)?summaryContentList.get(0).get("summarycontent").toString():"";
			chargeDept = (summaryContentList.get(0).get("charge_dept").toString()!=null)?summaryContentList.get(0).get("charge_dept").toString():"";
			chargeEmp = (summaryContentList.get(0).get("charge_emp").toString()!=null)?summaryContentList.get(0).get("charge_emp").toString():"";
			confirmDept = (summaryContentList.get(0).get("confirm_dept").toString()!=null)?summaryContentList.get(0).get("confirm_dept").toString():"";
			confirmEmp = (summaryContentList.get(0).get("confirm_emp").toString()!=null)?summaryContentList.get(0).get("confirm_emp").toString():"";
			
		if(summaryContentList.get(0).get("content") != null) modelAndView.addObject("helpCount", 1);
		}else{
			modelAndView.addObject("helperCount", 0);
		}
		
		Map<String, Object> systemConfigMap = systemService.getSystemConfig();
		modelAndView.addObject("systemConfig", systemConfigMap);
		modelAndView.addObject("cid", cid.toLowerCase());
		modelAndView.addObject("realCid", realCid);
		modelAndView.addObject("path",path);
		modelAndView.addObject("isEdge", isEdge);
		url = url.replace("@USERID", loginInfo.getUserId());
		logger.info("url:{}",url);
		modelAndView.addObject("url", url);
		modelAndView.addObject("menuNm", menuNm);
		modelAndView.addObject("summaryContent", summaryContent);
		modelAndView.addObject("chargeDept",chargeDept);
		modelAndView.addObject("chargeEmp",chargeEmp);
		modelAndView.addObject("confirmDept", confirmDept);
		modelAndView.addObject("confirmEmp", confirmEmp); 
		modelAndView.addObject("type", type);
		modelAndView.addObject("winType", winType);
		
		modelAndView.setViewName("/home/loader/loader0"+Integer.valueOf(systemConfigMap.get("theme_layout").toString()));
		return modelAndView;
	}
	@RequestMapping(value="/home/layout/getRootCID", method=RequestMethod.POST)
	public void getRootCID(HttpServletResponse response, HttpServletRequest request) throws Exception {
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = menuService.getRootCID();
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			msg.setExceptionText("데이터 로딩 중 ERROR 발생 " + e.getMessage());
			logger.error("ERROR : {}" , e);
			e.printStackTrace();
			throw e;
		}
		msg.send(response);
	}
	@RequestMapping(value="/home/layout/getTopMenuList", method=RequestMethod.POST)
	public void getTopMenuList( HttpServletResponse response, HttpServletRequest request,
											  @RequestParam(required=false) String pID) throws Exception{
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = menuService.getTopMenuList(pID);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			msg.setExceptionText("데이터 로딩 중 ERROR 발생 " + e.getMessage());
			logger.error("ERROR : {}" , e);
			e.printStackTrace();
			throw e;
		}
		msg.send(response);
	}
	@RequestMapping(value="/home/layout/getMenuList", method=RequestMethod.POST)
	public void getMenuList( HttpServletResponse response, HttpServletRequest request
										,@RequestParam(required=false) String pID) throws Exception{
		 JSONArray resultValue = null;
		 AjaxMessage msg = new AjaxMessage();
		 try{
			 resultValue = menuService.getMenuList(pID);
			 msg.setSuccessMessage(resultValue);
		 }catch(Exception e){
			 msg.setExceptionText("데이터 로딩 중 ERROR 발생 " + e.getMessage());
			 logger.error("ERROR : {}" , e);
			 e.printStackTrace();
		 }
		 msg.send(response);
	} 
	@RequestMapping(value="/home/layout/getPageletByUserID", method=RequestMethod.POST)
	public void getPageletByUserID ( HttpServletResponse response, HttpServletRequest request )  throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = portletService.getPageletByUserID();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get PageletBy UserID Data Error : {}", e);
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/home/layout/getBookList", method=RequestMethod.POST)
	public void getBookList ( HttpServletResponse response, HttpServletRequest request ) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = menuService.getBookList();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("ERROR > Get Book List Data Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/home/layout/insertMyfolderNoCog", method=RequestMethod.POST)
	public void insertMyfolderNoCog ( HttpServletResponse response, HttpServletRequest request
												     ,@RequestParam(required=false) String cName ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = menuService.insertMyfolderNoCog(cName);
			if(returnValue == 0){
				msg.setSuccessText("폴더가 추가되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Insert My Folder No Cognos Data Error : {}", e);
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/home/layout/renameMyfolderNoCog", method=RequestMethod.POST)
	public void renameMyfolderNoCog ( HttpServletResponse response, HttpServletRequest request
			,@RequestParam(required=false) String cId,@RequestParam(required=false) String cName ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = menuService.renameMyfolderNoCog(cId,cName);
			if(returnValue == 0){
				msg.setSuccessText("이름이 변경되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Rename My Folder No Cognos Data Error : {}", e);
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/home/layout/insertBookReport", method=RequestMethod.POST)
	public void insertBookReport ( HttpServletResponse response, HttpServletRequest request
												,@RequestParam(required=false) String cID
												,@RequestParam(required=false) String menuName
												,@RequestParam(required=false) String menuType
												,@RequestParam(required=false) String pID ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = menuService.insertBookReport(cID, menuName, menuType, pID);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("ERROR > Insert Book Report Data Error {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/home/loader/insertBookReport", method=RequestMethod.POST)
	public void insertBookReportByloader ( HttpServletResponse response, HttpServletRequest request
												,@RequestParam(required=false) String cID
												,@RequestParam(required=false) String menuName
												,@RequestParam(required=false) String menuType
												,@RequestParam(required=false) String pID ) throws Exception {
		this.insertBookReport(response, request, cID, menuName, menuType, pID);
	}
	@RequestMapping(value="/home/layout/deleteBookUserData", method=RequestMethod.POST)
	public void deleteBookUserData ( HttpServletResponse response, HttpServletRequest request
													,@RequestParam(required=false) String cID ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = menuService.deleteBookUserData(cID);
			if(returnValue == 0){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("ERROR > Delete Book User Data Error : {}" , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/home/layout/moveMyfolderNoCog", method=RequestMethod.POST)
	public void moveMyfolderNoCog ( HttpServletResponse response, HttpServletRequest request
			,@RequestParam(required=false) String cId ,@RequestParam(required=false) String pId ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			if(pId==null||pId.equals("")) pId="5999";
			returnValue = menuService.moveMyfolderNoCog(cId,pId);
			if(returnValue == 0){
				msg.setSuccessText("이동 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("ERROR > moveMyfolderNoCog Error : {}" , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/home/layout/sortMyfolderNoCog", method=RequestMethod.POST)
	public void sortMyfolderNoCog ( HttpServletResponse response, HttpServletRequest request
			,@RequestParam(required=false) String cId ,@RequestParam(required=false) String targetId ,@RequestParam(required=false) String position,@RequestParam(required=false) String sortOrder) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = menuService.sortMyfolderNoCog(cId,targetId,position,sortOrder);
			if(returnValue == 0){
				msg.setSuccessText("이동 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("ERROR > sortMyfolderNoCog Error : {}" , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/home/layout/manageBookList", method=RequestMethod.GET)
	public ModelAndView manageBookList() throws Exception{
		ModelAndView mview = new ModelAndView();
		mview.setViewName("/home/booklist/manageBookList");
		return mview;
	}
												     
}
