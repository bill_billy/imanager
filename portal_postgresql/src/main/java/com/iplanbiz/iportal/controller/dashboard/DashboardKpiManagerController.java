package com.iplanbiz.iportal.controller.dashboard;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.dto.DashboardKpiManager;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.dashboard.DashboardCodeService;
import com.iplanbiz.iportal.service.dashboard.DashboardKpiManagerService;

@Controller
public class DashboardKpiManagerController {

	@Autowired DashboardKpiManagerService dashboardKpiManagerService;
	@Autowired DashboardCodeService dashboardCodeService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	@RequestMapping(value="/dashboard/kpiManager/crud", method=RequestMethod.GET)
	public ModelAndView crud () throws Exception{
		ModelAndView modelAndView = new ModelAndView("/dashboard/kpiManager/crud");
		
		return modelAndView;
	}
	@RequestMapping(value="/dashboard/kpiManager/popupMappingSqlData", method= RequestMethod.GET)
	public ModelAndView popupMappingSqlData() throws Exception{
		ModelAndView modelAndView = new ModelAndView("/dashboard/kpiManager/popupMappingSqlData");
		
		return modelAndView;
	}
	@RequestMapping(value="/dashboard/kpiManager/getDashboardKpiList", method=RequestMethod.POST)
	public void getDashboardKpiList( HttpServletResponse response, HttpServletRequest request
													,@RequestParam(required=false) String kpiGbn) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dashboardKpiManagerService.getDashboardKpiList(kpiGbn);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Dash Board Kpi List Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다");
		}
		msg.send(response);
	}
	@RequestMapping(value="/dashboard/kpiManager/getDashboardKpiDetail", method=RequestMethod.POST)
	public void getDashboardKpiDetail( HttpServletResponse response, HttpServletRequest request
													  ,@RequestParam(required=false) String kpiID) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dashboardKpiManagerService.getDashboardKpiDetail(kpiID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Dash Board Kpi Detail Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다");
		}
		msg.send(response);
	}
	@RequestMapping(value="/dashboard/kpiManager/getDashboardCodeList", method=RequestMethod.POST)
	public void getKpiManagerCodeList ( HttpServletResponse response, HttpServletRequest request
														,@RequestParam(required=false) String comlCod) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dashboardCodeService.getDashboardCodeList(comlCod);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Dash Board Code List Error : {} ", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/dashboard/kpiManager/getDashboardCheckMm", method=RequestMethod.POST)
	public void getDashboardCheckMm ( HttpServletResponse response, HttpServletRequest request
														,@RequestParam(required=false) String kpiID) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dashboardKpiManagerService.getDashboardCheckMm(kpiID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Dash Board Check Month Data Error : {}" , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/dashboard/kpiManager/insert", method=RequestMethod.POST)
	public void insertDashBoardKpiManager ( HttpServletResponse response, HttpServletRequest request
																,@ModelAttribute DashboardKpiManager dash) throws Exception{
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dashboardKpiManagerService.insertDashBoardKpiManager(dash);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else if(returnValue == 1){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Dash Board Kpi Manager Insert & Update Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/dashboard/kpiManager/remove", method=RequestMethod.POST)
	public void removeDashBoardKpiManager ( HttpServletResponse response, HttpServletRequest request
																 ,@ModelAttribute DashboardKpiManager dash) throws Exception{
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = dashboardKpiManagerService.removeDashBoardKpiManager(dash);
			if(returnValue == 0){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Dash Board Kpi Manager Delere Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
}
