package com.iplanbiz.iportal.controller.system;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ErrorController {
	private Logger logger = LoggerFactory.getLogger(getClass());
 
	@RequestMapping(value="/system/error/info", method=RequestMethod.GET)
	public ModelAndView list(HttpServletRequest request){
		ModelAndView modelAndView = new ModelAndView("/system/error/info");
		Object status_code = request.getAttribute("javax.servlet.error.status_code");
		Object exception_type =  request.getAttribute("javax.servlet.error.exception_type");
		Object message = request.getAttribute("javax.servlet.error.message");
		Object request_uri =request.getAttribute("javax.servlet.error.request_uri"); 
		Object exception = request.getAttribute("javax.servlet.error.exception"); 
		Object servlet_name = request.getAttribute("javax.servlet.error.servlet_name"); 
		modelAndView.addObject("status_code", status_code);
		modelAndView.addObject("exception_type", exception_type);
		modelAndView.addObject("message", message);
		modelAndView.addObject("request_uri", request_uri);
		modelAndView.addObject("exception", exception);
		modelAndView.addObject("servlet_name", servlet_name);
		return modelAndView;
	}
}
