package com.iplanbiz.iportal.controller.system;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage; 
import com.iplanbiz.iportal.service.system.CustomSqlHouseService;
 

@Controller
public class CustomSqlHouseController {


	@Autowired
	CustomSqlHouseService customSqlHouseService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	@RequestMapping(value="/system/customsqlhouse/list", method={RequestMethod.GET})
	public ModelAndView list_get(@RequestParam(required=false) String searchText){
		ModelAndView mview = new ModelAndView("/system/customsqlhouse/list"); 
		AjaxMessage msg = new AjaxMessage();
		try {
			
			mview.addObject("mainList", customSqlHouseService.getCustomSqlHouseList(searchText));
			
			msg.setSuccessMessage(new JSONArray(),new JSONObject());
			 
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error : {}",e);
			msg.setExceptionText("");
		}
		return mview;
	}
	
	/*ajax insert*/
	@RequestMapping(value= "/system/customsqlhouse/insert", method=RequestMethod.POST)
	public void list_insert(    HttpServletResponse response,HttpServletRequest request,
			@RequestParam(required=true) String queryID,
			@RequestParam(required=true) String dbkey,
			@RequestParam(required=true) String description,
			@RequestParam(required=false) String query
			
	        )  {
		
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try {
			resultValue = customSqlHouseService.insertCustomSqlHouse(queryID, dbkey, description, query);
			msg.setSuccessText("등록 되었습니다.");
		}  catch (Exception e1) {
			// TODO Auto-generated catch block
			msg.setExceptionText("등록 중 오류가 발생 했습니다 : "+e1.getMessage());
			e1.printStackTrace();
		} 
		
		msg.send(response);
	}
	
	/*ajax update*/
	@RequestMapping(value= "/system/customsqlhouse/update", method=RequestMethod.POST)
	public void list_update(  HttpServletResponse response,HttpServletRequest request,
			@RequestParam(required=true) String queryID,
			@RequestParam(required=true) String idx,
			@RequestParam(required=false) String dbkey,
			@RequestParam(required=true) String description,
			@RequestParam(required=false) String sql
			
	        )  {
		
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try {
			resultValue = customSqlHouseService.updateCustomSqlHouse(queryID,idx,dbkey,description,sql);
			msg.setSuccessText("complete modify.");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			msg.setExceptionText("수정 중 오류가 발생 했습니다 : "+e1.getMessage());
			e1.printStackTrace();
		} 
		
		msg.send(response);
		
	}

}
