package com.iplanbiz.iportal.controller.procedure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.dto.Procedure;
import com.iplanbiz.iportal.service.procedure.ProcedureService;


@Controller
public class ProcedureController {
	@Autowired         
    private ProcedureService procedureService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	 @RequestMapping(value="/procedure/procedureManager/list", method = {RequestMethod.GET})
	    public ModelAndView list() {
	    	
			ModelAndView modelAndView = new ModelAndView();
			List<HashMap<String, String>> yearList = procedureService.procedureCodeList("EVAL_YEAR");
			
			modelAndView.addObject("yearList", yearList);
			modelAndView.setViewName("procedure/procedureManager/list");
			return modelAndView;
	    }
	 @RequestMapping(value="/procedure/procedureManager/callSp", method=RequestMethod.POST)
		public void getProcedureList ( HttpServletResponse response , HttpServletRequest request, 
				@ModelAttribute Procedure pr) throws Exception {
			AjaxMessage msg = new AjaxMessage();
			
			String result = ""; 
			try{
				result = procedureService.callSp(pr.getDbGubn(), pr.getDbUser(), pr.getSpName(), pr.getYyyy(), pr.getIdx());
				if(result.equals("SUCCESS"))
					msg.setSuccessText("Procedure가 정상적으로 실행되었습니다.");
				else
					msg.setExceptionText("Procedure 실행 도중 에러가 발생했습니다.");
			}catch(Exception e){
				logger.error("procedureManager callSp Error : {} " ,e);
				e.printStackTrace();
				msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
			}
			msg.send(response);
		}
	 @RequestMapping(value="/procedure/procedureManager/getProcedureList", method=RequestMethod.POST)
		public void getProcedureList ( HttpServletResponse response , HttpServletRequest request 
																	   ,@RequestParam(required=false) String procedureNm) throws Exception {
			JSONArray returnValue = null;
			AjaxMessage msg = new AjaxMessage();
			
			try{
				returnValue = procedureService.getProcedureList(procedureNm);
				msg.setSuccessMessage(returnValue);
			}catch(Exception e){
				logger.error("Get Procedure List Data Error : {} " ,e);
				e.printStackTrace();
				msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
			}
			msg.send(response);
		} 
	 @RequestMapping(value="/procedure/procedureManager/procedureResultListCogdw", method=RequestMethod.POST)
		public void procedureResultListCogdw ( HttpServletResponse response , HttpServletRequest request 
				,@RequestParam(required=false) String dbGubn,@RequestParam(required=false) String dbUser,@RequestParam(required=false) String paramProcedureName) throws Exception {
			JSONArray returnValue = null;
			AjaxMessage msg = new AjaxMessage();
			
			try{
				returnValue = procedureService.procedureResultListCogdw(dbGubn,dbUser,paramProcedureName);
				msg.setSuccessMessage(returnValue);
			}catch(Exception e){
				logger.error("Get procedureResultListCogdw Data Error : {} " ,e);
				e.printStackTrace();
				msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
			}
			msg.send(response);
		}
 
}
