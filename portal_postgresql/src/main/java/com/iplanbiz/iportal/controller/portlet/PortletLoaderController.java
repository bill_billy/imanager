package com.iplanbiz.iportal.controller.portlet;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo; 
 
import com.iplanbiz.iportal.service.portlet.PortletService;
import com.iplanbiz.iportal.service.system.SystemService;

@Controller
public class PortletLoaderController {

	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	
	@Autowired
	SystemService systemService;
	
	@Autowired
	PortletService portletService;
	 
	 
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	
	@RequestMapping(value="/portlet/loader/l{idx}", method=RequestMethod.GET)
	public ModelAndView portletLayout ( @PathVariable(value="idx") String idx, @RequestParam(required=false) String plID ) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		 
		Map<String, Object> systemConfigMap = systemService.getSystemConfig();
		
		modelAndView.addObject("systemConfig", systemConfigMap);
		if(plID != null){
			modelAndView.addObject("plid", plID);
		}
		modelAndView.addObject("portletUserType", "normal");
		String viewName = "/portlet/loader/portlet_layout_"+Integer.parseInt(idx);
		modelAndView.setViewName(viewName);
		
		return modelAndView;
	}
	@RequestMapping(value="/portlet/loader/getPageletPortletInfo", method=RequestMethod.POST)
	public void getPageletPortletInfo ( HttpServletResponse response, HttpServletRequest request 
													 ,@RequestParam(required=false) String plID ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletService.getPageletPortletInfo(plID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("ERROR > Get Pagelet Portlet Info Data Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("에러가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
		@RequestMapping(value="/portlet/loader/getPagetletPortletInfoByUser", method=RequestMethod.POST)
		public void getPagetletPortletInfoByUser ( HttpServletResponse response, HttpServletRequest request
																	,@RequestParam(required=false) String plID ) throws Exception {
			JSONArray returnValue = null;
			AjaxMessage msg = new AjaxMessage();
			
			try{
				returnValue = portletService.getPagetletPortletInfoByUser(plID);
				msg.setSuccessMessage(returnValue);
			}catch(Exception e){
				logger.error("ERROR > Get Pagelet Portlet Info Data By User Error : {} ", e);
				e.printStackTrace();
				msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
			}
			msg.send(response);
		}
	@RequestMapping(value="/portlet/loader/getPortletInfoData", method=RequestMethod.POST)
	public void getPortletInfoData ( HttpServletRequest request , HttpServletResponse response 
												 ,@RequestParam(required=false) String poID
												 ,@RequestParam(required=false) String gx) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletService.getPortletInfoData(poID, gx);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("ERROR > Get Portlet Info Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}

	@RequestMapping(value="/portlet/loader/setPortletDefault", method=RequestMethod.POST)
	public void setPortletDefault (HttpServletResponse response, HttpServletRequest request) {
		JSONObject returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			logger.info("Default Portlet Setting Start");
			portletService.defaultUserPortlet();
			logger.info("Default Portlet Setting End");
		}catch(Exception e){
			logger.error("ERROR Set Portlet Default Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " +  e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/loader/getPageletPortlet", method=RequestMethod.POST)
	public void pageletPortletInfo ( HttpServletResponse response, HttpServletRequest request 
			                                    ,@RequestParam(required=false) String poID
			                                    ,@RequestParam(required=false) String plID ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletService.getPageletPortlet(poID, plID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Get Pagelet Portlet Data Error : {}" ,e);
			msg.setExceptionText("오류가 발생했습니다 : " +  e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/loader/getPageletPortletByUser", method=RequestMethod.POST)
	public void getPageletPortletByUser ( HttpServletResponse response, HttpServletRequest request
														 ,@RequestParam(required=false) String poID
														 ,@RequestParam(required=false) String plID ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletService.getPageletPortletByUser(poID, plID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Get Pagelet Portlet By User Data Error {} " ,e);
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/portlet/loader/getPageletPortletDefault", method=RequestMethod.POST)
	public void getPageletPortletDefault ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		JSONObject returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = portletService.getPageletPortletDefault();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Get Pagelet Portlet Default Data Error : {} " ,e);
			msg.setExceptionText("오류가 발생했습니다 : " +  e.getMessage());
		}
		msg.send(response);
	}
}
