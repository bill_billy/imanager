package com.iplanbiz.iportal.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.io.butler.Proxy;
import com.iplanbiz.core.io.butler.ProxyPool;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dto.schedulerManager;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.CodeService; 
import com.iplanbiz.iportal.service.SchedulerManagerService;

@Controller
public class SchedulerManagerController {

	@Autowired SchedulerManagerService schedulerService;
	@Autowired CodeService codeService;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessinInfoFactory;
	@Resource(name="proxyPool") ProxyPool proxyPool;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	@RequestMapping(value="/scheduler/schedulerManager/list", method=RequestMethod.GET)
	public ModelAndView schedulerList () throws Exception {
		ModelAndView modelAndView = new ModelAndView("/scheduler/schedulerManager/list");
		
		return modelAndView;
	}
	@RequestMapping(value="/scheduler/schedulerManager/detail", method=RequestMethod.GET)
	public ModelAndView detail( @RequestParam(required=false, defaultValue="") String schID) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/scheduler/schedulerManager/detail");
		
		if(!schID.equals("")){
			modelAndView.addObject("mainList", schedulerService.getSchedulerManagerDetail(schID).get(0));
		}
		modelAndView.addObject("codeList", codeService.getCodeListByAsc("JOB_CYC"));
		modelAndView.addObject("jobDistList", codeService.getCodeListByAsc("JOB_DIST"));
		
		return modelAndView;
	}
	@RequestMapping(value="/scheduler/schedulerManager/getSchedulerManagerList", method=RequestMethod.POST)
	public void getSchedulerManagerList ( HttpServletResponse response , HttpServletRequest request 
																   ,@RequestParam(required=false) String useYn
																   ,@RequestParam(required=false) String schStatus ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = schedulerService.getSchedulerManagerList(useYn, schStatus);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Scheduler Manager List Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/scheduler/schedulerManager/getCodeListByScheduler", method=RequestMethod.POST)
	public void getCodeListByScheduler ( HttpServletResponse response, HttpServletRequest request
			                                            ,@RequestParam(required=false) String comlCod ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = codeService.getCodeListByAsc(comlCod);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
			e.printStackTrace();
			throw e;
		}
		msg.send(response);
	}
	@RequestMapping(value="/scheduler/schedulerManager/getJobMappingScheduler", method=RequestMethod.POST)
	public void getJobMappingScheduler ( HttpServletResponse response, HttpServletRequest request
														  ,@RequestParam(required=false, defaultValue="") String schID
														  ,@RequestParam(required=false, defaultValue="") String jobTyp ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = schedulerService.getJobMappingScheduler(schID, jobTyp);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Get Job Mapping Scheduler Data Error : {} " ,e);
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/scheduler/schedulerManager/getDBinfoListByScheduler", method=RequestMethod.POST)
	public void getDBinfoListByScheduler ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = schedulerService.getDBinfoListByScheduler();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get DB Info List Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/scheduler/schedulerManager/getSeverJobList", method=RequestMethod.POST)
	public void getSeverJobList ( HttpServletResponse response, HttpServletRequest request 
			                                 ,@RequestParam(required=false, defaultValue="") String jobDist
			                                 ,@RequestParam(required=false, defaultValue="") String schID
			                                 ,@RequestParam(required=false, defaultValue="") String searchName
			                                 ,@RequestParam(required=false, defaultValue="") String dbKey ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = schedulerService.getSeverJobList(jobDist, schID, searchName, dbKey);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Server Job List Data Error : {} " , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/scheduler/schedulerManager/setSchedulerData", method=RequestMethod.POST)
	public void setSchedulerData ( HttpServletResponse response, HttpServletRequest request
											   ,@RequestParam(required=false) String schID
											   ,@RequestParam(required=false) String schStatus ) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		
		try{
			JSONObject param = new JSONObject();
			param.put("JOBGROUP"					, "etl");
			param.put("JOBNAME"						, schID+"_"+loginSessinInfoFactory.getObject().getAcctID());
			param.put("REQUEST_USER_ID"			, loginSessinInfoFactory.getObject().getUserId());
			Proxy proxy = proxyPool.createProxy();
			proxy.start();
			try{
				if(schStatus.equals("UNREG")){
					proxy.requestRule("scheduler",  "AddJob", param);
					msg.setSuccessText("등록이 되었습니다");
				}else if(schStatus.equals("WAIT")){
					proxy.requestRule("scheduler",  "RemoveJob", param);
					msg.setSuccessText("등록이 해제되었습니다");
				}
			}catch(Exception e){
				logger.error("ERROR > Set Scheduler Data Error : {}", e);
				msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
			}finally{
				proxy.stop();
			}
		}catch(Exception e){
			logger.error("ERROR > Set Scheduler Data Error : {} ", e);
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/scheduler/schedulerManager/insertBySchedulerManager", method=RequestMethod.POST)
	public void insertBySchedulerManager ( HttpServletResponse response, HttpServletRequest request 
															,@ModelAttribute schedulerManager scheduler ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = schedulerService.insertBySchedulerManager(scheduler);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else if(returnValue == 1){
				msg.setSuccessText("수정 되었습니다");
			}else {
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
			logger.error("ERROR > Insert & Update Scheduler Manager Data Error : {} " ,e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/scheduler/schedulerManager/deleteBySchedulerManager", method={RequestMethod.POST})
	public void deleteBySchedulerManager ( HttpServletResponse response, HttpServletRequest request
															 ,@ModelAttribute schedulerManager scheduler ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = schedulerService.deleteBySchedulerManager(scheduler);
			if(returnValue == 0){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
			logger.error("ERROR > Delete Scheduler Manager Data Error : {} " ,e);
			e.printStackTrace();
			
		}
		msg.send(response);
	}
}
