package com.iplanbiz.iportal.controller.admin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.service.admin.UserExcelLogService;

@Controller
public class UserExcelLogController {
	@Autowired UserExcelLogService userExcelLogService;
	
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@RequestMapping(value="/admin/audit/userExcelLog", method={RequestMethod.GET})
	public ModelAndView userExcelLog(HttpServletRequest request){
		ModelAndView modelAndView = new ModelAndView();
		
		modelAndView.setViewName("admin/audit/userExcelLog/list");
		
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/audit/getNavi", method={RequestMethod.POST})
	public void getMenuList ( HttpServletResponse response , HttpServletRequest request 
																   ,@RequestParam(required=false) String cid) throws Exception {
		
		JSONObject returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = userExcelLogService.getNavi(cid);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getMenuList Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
		
	}
	@RequestMapping(value="/admin/audit/getExcelLogReport", method={RequestMethod.POST})
	public void getExcelLogReport ( HttpServletResponse response , HttpServletRequest request 
																   ,@RequestParam(required=false) String cid) throws Exception {
		
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = userExcelLogService.getExcelLogReport(cid);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getMenuList Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	
	}
	@RequestMapping(value="/admin/audit/getExcelLogDate", method={RequestMethod.POST})
	public void getExcelLogDate ( HttpServletResponse response , HttpServletRequest request 
																   ,@RequestParam(required=false) String start_dat
																   ,@RequestParam(required=false) String end_dat) throws Exception {
		
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = userExcelLogService.getExcelLogDate(start_dat,end_dat);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getMenuList Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	
	}
	@RequestMapping(value="/admin/audit/downExcelLogReport", method={RequestMethod.GET})
	public ModelAndView downExcelLogReport ( HttpServletResponse response , HttpServletRequest request 
																   ,@RequestParam(required=false) String cid
																   ,@RequestParam(required=false) String fileName) throws Exception {
		
		ModelAndView modelAndView = new ModelAndView();
		
		
		List<LinkedHashMap<String, String>> data = userExcelLogService.downExcelLogReport(cid);					// 데이터 가져오기
		
		List<String> column = new ArrayList<String>();
		for (Map.Entry<String, String> entry : data.get(0).entrySet()) {
			column.add(entry.getKey());
		}
		
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");


        
        
		modelAndView.addObject("data", data);
		modelAndView.addObject("columnNm", column);
		String filename = fileName+"_"+sdf.format(d)+".xls";
		modelAndView.addObject("fileName", filename);
		modelAndView.setViewName("excelDownload");
		return modelAndView;
	
	}
	
	@RequestMapping(value="/admin/audit/downExcelLogDate", method={RequestMethod.GET})
	public ModelAndView downExcelLogDate ( HttpServletResponse response , HttpServletRequest request 
																   ,@RequestParam(required=false) String start_dat
																   ,@RequestParam(required=false) String end_dat
																   ,@RequestParam(required=false) String fileName) throws Exception {
		
		
		ModelAndView modelAndView = new ModelAndView();
		
		
		List<LinkedHashMap<String, String>> data = userExcelLogService.downExcelLogDate(start_dat,end_dat);					// 데이터 가져오기
		
		List<String> column = new ArrayList<String>();
		for (Map.Entry<String, String> entry : data.get(0).entrySet()) {
			column.add(entry.getKey());
			System.out.println(entry.getKey());
		}
		
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");


        
        
		modelAndView.addObject("data", data);
		modelAndView.addObject("columnNm", column);
		String filename= fileName+"_"+sdf.format(d)+".xls";
		modelAndView.addObject("fileName", filename);
		modelAndView.setViewName("excelDownload");
		return modelAndView;
	
	}
	
}
