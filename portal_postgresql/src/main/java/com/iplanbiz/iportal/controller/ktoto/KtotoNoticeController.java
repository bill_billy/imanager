package com.iplanbiz.iportal.controller.ktoto;

import java.beans.Encoder;
import java.net.URLEncoder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dto.ktoto.KtotoNotice;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.cms.NoticeService;
import com.iplanbiz.iportal.service.file.FileService;
import com.iplanbiz.iportal.service.ktoto.KtotoNoticeService;

@Controller
public class KtotoNoticeController {

	@Autowired
	KtotoNoticeService ktotoService;
	@Autowired
	FileService fileService;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/ktoto/notice/list", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView list (HttpServletResponse response, HttpServletRequest request) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/ktoto/notice/list");
		
		return modelAndView;
	}
	@RequestMapping(value="/ktoto/notice/crud", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView crud ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/ktoto/notice/crud");
		
		return modelAndView;
	}
	@RequestMapping(value="/ktoto/notice/popupUserList", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupUserList ( HttpServletResponse response, HttpServletRequest request) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/ktoto/notice/popupUserList");
		
		return modelAndView;
	}
	@RequestMapping(value="/ktoto/notice/setKtotoNoticeHitCount", method=RequestMethod.POST)
	public void setKtotoNoticeHitCount ( HttpServletResponse response, HttpServletRequest request
			                             , @RequestParam(required=false) String boardNo ) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		try {
			ktotoService.setKtotoNoticeHitCount(boardNo);
			msg.setSuccessText("성공");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Set Ktoto Notice HitCount Error : {}", e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value = "/ktoto/notice/deleteFile", method = RequestMethod.GET)
	public String deleteFile(
			@RequestParam(required = false, value = "boardNo") String boardNo,
			int fileId, String type, Model model) throws Exception {
		logger.debug("boardNo {} fileId {}", Utils.getString(boardNo, "").replaceAll("[\r\n]",""), fileId);
		logger.debug("type {} model {}", Utils.getString(type, "").replaceAll("[\r\n]",""), Utils.getString(model.toString(), "").replaceAll("[\r\n]",""));
		fileService.removeFile(fileId);
		String result = "redirect:/ktoto/notice/list";
		return result;
	}
	@SuppressWarnings("deprecation")
	@RequestMapping(value="/ktoto/notice/save", method={RequestMethod.POST, RequestMethod.GET})
	public String noticeSave(Model model, HttpServletResponse response, HttpServletRequest requeset, @ModelAttribute KtotoNotice notice, @RequestParam(required=false) String saveGubn
			, @RequestParam(required=false) String remoteIP, @RequestParam(required=false) String remoteOS, @RequestParam(required=false) String remoteBrowser) throws Exception {
		String result = "";
		String returnValue = "Y";
		AjaxMessage msg = new AjaxMessage();
		logger.info("Ktoto Notice Save Start");
		logger.info("Save GUBN = " + saveGubn);
		logger.info("Get FIle = " + notice.getFile());
		if(notice.getFile().length > 0) {
			for (CommonsMultipartFile cmfile : notice.getFile()) {
				String FileFilter = "txt,zip,jpg,png,xls,xlsx,ppt,pptx,doc,docx,hwp,pdf,mp4";
				String ext = (cmfile.getOriginalFilename().substring(cmfile
						.getOriginalFilename().lastIndexOf(".") + 1, cmfile
						.getOriginalFilename().length())).toLowerCase();
				if (FileFilter.indexOf(ext) != -1) {
					returnValue = "Y";
				} else {
					returnValue = "N";
				}
			}
		} else {   
			returnValue = "Y";
		}
		if("N".equalsIgnoreCase(saveGubn)) {
			if(returnValue.equals("N")) {
				result = Utils.getString(WebConfig.getSystemPortalUrl() + ":" + WebConfig.getPortalPort() + "/accessRight","");
			} else {
				ktotoService.insertNotice(notice);
				model.addAttribute("msg", URLEncoder.encode("저장되었습니다", "UTF-8"));
				if(notice.getPopup().equals("E")) {
					ktotoService.noticeSendEmail(notice.getTitle(), ktotoService.removeScriptTag(notice.getContext()), notice.getBoardNO(),remoteIP,remoteOS,remoteBrowser);
				}
				result = "redirect:/ktoto/notice/list";
			}
		}else if("U".equalsIgnoreCase(saveGubn)) {
			logger.info("Notice Udate Start");
			if(returnValue.equals("N")) {
				result = Utils.getString(WebConfig.getSystemPortalUrl() + ":" + WebConfig.getPortalPort() + "/accessRight","");
			} else {
				logger.info("Notice Udate  Query Start");
				model.addAttribute("msg", URLEncoder.encode("수정되었습니다.", "UTF-8"));
				ktotoService.updateNotice(notice);
				if(notice.getPopup().equals("E")) {
					ktotoService.noticeSendEmail(notice.getTitle(), ktotoService.removeScriptTag(notice.getContext()), notice.getBoardNO(),remoteIP,remoteOS,remoteBrowser);
				}
				result = "redirect:/ktoto/notice/list";
			}
		}
		return result;
	}
	@RequestMapping(value="/ktoto/notice/remove", method=RequestMethod.POST)
	public String noticeDelete ( @RequestParam(required=false) String boardNO) throws Exception {
		String result = "";
		try {
			ktotoService.removeNotice(boardNO);
			result = "redirect:/ktoto/notice/list";
		} catch ( Exception e ) {
			result = Utils.getString(WebConfig.getSystemPortalUrl() + ":" + WebConfig.getPortalPort() + "/accessRight","");
			e.printStackTrace();
			logger.error("ERROR > Delete Notice Data Error : {} ", e.getMessage());
		}
		return result;
	}
	@RequestMapping(value="/ktoto/notice/getKtotoNoticeList", method={RequestMethod.POST, RequestMethod.GET})
	public void getKtotoNoticeList ( HttpServletResponse response, HttpServletRequest request
			                         , @RequestParam(required=false, defaultValue="") String name
			                         , @RequestParam(required=false, defaultValue="") String beginStartDat
			                         , @RequestParam(required=false, defaultValue="") String beginEndDat
			                         , @RequestParam(required=false, defaultValue="") String endStartDat
			                         , @RequestParam(required=false, defaultValue="") String endEndDat ) throws Exception {
		JSONArray returnValue = new JSONArray();
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = ktotoService.getKtotoNoticeList(name, beginStartDat, beginEndDat, endStartDat, endEndDat);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다. : " + e.getMessage());
			logger.error("ERROR > Get Ktoto Notice List Data Controller Error : {}", e.getMessage());
		}
		
		msg.send(response);
	}
	@RequestMapping(value="/ktoto/notice/getKtotoNoticeDetail", method={RequestMethod.POST, RequestMethod.GET})
	public void getNoticeDetail ( HttpServletResponse response, HttpServletRequest request 
			                      , @RequestParam(required=false) String boardNO ) throws Exception {
		JSONObject returnValue = new JSONObject();
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = ktotoService.getNoticeDetail(boardNO);
			msg.setSuccessMessage(returnValue);
		} catch ( Exception e ) {
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
			logger.error("ERROR > Get Ktoto Notice Detail Data Controller Error : {}", e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/ktoto/notice/getNoticeDtailFileInfo", method={RequestMethod.POST, RequestMethod.GET})
	public void getNoticeDtailFileInfo ( HttpServletResponse response, HttpServletRequest request
			                             , @RequestParam(required=false) String boardNO ) throws Exception {
		JSONArray returnValue = new JSONArray();
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = ktotoService.getNoticeDtailFileInfo(boardNO);
			msg.setSuccessMessage(returnValue);
		} catch ( Exception e ) {
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
			logger.error("ERROR > Get Notice Detail File Info Controller Error : {}", e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/ktoto/notice/getKtotoNoticeUserList", method={RequestMethod.GET, RequestMethod.POST})
	public void getKtotoNoticeUserList ( HttpServletResponse response, HttpServletRequest request
			                             , @RequestParam(required=false) String searchValue ) throws Exception {
		JSONArray returnValue = new JSONArray();
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = ktotoService.getKtotoNoticeUserList(searchValue);
			msg.setSuccessMessage(returnValue);
		} catch ( Exception e ) {
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
			e.printStackTrace();
			logger.error("ERROR > Get Ktoto Notice User List Data Error : {}", e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/ktoto/notice/getKtotoNoticeInformList", method={RequestMethod.POST})
	public void getKtotoNoticeInformList ( HttpServletResponse response, HttpServletRequest request
			, @RequestParam(required=false) String num) throws Exception {
		JSONArray returnValue = new JSONArray();
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = ktotoService.getKtotoNoticeInformList();
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다. : " + e.getMessage());
			logger.error("ERROR > Get Ktoto Notice Inform List Data Controller Error : {}", e.getMessage());
		}
		
		msg.send(response);
	}
	@RequestMapping(value="/ktoto/notice/getKtotoNoticeAuthList", method={RequestMethod.POST, RequestMethod.GET})
	public void getKtotoNoticeAuthList ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		JSONArray returnValue = new JSONArray();
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = ktotoService.getKtotoNoticeAuthList();
			msg.setSuccessMessage(returnValue);
		} catch (Exception e){
			msg.setExceptionText("오류 발생했습니다 " + e.getMessage());
			e.printStackTrace();
			logger.error("ERROR > Get Ktoto Notice Authorty List Data Error : {}", e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/ktoto/notice/getKtotoNoticeSelectAuthData", method={RequestMethod.POST, RequestMethod.GET})
	public void getKtotoNoticeSelectAuthData ( HttpServletResponse response, HttpServletRequest request
			                                   , @RequestParam(required=false) String boardNO ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = ktotoService.getKtotoNoticeSelectAuthData(boardNO);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Ktoto Notice Select Auth Data Error : {}", e.getMessage());
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/ktoto/notice/getKtotoNoticePopupList", method={RequestMethod.POST, RequestMethod.GET})
	public void getKtotoNoticePopupList ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		JSONArray returnValue = new JSONArray();
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = ktotoService.getKtotoNoticePopupList();
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Ktoto Notice Popup List Data Error : {}", e.getMessage());
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
}
