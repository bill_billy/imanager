package com.iplanbiz.iportal.controller.custom;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.dto.KtotoUser;
import com.iplanbiz.iportal.service.CodeService;
import com.iplanbiz.iportal.service.RoleService;
import com.iplanbiz.iportal.service.custom.KtotoUserService;
import com.iplanbiz.core.secure.GeneratePassword;
@Controller
public class KtotoUserController {
	
	@Autowired
	KtotoUserService ktotoUserService;
	@Autowired
	CodeService codeService;
	@Autowired
	RoleService roleService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/custom/ktotoUser/crud", method=RequestMethod.GET)
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView("/custom/ktotoUser/crud");
		return modelAndView;
	}
	@RequestMapping(value="/custom/ktotoUser/getCodeList", method=RequestMethod.POST)
	public void getCodeList( HttpServletResponse response, HttpServletRequest request
			                                    ,@RequestParam(required=false) String com_grp_cd) throws Exception{
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = ktotoUserService.getCodeList(com_grp_cd);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("ERROR : {}", e);
			msg.setExceptionText("데이터 로딩중 Error" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/custom/ktotoUser/getCheckUserID", method=RequestMethod.POST)
	public void getCheckUserID( HttpServletResponse response, HttpServletRequest request
			                    ,@RequestParam(required=false) String userID ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = ktotoUserService.getCheckUserID(userID);
			if(returnValue > 0) {
				msg.setExceptionText("중복 ID가 있습니다");
			} else if(returnValue == 0) {
				msg.setSuccessText("가능한 ID 입니다.");
			} else {
				msg.setExceptionText("오류가 발생했습니다");
			}
		} catch (Exception e) {
			logger.error("ERROR : Get Check User ID Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/custom/ktotoUser/getCheckOlapID", method=RequestMethod.POST)
	public void getCheckOlapID( HttpServletResponse response, HttpServletRequest request
			                    ,@RequestParam(required=false) String olapID ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = ktotoUserService.getCheckOlapID(olapID);
			if(returnValue > 0) {
				msg.setExceptionText("중복 ID가 있습니다");
			} else if(returnValue == 0) {
				msg.setSuccessText("가능한 ID 입니다.");
			} else {
				msg.setExceptionText("오류가 발생했습니다");
			}
		} catch (Exception e) {
			logger.error("ERROR : Get Check User ID Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/custom/ktotoUser/getUserList", method=RequestMethod.POST)
	public void getUserList( HttpServletResponse response, HttpServletRequest request
											   ,@RequestParam(required=false) String dept
											   ,@RequestParam(required=false) String team
											   ,@RequestParam(required=false) String status){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = ktotoUserService.getUserList(dept, team,status);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			msg.setExceptionText("데이터 불러오는 중 에러 : " + e.getMessage());
			logger.error("ERROR : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/custom/ktotoUser/resetPassword", method=RequestMethod.POST)
	public void resetPassword( HttpServletResponse response, HttpServletRequest request
															,@RequestParam(required=false) String type
															,@RequestParam(required=false) String unid
															,@RequestParam(required=false) String userId
															,@RequestParam(required=false) String remoteIP
															,@RequestParam(required=false) String remoteOS
															,@RequestParam(required=false) String remoteBrowser) throws Exception{
		int resultValue = 0;
		String serverUrl = request.getRequestURL().toString().replace(request.getRequestURI(),"");
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = ktotoUserService.resetPassword(type, unid, userId,remoteIP,remoteOS,remoteBrowser,serverUrl);
			if(resultValue == 1){
				msg.setSuccessText("비밀번호가 초기화 되었습니다.");
			}else if(resultValue == -1){//수정
				msg.setExceptionText("비밀번호 변경 중 오류가 발생했습니다");
			}else if(resultValue == -2){
				msg.setExceptionText("MAILERROR");
			}else if(resultValue == -3){
				msg.setExceptionText("SMSERROR");
			}else if(resultValue == 5){
				msg.setExceptionText("준비중입니다.");
			}else {
				msg.setExceptionText("비밀번호 초기화 중 오류가 발생했습니다 ");
			}
		}catch(Exception e){
			msg.setExceptionText("비밀번호 초기화 중 오류가 발생했습니다 " + e.getMessage());
			logger.error("ERROR : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/custom/ktotoUser/insert", method=RequestMethod.POST)
	public void insert( HttpServletResponse response, HttpServletRequest request,KtotoUser user
			                  ,@RequestParam(required=false) String checkValue
			                  ,@RequestParam(required=false) String remoteIP
			                  ,@RequestParam(required=false) String remoteOS
			                  ,@RequestParam(required=false) String remoteBrowser
			                  ,@RequestParam(required=false, defaultValue="") String orgPassword) throws Exception{
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			String userPassword = "";
			if(user.getUnID()==null||user.getUnID().equals("")) {
				userPassword=GeneratePassword.getPassword(); //임시비밀번호생성
				user.setUserPassword(userPassword);
			}	
			
			resultValue = ktotoUserService.saveKtotoUser(user, checkValue, remoteIP, remoteOS, remoteBrowser);
			if(resultValue == 0){//신규
				//resultValue = ktotoUserService.saveIect7003(user, checkValue, remoteIP, remoteOS, remoteBrowser);
				int sendResult = ktotoUserService.sendEmail(user.getUserID(),userPassword,user.getUserEmail());
				if(sendResult==1) {
					msg.setSuccessText("저장 되었습니다");
				}
				else {
					msg.setExceptionText("MAILERROR");
				}
			}else if(resultValue == 1){//수정
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다 " + e.getMessage());
			logger.error("ERROR : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/custom/ktotoUser/remove", method=RequestMethod.POST)
	public void remove( HttpServletResponse response, HttpServletRequest request
			                     ,@RequestParam(required=false) String userID
			                     ,@RequestParam(required=false) String unID
			                     ,@RequestParam(required=false) String remoteIP
			                     ,@RequestParam(required=false) String remoteOS
			                     ,@RequestParam(required=false) String remoteBrowser) throws Exception{
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = ktotoUserService.deleteUser(unID, userID, remoteIP, remoteOS, remoteBrowser);
			if(resultValue == 0){
				msg.setSuccessText("삭제되었습니다");
			}else{
				msg.setExceptionText("에러가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("에러 : " + e.getMessage());
			logger.error("User Delete Error : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/custom/ktotoUser/excelDownByUserInfo", method= RequestMethod.GET)
	public ModelAndView excelDownByUserInfo ( @RequestParam(required=false) String dept
					,@RequestParam(required=false) String team
					,@RequestParam(required=false) String status) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/custom/ktotoUser/excelDownByUserInfo");
		
		modelAndView.addObject("mainList", ktotoUserService.getUserListForExcel(dept, team,status));
		
		return modelAndView;
	}
	
}