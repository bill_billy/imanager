package com.iplanbiz.iportal.controller.admin;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.service.admin.MenuAuthLogService;
@Controller
public class MenuAuthLogController {
	
	@Autowired
	MenuAuthLogService menuAuthLogService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/admin/menuAuthLog/list", method=RequestMethod.GET)
	public ModelAndView list(){
		ModelAndView modelAndView = new ModelAndView("/admin/menuAuthLog/list");
		return modelAndView;
	}
	@RequestMapping(value="/admin/menuAuthLog/getLeftList", method=RequestMethod.POST)
	public void getLeftList(HttpServletResponse response, HttpServletRequest request
			,@RequestParam(required=false) String type,@RequestParam(required=false) String search
			,@RequestParam(required=false) String startDat,@RequestParam(required=false) String endDat){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			if(type.equals("R"))
				resultValue = menuAuthLogService.getRoleList(search,startDat,endDat);
			if(type.equals("G"))
				resultValue = menuAuthLogService.getGroupList(search,startDat,endDat);
			if(type.equals("U"))
				resultValue = menuAuthLogService.getUserList(search,startDat,endDat);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("ERROR : {}", e);
			msg.setExceptionText("데이터 로딩중 Error" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/menuAuthLog/getLogList", method=RequestMethod.POST)
	public void getLogList(HttpServletResponse response, HttpServletRequest request
			,@RequestParam(required=false) String type,@RequestParam(required=false) String id
			,@RequestParam(required=false) String startDat,@RequestParam(required=false) String endDat){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = menuAuthLogService.getLogList(type,id,startDat,endDat);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("ERROR : {}", e);
			msg.setExceptionText("데이터 로딩중 Error" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/menuAuthLog/excelDownByMenuAuthLog", method= RequestMethod.GET)
	public ModelAndView excelDownByUserInfo (@RequestParam(required=false) String type,@RequestParam(required=false) String id
			, @RequestParam(required=false) String startDat,@RequestParam(required=false) String endDat) throws Exception {
		ModelAndView modelAndView = new ModelAndView("/admin/menuAuthLog/excelDownByMenuAuthLog");
		modelAndView.addObject("mainList", menuAuthLogService.getLogListForExcel(type,id,startDat,endDat));
		
		return modelAndView;
	}
	
}