/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.iportal.controller.excelPgm;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.service.excelPgm.CloseEduStatsService;
import com.iplanbiz.iportal.service.excelPgm.StepCloseEduStatsService;


@Controller
public class CloseEduStatsController {
	
	@Autowired CloseEduStatsService closeEduStatService;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
		 
	/**
	 * 고등교육통계 > 통계마감처리
	 * -고등통계 진행관리
	 */
	@RequestMapping(value="/excelPgm/closeEduStats/crud", method={RequestMethod.GET})
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("excelPgm/closeEduStats/crud");		 
		return modelAndView; 
	}  
	@RequestMapping(value="/excelPgm/closeEduStats/getCommonCodeMmOrder", method={RequestMethod.POST})
	public void getCommonCodeMmOrder(HttpServletResponse response , HttpServletRequest request 
			   ,@RequestParam(required=false) String comlcd){
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = closeEduStatService.getCommonCodeMmOrder(comlcd);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getCommonCodeMmOrder Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
		
	}  
	@RequestMapping(value="/excelPgm/closeEduStats/getStepOneForCloseEduStat", method={RequestMethod.POST})
	public void getStepOneForCloseEduStat(HttpServletResponse response , HttpServletRequest request 
			   ,@RequestParam(required=false) String yyyy,@RequestParam(required=false) String mm){		 
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = closeEduStatService.getStepOneForCloseEduStat(yyyy,mm);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getStepOneForCloseEduStat Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
		
	}  
	@RequestMapping(value="/excelPgm/closeEduStats/closeEduStatsGetProgramID", method={RequestMethod.POST})
	public void closeEduStatsGetProgramID(HttpServletResponse response , HttpServletRequest request){
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = closeEduStatService.closeEduStatsGetProgramID();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("closeEduStatsGetProgramID Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}  
	@RequestMapping(value="/excelPgm/closeEduStats/getProceduerDetailByClose", method={RequestMethod.POST})
	public void getProceduerDetailByClose(HttpServletResponse response , HttpServletRequest request 
			   ,@RequestParam(required=false) String gubn){
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = closeEduStatService.getProceduerDetailByClose(gubn);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getProceduerDetailByClose Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}  
	@RequestMapping(value="/excelPgm/closeEduStats/getProcedureErrorByClose", method={RequestMethod.POST})
	public void getProcedureErrorByClose(HttpServletResponse response , HttpServletRequest request 
			   ,@RequestParam(required=false) String gubn){
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = closeEduStatService.getProcedureErrorByClose(gubn);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getProcedureErrorByClose Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}  
	@RequestMapping(value="/excelPgm/closeEduStats/closeEduStatsExcelList", method={RequestMethod.POST})
	public void closeEduStatsExcelList(HttpServletResponse response , HttpServletRequest request 
			   ,@RequestParam(required=false) String year,@RequestParam(required=false) String month){
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = closeEduStatService.closeEduStatsExcelList(year,month);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("closeEduStatsExcelList Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}  
	@RequestMapping(value="/excelPgm/closeEduStats/procedureStartYyyymmddBySch", method={RequestMethod.POST})
	public void procedureStartYyyymmddBySch(HttpServletResponse response , HttpServletRequest request 
			,@RequestParam(required=false)String[] procedureId,@RequestParam(required=false)String[] procedureNm
			,@RequestParam(required=false)String[] schID,@RequestParam(required=false)String[] schName
			,@RequestParam(required=false)String gubn,@RequestParam(required=false) String yyyy
			,@RequestParam(required=false) String mm,@RequestParam(required=false) String[] dbKey){
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			msg = closeEduStatService.procedureStartYyyymmddBySch(procedureId,procedureNm,schID,schName,gubn,yyyy,mm,dbKey);
		//	msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("procedureStartYyyymmddBySch Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}  
	@RequestMapping(value="/excelPgm/closeEduStats/download", method={RequestMethod.GET})
	public ModelAndView closeEduStatsDownload(@RequestParam(required=false) String year,@RequestParam(required=false) String month,@RequestParam(required=false) String dbkey){
		//ExcelPgmDownload
		ModelAndView modelAndView = new ModelAndView(); 
		
		List<LinkedHashMap<String, Object>> data = null;
		try {
			
			data = closeEduStatService.closeEduStatsDownload(year,month,dbkey);
			/*
			if(data == null){
				modelAndView.addObject("errMessage","데이터 다운로드 중 에러가 발생했습니다.");
				return modelAndView;
			}*/
			logger.info("data::"+data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		modelAndView.addObject("data", data);
		modelAndView.addObject("fileName", new String("standard.xls"));
		modelAndView.setViewName("excelPgm/closeEduStats/excelDownload");
		
		return modelAndView;
	}
	@RequestMapping(value="/excelPgm/closeEduStats/upload", method={RequestMethod.GET})
	public ModelAndView ExcelCloseUpload(@RequestParam(required=false) CommonsMultipartFile upFile,@RequestParam(required=false) String ac, @RequestParam(required=false) String dbkey, @RequestParam(required=false) String year, @RequestParam(required=false) String month){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("excelPgm/closeEduStats/closeEduStatsExcelUpload"); 
		return modelAndView;
	}   
	@RequestMapping(value="/excelPgm/closeEduStats/upload", method={RequestMethod.POST})
	public ModelAndView ExcelCloseUploadPOST(@RequestParam(required=false) CommonsMultipartFile upFile,@RequestParam(required=false) String ac, @RequestParam(required=false) String dbkey, @RequestParam(required=false) String year, @RequestParam(required=false) String month){
		ModelAndView modelAndView = new ModelAndView();
		AjaxMessage msg;
		if("upload".equals(Utils.getString(String.valueOf(ac), ""))){  
			msg = closeEduStatService.uploadCloseExcel(upFile, dbkey, year, month); 
			modelAndView.addObject("errMessage",msg.getReturnText());			
		} 
		modelAndView.setViewName("excelPgm/closeEduStats/closeEduStatsExcelUpload"); 
		return modelAndView;
	}   
}
