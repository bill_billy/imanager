package com.iplanbiz.iportal.controller.common;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.secure.GeneratePassword;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dto.KtotoUser;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.common.CodeMasterService;

@Controller
public class CodeMasterController {

	@Autowired
	CodeMasterService codeMasterService;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/common/codeMaster/crud", method=RequestMethod.GET)
	public ModelAndView crud() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("common/codeMaster/crud");
		return modelAndView;
	}
	@RequestMapping(value="/common/codeMaster/getGroupCodeList", method={RequestMethod.POST})
	public void getGroupCodeList ( HttpServletResponse response , HttpServletRequest request 
																   ,@RequestParam(required=false) String searchText) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = codeMasterService.getGroupCodeList(searchText);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getGroupCodeList Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/common/codeMaster/getCodeList", method={RequestMethod.POST})
	public void getCodeList ( HttpServletResponse response , HttpServletRequest request 
																   ,@RequestParam(required=false) String gCd) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = codeMasterService.getCodeList(gCd);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getCodeList Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/common/codeMaster/insertGroupCode", method=RequestMethod.POST)
	public void insertGroupCode( HttpServletResponse response, HttpServletRequest request
			 				  ,@RequestParam(required=false) String ac
			                  ,@RequestParam(required=false) String gCd
			                  ,@RequestParam(required=false) String gCdNm) throws Exception{
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = codeMasterService.insertGroupCode(gCd, gCdNm, ac);
			if(resultValue == 1){//신규
				msg.setSuccessText("저장 되었습니다");
			}else if(resultValue == 2){//수정
				msg.setSuccessText("수정 되었습니다");
			}else if(resultValue == 3) {//중복
				msg.setExceptionText("중복된 그룹코드가 존재합니다.");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다 " + e.getMessage());
			logger.error("ERROR : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/common/codeMaster/insertCommonCode", method=RequestMethod.POST)
	public void insertCommonCode( HttpServletResponse response, HttpServletRequest request
							  ,@RequestParam(required=false) String ac
			 				  ,@RequestParam(required=false) String gCd
			                  ,@RequestParam(required=false) String cCd
			                  ,@RequestParam(required=false) String cNm
			                  ,@RequestParam(required=false) String useYn
			                  ,@RequestParam(required=false, defaultValue="0") String sortSeq
			                  ,@RequestParam(required=false) String cDesc) throws Exception{
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = codeMasterService.insertCommonCode(gCd,cCd,cNm,useYn,Integer.parseInt(Utils.getString(sortSeq,"0")),cDesc,ac);
			if(resultValue == 1){//신규
				msg.setSuccessText("저장 되었습니다");
			}else if(resultValue == 2){//수정
				msg.setSuccessText("수정 되었습니다");
			}else if(resultValue == 3) {//중복
				msg.setExceptionText("중복된 코드가 존재합니다.");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다 " + e.getMessage());
			logger.error("ERROR : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/common/codeMaster/deleteGroupCode", method=RequestMethod.POST)
	public void deleteGroupCode( HttpServletResponse response, HttpServletRequest request
												,@RequestParam(required=false) String gCd) throws Exception{
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = codeMasterService.deleteGroupCode(gCd);
			if(resultValue == 1){
				msg.setSuccessText("삭제되었습니다");
			}else{
				msg.setExceptionText("에러가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("에러 : " + e.getMessage());
			logger.error("User Delete Error : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/common/codeMaster/deleteCommonCode", method=RequestMethod.POST)
	public void deleteCommonCode( HttpServletResponse response, HttpServletRequest request
												,@RequestParam(required=false) String gCd
												,@RequestParam(required=false) String cCd) throws Exception{
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = codeMasterService.deleteCommonCode(gCd,cCd);
			if(resultValue == 1){
				msg.setSuccessText("삭제되었습니다");
			}else{
				msg.setExceptionText("에러가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("에러 : " + e.getMessage());
			logger.error("User Delete Error : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	
}
