package com.iplanbiz.iportal.controller.admin;

import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.avro.ipc.HttpServer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.service.BasicService;
import com.iplanbiz.iportal.service.CodeService;
import com.iplanbiz.iportal.service.admin.MenuAuthoritySearchService;
import com.iplanbiz.iportal.service.admin.MenuAuthorityService;
import com.iplanbiz.iportal.service.RoleService;

@Controller
public class MenuAuthorityController {

	@Autowired MenuAuthoritySearchService menuService;
	@Autowired MenuAuthorityService menuAuthorityService;
	@Autowired RoleService roleService;
	@Autowired CodeService codeService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/admin/auth/crud", method=RequestMethod.GET)
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView("/admin/auth/crud");
		
		return modelAndView;
	}
	@RequestMapping(value="/admin/auth/crudByAdmin", method=RequestMethod.GET)
	public ModelAndView crudAdmin(){
		ModelAndView modelAndView = new ModelAndView("/admin/auth/crudByAdmin");
		
		return modelAndView;
	}
	@RequestMapping(value="/admin/auth/getRoleList", method=RequestMethod.POST)
	public void getRoleList( HttpServletResponse response, HttpServletRequest request
			          				  ,@RequestParam(required=false) String groupName) throws Exception{
		AjaxMessage msg = new AjaxMessage();
		JSONArray returnValue = null;
		try{
			returnValue = roleService.getRoleList(groupName);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("ERROR:{}", e);
			e.printStackTrace();
			msg.setExceptionText("에러가 발생했습니다 \n" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getRoleListAdmin", method=RequestMethod.POST)
	public void getRoleListAdmin ( HttpServletResponse response, HttpServletRequest request
							       ,@RequestParam(required=false) String groupName ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = roleService.getRoleListAdmin(groupName);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR : Get Role List Admin Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityTreeList", method=RequestMethod.POST)
	public void getMenuAuthorityTreeList( HttpServletResponse response, HttpServletRequest request
			                                     ,@RequestParam(required=false) String type
			                                     , @RequestParam(required=false) String fkey){
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.getMenuAuthorityTreeList(type, fkey);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			msg.setExceptionText("에러가 발생했습니다");
			logger.error("Error : {} " ,e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityTreeListAdmin", method=RequestMethod.POST)
	public void getMenuAuthorityTreeListAdmin ( HttpServletResponse response, HttpServletRequest request
											    , @RequestParam(required=false) String type
											    , @RequestParam(required=false) String fkey ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = menuAuthorityService.getMenuAuthorityTreeListAdmin(type, fkey);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR : Get MenuAuthority Tree List Admin Data Error : {}", e);
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityList", method=RequestMethod.POST)
	public void getMenuAuthorityList (HttpServletResponse response, HttpServletRequest request
			                                        ,@RequestParam(required=false) String fkey
			                                        ,@RequestParam(required=false) String type) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.getMenuAuthorityList(fkey, type);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			msg.setExceptionText("에러가 발생했습니다");
			logger.error("Error : {}" , e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityListAdmin", method=RequestMethod.POST)
	public void getMenuAuthorityListAdmin ( HttpServletResponse response, HttpServletRequest request
			                                , @RequestParam(required=false) String fkey
			                                , @RequestParam(required=false) String type ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityListAdmin(fkey, type);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			msg.setExceptionText("에러가 발생했습니다" + e.getMessage());
			logger.error("ERROR : Get MenuAuthority List Admin Data Error : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getGroupListNotInRole", method=RequestMethod.POST)
	public void getMenuAuthorityGroupNoList ( HttpServletResponse response, HttpServletRequest request
											  ,@RequestParam(required=false) String gID
											  ,@RequestParam(required=false) String userGrpName
											  ,@RequestParam(required=false) String grpTyp ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getGroupListNotInRole(gID, userGrpName, grpTyp);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			msg.setExceptionText("오류가 발생했습니다");
			logger.error("Error MenuAuthority Group No Select List Data : {}" ,e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityGroupNoListAdmin", method=RequestMethod.POST)
	public void getMenuAuthorityGroupNoListAdmin ( HttpServletResponse response, HttpServletRequest request
			                                       , @RequestParam(required=false) String gID
			                                       , @RequestParam(required=false) String userGrpName
			                                       , @RequestParam(required=false) String grpTyp ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityGroupNoListAdmin(gID, userGrpName, grpTyp);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
			logger.error("ERROR : Get MenuAuthority Group No List Admin Data Error : {}",e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityGroupList", method=RequestMethod.POST)
	public void getMenuAuthorityGroupList( HttpServletResponse response, HttpServletRequest request
			                                                 ,@RequestParam(required=false) String gID
			                                                 ,@RequestParam(required=false) String userGrpName
			                                                 ,@RequestParam(required=false) String grpTyp) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.getMenuAuthorityGroupList(gID, userGrpName, grpTyp);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			msg.setExceptionText("에러가 발생했습니다");
			logger.error("Error MenuAuthority Group Select List Data : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityGroupListAdmin", method=RequestMethod.POST)
	public void getMenuAuthorityGroupListAdmin ( HttpServletResponse response, HttpServletRequest request
			                                , @RequestParam(required=false) String gID
			                                , @RequestParam(required=false) String userGrpName
			                                , @RequestParam(required=false) String grpTyp ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityGroupListAdmin(gID, userGrpName, grpTyp);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
			logger.error("ERROR > Get MenuAuthority Group List Admin Data Error : {}", e);
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityUserListNoByRole", method= RequestMethod.POST)
	public void getMenuAuthorityUserListNoByRole ( HttpServletResponse response, HttpServletRequest request
												   ,@RequestParam(required=false) String gID
												   ,@RequestParam(required=false) String empID
												   ,@RequestParam(required=false) String empName
												   ,@RequestParam(required=false) String isNull ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityUserListNoByRole(gID, empID, empName, isNull);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("Error : {} ", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다. \n" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityUserListNoByRoleAdmin", method=RequestMethod.POST)
	public void getMenuAuthorityUserListNoByRoleAdmin ( HttpServletResponse response, HttpServletRequest request
													    , @RequestParam(required=false) String gID
													    , @RequestParam(required=false) String empID
													    , @RequestParam(required=false) String empName
													    , @RequestParam(required=false) String isNull ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityUserListNoByRoleAdmin(gID, empID, empName, isNull);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get MenuAuthority User List No By Role Admin Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityUserListByRole", method=RequestMethod.POST)
	public void getMenuAuthorityUserListByRole( HttpServletResponse response, HttpServletRequest request
			                                                        ,@RequestParam(required=false) String gID
			                                                        ,@RequestParam(required=false) String empID
			                                                        ,@RequestParam(required=false) String empName
			                                                        ,@RequestParam(required=false) String isNull ) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.getMenuAuthorityUserListByRole(gID, empID, empName, isNull);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("에러가 발생했습니다. \n" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityUserListByRoleAdmin", method=RequestMethod.POST)
	public void getMenuAuthorityUserListByRoleAdmin( HttpServletResponse response, HttpServletRequest request
			                                                        ,@RequestParam(required=false) String gID
			                                                        ,@RequestParam(required=false) String empID
			                                                        ,@RequestParam(required=false) String empName
			                                                        ,@RequestParam(required=false) String isNull) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.getMenuAuthorityUserListByRoleAdmin(gID, empID, empName, isNull);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("에러가 발생했습니다. \n" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/insertRoleInfo", method=RequestMethod.POST)
	public void insertRoleInfo( HttpServletResponse response, HttpServletRequest request
			                              ,@RequestParam(required=false) String roleID
			                              ,@RequestParam(required=false) String roleName
			                              ,@RequestParam(required=false) String roleNameOrg
			                              ,@RequestParam(required=false) String remoteIP
			                              ,@RequestParam(required=false) String remoteOS
			                              ,@RequestParam(required=false) String remoteBrowser) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.insertRoleInfo(roleID, roleName,roleNameOrg,remoteIP,remoteOS,remoteBrowser);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else if(returnValue == 1){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("에러가 발생했습니다.");
			}
		}catch(Exception e){
			logger.error("Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("에러가 발생했습니다. \n" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/insertRoleInfoAdmin", method=RequestMethod.POST)
	public void insertRoleInfoAdmin( HttpServletResponse response, HttpServletRequest request
			                              ,@RequestParam(required=false) String roleID
			                              ,@RequestParam(required=false) String roleName) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.insertRoleInfoAdmin(roleID, roleName);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else if(returnValue == 1){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("에러가 발생했습니다.");
			}
		}catch(Exception e){
			logger.error("Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("에러가 발생했습니다. \n" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/deleteRoleInfo", method=RequestMethod.POST)
	public void deleteRoleInfo( HttpServletResponse response, HttpServletRequest request
			                               ,@RequestParam(required=false) String roleID
			                               ,@RequestParam(required=false) String roleName
			                               ,@RequestParam(required=false) String remoteIP
				                           ,@RequestParam(required=false) String remoteOS
				                           ,@RequestParam(required=false) String remoteBrowser) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.deleteRoleInfo(roleID,roleName,remoteIP,remoteOS,remoteBrowser);
			if(returnValue == 0){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("에러가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Role Info Delere Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("에러가 발생했습니다. \n" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/deleteRoleInfoAdmin", method=RequestMethod.POST)
	public void deleteRoleInfoAdmin( HttpServletResponse response, HttpServletRequest request
			                               ,@RequestParam(required=false) String roleID) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.deleteRoleInfoAdmin(roleID);
			if(returnValue == 0){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("에러가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Role Info Delere Admin Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("에러가 발생했습니다. \n" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/insertMappingRole", method=RequestMethod.POST)
	public void insertRoleMapping( HttpServletResponse response , HttpServletRequest request
												 ,@RequestParam(required=false) String roleID
												 ,@RequestParam(required=false) String roleName
												 ,@RequestParam(required=false, defaultValue="") String[] menuCheckValue
												 ,@RequestParam(required=false, defaultValue="") String[] userCheckValue
												 ,@RequestParam(required=false, defaultValue="") String[] groupCheckValue
												 ,@RequestParam(required=false, defaultValue="") String[] menuCheckName
												 ,@RequestParam(required=false, defaultValue="") String[] userCheckName
												 ,@RequestParam(required=false, defaultValue="") String[] groupCheckName
												 ,@RequestParam(required=false) String remoteIP
						                         ,@RequestParam(required=false) String remoteOS
						                         ,@RequestParam(required=false) String remoteBrowser) throws Exception{
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.insertRoleMapping(roleID,roleName,menuCheckValue, userCheckValue, groupCheckValue,menuCheckName, userCheckName, groupCheckName,remoteIP,remoteOS,remoteBrowser);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else{
				msg.setExceptionText("에러가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Insert Role Mapping Error : {}", e);
			msg.setExceptionText("에러가 발생했습니다 \n" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/insertMappingRoleAdmin", method=RequestMethod.POST)
	public void insertMappingRoleAdmin( HttpServletResponse response , HttpServletRequest request
												 ,@RequestParam(required=false) String roleID
												 ,@RequestParam(required=false, defaultValue="") String[] menuCheckValue
												 ,@RequestParam(required=false, defaultValue="") String[] userCheckValue
												 ,@RequestParam(required=false, defaultValue="") String[] groupCheckValue) throws Exception{
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.insertRoleMappingAdmin(roleID, menuCheckValue, userCheckValue, groupCheckValue);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else{
				msg.setExceptionText("에러가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Insert Role Mapping Error : {}", e);
			msg.setExceptionText("에러가 발생했습니다 \n" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getGroupList", method=RequestMethod.POST)
	public void getGroupList( HttpServletResponse response, HttpServletRequest request
			                            ,@RequestParam(required=false, defaultValue="") String userGrpName
			                            ,@RequestParam(required=false, defaultValue="") String grpTyp){
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.getGroupList(userGrpName, grpTyp);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Group Data Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("데이터 로딩 중 에러가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getGroupListAdmin", method=RequestMethod.POST)
	public void getGroupListAdmin( HttpServletResponse response, HttpServletRequest request
			                            ,@RequestParam(required=false, defaultValue="") String userGrpName
			                            ,@RequestParam(required=false, defaultValue="") String grpTyp){
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.getGroupListAdmin(userGrpName, grpTyp);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Group Data Admin Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("데이터 로딩 중 에러가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getUsersInGroup", method=RequestMethod.POST)
	public void getUsersInGroup( HttpServletResponse response, HttpServletRequest request
																	   ,@RequestParam(required=false) String pID
																	   ,@RequestParam(required=false, defaultValue="") String empID
																	   ,@RequestParam(required=false, defaultValue="") String empName) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.getUsersInGroup(pID, empID, empName);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get MenuAuthority User Data By Group Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("에러가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getUsersInGroupAdmin", method=RequestMethod.POST)
	public void getUsersInGroupAdmin( HttpServletResponse response, HttpServletRequest request
																	   ,@RequestParam(required=false) String pID
																	   ,@RequestParam(required=false, defaultValue="") String empID
																	   ,@RequestParam(required=false, defaultValue="") String empName) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.getUsersInGroupAdmin(pID, empID, empName);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get MenuAuthority User Data By Group Admin Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("에러가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getRoleListNoByGroup", method=RequestMethod.POST)
	public void getRoleListNoByGroup ( HttpServletResponse response, HttpServletRequest request
									   ,@RequestParam(required=false) String fkey
									   ,@RequestParam(required=false, defaultValue="") String groupName) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getRoleListNoByGroup(fkey, groupName);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("Get Role Data No Select By Group Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getRoleListNoByGroupAdmin", method=RequestMethod.POST)
	public void getRoleListNoByGroupAdmin ( HttpServletResponse response, HttpServletRequest request
			                                , @RequestParam(required=false) String fkey
			                                , @RequestParam(required=false, defaultValue="" ) String groupName ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getRoleListNoByGroupAdmin(fkey, groupName);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("Get Role Data No Select By Group Admin Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getRoleListByGroup", method=RequestMethod.POST)
	public void getRoleListByGroup( HttpServletResponse response, HttpServletRequest request
												  ,@RequestParam(required=false) String fkey
												  ,@RequestParam(required=false, defaultValue="") String groupName) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.getRoleListByGroup(fkey, groupName);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Role Data By Group Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("에러가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getRoleListByGroupAdmin", method=RequestMethod.POST)
	public void getRoleListByGroupAdmin ( HttpServletResponse response, HttpServletRequest request
									      , @RequestParam(required=false) String fkey
									      , @RequestParam(required=false, defaultValue="") String groupName) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getRoleListByGroupAdmin(fkey, groupName);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("Get Role Data By Group Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/insertMappingGroup", method=RequestMethod.POST)
	public void insertGroupMapping( HttpServletResponse response, HttpServletRequest request
			                                       ,@RequestParam(required=false, defaultValue="") String groupID
			                                       ,@RequestParam(required=false, defaultValue="") String groupName
			                                       ,@RequestParam(required=false, defaultValue="") String[] menuCheckValue
			                                       ,@RequestParam(required=false, defaultValue="") String[] roleCheckValue
			                                       ,@RequestParam(required=false, defaultValue="") String[] menuCheckName
			                                       ,@RequestParam(required=false, defaultValue="") String[] roleCheckName
			                                       ,@RequestParam(required=false) String remoteIP
						                           ,@RequestParam(required=false) String remoteOS
						                           ,@RequestParam(required=false) String remoteBrowser)  throws Exception{
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.insertGroupMapping(groupID, groupName, menuCheckValue, roleCheckValue, menuCheckName, roleCheckName,remoteIP,remoteOS,remoteBrowser);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Insert Group Mapping Error : {}" , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/insertMappingGroupAdmin", method=RequestMethod.POST)
	public void insertGroupMappingAdmin( HttpServletResponse response, HttpServletRequest request
			                                       ,@RequestParam(required=false, defaultValue="") String groupID
			                                       ,@RequestParam(required=false, defaultValue="") String[] menuCheckValue
			                                       ,@RequestParam(required=false, defaultValue="") String[] roleCheckValue)  throws Exception{
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.insertGroupMappingAdmin(groupID, menuCheckValue, roleCheckValue);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Insert Group Mapping Admin Error : {}" , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityUserListNoByMenu", method=RequestMethod.POST)
	public void getMenuAuthorityUserListNoByMenu( HttpServletResponse response, HttpServletRequest request
												  ,@RequestParam(required=false) String cID
												  ,@RequestParam(required=false, defaultValue="") String empID
												  ,@RequestParam(required=false, defaultValue="") String empNm
												  ,@RequestParam(required=false, defaultValue="") String gubn) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityUserListNoByMenu(cID, empID, empNm, gubn);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("Get User List No Select By Menu Error : {}" ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityUserListNoByMenuAdmin", method=RequestMethod.POST)
	public void getMenuAuthorityUserListNoByMenuAdmin ( HttpServletResponse response, HttpServletRequest request
			                                            , @RequestParam(required=false) String cID
			                                            , @RequestParam(required=false, defaultValue="") String empID
			                                            , @RequestParam(required=false, defaultValue="") String empNm
			                                            , @RequestParam(required=false, defaultValue="") String gubn ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityUserListNoByMenuAdmin(cID, empID, empNm, gubn);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("Get User List No Select By Menu Admin Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getUsersInMenu", method=RequestMethod.POST)
	public void getUsersInMenu( HttpServletResponse response, HttpServletRequest request
																	  ,@RequestParam(required=false) String cID
																	  ,@RequestParam(required=false, defaultValue="") String empID
																	  ,@RequestParam(required=false, defaultValue="") String empNm) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.getUsersInMenu(cID, empID, empNm);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get User List By Menu Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getUsersInMenuAdmin", method=RequestMethod.POST)
	public void getUsersInMenuAdmin( HttpServletResponse response, HttpServletRequest request
																	  ,@RequestParam(required=false) String cID
																	  ,@RequestParam(required=false, defaultValue="") String empID
																	  ,@RequestParam(required=false, defaultValue="") String empNm) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.getUsersInMenuAdmin(cID, empID, empNm);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get User List By Menu Admin Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityGroupListNoSelectByMenu", method= RequestMethod.POST)
	public void getMenuAuthorityGroupListNoSelectByMenu ( HttpServletResponse response, HttpServletRequest request
														  ,@RequestParam(required=false) String cID
														  ,@RequestParam(required=false, defaultValue="") String userGrpNm ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = menuAuthorityService.getMenuAuthorityGroupListNoSelectByMenu(cID, userGrpNm);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("Get Group List No Select By Menu Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityGroupListNoSelectByMenuAdmin", method=RequestMethod.POST)
	public void getMenuAuthorityGroupListNoSelectByMenuAdmin ( HttpServletResponse response, HttpServletRequest request
			                                              , @RequestParam(required=false) String cID
			                                              , @RequestParam(required=false, defaultValue="") String userGrpNm ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityGroupListNoSelectByMenuAdmin(cID, userGrpNm);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("Get Group List No Select By Menu Admin Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
			
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityGroupListByMenu", method=RequestMethod.POST)
	public void getMenuAuthorityGroupListByMenu ( HttpServletResponse response, HttpServletRequest request
																		 ,@RequestParam(required=false) String cID
																		 ,@RequestParam(required=false, defaultValue="") String userGrpNm) throws Exception{
		 JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.getMenuAuthorityGroupListByMenu(cID, userGrpNm);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Group List By Menu Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityGroupListByMenuAdmin", method=RequestMethod.POST)
	public void getMenuAuthorityGroupListByMenuAdmin ( HttpServletResponse response, HttpServletRequest request
			                                           , @RequestParam(required=false) String cID
			                                           , @RequestParam(required=false, defaultValue = "") String userGrpNm ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityGroupListByMenuAdmin(cID, userGrpNm);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("Get MenuAuthority Group List By Menu Admin Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityRoleListNoSelectByMenu", method=RequestMethod.POST)
	public void getMenuAuthorityRoleListNoSelectByMenu ( HttpServletResponse response, HttpServletRequest request
														 ,@RequestParam(required=false) String cID
														 ,@RequestParam(required=false, defaultValue="") String groupName ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityRoleListNoSelectByMenu(cID, groupName);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("Get Group List No Select By Menu Error : {} ", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityRoleListNoSelectByMenuAdmin", method=RequestMethod.POST)
	public void getMenuAuthorityRoleListNoSelectByMenuAdmin ( HttpServletResponse response, HttpServletRequest request
			                                             , @RequestParam(required=false) String cID
			                                             , @RequestParam(required=false, defaultValue="") String groupName ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityRoleListNoSelectByMenuAdmin(cID, groupName);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR : Get MenuAuthority ROle List No Select By Menu Admin Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityRoleListByMenu", method=RequestMethod.POST)
	public void getMenuAuthorityRoleListByMenu ( HttpServletResponse response, HttpServletRequest request
																		 ,@RequestParam(required=false) String cID
																		 ,@RequestParam(required=false, defaultValue="") String groupName) throws Exception{
		 JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.getMenuAuthorityRoleListByMenu(cID, groupName);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Group List By Menu Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityRoleListByMenuAdmin", method=RequestMethod.POST)
	public void getMenuAuthorityRoleListByMenuAdmin ( HttpServletResponse response, HttpServletRequest request
			                                          , @RequestParam(required=false) String cID
			                                          , @RequestParam(required=false, defaultValue="") String groupName ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityRoleListByMenuAdmin(cID, groupName);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("Error > Get MenuAuthority Role List By Menu Admin Data Error : {}" , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
//	insertMappingMenu
	@RequestMapping(value="/admin/auth/insertMappingMenu", method=RequestMethod.POST)
	public void insertMappingMenu( HttpServletResponse response, HttpServletRequest request
			                                       ,@RequestParam(required=false, defaultValue="") String menuID
			                                       ,@RequestParam(required=false, defaultValue="") String menuName
			                                       ,@RequestParam(required=false, defaultValue="") String[] userCheckValue
			                                       ,@RequestParam(required=false, defaultValue="") String[] groupCheckValue
			                                       ,@RequestParam(required=false, defaultValue="") String[] roleCheckValue
			                                       ,@RequestParam(required=false, defaultValue="") String[] userCheckName
			                                       ,@RequestParam(required=false, defaultValue="") String[] groupCheckName
			                                       ,@RequestParam(required=false, defaultValue="") String[] roleCheckName
			                                       ,@RequestParam(required=false) String remoteIP
						                           ,@RequestParam(required=false) String remoteOS
						                           ,@RequestParam(required=false) String remoteBrowser)  throws Exception{
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.insertMenuMapping(menuID, menuName,userCheckValue, groupCheckValue, roleCheckValue,userCheckName, groupCheckName, roleCheckName,remoteIP,remoteOS,remoteBrowser);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Insert Group Mapping Error : {}" , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/insertMappingMenuAdmin", method=RequestMethod.POST)
	public void insertMappingMenuAdmin( HttpServletResponse response, HttpServletRequest request
			                                       ,@RequestParam(required=false, defaultValue="") String menuID
			                                       ,@RequestParam(required=false, defaultValue="") String[] userCheckValue
			                                       ,@RequestParam(required=false, defaultValue="") String[] groupCheckValue
			                                       ,@RequestParam(required=false, defaultValue="") String[] roleCheckValue)  throws Exception{
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuAuthorityService.insertMenuMappingAdmin(menuID, userCheckValue, groupCheckValue, roleCheckValue);
			if(returnValue == 0){
				msg.setSuccessText("저장 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("Insert Group Mapping Admin Error : {}" , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getGrpTypList", method=RequestMethod.POST)
	public void getGrpTypList ( HttpServletResponse response, HttpServletRequest request ) throws Exception {
		List<LinkedHashMap<String,Object>> returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = menuAuthorityService.getGrpTypList();
			JSONObject returnObject = new JSONObject();
			returnObject.put("returnArray", returnValue);
			msg.setSuccessMessage(returnObject);
		}catch(Exception e){
			logger.error("Get Typ List Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getBusinessList", method=RequestMethod.POST)
	public void getBusinessList ( HttpServletResponse response, HttpServletRequest request
								  ,@RequestParam(required=false, defaultValue="") String businessName ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getBusinessList(businessName);
			msg.setSuccessMessage(returnValue);
		} catch(Exception e) {
			logger.error("Error : Get Business List Data : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getBusinessListAdmin", method=RequestMethod.POST)
	public void getBusinessListAdmin ( HttpServletResponse response, HttpServletRequest request
	                                   , @RequestParam(required=false, defaultValue="") String businessName ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getBusinessListAdmin(businessName);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR : Get Buniness List Admin Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityUserListNoByData", method=RequestMethod.POST)
	public void getMenuAuthorityUserListNoByData ( HttpServletResponse response, HttpServletRequest request
		      											,@RequestParam(required=false) String bID
														,@RequestParam(required=false, defaultValue="") String empID
														,@RequestParam(required=false, defaultValue="") String empName
														,@RequestParam(required=false, defaultValue="") String gubn) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityUserListNoByData(bID, empID, empName, gubn);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority User List No By Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityUserListNoByDataAdmin", method=RequestMethod.POST)
	public void getMenuAuthorityUserListNoByDataAdmin ( HttpServletResponse response, HttpServletRequest request
		      											,@RequestParam(required=false) String bID
														,@RequestParam(required=false, defaultValue="") String empID
														,@RequestParam(required=false, defaultValue="") String empName
														,@RequestParam(required=false, defaultValue="") String gubn) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityUserListNoByDataAdmin(bID, empID, empName, gubn);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority User List No By Data Admin Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityUserListByData", method=RequestMethod.POST)
	public void getMenuAuthorityUserListByData ( HttpServletResponse response, HttpServletRequest request
												      ,@RequestParam(required=false) String bID
													  ,@RequestParam(required=false, defaultValue="") String empID
													  ,@RequestParam(required=false, defaultValue="") String empName
													  ,@RequestParam(required=false, defaultValue="") String gubn ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityUserListByData(bID, empID, empName, gubn);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority User List By Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityUserListByDataAdmin", method=RequestMethod.POST)
	public void getMenuAuthorityUserListByDataAdmin ( HttpServletResponse response, HttpServletRequest request
												      ,@RequestParam(required=false) String bID
													  ,@RequestParam(required=false, defaultValue="") String empID
													  ,@RequestParam(required=false, defaultValue="") String empName
													  ,@RequestParam(required=false, defaultValue="") String gubn ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityUserListByDataAdmin(bID, empID, empName, gubn);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority User List By Data Admin Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityGroupListNoByData", method=RequestMethod.POST)
	public void getMenuAuthorityGroupListNoByData ( HttpServletResponse response, HttpServletRequest request
														 ,@RequestParam(required=false) String bID
														 ,@RequestParam(required=false) String grpTyp
														 ,@RequestParam(required=false) String userGrpName ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityGroupListNoByData(bID, grpTyp, userGrpName);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority User List No By Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityGroupListByData", method=RequestMethod.POST)
	public void getMenuAuthorityGroupListByData ( HttpServletResponse response, HttpServletRequest request 
													   ,@RequestParam(required=false) String bID
													   ,@RequestParam(required=false, defaultValue="") String grpTyp
													   ,@RequestParam(required=false, defaultValue="") String userGrpName ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityGroupListByData(bID, grpTyp, userGrpName);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("Error > Get Menu Authority User List By Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityGroupListByDataAdmin", method=RequestMethod.POST)
	public void getMenuAuthorityGroupListByDataAdmin ( HttpServletResponse response, HttpServletRequest request 
													   ,@RequestParam(required=false) String bID
													   ,@RequestParam(required=false, defaultValue="") String grpTyp
													   ,@RequestParam(required=false, defaultValue="") String userGrpName ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityGroupListByDataAdmin(bID, grpTyp, userGrpName);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("Error > Get Menu Authority User List By Data Admin Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityRoleListNoByData", method=RequestMethod.POST)
	public void getMenuAuthorityRoleListNoByData ( HttpServletResponse response, HttpServletRequest request 
													    ,@RequestParam(required=false) String bID
													    ,@RequestParam(required=false, defaultValue="") String groupName ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityRoleListNoByData(bID, groupName);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority Role List No By Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityRoleListNoByDataAdmin", method=RequestMethod.POST)
	public void getMenuAuthorityRoleListNoByDataAdmin ( HttpServletResponse response, HttpServletRequest request 
													    ,@RequestParam(required=false) String bID
													    ,@RequestParam(required=false, defaultValue="") String groupName ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityRoleListNoByDataAdmin(bID, groupName);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority Role List No By Data Admin Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityRoleListByData", method=RequestMethod.POST)
	public void getMenuAuthorityRoleListByData ( HttpServletResponse response, HttpServletRequest request
													  ,@RequestParam(required=false) String bID
													  ,@RequestParam(required=false) String groupName ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityRoleListByData(bID, groupName);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority Role List Data By Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthorityRoleListByDataAdmin", method=RequestMethod.POST)
	public void getMenuAuthorityRoleListByDataAdmin ( HttpServletResponse response, HttpServletRequest request
													  ,@RequestParam(required=false) String bID
													  ,@RequestParam(required=false) String groupName ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.getMenuAuthorityRoleListByDataAdmin(bID, groupName);
			msg.setSuccessMessage(returnValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority Role List Data By Data Admin Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/insertDataInfo", method=RequestMethod.POST)
	public void insertDataInfo ( HttpServletResponse response, HttpServletRequest request
								 ,@RequestParam(required=false, defaultValue="") String dataID
								 ,@RequestParam(required=false) String dataName 
								 ,@RequestParam(required=false) String dataNameOrg
								 ,@RequestParam(required=false) String remoteIP
		                         ,@RequestParam(required=false) String remoteOS
		                         ,@RequestParam(required=false) String remoteBrowser) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.insertDataInfo(dataID, dataName, dataNameOrg,remoteIP,remoteOS,remoteBrowser);
			if(returnValue == 0) {
				msg.setSuccessText("저장 되었습니다");
			} else if(returnValue == 1) {
				msg.setSuccessText("수정 되었습니다");
			} else {
				msg.setExceptionText("오류가 발생했습니다");
			}
		} catch (Exception e) {
			logger.error("ERROR > Insert/Update Data Info Error {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/insertDataInfoAdmin", method=RequestMethod.POST)
	public void insertDataInfoAdmin ( HttpServletResponse response, HttpServletRequest request
								 ,@RequestParam(required=false, defaultValue="") String bID
								 ,@RequestParam(required=false) String dataName ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.insertDataInfoAdmin(bID, dataName);
			if(returnValue == 0) {
				msg.setSuccessText("저장 되었습니다");
			} else if(returnValue == 1) {
				msg.setSuccessText("수정 되었습니다");
			} else {
				msg.setExceptionText("오류가 발생했습니다");
			}
		} catch (Exception e) {
			logger.error("ERROR > Insert/Update Data Info Admin Error {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/deleteDataInfo", method=RequestMethod.POST)
	public void deleteDataInfo ( HttpServletResponse response, HttpServletRequest request
							     ,@RequestParam(required=false) String dataID
							     ,@RequestParam(required=false) String dataName
							     ,@RequestParam(required=false) String remoteIP
		                         ,@RequestParam(required=false) String remoteOS
		                         ,@RequestParam(required=false) String remoteBrowser) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		
		try {
			returnValue = menuAuthorityService.deleteDataInfo(dataID,dataName,remoteIP,remoteOS,remoteBrowser);
			if(returnValue == 0) {
				msg.setSuccessText("삭제 되었습니다");
			} else {
				msg.setExceptionText("오류가 발생했습니다");
			}
		} catch (Exception e) {
			logger.error("ERROR > Delete Data Info/Authority Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다 " + e.getMessage());
		}
		msg.send(response);
	}
 
	@RequestMapping(value="/admin/auth/insertDataMapping", method={RequestMethod.POST})
	public void insertDataMapping ( HttpServletResponse response, HttpServletRequest request
									,@RequestParam(required=false) String dataID
									,@RequestParam(required=false) String dataName
									,@RequestParam(required=false, defaultValue="") String[] userCheckValue
									,@RequestParam(required=false, defaultValue="") String[] groupCheckValue
									,@RequestParam(required=false, defaultValue="") String[] roleCheckValue 
									,@RequestParam(required=false, defaultValue="") String[] userCheckName
									,@RequestParam(required=false, defaultValue="") String[] groupCheckName
									,@RequestParam(required=false, defaultValue="") String[] roleCheckName
									,@RequestParam(required=false) String remoteIP
			                        ,@RequestParam(required=false) String remoteOS
			                        ,@RequestParam(required=false) String remoteBrowser) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.insertDataMapping(dataID, dataName, userCheckValue, groupCheckValue, roleCheckValue, userCheckName, groupCheckName, roleCheckName,remoteIP,remoteOS,remoteBrowser);
			if(returnValue == 0) {
				msg.setSuccessText("권한이 적용되었습니다");
			} else {
				msg.setExceptionText("오류가 발생했습니다");
			}
		} catch ( Exception e ) {
			logger.error("ERROR > Insert Data Authority User/Group/Role Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/insertDataMappingAdmin", method={RequestMethod.POST})
	public void insertDataMappingAdmin ( HttpServletResponse response, HttpServletRequest request
									,@RequestParam(required=false) String dataID
									,@RequestParam(required=false, defaultValue="") String[] userCheckValue
									,@RequestParam(required=false, defaultValue="") String[] groupCheckValue
									,@RequestParam(required=false, defaultValue="") String[] roleCheckValue ) throws Exception {
		int returnValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try {
			returnValue = menuAuthorityService.insertDataMappingAdmin(dataID, userCheckValue, groupCheckValue, roleCheckValue);
			if(returnValue == 0) {
				msg.setSuccessText("권한이 적용되었습니다");
			} else {
				msg.setExceptionText("오류가 발생했습니다");
			}
		} catch ( Exception e ) {
			logger.error("ERROR > Insert Data Authority User/Group/Role Data Admin Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/admin/auth/list", method=RequestMethod.GET)
	public ModelAndView list () throws Exception {
		ModelAndView modelAndView = new ModelAndView("/admin/auth/list");
		
		return modelAndView;
	}
	@RequestMapping(value="/admin/auth/getMenuAuthroitySearchUserList", method=RequestMethod.POST)
	public void getMenuAuthroitySearchUserList ( HttpServletResponse response, HttpServletRequest request
																	 ,@RequestParam(required=false, defaultValue="") String empName
																	 ,@RequestParam(required=false, defaultValue="") String userGrpName) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuService.getMenuAuthroitySearchUserList(empName, userGrpName);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Menu Authority Search User List Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/admin/auth/getMenuAuthoritySearchList", method=RequestMethod.POST)
	public void getMenuAuthoritySearchList ( HttpServletResponse response, HttpServletRequest request 
																,@RequestParam(required=false) String empID ) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuService.getMenuAuthoritySearchList(empID,"5999");
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Menu Authority Search List Data Error {} " , e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthoritySearchGroupByEmp", method=RequestMethod.POST)
	public void getMenuAuthoritySearchGroupByEmp ( HttpServletResponse response, HttpServletRequest request
																			,@RequestParam(required=false) String empID) throws Exception{
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuService.getMenuAuthoritySearchGroupByEmp(empID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Menu Authority Search Group By Emp List Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthoritySearchRoleByEmp", method=RequestMethod.POST)
	public void getMenuAuthoritySearchRoleByEmp ( HttpServletResponse response, HttpServletRequest request
																		 ,@RequestParam(required=false) String empID) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuService.getMenuAuthoritySearchRoleByEmp(empID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Menu Authority Search Role By Emp Data : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니가 " + e.getLocalizedMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthoritySearchMenuByReport", method=RequestMethod.POST)
	public void getMenuAuthoritySearchMenuByReport ( HttpServletResponse response, HttpServletRequest request) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = menuService.getMenuAuthoritySearchMenuByReport();
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Menu Authority Search Menu By Report Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());			 
		}
		msg.send(response);
	}
	@RequestMapping(value="/admin/auth/getMenuAuthoritySearchMenuUserByReport", method=RequestMethod.POST)
	public void getMenuAuthoritySearchMenuUserByReport ( HttpServletResponse response, HttpServletRequest request 
																					,@RequestParam(required=false) String cID) throws Exception {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			returnValue = menuService.getMenuAuthoritySearchMenuUserByReport(cID);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Menu Authority Search Menu User By Report Data Error : {}", e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
}
