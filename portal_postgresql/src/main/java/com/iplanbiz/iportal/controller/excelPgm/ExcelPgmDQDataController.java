package com.iplanbiz.iportal.controller.excelPgm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.message.AjaxCode;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dto.ExcelDQDTO;
import com.iplanbiz.iportal.dto.ExcelPgmDQ;
import com.iplanbiz.iportal.service.excelPgm.ExcelPgmDQDataService;

@Controller
public class ExcelPgmDQDataController {
	@Autowired ExcelPgmDQDataService excelPgmDQDataService;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/**
	 * 엑셀 자료 조회(DQ연동)<br/>
	 */
	@RequestMapping(value="/ExcelPgmDQData", method={RequestMethod.GET})
	public ModelAndView ExcelPgmDQData()  {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("excelPgm/excelPgmDQData/crud");
		/*
		*/
		
		return mav;
	}
	
	/**
	 * 엑셀 프로그램 관리(DQ)
	 *  - 등록된 엑셀 프로그램 검색
	 */
	@RequestMapping(value="/ExcelPgmDQData/excelPgmDQSearchProgram", method={RequestMethod.GET})
	public ModelAndView searchExcelProgramDQ(@RequestParam(required=false) String pCd, @RequestParam(required=false) String cCd){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("excelPgm/excelPgmDQData/excelPgmDQSearchProgram");
		return modelAndView;
	}
	@RequestMapping(value="/ExcelPgmDQData/getProgramList", method={RequestMethod.POST})
	public void getProgramList(HttpServletResponse response , HttpServletRequest request,@RequestParam(required=false) String pCd,
			@RequestParam(required=false) String cCd, @RequestParam(required=false) String searchText) {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = excelPgmDQDataService.getProgramList(pCd,cCd,searchText);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getProgramList Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/ExcelPgmDQData/getExcelPgmDQDataRowId", method={RequestMethod.POST})
	public void getExcelPgmDQDataRowId(HttpServletResponse response , HttpServletRequest request,@RequestParam(required=false) String pCd,
			@RequestParam(required=false) String cCd, @RequestParam(required=false) String pgmId, @RequestParam(required=false) String rowId) {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			//excelPgmDQDataService.getExcelPgmDQDataRowId(pCd,cCd,pgmId,rowId);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getExcelPgmDQDataRowId Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/ExcelPgmDQData/getExcelPgmDQData", method={RequestMethod.POST})
	public void getExcelPgmDQData(HttpServletResponse response , HttpServletRequest request,@RequestParam(required=false) String pCd,
			@RequestParam(required=false) String cCd, @RequestParam(required=false) String pgmId) {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			excelPgmDQDataService.getExcelPgmDQData(pCd,cCd,pgmId);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("getExcelPgmDQData Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	
	
	
	
	
	
}
