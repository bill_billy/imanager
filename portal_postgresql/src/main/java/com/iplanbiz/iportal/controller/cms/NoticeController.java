package com.iplanbiz.iportal.controller.cms;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dto.Board;
import com.iplanbiz.iportal.dto.File;
import com.iplanbiz.iportal.dto.Notice;
import com.iplanbiz.iportal.service.cms.BoardService;
import com.iplanbiz.iportal.service.file.FileService;
import com.iplanbiz.iportal.service.cms.NoticeService;

@Controller
public class NoticeController {

	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired NoticeService noticeService;
	@Autowired BoardService boardService;
	@Autowired FileService fileService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value = "/cms/notice/portlet", method = RequestMethod.GET)
	public ModelAndView portlet(@ModelAttribute Notice notice) {
		ModelAndView modelAndView = new ModelAndView();
		notice.setStartDay1("0000-00-00");
		notice.setEndDay1("9999-99-99");
		modelAndView.addObject("noticeList",noticeService.getNoticeList(notice));
		notice.setInformNum(5);
		modelAndView.addObject("informList", noticeService.getInformList(notice));
		modelAndView.setViewName("/cms/notice/portlet");
		return modelAndView;
	}
	@RequestMapping(value = "/cms/notice/popupList", method = RequestMethod.POST)
	public void popup(HttpServletResponse response , HttpServletRequest request ,@RequestParam(required=false) String START_DAT
			   ,@RequestParam(required=false) String END_DAT) {
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = noticeService.getPopupList(START_DAT,END_DAT);
			System.out.println("popupList:::"+returnValue);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get popup List Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
		
	}
	
	@RequestMapping(value = "/cms/notice/list", method = RequestMethod.GET)
	public ModelAndView list(@ModelAttribute Notice notice) {

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/cms/notice/list");
		return modelAndView;
	}
	
	
	@RequestMapping(value="/cms/notice/getNoticeList", method={RequestMethod.POST})
	public void getNoticeList ( HttpServletResponse response , HttpServletRequest request 
																   ,@RequestParam(required=false) String TITLE
																   ,@RequestParam(required=false) String CONTENT
																   ,@RequestParam(required=false) String NAME
																   ,@RequestParam(required=false) String GUBN
																   ,@RequestParam(required=false) String START_DAT
																   ,@RequestParam(required=false) String END_DAT) throws Exception {
		
		JSONArray returnValue = null;
		AjaxMessage msg = new AjaxMessage();
		
		try{
			returnValue = noticeService.getNoticeList(TITLE,CONTENT,NAME,GUBN,START_DAT,END_DAT);
			msg.setSuccessMessage(returnValue);
		}catch(Exception e){
			logger.error("Get Scheduler Manager List Data Error : {} " ,e);
			e.printStackTrace();
			msg.setExceptionText("오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value = "/cms/notice/form", method = RequestMethod.GET)
	public String form(
			@RequestParam(required = false, value = "boardNo") String boardNo,
			String type, Model model) throws Exception {

		Map<String, String> noticeMap = new HashMap<String, String>();

		if (boardNo != null && boardNo.length() > 0) { // 수정
			//model.addAttribute("notice", noticeService.getNoticeNew(new Notice(Integer.parseInt(boardNo))).get(0));
			noticeMap = noticeService.getNoticeNew(new Notice(Integer.parseInt(boardNo))).get(0);

			File file = new File();
			file.setBoardNo(Integer.parseInt(String.valueOf(noticeMap.get("boardno"))));
			file.setTableNm("NOTICE");

			model.addAttribute("fileList", fileService.getFileList(file));
		} else { // 신규
			noticeMap = noticeService.getNewNotice();

		}
		noticeMap.put("inform_yn",
				Utils.getString(noticeMap.get("inform_yn"), "n"));
		model.addAttribute("notice", noticeMap);
		String returnURL = "";
		if ("M".equalsIgnoreCase(type) || "I".equalsIgnoreCase(type)) {
			model.addAttribute("type", type);
			returnURL = "/cms/notice/form";
		} else {
			returnURL = Utils.getString(WebConfig.getSystemPortalUrl() + ":"
					+ WebConfig.getPortalPort() + "/accessRight", "");
		}
		return returnURL;
	}

	@RequestMapping(value = "/cms/notice/save", method = RequestMethod.POST)
	public String save(Notice notice, String type, Model model)
			throws Exception {
		String result = "";
		String returnValue = "";
		if (notice.getFile().length > 0) {
			for (CommonsMultipartFile cmfile : notice.getFile()) {
				String FileFilter = "txt,zip,jpg,png,xls,xlsx,ppt,pptx,doc,docx,hwp,pdf,mp4";
				String ext = (cmfile.getOriginalFilename().substring(cmfile
						.getOriginalFilename().lastIndexOf(".") + 1, cmfile
						.getOriginalFilename().length())).toLowerCase();
				if (FileFilter.indexOf(ext) != -1) {
					returnValue = "Y";
				} else {
					returnValue = "N";
				}
			}
		} else {
			returnValue = "Y";
		}
		if ("M".equalsIgnoreCase(type)) { // 수정
			if (returnValue == "N") {
				result = Utils.getString(WebConfig.getSystemPortalUrl() + ":"
						+ WebConfig.getPortalPort() + "/accessRight", "");
			} else {
				noticeService.modifyNotice(notice);

				Map<String, String> noticeMap = new HashMap<String, String>();
				noticeMap = noticeService.getNoticeNew(notice).get(0);
				// noticeMap.put("CONTENT",
				// noticeMap.get("CONTENT").replaceAll(" ",
				// "&nbsp;").replaceAll("\n", "<BR>"));
				model.addAttribute("notice", noticeMap);

				File file = new File();
				file.setBoardNo(Integer.parseInt(String.valueOf(noticeMap.get("boardno"))));
				file.setTableNm("NOTICE");
				model.addAttribute("fileList", fileService.getFileList(file));

				result = "/cms/notice/get";
			}
			
		} else if ("I".equalsIgnoreCase(type)) { // 신규
			if (returnValue == "Y") {
				noticeService.createNotice(notice);
				result = "redirect:/cms/notice/list";
			} else {
				result = Utils.getString(WebConfig.getSystemPortalUrl() + ":"
						+ WebConfig.getPortalPort() + "/accessRight", "");
			}
		}
		return result;
	}
	@SuppressWarnings("unused")
	@RequestMapping(value = "/cms/notice/get", method = RequestMethod.GET)
	public ModelAndView get(
			@RequestParam(required = false, value = "boardNo") int boardNo,
			@RequestParam(required = false, value = "type") String type) {
		ModelAndView modelAndView = new ModelAndView();
		Notice notice = new Notice(boardNo);
		noticeService.modifyHitCount(notice); // 조회수 + 1

		/* Map<String,String> noticeMap = new HashMap<String,String>();
		 noticeMap = noticeService.getNoticeNew(notice).get(0);
		 if(noticeMap.get("CONTENT") != null){
		 noticeMap.put("CONTENT", noticeMap.get("CONTENT").replaceAll(" ","&nbsp;").replaceAll("\n", "<BR>"));
		  
		 }*/
		 modelAndView.addObject("notice", noticeService.getNoticeNew(notice).get(0));
		 //modelAndView.addObject("notice", noticeMap);
		 
		 List<HashMap<String,String>> notBuList = noticeService.getNoticeBuser(notice);
		 String notBu = "";
		 if((notBuList!=null) && (notBuList.size() >0)){
			 if(notBuList.get(0)!=null)
				 notBu=notBuList.get(0).get("user_grp_nm");
		 }
		/* List<HashMap<String,String>>noticeNextList = noticeService.getNoticeNext(notice);
		 String noticeNext = "";
		 if(noticeNextList.size() >0){
			 noticeNext=noticeNextList.get(0).get("CONTENT");
		 }
		 */
		 modelAndView.addObject("notBu", notBu);
		 //modelAndView.addObject("noticeNext",noticeNext);

		File file = new File();
		file.setBoardNo(boardNo);
		file.setTableNm("NOTICE");
		modelAndView.addObject("fileList", fileService.getFileList(file));

		if (Utils.getString(type, "").equals("popup")) {
			modelAndView.setViewName("/cms/notice/popup");
		}
		return modelAndView;
	}
	@RequestMapping(value = "/cms/notice/delete", method = RequestMethod.GET)
	public String delete(int boardNo) throws Exception {
		noticeService.removeNotice(new Notice(boardNo));
		return "redirect:/cms/notice/list";
	}

	@RequestMapping(value = "/cms/notice/deleteFile", method = RequestMethod.GET)
	public String deleteFile(
			@RequestParam(required = false, value = "boardNo") String boardNo,
			int fileId, String type, Model model) throws Exception {
		logger.debug("boardNo {} fileId {}", Utils.getString(boardNo, "").replaceAll("[\r\n]",""), fileId);
		logger.debug("type {} model {}", Utils.getString(type, "").replaceAll("[\r\n]",""), Utils.getString(model.toString(), "").replaceAll("[\r\n]",""));
		fileService.removeFile(fileId);
		return form(boardNo, type, model);
	}

	@RequestMapping(value = "/cms/notice/download", method = { RequestMethod.GET})
	public ModelAndView download(@RequestParam int fileId) throws SQLException {

		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("downloadFile", fileService.getFile(fileId));
		modelAndView.setViewName("downloadView");

		return modelAndView;
	}
}
