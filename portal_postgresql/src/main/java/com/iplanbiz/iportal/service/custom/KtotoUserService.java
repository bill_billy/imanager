package com.iplanbiz.iportal.service.custom;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
 
import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.email.EmailService;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.admin.UserDAO;
import com.iplanbiz.iportal.dao.custom.KtotoUserDAO;
import com.iplanbiz.iportal.dto.KtotoUser;
import com.iplanbiz.iportal.dto.User;

@Service
public class KtotoUserService {
	
	@Autowired
	KtotoUserDAO ktotoUserDAO;
	public String getKtotoMaxUnID() throws Exception {
		return ktotoUserDAO.getKtotoMaxUnID();
	}
	
	/*public int saveIect7003(KtotoUser user, String checkValue, String remoteIP, String remoteOS, String remoteBrowser) throws Exception{
		int result = 0;
		result = ktotoUserDAO.saveIect7003(user, checkValue, remoteIP, remoteOS, remoteBrowser);
		return result;
	}*/
	public int saveKtotoUser(KtotoUser user, String checkValue, String remoteIP, String remoteOS, String remoteBrowser) throws Exception{
		int result = 0;
		result = ktotoUserDAO.saveKtotoUser(user, checkValue, remoteIP, remoteOS, remoteBrowser);
		return result;
	}
	
	public int getCheckUserID ( String userID ) throws Exception {
		return ktotoUserDAO.getCheckUserID(userID);
	}
	public int getCheckOlapID ( String olapID ) throws Exception {
		return ktotoUserDAO.getCheckOlapID(olapID);
	}
	
	public String getSystemName() {
		return ktotoUserDAO.getSystemValue("system_name");
	}
	public String getSystemCopyRight() {
		return ktotoUserDAO.getSystemValue("copyright");
	}
	public int sendEmail(String userID, String userPassword, String userEmail) {
		String systemNm = this.getSystemName();
		String copyRight = this.getSystemCopyRight();
	    String subject = "["+systemNm+"] 임시 비밀번호 발급 안내"; // 메일 제목
	    // 메일 내용
	    
	    /*
	    String message = "<html><body>"+
	    "<p><b style='color:blue;'>["+systemNm+"]</b>에서 발급된 임시 비밀번호 안내 메일입니다.</p>"+ 
	    "<br><br>" + 
	    "<table style='border: 1px solid #444444;border-collapse: collapse;width:500px;'>"+
	    "<tr><th colspan=2 style=\"height:30px;\">회원 아이디/임시비밀번호</th></tr>"+
	    "<tr><th style='border: 1px solid #444444;width:200px;height:30px;'>아이디</th><td style='border: 1px solid #444444;text-align:center;'>"+userID+"</td></tr>"+
	    "<tr><th \r\n" + 
	    "\r\n" + 
	    "style='border: 1px solid #444444;width:200px;height:30px;'>비밀번호</th><td style='border: 1px solid #444444;text-align:center;'>"+userPassword+"</td></tr></table>"+
	    "<br><br>" + 
	    "<p style='color:gray;'>해당 비밀번호는 임시 비밀번호이므로 로그인 후 비밀번호를 변경해 주시기 바랍니다.</p><p style='color:gray;'>감사합니다.</p></body></html>"; 
*/
	    String message = "<html>\r\n" + 
	    		"    <head>\r\n" + 
	    		"    <meta charset=\"utf-8\">\r\n" + 
	    		"    <title id='Description'>임시 비밀번호 안내</title>\r\n" + 
	    		"\r\n" + 
	    		"</head>\r\n" + 
	    		"<body style=\"font-size:12px; background:#eeefef; margin:0; padding:0; font-family: Malgun Gothic,Verdana, Arial; \">\r\n" + 
	    		"<div style=\"width:98%; min-width:600px; margin:0 1%;\">\r\n" + 
	    		"  <div style=\"float:left; width:100%; margin:30px 0;\">\r\n" + 
	    		"    <div style=\"width:650px; margin:0 auto; padding:0px;\">\r\n" + 
	    		"  	 <h1 style=\"margin:0; padding:0;\"><img src=\"cid:image1\"></h1>\r\n" + 
	    		"  	 <h2 style=\"color:#FFF; background:#003664; font-size:21px; letter-spacing:-1px; line-height:45px; margin:0; padding: 0 0 3px 0px; text-align:center;\">\r\n" + 
	    		"     임시 비밀번호 안내</h2>\r\n" + 
	    		"       \r\n" + 
	    		"       <div style=\"width:648px; height:410px; background-color:#FFF; border:1px solid #d8d8d8;\">\r\n" + 
	    		"  	   <div style=\"color:#424242; font-size:20px; font-weight:normal; text-align:center; letter-spacing:-1px; background:url('cid:image2') center 40px no-repeat; padding:120px 0 0 0; line-height:28px;\">\r\n" + 
	    		systemNm+"에서 발급된<br>\r\n" + 
	    		"  	     <span style=\"color:#0166c2;\"> 임시 비밀번호 안내</span> 메일입니다.<br>\r\n" + 
	    		"         <span style=\"color:#666666; font-size:12px; font-weight:normal; letter-spacing:-1px; margin-top:20px;\">* 해당 비밀번호는 임시 비밀번호이므로 <span style=\"color:#000;\">로그인 후 비밀번호를 변경</span>해 주시기 바랍니다.</span>       \r\n" + 
	    		"       </div>\r\n" + 
	    		"       \r\n" + 
	    		"		<div style=\"width:400px; margin:0 auto; margin-top:30px; padding:25px 30px 30px 40px; font-size:15px; color:#666666; background:#f9f9f9;\">\r\n" + 
	    		"          <div style=\"line-height:32px;\">\r\n" + 
	    		"            <span>아이디 : </span>\r\n" + 
	    		"            <span style=\"color:#000;font-weight:bold; margin-left:10px;\">"+userID+"</span>\r\n" + 
	    		"          </div>\r\n" + 
	    		"          <div style=\"line-height:32px;\">\r\n" + 
	    		"            <span>임시비밀번호 : </span>\r\n" + 
	    		"            <span style=\"color:#000; margin-left:10px;\">"+userPassword+"</span>\r\n" + 
	    		"          </div>\r\n" + 
	    		"        </div>\r\n" + 
	    		"       </div>\r\n" + 
	    		"  <div style=\"color:#868686; font-size:11px; margin-top:15px; text-align:center;\">"+copyRight+"</div>       \r\n" + 
	    		"  </div>\r\n" + 
	    		"</div>\r\n" + 
	    		"</div>\r\n" + 
	    		"</body>\r\n" + 
	    		"</html>";
	    // SMTP 서버 설정
	    final String host = WebConfig.getSmtpHost(); // 사용할 smtp host, naver라면 smtp.naver.com
	    final String accountId = WebConfig.getSmtpAdminId();
	    final String accountPwd = WebConfig.getSmtpAdminPwd();
	    final int port = Integer.parseInt(Utils.getString(WebConfig.getSmtpPort(), "0")); // SMTP 포트
	    String sender = WebConfig.getSmtpAdminEmail(); // 보내는사람 이메일 
	    String receiver = userEmail; // 받는사람 이메일
	    EmailService emailService = new EmailService(host,accountId,accountPwd,port,sender,receiver,subject,message);
	    HashMap<String,String> imageMap = new HashMap<String,String>();
	    
	    imageMap.put("image1", WebConfig.getDefaultServerRoot()+"/resources/img/n/logo_sch.gif");
	    imageMap.put("image2", WebConfig.getDefaultServerRoot()+"/resources/img/img_icons/ico_lock.png");
	    
	    //이메일로그찍기
	    
	    return emailService.sendHtml(imageMap);
	}
	public JSONArray getCodeList(String com_grp_cd) throws Exception {
		return ktotoUserDAO.getCodeList(com_grp_cd);
	}
	public JSONArray getUserList(String dept, String team, String status) throws Exception {
		return ktotoUserDAO.getUserList(dept,team,status);
	}
	public int deleteUser(String unID, String userID, String remoteIP, String remoteOS, String remoteBrowser) throws Exception {
		int result = 0;
		result = ktotoUserDAO.deleteUser(userID, unID, remoteIP, remoteOS, remoteBrowser);
		return result;
	}
	public List<LinkedHashMap<String, Object>> getUserListForExcel(String dept, String team, String status) {
		return ktotoUserDAO.getUserListForExcel(dept,team,status);
	} 
	public Map<String, Object> getIpAddress(String unid) throws Exception{
		return ktotoUserDAO.getIpAddress(unid);
	}
	public Map<String, Object> getKtotoUserInfo(String unid) throws Exception{
		return ktotoUserDAO.getKtotoUserInfo(unid);
	}

	public int resetPassword(String type, String unid, String userId, String remoteIP, String remoteOS, String remoteBrowser,String serverUrl) throws Exception {
		return ktotoUserDAO.resetPassword(type,unid,userId,remoteIP,remoteOS,remoteBrowser,serverUrl);
	}
	public int insertEmailLog(String userId,String userName,String email,String type,String key,String remoteIP, String remoteOS, String remoteBrowser) {
		return ktotoUserDAO.insertEmailLog(userId, userName, email, type, key, remoteIP, remoteOS, remoteBrowser);
	}
	
}
