package com.iplanbiz.iportal.service.admin;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.admin.MenuAuthoritySearchDAO;

@Service
@Transactional(readOnly=true)
public class MenuAuthoritySearchService {

	@Autowired MenuAuthoritySearchDAO menuDAO;
	
	public JSONArray getMenuAuthroitySearchUserList( String empName, String userGrpName ) throws Exception {
		return menuDAO.getMenuAuthroitySearchUserList(empName, userGrpName);
	}
	public JSONArray getMenuAuthoritySearchList( String empID ,String parentMenuID ) throws Exception{
		return menuDAO.getMenuAuthoritySearchList(empID,parentMenuID);
	}
	public JSONArray getMenuAuthoritySearchGroupByEmp( String empID ) throws Exception {
		return menuDAO.getMenuAuthoritySearchGroupByEmp(empID);
	}
	public JSONArray getMenuAuthoritySearchRoleByEmp ( String empID ) throws Exception {
		return menuDAO.getMenuAuthoritySearchRoleByEmp(empID);
	}
	public JSONArray getMenuAuthoritySearchMenuByReport () throws Exception {
		return menuDAO.getMenuAuthoritySearchMenuByReport();
	}
	public JSONArray getMenuAuthoritySearchMenuUserByReport ( String cID ) throws Exception {
		return menuDAO.getMenuAuthoritySearchMenuUserByReport(cID);
	}

}
