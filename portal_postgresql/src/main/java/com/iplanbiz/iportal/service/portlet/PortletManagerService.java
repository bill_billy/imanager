package com.iplanbiz.iportal.service.portlet;

import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.portlet.PortletManagerDAO;

@Service
public class PortletManagerService {

	@Autowired PortletManagerDAO portletManagerDAO;
	
	public JSONArray getPortletManagerList () throws Exception {
		return portletManagerDAO.getPortletManagerList();
	}
	public int defalutPortletSetting ( String plID) throws Exception {
		return portletManagerDAO.defalutPortletSetting( plID );
	}
	public int defaultUserPortlet () throws Exception {
		return portletManagerDAO.defaultUserPortlet();
	}
	public JSONArray getPortletInfo ( String plID ) throws Exception {
		return portletManagerDAO.getPortletInfo(plID);
	}
	public JSONArray getPageletPortletList ( String pID, String searchValue) throws Exception {
		return portletManagerDAO.getPageletPortletList(pID, searchValue);
	}
	public JSONArray getPortletCategory () throws Exception {
		return portletManagerDAO.getPortletCategory();
	}
	public int updatePageletInfo ( String plName, String plLayout, String plID) throws Exception {
		return portletManagerDAO.updatePageletInfo(plName, plLayout, plID);
	}
	public int deletePageletPortlet ( String plID ) throws Exception {
		return portletManagerDAO.deletePageletPortlet(plID);
	}
	public int insertPageletPortlet ( String plID, String poID, String xAxis, String yAxis, JSONObject cssInfo) throws Exception {
		return portletManagerDAO.insertPageletPortlet(plID, poID, xAxis, yAxis, cssInfo);
	}
	public int updatePageletTheme ( String portletType, String plID, String poID ) throws Exception {
		return portletManagerDAO.updatePageletTheme(portletType, plID, poID);
	}
	public int insertPageletInfo ( String plName, String plLayout ) throws Exception {
		return portletManagerDAO.insertPageletInfo(plName, plLayout);
	}
	public int deletePageletInfo ( String plID ) throws Exception {
		return portletManagerDAO.deletePageletInfo(plID);
	}
}
