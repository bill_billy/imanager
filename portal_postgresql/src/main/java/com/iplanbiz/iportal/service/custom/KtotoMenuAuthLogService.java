package com.iplanbiz.iportal.service.custom;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
 
import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.custom.KtotoMenuAuthLogDAO;

@Service
public class KtotoMenuAuthLogService {
	
	@Autowired
	KtotoMenuAuthLogDAO ktotoMenuAuthLogDAO;
	
	public JSONArray getRoleList(String rolenm, String startDat, String endDat) throws Exception{
		return ktotoMenuAuthLogDAO.getRoleList(rolenm,startDat,endDat);
	}
	public JSONArray getGroupList(String groupnm, String startDat, String endDat) throws Exception{
		return ktotoMenuAuthLogDAO.getGroupList(groupnm,startDat,endDat);
	}
	public JSONArray getUserList(String usernm, String startDat, String endDat) throws Exception{
		return ktotoMenuAuthLogDAO.getUserList(usernm,startDat,endDat);
	}
	public JSONArray getLogList(String type, String id, String startDat, String endDat) throws Exception{
		return ktotoMenuAuthLogDAO.getLogList(type,id,startDat,endDat);
	}
	public List<LinkedHashMap<String, Object>> getLogListForExcel(String type, String id, String startDat, String endDat){
		return ktotoMenuAuthLogDAO.getLogListForExcel(type,id,startDat,endDat);
	}
}
