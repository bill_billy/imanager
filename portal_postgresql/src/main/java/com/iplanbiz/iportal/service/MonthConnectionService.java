package com.iplanbiz.iportal.service;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.MonthConnectionDAO;

@Service
@Transactional(readOnly=true)
public class MonthConnectionService {

	@Autowired 
	MonthConnectionDAO monthDAO;
	
	public JSONArray getMonthUserYear() throws Exception{
		return monthDAO.getMonthUserYear();
	}
	public JSONArray getMonthUserConnectionTotal(String year) throws Exception{
		return monthDAO.getMonthUserConnectionTotal(year);
	}
	public JSONArray getMonthUserConnectionList(String year) throws Exception {
		return monthDAO.getMonthUserConnectionList(year);
	}

}
