package com.iplanbiz.iportal.service.excelPgm;

import java.util.LinkedHashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.dao.excelPgm.CloseEduStatsDAO;
import com.iplanbiz.iportal.dao.excelPgm.StepCloseEduStatsDAO;



@Service
public class CloseEduStatsService {

	@Autowired
	CloseEduStatsDAO closeEduStatDAO;
	
	public JSONArray getCommonCodeMmOrder(String comlcd) throws Exception {
		return closeEduStatDAO.getCommonCodeMmOrder(comlcd);
	}
	public JSONArray getStepOneForCloseEduStat(String yyyy, String mm) throws Exception {
		return closeEduStatDAO.getStepOneForCloseEduStat(yyyy,mm);
	}
	public JSONArray closeEduStatsGetProgramID() throws Exception {
		return closeEduStatDAO.closeEduStatsGetProgramID();
	}
	public JSONArray getProceduerDetailByClose(String gubn) throws Exception {
		return closeEduStatDAO.getProceduerDetailByClose(gubn);
	}
	public JSONArray getProcedureErrorByClose(String gubn) throws Exception {
		return closeEduStatDAO.getProcedureErrorByClose(gubn);
	}
	
	public JSONArray closeEduStatsExcelList(String year, String month) throws Exception {
		return closeEduStatDAO.closeEduStatsExcelList(year,month);
	}
	public AjaxMessage procedureStartYyyymmddBySch(String[] procedureId, String[] procedureNm, String[] schID, String[] schName, String gubn, String yyyy, String mm, String[] dbKey) throws Exception {
		return closeEduStatDAO.procedureStartYyyymmddBySch(procedureId,procedureNm,schID,schName,gubn,yyyy,mm,dbKey);
	}
	public List<LinkedHashMap<String, Object>> closeEduStatsDownload(String year, String month, String dbkey){
		return closeEduStatDAO.closeEduStatsDownload(year,month,dbkey);
	}
	public AjaxMessage uploadCloseExcel(CommonsMultipartFile upFile, String dbkey, String year, String month) {
		return closeEduStatDAO.uploadCloseExcel(upFile,dbkey,year,month);
	}
}
