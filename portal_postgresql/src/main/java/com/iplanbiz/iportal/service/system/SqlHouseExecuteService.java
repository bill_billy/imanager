package com.iplanbiz.iportal.service.system;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.system.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class SqlHouseExecuteService {
	
	@Autowired 
	SqlHouseExecuteDAO sqlHouseExecuteDAO;

	public List<LinkedHashMap<String,Object>> selectHashMap(int idx, HashMap<String,Object> param) throws Exception{
		return sqlHouseExecuteDAO.selectHashMap(idx, param);
	}
	public JSONArray selectJSONArray(int idx, HashMap<String,Object> param) throws Exception{
		return sqlHouseExecuteDAO.selectJSONArray(idx, param);
	}
}
