package com.iplanbiz.iportal.service;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.EtlResultDAO;

@Service
public class EtlResultService {

	@Autowired EtlResultDAO etlResultDAO;
	
	public JSONArray getEtlResultList ( String sourceTable, String targetTable ) throws Exception {
		return etlResultDAO.getEtlResultList(sourceTable, targetTable);
	}
	public JSONArray getEtlResultErrorList () throws Exception {
		return etlResultDAO.getEtlResultErrorList();
	}
}
