package com.iplanbiz.iportal.service.dashboard;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.DashboardPathManagerDAO;

@Service
public class DashboardPathManagerService {

	@Autowired DashboardPathManagerDAO pathManagerDAO;
	
	public JSONArray getDashboardPathList() throws Exception {
		return pathManagerDAO.getDashboardPathList();
	}
	public JSONArray getDashboardPathDetail( String pathIdx ) throws Exception{
		return pathManagerDAO.getDashboardPathDetail(pathIdx);
	}
	public int insertDashboardPath(String pathIdx, String fullPath, String pathDesc, String bigo, String useYn) throws Exception{
		return pathManagerDAO.insertDashboardPath(pathIdx, fullPath, pathDesc, bigo, useYn);
	}
	public int removeDashboardPath(String[] pathIdx) throws Exception{
		return pathManagerDAO.removeDashboardPath(pathIdx);
	}

}
