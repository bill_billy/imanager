package com.iplanbiz.iportal.service.admin;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.admin.DbinfoManagerDAO;
import com.iplanbiz.iportal.dto.DbInfo;

@Service
public class DbInfoManagerService {

	@Autowired DbinfoManagerDAO dbInfoDAO;
	
	public JSONArray getDbInfoList() throws Exception{
		return dbInfoDAO.getDbInfoList();
	}
	public JSONArray getDbInfoDetail( String dbKey ) throws Exception {
		return dbInfoDAO.getDbInfoDetail(dbKey);
	}
	public int insertDbInfo( DbInfo dbinfo ) throws Exception {
		return dbInfoDAO.insertDbInfo(dbinfo);
	}
	public int removeDbInfo ( DbInfo dbinfo ) throws Exception {
		return dbInfoDAO.removeDbInfo(dbinfo);
	}
	public int checkDbInfo ( String dbKey ) throws Exception {
		return dbInfoDAO.checkDbInfo(dbKey);
	}
	public String checkDbKey ( String dbKey ) throws Exception {
		return dbInfoDAO.checkDbKey(dbKey);
	}

}
