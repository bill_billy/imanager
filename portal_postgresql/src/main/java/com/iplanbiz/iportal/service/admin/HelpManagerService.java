package com.iplanbiz.iportal.service.admin;

import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.admin.HelpManagerDAO;

@Service
public class HelpManagerService {

	@Autowired 
	HelpManagerDAO helpManagerDAO;
	
	public JSONArray helpManagerMenuList() throws Exception {
		return helpManagerDAO.helpManagerMenuList();
	}
	public JSONArray helpManagerMenuListByUser () throws Exception {
		return helpManagerDAO.helpManagerMenuListByUser();
	}
	public JSONArray getHelpManagerMenuGrid(String searchType, String searchValue) throws Exception{
		return helpManagerDAO.getHelpManagerMenuGrid(searchType, searchValue);
	}
	public JSONArray getHelpManagerMenuDetail(String cID) throws Exception {
		return helpManagerDAO.getHelpManagerMenuDetail(cID);
	}
	public JSONArray getHelpManagerMenuDetailByUser ( String cID ) throws Exception {
		return helpManagerDAO.getHelpManagerMenuDetailByUser(cID);
	}
	public int insertHelpManager(String cID, String summarycontent, String content, String etcContent, String dept, String deptCd, String chargeDept, String chargeEmp, String confirmDept, String confirmEmp, String updatePer) throws Exception{
		return helpManagerDAO.insertHelpManager(cID, summarycontent, content, etcContent, dept, deptCd, chargeDept, chargeEmp, confirmDept, confirmEmp, updatePer);
	}
	public int insertHelpManagerByUser( String cID, String content, String chargeEmp, String confirmEmp ) throws Exception {
		return helpManagerDAO.insertHelpManagerByUser(cID, content, chargeEmp, confirmEmp);
	}
	public int deleteHelpManagerMenu(String cID) throws Exception{
		return helpManagerDAO.deleteHelpManagerMenu(cID);
	}
	public List<HashMap<String, Object>> getSummaryContentData ( String cID ) throws Exception {
		return helpManagerDAO.getSummaryContentData(cID);
	}
	public JSONArray getHelpPopup ( String cID ) throws Exception {
		return helpManagerDAO.getHelpPopup(cID);
	}
	public JSONArray getNaviByHelpManager ( String cID ) throws Exception {
		return helpManagerDAO.getNaviByHelpManager(cID);
	}
	public JSONArray getPopHelpManagerList ( String userGrpNm ) throws Exception {
		return helpManagerDAO.getPopHelpManagerList(userGrpNm);
	}
}
