package com.iplanbiz.iportal.service.ktoto;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.ktoto.KtotoBoardDAO;
import com.iplanbiz.iportal.dao.ktoto.KtotoNoticeDAO;
import com.iplanbiz.iportal.dto.ktoto.KtotoBoard;
import com.iplanbiz.iportal.dto.ktoto.KtotoNotice;

@Service
public class KtotoBoardService {

	@Autowired 
	KtotoBoardDAO ktotoDAO;
	
	public JSONArray getKtotoBoardList ( String boardType, String startDat, String endDat, String titleName) throws Exception {
		return ktotoDAO.getKtotoBoardList(boardType, startDat, endDat, titleName);
	}
	public JSONObject getKtotoBoardDetail ( String boardType, String boardNO ) throws Exception {
		return ktotoDAO.getKtotoBoardDetail(boardType, boardNO);
	}
	public JSONArray getKtotoBoardFileList ( String tableNM , String boardNO ) throws Exception {
		return ktotoDAO.getKtotoBoardFileList(tableNM, boardNO);
	}
	public void insertBoard ( KtotoBoard board ) throws Exception {
		ktotoDAO.insertBoard(board);
	}
	public void updateBoard ( KtotoBoard board ) throws Exception {
		ktotoDAO.updateBoard(board);
	}
	public void removeBoard ( String boardType, String boardNO ) throws Exception {
		ktotoDAO.removeBoard(boardType, boardNO);
	}
	public JSONObject getKtotoBoardUserName () throws Exception {
		return ktotoDAO.getKtotoBoardUserName();
	}
	public void setKtotoBoardHitCount ( String boardNo, String boardType ) throws Exception {
		ktotoDAO.setKtotoBoardHitCount(boardNo, boardType);
	}
	public JSONArray getKtotoBoardReplyList ( String boardType, String boardNo, String startNum, String endNum ) throws Exception {
		return ktotoDAO.getKtotoBoardReplyList(boardType, boardNo, startNum, endNum);
	}
	public void insertReply ( String boardNo, String boardType, String content ) throws Exception {
		ktotoDAO.insertReply(boardType, boardNo, content);
	}
	public void updateReply ( String boardNo, String boardType, String pStep, String content ) throws Exception {
		ktotoDAO.updateReply(boardType, boardNo, pStep, content);
	}
	public void removeReply ( String boardNo, String boardType, String pStep ) throws Exception {
		ktotoDAO.removeReply(boardType, boardNo, pStep);
	}
	
}
