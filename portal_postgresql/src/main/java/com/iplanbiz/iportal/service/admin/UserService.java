package com.iplanbiz.iportal.service.admin;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletRequest;
 
import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.email.EmailService;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.admin.UserDAO;
import com.iplanbiz.iportal.dto.KtotoUser;
import com.iplanbiz.iportal.dto.User;

@Service
public class UserService {
	
	@Autowired
	UserDAO userDAO;
	public String getAcctIDByAcctName(String acctName) throws Exception{
		return userDAO.getAcctIDByAcctName(acctName);
	}
	public User getUserInfo(String empID, String acctID) throws Exception{
		return userDAO.getUserInfo(empID, acctID);
	}
	public void setLoginFailInfo(User user, HttpServletRequest request) throws Exception{
		userDAO.setLoginFailInfo(user, request);
	}
	public boolean isExistUser(User user) throws Exception {
		return (userDAO.isExistUser(user) > 0) ? true : false;
	}
	public HashMap<String, Object> getUserStatusAndFailCount(User user) throws Exception{
		return userDAO.getUserStatusAndFailCount(user);
	}
	public List<LinkedHashMap<String, Object>> getCustomAuthQuery(String query) throws Exception{
		return userDAO.getCustomAuthQuery(query);
	} 
	public void resetUserStatus(User user) throws Exception{
		userDAO.resetUserStatus(user);
	}
	public Map<String, Object> getAdminUserInfo(String userID) throws Exception{
		return userDAO.getAdminUserInfo(userID);
	}
	public void setLoginFailCountAdd(User user) throws Exception{
		userDAO.setLoginFailCountAdd(user);
	}
	public JSONArray getAdminUserList(String searchName, String value) throws Exception{
		return userDAO.getAdminUserList(searchName, value);
	}
	public JSONArray getDetailUserInfo(String userID,String remoteIP,String remoteOS,String remoteBrowser) throws Exception{
		return userDAO.getDetailUserInfo(userID,remoteIP,remoteOS,remoteBrowser);
	}
	public int userSave(String unID, String userID, String userName, String userPassword, String userPosition, String userEmail, String adminYn, String locale, String checkValue, String remoteIP, String remoteOS, String remoteBrowser, String orgPassword) throws Exception{
		int result = 0;
		result = userDAO.inserUser(unID, userID, userName, userPassword, userPosition, userEmail, adminYn, locale, checkValue, remoteIP, remoteOS, remoteBrowser, orgPassword);
		return result;
	}
	
	public int userDelete(String unID, String userID, String remoteIP, String remoteOS, String remoteBrowser) throws Exception{
		int result = 0;
		result = userDAO.deleteUser(userID, unID, remoteIP, remoteOS, remoteBrowser);
		return result;
	}
	public int resetPassworedUser(String userID) throws Exception{
		return userDAO.resetPassworedUser(userID);
	}
	public JSONArray excelDownByUserBackData(String userID) throws Exception{
		return userDAO.excelDownByUserBackData(userID);
	}
	public int getCheckUserID ( String userID ) throws Exception {
		return userDAO.getCheckUserID(userID);
	}
	public int getCheckOlapID ( String olapID ) throws Exception {
		return userDAO.getCheckOlapID(olapID);
	}
	public int adminExpenses(String unId, String gubn, String remoteIp, String remoteOs, String remoteBrowser){
		return userDAO.adminExpenses(unId, gubn, remoteIp, remoteOs, remoteBrowser);
	}
	public int updateLock(String empId){
		return userDAO.updateLock(empId);
	}
	public String getSystemName() {
		return userDAO.getSystemValue("system_name");
	}
	public String getSystemCopyRight() {
		return userDAO.getSystemValue("copyright");
	}
	
}
