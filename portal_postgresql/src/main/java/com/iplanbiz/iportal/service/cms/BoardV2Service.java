package com.iplanbiz.iportal.service.cms;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplanbiz.iportal.dao.cms.BoardV2DAO;
import com.iplanbiz.iportal.dto.BoardV2;

@Service
public class BoardV2Service {

	@Autowired 
	BoardV2DAO boardDAO;
	
	public JSONArray getBoardList ( String boardType, String startDat, String endDat, String titleName) throws Exception {
		return boardDAO.getBoardList(boardType, startDat, endDat, titleName);
	}
	public JSONObject getBoardDetail ( String boardType, String boardNO ) throws Exception {
		return boardDAO.getBoardDetail(boardType, boardNO);
	}
	public JSONArray getBoardFileList ( String tableNM , String boardNO ) throws Exception {
		return boardDAO.getBoardFileList(tableNM, boardNO);
	}
	public void insertBoard ( BoardV2 board ) throws Exception {
		boardDAO.insertBoard(board);
	}
	public void updateBoard ( BoardV2 board ) throws Exception {
		boardDAO.updateBoard(board);
	}
	public void removeBoard ( String boardType, String boardNO ) throws Exception {
		boardDAO.removeBoard(boardType, boardNO);
	}
	public JSONObject getBoardUserName () throws Exception {
		return boardDAO.getBoardV2UserName();
	}
	public void setBoardHitCount ( String boardNo, String boardType ) throws Exception {
		boardDAO.setBoardV2HitCount(boardNo, boardType);
	}
	public JSONArray getBoardReplyList ( String boardType, String boardNo, String startNum, String endNum ) throws Exception {
		return boardDAO.getBoardV2ReplyList(boardType, boardNo, startNum, endNum);
	}
	public void insertReply ( String boardNo, String boardType, String content ) throws Exception {
		boardDAO.insertReply(boardType, boardNo, content);
	}
	public void updateReply ( String boardNo, String boardType, String pStep, String content ) throws Exception {
		boardDAO.updateReply(boardType, boardNo, pStep, content);
	}
	public void removeReply ( String boardNo, String boardType, String pStep ) throws Exception {
		boardDAO.removeReply(boardType, boardNo, pStep);
	}
	
}
