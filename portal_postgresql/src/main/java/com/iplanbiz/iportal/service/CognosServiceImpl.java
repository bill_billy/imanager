package com.iplanbiz.iportal.service;

import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.cognos.developer.schemas.bibus._3.Account;
import com.cognos.developer.schemas.bibus._3.BaseClass;
import com.cognos.developer.schemas.bibus._3.LanguageProp;
import com.cognos.developer.schemas.bibus._3.Locale;
import com.cognos.developer.schemas.bibus._3.MultilingualToken;
import com.cognos.developer.schemas.bibus._3.MultilingualTokenProp;
import com.cognos.developer.schemas.bibus._3.PropEnum;
import com.cognos.developer.schemas.bibus._3.QueryOptions;
import com.cognos.developer.schemas.bibus._3.Role;
import com.cognos.developer.schemas.bibus._3.SearchPathMultipleObject;
import com.cognos.developer.schemas.bibus._3.Sort;
import com.cognos.developer.schemas.bibus._3.UpdateOptions;
import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.comm.cognos.CRNConnect;
import com.iplanbiz.iportal.comm.cognos.GroupsAndRoles;
import com.iplanbiz.iportal.comm.cognos.Logon;
import com.iplanbiz.iportal.comm.util.CognosInfo;
import com.iplanbiz.iportal.comm.util.CognosUtil;
import com.iplanbiz.iportal.comm.util.GroupsAndRolesUtil;
import com.iplanbiz.iportal.comm.util.PermissionUtil;
import com.iplanbiz.iportal.dto.User;

/**
 * @author : yun hee Kim(yhkim@khantech.co.kr)
 * @date   : 2012. 7. 6.
 * @desc   : 
 */

@Service("cognosService")
public class CognosServiceImpl implements CognosService{
	
	/**
	 * 
	 */
	public CognosServiceImpl() {
		super();
	}

	private Logger logger = LoggerFactory.getLogger(getClass());
//	private CRNConnect connect;
	private static Logon sessionLogon;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	
	// return value
	// 100 성공
	// -1 코그너스 연결 안됨
	// -2 패스워드 틀림
	public int logon(User user){
		CRNConnect connection = new CRNConnect();
		
		//cognos와 연결 되는지 확인
		if(connection.connectToCognosServer(CognosInfo.getDispatch()) == null){
			logger.info("Cognos Login: connection Fail!!");
			return -1;
		}else logger.info("Cognos Login: connection Success!");
		loginSessionInfoFactory.getObject().setExternalInfo("connect", connection);
//		this.setConnect(connection);
		
		//login info
		String namespace = CognosInfo.getNamespace();
		String userId = user.getUserID(); 
		logger.info("Cognos Login: userId={}", Utils.getString(userId, "").replaceAll("[\r\n]",""));
		String password = user.getPasswd(); 
		logger.info("Cognos Login: password={}", Utils.getString(password, "").replaceAll("[\r\n]",""));
		boolean loginResult = false;
		
		logger.debug("Cognos Login: connection={}", Utils.getString(connection.toString(), "").replaceAll("[\r\n]",""));
		logger.debug("Cognos Login: namespace={}", Utils.getString(namespace, "").replaceAll("[\r\n]",""));
		
		logger.info("Cognos Login: Logon Begin");
		
		if(!"Cognos".equals(namespace)){
			try{
				logger.info("Cognos Login: Logon Try Start");
				sessionLogon = new Logon();
				logger.info("Cognos Login: Logon Try End");
			}catch(Exception ex){
				logger.error("Cognos Login: Logon Error!!!");
				return -2;
			}
					
			loginResult = sessionLogon.logon(connection, namespace, userId, password);
			
			logger.info("Cognos Login: Logon End");
		}else{
			//TODO : anonymousLogin 인한 추가
//			if (Logon.doTestForAnonymous(connection)) {
				anonymousLogin(connection);
				return 100;
//			}
		}
		// end
		
		if(!Logon.loggedIn(connection))
		{
			logger.info("Cognos Login: login Fail!!");
			return -2;
		}else logger.info("Cognos Login: login Success!");
		

		
		if(loginResult){
			Account myAccount = Logon.getLogonAccount(connection);
			
			String userName = myAccount.getDefaultName().getValue();
			String passport = CognosUtil.getPassport(connection);
			Boolean isAdmin = CognosUtil.isRole(connection, (String) myAccount.getSearchPath().getValue(), "CAMID(\"::System Administrators\")");
			String CAMID = myAccount.getSearchPath().getValue();
			
			loginSessionInfoFactory.getObject().setLogin(loginResult);
			logger.info("===>>>>Session loginResult: {}", loginResult);
			loginSessionInfoFactory.getObject().setUserId(userId.trim());
			loginSessionInfoFactory.getObject().setUserName(userName);
			loginSessionInfoFactory.getObject().setPassport(passport);
			loginSessionInfoFactory.getObject().setIsAdmin(isAdmin);
			loginSessionInfoFactory.getObject().setCAMID(CAMID);
			
			logger.info("===>>>>Session Create: {}", "loginSessionInfo");
			
			//다국어 설정
			PropEnum[] prop = { PropEnum.searchPath, PropEnum.productLocale, PropEnum.contentLocale }; 
            BaseClass[] account;
			try {
				account = connection.getCMService(passport).query(new SearchPathMultipleObject("~"), prop, new Sort[] {}, new QueryOptions());
             
	            if(account != null && account.length > 0) { 
	                Account a = (Account)account[0]; 
	                                 
	                LanguageProp lProp = new LanguageProp();  
	                 
	                lProp.setValue(user.getUserLocale()); 
	                a.setProductLocale(lProp); 
	
	                lProp = new LanguageProp();  
	                lProp.setValue(user.getUserLocale()); 
	                a.setContentLocale(lProp);
	 
	                connection.getCMService(passport).update(account, new UpdateOptions()); 
	            }
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
            
		}else{
//			System.out.println("Login Failed. Please try again.");
			return -2;
		}
		return 100;
	}
	
	//TODO : anonymousLogin 인한 추가 
	private void anonymousLogin(CRNConnect connection) {
		Account myAccount = Logon.getLogonAccount(connection);
		String userId = "admin";
		String userName = "관리자";
//		String passport = CognosUtil.getPassport(connection);
		String passport = "anonymous";
//		Boolean isAdmin = CognosUtil.isRole(connection, (String) myAccount.getSearchPath().getValue(), "CAMID(\"::System Administrators\")");
		Boolean isAdmin = true;
//		String CAMID = myAccount.getSearchPath().getValue();
		String CAMID = "";		
		loginSessionInfoFactory.getObject().setLogin(true);
		logger.info("===>>>>Session loginResult: {}", true);
		loginSessionInfoFactory.getObject().setUserId(userId);
		loginSessionInfoFactory.getObject().setUserName(userName);
		loginSessionInfoFactory.getObject().setPassport(passport);
		loginSessionInfoFactory.getObject().setIsAdmin(isAdmin);
		loginSessionInfoFactory.getObject().setCAMID(CAMID);
		
		logger.info("===>>>>Session Create: {}", "loginSessionInfo");	
	}
	//end
	
	public void logout() {
		CRNConnect connect = (CRNConnect) loginSessionInfoFactory.getObject().getExternalInfo("connect");
		sessionLogon.logoff(connect);
		return;
	}
	
	/*
	 * 권한 초기화
	 * target : Folder or Report .. storeId
	 */
	
	public boolean delPermissionAll(String target){
		CRNConnect connection = (CRNConnect) loginSessionInfoFactory.getObject().getExternalInfo("connect");
		BaseClass[] results = null; 
		BaseClass targetBC = null;
		String cmSearchPath = "storeID(\"" + target + "\")";
		
		try {
			results = connection.getCMService(CognosUtil.getPassport(connection)).query(new SearchPathMultipleObject(cmSearchPath),
					new PropEnum[] { PropEnum.searchPath, PropEnum.policies }, new Sort[] {}, new QueryOptions());
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
		if(results.length > 0){
			targetBC = results[0];
		}else{
			return false;
		}
		PermissionUtil.delPermissionAll(connection, targetBC);
		return true;
	}

	/*
	 * 권한 부여
	 * user id (String)
	 * group id (Number)
	 * role name (String)
	 *  
	 * 
	 * Flag1 : u (User), g (Group), r (Role)
	 * Flag2 : NameSpace [auth, cognos ] -- source 소속
	 * Flag3 : permissionType :: normal, admin, del
	 */
// bonnie	
	public boolean setPermission(String target, List<Map<String, Object>> permissionStrList){
		CRNConnect connection = (CRNConnect) loginSessionInfoFactory.getObject().getExternalInfo("connect");
		String cmSearchPath = "storeID(\"" + target + "\")";
//		String sourceName = GroupsAndRolesUtil.makeSecurityPath(source, namespace, sourceType);
//		namespace = GroupsAndRolesUtil.makeSecurityNamespacePath(namespace);
		
//		Permission replacePermission[] = PermissionUtil.getPermission(permissionStr);
		BaseClass[] results = null; 
		BaseClass targetBC = null;
		
		try {
			results = connection.getCMService(CognosUtil.getPassport(connection)).query(new SearchPathMultipleObject(cmSearchPath),
					new PropEnum[] { PropEnum.searchPath, PropEnum.policies }, new Sort[] {}, new QueryOptions());
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
		logger.info("result.length:{}",results.length);
		
		if(results.length > 0){
			targetBC = results[0];			
		}else{
			return false;
		}
		
		return PermissionUtil.addPermission(connection, targetBC, permissionStrList);
 
	}
	 
	public boolean addRole(String roleName){
		CRNConnect connection = (CRNConnect) loginSessionInfoFactory.getObject().getExternalInfo("connect");
		BaseClass role = new Role();
		String path = "CAMID(\":\")"; // role은 cognos에 생성
		
		//get cognos Locale
		MultilingualToken[] names = new MultilingualToken[1];
		names[0] = new MultilingualToken();
		Locale[] locales = CognosUtil.getConfiguration(connection);
		names[0].setLocale(locales[0].getLocale());
		names[0].setValue(roleName);
		role.setName(new MultilingualTokenProp());
		role.getName().setValue(names);
		
		//add the new role
		try {
			CognosUtil.addObjectToCS(connection, role, path);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	public boolean deleteRole(String roleName){
		CRNConnect connection = (CRNConnect) loginSessionInfoFactory.getObject().getExternalInfo("connect");
		String rolePath = GroupsAndRolesUtil.makeSecurityPath(roleName, "cognos", "r");
		
		GroupsAndRoles gar = new GroupsAndRoles();
		try {
			gar.deleteRole(connection, rolePath);
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
		
	}
	public boolean toRole(String id, String role, String type, String ac){
		CRNConnect connection = (CRNConnect) loginSessionInfoFactory.getObject().getExternalInfo("connect");
		String objectPath = GroupsAndRolesUtil.makeSecurityPath(id, "auth", type);
		String rolePath = GroupsAndRolesUtil.makeSecurityPath(role, "cognos", "r");
		
		logger.debug("objectPath :: {}, rolePath :: {}",Utils.getString(objectPath, "").replaceAll("[\r\n]",""), Utils.getString(rolePath, "").replaceAll("[\r\n]",""));
		
		try {
			BaseClass user[] = connection.getCMService(CognosUtil.getPassport(connection)).query(new SearchPathMultipleObject(objectPath),
					new PropEnum[] { PropEnum.searchPath, PropEnum.defaultName }, new Sort[] {}, new QueryOptions());
			
			if(user.length == 0){
				return false;
			}
			GroupsAndRoles gar = new GroupsAndRoles();
			
			if(ac.equals("add")){
				gar.addToRole(connection, rolePath, user);
			}else if(ac.equals("del")){
				gar.removeFromRole(connection, rolePath, user[0]);
			}else if(ac.equals("delAll")){
				gar.removeAllFromRole(connection, rolePath);
			}else{
				return false;
			}
			
		} catch (RemoteException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	public boolean portalAddToRole(String userId, String role, String type){
		return toRole(userId, role, type, "add");
	}
	
	public boolean delUserFromRole(String userId, String role){
		return toRole(userId, role, "u", "del");
	}
	public boolean delGroupFromRole(String groupId, String role){
		return toRole(groupId, role, "g", "del");
	}
	
	public boolean delAllDataFromRole(String role){
		return toRole("", role, "", "delAll");
	}

}
