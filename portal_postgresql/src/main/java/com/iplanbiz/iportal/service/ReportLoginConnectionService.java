package com.iplanbiz.iportal.service;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.ReportLoginConnectionDAO;

@Service
@Transactional(readOnly=true)
public class ReportLoginConnectionService {

	@Autowired ReportLoginConnectionDAO reportLoginDAO;
	
	public JSONArray getReportLoginConnectionYyyymm() throws Exception{
		return reportLoginDAO.getReportLoginConnectionYyyymm();
	}
	public JSONArray getReportLoginConnectionList ( String yyyymm ) throws Exception {
		return reportLoginDAO.getReportLoginConnectionList(yyyymm);
	}
	public JSONArray getReportLoginConnectionByExcel ( String yyyymm ) throws Exception {
		return reportLoginDAO.getReportLoginConnectionByExcel(yyyymm);
	}

}
