package com.iplanbiz.iportal.service.portlet;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.portlet.ContentsManagerDAO;
import com.iplanbiz.iportal.dto.contentsManager;

@Service
public class ContentsManagerService {

	
	@Autowired ContentsManagerDAO contentsManagerDAO;

	public JSONArray getPortletContentsList ( String portletGrp ) throws Exception {
		 return contentsManagerDAO.getPortletContentsList(portletGrp);
	}
	public JSONArray getPortletGrpList () throws Exception {
		return contentsManagerDAO.getPortletGrpList();
	}
	public JSONArray getPortletContentsListKeyList ( String poID ) throws Exception {
		return contentsManagerDAO.getPortletContentsListKeyList(poID);
	}
	public JSONArray getPortletContentsDetail ( String poID ) throws Exception {
		return contentsManagerDAO.getPortletContentsDetail(poID);
	}
	public JSONArray getPortletContentsInfoData ( String poID ) throws Exception {
		return contentsManagerDAO.getPortletContentsInfoData(poID);
	}
	public int insert ( contentsManager contents) throws Exception {
		return contentsManagerDAO.insert(contents);
	}
	public int deleteByContents ( contentsManager contents ) throws Exception {
		return contentsManagerDAO.deleteByContents(contents);
	}
}
