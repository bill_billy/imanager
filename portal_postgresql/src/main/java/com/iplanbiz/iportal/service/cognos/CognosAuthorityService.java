package com.iplanbiz.iportal.service.cognos;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.comm.cognos.CRNConnect;
import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.cognos.CognosAuthorityDAO;

@Service
public class CognosAuthorityService {

	@Autowired
	CognosAuthorityDAO cognosDAO;
	
	public JSONArray getMenuListByCognos ( String userID ) throws Exception {
		return cognosDAO.getMenuListByCognos(userID);
	}
	public JSONArray getCognosChildListByArray ( String storeID, String defaultRootSearchPath, CRNConnect conn, String ac ) throws Exception {
		return cognosDAO.getCognosChildListByArray(storeID, defaultRootSearchPath, conn, ac);
	}
	public String getSearchPathUseStoreID ( String storeID ) throws Exception {
		return cognosDAO.getSearchPathUseStoreID(storeID);
	}
	public JSONObject getReportPath ( String searchPath ) throws Exception {
		return cognosDAO.getReportPath(searchPath);
	}
	public List<Map<String, Object>> getMenuByJSON ( String storeID, String menuName ) throws Exception {
		return cognosDAO.getMenuByJSON(storeID, menuName);
	}
	public JSONArray getDataMenuPermissionByCognos ( String cID ) throws Exception {
		return cognosDAO.getDataMenuPermissionByCognos(cID);
	}
	public JSONArray getUserDataByCognos ( String cID ) throws Exception {
		return cognosDAO.getUserDataByCognos(cID);
	}
	public JSONArray getGroupDataByCognos ( String cID ) throws Exception {
		return cognosDAO.getGroupDataByCognos(cID);
	}
	public JSONArray getRoleDataByCognos ( String cID ) throws Exception {
		return cognosDAO.getRoleDataByCognos(cID);
	}
	public boolean insert ( String cID, String userPermission, String ac) throws Exception {
		return cognosDAO.insert(cID, userPermission, ac);
	}
}
