package com.iplanbiz.iportal.service;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.UserLoginInfoDAO;

@Service
@Transactional(readOnly=true)
public class UserLoginInfoService {

	@Autowired UserLoginInfoDAO userLoginInfoDAO;
	
	public JSONArray getUserLoinInfoGubn () throws Exception {
		return userLoginInfoDAO.getUserLoinInfoGubn();
	}
	public JSONArray getUserLoginInfoList ( String yyyymm, String gubn ) throws Exception {
		return userLoginInfoDAO.getUserLoginInfoList(yyyymm, gubn);
	}
	public JSONArray getUserLoginInfoListByExcel ( String yyyymm, String gubn ) throws Exception {
		return userLoginInfoDAO.getUserLoginInfoListByExcel(yyyymm, gubn);
	}

}
