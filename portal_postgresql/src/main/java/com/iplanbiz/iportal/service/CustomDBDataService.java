package com.iplanbiz.iportal.service;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.CustomDBDataDAO;

@Service
public class CustomDBDataService {

	@Autowired
	CustomDBDataDAO customDAO;
	
	public JSONArray getCognosLoginEmpTyp ( String empID ) throws Exception {
		return customDAO.getCognosLoginEmpTyp(empID);
	}
 
	public JSONObject getCognosAccountEmpID ( String empID, String rownum ) throws Exception {
		return customDAO.getCognosAccountEmpID(empID, rownum);
	}
	public JSONObject getConosAccountEmpEmail ( String empID, String rownum ) throws Exception {
		return customDAO.getConosAccountEmpEmail(empID, rownum);
	}
	public JSONObject getCognosAccountUserGrpID ( String empID, String rownum ) throws Exception {
		return customDAO.getCognosAccountUserGrpID(empID, rownum);
	}
	public JSONObject getCognosAccountRoleID ( String empID, String rownum ) throws Exception {
		return customDAO.getCognosAccountRoleID(empID, rownum);
	}
	public JSONObject getCognosAccountLocale ( String empID, String rownum ) throws Exception {
		return customDAO.getCognosAccountLocale(empID, rownum);
	}
	public JSONArray getCognosMemberShip ( String empID ) throws Exception {
		return customDAO.getCognosMemberShip(empID);
	}
	public JSONArray getCognosQueryMember ( String userGrpID , String sortOrder) throws Exception {
		return customDAO.getCognosQueryMember(userGrpID, sortOrder);
	}
	public JSONArray getCognosQueryMemberByRole ( String gID, String sortOrder ) throws Exception {
		return customDAO.getCognosQueryMemberByRole(gID, sortOrder);
	}
	public JSONArray getCognosQuery ( String sortOrder, String sqlWhere ) throws Exception {
		return customDAO.getCognosQuery(sortOrder, sqlWhere);
	}
	public JSONArray getCrudTest () throws Exception {
		return customDAO.getCrudTest(); 
	}
	public int insertCrudTest ( String comCod, String comName ) throws Exception {
		return customDAO.insertCrudTest(comCod, comName);
	}
	public int deleteCrudTest ( String comCod ) throws Exception {
		return customDAO.deleteCrudTest(comCod);
	}

}
