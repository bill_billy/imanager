package com.iplanbiz.iportal.service;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.DayConnectionDAO;

@Service
@Transactional(readOnly=true)
public class DayConnectionService {

	@Autowired
	DayConnectionDAO dayDAO;
	
	public JSONArray getDayConnectionUserList(String startDat, String endDat) throws Exception{
		return dayDAO.getDayConnectionUserList(startDat, endDat);
	}
	public JSONArray getDayConnectionReportList(String startDat, String endDat, String userID) throws Exception {
		return dayDAO.getDayConnectionReportList(startDat, endDat, userID);
	}
}
