package com.iplanbiz.iportal.service.system;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
 
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
 
import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.file.FileService;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.system.PortalSettingDAO;
import com.iplanbiz.iportal.dao.system.SystemDAO;
import com.iplanbiz.iportal.dto.File;
import com.iplanbiz.iportal.dto.IECT7002;

@Service
public class PortalSettingService{ 
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	PortalSettingDAO portalDAO;
	
	public int savePortalSettingFile(IECT7002 iect7002){

		// 저장 위치(기존 webConfig 값을 동일하게 사용)
//		String upLoadPath = WebConfig.getDefaultSaveDir()+"../../";
		String upLoadPath = WebConfig.getDefaultServerRoot();
		System.out.println(upLoadPath);
		String[] fileSavePathArr = new String[6];
		
		fileSavePathArr[1] = "/resources/img/n/";
	//	fileSavePathArr[2] = "/resource/img/top/";
	//	fileSavePathArr[3] = "/resource/img/login/";
	//	fileSavePathArr[4] = "/resource/img/dTree/";
	//	fileSavePathArr[5] = "/resource/img/cognos/";
		try {
			if(iect7002.getFile().length > 0){
				for(int i = 0; i < iect7002.getFile().length; i++){
					if(iect7002.getFile()[i].getSize() > 0){
						String fileName = iect7002.getFile_name()[i].replace("..", "");
						fileSave(iect7002.getFile()[i], upLoadPath+fileSavePathArr[Integer.parseInt(iect7002.getFile_src()[i])], fileName);
					}
				}	
			}
		}catch(Exception e) {
			return 0;
		}
		return 1;
//		for(int i = 0; i < keyName.length; i++){
//			if(keyName[i].equals("main_logo_img")){
//				img = iect7002.getMain_logo_img();
//				upLoadImgPath = upLoadPath+iect7002.getMain_logo_img_src();
//				imgName = iect7002.getMain_logo_img_name();
//				fileSave(img, upLoadImgPath, imgName);
//			}else if(keyName[i].equals("main_title_img")){
//				img = iect7002.getMain_title_img();
//				upLoadImgPath = upLoadPath+iect7002.getMain_title_img_src();
//				imgName = iect7002.getMain_title_img_name();
//				fileSave(img, upLoadImgPath, imgName);
//			}
//		}
	}
	
	public void fileSave(CommonsMultipartFile file,String upLoadPath, String fileName){
		System.out.println(upLoadPath);
		FileService fileService = new FileService(WebConfig.getProperty("system.upload.SaveDir"),Long.valueOf(WebConfig.getProperty("system.upload.maxUploadSize").toString()));
//		String extType[] = {}; // upload 가능한 파일 타입을 지정해줄 수 있음 -- 사용하지 않음.. 사용하려면 FileUtil 수정 필요
		fileService.setUpLoadPath(upLoadPath);
		fileService.writeFile(file, fileName);	// 결과값으로 에러 코드 넘어옴...
	}
	
	public File saveFile(CommonsMultipartFile file, String[] extType) throws IOException
	{
		String fileSaveName = "";
		int result = 0;
		
		FileService fileService = new FileService(WebConfig.getProperty("system.upload.SaveDir"),Long.valueOf(WebConfig.getProperty("system.upload.maxUploadSize").toString()));
		fileSaveName = fileService.setFileName();
		result = fileService.saveFile(file, extType, fileSaveName);
		
		File returnFile = new File();
		
		if(result == 0){
			returnFile.setFileOrgName(file.getOriginalFilename());
			returnFile.setFileSaveName(fileSaveName);
			returnFile.setFilePath(fileService.getUpLoadPath());
			returnFile.setFileSize(Utils.getString(String.valueOf(file.getSize()), ""));
			returnFile.setFileExt(fileService.getFileExt());
		}
		
		return returnFile;
	}
	
	
}
