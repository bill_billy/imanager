package com.iplanbiz.iportal.service.excelPgm;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplanbiz.iportal.dao.excelPgm.ExcelPgmDAO;


@Service
public class ExcelPgmService {

	@Autowired
	ExcelPgmDAO excelPgmDAO;
	
	public JSONArray getCommonCode(String comlcd) throws Exception {
		return excelPgmDAO.getCommonCode(comlcd);
	}

	public JSONArray getExcelPgmDQCategory(String category, String pCd, String cCd, String useYn, String searchText) throws Exception {
		JSONArray list = null;
		if(category.equals("P")) { //대분류
			list= excelPgmDAO.getExcelPgmDQCategoryParent(category,pCd,cCd,useYn,searchText);
		} else if(category.equals("C")) { //중분류
			list= excelPgmDAO.getExcelPgmDQCategoryChild(category,pCd,cCd,useYn,searchText);
		}
	
		return list;
	}

	public void insertExcelPgmCategoryParent(String pCd, String pNm, String useYn, String sortOrder) {
		excelPgmDAO.insertExcelPgmCategoryParent(pCd, pNm, useYn, sortOrder);
		
	}

	public void deleteExcelPgmCategoryParent(String pCd) {
		excelPgmDAO.deleteExcelPgmCategoryParent(pCd);
		
	}

	public void updateExcelPgmCategoryParent(String pCd, String pNm, String useYn, String sortOrder) {
		excelPgmDAO.updateExcelPgmCategoryParent(pCd, pNm, useYn, sortOrder);
		
	}

	public void insertExcelPgmCategoryChild(String pCd, String cCd, String cNm, String useYn, String sortOrder) {
		excelPgmDAO.insertExcelPgmCategoryChild(pCd, cCd, cNm, useYn, sortOrder);
		
	}

	public void updateExcelPgmCategoryChild(String pCd, String cCd, String cNm, String useYn, String sortOrder) {
		excelPgmDAO.updateExcelPgmCategoryChild(pCd, cCd, cNm, useYn, sortOrder);
		
	}

	public void deleteExcelPgmCategoryChild(String pCd, String cCd) {
		excelPgmDAO.deleteExcelPgmCategoryChild(pCd,cCd);
		
	}

}
