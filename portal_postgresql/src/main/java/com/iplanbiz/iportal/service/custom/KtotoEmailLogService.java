package com.iplanbiz.iportal.service.custom;

import java.util.LinkedHashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplanbiz.iportal.dao.custom.KtotoEmailLogDAO;

@Service
public class KtotoEmailLogService {
	
	@Autowired
	KtotoEmailLogDAO kotoEmailLogDAO;
	
	public JSONArray getEmailLogList(String startDat,String endDat) throws Exception{
		return kotoEmailLogDAO.getEmailLogList(startDat,endDat);
	}
	public List<LinkedHashMap<String, Object>> getEmailLogListForExcel(String startDat,String endDat){
		return kotoEmailLogDAO.getEmailLogListForExcel(startDat,endDat);
	}
}
