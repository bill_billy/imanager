package com.iplanbiz.iportal.service.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
 
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dao.system.SystemDAO;

@Service
public class SystemService{ 
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	SystemDAO systemDAO;
	
	private Map<String,Object> configMap = null;
	
	public Map<String, Object> getSystemConfig() throws Exception{
		//평상시 설정 정보는 Memory에 있는 정보를 불러옴..
		//시스템>환경설정 화면에서 수정해야지만 flag가 true로 해서 들어감.
		return this.getSystemConfig(false);
	}
	
	public Map<String, Object> getSystemConfig(Boolean force) throws Exception{
		if(force==false) {
			if(configMap!=null) return configMap;
		}
		List<Map<String, Object>> hashCode = systemDAO.getSystemConfig();
		Map<String, Object> mapTmp = new HashMap<String, Object>();
		
		for(int i=0;i<hashCode.size();i++){
			mapTmp.put(hashCode.get(i).get("key_name").toString(), hashCode.get(i).get("key_value").toString());
		}
		
//		mapTmp.put("root_domain", Utils.getString(loginSessionInfoFactory.getObject().getRootDomain(), mapTmp.get("root_domain")));
		configMap = mapTmp;
		
		return mapTmp;
		 
	}
	
	public void setSystem(String keyValues, String keyNames) throws Exception{
		String[] keyValue = keyValues.split(",", -1);
		String[] keyName = keyNames.split(",", -1);
//		Map<String, String> map = CodeManager.QUERY_MAP();
		Map<String, String> map = new HashMap<String, String>();
		for(int i = 0; i < keyValue.length; i++){
			System.out.println("key Name = "+keyName[i]);
			map.put("KEY_VALUE", keyValue[i]);
			map.put("KEY_NAME", keyName[i]);
			map.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
			
			systemDAO.setSystem(map);
		}
		
	}
	
	
	
}
