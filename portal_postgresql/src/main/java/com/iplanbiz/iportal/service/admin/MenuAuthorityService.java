package com.iplanbiz.iportal.service.admin;

import java.util.LinkedHashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.mortbay.util.ajax.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.admin.MenuAuthorityDAO;

@Service
public class MenuAuthorityService {

	@Autowired 
	MenuAuthorityDAO menuAuthorityDAO;
	
	
	public JSONArray getMenuAuthorityTreeList(String type, String fkey) throws Exception{
		return menuAuthorityDAO.getMenuAuthorityTreeList(type, fkey);
	}
	public JSONArray getMenuAuthorityTreeListAdmin ( String type, String fkey ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityTreeListAdmin(type, fkey);
	}
	public JSONArray getMenuAuthorityList(String fkey, String type) throws Exception{
		return menuAuthorityDAO.getMenuAuthorityList(fkey, type);
	}
	public JSONArray getMenuAuthorityListAdmin ( String fkey, String type ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityListAdmin(fkey, type);
	}
	public JSONArray getGroupListNotInRole ( String roleID, String userGrpName, String grpTyp ) throws Exception {
		return menuAuthorityDAO.getGroupListNotInRole(roleID, userGrpName, grpTyp);
	}
	public JSONArray getMenuAuthorityGroupNoListAdmin ( String gID, String userGrpName, String grpTyp ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityGroupNoListAdmin(gID, userGrpName, grpTyp);
	}
	public JSONArray getMenuAuthorityGroupList(String gID, String userGrpName, String grpTyp) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityGroupList(gID, userGrpName, grpTyp);
	}
	public JSONArray getMenuAuthorityGroupListAdmin ( String gID, String userGrpName, String grpTyp ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityGroupList(gID, userGrpName, grpTyp);
	}
	
	//메뉴권한 관리 > Role Tabs > 전체 사용자
	public JSONArray getMenuAuthorityUserListNoByRole(String gID, String empID, String empName, String isNull) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityUserListNoByRole(gID, empID, empName, isNull);
	}
	public JSONArray getMenuAuthorityUserListNoByRoleAdmin ( String gID, String empID, String empName, String isNull ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityUserListNoByRoleAdmin(gID, empID, empName, isNull);
	}
	//메뉴권한 관리 > Role Tabs > 권한이 부여된 사용자
	public JSONArray getMenuAuthorityUserListByRole(String gID, String empID, String empName, String isNull) throws Exception{
		return menuAuthorityDAO.getMenuAuthorityUserListByRole(gID, empID, empName, isNull);
	}
	public JSONArray getMenuAuthorityUserListByRoleAdmin(String gID, String empID, String empName, String isNull) throws Exception{
		return menuAuthorityDAO.getMenuAuthorityUserListByRoleAdmin(gID, empID, empName, isNull);
	}
	public int insertRoleInfo(String roleID, String roleName, String roleNameOrg, String remoteIP, String remoteOS, String remoteBrowser) throws Exception{
		return menuAuthorityDAO.insertRoleInfo(roleID, roleName, roleNameOrg,remoteIP,remoteOS,remoteBrowser);
	}
	public int insertRoleInfoAdmin ( String roleID, String roleName ) throws Exception {
		return menuAuthorityDAO.insertRoleInfoAdmin(roleID, roleName);
	}
	public int deleteRoleInfo(String roleID, String roleName, String remoteIP, String remoteOS, String remoteBrowser) throws Exception{
		return menuAuthorityDAO.deleteRoleInfo(roleID, roleName,remoteIP,remoteOS,remoteBrowser);
	}
	public int deleteRoleInfoAdmin ( String roleID ) throws Exception {
		return menuAuthorityDAO.deleteRoleInfoAdmin(roleID);
	}
	public int insertRoleMapping(String roleID, String roleName, String[] menuCheckValue, String[] userCheckValue, String[] groupCheckValue, String[] menuCheckName, String[] userCheckName, String[] groupCheckName, String remoteIP, String remoteOS, String remoteBrowser) throws Exception{
		return menuAuthorityDAO.insertRoleMapping(roleID,roleName, menuCheckValue, userCheckValue, groupCheckValue,menuCheckName, userCheckName, groupCheckName,remoteIP,remoteOS,remoteBrowser);
	}
	public int insertRoleMappingAdmin(String roleID, String[] menuCheckValue, String[] userCheckValue, String[] groupCheckValue) throws Exception {
		return menuAuthorityDAO.insertRoleMappingAdmin(roleID, menuCheckValue, userCheckValue, groupCheckValue);
	}
	public JSONArray getGroupList(String userGrpName,  String grpTyp) throws Exception{
		return menuAuthorityDAO.getGroupList(userGrpName, grpTyp);
	}
	public JSONArray getGroupListAdmin(String userGrpName, String grpTyp) throws Exception {
		return menuAuthorityDAO.getGroupListAdmin(userGrpName, grpTyp);
	}
	public JSONArray getUsersInGroup(String pID, String empID, String empName) throws Exception{
		return menuAuthorityDAO.getUsersInGroup(pID, empID, empName);
	}
	public JSONArray getUsersInGroupAdmin(String pID, String empID, String empName) throws Exception{
		return menuAuthorityDAO.getUsersInGroupAdmin(pID, empID, empName);
	}
	public JSONArray getRoleListNoByGroup ( String fkey, String groupName ) throws Exception{
		return menuAuthorityDAO.getRoleListNoByGroup(fkey, groupName);
	}
	public JSONArray getRoleListNoByGroupAdmin ( String fkey, String groupName ) throws Exception {
		return menuAuthorityDAO.getRoleListNoByGroupAdmin(fkey, groupName);
	}
	public JSONArray getRoleListByGroup(String fkey, String groupName) throws Exception{
		return menuAuthorityDAO.getRoleListByGroup(fkey, groupName);
	}
	public JSONArray getRoleListByGroupAdmin ( String fkey, String groupName ) throws Exception {
		return menuAuthorityDAO.getRoleListByGroupAdmin(fkey, groupName);
	}
	public int insertGroupMapping(String groupID, String groupName, String[] menuCheckValue, String[] roleCheckValue, String[] menuCheckName, String[] roleCheckName, String remoteIP, String remoteOS, String remoteBrowser) throws Exception{
		return menuAuthorityDAO.insertGroupMapping(groupID, groupName, menuCheckValue, roleCheckValue, menuCheckName, roleCheckName,remoteIP,remoteOS,remoteBrowser);
	}
	public int insertGroupMappingAdmin ( String groupID, String[] menuCheckValue, String[] roleCheckValue ) throws Exception {
		return menuAuthorityDAO.insertGroupMappingAdmin(groupID, menuCheckValue, roleCheckValue);
	}
	public JSONArray getMenuAuthorityUserListNoByMenu ( String cID, String empID, String empNm, String gubn ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityUserListNoByMenu(cID, empID, empNm, gubn);
	}
	public JSONArray getMenuAuthorityUserListNoByMenuAdmin ( String cID, String empID, String empNm, String gubn ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityUserListNoByMenuAdmin(cID, empID, empNm, gubn);
	}
	public JSONArray getUsersInMenu(String cID, String empID, String empNm)  throws Exception{
		return menuAuthorityDAO.getUsersInMenu(cID, empID, empNm);
	}
	public JSONArray getUsersInMenuAdmin(String cID, String empID, String empNm)  throws Exception{
		return menuAuthorityDAO.getUsersInMenuAdmin(cID, empID, empNm);
	}
	public JSONArray getMenuAuthorityGroupListNoSelectByMenu (String cID, String userGrpNm) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityGroupListNoSelectByMenu(cID, userGrpNm);
	}
	public JSONArray getMenuAuthorityGroupListNoSelectByMenuAdmin ( String cID, String userGrpNm ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityGroupListNoSelectByMenuAdmin(cID, userGrpNm);
	}
	public JSONArray getMenuAuthorityGroupListByMenu(String cID, String userGrpNm) throws Exception{
		return menuAuthorityDAO.getMenuAuthorityGroupListByMenu(cID, userGrpNm);
	}
	public JSONArray getMenuAuthorityGroupListByMenuAdmin ( String cID, String userGrpNm ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityGroupListByMenuAdmin(cID, userGrpNm);
	}
	public JSONArray getMenuAuthorityRoleListNoSelectByMenu ( String cID, String groupName ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityRoleListNoSelectByMenu(cID, groupName);
	}
	public JSONArray getMenuAuthorityRoleListNoSelectByMenuAdmin ( String cID, String groupName ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityRoleListNoSelectByMenuAdmin(cID, groupName);
	}
	public JSONArray getMenuAuthorityRoleListByMenu(String cID, String groupName) throws Exception{
		return menuAuthorityDAO.getMenuAuthorityRoleListByMenu(cID, groupName);
	}
	public JSONArray getMenuAuthorityRoleListByMenuAdmin ( String cID, String groupName ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityRoleListByMenuAdmin(cID, groupName);
	}
	public int insertMenuMapping(String menuID, String menuName, String[] userCheckValue, String[] groupCheckValue, String[] roleCheckValue, String[] userCheckName, String[] groupCheckName, String[] roleCheckName, String remoteIP, String remoteOS, String remoteBrowser) throws Exception{
		return menuAuthorityDAO.insertMenuMapping(menuID, menuName, userCheckValue, groupCheckValue, roleCheckValue,userCheckName, groupCheckName, roleCheckName,remoteIP,remoteOS,remoteBrowser);
	}
	public int insertMenuMappingAdmin ( String menuID, String[] userCheckValue, String[] groupCheckValue, String[] roleCheckValue ) throws Exception {
		return menuAuthorityDAO.insertMenuMappingAdmin(menuID, userCheckValue, groupCheckValue, roleCheckValue);
	}
	public JSONArray getBusinessList ( String businessName ) throws Exception {
		return menuAuthorityDAO.getBusinessList(businessName);
	}
	public JSONArray getBusinessListAdmin ( String businessName ) throws Exception {
		return menuAuthorityDAO.getBusinessListAdmin(businessName);
	}
	public JSONArray getMenuAuthorityUserListNoByData ( String bID, String empID, String empName, String gubn ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityUserListNoByData(bID, empID, empName, gubn);
	}
	public JSONArray getMenuAuthorityUserListNoByDataAdmin ( String bID, String empID, String empName, String gubn ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityUserListNoByDataAdmin(bID, empID, empName, gubn);
	}
	public JSONArray getMenuAuthorityUserListByData ( String bID, String empID, String empName, String gubn ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityUserListByData(bID, empID, empName, gubn);
	}
	public JSONArray getMenuAuthorityUserListByDataAdmin ( String bID, String empID, String empName, String gubn) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityUserListByDataAdmin(bID, empID, empName, gubn);
	}
	public JSONArray getMenuAuthorityGroupListNoByData ( String bID, String grpTyp, String userGrpName ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityGroupListNoByData(bID, grpTyp, userGrpName);
	}
	public JSONArray getMenuAuthorityGroupListByData ( String bID, String grpTyp, String userGrpName ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityGroupListByData(bID, grpTyp, userGrpName);
	}
	public JSONArray getMenuAuthorityGroupListByDataAdmin ( String bID, String grpTyp, String userGrpName ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityGroupListByDataAdmin(bID, grpTyp, userGrpName );
	}
	public JSONArray getMenuAuthorityRoleListNoByData ( String bID, String groupName ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityRoleListNoByData(bID, groupName);
	}
	public JSONArray getMenuAuthorityRoleListNoByDataAdmin ( String bID, String groupName ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityRoleListNoByDataAdmin(bID, groupName);
	}
	public JSONArray getMenuAuthorityRoleListByData ( String bID, String groupName ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityRoleListByData(bID, groupName);
	}
	public JSONArray getMenuAuthorityRoleListByDataAdmin ( String bID, String groupName ) throws Exception {
		return menuAuthorityDAO.getMenuAuthorityRoleListByDataAdmin(bID, groupName);
	}
	public int insertDataInfo ( String bID, String businessName, String businessNameOrg, String remoteIP, String remoteOS, String remoteBrowser ) throws Exception {
		return menuAuthorityDAO.insertDataInfo(bID, businessName, businessNameOrg,remoteIP,remoteOS,remoteBrowser);
	}
	public int insertDataInfoAdmin ( String bID, String businessName ) throws Exception {
		return menuAuthorityDAO.insertDataInfoAdmin(bID, businessName);
	}
	public int deleteDataInfo ( String bID, String businessName, String remoteIP, String remoteOS, String remoteBrowser ) throws Exception {
		return menuAuthorityDAO.deleteDataInfo(bID,businessName,remoteIP,remoteOS,remoteBrowser);
	}
 
	public int insertDataMapping ( String bID, String dataName, String[] userCheckValue, String[] groupCheckValue, String[] roleCheckValue, String[] userCheckName, String[] groupCheckName, String[] roleCheckName, String remoteIP, String remoteOS, String remoteBrowser) throws Exception {
		return menuAuthorityDAO.insertDataMapping(bID, dataName, userCheckValue, groupCheckValue, roleCheckValue, userCheckName, groupCheckName, roleCheckName,remoteIP,remoteOS,remoteBrowser);
	}
	public int insertDataMappingAdmin ( String bID, String[] userCheckValue, String[] groupCheckValue, String[] roleCheckValue ) throws Exception {
		return menuAuthorityDAO.insertDataMappingAdmin(bID,userCheckValue,groupCheckValue,roleCheckValue);
	}
	public List<LinkedHashMap<String,Object>> getGrpTypList() {
		// TODO Auto-generated method stub
		return menuAuthorityDAO.getGrpTypList();
	}
}
