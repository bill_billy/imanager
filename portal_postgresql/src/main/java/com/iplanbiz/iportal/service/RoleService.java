package com.iplanbiz.iportal.service;

import java.util.LinkedHashMap;
import java.util.List;
 
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.iportal.dao.home.HomeDAO;
import com.iplanbiz.iportal.dao.RoleDAO;

@Service
@Transactional(readOnly=true)
public class RoleService { 
	
	@Autowired
	RoleDAO roleDAO;

	public JSONArray getRoleList(String groupName) throws Exception{
		return roleDAO.getRoleList(groupName);
	}
	public JSONArray getRoleListAdmin ( String groupName ) throws Exception {
		return roleDAO.getRoleListAdmin(groupName);
	}
	public JSONArray getRoleCheckUser(String userID) throws Exception{
		return roleDAO.getRoleCheckUser(userID);
	}
	
}
