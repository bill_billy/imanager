package com.iplanbiz.iportal.service.dashboard;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.DashboardKpiManagerDAO;
import com.iplanbiz.iportal.dto.DashboardKpiManager;

@Service
public class DashboardKpiManagerService {
	
	@Autowired
	DashboardKpiManagerDAO dashboardKpiManagerDAO;
	
	public JSONArray getDashboardKpiList(String kpiGbn) throws Exception{
		return dashboardKpiManagerDAO.getDashboardKpiList(kpiGbn);
	}
	public JSONArray getDashboardKpiDetail(String kpiID) throws Exception{
		return dashboardKpiManagerDAO.getDashboardKpiDetail(kpiID);
	}
	public JSONArray getDashboardCheckMm(String kpiID) throws Exception{
		return dashboardKpiManagerDAO.getDashboardCheckMm(kpiID);
	}
	public int insertDashBoardKpiManager(DashboardKpiManager dash) throws Exception{
		return dashboardKpiManagerDAO.insertDashBoardKpiManager(dash);
	}
	public int removeDashBoardKpiManager(DashboardKpiManager dash) throws Exception{
		return dashboardKpiManagerDAO.removeDashBoardKpiManager(dash);
	}
}
