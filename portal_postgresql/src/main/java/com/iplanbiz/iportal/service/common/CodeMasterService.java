package com.iplanbiz.iportal.service.common;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplanbiz.iportal.dao.common.CodeMasterDAO;

@Service
public class CodeMasterService {
	@Autowired
	CodeMasterDAO codeMasterDAO;
	
	public JSONArray getGroupCodeList(String searchText) throws Exception  {
		return codeMasterDAO.getGroupCodeList(searchText);
	}

	public JSONArray getCodeList(String gCd) throws Exception {
		return codeMasterDAO.getCodeList(gCd);
	}

	public int insertGroupCode(String gCd, String gCdNm,String ac) {
		return codeMasterDAO.insertGroupCode(gCd,gCdNm,ac);
	}

	public int insertCommonCode(String gCd, String cCd, String cCdNm, String useYn, int sortSeq, String cDesc,String ac) {
		return codeMasterDAO.insertCommonCode(gCd,cCd,cCdNm,useYn,sortSeq,cDesc,ac);
	}

	public int deleteGroupCode(String gCd) {
		return codeMasterDAO.deleteGroupCode(gCd);
	}
	
	public int deleteCommonCode(String gCd, String cCd) {
		return codeMasterDAO.deleteCommonCode(gCd, cCd);
	}

}
