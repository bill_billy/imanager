package com.iplanbiz.iportal.service.admin;

import org.json.simple.JSONArray;
import org.mortbay.util.ajax.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.admin.DataCenterDAO;

@Service
public class DataCenterService {
	@Autowired DataCenterDAO dataCenterDAO;
	
	public JSONArray getInfoDataList(String searchComment, String searchTableName, String yyyy) throws Exception {
		return dataCenterDAO.getInfoDataList(searchComment, searchTableName, yyyy);
	}
	public JSONArray getDataInfoTableList(String searchOwner, String searchTableName, String searchComment) throws Exception {
		return dataCenterDAO.getDataInfoTableList(searchOwner, searchTableName, searchComment);
	}
	public JSONArray getInfoDataDetail(String idx) throws Exception{
		return dataCenterDAO.getInfoDataDetail(idx);
	}
	public int insertInfoData(String idx, String userID, String tableName, String tableComment, String dtStartDat, String dtEndDat, String upStartDat, String upEndDat, String bigo) throws Exception{
		return dataCenterDAO.insertInfoData(idx, userID, tableName, tableComment, dtStartDat, dtEndDat, upStartDat, upEndDat, bigo);
	}
	public int removeInfoData(String idx) throws Exception{
		return dataCenterDAO.removeInfoData(idx);
	}
}
