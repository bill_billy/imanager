package com.iplanbiz.iportal.service.cms;

import java.io.IOException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.iportal.dao.cms.BoardDAO;
import com.iplanbiz.iportal.dao.cms.NoticeDAO;
import com.iplanbiz.iportal.dto.Board;
import com.iplanbiz.iportal.dto.Notice;

@Service
public class NoticeService {

	@Autowired
	NoticeDAO noticeDAO;

	
	public JSONArray getNoticeList(String TITLE, String CONTENT, String NAME, String GUBN,
			String START_DAT, String END_DAT) throws Exception {
		return noticeDAO.getNoticeList(TITLE, CONTENT, NAME, GUBN, START_DAT, END_DAT);
	}
	public List<HashMap<String, Object>> getNoticeList(Notice notice)  {
		return noticeDAO.getNoticeList(notice);
	}
	public List<HashMap<String, Object>> getPopup(String start_dat,String end_dat)  {
		return noticeDAO.getPopup(start_dat,end_dat);
	}
	public JSONArray getPopupList(String start_dat,String end_dat) throws Exception  {
		return noticeDAO.getPopupList(start_dat,end_dat);
	}
	public List<HashMap<String, Object>> getInformList(Notice notice)  {
		return noticeDAO.getInformList(notice);
	}
	public Map<String, String> getNewNotice(){
		return noticeDAO.getNewNotice();
	}
	public void createNotice(Notice notice) throws IOException {
		noticeDAO.createNotice(notice);
	}
	public void modifyHitCount(Notice notice){
		noticeDAO.modifyHitCount(notice);
	}
	public List<HashMap<String, String>> getNoticeNew(Notice notice){
		return noticeDAO.getNoticeNew(notice);
	}
	public List<HashMap<String, String>> getNoticeBuser(Notice notice) {
		return noticeDAO.getNoticeBuser(notice);
	}
	public void modifyNotice(Notice notice) throws IOException {
		noticeDAO.modifyNotice(notice);
	}
	public void removeNotice(Notice notice) {
		noticeDAO.removeNotice(notice);          
	}
	
}
