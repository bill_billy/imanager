package com.iplanbiz.iportal.service.portlet;

import java.util.LinkedHashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.admin.MenuAuthorityDAO;
import com.iplanbiz.iportal.dao.portlet.PortletAuthorityManagerDAO;

@Service
public class PortletAuthorityManagerService {

	@Autowired PortletAuthorityManagerDAO portletAuthoirtyManagerDAO;
	@Autowired MenuAuthorityDAO menuAuthorityDAO;
	
	public JSONArray getPortletAuthorityList () throws Exception {
		return portletAuthoirtyManagerDAO.getPortletAuthorityList();
	}
	public JSONArray getPortletPermissionUserList( String plID, String empName, String empID ) throws Exception {
		return portletAuthoirtyManagerDAO.getPortletPermissionUserList(plID, empName, empID);
	}
	public JSONArray getPortletPermissionGroupList ( String plID, String grpTyp, String groupName ) throws Exception {
		return portletAuthoirtyManagerDAO.getPortletPermissionGroupList(plID, grpTyp, groupName);
	}
	public JSONArray getPortletPermissionRoleList ( String plID ) throws Exception {
		return portletAuthoirtyManagerDAO.getPortletPermissionRoleList(plID);
	}
	public int insertPortletPermission ( String plID, String[] arrayUserData, String[] arrayGroupData, String[] arrayRoleData ) throws Exception {
		return portletAuthoirtyManagerDAO.insertPortletPermission(plID, arrayUserData, arrayGroupData, arrayRoleData);
	}
	public List<LinkedHashMap<String, Object>> getGrpTypList() {
		// TODO Auto-generated method stub
		return menuAuthorityDAO.getGrpTypList();
	}

}
