package com.iplanbiz.iportal.service.dashboard;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.DashboardUnivManagerDAO;

@Service
public class DashboardUnivManagerService {

	@Autowired DashboardUnivManagerDAO univManagerDAO;
	
	public JSONArray getDashboardUnivList(String scID)  throws Exception {
		return univManagerDAO.getDashboardUnivList(scID);
	}
	public JSONObject getDashboardUnivScGubn() throws Exception{
		return univManagerDAO.getDashboardUnivScGubn();
	}
	public JSONArray getDashboardPopupUnivList(String univName) throws Exception{
		return univManagerDAO.getDashboardPopupUnivList(univName);
	}
	public int insertDashboardUniv(String scID, String univID, String univGrp1) throws Exception {
		return univManagerDAO.insertDashboardUniv(scID, univID, univGrp1);
	}
	public int removeDashboardUniv(String scID, String[] univID) throws Exception {
		return univManagerDAO.removeDashboardUniv(scID, univID);
	}
}
