package com.iplanbiz.iportal.service;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.MonthUserConnectionDAO;

@Service
public class MonthUserConnectionService {

	@Autowired MonthUserConnectionDAO monthUserDAO;
	
	public JSONArray getMonthUserYearList() throws Exception{
		return monthUserDAO.getMonthUserYearList();
	}
	public JSONArray getMonthUserConnectionTotalList ( String year ) throws Exception {
		return monthUserDAO.getMonthUserConnectionTotalList(year);
	}
	public JSONArray getMonthUserConnectionListByUser ( String year ) throws Exception {
		return monthUserDAO.getMonthUserConnectionListByUser(year);
	}
	public JSONArray getMonthUserConnectionPopupList ( String year , String userID) throws Exception {
		return monthUserDAO.getMonthUserConnectionPopupList(year, userID);
	}
	public JSONArray getMonthUserConnectionListByExcel (String year) throws Exception {
		return monthUserDAO.getMonthUserConnectionListByExcel(year);
	}
	public JSONArray getMonthUserConnectionPopupListByExcel ( String year, String userID ) throws Exception {
		return monthUserDAO.getMonthUserConnectionPopupListByExcel(year, userID);
	}
}
