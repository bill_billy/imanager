package com.iplanbiz.iportal.service.system;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.io.dbms.TableCondition.Condition;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.auth.EmpView;
import com.iplanbiz.iportal.dao.auth.GroupView;
import com.iplanbiz.iportal.dao.auth.RoleView;
import com.iplanbiz.iportal.dao.system.SqlHouseExecuteDAO; 

@Service
@Transactional
public class SessionService {
 
	
	@Autowired
	private DBMSDAO dbmsDao; 
	
	
	@Autowired
	private DBMSService dbmsService;
	
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	 
	public void addSession(HttpSession session) throws Exception {
		LoginSessionInfo info = (LoginSessionInfo)session.getAttribute("loginSessionInfo");
		TableObject sessionTbl = new TableObject();
		sessionTbl.put("SESSIONID", session.getId());
		sessionTbl.put("USERID", info.getUserId());
		sessionTbl.put("ACCT_ID", info.getAcctID());
		sessionTbl.put("IPADDR", info.getIpAddress());
		sessionTbl.put("STATE", LoginSessionInfo.State.NORMAL.toString());
		sessionTbl.putCurrentTimeStamp("CREATIONTIME");
		sessionTbl.putCurrentTimeStamp("STATEUPDATETIME");
		
		dbmsDao.insertTable(WebConfig.getCustomTableName("MST_SESSION"), sessionTbl);
		
		
	}
	
	public void setDuplicationSession(HttpSession sessionDTO) throws RemoteException, Exception {
		LoginSessionInfo info = (LoginSessionInfo)sessionDTO.getAttribute("loginSessionInfo");
		if(info!=null&&info.getUserId()!=null) {
			String query = "UPDATE " + WebConfig.getCustomTableName("MST_SESSION") + " SET STATE='"+LoginSessionInfo.State.DUPLICATION.toString()+"' WHERE STATE='"+LoginSessionInfo.State.NORMAL.toString()+"'";
			query+=" AND USERID = '"+info.getUserId().replaceAll("'","")+"' and ACCT_ID = '"+info.getAcctID()+"' and DROPTIME IS NULL and ipaddr !='"+info.getIpAddress()+"'";
			System.out.println("set Duplication Session query : " + query);
			dbmsService.executeUpdate(query);		
		}
	}
	public int countDuplicationSession(HttpSession sessionDTO) throws RemoteException, Exception {
		LoginSessionInfo info = (LoginSessionInfo)sessionDTO.getAttribute("loginSessionInfo");
		TableCondition session = new TableCondition(); 
		session.put("USERID", info.getUserId());
		session.put("ACCT_ID", info.getAcctID());
		//Duplicate는 이미 중복 처리되어 접속 불능 상태이고
		//IP가 다른 세션 중에 Normal인걸 체크해야한다.
		session.put("STATE", LoginSessionInfo.State.NORMAL.toString());
		
		session.put("IPADDR", info.getIpAddress(),Condition.NotEqual);
		session.putIsNull("DROPTIME");
		
		int result =  dbmsDao.countTable(WebConfig.getCustomTableName("MST_SESSION"), session);
		
		//if중복 접속 불허일 경우
		this.setDuplicationSession(sessionDTO);
		
		return result;
	}
 
	public void dropSession(HttpSession sessionDTO) throws RemoteException, Exception {
		
		String query = "UPDATE " + WebConfig.getCustomTableName("MST_SESSION") + " SET STATE='"+LoginSessionInfo.State.DESTROY.toString()+"', DROPTIME = now() WHERE ";
		query+=" SESSIONID = '"+sessionDTO.getId()+"' and DROPTIME IS NULL";
		
		dbmsService.executeUpdate(query);		
	}
	public String getState(String sessionID) throws Exception {
		TableCondition session = new TableCondition();
		session.put("SESSIONID", sessionID);  
		List<LinkedHashMap<String,Object>> list = dbmsDao.selectTable(false,WebConfig.getCustomTableName("MST_SESSION"), session);
		if(list.size()==0) return LoginSessionInfo.State.NORMAL.toString();
		else return list.get(0).get("STATE").toString();
	}
	
	
}
