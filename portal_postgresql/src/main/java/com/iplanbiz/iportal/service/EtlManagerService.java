package com.iplanbiz.iportal.service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.EtlManagerDAO;
import com.iplanbiz.iportal.dto.Etl;

@Service
public class EtlManagerService {

	@Autowired EtlManagerDAO etlManagerDAO;
	
	public JSONArray getEtlManagerGbnList () throws Exception {
		return etlManagerDAO.getEtlManagerGbnList();
	}
	public JSONArray getEtlManagerList ( String gbn, String searchKey, String searchValue, String etlYn ) throws Exception {
		return etlManagerDAO.getEtlManagerList(gbn, searchKey, searchValue, etlYn);
	}
	public JSONArray getEtlManagerDetail ( String etlID ) throws Exception {
		return etlManagerDAO.getEtlManagerDetail(etlID);
	}
	public JSONArray getEtlManagerDBInfoList () throws Exception {
		return etlManagerDAO.getEtlManagerDBInfoList();
	}
	public int insertEtlManager ( Etl etl ) throws Exception {
		return etlManagerDAO.insertEtlManager(etl);
	}
	public int removeEtlManager ( Etl etl ) throws Exception {
		return etlManagerDAO.removeEtlManager(etl);
	}
	public AjaxMessage callEtl ( String etlID ) throws Exception {
		return etlManagerDAO.callEtl(etlID);
	}
	public List<HashMap<String,String>> excelDownByEtl(String gbn, String searchKey, String searchValue, String etlYn) throws Exception {
		return etlManagerDAO.excelDownByEtl(gbn, searchKey, searchValue, etlYn);
	}
}
