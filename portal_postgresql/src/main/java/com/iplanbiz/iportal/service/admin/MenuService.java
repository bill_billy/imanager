package com.iplanbiz.iportal.service.admin;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.print.attribute.HashAttributeSet;
import javax.servlet.http.HttpServletRequest;
 
import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dao.home.HomeDAO;
import com.iplanbiz.iportal.dao.admin.MenuDAO;
import com.iplanbiz.iportal.dto.Menu;

@Service
public class MenuService { 
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	
	@Autowired
	MenuDAO menuDAO;
	
	public JSONArray getRootCID() throws Exception{
		return menuDAO.getRootCID();
	}
	public JSONArray getTopMenuList(String pID) throws Exception{
		return menuDAO.getTopMenuList(pID);
	}
	public JSONArray getMenuList(String pID) throws Exception{
		return menuDAO.getMenuList(pID);
	}
	public JSONArray getTreeGridMenuList() throws Exception{
		return menuDAO.getTreeGridMenuList();
	}
	public JSONArray getMenuDomain() throws Exception {
		return menuDAO.getMenuDomain();
	}
	public JSONArray getMenuInfoDetail(String cID) throws Exception{
		return menuDAO.getMenuInfoDetail(cID);
	}
	public JSONArray getPopupMenuTreeData(String searchValue) throws Exception{
		return menuDAO.getPopupMenuTreeData(searchValue);
	}
	public int insertMenuInfo(Menu menu) throws Exception{
		return menuDAO.insertMenuInfo(menu);
	}
	public int insertMenuInfoArray(Menu menu) throws Exception{
		return menuDAO.insertMenuInfoArray(menu);
	}
	public int deleteMenuInfo(Menu menu) throws Exception{
		return menuDAO.deleteMenuInfo(menu);
	}
	public String getCognosReportURL ( String storeID, boolean isView, boolean isIE8) throws Exception {
		return menuDAO.getCognosReportURL(storeID, isView, isIE8);
	}
	public JSONArray getBookList () throws Exception {
		return menuDAO.getBookList();
	}
	public String getNavi( String cID ) throws Exception {
		return menuDAO.getNavi(cID);
	}
	public int insertMyfolderNoCog ( String cName ) throws Exception {
		return menuDAO.insertMyfolderNoCog(cName);
	}
	public int insertBookReport ( String cID, String menuName, String menuType, String pID) throws Exception {
		return menuDAO.insertBookReport(cID, menuName, menuType, pID);
	}
	public int deleteBookUserData ( String cID ) throws Exception {
		return menuDAO.deleteBookUserData(cID);
	}
	public JSONArray getMenuBiSolutionPathDeepScan ( String cID, String cName, String cLink, String ac ) throws Exception {
		return menuDAO.getMenuBiSolutionPathDeepScan(cID, cName, cLink, ac);
	}
	public String getAuditStoreID ( HttpServletRequest request ) throws Exception {
		return menuDAO.getAuditStoreID(request);
	}
	public String getCogReportUrl ( String storeID, boolean isView, boolean isIE8 ) throws Exception {
		return menuDAO.getCogReportUrl(storeID, isView, isIE8);
	}
	public String getCogSearchPath ( String storeID ) throws Exception {
		return menuDAO.getCogSearchPath(storeID);
	}
	public String getReportPath ( String searchPath ) throws Exception {
		return menuDAO.getReportPath(searchPath);
	}
	public int renameMyfolderNoCog(String cId, String cName) throws Exception {
		return menuDAO.renameMyfolderNoCog(cId, cName);
	}
	public int moveMyfolderNoCog(String cId, String pId) throws Exception {
		return menuDAO.moveMyfolderNoCog(cId, pId);
	}
	public int sortMyfolderNoCog(String cId, String targetId, String position, String sortOrder) throws Exception{
		return menuDAO.sortMyfolderNoCog(cId, targetId, position,sortOrder);
	}
	public int moveMenuPosition(String cId, String pId) throws Exception{
		return menuDAO.moveMenuPosition(cId, pId);
	}
	public int sortMenuPosition(String cId, String targetId, String position, String sortOrder) throws Exception{
		return menuDAO.sortMenuPosition(cId, targetId, position,sortOrder);
	}
}
