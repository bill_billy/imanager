package com.iplanbiz.iportal.service;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.MonthReportConnectionDAO;

@Service
@Transactional(readOnly=true)
public class MonthReportConnectionService {

	@Autowired
	MonthReportConnectionDAO monthReportDAO;
	
	public JSONArray getMonthReportYear() throws Exception{
		return monthReportDAO.getMonthReportYear();
	}
	public JSONArray getMonthReportConnectionTotal( String year ) throws Exception{
		return monthReportDAO.getMonthReportConnectionTotal(year);
	}
	public JSONArray getMonthReportConnectionList( String year ) throws Exception{
		return monthReportDAO.getMonthReportConnectionList(year);
	}
	public JSONArray getMonthReportConnectionListByExcel( String year) throws Exception {
		return monthReportDAO.getMonthReportConnectionListByExcel(year);
	}
	public JSONArray getMonthReportConnectionListByExcelAll(String year) throws Exception{
		return monthReportDAO.getMonthReportConnectionListByExcelAll(year);
	}
	public JSONArray getMonthReportConnectionDetail( String year ,String targetID) throws Exception{
		return monthReportDAO.getMonthReportConnectionDetail(year, targetID);
	}

}
