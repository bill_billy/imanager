package com.iplanbiz.iportal.service.dashboard;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
 
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
 
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.DashboardSlideManagerDAO;
import com.iplanbiz.iportal.dto.DashboardSlideManager;

@Service
public class DashBoardSlideManagerService {

	@Autowired DashboardSlideManagerDAO dashboardDAO;
	
	public JSONArray getDashboardSlideManagerList () throws Exception {
		return dashboardDAO.getDashboardSlideManagerList();
	}
	
	/*public int insertDashboardSlide (String dash_grp_id, String path_idx, String sortord, 
            						 String interval, String eft_type, String apply_month) throws Exception {
		
		return dashboardDAO.insertDashboardSlide(dash_grp_id, path_idx, sortord, interval, eft_type, apply_month);
	}*/
	
	public int insertDashboardSlide (DashboardSlideManager slide) throws Exception {

		return dashboardDAO.insertDashboardSlide(slide);
	}
	
	public JSONArray getDashboardSlidePathList () throws Exception {
		return dashboardDAO.getDashboardSlidePathList();
	}
	public JSONArray getDashboardSlidePopupData (String dash_grp_id) throws Exception {
		return dashboardDAO.getDashboardSlidePopupData(dash_grp_id);
	}
	public List<HashMap<String, Object>> getSlideMaster() {
		// TODO Auto-generated method stub
		return dashboardDAO.getSlideMaster();
	}
	public int insertDashInfo(String txtDashGrpName, String txtDashGrpID) throws Exception{
		// TODO Auto-generated method stub
		return dashboardDAO.insertDashInfo(txtDashGrpName, txtDashGrpID);
	}
	public int deleteDashInfo(String txtDashGrpName, String txtDashGrpID) throws Exception{
		
		return dashboardDAO.deleteDashInfo(txtDashGrpName, txtDashGrpID);
	}

	public List<HashMap<String, Object>> getRowSelectView(String dash_grp_id) throws Exception{
		// TODO Auto-generated method stub
		return dashboardDAO.getRowSelectView(dash_grp_id);
	}

	public List<HashMap<String, String>> getDashGrpList(String dashGrp, String searchDashGrpName) throws Exception{
		// TODO Auto-generated method stub
		return dashboardDAO.getDashGrpList(dashGrp, searchDashGrpName);
	}

	public int deleteSlideMaster(String DASH_GRP_ID) throws Exception{
		
		return dashboardDAO.deleteSlideMaster(DASH_GRP_ID);
	}

	public int insertSlideMaster(String dash_grp_id, String slideIDX, String pathIDX, String interval, String eftType,
			String applyMonthValue, String sortOrd) {
		// TODO Auto-generated method stub
		return dashboardDAO.insertSlideMaster( dash_grp_id,  slideIDX,  pathIDX,  interval,  eftType,
				applyMonthValue,  sortOrd);
	}

	public List<HashMap<String, Object>> dashIdForm(String dash_grp_id) {
		// TODO Auto-generated method stub
		return dashboardDAO.dashIdForm(dash_grp_id);
	}
	

}
