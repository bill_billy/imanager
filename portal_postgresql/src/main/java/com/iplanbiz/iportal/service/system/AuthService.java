package com.iplanbiz.iportal.service.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.auth.EmpView;
import com.iplanbiz.iportal.dao.auth.GroupView;
import com.iplanbiz.iportal.dao.auth.RoleView;
import com.iplanbiz.iportal.dao.system.SqlHouseExecuteDAO;

@Service
@Transactional
public class AuthService {

	@Autowired
	private DBMSService dbmsService; 
	
	@Autowired
	private DBMSDAO dbmsDao;
	@Autowired
	private SqlHouseExecuteDAO sqlhouseExecuteDao;
	
	@Autowired
	private EmpView empView;
	
	@Autowired
	private GroupView groupView;
	
	@Autowired
	private RoleView roleView;
	
	
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	
	public List<LinkedHashMap<String, Object>> getEmpViewListByName(String name) throws Exception{ 
		return empView.getListByUserName(name);
	}
	public List<LinkedHashMap<String, Object>> getEmpViewList() throws Exception{ 
		return empView.getList();
	}
	public List<LinkedHashMap<String, Object>> getGroupViewList() throws Exception{
		return groupView.getList();
	}
	public JSONArray getRoleViewList() throws Exception{
		return roleView.getList();
	}
	
	public Boolean myDataPermission(String dataName, String userID, String acctID) throws Exception{
		TableCondition iect7037 = new TableCondition();
		iect7037.put("ACCT_ID", acctID);
		iect7037.put("BUSINESS_NAME", dataName);		
		List<LinkedHashMap<String,Object>> list = dbmsDao.selectTable(false,WebConfig.getCustomTableName("IECT7037"), iect7037);
		ArrayList<String> bidList = new ArrayList<String>();
		ArrayList<String> groupList = new ArrayList<String>();
		ArrayList<String> roleList = new ArrayList<String>();
		for(int i = 0; i < list.size();i++) {
			String bid = list.get(i).get("BID").toString();
			bidList.add(bid);
		}
		
		
		TableCondition iect7038 = new TableCondition();
		iect7038.put("ACCT_ID", acctID);
		iect7038.put("BID", bidList,TableCondition.Condition.In);
		
		
		iect7038.put("TYPE", "g");
		list = this.getRoleViewListByUserID(userID,acctID);
		for(int i = 0; i < list.size();i++) {
			String roleID = list.get(i).get("GID").toString();
			roleList.add(roleID);
		}
		iect7038.put("FKEY",roleList,TableCondition.Condition.In );
		int roleCnt = dbmsDao.countTable(WebConfig.getCustomTableName("IECT7038"), iect7038);
		
		if(roleCnt>0) return true;
		
		iect7038.put("TYPE", "p");
		list = this.getGroupViewListByUserID(userID,acctID);
		for(int i = 0; i < list.size();i++) {
			String grpID = list.get(i).get("USER_GRP_ID").toString();
			groupList.add(grpID);
		}
		iect7038.put("FKEY",groupList,TableCondition.Condition.In );
		int groupCnt = dbmsDao.countTable(WebConfig.getCustomTableName("IECT7038"), iect7038);
		
		if(groupCnt>0) return true;
		
		iect7038.put("TYPE", "u");
		iect7038.put("FKEY", userID);
		int userCnt = dbmsDao.countTable(WebConfig.getCustomTableName("IECT7038"), iect7038);
		
		if(userCnt>0) return true; 
		
		return false;
	}
	
	
	public List<LinkedHashMap<String, Object>> getEmpViewListByUserID(String userID) throws Exception{ 
		return empView.getListByUserID(userID);
	}
	public List<LinkedHashMap<String, Object>> getEmpViewListByUserID(String userID,String acctID) throws Exception{ 
		return empView.getListByUserID(userID);
	}
	public List<LinkedHashMap<String, Object>> getGroupViewListByUserID(String userID) throws Exception{
		return groupView.getListByUserID(userID);
	}
	public List<LinkedHashMap<String, Object>> getGroupViewListByUserID(String userID,String acctID) throws Exception{
		return groupView.getListByUserID(userID,acctID);
	}
	public List<LinkedHashMap<String, Object>> getRoleViewListByUserID(String userID) throws Exception{
		return roleView.getListByUserID(userID);
	}
	public List<LinkedHashMap<String, Object>> getRoleViewListByUserID(String userID,String acctID) throws Exception{
		return roleView.getListByUserID(userID,acctID);
	}
	public JSONArray isSaveAbleDocument(String cID, String pID) throws Exception {
		String userID = loginSessionInfoFactory.getObject().getUserId();
		String acctID = loginSessionInfoFactory.getObject().getAcctID();
		
		StringBuilder groups = new StringBuilder();
		List<LinkedHashMap<String,Object>> list = this.getGroupViewListByUserID(userID);
		for(int i = 0; i < list.size();i++) {
			groups.append(list.get(i).get("USER_GRP_ID").toString());
			if(i!=list.size()-1)
				groups.append(",");
		}
		HashMap<String,Object> param = new HashMap<String,Object>();
		param.put("S_P_ID", pID);
		param.put("S_C_ID", cID);
		param.put("S_ACCT_ID", acctID);
		param.put("S_USER_ID", userID);
		param.put("S_USER_GRP_ID", groups);
		
		return sqlhouseExecuteDao.selectJSONArray(34, param); 
	}
	
	
	
}
