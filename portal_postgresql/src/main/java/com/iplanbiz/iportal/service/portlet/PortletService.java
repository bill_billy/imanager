package com.iplanbiz.iportal.service.portlet;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;
 
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dao.home.HomeDAO;
import com.iplanbiz.iportal.dao.portlet.PortletDAO;

@Service
public class PortletService{ 
	
	@Autowired
	PortletDAO portletDAO;
	
	public List<HashMap<String, Object>> getUserPortletInfo() throws Exception{
		return portletDAO.getUserPortletInfo();
	}
	public void defaultUserPortlet() throws Exception{
		portletDAO.setDefaultUserPortlet();
	}
	public int getUserPortletLayoutNo() throws Exception{
		return portletDAO.getUserPortletLayoutNo();
	}
	public JSONArray getPageletByUserID () throws Exception {
		return portletDAO.getPageletByUserID();
	}
	public JSONArray getPageletPortletInfo ( String plID ) throws Exception {
		return portletDAO.getPageletPortletInfo(plID);
	}
	public JSONArray getPagetletPortletInfoByUser ( String plID ) throws Exception {
		return portletDAO.getPagetletPortletInfoByUser(plID);
	}
	public JSONArray getPortletInfoData ( String poID, String gx ) throws Exception {
		return portletDAO.getPortletInfoData(poID, gx);
	}
	public JSONArray getPageletPortlet ( String poID, String plID) throws Exception {
		return portletDAO.getPageletPortlet(poID, plID);
	}
	public JSONArray getPageletPortletByUser ( String poID, String plID ) throws Exception {
		return portletDAO.getPageletPortletByUser(poID, plID);
	}
	public JSONObject getPageletPortletDefault () throws Exception {
		return portletDAO.getPageletPortletDefault();
	}
	
}
