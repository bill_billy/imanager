package com.iplanbiz.iportal.service.admin;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplanbiz.iportal.dao.admin.MenuContentManagerDAO;

@Service
public class MenuContentManagerService {

	@Autowired 
	MenuContentManagerDAO menuContentManagerDAO;
	
	
	public JSONArray getSolutionListCbo() throws Exception{
		return menuContentManagerDAO.getSolutionListCbo();
	}
	public JSONArray getMenuContentList(String menualId,String searchType,String searchValue) throws Exception{
		return menuContentManagerDAO.getMenuContentList(menualId,searchType,searchValue);
	}
	public JSONArray getMenuContenGridtList(String menualId, String searchType, String searchValue) throws Exception {
		return menuContentManagerDAO.getMenuContenGridtList(menualId,searchType,searchValue);
	}
	public JSONArray getMenuContentInfo(String cid) throws Exception{
		return menuContentManagerDAO.getMenuContentInfo(cid);
	}
	public int insertMenuContent(String solutionList, String progNm, String progId, String localCd, String parentId, String content) throws Exception {
		return menuContentManagerDAO.insertMenuContent(solutionList,progNm,progId,localCd,parentId,content);
	}
	public int deleteMenuContent(String solutionList, String progId, String parentId) throws Exception{
		return menuContentManagerDAO.deleteMenuContent(solutionList,progId,parentId);
	}
	
}
