package com.iplanbiz.iportal.service.cms;

import java.io.IOException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.iportal.dao.cms.BoardDAO;
import com.iplanbiz.iportal.dto.Board;

@Service
public class BoardService {

	@Autowired
	BoardDAO boardDAO;

	public JSONArray getBoardList(String TABLE_NM, String TITLE, String CONTENT, String NAME, String GUBN,
			String START_DAT, String END_DAT) throws Exception {
		return boardDAO.getBoardList(TABLE_NM, TITLE, CONTENT, NAME, GUBN, START_DAT, END_DAT);
	}
	public List<HashMap<String, Object>> getBoardList(Board board) {
		return boardDAO.getBoardList(board);
	}


	public List<HashMap<String, String>> getPstepMax(Board board) throws Exception {
		return boardDAO.getPstepMax(board);
	}

	public void insertReplyTxt(Board board) throws Exception {
		boardDAO.insertReplyTxt(board);
	}

	public void deleteReplyTxt(Board board) throws Exception {
		boardDAO.deleteReplyTxt(board);
	}

	public void modifyHitCount(Board board) throws Exception {
		boardDAO.modifyHitCount(board);
	}
	
	public void createBoard(Board board) throws IOException {
		boardDAO.createBoard(board);
	}
	public void modifyBoard(Board board) throws IOException {
		boardDAO.modifyBoard(board);
	}
	
	public List<HashMap<String, String>> getBoardNew(Board board){
		return boardDAO.getBoardNew(board);
	}
	public Map<String, String> getNewBoard(String boardType, String pBoardno, String answerLevel){	
		return boardDAO.getNewBoard(boardType,pBoardno,answerLevel);
	}
	public List<HashMap<String, String>> getBoardBuser(Board board) {
		return boardDAO.getBoardBuser(board);
		
	}
	public List<HashMap<String, String>> getReplyTxt(Board board) {
		return boardDAO.getReplyTxt(board);
	}
	public void removeBoard(Board board) {
		boardDAO.removeBoard(board);          
	}
}
