package com.iplanbiz.iportal.service;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.SchedulerManagerDAO;
import com.iplanbiz.iportal.dto.schedulerManager;

@Service
public class SchedulerManagerService {

	@Autowired SchedulerManagerDAO schedulerDAO;
	
	public JSONArray getSchedulerManagerList ( String useYn, String schStatus ) throws Exception {
		return schedulerDAO.getSchedulerManagerList(useYn, schStatus);
	}
	public JSONArray getSchedulerManagerDetail ( String schID )  throws Exception {
		return schedulerDAO.getSchedulerManagerDetail(schID);
	}
	public JSONArray getJobMappingScheduler ( String schID, String jobTyp ) throws Exception {
		return schedulerDAO.getJobMappingScheduler(schID, jobTyp);
	}
	public JSONArray getDBinfoListByScheduler () throws Exception {
		return schedulerDAO.getDBinfoListByScheduler();
	}
	public JSONArray getSeverJobList( String jobDist, String schID, String searchName, String dbKey ) throws Exception {
		return schedulerDAO.getSeverJobList(jobDist, schID, searchName, dbKey);
	}
	public int insertBySchedulerManager ( schedulerManager scheduler ) throws Exception {
		return schedulerDAO.insertBySchedulerManager(scheduler);
	}
	public int deleteBySchedulerManager ( schedulerManager scheduler ) throws Exception {
		return schedulerDAO.deleteBySchedulerManager(scheduler);
	}

}
