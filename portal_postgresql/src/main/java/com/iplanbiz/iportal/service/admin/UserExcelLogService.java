package com.iplanbiz.iportal.service.admin;


import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplanbiz.iportal.dao.admin.UserExcelLogDAO;


@Service
public class UserExcelLogService {
	@Autowired
	UserExcelLogDAO userExcelLogDAO;
	
	

	public JSONArray getExcelLogReport(String cid) throws Exception {
		return userExcelLogDAO.getExcelLogReport(cid);
	}
	public JSONArray getExcelLogDate(String startdat,String enddat) throws Exception {
		return userExcelLogDAO.getExcelLogDate(startdat,enddat);
	}
	public JSONObject getNavi(String cid) throws Exception {
		return userExcelLogDAO.getNavi(cid);
	}
	public List<LinkedHashMap<String, String>> downExcelLogReport(String cid) {
		return userExcelLogDAO.downExcelLogReport(cid);
	}
	public List<LinkedHashMap<String, String>> downExcelLogDate(String start_dat,String end_dat) {
		return userExcelLogDAO.downExcelLogDate(start_dat,end_dat);
	}
	
}
