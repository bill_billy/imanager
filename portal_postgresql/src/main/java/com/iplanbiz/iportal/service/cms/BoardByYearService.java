package com.iplanbiz.iportal.service.cms;

import java.io.IOException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.iportal.dao.cms.BoardByYearDAO;
import com.iplanbiz.iportal.dto.Board;

@Service
public class BoardByYearService {

	@Autowired
	BoardByYearDAO boardDAO;

	public JSONArray getYears()  {
		try {
			List<LinkedHashMap<String, Object>> list = boardDAO.getYears();
			JSONArray returnData = new JSONArray();
			for(LinkedHashMap<String, Object> row : list) {
				JSONObject json = new JSONObject();
				json.put("COM_COD", row.get("COM_COD"));
				json.put("COM_NM", row.get("COM_NM"));
				returnData.add(json);
			}
			
			return returnData;
		} catch (Exception e) {
			return new JSONArray();
		}
	}
	
	public JSONArray getBoardList(String TABLE_NM, String TITLE, String CONTENT, String NAME, String GUBN,
			String START_DAT, String END_DAT, String yYYY) throws Exception {
		return boardDAO.getBoardList(TABLE_NM, TITLE, CONTENT, NAME, GUBN, START_DAT, END_DAT,yYYY);
	}
	public List<HashMap<String, Object>> getBoardList(Board board) {
		return boardDAO.getBoardList(board);
	}


	public List<HashMap<String, String>> getPstepMax(Board board) throws Exception {
		return boardDAO.getPstepMax(board);
	}

	public void insertReplyTxt(Board board) throws Exception {
		boardDAO.insertReplyTxt(board);
	}

	public void deleteReplyTxt(Board board) throws Exception {
		boardDAO.deleteReplyTxt(board);
	}

	public void modifyHitCount(Board board) throws Exception {
		boardDAO.modifyHitCount(board);
	}
	
	public void createBoard(Board board) throws IOException {
		boardDAO.createBoard(board);
	}
	public void modifyBoard(Board board) throws IOException {
		boardDAO.modifyBoard(board);
	}
	
	public List<HashMap<String, String>> getBoardNew(Board board){
		return boardDAO.getBoardNew(board);
	}
	public Map<String, String> getNewBoard(String boardType, String pBoardno, String answerLevel){	
		return boardDAO.getNewBoard(boardType,pBoardno,answerLevel);
	}
	public List<HashMap<String, String>> getBoardBuser(Board board) {
		return boardDAO.getBoardBuser(board);
		
	}
	public List<HashMap<String, String>> getReplyTxt(Board board) {
		return boardDAO.getReplyTxt(board);
	}
	public void removeBoard(Board board) {
		boardDAO.removeBoard(board);          
	}
}
