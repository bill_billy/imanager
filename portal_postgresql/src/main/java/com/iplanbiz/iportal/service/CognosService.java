package com.iplanbiz.iportal.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.iplanbiz.iportal.dto.User;

/**
 * @author : yun hee Kim(yhkim@khantech.co.kr)
 * @date   : 2012. 7. 6.
 * @desc   : 
 */
public interface CognosService {
	public int logon(User user);
	public void logout();
	
	// Cognos API
	// TODO : Cognos API 개발 1차 완료 - 개발자 김윤희
	public boolean delPermissionAll(String target);
	public boolean setPermission(String source, List<Map<String, Object>> setPermission);
//	public String getPermission(String source, String sourceType, String target);
//	public boolean delPermission(String source, String sourceType, String target);
	public boolean addRole(String roleName);
	public boolean deleteRole(String roleName);
	public boolean portalAddToRole(String userId, String Role, String type);
	
	public boolean delUserFromRole(String userId, String role);
	public boolean delGroupFromRole(String groupId, String role);
	public boolean delAllDataFromRole(String role);
}
