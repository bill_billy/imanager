package com.iplanbiz.iportal.service.admin;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplanbiz.iportal.dao.admin.MenuContentManagerDAO;
import com.iplanbiz.iportal.dao.admin.TechnologyContentListDAO;

@Service
public class TechnologyContentListService {

	@Autowired 
	TechnologyContentListDAO technoDAO;
	
	
	public JSONArray getTechnoContentList() throws Exception{
		return technoDAO.getTechnoContentList();
	}

	public JSONArray getLabelTechnoContent(String cid) throws Exception{
		return technoDAO.getLabelTechnoContent(cid);
	}

	public JSONArray getTechnoContentInfo(String cid) throws Exception{
		return technoDAO.getTechnoContentInfo(cid);
	}

	public JSONArray getSearchTechnoContent(String content_text) throws Exception{
		return technoDAO.getSearchTechnoContent(content_text);
	}
	
}
