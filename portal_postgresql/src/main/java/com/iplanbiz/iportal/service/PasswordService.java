package com.iplanbiz.iportal.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplanbiz.iportal.dao.PasswordDAO;

@Service
public class PasswordService{ 
	
	@Autowired
	PasswordDAO passwordDAO;
	
	
	public int update(String newpwd,String oldpwd,String remoteIP, String remoteOS, String remoteBrowser) throws Exception{
		return passwordDAO.update(newpwd,oldpwd,remoteIP,remoteOS,remoteBrowser);
	}
	public int equalOrgpwd(String orgpwd) throws Exception{
		return passwordDAO.equalOrgpwd(orgpwd);
	}
	public String passwordUpdateDat(String unid) {
		return passwordDAO.passwordUpdateDat(unid);
	}
	
	
}
