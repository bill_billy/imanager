package com.iplanbiz.iportal.service.procedure;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dao.procedure.ProcedureDAO;
import com.iplanbiz.iportal.dto.Procedure;

@Service
public class ProcedureService { 
	@Autowired SqlSessionFactory sqlSessionFactory;
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ProcedureDAO procedureDao;
	
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	public List<HashMap<String, String>> procedureList(String procedureName){
		return procedureDao.procedureList(procedureName);
	}
	public List<HashMap<String, String>> procedureListTimeCogdw(String dbGubn, String dbUser, String procedure){
		return procedureDao.procedureListTimeCogdw(dbGubn, dbUser, procedure);
	}
	
	public List<HashMap<String, String>> procedureCodeList(String comlCod){
		return procedureDao.procedureCodeList(comlCod);
	}
	public String callSp(String dbGubn, String dbUser, String procedure, String year, String idx) throws Exception{
		return procedureDao.callSp(dbGubn, dbUser, procedure, year, idx);
	}
	public JSONArray getProcedureList(String procedureName) throws Exception{
		return procedureDao.getProcedureList(procedureName);
	}
	
	public JSONArray procedureResultListCogdw(String dbGubn, String dbUser, String paramProcedureName) throws Exception{
		return procedureDao.procedureResultListCogdw(dbGubn,dbUser,paramProcedureName);
	}
}
