package com.iplanbiz.iportal.service.dashboard;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.DashboardCodeDAO;

@Service
public class DashboardCodeService {
	@Autowired
	DashboardCodeDAO dashboardCodeDAO;
	
	public JSONArray getDashboardCodeList(String comlCod) throws Exception{
		return dashboardCodeDAO.getDashboardCodeList(comlCod);
	}

}
