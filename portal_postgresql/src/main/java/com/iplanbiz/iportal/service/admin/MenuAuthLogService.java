package com.iplanbiz.iportal.service.admin;

import java.util.LinkedHashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplanbiz.iportal.dao.admin.MenuAuthLogDAO;

@Service
public class MenuAuthLogService {
	
	@Autowired
	MenuAuthLogDAO menuAuthLogDAO;
	
	public JSONArray getRoleList(String rolenm, String startDat, String endDat) throws Exception{
		return menuAuthLogDAO.getRoleList(rolenm,startDat,endDat);
	}
	public JSONArray getGroupList(String groupnm, String startDat, String endDat) throws Exception{
		return menuAuthLogDAO.getGroupList(groupnm,startDat,endDat);
	}
	public JSONArray getUserList(String usernm, String startDat, String endDat) throws Exception{
		return menuAuthLogDAO.getUserList(usernm,startDat,endDat);
	}
	public JSONArray getLogList(String type, String id, String startDat, String endDat) throws Exception{
		return menuAuthLogDAO.getLogList(type,id,startDat,endDat);
	}
	public List<LinkedHashMap<String, Object>> getLogListForExcel(String type, String id, String startDat, String endDat){
		return menuAuthLogDAO.getLogListForExcel(type,id,startDat,endDat);
	}
}
