package com.iplanbiz.iportal.service.excelPgm;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplanbiz.iportal.dao.excelPgm.StepCloseEduStatsDAO;



@Service
public class StepCloseEduStatsService {

	@Autowired
	StepCloseEduStatsDAO stepCloseEduStatDAO;
	
	public JSONArray getStepListForCloseEduStat(String yyyy) throws Exception {
		return stepCloseEduStatDAO.getStepListForCloseEduStat(yyyy);
	}
	public JSONArray getStepHistoryForCloseEduStat(String yyyy,String mm) throws Exception {
		return stepCloseEduStatDAO.getStepHistoryForCloseEduStat(yyyy,mm);
	}
	public void stepCloseEduStatsSave(String yyyy,String mm,String status) throws Exception {
		stepCloseEduStatDAO.stepCloseEduStatsSave(yyyy,mm,status);
	}

}
