package com.iplanbiz.iportal.service.portlet;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.portlet.PortletGroupManagerDAO;

@Service
public class PortletGroupManagerService {

	@Autowired PortletGroupManagerDAO portletGroupManagerDAO;
	
	public JSONArray getPortletGroupManagerList() throws Exception {
		return portletGroupManagerDAO.getPortletGroupManagerList();
	}
	public JSONArray getPortletGroupDataDetail ( String unID ) throws Exception {
		return portletGroupManagerDAO.getPortletGroupDataDetail(unID);
	}
	public int insertPortletGroup ( String unID, String grpName, String etc ) throws Exception {
		return portletGroupManagerDAO.insertPortletGroup(unID, grpName, etc);
	}
	public int deletePortletGroup ( String[] arrayDeleteUnID ) throws Exception {
		return portletGroupManagerDAO.deletePortletGroup(arrayDeleteUnID);
	}
}
