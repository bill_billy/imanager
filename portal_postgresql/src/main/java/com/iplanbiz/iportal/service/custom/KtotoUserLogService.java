package com.iplanbiz.iportal.service.custom;

import java.util.LinkedHashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplanbiz.iportal.dao.custom.KtotoUserLogDAO;

@Service
public class KtotoUserLogService {
	
	@Autowired
	KtotoUserLogDAO ktotoUserLogDAO;
	
	public JSONArray getUserList(String team,String userNm) throws Exception{
		return ktotoUserLogDAO.getUserList(team,userNm);
	}
	public JSONArray getUserLogList(String unid) throws Exception{
		return ktotoUserLogDAO.getUserLogList(unid);
	}
	public List<LinkedHashMap<String, Object>> getUserLogListForExcel(String unid) throws Exception {
		return ktotoUserLogDAO.getUserLogListForExcel(unid);
	}
}
