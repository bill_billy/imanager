package com.iplanbiz.iportal.service.custom;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.custom.UnivKNCAFCustomDAO;

@Service
@Transactional(readOnly=true)
public class UnivKNCAFCustomService {

	@Autowired 
	UnivKNCAFCustomDAO kncafDAO;
	/**
	 * @param args
	 */
	
	public JSONArray getMainDashboardKpiList () throws Exception {
		return kncafDAO.getMainDashboardKpiList();
	}
	public JSONArray getMainDashboardKpiTarget ( String scID ) throws Exception {
		return kncafDAO.getMainDashboardKpiTarget(scID);
	}
	public JSONArray getMainDashbaordSqlDATA ( String scID, String kpiID ) throws Exception {
		return kncafDAO.getMainDashbaordSqlDATA(scID, kpiID);
	}

}
