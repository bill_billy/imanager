package com.iplanbiz.iportal.service.file;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
 
import com.iplanbiz.comm.Utils; 
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.file.FileDao;
import com.iplanbiz.iportal.dto.File;

@Service
public class FileService {
	

	@Autowired
	private FileDao fileDao;
	

	public void removeFile(int fileId) throws FileNotFoundException, IOException{
		fileDao.removeFile(fileId);
	}

	
	public File saveFile(CommonsMultipartFile file, String[] extType) throws IOException
	{
		String fileSaveName = "";
		int result = 0;
		com.iplanbiz.core.io.file.FileService fileService = new com.iplanbiz.core.io.file.FileService(WebConfig.getProperty("system.upload.SaveDir"),Long.valueOf(WebConfig.getProperty("system.upload.maxUploadSize").toString()));
		
		fileSaveName = fileService.setFileName();
		result = fileService.saveFile(file, extType, fileSaveName);
		
		File returnFile = new File();
		
		if(result == 0){
			returnFile.setFileOrgName(file.getOriginalFilename());
			returnFile.setFileSaveName(fileSaveName);
			returnFile.setFilePath(fileService.getUpLoadPath());
			returnFile.setFileSize(Utils.getString(String.valueOf(file.getSize()), ""));
			returnFile.setFileExt(fileService.getFileExt());
		}
		
		return returnFile;
	}
	
	public List<HashMap<String, String>> getFileList(File file) {
		return fileDao.getFileList(file);
	}
	

	public Map<String, String> getFile(int fileId) {
		return fileDao.getFile(fileId);
	}

	public void insertFile(File file) {
		fileDao.insertFile(file);
	}

	public void removeFileByBoardNo(File file) {
		fileDao.removeFileByBoardNo(file);
	}
}