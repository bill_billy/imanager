package com.iplanbiz.iportal.service.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.system.CustomSqlHouseDAO;
import com.iplanbiz.iportal.dao.system.SqlHouseDAO;

@Service
@Transactional 
public class CustomSqlHouseService {

	@Autowired
	CustomSqlHouseDAO customSqlHouseDAO;
	
	@Autowired
	DBMSDAO dbmsDAO;
	
	@Autowired
	DBMSService dbmsService;
	
	
	public List<HashMap<String, Object>> getCustomSqlHouseList() throws Exception{ 
		return customSqlHouseDAO.getCustomHouseListBySearchText("");  
	}
	public List<HashMap<String, Object>> getCustomSqlHouseList(String searchText) throws Exception{ 
		return customSqlHouseDAO.getCustomHouseListBySearchText(searchText);  
	}
	public int insertCustomSqlHouse(String queryID, String dbkey, String description,
			String sql) throws Exception {
		return customSqlHouseDAO.insertCustomSqlHouse(queryID, dbkey, description, sql);  
	}
	public int updateCustomSqlHouse(String queryID, String idx, String dbkey, String description,
			String sql) throws Exception {
		// TODO Auto-generated method stub
		return customSqlHouseDAO.updateCustomSqlHouse(queryID, idx, dbkey, description, sql); 
	} 
	
	public List<LinkedHashMap<String,Object>> executeCustomQuery(String queryID, LinkedHashMap<String,Object> param) throws Exception{		

		
		return customSqlHouseDAO.executeCustomQuery(queryID, param);
	}
}
