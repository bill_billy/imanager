package com.iplanbiz.iportal.service;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.iportal.dao.BasicDAO;
import com.iplanbiz.iportal.dao.UserLoginConnectionDAO;

@Service
public class UserLoginConnectionService {

	@Autowired UserLoginConnectionDAO userLoginDAO; 
	
	public JSONArray getUserLoginConnectionYyyymm () throws Exception {
		return userLoginDAO.getUserLoginConnectionYyyymm();
	}
	public JSONArray getUserLoginConnectionList ( String yyyymm ) throws Exception {
		return userLoginDAO.getUserLoginConnectionList(yyyymm);
	}
	public JSONArray getUserLoginConnectionByExcel ( String yyyymm ) throws Exception {
		return userLoginDAO.getUserLoginConnectionByExcel(yyyymm);
	}
	public int insertUserLog ( String actionTypeNo, String targetTypeNo, String targetValue, String cID, String remoteIP, String osInfo, String browserInfo, String browserVersion, String targetPath) throws Exception {
		return userLoginDAO.insertUserLog(actionTypeNo, targetTypeNo, targetValue, cID, remoteIP, osInfo, browserInfo, browserVersion, targetPath);
	}
}
