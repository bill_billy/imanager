package com.iplanbiz.iportal.service.excelPgm;

import java.util.Map;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.iportal.dao.excelPgm.ExcelPgmDQDataDAO;


@Service
public class ExcelPgmDQDataService {
	@Autowired
	ExcelPgmDQDataDAO excelPgmDQDataDAO;

	public JSONArray getProgramList(String pCd, String cCd, String searchText) throws Exception {
		return excelPgmDQDataDAO.getProgramList(pCd,cCd,searchText);
	}
/*
	public Map<String, Object> getExcelPgmDQDataRowId(String pCd, String cCd, String pgmId, String rowId) {
		return excelPgmDQDataDAO.getExcelPgmDQDataRowId(pCd,cCd,pgmId,rowId);
	}*/
	public void getExcelPgmDQData(String pCd, String cCd, String pgmId) {
		//return excelPgmDQDataDAO.getExcelPgmDQData(pCd,cCd,pgmId);
		
	}

}
