package com.iplanbiz.iportal.service;

import java.util.LinkedHashMap;
import java.util.List;
 
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.iportal.dao.CodeDAO;
import com.iplanbiz.iportal.dao.home.HomeDAO;

@Service
public class CodeService { 
	
	@Autowired
	CodeDAO codeDAO;
	
	public JSONArray getCodeListByAsc(String comlCod) throws Exception {
		return codeDAO.getCodeListByAsc(comlCod);
	}
	public JSONArray getCodeListByDesc(String comlCod) throws Exception{
		return codeDAO.getCodeListByDesc(comlCod);
	} 
}
