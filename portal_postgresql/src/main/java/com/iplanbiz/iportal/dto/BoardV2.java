package com.iplanbiz.iportal.dto;

import java.util.HashMap;
import java.util.List;

import org.springframework.web.multipart.commons.CommonsMultipartFile;


public class BoardV2 {
	public BoardV2() {        }
//	public KtotoNotice(String boardType, int boardNo) { 
//		super();
//		this.boardType = boardType;
//		this.boardNo = boardNo; 
//	}
//	
//	public KtotoNotice(String boardType, int boardNo,String yyyy) { 
//		super();
//		this.boardType = boardType;
//		this.boardNo = boardNo; 
//		this.yyyy = yyyy; 
//	}
	private String boardType;
	private String boardNO;
	private String title;
	private String userID;
	private String userName;
	private String context;
	//	private String division1;
	private CommonsMultipartFile[] file;	//파일
	private List<HashMap<String,String>> files;
	
	
	public String getBoardType() {
		return boardType;
	}
	public void setBoardType(String boardType) {
		this.boardType = boardType;
	}
	public String getBoardNO() {
		return boardNO;
	}
	public void setBoardNO(String boardNO) {
		this.boardNO = boardNO;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public CommonsMultipartFile[] getFile() {
		return file;
	}
	public void setFile(CommonsMultipartFile[] file) {
		this.file = file;
	}
	public List<HashMap<String, String>> getFiles() {
		return files;
	}
	public void setFiles(List<HashMap<String, String>> files) {
		this.files = files;
	}
	
	
	
}
