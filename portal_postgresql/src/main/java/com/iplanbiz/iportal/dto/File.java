package com.iplanbiz.iportal.dto;

import com.iplanbiz.iportal.config.WebConfig;

public class File {
	public File() {        }
    
	private String tableNm;	// 게시판 테이블 명
	private int fileId;	// 파일 NO      
	private int boardNo;	// 게시물 NO
	private String fileSaveName;	// 파일저장명
	private String fileOrgName;	// 파일명
	private String filePath;	// 파일경로
	private String fileSize;	// 파일크기
	private String fileExt;	// 파일 확장자
	private String delBoardNo; // 자식 있을 경우 삭제할 게시물 No
	
	
	public String getTableNm() {
		return tableNm;
	}
	public void setTableNm(String tableNm) {
		this.tableNm = tableNm;
	}
	public int getFileId() {
		return fileId;
	}
	public void setFileId(int fileId) {
		this.fileId = fileId;
	}
	public int getBoardNo() {
		return boardNo;
	}
	public void setBoardNo(int boardNo) {
		this.boardNo = boardNo;
	}
	public String getFileSaveName() {
		return fileSaveName;
	}
	public void setFileSaveName(String fileSaveName) {
		this.fileSaveName = fileSaveName;
	}
	public String getFileOrgName() {
		return fileOrgName;
	}
	public void setFileOrgName(String fileOrgName) {
		this.fileOrgName = fileOrgName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public String getFileExt() {
		return fileExt;
	}
	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}
	public String getDelBoardNo() {
		return delBoardNo;
	}
	public void setDelBoardNo(String delBoardNo) {
		this.delBoardNo = delBoardNo;
	}
	@Override
	public String toString() {
		return "BoardFile [fileId=" + fileId + ", boardNo=" + boardNo
				+ ", fileSaveName=" + fileSaveName + ", fileOrgName="
				+ fileOrgName + ", filePath=" + filePath + ", fileSize="
				+ fileSize + ", fileExt=" + fileExt + "]";
	}
}
