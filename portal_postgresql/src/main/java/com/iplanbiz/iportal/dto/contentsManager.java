package com.iplanbiz.iportal.dto;



import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class contentsManager {
	private String poID;
	private String name;
	private String active;
	private String alwaysActive;
	private String menuOpenType;
	private String[] keyName;
	private String[] keyValue;
	private String[] explain;
	private CommonsMultipartFile portletImage;
	
//	private CommonsMultipartFile[] portletImage;
	
	
	
	
	
	
	
//	public CommonsMultipartFile[] getPortletImage() {
//		return portletImage;
//	}
//	public void setPortletImage(CommonsMultipartFile[] portletImage) {
//		this.portletImage = portletImage;
//	}
	
	
	public String[] getKeyName() {
		return keyName;
	}
	public CommonsMultipartFile getPortletImage() {
		return portletImage;
	}
	public void setPortletImage(CommonsMultipartFile portletImage) {
		this.portletImage = portletImage;
	}
	public void setKeyName(String[] keyName) {
		this.keyName = keyName;
	}
	public String[] getKeyValue() {
		return keyValue;
	}
	public void setKeyValue(String[] keyValue) {
		this.keyValue = keyValue;
	}
	public String[] getExplain() {
		return explain;
	}
	public void setExplain(String[] explain) {
		this.explain = explain;
	}
	public String getPoID() {
		return poID;
	}
	public void setPoID(String poID) {
		this.poID = poID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getAlwaysActive() {
		return alwaysActive;
	}
	public void setAlwaysActive(String alwaysActive) {
		this.alwaysActive = alwaysActive;
	}
	public String getMenuOpenType() {
		return menuOpenType;
	}
	public void setMenuOpenType(String menuOpenType) {
		this.menuOpenType = menuOpenType;
	}
}
