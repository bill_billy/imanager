/**
 * Copyright(c) 2012 iPlanBiz All Rights Reserved
 */
package com.iplanbiz.iportal.dto;



/**
 * @author : 장민수(msjang@iplanbiz.co.kr)
 * @date   : 2016. 3. 10.
 * @desc   : 
 */
public class ExcelDQDTO{

	 private String PGM_ID;
	 private String  PGM_NM;
	 private String  P_CD;
	 private String  P_NM;
	 private String  C_CD;
     private String  C_NM;
     private String  JOB_CYC;
     private String  JOB_CYC_NM;
     private String  DBKEY;
     private String  SYSTEM_ID;
     private String  PGM_EMP;
     private String  PGM_EMP_NM; 
     private String  USE_YN;
     private String  SORT_ORDER;
     private String  PGM_DEF;
     private String  DOWN_SQL;
     private String  INSERT_SQL;
     private String  UP_SQL;
     private String  DEL_SQL;
	public String getPGM_ID() {
		return PGM_ID;
	}
	public void setPGM_ID(String pGM_ID) {
		PGM_ID = pGM_ID;
	}
	public String getPGM_NM() {
		return PGM_NM;
	}
	public void setPGM_NM(String pGM_NM) {
		PGM_NM = pGM_NM;
	}
	public String getP_CD() {
		return P_CD;
	}
	public void setP_CD(String p_CD) {
		P_CD = p_CD;
	}
	public String getP_NM() {
		return P_NM;
	}
	public void setP_NM(String p_NM) {
		P_NM = p_NM;
	}
	public String getC_CD() {
		return C_CD;
	}
	public void setC_CD(String c_CD) {
		C_CD = c_CD;
	}
	public String getC_NM() {
		return C_NM;
	}
	public void setC_NM(String c_NM) {
		C_NM = c_NM;
	}
	public String getJOB_CYC() {
		return JOB_CYC;
	}
	public void setJOB_CYC(String jOB_CYC) {
		JOB_CYC = jOB_CYC;
	}
	public String getJOB_CYC_NM() {
		return JOB_CYC_NM;
	}
	public void setJOB_CYC_NM(String jOB_CYC_NM) {
		JOB_CYC_NM = jOB_CYC_NM;
	}
	public String getDBKEY() {
		return DBKEY;
	}
	public void setDBKEY(String dBKEY) {
		DBKEY = dBKEY;
	}
	public String getSYSTEM_ID() {
		return SYSTEM_ID;
	}
	public void setSYSTEM_ID(String sYSTEM_ID) {
		SYSTEM_ID = sYSTEM_ID;
	}
	public String getPGM_EMP() {
		return PGM_EMP;
	}
	public void setPGM_EMP(String pGM_EMP) {
		PGM_EMP = pGM_EMP;
	}
	public String getPGM_EMP_NM() {
		return PGM_EMP_NM;
	}
	public void setPGM_EMP_NM(String pGM_EMP_NM) {
		PGM_EMP_NM = pGM_EMP_NM;
	}
	public String getUSE_YN() {
		return USE_YN;
	}
	public void setUSE_YN(String uSE_YN) {
		USE_YN = uSE_YN;
	}
	public String getSORT_ORDER() {
		return SORT_ORDER;
	}
	public void setSORT_ORDER(String sORT_ORDER) {
		SORT_ORDER = sORT_ORDER;
	}
	public String getPGM_DEF() {
		return PGM_DEF;
	}
	public void setPGM_DEF(String pGM_DEF) {
		PGM_DEF = pGM_DEF;
	}
	public String getDOWN_SQL() {
		return DOWN_SQL;
	}
	public void setDOWN_SQL(String dOWN_SQL) {
		DOWN_SQL = dOWN_SQL;
	}
	public String getUP_SQL() {
		return UP_SQL;
	}
	public void setUP_SQL(String uP_SQL) {
		UP_SQL = uP_SQL;
	}
	public String getDEL_SQL() {
		return DEL_SQL;
	}
	public void setDEL_SQL(String dEL_SQL) {
		DEL_SQL = dEL_SQL;
	}
	public String getINSERT_SQL() {
		return INSERT_SQL;
	}
	public void setINSERT_SQL(String iNSERT_SQL) {
		INSERT_SQL = iNSERT_SQL;
	}
}
