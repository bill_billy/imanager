package com.iplanbiz.iportal.dto;


public class ExcelPgmDQ {
	
	private String acctId;
	private String division;
	private String ac; //상태구분:저장, 삭제
	private String pgmId;
	private String pgmNm;
	private String comlCod; //대분류
	private String comlNm;
	private String comCod; //중분류
	private String comNm;
	private String jobCyc; //주기
	private String jobCycNm; //주기
	private Integer systemid; //업무규칙
	private String pgmEmp;
	private String pgmEmpNm;
	private String dbkey;
	private String useYn;
	private String useYnNm;
	private Integer sortOrder;
	private String pgmDef;
	private String downSql;
	private String upSql;
	private String insertSql;
	private String delSql;
	private String udc1; //사용자 정의 컬럼1
	private String udc2; //사용자 정의 컬럼2
	private String insertDat;
	private String insertEmp;
	private String updateDat;
	private String updateEmp; 
	
	
	
	@Override
	public String toString() {
		return "ExcelPgmDQ [acctId=" + acctId + ", division=" + division
				+ ", ac=" + ac + ", pgmId=" + pgmId + ", pgmNm=" + pgmNm
				+ ", comlCod=" + comlCod + ", comlNm=" + comlNm + ", comCod="
				+ comCod + ", comNm=" + comNm + ", jobCyc=" + jobCyc
				+ ", jobCycNm=" + jobCycNm + ", systemid=" + systemid +", pgmEmp=" + pgmEmp + ", pgmEmpNm=" + pgmEmpNm
				+ ", useYn=" + useYn + ", useYnNm=" + useYnNm + ", sortOrder="
				+ sortOrder + ", pgmDef=" + pgmDef + ", downSql=" + downSql
				+ ", upSql=" + upSql + ", insertSql=" + insertSql+ ", delSql=" + delSql + ", udc1=" + udc1
				+ ", udc2=" + udc2 + ", insertDat=" + insertDat
				+ ", insertEmp=" + insertEmp + ", updateDat=" + updateDat
				+ ", updateEmp=" + updateEmp + "]";
	}
	public String getAcctId() {
		return acctId;
	}
	public void setAcctId(String acctId) {
		this.acctId = acctId;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getAc() {
		return ac;
	}
	public void setAc(String ac) {
		this.ac = ac;
	}
	public String getPgmId() {
		return pgmId;
	}
	public void setPgmId(String pgmId) {
		this.pgmId = pgmId;
	}
	public String getPgmNm() {
		return pgmNm;
	}
	public void setPgmNm(String pgmNm) {
		this.pgmNm = pgmNm;
	}
	public String getComlCod() {
		return comlCod;
	}
	public void setComlCod(String comlCod) {
		this.comlCod = comlCod;
	}
	public String getComlNm() {
		return comlNm;
	}
	public void setComlNm(String comlNm) {
		this.comlNm = comlNm;
	}
	public String getComCod() {
		return comCod;
	}
	public void setComCod(String comCod) {
		this.comCod = comCod;
	}
	public String getComNm() {
		return comNm;
	}
	public void setComNm(String comNm) {
		this.comNm = comNm;
	}
	public String getJobCyc() {
		return jobCyc;
	}
	public void setJobCyc(String jobCyc) {
		this.jobCyc = jobCyc;
	}
	public String getJobCycNm() {
		return jobCycNm;
	}
	public void setJobCycNm(String jobCycNm) {
		this.jobCycNm = jobCycNm;
	}
	public String getPgmEmp() {
		return pgmEmp;
	}
	public void setPgmEmp(String pgmEmp) {
		this.pgmEmp = pgmEmp;
	}
	public String getPgmEmpNm() {
		return pgmEmpNm;
	}
	public void setPgmEmpNm(String pgmEmpNm) {
		this.pgmEmpNm = pgmEmpNm;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getUseYnNm() {
		return useYnNm;
	}
	public void setUseYnNm(String useYnNm) {
		this.useYnNm = useYnNm;
	}
	public Integer getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	public String getPgmDef() {
		return pgmDef;
	}
	public void setPgmDef(String pgmDef) {
		this.pgmDef = pgmDef;
	}
	public String getDownSql() {
		return downSql;
	}
	public void setDownSql(String downSql) {
		this.downSql = downSql;
	}
	public String getUpSql() {
		return upSql;
	}
	public void setUpSql(String upSql) {
		this.upSql = upSql;
	}
	public String getDelSql() {
		return delSql;
	}
	public void setDelSql(String delSql) {
		this.delSql = delSql;
	}
	public String getUdc1() {
		return udc1;
	}
	public void setUdc1(String udc1) {
		this.udc1 = udc1;
	}
	public String getUdc2() {
		return udc2;
	}
	public void setUdc2(String udc2) {
		this.udc2 = udc2;
	}
	public String getInsertDat() {
		return insertDat;
	}
	public void setInsertDat(String insertDat) {
		this.insertDat = insertDat;
	}
	public String getInsertEmp() {
		return insertEmp;
	}
	public void setInsertEmp(String insertEmp) {
		this.insertEmp = insertEmp;
	}
	public String getUpdateDat() {
		return updateDat;
	}
	public void setUpdateDat(String updateDat) {
		this.updateDat = updateDat;
	}
	public String getUpdateEmp() {
		return updateEmp;
	}
	public void setUpdateEmp(String updateEmp) {
		this.updateEmp = updateEmp;
	}
	/**
	 * @return the systemid
	 */
	public Integer getSystemid() {
		return systemid;
	}
	/**
	 * @param systemid the systemid to set
	 */
	public void setSystemid(Integer systemid) {
		this.systemid = systemid;
	}
	/**
	 * @return the dbkey
	 */
	public String getDbkey() {
		return dbkey;
	}
	/**
	 * @param dbkey the dbkey to set
	 */
	public void setDbkey(String dbkey) {
		this.dbkey = dbkey;
	}
	public String getInsertSql() {
		return insertSql;
	}
	public void setInsertSql(String insertSql) {
		this.insertSql = insertSql;
	}
}