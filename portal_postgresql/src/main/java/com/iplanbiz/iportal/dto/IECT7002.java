/**
 * 
 */
package com.iplanbiz.iportal.dto;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * @author Sungjin, Woo (newanto@gmail.net)
 * @date	2012.08.16
 */
public class IECT7002 {
	
	// admin>system>dafaultSetting>modify 에서 input 된 file 정보를 자동 매핑해주기 위해 생성
	// 다른 정보들도 자동 매핑시키려면 추가해줘야함
	// 임시로 IECT7002 객체에 넣어줌
	
	//////////////// DefaultSetting 
	private CommonsMultipartFile logo_img; 
	private CommonsMultipartFile footer_img;
	private String poid;
	
    public String getPoid() {
		return poid;
	}

	public void setPoid(String poid) {
		this.poid = poid;
	}

	public CommonsMultipartFile getLogo_img() {
		return logo_img;
	}

	public void setLogo_img(CommonsMultipartFile logo_img) {
		this.logo_img = logo_img;
	}

	public CommonsMultipartFile getFooter_img() {
		return footer_img;
	}

	public void setFooter_img(CommonsMultipartFile footer_img) {
		this.footer_img = footer_img;
	}

	////////////////PortalSetting 
	
	private CommonsMultipartFile[] file;
	
	private String[] file_src;
	
	private String[] file_name;

	public CommonsMultipartFile[] getFile() {
		return file;
	}

	public void setFile(CommonsMultipartFile[] file) {
		this.file = file;
	}

	public String[] getFile_src() {
		return file_src;
	}

	public void setFile_src(String[] file_src) {
		this.file_src = file_src;
	}

	public String[] getFile_name() {
		return file_name;
	}

	public void setFile_name(String[] file_name) {
		this.file_name = file_name;
	}
	
	////////////// test
	
	private CommonsMultipartFile portletImage;

	public CommonsMultipartFile getPortletImage() {
		return portletImage;
	}

	public void setPortletImage(CommonsMultipartFile portletImage) {
		this.portletImage = portletImage;
	}
}