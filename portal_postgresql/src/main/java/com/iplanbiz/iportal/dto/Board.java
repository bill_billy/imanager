package com.iplanbiz.iportal.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.iportal.config.WebConfig;


public class Board {
	public Board() {        }
	public Board(String boardType, int boardNo) { 
		super();
		this.boardType = boardType;
		this.boardNo = boardNo; 
	}
	
	public Board(String boardType, int boardNo,String yyyy) { 
		super();
		this.boardType = boardType;
		this.boardNo = boardNo; 
		this.yyyy = yyyy; 
	}
	private int boardNo;	// 게시판 NO
	private String boardType;	// 게시판 종류
	private String pBoardno; //부모 게시판반호
	private String answerLevel;//게시글 레벨
	private String title;	// 제목
	private String userId;	// 작성자ID
	private String name;	// 작성자명
	private String content;	// 글내용
	private int hitCount;	// 조회수
	private String createDate;	//작성일자
	private String noticeYn;
	private String boardGroup;
	private String gubn;
	private String yyyy;

	//	private String division1;
	private CommonsMultipartFile[] file;	//파일
	private List<HashMap<String,String>> files;
	
	private String startDay1;
	private String endDay1;
	private String select;
	private String srcTxt;
	
	private String tableNm;
	private int pid;		// 게시판 글 번호
	private int pStep;      // 답글 
	private int pLevel;     // 깊이(들여쓰기)
	private String ac;
	private String replyTxt;
	
	
	public String getYyyy() {
		return yyyy;
	}
	public void setYyyy(String yyyy) {
		this.yyyy = yyyy;
	}
	public String getGubn() {
		return gubn;
	}
	public void setGubn(String gubn) {
		this.gubn = gubn;
	}
	
	public int getBoardNo() {
		return boardNo;
	}
	public void setBoardNo(int boardNo) {
		this.boardNo = boardNo;
	}
	public String getBoardType() {
		return boardType;
	}
	public void setBoardType(String boardType) {
		this.boardType = boardType;
	}
	public String getpBoardno() {
		return pBoardno;
	}
	public void setpBoardno(String pBoardno) {
		this.pBoardno = pBoardno;
	}
	public String getAnswerLevel() {
		return answerLevel;
	}
	public void setAnswerLevel(String answerLevel) {
		this.answerLevel = answerLevel;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getHitCount() {
		return hitCount;
	}
	public void setHitCount(int hitCount) {
		this.hitCount = hitCount;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getNoticeYn() {
		return noticeYn;
	}
	public void setNoticeYn(String noticeYn) {
		this.noticeYn = noticeYn;
	}
	public CommonsMultipartFile[] getFile() {
		return file;
	}
	public void setFile(CommonsMultipartFile[] file) {
		this.file = file;
	}
	public List<HashMap<String, String>> getFiles() {
		return files;
	}
	public void setFiles(List<HashMap<String, String>> files) {
		this.files = files;
	}
	public String getStartDay1() {
		return startDay1;
	}
	public void setStartDay1(String startDay1) {
		this.startDay1 = startDay1;
	}
	public String getEndDay1() {
		return endDay1;
	}
	public void setEndDay1(String endDay1) {
		this.endDay1 = endDay1;
	}
	public String getSelect() {
		return select;
	}
	public void setSelect(String select) {
		this.select = select;
	}
	public String getSrcTxt() {
		return srcTxt;
	}
	public void setSrcTxt(String srcTxt) {
		this.srcTxt = srcTxt;
	}
	public String getTableNm() {
		return tableNm;
	}
	public void setTableNm(String tableNm) {
		this.tableNm = tableNm;
	}
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public int getpStep() {
		return pStep;
	}
	public void setpStep(int pStep) {
		this.pStep = pStep;
	}
	public int getpLevel() {
		return pLevel;
	}
	public void setpLevel(int pLevel) {
		this.pLevel = pLevel;
	}
	public String getAc() {
		return ac;
	}
	public void setAc(String ac) {
		this.ac = ac;
	}
	public String getReplyTxt() {
		return replyTxt;
	}
	public void setReplyTxt(String replyTxt) {
		this.replyTxt = replyTxt;
	}
	public String getBoardGroup() {
		return boardGroup;
	}
	public void setBoardGroup(String boardGroup) {
		this.boardGroup = boardGroup;
	}
	
}
