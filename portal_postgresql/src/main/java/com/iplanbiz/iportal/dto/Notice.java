package com.iplanbiz.iportal.dto;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.iportal.config.WebConfig;



public class Notice{
	public Notice() {        }
	public Notice(int boardNo) { super(); this.boardNo = boardNo; }

	private int boardNo;	// 게시판 NO  
	private String title;	// 제목
	private String userId;	// 작성자ID
	private String name;	// 작성자명
//	private String email;	
//	private String password;
	private String createDate;	//작성일자
	private String content;	// 글내용
	private int hitCount;	// 조회수
	private String informYn;
	private String popupYn;
	private String beginDay;
	private String endDay;
	private CommonsMultipartFile[] file;	//파일
	private List<File> files;
	private int informNum;
	
	private String startDay1;
	private String endDay1;
	private String select;
	private String srcTxt;
	
	public int getBoardNo() {
		return boardNo;
	}
	public void setBoardNo(int boardNo) {
		this.boardNo = boardNo;
	}	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public int getHitCount() {
		return hitCount;
	}
	public void setHitCount(int hitCount) {
		this.hitCount = hitCount;
	}
	public String getInformYn() {
		return informYn;
	}
	public void setInformYn(String informYn) {
		this.informYn = informYn;
	}
	public String getPopupYn() {
		return popupYn;
	}
	public void setPopupYn(String popupYn) {
		this.popupYn = popupYn;
	}
	public String getBeginDay() {
		return beginDay;
	}
	public void setBeginDay(String beginDay) {
		this.beginDay = beginDay;
	}
	public String getEndDay() {
		return endDay;
	}
	public void setEndDay(String endDay) {
		this.endDay = endDay;
	}
	public CommonsMultipartFile[] getFile() {
		return file;
	}
	public void setFile(CommonsMultipartFile[] file) {
		this.file = file;
	}
	public List<File> getFiles() {
		return files;
	}
	public void setFiles(List<File> files) {
		this.files = files;
	}
	public int getInformNum() {
		return informNum;
	}
	public void setInformNum(int informNum) {
		this.informNum = informNum;
	}
	public String getStartDay1() {
		return startDay1;
	}
	public void setStartDay1(String startDay1) {
		this.startDay1 = startDay1;
	}
	public String getEndDay1() {
		return endDay1;
	}
	public void setEndDay1(String endDay1) {
		this.endDay1 = endDay1;
	}
	public String getSelect() {
		return select;
	}
	public void setSelect(String select) {
		this.select = select;
	}
	public String getSrcTxt() {
		return srcTxt;
	}
	public void setSrcTxt(String srcTxt) {
		this.srcTxt = srcTxt;
	}
	@Override
	public String toString() {
		return "Notice [boardNo=" + boardNo + ", title=" + title + ", userId="
				+ userId + ", name=" + name + ", createDate=" + createDate
				+ ", content=" + content + ", hitCount=" + hitCount
				+ ", informYn=" + informYn + ", popupYn=" + popupYn
				+ ", beginDay=" + beginDay + ", endDay=" + endDay + ", file="
				+ Arrays.toString(file) + ", files=" + files + "]";
	}
	

}
