package com.iplanbiz.iportal.dto.ktoto;

import java.util.HashMap;
import java.util.List;

import org.springframework.web.multipart.commons.CommonsMultipartFile;


public class KtotoNotice {
	public KtotoNotice() {        }
//	public KtotoNotice(String boardType, int boardNo) { 
//		super();
//		this.boardType = boardType;
//		this.boardNo = boardNo; 
//	}
//	
//	public KtotoNotice(String boardType, int boardNo,String yyyy) { 
//		super();
//		this.boardType = boardType;
//		this.boardNo = boardNo; 
//		this.yyyy = yyyy; 
//	}
	private String boardNO;
	private String title;
	private String popup;
	private String informYn;
	private String userID;
	private String userName;
	private String beginDay;
	private String endDay;
	private String context;
	//	private String division1;
	private CommonsMultipartFile[] file;	//파일
	private List<HashMap<String,String>> files;
	private String[] fkey;
	private String[] type;
	private String[] checkfkey;
	
	
	
	
	
	
	
	public String[] getCheckfkey() {
		return checkfkey;
	}
	public void setCheckfkey(String[] checkfkey) {
		this.checkfkey = checkfkey;
	}
	public String[] getFkey() {
		return fkey;
	}
	public void setFkey(String[] fkey) {
		this.fkey = fkey;
	}
	public String[] getType() {
		return type;
	}
	public void setType(String[] type) {
		this.type = type;
	}
	public String getBoardNO() {
		return boardNO;
	}
	public void setBoardNO(String boardNO) {
		this.boardNO = boardNO;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPopup() {
		return popup;
	}
	public void setPopup(String popup) {
		this.popup = popup;
	}
	public String getInformYn() {
		return informYn;
	}
	public void setInformYn(String informYn) {
		this.informYn = informYn;
	}
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getBeginDay() {
		return beginDay;
	}
	public void setBeginDay(String beginDay) {
		this.beginDay = beginDay;
	}
	public String getEndDay() {
		return endDay;
	}
	public void setEndDay(String endDay) {
		this.endDay = endDay;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public CommonsMultipartFile[] getFile() {
		return file;
	}
	public void setFile(CommonsMultipartFile[] file) {
		this.file = file;
	}
	public List<HashMap<String, String>> getFiles() {
		return files;
	}
	public void setFiles(List<HashMap<String, String>> files) {
		this.files = files;
	}
	
	
	
}
