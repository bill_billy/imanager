package com.iplanbiz.iportal.dto;


public class Procedure {
	
	
	private String ac;
	private String cdprocedure;
	private String cdprocedureNm;
	private String bigo;
	private String orgProcedure;
	private String spName;
	private String yyyy;
	private String dbGubn;
	private String dbUser;
	private String mm;
	private String sText;
	private String idx;
	
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getDbGubn() {
		return dbGubn;
	}
	public void setDbGubn(String dbGubn) {
		this.dbGubn = dbGubn;
	}
	public String getDbUser() {
		return dbUser;
	}
	public void setDbUser(String dbUser) {
		this.dbUser = dbUser;
	}
	public String getsText() {
		return sText;
	}
	public void setsText(String sText) {
		this.sText = sText;
	}
	public String getSpName() {
		return spName;
	}
	public void setSpName(String spName) {
		this.spName = spName;
	}
	public String getYyyy() {
		return yyyy;
	}
	public void setYyyy(String yyyy) {
		this.yyyy = yyyy;
	}
	public String getMm() {
		return mm;
	}
	public void setMm(String mm) {
		this.mm = mm;
	}
	public String getOrgProcedure() {
		return orgProcedure;
	}
	public void setOrgProcedure(String orgProcedure) {
		this.orgProcedure = orgProcedure;
	}
	public String getAc() {
		return ac;
	}
	public void setAc(String ac) {
		this.ac = ac;
	}
	public String getCdprocedure() {
		return cdprocedure;
	}
	public void setCdprocedure(String cdprocedure) {
		this.cdprocedure = cdprocedure;
	}
	public String getCdprocedureNm() {
		return cdprocedureNm;
	}
	public void setCdprocedureNm(String cdprocedureNm) {
		this.cdprocedureNm = cdprocedureNm;
	}
	public String getBigo() {
		return bigo;
	}
	public void setBigo(String bigo) {
		this.bigo = bigo;
	}
		
}