package com.iplanbiz.iportal.dto;


public class Menu {
	
	private String btmCID;
	private String btmCName;
	private String btmPID;
	private String btmSortOrder;
	private String btmUseYn;
	private String btmProgmID;
	private String menuType;
	private String btmCommonType;
	private String btmSourceOwner;
	private String btmCLink;
	private String btmLocale;
	private String saveCname;
	private String saveLocale;
	
	private String arrayCID;
	private String arrayPID;
	private String arraySortOrder;
	private String arrayUseYn;
	private String arrayProgID;
	private String arrayMenuType;
	private String arrayMenuOpenType;
	private String arrayClink;
	private String arraySourceOwner;
	
	
	
	
	
	
	
	public String getSaveCname() {
		return saveCname;
	}
	public void setSaveCname(String saveCname) {
		this.saveCname = saveCname;
	}
	public String getSaveLocale() {
		return saveLocale;
	}
	public void setSaveLocale(String saveLocale) {
		this.saveLocale = saveLocale;
	}
	public String getArraySourceOwner() {
		return arraySourceOwner;
	}
	public void setArraySourceOwner(String arraySourceOwner) {
		this.arraySourceOwner = arraySourceOwner;
	}
	public String getArrayCID() {
		return arrayCID;
	}
	public void setArrayCID(String arrayCID) {
		this.arrayCID = arrayCID;
	}
	public String getArrayPID() {
		return arrayPID;
	}
	public void setArrayPID(String arrayPID) {
		this.arrayPID = arrayPID;
	}
	public String getArraySortOrder() {
		return arraySortOrder;
	}
	public void setArraySortOrder(String arraySortOrder) {
		this.arraySortOrder = arraySortOrder;
	}
	public String getArrayUseYn() {
		return arrayUseYn;
	}
	public void setArrayUseYn(String arrayUseYn) {
		this.arrayUseYn = arrayUseYn;
	}
	public String getArrayProgID() {
		return arrayProgID;
	}
	public void setArrayProgID(String arrayProgID) {
		this.arrayProgID = arrayProgID;
	}
	public String getArrayMenuType() {
		return arrayMenuType;
	}
	public void setArrayMenuType(String arrayMenuType) {
		this.arrayMenuType = arrayMenuType;
	}
	public String getArrayMenuOpenType() {
		return arrayMenuOpenType;
	}
	public void setArrayMenuOpenType(String arrayMenuOpenType) {
		this.arrayMenuOpenType = arrayMenuOpenType;
	}
	public String getArrayClink() {
		return arrayClink;
	}
	public void setArrayClink(String arrayClink) {
		this.arrayClink = arrayClink;
	}
	public String getBtmLocale() {
		return btmLocale;
	}
	public void setBtmLocale(String btmLocale) {
		this.btmLocale = btmLocale;
	}
	public String getBtmCID() {
		return btmCID;
	}
	public void setBtmCID(String btmCID) {
		this.btmCID = btmCID;
	}
	public String getBtmCName() {
		return btmCName;
	}
	public void setBtmCName(String btmCName) {
		this.btmCName = btmCName;
	}
	public String getBtmPID() {
		return btmPID;
	}
	public void setBtmPID(String btmPID) {
		this.btmPID = btmPID;
	}
	public String getBtmSortOrder() {
		return btmSortOrder;
	}
	public void setBtmSortOrder(String btmSortOrder) {
		this.btmSortOrder = btmSortOrder;
	}
	public String getBtmUseYn() {
		return btmUseYn;
	}
	public void setBtmUseYn(String btmUseYn) {
		this.btmUseYn = btmUseYn;
	}
	public String getBtmProgmID() {
		return btmProgmID;
	}
	public void setBtmProgmID(String btmProgmID) {
		this.btmProgmID = btmProgmID;
	}
	public String getMenuType() {
		return menuType;
	}
	public void setMenuType(String menuType) {
		this.menuType = menuType;
	}
	public String getBtmCommonType() {
		return btmCommonType;
	}
	public void setBtmCommonType(String btmCommonType) {
		this.btmCommonType = btmCommonType;
	}
	public String getBtmSourceOwner() {
		return btmSourceOwner;
	}
	public void setBtmSourceOwner(String btmSourceOwner) {
		this.btmSourceOwner = btmSourceOwner;
	}
	public String getBtmCLink() {
		return btmCLink;
	}
	public void setBtmCLink(String btmCLink) {
		this.btmCLink = btmCLink;
	}
}