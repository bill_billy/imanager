package com.iplanbiz.iportal.dto;


public class ExcelPgm {
	private String acctId;
	private String division;
	
	private String pgmId;
	private String oldPgmId;
	private String pgmNm;
	private String tableNm;
	private Integer sortOrder;
	private String udc1;
	private String udc2;
	private String udc3;
//
	private String udc4;
	private String udc5;
	private String udc6;
	private String udc7;
	private String udc8;
	private String udc9;
//	
	private String insertDat;
	private String insertEmp;
	private String updateDat;
	private String updateEmp;
	public String getDownSql() {
		return downSql;
	}
	public void setDownSql(String downSql) {
		this.downSql = downSql;
	}
	public String getUpSql() {
		return upSql;
	}
	public void setUpSql(String upSql) {
		this.upSql = upSql;
	}
	public String getDelSql() {
		return delSql;
	}
	public void setDelSql(String delSql) {
		this.delSql = delSql;
	}
	private String downSql;
	private String upSql;
	private String delSql;
 
    
    
    
	private String dataGbn;
	
	public String getAcctId() {
		return acctId;
	}
	public void setAcctId(String acctId) {
		this.acctId = acctId;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getPgmId() {
		return pgmId;
	}
	public void setPgmId(String pgmId) {
		this.pgmId = pgmId;
	}
	public String getPgmNm() {
		return pgmNm;
	}
	public String getOldPgmId() {
		return oldPgmId;
	}
	public void setOldPgmId(String oldPgmId) {
		this.oldPgmId = oldPgmId;
	}
	public void setPgmNm(String pgmNm) {
		this.pgmNm = pgmNm;
	}
	public String getTableNm() {
		return tableNm;
	}
	public void setTableNm(String tableNm) {
		this.tableNm = tableNm;
	}
	public Integer getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	public String getUdc1() {
		return udc1;
	}
	public void setUdc1(String udc1) {
		this.udc1 = udc1;
	}
	public String getUdc2() {
		return udc2;
	}
	public void setUdc2(String udc2) {
		this.udc2 = udc2;
	}
	public String getUdc3() {
		return udc3;
	}
	public void setUdc3(String udc3) {
		this.udc3 = udc3;
	}
	public String getInsertDat() {
		return insertDat;
	}
	public void setInsertDat(String insertDat) {
		this.insertDat = insertDat;
	}
	public String getInsertEmp() {
		return insertEmp;
	}
	public void setInsertEmp(String insertEmp) {
		this.insertEmp = insertEmp;
	}
	public String getUpdateDat() {
		return updateDat;
	}
	public void setUpdateDat(String updateDat) {
		this.updateDat = updateDat;
	}
	public String getUpdateEmp() {
		return updateEmp;
	}
	public void setUpdateEmp(String updateEmp) {
		this.updateEmp = updateEmp;
	}
	public String getUdc4() {
		return udc4;
	}
	public void setUdc4(String udc4) {
		this.udc4 = udc4;
	}
	public String getUdc5() {
		return udc5;
	}
	public void setUdc5(String udc5) {
		this.udc5 = udc5;
	}
	public String getUdc6() {
		return udc6;
	}
	public void setUdc6(String udc6) {
		this.udc6 = udc6;
	}
	public String getUdc7() {
		return udc7;
	}
	public void setUdc7(String udc7) {
		this.udc7 = udc7;
	}
	public String getUdc8() {
		return udc8;
	}
	public void setUdc8(String udc8) {
		this.udc8 = udc8;
	}
	public String getUdc9() {
		return udc9;
	}
	public void setUdc9(String udc9) {
		this.udc9 = udc9;
	}
	public String getDataGbn() {
		return dataGbn;
	}
	public void setDataGbn(String dataGbn) {
		this.dataGbn = dataGbn;
	}
	
	
	
	
	
}