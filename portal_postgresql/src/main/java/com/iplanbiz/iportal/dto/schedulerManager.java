package com.iplanbiz.iportal.dto;


public class schedulerManager {
	private String schID;
	private String schName;
	private String distYn;
	private String useYn;
	private String schStatus;
	private String schCyc;
	private String aType;
	private String aTime;
	private String bDay;
	private String cDate;
	private String cWeek;
	private String cDay;
	private String dMon;
	private String dDate;
	private String dWeek;
	private String dDay;
	private String fromDat;
	private String toDat;
	private String fromTime;
	private String fromMinute;
	
	private String[] jobID;
	private String[] dbKey;
	private String[] jobName;
	private String[] jobObjID;
	private String[] jobTyp;
	private String[] sortOrd;
	
	
	
	
	public String getSchID() {
		return schID;
	}
	public void setSchID(String schID) {
		this.schID = schID;
	}
	public String getSchName() {
		return schName;
	}
	public void setSchName(String schName) {
		this.schName = schName;
	}
	public String getDistYn() {
		return distYn;
	}
	public void setDistYn(String distYn) {
		this.distYn = distYn;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getSchStatus() {
		return schStatus;
	}
	public void setSchStatus(String schStatus) {
		this.schStatus = schStatus;
	}
	public String getSchCyc() {
		return schCyc;
	}
	public void setSchCyc(String schCyc) {
		this.schCyc = schCyc;
	}
	public String getaType() {
		return aType;
	}
	public void setaType(String aType) {
		this.aType = aType;
	}
	public String getaTime() {
		return aTime;
	}
	public void setaTime(String aTime) {
		this.aTime = aTime;
	}
	public String getbDay() {
		return bDay;
	}
	public void setbDay(String bDay) {
		this.bDay = bDay;
	}
	public String getcDate() {
		return cDate;
	}
	public void setcDate(String cDate) {
		this.cDate = cDate;
	}
	public String getcWeek() {
		return cWeek;
	}
	public void setcWeek(String cWeek) {
		this.cWeek = cWeek;
	}
	public String getcDay() {
		return cDay;
	}
	public void setcDay(String cDay) {
		this.cDay = cDay;
	}
	public String getdMon() {
		return dMon;
	}
	public void setdMon(String dMon) {
		this.dMon = dMon;
	}
	public String getdDate() {
		return dDate;
	}
	public void setdDate(String dDate) {
		this.dDate = dDate;
	}
	public String getdWeek() {
		return dWeek;
	}
	public void setdWeek(String dWeek) {
		this.dWeek = dWeek;
	}
	public String getdDay() {
		return dDay;
	}
	public void setdDay(String dDay) {
		this.dDay = dDay;
	}
	public String getFromDat() {
		return fromDat;
	}
	public void setFromDat(String fromDat) {
		this.fromDat = fromDat;
	}
	public String getToDat() {
		return toDat;
	}
	public void setToDat(String toDat) {
		this.toDat = toDat;
	}
	public String getFromTime() {
		return fromTime;
	}
	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}
	public String getFromMinute() {
		return fromMinute;
	}
	public void setFromMinute(String fromMinute) {
		this.fromMinute = fromMinute;
	}
	public String[] getJobID() {
		return jobID;
	}
	public void setJobID(String[] jobID) {
		this.jobID = jobID;
	}
	public String[] getJobName() {
		return jobName;
	}
	public void setJobName(String[] jobName) {
		this.jobName = jobName;
	}
	public String[] getJobObjID() {
		return jobObjID;
	}
	public void setJobObjID(String[] jobObjID) {
		this.jobObjID = jobObjID;
	}
	public String[] getJobTyp() {
		return jobTyp;
	}
	public void setJobTyp(String[] jobTyp) {
		this.jobTyp = jobTyp;
	}
	public String[] getSortOrd() {
		return sortOrd;
	}
	public void setSortOrd(String[] sortOrd) {
		this.sortOrd = sortOrd;
	}
	public String[] getDbKey() {
		return dbKey;
	}
	public void setDbKey(String[] dbKey) {
		this.dbKey = dbKey;
	}
	
}
