package com.iplanbiz.iportal.dto;


public class DashboardSlideManager {
	
	private String dash_grp_id;
	private String[] slideIDX;
	private String[] pathIDX;
	private String[] sortOrd;
	private String[] interval;
	private String[] eftType;
	private String[] applyMonthValue;
	
	public String getDash_grp_id() {
		return dash_grp_id;
	}
	public void setDash_grp_id(String dash_grp_id) {
		this.dash_grp_id = dash_grp_id;
	}
	
	
	
	
	public String[] getSlideIDX() {
		return slideIDX;
	}
	public void setSlideIDX(String[] slideIDX) {
		this.slideIDX = slideIDX;
	}
	public String[] getPathIDX() {
		return pathIDX;
	}
	public void setPathIDX(String[] pathIDX) {
		this.pathIDX = pathIDX;
	}
	public String[] getSortOrd() {
		return sortOrd;
	}
	public void setSortOrd(String[] sortOrd) {
		this.sortOrd = sortOrd;
	}
	public String[] getInterval() {
		return interval;
	}
	public void setInterval(String[] interval) {
		this.interval = interval;
	}
	public String[] getEftType() {
		return eftType;
	}
	public void setEftType(String[] eftType) {
		this.eftType = eftType;
	}
	public String[] getApplyMonthValue() {
		return applyMonthValue;
	}
	public void setApplyMonthValue(String[] applyMonthValue) {
		this.applyMonthValue = applyMonthValue;
	}
	
}
