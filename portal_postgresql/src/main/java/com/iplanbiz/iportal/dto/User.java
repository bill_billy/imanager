package com.iplanbiz.iportal.dto;


public class User {
	
	private String userID;
	private String passwd;   
	private String userType;
	private String userName;
	private String userLocale;
	private String orgComlCod;
	private String comlCod;  
	private String comlNm;
	private String sortOrder;
	private String bigo;
	private String useYn;
	
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" + userID);
		this.userID = userID;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserLocale() {
		return userLocale;
	}
	public void setUserLocale(String userLocale) {
		this.userLocale = userLocale;
	}
	public String getOrgComlCod() {
		return orgComlCod;
	}
	public String getComlCod() {
		return comlCod;
	}
	public String getComlNm() {
		return comlNm;
	}
	public String getSortOrder() {
		return sortOrder;
	}
	public String getBigo() {
		return bigo;
	}
	public String getUseYn() {
		return useYn;
	}
}