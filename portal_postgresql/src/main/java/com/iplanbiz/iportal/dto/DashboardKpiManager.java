package com.iplanbiz.iportal.dto;


public class DashboardKpiManager {
	private String kpiID;
	private String kpiGbn;
	private String kpiName;
	private String sol;
	private String bigo;
	private String sql;
	private String[] mon;
	private String startDat;
	private String endDat;
	private String measCycle;
	private String sortOrder;
	private String useUnit;
	private String colSystem;
	private String kpiDir;
	private String useYn;
	private String ownerUser;
	private String division;
	private String sDate;
	private String eDate;
	private String kpiUdc1;
	private String kpiUdc2;
	private String kpiUdc3;
	private String kpiUdc4;
	private String kpiUdc5;
	
	
	public String getBigo() {
		return bigo;
	}
	public void setBigo(String bigo) {
		this.bigo = bigo;
	}
	public String getKpiID() {
		return kpiID;
	}
	public void setKpiID(String kpiID) {
		this.kpiID = kpiID;
	}
	public String getKpiGbn() {
		return kpiGbn;
	}
	public void setKpiGbn(String kpiGbn) {
		this.kpiGbn = kpiGbn;
	}
	public String getKpiName() {
		return kpiName;
	}
	public void setKpiName(String kpiName) {
		this.kpiName = kpiName;
	}
	public String getSol() {
		return sol;
	}
	public void setSol(String sol) {
		this.sol = sol;
	}
	public String getSql() {
		return sql;
	}
	public void setSql(String sql) {
		this.sql = sql;
	}
	public String[] getMon() {
		return mon;
	}
	public void setMon(String[] mon) {
		this.mon = mon;
	}
	public String getStartDat() {
		return startDat;
	}
	public void setStartDat(String startDat) {
		this.startDat = startDat;
	}
	public String getEndDat() {
		return endDat;
	}
	public void setEndDat(String endDat) {
		this.endDat = endDat;
	}
	public String getMeasCycle() {
		return measCycle;
	}
	public void setMeasCycle(String measCycle) {
		this.measCycle = measCycle;
	}
	public String getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
	public String getUseUnit() {
		return useUnit;
	}
	public void setUseUnit(String useUnit) {
		this.useUnit = useUnit;
	}
	public String getColSystem() {
		return colSystem;
	}
	public void setColSystem(String colSystem) {
		this.colSystem = colSystem;
	}
	public String getKpiDir() {
		return kpiDir;
	}
	public void setKpiDir(String kpiDir) {
		this.kpiDir = kpiDir;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getOwnerUser() {
		return ownerUser;
	}
	public void setOwnerUser(String ownerUser) {
		this.ownerUser = ownerUser;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getsDate() {
		return sDate;
	}
	public void setsDate(String sDate) {
		this.sDate = sDate;
	}
	public String geteDate() {
		return eDate;
	}
	public void seteDate(String eDate) {
		this.eDate = eDate;
	}
	public String getKpiUdc1() {
		return kpiUdc1;
	}
	public void setKpiUdc1(String kpiUdc1) {
		this.kpiUdc1 = kpiUdc1;
	}
	public String getKpiUdc2() {
		return kpiUdc2;
	}
	public void setKpiUdc2(String kpiUdc2) {
		this.kpiUdc2 = kpiUdc2;
	}
	public String getKpiUdc3() {
		return kpiUdc3;
	}
	public void setKpiUdc3(String kpiUdc3) {
		this.kpiUdc3 = kpiUdc3;
	}
	public String getKpiUdc4() {
		return kpiUdc4;
	}
	public void setKpiUdc4(String kpiUdc4) {
		this.kpiUdc4 = kpiUdc4;
	}
	public String getKpiUdc5() {
		return kpiUdc5;
	}
	public void setKpiUdc5(String kpiUdc5) {
		this.kpiUdc5 = kpiUdc5;
	}
}
