/**
 * Copyright(c) 2012 IPLANBIZ All Rights Reserved
 */
package com.iplanbiz.iportal.listener;

import java.util.Calendar;
import java.util.Enumeration;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.comm.session.SessionManager;

/**
 * @author : woon sik Shin(sky3473@iplanbiz.co.kr or gmail.com)
 * @date   : 2012. 11. 16.
 * @desc   : 
 */
public class SessionListener implements HttpSessionListener {

	private Logger logger = LoggerFactory.getLogger(getClass()); 
	
	@Override 
	public void sessionCreated(HttpSessionEvent se) {
		//세션생성시호출 

	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		// 세션만료시호출
		HttpSession session = se.getSession();
		//WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());
		WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(session.getServletContext(),org.springframework.web.servlet.FrameworkServlet.SERVLET_CONTEXT_PREFIX + "SpringDispatcher");
		SessionManager sessionManager = (SessionManager)wac.getBean("sessionManager");
		
		 
		long ltime = session.getLastAccessedTime(); //마지막접속시간
		long ctime = session.getCreationTime(); //로그인 시간
		
		String id = session.getId();
		logger.info(showTime(ltime, 1) +" drop session info : sid=>{}", id);
		logger.info(showTime(ltime, 1) +" drop session info : period log on(" + showTime((ctime-ltime), 2) + ")");
		logger.info(showTime(ltime, 1) +" drop session info : id=>{}", se.getSession().getAttribute("userId"));
		LoginSessionInfo info = (LoginSessionInfo)se.getSession().getAttribute("loginSessionInfo");
		logger.info("info!=null = {}",info!=null);
		if(info!=null){
			logger.info(showTime(ltime, 1) +" drop session info : factory id=>{}", info.getUserId());
			if( info.getUserId()!=null)
				sessionManager.removeLoginSessionInfo(info.getUserId(), info.getAcctID(),session);
		}
		/**
		Enumeration e = se.getSession().getAttributeNames();
		while(e.hasMoreElements()){
			String element = e.nextElement().toString();
			logger.info("{}=>{}",element, se.getSession().getAttribute(element));
		}**/
		
	}
	
	public String showTime(long time, int type){
		Calendar c =Calendar.getInstance();
		c.setTimeInMillis(time);
		
		int year 	= c.get(Calendar.YEAR);
		int month 	= c.get(Calendar.MONTH) + 1;
		int day 	= c.get(Calendar.DAY_OF_MONTH);
		int hour 	= c.get(Calendar.HOUR_OF_DAY);
		int minute 	= c.get(Calendar.MINUTE);
		int second 	= c.get(Calendar.SECOND);
		
		if(type == 1)
			return year+"년" + month + "월" + day + "일" + hour + "시" + minute + "분" + second+ "초 ";
		else 
			return minute + "분" + second+ "초 ";
	}
}

