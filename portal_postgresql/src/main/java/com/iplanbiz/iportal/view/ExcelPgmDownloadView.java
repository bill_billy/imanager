package com.iplanbiz.iportal.view;

import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.iplanbiz.comm.Utils;

/**
 * @author : woon sik Shin(sky3473@iplanbiz.co.kr or gmail.com)
 * @date   : 2012. 10. 12.
 * @desc   : default excel download form
 */
public class ExcelPgmDownloadView extends AbstractExcelView{

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> model, 
			HSSFWorkbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		OutputStream fileOut = response.getOutputStream();
		
		HSSFCellStyle titleStyle = workbook.createCellStyle();
		titleStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		titleStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		titleStyle.setAlignment(CellStyle.ALIGN_CENTER);
		titleStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		titleStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
		titleStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
		titleStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		
		String userAgent = request.getHeader("User-Agent");
		boolean ie = userAgent.indexOf("MSIE") > -1;
		boolean chrom = userAgent.indexOf("Chrome") > -1;
		String fileName = (String) model.get("fileName");  
		
		if(ie || chrom){  
			fileName = URLEncoder.encode(fileName, "utf-8");
		}else{
			fileName = new String(fileName.getBytes("utf-8"));
		}
		
		logger.debug("===>User-Agent : {}", Utils.getString(userAgent, "").replaceAll("[\r\n]",""));
		
		response.setHeader("Content-Disposition", "attachment; filename=\"" + Utils.getString(fileName, "").replaceAll("[\r\n]","") + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");
		
		List<String> columnNm = (List<String>) model.get("columnNm");
		List<HashMap<String, String>> data = (List<HashMap<String, String>>) model.get("data");
		//create a wordsheet
		HSSFSheet sheet = workbook.createSheet("sheet 1");
 
		HSSFRow header = sheet.createRow(0);
		for(int i=0; i < columnNm.size(); i++){
			Cell cell = header.createCell(i);
			cell.setCellValue((String) columnNm.get(i));
			cell.setCellStyle(titleStyle);
		}
		
		for(int i=0; i < data.size(); i++){
			HSSFRow row = sheet.createRow(i+1);
			Map map = (Map) data.get(i);
			
			for(int j=0; j < columnNm.size(); j++){
				row.createCell(j).setCellValue(Utils.getString(String.valueOf(map.get(columnNm.get(j))),""));
			}
		}
	}
}
