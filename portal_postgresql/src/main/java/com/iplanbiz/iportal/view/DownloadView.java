/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.iportal.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.iportal.config.WebConfig;

/**
 * @author : woon sik Shin(sky3473@iplanbiz.co.kr or gmail.com)
 * @date   : 2012. 7. 5.
 * @desc   : 
 */
public class DownloadView extends AbstractView {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public void Download(){                 
		setContentType("application/download; utf-8");             
	}
	
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		String filePath = "";
		String fileOrgName = "";
		String fileSaveName = "";
		
		if("temp".equals(model.get("fileType"))){
			filePath = WebConfig.getProperty("system.upload.SaveDir");
//			filePath = WebConfig.getDefultTempDir();
			fileSaveName = (String) model.get("downloadFile");
			fileOrgName = fileSaveName.split("_")[1];
		}else{
			@SuppressWarnings("unchecked")
			Map<String, Object> fileInfo = (Map<String, Object>) model.get("downloadFile");
			
//			
			filePath = String.valueOf(fileInfo.get("file_path"));
			fileOrgName = String.valueOf(fileInfo.get("file_org_name"));
			fileSaveName = String.valueOf(fileInfo.get("file_save_name")) + "." + String.valueOf(fileInfo.get("file_ext"));
//			
		}
		

		File file = new File(filePath + fileSaveName); 
				
		response.setContentType(getContentType());        
		response.setContentLength((int)file.length());                 
		
		String userAgent = request.getHeader("User-Agent");
		boolean ie = userAgent.indexOf("MSIE") > -1;
		boolean chrom = userAgent.indexOf("Chrome") > -1;
		String fileName = "";                 
		
		logger.debug("--->origin filename : {}",Utils.getString(fileName, "").replaceAll("[\r\n]",""));
		logger.debug("--->URLEncoder.encode(fileName, utf-8) : {}",URLEncoder.encode(Utils.getString(fileName, ""), "utf-8").replaceAll("[\r\n]",""));
		logger.debug("--->new String(fileName.getBytes(MS949) : {}" ,new String(Utils.getString(fileName, "").getBytes("MS949")).replaceAll("[\r\n]",""));
				
		logger.debug("--->new String(fileName.getBytes(utf-8)) : {} " , new String(Utils.getString(fileName, "").getBytes("utf-8")).replaceAll("[\r\n]",""));
		logger.debug("--->!new String(fileName.getBytes(KSC5601), 8859_1) : {} " , new String(Utils.getString(fileName, "").getBytes("KSC5601"), "8859_1").replaceAll("[\r\n]",""));
		fileName = new String(fileOrgName.getBytes("KSC5601"), "8859_1"); 
		
		
		
		logger.debug("===>User-Agent : {}", Utils.getString(userAgent, "").replaceAll("[\r\n]",""));
		logger.info("===>>>>file.getPath() : {}", Utils.getString(filePath, "").replaceAll("[\r\n]",""));
		logger.info("===>>>>file.getName() : {}", Utils.getString(fileOrgName, "").replaceAll("[\r\n]",""));
		
		String h1 =  "attachment; filename=\"" +Utils.getString(fileName, "").replaceAll("[\r\n]","")+ "\";";
		response.setHeader("Content-Disposition", h1);                 
		response.setHeader("Content-Transfer-Encoding", "binary");                 
		
		OutputStream out = response.getOutputStream();                 
		FileInputStream fis = null;                 
		
		try {                         
			fis = new FileInputStream(file);                         
			FileCopyUtils.copy(fis, out);
			logger.info("===>>>>FileDownload Complete : {}", Utils.getString(fileName, "").replaceAll("[\r\n]",""));
		}catch(Exception e){                         
			logger.error("===>>>>FileDownload Error : {}", e);
			}finally{ 
				if(fis != null){try{fis.close();}catch(Exception e){}
			}                     
		}                 
		out.flush();             
	}

}
