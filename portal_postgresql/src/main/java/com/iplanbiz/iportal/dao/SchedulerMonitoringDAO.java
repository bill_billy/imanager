package com.iplanbiz.iportal.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.butler.Proxy;
import com.iplanbiz.core.io.butler.ProxyPool;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;

@Repository
public class SchedulerMonitoringDAO extends SqlSessionDaoSupport {
	
 
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Resource(name="proxyPool") ProxyPool proxyPool;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public JSONArray getSchedulerMonitorData ( String jobStatus ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("SCH_STATUS"									, jobStatus);
		
		try{
			logger.info("Get Scheduler Monitor Data Start");
			list = getSqlSession().selectList("getSchedulerMonitorData", map);
			logger.info("Get Scheduler Monitor Data End");
			
			if(list.size()>0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();

					value.put("SCH_ID"								, list.get(i).get("sch_id"));
					value.put("SCH_NM"								, list.get(i).get("sch_nm"));
					value.put("START_DAT"							, list.get(i).get("start_dat"));
					value.put("END_DAT"							, list.get(i).get("end_dat"));
					value.put("LEAD_TIME"							, list.get(i).get("lead_time"));
					value.put("RESULT"								, list.get(i).get("result"));
					value.put("SCH_CYC"							, list.get(i).get("sch_cyc"));
					value.put("RESULT_NM"						, list.get(i).get("result_nm"));
					value.put("SCH_STATUS"						, list.get(i).get("sch_status"));
					value.put("SCH_SNM"							, list.get(i).get("sch_snm"));
					value.put("SUCESS"								, list.get(i).get("sucess"));
					value.put("ERROR"								, list.get(i).get("error"));
					value.put("TOTAL"								, list.get(i).get("total"));
					logger.info("Value = " + value.size());
					returnValue.add(value);
				}
			}
			logger.info("returnValue Size = " + returnValue.size());
		}catch(Exception e){
			logger.error("ERROR > Get Scheduler Monitor Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getSchedulerMonitorDetailData ( String schID, String resultGubn ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		         
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("SCH_ID"											, Integer.valueOf(schID));
		map.put("RESULT"											, resultGubn);
		
		try{
			logger.info("Get Scheduler Monitor Detail Data Start");
			list = getSqlSession().selectList("getSchedulerMonitorDetailData", map);
			logger.info("Get Scheduler Monitor Detail Data End");
			
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("IDX"									, list.get(i).get("idx"));
					value.put("SCH_ID"								, list.get(i).get("sch_id"));
					value.put("SCH_NM"								, list.get(i).get("sch_nm"));
					value.put("START_DAT"							, list.get(i).get("start_dat"));
					value.put("END_DAT"							    , list.get(i).get("end_dat"));
					value.put("LEAD_TIME"							, list.get(i).get("lead_time"));
					value.put("CMD_TYPE"							, list.get(i).get("cmd_type"));
					value.put("RESULT"								, list.get(i).get("result"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("ERROR > Get Scheduler Monitor Detail Data Error :{} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public AjaxMessage runJob ( String schID ) throws Exception {
		/*JSONObject value = new JSONObject();
		value.put("sch_id"	, schID);*/
		Proxy proxy = proxyPool.createProxy();
		proxy.start();
		AjaxMessage msg = new AjaxMessage();
		try{
			
			logger.info("Run JOB Start");
			//String returnValue = "sucess";
			JSONObject param = new JSONObject();
			param.put("JOBGROUP"						, "etl");
			param.put("JOBNAME"							, schID + "_" + loginSessionInfoFactory.getObject().getAcctID());
			param.put("REQUEST_USER_ID"				, loginSessionInfoFactory.getObject().getUserId());
			
			msg = proxy.requestRule("scheduler", "RunImmediately", param);
			System.out.println("runjobMsg:::::"+msg.getReturnText());
			logger.info("Run JOB End");
			//value.put("result", returnValue);
			
		}catch(Exception e){
			logger.error("ERROR > Run JOB Error : {} " ,e);
			throw e;
		}finally{
			proxy.stop();
		}
		return msg;
	}
	public JSONArray getSchedulerDetailError ( String schID, String idx ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		map.put("SCH_ID"									, Integer.valueOf(schID));
		map.put("IDX"										, Integer.valueOf(idx));
		
		try {
			logger.info("Get Scheduler Detail Error Data Start");
			listValue = getSqlSession().selectList("getSchedulerDetailError", map);
			logger.info("Get Scheduler Detail Error Data End");
			if(listValue.size() > 0) {
				for(int i=0; i<listValue.size(); i++) {
					JSONObject putValue = new JSONObject();
					putValue.put("IDX"						, listValue.get(i).get("idx"));
					putValue.put("SCH_ID"					, listValue.get(i).get("sch_id"));
					putValue.put("ERR_IDX"					, listValue.get(i).get("err_idx"));
					putValue.put("JOB_NM"					, listValue.get(i).get("job_nm"));
					putValue.put("JOB_TYP"					, listValue.get(i).get("job_typ"));
					putValue.put("JOB_TYP_NM"				, listValue.get(i).get("job_typ_nm"));
					putValue.put("START_DAT"				, listValue.get(i).get("start_dat"));
					putValue.put("TAB_NM"					, listValue.get(i).get("tab_nm"));
					putValue.put("ERR_MESSAGE"				, listValue.get(i).get("err_message"));
					putValue.put("ERR_SQL"					, listValue.get(i).get("err_sql"));
					returnValue.add(putValue);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR : Get Scheduler Detail Error Data Error : {}", e);
			throw e;
		}
		return returnValue;
	}
	public JSONArray getSchedulerSqlDetail ( String errIDX, String idx ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		map.put("ERR_IDX"									, Integer.valueOf(errIDX));
		map.put("IDX"										, Integer.valueOf(idx));
		
		try {
			logger.info("Get Scheduler Sql Detail Data Start");
			listValue = getSqlSession().selectList("getSchedulerSqlDetail",map);
			logger.info("Get Scheduler Sql Detail Data End");
			if(listValue.size() > 0) {
				for(int i=0;i<listValue.size();i++) {
					JSONObject putValue = new JSONObject();
					putValue.put("IDX"						, listValue.get(i).get("idx"));
					putValue.put("ERR_IDX"					, listValue.get(i).get("err_idx"));
					putValue.put("SCH_ID"					, listValue.get(i).get("sch_id"));
					putValue.put("JOB_ID"					, listValue.get(i).get("job_id"));
					putValue.put("ERR_SQL"					, listValue.get(i).get("err_sql"));
					returnValue.add(putValue);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR : Get Scheduler Sql Detail Data Error : {}" ,e);
			throw e;
		}
		return returnValue;
	}
}
