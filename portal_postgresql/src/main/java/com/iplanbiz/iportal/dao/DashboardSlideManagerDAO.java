package com.iplanbiz.iportal.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dto.DashboardSlideManager;

@Repository
public class DashboardSlideManagerDAO extends SqlSessionDaoSupport {
	
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Autowired DBMSDAO dbmsDAO;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public JSONArray getDashboardSlideManagerList () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		try{
			logger.info("Get Dash board Slide Manager List Data Start");
			list = getSqlSession().selectList("getDashboardSlideManagerList");
			logger.info("Get Dash board Slide Manager List Data End");
			if(list.size()>0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("SLIDE_IDX"											, list.get(i).get("slide_idx"));
					value.put("PATH_IDX"											, list.get(i).get("path_idx"));
					value.put("INTERVAL"											, list.get(i).get("interval"));
					value.put("EFT_TYPE"											, list.get(i).get("eft_type"));
					value.put("APPLY_MONTH"									, list.get(i).get("apply_month"));
					value.put("SORT_ORD"											, list.get(i).get("sort_ord"));
					value.put("USE_YN"												, list.get(i).get("use_yn"));
					value.put("FULLPATH"											, list.get(i).get("fullpath"));
					value.put("PATH_DESC"										, list.get(i).get("path_desc"));
					value.put("BIGO"													, list.get(i).get("bigo"));
					value.put("PATH_USE_YN"										, list.get(i).get("path_use_yn"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Dash board Slide Manager List Data Error : {} ", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	
	public JSONObject getDashboardMaxSlideIDX () throws Exception {
		JSONObject returnValue = new JSONObject();
		List<HashMap<String, Object>> list = null;
		try{
			logger.info("Get Dashboard Max Slide IDX Start");
			list = getSqlSession().selectList("getDashboardMaxSlideIDX");
			logger.info("Get Dashboard Max Slide IDX End");
			if(list.size()>0){
				returnValue.put("SLIDE_IDX"								, list.get(0).get("slide_idx"));
			}
		}catch(Exception e){
			logger.error("Get Dashboard Max Slide IDX Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	
	/*public int insertDashboardSlide (String dash_grp_id, String path_idx, String sortord, 
			 						 String interval, String eft_type, String apply_month) throws Exception {
	
		int returnValue = 0;

		HashMap<String, String> map = new HashMap<String, String>();
		map.put("dash_grp_id", dash_grp_id);
		map.put("path_idx", path_idx);
		map.put("sort_ord", sortord);
		map.put("interval", interval);
		map.put("eft_type", eft_type);
		map.put("apply_month", apply_month);
		
		
		try{
			getSqlSession().insert("insertSlide");
			returnValue=0;
		}catch(Exception e){
			returnValue = 1;
		}
		return returnValue;
		
		
	}*/
		
	
	
	
	
	//DTO(slide)로 넘겨주는 방식
	public int insertDashboardSlide ( DashboardSlideManager slide) throws Exception {
		
		int returnValue = 0;
		//max
		
		String maxIdx="1";	
		List<HashMap<String, Object>> list = null;
		try{
			list = getSqlSession().selectList("getDashboardMaxSlideIDX");
			if(list.size()>0){
				maxIdx=list.get(0).get("slide_idx").toString();
			}
		}catch(Exception e){
			logger.error("Get Dashboard Max Slide IDX Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		System.out.println("max::"+maxIdx);
		
		
		
		
		ArrayList<LinkedHashMap<String, Object>> arrayIDFE0003 = new ArrayList<LinkedHashMap<String,Object>>();
		/*logger.info("slideUIDx:"+slide.getPathIDX()[0]);*/
		logger.info(slide.getDash_grp_id()+"ㅎㅎㅎ:");
		if(slide.getDash_grp_id()!="") { //그룹명이 없을때 저장안됨
		if(slide.getPathIDX()!=null) {
		for(int i=0;i<slide.getPathIDX().length;i++){
			
			TableObject idfe0003 = new TableObject();
			
			idfe0003.put("dash_grp_id"								,	 slide.getDash_grp_id());
			idfe0003.put("slide_idx"										, Integer.valueOf(maxIdx)+i+1);
			idfe0003.put("path_idx"										, Integer.valueOf(slide.getPathIDX()[i]));
			idfe0003.put("interval"										, Integer.valueOf(slide.getInterval()[i]));
			idfe0003.put("eft_type"										, slide.getEftType()[i]);
			idfe0003.put("apply_month"								, slide.getApplyMonthValue()[i]);
			idfe0003.put("sort_ord"										, Integer.valueOf(slide.getSortOrd()[i]));
			idfe0003.put("insert_emp"									, loginSessionInfoFactory.getObject().getUserId());
			idfe0003.putCurrentTimeStamp("insert_dat");
			arrayIDFE0003.add(idfe0003);
		}}
		}
		try{
			/*logger.info("Dash board Slide Manager Data Delete Start");
			dbmsDAO.deleteTable("iportal_custom.IDFE0003", new TableCondition());
			logger.info("Dash board Slide Manager Data Delete End");
			logger.info("Dash board Slide Insert Start");*/
			dbmsDAO.insertTable("iportal_custom.IDFE0003",  arrayIDFE0003);
			/*logger.info("Dash board Slide Insert End");*/
			returnValue = 0;
		}catch(Exception e){
			logger.error("Dash board Slide Insert / Update Error : {}", e);
			e.printStackTrace();
			returnValue = 1;
			throw e;
		}
		logger.info("dao타기 후");
		return returnValue;
	}
	
	
	
	
	public JSONArray getDashboardSlidePathList () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		try{
			logger.info("Get Dash Board Path List Data Start");
			list = getSqlSession().selectList("getDashboardSlidePathList");
			logger.info("Get Dash Board Path List Data End");
			if(list.size()>0){
				for(int i=0; i<list.size(); i++){
					JSONObject value = new JSONObject();
					value.put("PATH_IDX"									, list.get(i).get("path_idx"));
					value.put("FULLPATH"									, list.get(i).get("fullpath"));
					value.put("PATH_DESC"								, list.get(i).get("path_desc"));
					value.put("BIGO"											, list.get(i).get("bigo"));
					value.put("COM_UDC1"								, list.get(i).get("com_udc1"));
					value.put("USE_YN"										, list.get(i).get("use_yn"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Dash Board Path List Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getDashboardSlidePopupData (String dash_grp_id) throws Exception {
		JSONArray returnValue = new JSONArray();
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("dash_grp_id", dash_grp_id);
		
		List<HashMap<String ,Object>> list = null;
		try{
			logger.info("Get Dashboard Slide Manager Popup Data Start");
			list = getSqlSession().selectList("getDashboardSlidePopupData", map);
			logger.info("Get Dashboard Slide Manager Popup Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("dash_grp_id"										, list.get(i).get("dash_grp_id"));
					value.put("SLIDE_IDX"										, list.get(i).get("slide_idx"));
					value.put("PATH_IDX"										, list.get(i).get("path_idx"));
					value.put("INTERVAL"										, list.get(i).get("interval"));
					value.put("EFT_TYPE"										, list.get(i).get("eft_type"));
					value.put("APPLY_MONTH"								, list.get(i).get("apply_month"));
					value.put("SORT_ORD"										, list.get(i).get("sort_ord"));
					value.put("USE_YN"											, list.get(i).get("use_yn"));
					value.put("FULLPATH"										, list.get(i).get("fullpath"));
					value.put("PATH_DESC"									, list.get(i).get("path_desc"));
					value.put("BIGO"												, list.get(i).get("bigo"));
					value.put("PATH_USE_YN"									, list.get(i).get("path_use_yn"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Dashboard Slide Manager Popup Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public List<HashMap<String, Object>> getSlideMaster() { 
		List<HashMap<String, Object>> list = null;
		try{ 
			list = getSqlSession().selectList("getSlideMaster"); 
			
		}catch(Exception e){
			logger.error("Get Dashboard Max Slide IDX Error : {} " ,e);
			e.printStackTrace();
		}
		return list;
	}
	
	
	public List<HashMap<String, Object>> getRowSelectView(String dash_grp_id) {
		List<HashMap<String, Object>> list = null;
		try {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("dash_grp_id", dash_grp_id);
			list = getSqlSession().selectList("getRowSelectView", map);
		}catch(Exception e) {
			logger.error("Get Dashboard Max Slide IDX Error : {}" , e);
		}
		return list;
	}
	
	
	public int insertDashInfo(String txtDashGrpName, String txtDashGrpID) {
		int returnValue = 0;
		// TODO Auto-generated method stub
		HashMap<String, String> map = new HashMap<String, String>();
		logger.info(loginSessionInfoFactory.getObject().getAcctID());
		map.put("dashGrpID", txtDashGrpID);
		map.put("DASH_GRP_NM", txtDashGrpName);
		map.put("acct_id", loginSessionInfoFactory.getObject().getAcctID());//세션 acct_id값
		
		try {
			if(txtDashGrpID.equals("")) {
				
				//insert
			getSqlSession().insert("insertDashInfo", map);
			
			returnValue = 0;
			}else {
				//update
			logger.info("dao에서 update쿼리문 타기전");
			getSqlSession().update("updateDashInfo", map);
			logger.info("dao에서 update쿼리문 탄 후");
			returnValue = 1;
			}
		} catch (Exception e) {
			returnValue = 2;
		}
		return returnValue;
	}
	public int deleteDashInfo(String txtDashGrpID, String txtDashGrpName) {
		int returnValue = 0;
		HashMap<String, String> map = new HashMap<String, String>();
		
		map.put("dashGrpID", txtDashGrpID);
		map.put("DASH_GRP_NM", txtDashGrpName);
		
		/*map.put("acct_id", loginSessionInfoFactory.getObject().getAcctID());*/
		
		try {
			logger.info("dao타기전txtDashGrpName="+txtDashGrpName+ "   dashID="+txtDashGrpID);
			getSqlSession().delete("deleteDashInfo", map);
			logger.info("dao타기후txtDashGrpName="+txtDashGrpName+ "   dashID="+txtDashGrpID);
			getSqlSession().delete("deleteSlide", map);
			returnValue = 0;
		}catch(Exception e) {
			returnValue= 1;
		}
		
		return returnValue;
	}

	public List<HashMap<String, String>> getDashGrpList(String dashGrp, String searchDashGrpName) {
		
		List<HashMap<String, String>> list = null;
		try {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("dashGrp", dashGrp);
			map.put("searchDashGrpName", searchDashGrpName);
			list = getSqlSession().selectList("getDashGrpList", map);
			
		}catch(Exception e) {
			logger.error("Get Dashboard Max Slide IDX Error : {} " ,e);
			e.printStackTrace();
		}
		return list;
	}

	public int deleteSlideMaster(String DASH_GRP_ID) {
		int returnValue = 0;
		HashMap<String, String> map = new HashMap<String, String>();
		
		map.put("dash_grp_id", DASH_GRP_ID);
		
		try {
			getSqlSession().delete("deleteSlideMaster",map);
			returnValue=0;
		}catch(Exception e) {
			returnValue= 1;
		}
		return 0;
	}

	public int insertSlideMaster(String dash_grp_id, String slideIDX, String pathIDX, String interval, String eftType,
			String applyMonthValue, String sortOrd) {
		int returnValue = 0;
		HashMap<String, String> map = new HashMap<String, String>();
		
		map.put("dash_grp_id", dash_grp_id);
		map.put("slide_idx", slideIDX);
		map.put("path_idx", pathIDX);
		map.put("interval", interval);
		map.put("eft_type", eftType);
		map.put("apply_month", applyMonthValue);
		map.put("sort_ord", sortOrd);
		
		try {
			getSqlSession().insert("insertSlideMaster",map);
			returnValue=0;
		}catch(Exception e) {
			returnValue=1;
		}
		return 0;
	}

	public List<HashMap<String, Object>> dashIdForm(String dash_grp_id) {
		List<HashMap<String, Object>> list = null;
		try {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("dash_grp_id", dash_grp_id);
			list = getSqlSession().selectList("dashIdForm" , map);
		}catch(Exception e) {
			logger.error("Get Dashboard Max Slide IDX Error : {}" , e);
		}
		return list;
	}

	
	
}
