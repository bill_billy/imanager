package com.iplanbiz.iportal.dao.ktoto;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.taglibs.standard.tag.common.core.RemoveTag;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.system.SystemDAO;
import com.iplanbiz.iportal.dto.File;
import com.iplanbiz.iportal.dto.ktoto.KtotoNotice;
import com.iplanbiz.iportal.service.file.FileService;

@Repository
public class KtotoNoticeDAO extends SqlSessionDaoSupport {
	
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired DBMSDAO dbmsDAO;
	@Autowired FileService fileService;
	@Autowired SystemDAO systemDAO;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public JSONArray getKtotoNoticeList ( String name, String beginStartDat, String beginEndDat, String endStartDat, String endEndDat ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("NAME"											, name);
		map.put("BEGIN_START_DAT"								, beginStartDat);
		map.put("BEGIN_END_DAT"									, beginEndDat);
		map.put("END_START_DAT"									, endStartDat);
		map.put("END_END_DAT"									, endEndDat);
		
		try {
			logger.info("Get Ktoto Notice List Data Start");
			list = getSqlSession().selectList("getKtotoNoticeList", map);
			logger.info("Get Ktoto Notice List Data ENd");
			if(list.size() > 0) {
				for( int i=0; i<list.size(); i++){
					JSONObject value = new JSONObject();
					value.put("BOARDNO"							, list.get(i).get("boardno"));
					value.put("TITLE"							, list.get(i).get("title"));
					value.put("INFORM_YN"						, list.get(i).get("inform_yn"));
					value.put("USERID"							, list.get(i).get("userid"));
					value.put("BEGINDAY"						, list.get(i).get("beginday"));
					value.put("ENDDAY"							, list.get(i).get("endday"));
					value.put("DATE"							, list.get(i).get("date"));
					value.put("NAME"							, list.get(i).get("name"));
					value.put("HITCOUNT"						, list.get(i).get("hitcount"));
					value.put("FILE_CNT"						, list.get(i).get("file_cnt"));
					returnValue.add(value);
				}
			}
		} catch ( Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Ktoto Notice List Data Error : {}", e.getMessage());
			throw e;
		}
		return returnValue;
	}
	public JSONObject getNoticeDetail ( String boardNO ) throws Exception {
		JSONObject returnValue = new JSONObject();
		HashMap<String, Object> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("BOARDNO"										, Integer.valueOf(boardNO));
		
		try {
			logger.info("Get Ktoto Notice Detail Data Start");
			list = getSqlSession().selectOne("getKtotoNoticeDetail", map);
			logger.info("Get Ktoto Notice Detail Data End");
			if(list.size() > 0) {
				returnValue.put("BOARDNO"							, list.get("boardno"));
				returnValue.put("TITLE"								, list.get("title"));
				returnValue.put("USERID"							, list.get("userid"));
				returnValue.put("NAME"								, list.get("name"));
				returnValue.put("CONTENT"							, list.get("content"));
				returnValue.put("BEGINDAY"							, list.get("beginday"));
				returnValue.put("ENDDAY"							, list.get("endday"));
				returnValue.put("POPUP"								, list.get("popup"));
				returnValue.put("INFORM_YN"							, list.get("inform_yn"));
				returnValue.put("CREATEDATE"						, list.get("createdate"));
			}
		} catch( Exception e ) {
			logger.error("ERROR > Get Ktoto Notice Detail Data Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getNoticeDtailFileInfo( String boardNO ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("BOARDNO"										, Integer.valueOf(boardNO));
		
		try {
			logger.info("Get Ktoto Notice Detail File Info Data Start");
			list = getSqlSession().selectList("getNoticeDtailFileInfo", map);
			logger.info("Get Ktoto Notice Detail File Info Data End");
			if(list.size() > 0) {
				for(int i=0;i<list.size();i++) {
					JSONObject value = new JSONObject();
					value.put("BOARD_NO"						, list.get(i).get("board_no"));
					value.put("FILE_ID"							, list.get(i).get("file_id"));
					value.put("FILE_ORG_NAME"					, list.get(i).get("file_org_name"));
					returnValue.add(value);
				}
			}
		} catch ( Exception e ) {
			logger.error("ERROR > Get Ktoto Notice Detail File Info Data Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getKtotoNoticeUserList ( String searchValue ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("SEARCH_VALUE"									, searchValue);
		
		try {
			logger.info("Get Ktoto Notice User List Data Start");
			list = getSqlSession().selectList("getKtotoNoticeUserList", map);
			logger.info("Get Ktoto Notice User List Data End");
			if(list.size() > 0) {
				for(int i=0; i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("EMP_ID"							, list.get(i).get("emp_id"));
					value.put("EMP_NM"							, list.get(i).get("emp_nm"));
					value.put("USER_GRP_NM"						, list.get(i).get("user_grp_nm"));
					value.put("USER_POSITION"					, list.get(i).get("user_position"));
					
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Ktoto Notice User List Data Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getKtotoNoticeEmailList ( String boardNO ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("BOARDNO"										, Integer.parseInt(boardNO));
		
		try {
			logger.info("Get Ktoto Notice Email List Data Start");
			list = getSqlSession().selectList("getKtotoNoticeEmailList", map);
			logger.info("Get Ktoto Notice Email List Data End");
			
			if(list.size() > 0) {
				for(int i=0; i<list.size(); i++) {
					JSONObject value = new JSONObject(); 
					value.put("USER_ID"						, list.get(i).get("user_id"));
					value.put("USER_NAME"						, list.get(i).get("user_name"));
					value.put("USER_EMAIL"						, list.get(i).get("user_email"));
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			logger.error("Get Ktoto Notice Email List Data Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public Map<String, Object> getKtotoNoticeMaxNo () {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		return getSqlSession().selectOne("getKtotoNoticeMaxNo", map);
	}
	public void insertNotice ( KtotoNotice notice ) throws Exception {
		TableObject noticeTable = new TableObject();
		int boardNO = Integer.parseInt(String.valueOf(getKtotoNoticeMaxNo().get("max_boardno")));
		notice.setBoardNO(String.valueOf(boardNO));
		noticeTable.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		noticeTable.put("BOARDNO"								, boardNO);
		noticeTable.put("TITLE"									, notice.getTitle());
		noticeTable.put("CONTENT"								, removeScriptTag(notice.getContext()));
		noticeTable.put("USERID"								, notice.getUserID());
		noticeTable.put("NAME"									, notice.getUserName());
		noticeTable.putCurrentTimeStamp("CREATEDATE");
		noticeTable.put("INFORM_YN"								, Utils.getString(notice.getInformYn(), "n"));
		noticeTable.put("BEGINDAY"								, notice.getBeginDay());
		noticeTable.put("ENDDAY"								, notice.getEndDay());
		noticeTable.put("POPUP"									, notice.getPopup());
		
		ArrayList<LinkedHashMap<String, Object>> arrayAuthData = new ArrayList<LinkedHashMap<String,Object>>();
		if(!notice.getFkey()[0].equals("")) {
			for(int i=0; i<notice.getFkey().length; i++) {
				TableObject noticeAuthTable = new TableObject();
				noticeAuthTable.put("BOARDNO"							, boardNO);
				noticeAuthTable.put("CHECKFKEY"							, notice.getCheckfkey()[i]);
				noticeAuthTable.put("FKEY"								, notice.getFkey()[i]);
				noticeAuthTable.put("TYPE"								, notice.getType()[i]);
				noticeAuthTable.put("INSERT_EMP"						, loginSessionInfoFactory.getObject().getUserId());
				noticeAuthTable.putCurrentTimeStamp("INSERT_DAT");
				arrayAuthData.add(noticeAuthTable);
			}
		}
		try {
			dbmsDAO.insertTable("iportal.IECT4001", noticeTable);
			if(!notice.getFkey()[0].equals("")) {
				dbmsDAO.insertTable("ktoto_custom.noticeAuth", arrayAuthData);
			}
			if(notice.getFile().length > 0) {
				String extType[] = {};
				for (CommonsMultipartFile cmfile : notice.getFile()) {
					if(cmfile.getSize() > 0) {
						File file = new File();
						file = fileService.saveFile(cmfile, extType);
						file.setTableNm("NOTICE");
						file.setBoardNo(boardNO);
						fileService.insertFile(file);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Notice Info Insert Error : {}", e.getMessage());
			throw e;
		}
	}
	public void updateNotice ( KtotoNotice notice ) throws Exception {
		TableObject noticeTable = new TableObject();
		noticeTable.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID(), true);
		noticeTable.put("BOARDNO"								, Integer.parseInt(notice.getBoardNO()), true);
		noticeTable.put("TITLE"									, notice.getTitle());
		noticeTable.put("CONTENT"								, removeScriptTag(notice.getContext()));
		noticeTable.put("USERID"								, notice.getUserID());
		noticeTable.put("NAME"									, notice.getUserName());
		noticeTable.put("INFORM_YN"								, Utils.getString(notice.getInformYn(), "n"));
		noticeTable.put("BEGINDAY"								, notice.getBeginDay());
		noticeTable.put("ENDDAY"								, notice.getEndDay());
		noticeTable.put("POPUP"									, notice.getPopup());
		
		TableObject noticeAuthDeleteTable = new TableObject();
		noticeAuthDeleteTable.put("BOARDNO"							, Integer.parseInt(notice.getBoardNO()));
		ArrayList<LinkedHashMap<String, Object>> arrayAuthData = new ArrayList<LinkedHashMap<String,Object>>();
		if(!notice.getFkey()[0].equals("")) {
			for(int i=0; i<notice.getFkey().length; i++) {
				TableObject noticeAuthTable = new TableObject();
				noticeAuthTable.put("BOARDNO"							, Integer.parseInt(notice.getBoardNO()));
				noticeAuthTable.put("CHECKFKEY"							, notice.getCheckfkey()[i]);
				noticeAuthTable.put("FKEY"								, notice.getFkey()[i]);
				noticeAuthTable.put("TYPE"								, notice.getType()[i]);
				noticeAuthTable.put("INSERT_EMP"						, loginSessionInfoFactory.getObject().getUserId());
				noticeAuthTable.putCurrentTimeStamp("INSERT_DAT");
				arrayAuthData.add(noticeAuthTable);
			}
		}
		try {
			dbmsDAO.updateTable("iportal.IECT4001", noticeTable.getKeyMap() , noticeTable);
			if(!notice.getFkey()[0].equals("")) {
				dbmsDAO.deleteTable("ktoto_custom.noticeAuth", noticeAuthDeleteTable);
				dbmsDAO.insertTable("ktoto_custom.noticeAuth", arrayAuthData);
			}
			if(notice.getFile().length > 0){
				String extType[] = {};
				
				for(CommonsMultipartFile cmfile : notice.getFile())
				{
					if(cmfile.getSize() > 0){
						File file = new File();
						//파일저장
						file = fileService.saveFile(cmfile, extType);
						file.setTableNm("NOTICE");
						file.setBoardNo(Integer.parseInt(notice.getBoardNO()));
						fileService.insertFile(file);
					}
				}
			}
			logger.info("Update Notice Info Start");
		} catch ( Exception e ) {
			e.printStackTrace();
			logger.error("ERROR > Get Notice Info Update Error : {}", e.getMessage());
			throw e;
		}
	}
	public void removeNotice( String boardNO ) throws Exception {
		TableObject noticeTable = new TableObject();
		noticeTable.put("BOARDNO"								, Integer.parseInt(boardNO));
		
		try {
			logger.info("Delete Notice Data Start");
			dbmsDAO.deleteTable("iportal.IECT4001", noticeTable);
			logger.info("Delete Notice Data End");
			
			JSONArray fileList = new JSONArray();		
			fileList = getNoticeDtailFileInfo(boardNO);
			for(int i=0; i<fileList.size();i++) {
				JSONObject fileArray = (JSONObject) fileList.get(i);
				int fileID = Integer.parseInt(fileArray.get("FILE_ID").toString());
				fileService.removeFile(fileID);
			}
			File file = new File();
			file.setTableNm("NOTICE");
			file.setBoardNo(Integer.parseInt(boardNO));
			this.fileService.removeFileByBoardNo(file);
		} catch (Exception e) {
			
		}
	}
	public void setKtotoNoticeHitCount ( String boardNo ) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("BOARDNO"										, Integer.valueOf(boardNo));
		
		try {
			getSqlSession().update("setKtotoNoticeHitCount", map);
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	public String getKtotoNoticeUserEmail( String userID ) throws Exception {
		String returnValue = "";
		HashMap<String, Object> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"										, userID);
		
		try {
			logger.info("Get Ktoto Notice User Email Data Start");
			list = getSqlSession().selectOne("getKtotoNoticeUserEmail", map);
			logger.info("Get Ktoto Notice User Email Data End");
			if(list.size() > 0) {
				returnValue = list.get("user_email").toString();
			}
		} catch ( Exception e ) {
			logger.error("ERROR > Get Ktoto Notice User Email Data Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getKtotoNoticeAuthList () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		
		try {
			logger.info("Get Ktoto Notice User Authority Data Start");
			list = getSqlSession().selectList("getKtotoNoticeAuthList", map);
			logger.info("Get Ktoto Notice User Authority Data End");
			if(list.size() > 0) {
				for(int i=0;i<list.size();i++) {
					JSONObject value = new JSONObject();
					value.put("C_ID"							, list.get(i).get("c_id"));
					value.put("PID"								, list.get(i).get("pid"));
					value.put("NAME"							, list.get(i).get("name"));
					value.put("TYPE"							, list.get(i).get("type"));
					value.put("C_VALUE"							, list.get(i).get("c_value"));
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Ktoto Notice User Authority Data Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getKtotoNoticeSelectAuthData ( String boardNO ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("BOARDNO"										, Integer.valueOf(boardNO));
		
		
		try {
			logger.info("Get Ktoto Notice Select Auth Data Start");
			list = getSqlSession().selectList("getKtotoNoticeSelectAuthData", map);
			logger.info("Get Ktoto Notice Select Auth Data End");
			if(list.size() > 0) {
				for(int i=0; i<list.size(); i++) {
					JSONObject value = new JSONObject();
					value.put("FKEY"							, list.get(i).get("fkey"));
					value.put("CHECKFKEY"						, list.get(i).get("checkfkey"));
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Ktoto Notice Select Auth Data Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public String getSystemValue(String key) {
		return systemDAO.getSystemValue(key);
	}
	public String removeScriptTag(String str ){ 
		Matcher mat;  
		// script 처리 
		Pattern script = Pattern.compile("(?i)<\\s*(no)?script\\s*[^>]*>.*?<\\s*/\\s*(no)?script\\s*>",Pattern.DOTALL);  
		mat = script.matcher(str);  
		str = mat.replaceAll("");  
		
		return str;
	}
	public JSONArray getKtotoNoticeInformList() throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"										, WebConfig.getAcctID());
		
		
		try {
			logger.info("Get Ktoto Notice Inform List Data Start");
			list = getSqlSession().selectList("getKtotoInformList", map);
			logger.info("Get Ktoto Notice Inform List Data End");
			if(list.size() > 0) {
				for(int i=0; i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("BOARDNO"							, list.get(i).get("boardno"));
					value.put("TITLE"							, list.get(i).get("title"));
					value.put("CONTENT"						, list.get(i).get("content"));
					value.put("BEGINDAY"					, list.get(i).get("beginday"));
					value.put("ENDDAY"					, list.get(i).get("endday"));
					value.put("USERID"					, list.get(i).get("userid"));
					value.put("NAME"					, list.get(i).get("name"));
					value.put("HITCOUNT"					, list.get(i).get("hitcount"));
					value.put("CREATEDATE"					, list.get(i).get("createdate"));
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Ktoto Notice Inform List Data Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getKtotoNoticePopupList () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String,Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"										, loginSessionInfoFactory.getObject().getUserId());
		
		try {
			logger.info("Get Ktoto Notice PopupList Data Start");
			list = getSqlSession().selectList("getKtotoNoticePopupList", map);
			logger.info("Get Ktoto Notice PopupList Data End");
			if(list.size() > 0) {
				for(int i=0;i<list.size();i++) {
					JSONObject value = new JSONObject();
					value.put("BOARDNO"							, list.get(i).get("boardno"));
					value.put("TITLE"							, list.get(i).get("title"));
					value.put("CONTENT"							, list.get(i).get("content"));
					value.put("BEGINDAY"						, list.get(i).get("beginday"));
					value.put("ENDDAY"							, list.get(i).get("endday"));
					value.put("USERID"							, list.get(i).get("userid"));
					value.put("NAME"							, list.get(i).get("name"));
					value.put("HITCOUNT"						, list.get(i).get("hitcount"));
					value.put("CREATEDATE"						, list.get(i).get("createdate"));
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Ktoto Notice PopupList Data error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	
}
