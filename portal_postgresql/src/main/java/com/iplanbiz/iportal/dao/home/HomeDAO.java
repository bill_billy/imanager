package com.iplanbiz.iportal.dao.home;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import com.iplanbiz.iportal.dao.system.SqlHouseDAO;

@Repository
public class HomeDAO extends SqlHouseDAO {

	public SqlSession getSqlSessionon(){
		return getSqlSession();
	}

}
