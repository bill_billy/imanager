package com.iplanbiz.iportal.dao.admin;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;

@Repository
public class UserExcelLogDAO extends SqlSessionDaoSupport {

	@Autowired
	SqlSessionFactory sqlSessionFactory;
	@Resource(name = "loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired
	DBMSDAO dbmsDAO;
	private Logger logger = LoggerFactory.getLogger(getClass());

	public JSONArray getMenuList(String solutionName, String searchValue, String searchType, String useMenuOpenType)
			throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		Map<String, Object> queryMap = new HashMap<String, Object>();
		
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		queryMap.put("SOLUTION_NAME", solutionName);
		queryMap.put("SEARCH_TYPE", searchType);
		queryMap.put("SEARCH_VALUE", searchValue);
		queryMap.put("MENU_OPEN_TYPE", useMenuOpenType);
		queryMap.put("USER_ID", loginSessionInfoFactory.getObject().getUserId());
		System.out.println(queryMap);
		try {
			logger.info("Get MENU List Data Start");
			list = getSqlSession().selectList("getTreeGridMenuList", queryMap);
			logger.info("Get MENU List Data End");
			if (list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject value = new JSONObject();
					value.put("C_ID", list.get(i).get("c_id"));
					value.put("C_NAME", list.get(i).get("c_name"));
					value.put("P_ID", list.get(i).get("p_id"));
					value.put("P_NAME", list.get(i).get("p_name"));
					value.put("SORT_ORDER", list.get(i).get("sort_order"));
					value.put("MENU_TYPE", list.get(i).get("menu_type"));
					value.put("SOURCE_OWNER", list.get(i).get("source_owner"));
					value.put("MENU_TYPE", list.get(i).get("menu_type"));
					value.put("PROG_ID", list.get(i).get("prog_id"));
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			logger.error("Get MENU List Data Error : {} ", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	
	

	public JSONArray getExcelLogReport(String cid) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		Map<String, Object> queryMap = new HashMap<String, Object>();
		
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		queryMap.put("CID", cid);
		System.out.println(queryMap);
		try {
			logger.info("Get MENU List Data Start");
			list = getSqlSession().selectList("getExcelLogReport", queryMap);
			logger.info("Get MENU List Data End");
			if (list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject value = new JSONObject();
					value.put("YYYYMM", list.get(i).get("yyyymm"));
					value.put("C_NAME", list.get(i).get("c_name"));
					value.put("C_PATH", list.get(i).get("c_path"));
					value.put("DEPT_NM", list.get(i).get("dept_nm"));
					value.put("EMP_ID", list.get(i).get("emp_id"));
					value.put("EMP_NM", list.get(i).get("emp_nm"));
					value.put("IP_ADDR", list.get(i).get("ip_addr"));
					value.put("INSERT_DAT", list.get(i).get("insert_dat"));
					value.put("PARAM_NM", list.get(i).get("param_nm"));
					value.put("C_ID", list.get(i).get("c_id"));
					value.put("BROWSER", list.get(i).get("browser"));
					value.put("DEVICE_TYPE", list.get(i).get("device_type"));
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			logger.error("Get MENU List Data Error : {} ", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getExcelLogDate(String start_dat, String end_dat) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		Map<String, Object> queryMap = new HashMap<String, Object>();
		
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		queryMap.put("START_DAT", start_dat);
		queryMap.put("END_DAT", end_dat);
		System.out.println(queryMap);
		
		try {
			logger.info("Get MENU List Data Start");
			list = getSqlSession().selectList("getExcelLogDate", queryMap);
			logger.info("Get MENU List Data End");
			if (list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject value = new JSONObject();
					value.put("YYYYMM", list.get(i).get("yyyymm"));
					value.put("C_NAME", list.get(i).get("c_name"));
					value.put("C_PATH", list.get(i).get("c_path"));
					value.put("DEPT_NM", list.get(i).get("dept_nm"));
					value.put("EMP_ID", list.get(i).get("emp_id"));
					value.put("EMP_NM", list.get(i).get("emp_nm"));
					value.put("IP_ADDR", list.get(i).get("ip_addr"));
					value.put("INSERT_DAT", list.get(i).get("insert_dat"));
					value.put("PARAM_NM", list.get(i).get("param_nm"));
					value.put("C_ID", list.get(i).get("c_id"));
					value.put("BROWSER", list.get(i).get("browser"));
					value.put("DEVICE_TYPE", list.get(i).get("device_type"));
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			logger.error("Get MENU List Data Error : {} ", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}



	public JSONObject getNavi(String cid) throws Exception {
		JSONObject value = new JSONObject();
		StringBuffer returnValue = new StringBuffer("");
		List<LinkedHashMap<String, String>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("C_ID"													, cid);
		
		try{
			logger.info("Get Menu Navi Start");
			list = getSqlSession().selectList("getNaviOrder", map);
			logger.info("Get Menu Navi End");
			if(list.size() > 0){
				for(int i=list.size()-1; i >= 0;i--){
					returnValue.append(list.get(list.size()-1-i).get("c_name"));
					if(i!=0) returnValue.append(">");
					
				}
				value.put("navi", returnValue.toString());
			}
		}catch(Exception e){
			logger.error("ERROR > Get Menu Navi Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return value;
	}

	
	public List<LinkedHashMap<String, String>> downExcelLogReport(String cid) {
		List<LinkedHashMap<String, String>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"	, loginSessionInfoFactory.getObject().getAcctID());
		map.put("CID"		, cid);
		
		list = getSqlSession().selectList("downExcelLogReport", map);
		
		return list;
	}
	public List<LinkedHashMap<String, String>> downExcelLogDate(String start_dat, String end_dat) {
		List<LinkedHashMap<String, String>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"	, loginSessionInfoFactory.getObject().getAcctID());
		map.put("START_DAT", start_dat);
		map.put("END_DAT", end_dat);
		
		list = getSqlSession().selectList("downExcelLogDate", map);
		return list;
	}

}
