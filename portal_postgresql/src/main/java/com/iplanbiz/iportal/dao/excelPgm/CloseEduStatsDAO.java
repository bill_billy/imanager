package com.iplanbiz.iportal.dao.excelPgm;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.butler.Proxy;
import com.iplanbiz.core.io.butler.ProxyPool;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.io.file.FileService;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.system.CustomSqlHouseDAO;

@Repository
public class CloseEduStatsDAO extends SqlSessionDaoSupport{
	
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Resource(name="proxyPool") 	ProxyPool proxyPool;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired DBMSDAO dbmsDAO;
	@Autowired CustomSqlHouseDAO customSqlHouseDAO; 
	
	public JSONArray getCommonCodeMmOrder(String comlcd)  throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("COML_COD"												, comlcd);
		
		try{
			logger.info("getCommonCodeMmOrder Data Start");
			list = getSqlSession().selectList("getCommonCodeMmOrder", map);
			logger.info("getCommonCodeMmOrder Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("COM_COD"									, list.get(i).get("com_cod"));
					value.put("COM_NM"										, list.get(i).get("com_nm"));
					value.put("SORT_ORDER"									, list.get(i).get("sort_order"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("getCommonCodeMmOrder Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getStepOneForCloseEduStat(String yyyy,String mm)  throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("YYYY"												, yyyy);
		map.put("MM"												, mm);
		
		try{
			logger.info("getStepOneForCloseEduStat Data Start");
			list = getSqlSession().selectList("getStepOneForCloseEduStat", map);
			logger.info("getStepOneForCloseEduStat Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("STATUS"									, list.get(i).get("status"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("getStepOneForCloseEduStat Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray closeEduStatsGetProgramID()  throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		try{
			logger.info("closeEduStatsGetProgramID Data Start");
			list = getSqlSession().selectList("closeEduStatsGetProgramID");
			logger.info("closeEduStatsGetProgramID Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("GUBN"										, list.get(i).get("gubn"));
					value.put("GUBN_NAME"									, list.get(i).get("gubn_name"));
					value.put("END_TIME"									, list.get(i).get("end_time"));
					value.put("SUCCESS_YN"									, list.get(i).get("success_yn"));
					value.put("SORT_ORDER"									, list.get(i).get("sort_order"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("closeEduStatsGetProgramID Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getProceduerDetailByClose(String gubn)  throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("GUBN"												, gubn);
		try{
			logger.info("getProceduerDetailByClose Data Start");
			list = getSqlSession().selectList("getProceduerDetailByClose",map);
			logger.info("getProceduerDetailByClose Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("GUBN"									, list.get(i).get("gubn"));
					value.put("IDX"										, list.get(i).get("idx"));
					value.put("PROCEDURE_ID"							, list.get(i).get("procedure_id"));
					value.put("PROCEDURE_NM"							, list.get(i).get("procedure_nm"));
					value.put("DBKEY"									, list.get(i).get("dbkey"));
					value.put("EXPLAIN"									, list.get(i).get("explain"));
					value.put("SCH_ID"									, list.get(i).get("sch_id"));
					value.put("SUCCESS_YN"									, list.get(i).get("success_yn"));
					value.put("SCH_NM"									, list.get(i).get("sch_nm"));
					value.put("RESULT"									, list.get(i).get("result"));
					value.put("RESULT_NM"								, list.get(i).get("result_nm"));
					value.put("END_TIME"									, list.get(i).get("end_time"));
					value.put("END_DAT"									, list.get(i).get("end_dat"));
					value.put("SCH_IDX"									, list.get(i).get("sch_idx"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("closeEduStatsGetProgramID Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getProcedureErrorByClose(String gubn)  throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("GUBN"												, gubn);
		try{
			logger.info("getProcedureErrorByClose Data Start");
			list = getSqlSession().selectList("getProcedureErrorByClose",map);
			logger.info("getProcedureErrorByClose Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					returnValue.add(list.get(i));
				}
			}
		}catch(Exception e) {
			logger.error("getProcedureErrorByClose Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray closeEduStatsExcelList(String year, String month)  throws Exception {
		JSONArray returnValue = new JSONArray();
		
		List<LinkedHashMap<String, Object>> list = new ArrayList<LinkedHashMap<String, Object>>();
		LinkedHashMap<String, Object> param = new LinkedHashMap<String, Object>();
		param.put("YEAR", year);
		param.put("MONTH", month);
		
		try{
			logger.info("closeEduStatsExcelList Data Start");
			list = customSqlHouseDAO.executeCustomQuery("CloseEduStatsDAO.closeEduStatsDownload",param);
			//list = getSqlSession().selectList("closeEduStatsExcelList",map);
			logger.info("closeEduStatsExcelList Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("YEAR"								, list.get(i).get("YEAR"));
					value.put("MONTH"									, list.get(i).get("MONTH"));
					value.put("SUST_NM"									, list.get(i).get("SUST_NM"));
					value.put("COLL_NM"								, list.get(i).get("COLL_NM"));
					value.put("DAN_NM"								, list.get(i).get("DAN_NM"));
					value.put("SUST_SPE_NM"								, list.get(i).get("SUST_SPE_NM"));
					value.put("PART_NM"								, list.get(i).get("PART_NM"));
					value.put("PART_DTL_NM"								, list.get(i).get("PART_DTL_NM"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("closeEduStatsExcelList Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public AjaxMessage procedureStartYyyymmddBySch(String[] procedureId, String[] procedureNm, String[] schID, String[] schName, String gubn, String yyyy, String mm,String[] dbKey)  throws Exception {
		AjaxMessage msg = new AjaxMessage();
		
		HashMap<String, String> map = new HashMap<String, String>();
		String programid="DATAGENERATE";
		loginSessionInfoFactory.getObject().initProgress(programid, procedureId.length);
		
		HashMap<String,Object> insertIECT3003 = new HashMap<String,Object>();
		insertIECT3003.put("GUBN", gubn);
		HashMap<String,Object> mapNextIECT3003 = getSqlSession().selectOne("getMaxIECT3003",insertIECT3003);
		BigDecimal  maxIECT3003 = (BigDecimal )mapNextIECT3003.get("NXT");
		insertIECT3003.put("IDX", maxIECT3003);
		insertIECT3003.put("USER_ID", loginSessionInfoFactory.getObject().getUserId());
		getSqlSession().insert("insertIECT3003",insertIECT3003);
		
		
		boolean totalsuccess = true;
		Proxy proxy = proxyPool.createProxy();
		ArrayList<HashMap<String,String>> params = new ArrayList<HashMap<String,String>>();
		HashMap<String,String> args1 = new HashMap<String,String>();
		args1.put("VALUE", yyyy);
		args1.put("TYPE",String.valueOf(Types.VARCHAR));
		args1.put("INOUT", "in");
		
		HashMap<String,String> args2 = new HashMap<String,String>();
		args2.put("VALUE", mm);
		args2.put("TYPE",String.valueOf(Types.VARCHAR));
		args2.put("INOUT", "in");
		params.add(args1);
		params.add(args2);
		
		try{
			proxy.start();
			String flag = "Y";
			for(int i=0; i<procedureNm.length; i++){
				
				if(!schID[i].equals("")){
					loginSessionInfoFactory.getObject().pushJob(programid,i+schName[i], schName[i]+"스케쥴러 수행 중.");
					//this.quartsService.executeJob(schID[i]);
					String acct_id = loginSessionInfoFactory.getObject().getAcctID();
					String jobName = schID[i]+"_"+acct_id;
					JSONObject proxyParam = new JSONObject();
					proxyParam.put("JOBGROUP", "etl");
					proxyParam.put("JOBNAME", jobName);
					proxyParam.put("REQUEST_USER_ID", loginSessionInfoFactory.getObject().getUserId());
					proxy.requestRule("scheduler", "RunImmediately", proxyParam);
				}
				
				
				JSONObject param = new JSONObject();
				param.put("USER_ID",loginSessionInfoFactory.getObject().getUserId());
				param.put("USER_NM",loginSessionInfoFactory.getObject().getUserName());
				param.put("PROC_NM",procedureId[i]);
				param.put("DB_KEY",dbKey[i]);
				param.put("PARAMS", params);
				logger.info(procedureId[i]+dbKey[i]);
				 
				msg = proxy.requestRule("etl", "Procedure", param); 
				if(msg.getReturnCode().toUpperCase().indexOf("EXCEPTION")!=-1) flag="N";
			}
			insertIECT3003.put("SUCCESS_YN",flag);
			getSqlSession().update("updateIECT3003",insertIECT3003);
		}catch(Exception e){
			insertIECT3003.put("SUCCESS_YN","N");
			getSqlSession().update("updateIECT3003",insertIECT3003);
			totalsuccess = false;
			logger.error("error",e);
			loginSessionInfoFactory.getObject().pushJob(programid,e.getClass().getName(),"오류: "+ e.getMessage());
			msg.setExceptionText("fail");
		}finally{
			proxy.stop();
			
			
		}
		if(totalsuccess)
			loginSessionInfoFactory.getObject().completeProgress(programid);
		return msg;
	}
	public List<LinkedHashMap<String, Object>> closeEduStatsDownload(String year, String month, String dbkey){
	/*	
		ArrayList<String> columnNm = new ArrayList<String>();
		columnNm.add("YEAR");
		columnNm.add("MONTH");
		columnNm.add("GUBUN");
		columnNm.add("SUST_ID");
		columnNm.add("SUST_NM");
		columnNm.add("COLL_NM");
		columnNm.add("DAN_NM");
		columnNm.add("SUST_SPE_NM");
		columnNm.add("PART_NM");
		columnNm.add("PART_DTL_NM");*/
		
		List<LinkedHashMap<String, Object>> data = new ArrayList<LinkedHashMap<String, Object>>();
		try {
			LinkedHashMap<String, Object> param = new LinkedHashMap<String, Object>();
			param.put("YEAR", year);
			param.put("MONTH", month);
			data = customSqlHouseDAO.executeCustomQuery("CloseEduStatsDAO.closeEduStatsDownload",param);
			logger.info("downloadData::"+data);
			/*
			if(data.size()==0){
				LinkedHashMap<String,Object> row  = new LinkedHashMap<String, Object>();
				for(int i=0;i<columnNm.size();i++){					
					row.put(columnNm.get(i), "");
				}
				data.add(row);
			}*/
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}
	public AjaxMessage uploadCloseExcel(CommonsMultipartFile upFile, String dbkey, String year, String month) {
		AjaxMessage msg = null;
		FileService fileService = new FileService(WebConfig.getProperty("system.upload.SaveDir"),Long.valueOf(WebConfig.getProperty("system.upload.maxUploadSize").toString()));
		String extType[] = {};
		
		String fileSaveName = fileService.setFileName();
		
		// 파일 확장자 가져오기
		String fileOrgName = upFile.getOriginalFilename();
		String fileExt =  fileOrgName.substring(fileOrgName.lastIndexOf(".")+1);
		
 
		Proxy proxy = proxyPool.createProxy();
		proxy.start();
		
		try { 
				
			fileService.saveFile(upFile, extType, fileSaveName);
				
			JSONObject param = new JSONObject();
			param.put("ACCT_ID",this.loginSessionInfoFactory.getObject().getAcctID());
			param.put("YYYY",year);
			param.put("MM", month);
			param.put("DBKEY",dbkey);		
			param.put("UPLOADPATH",fileService.getUpLoadPath());
			param.put("SAVEDFILENAME",fileSaveName); 
			param.put("FILEEXT",fileExt);
			param.put("ORGFILEPATH",upFile.getOriginalFilename());
			 
			msg = proxy.requestRule("univ", "UploadStandardClassification", param);
				
			java.io.File realFile = new java.io.File(fileService.getUpLoadPath() + fileSaveName + "."+fileExt);
			fileService.deleteFile(realFile, Utils.getString(upFile.getOriginalFilename(), ""));
			
			
		}catch(Exception e){
			msg = new AjaxMessage();
			msg.setExceptionText("error:"+e); 
		} finally{
			proxy.stop();
		}
		
		return msg;
	}
	
	
	
	
	
	
	
	
	
	
}
