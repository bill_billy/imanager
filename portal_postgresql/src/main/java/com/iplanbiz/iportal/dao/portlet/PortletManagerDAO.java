package com.iplanbiz.iportal.dao.portlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;

@Repository
public class PortletManagerDAO extends SqlSessionDaoSupport {
	
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Autowired DBMSDAO dbmsDAO;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public JSONArray getPortletManagerList () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		
		try{
			logger.info("Get Portlet Manager List Data Start");
			list = getSqlSession().selectList("getPortletManagerList", map);
			logger.info("Get Portlet Manager List Data End");
			
			if(list.size() > 0){
				for(int i=0;i<list.size(); i++){
					JSONObject value = new JSONObject();
					value.put("PLID"										, list.get(i).get("plid"));
					value.put("PL_NAME"								, list.get(i).get("pl_name"));
					value.put("PL_DATE"									, list.get(i).get("pl_date"));
					value.put("PL_STATUS"								, list.get(i).get("pl_status"));
					value.put("PL_DEFAULT"							, list.get(i).get("pl_default"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("ERROR > Get Portlet Manager Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		
		return returnValue;
	}
	public int defalutPortletSetting( String plID ) throws Exception{
		int returnValue = 0;
		
		TableObject defaultIECT7106 = new TableObject();
		defaultIECT7106.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID(), true);
		defaultIECT7106.put("PL_DEFAULT"								, "0");
		
		TableObject IECT7106 = new TableObject();
		IECT7106.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID(), true);
		IECT7106.put("PLID"													, Integer.valueOf(plID), true);
		IECT7106.put("PL_DEFAULT"										, "1");
		

		try{
			logger.info("Default Portlet IECT7106 Table Start");
			dbmsDAO.updateTable("iportal_custom.IECT7106"	, defaultIECT7106.getKeyMap(), defaultIECT7106);
			logger.info("Default Portlet IECT7106 Table End");
			
			logger.info("Update IECT7106 Table Start");
			dbmsDAO.updateTable("iportal_custom.IECT7106"	, IECT7106.getKeyMap(), IECT7106);
			logger.info("Update IECT7106 Table End");
			returnValue = 0;
		}catch(Exception e){
			logger.error("ERROR > Default Portlet IECT7106 Error : {}" , e);
			e.printStackTrace();
			returnValue = 1;
			throw e;
		}
		return returnValue;
	}
	public int defaultUserPortlet () throws Exception {
		int returnValue = 0;
		
		TableObject IECT7104 = new TableObject();
		IECT7104.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		
		try{
			logger.info("Portlet Default User Start");
			dbmsDAO.deleteTable("iportal_custom.IECT7104", IECT7104);
			logger.info("Portlet Default User End");
			returnValue = 0;
		}catch(Exception e){
			logger.error("ERROR > Portlet Default User Error : {} " ,e);
			e.printStackTrace();
			returnValue = 1;
			throw e;
		}
		return returnValue;
	}
	public JSONArray getPortletInfo ( String plID ) throws Exception {
		
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("PLID"												, Integer.valueOf(plID));
		
		try{
			logger.info("Get Portlet Info Data Start");
			list = getSqlSession().selectList("getPortletInfo", map);
			logger.info("Get Portlet Info Data End");
			
			if(list.size() > 0){
				JSONObject value = new JSONObject();
				for(int i=0; i<list.size(); i++){
					value.put("PL_NAME"								, list.get(i).get("pl_name"));
					value.put("PL_LAYOUT"								, list.get(i).get("pl_layout"));
					value.put("PL_STATUS"								, list.get(i).get("pl_status"));
					value.put("PL_DEFAULT"							, list.get(i).get("pl_default"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("ERROR > Get Portlet Info Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getPageletPortletList ( String pID, String searchValue ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("PID"														, pID);
		
		try{
			logger.info("Get Pagelet Portlet List Data Start");
			list = getSqlSession().selectList("getPageletPortletList", map);
			logger.info("Get Pagelet Portlet List Data End");
			
			if(list.size() > 0){
				for( int i=0; i<list.size(); i++){
					JSONObject value = new JSONObject();
					value.put("POID"										, list.get(i).get("poid"));
					value.put("NAME"									, list.get(i).get("name"));
					value.put("TYPE"										, list.get(i).get("type"));
					value.put("ACCT_ID"									, list.get(i).get("acct_id"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("ERROR > Get Pagelet Portlet List Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getPortletCategory () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		
		try{
			logger.info("Get Portlet Category Data Start");
			list = getSqlSession().selectList("getPortletCategory", map);
			logger.info("Get Portlet Category Data End");
			
			if(list.size() > 0){
				for(int i=0; i<list.size(); i++){
					JSONObject value = new JSONObject();
					value.put("COM_COD"								, list.get(i).get("com_cod"));
					value.put("COM_NM"								, list.get(i).get("com_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("ERROR > Get Portlet Category Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int updatePageletInfo ( String plName, String plLayout, String plID) throws Exception {
		int returnValue = 0;
		
		TableObject IECT7106 = new TableObject();
		IECT7106.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID(), true);
		IECT7106.put("PLID"														, Integer.valueOf(plID), true);
		IECT7106.put("PL_NAME"												, plName);
		IECT7106.put("PL_LAYOUT"											, plLayout);
		
		try{
			logger.info("Update Pagelet Info Data Start");
			dbmsDAO.updateTable("iportal_custom.IECT7106", IECT7106.getKeyMap(), IECT7106);
			logger.info("Update Pagelet Info Data End");
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			e.printStackTrace();
			logger.error("ERROR > Update PageletInfo Data Error : {}", e);
			throw e;
		}
		return returnValue;
	}
	public int deletePageletPortlet ( String plID ) throws Exception{
		int returnValue = 0;
		
		TableObject DIECT7107 = new TableObject();
		DIECT7107.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		DIECT7107.put("PLID"													, plID);
		
		try{
			logger.info("Delete Pagelet Portlet Data Start");
			dbmsDAO.deleteTable("iportal_custom.IECT7107", DIECT7107);
			logger.info("Delete Pagelet Portlet Data End");
			returnValue = 0;
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Delete Pagelet Portlet Data Error : {}" , e);
			returnValue = 1;
			throw e;
		}
		return returnValue;
	}
	public int insertPageletPortlet ( String plID, String poID, String xAxis, String yAxis, JSONObject cssInfo) throws Exception {
		int returnValue = 0;
		TableObject IECT7107 = new TableObject();
		IECT7107.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		IECT7107.put("PLID"											, plID);
		IECT7107.put("POID"										, poID);
		IECT7107.put("XAXIS"										, Integer.valueOf(xAxis));
		IECT7107.put("YAXIS"										, Integer.valueOf(yAxis));
		IECT7107.put("TITLE_BACKGROUND_COLOR"		, cssInfo.get("title_background_color"));
		IECT7107.put("TITLE_FONT_COLOR"					, cssInfo.get("title_font_color"));
		if(cssInfo.get("title_font_size") != null){
			IECT7107.put("TITLE_FONT_SIZE"							, Integer.parseInt((String) cssInfo.get("title_font_size")));	
		}
		IECT7107.put("CONTENT_BACKGROUND_COLOR", cssInfo.get("content_background_color"));
		IECT7107.put("CONTENT_FONT_COLOR"				, cssInfo.get("content_font_color"));
		if(cssInfo.get("content_font_size") != null){
			IECT7107.put("CONTENT_FONT_SIZE"					, Integer.parseInt((String) cssInfo.get("content_font_size")));
		}
		IECT7107.put("PORTLET_TYPE"							, cssInfo.get("portletType"));
		try{
			logger.info("Insert Pagelet Portlet Data Start");
			dbmsDAO.insertTable("iportal_custom.IECT7107", IECT7107);
			logger.info("Insert Pagelet Portlet Data End");
			
			returnValue = 0;
		}catch(Exception e){
			logger.error("ERROR > Insert Pagelet Portlet Data Error : {}" , e);
			e.printStackTrace();
			returnValue = 1;
			throw e;
		}
		return returnValue;
	}
	public int updatePageletTheme ( String portletType, String plID, String poID ) throws Exception {
		int returnValue = 0;
		
		TableObject UIECT7107 = new TableObject();
		UIECT7107.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		UIECT7107.put("PORTLET_TYPE"								, portletType);
		UIECT7107.put("PLID"											, String.valueOf(plID));
		UIECT7107.put("POID"											, String.valueOf(poID));
		
		try{
			logger.info("Update Pagelet Theme Data Start");
			dbmsDAO.updateTable("iportal_custom.IECT7107", UIECT7107.getKeyMap(), UIECT7107);
			logger.info("Update Pagelet Theme Data End");
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			e.printStackTrace();
			logger.error("ERROR > Update Pagelet Theme Data Error : {} " ,e);
			throw e;
		}
		return returnValue;
	}
	public String getMaxPlID () throws Exception {
		String returnValue = "";
		List<HashMap<String ,Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		
		try{
			logger.info("Get Max PLID Data Start");
			list = getSqlSession().selectList("getMaxPlID", map);
			logger.info("Get Max PLID Data End");
			
			if(list.size() > 0){
				returnValue = list.get(0).get("plid").toString();
			}
		}catch(Exception e){
			logger.error("ERROR > Get Max PLID Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertPageletInfo ( String plName, String plLayout ) throws Exception {
		int returnValue = 0;
		String getMaxPID = getMaxPlID();
		TableObject IECT7106 = new TableObject();
		IECT7106.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		IECT7106.put("PLID"												, Integer.valueOf(getMaxPID));
		IECT7106.put("PL_NAME"										, plName);
		IECT7106.putCurrentTimeStamp("PL_DATE");
		IECT7106.put("PL_STATUS"									, "1");
		IECT7106.put("PL_LAYOUT"									, plLayout);
		
		try{
			logger.info("Insert Pagelet Info Data Start");
			dbmsDAO.insertTable("iportal_custom.IECT7106",  IECT7106);
			logger.info("Insert Pagelet Info Data End");
			returnValue = Integer.parseInt(getMaxPID);
			logger.info("Param Insert Return Value : {}" , returnValue);
		}catch(Exception e){
			logger.error("ERROR > Insert Pagelet Info Data Error : {} " ,e);
			e.printStackTrace();
			returnValue = 1;
			throw e;
		}
		return returnValue;
	}
	public int deletePageletInfo ( String plID ) throws Exception {
		int returnValue = 0;
		
		TableObject DIECT7106 = new TableObject();
		DIECT7106.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		DIECT7106.put("PLID"													, Integer.valueOf(plID));
		
		try{
			logger.info("Delete Pagelet Info Data Start");
			dbmsDAO.deleteTable("iportal_custom.IECT7106", DIECT7106);
			logger.info("Delete Pagelet Info Data End");
			
			returnValue = 0;
		}catch(Exception e){
			logger.error("ERROR > Delete Pagelet Info Data Error : {} ", e);
			e.printStackTrace();
			returnValue = 1;
			throw e;
		}
		return returnValue;
	}
}