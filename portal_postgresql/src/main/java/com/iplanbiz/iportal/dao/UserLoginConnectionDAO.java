package com.iplanbiz.iportal.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dao.auth.GroupView;

@Repository
public class UserLoginConnectionDAO extends SqlSessionDaoSupport {
	
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Autowired DBMSDAO dbmsDAO;
	@Autowired GroupView groupView;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public JSONArray getUserLoginConnectionYyyymm () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		try{
			logger.info("Get User Login Connection YYYYMM Data Start");
			list = getSqlSession().selectList("getUserLoginConnectionYyyymm", map);
			logger.info("Get User Login Connection YYYYMM Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("COM_COD"								, list.get(i).get("com_cod"));
					value.put("COM_NM"								, list.get(i).get("com_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get User Login Connection YYYYMM Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getUserLoginConnectionList ( String yyyymm) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("YYYYMM"										, yyyymm);
		try{
			logger.info("Get User Login Connection List Data Start");
			list = getSqlSession().selectList("getUserLoginConnectionList", map);
			logger.info("Get User Login Connection List Data End");
			if(list.size()>0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("EMP_ID"									, list.get(i).get("emp_id"));
					value.put("EMP_NM"								, list.get(i).get("emp_nm"));
					value.put("LAST_USER_DAY"						, list.get(i).get("last_user_day"));
					value.put("USER_GRP_NM"						, list.get(i).get("user_grp_nm"));
					value.put("FIRST_DATE"								, list.get(i).get("first_date"));
					value.put("SECOND_DATE"						, list.get(i).get("second_date"));
					value.put("THIRD_DATE"							, list.get(i).get("third_date"));
					value.put("FOURTH_DATE"						, list.get(i).get("fourth_date"));
					value.put("FIRST_VALUE"							, list.get(i).get("first_value"));
					value.put("SECOND_VALUE"						, list.get(i).get("second_value"));
					value.put("THIRD_VALUE"							, list.get(i).get("third_value"));
					value.put("FOURTH_VALUE"						, list.get(i).get("fourth_value"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get User Login Connection List Data Error : {} ", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getUserLoginConnectionByExcel ( String yyyymm ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("YYYYMM"										, yyyymm);
		try{
			logger.info("Get User Login Connection By Excel Data Start");
			list = getSqlSession().selectList("getUserLoginConnectionByExcel", map);
			logger.info("Get User Login Connection By Excel Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("EMP_ID"									, list.get(i).get("emp_id"));
					value.put("EMP_NM"								, list.get(i).get("emp_nm"));
					value.put("USER_GRP_NM"						, list.get(i).get("user_grp_nm"));
					value.put("LAST_USER_DAY"						, list.get(i).get("last_user_day"));
					value.put("FIRST_DATE"								, list.get(i).get("first_date"));
					value.put("SECOND_DATE"						, list.get(i).get("second_date"));
					value.put("THIRD_DATE"							, list.get(i).get("third_date"));
					value.put("FOURTH_DATE"						, list.get(i).get("fourth_date"));
					value.put("FIRST_VALUE"							, list.get(i).get("first_value"));
					value.put("SECOND_VALUE"						, list.get(i).get("second_value"));
					value.put("THIRD_VALUE"							, list.get(i).get("third_value"));
					value.put("FOURTH_VALUE"						, list.get(i).get("fourth_value"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get User Login Connection By Excel Data Error : {} ", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertUserLog ( String actionTypeNo, String targetTypeNo, String targetValue, String cID, String remoteIP, String osInfo, String browserInfo, String browserVersion, String targetPath ) throws Exception {
		int returnValue = 0;
		String userGrpName = groupView.getStringByUserID("USER_GRP_NM",loginSessionInfoFactory.getObject().getUserId());
		TableObject iect7017 = new TableObject();
		iect7017.put("USER_ID"															, loginSessionInfoFactory.getObject().getUserId());
		iect7017.put("USER_NM"															, loginSessionInfoFactory.getObject().getUserName());
		iect7017.put("USER_GRP_NM"														, userGrpName);
		iect7017.putCurrentTimeStamp("INSERT_TIME");
		iect7017.put("ACTION_TYPE_NO"													, Integer.valueOf(actionTypeNo));
		iect7017.put("TARGET_TYPE_NO"													, Integer.valueOf(targetTypeNo));
		iect7017.put("TARGET_VALUE"														, targetValue);
		iect7017.put("TARGET_ID"														, cID);
		iect7017.put("REMOTE_IP"														, remoteIP);
		iect7017.put("REMOTE_OS"														, osInfo);
		iect7017.put("REMOTE_BROWSER"													, browserInfo);
		iect7017.put("REMOTE_BROWSER_DETAIL"											, browserVersion);
		iect7017.put("TARGET_PATH"														, targetPath);
		iect7017.put("ACCT_ID"															, loginSessionInfoFactory.getObject().getAcctID());
		LoginSessionInfo loginInfo = loginSessionInfoFactory.getObject();
		if(actionTypeNo.equals("3")){
			loginInfo.setUserBrowserVersion(browserVersion);
			loginInfo.setUserBrowserType(browserInfo);
		}
		try{
			logger.info("User Log Insert Start");
			dbmsDAO.insertTable("iportal_custom.IECT7017", iect7017);
			logger.info("User Log Insert End");
		}catch(Exception e){
			logger.error("User Log Insert Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
}
