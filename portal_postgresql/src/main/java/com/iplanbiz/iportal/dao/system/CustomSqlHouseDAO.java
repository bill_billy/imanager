package com.iplanbiz.iportal.dao.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.iportal.config.WebConfig;

@Repository
public class CustomSqlHouseDAO extends SqlSessionDaoSupport {
	
	@Autowired
	private DBMSService dbmsService;
	
	
	@Autowired
	private DBMSDAO dbmsDao;
	
	
	@Autowired SqlSessionFactory sqlSessionFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	//참고..databaseId 가져오기.
	//String databaseId = this.getSqlSession().getConfiguration().getDatabaseId();
	

	public HashMap<String,Object> getSqlCustomHouseOne(String queryID) throws Exception{
		HashMap<String,Object> sqlmap  = null;
		try { 
			TableCondition customhouse = new TableCondition();
			customhouse.put("QUERYID", queryID);
			List<LinkedHashMap<String,Object>> list = dbmsDao.selectTable(false,WebConfig.getCustomTableName("CUSTOMHOUSE"), customhouse);
			if(list.size()!=0) {
				sqlmap = list.get(0);
				logger.info("getSqlCustomHouseOne idx : {}",Utils.getString(queryID, "").replaceAll("[\r\n]",""));
				logger.info("getSqlCustomHouseOne SQL : {}",Utils.getString(sqlmap.get("QUERY").toString(), "").replaceAll("[\r\n]","")); 
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error",e);
			throw e;
		}
		return sqlmap;
	}
	public List<HashMap<String,Object>> getCustomHouseListBySearchText(String searchText) throws Exception{
		List<HashMap<String,Object>> sqlmapList  = null;
		try {
			HashMap<String,Object> param = new HashMap<String, Object>();
			param.put("searchText", searchText);
			sqlmapList = this.getSqlSession().selectList("getCustomSqlHouseList", param);  
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error",e);
			throw e;
		}
		return sqlmapList;
	} 
	public List<LinkedHashMap<String,Object>> getCustomHouseList(String queryID) throws Exception{
		List<LinkedHashMap<String,Object>> list = null;
		try { 
			TableCondition customhouse = new TableCondition();
			customhouse.put("QUERYID", queryID); 
			list = dbmsDao.selectTable(false,WebConfig.getCustomTableName("CUSTOMHOUSE"), customhouse);
 
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error",e);
			throw e;
		}
		return list;
	}
	  
	public int insertCustomSqlHouse(String queryID,String dbkey, String description,
			String sql) throws Exception {
		Integer result = 1;
		try {
			
			TableObject customhouse = new TableObject();
			customhouse.put("QUERYID", queryID,true);
			customhouse.put("DBKEY", dbkey);
			customhouse.put("DESCRIPTION", description);
			customhouse.put("QUERY", sql);
			
			customhouse.put("INSERT_EMP", "admin");
			customhouse.putCurrentTimeStamp("INSERT_DAT");
			dbmsDao.insertTable(WebConfig.getAdminTableName("customhouse"),"IDX",customhouse.getKeyMap(), customhouse);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error",e);
			result = 0;
			throw e;
		}
		return result;
	}
	
	public int updateCustomSqlHouse(String queryID,String idx, String dbkey, String description,
			String sql) throws Exception {
		Integer result = 0;
		try {
			HashMap<String,Object> param = new HashMap<String, Object>();
			param.put("QUERYID", queryID);
			param.put("IDX", Integer.valueOf(idx));
			param.put("DBKEY", dbkey);
			param.put("DESCRIPTION", description);
			param.put("QUERY", sql); 
			param.put("USER_ID", "admin"); 
			logger.info(Utils.getString(param.toString(), "").replaceAll("[\r\n]",""));
			logger.debug("updateCustomSqlHouse :{}",Utils.getString(param.toString(), "").replaceAll("[\r\n]",""));
			result = this.getSqlSession().update("updateCustomSqlHouse", param); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error",e);
			throw e;
		}
		return result;
	}
	
	public List<LinkedHashMap<String,Object>> executeCustomQuery(String queryID, LinkedHashMap<String,Object> param) throws Exception{		
		logger.info("executeCustomQuery:{}",Utils.getString(queryID, "").replaceAll("[\r\n]",""));		
		TableCondition customhouse = new TableCondition();
		customhouse.put("QUERYID", queryID);
		List<LinkedHashMap<String,Object>> result = dbmsDao.selectTable(false, WebConfig.getCustomTableName("CUSTOMHOUSE"),customhouse,"IDX","ASC");
		List<LinkedHashMap<String,Object>> listAll = new ArrayList<LinkedHashMap<String,Object>>(); 
		for(int i = 0; i < result.size();i++) {
			String query = result.get(i).get("QUERY").toString();
			String dbkey = result.get(i).get("DBKEY").toString();
			if(dbmsService.containsKey(dbkey)) {
				try {
					List<LinkedHashMap<String,Object>> list = dbmsService.executeQuery(query,param,dbkey);
					listAll.addAll(list);
				}catch(Exception e) {
					logger.error("error",e);
				}
			}
		}		 
		
		return listAll;
	}
	

	public List<LinkedHashMap<String,Object>>  updateCustomQuery(String queryID, LinkedHashMap<String,Object> param) throws Exception{		
		logger.info("executeCustomQuery:{}",Utils.getString(queryID, "").replaceAll("[\r\n]",""));	
		TableCondition customhouse = new TableCondition();
		customhouse.put("QUERYID", queryID);
		List<LinkedHashMap<String,Object>> result = dbmsDao.selectTable(false, WebConfig.getCustomTableName("CUSTOMHOUSE"),customhouse,"IDX","ASC");
		List<LinkedHashMap<String,Object>> listAll = new ArrayList<LinkedHashMap<String,Object>>(); 
		for(int i = 0; i < result.size();i++) {
			String query = result.get(i).get("QUERY").toString();
			String dbkey = result.get(i).get("DBKEY").toString();
			if(dbmsService.containsKey(dbkey)) {
				dbmsService.executeUpdate(query,param,dbkey);
			}
		}		 
		
		return listAll;
	}

}
