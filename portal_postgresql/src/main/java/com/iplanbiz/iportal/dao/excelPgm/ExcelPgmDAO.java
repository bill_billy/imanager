package com.iplanbiz.iportal.dao.excelPgm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;

@Repository
public class ExcelPgmDAO extends SqlSessionDaoSupport{
	
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired DBMSDAO dbmsDAO;
	
	public JSONArray getCommonCode(String comlcd)  throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("COML_COD"												, comlcd);
		
		try{
			logger.info("getCommonCode Data Start");
			list = getSqlSession().selectList("getCommonCode", map);
			logger.info("getCommonCode Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("COM_COD"									, list.get(i).get("com_cod"));
					value.put("COM_NM"									, list.get(i).get("com_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("getCommonCode Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray  getExcelPgmDQCategoryParent(String category, String pCd, String cCd, String useYn, String searchText) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("pCd"												, pCd);
		map.put("cCd"												, cCd);
		map.put("useYn"												, useYn);
		map.put("searchText"										, searchText);
	
		try{
			logger.info("getExcelPgmDQCategoryParent Data Start");
			list = getSqlSession().selectList("getExcelPgmDQCategoryParent", map);
			logger.info("getExcelPgmDQCategoryParent Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("COML_COD"									, list.get(i).get("coml_cod"));
					value.put("COML_NM"									, list.get(i).get("coml_nm"));
					value.put("SORT_ORDER"									, list.get(i).get("sort_order"));
					value.put("BIGO"									, list.get(i).get("bigo"));
					value.put("USE_YN"									, list.get(i).get("use_yn"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("getExcelPgmDQCategoryParent Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertExcelPgmCategoryParent(String pCd, String pNm, String useYn, String sortOrder) {
		Map<String, Object> sqlMap = new HashMap<String, Object>();
		sqlMap.put("pCd", pCd);
		sqlMap.put("pNm", pNm);
		sqlMap.put("useYn", useYn);
		sqlMap.put("sortOrder", Integer.parseInt(sortOrder));
		sqlMap.put("insertEmp", loginSessionInfoFactory.getObject().getUserId());
		return getSqlSession().insert("insertExcelPgmCategoryParent", sqlMap);
		
		
	}
	public int updateExcelPgmCategoryParent(String pCd, String pNm, String useYn, String sortOrder) {
		Map<String, Object> sqlMap = new HashMap<String, Object>();
		sqlMap.put("pCd", pCd);
		sqlMap.put("pNm", pNm);
		sqlMap.put("useYn", useYn);
		sqlMap.put("sortOrder", Integer.parseInt(sortOrder));
		sqlMap.put("updateEmp", loginSessionInfoFactory.getObject().getUserId());
		return getSqlSession().update("updateExcelPgmCategoryParent", sqlMap);
	}
	public int deleteExcelPgmCategoryParent(String pCd) {
		Map<String, String> sqlMap = new HashMap<String, String>();
		sqlMap.put("pCd", pCd);
		return getSqlSession().delete("deleteExcelPgmCategoryParent", sqlMap);
		
	}
	public JSONArray getExcelPgmDQCategoryChild(String category, String pCd, String cCd, String useYn,
			String searchText) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("pCd"												, pCd);
		map.put("cCd"												, cCd);
		map.put("useYn"												, useYn);
		map.put("searchText"										, searchText);
	
		try{
			logger.info("getExcelPgmDQCategoryChild Data Start");
			list = getSqlSession().selectList("getExcelPgmDQCategoryChild", map);
			logger.info("getExcelPgmDQCategoryChild Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("COML_COD"									, list.get(i).get("coml_cod"));
					value.put("COM_COD"									, list.get(i).get("com_cod"));
					value.put("COM_NM"									, list.get(i).get("com_nm"));
					value.put("SORT_ORDER"									, list.get(i).get("sort_order"));
					value.put("BIGO"									, list.get(i).get("bigo"));
					value.put("USE_YN"									, list.get(i).get("use_yn"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("getExcelPgmDQCategoryChild Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	
	public int insertExcelPgmCategoryChild(String pCd, String cCd, String cNm, String useYn, String sortOrder) {
		Map<String, Object> sqlMap = new HashMap<String, Object>();
		sqlMap.put("pCd", pCd);
		sqlMap.put("cCd", cCd);
		sqlMap.put("cNm", cNm);
		sqlMap.put("useYn", useYn);
		sqlMap.put("sortOrder", Integer.parseInt(sortOrder));
		sqlMap.put("insertEmp", loginSessionInfoFactory.getObject().getUserId());
		return getSqlSession().insert("insertExcelPgmCategoryChild", sqlMap);
		
	}
	public int updateExcelPgmCategoryChild(String pCd, String cCd, String cNm, String useYn, String sortOrder) {
		Map<String, Object> sqlMap = new HashMap<String, Object>();
		sqlMap.put("pCd", pCd);
		sqlMap.put("cCd", cCd);
		sqlMap.put("cNm", cNm);
		sqlMap.put("useYn", useYn);
		sqlMap.put("sortOrder", Integer.parseInt(sortOrder));
		sqlMap.put("updateEmp", loginSessionInfoFactory.getObject().getUserId());
		return getSqlSession().update("updateExcelPgmCategoryChild", sqlMap);
		
	}
	public int deleteExcelPgmCategoryChild(String pCd, String cCd) {
		Map<String, String> sqlMap = new HashMap<String, String>();
		sqlMap.put("pCd", pCd);
		sqlMap.put("cCd", cCd);
		return getSqlSession().delete("deleteExcelPgmCategoryChild", sqlMap);
		
	}
	
}
