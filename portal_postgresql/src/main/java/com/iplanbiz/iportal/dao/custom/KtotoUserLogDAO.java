package com.iplanbiz.iportal.dao.custom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.secure.GeneratePassword;
import com.iplanbiz.core.secure.SHA256;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.GroupDAO;
import com.iplanbiz.iportal.dao.RoleDAO;
import com.iplanbiz.iportal.dao.admin.UserDAO;
import com.iplanbiz.iportal.dao.auth.EmpView;
import com.iplanbiz.iportal.dao.system.CustomSqlHouseDAO;
import com.iplanbiz.iportal.dao.system.SqlHouseExecuteDAO;
import com.iplanbiz.iportal.dao.system.SystemDAO;
import com.iplanbiz.iportal.dto.KtotoUser;
import com.iplanbiz.iportal.dto.User;
import com.iplanbiz.iportal.service.admin.UserService;
import com.iplanbiz.iportal.service.custom.KtotoUserService;

@Repository
public class KtotoUserLogDAO extends SqlSessionDaoSupport {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	DBMSService dbmsService;
	@Autowired
	DBMSDAO dbmsDAO;
	@Autowired
	UserDAO userDAO;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	public JSONArray getUserList(String team,String userNm) throws Exception {
		JSONArray resultValue = new JSONArray();
		List<HashMap< String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("K_TEAM"										, team);
			map.put("USER_NAME"										, userNm);
			listValue = getSqlSession().selectList("getKtotoUserHistList", map);
			if(listValue!=null&&listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putData = new JSONObject();
					putData.put("UNID"										, listValue.get(i).get("unid"));
					putData.put("USER_ID"									, listValue.get(i).get("user_id"));
					putData.put("USER_NAME"							, listValue.get(i).get("user_name"));
					putData.put("K_TEAM"							, listValue.get(i).get("k_team"));
					putData.put("K_TEAM_NM"							, listValue.get(i).get("k_team_nm"));
					resultValue.add(putData);
				}
			}
		}catch(Exception e){
			logger.error("ERROR : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return resultValue;
	}
	
	public JSONArray getUserLogList(String unid) throws Exception {
		JSONArray resultValue = new JSONArray();
		List<HashMap< String, Object>> list = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("UNID"										, Integer.parseInt(unid));
			list = getSqlSession().selectList("getKtotoUserLogList", map);
			if(list!=null&&list.size()!=0){
				for(int i=0;i<list.size();i++){
					JSONObject putData = new JSONObject();
					putData.put("UNID"									, list.get(i).get("unid"));
					putData.put("IDX"									, list.get(i).get("idx"));
					putData.put("STATUS_GUBN"							, list.get(i).get("status_gubn"));
					putData.put("USER_ID"								, list.get(i).get("user_id"));
					putData.put("USER_NAME"								, list.get(i).get("user_name"));
					putData.put("USER_EMAIL"							, list.get(i).get("user_email"));
					putData.put("K_OLAPID"								, list.get(i).get("k_olapid"));
					putData.put("K_PHONE"								, list.get(i).get("k_phone"));
					putData.put("K_STATUS"								, list.get(i).get("k_status"));
					putData.put("K_COMPANY"								, list.get(i).get("k_company"));
					putData.put("K_DEPT"								, list.get(i).get("k_dept"));
					putData.put("K_TEAM"								, list.get(i).get("k_team"));
					putData.put("K_STATUS_NM"							, list.get(i).get("k_status_nm"));
					putData.put("K_COMPANY_NM"							, list.get(i).get("k_company_nm"));
					putData.put("K_DEPT_NM"								, list.get(i).get("k_dept_nm"));
					putData.put("K_TEAM_NM"								, list.get(i).get("k_team_nm"));
					putData.put("K_IP_IN"								, list.get(i).get("k_ip_in"));
					putData.put("K_IP_OUT"								, list.get(i).get("k_ip_out"));
					putData.put("K_SYS"									, list.get(i).get("k_sys"));
					putData.put("K_FAILCOUNT"							, list.get(i).get("k_failcount"));
					putData.put("REMOTE_IP"								, list.get(i).get("remote_ip"));
					putData.put("REMOTE_OS"								, list.get(i).get("remote_os"));
					putData.put("REMOTE_BROWSER"						, list.get(i).get("remote_browser"));
					putData.put("UPDATE_DAT"							, list.get(i).get("update_dat"));
					putData.put("UPDATE_EMP"							, list.get(i).get("update_emp"));
					putData.put("UPDATE_EMP_NM"							, list.get(i).get("update_emp_nm"));
					resultValue.add(putData);
				}
			}
		
		}catch(Exception e){
			logger.error("ERROR : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return resultValue;
	}

	public List<LinkedHashMap<String, Object>> getUserLogListForExcel(String unid) {
		List<LinkedHashMap< String, Object>> listValue = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("UNID"										, Integer.parseInt(unid));
		listValue = getSqlSession().selectList("getKtotoUserLogList", map);
		
		return listValue;
	}
 
}
