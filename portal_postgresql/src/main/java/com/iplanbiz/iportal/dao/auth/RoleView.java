package com.iplanbiz.iportal.dao.auth;

import java.io.BufferedReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.system.CustomSqlHouseDAO; 

import hidden.org.codehaus.plexus.interpolation.util.StringUtils;
 
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableCondition; 

@Repository
public class RoleView {
 
	
	@Autowired
	private GroupView groupView; 
	
	@Autowired
	private DBMSDAO dbmsDao; 
	
	
	@Autowired
	private CustomSqlHouseDAO customSqlHouseDao;	
	
	@Autowired SqlSessionFactory sqlSessionFactory;
	
	public JSONArray getList() throws Exception {
		TableCondition iect7009 = new TableCondition();
		iect7009.put("ACCT_ID", WebConfig.getAcctID());
		
		return dbmsDao.selectTableByJSONArray(false, WebConfig.getCustomTableName("IECT7009"), iect7009 ,"GID","ASC");
	}
	
	public List<LinkedHashMap<String, Object>> getListByUserID(String userID) throws Exception {
		LinkedHashMap<String,Object> param = new LinkedHashMap<String,Object>();
		param.put("S_USER_ID", userID);
		List<LinkedHashMap<String,Object>> list = groupView.getListByUserID(userID);
		StringBuilder groupsBuilder = new StringBuilder();
		for(int i = 0; i < list.size();i++) {
			groupsBuilder.append(list.get(i).get("USER_GRP_ID"));
			if(i!=list.size()-1)
				groupsBuilder.append(",");
		}
		param.put("SI_USER_GRP_ID", groupsBuilder.toString()); 
		
		return customSqlHouseDao.executeCustomQuery("RoleView.getList", param);
	}
	public List<LinkedHashMap<String, Object>> getListByUserID(String userID,String acctID) throws Exception {
		LinkedHashMap<String,Object> param = new LinkedHashMap<String,Object>();
		param.put("S_USER_ID", userID);
		List<LinkedHashMap<String,Object>> list = groupView.getListByUserID(userID,acctID);
		StringBuilder groupsBuilder = new StringBuilder();
		for(int i = 0; i < list.size();i++) {
			groupsBuilder.append(list.get(i).get("USER_GRP_ID"));
			if(i!=list.size()-1)
				groupsBuilder.append(",");
		}
		param.put("SI_USER_GRP_ID", groupsBuilder.toString());
		return customSqlHouseDao.executeCustomQuery("RoleView.getList", param);
	}

}
