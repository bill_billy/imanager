package com.iplanbiz.iportal.dao.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;

@Repository
public class SystemDAO extends SqlSessionDaoSupport {
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	private Logger logger = LoggerFactory.getLogger(getClass());

	public List<Map<String, Object>> getSystemConfig() throws Exception{
		List<Map<String, Object>> resultMap = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			String acct_id = WebConfig.getAcctID();
			if(loginSessionInfoFactory.getObject()!=null&&loginSessionInfoFactory.getObject().getAcctID()!=null)
				acct_id = loginSessionInfoFactory.getObject().getAcctID();
			map.put("ACCT_ID"								, acct_id);
			resultMap = this.getSqlSession().selectList("getSystemConfig", map);
		}catch(Exception e){
			logger.error("ERROR", e);
			throw e;
		} 
		return resultMap;
	}
	public String getSystemValue(String keyNm) {
		String systemValue="";
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
			map.put("KEY_NAME"									, keyNm);
			systemValue = getSqlSession().selectOne("getSystemValue", map);
		}catch(Exception e){
			logger.error("ERROR", e);
			return systemValue;
		} 
		return systemValue;
	}
	public Integer setSystem(Map<String, String> queryMap) throws Exception{
		int result = 0;
		try{
			result = this.getSqlSession().update("setSystem", queryMap);
		}catch(Exception e){
			logger.error("ERROR", e);
			throw e;
		}
		return result;
	}
}
