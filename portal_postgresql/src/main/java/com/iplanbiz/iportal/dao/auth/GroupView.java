package com.iplanbiz.iportal.dao.auth;

import java.io.BufferedReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
 
import com.iplanbiz.iportal.config.WebConfig; 
import com.iplanbiz.iportal.dao.system.CustomSqlHouseDAO; 

import hidden.org.codehaus.plexus.interpolation.util.StringUtils;
 
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.session.LoginSessionInfo; 

@Repository
public class GroupView {
 
	@Autowired
	private DBMSDAO dbmsDao;
	
	@Autowired
	private CustomSqlHouseDAO customSqlHouseDao;	
	
	@Autowired SqlSessionFactory sqlSessionFactory;
	
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public List<LinkedHashMap<String, Object>> getList() throws Exception {
		return this.search("", "", loginSessionInfoFactory.getObject().getAcctID());
	}
	
	public String getStringByUserID(String userID) throws Exception {
		StringBuilder userGrpID = new StringBuilder();
		List<LinkedHashMap<String,Object>> list = this.getListByUserID(userID);
		for(int i = 0; i <list.size();i++) {
			userGrpID.append(list.get(i).get("USER_GRP_ID").toString());
			if(i!=list.size()-1) {
				userGrpID.append(",");
			}
		}
		return userGrpID.toString();
	}
	public String getStringByUserID(String columnName,String userID) throws Exception {
		StringBuilder userGrpID = new StringBuilder();
		List<LinkedHashMap<String,Object>> list = this.getListByUserID(userID);
		for(int i = 0; i <list.size();i++) {
			userGrpID.append(list.get(i).get(columnName).toString());
			if(i!=list.size()-1) {
				userGrpID.append(",");
			}
		}
		return userGrpID.toString();
	}
	
	
	public List<LinkedHashMap<String, Object>> getListByUserID(String userID) throws Exception {
		return this.search(userID, "", loginSessionInfoFactory.getObject().getAcctID());
	}
	public List<LinkedHashMap<String, Object>> getListByUserID(String userID,String acctID) throws Exception {
		return this.search(userID, "", acctID);
	}
	/**
	 * NOT USE
	 * @param userID
	 * @return
	 * @throws Exception
	 */
	public List<LinkedHashMap<String, Object>> getListByUserIDList(List<String> userID) throws Exception { 
		String userStr = org.apache.commons.lang.StringUtils.join(userID, ","); 
		return this.search(userStr, "", loginSessionInfoFactory.getObject().getAcctID());
	}
	
	
	public List<LinkedHashMap<String, Object>> search(String empIDs,String deptName, String acctID) throws Exception {
		return this.search(empIDs, deptName,"","","", acctID);
	}
	public List<LinkedHashMap<String, Object>> search(String empIDs,String deptName,String groupType,String groups,String excludeGroups, String acctID) throws Exception {
		if(empIDs==null)empIDs="";
		if(deptName==null)deptName="";
		if(groupType==null)groupType="";
		if(groups==null)groups="";
		if(excludeGroups==null)excludeGroups="";
		
		LinkedHashMap<String,Object> param = new LinkedHashMap<String,Object>();
		if(empIDs.indexOf(",")==-1) {
			param.put("S_USER_ID", empIDs);
			param.put("SI_USER_ID", "");
		}
		else {
			param.put("S_USER_ID", "");
			param.put("SI_USER_ID", empIDs);
		}
		param.put("S_GRP_TYP", groupType);
		param.put("SI_USER_GRP_ID",groups);
		param.put("SNI_USER_GRP_ID",excludeGroups);
		param.put("S_USER_GRP_NM", deptName); 
		param.put("S_ACCT_ID", acctID);
		logger.debug("groupview.search param :{}",param);
		return customSqlHouseDao.executeCustomQuery("GroupView", param);
	}
}
