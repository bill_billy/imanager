package com.iplanbiz.iportal.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.util.concurrent.ExecutionError;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dto.DashboardKpiManager;

@Repository
public class DashboardKpiManagerDAO extends SqlSessionDaoSupport {
	
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Autowired DBMSDAO dbmsDAO;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	public JSONArray getDashboardKpiList(String kpiGbn) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("KPI_GBN"												, kpiGbn);
		try{
			logger.info("Get DashBoard Kpi List Data Start");
			list = getSqlSession().selectList("getDashboardKpiList", map);
			logger.info("Get DashBoard Kpi List Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("KPI_ID"										, list.get(i).get("kpi_id"));
					value.put("KPI_NM"										, list.get(i).get("kpi_nm"));
					value.put("USE_NM"										, list.get(i).get("use_nm"));
					value.put("KPI_DIR_NM"								, list.get(i).get("kpi_dir_nm"));
					value.put("USE_YN"										, list.get(i).get("use_yn"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Dash board Kpi Data Error : {}" , e);
			e.printStackTrace(); 
			throw e;
		}
		return returnValue;
	}
	public JSONArray getDashboardKpiDetail(String kpiID) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		map.put("KPI_ID"														, kpiID);
		try{
			logger.info("Get Dashboard Kpi Detail Data Start");
			list = getSqlSession().selectList("getDashboardKpiDetail", map);
			logger.info("Get Dashboard Kpi Detail Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("KPI_ID"										, list.get(i).get("kpi_id"));
					value.put("KPI_GBN"										, list.get(i).get("kpi_gbn"));
					value.put("KPI_NM"										, list.get(i).get("kpi_nm"));
					value.put("ETC_DESC"									, list.get(i).get("etc_desc"));
					value.put("KPI_DESC"									, list.get(i).get("kpi_desc"));
					value.put("DATA_START_DAT"						, list.get(i).get("data_start_dat"));
					value.put("DATA_END_DAT"							, list.get(i).get("data_end_dat"));
					value.put("SORT_ORDER"								, list.get(i).get("sort_order"));
					value.put("MEAS_NM"									, list.get(i).get("meas_nm"));
					value.put("MEAS_CYCLE"								, list.get(i).get("meas_cycle"));
					value.put("USE_NM"										, list.get(i).get("use_nm"));
					value.put("USE_UNIT"									, list.get(i).get("use_unit"));
					value.put("COL_SYSTEM"								, list.get(i).get("col_system"));
					value.put("COL_NM"										, list.get(i).get("col_nm"));
					value.put("OWNER_USER_ID"							, list.get(i).get("owner_user_id"));
					value.put("START_DAT"									, list.get(i).get("start_dat"));
					value.put("END_DAT"									, list.get(i).get("end_dat"));
					value.put("USE_YN"										, list.get(i).get("use_yn"));
					value.put("KPI_UDC1"									, list.get(i).get("kpi_udc1"));
					value.put("KPI_UDC2"									, list.get(i).get("kpi_udc2"));
					value.put("KPI_UDC3"									, list.get(i).get("kpi_udc3"));
					value.put("KPI_UDC4"									, list.get(i).get("kpi_udc4"));
					value.put("KPI_UDC5"									, list.get(i).get("kpi_udc5"));
					value.put("SQL_DATA"									, list.get(i).get("sql_data"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Dashboard Kpi Detail Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getDashboardCheckMm(String kpiID) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		map.put("KPI_ID"														, kpiID);
		try{
			logger.info("Get Dash Board Check Month Data Start");
			list = getSqlSession().selectList("getDashboardCheckMm", map);
			logger.info("Get Dash Board Check Month Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("DISP_NM"											, list.get(i).get("disp_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Dash Board Check Month Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public String getDashBoardMaxKpiID() throws Exception{
		String returnValue = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		
		try{
			logger.info("Get Dash Board Kpi ID Start");
			HashMap<String, Object> value = getSqlSession().selectOne("getDashBoardMaxKpiID",map);
			returnValue = value.get("kpi_id").toString();
			logger.info("Get Dash Board Kpi ID End");
		}catch(Exception e){
			logger.error("Get Dash Board Kpi ID Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertDashBoardKpiManager(DashboardKpiManager dash) throws Exception{
		int returnValue = 0;
		TableObject idfe0100 = new TableObject();
		idfe0100.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID(), true);
		idfe0100.put("KPI_NM"												, dash.getKpiName());
		idfe0100.put("KPI_GBN"												, dash.getKpiGbn());
		idfe0100.put("KPI_DESC"												, dash.getBigo());
		idfe0100.put("ETC_DESC"												, dash.getSol());
		idfe0100.put("DATA_START_DAT"										, dash.getStartDat());
		idfe0100.put("DATA_END_DAT"											, dash.getEndDat());
		idfe0100.put("OWNER_USER_ID"										, dash.getOwnerUser());
		idfe0100.put("MEAS_CYCLE"											, dash.getMeasCycle());
		idfe0100.put("COL_SYSTEM"											, dash.getColSystem());
		idfe0100.put("USE_UNIT"												, dash.getUseUnit());
		idfe0100.put("SORT_ORDER"											, Integer.valueOf(dash.getSortOrder()));
		idfe0100.put("START_DAT"											, dash.getsDate());
		idfe0100.put("END_DAT"												, dash.geteDate());
		idfe0100.put("KPI_UDC1"												, dash.getKpiUdc1());
		idfe0100.put("KPI_UDC2"												, dash.getKpiUdc2());
		idfe0100.put("KPI_UDC3"												, dash.getKpiUdc3());
		idfe0100.put("KPI_UDC4"												, dash.getKpiUdc4());
		idfe0100.put("KPI_UDC5"												, dash.getKpiUdc5());
		idfe0100.put("KPI_DIR"												, dash.getKpiDir());
		idfe0100.put("USE_YN"												, dash.getUseYn());
		idfe0100.put("SQL_DATA"												, dash.getSql());
		
		TableObject deleteIdfe0101 = new TableObject();
		deleteIdfe0101.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		deleteIdfe0101.put("KPI_ID"											, dash.getKpiID());
		
		ArrayList<LinkedHashMap<String, Object>> arrayIdfe0101 = new ArrayList<LinkedHashMap<String,Object>>();
		if(dash.getMon() != null) {
			if(dash.getMon().length>0){
				for(int i=0;i<dash.getMon().length;i++){
					TableObject idfe0101 = new TableObject();
					idfe0101.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
					if(dash.getKpiID().equals("")){
						idfe0101.put("KPI_ID"											, this.getDashBoardMaxKpiID());
					}else{
						idfe0101.put("KPI_ID"											, dash.getKpiID());
					}
					idfe0101.put("DISP_NM"										, dash.getMon()[i]);
					idfe0101.put("INSERT_EMP"									, loginSessionInfoFactory.getObject().getUserId());
					idfe0101.putCurrentTimeStamp("INSERT_DAT");
					arrayIdfe0101.add(idfe0101);
				}
			}
		}
		try{
			if(dash.getKpiID().equals("")){
				idfe0100.put("KPI_ID"											, this.getDashBoardMaxKpiID());
				idfe0100.put("INSERT_EMP"									, loginSessionInfoFactory.getObject().getUserId());
				idfe0100.putCurrentTimeStamp("INSERT_DAT");
				
				logger.info("Dash Board Insert Start");
				dbmsDAO.insertTable("iportal_custom.IDFE0100", idfe0100);
				logger.info("Dash Board Insert End");
				returnValue = 0;
			}else{
				idfe0100.put("KPI_ID"											, dash.getKpiID(), true);
				idfe0100.put("UPDATE_EMP"									, loginSessionInfoFactory.getObject().getUserId());
				idfe0100.putCurrentTimeStamp("UPDATE_DAT");
				
				logger.info("Dash Board Update Start");
				dbmsDAO.updateTable("iportal_custom.IDFE0100", idfe0100.getKeyMap(), idfe0100);
				logger.info("Dash Board Update End");
				returnValue = 1;
			}
			dbmsDAO.deleteTable("iportal_custom.IDFE0101", deleteIdfe0101);
			dbmsDAO.insertTable("iportal_custom.IDFE0101",  arrayIdfe0101);
		}catch(Exception e){
			returnValue = 2;
			logger.error("Dash Board Kpi Manager Insert & Update Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int removeDashBoardKpiManager(DashboardKpiManager dash) throws Exception {
		int returnValue = 0;
		TableObject delete= new TableObject();
		delete.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		delete.put("KPI_ID"											, dash.getKpiID());
		try{
			logger.info("Dash Board Kpi Manager Delete Start");
			dbmsDAO.deleteTable("iportal_custom.IDFE0100", delete);
			dbmsDAO.deleteTable("iportal_custom.IDFE0101", delete);
			logger.info("Dash Board Kpi Manager Delete End");
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.info("Dash Board Kpi Manager Delete Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
}