package com.iplanbiz.iportal.dao.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.secure.SHA256;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.GroupDAO;
import com.iplanbiz.iportal.dao.RoleDAO;
import com.iplanbiz.iportal.dao.auth.EmpView;
import com.iplanbiz.iportal.dao.system.CustomSqlHouseDAO;
import com.iplanbiz.iportal.dao.system.SqlHouseExecuteDAO;
import com.iplanbiz.iportal.dao.system.SystemDAO;
import com.iplanbiz.iportal.dto.KtotoUser;
import com.iplanbiz.iportal.dto.User;

@Repository
public class UserDAO extends SqlSessionDaoSupport {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	DBMSService dbmsService;
	@Autowired
	SqlHouseExecuteDAO sqlHouseExecuteDAO;
	@Autowired
	DBMSDAO dbmsDAO;
	@Autowired
	RoleDAO roleDAO;
	@Autowired
	GroupDAO groupDAO;
	@Autowired
	EmpView empView;
	@Autowired
	SystemDAO systemDAO;
	@Autowired
	CustomSqlHouseDAO customSqlHouseDAO;
	
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	public String getAcctIDByAcctName(String acctName) throws Exception {
		String resultValue = "";
		try{
			TableCondition  iect9010 = new TableCondition();
			iect9010.put("ACCT_NM"								, acctName);
			List<LinkedHashMap<String,Object>> list = dbmsDAO.selectTable(false,WebConfig.getCustomTableName("IECT9010"), iect9010);
			if(list.size()!=0) {
				resultValue = list.get(0).get("ACCT_ID").toString();
			}
		}catch(Exception e){
			logger.error("ERROR" , e);
			throw e;
		}
		return resultValue;
	}
	public User getUserInfo(String empID, String acctID) {
		User user = null;
		try{
 
			LinkedHashMap<String, Object> resultList = this.empView.getLoginUserInfo(empID,acctID);
			
			if(resultList!=null && resultList.size() != 0){
				user = new User(); 
				user.setUserID(resultList.get("EMP_ID").toString());
				user.setPasswd(resultList.get("EMP_PWD").toString());
				user.setUserType(resultList.get("EMP_TYP").toString());
				user.setUserName(resultList.get("EMP_NM").toString());
				user.setUserLocale(resultList.get("EMP_LOCALE").toString()); 
			}
		}catch(Exception e){
			user = null;
			logger.error("ERROR" , e);
		}
		return user;
	}
	public String getLogginIdx() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		
		HashMap<String, Object> returnValue = getSqlSession().selectOne("getLogginIdx", map);
		return returnValue.get("idx").toString();
	}
	public void setLoginFailInfo(User user, HttpServletRequest request) throws Exception{
		try{
			TableObject iect7025_1 = new TableObject();
			iect7025_1.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID(),true);
		 
			iect7025_1.put("USER_ID"								, user.getUserID());
			iect7025_1.put("REMOTE_IP"							, request.getParameter("pIp"));
			iect7025_1.put("REMOTE_OS"							, request.getParameter("osVer"));
			iect7025_1.put("REMOTE_BROWSER"				, request.getParameter("browserNm"));
			
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7025_1"),"IDX", iect7025_1.getKeyMap(),iect7025_1);
		}catch(Exception e){
			logger.error("ERROR", e);
			throw e;
		}
	}
	public int isExistUser(User user) throws Exception{
		int resultValue = 0;
		try{
			TableCondition iect7025 = new TableCondition();
			iect7025.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
			iect7025.put("EMP_ID"										, user.getUserID());
			int cnt = dbmsDAO.countTable(WebConfig.getCustomTableName("IECT7025"), iect7025);
			resultValue = cnt;
		}catch(Exception e){
			logger.error("ERROR" , e);
			resultValue = 0;
			throw e;
		}
		return resultValue;
	}
	public HashMap<String, Object> getUserStatusAndFailCount(User user) throws Exception {
		HashMap<String, Object> resultValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
			map.put("EMP_ID"										, user.getUserID());
			resultValue = this.getSqlSession().selectOne("getUserStatusAndFailCount", map);
		}catch(Exception e){
			resultValue = null;
			logger.error("ERROR", e);
			throw e;
		}
		return resultValue;
	}
	public List<LinkedHashMap<String, Object>> getCustomAuthQuery(String query) throws Exception{
		return sqlHouseExecuteDAO.selectQuery(query); 
	}
	 
	public void resetUserStatus(User user) throws Exception{
		TableObject iect7025 = new TableObject();
		iect7025.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID(), true);
		iect7025.put("EMP_ID"											, user.getUserID(), true);
		iect7025.put("FAIL_CNT"										, 0);
		iect7025.put("EMP_STATUS"										, "OPEN");
		try{
			dbmsDAO.updateTable(WebConfig.getCustomTableName("IECT7025"), iect7025.getKeyMap(), iect7025);
		}catch(Exception e){
			logger.error("ERROR", e);
			throw e;
		}
	}
	public Map<String, Object> getAdminUserInfo(String userID) throws Exception{
		Map<String, Object> resultValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
			map.put("USER_ID"											, userID);
			resultValue = this.getSqlSession().selectOne("getAdminUserInfo", map);
		}catch(Exception e){
			resultValue = null;
			logger.error("ERROR" ,e);
			throw e;
		}
		return resultValue;
	}
	public void setLoginFailCountAdd(User user) throws Exception{
		TableObject iect7025 = new TableObject();
		iect7025.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID(), true);
		iect7025.put("EMP_ID"									, user.getUserID(), true);
		try{
			if(this.isExistUser(user) > 0){
				int cnt = Integer.parseInt(String.valueOf(this.getUserStatusAndFailCount(user).get("fail_cnt")));
				if(cnt >= (WebConfig.getLockedCount()-1)){
					iect7025.put("EMP_STATUS"				, "LOCKED");
				}else{
					iect7025.put("EMP_STATUS"				, "OPEN");
				}
				iect7025.put("FAIL_CNT"				, cnt+1);
				dbmsDAO.updateTable("iportal_custom.IECT7025", iect7025.getKeyMap(), iect7025);
			}else{
				iect7025.put("EMP_STATUS"					, "OPEN");
				iect7025.put("FAIL_CNT"						, 1);
				dbmsDAO.insertTable("iportal_custom.IECT7025", iect7025);
			}
		}catch(Exception e){
			logger.error("ERROR", e);
			throw e;
		}
	}
	public JSONArray getAdminUserList(String searchName, String value) throws Exception{
		JSONArray resultValue = new JSONArray();
		List<HashMap< String, Object>> listValue = null;
		try{
			String keyText = "USER_ID";
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
			map.put("USER_ID"									, loginSessionInfoFactory.getObject().getUserId());
			if(searchName.equals("id")){
				keyText = "USER_ID";
			}else if(searchName.equals("name")){
				keyText = "USER_NAME";
			}else if(searchName.equals("email")){
				keyText = "USER_EMAIL";
			}
			map.put("KEY"											, keyText);
			map.put("VALUE"										, value);
			listValue = getSqlSession().selectList("getAdminUserList", map);
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putData = new JSONObject();
					for (Map.Entry<String, Object> entry : listValue.get(i).entrySet()) { 
						String key = entry.getKey();
			            Object objectValue = entry.getValue();
			            logger.info("Key" + key);
			            logger.info("objectValue" + objectValue);
					};
					putData.put("UNID"										, listValue.get(i).get("unid"));
					putData.put("ACCT_ID"									, listValue.get(i).get("acct_id"));
					putData.put("USER_ID"									, listValue.get(i).get("user_id"));
					putData.put("USER_NAME"							, listValue.get(i).get("user_name"));
					putData.put("USER_POSITION"						, listValue.get(i).get("user_position"));
					putData.put("USER_EMAIL"							, listValue.get(i).get("user_email"));
					putData.put("USER_REGDATE"						, listValue.get(i).get("user_regdate"));
					putData.put("USER_LASTLOGIN"						, listValue.get(i).get("user_lastlogin"));
					putData.put("USER_STATUS"							, listValue.get(i).get("user_status"));
					putData.put("AUTHORITY"								, listValue.get(i).get("authority"));
					putData.put("FIRST_LOGIN_DATE"					, listValue.get(i).get("first_login_date"));
					putData.put("ADMIN_YN"								, listValue.get(i).get("admin_yn"));
					putData.put("LOCALE"									, listValue.get(i).get("locale"));
					putData.put("EMP_STATUS"							, listValue.get(i).get("emp_status"));
					resultValue.add(putData);
				}
			}
		}catch(Exception e){
			logger.error("ERROR : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return resultValue;
	}
	public String getAdminYn() throws Exception{
		String resultValue = "";
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		}catch(Exception e){
			
		}
		return resultValue;
	}
	public JSONArray getDetailUserInfo(String userID,String remoteIP,String remoteOS,String remoteBrowser) throws Exception{
		JSONArray resultValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
			map.put("USER_ID"									, userID);
			listValue = getSqlSession().selectList("getAdminUserInfo", map);
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putData = new JSONObject();
					putData.put("USER_ID"												, listValue.get(0).get("user_id"));
					putData.put("USER_PASSWORD"								, listValue.get(0).get("user_password"));
					putData.put("USER_NAME"										, listValue.get(0).get("user_name"));
					putData.put("USER_POSITION"									, listValue.get(0).get("user_position"));
					putData.put("USER_EMAIL"										, listValue.get(0).get("user_email"));
					putData.put("USER_REGDATE"									, listValue.get(0).get("user_regdate"));
					putData.put("USER_LASTLOGIN"									, listValue.get(0).get("user_lastlogin"));
					putData.put("USER_STATUS"										, listValue.get(0).get("user_status"));
					putData.put("ADMIN_YN"											, listValue.get(0).get("admin_yn"));
					putData.put("LOCALE"												, listValue.get(0).get("locale"));
					putData.put("UNID"													, listValue.get(0).get("unid"));
					resultValue.add(putData);
				}
			}
			map.put("UNID"											, listValue.get(0).get("unid"));
			map.put("GUBN"											, "6");
			map.put("REMOTE_IP"										, remoteIP);
			map.put("REMOTE_OS"										, remoteOS);
			map.put("REMOTE_BROWSER"								, remoteBrowser);
			map.put("UPDATE_USER" 									, loginSessionInfoFactory.getObject().getUserId());
			map.put("UPDATE_USER_NM"								, loginSessionInfoFactory.getObject().getUserName());
			this.getSqlSession().update("insertUserHistory", map);
		}catch(Exception e){
			logger.error("ERROR : {}", e);
			e.printStackTrace();
			throw e;
			
		}
		return resultValue;
	}
	public String getMaxUnid() throws Exception{
		String returnValue = "";
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
			HashMap<String, Object> returnMap = getSqlSession().selectOne("getMaxUnID", map);
			returnValue = returnMap.get("unid").toString();
		}catch(Exception e){
			logger.error("GET UNID ERROR : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int inserUser(String unID, String userID, String userName, String userPassword, String userPosition, String userEmail, String adminYn, String locale, String checkValue, String remoteIP, String remoteOS, String remoteBrowser, String orgPassword) throws Exception{
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"									, userID);
		if(orgPassword.equals(userPassword)){ 
			map.put("USER_PASSWORD"						, userPassword);
		}else{
			map.put("USER_PASSWORD"						, SHA256.getSHA256(userPassword));
		}
		map.put("USER_NAME"								, userName);
		map.put("USER_POSITION"						, userPosition);
		map.put("USER_EMAIL"								, userEmail);
		map.put("USER_STATUS"							, "정상");
		map.put("ADMIN_YN"								, adminYn);
		map.put("LOCALE"									, locale);
		map.put("REMOTE_IP"								, remoteIP);
		map.put("REMOTE_OS"								, remoteOS);
		map.put("REMOTE_BROWSER"					, remoteBrowser);
		map.put("INSERT_EMP"								, loginSessionInfoFactory.getObject().getUserId());
		map.put("UPDATE_EMP"							, loginSessionInfoFactory.getObject().getUserId());
		map.put("UPDATE_USER"							, loginSessionInfoFactory.getObject().getUserId());
		map.put("UPDATE_USER_NM"							, loginSessionInfoFactory.getObject().getUserName());
		
		HashMap<String, Object> groupMap = new HashMap<String, Object>();
		groupMap.put("ACCT_ID"							, loginSessionInfoFactory.getObject().getAcctID());
		groupMap.put("FKEY"								, userID);
		
		TableObject iect7003 = new TableObject();
		iect7003.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID(), true);
		iect7003.put("USER_ID"											, userID, true);
		if(orgPassword.equals(userPassword)){ 
			iect7003.put("USER_PASSWORD"						, userPassword);
		}else{
			iect7003.put("USER_PASSWORD"						, SHA256.getSHA256(userPassword));
		}
		iect7003.put("USER_NAME"									, userName);
		iect7003.put("USER_POSITION"								, userPosition);
		iect7003.put("USER_EMAIL"									, userEmail);
		iect7003.putCurrentDate("USER_REGDATE");
		iect7003.put("USER_STATUS"									, "정상");
		iect7003.put("ADMIN_YN"										, adminYn);
		iect7003.put("LOCALE"											, locale);
		iect7003.put("INSERT_EMP"									, loginSessionInfoFactory.getObject().getUserId());
		iect7003.putCurrentDate("INSERT_DAT");
		iect7003.put("UPDATE_EMP"									, loginSessionInfoFactory.getObject().getUserId());
	//	iect7003.putCurrentDate("PASSWORD_UPDATE_DAT");
		
		TableObject iect7010 = new TableObject();
		iect7010.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		iect7010.put("FKEY"												, userID);
		
		try{
			if(unID.equals("")){
//			IECT7003 사용자 기본 정보 저장
				iect7003.put("UNID"								, Integer.valueOf(getMaxUnid()));
				map.put("UNID"									, getMaxUnid());
				map.put("GUBN"										, "1");
				dbmsDAO.insertTable("iportal_custom.IECT7003", iect7003);
				result = 0;
			}else{
//			IECT7003 사용자 기본 정보 저장
				iect7003.put("UNID"								, Integer.valueOf(unID));
				map.put("UNID"									, unID);
				map.put("GUBN"										, "2");
				dbmsDAO.updateTable("iportal_custom.IECT7003", iect7003.getKeyMap(),iect7003);
				result = 1;
			}
			this.getSqlSession().update("insertUserHistory", map);
			dbmsDAO.deleteTable("iportal_custom.IECT7010", iect7010);
			if(!checkValue.equals("")){
				ArrayList<LinkedHashMap<String, Object>> arrayInsertIect7010 = new ArrayList<LinkedHashMap<String,Object>>();
				for(int i=0;i<checkValue.split(",").length;i++){
					TableObject insertIect7010 = new TableObject();
					insertIect7010.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
					insertIect7010.put("TYPE"									, 'u');
					insertIect7010.put("FKEY"									, userID);
					insertIect7010.put("GID"									, Integer.valueOf(checkValue.split(",")[i]));
					arrayInsertIect7010.add(insertIect7010);
				}
				dbmsDAO.insertTable("iportal_custom.IECT7010",  arrayInsertIect7010);
			}
		}catch(Exception e){
			logger.error("USER INFO INSERT ERROR : {}", e);
			e.printStackTrace();
			result = 2;
			throw e;
		}
		return result;
	}
	
	
	
	public int deleteUser(String userID, String unID, String remoteIP, String remoteOS, String remoteBrowser) throws Exception{
		int result = 0;
		TableObject deleteIect7003 = new TableObject();
		deleteIect7003.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		deleteIect7003.put("UNID"										, Integer.valueOf(unID));
		deleteIect7003.put("USER_ID"									, userID);
		
		TableObject deleteIect7008 = new TableObject();
		deleteIect7008.put("UNID"										, Integer.valueOf(unID));
		deleteIect7008.put("EMP_ID"									, userID);
		deleteIect7008.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		
		
		TableObject deleteIect7010 = new TableObject();
		deleteIect7010.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		deleteIect7010.put("FKEY"										, userID);
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
			map.put("USER_ID"										, userID);
			map.put("UNID"											, Integer.valueOf(unID));
			map.put("GUBN"											, "5");
			map.put("REMOTE_IP"										, remoteIP);
			map.put("REMOTE_OS"										, remoteOS);
			map.put("REMOTE_BROWSER"							, remoteBrowser);
			map.put("UPDATE_USER" 							,loginSessionInfoFactory.getObject().getUserId());
			map.put("UPDATE_USER_NM"							, loginSessionInfoFactory.getObject().getUserName());
			logger.info("User Log Delete Start");
			this.getSqlSession().update("insertUserHistory", map);
			logger.info("User Log Delete End");
			
			logger.info("User Info Delete Start");
			dbmsDAO.deleteTable("iportal_custom.IECT7003", deleteIect7003);
			logger.info("User Info Delete End");

			logger.info("User Group Delete Start");
			dbmsDAO.deleteTable("iportal_custom.IECT7008", deleteIect7008);
			logger.info("User Group Delete End");
			
			logger.info("User Role Delete Start");
			dbmsDAO.deleteTable("iportal_custom.IECT7010", deleteIect7010);
			logger.info("User Role Delete End");
			
			result = 0;
		}catch(Exception e){
			result = 1;
			e.printStackTrace();
			logger.error("User Log Insert&Info Delete&Group Delete&Role Delete Error : {}", e);
			throw e;
		}
		return result;
	}
	public int resetPassworedUser(String userID) throws Exception{
		int result = 0;
		TableObject iect7003 = new TableObject();
		iect7003.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID(), true);
		iect7003.put("USER_ID"										, userID, true);
		iect7003.put("USER_PASSWORD"						, "761e36be777a1888358d85cfc0b3d7349b7073ee3a514ba242481ec294ca210e");
		try{
			logger.info("User Password Reset Start");
			dbmsDAO.updateTable("iportal_custom.IECT7003", iect7003.getKeyMap(), iect7003);
			logger.info("User Password Reset End");
			result = 0;
		}catch(Exception e){
			result = 1;
			logger.error("User Password Reset Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return result;
	}
	public JSONArray excelDownByUserBackData(String userID) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
			map.put("USER_ID"									, userID);
			logger.info("User BackData Get Start");
			listValue = getSqlSession().selectList("excelDownByUserBackData", map);
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("USER_ID"							, listValue.get(i).get("user_id"));
					putValue.put("USER_NAME"						, listValue.get(i).get("user_name"));
					putValue.put("USER_POSITION"					, listValue.get(i).get("user_position"));
					putValue.put("USER_EMAIL"						, listValue.get(i).get("user_email"));
					putValue.put("ADMIN_YN"						, listValue.get(i).get("admin_yn"));
					putValue.put("LOCALE"								, listValue.get(i).get("locale"));
					returnValue.add(putValue);
				}
			}
			logger.info("User BackData Get End");
		}catch(Exception e){
			logger.error("User BackData Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int getCheckUserID ( String userID ) throws Exception {
		int returnValue = 0;
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("EMP_ID"										, userID);
		
		try {
			listValue = getSqlSession().selectList("getCheckUserID", map);
			returnValue = Integer.parseInt(listValue.get(0).get("cnt").toString());
			logger.info("Get CHECK Value = " + returnValue);
		} catch (Exception e) {
			returnValue = -1;
			e.printStackTrace();
			logger.error("ERROR > Get Check User ID Error : {}", e);
		}
		return returnValue;
	}
	public int getCheckOlapID ( String olapID) throws Exception {
		int returnValue = 0;
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("K_OLAPID"										, olapID);
		
		try {
			listValue = getSqlSession().selectList("getCheckOlapID", map);
			returnValue = Integer.parseInt(listValue.get(0).get("cnt").toString());
			logger.info("Get CHECK Value = " + returnValue);
		} catch (Exception e) {
			returnValue = -1;
			e.printStackTrace();
			logger.error("ERROR > Get Check User ID Error : {}", e);
		}
		return returnValue;
	}
	
	public String getSystemValue(String key) {
		return systemDAO.getSystemValue(key);
	}
	
	public int adminExpenses(String unId, String gubn, String remoteIp, String remoteOs, String remoteBrowser){
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"		, loginSessionInfoFactory.getObject().getAcctID());
		map.put("UPDATE_USER"	, loginSessionInfoFactory.getObject().getUserId());
		map.put("UPDATE_USER_NM", loginSessionInfoFactory.getObject().getUserName());
		map.put("UNID"       	, Integer.parseInt(unId));
		map.put("GUBN"          , gubn);
		map.put("REMOTE_IP"     , remoteIp);
		map.put("REMOTE_OS"   	, remoteOs);
		map.put("REMOTE_BROWSER", remoteBrowser);
		result = getSqlSession().update("adminExpenses",map);
		return result;
	}
	public int updateLock(String empId){
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		map.put("EMP_ID", empId);
		result = getSqlSession().update("updateLock",map);
		return result;
	}
 
}
