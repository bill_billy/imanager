package com.iplanbiz.iportal.dao.portlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;

@Repository
public class PortletGroupManagerDAO extends SqlSessionDaoSupport {
	
 
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Autowired DBMSDAO dbmsDAO;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	 public JSONArray getPortletGroupManagerList () throws Exception {
		 JSONArray returnValue = new JSONArray();
		 List<HashMap<String, Object>> list = null;
		 
		 HashMap<String, Object> map = new HashMap<String, Object>();
		 map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		 
		 try{
			 logger.info("Get Portlet Group Manager List Data Start");
			 list = getSqlSession().selectList("getPortletGroupManagerList", map);
			 logger.info("Get Portlet Group Manager List Data End");
			 
			 if(list.size() > 0){
				 for(int i=0; i<list.size(); i++){
					 JSONObject value = new JSONObject();
					 value.put("UNID"						, list.get(i).get("unid"));
					 value.put("GRP_NAME"				, list.get(i).get("grp_name"));
					 value.put("ETC"							, list.get(i).get("etc"));
					 returnValue.add(value);
				 }
			 }
		 }catch(Exception e){
			 logger.error("ERROR => Get Portlet Group Manager List Data Error : {} ", e);
			 e.printStackTrace();
			 throw e;
		 }
		 return returnValue;
	 }
	 
	 public JSONArray getPortletGroupDataDetail ( String unID ) throws Exception {
		 JSONArray returnValue = new JSONArray();
		 List<HashMap<String, Object>> list = null;
		 
		 HashMap<String, Object> map = new HashMap<String, Object>();
		 map.put("ACCT_ID"														, loginSessionInfoFactory.getObject().getAcctID());
		 map.put("UNID"															, unID);
		 
		 try{
			 logger.info("Get Portlet Group Data Detail Start");
			 list = getSqlSession().selectList("getPortletGroupDataDetail", map);
			 logger.info("Get Portlet Group Data Detail End");
			 
			 if(list.size() > 0){
				 for(int i=0;i<list.size();i++){
					 JSONObject value = new JSONObject();
					 value.put("UNID"											, list.get(i).get("unid"));
					 value.put("GRP_NAME"									, list.get(i).get("grp_name"));
					 value.put("ETC"												, list.get(i).get("etc"));
					 returnValue.add(value);
				 }
			 }
		 }catch(Exception e){
			 logger.error("ERROR => Get Portlet Group Data Detail Error : {}" , e);
			 e.printStackTrace();
			 throw e;
		 }
		 return returnValue;
	 }

	 public String getMaxPortletGrpUNID() throws Exception{
		 String returnValue = "";
		 
		 HashMap<String, Object> map = new HashMap<String, Object>();
		 map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		 
		 try{
			 logger.info("Get Max Portlet Group UNID Data Start");
			 List<HashMap<String, Object>> list = getSqlSession().selectList("getMaxPortletUNID", map);
			 logger.info("Get Max Portlet Group UNID Data End");
			 
			 if(list.size() > 0){
				 returnValue = list.get(0).get("unid").toString();
			 }
		 }catch(Exception e){
			 logger.error("ERROR => Get Max Portlet Group UNID Data Error : {}" , e);
			 e.printStackTrace();
		 }
		 return returnValue;
	 }
	 public int insertPortletGroup(String unID, String grpName, String etc) throws Exception {
		 int returnValue = 0;
		 TableObject iect7105 = new TableObject();
		 iect7105.put("ACCT_ID"							, loginSessionInfoFactory.getObject().getAcctID(), true);
		 iect7105.put("GRP_NAME"						, grpName);
		 iect7105.put("ETC"									, etc);
		 
		 try{
			 if(unID.equals("")){
				 logger.info("Insert Portlet Group Start");
				 iect7105.put("UNID"								, getMaxPortletGrpUNID());
				 dbmsDAO.insertTable("iportal_custom.IECT7105", iect7105);
				 logger.info("Inset Portlet Group End");
				 returnValue = 0;
			 }else{
				 logger.info("Update Portlet Group Start");
				 iect7105.put("UNID"								, unID, true);
				 dbmsDAO.updateTable("iportal_custom.IECT7105", iect7105.getKeyMap(), iect7105);
				 logger.info("Update Portlet Group End");
				 returnValue = 1;
			 }
		 }catch(Exception e){
			 String insertTyp = "";
			 if(unID.equals("")){
				 insertTyp = "Insert";
			 }else{
				 insertTyp = "Update";
			 }
			 returnValue = 2;
			 logger.error("Error => " + Utils.getString(insertTyp, "").replaceAll("[\r\n]","") + " Portlet Group Error : {} " ,e);
			 e.printStackTrace();
			 throw e;
		 }
		 return returnValue;
	 }
	 public int deletePortletGroup ( String[] arrayDeleteUnID ) throws Exception {
		 int returnValue = 0;
		 ArrayList<LinkedHashMap<String, Object>> arrayIECT7105 = new ArrayList<LinkedHashMap<String,Object>>();
		 
		 for( int i=0; i<arrayDeleteUnID.length; i++){
			 TableObject iect7105 = new TableObject();
			 iect7105.put("UNID"							, arrayDeleteUnID[i]);
			 arrayIECT7105.add(iect7105);
		 }
		 
		 try{
			 if(arrayDeleteUnID.length > 0){
				 logger.info("Delete Portlet Group Data Start");
				 dbmsDAO.deleteTable("iportal_custom.IECT7105", new String []{"UNID"},  arrayIECT7105);
				 logger.info("Delete Portlet Group Data End");
				 returnValue = 0;
			 }else{
				 logger.info("Delete Portlet Group Data None!");
				 returnValue = 1;
			 }
		 }catch(Exception e){
			 returnValue = 2;
			 logger.error("ERROR => Delete Portlet Group Data Error : {} " ,e);
			 e.printStackTrace();
			 throw e;
		 }
		 return returnValue;
	 }
}
