package com.iplanbiz.iportal.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.secure.SHA256;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dao.auth.GroupView;


@Repository
public class PasswordDAO extends SqlSessionDaoSupport {

	@Autowired DBMSDAO dbmsDAO;
	
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public int update(String newpwd,String oldpwd,String remoteIP, String remoteOS, String remoteBrowser){
		
		if(newpwd.equals(oldpwd)) return -1;
		TableObject iect7003 = new TableObject();
		iect7003.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID(), true);
		iect7003.put("USER_ID"										, loginSessionInfoFactory.getObject().getUserId(), true);
		iect7003.put("USER_PASSWORD"								, SHA256.getSHA256(newpwd));
		iect7003.putCurrentTimeStamp("PASSWORD_UPDATE_DAT");
		try{
			logger.info("User Password change Start");
			dbmsDAO.updateTable("iportal_custom.IECT7003", iect7003.getKeyMap(), iect7003);
			logger.info("User Password change End");
			loginSessionInfoFactory.getObject().setExternalInfo("passwordUpdateDat", iect7003.get("PASSWORD_UPDATE_DAT"));
			
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
			map.put("USER_ID"										, loginSessionInfoFactory.getObject().getUserId());
			map.put("GUBN"											, "8"); //암호변경
			map.put("REMOTE_IP"										, remoteIP);
			map.put("REMOTE_OS"										, remoteOS);
			map.put("REMOTE_BROWSER"								, remoteBrowser);
			map.put("INSERT_EMP"									, loginSessionInfoFactory.getObject().getUserId());
			map.put("UPDATE_EMP"									, loginSessionInfoFactory.getObject().getUserId());
			map.put("UPDATE_EMP_NM"									, loginSessionInfoFactory.getObject().getUserName());
			
			this.getSqlSession().update("insertUserHistory", map);
			
			HashMap<String, Object> uMap = new HashMap<String, Object>();
			uMap.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
			uMap.put("USER_ID"										, loginSessionInfoFactory.getObject().getUserId());
			Map<String, Object> unidMap = null;
			unidMap = this.getSqlSession().selectOne("getKtotoUserUnid", uMap);
			if(unidMap!=null) {
				int unid=Integer.parseInt(unidMap.get("unid").toString());
				map.put("UNID"										, unid); //암호변경
				this.getSqlSession().update("insertKtotoUserHistory", map);
			}
			return 1;
		}catch(Exception e){
			logger.error("ERROR", e);
			return -2;
		}
		
	}	
public int initPassword(String acctId, String userId, String loginId, String loginName, String newpwd, String remoteIP, String remoteOS, String remoteBrowser){
		
		TableObject iect7003 = new TableObject();
		iect7003.put("ACCT_ID"										, acctId, true);
		iect7003.put("USER_ID"										, userId, true);
		iect7003.put("USER_PASSWORD"								, SHA256.getSHA256(newpwd));
		iect7003.put("UPDATE_EMP"									, loginId);
		iect7003.put("PASSWORD_UPDATE_DAT"							, null);
		try{
			logger.info("User Password change Start");
			dbmsDAO.updateTable("iportal_custom.IECT7003", iect7003.getKeyMap(), iect7003);
			logger.info("User Password change End");
			
			
			
			
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"										, acctId);
			map.put("USER_ID"										, userId);
			map.put("GUBN"											, "10"); //암호초기화 
			map.put("REMOTE_IP"									, remoteIP);
			map.put("REMOTE_OS"									, remoteOS);
			map.put("REMOTE_BROWSER"							, remoteBrowser);
			map.put("INSERT_EMP"								, loginId);
			map.put("UPDATE_EMP"								, loginId);
			map.put("UPDATE_EMP_NM"								, loginName);
			map.put("UPDATE_USER"								, loginId);
			map.put("UPDATE_USER_NM"							, loginName);
			
			
			
			
			HashMap<String, Object> uMap = new HashMap<String, Object>();
			uMap.put("ACCT_ID"										, acctId);
			uMap.put("USER_ID"										, userId);
			Map<String, Object> unidMap = null;
			unidMap = this.getSqlSession().selectOne("getKtotoUserUnid", uMap);
			if(unidMap!=null) {
				int unid=Integer.parseInt(unidMap.get("unid").toString());
				map.put("UNID"										, unid); //암호변경
			}
			
			//this.getSqlSession().update("insertUserHistory", map);
			this.getSqlSession().update("insertKtotoUserHistory", map);
			
			return 1;
		}catch(Exception e){
			logger.error("ERROR", e);
			return -2;
		}
		
	}	
	public int equalOrgpwd(String orgpwd){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"											, loginSessionInfoFactory.getObject().getUserId());
		map.put("USER_PASSWORD"										, SHA256.getSHA256(orgpwd));
		String equalpwd = getSqlSession().selectOne("equalPassword", map);
		if(equalpwd==null) {
			return 0;
		}
		return 1;
	}
	public String passwordUpdateDat(String unid) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"											, loginSessionInfoFactory.getObject().getUserId());
		String pwdUpdatedate = getSqlSession().selectOne("getPwdUpdateDate", map);
		return pwdUpdatedate;
	}
	
}
