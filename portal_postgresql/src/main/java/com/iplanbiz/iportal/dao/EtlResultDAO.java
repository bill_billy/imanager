package com.iplanbiz.iportal.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.session.LoginSessionInfo;

@Repository
public class EtlResultDAO extends SqlSessionDaoSupport {
	
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired SqlSessionFactory sqlSessionFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public JSONArray getEtlResultList (String sourceTable, String targetTable ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("SOURCE_TABLE"											, sourceTable);
		map.put("TARGET_TABLE"											, targetTable);
		try{
			logger.info("Get ETL Result List Data Start");
			list = getSqlSession().selectList("getEtlResultList", map);
			logger.info("Get ETL Result List Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("CREATE_TABLE"										, list.get(i).get("create_table"));
					value.put("SOURCE_USER"											, list.get(i).get("source_user"));
					value.put("SOURCE_TABLE"										, list.get(i).get("source_table"));
					value.put("TARGET_USER"											, list.get(i).get("target_user"));
					value.put("TARGET_TABLE"										, list.get(i).get("target_table"));
					value.put("ETL_CNT"													, list.get(i).get("etl_cnt"));
					value.put("RESULT_TIME"											, list.get(i).get("result_time"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get ETL Result List Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getEtlResultErrorList () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		try{
			logger.info("Get ETL Result Error List Data Start");
			list = getSqlSession().selectList("getEtlResultErrorList");
			logger.info("Get ETL Result Error List Data End");
			if(list.size()>0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("CREATE_TABLE"										, list.get(i).get("create_table"));
					value.put("SOURCE_USER"											, list.get(i).get("source_user"));
					value.put("SOURCE_TABLE"										, list.get(i).get("source_table"));
					value.put("TARGET_USER"											, list.get(i).get("target_user"));
					value.put("TARGET_TABLE"										, list.get(i).get("target_table"));
					value.put("DSCMESSAGE"											, list.get(i).get("dscmessage"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get ETL Result Error List Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	
}
