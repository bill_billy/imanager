package com.iplanbiz.iportal.dao;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dao.system.SqlHouseDAO;

@Repository
public class RoleDAO extends SqlHouseDAO {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	public JSONArray getRoleList(String groupName) throws Exception {
		JSONArray resultValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
			map.put("GROUP_NAME"							, groupName);
			listValue = getSqlSession().selectList("getRoleList", map);
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("GID"									, listValue.get(i).get("gid"));
					putValue.put("GROUP_NAME"					, listValue.get(i).get("group_name"));
					resultValue.add(putValue);
				}
			}
		}catch(Exception e){
			resultValue = null;
			logger.error("ERROR : {} ", e);
			throw e;
		}
		return resultValue;
	}
	public JSONArray getRoleListAdmin(String groupName) throws Exception {
		JSONArray resultValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
			map.put("GROUP_NAME"							, groupName);
			listValue = getSqlSession().selectList("getRoleListAdmin", map);
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("GID"									, listValue.get(i).get("gid"));
					putValue.put("GROUP_NAME"					, listValue.get(i).get("group_name"));
					resultValue.add(putValue);
				}
			}
		}catch(Exception e){
			resultValue = null;
			logger.error("ERROR : {} ", e);
			throw e;
		}
		return resultValue;
	}
	public JSONArray getRoleCheckUser(String userID) throws Exception{
		JSONArray resultValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
			map.put("USER_ID"											, userID);
			listValue = getSqlSession().selectList("getRoleCheckUser", map);
			if(listValue.size() >  0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("GID"									, listValue.get(i).get("gid"));
					putValue.put("GROUP_NAME"					, listValue.get(i).get("group_name"));
					resultValue.add(putValue);
				}
			}
		}catch(Exception e){
			logger.error("e : {}", e);
			resultValue = null;
			throw e;
		}
		return resultValue;
	}
}
