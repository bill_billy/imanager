package com.iplanbiz.iportal.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.comm.SecurityUtil;
import com.iplanbiz.core.io.butler.Proxy;
import com.iplanbiz.core.io.butler.ProxyPool;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dto.schedulerManager;

@Repository
public class SchedulerManagerDAO extends SqlSessionDaoSupport {
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Resource(name="proxyPool") 	ProxyPool proxyPool;
	@Autowired DBMSDAO dbmsDAO;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	
	public JSONArray getSchedulerManagerList ( String useYn, String schStatus ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USE_YN"												, useYn);
		map.put("SCH_STATUS"										, schStatus);
		
		try{
			logger.info("Get Scheduler Manager List Data Start");
			list = getSqlSession().selectList("getSchedulerManagerList", map);
			logger.info("Get Scheduler Manager List Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("SCH_ID"									, list.get(i).get("sch_id"));
					value.put("SCH_NM"									, list.get(i).get("sch_nm"));
					value.put("SCH_STATUS"							, list.get(i).get("sch_status"));
					value.put("SCH_SNM"								, list.get(i).get("sch_snm"));
					value.put("SCH_CYC"								, list.get(i).get("sch_cyc"));
					value.put("SCH_CYC_DETAIL"								, list.get(i).get("sch_cyc_detail"));
					value.put("SCH_CNM"								, list.get(i).get("sch_cnm"));
					value.put("FROM_DAT"								, list.get(i).get("from_dat"));
					value.put("TO_DAT"									, list.get(i).get("to_dat"));
					value.put("TIME"										, list.get(i).get("time"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("Get Scheduler Manager List Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getSchedulerManagerDetail ( String schID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("SCH_ID"												, Integer.valueOf(schID));
		try{
			logger.info("Get Scheduler Manager Detail Data Start");
			list = getSqlSession().selectList("getSchedulerManagerDetail", map);
			logger.info("Get Scheduler Manager Detail Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("SCH_ID"									, list.get(i).get("sch_id"));
					value.put("SCH_NM"									, list.get(i).get("sch_nm"));
					value.put("USE_YN"									, list.get(i).get("use_yn"));
					value.put("A_TYPE"									, list.get(i).get("a_type"));
					value.put("A_TIME"									, list.get(i).get("a_time"));
					value.put("B_DAY"									, list.get(i).get("b_day"));
					value.put("C_DATE"									, list.get(i).get("c_date"));
					value.put("C_WEEK"									, list.get(i).get("c_week"));
					value.put("C_DAY"									, list.get(i).get("c_day"));
					value.put("D_MON"									, list.get(i).get("d_mon"));
					value.put("D_DATE"									, list.get(i).get("d_date"));
					value.put("D_WEEK"									, list.get(i).get("d_week"));
					value.put("D_DAY"									, list.get(i).get("d_day"));
					value.put("DIST_YN"									, list.get(i).get("dist_yn"));
					value.put("SCH_CYC"								, list.get(i).get("sch_cyc"));
					value.put("FROM_DAT"								, list.get(i).get("from_dat"));
					value.put("TO_DAT"									, list.get(i).get("to_dat"));
					value.put("FROM_TIME"							, list.get(i).get("from_time"));
					value.put("FROM_MINUTE"						, list.get(i).get("from_minute"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Scheduler Manager Detail Data Error : {} " , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getJobMappingScheduler ( String schID, String jobTyp ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		 if(schID.equals("")){
			 map.put("SCH_ID"												, "");
		 }else{
			 map.put("SCH_ID"												, schID);
		 }
		
		map.put("JOB_TYP"												, jobTyp);
		
		try{
			logger.info("Get Job Mapping Scheduler Data Start");
			list = getSqlSession().selectList("getJobMappingScheduler", map);
			logger.info("Get Job Mapping Scheduler Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("JOB_ID"										, list.get(i).get("job_id"));
					value.put("SCH_ID"										, list.get(i).get("sch_id"));
					value.put("SCH_NM"										, list.get(i).get("sch_nm"));
					value.put("JOB_NM"										, list.get(i).get("job_nm"));
					value.put("JOB_OBJ_ID"									, list.get(i).get("job_obj_id"));
					value.put("DB_KEY"										, list.get(i).get("db_key"));
					value.put("JOB_TYP"										, list.get(i).get("job_typ"));
					value.put("SORT_ORD"									, list.get(i).get("sort_ord"));
					value.put("JOB_TYP_NM"								, list.get(i).get("job_typ_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Get Job Mapping Scheduler Data Error : {} " ,e);
			throw e;
		}
		return returnValue;
	}
	public JSONArray getDBinfoListByScheduler () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		try{
			logger.info("Get DB Info List By Scheduler Data Start");
			list = getSqlSession().selectList("getDBinfoListByScheduler");
			logger.info("Get DB Info List By Scheduler Data End");
			if(list.size()>0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("DBKEY"											, list.get(i).get("dbkey"));
					value.put("DBNAME"										, list.get(i).get("dbname"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get DB Info List By Scheduler Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getSeverJobList ( String jobDist, String schID, String searchName, String dbKey ) throws Exception {
		JSONArray returnValue = new JSONArray();
		AjaxMessage msg = new AjaxMessage();
		Proxy proxy = proxyPool.createProxy();
		if(jobDist.equals("P")){
			try {
				proxy.start();
				JSONObject param = new JSONObject();
				param.put("DB_KEY",dbKey);
				msg=proxy.requestRule("etl", "ShowProcedure", param);
				returnValue = msg.getReturnArray();
			}catch(Exception e) {
				e.printStackTrace();
			}
		
			//returnValue = this.getProcedureListByScheduler();
		}
		else if(jobDist.equals("R")){
			try {
				proxy.start();
				JSONObject param = new JSONObject();
				param.put("SEARCH_TEXT",searchName);
				msg=proxy.requestRule("etl", "ShowRule", param);
				returnValue = msg.getReturnArray();
			}catch(Exception e) {
				e.printStackTrace();
			}
		
			//returnValue = this.getProcedureListByScheduler();
		}
		else if(jobDist.equals("S")){
//			Shell의 의미를 모르겠음
			returnValue = null;
		}else if(jobDist.equals("E")){
			returnValue = this.getEtlListByScheduler(searchName, schID, "Y");
		}else{
			returnValue = null;
		}
		
		return returnValue;
	}
	public JSONArray getProcedureListByScheduler () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		try{
			logger.info("Get Procedure List By Scheduler Data Start");
			list = getSqlSession().selectList("getProcedureListByScheduler");
			logger.info("Get Procedure List By Scheduler Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("JOB_OBJ_ID"										, list.get(i).get("job_obj_id"));
					value.put("JOB_OBJ_NM"									, list.get(i).get("job_obj_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Procedure List By Scheduler Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getEtlListByScheduler ( String searchName, String schID, String etlYn ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(schID==null||schID.equals("")) schID = "-1"; //공백
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("NAME"													, searchName);
		map.put("SCH_ID"												, Integer.valueOf(schID));
		map.put("ETL_YN"												, etlYn);
		
		try{
			logger.info("Get Etl List Data By Scheduler Start");
			list = getSqlSession().selectList("getEtlListByScheduler", map);
			logger.info("Get Etl List Data By Scheduler End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("JOB_OBJ_ID"									, list.get(i).get("job_obj_id"));
					value.put("JOB_OBJ_NM"								, list.get(i).get("job_obj_nm"));
					value.put("ETL_TITLE"								, list.get(i).get("etl_title"));
					value.put("TARGET_DBINFO"							, list.get(i).get("target_dbinfo"));
					value.put("TARGET_USER"								, list.get(i).get("target_user"));
					value.put("TARGET_TABLE"							, list.get(i).get("target_table"));
					value.put("SOURCE_DBINFO"							, list.get(i).get("source_dbinfo"));
					value.put("SOURCE_USER"								, list.get(i).get("source_user"));
					value.put("SOURCE_TABLE"							, list.get(i).get("source_table"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Etl List Data By Scheduler Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public String getMaxSchID () throws Exception {
		String returnValue = "";
		try{
			HashMap<String, Object> getMaxSchID = getSqlSession().selectOne("getMaxSchID");
			returnValue = getMaxSchID.get("sch_id").toString();
		}catch(Exception e){
			logger.error("ERROR > Get Max SchID Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int getMaxJobID (String acct_id, String sch_id) throws Exception {
		int returnValue = 0;
		HashMap<String,Object> map = new HashMap<String,Object>();
		map.put("ACCT_ID", acct_id);
		map.put("SCH_ID",Integer.parseInt(sch_id));
		try{
			HashMap<String, Object> getMaxJobID = getSqlSession().selectOne("getMaxJobID",map);
			returnValue = Integer.parseInt(String.valueOf(getMaxJobID.get("job_id")));
		}catch(Exception e){
			logger.error("ERROR > getJobID Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertBySchedulerManager ( schedulerManager scheduler ) throws Exception {
		int returnValue = 0;
		TableObject IECT1004 = new TableObject();
		ArrayList<LinkedHashMap<String, Object>> arrayIECT1005 = new ArrayList<LinkedHashMap<String,Object>>();
		
		String getMaxSchID = this.getMaxSchID();
		int getMaxJobId = 0;
		
		IECT1004.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID(), true);
		if((scheduler.getSchID()).equals("")){
			IECT1004.put("SCH_ID"												, Integer.valueOf(getMaxSchID), true);
			IECT1004.put("INSERT_EMP"										, loginSessionInfoFactory.getObject().getUserId());
			IECT1004.putCurrentTimeStamp("INSERT_DAT");
		}else{
			IECT1004.put("SCH_ID"												, Integer.valueOf(scheduler.getSchID()), true);
			IECT1004.put("UPDATE_EMP"										, loginSessionInfoFactory.getObject().getUserId());
			IECT1004.putCurrentTimeStamp("UPDATE_DAT");
		}
		
		IECT1004.put("SCH_NM"											, SecurityUtil.unEscape(scheduler.getSchName()));
		IECT1004.put("DIST_YN"											, scheduler.getDistYn());
		IECT1004.put("USE_YN"												, scheduler.getUseYn());
		IECT1004.put("SCH_STATUS"										, scheduler.getSchStatus());
		IECT1004.put("SCH_CYC"											, scheduler.getSchCyc());
		IECT1004.put("A_TYPE"												, scheduler.getaType());
		IECT1004.put("A_TIME"												, scheduler.getaTime());
		IECT1004.put("B_DAY"												, scheduler.getbDay());
		IECT1004.put("C_DATE"												, scheduler.getcDate());
		IECT1004.put("C_WEEK"											, scheduler.getcWeek());
		IECT1004.put("C_DAY"												, scheduler.getcDay());
		IECT1004.put("D_MON"												, scheduler.getdMon());
		IECT1004.put("D_WEEK"											, scheduler.getdWeek());
		IECT1004.put("D_DAY"												, scheduler.getdDay());
		IECT1004.put("D_DATE"												, scheduler.getdDate());
		IECT1004.put("FROM_DAT"										, scheduler.getFromDat());
		IECT1004.put("TO_DAT"											, scheduler.getToDat());
		IECT1004.put("FROM_TIME"										, scheduler.getFromTime());
		IECT1004.put("FROM_MINUTE"									, scheduler.getFromMinute());
		
		if(scheduler.getJobID().length > 0){
			for(int i=0; i<scheduler.getJobID().length;i++){
				TableObject IECT1005 = new TableObject();
				IECT1005.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
				if((scheduler.getSchID()).equals("")){
					IECT1005.put("SCH_ID"											, Integer.valueOf(getMaxSchID));
					IECT1005.put("INSERT_EMP"									, loginSessionInfoFactory.getObject().getUserId());
					IECT1005.putCurrentTimeStamp("INSERT_DAT");
				}else{
					IECT1005.put("SCH_ID"											, Integer.valueOf(scheduler.getSchID()));
					IECT1005.put("UPDATE_EMP"									, loginSessionInfoFactory.getObject().getUserId());
					IECT1005.putCurrentTimeStamp("UPDATE_DAT");
				}
				if(scheduler.getJobID()[i].equals("")) {
					if(getMaxJobId==0)
						getMaxJobId=this.getMaxJobID(loginSessionInfoFactory.getObject().getAcctID(),IECT1005.get("SCH_ID").toString());
					IECT1005.put("JOB_ID"											, getMaxJobId++);
				}else {
					IECT1005.put("JOB_ID"											, Integer.valueOf(scheduler.getJobID()[i]));
				}
				IECT1005.put("DB_KEY"											, scheduler.getDbKey()[i]);
				IECT1005.put("JOB_NM"										, scheduler.getJobName()[i]);
				IECT1005.put("JOB_OBJ_ID"									, scheduler.getJobObjID()[i]);
				IECT1005.put("JOB_TYP"										, scheduler.getJobTyp()[i]);
				IECT1005.put("SORT_ORD"									, Integer.valueOf(scheduler.getSortOrd()[i]));
	
				arrayIECT1005.add(IECT1005);
			}
		}
		
		
		TableObject deleteIECT1005 = new TableObject();
		
		try{
			if((scheduler.getSchID()).equals("")){
				logger.info("Insert Scheduler Manager Info Data Start");
				dbmsDAO.insertTable("iportal_custom.IECT1004", IECT1004);
				logger.info("Insert Scheduler Manager Info Data End");
				returnValue = 0;
			}else{
				deleteIECT1005.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
				deleteIECT1005.put("SCH_ID"										, Integer.valueOf(scheduler.getSchID()));
				
				logger.info("Update Scheduler Manager Info Data Start");
				dbmsDAO.updateTable("iportal_custom.IECT1004", IECT1004.getKeyMap(), IECT1004);
				logger.info("Update Scheduler Manager Info Data End");
				
				logger.info("Delete Scheduler JOB Data Start");
				dbmsDAO.deleteTable("iportal_custom.IECT1005", deleteIECT1005);
				logger.info("Delete Scheduler JOB Data End");
				returnValue = 1;
			}
			logger.info("Insert Scheduler Manager JOB Data Start");
			dbmsDAO.insertTable("iportal_custom.IECT1005", arrayIECT1005);
			logger.info("Insert Scheduler Manager JOB Data End");
			
		}catch(Exception e){
			logger.error("INSERT & UPDATE Scheduler Manager Info Data Error : {} " , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	
	public int deleteBySchedulerManager ( schedulerManager scheduler ) throws Exception {
		int returnValue = 0;
		TableObject DELETE = new TableObject();
		
		DELETE.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		DELETE.put("SCH_ID"									, Integer.parseInt(scheduler.getSchID()));
		
		try{
			logger.info("Delete Scheduler Manager Info Data Start");
			dbmsDAO.deleteTable("iportal_custom.IECT1004", DELETE);
			logger.info("Delete Scheduler Manager Info Data End");
			
			logger.info("Delete Scheduler Manager JOB Data Start");
			dbmsDAO.deleteTable("iportal_custom.IECT1005", DELETE);
			logger.info("Delete Scheduler Manager JOB Data End");
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			e.printStackTrace();
			logger.error("ERROR > Delete Scheduler Manager Info Data Error : {} " ,e);
			throw e;
		}
		return returnValue;
		
	}
}
