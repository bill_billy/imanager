package com.iplanbiz.iportal.dao.ktoto;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.taglibs.standard.tag.common.core.RemoveTag;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dto.Board;
import com.iplanbiz.iportal.dto.File;
import com.iplanbiz.iportal.dto.ktoto.KtotoBoard;
import com.iplanbiz.iportal.dto.ktoto.KtotoNotice;
import com.iplanbiz.iportal.service.file.FileService;

@Repository
public class KtotoBoardDAO extends SqlSessionDaoSupport {
	
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired DBMSDAO dbmsDAO;
	@Autowired FileService fileService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public JSONArray getKtotoBoardList ( String boardType, String startDat, String endDat, String titleName ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("BOARD_TYPE"									, Integer.parseInt(boardType));
		map.put("TITLE_NAME"									, titleName);
		map.put("START_DAT"										, startDat);
		map.put("END_DAT"										, endDat);
		
		
		try {
			logger.info("Get Ktoto Board List Data Start");
			list = getSqlSession().selectList("getKtotoBoardList", map);
			logger.info("Get Ktoto Board List Data End");
			if(list.size() > 0) {
				for(int i=0; i<list.size(); i++) {
					JSONObject value = new JSONObject();
					value.put("BOARD_TYPE"						, list.get(i).get("board_type"));
					value.put("BOARDNO"							, list.get(i).get("boardno"));
					value.put("BOARD_GROUP"						, list.get(i).get("board_group"));
					value.put("TITLE"							, list.get(i).get("title"));
					value.put("USERID"							, list.get(i).get("userid"));
					value.put("NAME"							, list.get(i).get("name"));
					value.put("CONTENT"							, list.get(i).get("content"));
					value.put("HITCOUNT"						, list.get(i).get("hitcount"));
					value.put("CREATEDATE"						, list.get(i).get("createdate"));
					value.put("FILE_CNT"						, list.get(i).get("file_cnt"));
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Ktoto Board List Data Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONObject getKtotoBoardDetail ( String boardType, String boardNO ) throws Exception {
		JSONObject returnValue = new JSONObject();
		HashMap<String, Object> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("BOARD_TYPE"									, Integer.parseInt(boardType));
		map.put("BOARDNO"										, Integer.parseInt(boardNO));
		
		try {
			logger.info("Get Ktoto Board Detail Data Start");
			list = getSqlSession().selectOne("getKtotoBoardDetail", map);
			logger.info("Get Ktoto Board Detail Data ENd");
			if(list.size() > 0) {
				returnValue.put("BOARD_TYPE"					, list.get("board_type"));
				returnValue.put("BOARDNO"						, list.get("boardno"));
				returnValue.put("BOARD_GROUP"					, list.get("board_group"));
				returnValue.put("USERID"						, list.get("userid"));
				returnValue.put("NAME"							, list.get("name"));
				returnValue.put("CREATEDATE"					, list.get("createdate"));
				returnValue.put("TITLE"							, list.get("title"));
				returnValue.put("CONTENT"						, list.get("content"));
			}
		} catch ( Exception e ) {
			logger.error("ERROR > Get Ktoto Board Detail Data Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getKtotoBoardFileList ( String tableNM, String boardNO ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("TABLE_NM"										, tableNM);
		map.put("BOARD_NO"										, Integer.parseInt(boardNO));
		
		try {
			logger.info("Get Ktoto Board File List Data Start");
			list = getSqlSession().selectList("getKtotoBoardFileList", map);
			logger.info("Get Ktoto Board File List Data End");
			if(list.size() > 0) {
				for(int i=0; i<list.size(); i++) {
					JSONObject value = new JSONObject();
					value.put("FILE_ID"							, list.get(i).get("file_id"));
					value.put("FILE_ORG_NAME"					, list.get(i).get("file_org_name"));
					value.put("TABLE_NM"						, list.get(i).get("table_nm"));
					value.put("BOARD_NO"						, list.get(i).get("board_no"));
					returnValue.add(value);
				}
			}
		} catch ( Exception e ) {
			logger.error("ERROR > Get Ktoto Board File List Data Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public void insertBoard ( KtotoBoard board ) throws Exception {
		TableObject boardTable = new TableObject();
		logger.info("Get Board Type = " + board.getBoardType());
		int boardNO = Integer.parseInt(String.valueOf(getKtotoMaxBoardNO(board.getBoardType()).get("max_boardno")));
		boardTable.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		boardTable.put("BOARD_TYPE"								, Integer.valueOf(board.getBoardType()));
		boardTable.put("BOARDNO"								, boardNO);
		boardTable.put("BOARD_GROUP"							, boardNO);
		boardTable.put("P_BOARDNO"								, 0);
		boardTable.put("ANSWER_LEVEL"							, 0);
		boardTable.put("TITLE"									, board.getTitle());
		boardTable.put("USERID"									, loginSessionInfoFactory.getObject().getUserId());
		boardTable.put("NAME"									, loginSessionInfoFactory.getObject().getUserName());
		boardTable.put("CONTENT"								, removeScriptTag(board.getContext()));
		boardTable.putCurrentDate("CREATEDATE");
		boardTable.putCurrentTimeStamp("INSERT_DAT");
		boardTable.put("INSERT_EMP"								, loginSessionInfoFactory.getObject().getUserId());
		
		try {
			dbmsDAO.insertTable("iportal_custom.IECT4000", boardTable);
			logger.info("boardLength = " + board.getFile().length);
			if(board.getFile().length > 0) {
				String extType[] = {};
				for ( CommonsMultipartFile cmfile : board.getFile()) {
					logger.info("boardLength =  " + board.getFile().length+"//"+cmfile.getSize());
					if(cmfile.getSize() > 0) {
						File file = new File();
						file = fileService.saveFile(cmfile, extType);
						file.setTableNm(board.getBoardType());
						file.setBoardNo(boardNO);
						fileService.insertFile(file);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Insert Board Data Start Error : {}", e.getMessage());
			throw e;
		}
	}
	public void updateBoard ( KtotoBoard board ) throws Exception {
		TableObject boardTable = new TableObject();
		boardTable.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID(), true);
		boardTable.put("BOARD_TYPE"								, Integer.parseInt(board.getBoardType()), true);
		boardTable.put("BOARDNO"								, Integer.parseInt(board.getBoardNO()), true);
		boardTable.put("BOARD_GROUP"							, Integer.parseInt(board.getBoardNO()));
		boardTable.put("TITLE"									, board.getTitle());
		boardTable.put("CONTENT"								, removeScriptTag(board.getContext()));
		boardTable.putCurrentDate("UPDATE_DAT");
		boardTable.put("UPDATE_EMP"								, loginSessionInfoFactory.getObject().getUserBrowserType());
		try {
			dbmsDAO.updateTable("iportal_custom.IECT4000", boardTable.getKeyMap(), boardTable);
			logger.info("boardLength = " + board.getFile().length);
			if(board.getFile().length > 0) {
				String extType[] = {};
				for ( CommonsMultipartFile cmfile : board.getFile()) {
					logger.info("boardLength =  " + board.getFile().length+"//"+cmfile.getSize());
					if(cmfile.getSize() > 0) {
						File file = new File();
						file = fileService.saveFile(cmfile, extType);
						file.setTableNm(board.getBoardType());
						file.setBoardNo(Integer.parseInt(board.getBoardNO()));
						fileService.insertFile(file);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Update Board Data Start Error : {}", e.getMessage());
			throw e;
		}
	}
	public void setKtotoBoardHitCount ( String boardNo, String boardType ) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("BOARD_TYPE"									, Integer.valueOf(boardType));
		map.put("BOARD_NO"										, Integer.valueOf(boardNo));
		
		try {
			getSqlSession().update("setKtotoBoardHitCount", map);
		} catch ( Exception e ) {
			e.printStackTrace();
			throw e;
		}
	}
	public void removeBoard( String boardType, String boardNO ) throws Exception {
		TableObject boardTable = new TableObject();
		boardTable.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		boardTable.put("BOARD_TYPE"								, Integer.valueOf(boardType));
		boardTable.put("BOARDNO"								, Integer.parseInt(boardNO));
		TableObject boardReply = new TableObject();
		boardReply.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		boardReply.put("BOARD_TYPE"									, Integer.valueOf(boardType));
		boardReply.put("PID"										, Integer.valueOf(boardNO));
		
		try {
			JSONArray fileList = new JSONArray();
			fileList = getKtotoBoardFileList(boardType, boardNO);
			for(int i=0; i<fileList.size(); i++) {
				JSONObject fileArray = (JSONObject) fileList.get(0);
				int fileID = Integer.parseInt(fileArray.get("FILE_ID").toString());
				fileService.removeFile(fileID);
			}
			File file = new File();
			file.setTableNm(boardType);
			file.setBoardNo(Integer.parseInt(boardNO));
			this.fileService.removeFileByBoardNo(file);
			dbmsDAO.deleteTable("iportal_custom.IECT4000", boardTable);
			dbmsDAO.deleteTable("iportal_custom.IECT4003", boardReply);
		} catch (Exception e) {
			logger.error("ERROR > Remove Board Delete Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
			
		}
	}
	public Map<String, Object> getKtotoMaxBoardNO ( String boardType ) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("BOARD_TYPE"									, Integer.valueOf(boardType));
		
		return getSqlSession().selectOne("getKtotoMaxBoardNO", map);
	}
	public JSONObject getKtotoBoardUserName () throws Exception {
		JSONObject returnValue = new JSONObject();
		HashMap<String, Object> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("EMP_ID"										, loginSessionInfoFactory.getObject().getUserId());
		
		try {
		 	logger.info("Get Ktoto Board User Name Data Start");
		 	list = getSqlSession().selectOne("getKtotoBoardUserName", map);
		 	logger.info("Get Ktoto Board User Name Data ENd");
		 	if(list.size() > 0 ) {
		 		returnValue.put("EMP_ID"						, list.get("emp_id"));
		 		returnValue.put("EMP_NM"						, list.get("emp_nm"));
		 	}
		} catch ( Exception e ) {
			logger.error("ERROR > Get Ktoto Board User Name Data Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getKtotoBoardReplyList ( String boardType, String boardNo, String startNum, String endNum ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("BOARD_TYPE"										, Integer.valueOf(boardType));
		map.put("PID"												, Integer.valueOf(boardNo));
//		map.put("START_NUM"											, Integer.valueOf(startNum));
//		map.put("END_NUM"											, Integer.valueOf(endNum));
		
		try {
			logger.info("Get Ktoto Board Reply List Data Start");
			list = getSqlSession().selectList("getKtotoBoardReplyList", map);
			logger.info("Get Ktoto Board Reply List Data End");
			if(list.size() > 0){
				for(int i=0; i<list.size(); i++) {
					JSONObject value = new JSONObject();
					value.put("RNUM"								, list.get(i).get("rnum"));
					value.put("USERID"								, list.get(i).get("userid"));
					value.put("NAME"								, list.get(i).get("name"));
					value.put("P_STEP"								, list.get(i).get("p_step"));
					value.put("CONTENT"								, list.get(i).get("content"));
					value.put("TOT_CNT"								, list.get(i).get("tot_cnt"));
					value.put("TOTAL"								, list.get(i).get("total"));
					value.put("DEPT"								, list.get(i).get("dept"));
					value.put("CREATEDATE"							, list.get(i).get("createdate"));
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Ktoto Board Reply List Data Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONObject getKtotoReplyNo ( String boardType, String boardNo ) throws Exception {
		JSONObject returnValue = new JSONObject();
		HashMap<String, Object> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("BOARD_TYPE"										, Integer.valueOf(boardType));
		map.put("PID"												, Integer.valueOf(boardNo));
		
		try {
			logger.info("Get Ktoto Reply No Data Start");
			list = getSqlSession().selectOne("getKtotoReplyNo",  map);
			if(list.size() > 0) {
				returnValue.put("MAX_P_STEP", list.get("max_p_step"));
			}
			logger.info("Get Ktoto Reply No Data End");
		} catch (Exception e) {
			logger.error("ERROR > Get Ktoto Reply No Data Error : {}",e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public void insertReply( String boardType, String boardNo, String content ) throws Exception {
		TableObject boardReply = new TableObject();
		boardReply.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		boardReply.put("BOARD_TYPE"									, Integer.valueOf(boardType));
		boardReply.put("PID"										, Integer.valueOf(boardNo));
		boardReply.put("P_STEP"										, Integer.valueOf(getKtotoReplyNo(boardType, boardNo).get("MAX_P_STEP").toString()));
		boardReply.put("P_LEVEL"									, 0);
		boardReply.put("USERID"										, loginSessionInfoFactory.getObject().getUserId());
		boardReply.put("NAME"										, loginSessionInfoFactory.getObject().getUserName());
		boardReply.put("CONTENT"									, removeScriptTag(content));
		boardReply.putCurrentTimeStamp("CREATEDATE");
		boardReply.putCurrentTimeStamp("INSERT_DAT");
		boardReply.put("INSERT_EMP"									, loginSessionInfoFactory.getObject().getUserId());
		
		try {
			logger.info("Ktoto Insert Board Reply Data Start");
			dbmsDAO.insertTable("iportal_custom.IECT4003", boardReply);
			logger.info("Ktoto Insert Board Reply Data End");
		} catch (Exception e) {
			logger.error("Ktoto Insert Board Reply Data Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
	}
	public void updateReply( String boardType, String boardNo, String pStep, String content ) throws Exception {
		TableObject boardReply = new TableObject();
		boardReply.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID(), true);
		boardReply.put("BOARD_TYPE"									, Integer.valueOf(boardType), true);
		boardReply.put("PID"										, Integer.valueOf(boardNo), true);
		boardReply.put("P_STEP"										, Integer.valueOf(pStep), true);
		boardReply.put("CONTENT"									, removeScriptTag(content));
		boardReply.putCurrentTimeStamp("UPDATE_DAT");
		boardReply.put("UPDATE_EMP"									, loginSessionInfoFactory.getObject().getUserId());
		
		try {
			logger.info("Ktoto Update Board Reply Data Start");
			dbmsDAO.updateTable("iportal_custom.IECT4003", boardReply.getKeyMap(), boardReply);
			logger.info("Ktoto Update Board Reply Data End");
		} catch (Exception e) {
			logger.error("ERROR > Ktoto Update Board Reply Data Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
	}
	public void removeReply( String boardType, String boardNo, String pStep ) throws Exception {
		TableObject boardReply = new TableObject();
		boardReply.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		boardReply.put("BOARD_TYPE"									, Integer.valueOf(boardType));
		boardReply.put("PID"										, Integer.valueOf(boardNo));
		boardReply.put("P_STEP"										, Integer.valueOf(pStep));
		
		try {
			logger.info("Ktoto Remove Board Reply Data Start");
			dbmsDAO.deleteTable("iportal_custom.IECT4003", boardReply);
			logger.info("Ktoto Remove Board Reply Data End");
		} catch (Exception e) {
			logger.error("ERROR > Ktoto Remove Board Reply Data Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
	}
	public String removeScriptTag(String str ){ 
		Matcher mat;  
		// script 처리 
		Pattern script = Pattern.compile("(?i)<\\s*(no)?script\\s*[^>]*>.*?<\\s*/\\s*(no)?script\\s*>",Pattern.DOTALL);  
		mat = script.matcher(str);  
		str = mat.replaceAll("");  
		
		return str;
	}
	
	
}
