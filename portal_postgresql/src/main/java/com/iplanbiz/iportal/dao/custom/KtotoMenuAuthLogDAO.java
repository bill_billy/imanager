package com.iplanbiz.iportal.dao.custom;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.session.LoginSessionInfo;

@Repository
public class KtotoMenuAuthLogDAO extends SqlSessionDaoSupport {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	DBMSService dbmsService;
	@Autowired
	DBMSDAO dbmsDAO;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	public JSONArray getRoleList(String roleNm, String startDat, String endDat) throws Exception {
		JSONArray resultValue = new JSONArray();
		List<HashMap< String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ROLE_NM"										, roleNm);
			map.put("STARTDAT"										, startDat);
			map.put("ENDDAT"										, endDat);
			listValue = getSqlSession().selectList("getKtotoMenuAuthLogRoleList", map);
			if(listValue!=null&&listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putData = new JSONObject();
					putData.put("ROLE_ID"										, listValue.get(i).get("role_id"));
					putData.put("ROLE_NM"									, listValue.get(i).get("role_nm"));
					resultValue.add(putData);
				}
			}
		}catch(Exception e){
			logger.error("ERROR : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return resultValue;
	}
	public JSONArray getGroupList(String groupNm, String startDat, String endDat) throws Exception {
		JSONArray resultValue = new JSONArray();
		List<HashMap< String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("GRP_NM"										, groupNm);
			map.put("STARTDAT"										, startDat);
			map.put("ENDDAT"										, endDat);
			listValue = getSqlSession().selectList("getKtotoMenuAuthLogGroupList", map);
			if(listValue!=null&&listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putData = new JSONObject();
					putData.put("GRP_ID"										, listValue.get(i).get("grp_id"));
					putData.put("GRP_NM"									, listValue.get(i).get("grp_nm"));
					resultValue.add(putData);
				}
			}
		}catch(Exception e){
			logger.error("ERROR : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return resultValue;
	}
	public JSONArray getUserList(String userNm, String startDat, String endDat) throws Exception {
		JSONArray resultValue = new JSONArray();
		List<HashMap< String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("USER_NM"										, userNm);
			map.put("STARTDAT"										, startDat);
			map.put("ENDDAT"										, endDat);
			listValue = getSqlSession().selectList("getKtotoMenuAuthLogUserList", map);
			if(listValue!=null&&listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putData = new JSONObject();
					putData.put("USER_ID"										, listValue.get(i).get("user_id"));
					putData.put("USER_NM"									, listValue.get(i).get("user_nm"));
					putData.put("GRP_ID"									, listValue.get(i).get("grp_id"));
					putData.put("GRP_NM"									, listValue.get(i).get("grp_nm"));
					resultValue.add(putData);
				}
			}
		}catch(Exception e){
			logger.error("ERROR : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return resultValue;
	}
	public JSONArray getLogList(String type,String id, String startDat, String endDat) throws Exception{
		JSONArray resultValue = new JSONArray();
		List<HashMap< String, Object>> list = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("TYPE"										, type);
			map.put("ID"										, id);
			map.put("STARTDAT"									, startDat);
			map.put("ENDDAT"									, endDat);
			list = getSqlSession().selectList("getKtotoMenuAuthLogList", map);
			if(list!=null&&list.size()!=0){
				for(int i=0;i<list.size();i++){
					JSONObject putData = new JSONObject();
					putData.put("OBJ_TYP"									, list.get(i).get("obj_typ"));
					putData.put("OBJ_TYP_NM"									, list.get(i).get("obj_typ_nm"));
					putData.put("OBJ_ID"							, list.get(i).get("obj_id"));
					putData.put("OBJ_NAME"								, list.get(i).get("obj_name"));
					putData.put("ACTION_NM"								, list.get(i).get("action_nm"));
					putData.put("TARGET_TYP"							, list.get(i).get("target_typ"));
					putData.put("TARGET_TYP_NM"								, list.get(i).get("target_typ_nm"));
					putData.put("TARGET_ID"								, list.get(i).get("target_id"));
					putData.put("TARGET_NAME"								, list.get(i).get("target_name"));
					putData.put("INSERT_DAT"								, list.get(i).get("insert_dat"));
					putData.put("INSERT_EMP"								, list.get(i).get("insert_emp"));
					resultValue.add(putData);
				}
			}
		
		}catch(Exception e){
			logger.error("ERROR : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return resultValue;
	}
	public List<LinkedHashMap<String, Object>> getLogListForExcel(String type,String id, String startDat, String endDat) {
		List<LinkedHashMap< String, Object>> listValue = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("TYPE"										, type);
		map.put("ID"										, id);
		map.put("STARTDAT"									, startDat);
		map.put("ENDDAT"									, endDat);
		listValue = getSqlSession().selectList("getKtotoMenuAuthLogList", map);
		
		return listValue;
	}
	
 
}
