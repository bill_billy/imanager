package com.iplanbiz.iportal.dao.file;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
 
import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.file.FileService;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dto.File;

@Repository("fileDao")
public class FileDao extends SqlSessionDaoSupport {
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired DBMSDAO dbmsDAO;

	
	public List<HashMap<String, String>> getFileList(File file)  {
		Map<String, Object> queryMap = new HashMap<String, Object>();
 
		queryMap.put("BOARD_NO", file.getBoardNo());
		queryMap.put("TABLE_NM", file.getTableNm());
		//queryMap.put("DIVISION", file.getDivision());
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		
		
		return getSqlSession().selectList("getFileList", queryMap);
	}
	

	public Map<String, String> getFile(int fileId) {
		Map<String, Object> queryMap = new HashMap<String, Object>();
 
		queryMap.put("FILE_ID", fileId);
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
	//	queryMap.put("DIVISION", WebConfig.getDivision());
		return super.getSqlSession().selectOne("getFile", queryMap);
	}

	public  void insertFile(File file){
		Map<String, Object> queryMap = new HashMap<String, Object>();
 
		queryMap.put("TABLE_NM", file.getTableNm());
		queryMap.put("BOARD_NO", file.getBoardNo());
//		queryMap.put("DIVISION", file.getDivision());
		queryMap.put("FILE_SAVE_NAME", file.getFileSaveName());
		queryMap.put("FILE_ORG_NAME", file.getFileOrgName());
		queryMap.put("FILE_PATH", file.getFilePath());
		queryMap.put("FILE_SIZE", file.getFileSize());
		queryMap.put("FILE_EXT", file.getFileExt());
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		
		super.getSqlSession().insert("insertFile", queryMap);
	}

	public void removeFileByBoardNo(File file) {
		Map<String, Object> queryMap = new HashMap<String, Object>();
 
		queryMap.put("TABLE_NM", file.getTableNm());
		if(file.getBoardNo() == 0 ){
			queryMap.put("BOARD_NO",Integer.parseInt(file.getDelBoardNo()));
		}else{
			queryMap.put("BOARD_NO", file.getBoardNo());
		}	
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		getSqlSession().delete("deleteFileByBoardNo", queryMap);
	}

	public void removeFile(int fileId){
		Map<String, Object> queryMap = new HashMap<String, Object>();
 
		Map<String, String> fileMap =this.getFile(fileId);
		java.io.File realFile = new java.io.File(fileMap.get("file_path")+fileMap.get("file_save_name")+"."+fileMap.get("file_ext"));
		
		int result = 0;
		FileService fileService = new FileService(WebConfig.getProperty("system.upload.SaveDir"),Long.valueOf(WebConfig.getProperty("system.upload.maxUploadSize").toString()));
		
		try {
			result = fileService.deleteFile(realFile, Utils.getString(fileMap.get("file_org_name"), ""));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(result == 0){
			queryMap.put("FILE_ID", fileId);
			queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
	//		queryMap.put("DIVISION", WebConfig.getDivision());
			
			getSqlSession().delete("deleteFile",queryMap);
		}
		
	}
	
	
}
