package com.iplanbiz.iportal.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.poi.ss.formula.functions.Value;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.auth.EmpView;
import com.iplanbiz.iportal.dao.auth.GroupView;
import com.iplanbiz.iportal.dao.auth.RoleView;

@Service("customDBDataDAO")
public class CustomDBDataDAO extends SqlSessionDaoSupport {
	
	@Autowired
	DBMSService dbmsService;
	
	@Autowired
	DBMSDAO dbmsDAO;
	
	
	@Autowired
	EmpView empView;
	
	@Autowired
	GroupView groupView;
	
	@Autowired
	RoleView roleView;
	
	@Autowired SqlSessionFactory sqlSessionFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public List<LinkedHashMap<String, Object>> customSqlHouse( String queryID, String idx , LinkedHashMap<String, Object> param) throws Exception {
		HashMap<String, Object> queryMap = new HashMap<String, Object>();
		queryMap.put("QUERYID", queryID);
		queryMap.put("IDX"    , idx);
		
		List<HashMap<String, Object>> queryList = getSqlSession().selectList("getCustomQuery", queryMap);
		String query = queryList.get(0).get("query").toString();
		String dbKey = queryList.get(0).get("dbkey").toString();
		List<LinkedHashMap<String, Object>> list = null;
		if(dbmsService.containsKey(dbKey))
			list = dbmsService.executeQuery(query, param, dbKey);
		else
			list = new ArrayList<LinkedHashMap<String, Object>>();
		return list;
	}
	public int customSqlHouseUpdate ( String queryID, String idx, LinkedHashMap<String, Object> param ) throws Exception {
		int returnValue = 0;
		HashMap<String, Object> queryMap = new HashMap<String, Object>();
		queryMap.put("QUERYID", queryID);
		queryMap.put("IDX"    , idx);
		
		List<HashMap<String, Object>> queryList = getSqlSession().selectList("getCustomQuery", queryMap);
		String query = queryList.get(0).get("query").toString();
		String dbKey = queryList.get(0).get("dbkey").toString();
		
		try {
			dbmsService.executeUpdate(query, param, dbKey);
			returnValue = 0;
		} catch ( Exception e ) {
			e.printStackTrace();
			returnValue = 1;
		}
		return returnValue;
	}
	public List<LinkedHashMap<String, Object>> customSqlHouse( String queryID, String idx , String[] column, String[] where, String[] sign, String orderBy) throws Exception {
		HashMap<String, Object> queryMap = new HashMap<String, Object>();
		queryMap.put("QUERYID", queryID);
		queryMap.put("IDX"    , idx);
		String sqlWhere = "WHERE 1=1 ";
		List<HashMap<String, Object>> queryList = getSqlSession().selectList("getCustomQuery", queryMap);
		String query = queryList.get(0).get("query").toString();
		String dbKey = queryList.get(0).get("dbkey").toString();
		for(int i=0;i<column.length;i++){
			logger.info("WHERE VALUE = " + Utils.getString(where[i], "").replaceAll("[\r\n]",""));
			if(where[i] !=  null&&where[i].trim().equals("NULL")==false) {
				if(where[i].equals("''")==false) {
					sqlWhere += " AND ";
					if(sign[i].equals("LIKE")) {
						sqlWhere += column[i] +" "+sign[i]+" "+" '%"+where[i]+"%'";
					}else if(sign[i].equals("NOT IN")){
						sqlWhere += column[i] +" "+sign[i]+" "+"("+where[i]+")";
					}else if(sign[i].equals("IN")){
						sqlWhere += column[i] +" "+sign[i]+" "+"("+where[i]+")";
					}else{
						sqlWhere += column[i] +" "+sign[i]+" "+" "+where[i];
					}	
				}
			}
		}
		query += sqlWhere + orderBy;
		logger.info("Query : {}", Utils.getString(query, "").replaceAll("[\r\n]",""));
		List<LinkedHashMap<String, Object>> list = dbmsService.executeQuery(query, dbKey);
		
		return list;
	}
	public JSONArray getCognosLoginEmpTyp ( String empID ) throws Exception {
		JSONArray returnValue = new JSONArray(); 
		try { 
			LinkedHashMap<String, Object> info = null; 
			info = empView.getLoginUserInfo(empID, WebConfig.getAcctID());
			
			if(info!=null) { 
				returnValue.add(info);
				info.remove("EMP_PWD");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Cognos Login EmpTyp Error :{}", e);
			throw e;
		}
		return returnValue;
	} 
	public JSONObject getCognosAccountEmpID ( String empID, String rownum ) throws Exception {
		JSONObject returnValue = new JSONObject(); 
		List<LinkedHashMap<String, Object>> customList = null;
 
		try {
			
			customList = empView.search(empID, "", "", WebConfig.getAcctID(), "");

			if(customList.size() > 0) { 
				returnValue.put("USER_ID"						, customList.get(0).get("EMP_ID"));
			} 
			
		} catch (Exception e) {
			logger.error("ERROR Get Cognos Account EMP ID Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONObject getConosAccountEmpEmail ( String empID, String rownum ) throws Exception {
		JSONObject returnValue = null; 
		LinkedHashMap<String, Object> info = null;
 
		try { 
			info = empView.getLoginUserInfo(empID, WebConfig.getAcctID());			
			returnValue = new JSONObject();
			returnValue.put("EMP_EMAIL", info.get("EMP_EMAIL"));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Cognos Account Emp Email Error : {}", e);
			throw e;
		}
		return returnValue;
	}
	public JSONObject getCognosAccountUserGrpID ( String empID, String rownum ) throws Exception {
		JSONObject returnValue =null; 
		List<LinkedHashMap<String, Object>> list = null; 
		
		try {
			list = groupView.search(empID, "", WebConfig.getAcctID()); 
			if(list.size() > 0) {
				returnValue = new JSONObject(list.get(0));
			}  
		} catch (Exception e) {
			logger.error("ERROR > Get Cognos Account User Grp ID : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONObject getCognosAccountRoleID ( String empID, String rownum ) throws Exception {
		JSONObject returnValue = new JSONObject();
		JSONArray returnArray = new JSONArray();
		List<LinkedHashMap<String, Object>> list = null; 
		 
		try {
			list = roleView.getListByUserID(empID,WebConfig.getAcctID()); 
			if(list.size() > 0) { 
				returnValue.put("ROLE_ID"								, list.get(0).get("GID")); 
			}
		 
		} catch (Exception e) {
			logger.error("ERROR > Get Cognos Account Role ID Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONObject getCognosAccountLocale ( String empID, String rownum ) throws Exception {
 
		LinkedHashMap<String, Object> info = null;  
		try {
			info = empView.getLoginUserInfo(empID,WebConfig.getAcctID()); 
			info.remove("EMP_PWD");
		 
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Cognos Account Locale Error {}", e);
			throw e;
		}
		return new JSONObject(info);
	}
	public JSONArray getCognosMemberShip ( String empID ) throws Exception {
		JSONArray returnValue = new JSONArray();
 
		try {
			List<LinkedHashMap<String,Object>> grpList = groupView.search(empID,"", WebConfig.getAcctID());
			List<LinkedHashMap<String,Object>> roleList = roleView.getListByUserID(empID, WebConfig.getAcctID());
			  
			if(grpList.size() > 0) {
				for(int i=0; i<grpList.size(); i++) {
					JSONObject groupJson = new JSONObject();
					groupJson.put("NAME"							, grpList.get(i).get("USER_GRP_NM"));
					groupJson.put("ID"								, grpList.get(i).get("USER_GRP_ID"));
					groupJson.put("ISGROUP"							, 1);
					groupJson.put("ISROLE"							, 0);
					returnValue.add(groupJson);
				}
			}
			if(roleList.size() > 0) {
				for(int j=0; j<roleList.size(); j++) {
					JSONObject roleJson = new JSONObject();
					roleJson.put("NAME"						, roleList.get(j).get("GROUP_NAME"));
					roleJson.put("ID"						, roleList.get(j).get("GID"));
					roleJson.put("ISGROUP"					, 0);
					roleJson.put("ISROLE"					, 1);
					returnValue.add(roleJson);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Cognos Member Ship Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getCognosQueryMember ( String userGrpID , String sortOrder) throws Exception {
		JSONArray returnValue = new JSONArray(); 
		List<LinkedHashMap<String, Object>> customList = null; 
		
		try { 
			customList = empView.search("", "", userGrpID, WebConfig.getAcctID(), "");  
			if(customList.size() > 0) {
				for(int j=0; j<customList.size(); j++) {
					JSONObject customValue = new JSONObject();
					customValue.put("ID"						, customList.get(j).get("EMP_ID"));
					customValue.put("NAME"						, customList.get(j).get("EMP_NM"));
					customValue.put("ISUSER"					, 1);
					customValue.put("ISGROUP"					, 0);
					returnValue.add(customValue);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Cognos Query Member Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getCognosQueryMemberByRole ( String gID, String sortOrder ) throws Exception {
		JSONArray returnValue = new JSONArray(); 
		List<LinkedHashMap<String, Object>> permUserList = null;
		List<LinkedHashMap<String, Object>> permGroupList = null;
		
		try {  
			TableCondition iect7010 = new TableCondition();
			iect7010.put("GID", gID);
			iect7010.put("ACCT_ID", WebConfig.getAcctID());
			iect7010.put("TYPE", "u");
			permUserList = dbmsDAO.selectTable(false,WebConfig.getCustomTableName("IECT7010"), iect7010);
			
			iect7010.put("GID", gID);
			iect7010.put("ACCT_ID", WebConfig.getAcctID());
			iect7010.put("TYPE", "p");
			permGroupList = dbmsDAO.selectTable(false,WebConfig.getCustomTableName("IECT7010"), iect7010);
			StringBuilder userBuilder = new StringBuilder();
			StringBuilder groupBuilder  = new StringBuilder();
			for(int i = 0; i < permUserList.size();i++) { 
				String fkey = permUserList.get(i).get("FKEY").toString();
				userBuilder.append(fkey);
				if(i!=permUserList.size()-1) {
					userBuilder.append(",");
				}
			}
			for(int i = 0; i < permGroupList.size();i++) { 
				String fkey = permGroupList.get(i).get("FKEY").toString();
				groupBuilder.append(fkey);
				if(i!=permGroupList.size()-1) {
					groupBuilder.append(",");
				}
			}
			List<LinkedHashMap<String,Object>> empList = empView.search(userBuilder.toString(),"", "", WebConfig.getAcctID(), "");
			List<LinkedHashMap<String,Object>> grpList = groupView.search("", "","", groupBuilder.toString(), "", WebConfig.getAcctID());
			
			for(int i=0; i<empList.size(); i++) {
				JSONObject value = new JSONObject();
				value.put("ID"									, empList.get(i).get("EMP_ID"));
				value.put("NAME"								, empList.get(i).get("EMP_NM"));	
				value.put("ISUSER"								, 1);
				value.put("ISGROUP"								, 0);
				returnValue.add(value);
			}
			for(int i=0; i<grpList.size(); i++) {
				JSONObject value = new JSONObject();
				value.put("ID"									, grpList.get(i).get("USER_GRP_ID"));
				value.put("NAME"								, grpList.get(i).get("USER_GRP_NM"));	
				value.put("ISUSER"								, 0);
				value.put("ISGROUP"								, 1);
				returnValue.add(value);
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Cognos Query Member By Role Data Error : {}", e);
			e.printStackTrace();
			throw e;
			
		}
		return returnValue;
	}
	public JSONArray getCognosQuery ( String sortOrder, String sqlWhere ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		List<LinkedHashMap<String, Object>> customList = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("SORT_ORDER"										, sortOrder);
		map.put("SQL_WHERE"											, sqlWhere);
		
		LinkedHashMap<String, Object> customMap = new LinkedHashMap<String, Object>();
		customMap.put("SORT_ORDER"									, sortOrder);
		customMap.put("SQL_WHERE"									, sqlWhere);
		
		try {
			list = getSqlSession().selectList("getCognosQuery", map);
			customList = this.customSqlHouse("cognosQueryQuery", "1", customMap);
			if(list.size() > 0) {
				for(int i=0; i<list.size(); i++) {
					JSONObject value = new JSONObject();
					for (Map.Entry<String, Object> entry : list.get(i).entrySet()) { 
						String key = entry.getKey();
			            Object objectValue = entry.getValue();
			            logger.info("Key : {}" , key);
			            logger.info("objectValue : {}" , objectValue);
					};
					value.put("ID"								, list.get(i).get("id"));
					value.put("NAME"							, list.get(i).get("name"));
					value.put("ISSQLUSER"						, list.get(i).get("issqluser"));
					value.put("ISSQLGROUP"						, list.get(i).get("issqlgroup"));
					value.put("ISSQLROLE"						, list.get(i).get("issqlrole"));
					value.put("EMP_EMAIL"						, list.get(i).get("emp_email"));
					returnValue.add(value);
				}
			}
			if(customList.size() > 0) {
				for(int j=0; j<customList.size(); j++) {
					JSONObject customValue = new JSONObject();
					customValue.put("ID"						, customList.get(j).get("ID"));
					customValue.put("NAME"						, customList.get(j).get("NAME"));
					customValue.put("ISSQLUSER"					, customList.get(j).get("ISSQLUSER"));
					customValue.put("ISSQLGROUP"				, customList.get(j).get("ISSQLGROUP"));
					customValue.put("ISSQLROLE"					, customList.get(j).get("ISSQLROLE"));
					customValue.put("EMP_EMAIL"					, customList.get(j).get("EMP_EMAIL"));
					returnValue.add(customValue);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Cognos Query : {}", e);
			throw e;
		}
		return returnValue;
	}
	public JSONArray getCrudTest () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<LinkedHashMap<String, Object>> customList = null;
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		
		try {
			customList = customSqlHouse("getPdaDataTest", "1", map);
			if(customList.size() > 0) {
				for(int i=0; i<customList.size(); i++) {
					JSONObject customValue = new JSONObject();
					customValue.put("A"						, customList.get(i).get("A"));
					customValue.put("B"						, customList.get(i).get("B"));
					returnValue.add(customValue);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get PDA Data Error : {}", e);
		}
		return returnValue;
	}
	public int insertCrudTest ( String comCod, String comName ) throws Exception {
		int returnValue = 0;
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		map.put("S_COM_COD"										, comCod);
		map.put("S_COM_NM"										, comName);
		
		try {
			returnValue = customSqlHouseUpdate("insertCrudTest", "1", map);
			
		} catch ( Exception e ) {
			e.printStackTrace();
			returnValue = 1;
		}
		return returnValue;
	}
	public int deleteCrudTest ( String comCod ) throws Exception {
		int returnValue = 0;
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		map.put("S_COM_COD"										, comCod);
		
		try {
			return customSqlHouseUpdate("deleteCrudTest", "1", map);
		} catch (Exception e) {
			returnValue = 1;
			e.printStackTrace();
		}
		return returnValue;
	}
}
