package com.iplanbiz.iportal.dao.excelPgm;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.ConnectException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.comm.SecurityUtil;
import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.butler.Proxy;
import com.iplanbiz.core.io.butler.ProxyPool;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.file.FileService;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dto.ExcelDQDTO;
import com.iplanbiz.iportal.dto.ExcelPgmDQ;

@Repository
public class ExcelPgmDQDAO extends SqlSessionDaoSupport{

	@Autowired SqlSessionFactory sqlSessionFactory;
	@Resource(name="proxyPool") 	ProxyPool proxyPool;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired DBMSDAO dbmsDAO;
	
	public JSONArray getParentCombobox() throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		try{
			logger.info("getParentCombobox Start");
			list = getSqlSession().selectList("getParentCombobox", map);
			logger.info("getParentCombobox End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject(); 
					value.put("P_CD"									, list.get(i).get("p_cd"));
					value.put("P_NM"									, list.get(i).get("p_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("getParentCombobox Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}

	public JSONArray getChildCombobox(String pCd) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("pCd", pCd);
		try{
			logger.info("getChildCombobox Start");
			list = getSqlSession().selectList("getChildCombobox", map);
			logger.info("getChildCombobox End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject(); 
					value.put("C_CD"									, list.get(i).get("c_cd"));
					value.put("C_NM"									, list.get(i).get("c_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("getChildCombobox Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}

	public JSONArray getExcelPgmDQList(String pCd, String cCd) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("acctId", loginSessionInfoFactory.getObject().getAcctID());
		map.put("pCd", pCd);
		map.put("cCd", cCd);
		try{
			logger.info("getExcelPgmDQList Start");
			list = getSqlSession().selectList("getExcelPgmDQList", map);
			logger.info("getExcelPgmDQList End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject(); 
					value.put("ACCT_ID"									, list.get(i).get("acct_id"));
					value.put("PGM_ID"									, list.get(i).get("pgm_id"));
					value.put("PGM_NM"									, list.get(i).get("pgm_nm"));
					value.put("P_CD"									, list.get(i).get("p_cd"));
					value.put("P_NM"									, list.get(i).get("p_nm"));
					value.put("C_CD"									, list.get(i).get("c_cd"));
					value.put("C_NM"									, list.get(i).get("c_nm"));
					value.put("JOB_CYC"									, list.get(i).get("job_cyc"));
					value.put("JOB_CYC_NM"									, list.get(i).get("job_cyc_nm"));
					value.put("SYSTEM_ID"									, list.get(i).get("system_id"));
					value.put("DBKEY"									, list.get(i).get("dbkey"));
					value.put("PGM_EMP"									, list.get(i).get("pgm_emp"));
					value.put("PGM_EMP_NM"									, list.get(i).get("pgm_emp_nm"));
					value.put("USE_YN"									, list.get(i).get("use_yn"));
					value.put("SORT_ORDER"									, list.get(i).get("sort_order"));
					value.put("PGM_DEF"									, list.get(i).get("pgm_def"));
					value.put("DOWN_SQL"									, list.get(i).get("down_sql"));
					value.put("INSERT_SQL"									, list.get(i).get("insert_sql"));
					value.put("UP_SQL"									, list.get(i).get("up_sql"));
					value.put("DEL_SQL"									, list.get(i).get("del_sql"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("getChildCombobox Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public  ExcelDQDTO  getExcelPgmDQView(String acctid,String pgmId) {
		Map<String, String> sqlMap = new HashMap<String, String>();
		sqlMap.put("acctId", acctid);
		sqlMap.put("pgmId", pgmId);
		
		ExcelDQDTO dto =  getSqlSession().selectOne("getExcelPgmDQView", sqlMap);
		return dto;
	}
	public  Map<String,Object>  getExcelPgmDQView2(String acctid,String pgmId) {
		Map<String, String> sqlMap = new HashMap<String, String>();
		sqlMap.put("acctId", acctid);
		sqlMap.put("pgmId", pgmId);
		
		ExcelDQDTO dto =  getSqlSession().selectOne("getExcelPgmDQView", sqlMap);
		return this.ConverObjectToMap(dto);
	}
	private Map<String,Object> ConverObjectToMap(Object obj){
		try {
			//Field[] fields = obj.getClass().getFields(); //private field는 나오지 않음.
			Field[] fields = obj.getClass().getDeclaredFields();
			Map resultMap = new HashMap();
			for(int i=0; i<=fields.length-1;i++){
				fields[i].setAccessible(true);
				resultMap.put(fields[i].getName(), fields[i].get(obj));
			}
			return resultMap;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public JSONArray getScList(String scId) throws Exception {
		JSONArray returnValue = new JSONArray();
		Map<String, String> map = new HashMap<String, String>();
		map.put("scId", scId);
		List<HashMap<String, Object>> list = null;
		try {
			logger.info("getScList Start");
			list = getSqlSession().selectList("getScList", map);
			logger.info("getScList End");
			if (list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject value = new JSONObject();
					value.put("SC_ID", list.get(i).get("sc_id"));
					value.put("SC_NM", list.get(i).get("sc_nm"));
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			logger.error("getScList Error : {} ", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}

	public JSONArray getUserList(String scId, String searchText) throws Exception {
		JSONArray returnValue = new JSONArray();
		Map<String, String> map = new HashMap<String, String>();
		map.put("acctId", loginSessionInfoFactory.getObject().getAcctID());
		map.put("scId", scId);
		map.put("searchText", searchText);
		List<HashMap<String, Object>> list = null;
		try {
			logger.info("getScList Start");
			list = getSqlSession().selectList("getUserList", map);
			logger.info("getScList End");
			if (list.size() > 0) {
				for (int i = 0; i < list.size(); i++) {
					JSONObject value = new JSONObject();
					value.put("DEPTCD", list.get(i).get("deptcd"));
					value.put("DEPT_NM", list.get(i).get("dept_nm"));
					value.put("NAMEHAN", list.get(i).get("namehan"));
					value.put("EMPNO", list.get(i).get("empno"));
					value.put("WKGD_NAME", list.get(i).get("wkgd_name"));
					value.put("JIKWI_NM", list.get(i).get("jikwi_nm"));
					value.put("BOJIC_NM", list.get(i).get("bojic_nm"));
					value.put("ROWNUM", list.get(i).get("rownum"));
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			logger.error("getScList Error : {} ", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public List<Map<String, String>> getDownloadExcelData(String query) throws DataAccessException{
		List<Map<String, String>> excelData = null;
		try{
			excelData = getSqlSession().selectList("getExcelData",query);
		}catch(DataAccessException e){
			e.printStackTrace();
			return null;
		}
		return excelData;
	}
	public AjaxMessage uploadExcelDQ(CommonsMultipartFile upFile, String pgmId, String yymmdd) throws Exception {		
		return uploadExcelDQByAcctID(upFile,loginSessionInfoFactory.getObject().getAcctID(), pgmId, yymmdd);	
	}
	public AjaxMessage uploadExcelDQByAcctID(CommonsMultipartFile upFile,String acctid, String pgmId, String yymmdd) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		IOException ie = new IOException();
		FileService fileService = new FileService(WebConfig.getProperty("system.upload.SaveDir"),Long.valueOf(WebConfig.getProperty("system.upload.maxUploadSize").toString()));
		String extType[] = {};
		
		String fileSaveName = fileService.setFileName();
		
		// 파일 확장자 가져오기
		String fileOrgName = upFile.getOriginalFilename();
		String fileExt =  fileOrgName.substring(fileOrgName.lastIndexOf(".")+1);
		
		//Proxy
		Proxy proxy = proxyPool.createProxy();
		
		try {
			proxy.start();
			/**
			String pgmID = this.parameters.get("PGM_ID").toString();
			String acctID = this.parameters.get("ACCT_ID").toString();
			String division = this.parameters.get("DIVISION").toString(); 	
			String uploadPath=this.parameters.get("UPLOADPATH").toString();
			String fileName=this.parameters.get("SAVEDFILENAME").toString();
			String fileExt=this.parameters.get("FILEEXT").toString(); 
			**/
			int fileResult = fileService.saveFile(upFile, extType, fileSaveName);
			if(fileResult!=0) {
				throw ie;
			} 
			JSONObject param = new JSONObject();
			param.put("PGM_ID",pgmId);
			param.put("ACCT_ID",loginSessionInfoFactory.getObject().getAcctID());
			param.put("UPLOADPATH",fileService.getUpLoadPath());
			param.put("SAVEDFILENAME",fileSaveName); 
			param.put("FILEEXT",fileExt); 
			 
			msg = proxy.requestRule("excel", "UploadToTempDB_V2", param);
			System.out.println("result:!!"+msg.getReturnText());
			
			if(msg.getReturnText().equals("FileTypeError")) {
				return msg;
			}else if(msg.getReturnText().equals("FileUploadError")) {
				return msg;
			}
			java.io.File realFile = new java.io.File(fileService.getUpLoadPath() + fileSaveName + "."+fileExt);
			fileService.deleteFile(realFile, Utils.getString(upFile.getOriginalFilename(), ""));
		
			
		}catch(IOException e1) {
			msg = new AjaxMessage();
			msg.setExceptionText("FileException"); 
		}
		catch(Exception e2){
			msg = new AjaxMessage();
			msg.setExceptionText("ConnectionException"); 
		} finally{
			proxy.stop();
		}
		return msg;
		
	}
	public AjaxMessage executeExcelPgmDQRun(String ac, String pgmId, String yymmdd) {
		AjaxMessage msg = new AjaxMessage();
		
		Proxy proxy = proxyPool.createProxy();
		
		
		try {
			proxy.start();
			/*
			String pgmID = this.parameters.get("PGM_ID").toString();
			String acctID = this.parameters.get("ACCT_ID").toString();
			String division = this.parameters.get("DIVISION").toString();
			String yyyymmdd = this.parameters.get("YYYYMMDD").toString();	
			String ac       = this.parameters.get("AC").toString();
			*/
			JSONObject param = new JSONObject();
			param.put("PGM_ID",pgmId);
			param.put("PARAMNAME","@YYMMDD");
			param.put("ACCT_ID",this.loginSessionInfoFactory.getObject().getAcctID());
			param.put("PARAMVALUE", yymmdd);
			param.put("AC",ac);  
			 
			msg = proxy.requestRule("excel", "UploadToRealDB_V2", param); 
			
		}catch(Exception e){
			msg = new AjaxMessage();
			msg.setExceptionText("ConnectionException"); 
		} finally{
			proxy.stop();
		}
		return msg; 
	}

	public int saveExcelPgmDQ(ExcelPgmDQ excelPgmDq) throws DataAccessException{
		excelPgmDq.setAcctId(loginSessionInfoFactory.getObject().getAcctID());
		excelPgmDq.setInsertEmp(loginSessionInfoFactory.getObject().getUserId());
		
		//pgmid 중복체크
		int dupCheck = getSqlSession().selectOne("pgmIdDupCheck", excelPgmDq);
		if(excelPgmDq.getSystemid()==null) excelPgmDq.setSystemid(0);  
		if(dupCheck > 0) {
			return -1;
		} 
		
		int result = getSqlSession().insert("insertExcelPgmDq", excelPgmDq);
		
		return result;
		
	}
	public int updateExcelPgmDQ(ExcelPgmDQ excelPgmDq) throws DataAccessException{
		excelPgmDq.setAcctId(loginSessionInfoFactory.getObject().getAcctID());
		excelPgmDq.setUpdateEmp(loginSessionInfoFactory.getObject().getUserId());
		return getSqlSession().update("updateExcelPgmDq", excelPgmDq);
	}
	
	public int deleteExcelPgmDQ(ExcelPgmDQ excelPgmDq) throws DataAccessException{
		excelPgmDq.setAcctId(loginSessionInfoFactory.getObject().getAcctID());
		
		return getSqlSession().delete("deleteExcelPgmDq", excelPgmDq);
	}

	public Map<String,Object> getDownloadExcelData(String dbKey, String query) {
		Map<String,Object> map = new HashMap<String,Object>();
		AjaxMessage msg = new AjaxMessage();
		
		Proxy proxy = proxyPool.createProxy();
	
		
		try {
			proxy.start();
			
			JSONObject param = new JSONObject();
			param.put("DBKEY",dbKey);
			param.put("QUERY",SecurityUtil.unEscape(query));
			
			msg = proxy.requestRule("excel", "DataSource", param); 
			
			List<String> columnNm = new ArrayList<String>();
			List<Map<String, String>> data = new ArrayList<Map<String,String>>();
			String result = msg.getReturnText();//응답결과(오류타입)
			result = result.replaceAll("(\r\n|\r|\n|\n\r)", " ");
			map.put("result", result);
			if(msg.getReturnCode().equals("EXCEPTION")) {
				return map;
			}
			JSONArray header = (JSONArray) msg.getReturnObject().get("header");//컬럼명
			JSONArray body = (JSONArray) msg.getReturnObject().get("body");//데이터
			//1.header->columnNm
			for(int i=0;i<header.size();i++) {
				JSONObject obj=(JSONObject) header.get(i);
				String colName = obj.get("ColumnName").toString();
				columnNm.add(colName);
			}
			//2.body->data
			for(int i=0;i<body.size();i++) {
				JSONObject obj=(JSONObject) body.get(i);
				Map<String,String> temp=new HashMap<String,String>();
				for(int j=0;j<columnNm.size();j++) {
					String key = columnNm.get(j);
					String value = obj.get(key).toString();
					temp.put(key, value);
				}
				data.add(temp);
			}
			//3.결과물->map return
			map.put("columnNm", columnNm);
			map.put("data", data);
			
			System.out.println("header::::"+columnNm);
			System.out.println("body::::"+data);
			
			return map;
			
		}catch(Exception e){
			e.printStackTrace();
			map.put("result", "ConnectionException");
		}
		finally{
			proxy.stop();
		}
		return map;
	}

	
}
