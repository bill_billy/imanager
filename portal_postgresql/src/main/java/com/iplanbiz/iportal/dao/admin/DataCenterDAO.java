package com.iplanbiz.iportal.dao.admin;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;

@Repository
public class DataCenterDAO extends SqlSessionDaoSupport {
	
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Autowired DBMSDAO dbmsDAO;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	
	public JSONArray getInfoDataList(String searchComment, String searchTableName, String yyyy) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("YYYY"										, yyyy);
		map.put("SEARCH_COMMENT"					, searchComment);
		map.put("SEARCH_TABLENAME"				, searchTableName);
		try{
			logger.info("Get Info Data Start");
			listValue = getSqlSession().selectList("getInfoDataList", map);
			logger.info("Get Info Data End");
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("IDX"										, listValue.get(i).get("idx"));
					putValue.put("USERID"									, listValue.get(i).get("userid"));
					putValue.put("TABLE_NAME"							, listValue.get(i).get("table_name"));
					putValue.put("TABLE_COMMENT"					, listValue.get(i).get("table_comment"));
					putValue.put("DT_STARTDAT"						, listValue.get(i).get("dt_startdat"));
					putValue.put("DT_ENDDAT"							, listValue.get(i).get("dt_enddat"));
					putValue.put("UP_STARTDAT"						, listValue.get(i).get("up_startdat"));
					putValue.put("UP_ENDDAT"							, listValue.get(i).get("up_enddat"));
					putValue.put("BIGO"									, listValue.get(i).get("bigo"));
					putValue.put("INSERT_DAT"							, listValue.get(i).get("insert_dat"));
					putValue.put("INSERT_EMP"							, listValue.get(i).get("insert_emp"));
					putValue.put("USER_NAME"							, listValue.get(i).get("user_name"));
					returnValue.add(putValue);
				}
			}
		}catch(Exception e){
			logger.error("Get Info Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getDataInfoTableList(String searchOwner, String searchTableName, String searchComment) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("SEARCH_OWNER"													, searchOwner);
		map.put("SEARCH_TABLENAME"											, searchTableName);
		map.put("SEARCH_COMMENT"												, searchComment);
		try{
			logger.info("Get Data Info Table List Start");
			list = getSqlSession().selectList("getDataInfoTableList", map);
			logger.info("Get Data Info Table List End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("OWNER"										, list.get(i).get("schemaname"));
					value.put("TABLE_NAME"								, list.get(i).get("relname"));
					value.put("TABLE_COMMENTS"						, list.get(i).get("description"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Data Info Table List Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getInfoDataDetail( String idx) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String ,Object> map = new HashMap<String, Object>();
		map.put("IDX"											, Integer.valueOf(idx));
		try{
			logger.info("Get Info Data Detail Start");
			list = getSqlSession().selectList("getInfoDataDetail", map);
			logger.info("Get Info Data Detail End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("IDX"														, list.get(i).get("idx"));
					value.put("USERID"													, list.get(i).get("userid"));
					value.put("TABLE_NAME"											, list.get(i).get("table_name"));
					value.put("TABLE_COMMENT"									, list.get(i).get("table_comment"));
					value.put("DT_STARTDAT"											, list.get(i).get("dt_startdat"));
					value.put("DT_ENDDAT"											, list.get(i).get("dt_enddat"));
					value.put("UP_STARTDAT"											, list.get(i).get("up_startdat"));
					value.put("UP_ENDDAT"											, list.get(i).get("up_enddat"));
					value.put("BIGO"														, list.get(i).get("bigo"));
					value.put("INSERT_DAT"											, list.get(i).get("insert_dat"));
					value.put("INSERT_EMP"											, list.get(i).get("insert_emp"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Info Data Detail Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public String getMaxIdx() throws Exception{
		HashMap<String, Object> value = getSqlSession().selectOne("getMaxInfoDataIdx");
		
		return value.get("idx").toString();
	}
	public int insertInfoData( String idx, String userID, String tableName, String tableComment, String dtStartDat, String dtEndDat, String upStartDat, String upEndDat, String bigo) throws Exception{
		int returnValue = 0;
		//logger.info("Param DT_STARTDAT = " + dtStartDat + "DT_ENDDAT = " + dtEndDat + "UpStartDate = " + upStartDat + " Up End Date = " + upEndDat);
		SimpleDateFormat boforeDate = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
		//SimpleDateFormat formatDateH = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dtStartDate = boforeDate.parse(dtStartDat+"0101");
		Date dtEndDate = boforeDate.parse(dtEndDat+"1231");
		Date upStartDate = formatDate.parse(upStartDat+" 00:00:00");
		Date upEndDate = formatDate.parse(upEndDat+" 00:00:00");
		
		java.sql.Date dtStartDateSql = new java.sql.Date(dtStartDate.getTime());
		java.sql.Date dtEndDateSql = new java.sql.Date(dtEndDate.getTime());
		java.sql.Date upStartDateSql = new java.sql.Date(upStartDate.getTime());
		java.sql.Date upEndDateSql = new java.sql.Date(upEndDate.getTime());
		
	//	logger.info("DT_STARTDAT = " + dtStartDateSql.toString().replaceAll("[\r\n]","") + "DT_ENDDAT = " + dtEndDateSql.toString().replaceAll("[\r\n]","") + "UpStartDate = " + upStartDateSql.toString().replaceAll("[\r\n]","") + " Up End Date = " + upEndDateSql.toString().replaceAll("[\r\n]",""));
		TableObject idwd0000 = new TableObject();
		idwd0000.put("USERID"																	, userID);
		idwd0000.put("TABLE_NAME"															, tableName);
		idwd0000.put("TABLE_COMMENT"													, tableComment);
		if(!dtStartDat.equals("")){
			idwd0000.put("DT_STARTDAT"														, dtStartDateSql);
			idwd0000.put("DT_ENDDAT"															, dtEndDateSql);
		}
		idwd0000.put("UP_STARTDAT"														, upStartDateSql);
		idwd0000.put("UP_ENDDAT"															, upEndDateSql);
		idwd0000.put("BIGO"																	, bigo);
		try{
			if(idx.equals("")){
				idwd0000.put("IDX"																, Integer.valueOf(this.getMaxIdx()));
				idwd0000.put("INSERT_EMP"													, loginSessionInfoFactory.getObject().getUserId());
				idwd0000.putCurrentDate("INSERT_DAT");
				logger.info("Insert Info Data Start");
				dbmsDAO.insertTable("iportal.IDWD0000", idwd0000);
				logger.info("Insert Info Data End");
				returnValue = 0;
			}else{
				idwd0000.put("IDX"																, Integer.valueOf(idx), true);
				idwd0000.put("UPDATE_EMP"												, loginSessionInfoFactory.getObject().getUserId());
				idwd0000.putCurrentDate("UPDATE_DAT");
				logger.info("Update Info Data Start");
				dbmsDAO.updateTable("iportal.IDWD0000", idwd0000.getKeyMap(), idwd0000);
				logger.info("Update Info Data End");
				returnValue = 1;
			}
		}catch(Exception e){
			returnValue = 2;
			logger.error("Info Data Insert/Update Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int removeInfoData(String idx) throws Exception{
		int returnValue = 0;
		TableObject idwd0000 = new TableObject();
		idwd0000.put("IDX"												, Integer.valueOf(idx));
		
		try{
			logger.info("Remove Info Data Start");
			dbmsDAO.deleteTable("iportal.IDWD0000", idwd0000);
			logger.info("Remove Info Data End");
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("Remove Info Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
}
