package com.iplanbiz.iportal.dao.cognos;

import java.io.BufferedReader;
import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cognos.developer.schemas.bibus._3.BaseClass;
import com.cognos.developer.schemas.bibus._3.PropEnum;
import com.cognos.developer.schemas.bibus._3.QueryOptions;
import com.cognos.developer.schemas.bibus._3.SearchPathMultipleObject;
import com.cognos.developer.schemas.bibus._3.Shortcut;
import com.cognos.developer.schemas.bibus._3.Sort;
import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.comm.CodeManager;
import com.iplanbiz.iportal.comm.cognos.CRNConnect;
import com.iplanbiz.iportal.comm.cognos.MenuUtil;
import com.iplanbiz.iportal.comm.util.CognosInfo;
import com.iplanbiz.iportal.comm.util.CognosUtil;
import com.iplanbiz.iportal.dto.Menu;
import com.iplanbiz.iportal.service.CognosService;

import ch.qos.logback.core.net.LoginAuthenticator;

@Repository
public class CognosAuthorityDAO extends SqlSessionDaoSupport {
	
	@Autowired DBMSDAO dbmsDAO;
	@Autowired CognosService cognosService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	public JSONArray getMenuListByCognos ( String userID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"										, userID);
		
		try {
			logger.info("Get Menu List By Cognos Data Start");
			list = getSqlSession().selectList("getMenuListByCognos", map);
			logger.info("Get Menu List By Cognos Data End");
			if(list.size() > 0) {
				for(int i=0; i<list.size(); i++) {
					JSONObject value = new JSONObject();
					value.put("PROG_ID"							, list.get(i).get("prog_id"));
					value.put("SOURCE_OWNER"					, list.get(i).get("source_owner"));
					value.put("C_ID"							, list.get(i).get("c_id"));
					value.put("MENU_TYPE"						, list.get(i).get("menu_type"));
					value.put("PID"								, list.get(i).get("pid"));
					value.put("NAME"							, list.get(i).get("name"));
					value.put("P_NAME"							, list.get(i).get("p_name"));
					value.put("SORT_ORDER"						, list.get(i).get("sort_order"));
					value.put("CHARGE_DEPT_CD"					, list.get(i).get("charge_dept_cd"));
					value.put("CHARGE_DEPT"						, list.get(i).get("charge_dept"));
					value.put("CHARGE_EMP"						, list.get(i).get("chatge_emp"));
					value.put("UPDATE_PER"						, list.get(i).get("update_per"));
					value.put("C_LINK"							, list.get(i).get("c_link"));
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Menu List By Cognos Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getCognosChildListByArray( String storeID, String defaultRootSearchPath, CRNConnect conn, String ac ){
		JSONArray returnValue = new JSONArray();
		
		SearchPathMultipleObject cmSearchPath = new SearchPathMultipleObject(defaultRootSearchPath);
		 
		JSONArray folderList = new JSONArray();
		JSONArray reportList = new JSONArray();
		JSONArray childrenList = new JSONArray();
		
		PropEnum[] properties = { PropEnum.defaultName, PropEnum.searchPath, PropEnum.objectClass, PropEnum.hasChildren, PropEnum.storeID, PropEnum.uri, PropEnum.target };
		String appendString = "/*[ permission('read') and permission('execute') ]";
		
		BaseClass myCMObject = null;
		BaseClass[] children = null;
		
		Sort[] nodeSorts = CognosUtil.getSortByDisplaySequence();
		
		try {
			String passport = CognosUtil.getPassport(conn);
			logger.debug("PASSPORT : {}", Utils.getString(passport, "").replaceAll("[\r\n]","") );
			logger.debug("myCMObject.length : {}", String.valueOf(conn.getCMService(passport).query(cmSearchPath, properties, nodeSorts, new QueryOptions()).length));
			myCMObject = conn.getCMService(passport).query(cmSearchPath, properties, nodeSorts,  new QueryOptions())[0];
			
			String searchPath = myCMObject.getSearchPath().getValue();
			
			if(myCMObject.getHasChildren().isValue()) {
				cmSearchPath.set_value(searchPath + appendString);
				children = conn.getCMService(passport).query(cmSearchPath, properties, nodeSorts, new QueryOptions());
				
				if(children.length > 0) {
					for(int i=0; i<children.length;i++) {
						String icon = "";
						String reportType = children[i].getObjectClass().getValue().toString();
						String reportName = children[i].getDefaultName().getValue();
						String reportClink = children[i].getSearchPath().getValue();
						String reportSearchPath = children[i].getSearchPath().getValue();
						if(reportType.equals("shortcut")){
							String scTargetSearchPath = ((Shortcut)children[i]).getTarget().getValue()[0].getSearchPath().getValue();
							
							SearchPathMultipleObject cmScSearchPath = new SearchPathMultipleObject(scTargetSearchPath);
							BaseClass shorcutTagetBcArr[] = conn.getCMService(passport).query(cmScSearchPath, properties, nodeSorts, new QueryOptions());
							BaseClass shorcutTargetBc = null;
							if(shorcutTagetBcArr.length > 0){
								shorcutTargetBc = shorcutTagetBcArr[0];
								icon = getShortCutReportImg(shorcutTargetBc.getObjectClass().getValue().toString());
							}else{
								continue;
							}
						}else{
							icon = getReportImg(reportType);
						}
						JSONObject value = new JSONObject();
						value.put("C_ID"							, children[i].getStoreID().getValue().toString());
						value.put("P_ID"							, storeID);
						value.put("id"								, children[i].getStoreID().getValue().toString());
						value.put("C_NAME"							, children[i].getDefaultName().getValue());
						value.put("C_LINK"							, children[i].getSearchPath().getValue());
						value.put("text"							, children[i].getDefaultName().getValue());
						value.put("cIcon"							, icon);
						value.put("reportType"						, reportType);
						value.put("cType"							, reportType);
						value.put("SOURCE_OWNER"					, "BIFOLDER");
						value.put("MENU_TYPE"						, "folder".equals(reportType) ? "1":"2");
						
						if(children[i].getHasChildren().isValue() && !ac.equals("top")) {
							if(children[i].getHasChildren().isValue() && "folder".equals(reportType)){
//								JSONObject items = new JSONObject();
								
								value.put("hasChildren"				, true);
								
								String childSearchPath = children[i].getSearchPath().getValue();
								String childPerStoreId = children[i].getStoreID().getValue().toString();
								childrenList.addAll(getCognosChildListByArray(childPerStoreId, childSearchPath, conn, ac));
//								menu.put("items",); 
							}
							else{
								value.put("hasChildren"				, false);
							}
						}
						if("folder".equals(reportType)) {
							folderList.add(value);
						}else {
							reportList.add(value);
						}
					}
					returnValue.addAll(folderList);
					returnValue.addAll(reportList);
					returnValue.addAll(childrenList);
				} else {
					
				}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
		return returnValue;
	}
	public String getSearchPathUseStoreID ( String storeID ) throws Exception {
		String returnValue = null;
		
		try {
			returnValue = MenuUtil.getSearchPathUseStoreID((CRNConnect) loginSessionInfoFactory.getObject().getExternalInfo("connect"), storeID);
		} catch ( Exception e ) {
			e.printStackTrace();
		}
		return returnValue;
	}
	public JSONObject getReportPath ( String searchPath ) throws Exception {
		JSONObject returnValue = new JSONObject();
		String value = null;
		try {
			value = MenuUtil.getReportPath((CRNConnect)loginSessionInfoFactory.getObject().getExternalInfo("connect"), searchPath);
			returnValue.put("returnValue", value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnValue;
	}
	public List<Map<String, Object>> getMenuByJSON ( String storeID, String menuName ) throws Exception {
		String userID = loginSessionInfoFactory.getObject().getUserId();
		String defaultRootSearchPath = "";
		if("내 폴더".equals(menuName) || "My Folders".equals(menuName)) {
			defaultRootSearchPath = "CAMID(\'auth:u:"+userID+"\')/folder[@name=\'내 폴더\' or @name=\'My Folders\']";
		} else {
			defaultRootSearchPath = MenuUtil.getSearchPathUseStoreID((CRNConnect)loginSessionInfoFactory.getObject().getExternalInfo("connect"), storeID);
		}
		
		return  MenuUtil.getChildList(storeID, defaultRootSearchPath, (CRNConnect)loginSessionInfoFactory.getObject().getExternalInfo("connect"));
	}
	public JSONArray getDataMenuPermissionByCognos ( String cID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		logger.info("getDataMenuPermissionByCognos C_ID:{}",cID);
		map.put("C_ID"											, cID);
		try {
			logger.info("Get Data Menu Permission By Cognos Data Start");
			list = getSqlSession().selectList("getDataMenuPermissionByCognos", map);
			logger.info("Get Data Menu Permission By Cognos Data End");
			if(list.size() > 0) {
				for(int i=0; i<list.size(); i++) {
					JSONObject value = new JSONObject();
					value.put("C_ID"							, list.get(i).get("c_id"));
					value.put("ID"								, list.get(i).get("id"));
					value.put("NM"								, list.get(i).get("nm"));
					value.put("SORT"							, list.get(i).get("sort"));  
					value.put("TYPE"							, list.get(i).get("type"));
					value.put("READ"							, list.get(i).get("read"));
					value.put("WRITE"							, list.get(i).get("write"));
					value.put("EXECUTE"							, list.get(i).get("execute"));
					value.put("POLICY"							, list.get(i).get("policy"));
					value.put("TRAVERSE"						, list.get(i).get("traverse"));
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR > Get Data Menu Permission By Cognos Data Error : {}", e);
			throw e;
		}
		return returnValue;
	}
	public JSONArray getUserDataByCognos ( String cID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("C_ID"											, cID);
		try {
			logger.info("Get User Data By Cognos Data Start");
			list = getSqlSession().selectList("getUserDataByCognos", map);
			logger.info("Get User Data By Cognos Data End");
			if(list.size() > 0) {
				for(int i=0; i<list.size(); i++) {
					JSONObject value = new JSONObject();
					value.put("ID"								, list.get(i).get("id"));
					value.put("NM"								, list.get(i).get("nm"));
					value.put("TYPE"							, list.get(i).get("type"));
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get User Data By Cognos Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getGroupDataByCognos ( String cID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("C_ID"											, cID);
		try {
			logger.info("Get Group Data By Cognos Data Start");
			list = getSqlSession().selectList("getGroupDataByCognos", map);
			logger.info("Get Group Data By Cognos Data End");
			if(list.size() > 0) {
				for(int i=0; i<list.size(); i++) {
					JSONObject value = new JSONObject();
					value.put("ID"								, list.get(i).get("id"));
					value.put("NM"								, list.get(i).get("nm"));
					value.put("TYPE"							, list.get(i).get("type"));
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Group Data By Cognos Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getRoleDataByCognos ( String cID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("C_ID"											, cID);
		
		try {
			logger.info("Get Role Data By Cognos Data Start");
			list = getSqlSession().selectList("getRoleDataByCognos", map);
			logger.info("Get Role Data By Cognos Data End");
			if(list.size() > 0) {
				for(int i=0; i<list.size(); i++) {
					JSONObject value = new JSONObject();
					value.put("ID"								, list.get(i).get("id"));
					value.put("NM"								, list.get(i).get("nm"));
					value.put("TYPE"							, list.get(i).get("type"));
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Role Data By Cognos Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public boolean insert( String cID, String userPermission, String ac ) throws Exception {
		boolean returnValue = false;
		boolean cognosResult = false;
		String storeID = null;
		TableObject deleteIECT7021 = new TableObject();
		deleteIECT7021.put("C_ID"										, cID);
		try {
			logger.info("userPermission:{}",userPermission);
			logger.info("Cognos Authority Delete Data By Database Start");  
			dbmsDAO.deleteTable("iportal_custom.IECT7021",deleteIECT7021);
			logger.info("Cognos Authority Delete Data By Database End");
			String[] arrUserPermission = userPermission.split(";", -1);
 
			if(cID.indexOf("i") < 0) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("C_ID"												,cID);
				List<HashMap<String, Object>> list = getSqlSession().selectList("getMenuClinkByCognos", map);
				String cLink = list.get(0).get("c_link").toString();
				SearchPathMultipleObject cmSearchPath = new SearchPathMultipleObject(cLink);
				PropEnum[] properties = { PropEnum.defaultName, PropEnum.searchPath, PropEnum.objectClass, PropEnum.hasChildren, PropEnum.storeID, PropEnum.uri, PropEnum.target };
				BaseClass myCMObject = null;				
				CRNConnect conn = (CRNConnect) loginSessionInfoFactory.getObject().getExternalInfo("connet");
				String passport = CognosUtil.getPassport(conn);
				logger.info("cognos Passport : {}", Utils.getString(passport, "").replaceAll("[\r\n]",""));
				
				myCMObject = conn.getCMService(passport).query(cmSearchPath, properties, new Sort[] {}, new QueryOptions())[0];
				storeID = myCMObject.getStoreID().getValue().toString();
			} else {
				storeID = cID;
			}
			cognosResult = deleteCogPermission(storeID);
			if(cognosResult == true) {
				cognosResult = removeTempAuth( storeID );
				if(cognosResult == true) {
					logger.info("결과값 true");
					if(!"".equals(arrUserPermission[0])) {
						logger.info("this step run"); 
						for(String permissionString : arrUserPermission) {
							permissionCognosDataByPortal(cID, permissionString);
						}
						returnValue = setDataMenuPermissionCognos(cID, storeID, arrUserPermission);
						logger.info("result:{}", returnValue);
					}
				} else {
					logger.info("결과값 false!");
					cognosResult = false;
					returnValue = false;
					logger.error("Set Cognos Permission Administrators Error");
				}
			} else {
				logger.info("결과값 false");
				cognosResult = false;
				returnValue = false;
				logger.error("Delete Cog Permission Error");
			}
		} catch (Exception e) {
			logger.info("오류 발생");
			returnValue = false;
			e.printStackTrace();
		}
		return returnValue;
	}
	public boolean deleteCogPermission( String storeID ) throws Exception {
		boolean returnValue = true;
		
		try {
			cognosService.delPermissionAll(storeID);
			returnValue = true;
		} catch (Exception e) {
			returnValue = false;
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public boolean removeTempAuth ( String storeID ) throws Exception {
		boolean returnValue = false;
		List<Map<String, Object>> permissionStrList = new ArrayList<Map<String,Object>>();
		Map<String, Object> map = new HashMap<String, Object> ();
		map.put("source"										, ":System Administrators");
		map.put("authType"										, "cognos");
		map.put("cogPerType"									, "r");
		map.put("authority"										, null);
		
		permissionStrList.add(map);
		try {
			cognosService.setPermission(storeID, permissionStrList);
			returnValue = true;
		} catch (Exception e) {
			logger.error("Set Permission Administrators Error : {}", e);
			e.printStackTrace();
			returnValue = false;
			throw e;
		}
		return returnValue; 
	}
	public boolean permissionCognosDataByPortal ( String cID, String permissionString ) throws Exception {
		boolean returnValue = false;
		String[] arrPermission = permissionString.split(",", -1);
		TableObject InsertIECT7021 = new TableObject();
		InsertIECT7021.put("C_ID"					, cID);
		InsertIECT7021.put("FKEY"					, arrPermission[0]);
		InsertIECT7021.put("TYPE"					, arrPermission[1]);
		InsertIECT7021.put("READ"					, arrPermission[2]);
		InsertIECT7021.put("WRITE"					, arrPermission[3]);
		InsertIECT7021.put("EXECUTE"				, arrPermission[4]);
		InsertIECT7021.put("POLICY"					, arrPermission[5]);
		InsertIECT7021.put("TRAVERSE"				, arrPermission[6]);
//		InsertIECT7021.put("USER_ID"				, loginSessionInfoFactory.getObject().getUserId());
		
		try {
			logger.info("Insert Permission Cognos Data By Portal Start");
			dbmsDAO.insertTable("iportal_custom.IECT7021", InsertIECT7021);
			logger.info("Insert Permission Cognos Data By Portal End");
			returnValue = true;
		} catch (Exception e) {
			logger.error("ERROR > insert Permission Cognos Data By Portal Error : {}", e);
			returnValue = false;
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public boolean setDataMenuPermissionCognos(String cid, String storeId, String[] arrUserPermission) {
		final int permissionCount = 5;
		
		List<Map<String, Object>> permissionStrList = new ArrayList<Map<String, Object>>();
		logger.info("##### setDataMenuPermissionCognos");
		logger.info("##### C_ID:{}",cid);
		logger.info("##### StoreID:{}",storeId);
		for(int i = 0; i < arrUserPermission.length;i++) {
			logger.info("##### arrUserPermission["+i+"]:{}",arrUserPermission[i]);
		}
		for(String permissionString : arrUserPermission){
			String [] permissionStr = new String[permissionCount];
			String [] arrPermission = permissionString.split(",", -1);
			Map<String, Object> permissionMap = new  HashMap<String, Object>(); 
//			String [] permissionStr = new String[permissionCount];
			
			String cogPersonalType = arrPermission[1];
			String fKey = arrPermission[0];
			
			int addCount = 0;
			for(int i = 0; i<permissionCount; i++){
				permissionStr[i] = getPermissionNumVal(arrPermission[i+(arrPermission.length - permissionCount)]);
				
				if(permissionStr[i].equals("0") || permissionStr[i].equals("1")){
					addCount++;
				}
			}
			
			if(addCount == 0){
				return false;
			}
			logger.info("permissionStr : {} ", permissionStr);
	
			permissionMap.put("authority", permissionStr);
			
			permissionMap.put("cogPerType", getCogPerType(cogPersonalType));
			permissionMap.put("authType", "cognos");
			permissionMap.put("source", "");
			
			if (!isCognosAuth(permissionMap.get("cogPerType"), fKey)) {
				permissionMap.put("authType", CognosInfo.getNamespace());
			}
			
			if("r".equals(permissionMap.get("cogPerType"))){
				Map<String, String> groupMap = CodeManager.QUERY_MAP();
				groupMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
				groupMap.put("FKEY", fKey);
				permissionMap.put("authType", CognosInfo.getNamespace());
				permissionMap.put("source", fKey);
			}else{
				permissionMap.put("source", fKey);
			}
			permissionStrList.add(permissionMap);
		}
		
		return cognosService.setPermission(storeId, permissionStrList);
	}
	public boolean isCognosAuth(Object object, String id) {
		if (object.equals("r")) {
			return true;
		}
		if (id.substring(0, 1).equals(":")) {
			return true;
		}
		return false;
	}
	public String getCogPerType(String permissionType) {
		String cogPerType = "";
		if (permissionType.equals("p")) {
			cogPerType = "g";
		} else if (permissionType.equals("g")) {
			cogPerType = "r";
		} else {
			cogPerType = permissionType;
		}
		return cogPerType;
	}
	public String getPermissionNumVal(String stringValue){
		if("ok".equals(stringValue) || "1".equals(stringValue)) return "1";
		else if("no".equals(stringValue)) return "0";
		else return "null";
//		return stringValue.equals("ok")? "1": (stringValue.equals("no")? "0": "null");
	}
	public static String getReportImg(String reportType){
		if(reportType.equals("folder")){
			return "folder.gif";
		}else if(reportType.equals("report")){
			return "leaf_r.gif";
		}else if(reportType.equals("query")){
			return "leaf_q.gif";
		}else if(reportType.equals("dashboard")){
			return "leaf_d.gif";
		}else if(reportType.equals("URL")){
			return "leaf_l.gif";
		}else if(reportType.equals("interactiveReport")){
			return "leaf_d.gif";
		}else if(reportType.equals("pagelet")){
			return "leaf_l.gif";
		}else if(reportType.equals("analysis")){
			return "leaf_a.gif";
		}else if(reportType.equals("shortcut")){
			return "leaf_s.gif";
		}
		return "leaf_r.gif";
	}
	public static String getShortCutReportImg(String targetReportType){
		if(targetReportType.equals("folder")){
			return "s_folder.gif";
		}else if(targetReportType.equals("report")){
			return "leaf_s_r.gif";
		}else if(targetReportType.equals("query")){
			return "leaf_s_q.gif";
		}else if(targetReportType.equals("dashboard")){
			return "leaf_s_d.gif";
		}else if(targetReportType.equals("URL")){
			return "leaf_s_l.gif";
		}else if(targetReportType.equals("interactiveReport")){
			return "leaf_s_d.gif";
		}else if(targetReportType.equals("pagelet")){
			return "leaf_s_l.gif";
		}else if(targetReportType.equals("analysis")){
			return "leaf_s_a.gif";
		}
		return "icon_broken_ref.gif";
	}
}
