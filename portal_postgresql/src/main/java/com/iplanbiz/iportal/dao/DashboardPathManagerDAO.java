package com.iplanbiz.iportal.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;

@Repository
public class DashboardPathManagerDAO extends SqlSessionDaoSupport {
	
 
	@Autowired DBMSDAO dbmsDAO;
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public JSONArray getDashboardPathList() throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		try{
			logger.info("Get Dash Board Path Data Start");
			list = getSqlSession().selectList("getDashboardPathList");
			logger.info("Get Dash Board Path Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("PATH_IDX"									, list.get(i).get("path_idx"));
					value.put("FULLPATH"									, list.get(i).get("fullpath"));
					value.put("PATH_DESC"								, list.get(i).get("path_desc"));
					value.put("BIGO"											, list.get(i).get("bigo"));
					value.put("COM_UDC1"								, list.get(i).get("com_udc1"));
					value.put("USE_YN"										, list.get(i).get("use_yn"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Dash Board Path Data Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getDashboardPathDetail(String pathIdx) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("PATH_IDX"										, Integer.valueOf(pathIdx));
		try{
			logger.info("Get Dash board Path Detail Data Start");
			list = getSqlSession().selectList("getDashboardPathDetail", map);
			logger.info("Get Dash board Path Detail Data End");
			if(list.size() >0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("PATH_IDX"							, list.get(i).get("path_idx"));
					value.put("FULLPATH"							, list.get(i).get("fullpath"));
					value.put("PATH_DESC"						, list.get(i).get("path_desc"));
					value.put("BIGO"									, list.get(i).get("bigo"));
					value.put("COM_UDC"							, list.get(i).get("com_udc1"));
					value.put("USE_YN"								, list.get(i).get("use_yn"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Dash board Path Detail Data Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int getDashboardPathIdxMax() throws Exception{
		HashMap<String, Object> value = getSqlSession().selectOne("getDashboardPathIdxMax");
		return Integer.parseInt(value.get("idx").toString());
	}
	public int insertDashboardPath( String pathIdx, String fullPath, String pathDesc, String bigo,String useYn) throws Exception{
		int returnValue = 0;
		TableObject idfe0004 = new TableObject();
		
		idfe0004.put("FULLPATH"									, fullPath);
		idfe0004.put("PATH_DESC"								, pathDesc);
		idfe0004.put("BIGO"											, bigo);
		idfe0004.put("USE_YN"										, useYn);
		
		try{
			if(pathIdx.equals("")){
				idfe0004.put("PATH_IDX"									, this.getDashboardPathIdxMax());
				idfe0004.put("INSERT_EMP"						, loginSessionInfoFactory.getObject().getUserId());
				idfe0004.putCurrentTimeStamp("INSERT_DAT");
				
				logger.info("Insert Dash board Path Start");
				dbmsDAO.insertTable("iportal_custom.IDFE0004", idfe0004);
				logger.info("Insert Dash board Path End");
				returnValue = 0;
			}else{
				idfe0004.put("PATH_IDX"									, Integer.valueOf(pathIdx), true);
				idfe0004.put("UPDATE_EMP"						, loginSessionInfoFactory.getObject().getUserId());
				idfe0004.putCurrentTimeStamp("UPDATE_DAT");
				
				logger.info("Update Dash board Path Start");
				dbmsDAO.updateTable("iportal_custom.IDFE0004", idfe0004.getKeyMap(), idfe0004);
				logger.info("Update Dashboard Path End");
				returnValue = 1;
			}
		}catch(Exception e){
			returnValue = 2;
			logger.error("Insert && Update Dash board Path Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int removeDashboardPath(String[] pathIdx) throws Exception{
		int returnValue = 0;
		
		ArrayList<LinkedHashMap<String, Object>> arrayIdfe0004 = new ArrayList<LinkedHashMap<String,Object>>();
		
		for(int i=0;i<pathIdx.length;i++){
			TableObject idfe0004 = new TableObject();
			idfe0004.put("PATH_IDX"						, Integer.valueOf(pathIdx[i]));
			arrayIdfe0004.add(idfe0004);
		}
		
		try{
			logger.info("Remove Dash board Path Start ");
			dbmsDAO.deleteTable("iportal_custom.IDFE0004", new String []{"PATH_IDX"}, arrayIdfe0004);
			logger.info("Remove Dash board path End ");
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.info("Remove Dash board Path Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
}
