package com.iplanbiz.iportal.dao.procedure;

import java.io.IOException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.butler.Proxy;
import com.iplanbiz.core.io.butler.ProxyPool;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dao.system.SqlHouseDAO;
import com.iplanbiz.iportal.dto.Procedure;

@Repository
public class ProcedureDAO extends SqlSessionDaoSupport  {

	@Autowired SqlSessionFactory sqlSessionFactory;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Resource(name="proxyPool") 	ProxyPool proxyPool;
	@Autowired DBMSDAO dbmsDAO;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public List<HashMap<String, String>> procedureList(String procedureName){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID",         loginSessionInfoFactory.getObject().getAcctID());
		map.put("CDPROCEDURE_NM",  procedureName);
		return getSqlSession().selectList("procedureList", map);
	}
	public List<HashMap<String, String>> procedureListTimeCogdw(String dbGubn, String dbUser, String procedure){
		List<HashMap<String, String>> procedureListTimeCogdw = new ArrayList<HashMap<String,String>>();
		String query = "";
		/*if("A".equals(dbGubn)){
			query = "SELECT PROCEDUREID, START_DATE, END_DATE, START_TIME, END_TIME, ROUND((END_TIME-START_TIME)*24*60*60) AS RESULT_TIME, DIFF_TIME, SOF_YN \r\n"+
		            "FROM ( \r\n"+
			        "      SELECT PROCEDUREID, START_DATE, END_DATE, START_TIME, END_TIME, DIFF_TIME, DTINSERT, SOF_YN, RANK() OVER(PARTITION BY PROCEDUREID ORDER BY DTINSERT DESC) AS CNT \r\n"+ 
			        "      FROM IDWD3001 \r\n"+
			        "     ) A WHERE CNT = 1 \r\n"+
			        "         AND   PROCEDUREID = '"+procedure+"'"; 
		}else{
			query = "SELECT PROCEDUREID, START_DATE, END_DATE, START_TIME, END_TIME, ROUND((END_TIME-START_TIME)*24*60*60) AS RESULT_TIME, DIFF_TIME, SOF_YN \r\n"+
		            "FROM ( \r\n"+
			        "      SELECT PROCEDUREID, START_DATE, END_DATE, START_TIME, END_TIME, DIFF_TIME, DTINSERT, SOF_YN, RANK() OVER(PARTITION BY PROCEDUREID ORDER BY DTINSERT DESC) AS CNT \r\n"+ 
			        "      FROM IDWD3001\r\n"+
			        "     ) A WHERE CNT = 1 \r\n"+
			        "         AND   PROCEDUREID = '"+procedure+"'";
		}*/
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("PROCEDUREID",procedure);
		
		procedureListTimeCogdw = getSqlSession().selectList("procedureListTimeCogdw", map);
		return procedureListTimeCogdw;
	}
	
	public List<HashMap<String, String>> procedureCodeList(String comlCod){
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("COML_COD",       comlCod);
		return getSqlSession().selectList("procedureCodeList", map);
	}
	
	public String callSp(String dbGubn, String dbUser, String procedure, String year, String idx) throws Exception{
		AjaxMessage msg = new AjaxMessage();
		Proxy proxy =  proxyPool.createProxy();
		String result = "";
		
		List<HashMap<String, String>> inparam = new ArrayList<HashMap<String,String>>();
		
		HashMap<String,String> map = new HashMap<String,String>();
		map.put("VALUE", year);
		map.put("TYPE",Integer.toString(Types.VARCHAR));
		map.put("INOUT","IN");
		inparam.add(map);
		try{
			//마감 프로시져 실행
			proxy.start();
			
			JSONObject param = new JSONObject();
			param.put("PROC_NM",procedure);
			param.put("DB_KEY",dbUser);
			param.put("PARAMS", inparam);
			param.put("USER_ID", loginSessionInfoFactory.getObject().getUserId());
			param.put("USER_NM",loginSessionInfoFactory.getObject().getUserName());
			logger.info(procedure+dbUser);
				 
			msg = proxy.requestRule("etl", "Procedure", param); 
			result = msg.getReturnCode();
		}catch(Exception e){	
			logger.error("error",e);
			result="EXCEPTION";
		}finally{
			proxy.stop();
		}
		return result;
	}
	public JSONArray getProcedureList(String procedureName) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID",         loginSessionInfoFactory.getObject().getAcctID());
		if(procedureName==null) procedureName="";
		map.put("CDPROCEDURE_NM",  procedureName);
		try{
			logger.info("Get procedureList Start");
			list = getSqlSession().selectList("procedureList", map);
			logger.info("Get procedureList End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					List<HashMap<String, String>> timelist=this.procedureListTimeCogdw(list.get(i).get("db_gubn").toString(),list.get(i).get("db_user").toString(),list.get(i).get("cdprocedure").toString());
					JSONObject value = new JSONObject();
					value.put("CDPROCEDURE"									, list.get(i).get("cdprocedure"));
					value.put("CDPROCEDURE_NM"								, list.get(i).get("cdprocedure_nm"));
					value.put("BIGO"								, list.get(i).get("bigo"));
					value.put("DB_GUBN"							, list.get(i).get("db_gubn"));
					value.put("DB_USER"								, list.get(i).get("db_user"));
					
					if(timelist.size()>0) {
						value.put("RESULT_TIME", timelist.get(0).get("result_time"));
						value.put("SOF_YN", timelist.get(0).get("sof_yn"));
					}else {
						value.put("RESULT_TIME", "0");
						value.put("SOF_YN", "");
					}
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get procedureList Data By Procedure Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	
	public JSONArray procedureResultListCogdw(String dbGubn, String dbUser, String paramProcedureName) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(paramProcedureName==null) paramProcedureName="";
		map.put("PROCEDUREID",  paramProcedureName);
		try{
			logger.info("Get procedureResultListCogdw Start");
			list = getSqlSession().selectList("procedureResultListCogdw", map);
			logger.info("Get procedureResultListCogdw End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("DTINSERT"									, list.get(i).get("dtinsert"));
					value.put("PROCEDUREID"								, list.get(i).get("procedureid"));
					value.put("START_TIME"								, list.get(i).get("start_time"));
					value.put("END_TIME"							, list.get(i).get("end_time"));
					value.put("SOF_YN"								, list.get(i).get("sof_yn")); 
					value.put("ERRORCOMMENT"								, list.get(i).get("errorcomment")); 					
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get procedureResultListCogdw Data By Procedure Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
}
