package com.iplanbiz.iportal.dao.custom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.io.web.WebService;
import com.iplanbiz.core.secure.GeneratePassword;
import com.iplanbiz.core.secure.SHA256;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.GroupDAO;
import com.iplanbiz.iportal.dao.RoleDAO;
import com.iplanbiz.iportal.dao.admin.UserDAO;
import com.iplanbiz.iportal.dao.auth.EmpView;
import com.iplanbiz.iportal.dao.system.CustomSqlHouseDAO;
import com.iplanbiz.iportal.dao.system.SqlHouseExecuteDAO;
import com.iplanbiz.iportal.dao.system.SystemDAO;
import com.iplanbiz.iportal.dto.KtotoUser;
import com.iplanbiz.iportal.dto.User;
import com.iplanbiz.iportal.service.admin.UserService;
import com.iplanbiz.iportal.service.custom.KtotoUserService;

@Repository
public class KtotoUserDAO extends SqlSessionDaoSupport {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	DBMSService dbmsService;
	@Autowired
	SqlHouseExecuteDAO sqlHouseExecuteDAO;
	@Autowired
	DBMSDAO dbmsDAO;
	@Autowired
	SystemDAO systemDAO;
	@Autowired
	CustomSqlHouseDAO customSqlHouseDAO;
	@Autowired
	UserDAO userDAO;
	@Autowired
	KtotoUserService ktotoUserService;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	
	public String getKtotoMaxUnID() throws Exception{
		String returnValue = "";
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
			HashMap<String, Object> returnMap = getSqlSession().selectOne("getKtotoMaxUnID", map);
			returnValue = returnMap.get("unid").toString();
		}catch(Exception e){
			logger.error("GET UNID ERROR : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int getCheckUserID ( String userID ) throws Exception {
		int returnValue = 0;
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("EMP_ID"										, userID);
		
		try {
			listValue = getSqlSession().selectList("getCheckUserID", map);
			returnValue = Integer.parseInt(listValue.get(0).get("cnt").toString());
			logger.info("Get CHECK Value = " + returnValue);
		} catch (Exception e) {
			returnValue = -1;
			e.printStackTrace();
			logger.error("ERROR > Get Check User ID Error : {}", e);
		}
		return returnValue;
	}
	public int getCheckOlapID ( String olapID) throws Exception {
		int returnValue = 0;
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("K_OLAPID"										, olapID);
		
		try {
			listValue = getSqlSession().selectList("getKtotoCheckOlapID", map);
			returnValue = Integer.parseInt(listValue.get(0).get("cnt").toString());
			logger.info("Get CHECK Value = " + returnValue);
		} catch (Exception e) {
			returnValue = -1;
			e.printStackTrace();
			logger.error("ERROR > Get Check User ID Error : {}", e);
		}
		return returnValue;
	}
	
	public String getSystemValue(String key) {
		return systemDAO.getSystemValue(key);
	}
	
	public JSONArray getCodeList(String com_grp_cd) throws Exception {
		JSONArray resultValue = new JSONArray();
		List<HashMap< String, Object>> list = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("COM_GRP_CD"										, com_grp_cd);
			list = getSqlSession().selectList("getKtotoCodeList", map);
			if(list!=null&&list.size()!=0){
				//System.out.println("customList:::"+list.size()+list);
				for(int i=0;i<list.size();i++){
					JSONObject putData = new JSONObject();
					putData.put("COM_GRP_CD"									, list.get(i).get("com_grp_cd"));
					putData.put("COM_CD"									, list.get(i).get("com_cd"));
					putData.put("COM_CD_NM"									, list.get(i).get("com_cd_nm"));
					resultValue.add(putData);
				}
			}
		
		}catch(Exception e){
			logger.error("ERROR : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return resultValue;
	}
	public JSONArray getUserList(String dept, String team, String status) throws Exception {
		JSONArray resultValue = new JSONArray();
		List<HashMap< String, Object>> listValue = null;
		try{
			String keyText = "USER_ID";
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
			map.put("K_DEPT"										, dept);
			map.put("K_TEAM"									, team);
			map.put("K_STATUS"										, status);
			listValue = getSqlSession().selectList("getKtotoUserList", map);
			if(listValue!=null&&listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putData = new JSONObject();
					
					putData.put("UNID"										, listValue.get(i).get("unid"));
					putData.put("ACCT_ID"									, listValue.get(i).get("acct_id"));
					putData.put("USER_ID"									, listValue.get(i).get("user_id"));
					putData.put("USER_NAME"							, listValue.get(i).get("user_name"));
					putData.put("USER_EMAIL"							, listValue.get(i).get("user_email"));
					putData.put("USER_REGDATE"							, listValue.get(i).get("user_regdate"));
					putData.put("K_OLAPID"							, listValue.get(i).get("k_olapid"));
					putData.put("K_PHONE"							, listValue.get(i).get("k_phone"));
					putData.put("K_STATUS"						, listValue.get(i).get("k_status"));
					putData.put("K_COMPANY"						, listValue.get(i).get("k_company"));
					putData.put("K_DEPT"							, listValue.get(i).get("k_dept"));
					putData.put("K_TEAM"						, listValue.get(i).get("k_team"));
					putData.put("K_STATUS_NM"						, listValue.get(i).get("k_status_nm"));
					putData.put("K_COMPANY_NM"						, listValue.get(i).get("k_company_nm"));
					putData.put("K_DEPT_NM"							, listValue.get(i).get("k_dept_nm"));
					putData.put("K_TEAM_NM"						, listValue.get(i).get("k_team_nm"));
					putData.put("K_FAILCOUNT"						, listValue.get(i).get("k_failcount"));
					putData.put("K_IP_IN"						, listValue.get(i).get("k_ip_in"));
					putData.put("K_IP_OUT"						, listValue.get(i).get("k_ip_out"));
					putData.put("K_SYS"						, listValue.get(i).get("k_sys"));
					putData.put("UPDATE_DAT"								, listValue.get(i).get("update_dat"));
					resultValue.add(putData);
				}
			}
		}catch(Exception e){
			logger.error("ERROR : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return resultValue;
	}
	
	
	
	public int saveKtotoUser(KtotoUser user, String checkValue, String remoteIP, String remoteOS, String remoteBrowser) throws Exception{
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"									, user.getUserID());
		map.put("USER_NAME"									, user.getUserName());
		map.put("USER_EMAIL"									, user.getUserEmail());
		map.put("USER_STATUS"								, "정상");
		map.put("USER_POSITION"								, "");
		map.put("ADMIN_YN"									, "N");
		map.put("LOCALE"									, "");
		
		map.put("K_OLAPID"									, user.getOlapID());
		map.put("K_PHONE"									, user.getkPhone());
		map.put("K_STATUS"									, user.getkStatus());
		map.put("K_COMPANY"									, user.getkCompany());
		map.put("K_DEPT"									, user.getkDept());
		map.put("K_TEAM"									, user.getkTeam());
		map.put("K_IP_IN"									, user.getkIpin());
		map.put("K_IP_OUT"									, user.getkIpout());
		map.put("K_SYS"										, user.getkSys());
		
		map.put("REMOTE_IP"									, remoteIP);
		map.put("REMOTE_OS"									, remoteOS);
		map.put("REMOTE_BROWSER"							, remoteBrowser);
		map.put("INSERT_EMP"								, loginSessionInfoFactory.getObject().getUserId());
		map.put("UPDATE_EMP"								, loginSessionInfoFactory.getObject().getUserId());
		map.put("UPDATE_EMP_NM"								, loginSessionInfoFactory.getObject().getUserName());
		
		
		TableObject ktotoUser = new TableObject();
		ktotoUser.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID(), true);
		ktotoUser.put("K_OLAPID"									, user.getOlapID());
		ktotoUser.put("K_PHONE"										, user.getkPhone());
		ktotoUser.put("K_STATUS"									, user.getkStatus());
		ktotoUser.put("K_COMPANY"									, user.getkCompany());
		ktotoUser.put("K_DEPT"										, user.getkDept());
		ktotoUser.put("K_TEAM"										, user.getkTeam());
		ktotoUser.put("K_IP_IN"										, user.getkIpin());
		ktotoUser.put("K_IP_OUT"									, user.getkIpout());
		ktotoUser.put("K_SYS"										, user.getkSys());
		
	
		TableObject iect7025 = new TableObject();
		iect7025.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID(), true);
		iect7025.put("EMP_ID"											, user.getUserID(), true);
		if(user.getkFailCount()==null||user.getkFailCount().equals(""))
			iect7025.put("FAIL_CNT"										, 0);
		else
			iect7025.put("FAIL_CNT"										, Integer.parseInt(user.getkFailCount()));
		
		if(Integer.parseInt(Utils.getString(user.getkFailCount(),"0"))<WebConfig.getLockedCount())
			iect7025.put("EMP_STATUS"									, "OPEN");
		else
			iect7025.put("EMP_STATUS"									, "LOCKED");
		iect7025.putCurrentDate("INSERT_DAT");
		
		try{
			if(user.getUnID()==null||user.getUnID().equals("")){//신규
				String unid = getKtotoMaxUnID();
				ktotoUser.put("UNID"									,Integer.parseInt(unid),true);
				ktotoUser.put("INSERT_EMP"								, loginSessionInfoFactory.getObject().getUserId());
				ktotoUser.putCurrentDate("INSERT_DAT");
		
				dbmsDAO.insertTable("ktoto_custom.ktotoUser", ktotoUser);
				
				
				TableObject iect7003 = new TableObject();
				iect7003.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID(), true);
				iect7003.put("UNID"											, Integer.valueOf(unid),true);
				iect7003.put("USER_ID"										, user.getUserID(), true);
				iect7003.put("USER_PASSWORD"								, SHA256.getSHA256(user.getUserPassword()));
				
				iect7003.put("USER_NAME"									, user.getUserName());
				iect7003.put("USER_POSITION"								, "");
				iect7003.put("USER_EMAIL"									, user.getUserEmail());
				iect7003.putCurrentDate("USER_REGDATE");
				iect7003.put("USER_STATUS"									, "정상");
				iect7003.put("ADMIN_YN"										, "N");
				iect7003.put("LOCALE"											, "");
				iect7003.put("INSERT_EMP"									, loginSessionInfoFactory.getObject().getUserId());
				iect7003.putCurrentDate("INSERT_DAT");
				iect7003.put("UPDATE_EMP"									, loginSessionInfoFactory.getObject().getUserId());
				
				dbmsDAO.insertTable("iportal_custom.IECT7003", iect7003);
				
				map.put("UNID"									, Integer.valueOf(unid));
				map.put("GUBN"									, "1");
				map.put("K_FAILCOUNT"							,0);
				result = 0;
				
				this.insertEmailLog(user.getUserID(), user.getUserName(), user.getUserEmail(), "password", unid, remoteIP, remoteOS, remoteBrowser);
				
			}else{//수정
				ktotoUser.put("UNID"								, Integer.valueOf(user.getUnID()),true);
				ktotoUser.put("UPDATE_EMP"									, loginSessionInfoFactory.getObject().getUserId());
				ktotoUser.putCurrentDate("UPDATE_DAT");
				
				dbmsDAO.updateTable("ktoto_custom.ktotoUser", ktotoUser.getKeyMap(),ktotoUser);
				
				TableObject iect7003 = new TableObject();
				iect7003.put("USER_NAME"									, user.getUserName());
				iect7003.put("USER_EMAIL"									, user.getUserEmail());
				iect7003.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID(), true);
				iect7003.put("UNID"											, Integer.parseInt(user.getUnID()), true);
				dbmsDAO.updateTable("iportal_custom.IECT7003", iect7003.getKeyMap(),iect7003);
				

				dbmsDAO.upsertTable("iportal_custom.iect7025", iect7025.getKeyMap(),iect7025);
				HashMap<String, Object> map2 = new HashMap<String, Object>();
				map2.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
				map2.put("EMP_ID"										, user.getUserID());
				HashMap<String, Object> statusCount = this.getSqlSession().selectOne("getUserStatusAndFailCount", map2);
				if(statusCount!=null) {
				//	dbmsDAO.upsertTable("iportal_custom.iect7025", iect7025.getKeyMap(),iect7025);
					Object status = statusCount.get("emp_status");
					if(status.toString().equals("LOCKED")) {
						if(iect7025.get("EMP_STATUS").toString().equals("OPEN")) {
							map.put("GUBN"									, "7");//lock해제 
							this.getSqlSession().update("insertUserHistory", map);
						}	
					}
				}
				
				map.put("UNID"									,Integer.valueOf(user.getUnID()));
				map.put("GUBN"									, "2");
				map.put("K_FAILCOUNT"							,Integer.valueOf(user.getkFailCount()));
				result = 1;
			}
			this.getSqlSession().update("insertKtotoUserHistory", map);
			this.getSqlSession().update("insertUserHistory", map);
			

			
			//dbmsDAO.deleteTable("iportal_custom.IECT7010", iect7010);
			/*if(!checkValue.equals("")){
				ArrayList<LinkedHashMap<String, Object>> arrayInsertIect7010 = new ArrayList<LinkedHashMap<String,Object>>();
				for(int i=0;i<checkValue.split(",").length;i++){
					TableObject insertIect7010 = new TableObject();
					insertIect7010.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
					insertIect7010.put("TYPE"									, 'u');
					insertIect7010.put("FKEY"									, user.getUserID());
					insertIect7010.put("GID"									, Integer.valueOf(checkValue.split(",")[i]));
					arrayInsertIect7010.add(insertIect7010);
				}
				dbmsDAO.insertTable("iportal_custom.IECT7010",  arrayInsertIect7010);
			}*/
		}catch(Exception e){
			logger.error("USER INFO INSERT ERROR : {}", e);
			e.printStackTrace();
			result = 2;
			throw e;
		}
		return result;
	}
	public int deleteUser(String userID, String unID, String remoteIP, String remoteOS, String remoteBrowser) throws Exception {
		int result = 0;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"										, userID);
		map.put("UNID"											, Integer.valueOf(unID));
		map.put("GUBN"											, "5");
		map.put("REMOTE_IP"									, remoteIP);
		map.put("REMOTE_OS"									, remoteOS);
		map.put("REMOTE_BROWSER"							, remoteBrowser);
		map.put("INSERT_EMP"								, loginSessionInfoFactory.getObject().getUserId());
		map.put("UPDATE_EMP"								, loginSessionInfoFactory.getObject().getUserId());
		map.put("UPDATE_EMP_NM"							, loginSessionInfoFactory.getObject().getUserName());
		map.put("UPDATE_USER"								, loginSessionInfoFactory.getObject().getUserId());
		map.put("UPDATE_USER_NM"							, loginSessionInfoFactory.getObject().getUserName());
		
		this.getSqlSession().update("insertKtotoUserHistory", map);
		this.getSqlSession().update("insertUserHistory", map);
		
		
		TableObject deletektotoUser = new TableObject();
		deletektotoUser.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		deletektotoUser.put("UNID"										, Integer.valueOf(unID));
		
		TableObject deleteIect7008 = new TableObject();
		deleteIect7008.put("UNID"									, Integer.valueOf(unID));
		deleteIect7008.put("EMP_ID"									, userID);
		deleteIect7008.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		
		TableObject deleteIect7003 = new TableObject();
		deleteIect7003.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		deleteIect7003.put("UNID"										, Integer.valueOf(unID));
		
		TableObject deleteIect7025 = new TableObject();
		deleteIect7025.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		deleteIect7025.put("EMP_ID"									, userID);
	/*	TableObject deleteIect7010 = new TableObject();
		deleteIect7010.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		deleteIect7010.put("FKEY"										, userID);*/
		try{
			logger.info("deletektotoUser Delete Start");
			dbmsDAO.deleteTable("ktoto_custom.ktotouser", deletektotoUser);
			logger.info("deletektotoUser Delete End");

			logger.info("deleteIect7008 Delete Start");
			dbmsDAO.deleteTable("iportal_custom.IECT7008", deleteIect7008);
			logger.info("deleteIect7008 Delete End");
			
			logger.info("deleteIect7003 Delete Start");
			dbmsDAO.deleteTable("iportal_custom.IECT7003", deleteIect7003);
			logger.info("deleteIect7003 Delete End");
			
			logger.info("deleteIect7025 Delete Start");
			dbmsDAO.deleteTable("iportal_custom.IECT7025", deleteIect7025);
			logger.info("deleteIect7025 Delete End");
		/*	logger.info("User Role Delete Start");
			dbmsDAO.deleteTable("iportal_custom.IECT7010", deleteIect7010);
			logger.info("User Role Delete End");*/
			
			result = 0;
		}catch(Exception e){
			result = 1;
			e.printStackTrace();
			logger.error("User Delete Error : {}", e);
			throw e;
		}
		return result;
	}
	public List<LinkedHashMap<String, Object>> getUserListForExcel(String dept, String team, String status) {
		List<LinkedHashMap< String, Object>> listValue = null;
		
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
			map.put("K_DEPT"										, dept);
			map.put("K_TEAM"									, team);
			map.put("K_STATUS"										, status);
			listValue = getSqlSession().selectList("getKtotoUserList", map);
			
			
		return listValue;
	}
	public Map<String, Object> getIpAddress(String unid) throws Exception {
		Map<String, Object> resultValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
			map.put("UNID"											, Integer.parseInt(unid));
			resultValue = this.getSqlSession().selectOne("getKtotoIpAddress", map);
		}catch(Exception e){
			resultValue = null;
			logger.error("ERROR" ,e);
			throw e;
		}
		return resultValue;
	}
	public Map<String, Object> getKtotoUserInfo(String unid) throws Exception {
		Map<String, Object> resultValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
			map.put("UNID"											, Integer.parseInt(unid));
			resultValue = this.getSqlSession().selectOne("getKtotoUserInfo", map);
		}catch(Exception e){
			resultValue = null;
			logger.error("ERROR" ,e);
			throw e;
		}
		return resultValue;
	}
	public int insertEmailLog(String userId,String userName,String email,String type,String key,String remoteIP, String remoteOS, String remoteBrowser) {
		Map<String, Object> sendDept = null;
		Map<String, Object> recvDept = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("SEND_ID"										, loginSessionInfoFactory.getObject().getUserId());
		int idx = getSqlSession().selectOne("getKtotoMaxIDXForEmailLog", map);
		
		HashMap<String, Object> sMap = new HashMap<String, Object>();//SEND
		sMap.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		sMap.put("USER_ID"										, loginSessionInfoFactory.getObject().getUserId());
		sendDept = getSqlSession().selectOne("getKtotoUserDept", sMap);
		
		HashMap<String, Object> rMap = new HashMap<String, Object>();//RECV
		rMap.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		rMap.put("USER_ID"										, userId);
		recvDept = getSqlSession().selectOne("getKtotoUserDept", rMap);
		
		HashMap<String, Object> mailMap = new HashMap<String, Object>();
		mailMap.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		mailMap.put("IDX"											, idx);
		mailMap.put("SEND_ID"										, loginSessionInfoFactory.getObject().getUserId());
		mailMap.put("SEND_NAME"										, loginSessionInfoFactory.getObject().getUserName());
		if(sendDept!=null) {
			mailMap.put("SEND_DEPT"										, sendDept.get("user_grp_id"));
			mailMap.put("SEND_DEPT_NM"									, sendDept.get("user_grp_nm"));
		}
		mailMap.put("RECV_ID"										, userId);
		mailMap.put("RECV_NAME"										, userName);
		mailMap.put("RECV_EMAIL"									, email);
		if(recvDept!=null) {
			mailMap.put("RECV_DEPT"										, recvDept.get("user_grp_id"));
			mailMap.put("RECV_DEPT_NM"									, recvDept.get("user_grp_nm"));
		}
		mailMap.put("SEND_TYP"										, type);
		mailMap.put("SEND_KEY"										, Integer.parseInt(key));//unid or noticeNo
		mailMap.put("REMOTE_IP"										, remoteIP);
		mailMap.put("REMOTE_OS"										, remoteOS);
		mailMap.put("REMOTE_BROWSER"								, remoteBrowser);
		mailMap.put("INSERT_EMP"									, loginSessionInfoFactory.getObject().getUserId());
		mailMap.put("UPDATE_EMP"									, loginSessionInfoFactory.getObject().getUserId());
		mailMap.put("UPDATE_EMP_NM"									, loginSessionInfoFactory.getObject().getUserName());
		
		int result = this.getSqlSession().update("insertKtotoEmailSendLog", mailMap);
		
		return result;
	}
	
	
	public int resetPassword(String type, String unid, String userId, String remoteIP, String remoteOS, String remoteBrowser,String serverUrl) throws Exception {
		int result = 0;
		String userPassword = GeneratePassword.getPassword(); //임시비밀번호생성
		if(type.equals("email")) {//이메일 전송방식
			//getAdminUserInfo
			Map<String,Object> User = userDAO.getAdminUserInfo(userId);
			String email = User.get("user_email").toString();
			if(email!=null) {
				//비밀번호 변경
				try{
					TableObject iect7003 = new TableObject();
					iect7003.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID(), true);
					iect7003.put("UNID"											, Integer.valueOf(unid),true);
					iect7003.put("USER_ID"										, userId, true);
					iect7003.put("USER_PASSWORD"								, SHA256.getSHA256(userPassword));
					iect7003.put("UPDATE_EMP"									, loginSessionInfoFactory.getObject().getUserId());
					iect7003.put("PASSWORD_UPDATE_DAT"							, null);
					dbmsDAO.updateTable("iportal_custom.IECT7003", iect7003.getKeyMap(),iect7003);
					
					int sendResult = ktotoUserService.sendEmail(userId,userPassword,email);
					if(sendResult==1) {
						result=1;
						//이메일전송로그
						String name = User.get("user_name").toString();
						this.insertEmailLog(userId, name, email, "password", unid, remoteIP, remoteOS, remoteBrowser);
						
					}else {
						result=-2;//이메일오류
					}
				
					HashMap<String, Object> map = new HashMap<String, Object>();
					map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
					map.put("USER_ID"										, userId);
					map.put("UNID"											, Integer.parseInt(unid));
					map.put("GUBN"											, "10"); //암호초기화
					map.put("REMOTE_IP"									, remoteIP);
					map.put("REMOTE_OS"									, remoteOS);
					map.put("REMOTE_BROWSER"							, remoteBrowser);
					map.put("INSERT_EMP"								, loginSessionInfoFactory.getObject().getUserId());
					map.put("UPDATE_EMP"								, loginSessionInfoFactory.getObject().getUserId());
					map.put("UPDATE_EMP_NM"								, loginSessionInfoFactory.getObject().getUserName());
					map.put("UPDATE_USER"								, loginSessionInfoFactory.getObject().getUserId());
					map.put("UPDATE_USER_NM"							, loginSessionInfoFactory.getObject().getUserName());
					
					this.getSqlSession().update("insertUserHistory", map);
					this.getSqlSession().update("insertKtotoUserHistory", map);
					//이메일 전송
				}catch(Exception e){
					logger.error("USER PASSWORD UPDATE ERROR : {}", e);
					e.printStackTrace();
					result = -1; //비밀번호 업데이트 오류
				}
			}
		}else if(type.equals("sms")){//sms 전송방식
			Map<String,Object> rUser = this.getKtotoUserInfo(unid);
			Object phone = rUser.get("k_phone");
			if(phone!=null && (!phone.toString().equals("") && phone.toString().length()>=10)) {
				String phoneNum = phone.toString();
				try{
					/*
					TableObject iect7003 = new TableObject();
					iect7003.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID(), true);
					iect7003.put("UNID"											, Integer.valueOf(unid),true);
					iect7003.put("USER_ID"										, userId, true);
					iect7003.put("USER_PASSWORD"								, SHA256.getSHA256(userPassword));
					iect7003.put("UPDATE_EMP"									, loginSessionInfoFactory.getObject().getUserId());
					iect7003.put("PASSWORD_UPDATE_DAT"							, null);
					dbmsDAO.updateTable("iportal_custom.IECT7003", iect7003.getKeyMap(),iect7003);
					*/
					Map<String,Object> recvUser = userDAO.getAdminUserInfo(userId);
					String userNm = recvUser.get("user_name").toString();
					String num1="",num2="", num3="";
					String num4="",num5="", num6="";
					num1 = phoneNum.substring(0,3);
					num2 = phoneNum.substring(3,phoneNum.length()-4);
					num2 = num2.replace("-", "");
					num3 = phoneNum.substring(phoneNum.length()-4,phoneNum.length());
					
					Map<String,Object> sendUser = userDAO.getAdminUserInfo(loginSessionInfoFactory.getObject().getUserId());
					String sendUnid = "";
					if(sendUser!=null)	sendUnid = sendUser.get("unid").toString();
					Map<String,Object> sUser = this.getKtotoUserInfo(sendUnid);
					if(sUser!=null) {
						Object sphone = sUser.get("k_phone");
						if(sphone!=null) {
							String sphoneNum = sphone.toString();
							if(!sphoneNum.equals("") && sphoneNum.length()>=10) {
								num4 = sphoneNum.substring(0,3);
								num5 = sphoneNum.substring(3,sphoneNum.length()-4);
								num5 = num5.replace("-", "");
								num6 = sphoneNum.substring(sphoneNum.length()-4,sphoneNum.length());
							}
						}
					}
					
					
					
					HashMap<String, Object> smsMap = new HashMap<String, Object>();
					smsMap.put("acctId", loginSessionInfoFactory.getObject().getAcctID());
					smsMap.put("sendId", loginSessionInfoFactory.getObject().getUserId());
					smsMap.put("sendName", loginSessionInfoFactory.getObject().getUserName());
					smsMap.put("sphone1", num4);
					smsMap.put("sphone2", num5);
					smsMap.put("sphone3", num6);
					
					smsMap.put("recvId", userId);
					smsMap.put("recvName", userNm);
					smsMap.put("rphone1", num1);
					smsMap.put("rphone2", num2);
					smsMap.put("rphone3", num3);
					
					smsMap.put("remoteIP", remoteIP);
					smsMap.put("remoteOS", remoteOS);
					smsMap.put("remoteBrowser", remoteBrowser);
					
					
					String url = serverUrl+"/resource/custom/ktoto/sms/sendSms.jsp";
					Object sendResult = WebService.post(url, smsMap);
					JSONArray jsonArray = (JSONArray)sendResult;
					JSONObject jsonObject = (JSONObject)jsonArray.get(0);
					String res = jsonObject.get("result").toString();
					if(res.equals("SUCCESS"))
						result=1;
					else
						result=-3;
					/*
					HashMap<String, Object> map = new HashMap<String, Object>();
					map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
					map.put("USER_ID"										, userId);
					map.put("UNID"											, Integer.parseInt(unid));
					map.put("GUBN"											, "10"); //암호초기화 
					map.put("REMOTE_IP"									, remoteIP);
					map.put("REMOTE_OS"									, remoteOS);
					map.put("REMOTE_BROWSER"							, remoteBrowser);
					map.put("INSERT_EMP"								, loginSessionInfoFactory.getObject().getUserId());
					map.put("UPDATE_EMP"								, loginSessionInfoFactory.getObject().getUserId());
					map.put("UPDATE_EMP_NM"								, loginSessionInfoFactory.getObject().getUserName());
					map.put("UPDATE_USER"								, loginSessionInfoFactory.getObject().getUserId());
					map.put("UPDATE_USER_NM"							, loginSessionInfoFactory.getObject().getUserName());
					
					this.getSqlSession().update("insertUserHistory", map);
					this.getSqlSession().update("insertKtotoUserHistory", map);
					*/
					//sms 전송
				}catch(Exception e){
					logger.error("USER PASSWORD UPDATE ERROR : {}", e);
					e.printStackTrace();
					result = -1; //비밀번호 업데이트 오류
				}
			
			}
		}
		return result;
	}
 
}
