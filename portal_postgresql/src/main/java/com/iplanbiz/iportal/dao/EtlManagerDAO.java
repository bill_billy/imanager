package com.iplanbiz.iportal.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.butler.Proxy;
import com.iplanbiz.core.io.butler.ProxyPool;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSDataSource;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.IRMIDBMSService;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.io.jndi.JNDIConfig;
import com.iplanbiz.core.message.AjaxMessage; 
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dto.Etl;

@Repository
public class EtlManagerDAO extends SqlSessionDaoSupport {
	
	@Resource(name="proxyPool") ProxyPool proxyPool;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Autowired DBMSDAO dbmsDAO;
	private Logger logger = LoggerFactory.getLogger(getClass());

	
	public JSONArray getEtlManagerGbnList () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		try{
			logger.info("Get Etl Manager Gbn List Data Start");
			list = getSqlSession().selectList("getEtlManagerGbnList");
			logger.info("Get Etl Manager Gbn List Data End");
			if(list.size()>0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("COM_COD"							, list.get(i).get("com_cod"));
					value.put("COM_NM"							, list.get(i).get("com_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Etl Manager Gbn List Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getEtlManagerList ( String gbn, String searchKey, String searchValue, String etlYn ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("GBN"													, gbn);
		map.put("KEY"														, searchKey);
		map.put("SEARCH_VALUE"									, searchValue);
		map.put("ETL_YN"												, etlYn);
		try{
			logger.info("Get Etl Manager List Data Start");
			list = getSqlSession().selectList("getEtlManagerList", map);
			logger.info("Get Etl Manager List Data End");
			if(list.size()>0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("ETL_ID"									, list.get(i).get("etl_id"));
					value.put("ETL_TITLE"									, list.get(i).get("etl_title"));
					value.put("TARGET_DBINFO"						, list.get(i).get("target_dbinfo"));
					value.put("TARGET_USER"							, list.get(i).get("target_user"));
					value.put("TARGET_TABLE"						, list.get(i).get("target_table"));
					value.put("TARGET_TABLE_NAME"				, list.get(i).get("target_table_name"));
					value.put("TARGET_REMK"							, list.get(i).get("target_remk"));
					value.put("SOURCE_DBINFO"						, list.get(i).get("source_dbinfo"));
					value.put("SOURCE_USER"							, list.get(i).get("source_user"));
					value.put("SOURCE_TABLE"						, list.get(i).get("source_table"));
					value.put("SOURCE_TABLE_NAME"				, list.get(i).get("source_table_name"));
					value.put("SOURCE_REMK"						, list.get(i).get("source_remk"));
					value.put("DBLINK_NAME"							, list.get(i).get("dblink_name"));
					value.put("GBN1"										, list.get(i).get("gbn1"));
					value.put("GBN2"										, list.get(i).get("gbn2"));
					value.put("ETL_YN"									, list.get(i).get("etl_yn"));
					value.put("ETL_MTHD"								, list.get(i).get("etl_mthd"));
					value.put("ROW_CNT"								, list.get(i).get("row_cnt"));
					value.put("ETL_CYCLE"								, list.get(i).get("etl_cycle"));
					value.put("SOURCE_SELECT_QUERY"			, list.get(i).get("soure_select_query"));
					value.put("TARGET_DELETE_QUERY"			, list.get(i).get("target_delete_query"));
					value.put("TARGET_INSERT_QUERY"			, list.get(i).get("target_insert_query"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Etl Manager List Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getEtlManagerDetail ( String etlID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ETL_ID"													, Integer.valueOf(etlID));
		try{
			logger.info("Get Etl Manager Detail Data Start");
			list = getSqlSession().selectList("getEtlManagerDetail", map);
			logger.info("Get Etl Manager Detail Data End");
			if(list.size()>0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("ETL_ID"									, list.get(i).get("etl_id"));
					value.put("ETL_TITLE"									, list.get(i).get("etl_title"));
					value.put("TARGET_DBINFO"						, list.get(i).get("target_dbinfo"));
					value.put("TARGET_USER"							, list.get(i).get("target_user"));
					value.put("TARGET_TABLE"						, list.get(i).get("target_table"));
					value.put("TARGET_TABLE_NAME"				, list.get(i).get("target_table_name"));
					value.put("TARGET_REMK"							, list.get(i).get("target_remk"));
					value.put("SOURCE_DBINFO"						, list.get(i).get("source_dbinfo"));
					value.put("SOURCE_USER"							, list.get(i).get("source_user"));
					value.put("SOURCE_TABLE"						, list.get(i).get("source_table"));
					value.put("SOURCE_TABLE_NAME"				, list.get(i).get("source_table_name"));
					value.put("SOURCE_REMK"						, list.get(i).get("source_remk"));
					value.put("DBLINK_NAME"							, list.get(i).get("dblink_name"));
					value.put("GBN1"										, list.get(i).get("gbn1"));
					value.put("GBN2"										, list.get(i).get("gbn2"));
					value.put("ETL_YN"									, list.get(i).get("etl_yn"));
					value.put("ETL_MTHD"								, list.get(i).get("etl_mthd"));
					value.put("ROW_CNT"								, list.get(i).get("row_cnt"));
					value.put("ETL_CYCLE"								, list.get(i).get("etl_cycle"));
					value.put("SOURCE_SELECT_QUERY"			, list.get(i).get("source_select_query"));
					value.put("TARGET_DELETE_QUERY"			, list.get(i).get("target_delete_query"));
					value.put("TARGET_INSERT_QUERY"			, list.get(i).get("target_insert_query"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Etl Detail Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getEtlManagerDBInfoList() throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		try{
			logger.info("Get Etl Manager DB Info List Data Start");
			list = getSqlSession().selectList("getEtlManagerDBInfoList");
			logger.info("Get Etl Manager DB Info List Data End");
			if(list.size()>0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("DBKEY"											, list.get(i).get("dbkey"));
					value.put("DBNAME"										, list.get(i).get("dbname"));
					value.put("VENDERCODE"									, list.get(i).get("vendercode"));
					value.put("SCHEMATYPE"									, list.get(i).get("schematype"));
					value.put("SCHEMANAME"								, list.get(i).get("schemaname"));
					value.put("IPADDRESS"										, list.get(i).get("ipaddress"));
					value.put("PORT"												, list.get(i).get("port"));
					value.put("USERID"											, list.get(i).get("userid"));
					value.put("PWD"												, list.get(i).get("pwd"));
					value.put("MAXACTIVE"									, list.get(i).get("maxactive"));
					value.put("MAXIDLE"										, list.get(i).get("maxidle"));
					value.put("MAXWAIT"										, list.get(i).get("maxwait"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Etl Manager DB Info List Data Error : {} " , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public String getMaxEtlID () throws Exception {
		String returnValue = "";
		List<HashMap<String, Object>> maxEtlID = getSqlSession().selectList("getMaxEtlID");
		
		returnValue = maxEtlID.get(0).get("etl_id").toString();
		
		return returnValue;
	}
	public int insertEtlManager ( Etl etl ) throws Exception{
		int returnValue = 0;
		TableObject idwd2000 = new TableObject();
		idwd2000.put("TARGET_DBINFO"											, etl.getTargetDbInfo());
		idwd2000.put("TARGET_USER"												, etl.getTargetUser());
		idwd2000.put("TARGET_TABLE"												, etl.getTargetTable());
		idwd2000.put("TARGET_TABLE_NAME"										, etl.getTargetTableName());
		idwd2000.put("TARGET_REMK"												, etl.getTargetRemk());
		idwd2000.put("SOURCE_DBINFO"											, etl.getSourceDbInfo());
		idwd2000.put("SOURCE_USER"												, etl.getSourceUser());
		idwd2000.put("SOURCE_TABLE"												, etl.getSourceTable());
		idwd2000.put("SOURCE_TABLE_NAME"										, etl.getSourceTableName());
		idwd2000.put("SOURCE_REMK"												, etl.getSourceRemk());
		idwd2000.put("GBN1"														, etl.getGbn1());
		idwd2000.put("GBN2"														, etl.getGbn2());
		idwd2000.put("ETL_YN"													, etl.getEtlYn());
		idwd2000.put("ETL_MTHD"													, etl.getEtlMthd());
		idwd2000.put("ETL_CYCLE"												, etl.getEtlCycle());
		idwd2000.put("ETL_TITLE"												, etl.getEtlTitle());
		idwd2000.put("SOURCE_SELECT_QUERY"										, etl.getSourceSelectQuery());
		idwd2000.put("TARGET_DELETE_QUERY"										, etl.getTargetDeleteQuery());
		idwd2000.put("TARGET_INSERT_QUERY"										, etl.getTargetInsertQuery());
		
		try{
			if(etl.getEtlID().equals("")){
				idwd2000.put("ETL_ID"													, Integer.valueOf(this.getMaxEtlID()));
				logger.info("Insert ETL Manager Data Start");
				dbmsDAO.insertTable("iportal_custom.IDWD2000", idwd2000);
				logger.info("Insert ETL Manager Data End");
			}else{
				idwd2000.put("ETL_ID"													, Integer.valueOf(etl.getEtlID()), true);
				logger.info("Update ETL Manager Data Start");
				dbmsDAO.updateTable("iportal_custom.IDWD2000", idwd2000.getKeyMap(), idwd2000);
				logger.info("Update ETL Manager Data End");
			}	
		}catch(Exception e){
			logger.info("Insert/Update ETL Manager Data Error : {} " , e);
			e.printStackTrace();
			returnValue = 2;
			throw e;
		}
		
		return returnValue;
	}
	public int removeEtlManager ( Etl etl ) throws Exception {
		int returnValue = 0;
		TableObject idwd2000 = new TableObject();
		idwd2000.put("ETL_ID"												, Integer.valueOf(etl.getEtlID()));
		
		try{
			logger.info("Remove ETL Manager Data Start");
			dbmsDAO.deleteTable("iportal_custom.IDWD2000", idwd2000);
			logger.info("Remove ETL Manager Data End");
		}catch(Exception e){
			logger.error("Remove ETL Manager Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public AjaxMessage callEtl ( String etlID ) throws Exception {
		AjaxMessage msg = new AjaxMessage();
		Proxy proxy = proxyPool.createProxy();
		proxy.start();      
//		
		try{
			JSONObject param = new JSONObject();
			param.put("REQUEST_USER_ID"								, loginSessionInfoFactory.getObject().getUserId());
			param.put("ETL_ID"												, etlID);
			
			msg = proxy.requestRule("etl", "Etl", param);
			
		}catch(Exception e){
			logger.info("Call ETL : {} " ,e);
			throw e;
		}finally{
			proxy.stop();
		}
		return msg;
	}
	public List<HashMap<String, String>> excelDownByEtl(String gbn, String searchKey, String searchValue, String etlYn) throws Exception {
		List<HashMap<String, String>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("GBN"													, gbn);
		map.put("KEY"													, searchKey);
		map.put("SEARCH_VALUE"											, searchValue);
		map.put("ETL_YN"												, etlYn);
		try{
			logger.info("Get Etl Manager List Data Start");
			list = getSqlSession().selectList("getEtlManagerList", map);
			logger.info("Get Etl Manager List Data End");
			
		}catch(Exception e){
			logger.error("Get Etl Manager List Data Error : {} " , e);
			e.printStackTrace();
			throw e;
		}
		return list;
	}
}
