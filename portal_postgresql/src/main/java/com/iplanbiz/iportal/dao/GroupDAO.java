package com.iplanbiz.iportal.dao;

import java.util.HashMap;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;

@Repository
public class GroupDAO extends SqlSessionDaoSupport {
	
	@Autowired
	DBMSDAO dbmsDAO;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	public int deleteGroup(String userID) throws Exception{
		int result = 0;
		try{
			TableObject iect7008 = new TableObject();
			iect7008.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
			iect7008.put("FEKY"									, userID);
			dbmsDAO.deleteTable("iportal_custom.IECT7008", iect7008);
			result = 0;
		}catch(Exception e){
			result = 1;
			logger.error("Group Delete Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return result;
	}


}
