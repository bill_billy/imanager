package com.iplanbiz.iportal.dao.admin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.mail.search.IntegerComparisonTerm;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.codehaus.jackson.map.ser.CustomSerializerFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.io.dbms.TableCondition.Condition;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.comm.CodeManager;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.CustomDBDataDAO;
import com.iplanbiz.iportal.dao.auth.EmpView;
import com.iplanbiz.iportal.dao.auth.GroupView;
import com.iplanbiz.iportal.dao.system.CustomSqlHouseDAO;
import com.iplanbiz.iportal.service.CustomDBDataService;

@Repository
public class MenuAuthorityDAO extends SqlSessionDaoSupport {
	
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Autowired CustomDBDataDAO customDBDAO;
	@Autowired CustomSqlHouseDAO customSqlHouseDAO; 
	@Autowired DBMSDAO dbmsDAO;
	@Autowired EmpView empView;
	@Autowired GroupView groupView;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	public JSONArray getMenuAuthorityTreeList(String type, String fkey) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
			map.put("FKEY"										, fkey);
			map.put("TYPE"										, type);
			map.put("LOCALE"										, loginSessionInfoFactory.getObject().getLocale());
			logger.info("Get Menu List By Role Data Start");
			listValue = getSqlSession().selectList("getMenuAuthorityTreeList", map);
			
			logger.info("Get Menu List By Role Data End");
			if(listValue.size() > 0){
				logger.info("Get Menu List By Role Data List > JSONArray Start");
				for(int i=0;i<listValue.size();i++){ 
					returnValue.add(listValue.get(i));
				}
				logger.info("Get Menu List By Role Data List > JSONArray End");

			}
		}catch(Exception e){
			logger.error("Get Menu By Role Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityTreeListAdmin(String type, String fkey) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
			map.put("FKEY"										, fkey);
			map.put("TYPE"										, type);
			logger.info("Get Menu List By Role Admin Data Start");
			listValue = getSqlSession().selectList("getMenuAuthorityTreeListAdmin", map);
			logger.info("Get Menu List By Role Admin Data End");
			if(listValue.size() > 0){
				logger.info("Get Menu List By Role Admin Data List > JSONArray Start");
				for(int i=0;i<listValue.size();i++){
					returnValue.add(listValue.get(i));
				}
				logger.info("Get Menu List By Role Admin Data List > JSONArray End");
			}
		}catch(Exception e){
			logger.error("Get Menu By Role Admin Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityList(String fkey, String type) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
			map.put("FKEY"												, fkey);
			map.put("TYPE"												, type);
			logger.info("Get Menu Authority List Data Start");
			listValue = getSqlSession().selectList("getMenuAuthorityList", map);
			logger.info("Get Menu Authority List Data End");
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("C_ID"										, listValue.get(i).get("c_id"));
					returnValue.add(putValue);
				}
			}
		}catch(Exception e){
			logger.error("Get Menu Authority List Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityListAdmin(String fkey, String type) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
			map.put("FKEY"												, fkey);
			map.put("TYPE"												, type);
			logger.info("Get Menu Authority List Admin Data Start");
			listValue = getSqlSession().selectList("getMenuAuthorityListAdmin", map);
			logger.info("Get Menu Authority List Admin Data End");
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("C_ID"										, listValue.get(i).get("c_id"));
					returnValue.add(putValue);
				}
			}
		}catch(Exception e){
			logger.error("Get Menu Authority List Admin Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	/**
	 * 롤에 포함되지 않은 그룹 리스트
	 * @param roleID      롤ID
	 * @param userGrpName 그룹명 검색 키워드
	 * @param grpTyp      그룹구분
	 * @return
	 * @throws Exception
	 */
	public JSONArray getGroupListNotInRole ( String roleID, String userGrpName, String grpTyp ) throws Exception {
		JSONArray returnValue = new JSONArray(); 
		List<HashMap<String, Object>> gidValue = null;
		try {
 
			if(!roleID.equals("")){
				HashMap<String, Object> gidMap = new HashMap<String, Object>();
				gidMap.put("GID"										, roleID);
				gidMap.put("TYPE"										, "p");
				gidValue = getSqlSession().selectList("getChildInRole", gidMap);
				String gidReturnValue = ""; 
	//			logger.info("GID Value = {}", gidValue.get(0).get("value").toString());
				logger.info("gidValue Size = {}", gidValue.size());
				if(!gidValue.get(0).get("value").toString().equals("NULL")){
					gidReturnValue = gidValue.get(0).get("value").toString();
				}else{
					gidReturnValue = "";
				}
				List<LinkedHashMap<String, Object>> listCustom = groupView.search("", userGrpName, grpTyp,"",gidReturnValue,loginSessionInfoFactory.getObject().getAcctID());
					
					
				if(listCustom != null){
					logger.info("listCustom.size():{}",listCustom.size());
					for(int j=0;j<listCustom.size();j++){
						JSONObject value = new JSONObject(); 
						returnValue.add(listCustom.get(j));
					}
				}
			}
		} catch (Exception e) {
			logger.error("Get Menu Authority Group List No Select Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityGroupNoListAdmin ( String gID, String userGrpName, String grpTyp ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
			map.put("GID"											, gID);
			map.put("USER_GRP_NM"									, userGrpName);
			map.put("GRP_TYP"										, grpTyp);
			
			logger.info("Get Menu Authority Group Lise No Select Admin Data Start");
			listValue = getSqlSession().selectList("getMenuAuthorityGroupNoListAdmin", map);
			logger.info("Get Menu Authority Group List No Select Admin Data End");
			if(listValue.size() > 0) {
				for(int i=0;i<listValue.size();i++) {
					JSONObject putValue = new JSONObject();
					putValue.put("USER_GRP_ID"						, listValue.get(i).get("user_grp_id"));
					putValue.put("USER_GRP_NM"						, listValue.get(i).get("user_grp_nm"));
					putValue.put("GRP_TYP"							, listValue.get(i).get("grp_typ"));
					putValue.put("GRP_TYP_NM"						, listValue.get(i).get("grp_typ_nm"));
					returnValue.add(putValue);
				}
			}
		} catch (Exception e) {
			logger.error("Get Menu Authority Group List No Select Admin Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityGroupList(String gID, String userGrpName, String grpTyp) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<LinkedHashMap<String, Object>> listValue = null;
		if(gID.trim().equals("")) {
			return returnValue;
		}
		try{
 
			TableCondition iect7010 = new TableCondition();
			iect7010.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
			iect7010.put("GID"												, gID); 
			iect7010.put("TYPE"												, "p");
			List<LinkedHashMap<String,Object>> list = dbmsDAO.selectTable(false, WebConfig.getCustomTableName("IECT7010"),iect7010);
			StringBuilder userGrps = new StringBuilder();
			for(int i = 0; i < list.size();i++) {
				String fkey = list.get(i).get("FKEY").toString();
				userGrps.append(fkey);
				if(i!=list.size()-1)
					userGrps.append(",");
			} 
			logger.info("userGrps:{}",userGrps.toString());
			if(userGrps.toString().equals("")==false)
				listValue = groupView.search("", userGrpName, grpTyp, userGrps.toString(), "", loginSessionInfoFactory.getObject().getAcctID());	
			else
				listValue = new ArrayList<LinkedHashMap<String,Object>>();
			logger.info("listValue.size():{}",listValue.size());
			if(listValue.size() > 0){
				for(int i=0; i<listValue.size();i++){ 
					returnValue.add(listValue.get(i));
				}
			}
			 
		}catch(Exception e){
			logger.error("Get Menu Authority Group List Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityGroupListAdmin(String gID, String userGrpName, String grpTyp) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
			map.put("GID"												, gID);
			map.put("USER_GRP_NM"									, userGrpName);
			map.put("GRP_TYP"											, grpTyp);
			logger.info("Get Menu Authority Group List Admin Data Start");
			listValue = getSqlSession().selectList("getMenuAuthorityGroupListAdmin", map);
			logger.info("Get Menu Authority Group List Admin Data End");
			if(listValue.size() > 0){
				for(int i=0; i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("USER_GRP_ID"								, listValue.get(i).get("user_grp_id"));
					putValue.put("USER_GRP_NM"							, listValue.get(i).get("user_grp_nm"));
					putValue.put("GRP_TYP"									, listValue.get(i).get("grp_typ"));
					putValue.put("GRP_TYP_NM"								, listValue.get(i).get("grp_typ_nm"));
					returnValue.add(putValue);
				}
			}
		}catch(Exception e){
			logger.error("Get Menu Authority Group List Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityUserListNoByRole ( String gID, String empID, String empName, String isNull ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try {
  
			if(!gID.equals("")){
				List<HashMap<String, Object>> empValue = null;
				HashMap<String, Object> customMap = new HashMap<String, Object>();
				customMap.put("GID"										, gID);
				customMap.put("TYPE"									, "u");
				empValue = getSqlSession().selectList("getChildInRole", customMap);
				List<LinkedHashMap<String, Object>> listCustom = null;
				if(isNull.equals("")==false){
					 listCustom = empView.search(empID, empName, "", loginSessionInfoFactory.getObject().getAcctID(), empValue.get(0).get("value").toString()); 
				}
				if(listCustom != null){
					for(int j=0;j<listCustom.size();j++){
						JSONObject customValue = new JSONObject(); 
						returnValue.add(listCustom.get(j));
					}
				}
			}
		} catch (Exception e) {
			logger.error("Get Menu Authority Role User List Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityUserListNoByRoleAdmin ( String gID, String empID, String empName, String isNull ) throws Exception {
		JSONArray returnValue = new JSONArray(); 
		List<HashMap<String, Object>> empValue = null;
		try {
 
			if(!gID.equals("")) {
				HashMap<String, Object> customMap = new HashMap<String, Object>();
				customMap.put("GID"										, gID);
				customMap.put("TYPE"									, "u");
				empValue = getSqlSession().selectList("getChildInRole", customMap);
 
				List<LinkedHashMap<String, Object>> listCustom = null;
				if(isNull.equals("")==false){
					listCustom = empView.search(empID, empName, "", loginSessionInfoFactory.getObject().getAcctID(), empValue.get(0).get("value").toString());
				}
				if(listCustom != null){
					for(int j=0;j<listCustom.size();j++){ 
						returnValue.add(listCustom.get(j));
					}
				}
			}
		} catch (Exception e) {
			logger.error("Get Menu Authority Role User List Admin Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityUserListByRole(String gID, String empID, String empName, String isNull) throws Exception{
		JSONArray returnValue = new JSONArray(); 
		List<HashMap<String, Object>> empValue = null;
		try{

			if(!gID.equals("")) {
				HashMap<String, Object> customMap = new HashMap<String, Object>();
				customMap.put("GID"										, gID);
				customMap.put("TYPE"									, "u");
				empValue = getSqlSession().selectList("getChildInRole", customMap); 
				String users = empValue.get(0).get("value").toString();
				List<LinkedHashMap<String, Object>> listCustom = null;
				if(isNull.equals("")==false){ 
					 listCustom = empView.search(users, empName, "", loginSessionInfoFactory.getObject().getAcctID(), "");
				}
				if(listCustom != null){
					for(int j=0;j<listCustom.size();j++){ 
						returnValue.add(listCustom.get(j));
					}
				}
			}
		}catch(Exception e){
			logger.error("Get Menu Authority Role User List Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityUserListByRoleAdmin(String gID, String empID, String empName, String isNull) throws Exception{
		JSONArray returnValue = new JSONArray(); 
		List<HashMap<String, Object>> empValue = null;
		try{
 
			if(!gID.equals("")) {
				HashMap<String, Object> customMap = new HashMap<String, Object>();
				customMap.put("GID"										, gID);
				customMap.put("TYPE"									, "u");
				empValue = getSqlSession().selectList("getChildInRole", customMap);
 
				List<LinkedHashMap<String, Object>> listCustom = null;
				if(isNull.equals("")==false){ 
					 listCustom = empView.search(empValue.get(0).get("value").toString(), empName, "", loginSessionInfoFactory.getObject().getAcctID(), "");
				}
				if(listCustom != null){
					for(int j=0;j<listCustom.size();j++){ 
						returnValue.add(listCustom.get(j));
					}
				}
			}
		}catch(Exception e){
			logger.error("Get Menu Authority Role User List Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public String getMaxRoleID() throws Exception{
		String returnValue = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		try{
			logger.info("Get Max Role ID Start");
			HashMap<String, Object> returnMap = getSqlSession().selectOne("getMaxRoleID", map);
			logger.info("Get Max Role ID End");
			returnValue = returnMap.get("gid").toString();
		}catch(Exception e){
			logger.error("Get Max Role ID Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertRoleInfo(String roleID, String roleName, String roleNameOrg, String remoteIP, String remoteOS, String remoteBrowser) throws Exception{
		int returnValue = 0;
		String gid = "";
		TableObject iect7009 = new TableObject();
		iect7009.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID(), true);
		iect7009.put("GROUP_NAME"								, roleName);
		
		HashMap<String,Object> logMap = new HashMap<String,Object>();
		logMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		logMap.put("OBJ_TYP", "R");
		logMap.put("REMOTE_IP", remoteIP);
		logMap.put("REMOTE_OS", remoteOS);
		logMap.put("REMOTE_BROWSER",remoteBrowser);
		logMap.put("INSERT_EMP", loginSessionInfoFactory.getObject().getUserId());
		logMap.put("INSERT_EMP_NAME", loginSessionInfoFactory.getObject().getUserName());
		
		try{
			if(roleID.equals("")){
				gid= this.getMaxRoleID();
				iect7009.put("GID"										, Integer.parseInt(gid));
				logger.info("Insert Role Start");
				dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7009"), iect7009);
				logger.info("Insert Role End");
				returnValue = 0;
				logMap.put("OBJ_ID", gid);
				logMap.put("OBJ_NAME", roleName);
				logMap.put("LOG_ACTION", "NEW");
				logMap.put("TARGET_TYP", null);
				logMap.put("TARGET_ID", null);
				logMap.put("TARGET_NAME", null);
			}else{
				gid= roleID;
				iect7009.put("GID"										, Integer.valueOf(gid), true);
				logger.info("Update Role Start");
				dbmsDAO.updateTable(WebConfig.getCustomTableName("IECT7009"), iect7009.getKeyMap(), iect7009);
				logger.info("Update Role End");
				returnValue = 1;
				logMap.put("OBJ_ID", gid);
				logMap.put("OBJ_NAME", roleNameOrg);
				logMap.put("LOG_ACTION", "MOD");
				logMap.put("TARGET_TYP", "R");
				logMap.put("TARGET_ID", gid);
				logMap.put("TARGET_NAME", roleName);
			}
			
			this.getSqlSession().insert("insertKtotoAuthLog", logMap);
			this.getSqlSession().insert("insertMenuAuthLog", logMap);
		}catch(Exception e){
			returnValue = 2;
			logger.error("Role Insert/Update Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertRoleInfoAdmin(String roleID, String roleName) throws Exception{
		int returnValue = 0;
		TableObject iect7009 = new TableObject();
		iect7009.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID(), true);
		iect7009.put("GROUP_NAME"								, roleName);
		try{
			if(roleID.equals("")){
				iect7009.put("GID"										, Integer.valueOf(this.getMaxRoleID()));
				logger.info("Insert Role Start");
				dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7009"), iect7009);
				logger.info("Insert Role End");
				returnValue = 0;
			}else{
				iect7009.put("GID"										, Integer.valueOf(roleID), true);
				logger.info("Update Role Start");
				dbmsDAO.updateTable(WebConfig.getCustomTableName("IECT7009"), iect7009.getKeyMap(), iect7009);
				logger.info("Update Role End");
				returnValue = 1;
			}
		}catch(Exception e){
			returnValue = 2;
			logger.error("Role Insert/Update Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int deleteRoleInfo(String roleID, String roleName, String remoteIP, String remoteOS, String remoteBrowser) throws Exception{
		int returnValue = 0;
		TableObject iect7009 = new TableObject();
		iect7009.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		iect7009.put("GID"														, Integer.valueOf(roleID));
		
		TableObject menuDelete = new TableObject();
		menuDelete.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		menuDelete.put("FKEY"										, roleID);
		menuDelete.put("TYPE"										, "g");
		
		TableObject userDelete = new TableObject();
		userDelete.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		userDelete.put("GID"										, Integer.valueOf(roleID));
		userDelete.put("TYPE"										, "u");
		
		TableObject groupDelete = new TableObject();
		groupDelete.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		groupDelete.put("GID"											, Integer.valueOf(roleID));
		groupDelete.put("TYPE"										, "p");
		try{
			logger.info("Role Info Delete Start");
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7009"), iect7009);
			logger.info("Role Info Delete End");
			logger.info("Role Authority Delete Start");
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7004"), menuDelete);
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7010"), userDelete);
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7010"), groupDelete);
			logger.info("Role Authority Delete End");
			returnValue = 0;
			
			HashMap<String,Object> logMap = new HashMap<String,Object>();
			logMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
			logMap.put("OBJ_TYP", "R");
			logMap.put("OBJ_ID", roleID);
			logMap.put("OBJ_NAME", roleName);
			logMap.put("REMOTE_IP", remoteIP);
			logMap.put("REMOTE_OS", remoteOS);
			logMap.put("REMOTE_BROWSER",remoteBrowser);
			logMap.put("INSERT_EMP", loginSessionInfoFactory.getObject().getUserId());
			logMap.put("INSERT_EMP_NAME", loginSessionInfoFactory.getObject().getUserName());
			logMap.put("LOG_ACTION", "DEL");
			logMap.put("TARGET_TYP", null);
			logMap.put("TARGET_ID", null);
			logMap.put("TARGET_NAME", null);
			this.getSqlSession().insert("insertKtotoAuthLog", logMap);
			this.getSqlSession().insert("insertMenuAuthLog", logMap);
		}catch(Exception e){
			returnValue = 1;
			logger.error("Role Info Delete Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int deleteRoleInfoAdmin(String roleID) throws Exception{
		int returnValue = 0;
		TableObject iect7009 = new TableObject();
		iect7009.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		iect7009.put("GID"														, Integer.valueOf(roleID));
		
		TableObject menuDelete = new TableObject();
		menuDelete.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		menuDelete.put("FKEY"										, roleID);
		menuDelete.put("TYPE"										, "g");
		
		TableObject userDelete = new TableObject();
		userDelete.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		userDelete.put("GID"										, Integer.valueOf(roleID));
		userDelete.put("TYPE"										, "u");
		
		TableObject groupDelete = new TableObject();
		groupDelete.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		groupDelete.put("GID"											, Integer.valueOf(roleID));
		groupDelete.put("TYPE"										, "p");
		try{
			logger.info("Role Info Delete Start");
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7009"), iect7009);
			logger.info("Role Info Delete End");
			logger.info("Role Authority Delete Start");
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7004"), menuDelete);
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7010"), userDelete);
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7010"), groupDelete);
			logger.info("Role Authority Delete End");
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("Role Info Delete Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertRoleMapping(String roleID, String roleName, String[] menuCheckValue, String[] userCheckValue, String[] groupCheckValue, String[] menuCheckName, String[] userCheckName, String[] groupCheckName, String remoteIP, String remoteOS, String remoteBrowser) throws Exception{
	
		List<HashMap<String, Object>> originMenuList = null;
		List<HashMap<String, Object>> originUserList = null;
		List<HashMap<String, Object>> originGroupList = null;
		
		HashMap<String, Object> menuMap = new HashMap<String, Object>();
		menuMap.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		menuMap.put("FKEY"													, roleID);
		menuMap.put("TYPE"													, "g");
		try{
			logger.info("getMenuAuthorityRtoM Start!");
			originMenuList = getSqlSession().selectList("getMenuAuthorityRtoM", menuMap);
			logger.info("getMenuAuthorityRtoM End!");
		}catch(Exception e){
			logger.error("getMenuAuthorityRtoM Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		

		HashMap<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		userMap.put("GID"													, Integer.parseInt(roleID));
		userMap.put("TYPE"													, "u");
		try{
			logger.info("getMenuAuthorityRtoU Start!");
			originUserList = getSqlSession().selectList("getMenuAuthorityRtoU", userMap);
			logger.info("getMenuAuthorityRtoU End!");
		}catch(Exception e){
			logger.error("getMenuAuthorityRtoU Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		HashMap<String, Object> groupMap = new HashMap<String, Object>();
		groupMap.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		groupMap.put("GID"													, Integer.parseInt(roleID));
		groupMap.put("TYPE"													, "p");
		try{
			logger.info("getMenuAuthorityRtoG Start!");
			originGroupList = getSqlSession().selectList("getMenuAuthorityRtoG", groupMap);
			logger.info("getMenuAuthorityRtoG End!");
		}catch(Exception e){
			logger.error("getMenuAuthorityRtoG Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		
		int returnValue = 0;
		TableObject menuDelete = new TableObject();
		menuDelete.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		menuDelete.put("FKEY"										, roleID);
		menuDelete.put("TYPE"										, "g");
		
		TableCondition userDelete = new TableCondition(); 
		userDelete.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		userDelete.put("GID"										, Integer.valueOf(roleID));
		userDelete.put("TYPE"										, "u");
		//userDelete.put("FKEY"										, "iplan", Condition.LikeStartWith);
		
		TableObject groupDelete = new TableObject();
		groupDelete.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		groupDelete.put("GID"										, Integer.valueOf(roleID));
		groupDelete.put("TYPE"										, "p");
		
		try{
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7004"), menuDelete);
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7010"), userDelete);
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7010"), groupDelete);
			
			
			ArrayList<LinkedHashMap<String, Object>> arrayMenu = new ArrayList<LinkedHashMap<String,Object>>(); 
			for(int i=0;i<menuCheckValue.length;i++){
				if(!menuCheckValue[i].equals("")){
					TableObject menu = new TableObject();
					menu.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
					menu.put("C_ID"											, Integer.valueOf(menuCheckValue[i]));
					menu.put("FKEY"											, roleID);
					menu.put("TYPE"											, "g");
					arrayMenu.add(menu);
				}
			}
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7004"), arrayMenu);
			 
			
			if(originMenuList.size() >0){
				if(menuCheckValue.length>0) {
					if(!menuCheckValue[0].equals("")){
						mappingLog("R",roleID,roleName,"M",originMenuList,menuCheckValue,menuCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
				mappingCancelLog("R",roleID,roleName,"M",originMenuList,menuCheckValue,menuCheckName,remoteIP,remoteOS,remoteBrowser);
			}else {
				if(menuCheckValue.length>0) {
					if(!menuCheckValue[0].equals("")){
						mappingLog("R",roleID,roleName,"M",originMenuList,menuCheckValue,menuCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
			}
			
			ArrayList<LinkedHashMap<String, Object>> arrayUser = new ArrayList<LinkedHashMap<String,Object>>();
			for(int j=0;j<userCheckValue.length;j++){
				if(!userCheckValue[j].equals("")){
					TableObject user = new TableObject();
					user.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
					user.put("GID"												, Integer.valueOf(roleID));
					user.put("FKEY"											, userCheckValue[j]);
					user.put("TYPE"											, "u");
					arrayUser.add(user);
				}
			}
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7010"), arrayUser);
			
			if(originUserList.size() >0){
				if(userCheckValue.length>0) {
					if(!userCheckValue[0].equals("")){
						mappingLog("R",roleID,roleName,"U",originUserList,userCheckValue,userCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
				mappingCancelLog("R",roleID,roleName,"U",originUserList,userCheckValue,userCheckName,remoteIP,remoteOS,remoteBrowser);
			}else {
				if(userCheckValue.length>0) {
					if(!userCheckValue[0].equals("")){
						mappingLog("R",roleID,roleName,"U",originUserList,userCheckValue,userCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
			}
			
			ArrayList<LinkedHashMap<String, Object>> arrayGroup = new ArrayList<LinkedHashMap<String,Object>>();
			for(int k=0;k<groupCheckValue.length;k++){
				if(!groupCheckValue[k].equals("")){
					TableObject group = new TableObject();
					group.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
					group.put("GID"												, Integer.valueOf(roleID));
					group.put("FKEY"												, groupCheckValue[k]);
					group.put("TYPE"												, "p");
					arrayGroup.add(group);
				}
			}
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7010"), arrayGroup);
			
			if(originGroupList.size() >0){
				if(groupCheckValue.length>0) {
					if(!groupCheckValue[0].equals("")){
						mappingLog("R",roleID,roleName,"G",originGroupList,groupCheckValue,groupCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
				mappingCancelLog("R",roleID,roleName,"G",originGroupList,groupCheckValue,groupCheckName,remoteIP,remoteOS,remoteBrowser);
			}else {
				if(groupCheckValue.length>0) {
					if(!groupCheckValue[0].equals("")){
						mappingLog("R",roleID,roleName,"G",originGroupList,groupCheckValue,groupCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
			}
			
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("Insert Role Mapping Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int mappingLog(String type,String id,String name,String target_type,List<HashMap<String, Object>> originList,String[] newList,String[] newNameList,String remoteIP,String remoteOS,String remoteBrowser) {
		HashMap<String,Object> logMap = new HashMap<String,Object>();
		logMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		logMap.put("REMOTE_IP", remoteIP);
		logMap.put("REMOTE_OS", remoteOS);
		logMap.put("REMOTE_BROWSER", remoteBrowser);
		logMap.put("INSERT_EMP", loginSessionInfoFactory.getObject().getUserId());
		logMap.put("INSERT_EMP_NAME", loginSessionInfoFactory.getObject().getUserName());
		logMap.put("LOG_ACTION", "MAPPING");
		
		//this.getSqlSession().insert("insertKtotoAuthLog", logMap);
		if(target_type.equals("M")) {//롤&그룹에 보고서 매핑
			for(int i=0;i<newList.length;i++) {
				boolean isMapping = true;
				for(int j=0;j<originList.size();j++) {
					String cid = originList.get(j).get("c_id").toString();
					if(newList[i].equals(cid)) {
						isMapping=false;
					}
				}
				if(isMapping) {//새로 매핑된 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					
					logMap.put("TARGET_TYP", target_type);
					logMap.put("TARGET_ID", newList[i]);
					logMap.put("TARGET_NAME", newNameList[i]);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
			}
		}
		if(type.equals("R")&&target_type.equals("U")) {//롤에 사용자 매핑
			for(int i=0;i<newList.length;i++) {
				boolean isMapping = true;
				for(int j=0;j<originList.size();j++) {
					String userId = originList.get(j).get("user_id").toString();
					if(newList[i].equals(userId)) {
						isMapping=false;
					}
				}
				if(isMapping) {//새로 매핑된 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					
					logMap.put("TARGET_TYP", target_type);
					logMap.put("TARGET_ID", newList[i]);
					logMap.put("TARGET_NAME", newNameList[i]);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
			}
		}
		if(type.equals("R")&&target_type.equals("G")) {//롤에 그룹 매핑
			for(int i=0;i<newList.length;i++) {
				boolean isMapping = true;
				for(int j=0;j<originList.size();j++) {
					String grpId = originList.get(j).get("grp_id").toString();
					if(newList[i].equals(grpId)) {
						isMapping=false;
					}
				}
				if(isMapping) {//새로 매핑된 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					
					logMap.put("TARGET_TYP", target_type);
					logMap.put("TARGET_ID", newList[i]);
					logMap.put("TARGET_NAME", newNameList[i]);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
			}
		}
		if(type.equals("G")&&target_type.equals("R")) {//그룹에 롤 매핑
			for(int i=0;i<newList.length;i++) {
				boolean isMapping = true;
				for(int j=0;j<originList.size();j++) {
					String roleId = originList.get(j).get("role_id").toString();
					if(newList[i].equals(roleId)) {
						isMapping=false;
					}
				}
				if(isMapping) {//새로 매핑된 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					
					logMap.put("TARGET_TYP", target_type);
					logMap.put("TARGET_ID", newList[i]);
					logMap.put("TARGET_NAME", newNameList[i]);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
			}
		}
		if(type.equals("M")&&target_type.equals("U")) {//메뉴에 사용자 매핑
			for(int i=0;i<newList.length;i++) {
				boolean isMapping = true;
				for(int j=0;j<originList.size();j++) {
					String userId = originList.get(j).get("user_id").toString();
					if(newList[i].equals(userId)) {
						isMapping=false;
					}
				}
				if(isMapping) {//새로 매핑된 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					
					logMap.put("TARGET_TYP", target_type);
					logMap.put("TARGET_ID", newList[i]);
					logMap.put("TARGET_NAME", newNameList[i]);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
			}
		}
		if(type.equals("M")&&target_type.equals("G")) {//메뉴에 그룹 매핑
			for(int i=0;i<newList.length;i++) {
				boolean isMapping = true;
				for(int j=0;j<originList.size();j++) {
					String grpId = originList.get(j).get("grp_id").toString();
					if(newList[i].equals(grpId)) {
						isMapping=false;
					}
				}
				if(isMapping) {//새로 매핑된 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					
					logMap.put("TARGET_TYP", target_type);
					logMap.put("TARGET_ID", newList[i]);
					logMap.put("TARGET_NAME", newNameList[i]);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
			}
		}
		if(type.equals("M")&&target_type.equals("R")) {//메뉴에 롤 매핑
			for(int i=0;i<newList.length;i++) {
				boolean isMapping = true;
				for(int j=0;j<originList.size();j++) {
					String roleId = originList.get(j).get("role_id").toString();
					if(newList[i].equals(roleId)) {
						isMapping=false;
					}
				}
				if(isMapping) {//새로 매핑된 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					
					logMap.put("TARGET_TYP", target_type);
					logMap.put("TARGET_ID", newList[i]);
					logMap.put("TARGET_NAME", newNameList[i]);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
			}
		}
		if(type.equals("D")&&target_type.equals("U")) {//데이터에 사용자 매핑
			for(int i=0;i<newList.length;i++) {
				boolean isMapping = true;
				for(int j=0;j<originList.size();j++) {
					String userId = originList.get(j).get("user_id").toString();
					if(newList[i].equals(userId)) {
						isMapping=false;
					}
				}
				if(isMapping) {//새로 매핑된 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					
					logMap.put("TARGET_TYP", target_type);
					logMap.put("TARGET_ID", newList[i]);
					logMap.put("TARGET_NAME", newNameList[i]);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
			}
		}
		if(type.equals("D")&&target_type.equals("G")) {//데이터에 그룹 매핑
			for(int i=0;i<newList.length;i++) {
				boolean isMapping = true;
				for(int j=0;j<originList.size();j++) {
					String grpId = originList.get(j).get("grp_id").toString();
					if(newList[i].equals(grpId)) {
						isMapping=false;
					}
				}
				if(isMapping) {//새로 매핑된 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					
					logMap.put("TARGET_TYP", target_type);
					logMap.put("TARGET_ID", newList[i]);
					logMap.put("TARGET_NAME", newNameList[i]);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
			}
		}
		if(type.equals("D")&&target_type.equals("R")) {//데이터에 롤 매핑
			for(int i=0;i<newList.length;i++) {
				boolean isMapping = true;
				for(int j=0;j<originList.size();j++) {
					String roleId = originList.get(j).get("role_id").toString();
					if(newList[i].equals(roleId)) {
						isMapping=false;
					}
				}
				if(isMapping) {//새로 매핑된 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					
					logMap.put("TARGET_TYP", target_type);
					logMap.put("TARGET_ID", newList[i]);
					logMap.put("TARGET_NAME", newNameList[i]);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
			}
		}
		return 0;
	}
	public int mappingCancelLog(String type,String id,String name,String target_type,List<HashMap<String, Object>> originList,String[] newList,String[] newNameList,String remoteIP, String remoteOS, String remoteBrowser) {
		
		HashMap<String,Object> logMap = new HashMap<String,Object>();
		logMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		logMap.put("REMOTE_IP", remoteIP);
		logMap.put("REMOTE_OS", remoteOS);
		logMap.put("REMOTE_BROWSER", remoteBrowser);
		logMap.put("INSERT_EMP", loginSessionInfoFactory.getObject().getUserId());
		logMap.put("INSERT_EMP_NAME", loginSessionInfoFactory.getObject().getUserName());
		logMap.put("LOG_ACTION", "CANCLE");
		
		if(target_type.equals("M")) {//롤&그룹에 보고서 매핑해제
			for(int i=0;i<originList.size();i++) {
				boolean isCancle = true;
				String cid = originList.get(i).get("c_id").toString();
				Object cname = originList.get(i).get("c_name");
				for(int j=0;j<newList.length;j++) {
					if(newList[j].equals(cid)) {
						isCancle=false;
					}
				}
				if(isCancle) {//매핑 해제할 타겟

					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					logMap.put("TARGET_TYP", target_type);
					
					logMap.put("TARGET_ID", cid);
					logMap.put("TARGET_NAME", cname);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
				
			}
		}
		if(type.equals("R")&&target_type.equals("U")) {//롤에 사용자 매핑해제
			for(int i=0;i<originList.size();i++) {
				boolean isCancle = true;
				String userId = originList.get(i).get("user_id").toString();
				Object userName = originList.get(i).get("user_name");
				for(int j=0;j<newList.length;j++) {
					if(newList[j].equals(userId)) {
						isCancle=false;
					}
				}
				if(isCancle) {//매핑 해제할 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					logMap.put("TARGET_TYP", target_type);
					
					logMap.put("TARGET_ID", userId);
					logMap.put("TARGET_NAME", userName);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
				
			}
		}
		if(type.equals("R")&&target_type.equals("G")) {//롤에 그룹 매핑해제
			for(int i=0;i<originList.size();i++) {
				boolean isCancle = true;
				String grpId = originList.get(i).get("grp_id").toString();
				Object grpName = originList.get(i).get("grp_nm");
				for(int j=0;j<newList.length;j++) {
					if(newList[j].equals(grpId)) {
						isCancle=false;
					}
				}
				if(isCancle) {//매핑 해제할 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					logMap.put("TARGET_TYP", target_type);
					
					logMap.put("TARGET_ID", grpId);
					logMap.put("TARGET_NAME", grpName);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
				
			}
		}
		
		if(type.equals("G")&&target_type.equals("R")) {//그룹에 롤 매핑해제
			for(int i=0;i<originList.size();i++) {
				boolean isCancle = true;
				String roleId = originList.get(i).get("role_id").toString();
				Object roleName = originList.get(i).get("role_nm");
				for(int j=0;j<newList.length;j++) {
					if(newList[j].equals(roleId)) {
						isCancle=false;
					}
				}
				if(isCancle) {//매핑 해제할 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					logMap.put("TARGET_TYP", target_type);
					
					logMap.put("TARGET_ID", roleId);
					logMap.put("TARGET_NAME", roleName);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
				
			}
		}
		if(type.equals("M")&&target_type.equals("U")) {//메뉴에 사용자 매핑해제
			for(int i=0;i<originList.size();i++) {
				boolean isCancle = true;
				String userId = originList.get(i).get("user_id").toString();
				Object userName = originList.get(i).get("user_name");
				for(int j=0;j<newList.length;j++) {
					if(newList[j].equals(userId)) {
						isCancle=false;
					}
				}
				if(isCancle) {//매핑 해제할 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					logMap.put("TARGET_TYP", target_type);
					
					logMap.put("TARGET_ID", userId);
					logMap.put("TARGET_NAME", userName);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
				
			}
		}
		if(type.equals("M")&&target_type.equals("G")) {//메뉴에 그룹 매핑해제
			for(int i=0;i<originList.size();i++) {
				boolean isCancle = true;
				String grpId = originList.get(i).get("grp_id").toString();
				Object grpName = originList.get(i).get("grp_nm");
				for(int j=0;j<newList.length;j++) {
					if(newList[j].equals(grpId)) {
						isCancle=false;
					}
				}
				if(isCancle) {//매핑 해제할 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					logMap.put("TARGET_TYP", target_type);
					
					logMap.put("TARGET_ID", grpId);
					logMap.put("TARGET_NAME", grpName);
					
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
				
			}
		}
		if(type.equals("M")&&target_type.equals("R")) {//메뉴에 롤 매핑해제
			for(int i=0;i<originList.size();i++) {
				boolean isCancle = true;
				String roleId = originList.get(i).get("role_id").toString();
				Object roleName = originList.get(i).get("role_nm");
				for(int j=0;j<newList.length;j++) {
					if(newList[j].equals(roleId)) {
						isCancle=false;
					}
				}
				if(isCancle) {//매핑 해제할 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					logMap.put("TARGET_TYP", target_type);
					
					logMap.put("TARGET_ID", roleId);
					logMap.put("TARGET_NAME", roleName);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
				
			}
		}
		if(type.equals("D")&&target_type.equals("U")) {//데이터에 사용자 매핑해제
			for(int i=0;i<originList.size();i++) {
				boolean isCancle = true;
				String userId = originList.get(i).get("user_id").toString();
				Object userName = originList.get(i).get("user_name");
				for(int j=0;j<newList.length;j++) {
					if(newList[j].equals(userId)) {
						isCancle=false;
					}
				}
				if(isCancle) {//매핑 해제할 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					logMap.put("TARGET_TYP", target_type);
					
					logMap.put("TARGET_ID", userId);
					logMap.put("TARGET_NAME", userName);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
				
			}
		}
		if(type.equals("D")&&target_type.equals("G")) {//데이터에 그룹매핑해제
			for(int i=0;i<originList.size();i++) {
				boolean isCancle = true;
				String grpId = originList.get(i).get("grp_id").toString();
				Object grpName = originList.get(i).get("grp_nm");
				for(int j=0;j<newList.length;j++) {
					if(newList[j].equals(grpId)) {
						isCancle=false;
					}
				}
				if(isCancle) {//매핑 해제할 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					logMap.put("TARGET_TYP", target_type);
					
					logMap.put("TARGET_ID", grpId);
					logMap.put("TARGET_NAME", grpName);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
				
			}
		}
		if(type.equals("D")&&target_type.equals("R")) {//데이터에 롤매핑해제
			for(int i=0;i<originList.size();i++) {
				boolean isCancle = true;
				String roleId = originList.get(i).get("role_id").toString();
				Object roleName = originList.get(i).get("role_nm");
				for(int j=0;j<newList.length;j++) {
					if(newList[j].equals(roleId)) {
						isCancle=false;
					}
				}
				if(isCancle) {//매핑 해제할 타겟
					logMap.put("OBJ_TYP", type);
					logMap.put("OBJ_ID", id);
					logMap.put("OBJ_NAME", name);
					logMap.put("TARGET_TYP", target_type);
					
					logMap.put("TARGET_ID", roleId);
					logMap.put("TARGET_NAME", roleName);
					this.getSqlSession().insert("insertKtotoAuthLog", logMap);
					this.getSqlSession().insert("insertMenuAuthLog", logMap);
				}
				
			}
		}
		return 0;
	}
	public int insertRoleMappingAdmin(String roleID, String[] menuCheckValue, String[] userCheckValue, String[] groupCheckValue) throws Exception{
		int returnValue = 0;
		TableObject menuDelete = new TableObject();
		menuDelete.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		menuDelete.put("FKEY"										, roleID);
		menuDelete.put("TYPE"										, "g");
		
		TableObject userDelete = new TableObject();
		userDelete.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		userDelete.put("GID"										, Integer.valueOf(roleID));
		userDelete.put("TYPE"										, "u");
		
		TableObject groupDelete = new TableObject();
		groupDelete.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		groupDelete.put("GID"											, Integer.valueOf(roleID));
		groupDelete.put("TYPE"										, "p");
		
		try{
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7004"), menuDelete);
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7010"), userDelete);
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7010"), groupDelete);
			
			
			ArrayList<LinkedHashMap<String, Object>> arrayMenu = new ArrayList<LinkedHashMap<String,Object>>();
			for(int i=0;i<menuCheckValue.length;i++){
				if(!menuCheckValue[i].equals("")){
					TableObject menu = new TableObject();
					menu.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
					menu.put("C_ID"											, Integer.valueOf(menuCheckValue[i]));
					menu.put("FKEY"											, roleID);
					menu.put("TYPE"											, "g");
					arrayMenu.add(menu);
				}
			}
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7004"), arrayMenu);
			
			
			ArrayList<LinkedHashMap<String, Object>> arrayUser = new ArrayList<LinkedHashMap<String,Object>>();
			for(int j=0;j<userCheckValue.length;j++){
				if(!userCheckValue[j].equals("")){
					TableObject user = new TableObject();
					user.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
					user.put("GID"												, Integer.valueOf(roleID));
					user.put("FKEY"											, userCheckValue[j]);
					user.put("TYPE"											, "u");
					arrayUser.add(user);
				}
			}
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7010"), arrayUser);
			
			
			ArrayList<LinkedHashMap<String, Object>> arrayGroup = new ArrayList<LinkedHashMap<String,Object>>();
			for(int k=0;k<groupCheckValue.length;k++){
				if(!groupCheckValue[k].equals("")){
					TableObject group = new TableObject();
					group.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
					group.put("GID"												, Integer.valueOf(roleID));
					group.put("FKEY"												, groupCheckValue[k]);
					group.put("TYPE"												, "p");
					arrayGroup.add(group);
				}
			}
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7010"), arrayGroup);
			
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("Insert Role Mapping Admin Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getGroupList(String userGrpName, String grpTyp) throws Exception{
		JSONArray returnValue = new JSONArray();
 
		try{
			 
			List<LinkedHashMap<String, Object>> listCustom = groupView.search("", userGrpName, grpTyp,"","",loginSessionInfoFactory.getObject().getAcctID());
			if(listCustom != null){
				for(int j=0;j<listCustom.size();j++){
				 
					returnValue.add(listCustom.get(j));
				}
			}
		}catch(Exception e){
			logger.error("Get Group Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getGroupListAdmin(String userGrpName, String grpTyp) throws Exception{
		JSONArray returnValue = new JSONArray(); 
 
		try{
			List<LinkedHashMap<String, Object>> listCustom = groupView.search("", userGrpName, grpTyp,"","",loginSessionInfoFactory.getObject().getAcctID());
			if(listCustom != null){
				for(int j=0;j<listCustom.size();j++){ 
					returnValue.add(listCustom.get(j));
				}
			}
		}catch(Exception e){
			logger.error("Get Group Admin Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getUsersInGroup(String pID, String empID, String empName) throws Exception{
		JSONArray returnValue = new JSONArray();
		try{ 
			if(!pID.equals("")){
				List<LinkedHashMap<String, Object>> customList = empView.search(empID, empName, pID, loginSessionInfoFactory.getObject().getAcctID(),"");
				if(customList != null) {
					if(customList.size() > 0){
						for(int j=0; j<customList.size(); j++){ 
							returnValue.add(customList.get(j));
						}
					}	
				}
			}
		}catch(Exception e){
			logger.error("Get MenuAuthority User List By Group Error : {}" ,e); 
			throw e;
		}
		return returnValue;
	}
	public JSONArray getUsersInGroupAdmin(String pID, String empID, String empName) throws Exception{
		JSONArray returnValue = new JSONArray();
		try{ 
			if(!pID.equals("")){
				List<LinkedHashMap<String, Object>> customList = empView.search(empID, empName, pID, loginSessionInfoFactory.getObject().getAcctID(),"");
				if(customList != null) {
					if(customList.size() > 0){
						for(int j=0; j<customList.size(); j++){ 
							returnValue.add(customList.get(j));
						}
					}	
				}
			}
		}catch(Exception e){
			logger.error("Get MenuAuthority User List By Group Error : {}" ,e); 
			throw e;
		}
		return returnValue;
	}
	public JSONArray getRoleListNoByGroup ( String fkey, String groupName ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("FKEY"												, fkey);
		map.put("GROUP_NAME"										, groupName);
		try {
			logger.info("Get Role Data No Select By Group Start");
			listValue = getSqlSession().selectList("getRoleListNoByGroup", map);
			logger.info("Get Role Data No Select By Group End");
			if(listValue.size() > 0) {
				for(int i=0;i<listValue.size();i++) {
					JSONObject putValue = new JSONObject();
					putValue.put("GID"								, listValue.get(i).get("gid"));
					putValue.put("GROUP_NAME"						, listValue.get(i).get("group_name"));
					returnValue.add(putValue);
				}
			}
		} catch (Exception e) {
			logger.error("Get Role Data No Select By Group Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getRoleListNoByGroupAdmin ( String fkey, String groupName ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("FKEY"												, fkey);
		map.put("GROUP_NAME"										, groupName);
		try {
			logger.info("Get Role Data No Select By Group Admin Start");
			listValue = getSqlSession().selectList("getRoleListNoByGroupAdmin", map);
			logger.info("Get Role Data No Select By Group Admin End");
			if(listValue.size() > 0) {
				for(int i=0;i<listValue.size();i++) {
					JSONObject putValue = new JSONObject();
					putValue.put("GID"								, listValue.get(i).get("gid"));
					putValue.put("GROUP_NAME"						, listValue.get(i).get("group_name"));
					returnValue.add(putValue);
				}
			}
		} catch (Exception e) {
			logger.error("Get Role Data No Select By Group Admin Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getRoleListByGroup(String fkey, String groupName) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("FKEY"												, fkey);
		map.put("GROUP_NAME"									, groupName);
		try{
			logger.info("Get Role Data By Group Start");
			listValue = getSqlSession().selectList("getRoleListByGroup", map);
			logger.info("Get Role Data By Group End");
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("GID"													, listValue.get(i).get("gid"));
					putValue.put("GROUP_NAME"									, listValue.get(i).get("group_name"));
					returnValue.add(putValue);
				}
			}
		}catch(Exception e){
			logger.error("Get Role Data By Group Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getRoleListByGroupAdmin(String fkey, String groupName) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("FKEY"												, fkey);
		map.put("GROUP_NAME"									, groupName);
		try{
			logger.info("Get Role Data By Group Admin Start");
			listValue = getSqlSession().selectList("getRoleListByGroupAdmin", map);
			logger.info("Get Role Data By Group Admin End");
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("GID"													, listValue.get(i).get("gid"));
					putValue.put("GROUP_NAME"									, listValue.get(i).get("group_name"));
					returnValue.add(putValue);
				}
			}
		}catch(Exception e){
			logger.error("Get Role Data By Group Admin Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertGroupMapping(String groupID, String groupName, String[] menuCheckValue, String[] roleCheckValue, String[] menuCheckName, String[] roleCheckName, String remoteIP, String remoteOS, String remoteBrowser) throws Exception{
		List<HashMap<String, Object>> originMenuList = null;
		List<HashMap<String, Object>> originRoleList = null;
		
		HashMap<String, Object> menuMap = new HashMap<String, Object>();
		menuMap.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		menuMap.put("FKEY"													, groupID);
		menuMap.put("TYPE"													, "p");
		try{
			logger.info("getMenuAuthorityRtoM Start!");
			originMenuList = getSqlSession().selectList("getMenuAuthorityRtoM", menuMap);
			logger.info("getMenuAuthorityRtoM End!");
		}catch(Exception e){
			logger.error("getMenuAuthorityRtoM Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		
		HashMap<String, Object> roleMap = new HashMap<String, Object>();
		roleMap.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		roleMap.put("FKEY"													, groupID);
		roleMap.put("TYPE"													, "p");
		try{
			logger.info("getMenuAuthorityGtoR Start!");
			originRoleList = getSqlSession().selectList("getMenuAuthorityGtoR", roleMap);
			logger.info("getMenuAuthorityGtoR End!");
		}catch(Exception e){
			logger.error("getMenuAuthorityGtoR Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		
		int returnValue = 0;
		TableObject menuDelete = new TableObject();
		menuDelete.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		menuDelete.put("FKEY"										, groupID);
		menuDelete.put("TYPE"										, "p");
		
		TableObject roleDelete = new TableObject();
		roleDelete.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		roleDelete.put("FKEY"										, groupID);
		roleDelete.put("TYPE"										, "p");
		
		try{
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7004"), menuDelete);
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7010"), roleDelete);
			
			
			ArrayList<LinkedHashMap<String, Object>> arrayMenu = new ArrayList<LinkedHashMap<String,Object>>();
			for(int i=0;i<menuCheckValue.length;i++){
				if(!menuCheckValue[i].equals("")){
					TableObject menu = new TableObject();
					menu.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
					menu.put("C_ID"											, Integer.valueOf(menuCheckValue[i]));
					menu.put("FKEY"											, groupID);
					menu.put("TYPE"											, "p");
					arrayMenu.add(menu);
				}
			}
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7004"), arrayMenu);

			if(originMenuList.size() >0){
				if(menuCheckValue.length>0) {
					if(!menuCheckValue[0].equals("")){
						mappingLog("G",groupID,groupName,"M",originMenuList,menuCheckValue,menuCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
				mappingCancelLog("G",groupID,groupName,"M",originMenuList,menuCheckValue,menuCheckName,remoteIP,remoteOS,remoteBrowser);
			}else {
				if(menuCheckValue.length>0) {
					if(!menuCheckValue[0].equals("")){
						mappingLog("G",groupID,groupName,"M",originMenuList,menuCheckValue,menuCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
			}
			
			
			
			ArrayList<LinkedHashMap<String, Object>> arrayRole = new ArrayList<LinkedHashMap<String,Object>>();
			for(int k=0;k<roleCheckValue.length;k++){
				if(!roleCheckValue[k].equals("")){
					TableObject role = new TableObject();
					role.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
					role.put("GID"												, Integer.valueOf(roleCheckValue[k]));
					role.put("FKEY"											, groupID);
					role.put("TYPE"											, "p");
					arrayRole.add(role);
				}
			}
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7010"), arrayRole);
			
			if(originRoleList.size() >0){
				if(roleCheckValue.length>0) {
					if(!roleCheckValue[0].equals("")){
						mappingLog("G",groupID,groupName,"R",originRoleList,roleCheckValue,roleCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
				mappingCancelLog("G",groupID,groupName,"R",originRoleList,roleCheckValue,roleCheckName,remoteIP,remoteOS,remoteBrowser);
			}else {
				if(roleCheckValue.length>0) {
					if(!roleCheckValue[0].equals("")){
						mappingLog("G",groupID,groupName,"R",originRoleList,roleCheckValue,roleCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
			}
			
			
			
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("Group Mapping Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertGroupMappingAdmin(String groupID, String[] menuCheckValue, String[] roleCheckValue) throws Exception{
		int returnValue = 0;
		TableObject menuDelete = new TableObject();
		menuDelete.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		menuDelete.put("FKEY"										, groupID);
		menuDelete.put("TYPE"										, "p");
		
		TableObject roleDelete = new TableObject();
		roleDelete.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		roleDelete.put("FKEY"										, groupID);
		roleDelete.put("TYPE"										, "p");
		
		try{
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7004"), menuDelete);
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7010"), roleDelete);
			
			
			ArrayList<LinkedHashMap<String, Object>> arrayMenu = new ArrayList<LinkedHashMap<String,Object>>();
			for(int i=0;i<menuCheckValue.length;i++){
				if(!menuCheckValue[i].equals("")){
					TableObject menu = new TableObject();
					menu.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
					menu.put("C_ID"											, Integer.valueOf(menuCheckValue[i]));
					menu.put("FKEY"											, groupID);
					menu.put("TYPE"											, "p");
					arrayMenu.add(menu);
				}
			}
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7004"), arrayMenu);
			
			
			ArrayList<LinkedHashMap<String, Object>> arrayRole = new ArrayList<LinkedHashMap<String,Object>>();
			for(int k=0;k<roleCheckValue.length;k++){
				if(!roleCheckValue[k].equals("")){
					TableObject role = new TableObject();
					role.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
					role.put("GID"												, Integer.valueOf(roleCheckValue[k]));
					role.put("FKEY"											, groupID);
					role.put("TYPE"											, "p");
					arrayRole.add(role);
				}
			}
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7010"), arrayRole);
			
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("Group Mapping Admin Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityUserListNoByMenu ( String cID, String empID, String empNm, String gubn ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;

		logger.info("empNm:{}",Utils.getString(empNm, "").replaceAll("[\r\n]",""));
		try {
		  
			List<LinkedHashMap<String, Object>> listCustom = empView.search(empID, empNm, "", loginSessionInfoFactory.getObject().getAcctID(),""); 
		 
			if(listCustom != null){
				for(int j=0;j<listCustom.size();j++){ 
					returnValue.add(listCustom.get(j));
				}
			}
		} catch (Exception e) {
			logger.error("Get User Data No Select By Menu Start Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
		
	}
	public JSONArray getMenuAuthorityUserListNoByMenuAdmin ( String cID, String empID, String empNm, String gubn ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;

		logger.info("empNm:{}",Utils.getString(empNm, "").replaceAll("[\r\n]",""));
		try {
		  
			List<LinkedHashMap<String, Object>> listCustom = empView.search(empID, empNm, "", loginSessionInfoFactory.getObject().getAcctID(),""); 
		 
			if(listCustom != null){
				for(int j=0;j<listCustom.size();j++){ 
					returnValue.add(listCustom.get(j));
				}
			}
		} catch (Exception e) {
			logger.error("Get User Data No Select By Menu Start Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getUsersInMenu(String cID, String empID, String empNm) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null; 
		try{
  
			if(!cID.equals("")){
				List<HashMap<String, Object>> empValue = null;
				HashMap<String, Object> customMap = new HashMap<String, Object>();
				customMap.put("C_ID"										, cID);
				customMap.put("TYPE"									, "u");
				empValue = getSqlSession().selectList("getMenuPermission", customMap);
				String cidReturnValue = null;
				if(!empValue.get(0).get("value").toString().equals("NULL")) {
					cidReturnValue = empValue.get(0).get("value").toString();
				}else {
					cidReturnValue = CodeManager.dummy;
				} 
				List<LinkedHashMap<String, Object>> listCustom = empView.search(cidReturnValue, empNm, "",loginSessionInfoFactory.getObject().getAcctID() , ""); 
				if(listCustom != null){
					for(int j=0;j<listCustom.size();j++){ 
						returnValue.add( listCustom.get(j));
					}
				}
			}
		}catch(Exception e){
			logger.error("Get User Data By Menu Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getUsersInMenuAdmin(String cID, String empID, String empNm) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null; 
		try{ 
			if(!cID.equals("")){
				List<HashMap<String, Object>> empValue = null;
				HashMap<String, Object> customMap = new HashMap<String, Object>();
				customMap.put("C_ID"										, cID);
				customMap.put("TYPE"									, "u");
				empValue = getSqlSession().selectList("getMenuPermission", customMap);
				String cidReturnValue = null;
				if(!empValue.get(0).get("value").toString().equals("NULL")) {
					cidReturnValue = empValue.get(0).get("value").toString();
				}else {
					cidReturnValue = null;
				} 
				List<LinkedHashMap<String, Object>> listCustom = empView.search(cidReturnValue, empNm, "",loginSessionInfoFactory.getObject().getAcctID() , ""); 
				if(listCustom != null){
					for(int j=0;j<listCustom.size();j++){ 
						returnValue.add( listCustom.get(j));
					}
				}
			}
		}catch(Exception e){
			logger.error("Get User Data By Menu Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityGroupListNoSelectByMenu (String cID, String userGrpNm ) throws Exception {
		JSONArray returnValue = new JSONArray(); 
 
		try {
			 
			if(!cID.equals("")){
				List<HashMap<String, Object>> empValue = null;
				HashMap<String, Object> customMap = new HashMap<String, Object>();
				customMap.put("C_ID"										, cID);
				customMap.put("TYPE"									, "p");
				empValue = getSqlSession().selectList("getMenuPermission", customMap);
				String cidReturnValue = null;
				if(!empValue.get(0).get("value").toString().equals("NULL")) {
					cidReturnValue = empValue.get(0).get("value").toString();
				}else {
					cidReturnValue = null;
				} 
				List<LinkedHashMap<String, Object>> listCustom = null; 
				listCustom = groupView.search("", userGrpNm, "", "", cidReturnValue, loginSessionInfoFactory.getObject().getAcctID());
				
				if(listCustom != null){
					for(int j=0;j<listCustom.size();j++){ 
						returnValue.add(listCustom.get(j));
					}
				}
			}
		} catch (Exception e) {
			logger.error("Get Group Data No Select By Menu Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityGroupListNoSelectByMenuAdmin (String cID, String userGrpNm ) throws Exception {
		JSONArray returnValue = new JSONArray();
 
		try {
 
			if(!cID.equals("")){
				List<HashMap<String, Object>> empValue = null;
				HashMap<String, Object> customMap = new HashMap<String, Object>();
				customMap.put("C_ID"										, cID);
				customMap.put("TYPE"									, "p");
				empValue = getSqlSession().selectList("getMenuPermission", customMap);
				String cidReturnValue = null;
				if(!empValue.get(0).get("value").toString().equals("NULL")) {
					cidReturnValue = empValue.get(0).get("value").toString();
				}else {
					cidReturnValue = null;
				}
 
				List<LinkedHashMap<String, Object>> listCustom = null; 
 
				listCustom = groupView.search("", userGrpNm, "", "", cidReturnValue, loginSessionInfoFactory.getObject().getAcctID());
				if(listCustom != null){
					for(int j=0;j<listCustom.size();j++){ 
						returnValue.add(listCustom.get(j));
					}
				}
			}
		} catch (Exception e) {
			logger.error("Get Group Data No Select By Menu Admin Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityGroupListByMenu(String cID, String userGrpNm)  throws Exception {
		JSONArray returnValue = new JSONArray(); 
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"															, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_GRP_NM"														, userGrpNm);
		map.put("C_ID"																, cID);
		try{
			 
			if(!cID.equals("")){
				List<HashMap<String, Object>> empValue = null;
				HashMap<String, Object> customMap = new HashMap<String, Object>();
				customMap.put("C_ID"										, cID);
				customMap.put("TYPE"									, "p");
				empValue = getSqlSession().selectList("getMenuPermission", customMap);
				String cidReturnValue = null;
				if(!empValue.get(0).get("value").toString().equals("NULL")) {
					cidReturnValue = empValue.get(0).get("value").toString();
				}else {
					cidReturnValue = CodeManager.dummy;
				} 
				List<LinkedHashMap<String, Object>> listCustom = null;  
				listCustom = groupView.search("", userGrpNm, "", cidReturnValue, "", loginSessionInfoFactory.getObject().getAcctID());
				if(listCustom != null){
					for(int j=0;j<listCustom.size();j++){ 
						returnValue.add(listCustom.get(j));
					}
				}
			}
		}catch(Exception e){
			logger.error("Get Group Data By Menu Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityGroupListByMenuAdmin(String cID, String userGrpNm)  throws Exception {
		JSONArray returnValue = new JSONArray();
 
		try{
 
			if(!cID.equals("")){
				List<HashMap<String, Object>> empValue = null;
				HashMap<String, Object> customMap = new HashMap<String, Object>();
				customMap.put("C_ID"										, cID);
				customMap.put("TYPE"									, "p");
				empValue = getSqlSession().selectList("getMenuPermission", customMap);
				String cidReturnValue = null;
				if(!empValue.get(0).get("value").toString().equals("NULL")) {
					cidReturnValue = empValue.get(0).get("value").toString();
				}else {
					cidReturnValue = CodeManager.dummy;
				} 
				List<LinkedHashMap<String, Object>> listCustom = null;  
				listCustom = groupView.search("", userGrpNm, "", cidReturnValue, "", loginSessionInfoFactory.getObject().getAcctID());
				if(listCustom != null){
					for(int j=0;j<listCustom.size();j++){ 
						returnValue.add(listCustom.get(j));
					}
				}
			}
		}catch(Exception e){
			logger.error("Get Group Data By Menu Admin Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityRoleListNoSelectByMenu ( String cID, String groupName ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"														, loginSessionInfoFactory.getObject().getAcctID());
		map.put("GROUP_NAME"													, groupName);
		map.put("C_ID"															, cID);
		
		try {
			logger.info("Get Role Data No Select By Menu Start");
			listValue = getSqlSession().selectList("getMenuAuthorityRoleListNoSelectByMenu", map);
			logger.info("Get Role Data No Select By Menu End");
			if(listValue.size() > 0) {
				for(int i=0;i<listValue.size();i++) {
					JSONObject putValue = new JSONObject();
					putValue.put("GID"											, listValue.get(i).get("gid"));
					putValue.put("GROUP_NAME"									, listValue.get(i).get("group_name"));
					returnValue.add(putValue);
				}
			}
		} catch (Exception e) {
			logger.error("Get Role Data No Select By Menu Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityRoleListNoSelectByMenuAdmin ( String cID, String groupName ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"														, loginSessionInfoFactory.getObject().getAcctID());
		map.put("GROUP_NAME"													, groupName);
		map.put("C_ID"															, cID);
		
		try {
			logger.info("Get Role Data No Select By Menu Admin Start");
			listValue = getSqlSession().selectList("getMenuAuthorityRoleListNoSelectByMenuAdmin", map);
			logger.info("Get Role Data No Select By Menu Admin End");
			if(listValue.size() > 0) {
				for(int i=0;i<listValue.size();i++) {
					JSONObject putValue = new JSONObject();
					putValue.put("GID"											, listValue.get(i).get("gid"));
					putValue.put("GROUP_NAME"									, listValue.get(i).get("group_name"));
					returnValue.add(putValue);
				}
			}
		} catch (Exception e) {
			logger.error("Get Role Data No Select By Menu Admin Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityRoleListByMenu(String cID, String groupName) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"														, loginSessionInfoFactory.getObject().getAcctID());
		map.put("GROUP_NAME"													, groupName);
		map.put("C_ID"																, cID);
		try{
			logger.info("Get Role Data by Menu Start!");
			listValue = getSqlSession().selectList("getMenuAuthorityRoleListByMenu", map);
			logger.info("Get Role Data By Menu End!");
			if(listValue.size() >0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("GID"											, listValue.get(i).get("gid"));
					putValue.put("GROUP_NAME"							, listValue.get(i).get("group_name"));
					returnValue.add(putValue);
				}
			}
		}catch(Exception e){
			logger.error("Get Role Data By Menu Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityRoleListByMenuAdmin(String cID, String groupName) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"														, loginSessionInfoFactory.getObject().getAcctID());
		map.put("GROUP_NAME"													, groupName);
		map.put("C_ID"																, cID);
		try{
			logger.info("Get Role Data by Menu Admin Start!");
			listValue = getSqlSession().selectList("getMenuAuthorityRoleListByMenuAdmin", map);
			logger.info("Get Role Data By Menu Admin End!");
			if(listValue.size() >0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("GID"											, listValue.get(i).get("gid"));
					putValue.put("GROUP_NAME"							, listValue.get(i).get("group_name"));
					returnValue.add(putValue);
				}
			}
		}catch(Exception e){
			logger.error("Get Role Data By Menu Admin Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertMenuMapping(String menuID, String menuName, String[] userCheckValue, String[] groupCheckValue, String[] roleCheckValue, String[] userCheckName, String[] groupCheckName, String[] roleCheckName, String remoteIP, String remoteOS, String remoteBrowser) throws Exception{
		List<HashMap<String, Object>> originUserList = null;
		List<HashMap<String, Object>> originGroupList = null;
		List<HashMap<String, Object>> originRoleList = null;
		
		HashMap<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		userMap.put("C_ID"													, menuID);
		userMap.put("TYPE"													, "u");
		try{
			logger.info("getMenuAuthorityMtoU Start!");
			originUserList = getSqlSession().selectList("getMenuAuthorityMtoU", userMap);
			logger.info("getMenuAuthorityMtoU End!");
		}catch(Exception e){
			logger.error("getMenuAuthorityMtoU Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		
		HashMap<String, Object> groupMap = new HashMap<String, Object>();
		groupMap.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		groupMap.put("C_ID"													, menuID);
		groupMap.put("TYPE"													, "p");
		
		try{
			logger.info("getMenuAuthorityMtoG Start!");
			originGroupList = getSqlSession().selectList("getMenuAuthorityMtoG", groupMap);
			logger.info("getMenuAuthorityMtoG End!");
		}catch(Exception e){
			logger.error("getMenuAuthorityMtoG Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		
		HashMap<String, Object> roleMap = new HashMap<String, Object>();
		roleMap.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		roleMap.put("C_ID"													, menuID);
		roleMap.put("TYPE"													, "g");
		
		try{
			logger.info("getMenuAuthorityMtoR Start!");
			originRoleList = getSqlSession().selectList("getMenuAuthorityMtoR", roleMap);
			logger.info("getMenuAuthorityMtoR End!");
		}catch(Exception e){
			logger.error("getMenuAuthorityMtoR Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		
		int returnValue = 0;
		HashMap<String, Object> deleteMenuAuthoirty = new HashMap<String, Object>();
		deleteMenuAuthoirty.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		deleteMenuAuthoirty.put("C_ID"													, Integer.valueOf(menuID));
		
		try{
			getSqlSession().update("menuAuthorityMenuDelete", deleteMenuAuthoirty);
			
			
			ArrayList<LinkedHashMap<String, Object>> arrayUser = new ArrayList<LinkedHashMap<String,Object>>();
			for(int i=0;i<userCheckValue.length;i++){
				if(!userCheckValue[i].equals("")){
					TableObject user = new TableObject();
					user.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
					user.put("C_ID"											, Integer.valueOf(menuID));
					user.put("FKEY"											, userCheckValue[i]);
					user.put("TYPE"											, "u");
					arrayUser.add(user);
				}
			}
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7004"), arrayUser);
			 
			if(originUserList.size() >0){
				if(userCheckValue.length>0) {
					if(!userCheckValue[0].equals("")){
						mappingLog("M",menuID,menuName,"U",originUserList,userCheckValue,userCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
				mappingCancelLog("M",menuID,menuName,"U",originUserList,userCheckValue,userCheckName,remoteIP,remoteOS,remoteBrowser);
			}else {
				if(userCheckValue.length>0) {
					if(!userCheckValue[0].equals("")){
						mappingLog("M",menuID,menuName,"U",originUserList,userCheckValue,userCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
			}
			
			ArrayList<LinkedHashMap<String, Object>> arrayGroup = new ArrayList<LinkedHashMap<String,Object>>();
			for(int j=0;j<groupCheckValue.length;j++){
				if(!groupCheckValue[j].equals("")){
					TableObject group = new TableObject();
					group.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
					group.put("C_ID"											, Integer.valueOf(menuID));
					group.put("FKEY"											, groupCheckValue[j]);
					group.put("TYPE"											, "p");
					arrayGroup.add(group);
				}
			}
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7004"), arrayGroup);
			
			if(originGroupList.size() >0){
				if(groupCheckValue.length>0) {
					if(!groupCheckValue[0].equals("")){
						mappingLog("M",menuID,menuName,"G",originGroupList,groupCheckValue,groupCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
				mappingCancelLog("M",menuID,menuName,"G",originGroupList,groupCheckValue,groupCheckName,remoteIP,remoteOS,remoteBrowser);
			}else {
				if(groupCheckValue.length>0) {
					if(!groupCheckValue[0].equals("")){
						mappingLog("M",menuID,menuName,"G",originGroupList,groupCheckValue,groupCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
			}
			
			
			ArrayList<LinkedHashMap<String, Object>> arrayRole = new ArrayList<LinkedHashMap<String,Object>>();
			for(int k=0;k<roleCheckValue.length;k++){
				if(!roleCheckValue[k].equals("")){
					TableObject role = new TableObject();
					role.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
					role.put("C_ID"											, Integer.valueOf(menuID));
					role.put("FKEY"											, roleCheckValue[k]);
					role.put("TYPE"											, "g");
					arrayRole.add(role);
				}
			}
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7004"), arrayRole);
			
			if(originRoleList.size() >0){
				if(roleCheckValue.length>0) {
					if(!roleCheckValue[0].equals("")){
						mappingLog("M",menuID,menuName,"R",originRoleList,roleCheckValue,roleCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
				mappingCancelLog("M",menuID,menuName,"R",originRoleList,roleCheckValue,roleCheckName,remoteIP,remoteOS,remoteBrowser);
			}else {
				if(roleCheckValue.length>0) {
					if(!roleCheckValue[0].equals("")){
						mappingLog("M",menuID,menuName,"R",originRoleList,roleCheckValue,roleCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
			}
			
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("Group Mapping Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertMenuMappingAdmin(String menuID, String[] userCheckValue, String[] groupCheckValue, String[] roleCheckValue) throws Exception{
		int returnValue = 0;
		HashMap<String, Object> deleteMenuAuthoirty = new HashMap<String, Object>();
		deleteMenuAuthoirty.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		deleteMenuAuthoirty.put("C_ID"													, Integer.valueOf(menuID));
		
		try{
			getSqlSession().update("menuAuthorityMenuDeleteAdmin", deleteMenuAuthoirty);
			
			
			ArrayList<LinkedHashMap<String, Object>> arrayUser = new ArrayList<LinkedHashMap<String,Object>>();
			for(int i=0;i<userCheckValue.length;i++){
				if(!userCheckValue[i].equals("")){
					TableObject user = new TableObject();
					user.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
					user.put("C_ID"											, Integer.valueOf(menuID));
					user.put("FKEY"											, userCheckValue[i]);
					user.put("TYPE"											, "u");
					arrayUser.add(user);
				}
			}
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7004"), arrayUser);
			
			
			ArrayList<LinkedHashMap<String, Object>> arrayGroup = new ArrayList<LinkedHashMap<String,Object>>();
			for(int j=0;j<groupCheckValue.length;j++){
				if(!groupCheckValue[j].equals("")){
					TableObject group = new TableObject();
					group.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
					group.put("C_ID"											, Integer.valueOf(menuID));
					group.put("FKEY"											, groupCheckValue[j]);
					group.put("TYPE"											, "p");
					arrayGroup.add(group);
				}
			}
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7004"), arrayGroup);
			
			
			ArrayList<LinkedHashMap<String, Object>> arrayRole = new ArrayList<LinkedHashMap<String,Object>>();
			for(int k=0;k<roleCheckValue.length;k++){
				if(!roleCheckValue[k].equals("")){
					TableObject role = new TableObject();
					role.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
					role.put("C_ID"											, Integer.valueOf(menuID));
					role.put("FKEY"											, roleCheckValue[k]);
					role.put("TYPE"											, "g");
					arrayRole.add(role);
				}
			}
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7004"), arrayRole);
			
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("Group Mapping Admin Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getBusinessList ( String businessName ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		map.put("BUSINESS_NAME"												, businessName);
		
		try {
			logger.info("Get Business List Data Start");
			listValue = getSqlSession().selectList("getBusinessList", map);
			logger.info("Get Business List Data End");
			if(listValue.size() > 0) {
				for(int i=0;i<listValue.size();i++) {
					JSONObject putValue = new JSONObject();
					putValue.put("BID"										, listValue.get(i).get("bid"));
					putValue.put("BUSINESS_NAME"							, listValue.get(i).get("business_name"));
					returnValue.add(putValue);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR : Get Business List Data : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getBusinessListAdmin ( String businessName ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		map.put("BUSINESS_NAME"												, businessName);
		
		try {
			logger.info("Get Business List Admin Data Start");
			listValue = getSqlSession().selectList("getBusinessListAdmin", map);
			logger.info("Get Business List Admin Data End");
			if(listValue.size() > 0) {
				for(int i=0;i<listValue.size();i++) {
					JSONObject putValue = new JSONObject();
					putValue.put("BID"										, listValue.get(i).get("bid"));
					putValue.put("BUSINESS_NAME"							, listValue.get(i).get("business_name"));
					returnValue.add(putValue);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR : Get Business List Admin Data : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityUserListNoByData ( String bID, String empID, String empName, String gubn ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null; 
		try {
			 
			if(!bID.equals("")){
				List<HashMap<String, Object>> empValue = null;
				HashMap<String, Object> customMap = new HashMap<String, Object>();
				customMap.put("BID"									, bID);
				customMap.put("TYPE"									, "u");
				empValue = getSqlSession().selectList("getChildInDataPermission", customMap);
				String bidReturnValue = null;
				if(!empValue.get(0).get("value").toString().equals("NULL")){
					bidReturnValue = empValue.get(0).get("value").toString();
				}else{
					bidReturnValue = null;
				}
 
				List<LinkedHashMap<String, Object>> listCustom = null;
				if(bidReturnValue != null){
					 listCustom = empView.search("", empName, "", loginSessionInfoFactory.getObject().getAcctID(),bidReturnValue);
				}
				if(listCustom != null){
					for(int j=0;j<listCustom.size();j++){
						JSONObject customValue = new JSONObject(); 
						returnValue.add(listCustom.get(j));
					}
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority User List By Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityUserListNoByDataAdmin ( String bID, String empID, String empName, String gubn ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null; 
		try {
			 
			if(!bID.equals("")){
				List<HashMap<String, Object>> empValue = null;
				HashMap<String, Object> customMap = new HashMap<String, Object>();
				customMap.put("BID"									, bID);
				customMap.put("TYPE"									, "u");
				empValue = getSqlSession().selectList("getChildInDataPermission", customMap);
				String bidReturnValue = null;
				if(!empValue.get(0).get("value").toString().equals("NULL")){
					bidReturnValue = empValue.get(0).get("value").toString();
				}else{
					bidReturnValue = null;
				}
 
				List<LinkedHashMap<String, Object>> listCustom = null;
				if(bidReturnValue != null){
					 listCustom = empView.search("", empName, "", loginSessionInfoFactory.getObject().getAcctID(),bidReturnValue);
				}
				if(listCustom != null){
					for(int j=0;j<listCustom.size();j++){
						JSONObject customValue = new JSONObject(); 
						returnValue.add(listCustom.get(j));
					}
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority User List By Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityUserListByData (String bID, String empID, String empName, String gubn ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null; 
		try {
			 
			if(!bID.equals("")){
				List<HashMap<String, Object>> empValue = null;
				HashMap<String, Object> customMap = new HashMap<String, Object>();
				customMap.put("BID"									, bID);
				customMap.put("TYPE"									, "u");
				empValue = getSqlSession().selectList("getChildInDataPermission", customMap);
				String bidReturnValue = null;
				if(!empValue.get(0).get("value").toString().equals("NULL")){
					bidReturnValue = empValue.get(0).get("value").toString();
				}else{
					bidReturnValue = null;
				}
 
				List<LinkedHashMap<String, Object>> listCustom = null;
				if(bidReturnValue != null){
					 listCustom = empView.search(bidReturnValue, empName, "", loginSessionInfoFactory.getObject().getAcctID(), "");
				}
				if(listCustom != null){
					for(int j=0;j<listCustom.size();j++){
						JSONObject customValue = new JSONObject(); 
						returnValue.add(listCustom.get(j));
					}
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority User List By Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityUserListByDataAdmin (String bID, String empID, String empName, String gubn ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null; 
		try {
			 
			if(!bID.equals("")){
				List<HashMap<String, Object>> empValue = null;
				HashMap<String, Object> customMap = new HashMap<String, Object>();
				customMap.put("BID"									, bID);
				customMap.put("TYPE"									, "u");
				empValue = getSqlSession().selectList("getChildInDataPermission", customMap);
				String bidReturnValue = null;
				if(!empValue.get(0).get("value").toString().equals("NULL")){
					bidReturnValue = empValue.get(0).get("value").toString();
				}else{
					bidReturnValue = null;
				}
 
				List<LinkedHashMap<String, Object>> listCustom = null;
				if(bidReturnValue != null){
					 listCustom = empView.search(bidReturnValue, empName, "", loginSessionInfoFactory.getObject().getAcctID(), "");
				}
				if(listCustom != null){
					for(int j=0;j<listCustom.size();j++){
						JSONObject customValue = new JSONObject(); 
						returnValue.add(listCustom.get(j));
					}
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority User List By Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityGroupListNoByData ( String bID, String grpTyp, String userGrpName ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
 
		try {
		 
			if(!bID.equals("")){ 
				List<HashMap<String, Object>> empValue = null;
				HashMap<String, Object> customMap = new HashMap<String, Object>();
				customMap.put("BID"										, bID);
				customMap.put("TYPE"									, "p"); 
				empValue = getSqlSession().selectList("getChildInDataPermission", customMap);
				String cidReturnValue = null;
				if(!empValue.get(0).get("value").toString().equals("NULL")) {
					cidReturnValue = empValue.get(0).get("value").toString();
				}else {
					cidReturnValue = null;
				}
 
				List<LinkedHashMap<String, Object>> listCustom = groupView.search("", userGrpName, grpTyp, "", cidReturnValue, loginSessionInfoFactory.getObject().getAcctID()); 
				if(listCustom != null){
					for(int j=0;j<listCustom.size();j++){ 
						returnValue.add(listCustom.get(j));
					} 
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority User List No By Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityGroupListByData ( String bID, String grpTyp, String userGrpName ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null; 
		try {
			 
			if(!bID.equals("")){
				List<HashMap<String, Object>> empValue = null;
				HashMap<String, Object> customMap = new HashMap<String, Object>();
				customMap.put("BID"										, bID);
				customMap.put("TYPE"									, "p");
				empValue = getSqlSession().selectList("getChildInDataPermission", customMap);
				String cidReturnValue = null;
				if(!empValue.get(0).get("value").toString().equals("NULL")) {
					cidReturnValue = empValue.get(0).get("value").toString();
				}else {
					cidReturnValue = null;
				}
 
				List<LinkedHashMap<String, Object>> listCustom = null; 
				if(cidReturnValue != null){ 
					listCustom = groupView.search("", userGrpName, grpTyp, cidReturnValue, "", loginSessionInfoFactory.getObject().getAcctID());
				}
				if(listCustom != null){
					for(int j=0;j<listCustom.size();j++){ 
						returnValue.add(listCustom.get(j));
					}
				}
			}
		} catch (Exception e) {
			logger.error("ERROR  > Get Menu Authority User List By Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityGroupListByDataAdmin ( String bID, String grpTyp, String userGrpName ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		map.put("BID"														, bID);
		map.put("GRP_TYP"													, grpTyp);
		map.put("USER_GRP_NM"												, userGrpName);
		
		try {
			 
			if(!bID.equals("")){
				List<HashMap<String, Object>> empValue = null;
				HashMap<String, Object> customMap = new HashMap<String, Object>();
				customMap.put("BID"										, bID);
				customMap.put("TYPE"									, "p");
				empValue = getSqlSession().selectList("getChildInDataPermission", customMap);
				String cidReturnValue = null;
				if(!empValue.get(0).get("value").toString().equals("NULL")) {
					cidReturnValue = empValue.get(0).get("value").toString();
				}else {
					cidReturnValue = null;
				}
 
				List<LinkedHashMap<String, Object>> listCustom = null; 
				if(cidReturnValue != null){ 
					listCustom = groupView.search("", userGrpName, grpTyp, cidReturnValue, "", loginSessionInfoFactory.getObject().getAcctID());
				}
				if(listCustom != null){
					for(int j=0;j<listCustom.size();j++){ 
						returnValue.add(listCustom.get(j));
					}
				}
			}
		} catch (Exception e) {
			logger.error("ERROR  > Get Menu Authority User List By Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityRoleListNoByData ( String bID, String groupName ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		map.put("BID"														, bID);
		map.put("GROUP_NAME"												, groupName);
		
		try {
			logger.info("Get Menu Authority Role List No By Data Start");
			listValue = getSqlSession().selectList("getMenuAuthorityRoleListNoByData", map);
			logger.info("Get Menu Authority Role List No By Data End");
			if(listValue.size() > 0) {
				for(int i=0;i<listValue.size();i++) {
					JSONObject putValue = new JSONObject();
					putValue.put("GID"										, listValue.get(i).get("gid"));
					putValue.put("GROUP_NAME"								, listValue.get(i).get("group_name"));
					returnValue.add(putValue);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority Role List No By Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityRoleListNoByDataAdmin ( String bID, String groupName ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		map.put("BID"														, bID);
		map.put("GROUP_NAME"												, groupName);
		
		try {
			logger.info("Get Menu Authority Role List No By Data Admin Start");
			listValue = getSqlSession().selectList("getMenuAuthorityRoleListNoByDataAdmin", map);
			logger.info("Get Menu Authority Role List No By Data Admin End");
			if(listValue.size() > 0) {
				for(int i=0;i<listValue.size();i++) {
					JSONObject putValue = new JSONObject();
					putValue.put("GID"										, listValue.get(i).get("gid"));
					putValue.put("GROUP_NAME"								, listValue.get(i).get("group_name"));
					returnValue.add(putValue);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority Role List No By Data Admin Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityRoleListByData ( String bID, String groupName ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		map.put("BID"														, bID);
		map.put("GROUP_NAME"												, groupName);
		
		try {
			logger.info("Get Menu Authority Role List By Data Start");
			listValue = getSqlSession().selectList("getMenuAuthorityRoleListByData", map);
			logger.info("Get Menu Authority Role List By Data End");
			if(listValue.size() > 0) {
				for(int i=0; i<listValue.size(); i++) {
					JSONObject putValue = new JSONObject();
					putValue.put("GID"										, listValue.get(i).get("gid"));
					putValue.put("GROUP_NAME"								, listValue.get(i).get("group_name"));
					returnValue.add(putValue);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority Role List By Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthorityRoleListByDataAdmin ( String bID, String groupName ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		map.put("BID"														, bID);
		map.put("GROUP_NAME"												, groupName);
		
		try {
			logger.info("Get Menu Authority Role List By Data Admin Start");
			listValue = getSqlSession().selectList("getMenuAuthorityRoleListByDataAdmin", map);
			logger.info("Get Menu Authority Role List By Data Admin End");
			if(listValue.size() > 0) {
				for(int i=0; i<listValue.size(); i++) {
					JSONObject putValue = new JSONObject();
					putValue.put("GID"										, listValue.get(i).get("gid"));
					putValue.put("GROUP_NAME"								, listValue.get(i).get("group_name"));
					returnValue.add(putValue);
				}
			}
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Authority Role List By Data Admin Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public String getMaxDataID () throws Exception {
		String returnValue = "";
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		
		try {
			logger.info("Get Max Data ID Data Start");
			listValue = getSqlSession().selectList("getMaxDataID",  map);
			logger.info("Get Max Data ID Data End");
			if(listValue.size() > 0) {
				returnValue = listValue.get(0).get("gid").toString();
			}
		} catch (Exception e) {
			logger.info("ERROR > Get Max Data ID Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertDataInfo ( String bID, String businessName , String businessNameOrg, String remoteIP, String remoteOS, String remoteBrowser) throws Exception {
		int returnValue = 0;
		String objID = "";
		TableObject iect7037 = new TableObject();
		iect7037.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID(), true);
		if(bID.equals("")) {
			objID = this.getMaxDataID();
			iect7037.put("BID"												, Integer.valueOf(objID), true);
		} else {
			objID = bID;
			iect7037.put("BID"												, Integer.valueOf(bID), true);	
		}
		iect7037.put("BUSINESS_NAME"										, businessName);
		
		HashMap<String,Object> logMap = new HashMap<String,Object>();
		logMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		logMap.put("OBJ_TYP", "D");
		logMap.put("REMOTE_IP", remoteIP);
		logMap.put("REMOTE_OS", remoteOS);
		logMap.put("REMOTE_BROWSER", remoteBrowser);
		logMap.put("INSERT_EMP", loginSessionInfoFactory.getObject().getUserId());
		logMap.put("INSERT_EMP_NAME", loginSessionInfoFactory.getObject().getUserName());
		
		try {
			if(bID.equals("")) {
				logger.info("Insert Data Info Start");
				dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7037"), iect7037);
				logger.info("Insert Data Info End");
				returnValue = 0;
				
				logMap.put("OBJ_ID", objID);
				logMap.put("OBJ_NAME", businessName);
				logMap.put("LOG_ACTION", "NEW");
				logMap.put("TARGET_TYP", null);
				logMap.put("TARGET_ID", null);
				logMap.put("TARGET_NAME", null);
			} else {
				logger.info("Update Data Info Start");
				dbmsDAO.updateTable(WebConfig.getCustomTableName("IECT7037"), iect7037.getKeyMap(), iect7037);
				logger.info("UPdate Data Info End");
				returnValue = 1;
				
				logMap.put("OBJ_ID", objID);
				logMap.put("OBJ_NAME", businessNameOrg);
				logMap.put("LOG_ACTION", "MOD");
				logMap.put("TARGET_TYP", "D");
				logMap.put("TARGET_ID", objID);
				logMap.put("TARGET_NAME", businessName);
			}
			this.getSqlSession().insert("insertKtotoAuthLog", logMap);
			this.getSqlSession().insert("insertMenuAuthLog", logMap);
		} catch (Exception e) {
			logger.error("Error > Insert/Update Data Info Error : {}", e);
			returnValue = 2;
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertDataInfoAdmin ( String bID, String businessName ) throws Exception {
		int returnValue = 0;
		TableObject iect7037 = new TableObject();
		iect7037.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID(), true);
		if(bID.equals("")) {
			iect7037.put("BID"												, Integer.valueOf(this.getMaxDataID()), true);
		} else {
			iect7037.put("BID"												, Integer.valueOf(bID), true);	
		}
		iect7037.put("BUSINESS_NAME"										, businessName);
		try {
			if(bID.equals("")) {
				logger.info("Insert Data Info Admin Start");
				dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7037"), iect7037);
				logger.info("Insert Data Info Admin End");
				returnValue = 0;
			} else {
				logger.info("Update Data Info Admin Start");
				dbmsDAO.updateTable(WebConfig.getCustomTableName("IECT7037"), iect7037.getKeyMap(), iect7037);
				logger.info("UPdate Data Info Admin End");
				returnValue = 1;
			}
		} catch (Exception e) {
			logger.error("Error > Insert/Update Data Info Admin Error : {}", e);
			returnValue = 2;
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int deleteDataInfo ( String bID, String businessName, String remoteIP, String remoteOS, String remoteBrowser ) throws Exception {
		int returnValue = 0;
		TableObject deleteData = new TableObject();
		deleteData.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		deleteData.put("BID"												, Integer.valueOf(bID));
		
		try {
			logger.info("Delete Data Info Start");
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7037"), deleteData);
			logger.info("Delete Data Info End");
			logger.info("Delete Data Authority Start");
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7038"), deleteData);
			logger.info("Delete Data Authority End");
			
			HashMap<String,Object> logMap = new HashMap<String,Object>();
			logMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
			logMap.put("OBJ_TYP", "D");
			logMap.put("OBJ_ID", bID);
			logMap.put("OBJ_NAME", businessName);
			logMap.put("REMOTE_IP", remoteIP);
			logMap.put("REMOTE_OS", remoteOS);
			logMap.put("REMOTE_BROWSER", remoteBrowser);
			logMap.put("INSERT_EMP", loginSessionInfoFactory.getObject().getUserId());
			logMap.put("INSERT_EMP_NAME", loginSessionInfoFactory.getObject().getUserName());
			logMap.put("LOG_ACTION", "DEL");
			logMap.put("TARGET_TYP", null);
			logMap.put("TARGET_ID", null);
			logMap.put("TARGET_NAME", null);
			this.getSqlSession().insert("insertKtotoAuthLog", logMap);
			this.getSqlSession().insert("insertMenuAuthLog", logMap);
			
			
		} catch (Exception e) {
			logger.error("ERROR > Delete Data Info/Authority Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	} 
	public int insertDataMapping ( String bID, String dataName, String[] userCheckValue, String[] groupCheckValue, String[] roleCheckValue , String[] userCheckName, String[] groupCheckName, String[] roleCheckName, String remoteIP, String remoteOS, String remoteBrowser) throws Exception {

		List<HashMap<String, Object>> originUserList = null;
		List<HashMap<String, Object>> originGroupList = null;
		List<HashMap<String, Object>> originRoleList = null;
		
		HashMap<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		userMap.put("B_ID"													, bID);
		userMap.put("TYPE"													, "u");
		try{
			logger.info("getMenuAuthorityDtoU Start!");
			originUserList = getSqlSession().selectList("getMenuAuthorityDtoU", userMap);
			logger.info("getMenuAuthorityDtoU End!");
		}catch(Exception e){
			logger.error("getMenuAuthorityDtoU Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		HashMap<String, Object> grpMap = new HashMap<String, Object>();
		grpMap.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		grpMap.put("B_ID"													, bID);
		grpMap.put("TYPE"													, "p");
		try{
			logger.info("getMenuAuthorityDtoG Start!");
			originGroupList = getSqlSession().selectList("getMenuAuthorityDtoG", grpMap);
			logger.info("getMenuAuthorityDtoG End!");
		}catch(Exception e){
			logger.error("getMenuAuthorityDtoG Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		HashMap<String, Object> roleMap = new HashMap<String, Object>();
		roleMap.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		roleMap.put("B_ID"													, bID);
		roleMap.put("TYPE"													, "g");
		try{
			logger.info("getMenuAuthorityDtoR Start!");
			originRoleList = getSqlSession().selectList("getMenuAuthorityDtoR", roleMap);
			logger.info("getMenuAuthorityDtoR End!");
		}catch(Exception e){
			logger.error("getMenuAuthorityDtoR Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		int returnValue = 0;
		
		TableObject deleteData = new TableObject();
		deleteData.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		deleteData.put("BID"												, Integer.valueOf(bID));
		
		try {
			logger.info("Delete Data Authority Start");
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7038"), deleteData);
			logger.info("Delete Data Authority End");
			
			//사용자 데이터 권한 INSERT
			
			ArrayList<LinkedHashMap<String, Object>> arrayUser = new ArrayList<LinkedHashMap<String,Object>>();
			for(int i=0; i<userCheckValue.length; i++) {
				if(!userCheckValue[i].equals("")) {
					TableObject IECT7038 = new TableObject();
					IECT7038.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
					IECT7038.put("BID"										, Integer.valueOf(bID));
					IECT7038.put("FKEY"										, userCheckValue[i]);
					IECT7038.put("TYPE"										, 'u');
					arrayUser.add(IECT7038);
				}
			}
			logger.info("Insert Data Authority User Data Start");
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7038"), arrayUser);
			logger.info("Insert Data Authority User Data End");
			
			if(originUserList.size() >0){
				if(userCheckValue.length>0) {
					if(!userCheckValue[0].equals("")){
						mappingLog("D",bID,dataName,"U",originUserList,userCheckValue,userCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
				mappingCancelLog("D",bID,dataName,"U",originUserList,userCheckValue,userCheckName,remoteIP,remoteOS,remoteBrowser);
			}else {
				if(userCheckValue.length>0) {
					if(!userCheckValue[0].equals("")){
						mappingLog("D",bID,dataName,"U",originUserList,userCheckValue,userCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
			}
			
			//그룹 데이터 권한 INSERT
			
			ArrayList<LinkedHashMap<String, Object>> arrayGroup = new ArrayList<LinkedHashMap<String,Object>>();
			for(int j=0; j<groupCheckValue.length; j++) {
				if(!groupCheckValue[j].equals("")) {
					TableObject IECT7038 = new TableObject();
					IECT7038.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
					IECT7038.put("BID"										, Integer.valueOf(bID));
					IECT7038.put("FKEY"										, groupCheckValue[j]);
					IECT7038.put("TYPE"										, 'p');
					arrayGroup.add(IECT7038);
				}
			}
			logger.info("Insert Data Authority Group Data Start");
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7038"), arrayGroup);
			logger.info("Insert Data Authority Group Data End");
			
			if(originGroupList.size() >0){
				if(groupCheckValue.length>0) {
					if(!groupCheckValue[0].equals("")){
						mappingLog("D",bID,dataName,"G",originGroupList,groupCheckValue,groupCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
				mappingCancelLog("D",bID,dataName,"G",originGroupList,groupCheckValue,groupCheckName,remoteIP,remoteOS,remoteBrowser);
			}else {
				if(groupCheckValue.length>0) {
					if(!groupCheckValue[0].equals("")){
						mappingLog("D",bID,dataName,"G",originGroupList,groupCheckValue,groupCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
			}
			
			//Role 데이터 권한 INSERT
			
			ArrayList<LinkedHashMap<String, Object>> arrayRole = new ArrayList<LinkedHashMap<String,Object>>();
			for(int k=0; k<roleCheckValue.length; k++) {
				if(!roleCheckValue[k].equals("")) {
					TableObject IECT7038 = new TableObject();
					IECT7038.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
					IECT7038.put("BID"										, Integer.valueOf(bID));
					IECT7038.put("FKEY"										, roleCheckValue[k]);
					IECT7038.put("TYPE"										, 'g');
					arrayRole.add(IECT7038);
				}
			}
			logger.info("Insert Data Authority Role Data Start");
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7038"), arrayRole);
			logger.info("Insert Data Authority Role Data End");
			
			if(originRoleList.size() >0){
				if(roleCheckValue.length>0) {
					if(!roleCheckValue[0].equals("")){
						mappingLog("D",bID,dataName,"R",originRoleList,roleCheckValue,roleCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
				mappingCancelLog("D",bID,dataName,"R",originRoleList,roleCheckValue,roleCheckName,remoteIP,remoteOS,remoteBrowser);
			}else {
				if(roleCheckValue.length>0) {
					if(!roleCheckValue[0].equals("")){
						mappingLog("D",bID,dataName,"R",originRoleList,roleCheckValue,roleCheckName,remoteIP,remoteOS,remoteBrowser);
					}
				}
			}
			
			returnValue = 0;
		} catch (Exception e) {
			logger.info("ERROR > Insert Data Authority User/Group/Role Error : {}" ,e);
			returnValue = 1;
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertDataMappingAdmin ( String bID, String[] userCheckValue, String[] groupCheckValue, String[] roleCheckValue ) throws Exception {
		int returnValue = 0;
		
		TableObject deleteData = new TableObject();
		deleteData.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		deleteData.put("BID"												, Integer.valueOf(bID));
		
		try {
			logger.info("Delete Data Authority Start");
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7038"), deleteData);
			logger.info("Delete Data Authority End");
			
			//사용자 데이터 권한 INSERT
			
			ArrayList<LinkedHashMap<String, Object>> arrayUser = new ArrayList<LinkedHashMap<String,Object>>();
			for(int i=0; i<userCheckValue.length; i++) {
				if(!userCheckValue[i].equals("")) {
					TableObject IECT7038 = new TableObject();
					IECT7038.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
					IECT7038.put("BID"										, Integer.valueOf(bID));
					IECT7038.put("FKEY"										, userCheckValue[i]);
					IECT7038.put("TYPE"										, 'u');
					arrayUser.add(IECT7038);
				}
			}
			logger.info("Insert Data Authority User Data Start");
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7038"), arrayUser);
			logger.info("Insert Data Authority User Data End");
			
			//그룹 데이터 권한 INSERT
			
			ArrayList<LinkedHashMap<String, Object>> arrayGroup = new ArrayList<LinkedHashMap<String,Object>>();
			for(int j=0; j<groupCheckValue.length; j++) {
				if(!groupCheckValue[j].equals("")) {
					TableObject IECT7038 = new TableObject();
					IECT7038.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
					IECT7038.put("BID"										, Integer.valueOf(bID));
					IECT7038.put("FKEY"										, groupCheckValue[j]);
					IECT7038.put("TYPE"										, 'p');
					arrayGroup.add(IECT7038);
				}
			}
			logger.info("Insert Data Authority Group Data Start");
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7038"), arrayGroup);
			logger.info("Insert Data Authority Group Data End");
			
			//Role 데이터 권한 INSERT
			
			ArrayList<LinkedHashMap<String, Object>> arrayRole = new ArrayList<LinkedHashMap<String,Object>>();
			for(int k=0; k<roleCheckValue.length; k++) {
				if(!roleCheckValue[k].equals("")) {
					TableObject IECT7038 = new TableObject();
					IECT7038.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
					IECT7038.put("BID"										, Integer.valueOf(bID));
					IECT7038.put("FKEY"										, roleCheckValue[k]);
					IECT7038.put("TYPE"										, 'g');
					arrayRole.add(IECT7038);
				}
			}
			logger.info("Insert Data Authority Role Data Start");
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7038"), arrayRole);
			logger.info("Insert Data Authority Role Data End");
			
			returnValue = 0;
		} catch (Exception e) {
			logger.info("ERROR > Insert Data Authority User/Group/Role Admin Error : {}" ,e);
			returnValue = 1;
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public List<LinkedHashMap<String,Object>>  getGrpTypList() {
		// TODO Auto-generated method stub
		List<LinkedHashMap<String,Object>> list = null;
		LinkedHashMap<String,Object> param = new LinkedHashMap<String,Object>(); 
		try {
			list = customSqlHouseDAO.executeCustomQuery("MenuAuthorityDAO.getGrpTypList",param);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
}