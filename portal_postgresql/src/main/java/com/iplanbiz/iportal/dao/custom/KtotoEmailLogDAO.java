package com.iplanbiz.iportal.dao.custom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.secure.GeneratePassword;
import com.iplanbiz.core.secure.SHA256;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.GroupDAO;
import com.iplanbiz.iportal.dao.RoleDAO;
import com.iplanbiz.iportal.dao.admin.UserDAO;
import com.iplanbiz.iportal.dao.auth.EmpView;
import com.iplanbiz.iportal.dao.system.CustomSqlHouseDAO;
import com.iplanbiz.iportal.dao.system.SqlHouseExecuteDAO;
import com.iplanbiz.iportal.dao.system.SystemDAO;
import com.iplanbiz.iportal.dto.KtotoUser;
import com.iplanbiz.iportal.dto.User;
import com.iplanbiz.iportal.service.admin.UserService;
import com.iplanbiz.iportal.service.custom.KtotoUserService;

@Repository
public class KtotoEmailLogDAO extends SqlSessionDaoSupport {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	DBMSService dbmsService;
	@Autowired
	DBMSDAO dbmsDAO;
	@Autowired
	UserDAO userDAO;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	public JSONArray getEmailLogList(String startDat,String endDat) throws Exception {
		JSONArray resultValue = new JSONArray();
		List<HashMap< String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("START_DAT"										, startDat);
			map.put("END_DAT"										, endDat);
			listValue = getSqlSession().selectList("getKtotoEmailLogList", map);
			if(listValue!=null&&listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putData = new JSONObject();
					putData.put("SEND_ID"									, listValue.get(i).get("send_id"));
					putData.put("SEND_NAME"									, listValue.get(i).get("send_name"));
					putData.put("SEND_DEPT_NM"								, listValue.get(i).get("send_dept_nm"));
					putData.put("SEND_TYP"									, listValue.get(i).get("send_typ"));
					putData.put("SEND_TYP_NM"								, listValue.get(i).get("send_typ_nm"));
					putData.put("INSERT_DAT"								, listValue.get(i).get("insert_dat"));
					putData.put("RECV_ID"									, listValue.get(i).get("recv_id"));
					putData.put("RECV_NAME"									, listValue.get(i).get("recv_name"));
					putData.put("RECV_DEPT_NM"								, listValue.get(i).get("recv_dept_nm"));
					putData.put("RECV_EMAIL"								, listValue.get(i).get("recv_email"));
					putData.put("REMOTE_IP"									, listValue.get(i).get("remote_ip"));
					putData.put("REMOTE_OS"									, listValue.get(i).get("remote_os"));
					putData.put("REMOTE_BROWSER"							, listValue.get(i).get("remote_browser"));
					resultValue.add(putData);
				}
			}
		}catch(Exception e){
			logger.error("ERROR : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return resultValue;
	}
	public List<LinkedHashMap<String, Object>> getEmailLogListForExcel(String startDat,String endDat) {
		List<LinkedHashMap< String, Object>> listValue = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("START_DAT"										, startDat);
		map.put("END_DAT"										, endDat);
		listValue = getSqlSession().selectList("getKtotoEmailLogList", map);
		
		return listValue;
	}
 
}
