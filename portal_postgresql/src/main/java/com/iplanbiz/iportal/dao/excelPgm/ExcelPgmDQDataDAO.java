package com.iplanbiz.iportal.dao.excelPgm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.butler.Proxy;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.comm.dataSource.ContextHolder;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dto.ExcelDQDTO;

@Repository
public class ExcelPgmDQDataDAO extends SqlSessionDaoSupport{
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;

	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired DBMSDAO dbmsDAO;
	@Autowired ExcelPgmDQDAO dqDAO;
	
	
	public JSONArray getProgramList(String pCd, String cCd, String searchText) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("acctId", loginSessionInfoFactory.getObject().getAcctID());
		map.put("pCd", pCd);
		map.put("cCd", cCd);
		map.put("searchText",searchText);
		
		if(!loginSessionInfoFactory.getObject().getIsAdmin()) {
			//어드민 권한이 없는 사용자는 자신의 데이터만 조회
			map.put("pgmEmp",loginSessionInfoFactory.getObject().getUserId());
		}
		
		try{
			logger.info("getProgramList Start");
			list = getSqlSession().selectList("getProgramList", map);
			logger.info("getProgramList End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject(); 
					value.put("PGM_ID"									, list.get(i).get("pgm_id"));
					value.put("PGM_NM"									, list.get(i).get("pgm_nm"));
					value.put("DBKEY"									, list.get(i).get("dbkey"));
					value.put("SYSTEM_ID"									, list.get(i).get("system_id"));
					value.put("PGM_EMP"									, list.get(i).get("pgm_emp"));
					value.put("PGM_EMP_NM"									, list.get(i).get("pgm_emp_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("getProgramList Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}

/*
	public Map<String, Object> getExcelPgmDQDataRowId(String pCd, String cCd, String pgmId, String rowid) {
		Map<String, Object> rtn = new HashMap<String, Object>();
		
		Map<String, Object> sqlMap = new HashMap<String, Object>();
		sqlMap.put("acctId", loginSessionInfoFactory.getObject().getAcctID());
		sqlMap.put("division", WebConfig.getDivision());
		sqlMap.put("pCd", pCd);
		sqlMap.put("cCd", cCd);
		sqlMap.put("pgmId", pgmId);
		sqlMap.put("rowid" , rowid);
		
		ExcelDQDTO program = (ExcelDQDTO) getSqlSession().selectList("getExcelPgmDQView", sqlMap);
		String dbkey = dataSource.getDefaultTargetDataSourceKey();
		if(program!=null) dbkey = program.getDBKEY();
		
		ContextHolder.setDataSourceType(dbkey);
		try{
		if(program == null) {
			rtn.put("column", new ArrayList<String>());
			rtn.put("data", new ArrayList<String>());
			return rtn;
		}
		
		String query = program.getDOWN_SQL();
		
		List<String> columnNm = null;
		
		logger.info("getColumn::::"+query);
		columnNm = this.dqDAO.getColumn(query);
		
		String[] columns = new String[columnNm.size()];
		
		for(int i = 0; i < columnNm.size(); i++) {
			columns[i] = "UDC"+(i+1)+ " AS " + columnNm.get(i);
		}	
		
		sqlMap.put("columns", Utils.setGlue("", "", "", true, columns));
	
		rtn.put("column", columnNm);
		rtn.put("data", getSqlSession().selectList("getExcelPgmDQDataRowId", sqlMap));
		}catch(Exception e){
			e.printStackTrace();
			
			rtn.put("column", new ArrayList<String>());
			rtn.put("data", new ArrayList<String>());
			return rtn;
		}finally{
			ContextHolder.setDataSourceType(dataSource.getDefaultTargetDataSourceKey());
		}
		return rtn;
	}
*/

	
}
