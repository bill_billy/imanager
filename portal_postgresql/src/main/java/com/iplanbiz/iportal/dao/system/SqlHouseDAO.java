package com.iplanbiz.iportal.dao.system;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.iportal.config.WebConfig;

@Repository
public class SqlHouseDAO extends SqlSessionDaoSupport {
	
	@Autowired
	private DBMSService dbmsService;
	
	
	@Autowired
	private DBMSDAO dbmsDao;
	
	
	@Autowired SqlSessionFactory sqlSessionFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	//참고..databaseId 가져오기.
	//String databaseId = this.getSqlSession().getConfiguration().getDatabaseId();
	
	
	public HashMap<String,Object> getSqlHouseOne(int idx) throws Exception{
		HashMap<String,Object> sqlmap  = null;
		try { 
			HashMap<String,Integer> param = new HashMap<String, Integer>();
			param.put("IDX", idx);
			logger.info("SqlHouseDAO idx : {}",idx);
			sqlmap = (HashMap<String,Object>)this.getSqlSession().selectOne("getSqlHouseOne", param);
		//	logger.info("SqlHouseDAO SQL : {}",Utils.getString((String)sqlmap.get("SQL"), "").replaceAll("[\r\n]",""));
		//	logger.info("SqlHouseDAO SQL_EXCEL : {}",Utils.getString((String)sqlmap.get("SQL_EXCEL"), "").replaceAll("[\r\n]",""));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error",e);
			throw e;
		}
		return sqlmap;
	}
	
	public HashMap<String,Object> getSqlCustomHouseOne(String queryID) throws Exception{
		HashMap<String,Object> sqlmap  = null;
		try { 
			TableCondition customhouse = new TableCondition();
			customhouse.put("QUERYID", queryID);
			List<LinkedHashMap<String,Object>> list = dbmsDao.selectTable(false,WebConfig.getCustomTableName("CUSTOMHOUSE"), customhouse);
			if(list.size()!=0) {
				sqlmap = list.get(0);
				logger.info("getSqlCustomHouseOne idx : {}",Utils.getString(queryID, "").replaceAll("[\r\n]",""));
				logger.info("getSqlCustomHouseOne SQL : {}",Utils.getString((String)sqlmap.get("QUERY"), "").replaceAll("[\r\n]",""));
				logger.info("getSqlCustomHouseOne SQL_EXCEL : {}",Utils.getString((String)sqlmap.get("QUERY_EXCEL"), "").replaceAll("[\r\n]",""));
			}
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error",e);
			throw e;
		}
		return sqlmap;
	}
	public List<LinkedHashMap<String,Object>> getSqlCustomHouseList(String queryID) throws Exception{
		List<LinkedHashMap<String,Object>> list = null;
		try { 
			TableCondition customhouse = new TableCondition();
			customhouse.put("QUERYID", queryID);
			list = dbmsDao.selectTable(false,WebConfig.getCustomTableName("CUSTOMHOUSE"), customhouse);
 
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error",e);
			throw e;
		}
		return list;
	}
	
	public List<HashMap<String,Object>> getSqlHouseList() throws Exception{
		List<HashMap<String,Object>> sqlmapList  = null;
		try {
			HashMap<String,Object> param = new HashMap<String, Object>();
			sqlmapList = this.getSqlSession().selectList("getSqlHouseList", param);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error",e);
			throw e;
		}
		return sqlmapList;
	}
	
	public List<HashMap<String,Object>> getSqlHouseList(String searchText) throws Exception{
		List<HashMap<String,Object>> sqlmapList  = null;
		try {
			HashMap<String,Object> param = new HashMap<String, Object>();
			param.put("searchText", searchText);
			sqlmapList = this.getSqlSession().selectList("getSqlHouseList", param);  
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error",e);
			throw e;
		}
		return sqlmapList;
	} 

	public int insertSqlHouse(String vendor, String description,
			String sql, String sql_excel) throws Exception {
		Integer result = 1;
		try {
			
			TableObject tbl9000 = new TableObject(); 
			tbl9000.put("VENDOR", vendor,true);
			tbl9000.put("DESCRIPTION", description);
			tbl9000.put("SQL", sql);
			tbl9000.put("SQL_EXCEL", sql_excel);
			tbl9000.put("INSERT_EMP", "admin");
			tbl9000.putCurrentTimeStamp("INSERT_DAT");
			dbmsDao.insertTable(WebConfig.getAdminTableName(WebConfig.getProperty("system.name").trim()+"9000"),"IDX",tbl9000.getKeyMap(), tbl9000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error",e);
			result = 0;
			throw e;
		}
		return result;
	}
	
	public int updateSqlHouse(String idx, String vendor, String description,
			String sql, String sql_excel) throws Exception {
		Integer result = 0;
		try {
			HashMap<String,Object> param = new HashMap<String, Object>();
			param.put("IDX", idx);
			param.put("VENDOR", vendor);
			param.put("DESCRIPTION", description);
			param.put("SQL", sql);
			param.put("SQL_EXCEL", sql_excel);
			param.put("USER_ID", "admin"); 
			logger.info(Utils.getString(param.toString(), "").replaceAll("[\r\n]",""));
			logger.debug("sqlhouse update:{}",Utils.getString(param.toString(), "").replaceAll("[\r\n]",""));
			result = this.getSqlSession().update("updateSqlHouse", param); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error",e);
			throw e;
		}
		return result;
	}
	
	public List<LinkedHashMap<String, Object>> executeCustomQuery(String query, String dbkey) throws Exception{		
		return this.dbmsService.executeQuery(query, dbkey);
	}

}
