package com.iplanbiz.iportal.dao.cms;

import java.io.IOException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dto.Board;
import com.iplanbiz.iportal.dto.File;
import com.iplanbiz.iportal.dto.Notice;
import com.iplanbiz.iportal.service.file.FileService;

@Repository
public class NoticeDAO extends SqlSessionDaoSupport{
	
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired DBMSDAO dbmsDAO;
	@Autowired FileService fileService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	
	public JSONArray getNoticeList (String TITLE,String CONTENT,String NAME,String GUBN,String START_DAT,String END_DAT) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
	//	map.put("DIVISION"												, WebConfig.getDivision());
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("TITLE"													, TITLE);
		map.put("CONTENT"												, CONTENT);
		map.put("NAME"													, NAME);
		map.put("GUBN"													, GUBN);
		map.put("START_DAT"												, START_DAT);
		map.put("END_DAT"												, END_DAT);
		System.out.println(map);
		try{
			logger.info("Get getNoticeList Data Start");
			list = getSqlSession().selectList("getNoticeList", map);
			logger.info("Get getNoticeList Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("RNUM"									, list.get(i).get("rnum"));
					value.put("BOARDNO"									, list.get(i).get("boardno"));
					value.put("TITLE"								, list.get(i).get("title"));
					value.put("CONTENT"								, list.get(i).get("content"));
					value.put("USERID"								, list.get(i).get("userid"));
					value.put("NAME"									, list.get(i).get("name"));
		//			value.put("DIVISION"									, list.get(i).get("division"));
					value.put("HITCOUNT"										, list.get(i).get("hitcount"));
					value.put("INFORM_YN"										, list.get(i).get("inform_yn"));
					value.put("CREATEDATE"										, list.get(i).get("createdate"));
					value.put("FILECOUNT"										, list.get(i).get("filecount"));
					returnValue.add(value);
				}
			}
			System.out.println(returnValue);
		}catch(Exception e) {
			logger.error("Get getNoticeList Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public List<HashMap<String, Object>> getNoticeList (Notice notice){
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
	//	map.put("DIVISION"												, WebConfig.getDivision());
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("TITLE"													, "");
		map.put("CONTENT"												, "");
		map.put("NAME"													, "");
		map.put("GUBN"													, "NULL");
		map.put("START_DAT"												, notice.getStartDay1());
		map.put("END_DAT"												, notice.getEndDay1());
		
		list = getSqlSession().selectList("getNoticeList", map);
		
		
		return list;
	}
	public List<HashMap<String, Object>> getPopup (String start_dat,String end_dat){
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		
		list = getSqlSession().selectList("getNoticePopupList", map);
		
		return list;
	}
	public JSONArray getPopupList (String start_dat,String end_dat) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		
		
		try{
			logger.info("Get getNoticePopupList Data Start");
			list = getSqlSession().selectList("getNoticePopupList", map);
			logger.info("Get getNoticePopupList Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("RNUM"									, list.get(i).get("rnum"));
					value.put("BOARDNO"									, list.get(i).get("boardno"));
					value.put("TITLE"								, list.get(i).get("title"));
					value.put("CONTENT"								, list.get(i).get("content"));
					value.put("USERID"								, list.get(i).get("userid"));
					value.put("NAME"									, list.get(i).get("name"));
		//			value.put("DIVISION"									, list.get(i).get("division"));
					value.put("HITCOUNT"										, list.get(i).get("hitcount"));
					value.put("INFORM_YN"										, list.get(i).get("inform_yn"));
					value.put("CREATEDATE"										, list.get(i).get("createdate"));
		//			value.put("FILECOUNT"										, list.get(i).get("filecount"));
					returnValue.add(value);
				}
			}
			System.out.println(returnValue);
		}catch(Exception e) {
			logger.error("Get getNoticeList Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public List<HashMap<String, Object>> getInformList (Notice notice){
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
//	map.put("DIVISION"												, WebConfig.getDivision());
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("INFORM_NUM"											, notice.getInformNum());
		
		list = getSqlSession().selectList("getInformList", map);
		return list;
	}
	public void modifyHitCount(Notice notice){
		Map<String, Object> queryMap = new HashMap<String, Object>();
 
		queryMap.put("BOARDNO", notice.getBoardNo());
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());

		
		getSqlSession().update("updateHitCount", queryMap);
		
	}
	
	public void createNotice(Notice notice) throws IOException {
		Map<String, Object> queryMap = new HashMap<String, Object>();
 
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		
		System.out.println("maxno!:"+String.valueOf(getMaxNoticeNo(queryMap).get("max_boardno")));
		int boardNo = Integer.parseInt(String.valueOf(getMaxNoticeNo(queryMap).get("max_boardno")));
		
		//파일저장
		if(notice.getFile().length > 0){
			String extType[] = {};
			
			for(CommonsMultipartFile cmfile : notice.getFile())
			{
				if(cmfile.getSize() > 0){
					File file = new File();
					//파일저장
					file = fileService.saveFile(cmfile, extType);
					file.setTableNm("NOTICE");
					file.setBoardNo(boardNo);
					fileService.insertFile(file);
				}
			}
		}

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		queryMap.put("BOARDNO", boardNo);
		queryMap.put("TITLE", notice.getTitle());
		queryMap.put("CONTENT", removeScriptTag(notice.getContent()));
		queryMap.put("USERID", notice.getUserId());
		queryMap.put("NAME", notice.getName());
		queryMap.put("CREATEDATE", sdf.format(date));
		queryMap.put("INFORM_YN", Utils.getString(notice.getInformYn(), "n"));
		queryMap.put("BEGINDAY", notice.getBeginDay());
		queryMap.put("ENDDAY", notice.getEndDay());
		queryMap.put("POPUP", Utils.getString(notice.getPopupYn(), "n"));		
		String[] arr = new String[1];
		arr[0] = "@CONTENT";
		System.out.println("@@@@@@@@@@@@@@@@" + removeScriptTag(notice.getContent()));

		getSqlSession().update("insertNotice", queryMap);
	}
	public void modifyNotice(Notice notice) throws IOException {
		if(notice.getFile().length > 0){
			String extType[] = {};
			
			for(CommonsMultipartFile cmfile : notice.getFile())
			{
				if(cmfile.getSize() > 0){
					File file = new File();
					//파일저장
					file = fileService.saveFile(cmfile, extType);
					file.setTableNm("NOTICE");
					file.setBoardNo(notice.getBoardNo());
					fileService.insertFile(file);
				}
			}
		}
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Map<String, Object> queryMap = new HashMap<String, Object>();
		
	//	queryMap.put("DIVISION", notice.getDivision());
		queryMap.put("BOARDNO",notice.getBoardNo());
		queryMap.put("TITLE", notice.getTitle());
		queryMap.put("CONTENT", removeScriptTag(notice.getContent()));
		queryMap.put("CREATEDATE", sdf.format(date));
		queryMap.put("INFORM_YN", notice.getInformYn());
		queryMap.put("BEGINDAY", notice.getBeginDay());
		queryMap.put("ENDDAY", notice.getEndDay());
		queryMap.put("POPUP", notice.getPopupYn());
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		String[] arr = new String[1];
		arr[0] = "@CONTENT";
		getSqlSession().update("updateNotice", queryMap);
	}
	
	
	
	
	public String removeScriptTag(String str ){ 
		Matcher mat;  
		// script 처리 
		Pattern script = Pattern.compile("(?i)<\\s*(no)?script\\s*[^>]*>.*?<\\s*/\\s*(no)?script\\s*>",Pattern.DOTALL);  
		mat = script.matcher(str);  
		str = mat.replaceAll("");  
		
		return str;
	}
	
	public List<HashMap<String, String>> getNoticeNew(Notice notice){
		Map<String, Object> queryMap = new HashMap<String, Object>();
 
		queryMap.put("BOARDNO", notice.getBoardNo());
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		System.out.println(queryMap);
		
		List<HashMap<String, String>> mainList = getSqlSession().selectList("getNoticeNew", queryMap);
		return mainList;
	}
	public Map<String, String> getNewNotice(){
		LoginSessionInfo loginInfo = loginSessionInfoFactory.getObject();
		
		Map<String, String> noticeMap = new HashMap<String, String>();
		noticeMap.put("boardno", "0");
		noticeMap.put("userid", loginInfo.getUserId());
		noticeMap.put("name", loginInfo.getUserName());

		return noticeMap;
	}
	
	public List<HashMap<String, String>> getNoticeBuser(Notice notice) {
		Map<String, Object> queryMap = new HashMap<String, Object>();
 
		queryMap.put("BOARDNO", notice.getBoardNo());
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		System.out.println(queryMap);
		List<HashMap<String, String>> mainList = getSqlSession().selectList("getNoticeBuser", queryMap);
		return mainList;
	}
	public List<HashMap<String, String>> getReplyTxt(Board board) {
		Map<String, Object> queryMap = new HashMap<String, Object>();
 
		queryMap.put("BOARDNO", board.getBoardNo());
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		queryMap.put("BOARD_TYPE", Integer.parseInt(board.getBoardType()));
		System.out.println(queryMap);
		List<HashMap<String, String>> mainList = getSqlSession().selectList("getReplyTxt", queryMap);
		return mainList;
	}

	public Map<String, String> getMaxNoticeNo(Map<String, Object> queryMap) {
		return getSqlSession().selectOne("getMaxNoticeNo", queryMap);
	}
	public void removeNotice(Notice notice) {
		File file = new File();
		file.setTableNm("NOTICE");
		file.setBoardNo(notice.getBoardNo());
		this.fileService.removeFileByBoardNo(file);
		
		Map<String, Object> queryMap = new HashMap<String, Object>();
 
		queryMap.put("BOARDNO", notice.getBoardNo());
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		getSqlSession().delete("deleteNotice", queryMap);      
		
		
		
		/*
		List<HashMap<String, String>> list = getSqlSession().selectList("getDelBoardNo",queryMap);
		for(int i=0; list.size() > i; i++){
			delBoardNo = delBoardNo + ((list.size() == i+1) ? String.valueOf(list.get(i).get("boardno")) : String.valueOf(list.get(i).get("boardno")));
		}
		
		queryMap.put("DEL_BOARDNO", Integer.parseInt(delBoardNo));
		
		File file = new File();
		file.setTableNm(board.getBoardType());
		file.setDelBoardNo(delBoardNo);
		this.fileService.removeFileByBoardNo(file);
		System.out.println(queryMap);
		getSqlSession().delete("deleteBoard", queryMap);
		getSqlSession().delete("deleteBoard1", queryMap);       */ 
	}




}
