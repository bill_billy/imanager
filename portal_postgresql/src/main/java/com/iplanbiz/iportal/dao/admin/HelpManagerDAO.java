package com.iplanbiz.iportal.dao.admin;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.util.concurrent.ExecutionError;
import com.iplanbiz.comm.SecurityUtil;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dao.auth.GroupView;
import com.nhncorp.lucy.security.xss.listener.SecurityUtils;

@Repository
public class HelpManagerDAO extends SqlSessionDaoSupport {
	
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Autowired DBMSDAO dbmsDAO;
	@Autowired GroupView groupView;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	
	public JSONArray helpManagerMenuList() throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"										, loginSessionInfoFactory.getObject().getUserId());
		try{
			logger.info("Get Help Manager Menu List Start");
			listValue = getSqlSession().selectList("helpManagerMenuList", map);
			
			logger.info("Get Help Manager Menu List End");
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("C_ID"													, listValue.get(i).get("c_id"));
					putValue.put("C_NAME"											, listValue.get(i).get("c_name"));
					putValue.put("P_ID"													, listValue.get(i).get("p_id"));
					putValue.put("P_NAME"											, listValue.get(i).get("p_name"));
					putValue.put("SORT_ORDER"										, listValue.get(i).get("sort_order"));
					putValue.put("USE_YN"												, listValue.get(i).get("use_yn"));
					putValue.put("MENU_TYPE"										, listValue.get(i).get("menu_type"));
					putValue.put("MENU_OPEN_TYPE"								, listValue.get(i).get("menu_open_type"));
					putValue.put("SOURCE_OWNER"								, listValue.get(i).get("source_owner"));
					putValue.put("C_LINK"												, listValue.get(i).get("c_link"));
					putValue.put("PROG_ID"											, listValue.get(i).get("prog_id"));
					returnValue.add(putValue);
				}
			}
		}catch(Exception e){
			logger.error("Get Help Manager Menu Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray helpManagerMenuListByUser() throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("EMP_ID"										, loginSessionInfoFactory.getObject().getUserId());
		String userGrpID = groupView.getStringByUserID(loginSessionInfoFactory.getObject().getUserId());
		map.put("USER_GRP_ID"										, userGrpID);
		map.put("P_ID"												, "5999");
		try{
			logger.info("Get Help Manager Menu List By User Start");
			//listValue = getSqlSession().selectList("helpManagerMenuListByUser", map);
			listValue = getSqlSession().selectList("getMenuList", map);
			
			logger.info("Get Help Manager Menu List By User End");
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("C_ID"												, listValue.get(i).get("c_id"));
					putValue.put("C_NAME"											, listValue.get(i).get("c_name"));
					putValue.put("P_ID"												, listValue.get(i).get("p_id"));
					putValue.put("P_NAME"											, listValue.get(i).get("p_name"));
					putValue.put("SORT_ORDER"										, listValue.get(i).get("sort_order"));
					putValue.put("USE_YN"											, listValue.get(i).get("use_yn"));
					putValue.put("MENU_TYPE"										, listValue.get(i).get("menu_type"));
					putValue.put("MENU_OPEN_TYPE"									, listValue.get(i).get("menu_open_type"));
					putValue.put("SOURCE_OWNER"										, listValue.get(i).get("source_owner"));
					putValue.put("C_LINK"											, listValue.get(i).get("c_link"));
					putValue.put("PROG_ID"											, listValue.get(i).get("prog_id"));
					returnValue.add(putValue);
				}
			}
		}catch(Exception e){
			logger.error("Get Help Manager Menu Data By User Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getHelpManagerMenuGrid(String searchType, String searchValue) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"													, loginSessionInfoFactory.getObject().getUserId());
		map.put("SEARCH_TYPE"											, searchType);
		map.put("SEARCH_VALUE"										, searchValue);
		try{
			logger.info("Get HelpManager Menu Grid Data Start");
			listValue = getSqlSession().selectList("getHelpManagerMenuGrid", map);
			logger.info("Get HelpManager Menu Grid Data End");
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("C_ID"												, listValue.get(i).get("c_id"));
					putValue.put("MENU_TYPE"									, listValue.get(i).get("menu_type"));
					putValue.put("PID"												, listValue.get(i).get("pid"));
					putValue.put("NAME"											, listValue.get(i).get("name"));
					putValue.put("P_NAME"										, listValue.get(i).get("p_name"));
					putValue.put("SORT"											, listValue.get(i).get("sort"));
					putValue.put("SORT_ORDER"									, listValue.get(i).get("sort_order"));
					putValue.put("MENU_OPEN_TYPE"							, listValue.get(i).get("menu_open_type"));
					returnValue.add(putValue);
				}
			}
		}catch(Exception e){
			logger.error("Get HelpManager Menu Grid Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getHelpManagerMenuDetail(String cID) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String ,Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("C_ID"												, cID);
		try{
			logger.info("Get Help Manager Menu Detail Data Start");
			listValue = getSqlSession().selectList("getHelpManagerMenuDetail", map);
			logger.info("Get Help Manager Menu Detail Data End");
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("SUMMARYCONTENT"											, listValue.get(i).get("summarycontent"));
					putValue.put("CONTENT"													, listValue.get(i).get("content"));
					putValue.put("ETC_CONTENT"												, listValue.get(i).get("etc_content"));
					putValue.put("DEPT_CD"													, listValue.get(i).get("dept_cd"));
					putValue.put("DEPT_NM"													, listValue.get(i).get("dept_nm"));
					putValue.put("CHARGE_DEPT"												, listValue.get(i).get("charge_dept"));
					putValue.put("CHARGE_EMP"												, listValue.get(i).get("charge_emp"));
					putValue.put("CONFIRM_DEPT"												, listValue.get(i).get("confirm_dept"));
					putValue.put("CONFIRM_EMP"												, listValue.get(i).get("confirm_emp"));
					putValue.put("UPDATE_PER"												, listValue.get(i).get("update_per"));
					putValue.put("C_NAME"													, listValue.get(i).get("c_name"));
					returnValue.add(putValue);
				}
			}
		}catch(Exception e){
			logger.error("Get Help Manager Menu Detail Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getHelpManagerMenuDetailByUser(String cID) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		HashMap<String ,Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("C_ID"												, cID);
		try{
			logger.info("Get Help Manager Menu Detail By User Data Start");
			listValue = getSqlSession().selectList("getHelpManagerMenuDetailByUser", map);
			logger.info("Get Help Manager Menu Detail By User Data End");
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("SUMMARYCONTENT"											, listValue.get(i).get("summarycontent"));
					putValue.put("CONTENT"													, listValue.get(i).get("content"));
					putValue.put("ETC_CONTENT"												, listValue.get(i).get("etc_content"));
					putValue.put("DEPT_CD"													, listValue.get(i).get("dept_cd"));
					putValue.put("DEPT_NM"													, listValue.get(i).get("dept_nm"));
					putValue.put("CHARGE_DEPT"												, listValue.get(i).get("charge_dept"));
					putValue.put("CHARGE_EMP"												, listValue.get(i).get("charge_emp"));
					putValue.put("CONFIRM_DEPT"												, listValue.get(i).get("confirm_dept"));
					putValue.put("CONFIRM_EMP"												, listValue.get(i).get("confirm_emp"));
					putValue.put("UPDATE_PER"												, listValue.get(i).get("update_per"));
					putValue.put("C_NAME"													, listValue.get(i).get("c_name"));
					returnValue.add(putValue);
				}
			}
		}catch(Exception e){
			logger.error("Get Help Manager Menu Detail By User Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public String getCheckIect7023(String cID) throws Exception{
		String returnValue = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"							, loginSessionInfoFactory.getObject().getAcctID());
		map.put("C_ID"								, cID);
		try{
			
		}catch(Exception e){
		}
		return returnValue;
	}
	public int insertHelpManager(String cID, String summaryContent, String content, String etcContent, String dept, String deptCd, String chargeDept, String chargeEmp, String confirmDept, String confirmEmp, String updatePer) throws Exception{
		int returnValue = 0;
		TableObject iect7023 = new TableObject();
		iect7023.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID(), true);
		iect7023.put("C_ID"										, cID, true);
		iect7023.put("SUMMARYCONTENT"							, SecurityUtil.unEscape(summaryContent) );
		iect7023.put("CONTENT"									, SecurityUtil.unEscape(content));
		iect7023.put("DEPT_NM"									, dept);
		iect7023.put("DEPT"									    , deptCd);
		iect7023.put("CHARGE_DEPT"								, chargeDept);
		iect7023.put("CHARGE_EMP"								, chargeEmp);
		iect7023.put("CONFIRM_DEPT"								, confirmDept);
		iect7023.put("CONFIRM_EMP"								, confirmEmp);
		iect7023.put("UPDATE_PER"								, updatePer);
		
		
		TableObject iect7024 = new TableObject();
		iect7024.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID(), true);
		iect7024.put("C_ID"										, cID, true);
		iect7024.put("ETC_CONTENT"								, SecurityUtil.unEscape(etcContent));
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("C_ID"											, cID);
//		getCheckIect7023
		try{
			logger.info("Insert Help Manager Data Start");
			if(getSqlSession().selectList("getCheckIect7023",map).size() > 0){
				iect7023.put("UPDATE_EMP"							, loginSessionInfoFactory.getObject().getAcctID());
				iect7023.putCurrentDate("UPDATE_DAT");
				dbmsDAO.updateTable("iportal_custom.IECT7023", iect7023.getKeyMap(), iect7023);
			}else{
				iect7023.put("INSERT_EMP"							, loginSessionInfoFactory.getObject().getAcctID());
				iect7023.putCurrentDate("INSERT_DAT");
				dbmsDAO.insertTable("iportal_custom.IECT7023", iect7023);
			}
			if(getSqlSession().selectList("getCheckIect7024",map).size() > 0){
				iect7024.put("UPDATE_EMP"							, loginSessionInfoFactory.getObject().getAcctID());
				iect7024.putCurrentDate("UPDATE_DAT");
				dbmsDAO.updateTable("iportal_custom.IECT7024", iect7024.getKeyMap(), iect7024);
				returnValue = 1;
			}else{
				iect7024.put("INSERT_EMP"							, loginSessionInfoFactory.getObject().getAcctID());
				iect7024.putCurrentDate("INSERT_DAT");
				dbmsDAO.insertTable("iportal_custom.IECT7024", iect7024);
				returnValue = 0;
			}
			logger.info("Insert Help Manager Data End");
		}catch(Exception e){
			returnValue = 2;
			logger.error("Insert Help Manager Menu Data Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertHelpManagerByUser(String cID, String content, String chargeEmp, String confirmEmp) throws Exception {
		int returnValue = 0;
		TableObject iect7023 = new TableObject();
		iect7023.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID(), true);
		iect7023.put("C_ID"											, cID, true);
		iect7023.put("CONTENT"										, content);
		iect7023.put("CHARGE_EMP"									, chargeEmp);
		iect7023.put("CONFIRM_EMP"									, confirmEmp);
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("C_ID"												, cID);
		
		try {
			logger.info("Insert Help Manager Data By User Start");
			if(getSqlSession().selectList("getCheckIect7023",map).size() > 0){
				iect7023.put("UPDATE_EMP"							, loginSessionInfoFactory.getObject().getAcctID());
				iect7023.putCurrentDate("UPDATE_DAT");
				dbmsDAO.updateTable("iportal_custom.IECT7023", iect7023.getKeyMap(), iect7023);
			}
			returnValue = 0;
		} catch (Exception e) {
			logger.error("Insert Help Manager Data By User Error : {}", e);
			e.printStackTrace();
			returnValue = 1;
			throw e;
		}
		return returnValue;
	}
	public int deleteHelpManagerMenu(String cID) throws Exception{
		int returnValue = 0;
		TableObject delete = new TableObject();
		delete.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		delete.put("C_ID"											, cID);
		
		try{
			logger.info("Delete Help Manager Data Start");
			dbmsDAO.deleteTable("iportal_custom.IECT7023", delete);
			dbmsDAO.deleteTable("iportal_custom.IECT7024", delete);
			logger.info("Delete Help Manager Data End");
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("Delete Help Manager Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public List<HashMap<String ,Object>> getSummaryContentData ( String cID ) throws Exception {
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("C_ID"													, cID);
		
		try{
			logger.info("Get Summary Content Data Start");
			list = getSqlSession().selectList("getHelpManagerMenuDetail", map);
			logger.info("Get Summary Content Data End");
			
		}catch(Exception e){
			logger.error("ERROR > Get Summary Content Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return list;
	}
	public JSONArray getHelpPopup ( String cID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String ,Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		map.put("C_ID"														, cID);
		
		try{
			logger.info("Get Help Popup Data Start");
			list = getSqlSession().selectList("getHelpPopup", map);
			logger.info("Get Help Popup Data End");
			
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("SUMMARYCONTENT"						, list.get(i).get("summarycontent"));
					value.put("CONTENT"										, list.get(i).get("content"));
					value.put("ETC_CONTENT"									, list.get(i).get("etc_content"));
					value.put("C_NAME"											, list.get(i).get("c_name"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("ERROR > Get Help Popup Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getNaviByHelpManager ( String cID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		map.put("C_ID"														, cID);
		
		try {
			StringBuffer path = new StringBuffer("");
			logger.info("Get Menu Navi By HelpManager Data Start");
			listValue = getSqlSession().selectList("getNaviByHelpManager",map);
			logger.info("Get Menu Navi By HelpManager Data End");
			if(listValue.size() > 0) {
				for(int i=0; i<listValue.size(); i++) {
					if(i != 0) {
						path.append(">");
					}
					path.append(listValue.get(i).get("c_name").toString());
				}
			}
			JSONObject putValue = new JSONObject();
			putValue.put("PATH", path.toString());
			
			returnValue.add(putValue);
		} catch (Exception e) {
			logger.error("ERROR > Get Menu Navi Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getPopHelpManagerList ( String userGrpNm ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<LinkedHashMap<String, Object>> listValue = null;
		
		
		try { 
			listValue = groupView.search("", userGrpNm, loginSessionInfoFactory.getObject().getAcctID()); 
			if(listValue.size() > 0) {
				for(int i=0; i<listValue.size(); i++) { 
					returnValue.add(listValue.get(i));
				}
			}
		} catch ( Exception e ) {
			logger.error("ERROR : Get Popup Help Manager List Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
}

