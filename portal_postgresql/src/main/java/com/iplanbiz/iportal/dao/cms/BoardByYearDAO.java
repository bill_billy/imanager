package com.iplanbiz.iportal.dao.cms;

import java.io.IOException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dto.Board;
import com.iplanbiz.iportal.dto.File;
import com.iplanbiz.iportal.service.file.FileService;

@Repository
public class BoardByYearDAO extends SqlSessionDaoSupport{
	
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired DBMSDAO dbmsDAO;
	@Autowired FileService fileService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public List<LinkedHashMap<String, Object>> getYears() throws Exception {
		TableCondition coml = new TableCondition();
		coml.put("COML_COD", "EXCEL_YEAR");
		coml.put("USE_YN", "Y");
		return dbmsDAO.selectTable(false,WebConfig.getCustomTableName("IECT0004"), coml, "COM_COD","DESC");
	}
	
	public JSONArray getBoardList (String TABLE_NM,String TITLE,String CONTENT,String NAME,String GUBN,String START_DAT,String END_DAT, String yYYY) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("TABLE_NM1"												, TABLE_NM);
		map.put("TABLE_NM2"												, Integer.parseInt(TABLE_NM));
		map.put("TITLE"													, TITLE);
		map.put("CONTENT"												, CONTENT);
		map.put("NAME"													, NAME);
		map.put("GUBN"													, GUBN);
		map.put("YYYY"												, yYYY);
		map.put("START_DAT"												, START_DAT);
		map.put("END_DAT"												, END_DAT);
		System.out.println(map);
		try{
			logger.info("Get Board List Data Start");
			
			list = getSqlSession().selectList("getBoardListByYear", map);
			logger.info("Get Board List Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("BOARD_TYPE"									, list.get(i).get("board_type"));
					value.put("BOARDNO"									, list.get(i).get("boardno"));
					value.put("P_BOARDNO"									, list.get(i).get("p_boardno"));
					value.put("BOARD_GROUP"							, list.get(i).get("board_group"));
					value.put("ANSWER_LEVEL"								, list.get(i).get("answer_level"));
					value.put("TITLE"								, list.get(i).get("title"));
					value.put("CONTENT"								, list.get(i).get("content"));
					value.put("USERID"								, list.get(i).get("userid"));
					value.put("NAME"									, list.get(i).get("name"));
					value.put("HITCOUNT"										, list.get(i).get("hitcount"));
					value.put("NOTICE_YN"										, list.get(i).get("notice_yn"));
					value.put("CREATEDATE"										, list.get(i).get("createdate"));
					value.put("COUNT"										, list.get(i).get("count"));
					value.put("FILECOUNT"										, list.get(i).get("filecount"));
					value.put("PRIORITY"									, list.get(i).get("priority"));
					value.put("YYYY"									, list.get(i).get("yyyy"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("Get Scheduler Manager List Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public List<HashMap<String, Object>> getBoardList (Board board){
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("TABLE_NM1"												, board.getBoardType());
		map.put("TABLE_NM2"												, Integer.parseInt(board.getBoardType()));
		map.put("TITLE"													, "");
		map.put("CONTENT"												, "");
		map.put("NAME"													, "");
		map.put("GUBN"													, board.getGubn());
		map.put("START_DAT"												, board.getStartDay1());
		map.put("END_DAT"												, board.getEndDay1());
		System.out.println(map);
		
		list = getSqlSession().selectList("getBoardListByYear", map);
		
		return list;
	}
	public List<HashMap<String, String>> getPstepMax(Board board) throws Exception {
		List<HashMap<String, String>> list = null;
		Map<String, Object> queryMap = new HashMap<String, Object>();

		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		queryMap.put("PID", board.getBoardNo());
		queryMap.put("BOARD_TYPE", Integer.parseInt(board.getBoardType()));

		System.out.println(queryMap);
		try{
			logger.info("getPstepMax Start");
			list = getSqlSession().selectList("getPstepMaxByYear", queryMap);
			logger.info("getPstepMax End");
			if(list.size() > 0){
				System.out.println(list);
			}
		}catch(Exception e) {
			logger.error("getPstepMax Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return list;
	}

	public void insertReplyTxt(Board board) throws Exception {
		Map<String, Object> queryMap = new HashMap<String, Object>();

		
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		

		queryMap.put("BOARD_TYPE", Integer.parseInt(board.getBoardType()));
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		queryMap.put("USERID", board.getUserId());
		// queryMap.put("@NAME", board.getName());
		queryMap.put("NAME", loginSessionInfoFactory.getObject().getUserName());
		queryMap.put("CONTENT", URLDecoder.decode(
				board.getReplyTxt().replaceAll("<", "&lt;").replaceAll(">", "&gt;"), "UTF-8"));
		queryMap.put("PID", board.getBoardNo());
		queryMap.put("P_STEP", board.getpStep());
		queryMap.put("HITCOUNT", 0);
		queryMap.put("P_LEVEL", board.getpLevel());
		queryMap.put("CREATEDATE", sdf.format(date));
		try{
			logger.info("insertReplyTxtStart");
			getSqlSession().insert("insertReplyTxtByYear", queryMap);
			logger.info("insertReplyTxt End");
			
		}catch(Exception e) {
			logger.error("insertReplyTxt Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
	}

	public void deleteReplyTxt(Board board) throws Exception {
		Map<String, Object> queryMap = new HashMap<String, Object>();

		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		queryMap.put("PID", board.getBoardNo());
		queryMap.put("P_STEP", board.getpStep());
		if (board.getpLevel() != 0) {
			queryMap.put("P_LEVEL", board.getpLevel());
		} else {
			queryMap.put("P_LEVEL", 0);
		}
		try{
			logger.info("deleteReplyTxt Start");
			getSqlSession().delete("deleteReplyTxtByYear", queryMap);
			logger.info("deleteReplyTxt End");
			
		}catch(Exception e) {
			logger.error("deleteReplyTxt Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
	}

	public void modifyHitCount(Board board) throws Exception {
		Map<String, Object> queryMap = new HashMap<String, Object>();


		queryMap.put("BOARDNO", board.getBoardNo());
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		queryMap.put("BOARD_TYPE", Integer.parseInt(board.getBoardType()));

		try{
			logger.info("Get Board List Data Start");
			getSqlSession().update("updateHitCountBoardByYear", queryMap);
			logger.info("Get Board List Data End");
			
		}catch(Exception e) {
			logger.error("Get Scheduler Manager List Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
	}
	
	public void createBoard(Board board) throws IOException {
		Map<String, Object> queryMap = new HashMap<String, Object>();
 
		
		queryMap.put("BOARD_TYPE", Integer.parseInt(board.getBoardType()));
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		
		int boardNo = Integer.parseInt(String.valueOf(getMaxBoardNo(queryMap).get("max_boardno")));
		
		//파일저장
		if(board.getFile().length > 0){
			
			String extType[] = {};
			
			for(CommonsMultipartFile cmfile : board.getFile())
			{
				if(cmfile.getSize() > 0){
					File file = new File();
					//파일저장
					file = fileService.saveFile(cmfile, extType);
					file.setTableNm(String.valueOf(board.getBoardType()));
					file.setBoardNo(boardNo);
					fileService.insertFile(file);
				}
			}
		}

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		if("".equals(Utils.getString(String.valueOf(board.getpBoardno()), "")))
			queryMap.put("ANSWER_LEVEL", 0);
		else 
			queryMap.put("ANSWER_LEVEL", Integer.parseInt(board.getAnswerLevel())+1);
		
		queryMap.put("P_BOARDNO", Integer.parseInt(Utils.getString(board.getpBoardno(), "0")));
		queryMap.put("BOARD_TYPE", Integer.parseInt(board.getBoardType()));
		queryMap.put("BOARDNO", (boardNo));
		
		if("".equals(Utils.getString(board.getpBoardno(), "")))
			queryMap.put("BOARD_GROUP", (boardNo));
		else{
			queryMap.put("BOARD_GROUP", board.getBoardGroup());
		}
		
		queryMap.put("TITLE", board.getTitle());
		queryMap.put("CONTENT",removeScriptTag( board.getContent()));
		queryMap.put("USERID", board.getUserId());
		queryMap.put("NAME", board.getName());
		queryMap.put("CREATEDATE", sdf.format(date));
		queryMap.put("YYYY", board.getYyyy());
		queryMap.put("NOTICE_YN", Utils.getString(board.getNoticeYn(), "N"));

		getSqlSession().update("insertBoardByYear", queryMap);
	}
	public void modifyBoard(Board board) throws IOException {
		//파일저장
		if(board.getFile().length > 0){
			String extType[] = {};
			
			for(CommonsMultipartFile cmfile : board.getFile())
			{
				if(cmfile.getSize() > 0){
					
					File file = new File();
					//파일저장
					file = fileService.saveFile(cmfile, extType);
					file.setTableNm(String.valueOf(board.getBoardType()));
					file.setBoardNo(board.getBoardNo());
					fileService.insertFile(file);
				}
			}
		}
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		Map<String, Object> queryMap = new HashMap<String, Object>();
		
		queryMap.put("BOARDNO", board.getBoardNo());
		queryMap.put("TITLE", board.getTitle());
		queryMap.put("YYYY", board.getYyyy());
		queryMap.put("CONTENT",removeScriptTag(board.getContent()));
		queryMap.put("CREATEDATE", sdf.format(date));
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		queryMap.put("BOARD_TYPE",  Integer.parseInt(board.getBoardType()));
		queryMap.put("NOTICE_YN", board.getNoticeYn());
		String[] arr = new String [1];
		arr[0] = "@CONTENT";
		
		System.out.println(queryMap);
		getSqlSession().update("updateBoardByYear", queryMap);
	}
	
	
	
	
	public String removeScriptTag(String str ){ 
		Matcher mat;  
		// script 처리 
		Pattern script = Pattern.compile("(?i)<\\s*(no)?script\\s*[^>]*>.*?<\\s*/\\s*(no)?script\\s*>",Pattern.DOTALL);  
		mat = script.matcher(str);  
		str = mat.replaceAll("");  
		
		return str;
	}
	
	public List<HashMap<String, String>> getBoardNew(Board board){
		Map<String, Object> queryMap = new HashMap<String, Object>();
 
		String[] content = {"CONTENT"};
		queryMap.put("ACCT_ID",    loginSessionInfoFactory.getObject().getAcctID());
		queryMap.put("BOARD_TYPE", Integer.parseInt(board.getBoardType()));
		queryMap.put("BOARD_NO",   board.getBoardNo());
		System.out.println(queryMap);
		List<HashMap<String, String>> mainList = getSqlSession().selectList("getBoardByYear", queryMap);
		return mainList;
	}
	public Map<String, String> getNewBoard(String boardType, String pBoardno, String answerLevel){	
		LoginSessionInfo loginInfo = loginSessionInfoFactory.getObject();
		
		Map<String, String> boardMap = new HashMap<String, String>();
		boardMap.put("boardno", "0");
		boardMap.put("userid", loginInfo.getUserId());
		boardMap.put("name", loginInfo.getUserName());
		boardMap.put("board_type", boardType);
		boardMap.put("p_boardno", Utils.getString(String.valueOf(pBoardno), ""));
		
		boardMap.put("answer_level", Utils.getString(String.valueOf(answerLevel), ""));
		boardMap.put("notice_n", "N");
		return boardMap;
	}
	public List<HashMap<String, String>> getBoardBuser(Board board) {
		Map<String, Object> queryMap = new HashMap<String, Object>();
 
		queryMap.put("BOARDNO", board.getBoardNo());
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		List<HashMap<String, String>> mainList = getSqlSession().selectList("getBoardBuserByYear", queryMap);
		return mainList;
	}
	public List<HashMap<String, String>> getReplyTxt(Board board) {
		Map<String, Object> queryMap = new HashMap<String, Object>();
 
		queryMap.put("BOARDNO", board.getBoardNo());
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		queryMap.put("BOARD_TYPE", Integer.parseInt(board.getBoardType()));
		List<HashMap<String, String>> mainList = getSqlSession().selectList("getReplyTxtByYear", queryMap);
		return mainList;
	}

	public Map<String, String> getMaxBoardNo(Map<String, Object> queryMap) {
		return getSqlSession().selectOne("getMaxBoardNoByYear", queryMap);
	}
	public void removeBoard(Board board) {
		StringBuffer delBoardNo = new StringBuffer("");
		Map<String, Object> queryMap = new HashMap<String, Object>();
 
		queryMap.put("BOARDNO", board.getBoardNo());
		queryMap.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		queryMap.put("PID", board.getPid());
		queryMap.put("BOARD_TYPE", Integer.parseInt(board.getBoardType()));
		
		List<HashMap<String, String>> list = getSqlSession().selectList("getDelBoardNoByYear",queryMap);
		for(int i=0; list.size() > i; i++){
			delBoardNo = delBoardNo.append(((list.size() == i+1) ? String.valueOf(list.get(i).get("boardno")) : String.valueOf(list.get(i).get("boardno"))));
		}
		
		queryMap.put("DEL_BOARDNO", Integer.parseInt(delBoardNo.toString()));
		
		File file = new File();
		file.setTableNm(board.getBoardType());
		file.setDelBoardNo(delBoardNo.toString());
		this.fileService.removeFileByBoardNo(file);
		getSqlSession().delete("deleteBoardByYear", queryMap);
		getSqlSession().delete("deleteBoard1ByYear", queryMap);        
	}




}
