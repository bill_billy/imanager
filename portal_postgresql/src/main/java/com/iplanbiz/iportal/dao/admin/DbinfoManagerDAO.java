package com.iplanbiz.iportal.dao.admin;

import java.io.BufferedReader;
import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSDataSource;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.comm.dataSource.ContextHolder;
import com.iplanbiz.iportal.comm.dataSource.RoutingDataSource;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dto.DbInfo;

@Repository
public class DbinfoManagerDAO extends SqlSessionDaoSupport {
	
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Autowired DBMSDAO dbmsDAO;
	@Autowired DBMSService dbmsService;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public JSONArray getDbInfoList () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		try{
			logger.info("Get Db Info List Data Start");
			list = getSqlSession().selectList("getDbInfoList");
			logger.info("Get Db Info List Data End");
			if(list.size()>0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("DBNAME"										, list.get(i).get("dbname"));
					value.put("DBKEY"											, list.get(i).get("dbkey"));
					value.put("VENDERCODE"									, list.get(i).get("vendercode"));
					value.put("VENDERNAME"									, list.get(i).get("vendername"));
					value.put("SCHEMATYPE"									, list.get(i).get("schematype"));
					value.put("SCHEMATYPENAME"							, list.get(i).get("schematypename"));
					value.put("IPADDRESS"										, list.get(i).get("ipaddress"));
					value.put("PORT"												, list.get(i).get("port"));
					value.put("SCHEMANAME"								, list.get(i).get("schemaname"));
					value.put("USERID"											, list.get(i).get("userid"));
					value.put("PWD"												, list.get(i).get("pwd"));
					value.put("MAXACTIVE"									, list.get(i).get("maxactive"));
					value.put("MAXIDLE"										, list.get(i).get("maxidle"));
					value.put("MAXWAIT"										, list.get(i).get("maxwait"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Db Info List Data Error : {} ",e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getDbInfoDetail ( String dbKey ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("DBKEY"													, dbKey);
		try{
			logger.info("Get Db Info Detail Data Start");
			list = getSqlSession().selectList("getDbInfoDetail", map);
			logger.info("Get Db Info Detail Data End");
			if(list.size()>0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("DBKEY"											, list.get(i).get("dbkey"));
					value.put("DBNAME"										, list.get(i).get("dbname"));
					value.put("VENDERCODE"									, list.get(i).get("vendercode"));
					value.put("SCHEMATYPE"									, list.get(i).get("schematype"));
					value.put("SCHEMANAME"								, list.get(i).get("schemaname"));
					value.put("IPADDRESS"										, list.get(i).get("ipaddress"));
					value.put("PORT"												, list.get(i).get("port"));
					value.put("USERID"											, list.get(i).get("userid"));
					value.put("PWD"												, list.get(i).get("pwd"));
					value.put("MAXACTIVE"									, list.get(i).get("maxactive"));
					value.put("MAXIDLE"										, list.get(i).get("maxidle"));
					value.put("MAXWAIT"										, list.get(i).get("maxwait"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Db Info Detail Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertDbInfo(DbInfo dbinfo) throws Exception {
		int returnValue = 0;
		TableObject iect0000 = new TableObject();
		iect0000.put("DBKEY"							, dbinfo.getDbKey(), true);
		iect0000.put("DBNAME"						, dbinfo.getDbName());
		iect0000.put("VENDERCODE"					, dbinfo.getVenderCode());
		iect0000.put("SCHEMATYPE"					, dbinfo.getSchemaType());
		iect0000.put("SCHEMANAME"				, dbinfo.getSchemaName());
		iect0000.put("IPADDRESS"						, dbinfo.getIpAddress());
		iect0000.put("PORT"								, Integer.valueOf(dbinfo.getPort()));
		iect0000.put("USERID"							, dbinfo.getUserID());
		iect0000.put("PWD"								, dbinfo.getPwd());
		iect0000.put("MAXACTIVE"					, Integer.valueOf(dbinfo.getMaxActive()));
		iect0000.put("MAXIDLE"						, Integer.valueOf(dbinfo.getMaxIDLE()));
		iect0000.put("MAXWAIT"						, Integer.valueOf(dbinfo.getMaxWait()));
		try{
			if(dbinfo.getInsertType().equals("new")){
				iect0000.putCurrentTimeStamp("INSERT_DAT");
				iect0000.put("INSERT_EMP"						, loginSessionInfoFactory.getObject().getUserId());
				logger.info("DB Info Insert Start");
				dbmsDAO.insertTable("iportal_custom.IECT0000", iect0000);
				logger.info("DB Info Insert End");
				returnValue = 0;
			}else{
				iect0000.putCurrentTimeStamp("UPDATE_DAT");
				iect0000.put("UPDATE_EMP"						, loginSessionInfoFactory.getObject().getUserId());
				logger.info("DB Info Update Start");
				dbmsDAO.updateTable("iportal_custom.IECT0000", iect0000.getKeyMap(),iect0000);
				logger.info("DB Info Update End");
				returnValue = 1;
			}
		}catch(Exception e){
			logger.error("DB Info Insert & Update Error : {} ", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int removeDbInfo(DbInfo dbinfo) throws Exception {
		int returnValue = 0;
		TableObject iect0000 = new TableObject();
		iect0000.put("DBKEY"										, dbinfo.getDbKey());
		try{
			logger.info("DB Info Delete Start");
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT0000"), iect0000);
			logger.info("DB Info Delete End");
			returnValue = 0;
		}catch(Exception e){
			logger.error("DB Info Delete Error : {} " ,e);
			returnValue = 1;
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public String checkDbKey ( String dbKey ) throws Exception {
		String returnValue = "";
		  
		TableCondition iect0000 = new TableCondition();
		iect0000.put("DBKEY", dbKey);
		int count = dbmsDAO.countTable(WebConfig.getCustomTableName("IECT0000"), iect0000);
		try{
			logger.info("Db Key Check Start"); 
			if(count == 0){
				returnValue = "true"; 
			}
			else{
				returnValue = "false";
			}
			logger.info("Db Key Check End");
		}catch(Exception e){
			logger.error("ERROR > Db Key Check Error : {}" ,e); 
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int checkDbInfo ( String dbKey ) throws Exception {
		int returnValue = 0; 
		List<LinkedHashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("DBKEY"													, dbKey);
		HashMap<String, Object> dbInfo = getSqlSession().selectOne("getDbInfoData", map);
		String query = "";
		logger.info("DB INFO : {} " , Utils.getString((String)dbInfo.get("vendercode"), "").replaceAll("[\r\n]",""));
		if((dbInfo.get("vendercode").toString()).equals("oracle")){
			query = "SELECT 1 FROM DUAL";
		}else{
			query = "SELECT 1";
		}
		try{
			logger.info("Check DB Info Connection Start");
			list = dbmsService.getMetaForSelect(query, dbInfo.get("dbkey").toString());
			logger.info("Check DB Info Connection End");
			returnValue = 0;
		}catch(Exception e){
			logger.error("ERROR > Check DB Info Connection Error : {} ", e);
			returnValue = 1;
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public List<HashMap<String, String>> excuteQuery(String query, String dbSourceType) throws Exception{
			
			this.dbmsService.loadContentsDataBase();
			ContextHolder.setDataSourceType(dbSourceType);
			DataSource targetDataSource = (DataSource) dbmsService.getDataSource(dbSourceType);
//			DBMSDataSource targetDataSOurce = (DBMSDataSource)
			logger.debug("@@@@@@DB INFO" + Utils.getString(ContextHolder.getDataSourceType(), "").replaceAll("[\r\n]",""));
			List<HashMap<String,String>> alRows = new ArrayList<HashMap<String,String>>();
			Connection con = null;
			SqlSession session = null;
			Statement st = null;
			ResultSet rs = null;
			
			logger.info("Custom Query: {}" , Utils.getString(query, "").replaceAll("[\r\n]",""));
			
			try{
				con = null;
				session = sqlSessionFactory.openSession();
				st = session.getConnection().createStatement();
				rs = st.executeQuery(query);	
				ResultSetMetaData rsMeta = rs.getMetaData();
			    int colCnt = rsMeta.getColumnCount();
			    
				while(rs.next()){
					
					HashMap<String, String> rowMap = new HashMap<String, String>();    
					for (int i = 1; i <= colCnt; i++) {
						Object obj = rs.getObject(i);
						
//						if(i == 1) rowMap.put("odd", Utils.getString(String.valueOf((rs.getRow() % 2)), ""));
						
						if(obj != null) {
							rowMap.put(rsMeta.getColumnName(i), obj.toString());
						}else {
							rowMap.put(rsMeta.getColumnName(i), "");
						}
			        }
 
					alRows.add(rowMap);
//					logger.debug("Custom rowMap: {}" , rowMap.toString());
				}	
				logger.info("Custom rowMap Count: {}", alRows.size());
			}catch(SQLException e){
				logger.error("SQLException: {}", e);
				throw new RuntimeException(e.toString());
			}finally{
				ContextHolder.setDataSourceType("PORTAL");
				if(session != null) 	session.close();
				if(st != null) 			try{ st.close(); }catch(SQLException e){}
				if(rs != null) 			try{ rs.close(); }catch(SQLException e){}
			}
			return alRows;
		}
}
