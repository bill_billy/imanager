package com.iplanbiz.iportal.dao.system;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSession;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.QueryUtil;
import com.iplanbiz.core.session.LoginSessionInfo;

@Repository
public class SqlHouseExecuteDAO extends SqlHouseDAO {

	enum SqlColumn { SQL, SQL_EXCEL };
	private Logger logger = LoggerFactory.getLogger(getClass()); 
	
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	/**
	 * Sql House 에서 insert, update, delete, merge등 crud성 문장을 실행할때 사용.
	 * @param idx
	 * @param param
	 * @param longColumnName 파라미터에 CLOB 데이터가 있는경우 사용.   생략 가능 ex)  new String[]{"Column1"} or null
	 * @return
	 * @throws Exception
	 */
	public int crudQuery(int idx, HashMap<String,Object> param, String[] longColumnName) throws Exception{
		logger.debug("sqlHouseExecuteDAO.crudQuery,idx="+idx+",param={}",Utils.getString(param.toString(), "").replaceAll("[\r\n]",""));
		HashMap<String,Object> sqlHouse = this.getSqlHouseOne(idx); 
		String[] query = sqlHouse.get("SQL").toString().split(";");
		return this.crudQuery(query, param, longColumnName); 
	}
	
	public int crudQuery(String sql, HashMap<String,Object> param, String[] longColumnName) throws Exception{
		return this.crudQuery(sql.split(";"), param, longColumnName);		
	}
	
	public int crudQuery(String[] sql, HashMap<String,Object> param, String[] longColumnName) throws Exception{
		logger.debug("sqlHouseExecuteDAO.crudQuery,sql="+Utils.getString(sql.toString(), "").replaceAll("[\r\n]","")+",param={}",Utils.getString(param.toString(), "").replaceAll("[\r\n]",""));
		SqlSession session = null;
		PreparedStatement ps = null;
		int	 rs = 0; 

		String[] query = sql;
		String str = "";
		
		try{
			session = sqlSessionFactory.openSession();
				
			for(int i = 0; i<query.length;i++){
				str = query[i];
				if(str.trim().equals("")==false){
					if(longColumnName != null){
						for(int j=0; j<longColumnName.length; j++){
							str = str.replace(longColumnName[j], "?");
							logger.info("Custom Query : {}" , Utils.getString(str, "").replaceAll("[\r\n]",""));	
						}
					}
					ps = session.getConnection().prepareStatement(QueryUtil.replaceQuery(str,param));
					if(longColumnName != null){
						for(int j=0; j<longColumnName.length; j++){
							StringReader sr = new StringReader((String) param.get(longColumnName[j]));
							ps.setCharacterStream(j+1, sr, ((String) param.get(longColumnName[j])).length());
						}
					}
					rs = ps.executeUpdate();
					logger.info("Custom rs: {}" ,rs);
				}
			}
		}catch(SQLException e){
			logger.error("SQLException: {}", e);
			throw e;
		}catch(RuntimeException e){
			logger.error("RuntimeException: {}", e);
			throw e;
		}
		finally{
			if(session != null) 	session.close();
			if(ps != null) 			try{ ps.close(); }catch(Exception e){
				throw new RuntimeException(e.getMessage());
			}
		}
		return rs;
	}
	/**
	 * Sql House 에서 insert, update, delete, merge등 crud성 문장을 실행할때 사용.
	 * @param idx
	 * @param param 
	 * @return
	 * @throws Exception
	 */
	public int crudQuery(int idx, HashMap<String,Object> param) throws Exception{
		return this.crudQuery(idx,param,null);
	}
	
	/**
	 * Sql House에서 select 문장 실행시.. 리턴 값을 List< Linked HashMap> 으로 반환, Clob 자동 변환됨.
	 * @param idx
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<LinkedHashMap<String,Object>> selectHashMap(int idx, HashMap<String,Object> param) throws Exception{
		return this.selectHashMap(idx, param, SqlColumn.SQL);
	}
	
	/**
	 * Sql House에서 select 문장 실행시.. 리턴 값을 Json Array 으로 반환, Clob 자동 변환됨.
	 * @param idx
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public JSONArray selectJSONArray(int idx, HashMap<String,Object> param) throws Exception{
		return this.selectJSONArray(idx, param, SqlColumn.SQL);
	}
	
	
	/**
	 * Sql House에서 Excel Query 실행시.. 리턴 값을 List< Linked HashMap> 으로 반환, Clob 자동 변환됨.
	 * @param idx
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<LinkedHashMap<String,Object>> excelHashMap(int idx, HashMap<String,Object> param) throws Exception{
		return this.selectHashMap(idx, param, SqlColumn.SQL_EXCEL);
	}
	
	/**
	 * Sql House에서 Excel Query 실행시.. 리턴 값을 Json Array 으로 반환, Clob 자동 변환됨.
	 * @param idx
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public JSONArray excelJSONArray(int idx, HashMap<String,Object> param) throws Exception{
		return this.selectJSONArray(idx, param, SqlColumn.SQL_EXCEL);
	}
	public List<LinkedHashMap<String, Object>> selectQuery(String query) throws Exception{
		return this.selectHashMap(query);
	}
	
	
	
	/*
	 * 아래부터 private 메소드. 디버그 중이 아니라면 볼 필요 없음.
	 */	
	
	private List<LinkedHashMap<String,Object>> selectHashMap(int idx, HashMap<String,Object> param, SqlColumn column) throws Exception{
		HashMap<String,Object> sqlHouse = this.getSqlHouseOne(idx);
		String query = sqlHouse.get(column.toString()).toString().replace(";","");
		return this.selectHashMap(query, param);
	}
	private List<LinkedHashMap<String,Object>> selectHashMap(String query) throws Exception{
		HashMap<String,Object> param = new HashMap<String, Object>();
		return this.selectHashMap(query, param);
	}
	private List<LinkedHashMap<String,Object>> selectHashMap(String query, HashMap<String,Object> param) throws Exception{
		
		List<LinkedHashMap<String,Object>> returnRows = new ArrayList<LinkedHashMap<String,Object>>(); 
		SqlSession session = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			  
			query = QueryUtil.replaceQuery(query, param);
			logger.debug("selectQuery : {}", Utils.getString(query, "").replaceAll("[\r\n]",""));
			
			
			session = sqlSessionFactory.openSession();
			st = session.getConnection().createStatement();
			rs = st.executeQuery(query);	
			ResultSetMetaData rsMeta = rs.getMetaData();
			   int colCnt = rsMeta.getColumnCount();
			   
			while(rs.next()){				
				LinkedHashMap<String, Object> rowMap = new LinkedHashMap<String, Object>();    
				for (int i = 1; i <= colCnt; i++) {
					Object obj = rs.getObject(i);
					String value = "";
					if(obj!=null){
						if(obj instanceof Clob){
							value = this.clobToString((Clob)obj);
						}else{
							value = obj.toString();
						}
					}

					rowMap.put(rsMeta.getColumnName(i), value);
					
		        }
				returnRows.add(rowMap);
				logger.debug("Custom rowMap: {}" , Utils.getString(rowMap.toString(), "").replaceAll("[\r\n]",""));
			}
			logger.debug("Custom rowMap Count: {}", returnRows.size());

		}catch(SQLException e){
			logger.error("SQLException: {}", e);
			throw e;
		}catch(RuntimeException e){
			logger.error("RuntimeException: {}", e);
			throw e;
		}finally{
			if(session != null) 	session.close();
			if(st != null) 			try{ st.close(); }catch(SQLException e){throw e;}
			if(rs != null) 			try{ rs.close(); }catch(SQLException e){throw e;}
		}
		return returnRows;
	}

	
	private JSONArray selectJSONArray(int idx, HashMap<String,Object> param, SqlColumn column) throws Exception{
		
		JSONArray returnRows = new JSONArray();
		HashMap<String,Object> sqlHouse = null;
		SqlSession session = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			sqlHouse = this.getSqlHouseOne(idx);
			String query = sqlHouse.get(column.toString()).toString().replace(";","");
			query = QueryUtil.replaceQuery(query, param);
			logger.debug("selectQuery : {}", Utils.getString(query, "").replaceAll("[\r\n]",""));
			
			
			session = sqlSessionFactory.openSession();
			st = session.getConnection().createStatement();
			rs = st.executeQuery(query);
			ResultSetMetaData rsMeta = rs.getMetaData();
			   int colCnt = rsMeta.getColumnCount();
			   
			while(rs.next()){
				JSONObject rowMap = new JSONObject();    
				for (int i = 1; i <= colCnt; i++) {
					Object obj = rs.getObject(i);            					
					String value = "";
					if(obj!=null){
						if(obj instanceof Clob){
							value = this.clobToString((Clob)obj);
						}else{
							value = obj.toString();
						}
					} 
					rowMap.put(rsMeta.getColumnName(i), value);
		        }
				returnRows.add(rowMap);
				logger.debug("Custom rowMap: {}" , Utils.getString(rowMap.toString(), "").replaceAll("[\r\n]",""));
			}
			logger.debug("Custom rowMap Count: {}", returnRows.size());

		}catch(SQLException e){
			logger.error("SQLException: {}", e);
			throw e;
		}catch(RuntimeException e){
			logger.error("RuntimeException: {}", e);
			throw e;
		}finally{
			if(session != null) 	session.close();
			if(st != null) 			try{ st.close(); }catch(SQLException e){throw e;}
			if(rs != null) 			try{ rs.close(); }catch(SQLException e){throw e;}
		}
		return returnRows;
	}  
	
	/*
	  * Clob 를 String 으로 변경
	  */
	 private String clobToString(Clob clob) throws SQLException,
	   IOException {

		  if (clob == null) {
		   return "";
		  }
	
		  StringBuffer strOut = new StringBuffer();
	
		  String str = "";
	
		  BufferedReader br = new BufferedReader(clob.getCharacterStream());
	
		  while ((str = br.readLine()) != null) {
		   strOut.append(str);
		  }
		  return strOut.toString();
	 } 
}
