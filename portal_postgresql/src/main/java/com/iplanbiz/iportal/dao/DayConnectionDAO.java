package com.iplanbiz.iportal.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.session.LoginSessionInfo;

@Repository
public class DayConnectionDAO extends SqlSessionDaoSupport {
	
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	
	public JSONArray getDayConnectionUserList(String startDat, String endDat) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("START_DAT"										, startDat);
		map.put("END_DAT"											, endDat);
		try{
			logger.info("Get Day Connection Detail Data Start");
			list = getSqlSession().selectList("getDayConnectionUserList", map);
			logger.info("GEt Day Connection Detail Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("EMP_ID"											, list.get(i).get("emp_id"));
					value.put("EMP_NM"										, list.get(i).get("emp_nm"));
					value.put("USER_GRP_NM"								, list.get(i).get("user_grp_nm"));
					value.put("COUNT"											, list.get(i).get("count"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Day Connection Detail Data Error : {} ", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	
	public JSONArray getDayConnectionReportList(String startDat, String endDat, String userID) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("START_DAT"										, startDat);
		map.put("END_DAT"											, endDat);
		map.put("USER_ID"											, userID);
		try{
			logger.info("Get Day Connection Data Start");
			list = getSqlSession().selectList("getDayConnectionReportList", map);
			logger.info("Get Day Connection Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("INSERT_TIME"								, list.get(i).get("insert_time"));
					value.put("TARGET_VALUE"							, list.get(i).get("target_value"));
					value.put("TARGET_PATH"								, list.get(i).get("target_path"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Day Connection Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
}
