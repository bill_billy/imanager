package com.iplanbiz.iportal.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSService;

@Repository
public class DashboardCodeDAO extends SqlSessionDaoSupport {
	
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public JSONArray getDashboardCodeList(String comlCod) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String , Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("COML_COD"														, comlCod);
		try{
			logger.info("Get Dash board Code : "+Utils.getString(comlCod, "").replaceAll("[\r\n]","")+" Start");
			list = getSqlSession().selectList("getDashboardCodeList", map);
			logger.info("Get Dash board Code : "+Utils.getString(comlCod, "").replaceAll("[\r\n]","")+" End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("COM_COD"										, list.get(i).get("com_cod"));
					value.put("COM_NM"										, list.get(i).get("com_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Dash board Code : " +Utils.getString(comlCod, "").replaceAll("[\r\n]","")+" Error");
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
}
