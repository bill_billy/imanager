package com.iplanbiz.iportal.dao.portlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.commons.io.FilenameUtils;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.core.io.file.FileService;
import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dto.contentsManager;

@Repository
public class ContentsManagerDAO extends SqlSessionDaoSupport {
	
 
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Autowired DBMSDAO dbmsDAO;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	
	public JSONArray getPortletContentsList ( String portletGrp ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("PORTLET_GRP"								, Integer.valueOf(portletGrp));
		
		try{
			logger.info("Get Portlet Contents List Data Start");        
			list = getSqlSession().selectList("getPortletContentsList", map);
			logger.info("Get Portlet Contents List Data End");
			if(list.size() > 0){
				for( int i = 0; i < list.size(); i ++){
					JSONObject value = new JSONObject();
					value.put("ACCT_ID"										, list.get(i).get("acct_id"));
					value.put("POID"											, list.get(i).get("poid"));
					value.put("NAME"										, list.get(i).get("name"));
					value.put("ACTIVE"										, list.get(i).get("active"));
					value.put("PORTLET_GRP"								, list.get(i).get("portlet_grp"));
					value.put("HEIGHT"										, list.get(i).get("height"));
					value.put("TYPE"											, list.get(i).get("type"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Portlet Contents List Data Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getPortletGrpList () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		try{
			logger.info("Get Portlet Grp List Data Start");
			list = getSqlSession().selectList("getPortletGrpList", map);
			logger.info("Get Portlet Grp List Data End");
			
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("COM_COD"									, list.get(i).get("com_cod"));
					value.put("COM_NM"									, list.get(i).get("com_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Error : Get Portlet Grp List Data Error {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getPortletContentsListKeyList ( String poID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String , Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("POID"													, Integer.valueOf(poID));
		
		try{
			logger.info("Get Portlet Contents List Key List Data Start");
			list = getSqlSession().selectList("getPortletContentsListKeyList", map);
			logger.info("Get Portlet Contetns List Key List Data End");
			if(list.size() > 0){
				for(int i=0; i<list.size(); i++){
					JSONObject value = new JSONObject();
					value.put("POID"										, list.get(i).get("poid"));
					value.put("KEY_NAME"								, list.get(i).get("key_name"));
					value.put("KEY_VALUE"								, list.get(i).get("key_value"));
					value.put("EXPLAIN"									, list.get(i).get("explain"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("ERROR > Get Portlet Contents List Key List Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getPortletContentsDetail ( String poID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		List<HashMap<String, Object>> keyList = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("POID"													, Integer.valueOf(poID));
		
		try{
			logger.info("Get Portlet Contents Detail Data Start");
			list = getSqlSession().selectList("getPortletContentsDetail", map);
			keyList = getSqlSession().selectList("getPortletContentsListKeyList",map);
			logger.info("Get Portlet Contents Detail Data End");
			
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("POID"										, list.get(i).get("poid"));
					value.put("NAME"									, list.get(i).get("name"));
					value.put("ACTIVE"									, list.get(i).get("active"));
					value.put("ALWAYS_ACTIVE"						, list.get(i).get("always_active"));
					value.put("MENU_OPEN_TYPE"					, list.get(i).get("menu_open_type"));
					if(keyList.size() > 0){
						for(int j=0;j<keyList.size();j++){
							value.put(((String) keyList.get(j).get("key_name")).toUpperCase(), keyList.get(j).get("key_value"));
						}
					}
					returnValue.add(value);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Get Portlet Contents Detail Data Error : {}", e);
			throw e;
		}
		return returnValue;
	}
	public JSONArray getPortletContentsInfoData ( String poID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("POID"													, Integer.valueOf(poID));
		
		try{
			logger.info("Get Portlet Contents Info Data Start");
			list = getSqlSession().selectList("getPortletContentsInfoData", map);
			logger.info("Get Portlet Contents Info Data End");
			
			if(list.size() > 0){
				for( int i=0; i<list.size(); i++){
					JSONObject value = new JSONObject();
					value.put("POID"										, list.get(i).get("poid"));
					value.put("NAME"									, list.get(i).get("name"));
					value.put("URL"										, list.get(i).get("url"));
					value.put("HEIGHT"									, list.get(i).get("height"));
					value.put("TYPE"										, list.get(i).get("type"));
					value.put("MORE"									, list.get(i).get("more"));
					value.put("MORE_URL"								, list.get(i).get("more_url"));
					value.put("VIEW_HEADER"							, list.get(i).get("view_header"));
					value.put("VIEW_ICON"								, list.get(i).get("view_icon"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("ERROR > Get Portlet Contents Info Data Error : {} ", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public String getMaxPOID () throws Exception {
		String maxPoID = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		
		HashMap<String, Object> getMaxPOID = getSqlSession().selectOne("getMaxPOID", map);
		maxPoID = getMaxPOID.get("poid").toString();
		
		return maxPoID;
		
	}
	public int insert ( contentsManager contents ) throws Exception {
		int returnValue = 0;
		String getMaxPOID = getMaxPOID();
		String uploadPath = WebConfig.getDefaultServerRoot() + "/resources/img/portletImage/";    
		String maxUploadSize = WebConfig.getMaxUploadSize();
		String fileName = "";
		
		
		logger.info("Upload Path : {}" , Utils.getString(uploadPath, "").replaceAll("[\r\n]",""));
		ArrayList<LinkedHashMap<String, Object>> arrayIect7101 = new ArrayList<LinkedHashMap<String,Object>>();
		TableObject iect7100 = new TableObject();
		iect7100.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID(), true);
		if((contents.getPoID()).equals("")){
			iect7100.put("POID"								, Integer.parseInt(getMaxPOID), true);
		}else{
			iect7100.put("POID"								, Integer.parseInt(contents.getPoID()), true);
		}
		iect7100.put("NAME"								, contents.getName());
		iect7100.put("ACTIVE"								, Integer.valueOf(contents.getActive()));
		iect7100.put("ALWAYS_ACTIVE"					, contents.getAlwaysActive());
		iect7100.put("MENU_OPEN_TYPE"				, contents.getMenuOpenType());
		
		try{
			if(contents.getPoID().equals("")){
				logger.info("Insert Contetns Info Data Start");
				dbmsDAO.insertTable("iportal_custom.IECT7100", iect7100);
				logger.info("Insert Contetns Info Data End");
				returnValue = 0;
			}else{
				logger.info("Update Contents Info Data Start");
				dbmsDAO.updateTable("iportal_custom.IECT7100", iect7100.getKeyMap(), iect7100);
				logger.info("Update Contents Info Data End");
				
				TableObject deleteIECT7101 = new TableObject();
				deleteIECT7101.put("ACCT_ID"							, loginSessionInfoFactory.getObject().getAcctID());
				deleteIECT7101.put("POID"								, Integer.parseInt(contents.getPoID()));
				logger.info("Delete Contents Key Data Start");
				dbmsDAO.deleteTable("iportal_custom.IECT7101", deleteIECT7101);
				logger.info("Delete Contents Key Data End");
				returnValue = 1;
			}
			for(int i=0; i<contents.getKeyName().length;i++){
				TableObject IECT7101 = new TableObject();
				IECT7101.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
				if((contents.getPoID()).equals("")){
					IECT7101.put("POID"								, Integer.parseInt(getMaxPOID));
				}else{
					IECT7101.put("POID"								, Integer.parseInt(contents.getPoID()));
				}
				
				IECT7101.put("KEY_NAME"								, contents.getKeyName()[i]);
				IECT7101.put("KEY_VALUE"							, contents.getKeyValue()[i]);
				IECT7101.put("EXPLAIN"								, contents.getExplain()[i]);
				arrayIect7101.add(IECT7101);
			}
			logger.info("Insert Contents Key Data Start");
			dbmsDAO.insertTable("iportal_custom.IECT7101", arrayIect7101);
			logger.info("Insert Contents Key Data End");

			if(contents.getPortletImage() != null){
				logger.info("File Upload Start");
				if(contents.getPoID().equals("")){
					fileName =getMaxPOID + ".gif";
				}else{
					fileName = contents.getPoID() + ".gif";
				}
				CommonsMultipartFile cmFile = contents.getPortletImage();
				fileSave(cmFile, uploadPath, fileName, maxUploadSize);
				logger.info("File Upload End");
			}
		}catch(Exception e){
			returnValue = 2;
			logger.error("ERROR > Insert & Update Contents Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int deleteByContents ( contentsManager contents ) throws Exception {
		int returnValue = 0;
		TableObject IECT7100 = new TableObject();
		IECT7100.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		IECT7100.put("POID"									, Integer.valueOf(contents.getPoID()));
		
		try{
			logger.info("Delete Contents Info Data Start");
			dbmsDAO.deleteTable("iportal_custom.IECT7100", IECT7100);
			logger.info("Delete Contents Info Data End");
			logger.info("Delete Contents Key Value Start");
			dbmsDAO.deleteTable("iportal_custom.IECT7101", IECT7100);
			logger.info("Delete Contents Key Value End");
			
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("ERROR > Delete Contents Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
		
	}
	public void fileSave( CommonsMultipartFile file, String upLoadPath, String fileName, String maxUploadSize ) throws Exception {
		FileService fileUtil = new FileService(upLoadPath, Long.parseLong(maxUploadSize));
		try{
			fileUtil.setUpLoadPath(upLoadPath);
			fileUtil.writeFile(file, fileName);
		}catch(Exception e){
			logger.error("ERROR > File Save Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
	}
}
