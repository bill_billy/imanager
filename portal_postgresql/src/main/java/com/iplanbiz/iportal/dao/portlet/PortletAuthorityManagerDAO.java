package com.iplanbiz.iportal.dao.portlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.auth.EmpView;
import com.iplanbiz.iportal.dao.auth.GroupView;

@Repository
public class PortletAuthorityManagerDAO extends SqlSessionDaoSupport {
	
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Autowired DBMSDAO dbmsDAO;
	@Autowired EmpView empView;
	@Autowired GroupView groupView;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public JSONArray getPortletAuthorityList () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"														, loginSessionInfoFactory.getObject().getAcctID());
		
		try{
			logger.info("Get Portlet Authority List Data Start");
			list = getSqlSession().selectList("getPortletAuthorityList", map);
			logger.info("Get Portlet Authority List Data End");
			
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("PID"													, 0);
					value.put("ACCT_ID"												, list.get(i).get("acct_id"));
					value.put("PLID"													, list.get(i).get("plid"));
					value.put("PL_NAME"											, list.get(i).get("pl_name"));
					value.put("PL_STATUS"											, list.get(i).get("pl_status"));
					value.put("PL_LAYOUT"											, list.get(i).get("pl_layout"));
					value.put("PL_DEFAULT"										, list.get(i).get("pl_default"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Get Portlet Authority List Data Error : {} " ,e);
			throw e;
		}
		return returnValue;
	}
	public JSONArray getPortletPermissionUserList ( String plID, String empName, String empID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<LinkedHashMap<String, Object>> list = null;
		HashMap<String,Boolean> checkedMap = new HashMap<String, Boolean>();
		HashMap<String,Boolean> pk = new HashMap<String, Boolean>();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("PLID"													, plID); 
		
		try{ 
			List<HashMap<String,Object>> checks = getSqlSession().selectList("getPortletPermissionUserList", map);
			StringBuilder permissionUsers = new StringBuilder();
			for(int i = 0; i < checks.size();i++) {
				permissionUsers.append(checks.get(i).get("FKEY").toString().trim());
				checkedMap.put(checks.get(i).get("FKEY").toString().trim(),true);
				if(i!=checks.size()-1) {
					permissionUsers.append(",");
				}
			} 
			List<LinkedHashMap<String, Object>> acceptlist = empView.search(permissionUsers.toString(), "", "", loginSessionInfoFactory.getObject().getAcctID(), "");
			list = empView.search(empID, empName, "", loginSessionInfoFactory.getObject().getAcctID(), "");
			acceptlist.addAll(list);
			
			list = acceptlist;
			//logger.info("checkedMap:{}",checkedMap);
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){ 
					if(list.get(i).get("EMP_ID")==null) continue;
					if(pk.containsKey(list.get(i).get("EMP_ID").toString())) continue;
					pk.put(list.get(i).get("EMP_ID").toString(), true);
					
					if(checkedMap.containsKey(list.get(i).get("EMP_ID").toString()))
						list.get(i).put("EMP_CHECK"							, list.get(i).get("EMP_ID"));
					else
						list.get(i).put("EMP_CHECK"							, "");
					//logger.info("list.get(i):{}",list.get(i)); 
					returnValue.add(list.get(i));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Get Portlet Permission User List Data Error : {} " ,e);
			throw e;
		}
		return returnValue;
	}
	public JSONArray getPortletPermissionGroupList ( String plID , String grpTyp, String groupName) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<LinkedHashMap<String, Object>> list = null;
		HashMap<String, Boolean> check = new HashMap<String, Boolean>();
		HashMap<String, Boolean> key = new HashMap<String, Boolean>();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("PLID"													, plID);
 
		list = groupView.search("", groupName, grpTyp, "", "", loginSessionInfoFactory.getObject().getAcctID());
		try{
			logger.info("Get Portlet Permission Group List Data Start");
			List<HashMap<String, Object>> accepts = getSqlSession().selectList("getPortletPermissionGroupList", map);
			StringBuilder groups = new StringBuilder();
			for(int i = 0; i < accepts.size();i++) {
				groups.append(accepts.get(i).get("FKEY").toString());
				check.put(accepts.get(i).get("FKEY").toString(), true);
				if(i!=accepts.size()-1) {
					groups.append(",");
				}
			}
			List<LinkedHashMap<String, Object>> acceptlist = groupView.search("", "", grpTyp, groups.toString(), "", loginSessionInfoFactory.getObject().getAcctID());
			acceptlist.addAll(list);
			list = acceptlist;
			logger.info("Get Portlet Permission Group List Data End");
			
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					if(key.containsKey(list.get(i).get("USER_GRP_ID").toString())) continue;
					key.put(list.get(i).get("USER_GRP_ID").toString(),true); 
					if(check.containsKey(list.get(i).get("USER_GRP_ID").toString()))
						list.get(i).put("USER_GRP_CHECK"					, list.get(i).get("USER_GRP_ID"));
					else
						list.get(i).put("USER_GRP_CHECK"					, "");
					returnValue.add(list.get(i));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Get Portlet Permission Group List Data Error : {} " ,e);
			throw e;
		}
		return returnValue;
	}
	public JSONArray getPortletPermissionRoleList ( String plID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("PLID"													, plID);
		
		try{
			logger.info("Get Portlet Permission Role List Data Start");
			list = getSqlSession().selectList("getPortletPermissionRoleList", map);
			logger.info("Get Portlet Permission Role List Data End");
			
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("GID"										, list.get(i).get("gid"));
					value.put("GROUP_NAME"						, list.get(i).get("group_name"));
					value.put("GROUP_CHECK"						, list.get(i).get("group_check"));
					value.put("SORT_ORDER"							, list.get(i).get("sort_order"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("ERROR > Get Portlet Permission Role List Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertPortletPermission (String plID, String[] arrayUserData, String[] arrayGroupData, String[] arrayRoleData) throws Exception {
		int returnValue = 0;
		ArrayList<TableObject> arrayUserList = new ArrayList<TableObject>();
		ArrayList<TableObject> arrayGroupList = new ArrayList<TableObject>();
		ArrayList<TableObject> arrayRoleList = new ArrayList<TableObject>();
		
		try{
			//해당 포틀릿 권한 전부 삭제
			TableObject deleteIECT7155 = new TableObject();
			deleteIECT7155.put("PLID"								, Integer.valueOf(plID));
			dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7155"), deleteIECT7155);
			
			if(arrayUserData!=null) {
				for(int i=0;i<arrayUserData.length;i++){
					TableObject insertIECT7155User = new TableObject();
					insertIECT7155User.put("PLID"					, Integer.valueOf(plID),true);
					insertIECT7155User.put("FKEY"					, arrayUserData[i],true);
					insertIECT7155User.put("TYPE"					, "u",true);
					arrayUserList.add(insertIECT7155User);
				}
			}
			if(arrayGroupData!=null) {
				for(int j=0;j<arrayGroupData.length;j++){
					TableObject insertIECT7155Group = new TableObject();
					insertIECT7155Group.put("PLID"					, Integer.valueOf(plID),true);
					insertIECT7155Group.put("FKEY"					, arrayGroupData[j],true);
					insertIECT7155Group.put("TYPE"					, "p",true);
					arrayGroupList.add(insertIECT7155Group);
				}
			}
			if(arrayRoleData!=null) {
				for(int k=0;k<arrayRoleData.length;k++){
					TableObject insertIECT7155Role = new TableObject();
					insertIECT7155Role.put("PLID"					, Integer.valueOf(plID),true);
					insertIECT7155Role.put("FKEY"					, arrayRoleData[k],true);
					insertIECT7155Role.put("TYPE"					, "g",true);
					arrayRoleList.add(insertIECT7155Role);
				}
			}
			
			if(arrayUserList.size() > 0){
				logger.info("Insert Portlet Authority User Data Start");
				dbmsDAO.upsertTable(WebConfig.getCustomTableName("IECT7155"), arrayUserList);
				logger.info("Insert Portlet Authority User Data End");
			}else{
				logger.info("Insert Portlet Authority Uset Data None");
			}
			
			if(arrayGroupList.size() > 0){
				logger.info("Insert Portlet Authority Group Data Start");
				dbmsDAO.upsertTable(WebConfig.getCustomTableName("IECT7155"), arrayGroupList);
				logger.info("Insert Portlet Authority Group Data End");
			}else{
				logger.info("Insert Portlet Authority Group Data None");
			}
			
			if(arrayRoleList.size() > 0){
				logger.info("Insert Portlet Authority Role Data Start");
				dbmsDAO.upsertTable(WebConfig.getCustomTableName("IECT7155"), arrayRoleList);
				logger.info("Insert Portlet Authority Role Data End");
			}else{
				logger.info("Inset Portlet Authority Role Data None");
			}
			
			
		}catch(Exception e){
			logger.error("ERROR > Insert Portlet Authority Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	 
	
}
