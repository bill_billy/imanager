package com.iplanbiz.iportal.dao.admin;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dao.auth.EmpView;
import com.iplanbiz.iportal.dao.auth.GroupView;
import com.iplanbiz.iportal.dao.auth.RoleView;

@Repository
public class MenuAuthoritySearchDAO extends SqlSessionDaoSupport {
	
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Autowired EmpView empView;
	@Autowired GroupView groupView;
	@Autowired RoleView roleView;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public JSONArray getMenuAuthroitySearchUserList(String empName, String userGrpName) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<LinkedHashMap<String, Object>> list = null; 
		try{ 
			list = empView.search("", empName, "",  loginSessionInfoFactory.getObject().getAcctID(), ""); 
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){ 
					returnValue.add(list.get(i));
				}
			}
		}catch(Exception e){
			logger.error("Get Menu Authority User List Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	
	public JSONArray getMenuAuthoritySearchList ( String empID ,String parentMenuID) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("EMP_ID"											, empID);
		map.put("P_ID"												, parentMenuID);
		map.put("USER_GRP_ID", groupView.getStringByUserID(empID));
		try{
			logger.info("Get Menu Authority Search List Data Start");
			list = getSqlSession().selectList("getMenuAuthoritySearchList", map); 
			logger.info("Get Menu Authority Search List Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("C_ID"									, list.get(i).get("c_id"));
					value.put("C_NAME"								, list.get(i).get("c_name"));
					value.put("P_ID"									, list.get(i).get("p_id"));
					value.put("MENU_TYPE"						, list.get(i).get("menu_type"));
					value.put("PERM_ALL"							, list.get(i).get("perm_all"));
					value.put("PERM_USER"						, list.get(i).get("perm_user"));
					value.put("PERM_GROUP"						, list.get(i).get("perm_group"));
					value.put("PERM_ROLE"						, list.get(i).get("perm_role"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Menu Authority Search List Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthoritySearchGroupByEmp ( String empID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<LinkedHashMap<String, Object>> list = null; 
		try{ 
			list = groupView.getListByUserID(empID); 
			if(list.size()>0){
				for(int i=0;i<list.size();i++){ 
					returnValue.add(list.get(i)); 
				}
			}
		}catch(Exception e){
			logger.error("Get Menu Authority Search Group By Emp List Data Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthoritySearchRoleByEmp ( String empID ) throws Exception {
		JSONArray returnValue = new JSONArray();;
		List<LinkedHashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("EMP_ID"												, empID);
		try{
			logger.info("Get Menu Authority Search Role By Emp List Data Start");
			list = roleView.getListByUserID(empID, loginSessionInfoFactory.getObject().getAcctID());//getSqlSession().selectList("getMenuAuthoritySearchRoleByEmp", map);
			logger.info("Get Menu Authority Search Role By Emp List Data Start");
			if(list.size()>0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("GID"										, list.get(i).get("GID"));
					value.put("GROUP_NAME"						, list.get(i).get("GROUP_NAME"));
					returnValue.add(value);
				}
			}
			
		}catch(Exception e){
			logger.error("Get Menu Authoirty Search Role By Emp List Data Error : {} ", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthoritySearchMenuByReport () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("USER_ID"												, loginSessionInfoFactory.getObject().getUserId());
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		
		try{
			logger.info("Get Menu Authority Search Menu By Report Data Start");
			list = getSqlSession().selectList("getMenuAuthoritySearchMenuByReport", map);
			logger.info("Get Menu Authority Search Menu By Report Data End");
			if(list.size()>0){
				for( int i = 0; i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("C_ID"										, list.get(i).get("c_id"));
					value.put("NAME"									, list.get(i).get("c_name"));
					value.put("PID"										, list.get(i).get("p_id"));
					value.put("MENU_TYPE"							, list.get(i).get("menu_type"));
					value.put("MENU_OPEN_TYPE"					, list.get(i).get("menu_open_type"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Menu Authority Search Menu By Report Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuAuthoritySearchMenuUserByReport ( String cID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		map.put("EMP_ID"													, loginSessionInfoFactory.getObject().getUserId());
		map.put("C_ID"														, cID);
		
		try{
			logger.info("Get Menu Authority Search Menu User By Report Data Start");
			list = getSqlSession().selectList("getMenuAuthoritySearchMenuUserByReport", map);
			logger.info("Get Menu Authority Search Menu User By Report Data End");
			if(list.size()>0){
				Map<String,JSONObject> empMap = new HashMap<String, JSONObject>();
				for(int i=0;i<list.size();i++){
					
					String fkey = list.get(i).get("fkey").toString();
					String type = list.get(i).get("type").toString();
					List<LinkedHashMap<String, Object>> emps = null;
					String pType = "";
					String gType = "";
					String uType = "";
					if(type.equals("g")) {
						emps = empView.getListByRoleID(fkey);
					}
					else if(type.equals("p")) {
						emps = empView.getListByGroupID(fkey);
					}
					else if(type.equals("u")) {
						emps = empView.getListByUserID(fkey);
					}
					else if(type.equals("all")) {
						JSONObject value = new JSONObject();
						value.put("EMP_ID", "ALL");
						value.put("EMP_NM", "전체공개");
						value.put("USER_GRP_NM", "");
						value.put("MENU_TYPE", "●");
						returnValue.add(value);
						continue;
					}
					
					if(emps!=null) {
						for(int j=0;j<emps.size();j++) {
							
							LinkedHashMap<String,Object> emp = emps.get(j);
							String empID = emp.get("EMP_ID").toString();
							JSONObject value = null;
							if(empMap.containsKey(empID)) {
								value = empMap.get(empID);
							}else {
								value = new JSONObject();
							}
							
							value.put("EMP_ID"										, empID);
							value.put("EMP_NM"									,emp.get("EMP_NM"));
							value.put("USER_GRP_NM"							, emp.get("USER_GRP_NM"));							
							value.put("MENU_TYPE"								,     "");
							
							
							
							if(type.equals("g")) {
								value.put("G_TYPE"										, "●");
							}
							else if(type.equals("p")) {
								value.put("P_TYPE"										, "●");
							}
							else if(type.equals("u")) {
								value.put("U_TYPE"										, "●"); 
							}
							
							if(empMap.containsKey(empID)==false) {
								returnValue.add(value);
								empMap.put(empID, value);
							} 
						}
					} 
				}
				
			}
		}catch(Exception e){
			logger.error("Get Menu Authority Search Menu User By Report Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
}
