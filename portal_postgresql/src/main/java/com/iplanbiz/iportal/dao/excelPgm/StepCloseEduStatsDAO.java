package com.iplanbiz.iportal.dao.excelPgm;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;

@Repository
public class StepCloseEduStatsDAO extends SqlSessionDaoSupport{
	
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired DBMSDAO dbmsDAO;
	
	public JSONArray getStepListForCloseEduStat(String yyyy)  throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("YYYY"												, yyyy);
		
		try{
			logger.info("getStepListForCloseEduStat Data Start");
			list = getSqlSession().selectList("getStepListForCloseEduStat", map);
			logger.info("getStepListForCloseEduStat Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("YYYY"									, list.get(i).get("yyyy"));
					value.put("MM"										, list.get(i).get("mm"));
					value.put("MM_NM"									, list.get(i).get("mm_nm"));
					value.put("STATUS"									, list.get(i).get("status"));
					value.put("UPDATE_DAT"								, list.get(i).get("update_dat"));
					value.put("EMP_NM"									, list.get(i).get("emp_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("getStepListForCloseEduStat Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getStepHistoryForCloseEduStat(String yyyy,String mm)  throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("YYYY"												, yyyy);
		map.put("MM"												, mm);
		
		try{
			logger.info("getStepHistoryForCloseEduStat Data Start");
			list = getSqlSession().selectList("getStepHistoryForCloseEduStat", map);
			logger.info("getStepHistoryForCloseEduStat Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("YYYY"									, list.get(i).get("yyyy"));
					value.put("MM"										, list.get(i).get("mm"));
					value.put("MM_NM"									, list.get(i).get("mm_nm"));
					value.put("BEFORE_STATUS"							, list.get(i).get("before_status"));
					value.put("AFTER_STATUS"							, list.get(i).get("after_status"));
					value.put("TXT"										, list.get(i).get("txt"));
					value.put("UPDATE_DAT"								, list.get(i).get("update_dat"));
					value.put("EMP_NM"									, list.get(i).get("emp_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("getStepHistoryForCloseEduStat Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public void stepCloseEduStatsSave(String yyyy, String mm, String status) throws Exception {
		//마감 상태 저장
		TableObject iect0100 = new TableObject();  
		//마감 이력 저장 
		TableObject iect0101 = new TableObject();
		TableCondition prevIect0100Condition = new TableCondition();
				
		iect0100.put("YYYY", yyyy,true);
		iect0100.put("MM", mm,true);
				
		prevIect0100Condition.put("YYYY", yyyy);
		prevIect0100Condition.put("MM", mm);
		String prevStatus = "";
		List<LinkedHashMap<String,Object>> original = dbmsDAO.selectTable(WebConfig.getCustomTableName("IECT0100"),prevIect0100Condition);
		if(original.isEmpty()==false){
			prevStatus = original.get(0).get("status").toString();
		}
				
		iect0100.put("STATUS", status);		
		iect0100.putCurrentTimeStamp("UPDATE_DAT");
		iect0100.put("UPDATE_EMP", loginSessionInfoFactory.getObject().getUserId());
				
				
		iect0101.put("YYYY", yyyy);
		iect0101.put("MM", mm);
		iect0101.put("BEFORE_STATUS", prevStatus);
		iect0101.put("AFTER_STATUS", status);		
		iect0101.putCurrentTimeStamp("UPDATE_DAT");
		iect0101.put("UPDATE_EMP", loginSessionInfoFactory.getObject().getUserId());
		iect0101.putCurrentTimeStamp("INSERT_DAT");
		iect0101.put("INSERT_EMP", loginSessionInfoFactory.getObject().getUserId());
		try {
			if(dbmsDAO.countTable(WebConfig.getCustomTableName("IECT0100"), iect0100.getKeyMap())==0){
				iect0100.putCurrentTimeStamp("INSERT_DAT");
				iect0100.put("INSERT_EMP", loginSessionInfoFactory.getObject().getUserId());
				dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT0100"),iect0100);
			}else{
				dbmsDAO.updateTable(WebConfig.getCustomTableName("IECT0100"), iect0100.getKeyMap(),iect0100);
			}
					
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT0101"),"IDX", iect0101.getKeyMap(), iect0101);
					
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
			throw e;
		}
		
	}
	
}
