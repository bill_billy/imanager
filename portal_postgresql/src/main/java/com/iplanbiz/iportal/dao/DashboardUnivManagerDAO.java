package com.iplanbiz.iportal.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;

@Repository
public class DashboardUnivManagerDAO extends SqlSessionDaoSupport {
	
 
	@Autowired DBMSDAO dbmsDAO;
	@Autowired SqlSessionFactory sqlSessionFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	public JSONArray getDashboardUnivList(String scID) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("SC_ID"											, Integer.valueOf(scID));
		
		try{
			logger.info("Get Dash Board Univ Data Start");
			list = getSqlSession().selectList("getDashboardUnivList", map);
			logger.info("Get Dash Board Univ Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("SC_ID"									, list.get(i).get("sc_id"));
					value.put("UNIV_ID"								, list.get(i).get("univ_id"));
					value.put("UNIV_NM"							, list.get(i).get("univ_nm"));
					value.put("UNIV_GRP1"							, list.get(i).get("univ_grp1"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Dash Board Univ Data Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONObject getDashboardUnivScGubn() throws Exception{
		JSONObject returnValue = new JSONObject();
		HashMap<String, Object> value = new HashMap<String, Object>();
		try{
			logger.info("Get Dash Board Univ Gubn Data Start");
			value = getSqlSession().selectOne("getDashboardUnivScGubn");
			logger.info("Get Dash Board Univ Gubn Data End");
			if(value.size() > 0){
				returnValue.put("SC_ID"							, value.get("sc_id"));
				returnValue.put("SC_NM"							, value.get("sc_nm"));
				returnValue.put("UNIV_GRP1"					, value.get("univ_grp1"));
			}
		}catch(Exception e){
			logger.error("Get Dash Board Univ Gubn Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getDashboardPopupUnivList(String univName) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("UNIV_NM"									, univName);
		try{
			logger.info("Get Dash board Popup Univ List Data Start");
			list = getSqlSession().selectList("getDashboardPopupUnivList", map);
			logger.info("Get Dash board Popup Univ List Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("UNIV_ID"										, list.get(i).get("univ_id"));
					value.put("UNIV_NM"									, list.get(i).get("univ_nm"));
					value.put("UNIV_GRP"									, list.get(i).get("univ_grp_nm"));
					value.put("UNIV_GRP1"									, list.get(i).get("univ_grp1_nm"));
					value.put("UNIV_GRP2"									, list.get(i).get("univ_grp2_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Dash board Popup Univ List Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertDashboardUniv(String scID, String univID, String univGrp1) throws Exception{
		int returnValue = 0;
		TableObject ivfe1001 = new TableObject();
		ivfe1001.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		ivfe1001.put("UNIV_GRP1"												, univGrp1);
		ivfe1001.put("SC_ID"														, Integer.valueOf(scID));
		ivfe1001.put("UNIV_ID"													, Integer.valueOf(univID));
		ivfe1001.putCurrentDate("INSERT_DAT");
		ivfe1001.put("INSERT_EMP"											, loginSessionInfoFactory.getObject().getUserId());
		try{
			logger.info("Insert Dash board Univ Data Start");
			dbmsDAO.insertTable("iportal_custom.IVFE1001", ivfe1001);
			logger.info("Insert Dash board Univ Data End");
			returnValue = 0;
		}catch(Exception e){
			logger.error("Insert Dash board Univ Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int removeDashboardUniv( String scID, String[] univID) throws Exception{
		int returnValue = 0;
		ArrayList<LinkedHashMap<String, Object>> arrayIvfe1001 = new ArrayList<LinkedHashMap<String,Object>>();
		for(int i=0;i<univID.length;i++){
			TableObject ivfe1001 = new TableObject();
			ivfe1001.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
			ivfe1001.put("SC_ID"														, Integer.valueOf(scID));
			ivfe1001.put("UNIV_ID"													, Integer.valueOf(univID[i]));
			arrayIvfe1001.add(ivfe1001);
		}
		
		try{
			logger.info("Delete Dash board Univ Data Start");
			dbmsDAO.deleteTable("iportal_custom.IVFE1001", new String []{"ACCT_ID", "SC_ID", "UNIV_ID"}, arrayIvfe1001);
			logger.info("Delete Dash board Univ Data End");
			returnValue = 0;
		}catch(Exception e){
			logger.error("Delete Dash board Univ Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	
	
}
