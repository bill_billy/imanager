package com.iplanbiz.iportal.dao.admin;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mortbay.util.ajax.JSON;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.util.concurrent.ExecutionError;
import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.comm.cognos.CRNConnect;
import com.iplanbiz.iportal.comm.cognos.MenuUtil;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.dao.auth.GroupView;
import com.iplanbiz.iportal.dto.Menu;

@Repository
public class MenuDAO extends SqlSessionDaoSupport {
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	DBMSDAO dbmsDAO; 
	
	@Autowired
	GroupView groupView; 
	
	
	public JSONArray getRootCID() throws Exception{
		JSONArray resultValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			listValue = this.getSqlSession().selectList("getRootCID", map);
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("C_ID", listValue.get(i).get("c_id").toString());
					resultValue.add(putValue);
				}
			}
		}catch(Exception e){
			resultValue = null;
			logger.error("ERROR : {}", e);
			throw e;
		}
		return resultValue;
	}
	public JSONArray getTopMenuList(String pID) throws Exception{
		JSONArray resultValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			String userGrpID = groupView.getStringByUserID(loginSessionInfoFactory.getObject().getUserId());

			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
			map.put("USER_ID"									, loginSessionInfoFactory.getObject().getUserId());
			map.put("P_ID"										, pID);
			
			map.put("USER_GRP_ID"								, userGrpID);
			listValue = this.getSqlSession().selectList("getTopMenuList", map);
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("C_ID", listValue.get(i).get("c_id").toString());
					putValue.put("C_NAME", listValue.get(i).get("c_name").toString());
					putValue.put("C_LINK", listValue.get(i).get("c_link"));
					putValue.put("SORT_ORDER", listValue.get(i).get("sort_order").toString());
					putValue.put("SOURCE_OWNER", listValue.get(i).get("source_owner").toString());
					resultValue.add(putValue);
				}
			}
		}catch(Exception e){
			logger.error("ERROR : {}", e);
			e.printStackTrace();
			throw e;
		}
		return resultValue;
	}
	public JSONArray getMenuList(String pID) throws Exception {
		JSONArray resultValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		
		try{
			String userGrpID = groupView.getStringByUserID(loginSessionInfoFactory.getObject().getUserId());
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
			map.put("EMP_ID"									, loginSessionInfoFactory.getObject().getUserId());
			map.put("P_ID"										, pID);
			map.put("USER_GRP_ID"										, userGrpID);
			listValue = this.getSqlSession().selectList("getMenuList", map);
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("C_ID"							, listValue.get(i).get("c_id"));
					putValue.put("C_NAME"					, listValue.get(i).get("c_name"));
					putValue.put("P_ID"							, listValue.get(i).get("p_id"));
					putValue.put("C_LINK"						, listValue.get(i).get("c_link"));
					putValue.put("SORT_ORDER" 			, listValue.get(i).get("sort_order"));
					putValue.put("MENU_TYPE"				, listValue.get(i).get("menu_type"));
					putValue.put("SOURCE_OWNER"		, listValue.get(i).get("source_owner"));
					resultValue.add(putValue);
				}
			}
		}catch(Exception e){
			logger.error("ERROR : {}", e);
			e.printStackTrace();
			throw e;
		}
		return resultValue;
	}
	public JSONArray getTreeGridMenuList() throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
			map.put("USER_ID"											, loginSessionInfoFactory.getObject().getUserId());
			logger.info("Get Menu Data Start");
			listValue = getSqlSession().selectList("getTreeGridMenuList", map);
			logger.info("Get Menu Data End");

			if(listValue.size() > 0){
				logger.info("List Data >> JSONArray Start");
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("C_ID"										, listValue.get(i).get("c_id"));
					putValue.put("C_NAME"								, listValue.get(i).get("c_name"));
					putValue.put("P_ID"										, listValue.get(i).get("p_id"));
					putValue.put("P_NAME"								, listValue.get(i).get("p_name"));
					putValue.put("SORT_ORDER"							, listValue.get(i).get("sort_order"));
					putValue.put("USE_YN"									, listValue.get(i).get("use_yn"));
					putValue.put("MENU_TYPE"							, listValue.get(i).get("menu_type"));
					putValue.put("MENU_OPEN_TYPE"					, listValue.get(i).get("menu_open_type"));
					putValue.put("SOURCE_OWNER"					, listValue.get(i).get("source_owner"));
					putValue.put("C_LINK"									, listValue.get(i).get("c_link"));
					putValue.put("PROG_ID"								, listValue.get(i).get("prog_id"));
					returnValue.add(putValue);
				}
				logger.info("List Data >> JSONArray End");
			}
		}catch(Exception e){
			logger.error("Error Menu Tree Data : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuDomain() throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
			listValue = getSqlSession().selectList("getMenuDomainList", map);
			if(listValue.size() > 0){
				logger.info("Get MenuDomain Data Start");
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("DIVISION"						, listValue.get(i).get("division"));
					putValue.put("DIVISION_NM"				, listValue.get(i).get("division_nm"));
					returnValue.add(putValue);
				}
				logger.info("Get MenuDomain Data End");
			}
		}catch(Exception e){
			logger.error("Error get Menu Domain Data : {} ", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuInfoDetail(String cID) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
			map.put("C_ID"									, cID);
			logger.info("Get Menu Info Data Start");
			listValue = getSqlSession().selectList("getMenuInfoDetail", map);
			logger.info("Get Menu Info Data End");
			if(listValue.size() > 0){
				logger.info("List >>> JSON Change Start");
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("C_ID"												, listValue.get(i).get("c_id"));
					putValue.put("C_NAME"										, listValue.get(i).get("c_name"));
					putValue.put("P_ID"												, listValue.get(i).get("p_id"));
					putValue.put("C_LINK"											, listValue.get(i).get("c_link"));
					putValue.put("STOREID"										, listValue.get(i).get("storeid"));
					putValue.put("DB_NAME"										, listValue.get(i).get("db_name"));
					putValue.put("ACCT_ID"										, listValue.get(i).get("acct_id"));
					putValue.put("SORT_ORDER"									, listValue.get(i).get("sort_order"));
					putValue.put("PROG_ID"										, listValue.get(i).get("prog_id"));
					putValue.put("USE_YN"											, listValue.get(i).get("use_yn"));
					putValue.put("BIGO"											, listValue.get(i).get("bigo"));
					putValue.put("PID_NAME"										, listValue.get(i).get("pid_name"));
					putValue.put("MENU_TYPE"									, listValue.get(i).get("menu_type"));
					putValue.put("MENU_OPEN_TYPE"							, listValue.get(i).get("menu_open_type"));
					putValue.put("SOURCE_OWNER"							, listValue.get(i).get("source_owner"));
					putValue.put("LOCALE"											, listValue.get(i).get("locale"));
					returnValue.add(putValue);
					logger.info("List >>> JSON Change End");
				}
			}
		}catch(Exception e){
			logger.error("Get Menu Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getPopupMenuTreeData(String searchValue) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
			map.put("EMP_ID"										, loginSessionInfoFactory.getObject().getUserId());
			map.put("SEARCH_VALUE"							, searchValue);
			
			logger.info("Get Popup Menu Data Start");
			listValue = getSqlSession().selectList("getPopupMenuTreeData", map);
			logger.info("Get Popup Menu Data End");
			if(listValue.size() > 0){
				logger.info("List >> JSON Change Start");
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("C_ID"										, listValue.get(i).get("c_id"));
					putValue.put("MENU_TYPE"							, listValue.get(i).get("menu_type"));
					putValue.put("PID"										, listValue.get(i).get("pid")); 
					putValue.put("NAME"									, listValue.get(i).get("name"));
					putValue.put("P_NAME"								, listValue.get(i).get("p_name"));
					putValue.put("SORT_ORDER"							, listValue.get(i).get("sort_order"));
					putValue.put("MENU_OPEN_TYPE"					, listValue.get(i).get("menu_open_type"));
					putValue.put("SORT"									, listValue.get(i).get("sort"));
					returnValue.add(putValue);
				}
				logger.info("List >> Grid Change End");
			}
		}catch(Exception e){
			logger.error("Get Popup Menu Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public String getMaxCID() throws Exception{
		String returnValue = "";
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		try{
			HashMap<String, Object> getCID = getSqlSession().selectOne("getMaxCID", map); 
			returnValue = getCID.get("c_id").toString();
		}catch(Exception e){
			logger.error("Get Max CID Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertMenuInfo(Menu menu) throws Exception{
		String maxCid = getMaxCID();
		int returnValue = 0;
		TableObject iect7001 = new TableObject();
//		iect7001.put("P_ID"											, menu.getBtmPID());
		iect7001.put("C_LINK"										, menu.getBtmCLink());
		iect7001.put("SORT_ORDER"								, Integer.valueOf(menu.getBtmSortOrder()));
		iect7001.put("PROG_ID"									, menu.getBtmProgmID());
		iect7001.put("BIGO"											, "");
		iect7001.put("MENU_TYPE"								, Integer.valueOf(menu.getMenuType()));
		iect7001.put("SOURCE_OWNER"							, menu.getBtmSourceOwner()); 
		TableObject iect7027 = new TableObject();
		iect7027.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID(), true);
		iect7027.put("P_ID"											, menu.getBtmPID());
		iect7027.put("USE_YN"										, menu.getBtmUseYn());
		iect7027.put("MENU_OPEN_TYPE"						, menu.getBtmCommonType());
		ArrayList<LinkedHashMap<String, Object>> arrayIect7028 = new ArrayList<LinkedHashMap<String,Object>>();
		try{
			if(menu.getBtmCID().equals("")){
				iect7001.put("C_ID"											, Integer.valueOf(maxCid));
				    
				iect7001.putCurrentTimeStamp("INSERT_DAT");
				iect7001.put("INSERT_EMP",loginSessionInfoFactory.getObject().getUserId());
				
				iect7027.put("C_ID"											, Integer.valueOf(maxCid));   
				dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7001"), iect7001);
				dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7027"), iect7027);
				String[] arrayLocale = menu.getSaveLocale().split(",");
				String[] arrayCName = menu.getSaveCname().split(",");
				for(int i=0;i<arrayLocale.length;i++){
					TableObject iect7028 = new TableObject();
					iect7028.put("C_ID"											, Integer.valueOf(maxCid));
					iect7028.put("LOCALE"										, arrayLocale[i]);
					iect7028.put("C_NAME"										, arrayCName[i]);
					arrayIect7028.add(iect7028);
				}
				dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7028"), arrayIect7028);
				returnValue = 0;
			}else{
				iect7001.put("C_ID"											, Integer.valueOf(menu.getBtmCID()), true);
				iect7001.putCurrentTimeStamp("UPDATE_DAT");
				iect7001.put("UPDATE_EMP",loginSessionInfoFactory.getObject().getUserId());
				iect7027.put("C_ID"											, Integer.valueOf(menu.getBtmCID()), true);
				dbmsDAO.updateTable(WebConfig.getCustomTableName("IECT7001"), iect7001.getKeyMap(), iect7001);
				dbmsDAO.updateTable(WebConfig.getCustomTableName("IECT7027"), iect7027.getKeyMap(), iect7027);
				String[] arrayLocale = menu.getSaveLocale().split(",");
				String[] arrayCName = menu.getSaveCname().split(",");
				for(int i=0;i<arrayLocale.length;i++){
					TableObject iect7028 = new TableObject();
					iect7028.put("C_ID"											, Integer.valueOf(menu.getBtmCID()), true);
					iect7028.put("LOCALE"										, arrayLocale[i], true);
					iect7028.put("C_NAME"										, arrayCName[i]);
					arrayIect7028.add(iect7028);
				}
				dbmsDAO.updateTable(WebConfig.getCustomTableName("IECT7028"), arrayIect7028);
				returnValue = 1;
			}
		}catch(Exception e){
			returnValue = 2;
			logger.error("Insert Menu Info Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertMenuInfoArray(Menu menu) throws Exception{
		int returnValue = 0;
		String[] arrayCID = menu.getArrayCID().split(",");
		String[] arrayPID = menu.getArrayPID().split(",");
		String[] arraySortOrder = menu.getArraySortOrder().split(",");
		String[] arrayUseYn = menu.getArrayUseYn().split(",");
		String[] arrayProgID = menu.getArrayProgID().split(",");
		String[] arrayMenuType = menu.getArrayMenuType().split(",");
		String[] arrayMenuOpenType = menu.getArrayMenuOpenType().split(",");
		String[] arraySourceOwner = menu.getArraySourceOwner().split(",");
		String[] arrayClink = menu.getArrayClink().split(",");
		ArrayList<LinkedHashMap<String, Object>> arrayIect7001 = new ArrayList<LinkedHashMap<String,Object>>();
		ArrayList<LinkedHashMap<String, Object>> arrayIect7027 = new ArrayList<LinkedHashMap<String,Object>>(); 
		try{
			logger.info("Array CID Length : {}" , arrayCID.length);
			logger.info("Array CLink Length : {}" , arrayClink.length);
			for(int i=0;i<arrayCID.length;i++){
				TableObject iect7001 = new TableObject();
				iect7001.put("C_ID"								, Integer.valueOf(arrayCID[i]), true);
				logger.info("Get Clink Data : {}" ,Utils.getString(arrayClink[i], "").replaceAll("[\r\n]",""));
				if(arrayClink.length < 1){
					iect7001.put("C_LINK"							, "");
				}else{
					iect7001.put("C_LINK"							, arrayClink[i]);
				}
				iect7001.put("SORT_ORDER"					, Integer.valueOf(arraySortOrder[i]));
				iect7001.put("PROG_ID"						, arrayProgID[i]);
				iect7001.put("MENU_TYPE"					, Integer.valueOf(arrayMenuType[i]));
				iect7001.put("SOURCE_OWNER"				, arraySourceOwner[i]);
				iect7001.putCurrentTimeStamp("UPDATE_DAT");
				iect7001.put("UPDATE_EMP",loginSessionInfoFactory.getObject().getUserId());
				arrayIect7001.add(iect7001);
				TableObject iect7027 = new TableObject();
				iect7027.put("ACCT_ID"							, loginSessionInfoFactory.getObject().getAcctID(), true);
				iect7027.put("C_ID"								, Integer.valueOf(arrayCID[i]), true);
				iect7027.put("P_ID"								, arrayPID[i], true);
				iect7027.put("USE_YN"							, arrayUseYn[i]);
				iect7027.put("MENU_OPEN_TYPE"			, arrayMenuOpenType[i]);
				arrayIect7027.add(iect7027);
			}  
			dbmsDAO.updateTable(WebConfig.getCustomTableName("IECT7001"), arrayIect7001);
			dbmsDAO.updateTable(WebConfig.getCustomTableName("IECT7027"), arrayIect7027);
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("Update Menu Info Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int deleteMenuInfo(Menu menu) throws Exception{
		int returnValue = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("C_ID"													, menu.getBtmCID());
 
		try{
			HashMap<String,Object> param = new HashMap<String,Object>();
			param.put("C_ID", menu.getBtmCID());
			param.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());			
			List<HashMap<String,Object>> list = getSqlSession().selectList("getChildMenu", param);
			
			
			for(int i = 0; i<list.size();i++) {
				TableCondition iect7001 = new TableCondition();
				TableCondition iect7028 = new TableCondition();
				
				TableCondition iect7004 = new TableCondition();
				TableCondition iect7027 = new TableCondition();
				
				iect7001.put("C_ID", list.get(i).get("C_ID"));
				iect7028.put("C_ID", list.get(i).get("C_ID"));
				iect7004.put("C_ID", list.get(i).get("C_ID"));
				iect7027.put("C_ID", list.get(i).get("C_ID"));
				
				iect7004.put("ACCT_ID", list.get(i).get("ACCT_ID"));
				iect7027.put("ACCT_ID", list.get(i).get("ACCT_ID"));
				dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7001"), iect7001);
				dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7004"), iect7004);
				dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7027"), iect7027);
				dbmsDAO.deleteTable(WebConfig.getCustomTableName("IECT7028"), iect7028);
			}
			
			
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("Menu Info Delete Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public String getCognosReportURL ( String storeID, boolean isView, boolean isIE8) throws Exception {
		LoginSessionInfo loginInfo = loginSessionInfoFactory.getObject();
		
		String cognosReportURL = "";
		
		try{
//			cognosReport
		}catch(Exception e){
		}
		return cognosReportURL;
	}
	public JSONArray getBookList () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String , Object> map = new HashMap<String ,Object>();
		map.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"													, loginSessionInfoFactory.getObject().getUserId());
		try{
			logger.info("Get Book List Data Start");
			list = getSqlSession().selectList("getBookList", map);
			logger.info("Get Book List Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size(); i++){
					JSONObject value = new JSONObject();
					value.put("ACCT_ID"										, list.get(i).get("acct_id"));
					value.put("USER_ID"										, list.get(i).get("user_id"));
					value.put("C_ID"											, list.get(i).get("c_id"));
					value.put("C_NAME"										, list.get(i).get("c_name"));
					value.put("REAL_C_ID"									, list.get(i).get("real_c_id"));
					value.put("P_ID"											, list.get(i).get("p_id"));
					value.put("SORT_ORDER"								, list.get(i).get("sort_order"));
					value.put("MENU_TYPE"								, list.get(i).get("menu_type"));
					value.put("SOURCE_OWNER"							, list.get(i).get("source_owner"));
					value.put("C_LINK"										, list.get(i).get("c_link"));
					value.put("DOC_TYP"										, list.get(i).get("doc_typ"));
					returnValue.add(value);
				}
			}
		}catch ( Exception e ){
			logger.info("ERROR > Error Get Book List Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public String getNavi ( String cID ) throws Exception {
		StringBuffer returnValue = new StringBuffer("");
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("C_ID"													, cID);
		
		try{
			logger.info("Get Menu Navi Start");
			list = getSqlSession().selectList("getNavi", map);
			logger.info("Get Menu Navi End");
			if(list.size() > 0){
				for(int i=0; i<list.size(); i++){
					if(i!=0){
						returnValue.append(">");
					}
					returnValue.append(list.get(i).get("c_name").toString());
				}
			}
		}catch(Exception e){
			logger.error("ERROR > Get Menu Navi Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue.toString();
	}
	public String getMaxBookCID () throws Exception {
		String returnValue = "";
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"											, loginSessionInfoFactory.getObject().getUserId());
		
		try{
			logger.info("Get Max Book CID Data Start");
			list = getSqlSession().selectList("getMaxBookCID", map);
			logger.info("Get Max Book CID Data End");
			
			returnValue = list.get(0).get("c_id").toString();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Get Max Book CID Data Error : {} " ,e);
			throw e;
		}
		return returnValue;
	}
	public int insertMyfolderNoCog ( String cName ) throws Exception {
		int returnValue = 0;
		
		TableObject IECT7034 = new TableObject();
		IECT7034.put("ACCT_ID"														, loginSessionInfoFactory.getObject().getAcctID());
		IECT7034.put("USER_ID"														, loginSessionInfoFactory.getObject().getUserId());
		IECT7034.put("C_ID"																, Integer.valueOf(getMaxBookCID()));
		IECT7034.put("C_NAME"														, cName);
		IECT7034.put("REAL_C_ID"														, Integer.valueOf("0"));
		IECT7034.put("P_ID"																, "5999");
		IECT7034.put("SORT_ORDER"													, Integer.valueOf("0"));
		IECT7034.put("MENU_TYPE"													, "1");
		IECT7034.put("DOC_TYP"													, "FOLDER");
		
		try{
			logger.info("Insert My Folder No Cognos Data Start");
			dbmsDAO.insertTable("iportal_custom.IECT7034",  IECT7034);
			logger.info("Insert My Folder No Cognos Data End");
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			e.printStackTrace();
			logger.error("ERROR > Insert My Folder No Cognos Data Error : {} " ,e);
			throw e;
		}
		return returnValue;
	}
	public int insertBookReport ( String cID, String menuName, String menuType, String pID) throws Exception {
		int returnValue = 0;
		
		TableObject IECT7034 = new TableObject();
		IECT7034.put("ACCT_ID"														, loginSessionInfoFactory.getObject().getAcctID());
		IECT7034.put("USER_ID"														, loginSessionInfoFactory.getObject().getUserId());
		IECT7034.put("C_ID"																, Integer.valueOf(getMaxBookCID()));
		IECT7034.put("C_NAME"														, URLDecoder.decode(menuName,"UTF-8"));
		IECT7034.put("REAL_C_ID"														, cID);
		IECT7034.put("P_ID"																, pID);
		IECT7034.put("SORT_ORDER"													, Integer.valueOf(getMaxBookCID()));
		IECT7034.put("MENU_TYPE"													, menuType);
		
		try{
			logger.info("Insert Book Report Data Start");
			dbmsDAO.insertTable(WebConfig.getCustomTableName("IECT7034"), IECT7034);
			logger.info("Insert Book Report Data End");
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("ERROR > Insert Book Report Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int deleteBookUserData ( String cID ) throws Exception {
		int returnValue = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"														, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"														, loginSessionInfoFactory.getObject().getUserId());
		map.put("C_ID"															, Integer.valueOf(cID));
		
		try{
			logger.info("Delete Book User Data Start");
			getSqlSession().update("deleteBookUserData", map);
			logger.info("Delete Book User Data End");
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("ERROR > Delete Book User Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMenuBiSolutionPathDeepScan( String cID, String cName, String cLink, String ac ) throws Exception {
		JSONArray returnValue = new JSONArray();
		LoginSessionInfo loginInfo = loginSessionInfoFactory.getObject();
		
		CRNConnect connect = (CRNConnect) loginInfo.getExternalInfo("connect");
		String userID = loginInfo.getUserId();
		String defaultRootSearchPath = cLink;
		
		try{
			logger.info("Get Menu BiSolution Path Deep Scan Data Start");
			List<Map<String, Object>> list = MenuUtil.getCognosChildListByMap(cID, defaultRootSearchPath, connect, ac);
			logger.info("Get Menu BiSolution Path Deep Scan Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("C_ID"									, list.get(i).get("C_ID"));
					value.put("P_ID"									, list.get(i).get("P_ID"));
					value.put("id"										, list.get(i).get("id"));
					value.put("C_NAME"								, list.get(i).get("C_NAME"));
					value.put("C_LINK"								, list.get(i).get("C_LINK"));
					value.put("text"									, list.get(i).get("text"));
					value.put("cIcon"									, list.get(i).get("cIcon"));
					value.put("reportType"							, list.get(i).get("reportType"));
					value.put("cType"									, list.get(i).get("cType"));
					value.put("SOURCE_OWNER"					, list.get(i).get("SOURCE_OWNER"));
					value.put("MENU_TYPE"						, list.get(i).get("MENU_TYPE"));
					value.put("hasChildren"						, list.get(i).get("hasChildren"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("ERROR > Get Menu By BISolution Path Deep Scan Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public String getAuditStoreID ( HttpServletRequest request ) throws Exception {
		LoginSessionInfo loginInfo = loginSessionInfoFactory.getObject();
		CRNConnect connect = (CRNConnect) loginInfo.getExternalInfo("connect");
		
		String reportPath = request.getParameter("reportPath");
		String reportNm = request.getParameter("reportNm");
		String reportType = request.getParameter("reportType");
		
		String searchPath = MenuUtil.makeSearchPath(reportPath, reportNm, reportType);
		String storeID = MenuUtil.getStoreIdUseSearchPath(connect, searchPath);
		
		return storeID;
		
	}
	public String getCogReportUrl ( String storeID, boolean isView, boolean isIE8 ) throws Exception {
		String returnValue = "";
		LoginSessionInfo loginInfo = loginSessionInfoFactory.getObject();
		try{
			CRNConnect connect = (CRNConnect) loginInfo.getExternalInfo("connect");
			returnValue = MenuUtil.getReportURL(connect, storeID, isView, isIE8);
		}catch(Exception e){
			logger.error("ERROR > Get Cog Report URL Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public String getCogSearchPath ( String storeID ) throws Exception {
		String returnValue = "";
		LoginSessionInfo loginInfo = loginSessionInfoFactory.getObject();
		
		try{
			CRNConnect connect = (CRNConnect) loginInfo.getExternalInfo("connect");
			returnValue = MenuUtil.getSearchPathUseStoreID(connect, storeID);
		}catch(Exception e){
			logger.error("ERROR > Get Cog Search Path Error : {}" , e);
			e.printStackTrace();
			throw e;
		}  
		return returnValue; 
	}
	public String getReportPath ( String searchPath ) throws Exception {
		String returnValue = "";
		try{
			CRNConnect connect = (CRNConnect) loginSessionInfoFactory.getObject().getExternalInfo("connect");
			returnValue = MenuUtil.getReportPath(connect, searchPath);
		}catch(Exception e){
			logger.error("ERROR > Get Report Path Error : {} " , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int renameMyfolderNoCog(String cId, String cName) throws Exception {
		int returnValue = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"														, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"														, loginSessionInfoFactory.getObject().getUserId());
		map.put("C_ID"															, Integer.valueOf(cId));
		map.put("C_NAME"														, cName);
		try{
			logger.info("renameMyfolderNoCog Start");
			getSqlSession().update("renameMyfolderNoCog", map);
			logger.info("renameMyfolderNoCog End");
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("ERROR > renameMyfolderNoCog Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int moveMyfolderNoCog(String cId, String pId) throws Exception {
		int returnValue = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"														, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"														, loginSessionInfoFactory.getObject().getUserId());
		map.put("C_ID"															, Integer.valueOf(cId));
		map.put("P_ID"															, Integer.valueOf(pId));
		try{
			logger.info("moveMyfolderNoCog Start");
			getSqlSession().update("moveMyfolderNoCog", map);
			logger.info("moveMyfolderNoCog End");
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("ERROR > moveMyfolderNoCog Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int sortMyfolderNoCog(String cId, String targetId, String position, String sortOrder) throws Exception{
		int returnValue = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(position.equals("inside")) {
			sortOrder="0";
		}else {
			String where = "";
			if(position.equals("after"))	where = "C_ID > "+targetId;
			if(position.equals("before"))	where = "C_ID >= "+targetId;
			map.put("ACCT_ID"														, loginSessionInfoFactory.getObject().getAcctID());
			map.put("USER_ID"														, loginSessionInfoFactory.getObject().getUserId());
			map.put("C_ID"															, Integer.valueOf(targetId));
			map.put("SORT_ORDER"													, Integer.valueOf(sortOrder));
			map.put("WHERE"															, where);
			try{
				logger.info("sortMyfolderNoCog Start");
				getSqlSession().update("sortMyfolderNoCog", map); //SORT_ORDER 하나씩 증가
				logger.info("sortMyfolderNoCog End");
				returnValue = 0;
			}catch(Exception e){
				returnValue = 1;
				logger.error("ERROR > sortMyfolderNoCog Error : {} " ,e);
				e.printStackTrace();
				throw e;
			}
		}
		TableObject iect7034 = new TableObject();
		if(position.equals("after")) {
			sortOrder=String.valueOf(Integer.valueOf(sortOrder)+1);
		}
		System.out.println("sortOrder:::"+sortOrder);
		iect7034.put("SORT_ORDER"										, Integer.valueOf(sortOrder));
		iect7034.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID(),true);
		iect7034.put("USER_ID"											, loginSessionInfoFactory.getObject().getUserId(),true);
		iect7034.put("C_ID"												, Integer.valueOf(cId),true);
		
		try{
			logger.info("sortMyfolderNoCog Start");
			dbmsDAO.updateTable(WebConfig.getCustomTableName("IECT7034"), iect7034.getKeyMap(), iect7034);//SORT_ORDER변경
			logger.info("sortMyfolderNoCog End");
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("ERROR > sortMyfolderNoCog Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		
		
		return returnValue;
	}
	public int moveMenuPosition(String cId, String pId) throws Exception {
		int returnValue = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"														, loginSessionInfoFactory.getObject().getAcctID());
		
		map.put("C_ID"															, Integer.valueOf(cId));
		map.put("P_ID"															, Integer.valueOf(pId));
		try{
			logger.info("moveMenuPosition Start");
			getSqlSession().update("moveMenuPosition", map);
			logger.info("moveMenuPosition End");
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("ERROR > moveMenuPosition Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int sortMenuPosition(String cId, String targetId, String position, String sortOrder) throws Exception{
		int returnValue = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		if(position.equals("inside")) {
			sortOrder="0";
		}else {
			String where = "";
			if(position.equals("after"))	where = "A.C_ID > "+targetId;
			if(position.equals("before"))	where = "A.C_ID >= "+targetId;
			map.put("ACCT_ID"														, loginSessionInfoFactory.getObject().getAcctID());
			map.put("C_ID"															, Integer.valueOf(targetId));
			map.put("SORT_ORDER"													, Integer.valueOf(sortOrder));
			map.put("WHERE"															, where);
			try{
				logger.info("sortMenuPosition Start");
				getSqlSession().update("sortMenuPosition", map); //SORT_ORDER 하나씩 증가
				logger.info("sortMenuPosition End");
				returnValue = 0;
			}catch(Exception e){
				returnValue = 1;
				logger.error("ERROR > sortMenuPosition Error : {} " ,e);
				e.printStackTrace();
				throw e;
			}
		}
		TableObject iect7001 = new TableObject();
		if(position.equals("after")) {
			sortOrder=String.valueOf(Integer.valueOf(sortOrder)+1);
		}
		iect7001.put("SORT_ORDER"										, Integer.valueOf(sortOrder));
		iect7001.put("C_ID"												, Integer.valueOf(cId),true);
		
		try{
			logger.info("sortMenuPosition Start");
			dbmsDAO.updateTable(WebConfig.getCustomTableName("IECT7001"), iect7001.getKeyMap(), iect7001);//SORT_ORDER변경
			logger.info("sortMenuPosition End");
			returnValue = 0;
		}catch(Exception e){
			returnValue = 1;
			logger.error("ERROR > sortMenuPosition Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		
		
		return returnValue;
	}
}
