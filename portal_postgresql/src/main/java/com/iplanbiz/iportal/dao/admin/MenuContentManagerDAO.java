package com.iplanbiz.iportal.dao.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.session.LoginSessionInfo;

@Repository
public class MenuContentManagerDAO extends SqlSessionDaoSupport {
	
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	public JSONArray getSolutionListCbo() throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			
			logger.info("Get get SolutionListCbo Start");
			listValue = getSqlSession().selectList("getSolutionListCbo");
			
			logger.info("Get get SolutionListCbo End");
			if(listValue.size() > 0){
				logger.info("Get get SolutionListCbo List > JSONArray Start");
				for(int i=0;i<listValue.size();i++){ 
					returnValue.add(listValue.get(i));
				}
				logger.info("Get get SolutionListCbo List > JSONArray End");

			}
		}catch(Exception e){
			logger.error("Get get SolutionListCbo Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}

	public JSONArray getMenuContentList(String menualId,String searchType,String searchValue) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String,Object> map = new HashMap<String,Object>();
			if(menualId=="") menualId = "-1";
			map.put("MENUAL_ID", Integer.parseInt(menualId));
			map.put("SEARCH_TYPE", searchType);
			map.put("SEARCH_VALUE", searchValue);
			
			logger.info("Get get getMenuContentList Start");
			listValue = getSqlSession().selectList("getMenuContentList",map);
			
			logger.info("Get get getMenuContentList End");
			if(listValue.size() > 0){
				logger.info("Get get getMenuContentList List > JSONArray Start");
				for(int i=0;i<listValue.size();i++){ 
					returnValue.add(listValue.get(i));
				}
				logger.info("Get get getMenuContentList List > JSONArray End");

			}
		}catch(Exception e){
			logger.error("Get get getMenuContentList Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}

	public JSONArray getMenuContenGridtList(String menualId, String searchType, String searchValue) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String,Object> map = new HashMap<String,Object>();
			if(menualId=="") menualId = "-1";
			map.put("MENUAL_ID", Integer.parseInt(menualId));
			map.put("SEARCH_TYPE", searchType);
			map.put("SEARCH_VALUE", searchValue);
			
			logger.info("Get get getMenuContenGridtList Start");
			listValue = getSqlSession().selectList("getMenuContenGridtList",map);
			
			logger.info("Get get getMenuContenGridtList End");
			if(listValue.size() > 0){
				logger.info("Get get getMenuContenGridtList List > JSONArray Start");
				for(int i=0;i<listValue.size();i++){ 
					returnValue.add(listValue.get(i));
				}
				logger.info("Get get getMenuContenGridtList List > JSONArray End");

			}
		}catch(Exception e){
			logger.error("Get get getMenuContenGridtList Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}

	public JSONArray getMenuContentInfo(String cid) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String,Object> map = new HashMap<String,Object>();
			map.put("C_ID", cid);
			
			logger.info("Get get getMenuContentInfo Start");
			listValue = getSqlSession().selectList("getMenuContentInfo",map);
			
			logger.info("Get get getMenuContentInfo End");
			if(listValue.size() > 0){
				logger.info("Get get getMenuContentInfo List > JSONArray Start");
				for(int i=0;i<listValue.size();i++){ 
					returnValue.add(listValue.get(i));
				}
				logger.info("Get get getMenuContentInfo List > JSONArray End");

			}
		}catch(Exception e){
			logger.error("Get get getMenuContentInfo Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public List<HashMap<String, String>> getMenuContentCount(String progId, String parentId) throws Exception{
		List<HashMap<String, String>> listValue = null;
		try{
			Map<String, Object> queryMap = new HashMap<String,Object>();
			queryMap.put("PROG_ID", progId);
			queryMap.put("PARENT_ID", parentId);
			
			logger.info("Get get getMenuContentCount Start");
			listValue = getSqlSession().selectList("getMenuContentCount", queryMap);
			
			logger.info("Get get getMenuContentCount End");
			
		}catch(Exception e){
			logger.error("Get get getMenuContentCount Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return listValue;
	}
	public int insertMenuContent(String solutionList, String progNm, String progId, String localCd, String parentId,  String content) throws Exception {
		int  result =0;
		Map<String, Object> queryMap = new HashMap<String,Object>();
		
		queryMap.put("MENUAL_ID", Integer.parseInt(solutionList));
		queryMap.put("PROG_ID", progId);
		queryMap.put("LOCALE_CD", localCd);
		queryMap.put("PROG_NM", progNm);
		queryMap.put("PARENT_ID", parentId);
		queryMap.put("USE_YN", "Y");
		if(content.length()>0) content = content.replaceAll("&amp;", "&").replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&quot;", "\"");
		queryMap.put("CONTENT_TEXT", content);
		queryMap.put("EMP_ID", loginSessionInfoFactory.getObject().getUserId());
		System.out.println("solutionList==="+solutionList);
		System.out.println("progId==="+progId);
		System.out.println("localCd==="+localCd);
		System.out.println("progNm==="+progNm);
		System.out.println("parentId==="+parentId);
		System.out.println("content==="+content);
		
		try{
			   if(getMenuContentCount(progId, parentId).size() > 0){    
				   getSqlSession().update("getMenuContentUpdate", queryMap);
					result =2;
				}else{
					getSqlSession().update("getMenuContentInSert", queryMap);	
					result =1;
				}
		}catch(Exception e){
			result = 3;
			e.printStackTrace(); 
			throw e;
		}		
		return result;
	}

	public int deleteMenuContent(String solutionList, String progId, String parentId) throws Exception {
		int  result =0;
		Map<String, Object> queryMap = new HashMap<String,Object>();
		queryMap.put("MENUAL_ID", Integer.parseInt(solutionList));
		queryMap.put("PROG_ID", progId);
		queryMap.put("PARENT_ID", parentId);
		queryMap.put("EMP_ID", loginSessionInfoFactory.getObject().getUserId());
		try{
			getSqlSession().update("getMenuContentDelete", queryMap);	
		}catch(Exception e){
			result = 3;
			e.printStackTrace(); 
			throw e;
		}		
		return result;
	}	
}