package com.iplanbiz.iportal.dao.admin;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.session.LoginSessionInfo;

@Repository
public class TechnologyContentListDAO extends SqlSessionDaoSupport {
	
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	public JSONArray getTechnoContentList() throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			
			logger.info("Get get getTechnoContentList Start");
			listValue = getSqlSession().selectList("getTechnoContentList");
			
			logger.info("Get get getTechnoContentList End");
			if(listValue.size() > 0){
				logger.info("Get get getTechnoContentList List > JSONArray Start");
				for(int i=0;i<listValue.size();i++){ 
					returnValue.add(listValue.get(i));
				}
				logger.info("Get get getTechnoContentList List > JSONArray End");

			}
		}catch(Exception e){
			logger.error("Get get getTechnoContentList Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}

	public JSONArray getLabelTechnoContent(String cid) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String,Object> map = new HashMap<String,Object>();
			map.put("CID", cid);
			
			logger.info("Get get getLabelTechnoContent Start");
			listValue = getSqlSession().selectList("getLabelTechnoContent",map);
			
			logger.info("Get get getLabelTechnoContent End");
			if(listValue.size() > 0){
				logger.info("Get get getLabelTechnoContent List > JSONArray Start");
				for(int i=0;i<listValue.size();i++){ 
					returnValue.add(listValue.get(i));
				}
				logger.info("Get get getLabelTechnoContent List > JSONArray End");

			}
		}catch(Exception e){
			logger.error("Get get getLabelTechnoContent Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}

	public JSONArray getTechnoContentInfo(String cid) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String,Object> map = new HashMap<String,Object>();
			map.put("CID", cid);
			
			logger.info("Get get getTechnoContentInfo Start");
			listValue = getSqlSession().selectList("getTechnoContentInfo",map);
			
			logger.info("Get get getTechnoContentInfo End");
			if(listValue.size() > 0){
				logger.info("Get get getTechnoContentInfo List > JSONArray Start");
				for(int i=0;i<listValue.size();i++){ 
					returnValue.add(listValue.get(i));
				}
				logger.info("Get get getTechnoContentInfo List > JSONArray End");

			}
		}catch(Exception e){
			logger.error("Get get getTechnoContentInfo Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}

	public JSONArray getSearchTechnoContent(String content_text) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String,Object> map = new HashMap<String,Object>();
			map.put("CONTENT_TEXT", content_text);
			
			logger.info("Get get getSearchTechnoContent Start");
			listValue = getSqlSession().selectList("getSearchTechnoContent",map);
			
			logger.info("Get get getSearchTechnoContent End");
			if(listValue.size() > 0){
				logger.info("Get get getSearchTechnoContent List > JSONArray Start");
				for(int i=0;i<listValue.size();i++){ 
					returnValue.add(listValue.get(i));
				}
				logger.info("Get get getSearchTechnoContent List > JSONArray End");

			}
		}catch(Exception e){
			logger.error("Get get getSearchTechnoContent Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}	
}