package com.iplanbiz.iportal.dao.portlet;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dao.auth.GroupView;


@Repository
public class PortletDAO extends SqlSessionDaoSupport {

	@Autowired DBMSDAO dbmsDAO;
	@Autowired GroupView groupView;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public List<HashMap<String, Object>> getUserPortletInfo() throws Exception{
		List<HashMap<String, Object>> result = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("UNID"									, loginSessionInfoFactory.getObject().getUserId());
			map.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
			result = this.getSqlSession().selectList("getUserPortletInfo", map);
		}catch(Exception e){
			logger.error("ERROR", e);
			throw e;
		}
		return result; 
	}
	
	public void setDefaultUserPortlet() throws Exception{
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("UNID"										, loginSessionInfoFactory.getObject().getUserId());
			map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
//			map.put("UNID"										, "15");
//			map.put("ACCT_ID"									, "339");
			
			this.getSqlSession().update("deleteUserPortlet", map);
			this.getSqlSession().update("defaultPortlet",map);
			this.getSqlSession().update("defaultLayoutNo",map);
		}catch(Exception e){
			logger.error("ERROR", e);
			throw e;
		}
	}	
	public int getUserPortletLayoutNo() throws Exception{
		int layoutNo = 1;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("USER_ID"										, loginSessionInfoFactory.getObject().getUserId());
			map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
			HashMap<String, Object> getLayoutNo = this.getSqlSession().selectOne("getUserPortletLayoutNo", map);
			layoutNo = Integer.parseInt(getLayoutNo.get("key_value").toString());
		}catch(Exception e){
			logger.error("ERROR" , e);
			layoutNo = 1;
		}
		return layoutNo;
	}
	public JSONArray getPageletByUserID () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"												, loginSessionInfoFactory.getObject().getUserId());
		
		logger.info("Get Pagelet By User ID List Data Start");
		list = getSqlSession().selectList("getPageletByUserID", map);
		logger.info("Get Pagelet By User ID List Data End");
		
		try{
			if(list.size() > 0){
				for(int i=0; i<list.size(); i++){
					JSONObject value = new JSONObject();
					value.put("PLID"											, list.get(i).get("plid"));
					value.put("PL_NAME"									, list.get(i).get("pl_name"));
					value.put("PL_LAYOUT"									, list.get(i).get("pl_layout"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Pagelet By User ID List Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		
		return returnValue;
	}
	public JSONArray getPageletPortletInfo ( String plID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("PLID"													, plID);
		
		try{
			logger.info("Get Pagelet Portlet Info Data Start");
			list = getSqlSession().selectList("getPageletPortletInfo", map);
			logger.info("Get Pagelet Portlet Info Data End");
			if(list.size() > 0){
				for(int i=0; i<list.size(); i++){
					JSONObject value = new JSONObject();
					value.put("POID"											, list.get(i).get("poid"));
					value.put("XAXIS"											, list.get(i).get("xaxis"));
					value.put("YAXIS"											, list.get(i).get("yaxis"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			e.printStackTrace();;
			logger.error("ERROR > Get Pagelet Portlet Info Data Error : {}", e);
			throw e;
		}
		return returnValue;
	}
	public JSONArray getPagetletPortletInfoByUser ( String plID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"														, loginSessionInfoFactory.getObject().getAcctID());
		map.put("PLID"															, Integer.valueOf(plID));
		map.put("UNID"															, loginSessionInfoFactory.getObject().getUserId());
		String userGrpID = groupView.getStringByUserID(loginSessionInfoFactory.getObject().getUserId());
		map.put("USER_GRP_ID",userGrpID);
		try{
			logger.info("Get Pagelet Portlet Info By User Data Start");
			list = getSqlSession().selectList("getPagetletPortletInfoByUser", map);
			logger.info("Get Pagelet Portlet Info By User Data End");
			
			if(list.size () > 0){
				for(int i=0; i<list.size(); i++){
					JSONObject value = new JSONObject();
					value.put("POID"											, list.get(i).get("poid"));
					value.put("XAXIS"											, list.get(i).get("xaxis"));
					value.put("YAXIS"											, list.get(i).get("yaxis"));
					value.put("PORTLET_TYPE"								, list.get(i).get("portlet_type"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Get Pagelet Portlet Info By User Data Error : {} " ,e);
			throw e;
		}
		return returnValue;
	}
	public JSONArray getPortletInfoData ( String poID, String gx ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"													, loginSessionInfoFactory.getObject().getAcctID());
		map.put("POID"														, Integer.valueOf(poID));
		
		try{
			logger.info("Get Portlet Info Data Start");
			list = getSqlSession().selectList("getPortletInfoData", map);
			logger.info("Get Portlet Info Data End");
			if(list.size() > 0){
				JSONObject value = new JSONObject();
				for(int i=0; i<list.size(); i++){
					value.put("POID"											, list.get(i).get("poid"));
					value.put("NAME"										, list.get(i).get("name"));
					value.put("URL"											, list.get(i).get("url"));
					value.put("HEIGHT"										, list.get(i).get("height"));
					value.put("TYPE"											, list.get(i).get("type"));
					value.put("MORE"										, list.get(i).get("more"));
					value.put("MORE_URL"									, list.get(i).get("more_url"));
					value.put("VIEW_HEADER"								, list.get(i).get("view_header"));
					value.put("VIEW_ICON"									, list.get(i).get("view_icon"));
					value.put("gX"												, gx);
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("ERROR > Get Portlet Info Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public String defaultPortletID () {
		String returnValue = "";
		List<LinkedHashMap<String, Object>> list = null;
		TableCondition IECT7104 = new TableCondition();
		IECT7104.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		IECT7104.put("PL_DEFAULT"										, "1");
		
		try{
			list = dbmsDAO.selectTable("iportal_custom.IECT7104", IECT7104);
			returnValue = list.get(0).get("plid").toString();
		}catch(Exception e){
			logger.error("ERROR > Get Default Portlet PLID Error : {} " ,e);
			e.printStackTrace();
			returnValue = null;
		}
		return returnValue;
	}
	public int defaultUserPortlet ( String plID) throws Exception {
		int returnValue = 0;
		TableObject deleteIECT7104 = new TableObject();
		deleteIECT7104.put("ACCT_ID"								, loginSessionInfoFactory.getObject().getAcctID());
		if(plID.equals("")){
			deleteIECT7104.put("PLID"									, this.defaultPortletID());
		}else{
			deleteIECT7104.put("PLID"									, plID);
		}
		deleteIECT7104.put("UNID"									, loginSessionInfoFactory.getObject().getUserId());
		
		try{
			dbmsDAO.deleteTable("iportal_custom.IECT7104",  deleteIECT7104);
			dbmsDAO.deleteTable("iportal_custom.IECT7104",  deleteIECT7104);
			dbmsDAO.deleteTable("iportal_custom.IECT7104",  deleteIECT7104);
		}catch(Exception e){
			
		}
		return returnValue;
	}
	public JSONArray getPageletPortlet ( String poID, String plID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"												, loginSessionInfoFactory.getObject().getAcctID());
		map.put("POID"													, poID);
		map.put("PLID"													, plID);
		
		try{
			logger.info("Get Pagelet Portlet Info Data Start");
			list = getSqlSession().selectList("getPageletPortlet", map);
			logger.info("Get Pagelet Portlet Info Data End");
			
			if(list.size() > 0){
				for(int i=0; i<list.size(); i++){
					JSONObject value = new JSONObject();
					value.put("TITLE_BACKGROUND_COLOR"				, list.get(i).get("title_background_color"));
					value.put("TITLE_FONT_COLOR"							, list.get(i).get("title_font_color"));
					value.put("TITLE_FONT_SIZE"								, list.get(i).get("title_font_size"));
					value.put("CONTENT_BACKGROUND_COLOR"		, list.get(i).get("content_background_color"));
					value.put("CONTENT_FONT_COLOR"					, list.get(i).get("content_font_color"));
					value.put("CONTENT_FONT_SIZE"						, list.get(i).get("content_font_size"));
					value.put("PORTLET_TYPE"									, list.get(i).get("portlet_type"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("ERROR > Get Pagelet Portlet Info Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getPageletPortletByUser ( String poID, String plID) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("ACCT_ID"															, loginSessionInfoFactory.getObject().getAcctID());
		map.put("POID"																, Integer.valueOf(poID));
		map.put("PLID"																, Integer.valueOf(plID));
		map.put("UNID"																, loginSessionInfoFactory.getObject().getUserId());
		
		try{
			logger.info("Get Pagelet Portlet By User Data Start");
			list = getSqlSession().selectList("getPageletPortletByUser", map);
			logger.info("Get Pagelet Portlet By User Data End");
			
			if(list.size() > 0){
				for(int i=0; i<list.size(); i++){
					JSONObject value = new JSONObject();
					value.put("TITLE_BACKGROUND_COLOR"				, list.get(i).get("title_background_color"));
					value.put("TITLE_FONT_COLOR"							, list.get(i).get("title_font_color"));
					value.put("TITLE_FONT_SIZE"								, list.get(i).get("title_font_size"));
					value.put("CONTENT_BACKGROUND_COLOR"		, list.get(i).get("content_background_color"));
					value.put("CONTENT_FONT_COLOR"					, list.get(i).get("content_font_color"));
					value.put("CONTENT_FONT_SIZE"						, list.get(i).get("content_font_size"));
					value.put("PORTLET_TYPE"									, list.get(i).get("portlet_type"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.error("ERROR > Get Pagelet Portlet By User Data Error : {}" , e);
			throw e;
		}
		return returnValue;
	}
	public JSONObject getPageletPortletDefault () throws Exception {
		JSONObject returnValue = new JSONObject();
		List<HashMap<String, Object>> list = null;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"															, loginSessionInfoFactory.getObject().getAcctID());
		
		try{
			logger.info("Get Pagelet Portlet Default Data Start");
			list = getSqlSession().selectList("getPageletPortletDefault", map);
			logger.info("Get Pagelet Portlet Default Data End");
			
			if(list.size() > 0){
				returnValue.put("KEY_VALUE", list.get(0).get("key_value"));
			}
		}catch(Exception e){
			logger.error("ERROR > Get Pagelet Portlet Default Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
}
