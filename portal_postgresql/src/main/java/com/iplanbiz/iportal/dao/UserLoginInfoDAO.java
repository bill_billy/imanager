package com.iplanbiz.iportal.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.session.LoginSessionInfo;

@Repository
public class UserLoginInfoDAO extends SqlSessionDaoSupport {
	
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired SqlSessionFactory sqlSessionFactory;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public JSONArray getUserLoinInfoGubn() throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		try{
			logger.info("Get User Login Info Gubn Data Start");
			list = getSqlSession().selectList("getUserLoinInfoGubn");
			logger.info("Get User Login Info Gubn Data End");
			if(list.size()>0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("COM_COD"									, list.get(i).get("com_cod"));
					value.put("COM_NM"									, list.get(i).get("com_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get User Login Info Gubn Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getUserLoginInfoList ( String yyyymm, String gubn ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"											, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"											, loginSessionInfoFactory.getObject().getUserId());
		map.put("YYYYMM"											, yyyymm);
		map.put("GUBN"												, gubn);
		try{
			logger.info("Get User Login Info List Data Start");
			list = getSqlSession().selectList("getUserLoginInfoList", map);
			logger.info("Get User Login Info List Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("USER_ID"									, list.get(i).get("user_id"));
					value.put("USER_NM"								, list.get(i).get("user_nm"));
					value.put("LOG_INFO"								, list.get(i).get("log_info"));
					value.put("TIME"										, list.get(i).get("time"));
					value.put("REMOTE_IP"								, list.get(i).get("remote_ip"));
					value.put("REMOTE_OS"							, list.get(i).get("remote_os"));
					value.put("REMOTE_BROWSER"					, list.get(i).get("remote_browser"));
					value.put("TARGET_USER"							, list.get(i).get("target_user"));
					value.put("TARGET_NM"							, list.get(i).get("target_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get User Login Info List Data Error : {} ", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getUserLoginInfoListByExcel ( String yyyymm, String gubn ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"										, loginSessionInfoFactory.getObject().getAcctID());
		map.put("USER_ID"										, loginSessionInfoFactory.getObject().getUserId());
		map.put("YYYYMM"										, yyyymm);
		map.put("GUBN"											, gubn);
		try{
			logger.info("Get User Login Info By Excel Data Start");
			list = getSqlSession().selectList("getUserLoginInfoListByExcel", map);
			logger.info("Get User Login Info By Excel Data End");
			if(list.size()>0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("USER_ID"									, list.get(i).get("user_id"));
					value.put("USER_NM"								, list.get(i).get("user_nm"));
					value.put("LOG_INFO"								, list.get(i).get("log_info"));
					value.put("TIME"										, list.get(i).get("time"));
					value.put("REMOTE_IP"								, list.get(i).get("remote_ip"));
					value.put("REMOTE_OS"							, list.get(i).get("remote_os"));
					value.put("REMOTE_BROWSER"					, list.get(i).get("remote_browser"));
					value.put("TARGET_USER"							, list.get(i).get("target_user"));
					value.put("TARGET_NM"							, list.get(i).get("target_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get User Login Info Data By Excel Error : {} ",e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
}
