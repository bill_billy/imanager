package com.iplanbiz.iportal.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.core.session.LoginSessionInfo;

@Repository
public class MonthConnectionDAO extends SqlSessionDaoSupport {
	
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	public JSONArray getMonthUserYear() throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		try{
			logger.info("Get Month Year Data Start");
			list = getSqlSession().selectList("getMonthUserYear");
			logger.info("Get Month Year Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("COM_COD"							, list.get(i).get("yyyy_cod"));
					value.put("COM_NM"							, list.get(i).get("yyyy_nm"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Month Year Data Error : {}" , e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMonthUserConnectionTotal(String year) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		map.put("YEAR"										, Integer.valueOf(year));
		try{
			logger.info("Get Month User Connection Total Data Start");
			list = getSqlSession().selectList("getMonthUserConnectionTotal", map);
			logger.info("Get Month User Connection Total Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("JAN"												, list.get(i).get("jan"));
					value.put("FEB"												, list.get(i).get("feb"));
					value.put("MAR"												, list.get(i).get("mar"));
					value.put("APR"												, list.get(i).get("apr"));
					value.put("MAY"												, list.get(i).get("may"));
					value.put("JUN"												, list.get(i).get("jun"));
					value.put("JUL"												, list.get(i).get("jul"));
					value.put("AUG"												, list.get(i).get("aug"));
					value.put("SEP"												, list.get(i).get("sep"));
					value.put("OCT"												, list.get(i).get("oct"));
					value.put("NOV"												, list.get(i).get("nov"));
					value.put("DEC"												, list.get(i).get("dec"));
					value.put("TOTAL"											, list.get(i).get("total"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Month User Connection Total Data Error : {}" ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMonthUserConnectionList(String year) throws Exception{
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("ACCT_ID"									, loginSessionInfoFactory.getObject().getAcctID());
		map.put("YEAR"										, Integer.valueOf(year));
		try{
			logger.info("Get Month User Connection List Data Start");
			list = getSqlSession().selectList("getMonthUserConnectionList", map);
			logger.info("Get Month User Connection List Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("RNUM"											, list.get(i).get("rnum"));
					value.put("EMP_ID"											, list.get(i).get("emp_id"));
					value.put("EMP_NM"										, list.get(i).get("emp_nm"));
					value.put("USER_GRP_NM"								, list.get(i).get("user_grp_nm"));
					value.put("JAN"												, list.get(i).get("jan"));
					value.put("FEB"												, list.get(i).get("feb"));
					value.put("MAR"												, list.get(i).get("mar"));
					value.put("APR"												, list.get(i).get("apr"));
					value.put("MAY"												, list.get(i).get("may"));
					value.put("JUN"												, list.get(i).get("jun"));
					value.put("JUL"												, list.get(i).get("jul"));
					value.put("AUG"												, list.get(i).get("aug"));
					value.put("SEP"												, list.get(i).get("sep"));
					value.put("OCT"												, list.get(i).get("oct"));
					value.put("NOV"												, list.get(i).get("nov"));
					value.put("DEC"												, list.get(i).get("dec"));
					value.put("TOTAL"											, list.get(i).get("total"));
					returnValue.add(value);
				}
			}
		}catch(Exception e){
			logger.error("Get Month User Connection Data Error : {}", e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
}
