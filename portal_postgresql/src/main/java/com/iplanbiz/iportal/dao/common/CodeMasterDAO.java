package com.iplanbiz.iportal.dao.common;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.dao.system.CustomSqlHouseDAO;

@Repository
public class CodeMasterDAO extends SqlSessionDaoSupport{
	@Autowired SqlSessionFactory sqlSessionFactory;
	@Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired DBMSDAO dbmsDAO;
	@Autowired CustomSqlHouseDAO customSqlHouseDAO; 
	
	public JSONArray getGroupCodeList(String searchText) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("searchText"										, searchText);
	
		try{
			logger.info("getGroupCodeList Data Start");
			list = getSqlSession().selectList("getCommonGroupCodeList", map);
			logger.info("getGroupCodeList Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("COM_GRP_CD"									, list.get(i).get("com_grp_cd"));
					value.put("COM_GRP_CD_NM"									, list.get(i).get("com_grp_cd_nm"));
					value.put("COM_CD_NUM"									, list.get(i).get("com_cd_num"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("getGroupCodeList Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public JSONArray getCodeList(String gCd) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<HashMap<String, Object>> list = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("COM_GRP_CD"												, gCd);
	
		try{
			logger.info("getCodeList Data Start");
			list = getSqlSession().selectList("getCommonCodeList", map);
			logger.info("getCodeList Data End");
			if(list.size() > 0){
				for(int i=0;i<list.size();i++){
					JSONObject value = new JSONObject();
					value.put("COM_GRP_CD"									, list.get(i).get("com_grp_cd"));
					value.put("COM_CD"									, list.get(i).get("com_cd"));
					value.put("COM_CD_NM"									, list.get(i).get("com_cd_nm"));
					value.put("SORT_SEQ"									, list.get(i).get("sort_seq"));
					value.put("USE_YN"									, list.get(i).get("use_yn"));
					value.put("COM_CD_DESC"									, list.get(i).get("com_cd_desc"));
					returnValue.add(value);
				}
			}
		}catch(Exception e) {
			logger.error("getCodeList Data Error : {} " ,e);
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
	public int insertGroupCode(String gCd, String gCdNm, String ac) {
		int result = 0;
		LinkedHashMap<String,Object> param = new LinkedHashMap<String,Object>(); 
		param.put("COM_GRP_CD"												, gCd);
		param.put("COM_GRP_CD_NM"											, gCdNm);
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("COM_GRP_CD"											, gCd);
		map.put("COM_GRP_CD_NM"											, gCdNm);
	
		if(ac.equals("INSERT")) {
			int check =  getSqlSession().selectOne("checkGroupCode",map);
			if(check>0) {//중복
				result=3;
			}else {
				try {
					param.put("INS_ID", loginSessionInfoFactory.getObject().getUserId());
					customSqlHouseDAO.updateCustomQuery("CodeMasterDAO.insertGroupCode",param);
					result=1;
					//map.put("INS_ID", loginSessionInfoFactory.getObject().getUserId());
					//result = getSqlSession().insert("insertGroupCode",map);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					result=-3;
				}
			}
		}else if(ac.equals("UPDATE")) {
			try {
				param.put("UPDR_ID", loginSessionInfoFactory.getObject().getUserId());
				customSqlHouseDAO.updateCustomQuery("CodeMasterDAO.updateGroupCode",param);
				result=2;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result=-3;
			}
			//map.put("UPDR_ID", loginSessionInfoFactory.getObject().getUserId());
			//result = getSqlSession().insert("updateGroupCode",map);
			//result=2;
		}
		return result;
	}
	public int insertCommonCode(String gCd, String cCd, String cCdNm, String useYn, int sortSeq, String cDesc, String ac) {
		int result = 0;
		LinkedHashMap<String,Object> param = new LinkedHashMap<String,Object>(); 
		param.put("COM_GRP_CD"											, gCd);
		param.put("COM_CD"												, cCd);
		param.put("COM_CD_NM"												, cCdNm);
		param.put("SORT_SEQ"												, sortSeq);
		param.put("USE_YN"												, useYn);
		param.put("COM_CD_DESC"											, cDesc);
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("COM_GRP_CD"											, gCd);
		map.put("COM_CD"												, cCd);
		map.put("COM_CD_NM"												, cCdNm);
		map.put("SORT_SEQ"												, sortSeq);
		map.put("USE_YN"												, useYn);
		map.put("COM_CD_DESC"											, cDesc);
		if(ac.equals("INSERT")) {
			int check =  getSqlSession().selectOne("checkCommonCode",map);
			if(check>0) {
				result=3;//중복코드
			}else {
				try {
					param.put("INS_ID", loginSessionInfoFactory.getObject().getUserId());
					customSqlHouseDAO.updateCustomQuery("CodeMasterDAO.insertCommonCode",param);
					result=1;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					result=-3;
				}
			//	map.put("INS_ID", loginSessionInfoFactory.getObject().getUserId());
			//	result = getSqlSession().insert("insertCommonCode",map);
			}
		}else if(ac.equals("UPDATE")) {
			try {
				param.put("UPDR_ID", loginSessionInfoFactory.getObject().getUserId());
				customSqlHouseDAO.updateCustomQuery("CodeMasterDAO.updateCommonCode",param);
				result=2;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result=-3;
			}
		//	map.put("UPDR_ID", loginSessionInfoFactory.getObject().getUserId());
		//	result = getSqlSession().update("updateCommonCode",map);
		//	result=2;
		}
		return result;
	}
	public int deleteGroupCode(String gCd) {
		int result = 0;
		LinkedHashMap<String,Object> param = new LinkedHashMap<String,Object>(); 
		param.put("COM_GRP_CD"											, gCd);
		try {
			customSqlHouseDAO.updateCustomQuery("CodeMasterDAO.deleteCommonByGroupCode",param);
			customSqlHouseDAO.updateCustomQuery("CodeMasterDAO.deleteGroupCode",param);
			result=1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result=-3;
		}
		
	/*	
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("COM_GRP_CD"											, gCd);
		
		result = getSqlSession().delete("deleteCommonByGroupCode",map);
		if(result>=0)
			result = getSqlSession().delete("deleteGroupCode",map);*/
		return result;
	}
	public int deleteCommonCode(String gCd, String cCd) {
		int result = 0;
		LinkedHashMap<String,Object> param = new LinkedHashMap<String,Object>(); 
		param.put("COM_GRP_CD"											, gCd);
		param.put("COM_CD"											, cCd);
		try {
			customSqlHouseDAO.updateCustomQuery("CodeMasterDAO.deleteCommonCode",param);
			result=1;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result=-3;
		}
	/*	HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("COM_GRP_CD"											, gCd);
		map.put("COM_CD"											, cCd);
		result = getSqlSession().delete("deleteCommonCode",map);*/
		return result;
	}
}
