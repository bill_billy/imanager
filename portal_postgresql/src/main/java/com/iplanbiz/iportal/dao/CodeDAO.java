package com.iplanbiz.iportal.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class CodeDAO extends SqlSessionDaoSupport {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public JSONArray getCodeListByAsc(String comlCod) throws Exception{
		JSONArray resultValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("COML_COD"									, comlCod);
			listValue = getSqlSession().selectList("getCodeListByAsc", map);
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("COM_COD"						, listValue.get(i).get("com_cod"));
					putValue.put("COM_NM"							, listValue.get(i).get("com_nm"));
					resultValue.add(putValue);
				}
			}
		}catch(Exception e){
			resultValue = null;
			logger.error("ERROR : {}" , e);
			throw e;
		}
		return resultValue;
	}
	public JSONArray getCodeListByDesc(String comlCod) throws Exception{
		JSONArray resultValue = new JSONArray();
		List<HashMap<String, Object>> listValue = null;
		try{
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("COML_COD"									, comlCod);
			listValue = getSqlSession().selectList("getCodeListByDesc", map);
			if(listValue.size() > 0){
				for(int i=0;i<listValue.size();i++){
					JSONObject putValue = new JSONObject();
					putValue.put("COM_COD"						, listValue.get(i).get("com_cod"));
					putValue.put("COM_NM"							, listValue.get(i).get("com_nm"));
					resultValue.add(putValue);
				}
			}
		}catch(Exception e){
			resultValue = null;
			logger.error("ERROR : {}" , e);
			throw e;
		}
		return resultValue;
	}
	 
}
