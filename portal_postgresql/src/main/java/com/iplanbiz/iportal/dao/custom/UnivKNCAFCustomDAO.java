package com.iplanbiz.iportal.dao.custom;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.DBMSService;
import com.iplanbiz.iportal.dao.CustomDBDataDAO;
import com.iplanbiz.iportal.service.CustomDBDataService;

@Repository
public class UnivKNCAFCustomDAO extends SqlSessionDaoSupport {
	
	@Autowired
	DBMSService dbmsService;
	
	@Autowired
	CustomDBDataDAO customDAO;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired SqlSessionFactory sqlSessionFactory;
	
	
	public JSONArray getMainDashboardKpiList () throws Exception {
		JSONArray returnValue = new JSONArray();
		List<LinkedHashMap<String, Object>> list = null;
		
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		
		try {
			logger.info("Get KNCAF Main Dash Board List Data Start");
			list = customDAO.customSqlHouse("UnivKNCAF.getKpiList", "1", map);
			logger.info("Get KNCAF Main Dash Board List Data End");
			if(list.size() > 0) {
				for(int i=0; i<list.size(); i++) {
					JSONObject value = new JSONObject();
					value.put("COM_COD"							, list.get(i).get("COM_COD"));
					value.put("COM_NM"							, list.get(i).get("COM_NM"));
					
					returnValue.add(value);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Get KNCAF Main Dash Board List Data Error : {}", e.getMessage());
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMainDashboardKpiTarget ( String scID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<LinkedHashMap<String, Object>> list = null;
		
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		
		map.put("S_SC_ID"											, scID);
		String[] column = {"SQL_DATA"};
		
		try {
			logger.info("Get Main Dash Board Kpi Target Data Start");
			list = customDAO.customSqlHouse("UnivKNCAF.getKpiTarget", "1", map);
			logger.info("Get Main Dash Board Kpi Target Data End");
			logger.info("@@@@@@@@@listSize = " + list.size());
			if(list.size() > 0) {
				logger.info("listSize = " + list.size());
				for(int i=0;i<list.size();i++) {
					JSONObject value = new JSONObject();
					value.put("UNIT_NM"							, list.get(i).get("UNIT_NM"));
					value.put("KPI_ID"							, list.get(i).get("KPI_ID"));
					value.put("KPI_NM"							, list.get(i).get("KPI_NM"));
					value.put("KPI_DESC"						, list.get(i).get("KPI_DESC"));
					value.put("ETC_DESC"						, list.get(i).get("ETC_DESC"));
					value.put("KPI_GBN"							, list.get(i).get("KPI_GBN"));
					value.put("DATA_START_DAT"					, list.get(i).get("DATA_START_DAT"));
					value.put("DATA_END_DAT"					, list.get(i).get("DATA_END_DAT"));
					value.put("OWNER_USER_ID"					, list.get(i).get("OWNER_USER_ID"));
					value.put("MEAS_CYCLE"						, list.get(i).get("MEAS_CYCLE"));
					value.put("COL_SYSTEM"						, list.get(i).get("COL_SYSTEM"));
					value.put("USE_UNIT"						, list.get(i).get("USE_UNIT"));
					value.put("DECIMAL_PLACE"					, list.get(i).get("DECIMAL_PLACE"));
					value.put("SORT_ORDER"						, list.get(i).get("SORT_ORDER"));
					value.put("START_DAT"						, list.get(i).get("START_DAT"));
					value.put("END_DAT"							, list.get(i).get("END_DAT"));
					value.put("KPI_UDC1"						, list.get(i).get("KPI_UDC1"));
					value.put("KPI_UDC2"						, list.get(i).get("KPI_UDC2"));
					value.put("KPI_UDC3"						, list.get(i).get("KPI_UDC3"));
					value.put("KPI_UDC4"						, list.get(i).get("KPI_UDC4"));
					value.put("KPI_UDC5"						, list.get(i).get("KPI_UDC5"));
					value.put("KPI_DIR"							, list.get(i).get("KPI_DIR"));
					value.put("USE_YN"							, list.get(i).get("USE_YN"));
					value.put("SQL_DATA"						, list.get(i).get("SQL_DATA"));
					value.put("PROG_NM"							, list.get(i).get("PROG_NM"));
					value.put("PROG_LINK"						, list.get(i).get("PROG_LINK"));
					value.put("C_ID"							, list.get(i).get("C_ID"));
					returnValue.add(value);
				}
				logger.info("ReturnValue = " + returnValue);
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("ERROR : Get Main Dash Board Kpi Target Data Error : {}", e.getMessage());
			throw e;
		}
		return returnValue;
	}
	public JSONArray getMainDashbaordSqlDATA ( String scID, String kpiID ) throws Exception {
		JSONArray returnValue = new JSONArray();
		List<LinkedHashMap<String, Object>> list = null;
		
		LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>();
		
		map.put("S_SC_ID"										, scID);
		map.put("S_KPI_ID"										, kpiID);
		
	    String[] column = {"SQL_DATA"};
		try {
			list = customDAO.customSqlHouse("UnivKNCAF.getSQLData", "1", map);
			if(list.size() > 0) {
				LinkedHashMap<String, Object> param = new LinkedHashMap<String, Object>();
				param.put("KPI_ID"								, kpiID);
				String query = list.get(0).get("SQL_DATA").toString();
				if(!query.equals("")) {
					logger.info("SQL DATA = " + query);
					List<LinkedHashMap<String, Object>> returnList = dbmsService.executeQuery(query, param, "LOGIN_TIBERO");
					if(returnList.size() > 0) {
						for(int i=0; i<returnList.size(); i++) {
							JSONObject value = new JSONObject();
							value.put("KPI_ID"								, returnList.get(i).get("KPI_ID"));
							value.put("YYYY"								, returnList.get(i).get("YYYY"));
							value.put("COL1"								, returnList.get(i).get("COL1"));
							value.put("COL2"								, returnList.get(i).get("COL2"));
							value.put("VAL"									, returnList.get(i).get("VAL"));
							value.put("TGT_VALUE"							, returnList.get(i).get("TGT_VALUE"));
							value.put("RATE"								, returnList.get(i).get("RATE"));
							returnValue.add(value);
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("ERROR : Get Main Dash Board SQL Data Error : {}", e.getMessage());
			e.printStackTrace();
			throw e;
		}
		return returnValue;
	}
}
