/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.iportal.comm.session;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.service.system.SessionService;

/**
 * @author : 장민수(msjang@iplanbiz.co.kr)
 * @date   : 2015. 7. 3.
 * @desc   : 
 */
public class SessionManager {
	
	 private  Logger  logger = LoggerFactory.getLogger(SessionManager.class); 
	 @Autowired SessionService sessionService;
	 
	 SessionManager(){
		 logger.info("Session Manager();");
	 }
	  
	 public  int addLoginSessionInfo(String userID, String acct_id,HttpServletRequest request,LoginSessionInfo sessionInfo){
		
		 try {
			 sessionService.addSession(request.getSession());
		 }catch(Exception e) {
			 logger.error("error",e);
			 return 0;
		 }
		return 1;
		 
	 }
	 public  int dropSession(HttpSession session){
			
		 try {
			 sessionService.dropSession(session);
		 }catch(Exception e) {
			 logger.error("error",e);
			 return 0;
		 }
		return 1;
		 
	 }
	 
	 public  boolean isDuplicateLogOn(String userID, String acct_id,HttpServletRequest request){
		 try {
			 if(sessionService.countDuplicationSession(request.getSession())==0) return false;
			 else return true;
		 }
		 catch(Exception e) {
			 e.printStackTrace();
			 return false;
		 } 
		 
	 }
	 public  void removeLoginSessionInfo(String userID,String acctID,HttpSession session){
		try {
			this.sessionService.dropSession(session);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 } 
	
	 public void printCurrentSessionList(){
		 
	 }
	 public String getClientIP(HttpServletRequest request) {

	     String ip = request.getHeader("X-FORWARDED-FOR"); 
	     
	     if (ip == null || ip.length() == 0) {
	         ip = request.getHeader("Proxy-Client-IP");
	     }

	     if (ip == null || ip.length() == 0) {
	         ip = request.getHeader("WL-Proxy-Client-IP");  // 웹로직
	     }

	     if (ip == null || ip.length() == 0) {
	         ip = request.getRemoteAddr() ;
	     } 
	     return ip; 
	 }
}
