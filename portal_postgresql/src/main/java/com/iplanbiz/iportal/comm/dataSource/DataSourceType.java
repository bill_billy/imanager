/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.iportal.comm.dataSource;

/**
 * @author : woon sik Shin(sky3473@iplanbiz.co.kr or gmail.com)
 * @date   : 2012. 7. 24.
 * @desc   : 
 */
public enum DataSourceType {
	PORTAL, COGNOS, AUDIT, MATRIX, LOGIN
}
