package com.iplanbiz.iportal.comm.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognos.developer.schemas.bibus._3.BaseClass;
import com.iplanbiz.iportal.comm.cognos.CRNConnect;
import com.iplanbiz.iportal.comm.cognos.GroupsAndRoles;

public class GroupsAndRolesUtil {
	static GroupsAndRoles gar = new GroupsAndRoles();
	public static String makeSecurityPath(String source, String namespace, String sourceType){
		StringBuffer sourceName = new StringBuffer("CAMID(\"");
		
		if(namespace.equals("cognos")){
			if(sourceType.equals("r")){
				sourceName.append(namespace+":");
				sourceName.append("r:");
			}else {
				sourceName.append(":");
			}
			
		}else{
			sourceName.append(namespace+":");
			if(sourceType.equals("u")){
				sourceName.append("u:");
			}else if(sourceType.equals("g")){
				sourceName.append("g:");
			}else if(sourceType.equals("r")){
				sourceName.append("r:");
			}
		}
		sourceName.append(source).append("\")");
		return sourceName.toString();
	}
	
	public static String makeSecurityNamespacePath(String namespace){
		
		if(namespace.equals("cognos")){
			namespace = "CAMID(\":\")";
		}else{
			namespace = "CAMID(\""+namespace+"\")";
		}

		return namespace;
	}
	
	public static BaseClass getSecurityObject(CRNConnect connection, String selectedNamespace, String theName, String type){
		BaseClass[] results = null;
		if(type.equals("u")){
			results = gar.getAvailableMembers(connection, theName);
		}else if(type.equals("g")){
			results = gar.getAvailableGroups(connection, selectedNamespace);
		}else if(type.equals("r")){
			results = gar.getAvailableRoles(connection, selectedNamespace);
		}
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ length" + results.length);
		if(results.length == 0){
			return null;
		}
		
		String[] searchPath = new String[results.length];
//		String[] defaultName = new String[results.length];
		
//		logger.info("getSecurityObject results for start");
//		logger.info("getSecurityObject results length: {}", results.length);
		for(int i=0; i<results.length; i++){
			searchPath[i] = results[i].getSearchPath().getValue();
// bonnie 임시 테스트로 인한 주석 처리
//			defaultName[i] = results[i].getDefaultName().getValue();
// bonnie 임시 테스트로 인한 추가 부문 Start			
			if(((String) theName).compareToIgnoreCase(searchPath[i]) == 0){
				return results[i];
			}
// bonnie End			
		}
//		logger.info("getSecurityObject result for end");
		
//		logger.info("getSecurityObject defaultName for start");

// bonnie 임시 테스트로 인한 주석 처리		
//		for(int i=0; i<defaultName.length;i++){

////			System.out.println("searchPath :: "+searchPath[i]+", defaultName"+defaultName[i]);
//			if(((String) theName).compareToIgnoreCase(searchPath[i]) == 0){
////				System.out.println("searchPath :: "+searchPath[i]);
//				return results[i];
//			}
//		}
//		logger.info("getSecurityObject defaultName for end");
		return null;
	}
}
