/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.iportal.comm.dataSource;

/**
 * @author : 장민수(msjang@iplanbiz.co.kr)
 * @date   : 2015. 2. 9.
 * @desc   : 
 */
public class IplanbizDataSource extends org.apache.commons.dbcp.BasicDataSource{
	private String comment;

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}
}
