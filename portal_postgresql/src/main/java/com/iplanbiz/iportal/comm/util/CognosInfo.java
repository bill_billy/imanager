/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.iportal.comm.util;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author : yun hee Kim(yhkim@khantech.co.kr)
 * @date   : 2012. 7. 12.
 * @desc   : 
 */

public class CognosInfo {
	
	   private static Logger logger = LoggerFactory.getLogger(CognosInfo.class);
	   private static Properties props = null;
	    
		static{
			String propsResource = "";
	    	props = new Properties();
		    try{
		    	propsResource = "cognosConfig.properties";
		    	props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(propsResource));
		
		    	StringBuffer logStr = new StringBuffer();
		    	Iterator iter = props.keySet().iterator();
//		    	logger.info("====>>> {} Open!!", propsResource);
		    	
			    while(iter.hasNext()){
			    	Object key = iter.next();
			    	Object val = props.get(key);
//			    	logger.info("====>>> * {}  = {}", key, val);
			    }
			    
			    logger.debug("====>>> {} Close!!", propsResource);
		    }catch(Exception e){
		    	logger.error("===>>>> Load {} Fail: {}", propsResource, e.toString());
		    }	
	    }
		public static String getAddMyFolderOnTop() { 
			return props.getProperty("cognos.info.addmyfolderontop");
		}
		public static String getConnect() {
			return props.getProperty("cognos.info.connect");
		}

		public static String getDispatch() {
			return props.getProperty("cognos.info.dispatch");
		}

		public static String getNamespace() {
			return props.getProperty("cognos.info.namespace");
		}

		public static String getReport() {
			return props.getProperty("cognos.info.report");
		}

		public static String getReportMid() {
			return props.getProperty("cognos.info.reportMid");
		}

		public static String getReportLast() {
			return props.getProperty("cognos.info.reportLast");
		}
		public static String getExploration() {
			return props.getProperty("cognos.info.exploration");
		}
		public static String getExplorationMid() {
			return props.getProperty("cognos.info.explorationMid");
		}
		public static String getExplorationLast() {
			return props.getProperty("cognos.info.explorationLast");
		}
		public static String getExplorationSet() {
			return props.getProperty("cognos.info.explorationSet");
		}
		public static String getExplorationSetMid() {
			return props.getProperty("cognos.info.explorationSetMid");
		}
		public static String getExplorationSetLast() {
			return props.getProperty("cognos.info.explorationSetLast");
		}
		public static String getModule() {
			return props.getProperty("cognos.info.module");
		}
		public static String getModuleLast() {
			return props.getProperty("cognos.info.moduleLast");
		}
		public static String getModuleSet() {
			return props.getProperty("cognos.info.moduleSet");
		}
		public static String getModuleSetLast() {
			return props.getProperty("cognos.info.moduleSetLast");
		}
		public static String getQuery() {
			return props.getProperty("cognos.info.query");
		}

		public static String getQueryLast() {
			return props.getProperty("cognos.info.queryLast");
		}

		public static String getAnalysis() {
			return props.getProperty("cognos.info.analysis");
		}

		public static String getCognosViewer() {
			return props.getProperty("cognos.info.cognosViewer");
		}

		public static String getCognosViewerMid() {
			return props.getProperty("cognos.info.cognosViewerMid");
		}

		public static String getCognosViewerLast() {
			return props.getProperty("cognos.info.cognosViewerLast");
		}
		
		public static String getUrl() {
			return props.getProperty("cognos.info.url");
		}

		public static String getReportView() {
			return props.getProperty("cognos.info.reportView");
		}
		
		public static String getReportViewLast() {
			return props.getProperty("cognos.info.reportViewLast");
		}

		public static String getPagelet() {
			return props.getProperty("cognos.info.pagelet");
		}

		public static String getPageletLast() {
			return props.getProperty("cognos.info.pageletLast");
		}

		public static String getSavedReport() {
			return props.getProperty("cognos.info.savedReport");
		}

		public static String getSavedReportMid() {
			return props.getProperty("cognos.info.savedReportMid");
		}

		public static String getSavedReportLast() {
			return props.getProperty("cognos.info.savedReportLast");
		}

		public static String getExpressMode() {
			return props.getProperty("cognos.info.expressMode");
		}

		public static String getShortcut() {
			return props.getProperty("cognos.info.shortcut");
		}

		public static String getPowerPlay10Report() {
			return props.getProperty("cognos.info.powerPlay10Report");
		}

		public static String getDocument() {
			return props.getProperty("cognos.info.document");
		}
		public static String getBIAMenuName(){
			return props.getProperty("cognos.info.BIAMenuName");
			/*
			try {
				return new String(props.getProperty("cognos.info.BIAMenuName").getBytes("ISO-8859-1"),"UTF-8");
			} catch (UnsupportedEncodingException e) 
			{	
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "";
				
			}
			*/
		}

		public static String getDocumentLast() {
			return props.getProperty("cognos.info.documentLast");
		}

		public static String getReportVersion() {
			return props.getProperty("cognos.info.reportVersion");
		}

		public static String getReportVersionLast() {
			return props.getProperty("cognos.info.reportVersionLast");
		}

		public static String getDashboard() {
			return props.getProperty("cognos.info.dashboard");
		}

		public static String getDashboardLast() {
			return props.getProperty("cognos.info.dashboardLast");
		}

		public static String getInteractiveReport() {
			return props.getProperty("cognos.info.interactiveReport");
		}

		public static String getInteractiveReportMid() {
			return props.getProperty("cognos.info.interactiveReportMid");
		}

		public static String getInteractiveReportLast() {
			return props.getProperty("cognos.info.interactiveReportLast");
		}
		
		public static String getInteractiveReportIe8() {
			return props.getProperty("cognos.info.interactiveReport.ie8");
		}

		public static String getInteractiveReportMidIe8() {
			return props.getProperty("cognos.info.interactiveReportMid.ie8");
		}

		public static String getInteractiveReportLastIe8() {
			return props.getProperty("cognos.info.interactiveReportLast.ie8");
		}
		
		public static String getRoot_name() {
			return props.getProperty("cognos.info.rootName");
		}
		
		public static String getCogRep() {
			return props.getProperty("cognos.info.cogrep");
		}
		
		public static String getBusinessInsight() {
			return props.getProperty("cognos.info.businessInsight");
		}
		
		public static String getReportInsight() {
			return props.getProperty("cognos.info.reportInsight");
		}
		
		public static String getReportInsightAndRS() {
			return props.getProperty("cognos.info.reportInsightAndRS");
		}
		public static String getDocumentForDocx() {
			return props.getProperty("cognos.info.documentForDocx");
		}
		public static String getDocumentForDoc() {
			return props.getProperty("cognos.info.documentForDoc");
		}
		
}
