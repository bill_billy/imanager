package com.iplanbiz.iportal.comm.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.rmi.RemoteException;

import org.apache.axis.client.Stub;
import org.apache.axis.message.SOAPHeaderElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognos.developer.schemas.bibus._3.Account;
import com.cognos.developer.schemas.bibus._3.AddOptions;
import com.cognos.developer.schemas.bibus._3.Analysis;
import com.cognos.developer.schemas.bibus._3.BaseAgentDefinitionActionEnum;
import com.cognos.developer.schemas.bibus._3.BaseClass;
import com.cognos.developer.schemas.bibus._3.BiBusHeader;
import com.cognos.developer.schemas.bibus._3.ConfigurationData;
import com.cognos.developer.schemas.bibus._3.ConfigurationDataEnum;
import com.cognos.developer.schemas.bibus._3.Dashboard;
import com.cognos.developer.schemas.bibus._3.Document;
import com.cognos.developer.schemas.bibus._3.Exploration;
import com.cognos.developer.schemas.bibus._3.InteractiveReport;
import com.cognos.developer.schemas.bibus._3.Locale;
import com.cognos.developer.schemas.bibus._3.Module;
import com.cognos.developer.schemas.bibus._3.OrderEnum;
import com.cognos.developer.schemas.bibus._3.Pagelet;
import com.cognos.developer.schemas.bibus._3.PropEnum;
import com.cognos.developer.schemas.bibus._3.Query;
import com.cognos.developer.schemas.bibus._3.QueryOptions;
import com.cognos.developer.schemas.bibus._3.Report;
import com.cognos.developer.schemas.bibus._3.ReportView;
import com.cognos.developer.schemas.bibus._3.Role;
import com.cognos.developer.schemas.bibus._3.SearchPathMultipleObject;
import com.cognos.developer.schemas.bibus._3.SearchPathSingleObject;
import com.cognos.developer.schemas.bibus._3.Shortcut;
import com.cognos.developer.schemas.bibus._3.Sort;
import com.cognos.developer.schemas.bibus._3.URL;
import com.cognos.developer.schemas.bibus._3.UpdateActionEnum;
import com.iplanbiz.comm.Utils;
import com.iplanbiz.iportal.comm.cognos.BIBusHeaderHelper;
import com.iplanbiz.iportal.comm.cognos.CRNConnect;

public abstract class CognosUtil {
	public static  Logger logger = LoggerFactory.getLogger(CognosUtil.class);
	
	 
	public static String getReportURL(final BaseClass bc, CRNConnect connect, boolean isView, boolean isIE8) throws RemoteException, NullPointerException {
		String passport = getPassport(connect);
		StringBuffer reportURL = new StringBuffer();
		PropEnum[] properties = { PropEnum.defaultName,	PropEnum.searchPath, PropEnum.storeID};
		
		Sort[] sortBy = getSort();
		QueryOptions options = new QueryOptions();
		
		boolean isURL = false;
		if (bc instanceof Report) {
			BaseClass[] reportVersionBc;
			// savedReport가 있는지 확인한다.
			reportVersionBc = connect.getCMService(passport).query(new SearchPathMultipleObject(bc.getSearchPath().getValue()+ "/reportVersion"), properties,sortBy, options);
			if (reportVersionBc.length > 0) {
				BaseClass[] savedReport =  connect.getCMService(passport).query(new SearchPathMultipleObject(reportVersionBc[0].getSearchPath().getValue()+ "/output"), properties,	sortBy, options);
				if (savedReport.length > 0) {
					reportURL.append(CognosInfo.getSavedReport());
					reportURL.append(Utils.getEncodingValue(bc.getSearchPath().getValue()));
					reportURL.append(CognosInfo.getSavedReportMid());
					reportURL.append(Utils.getEncodingValue(bc.getDefaultName().getValue()));
					reportURL.append(CognosInfo.getSavedReportLast());
			    }else{
			    	//일반 report
	    			reportURL.append(CognosInfo.getReport());
			    	reportURL.append(Utils.getEncodingValue(bc.getSearchPath().getValue()));
					reportURL.append(CognosInfo.getReportMid());
					reportURL.append(Utils.getEncodingValue(bc.getDefaultName().getValue()));
					reportURL.append(CognosInfo.getReportLast());
			    }
			}else{
				Report report = (Report) bc;
				
				boolean isBIA = false;
				if(CognosInfo.getBIAMenuName()!=null){
					String[] biaMenus = CognosInfo.getBIAMenuName().split(",");
					
					for(int i = 0; i < biaMenus.length;i++){
						
						String biaMenuItem = biaMenus[i].trim();
						if(biaMenuItem.equals("")==false&&bc.getSearchPath().getValue().indexOf(biaMenuItem) > -1){
							isBIA = true;
							break;
						}
						String checkBIA = "[@name='"+biaMenuItem+"']";
						logger.info("bc.getSearchPath() : {}",bc.getSearchPath().getValue());
						logger.info("checkBIA : {}",checkBIA);
						
					}
				}
				
				if(report.getDefaultPortalAction() != null ){
					logger.info("BaseAgentDefinitionActionEnum._edit.compareToIgnoreCase(report.getDefaultPortalAction().getValue().toString()) == 0 : {}",(BaseAgentDefinitionActionEnum._edit.compareToIgnoreCase(report.getDefaultPortalAction().getValue().toString()) == 0));
				   	if(BaseAgentDefinitionActionEnum._edit.compareToIgnoreCase(report.getDefaultPortalAction().getValue().toString()) == 0){
			    		reportURL.append(CognosInfo.getReportInsightAndRS());
				   	}else{
				   		logger.info("bc.getSearchPath().getValue() : {}",bc.getSearchPath().getValue());
				   		logger.info("isBIA : {}",isBIA);
				   		if(isBIA){
				   			reportURL.append(CognosInfo.getReportInsight());
			   			}else{
			   				reportURL.append(CognosInfo.getReport());
			   			}
				   	}		
		   		}else{
		   			logger.info("isBIA : {}",isBIA);
		   			if(isBIA){
						reportURL.append(CognosInfo.getReportInsight());
		   			}else{
		   				reportURL.append(CognosInfo.getReport());
		   			}
	    		}

//				if(bc.getSearchPath().getValue().indexOf("내 폴더") > -1){
					reportURL.append(Utils.getEncodingValue("storeID('" + report.getStoreID().getValue() + "')"));
//				}else{
//					reportURL.append(Utils.getEncodingValue(bc.getSearchPath().getValue()));
//				}
				reportURL.append(CognosInfo.getReportMid());
				reportURL.append(Utils.getEncodingValue(bc.getDefaultName().getValue()));
				reportURL.append(CognosInfo.getReportLast());
			}

		} else if (bc instanceof ReportView) {
			// 레포트 뷰
//			reportURL.append(CognosInfo.getReportView());
//			reportURL.append(Utils.getEncodingValue(bc.getSearchPath().getValue()));
			reportURL.append(CognosInfo.getCognosViewer());
			reportURL.append(Utils.getEncodingValue(bc.getSearchPath().getValue()));
			reportURL.append(CognosInfo.getCognosViewerMid());
			reportURL.append(Utils.getEncodingValue(bc.getDefaultName().getValue()));
			reportURL.append(CognosInfo.getCognosViewerLast());

		} else if (bc instanceof Analysis) {
			
			if(isView == true){
				reportURL.append(CognosInfo.getCognosViewer());
				reportURL.append(Utils.getEncodingValue(bc.getSearchPath().getValue()));
				reportURL.append(CognosInfo.getCognosViewerMid());
				reportURL.append(Utils.getEncodingValue(bc.getDefaultName().getValue()));
				reportURL.append(CognosInfo.getCognosViewerLast());
			}else{
				reportURL.append(CognosInfo.getAnalysis());
				reportURL.append(Utils.getEncodingValue(bc.getSearchPath().getValue()));
			}
			
		} else if (bc instanceof Query) {
			if(isView == true){
				reportURL.append(CognosInfo.getCognosViewer());
				reportURL.append(Utils.getEncodingValue(bc.getSearchPath().getValue()));
				reportURL.append(CognosInfo.getCognosViewerMid());
				reportURL.append(Utils.getEncodingValue(bc.getDefaultName().getValue()));
				reportURL.append(CognosInfo.getCognosViewerLast());
			}else{
				reportURL.append(CognosInfo.getQuery());
				reportURL.append(Utils.getEncodingValue(bc.getSearchPath().getValue()));
				reportURL.append(CognosInfo.getQueryLast());
			}
		}else if (bc instanceof InteractiveReport) {
			if(isIE8){
				reportURL.append(CognosInfo.getInteractiveReportIe8());
				reportURL.append(Utils.getEncodingValue(bc.getSearchPath().getValue()));
				reportURL.append(CognosInfo.getInteractiveReportMidIe8());
				reportURL.append(Utils.getEncodingValue(bc.getDefaultName().getValue()));
				reportURL.append(CognosInfo.getInteractiveReportLastIe8());			
			}else{
				reportURL.append(CognosInfo.getInteractiveReport());
				reportURL.append(Utils.getEncodingValue(bc.getSearchPath().getValue()));
				reportURL.append(CognosInfo.getInteractiveReportMid());
				reportURL.append(Utils.getEncodingValue(bc.getDefaultName().getValue()));
				reportURL.append(CognosInfo.getInteractiveReportLast());
			}
		}else if (bc instanceof Shortcut) {
			Shortcut shortcut = (Shortcut) bc;

			String searchPath = shortcut.getTarget().getValue()[0].getSearchPath().getValue();

			BaseClass[] baseClasses = null;

			PropEnum props[] = new PropEnum[] { PropEnum.searchPath, PropEnum.defaultName, PropEnum.ownerPassport,
					PropEnum.owner, PropEnum.ancestors, PropEnum.storeID, PropEnum.target,
					PropEnum.uri, PropEnum.defaultOutputFormat, PropEnum.defaultPortalAction 
			};

			System.out.println("Search Path:::::::::::::::::::::::::::::::"+searchPath);
			baseClasses = CognosUtil.getObjects(connect, searchPath, props, null, null);

			String rptURL = CognosUtil.getReportURL(baseClasses[0], connect, isView, isIE8);
			return rptURL;

		} else if (bc instanceof URL) {
			isURL = true;
			String uri = ((URL) bc).getUri().getValue();
			// buffer.append(URLEncoder.encode(uri, "UTF-8"));
			reportURL.append(uri);
		} else if (bc instanceof Pagelet) {
			reportURL.append(CognosInfo.getPagelet());
			reportURL.append(bc.getStoreID().getValue().get_value());
			reportURL.append(CognosInfo.getPageletLast());
		} else if (bc instanceof Dashboard) {
			reportURL.append(CognosInfo.getDashboard());
			reportURL.append(bc.getStoreID().getValue().get_value());
			reportURL.append(CognosInfo.getDashboardLast());
		} else if (bc instanceof Document) {
			if(bc.getDefaultName().getValue().toString().indexOf(".doc") > -1){
				if(bc.getDefaultName().getValue().toString().indexOf(".docx") > -1)
					reportURL.append(CognosInfo.getDocumentForDocx());
				else
					reportURL.append(CognosInfo.getDocumentForDoc());
				
				reportURL.append(bc.getStoreID().getValue().get_value());
				reportURL.append(CognosInfo.getDocumentLast());
			}else{
				reportURL.append(CognosInfo.getDocument());
				reportURL.append(bc.getStoreID().getValue().get_value());
				reportURL.append(CognosInfo.getDocumentLast());
			}
			try {
				reportURL.append(URLEncoder.encode(bc.getDefaultName().getValue().toString(), "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		} else if ( bc instanceof Exploration ) {
			reportURL.append(CognosInfo.getExploration());
			reportURL.append(bc.getStoreID().getValue().get_value());
			reportURL.append(CognosInfo.getExplorationMid());
			reportURL.append(bc.getStoreID().getValue().get_value());
			reportURL.append(CognosInfo.getExplorationLast());
			reportURL.append(bc.getStoreID().getValue().get_value());
		} else if ( bc instanceof Module ) {
			reportURL.append(CognosInfo.getModule());
			reportURL.append(bc.getStoreID().getValue().get_value());
			reportURL.append(CognosInfo.getModuleLast());
			reportURL.append(bc.getStoreID().getValue().get_value());
		}
		
		System.out.println("reportURL:::::::::::"+reportURL);
		return isURL == true ? reportURL.toString() : CognosInfo.getConnect() + reportURL.toString();

	}
	
	public static String getPassport(CRNConnect connection){
		
		SOAPHeaderElement x = ((Stub) connection.getCMService()).getResponseHeader(
				"http://developer.cognos.com/schemas/bibus/3/", "biBusHeader");
		BiBusHeader bibus = BIBusHeaderHelper.getHeaderObject(x);

		String passport = bibus.getCAM().getCAMPassport().getId();
		return passport;
	}

	public static boolean isRole(CRNConnect connection, final String CAMID, final String searchPath) {

		boolean isRole = false;
		try {
			BaseClass[] adminBcs = getRole(connection,searchPath);
			if (adminBcs[0] instanceof Role) {
				Role role = (Role) adminBcs[0];
				BaseClass[] members = role.getMembers().getValue();

				if (members != null) {
					for (int i = 0; i < members.length; i++) {
						if (members[i] instanceof Account) {
							Account account = (Account) members[i];
							String memberSearchPath = account.getSearchPath().getValue();
							if (memberSearchPath.equals(CAMID)) {
								isRole = true;
								break;
							}
						}
					}
				}
			}
		}
		catch (Exception e) {
			isRole = false;
		}

		return isRole;
	}
	
	
	private static BaseClass[] getRole(CRNConnect connection, String searchPath) {
		PropEnum props[] = new PropEnum[] { PropEnum.searchPath, PropEnum.members };

		Sort[] sort = null;
		BaseClass[] bcs = getObjects(connection, searchPath, props, sort, new QueryOptions());
		return bcs;
	}

//	public static BaseClass[] getObjects(HttpServletRequest request, final String searchPath,
//			final PropEnum[] properties, final Sort[] sortBy, final QueryOptions options) {
//
//		BaseClass[] results = null;
//
//		CRNConnect connection = (CRNConnect) request.getSession().getAttribute("connect");
//		
//		try {
//			results = connection.getCMService().query(new SearchPathMultipleObject(searchPath), properties, sortBy, options);
//		} catch (RemoteException e) {
//		} catch (Exception e) {
//		}
//		return results;
//	}
	
	public static BaseClass[] getObjects(CRNConnect connection, final String searchPath, final PropEnum[] properties, final Sort[] sortBy, final QueryOptions options) throws NullPointerException{

		BaseClass[] results = null;
		 
		try {
			String passport = getPassport(connection);
			logger.debug("======cogutil.getObject parameter=======" );
					
			logger.debug("passport : {}" ,Utils.getString(passport, "").replaceAll("[\r\n]",""));
			logger.debug("searchPath : {}" ,Utils.getString(searchPath, "").replaceAll("[\r\n]",""));
			logger.debug("======cogutil.getObject parameter=======" );
			
			results = connection.getCMService(passport).query(new SearchPathMultipleObject(searchPath), properties, sortBy, options);
		} catch (RemoteException e) {
			logger.error("CognosUtil.getObjects: {}", e);
		} catch (Exception e) {
			logger.error("CognosUtil.getObjects: {}", e);
		}
		return results;
	}
	
	public static Sort[] getSort() {
		Sort sort[] = new Sort[1]; 
		sort[0] = new Sort();
		sort[0].setOrder(OrderEnum.ascending);
		sort[0].setPropName(PropEnum.name);
//		sort[0].setPropName(PropEnum.displaySequence);
		return sort;
	}
	public static Sort[] getSortByDisplaySequence() {
		Sort sort[] = new Sort[1]; 
		sort[0] = new Sort();
		sort[0].setOrder(OrderEnum.descending);
		//sort[0].setPropName(PropEnum.name);
		sort[0].setPropName(PropEnum.displaySequence);
		return sort;
	}
	
	/**
	* Get the server locale setting.
	*
 	* @param connection Connection to Server
	*
	* @return           Server Locale.
	* 
	* 출처 : SDK HandlersCS>CSHandlers
	*/
	public static Locale[] getConfiguration(CRNConnect connection){
		ConfigurationData data = null;
		Locale[] locales = null;
		
		ConfigurationDataEnum[] config = new ConfigurationDataEnum[1];
		config[0] = ConfigurationDataEnum.fromString("serverLocale");
		
		try {
			data = connection.getSystemService().getConfiguration(config);
			locales = data.getServerLocale();
			
			if(locales == null)
			{
//				System.out.println("No serverLocale configured!");
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return locales;
	}
	
	/**
	 * Add an object to the Content Store.
	 *
	 * @param 	 connection Connection to Server
	 * @param    bc      	An object that extends baseClass, such as a Report.
	 * @param    path    	Search path that will contain the new object.
	 *
	 * @return           	The new object.
	 *
	 * 출처 : SDK HandlersCS>CSHandlers
	 */
	public static BaseClass addObjectToCS(CRNConnect connection, BaseClass bc, 	String path)
		throws java.rmi.RemoteException
	{
		// sn_dg_sdk_method_contentManagerService_add_start_1
		AddOptions ao = new AddOptions();
		ao.setUpdateAction(UpdateActionEnum.replace);
		String passport = getPassport(connection);
		
		return connection.getCMService(passport).add(new SearchPathSingleObject(path), new BaseClass[] { bc }, ao)[0];
		// sn_dg_sdk_method_contentManagerService_add_end_1
	}
}
