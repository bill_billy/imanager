package com.iplanbiz.iportal.comm.message;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.i18n.SessionLocaleResolver;

public class LocaleUtil {

//	public static Locale getLocale(HttpServletRequest request){
//    	//Locale Cookie Config
//		Locale portalLocal = null;
//    	Cookie[] cookies = request.getCookies();
//    	if(cookies != null){
//	        for(int i=0 ; i<cookies.length ; i++) {
//	            Cookie cookie = cookies[i];
//	            if(cookie.getName().equals("portal_locale")) {
//	            	if("ko".equals(cookie.getValue())) portalLocal = Locale.KOREAN;
//	            	if("en".equals(cookie.getValue())) portalLocal = Locale.ENGLISH;
//	                break;
//	            }
//	        }
//    	}
//    	return portalLocal;
//	}
//	public static void setLocale(HttpServletRequest request, HttpServletResponse response){
		/*
		 * TODO
		 * 브라우저를 닫아야만 제설정됨
		 * 향후 URL을 호출하면 변경되도록 변경가능 하도록 개발해야함
		 */
//        if(getLocale(request) == null){
//        	Cookie cookie = new Cookie("portal_locale", WebConfig.getLocale());
//        	localeResolver.setLocale(request, response, getLocale(request));
//        	response.addCookie(cookie);
//        }
        
    	
        //Locale Session Config 
//    	HttpSession session = request.getSession();
//		Locale locales = null;
//    	if ("ko".matches(WebConfig.getLocale())){
//			locales = Locale.KOREAN;
//		}else{
//			locales = Locale.ENGLISH;
//		}
//		 //세션에 존재하는 Locale을 새로운 언어로 변경해준다.
//		session.setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, locales);
//	}

    public static Locale getDefaultLocale() {
        return Locale.KOREAN;
    }
	
	public static Locale getLocale(HttpServletRequest request){
		Locale locale = null;
		
		HttpSession session = request.getSession();
		locale = (Locale) session.getAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME);
		
		if (locale == null ) {
            locale = getDefaultLocale();
        }

        return locale;
	}
	
	public static void changeLocale(HttpServletRequest request, String locale) {
        HttpSession session = request.getSession();
        Locale lo = null;
        
        //step. 파라메터에 따라서 로케일 생성, 기본은 KOREAN 
        if (locale.matches("en")) {
                lo = Locale.ENGLISH;
        } else {
                lo = Locale.KOREAN;
        }

        // step. Locale을 새로 설정한다.          
        session.setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, lo);
    }
}
