/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.iportal.comm.cognos;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.cognos.developer.schemas.bibus._3.AncestorInfo;
import com.cognos.developer.schemas.bibus._3.BaseClass;
import com.cognos.developer.schemas.bibus._3.OrderEnum;
import com.cognos.developer.schemas.bibus._3.PropEnum;
import com.cognos.developer.schemas.bibus._3.QueryOptions;
import com.cognos.developer.schemas.bibus._3.SearchPathMultipleObject;
import com.cognos.developer.schemas.bibus._3.Shortcut;
import com.cognos.developer.schemas.bibus._3.Sort;
//import com.iplanbiz.admin.service.GroupManagerServiceImpl;
import com.iplanbiz.comm.Utils;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.comm.cognos.CRNConnect;
import com.iplanbiz.iportal.comm.util.CognosUtil;

/**
 * @author : yun hee Kim(yhkim@khantech.co.kr)
 * @date   : 2012. 7. 12.
 * @desc   : 
**/
public class MenuUtil {
	
	static Logger logger = LoggerFactory.getLogger(MenuUtil.class);

//	@Autowired private static GroupManagerServiceImpl groupManagerServiceImpl;
	
	
	public static String getMyFolderStoreID(LoginSessionInfo loginInfo)
	{
		if (loginInfo.getMyFolderStoreId() == null || loginInfo.getMyFolderStoreId().equals("")) {
			CRNConnect connect = (CRNConnect) loginInfo.getExternalInfo("connect");

			BaseClass[] bcs = null;
			PropEnum props[] = new PropEnum[] { PropEnum.storeID };
			Sort[] sort = CognosUtil.getSort();
			String searchPath = (String) loginInfo.getCAMID()
					+ "/folder[@name='내 폴더' or @name='My Folders']";
			bcs = CognosUtil.getObjects(connect, searchPath, props, sort, new QueryOptions());

			loginInfo.setMyFolderStoreId(bcs[0].getStoreID().getValue().get_value());

			return bcs[0].getStoreID().getValue().get_value();
		} else {
			return loginInfo.getMyFolderStoreId();
		}
	}
	
	public static String getSearchPathUseStoreID(CRNConnect connection, String storeId){
		
		PropEnum props[] = new PropEnum[] { PropEnum.searchPath,PropEnum.storeID };
		logger.debug("PROP VLAUE = " + Utils.getString(PropEnum.searchPath.toString(), "").replaceAll("[\r\n]","") + " /// " + Utils.getString(PropEnum.storeID.toString(), "").replaceAll("[\r\n]",""));
		String cmSearchPath = "storeID(\"" + storeId + "\")"; 
		logger.debug("cmSearchPath" + Utils.getString(cmSearchPath, "").replaceAll("[\r\n]",""));
		BaseClass[] baseClasses = CognosUtil.getObjects(connection, cmSearchPath, props, null, null);
		if (baseClasses == null || baseClasses.length == 0){ 
			return "";
		}

		logger.debug("baseClasses = " + baseClasses.length);
			
		String searchPathName = baseClasses[0].getSearchPath().getValue();
		
		String searchPathReplace = searchPathName.replace("\"", "\'");
				
		return searchPathReplace;
	}
	
	public static String getStoreIdUseSearchPath(CRNConnect connection, String searchPath){
		
		PropEnum props[] = new PropEnum[] {PropEnum.storeID };
		
		BaseClass[] baseClasses = CognosUtil.getObjects(connection, searchPath, props, null, null);
		
		if (baseClasses == null || baseClasses.length == 0){
			return "";
		}
			
		String storeId = baseClasses[0].getStoreID().getValue().get_value();
		
		return storeId;
	}

	/**
	 * 좌측 메뉴리스트
	 * @param storeId
	 * @param defaultRootSearchPath
	 * @param conn
	 * @return 
	 */
	public static List<Map<String, Object>> getChildList(String storeId, String defaultRootSearchPath, CRNConnect conn){
		
		List<Map<String, Object>> menuList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> reportList = new ArrayList<Map<String, Object>>();
		
		SearchPathMultipleObject cmSearchPath = new SearchPathMultipleObject(defaultRootSearchPath);
		
		PropEnum[] properties =	{ PropEnum.defaultName,	PropEnum.searchPath,PropEnum.objectClass, PropEnum.hasChildren, PropEnum.storeID,PropEnum.uri, PropEnum.target };
		//권한에 따라 정보를 가져오기 위한 스트링
		String appendString = "/*[ permission('read') and permission('execute')]";
		
		BaseClass myCMObject = null;
		BaseClass[] children = null;
		
		try {
			//부모
			String passport = CognosUtil.getPassport(conn);
			logger.info("myCMObject.length : {}", String.valueOf(conn.getCMService(passport).query(cmSearchPath, properties, new Sort[]{}, new QueryOptions()).length));
			myCMObject = conn.getCMService(passport).query(cmSearchPath, properties, new Sort[]{}, new QueryOptions())[0];
		
			String searchPath = myCMObject.getSearchPath().getValue();
			Sort[] nodeSorts = CognosUtil.getSort();
			
			logger.info("myCMObject.getHasChildren().isValue() : {}", myCMObject.getHasChildren().isValue());
			if(myCMObject.getHasChildren().isValue()){
				cmSearchPath.set_value(searchPath + appendString);
				//자식
				children =
						conn.getCMService(passport).query(
						cmSearchPath,
						properties,
						nodeSorts,
						new QueryOptions());
				// 자식이 있으면
				logger.info("children.length : {}", children.length);
				for(int i =0; i< children.length;i++){
					String icon = "";
					String reportType = children[i].getObjectClass().getValue().toString();
					Map<String, Object> menu = new HashMap<String, Object>();
					
					if("shortcut".equals(reportType)){
						String scTargetSearchPath = ((Shortcut)children[i]).getTarget().getValue()[0].getSearchPath().getValue();
						SearchPathMultipleObject cmScSearchPath = new SearchPathMultipleObject(scTargetSearchPath);
						BaseClass shorcutTagetBcArr[] = conn.getCMService(passport).query(cmScSearchPath, properties, new Sort[]{}, new QueryOptions());
						BaseClass shorcutTargetBc = null;
						if(shorcutTagetBcArr.length > 0){
							shorcutTargetBc = shorcutTagetBcArr[0];
							icon = getShortCutReportImg(shorcutTargetBc.getObjectClass().getValue().toString());
							reportType = shorcutTargetBc.getObjectClass().getValue().toString() + "_s";
						}else{
							continue;
						}
					}else{
						icon = getReportImg(reportType);
//						type = "";
					}
//					children[i].getSearchPath().getValue()
					menu.put("C_ID", children[i].getStoreID().getValue().toString());
				    menu.put("P_ID", storeId);
					menu.put("id", children[i].getStoreID().getValue().toString());
					menu.put("C_NAME", children[i].getDefaultName().getValue());
					menu.put("C_LINK", children[i].getSearchPath().getValue());
					menu.put("text", children[i].getDefaultName().getValue());
					menu.put("cIcon", icon);
					menu.put("reportType", reportType);
					menu.put("cType",reportType);
//					menu.put("SOURCE_OWNER",WebConfig.getDivision());
					menu.put("MENU_TYPE","folder".equals(reportType)?"1":"2");
//					if(!children[i].getObjectClass().getValue().toString().equals("folder")){
//						menu.put("cLink", "do not use");
//						menu.put("hasChildren", true);
//					}
					
					if(children[i].getHasChildren().isValue() && "folder".equals(reportType)) 
						menu.put("hasChildren", true);
					else 
						menu.put("hasChildren", false);
						
					if("folder".equals(reportType)){
						menuList.add(menu);
					}else{
						reportList.add(menu);
					}
				}
			}
		}catch(Exception ex){
			menuList.add(null);
		}
		
		menuList.addAll(reportList);		
		
		return menuList;
	}
 
	public static StringBuffer getChildList(String defaultRootSearchPath, CRNConnect conn, StringBuffer leftXML, String ac, String op){
		SearchPathMultipleObject cmSearchPath = new SearchPathMultipleObject(defaultRootSearchPath);
		
		//코그너스에 요청 정보
		PropEnum[] properties =	{ PropEnum.defaultName,	PropEnum.searchPath,PropEnum.objectClass, PropEnum.hasChildren, PropEnum.storeID,PropEnum.uri, PropEnum.target };
		//권한에 따라 정보를 가져오기 위한 스트링
		String appendString = "/*[ permission('read') and permission('execute')]";
		
		BaseClass myCMObject = null;
		BaseClass[] children = null;
		
		try {
			//부모
			String passport = CognosUtil.getPassport(conn);
			logger.debug("myCMObject.length : {}", String.valueOf(conn.getCMService(passport).query(cmSearchPath, properties, new Sort[]{}, new QueryOptions()).length));
			myCMObject = conn.getCMService(passport).query(cmSearchPath, properties, new Sort[]{}, new QueryOptions())[0];
		
			String searchPath = myCMObject.getSearchPath().getValue();
			Sort[] nodeSorts = CognosUtil.getSort();
			
			logger.debug("myCMObject.getHasChildren().isValue() : {}", myCMObject.getHasChildren().isValue());
			if(myCMObject.getHasChildren().isValue())
			{
				
				//TODO: 메뉴 가져오는 부분 변경. [ DB에서 가져오도록 ]
				cmSearchPath.set_value(searchPath + appendString);
				//자식
				children =
						conn.getCMService(passport).query(
						cmSearchPath,
						properties,
						nodeSorts,
						new QueryOptions());
				// 자식이 있으면
				logger.debug("children.length : {}", children.length);
				if(children.length > 0){
					
					for(int i =0; i< children.length;i++){
						
						String icon = "";
						String reportType = children[i].getObjectClass().getValue().toString();
						
						if(reportType.equals("shortcut")){
							String scTargetSearchPath = ((Shortcut)children[i]).getTarget().getValue()[0].getSearchPath().getValue();
							SearchPathMultipleObject cmScSearchPath = new SearchPathMultipleObject(scTargetSearchPath);
							BaseClass shorcutTagetBcArr[] = conn.getCMService(passport).query(cmScSearchPath, properties, new Sort[]{}, new QueryOptions());
							BaseClass shorcutTargetBc = null;
							if(shorcutTagetBcArr.length > 0){
								shorcutTargetBc = shorcutTagetBcArr[0];
								icon = getShortCutReportImg(shorcutTargetBc.getObjectClass().getValue().toString());
							}else{
								continue;
//								icon = getShortCutReportImg("");
							}
						}else{
							icon = getReportImg(reportType);
						}
						
						//하위 자식
						leftXML.append("<menu>");
						leftXML.append("<pid>");
						leftXML.append(myCMObject.getStoreID().getValue());
						leftXML.append("</pid>");
						leftXML.append("<uid>");
						leftXML.append(children[i].getStoreID().getValue());
						leftXML.append("</uid>");
						leftXML.append("<name>");
						leftXML.append("<![CDATA["+children[i].getDefaultName().getValue()+"]]>");
						leftXML.append("</name>");
						leftXML.append("<type>");
						leftXML.append("cog");
						leftXML.append("</type>");
						if(!op.equals("noUrl")){
							leftXML.append("<link>");
							if(!children[i].getObjectClass().getValue().toString().equals("folder")){
								leftXML.append("do not use");
							}
							leftXML.append("</link>");
							leftXML.append("<icon>");
							leftXML.append(icon);
							leftXML.append("</icon>");
							leftXML.append("<copyUrl>");
							leftXML.append("<![CDATA["+children[i].getSearchPath().getValue()+"]]>");
							leftXML.append("</copyUrl>");
						}
						leftXML.append("</menu>");
						
						if(children[i].getHasChildren().isValue() && !ac.equals("top"))
						{
							String childSearchPath = children[i].getSearchPath().getValue();
							if(children[i].getObjectClass().getValue().toString().equals("folder")){
								getChildList(childSearchPath, conn, leftXML, ac, op);
							}		
						}
					}
				}else{
					leftXML.append("");
				}
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return leftXML;
	}
	
	public static List<Map<String,Object>> getChildListByMap(String storeId, String defaultRootSearchPath, CRNConnect conn, String ac){
		
		//log
		logger.debug("storeid:"+Utils.getString(storeId, "").replaceAll("[\r\n]",""));
		

		logger.debug("searchpath:"+Utils.getString(defaultRootSearchPath, "").replaceAll("[\r\n]",""));
		logger.debug("ac:"+Utils.getString(ac, "").replaceAll("[\r\n]",""));

		
		SearchPathMultipleObject cmSearchPath = new SearchPathMultipleObject(defaultRootSearchPath);
		List<Map<String, Object>> menuList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> reportList = new ArrayList<Map<String, Object>>();
		//코그너스에 요청 정보
		PropEnum[] properties =	{ PropEnum.defaultName,	PropEnum.searchPath,PropEnum.objectClass, PropEnum.hasChildren, PropEnum.storeID,PropEnum.uri, PropEnum.target };
		//권한에 따라 정보를 가져오기 위한 스트링
		String appendString = "/*[ permission('read') and permission('execute')]";
		
		BaseClass myCMObject = null;
		BaseClass[] children = null;
		
		try {
			//부모
			String passport = CognosUtil.getPassport(conn);
			logger.debug("myCMObject.length : {}", String.valueOf(conn.getCMService(passport).query(cmSearchPath, properties, new Sort[]{}, new QueryOptions()).length));
			myCMObject = conn.getCMService(passport).query(cmSearchPath, properties, new Sort[]{}, new QueryOptions())[0];
		
			String searchPath = myCMObject.getSearchPath().getValue();
			Sort[] nodeSorts = CognosUtil.getSort();
			
			logger.debug("myCMObject.getHasChildren().isValue() : {}", myCMObject.getHasChildren().isValue());
			if(myCMObject.getHasChildren().isValue())
			{
				
				//TODO: 메뉴 가져오는 부분 변경. [ DB에서 가져오도록 ]
				cmSearchPath.set_value(searchPath + appendString);
				//자식
				children =
						conn.getCMService(passport).query(
						cmSearchPath,
						properties,
						nodeSorts,
						new QueryOptions());
				// 자식이 있으면
				logger.debug("children.length : {}", children.length);
				if(children.length > 0){
					
					for(int i =0; i< children.length;i++){
						
						String icon = "";
						String reportType = children[i].getObjectClass().getValue().toString();
						
						if(reportType.equals("shortcut")){
							String scTargetSearchPath = ((Shortcut)children[i]).getTarget().getValue()[0].getSearchPath().getValue();
							SearchPathMultipleObject cmScSearchPath = new SearchPathMultipleObject(scTargetSearchPath);
							BaseClass shorcutTagetBcArr[] = conn.getCMService(passport).query(cmScSearchPath, properties, new Sort[]{}, new QueryOptions());
							BaseClass shorcutTargetBc = null;
							if(shorcutTagetBcArr.length > 0){
								shorcutTargetBc = shorcutTagetBcArr[0];
								icon = getShortCutReportImg(shorcutTargetBc.getObjectClass().getValue().toString());
							}else{
								continue;
							}
						}else{
							icon = getReportImg(reportType);
						}
						Map<String, Object> menu = new HashMap<String,Object>();
						menu.put("C_ID", children[i].getStoreID().getValue().toString());
					    menu.put("P_ID", storeId);
						menu.put("id", children[i].getStoreID().getValue().toString());
						menu.put("C_NAME", children[i].getDefaultName().getValue());
						menu.put("C_LINK", children[i].getSearchPath().getValue());
						menu.put("text", children[i].getDefaultName().getValue());
						menu.put("cIcon", icon);
						menu.put("reportType", reportType);
						menu.put("cType",reportType);
						menu.put("SOURCE_OWNER","BIFOLDER");
						menu.put("MENU_TYPE","folder".equals(reportType)?"1":"2");
						
						if(children[i].getHasChildren().isValue() && !ac.equals("top")){
							if(children[i].getHasChildren().isValue() && "folder".equals(reportType)){
								JSONObject items = new JSONObject();
								
								menu.put("hasChildren", true);
								String childSearchPath = children[i].getSearchPath().getValue();
								String childPerStoreId = children[i].getStoreID().getValue().toString();
								
								menu.put("items",getChildListByMap(childPerStoreId, childSearchPath, conn, ac)); 
							}
							else{
								menu.put("hasChildren", false);
							}
						}
						if("folder".equals(reportType)){
							menuList.add(menu);
						}else{
							reportList.add(menu);
						}
						
					}
					menuList.addAll(reportList);
				}else{
//					leftXML.append("");
				}
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return menuList;
//		return leftXML;
	}
	public static List<Map<String,Object>> getCognosChildListByMap(String storeId, String defaultRootSearchPath, CRNConnect conn, String ac){
		
		
		SearchPathMultipleObject cmSearchPath = new SearchPathMultipleObject(defaultRootSearchPath);
		List<Map<String, Object>> menuList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> folderList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> reportList = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> childrenList = new ArrayList<Map<String, Object>>();
		//코그너스에 요청 정보
		PropEnum[] properties =	{ PropEnum.defaultName,	PropEnum.searchPath,PropEnum.objectClass, PropEnum.hasChildren, PropEnum.storeID,PropEnum.uri, PropEnum.target };
		//권한에 따라 정보를 가져오기 위한 스트링
		String appendString = "/*[ permission('read') and permission('execute')]";
		
		BaseClass myCMObject = null;
		BaseClass[] children = null;          
		
		Sort[] nodeSorts = CognosUtil.getSort();
		try {
			//부모
			String passport = CognosUtil.getPassport(conn);
			logger.debug("myCMObject.length : {}", String.valueOf(conn.getCMService(passport).query(cmSearchPath, properties, nodeSorts, new QueryOptions()).length));
			myCMObject = conn.getCMService(passport).query(cmSearchPath, properties, nodeSorts, new QueryOptions())[0];
		
			String searchPath = myCMObject.getSearchPath().getValue();
//			Sort[] nodeSorts = CognosUtil.getSort();
			
			logger.debug("myCMObject.getHasChildren().isValue() : {}", myCMObject.getHasChildren().isValue());
			if(myCMObject.getHasChildren().isValue())
			{
				
				//TODO: 메뉴 가져오는 부분 변경. [ DB에서 가져오도록 ]
				cmSearchPath.set_value(searchPath + appendString);
				//자식
				children =
						conn.getCMService(passport).query(
						cmSearchPath,
						properties,
						nodeSorts,
						new QueryOptions());    
				// 자식이 있으면
				logger.debug("children.length : {}", children.length);
				if(children.length > 0){
					
					for(int i =0; i< children.length;i++){
						
						String icon = "";
						String reportType = children[i].getObjectClass().getValue().toString();
						logger.info("Report TYPE : {}" , Utils.getString(reportType, "").replaceAll("[\r\n]",""));
						if(reportType.equals("shortcut")){
							String scTargetSearchPath = ((Shortcut)children[i]).getTarget().getValue()[0].getSearchPath().getValue();
							SearchPathMultipleObject cmScSearchPath = new SearchPathMultipleObject(scTargetSearchPath);
							BaseClass shorcutTagetBcArr[] = conn.getCMService(passport).query(cmScSearchPath, properties, nodeSorts, new QueryOptions());
							BaseClass shorcutTargetBc = null;
							if(shorcutTagetBcArr.length > 0){
								shorcutTargetBc = shorcutTagetBcArr[0];
								icon = getShortCutReportImg(shorcutTargetBc.getObjectClass().getValue().toString());
							}else{
								continue;
							}
						}else{
							icon = getReportImg(reportType);
						}
						Map<String, Object> menu = new HashMap<String,Object>();
						menu.put("C_ID", children[i].getStoreID().getValue().toString());
					    menu.put("P_ID", storeId);
						menu.put("id", children[i].getStoreID().getValue().toString());
						menu.put("C_NAME", children[i].getDefaultName().getValue());
						menu.put("C_LINK", children[i].getSearchPath().getValue());
						menu.put("text", children[i].getDefaultName().getValue());
						menu.put("cIcon", icon);
						menu.put("reportType", reportType);
						menu.put("cType",reportType);
						menu.put("SOURCE_OWNER","BIFOLDER");
						menu.put("MENU_TYPE","folder".equals(reportType)?"1":"2");
						
						if(children[i].getHasChildren().isValue() && !ac.equals("top")){
							if(children[i].getHasChildren().isValue() && "folder".equals(reportType)){
//								JSONObject items = new JSONObject();
								
								menu.put("hasChildren", true);
								
								String childSearchPath = children[i].getSearchPath().getValue();
								String childPerStoreId = children[i].getStoreID().getValue().toString();
								childrenList.addAll(getCognosChildListByMap(childPerStoreId, childSearchPath, conn, ac));
//								menu.put("items",); 
							}
							else{
								menu.put("hasChildren", false);
							}
						}
						if("folder".equals(reportType)){
							folderList.add(menu);
						}else{
							reportList.add(menu);
						}
					}
					menuList.addAll(folderList);
					menuList.addAll(reportList);
					menuList.addAll(childrenList);
				}else{
//					leftXML.append("");
				}
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return menuList;
//		return leftXML;
	}
	
	
	public static String getReportImg(String reportType){
		if(reportType.equals("folder")){
			return "folder.gif";
		}else if(reportType.equals("report")){
			return "leaf_r.gif";
		}else if(reportType.equals("query")){
			return "leaf_q.gif";
		}else if(reportType.equals("dashboard")){
			return "leaf_d.gif";
		}else if(reportType.equals("URL")){
			return "leaf_l.gif";
		}else if(reportType.equals("interactiveReport")){
			return "leaf_d.gif";
		}else if(reportType.equals("pagelet")){
			return "leaf_l.gif";
		}else if(reportType.equals("analysis")){
			return "leaf_a.gif";
		}else if(reportType.equals("shortcut")){
			return "leaf_s.gif";
		}
		return "leaf_r.gif";
	}
	
	public static String getShortCutReportImg(String targetReportType){
		if(targetReportType.equals("folder")){
			return "s_folder.gif";
		}else if(targetReportType.equals("report")){
			return "leaf_s_r.gif";
		}else if(targetReportType.equals("query")){
			return "leaf_s_q.gif";
		}else if(targetReportType.equals("dashboard")){
			return "leaf_s_d.gif";
		}else if(targetReportType.equals("URL")){
			return "leaf_s_l.gif";
		}else if(targetReportType.equals("interactiveReport")){
			return "leaf_s_d.gif";
		}else if(targetReportType.equals("pagelet")){
			return "leaf_s_l.gif";
		}else if(targetReportType.equals("analysis")){
			return "leaf_s_a.gif";
		}
		return "icon_broken_ref.gif";
	}	
	public static String getReportURL( CRNConnect conn, String storeId, boolean isView, boolean isIE8) throws NullPointerException{
		String reportURL = "";
		
		String searchPath = "storeID(\""+storeId+"\")";
		logger.info("Report storeID: {}",  Utils.getString(storeId, "").replaceAll("[\r\n]",""));
		BaseClass[] baseClasses = null;
		
		PropEnum props[] = new PropEnum[] { PropEnum.searchPath,
				PropEnum.defaultName, PropEnum.description,
				PropEnum.ownerPassport, PropEnum.owner, PropEnum.ancestors,
				PropEnum.storeID, PropEnum.target, PropEnum.uri,PropEnum.objectClass,
				PropEnum.defaultOutputFormat, PropEnum.objectClass, PropEnum.report, PropEnum.defaultPortalAction};
		baseClasses = CognosUtil.getObjects(conn, searchPath, props, null, null);
		
		if (baseClasses == null || baseClasses.length == 0)
			return "";
		
		BaseClass reportBc = baseClasses[0];
		
		try {
			reportURL = CognosUtil.getReportURL(reportBc, conn, isView, isIE8);
			logger.info("Cognos reportURL: {}",  Utils.getString(reportURL, "").replaceAll("[\r\n]",""));			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return reportURL;
	}
	
	public static String makeSearchPath(String reportPath, String reportNm, String reportType){
		StringBuffer searchPath = new StringBuffer("/content");
		
		reportType = reportType.toLowerCase();
		
		String[] spReportPath = reportPath.split("/");
		
		for(int i=0; i < spReportPath.length -1; i++){
			if( i+1 == spReportPath.length - 1){
				searchPath.append("/"+reportType+"[@name=\'"+spReportPath[i+1]+"\']");
			}else if((i+1) > 1){
				searchPath.append("/folder[@name=\'"+spReportPath[i+1]+"\']");
			}
		}
		return searchPath.toString();
	}
	
	public static String getReportPath(CRNConnect conn, String searchPath){
		PropEnum props[] = new PropEnum[] { PropEnum.searchPath, PropEnum.defaultName, PropEnum.storeID,
				PropEnum.ancestors, PropEnum.objectClass};

		StringBuffer reportPath = new StringBuffer();
		BaseClass[] bsc = CognosUtil.getObjects(conn, searchPath, props, null, null);
		
		if(bsc != null){
			AncestorInfo[] ancestorArray = bsc[0].getAncestors().getValue();
			
			
			for(int i = 2; i <ancestorArray.length; i++){
				AncestorInfo ancestorInfo = ancestorArray[i];
				if(i != ancestorArray.length-1) {	
					reportPath.append(ancestorInfo.getTitle());    						
					reportPath.append(" > ");						
				} else {
					reportPath.append(ancestorInfo.getTitle());
				}
			}
		}
		return Utils.getString(reportPath.toString(), "");
	}
	
}