/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.iportal.comm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.ObjectFactory;

import com.iplanbiz.iportal.config.WebConfig;

/**
 * 
 * @author : woon sik Shin(sky3473@iplanbiz.co.kr or gmail.com) 퇴사하심. ㅠㅠ
 * @date   : 2012. 6. 28.
 * @desc   :
 */
public class CodeManager {

	public static final String dummy = "StrDummyStringIPZ";
	public static final Map<String, String> MSG_MAP; 
	static{ 
	        Map<String, String> map = new HashMap<String, String>(); 

	        map.put("SUCCESS"					, "성공");
	        map.put("SUCCESS_SAVE"				, "저장 되었습니다.");//100 저장 되었습니다.!
	        map.put("SUCCESS_EDIT"				, "수정 되었습니다.");//200 수정 되었습니다.
	        map.put("SUCCESS_DELETE"			, "삭제 되었습니다.");//300 삭제 되었습니다.
	        map.put("SUCCESS_SP"				, "프로시져가 정상적으로 수행되었습니다.");
	        
	        map.put("FAIL"						, "실패");
	        map.put("FAIL_SP"					, "프로시져 실행이 실패하였습니다.");
	        
	        map.put("QUESTION_DELETE"			, "삭제하시겠습니까?");
	        map.put("QUESTION_EXECUTE_SP"		, "프로시져가 실행되어 데이터가 변경됩니다.\\n실행하시겠습니까?");
	        map.put("QUESTION_RESET"			, "확인을 누르면 기존데이터가 삭제될 수 있습니다.");
	        map.put("QUESTION_BEFORESTEP"		, "이전 단계로 이동하시겠습니까?");
	        map.put("QUESTION_NOWSTEP"			, "현 단계를 실행하고 다음단계로 이동하시겠습니까?");
	        map.put("QUESTION_RE_EXTRACTION"	, "전체 재추출  하시겠습니까?");
	        		
	        map.put("UNDIFINE_CODE"				, "공통코드를 선택해 주세요.");
	        map.put("UNDIFINE_KPI_COPY"			, "복사된 지표가 없습니다.");
	        map.put("UNDIFINE_SC_ID"			, "평가구분을 선택해 주세요.");
	        map.put("UNDIFINE_UNIV"				, "대학명을 입력해주세요.");
	        map.put("UNDIFINE_COL"				, "항목을 선택해주세요.");
	        map.put("UNDIFINE_KPI"				, "지표를 선택해주세요.");
	        map.put("UNDIFINE_RPRT"				, "보고서를 선택해주세요.");
	        map.put("UNDIFINE_ORG"				, "트리에서 조직을 선택하십시오.");

	        map.put("WARN_CAL_EXCESS"			, "사용횟수가 0초과 산식은 삭제할 수 없습니다.");//4001 
	        map.put("WARN_COL_EXCESS"			, "사용횟수가 0초과 연계항목은 삭제할 수 없습니다.");//400
	        map.put("WARN_SCID_DUPLICATION"		, "동일한 평가구분을 선택할 수 없습니다.");//4003
	        map.put("WARN_CAL_VALIDATION"		, "지표명과 산식패턴은 필수 입력항목 입니다.");//4004
	        map.put("WARN_STRA_EXCESS"			, "사용횟수가 0초과 전략목표는 삭제할 수 없습니다.");//4005
	        map.put("WARN_SUBSTRA_EXCESS"		, "사용횟수가 0초과 세부전략목표는 삭제할 수 없습니다.");//4006
	        map.put("WARN_CAL_EXCESS"			, "사용횟수가 0초과 산식은 삭제할 수 없습니다.");//4007
	        map.put("WARN_UPLOAD_EXT"			, "업로드 가능파일은 {}입니다.");//5002
	        map.put("WARN_UPLOAD_MKDIR"			, "업로드 폴더생성 실패");//5003
	        
	        map.put("WARN_UPLOAD_SIZE"			, "업로드 용량초과");//5005
	        map.put("WARN_WIGHT_SUM"			, "가중치의 합은 100이여야 합니다.");
	        map.put("WARN_WIGHT_SUM"			, "지표별 항목 계산시 errCnt건의 오류가  존재합니다!");
	        map.put("EXCEPTION_SQL"				, "SQLException!");//5000
	        map.put("EXCEPTION_IO"				, "IOException!");//5001
	        map.put("EXCEPTION"					, "Exception: @MSG");//5004
	        
	        map.put("COGNOS_CONNECT_FAILED"		, "코그너스 연결에 실패했습니다.");
	        map.put("LOGIN_FAILED"				, "ID와 PASSWORD를 확인해주세요.");
	        map.put("SSO_FAILED"				, "SSO에서 ID를 받아오지 못했습니다.");
	        
	        MSG_MAP = Collections.unmodifiableMap(map); 
	} 	
	
	public static final Map<String, String> WWW;
	static{
		Map<String, String> map = new HashMap<String, String>();
        
       // map.put("ROOT", 		WebConfig.getRoot() + WebConfig.getUriContext());
       // map.put("CONTEXT", WebConfig.getUriContext());
        map.put("IPORTAL", 		map.get("ROOT"));
        map.put("DWR", 			map.get("ROOT") 	+ "/dwr");
        map.put("RESOURCE", 	map.get("ROOT") 	+ "/resource");
        map.put("IVISION", 		map.get("ROOT") 	+ "/ivision");
        map.put("ADMIN", 		map.get("ROOT") 	+ "/admin");
        map.put("IMANAGER", 	map.get("ROOT") 	+ "/imanager");
        map.put("COMMON", 		map.get("ROOT") 	+ "/comm");
        map.put("IMG", 			map.get("RESOURCE") + "/img");
        map.put("JS", 			map.get("RESOURCE") + "/js");
        map.put("CSS", 			map.get("RESOURCE") + "/css");

        map.put("DEFAULTSIZE", String.valueOf(WebConfig.getDefaultMaxUpSize()));
        
        map.put("FILEDOWN", 	map.get("COMMON") 	+ "/download?fileId=");
        map.put("SELECT_BOX", 	"===선택===");
        
        WWW = Collections.unmodifiableMap(map); 
	}
	
	public static final Map<String, String> WEB;
	static{
		
		Map<String, String> webMap = new HashMap<String, String>();
		
	//	webMap.put("ROOT" 		, WebConfig.getRoot() + WebConfig.getUriContext());
		webMap.put("LOGOUTPATH" , WebConfig.getLogoutPath());
	//	webMap.put("SECURITY_ROOT" 		, WebConfig.getSecurityRoot() + WebConfig.getUriContext());
		webMap.put("ADMIN_ROOT"	, webMap.get("ROOT")		+"/admin");
		webMap.put("THEME_ROOT"	, webMap.get("ROOT")		+"/resource/theme");
		webMap.put("RESOURCE"	, webMap.get("ROOT")		+"/resource");
		
		webMap.put("IMG"		, webMap.get("RESOURCE")	+"/img");
		webMap.put("JS"			, webMap.get("RESOURCE")	+"/js");

		webMap.put("JQUERY"		, webMap.get("JS")			+"/jquery");
		webMap.put("JSUTIL"		, webMap.get("JS")			+"/util");
		
		webMap.put("JSDWR"		, webMap.get("ROOT")		+"/dwr");
		
		webMap.put("CSS"		, webMap.get("RESOURCE")	+"/css");
		
		webMap.put("ADMIN_SYSTEM_ROOT"		, webMap.get("ADMIN_ROOT")+"/system");
		webMap.put("ADMIN_PORTLET_ROOT"		, webMap.get("ADMIN_ROOT")+"/portlet");
		 
		
		WEB = Collections.unmodifiableMap(webMap);
	}
	
	public static String getReturnMessage(int res){
		
		String msg = "";
		
		switch(res){
			case 100:
				msg = MSG_MAP.get("SUCCESS_SAVE");
				break;
			case 200:
				msg = MSG_MAP.get("SUCCESS_EDIT");
				break;
			case 300:
				msg = MSG_MAP.get("SUCCESS_DELETE");
				break;
			case 4002:
				msg = MSG_MAP.get("WARN_COL_EXCESS");
				break;
			case 4005:
				msg = MSG_MAP.get("WARN_STRA_EXCESS");
				break;
			case 4006:
				msg = MSG_MAP.get("WARN_SUBSTRA_EXCESS");
				break;
			case 4007:
				msg = MSG_MAP.get("WARN_CAL_EXCESS");
				break;
			case 5000:
				msg = MSG_MAP.get("EXCEPTION_SQL");
				break;
			case 5001:
				msg = MSG_MAP.get("EXCEPTION_IO");
				break;
			case 5002:
				msg = MSG_MAP.get("WARN_UPLOAD_EXT");
				break;
			case 5003:
				msg = MSG_MAP.get("WARN_UPLOAD_MKDIR");
				break;
			case 5004:
				msg = MSG_MAP.get("EXCEPTION");
				break;
			case 5005:
				msg = MSG_MAP.get("WARN_UPLOAD_SIZE");
				break;
			default:
				msg = MSG_MAP.get("FAIL");
		}
		
		return msg;
	}
	
	public static final Map<String, String> STATIC_QUERY;
	static{
		Map<String, String> queryMap = new HashMap<String, String>();

		STATIC_QUERY = Collections.unmodifiableMap(queryMap);
	}
	
	public static final Map<String, String>  QUERY_MAP(){
		Map<String, String> queryMap = new HashMap<String, String>();
		 
		return queryMap;
	}
	
	public static final List<HashMap<String, String>> STATIC_PORTLET_THEME;
	static{
		
		List<HashMap<String, String>> portletThemeList = new ArrayList<HashMap<String, String>>();
		
		HashMap<String,String> portletThemeMap = new HashMap<String,String>();
		
		portletThemeMap.put("DEFAULT_PORTLET_TOP_BACKGROUND_COLOR", 	"#DDDDDD");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_FONT_COLOR", 					"#000000");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_UNDERLINE", 					"0");

		portletThemeList.add(0, portletThemeMap);
		
		portletThemeMap.clear();
		
		portletThemeMap.put("DEFAULT_PORTLET_TOP_BACKGROUND_COLOR", 	"#FFFFFF");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_FONT_COLOR", 					"#5D5D5D");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_UNDERLINE", 					"0");
		
		portletThemeList.add(1, portletThemeMap);
		
		portletThemeMap.clear();
		
		portletThemeMap.put("DEFAULT_PORTLET_TOP_BACKGROUND_COLOR", 	"#67799D");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_FONT_COLOR", 					"#FFFFFF");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_UNDERLINE", 					"0");
		
		portletThemeList.add(2, portletThemeMap);
		
		portletThemeMap.clear();
		
		portletThemeMap.put("DEFAULT_PORTLET_TOP_BACKGROUND_COLOR", 	"#FFFFFF");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_FONT_COLOR", 					"#5D5D5D");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_UNDERLINE", 					"0");
		
		portletThemeList.add(3, portletThemeMap);
		
		portletThemeMap.clear();
		
		portletThemeMap.put("DEFAULT_PORTLET_TOP_BACKGROUND_COLOR", 	"0");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_FONT_COLOR", 					"0");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_UNDERLINE", 					"0");
		
		portletThemeList.add(4, portletThemeMap);

		portletThemeMap.clear();
		
		portletThemeMap.put("DEFAULT_PORTLET_TOP_BACKGROUND_COLOR", 	"0");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_FONT_COLOR", 					"0");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_UNDERLINE", 					"0");
		
		portletThemeList.add(5, portletThemeMap);
		
		portletThemeMap.clear();
		
		portletThemeMap.put("DEFAULT_PORTLET_TOP_BACKGROUND_COLOR", 	"0");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_FONT_COLOR", 					"0");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_UNDERLINE", 					"0");
		
		portletThemeList.add(6, portletThemeMap);

		portletThemeMap.clear();
		
		portletThemeMap.put("DEFAULT_PORTLET_TOP_BACKGROUND_COLOR", 	"0");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_FONT_COLOR", 					"0");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_UNDERLINE", 					"0");
		
		portletThemeList.add(7, portletThemeMap);
		
		portletThemeMap.clear();
		
		portletThemeMap.put("DEFAULT_PORTLET_TOP_BACKGROUND_COLOR", 	"0");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_FONT_COLOR", 					"0");
		portletThemeMap.put("DEFAULT_PORTLET_TOP_UNDERLINE", 					"0");
		
		portletThemeList.add(8, portletThemeMap);
		
		STATIC_PORTLET_THEME = Collections.unmodifiableList(portletThemeList);
	}
}