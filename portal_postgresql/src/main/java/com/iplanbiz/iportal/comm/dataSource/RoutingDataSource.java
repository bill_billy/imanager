/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.iportal.comm.dataSource;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import com.iplanbiz.comm.Utils; 

//import com.mysql.jdbc.PreparedStatement;
/**
 * @author : woon sik Shin(sky3473@iplanbiz.co.kr or gmail.com)
 * @date   : 2012. 7. 24.
 * @desc   : DataSource를 Routing
 * msjang 2015-02-09 다중 접속의 기능 강화를 위해 메소드 추가
 * 
 */
public class RoutingDataSource extends AbstractRoutingDataSource{
	private Logger logger = LoggerFactory.getLogger(getClass());
	 
	//private Map<Object,Object> sourceList = null;
	private String defaultTargetDataSourceKey = null;	
	
	private HashMap<String, DataSource> dataSourceMap;
	private HashMap<String, LinkedHashMap<String, Object>> dataSourceInfo;
	
	@Override
	protected Object determineCurrentLookupKey() {
		logger.info("Current Data Source:{}",Utils.getString(ContextHolder.getDataSourceType(), "").replaceAll("[\r\n]",""));
		return ContextHolder.getDataSourceType();
	} 
	
	@Override
	public void setTargetDataSources(Map<Object,Object> map){ 
		HashMap<String, DataSource> temp =ConvertMapToHashMap(map);
		this.dataSourceMap = temp;
		try{
			super.setTargetDataSources(map);		
			this.refresh(); 
		}catch(Exception e){
			logger.error("error",e);
		} 
		
	}
	public Map<Object,Object> getTargetDataSources(){
		return ConvertHashMapToMap(this.dataSourceMap);
	}
	private HashMap<String, DataSource> ConvertMapToHashMap(Map<Object,Object> map){
		HashMap<String, DataSource> temp = new HashMap<String, DataSource>();
		Iterator it = map.keySet().iterator();
		while(it.hasNext()){
			String key = it.next().toString();
			temp.put(key, (DataSource)map.get(key));
		}
		return temp;
	}
	private Map<Object, Object> ConvertHashMapToMap(HashMap<String, DataSource> hashmap){
		Map<Object, Object> temp = new HashMap<Object, Object>();
		Iterator it = hashmap.keySet().iterator();
		while(it.hasNext()){
			String key = it.next().toString();
			temp.put(key, hashmap.get(key));
		}
		return temp;
	} 
	public void refresh(){		
		this.afterPropertiesSet();
		logger.info("RoutingDataSource refresh");
	} 
	
	private void setDataSourceMap(HashMap<String, DataSource> dataSourceMap) {
		this.dataSourceMap = dataSourceMap; 
	}
	public void setDataSourceInfo(HashMap<String, LinkedHashMap<String, Object>> dataSourceInfo) {
		this.dataSourceInfo = dataSourceInfo;
	}
	public HashMap<String, LinkedHashMap<String, Object>> getDataSourceInfo() {
		return dataSourceInfo;
	}  
	 
	//DB접속정보 LISTHASHMAP
	public List<LinkedHashMap<String,Object>> getDBInfoList(){
		ArrayList<LinkedHashMap<String,Object>> list = new ArrayList<LinkedHashMap<String,Object>>();
		Iterator it = this.getDataSourceInfo().keySet().iterator();
		while(it.hasNext()){
			String key = it.next().toString();
			list.add(this.getDataSourceInfo().get(key));
		}
		return list;
	}
	//DB접속정보 JSONARRAY
	public JSONArray getDBInfoJsonAray(){
		JSONArray  list = new JSONArray();
		Iterator it = this.getDataSourceInfo().keySet().iterator();
		while(it.hasNext()){
			String key = it.next().toString();
			list.add(this.getDataSourceInfo().get(key));
		}
		return list;
	}
	//DB접속정보 JSONARRAY
	public JSONObject getDBInfo(String key){ 
		JSONObject item = new JSONObject(); 
		LinkedHashMap<String,Object> src = this.dataSourceInfo.get(key); 
			item.put("KEY",key);
			item.put("COMMENT", src.get("DBNAME"));
			String vender = src.get("VENDERCODE").toString();
		    String ip = "";
		    String dbname = "";
		    try{
		    	dbname =src.get("USERID").toString();
			    ip = src.get("IPADDRESS").toString();
			    HashMap<String,String> hashItem = new HashMap<String, String>();
			    hashItem.put("DB_NAME",dbname);
			    hashItem.put("DB_IP",ip);  
		    }catch(Exception e){
		    	logger.error("ERROR > Get DBInfo Data Error : {}" ,e);
		    }
		    
			String comment = src.get("DBNAME").toString();
			
			item.put("DB_CLASSNAME",src.get("DB_CLASSNAME").toString());
			item.put("DB_VENDER", vender);
			item.put("DB_USER", src.get("USERID").toString());
			item.put("DB_NAME",dbname);
			item.put("DB_IP",ip);
			item.put("DB_COMMENT",comment); 
		
		return item;
	}

	/**
	 * @return the defaultTargetDataSourceKey
	 */
	public String getDefaultTargetDataSourceKey() {
		return defaultTargetDataSourceKey;
	}

	/**
	 * @param defaultTargetDataSourceKey the defaultTargetDataSourceKey to set
	 */
	public void setDefaultTargetDataSourceKey(
			String defaultTargetDataSourceKey) {
		this.defaultTargetDataSourceKey = defaultTargetDataSourceKey; 
		
	}

}
