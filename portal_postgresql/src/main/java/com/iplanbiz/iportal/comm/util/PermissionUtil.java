package com.iplanbiz.iportal.comm.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cognos.developer.schemas.bibus._3.AccessEnum;
import com.cognos.developer.schemas.bibus._3.BaseClass;
import com.cognos.developer.schemas.bibus._3.Permission;
import com.cognos.developer.schemas.bibus._3.Policy;
import com.cognos.developer.schemas.bibus._3.PolicyArrayProp;
import com.cognos.developer.schemas.bibus._3.UpdateOptions;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.comm.Utils;
import com.iplanbiz.iportal.comm.cognos.CRNConnect;

public class PermissionUtil {
	 private static Logger logger = LoggerFactory.getLogger(WebConfig.class);
	// 코그너스 특성상 모든 권한을 삭제 할 경우 상위 권한을 가져오기 때문에 초기화가 안됨. 
	// 때문에 시스템 관리자의 권한을 임시로 넣어둔다.
	public static boolean delPermissionAll(CRNConnect connection, BaseClass target){
		BaseClass securityObject = GroupsAndRolesUtil.getSecurityObject(connection, GroupsAndRolesUtil.makeSecurityNamespacePath("cognos"), "CAMID(\"::System Administrators\")", "r");
																																	
		PolicyArrayProp policyPropForUpdate = new PolicyArrayProp();
		Policy[] tmpPoliciesSame = new Policy[1];
		
		String[] tmpPermission = {"1","1","1","1","1"};
		
		Policy newPol = newPolicy(securityObject, getPermission(tmpPermission));
		tmpPoliciesSame[0] = newPol;
		policyPropForUpdate.setValue(tmpPoliciesSame);
		target.setPolicies(policyPropForUpdate);
		
		try {
			connection.getCMService(CognosUtil.getPassport(connection)).update(new BaseClass[] { target }, new UpdateOptions());
			return true;
		} catch (java.rmi.RemoteException remoteEx) {
			remoteEx.printStackTrace();
			return false;
		}
	}
	
//	public static boolean addPermission(CRNConnect connection, String sourceName, String sourceType, BaseClass target, String nameSpace, Permission[] replacePermission) {
	public static boolean addPermission(CRNConnect connection, BaseClass target, List<Map<String, Object>> permissionStrList) {
		Policy newPol;
		PolicyArrayProp policyPropForUpdate = new PolicyArrayProp();
		Policy[] tmpPolicies = new Policy[permissionStrList.size()];
		Map<String, Object> permissionMap = new  HashMap<String, Object>();
		
		logger.info("Policies Begin");
		logger.info("permissionSTRLIST Size : {}", permissionStrList.size());
		for(int cnt=0; cnt<permissionStrList.size(); cnt++){
			logger.info("Policies Set Cnt:{} ",cnt);
			permissionMap = permissionStrList.get(cnt);  
					
			String sourceName = GroupsAndRolesUtil.makeSecurityPath((String)permissionMap.get("source"), (String)permissionMap.get("authType"),  (String)permissionMap.get("cogPerType"));
			String namespace = GroupsAndRolesUtil.makeSecurityNamespacePath((String)permissionMap.get("authType"));
			
			Permission replacePermission[] = PermissionUtil.getPermission((String[])permissionMap.get("authority"));
			
//			logger.info("securityObject Start");
//			BaseClass securityObject = GroupsAndRolesUtil.getSecurityObject(connection, namespace, sourceName, sourceType);
//			logger.info("Connection : {}", connection);
//			logger.info("namespace: {} {}", (String)permissionMap.get("authType"), namespace);
//			logger.info("sourceName: {} ", Utils.getString(sourceName, "").replaceAll("[\r\n]",""));
//			logger.info("cogPerType: {}",  (String)permissionMap.get("cogPerType"));
			BaseClass securityObject = GroupsAndRolesUtil.getSecurityObject(connection, namespace, sourceName, (String)permissionMap.get("cogPerType"));
//			logger.info("SecurityObject Size : {}", securityObject);
//			logger.info("securityObject end");
			
			if(securityObject == null){
				return false;
			}
			newPol = newPolicy(securityObject, replacePermission);
//			int numPolicies = target.getPolicies().getValue().length;
//			Policy[] tmpPoliciesSame = new Policy[numPolicies];
//			Policy[] tmpPoliciesPlus = new Policy[permissionStrList.size() - numPolicies];
//			policyPropForUpdate = new PolicyArrayProp();
	
//			boolean matchFound = false;
	
			// target's policies
//			for (int i = 0; i < numPolicies; i++) {
//				pol = target.getPolicies().getValue()[i];
				
//				String polSecPath = pol.getSecurityObject().getSearchPath().getValue();
				
				// Check for match on account
				// Permission을 통채로 바꾼다 grant아님 deny
//				if (polSecPath.equals(securityObject.getSearchPath().getValue())) {
//					pol.setPermissions(replacePermission);
//					tmpPolicies[cnt] = pol;
//				}else{
//					tmpPolicies[cnt] = newPol;
//				}
				
//			}
//			logger.info("matchFound");
//			if (matchFound) {
				// Update with tmpPoliciesSame
//				policyPropForUpdate.setValue(tmpPoliciesSame);
//			} else {
				// add new policy and update with tmpPoliciesPlus
//				tmpPoliciesPlus[numPolicies] = newPol;
//				policyPropForUpdate.setValue(tmpPoliciesPlus);
//			}
//			logger.info("matchFound End"); 
			tmpPolicies[cnt] = newPol; 
		}
		logger.info("Policies Set ");
		policyPropForUpdate.setValue(tmpPolicies);
		target.setPolicies(policyPropForUpdate);
		try {
				logger.info("Policies Updating");
				connection.getCMService(CognosUtil.getPassport(connection)).update(new BaseClass[] { target }, new UpdateOptions());
		} catch (java.rmi.RemoteException remoteEx) {
			remoteEx.printStackTrace();
			return false;
		} finally{
			logger.info("Policies End");			
		}
		return true;

	}
	
	public static Permission[] getPermission(String[] permissionStr){
		
		if(permissionStr == null){
			Permission[] permissions = null;
			return permissions;
		}
		
		Permission execPermission = new Permission();
		Permission travPermission = new Permission();
		Permission writePermission = new Permission();
		Permission readPermission = new Permission();
		Permission polPermission = new Permission();
	
		execPermission.setName("execute");
		writePermission.setName("write");
		readPermission.setName("read");
		polPermission.setName("setPolicy");
		travPermission.setName("traverse");

		for(int i = 0; i<5; i++){
//			System.out.println(permissionStr[i]);
		}
		
		if(permissionStr[0].equals("0")){
			readPermission.setAccess(AccessEnum.deny);
		}else if(permissionStr[0].equals("1")){
			readPermission.setAccess(AccessEnum.grant);
		}
		
		if(permissionStr[1].equals("0")){
			writePermission.setAccess(AccessEnum.deny);
		}else if(permissionStr[1].equals("1")){
			writePermission.setAccess(AccessEnum.grant);
		}
		
		if(permissionStr[2].equals("0")){
			execPermission.setAccess(AccessEnum.deny);
		}else if(permissionStr[2].equals("1")){
			execPermission.setAccess(AccessEnum.grant);
		}
		
		if(permissionStr[3].equals("0")){
			polPermission.setAccess(AccessEnum.deny);
		}else if(permissionStr[3].equals("1")){
			polPermission.setAccess(AccessEnum.grant);
		}
		
		if(permissionStr[4].equals("0")){
			travPermission.setAccess(AccessEnum.deny);
		}else if(permissionStr[4].equals("1")){
			travPermission.setAccess(AccessEnum.grant);
		}
		
		Permission[] permissions = { writePermission, readPermission, execPermission, travPermission, polPermission };
	
		return permissions;
	
	}

	private static Policy newPolicy(BaseClass securityObject, Permission[] replacePermission) {
		Policy policy = new Policy();
		policy.setPermissions(replacePermission);
		policy.setSecurityObject(securityObject);
		return policy;
	}
}

