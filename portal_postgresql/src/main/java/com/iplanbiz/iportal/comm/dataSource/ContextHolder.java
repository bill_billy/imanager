/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.iportal.comm.dataSource;

/**
 * @author : woon sik Shin(sky3473@iplanbiz.co.kr or gmail.com)
 * @date   : 2012. 7. 24.
 * @desc   : 현재 DataSource를 판단
 */
public class ContextHolder {
	 
		private static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();
		
		public static void setDataSourceType(String dataSourceType){
			contextHolder.set(dataSourceType);
		}
		
		
		public static String getDataSourceType(){
			return contextHolder.get();
		}
		
		public static void clearDataSourceType(){
			contextHolder.remove();
		}
		public static void setDataSourceType(DataSourceType dataSourceType){
			contextHolder.set(dataSourceType.toString());
		}
		


}
