/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.iportal.interceptor;
 
import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration; 

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;  

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory; 
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView; 
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.view.RedirectView;

import com.iplanbiz.comm.Utils;
import com.iplanbiz.core.secure.*; 
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.iportal.config.WebConfig;
import com.iplanbiz.iportal.service.PasswordService;
import com.iplanbiz.iportal.service.system.SessionService;

 

/**
 * @author : 장민수
 * @date   : 2016. 10. 19.
 * @desc   : 
 */
public class LoggingHandlerInterceptor extends HandlerInterceptorAdapter {
	
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
    
    @Autowired SessionService sessionService;
     
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object obj) throws Exception {
    	logger.info("is login? : {}",request.getSession().getId());
    	logger.info("is login? : {}",loginSessionInfoFactory.getObject().getUserId());
    	this.printParameter(request);
    	if("/favicon.ico".equals(request.getRequestURI())) return false;
    	
    	logger.debug("loginSessionInfoFactory.getObject().getIsLogin() 	:{}", loginSessionInfoFactory.getObject().isLogin());
    	logger.info("remote addr : {}", Utils.getClientIpAddr(request));
    	String serialKey = WebConfig.getSerialKey();
    	String uri = request.getRequestURI();
//    	String division = WebConfig.getDivision();
    	Crypto cs = new Crypto();
    	
    	cs.setSecretKey("ioneiplan");
		cs.setAlgorithm("BlowFish");
		if(request.getRequestURI().indexOf("/auth")!=-1) return true;
		
		InetAddress local = InetAddress.getLocalHost();
		String svIp = local.getHostAddress(); 
		String valueUri = "";
		int uriIndexOf = uri.lastIndexOf("/");
		@SuppressWarnings("rawtypes")
		Enumeration eParam = request.getParameterNames();
		while (eParam.hasMoreElements()) {  
	         String pName = (String)eParam.nextElement();  
	         logger.debug("				{}			:{}", Utils.getString(pName, "").replaceAll("[\r\n]",""), Utils.getString(request.getParameter(pName), "").replaceAll("[\r\n]",""));
	    }
//		if(WebConfig.getDivision() != "IPORTAL"){
//			if(request.getRequestURI().indexOf("sqlhouse/") > -1){
//				valueUri = uri.substring(0, uriIndexOf);
//				//src랑 uri비교해서 src 이외의부분을 제거
//			}else{
//				valueUri = uri;
//			}
//		}else{
//			valueUri = request.getParameter("src");
//		}
		
		/* 로그인 없이 사용하는 페이지*/
		String url = request.getRequestURI();
		String checkValue = "";
		//iDashboard 메뉴
		if(request.getRequestURI().indexOf("/idashboard") > -1) {
			if(request.getRequestURI().indexOf("/idashboardItem") > -1||request.getRequestURI().indexOf("/idashboardView") > -1) {
				String documentid = request.getParameter("filename").replace("/", "");
				//파일 아이디로 메뉴에 프로그램id에 전체공개 있는 보고서 패스.
//				if( loginSessionInfoFactory.getObject().getIsLogin() == false){
//					if(commonService.getDashBoardCheck(documentid, division) == false){
//						ModelAndView mview = new ModelAndView("redirect:/accessRight");
//						throw new ModelAndViewDefiningException(mview);
//					}else{
//						checkValue = "true";
//						return true;
//					}
//				}
				
			}
			else if(request.getRequestURI().indexOf("/EDIT0001")>-1){}
			else{
				return true;
			}		
		}
		logger.info("requestURI:{}",request.getRequestURI());
		if(request.getRequestURI().indexOf("/dashboard/dashboardView") > -1) return true; //경인여대 하드코딩.대시보드 무조건 아무나 조회 가능하도록
		if(request.getRequestURI().indexOf("/system/session/") > -1) return true;
		if(request.getRequestURI().indexOf("/system/install/") > -1) return true;
		if(request.getRequestURI().indexOf("customDBData") > -1) return true; 
		/* 로그인 없이 사용하는 페이지*/
		
//		String decodeKey = serialKey.replaceAll("-", "").toLowerCase();
		String decStr="";
//		String menuInfo = commonService.getMenuInfo(valueUri);
		System.out.println("SRC ================== " + request.getParameter("src"));
//		logger.debug("================="+menuInfo+"======================");
		//System.out.println("@@@@@@@@@@@@@ URI" + request.getRequestURI() + "/////" + request.getRequestURL() + "!!!!!!!" + request.getRequestURI().length()+"$$$$$" + menuInfo + "////////////" + uriIndexOf);
//		if(!checkValue.equals("true")){
//			if( loginSessionInfoFactory.getObject().isLogin()() == false){
//				if(commonService.getMenuAllCheck(valueUri, division) == false){
//					if(!"-1".equals(menuInfo)){
//						logger.debug("=================메뉴 확인======================"); 
//						if(valueUri.length() > 1){ 
//							if(commonService.getMenuCheck(valueUri, division) == false){
//								ModelAndView mview = new ModelAndView("redirect:/accessRight");
//								throw new ModelAndViewDefiningException(mview); 
//							}
//						}	
//					}
//				}else{
//					return true;
//				}
//			}
//		}
		try{
//			decStr=cs.decrypt(decodeKey);
		}catch(Exception e){
			if(request.getRequestURI().indexOf("/periodOver") > -1) return true;
			throw new ModelAndViewDefiningException(new ModelAndView("redirect:/periodOver"));
		}
		
		String[] svIpArr = svIp.split("\\.");
		String[] keySplit = decStr.split("A");
		
		//이덕희 - 세션체크하는 부분 나중에 처리해야함
//		for(int i=0;i<svIpArr.length;i++){
//			
//			String tmp = keySplit[0].substring(i*3, i*3+3);
//			
//			String intTmp = String.valueOf(Integer.valueOf(tmp).intValue());
//			
//			// Server IP 체크.
//			logger.debug("ServIP:"+svIpArr[i]);
//			logger.debug("LiceIP:"+intTmp);
//			
//			if(!svIpArr[i].equals(intTmp)){
//				if(request.getRequestURI().indexOf("/periodOver") > -1) return true;
//				throw new ModelAndViewDefiningException(new ModelAndView("redirect:/periodOver"));
//			}
//			
//			Calendar cal = Calendar.getInstance ();
//			
//			SimpleDateFormat dFormat = new SimpleDateFormat ( "yyyyMMdd"  );
//			String nowDate = dFormat.format (cal.getTime());
//			
//			// 사용기간 체크.
//			
//			logger.debug("NOW:"+Integer.valueOf(nowDate).intValue());
//			logger.debug("EXPIRED DATE:"+Integer.valueOf(keySplit[1]).intValue());
//			//라이센스 만료 후 두달 경과.. 차단 
//			if(Integer.valueOf(nowDate).intValue() > Integer.valueOf(keySplit[1]).intValue()+200 ){
//				ModelAndView mview = new ModelAndView("redirect:/periodOver");
//				mview.addObject("LICENSEEXPIREDATE",Integer.valueOf(keySplit[1]).intValue());
//				if(request.getRequestURI().indexOf("/periodOver") > -1) return true;
//				System.out.println("라이센스 체크..");
//				throw new ModelAndViewDefiningException(mview); 
//			}
//		}
		//라이센스 만기일 표시.
//		SecurityUtil.expireDate = keySplit[1];	
//		if(uri.length() > 1){
//			String division = WebConfig.getDivision();
//			if(commonService.getMenuCheck(uri, division) == false){
//				ModelAndView mview = new ModelAndView("redirect:/periodOver");
//				throw new ModelAndViewDefiningException(mview);
//			}
//		}
//    	if(commonService.getMenuCheck(C_Link, division) == false)
    	if(loginSessionInfoFactory.getObject().isLogin()
    			&& request.getRequestURI().indexOf("/ivision") == -1){
			logger.debug("=====================================Session Information=====================================");
			logger.debug("					userId 			: {}", Utils.getString(loginSessionInfoFactory.getObject().getUserId(), "").replaceAll("[\r\n]",""));		
			
			logger.debug("					userName 		: {}", Utils.getString(loginSessionInfoFactory.getObject().getUserName(), "").replaceAll("[\r\n]",""));
//			logger.debug("					passport 		: {}", loginSessionInfoFactory.getObject().getPassport());
			logger.debug("					isAdmin 		: {}", loginSessionInfoFactory.getObject().getIsAdmin(), "");
//			logger.debug("					CAMID 			: {}", loginSessionInfoFactory.getObject().getCAMID());
//			logger.debug("					Connection  	: {}", loginSessionInfoFactory.getObject().getConnect());
			logger.debug("					URL         	: {}", Utils.getString(request.getRequestURL().toString(), "").replaceAll("[\r\n]",""));
//			logger.debug("					DIVISION         	: {}", WebConfig.getDivision());
//			logger.debug("					retUrl         	: {}", loginSessionInfoFactory.getObject().getRootDomain());
			logger.debug("=============================================================================================");
    	}
    	
		
    	/** Session 회원정보여부 체크 */
    	if(loginSessionInfoFactory.getObject().isLogin()){
    
    		//중복로그인 check
    		if(sessionService.getState(request.getSession().getId()).equals(LoginSessionInfo.State.DUPLICATION.toString())) {
    			String userId = loginSessionInfoFactory.getObject().getUserId();
    			loginSessionInfoFactory.getObject().setLogin(false);
    			request.getSession().invalidate(); 
    			logger.info("중복로그인검출됨.");
    			if(request.getMethod().equals("POST")) {
        			throw new ModelAndViewDefiningException(new ModelAndView("redirect:/login/action/duplicationwarningPost"));
        		}
    			throw new ModelAndViewDefiningException(new ModelAndView("redirect:/login/action/duplicationwarning"));
    		}
    		/**
    		if(loginSessionInfoFactory.getObject().getState()==LoginSessionInfo.State.DUPLICATION){
    			loginSessionInfoFactory.getObject().setState(LoginSessionInfo.State.DESTROY); 
    			String userId = loginSessionInfoFactory.getObject().getUserId();
    			loginSessionInfoFactory.getObject().setLogin(false);
    			request.getSession().invalidate(); 
    			logger.info("중복로그인검출됨.");
    			if(request.getMethod().equals("POST")) {
        			throw new ModelAndViewDefiningException(new ModelAndView("redirect:/login/action/duplicationwarningPost"));
        		}
    			throw new ModelAndViewDefiningException(new ModelAndView("redirect:/login/action/duplicationwarning"));
    		}
    		**/
    		if(request.getRequestURI().indexOf("home/pwd/changePassword" ) > -1) return true;
    		if(request.getRequestURI().indexOf("home/pwd/changePassword?type=welcome" ) > -1) return true;
    		if(request.getRequestURI().indexOf("home/pwd/update" ) > -1) return true;
    		if(request.getRequestURI().indexOf("login/action/cogLogout" ) > -1) return true;
    		if(request.getRequestURI().indexOf("login/action/logout") > -1) return true;
    		
    		if(loginSessionInfoFactory.getObject().getExternalInfo("checkPwdUpdate") != null && 
    				(Boolean)loginSessionInfoFactory.getObject().getExternalInfo("checkPwdUpdate")==true) {//비밀번호 변경일자 확인대상
    			if(loginSessionInfoFactory.getObject().getExternalInfo("passwordUpdateDat")==null) {
    				throw new ModelAndViewDefiningException(new ModelAndView("redirect:/home/pwd/changePassword?type=welcome"));
    			}
    			String pwdUpdateDate = loginSessionInfoFactory.getObject().getExternalInfo("passwordUpdateDat").toString();
    			long diff = 0;
    			long diffDays = 0;
    			
    			SimpleDateFormat format = new SimpleDateFormat ("yyyy-MM-dd");
    			Date currentTime = new Date ();
    			Date pwdDate = format.parse(pwdUpdateDate);
    			diff = currentTime.getTime() - pwdDate.getTime();
    			diffDays = diff/(24*60*60*1000); // 몇 일 이전에 변경했는지
    			
    			logger.info("diff:"+diff+"/diffDays:"+diffDays);
    			if(diffDays>WebConfig.getPasswordCycle()*30) {//첫 로그인 & 3개월마다
    				throw new ModelAndViewDefiningException(new ModelAndView("redirect:/home/pwd/changePassword?type=welcome"));
    			}
    		}
    		
    		
    		return true;
    	}else{
    		//TODO 로그인 URL 변경
    		logger.debug("RequestURL = " + Utils.getString(request.getRequestURI(), "").replaceAll("[\r\n]","")); 
    		if(request.getRequestURI().indexOf("login/action/duplicationwarningPost" ) > -1) return true;
    		if(request.getRequestURI().indexOf("login/action/duplicationwarning" ) > -1) return true;
    		if(request.getRequestURI().indexOf("login/action/duplicationCheck" ) > -1) return true;
    		if(request.getRequestURI().indexOf("login/action/noSSOScript") > -1) return true;
    		if(request.getRequestURI().indexOf("login/page/index") > -1) return true;
    		if(request.getRequestURI().indexOf("login/action/csrfvalidation")>-1) return true;
    		if(request.getRequestURI().indexOf("system/install")>-1) return true; 
    		if(request.getRequestURI().indexOf("login/action/logout") > -1) return true;
    		
    		if(request.getRequestURI().indexOf("ktoto/notice/getKtotoNoticeInformList") > -1) return true;
    	//	if(request.getRequestURI().indexOf("cms/notice/get") > -1) return true;
    		throw new ModelAndViewDefiningException(new ModelAndView("redirect:/login/action/noSSOScript"));
    	}
    }
    
	private void printRequest(HttpServletRequest request) {
		@SuppressWarnings("rawtypes")
		Enumeration eParam = request.getParameterNames();
		String url = request.getRequestURL().toString();
		String context = request.getContextPath();
		String servletpath = request.getServletPath(); 
    	logger.debug("=====================================preHandle Print Request Values=====================================");
	    logger.debug("				Request Url		:{}", Utils.getString(url,"").replaceAll("[\r\n]",""));
		logger.debug("				Context		:{}", Utils.getString(context,"").replaceAll("[\r\n]",""));
		logger.debug("				getServletPath		:{}", Utils.getString(servletpath,"").replaceAll("[\r\n]",""));
		logger.debug("				Method Name		:{}", request.getMethod());
		while (eParam.hasMoreElements()) {  
	         String pName = (String)eParam.nextElement();  
	         logger.debug("				{}			:{}", Utils.getString(pName, "").replaceAll("[\r\n]",""), Utils.getString(request.getParameter(pName), "").replaceAll("[\r\n]",""));
	    } 
		logger.debug("==============================================================================================");
		
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception{
 
		this.printRequest(request);
	}
	
    public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3) throws Exception {
		/** 공통 Exception 처리를 해주는것이 좋다. 에러 메일을 보내는것등 */
    	logger.debug("Interceptor:afterComplete");
    }
    public void printParameter(HttpServletRequest request){
    	 @SuppressWarnings("rawtypes")
 		Enumeration eParam = request.getParameterNames();
 	    logger.debug("=====================================Print Request Values=====================================");
 	    logger.debug("				Request Url		:{}", Utils.getString(request.getRequestURI(), "").replaceAll("[\r\n]",""));
 		logger.debug("				Method Name		:{}", Utils.getString(request.getMethod(), ""));
 		while (eParam.hasMoreElements()) {  
 	         String pName = (String)eParam.nextElement();  
 	         logger.debug("				{}			:{}", Utils.getString(pName, "").replaceAll("[\r\n]",""), Utils.getString(request.getParameter(pName), "").replaceAll("[\r\n]",""));
 	    }
 		logger.debug("==============================================================================================");
    }
}