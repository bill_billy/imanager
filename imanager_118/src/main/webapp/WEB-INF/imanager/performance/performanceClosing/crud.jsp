<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>실적마감관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/widget/Form.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>  
        
        <script>
       
        var gubn1,gubn2,gubn3;
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$("#btnSearch").jqxButton({ width: '',  theme:'blueish'});
        		init();
			}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		makeMissionCombo();
        	}
        	var missionList = [];
        	
	   		var missionDat;
				function makeMissionCombo(){
					missionDat = $i.sqlhouse(322,{}); 
						missionDat.done(function(res){
							missionList = res.returnArray;
							missionList.forEach(function(dataOne) {
			    				dataOne.MS_NM = $i.secure.scriptToText(dataOne.MS_NM);
			    			});
		    				var source = 
		    				{ 
			        			datatype:"json",
			        			datafields:[
			        				{ name : "MS_ID" },
			        				{ name : "START_YYYY" },
			        				{ name : "END_YYYY" },
			        				{ name : "MS_NM" } 
			        			],
			        			id:"id",
			        			localdata:res,
			        			async : false 
			        		};
			        		var dataAdapter = new $.jqx.dataAdapter(source);
			        		 $("#getMission").jqxComboBox({ 
			        				selectedIndex: 0, 
			        				source: dataAdapter, 
			        				animationType: 'fade', 
			        				dropDownHorizontalAlignment: 'right', 
			        				displayMember: "MS_NM", 
			        				valueMember: "MS_ID",  
			        				dropDownWidth: 300, 
			        				dropDownHeight: 100, 
			        				width: 300, 
			        				height: 22,  
			        				theme:'blueish'} );
			        		 var datas=source.localdata.returnArray;
			        		 $("#getMission").jqxComboBox('selectIndex', 0 ); 
			        		var startyyyy;
			        		var endyyyy;
			        		var msid;
			        		var index = $("#getMission").jqxComboBox('getSelectedIndex');
			        		msid=datas[index].MS_ID;
			        		startyyyy=datas[index].START_YYYY;
			        		endyyyy=datas[index].END_YYYY;
			        		$("#startyyyy").val(startyyyy);
			        		$("#endyyyy").val(endyyyy);
			        		search(msid,startyyyy,endyyyy);
			        		$('#getMission').on('change', function (event) 
			        				{
			        			index = $("#getMission").jqxComboBox('getSelectedIndex');
			        			startyyyy=datas[index].START_YYYY;
				        		endyyyy=datas[index].END_YYYY;
			        			$("#startyyyy").val(startyyyy);
				        		$("#endyyyy").val(endyyyy);
			        				});		 
			      });  
		 		}//makeMissionCombo 
        	//조회
        function search(msid,startyyyy,endyyyy){
		 			var k=1;
		 			 var arra=[]; 	
		 			for(var i=startyyyy*1;i<=endyyyy*1;i++){
		 				var yyyyList = {};
		 				yyyyList['MS_ID']=msid*1;
		 				yyyyList['YYYY']=i;
		 				yyyyList['CLOSING_STEP']=1;
		 				yyyyList['STATUS']=1;
		 				yyyyList['SAVE']=i;
		 				arra.push(yyyyList);
		 				k++;
		 			}
		 			if(msid){
		 				var msid2="AND MS_ID='"+msid+"' ";
		 			}
		 			else
		 				var msid2="";
        		var dat = $i.sqlhouse(470,{MS_ID:msid2, YYYY:''});
        		dat.done(function(res){
		            // prepare the data
		            for(var w=0;w<arra.length;w++){
		            	if(res.returnArray.length>0){
		            	for(var q=0; q<res.returnArray.length;q++){
		            		if(arra[w].YYYY==res.returnArray[q].YYYY){
		            			arra[w].CLOSING_STEP=res.returnArray[q].CLOSING_STEP;
		            			
		            		}
		            	}}
		            }
		            res.returnArray=arra;
		          	missionList = res.returnArray;
		          	$("#makeTable").empty();
		          	var tableList ="";  
		          	for(var w=0;w<missionList.length;w++){
		          		var msid=missionList[w].MS_ID;
		          		var yyyy=missionList[w].YYYY;
		          		var status=missionList[w].CLOSING_STEP;
		          		var msid=missionList[w].MS_ID;
		          		
		          		tableList +="<tr>";
	        			tableList +="<td class='t_center' style='width:20%;'>"+yyyy+"</td>";
	        			tableList +="<td class='t_center' style='width:40%;'>";
	        			tableList +="<input type='radio'  name='status"+w+"' value='1'";
	        			if(status==1)
	        				tableList +="checked";
	        			tableList +=" id='status' width='100%' height='100%'  /><label style='padding-right: 30px;'>대기</label>";
	        			tableList +="<input type='radio'  name='status"+w+"' value='2'";
	        			if(status==2)
	        				tableList +="checked";
	        			tableList +=" id='status' width='100%' height='100%'  /><label style='padding-right: 30px;'>집계</label>";
	        			tableList +="<input type='radio'  name='status"+w+"' value='3'";
	        			if(status==3)
	        				tableList +="checked";
	        			tableList +=" id='status' width='100%' height='100%'  /><label>완료</label>";
	        			tableList +="<td class='t_center' style='width:20%;'><div class='button type2 f_center' style='margin:5px 0 5px 0;'><input type='button' value='저장' class='btnSave' style='width=100%; height=100%; padding:2px 10px 3px 10px !important;' onclick='save("+msid+","+yyyy+",\"status"+w+"\")'/></td>";
	        			tableList +="<td class='t_center' style='width:20%;'>";
	        			if(status==2){
	        				tableList +="<div class='button type4 f_center' style='margin:5px 0 5px 0;'><input type='button' value='집계' class='btnSave' style='width=100%; height=100%; padding:2px 10px 3px 10px !important;' onclick='calculate("+msid+","+yyyy+")'/>";
	        			}else{
	        				tableList +="&nbsp;";
	        			}
	        			tableList +="</td>";
	        			tableList +="</tr>";
		            }
		          	$("#makeTable").append(tableList); 
		         $("#msid").val($("#getMission").val());
		    }); 
		 }
		 		
		 		
		 		
		 		
        	function calculate(ms_id,yyyy){
        		
        		$i.dialog.confirm('SYSTEM','해당 년도 성과지표의 실적이 집계됩니다. 계속하시겠습니까?',function(){
        			$i.post("./calculateKpiVal",{MS_ID:ms_id,YYYY:yyyy}).done(function(args){
        				if(args.returnCode == "EXCEPTION"){
            				$i.dialog.error("SYSTEM","집계 중 오류가 발생했습니다."+args.returnMessage);
            			}else{
            				$i.dialog.alert("SYSTEM",args.returnMessage);
            			}
            		});
        			
        		});
        	}
		 		
		 		
		 		
        	
        	function search0(){
        		search($("#getMission").val(),$("#startyyyy").val(),$("#endyyyy").val());
        	}
        	function save(msid,yyyy,stts){
        		var status = $('input[name='+stts+']:checked').val();
        		var userId = "${sessionScope.loginSessionInfo.userId}";
        	   	var check=$i.sqlhouse(470,{MS_ID:'AND MS_ID='+msid+'',YYYY:'AND YYYY='+yyyy+''});
          		check.done(function(res){
        			check=res.returnArray;
        			if(check==null ||check==""){
        				$i.dialog.confirm("SYSTEM","저장하시겠습니까?",function(){
        					$i.post("./saveClosing",{ac:"insert",ms_id:msid,yyyy:yyyy,step:status}).done(function(args){
        						if(args.returnCode == "EXCEPTION"){
                    				$i.dialog.error("SYSTEM","집계 중 오류가 발생했습니다."+args.returnMessage);
                    			}else{
                    				$i.dialog.alert("SYSTEM",args.returnMessage);
                    				search0();
                    			}
        					});
        					/*try {
        						$i.sqlhouse(471,{MS_ID:msid*1,YYYY:yyyy*1,CLOSING_STEP:status*1, INSERT_EMP : userId}).done(function(){
        							$i.dialog.alert("SYSTEM","저장되었습니다.");
            						search0();
        						});
							} catch (e) {
								$i.dialog.error("SYSTEM","저장 중 오류가 발생했습니다.");
							}*/
        				});
        			}
        			else{
        				$i.dialog.confirm("SYSTEM","저장하시겠습니까?",function(){
        					$i.post("./saveClosing",{ac:"update",ms_id:msid,yyyy:yyyy,step:status}).done(function(args){
        						if(args.returnCode == "EXCEPTION"){
                    				$i.dialog.error("SYSTEM","집계 중 오류가 발생했습니다."+args.returnMessage);
                    			}else{
                    				$i.dialog.alert("SYSTEM",args.returnMessage);
                    				search0();
                    			}
        					});
        				/*try {
        					$i.sqlhouse(472,{MS_ID:'AND MS_ID=\''+msid+'\'',YYYY:'AND YYYY=\''+yyyy+'\'' ,CLOSING_STEP:status, UPDATE_EMP : userId}).done(function(){
        						$i.dialog.alert("SYSTEM","저장되었습니다.");
        						search0();
        					});
						} catch (e) {
							$i.dialog.error("SYSTEM","저장 중 오류가 발생했습니다.");
						}*/
        				});
        			}
        		});
        	}
        </script>
        <style>
        		 .blueish .datatable .datatable_fixed table tbody tr:nth-child(odd):hover td:not(.common){
				  	color:#223648 !important;
				  	border-color:#e1e1e1  !important; 
				  	background:#ffffff !important;
				  }
				  
				  .blueish .datatable .datatable_fixed table tbody tr:nth-child(even):hover td:not(.common){
				  	color:#223648 !important;
				  	border-color:#e1e1e1  !important; 
				  	background:#f0f5f9 !important;
				  }
				  .blueish .datatable .datatable_fixed table tbody tr td.common{
				  	background:#ffffff !important;
				  
				  }
				  .blueish .datatable .datatable_fixed table tbody tr:hover td.common{
				  	color:#223648 !important;
				  	border-color:#e1e1e1  !important
				  } 
				  
				  .blueish .datatable .datatable_fixed table tbody tr:hover td.red{
				  	color:red !important;
				  	border-color:#e1e1e1  !important
				  }  
        
        
        </style>
    </head>
    <body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 1%;">
			<form id="searchForm">
				<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
					<div class="label type1 f_left">
						비젼:
					</div> 
					<div class="combobox f_left"  id='getMission' >  
					</div>
					<div class="group f_right pos_a" style="right:0px;">
						<div class="button type1 f_left">
							<input type="hidden" id="startyyyy" name="startyyyy" />  
							<input type="hidden" id="msid" name="msid" />  
							<input type="hidden" id="endyyyy" name="endyyyy" />  
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search0()" />
						</div>
					</div>
				</div>
			</form>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
					<div class="content f_left" style=" width:100%;height: 700px; margin-right:1%;">
						<div class="blueish datatable f_left" style="width:100%; height:450px; margin:5px 0; overflow:hidden;">      
								<!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 헤더가 50px일경우, 300px - 50px= 250px)-->
								<div class="datatable_fixed" style="width:100%; height:450px; float:left; padding:25px 0;">
									<!--group으로 헤더높이가 길어질 경우 padding  (기본높이는 25px)줘야 함-->
									<div style=" height:425px; !important; overflow-x:hidden;">  
										
										<table  width="100%" cellspacing="0" cellpadding="0" border="0">
											<thead>
											<tr> 
												<th style="width:20%">년도</th>  
												<th style="width:40%">마감단계</th> 
												<th style="width:20%">저장</th>    
												<th style="width:20%">마감작업</th> 
												<th style="width:17px; min-width:17px; border-bottom:0;"></th>
											</tr>  
										</thead>      
										<tbody id="makeTable"> 
										</tbody>     
									</table> 
								</div>     
							</div>
						</div> 
					</div>
			</div>
					
		</div>		
					
			
	</body>
</html>

