<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <title>프로그램확정화면</title>
      	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/widget/Form.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
        <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <style>
				 .blueish .datatable .datatable_fixed table tbody tr:nth-child(odd):hover td:not(.common){
				  	color:#223648 !important;
				  	border-color:#e1e1e1  !important; 
				  	background:#ffffff !important;
				  }
				  
				  .blueish .datatable .datatable_fixed table tbody tr:nth-child(even):hover td:not(.common){
				  	color:#223648 !important;
				  	border-color:#e1e1e1  !important; 
				  	background:#f0f5f9 !important;
				  }
				  .blueish .datatable .datatable_fixed table tbody tr td.common{
				  	background:#ffffff !important;
				  
				  }
				  .blueish .datatable .datatable_fixed table tbody tr:hover td.common{
				  	color:#223648 !important;
				  	border-color:#e1e1e1  !important
				  } 
				  
				  .blueish .datatable .datatable_fixed table tbody tr:hover td.red{
				  	color:red !important;
				  	border-color:#e1e1e1  !important
				  }  
			</style>
        <script type="text/javascript">
        $(this).ready(function () {               
			makeMissionCombo();
			makeStatCombo();
			
			var step=[missionDat,straCombo,substraCombo];
			$.when.apply($,step).done(function(){
				/* var msid = $("#getMission").val();
				var yyyy = $("#getYY").val();
				var stat = $("#getStat").val();
				var straid = $("#getStra").val();
				var substraid = $("#getSubstra").val(); */
				makeTable();
				
			});
			
			$("#BtnSrch").jqxButton({ width: '',  theme:'blueish'}).on('click',makeTable); //조회 버튼
 		                      
     }); 
		</script>
		<script>
   			var missionList = [];
   			var init = true;
	   		var missionDat;
	   		var straCombo;
	   		var substraCombo;
	   		function makeMissionCombo(){
			missionDat = $i.sqlhouse(322,{}); 
				missionDat.done(function(res){
					missionList = res.returnArray;
    				var source = 
    				{
	        			datatype:"json",
	        			datafields:[
	        				{ name : "MS_ID" },
	        				{ name : "MS_NM" }
	        			],
	        			id:"id",
	        			localdata:res,
	        			async : false 
	        		};
	        		var dataAdapter = new $.jqx.dataAdapter(source);
	        		 $("#getMission").jqxComboBox({ 
	        			//	selectedIndex: 0, 
	        				source: dataAdapter, 
	        				animationType: 'fade', 
	        				dropDownHorizontalAlignment: 'right', 
	        				displayMember: "MS_NM", 
	        				valueMember: "MS_ID",  
	        				dropDownWidth: 300, 
	        				dropDownHeight: 100, 
	        				width: 300, 
	        				height: 22,  
	        				theme:'blueish'} );
	        		 
	        		$('#getMission').on('select', function (event)  
	        		{
	        			var args = event.args;
	        			if (args) {
	        				var index = args.index;
	        				makeYearCombo(index);
	        				makeStraCombo(index);
	        			}	     
	        		}); 
	        		
	        		 
    			$("#getMission").jqxComboBox('selectIndex', 0 ); 
    			
    		});  
 		}//makeMissionCombo 
 	
	 		function makeYearCombo(idx){
	 			var start = missionList[idx].START_YYYY;
	 			var end = missionList[idx].END_YYYY;
	 			var nowYear = new Date().getFullYear();
	 				if(nowYear >= start && nowYear < end) //현재기준으로 설정 
	 					end = nowYear;
	 			var yyyyList = [];
	 				for(var i=Number(start);i<=end;i++){ 
	 					yyyyList.push(i);
	 				}
	     			$("#getYY").jqxComboBox({  
						source: yyyyList, 
						animationType: 'fade', 
						dropDownHorizontalAlignment: 'right', 
						dropDownWidth: 80, 
						dropDownHeight: 80, 
						width: 80, 
						height: 22,  
						theme:'blueish'});
	 			
	 			$("#getYY").jqxComboBox('selectIndex', yyyyList.length-1 );  
	    	}//makeYearCombo
	    	
	    	var statCombo;
	    	function makeStatCombo(){
	    		statCombo = $i.sqlhouse(399,{COML_COD:"PROGRAM_STATUS"});     
	    		statCombo.done(function(res){  				 
					var dat=res.returnArray; 
					dat.unshift({COM_COD:"Z",COM_NM:"전체"});    				
					 var source =
	             	{
	                     datatype: "json",
	                     datafields: [
	                         { name: 'COML_COD' }, 
	                         { name: 'COM_COD' },
	                         { name: 'COM_NM' }
	                     ],
	                     id: 'id', 
	 					localdata:dat,
	//                     url: url,
	                     async: false
	                 };
	             	var dataAdapter = new $.jqx.dataAdapter(source);
	             	$("#getStat").jqxComboBox({ 
		 				selectedIndex: 0,  
		 				source: dataAdapter, 
		 				animationType: 'fade', 
		 				dropDownHorizontalAlignment: 'right', 
		 				displayMember: "COM_NM",  
		 				valueMember: "COM_COD",        
		 				dropDownWidth: 100,  
		 				dropDownHeight: 100,     
		 				width: 100,  
		 				height: 22,  
		 				theme:'blueish'});
	    			});
					
	    			$("#getStat").jqxComboBox('selectIndex', 0 ); 
	    		}//makeStatCombo 
	    		
	    		var straList = [];
	    		
	    		function makeStraCombo(idx){
	    			straCombo = $i.sqlhouse(344,{MS_ID:missionList[idx].MS_ID });     
	    			straCombo.done(function(res){  
	    				straList = res.returnArray; 
						 var source =
		             	{
		                     datatype: "json",
		                     datafields: [
		                         { name: 'STRA_ID' },
		                         { name: 'STRA_NM' }
		                     ],
		                     id: 'id', 
		 					localdata:res,
		//                     url: url,
		                     async: false
		                 };
		             	var dataAdapter = new $.jqx.dataAdapter(source);
		             	$("#getStra").jqxComboBox({ 
			 			//	selectedIndex: 0,  
			 				source: dataAdapter, 
			 				animationType: 'fade', 
			 				dropDownHorizontalAlignment: 'right', 
			 				displayMember: "STRA_NM",  
			 				valueMember: "STRA_ID",        
			 				dropDownWidth: 250,  
			 				dropDownHeight: 100,     
			 				width: 250,  
			 				height: 22,  
			 				theme:'blueish'});
		             	$('#getStra').on('select', function (event)  
		    	        		{
		    	        			var args = event.args;
		    	        			if (args) {
		    	        				var index = args.index;
		    	        				makeSubstraCombo(index);
		    	        			}	     
		    	        		}); 

		    			$("#getStra").jqxComboBox('selectIndex', 0 ); 
	    			
	    			});
		    			
	    		}//makeStraCombo
	    		
	    		
	    		function makeSubstraCombo(idx){
	    			substraCombo = $i.sqlhouse(345,{ MS_ID: straList[idx].MS_ID, STRA_ID:straList[idx].STRA_ID });     
	    			substraCombo.done(function(res){
						var dat=res.returnArray;
						 var source =
		             	{
		                     datatype: "json",
		                     datafields: [
		                         { name: 'SUBSTRA_ID' },
		                         { name: 'SUBSTRA_NM' }
		                     ],
		                     id: 'id',
		 					localdata:dat,
		//                     url: url,
		                     async: false
		                 };
		             	var dataAdapter = new $.jqx.dataAdapter(source);
		             	$("#getSubstra").jqxComboBox({ 
			 			//	selectedIndex: 0,  
			 				source: dataAdapter, 
			 				animationType: 'fade', 
			 				dropDownHorizontalAlignment: 'right', 
			 				displayMember: "SUBSTRA_NM",  
			 				valueMember: "SUBSTRA_ID",         
			 				dropDownWidth: 250,  
			 				dropDownHeight: 100,     
			 				width: 250,  
			 				height: 22,   
			 				theme:'blueish'});
		             	$("#getSubstra").jqxComboBox('selectIndex', 0 ); 	
		             	if(init==true){
		    				makeTable();
		    				init=false;
		    			}
	    			});	
	    			
	    			
	    		}//makeSubstraCombo
    	</script><!-- ComboBox -->
    	<script>
    		function makeTable(){
    			var data = $i.sqlhouse(343,{MS_ID:$('#getMission').val(), YYYY:$('#getYY').val(), PROGRAM_STATUS:$('#getStat').val(), STRA_ID:$('#getStra').val(), SUBSTRA_ID:$('#getSubstra').val()});
    			$("#makeTable").empty();
    			data.done(function(res){
    				res = res.returnArray;
    				for(var i=0;i<res.length;i++){ 
    					var html ="";  
    					var assignId = res[i].ASSIGN_ID;
    					var programId = res[i].PROGRAM_ID;
    					html += "<tr id='makeTr'>";
    					html += "<input type='hidden' id='assign_id' name='assignId'>"+res[i].ASSIGN_ID;
    					html += "<td align='center' id='assigncd' class='td_side_left' style='width:10%;'>"+res[i].ASSIGN_CD+"</td>"; 
    					html += "<td align='left' id='assignNm' class='borderNone tr_hover odd' style='width:25%; padding: 0 7px;'>"+res[i].ASSIGN_NM+"</td>";
    					html += "<input type='hidden' id='program_id' name='programId'>"+res[i].PROGRAM_ID;
    					html += "<td align='left' id='programNm' class='borderNone tr_hover odd' style='width:25%; padding: 0 7px;'>"+res[i].PROGRAM_NM+"</td>";
    					html += "<td align='center' id='programStat' class='borderNone tr_hover odd' style='width:10%;'>"+res[i].COM_NM+"</td>";
    					html += "<td align='center' id='' class='borderNone tr_hover odd' style='width:10%;'>";
	    					if(res[i].COM_NM == "계획완료"){
	    						html += "<div class='button type2 f_center' style='margin: 4px 0;'>";
	    						html +=	"<input type='button' id='BtnCancel' value='확인' width='100%' height='100%' style='padding:2px 10px 3px 10px !important;' onclick='insertProgramStatConfirm("+ assignId + ","+ programId +");'/></div>"; 
	    					}else if(res[i].COM_NM == "실행확정"){
	    						html += "<div class='button type3 f_center' style='margin: 4px 0;'>";
	    						html +=	"<input type='button' id='BtnCancel' value='취소' width='100%' height='100%' style='padding:2px 10px 3px 10px !important;' onclick='insertProgramStatCancel("+ assignId + ","+ programId +");'/></div>"; 
	    					}
						html +=	"</td>";
    					html += "<td align='center' id='empNm"+i+"' class='borderNone tr_hover odd' style='width:10%;'>"+res[i].EMP_NM+"</td>";
    					html += "<td align='center' id='evalEmpNm"+i+"' class='borderNone tr_hover odd' style='width:80%;'>"+res[i].EVAL_EMP_NM+"</td>";
    					html += "</tr>";
    					
    					$("#makeTable").append(html);
    				}
    			});
    	}
    		
    	function insertProgramStatConfirm(assignId, programId){
    		var msId = $('#getMission').val();
    		var yyYy = $('#getYY').val(); 
    		var a = $i.post("./insert1",{ ms_id : msId, yy_yy: yyYy, assign_id : assignId, program_id : programId});
			a.done(function(args){
      			if(args.returnCode == "EXCEPTION"){ 
      				$i.dialog.error("SYSTEM",args.returnMessage);
      			}else{
      				$i.dialog.alert("SYSTEM","저장 되었습니다.",function(){
      					makeTable();
      				});
      			}
      		}).fail(function(e){ 
      			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
      		});
      	}
    	
    	function insertProgramStatCancel(assignId, programId){
    		var msId = $('#getMission').val();
    		var yyYy = $('#getYY').val(); 
    		var a = $i.post("./insert2",{ ms_id : msId, yy_yy: yyYy, assign_id : assignId, program_id : programId});
			a.done(function(args){
      			if(args.returnCode == "EXCEPTION"){ 
      				$i.dialog.error("SYSTEM",args.returnMessage);
      			}else{
      				$i.dialog.alert("SYSTEM","저장 되었습니다.",function(){
      					makeTable();
      				});
      			}
      		}).fail(function(e){ 
      			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
      		});
      	}
    	
    	</script>
   	</head>
	<body class='blueish'>
   		<div class="wrap" style="width:98%; min-width:1580px; margin:0 1%;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<form id="srchFrm" name="srchFrm"> 
					<div class="label type1 f_left">
						비젼:
					</div>
					<div class="combobox f_left"  id='getMission' >  
					</div>
					<div class="label type1 f_left">
						년도:
					</div>
					<div class="combobox f_left"  id='getYY' > 
					</div>
					<div class="label type1 f_left">
						평가상태:
					</div> 
					<div class="combobox f_left"  id='getStat' > 
					</div>
					<div class="label type1 f_left">
						전략목표:
					</div>
					<div class="combobox f_left"  id='getStra' >
					</div>
					<div class="label type1 f_left">
						세부전략목표:
					</div>
					<div class="combobox f_left"  id='getSubstra' >  
					</div>
					<div class="group_button f_right"> 
						<div class="button type1 f_left">
							<input type="button" value="조회" id='BtnSrch' width="100%" height="100%" />  
						</div>
					</div>
				</form> <!-- //srchFrm -->
			</div><!-- //header -->
				<div class="container  f_left" style="width:100%; margin:10px 0;">
					<div class="content f_left" style=" width:100%; margin-right:1%;">
							<div class="label type2 f_right">
								계획완료 → 실행확정 → 운영중 → 운영완료 → 평가완료
							</div>
						<div class="blueish datatable f_left" style="width:100%; height:450px; margin:5px 0; overflow:hidden;">      
								<!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 헤더가 50px일경우, 300px - 50px= 250px)-->
								<div class="datatable_fixed" style="width:100%; height:450px; float:left; padding:25px 0;">
									<!--group으로 헤더높이가 길어질 경우 padding  (기본높이는 25px)줘야 함-->
									<div style=" height:425px; !important; overflow-x:hidden;">  
										<form id="saveForm">
										<table  width="100%" cellspacing="0" cellpadding="0" border="0">
											<thead>
												<tr>
													<th style="width:10%">추진과제코드</th>
													<th style="width:25%">추진과제명</th>
													<th style="width:25%">프로그램</th>
													<th style="width:10%">상태</th>
													<th style="width:10%">확정여부</th>
													<th style="width:10%">담당자</th>
													<th style="width:80%">부서평가자</th>
													<th style="width:17px; min-width:17px; border-bottom:0;"></th>
												</tr>
											</thead> 	
											<tbody id="makeTable">
											</tbody>
										</table>
									</form>
									</div>
								</div>
						</div>
					</div><!-- content -->
					<input type='hidden' name="assignId"/>
					<input type='hidden' name="programId"/> 
				</div><!-- //container -->	
		</div><!-- //wrap -->	
   </body>
</html>