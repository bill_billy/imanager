<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <title>프로그램관리 </title>
      	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/widget/Form.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
        <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <script src="../../resources/cmresource/js/thirdparty/LGPL/v2/smartEditor2/js/HuskyEZCreator.js" charset="UTF-8"></script>
		<script type="text/javascript">// button
            $(this).ready(function () {               
	        	init();
	        	$('#saveForm0').jqxValidator({
    				rtl:true,  
    				rules:[
    				    { input: "#program_nm", message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' },
                        { input: '#txtAssignNm', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' },
                        //{ input: '#txtKpiId', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' },
                        { input: '#txtEmpnm', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' },
                        { input: '#txtEmpnm_c', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' },
                        { input: '#txtEmpnm_e', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' },
                        { input: '#sch_start_dat', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' },
                        { input: '#sch_end_dat', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' }
    		        ]
    			});
	        	$('#saveForm3').jqxValidator({
    				rtl:true,  
    				rules:[
    					{ input: "#quan_ach_val", message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'number' },
    				  //  { input: "#cnt_tot", message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'number' },
                        { input: '#cnt_a', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'number' },
                      	{ input: '#cnt_b', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'number' },
                        { input: '#cnt_c', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'number' },
                        { input: '#cnt_d', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'number' },
                        { input: '#cnt_e', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'number' }
                   ]
    			});
	        	
            });
            function init(){
            	var assignId = $("#assign_id").val();
            	$("#jqxButtonSearch").jqxButton({ width: '',  theme:'blueish'}).on('click',function(){search();}); 
 				$("#jqxButtonFind01").jqxButton({ width: '',  theme:'blueish'}); 
 				$("#newProgramBtn").jqxButton({ width: '',  theme:'blueish'}).on('click',function(){resetForm0();makeProgramGrid($("#assign_id").val());}); 
 				$("#saveProgramBtn").jqxButton({ width: '',  theme:'blueish'}).on('click',function(){saveForm0($("#assign_id").val());});
 				$("#delProgramBtn").jqxButton({ width: '',  theme:'blueish'}).on('click',function(){deleteForm0($("#assign_id").val());}); 
 				$("#confirmBudgetBtn").jqxButton({ width: '',  theme:'blueish'}).on('click',function(){confirmForm1($("#assign_id").val());}); 
 				$("#cancleBudgetBtn").jqxButton({ width: '',  theme:'blueish'}).on('click',function(){cancleForm1($("#assign_id").val());}); 
 				$("#saveBudgetBtn").jqxButton({ width: '',  theme:'blueish'}).on('click',function(){saveForm1($("#assign_id").val());}); 
 				$("#confirmEnforceBtn").jqxButton({ width: '',  theme:'blueish'}).on('click',function(){confirmForm2($("#assign_id").val());});
 				$("#cancleEnforceBtn").jqxButton({ width: '',  theme:'blueish'}).on('click',function(){cancleForm2($("#assign_id").val());});
 				$("#saveEnforceBtn").jqxButton({ width: '',  theme:'blueish'}).on('click',function(){saveForm2($("#assign_id").val());}); ;
 			
 				$("#deptEvalCancleBtn").jqxButton({ width: '',  theme:'blueish'}).on('click',function(){cancleEval($("#assign_id").val());}); 
 				$("#deptEvalConfirmBtn").jqxButton({ width: '',  theme:'blueish'}).on('click',function(){confirmForm3($("#assign_id").val());}); 
 				$("#deptEvalSaveBtn").jqxButton({ width: '',  theme:'blueish'}).on('click',function(){saveForm3($("#assign_id").val());}); 
 				$("#program_status").jqxDropDownList({animationType: 'fade',dropDownHorizontalAlignment: 'right',dropDownWidth: 150,width: 100,dropDownHeight:100,height: 23,theme:'blueish' ,disabled: true});
 				$('#jqxTabs01').jqxTabs({ width: '100%', height: '100%', position: 'top', theme:'blueish', selectedItem: 0});
 				$('#jqxTabs02').jqxTabs({ width: '100%', height: '100%', position: 'top',theme:'blueish', selectedItem: 0});
 				$('#jqxTabs03').jqxTabs({ width: '100%', height: '100%', position: 'top',theme:'blueish', selectedItem: 0});
 				
 				/* $('#jqxTabs03').click(function () {
 				     $('#jqxTabs03').jqxTabs('focus');
 				 });*/
 				
 				$("#quan_unit").jqxDropDownList({animationType: 'fade',dropDownHorizontalAlignment: 'right',dropDownWidth: 90,dropDownHeight:100,width: 90,height: 23,theme:'blueish'});
 				$(".budgetType").jqxDropDownList({animationType: 'fade',dropDownHorizontalAlignment: 'right',dropDownVerticalAlignment:'top',dropDownWidth: 150,dropDownHeight:100,width: 150,height: 23,theme:'blueish'});
 			
 				$("#eval_score").jqxDropDownList({animationType: 'fade',dropDownHorizontalAlignment: 'right',dropDownHeight:120,dropDownWidth: 100,width:100,height: 23,theme:'blueish'});
 				
 				$('#jqxTabs02').on('selected', function (event) 
 				{ 
 					$('#saveForm0').jqxValidator('hide');
 					$('#saveForm1').jqxValidator('hide');
 					$('#saveForm3').jqxValidator('hide');
 					var tabIdx = event.args.item;
 					var gridIdx = $('#programGrid').jqxGrid('getselectedrowindex');
 					var programId=-1;
 					if(tabIdx==1){//계획탭
 						var size = $("#content").next("iframe").length;
 						if(size<1){
 							if(gridIdx!=-1){
 		 						programId = $("#programGrid").jqxGrid('getrowdata',gridIdx).PROGRAM_ID;
 		 					}
 							initSmartEditor('content',gridIdx,programId);
 							return;
 						}

 					}else if(tabIdx==2){//운영탭
 						var size = $("#result").next("iframe").length;
 						if(size<1){
 							if(gridIdx!=-1){
 		 						programId = $("#programGrid").jqxGrid('getrowdata',gridIdx).PROGRAM_ID;
 		 					}
 							initSmartEditor('result',gridIdx,programId);
 							return;
 						}
 					}else{}
 					
 					if(gridIdx!=-1){
 						programId = $("#programGrid").jqxGrid('getrowdata',gridIdx).PROGRAM_ID;
 						detailProgram(gridIdx,programId);
 					}
 					
 				}); 
 				
 				
				// 입력시작일, 입력종료일
				$('#sch_start_dat, #sch_end_dat').datepicker({
					//changeYear: true,
					//changeMonth: true,
					showOn: 'button',
					buttonImage: '../../resources/cmresource/image/icon-calendar.png',
					buttonImageOnly: true,
					dateFormat: 'yymmdd', //2014.12.12
					onSelect:function(dateText){
						if($("#sch_end_dat").val() != ""){
							if($("#sch_end_dat").val() < $("#sch_start_dat").val()){
								$i.dialog.warning("SYSTEM","종료일자가 시작일자보다 과거입니다.");
								$("#sch_end_dat").val("");
							}	
						}
					}
				});
				
				$('.dateInput').datepicker({
					//changeYear: true,
					//changeMonth: true,
					showOn: 'button',
					buttonImage: '../../resources/cmresource/image/icon-calendar.png',
					buttonImageOnly: true,
					dateFormat: 'yymmdd' //2014.12.12
				});
				$('.ui-datepicker-trigger').addClass('icon-calendar f_left m_t7 pointer');
				
				 $("#txtSearchWord").on('keydown',function() {
						if(event.keyCode==13) {
							makeAssignSearch();
							return false;
						}
					}
				);
 				makeMissionCombo();
 				search();
 				makeProgramGrid();
            }
            function byteCheck(code) {
    			var size = 0;
    			for (i = 0; i < code.length; i++) {
    				var temp = code.charAt(i);
    				if (escape(temp) == '%0D')
    			   		continue;
    			  	if (escape(temp).indexOf("%u") != -1) {
    			   		size += 3;
    			  	} else {
    			   		size++;
    			 	}
    		 	}
    		 	return size;
    		}
        	function fRemoveHtmlTag(string) { 
     		   var objReplace = new RegExp();
     		   var objnbsp = new RegExp();
     		   objReplace = /[<][^>]*[>]/gi; 
     		   objnbsp = /&nbsp;/gi;
     		   return string.replace(objReplace, "").replace(objnbsp,"").replace(/(\s*)/g, ""); 
        	}
        	var oEditors = [];
     		function initSmartEditor(id,gridIdx,programId) {
     			nhn.husky.EZCreator.createInIFrame({
    				oAppRef : oEditors,
    				elPlaceHolder : id,
    				sSkinURI : "../../resources/cmresource/js/thirdparty/LGPL/v2/smartEditor2/SmartEditor2Skin.html",
    				htParams : {
    					bUseToolbar : true,
    					fOnBeforeUnload : function() {
    					}
    				},
    				fOnAppLoad : function() {
    					if(gridIdx!=-1 && programId!=-1)
    						detailProgram(gridIdx,programId);
    				},
    				fCreator : "createSEditor2"
    			});
     		}
     		
            var missionList = [];
	    	var missionDat;
			function makeMissionCombo(){
				missionDat = $i.sqlhouse(322,{});
				missionDat.done(function(res){
					missionList = res.returnArray;
					missionList.forEach(function(dataOne) {
	    				dataOne.MS_NM = $i.secure.scriptToText(dataOne.MS_NM);
	    			});
					
	        		var source = 
	        		{
	        			datatype:"json",
	        			datafields:[
	        				{ name : "MS_ID" },
	        				{ name : "MS_NM" }
	        			],
	        			id:"id",
	        			localdata:res,
	        			async : false
	        		};
	        		var dataAdapter = new $.jqx.dataAdapter(source);
	        		 $("#jqxCombobox01").jqxComboBox({ 
	        			//	selectedIndex: 0, 
	        				source: dataAdapter, 
	        				animationType: 'fade', 
	        				dropDownHorizontalAlignment: 'right', 
	        				displayMember: "MS_NM", 
	        				valueMember: "MS_ID", 
	        				dropDownWidth: 300, 
	        				dropDownHeight: 100, 
	        				width: 300, 
	        				height: 22,  
	        				theme:'blueish'});
	        		 
	        		$('#jqxCombobox01').on('select', function (event) 
	        		{
	        			var args = event.args;
	        			if (args) {                  
	        				var index = args.index;
	        				makeYearCombo(index);
	        				
	        			}	     
	        		}); 
	        		$("#jqxCombobox01").jqxComboBox('selectIndex', 0 ); 
	        		
	        	});  
	     	}//makeMissionCombo
	     	function makeYearCombo(idx){
	     		var start = missionList[idx].START_YYYY;
	     		var end = missionList[idx].END_YYYY;
	     		
	     		var nowYear = new Date().getFullYear();
	     		if(nowYear >= start && nowYear < end) //현재기준으로 설정
	     			end = nowYear;
	     		
	     		var yyyyList = [];
	     		for(var i=Number(start);i<=end;i++){
	     			yyyyList.push(i);
	     		}
	     		$("#jqxCombobox02").jqxComboBox({ 
					source: yyyyList, 
					animationType: 'fade', 
					dropDownHorizontalAlignment: 'right', 
					dropDownWidth: 80, 
					dropDownHeight: 80, 
					width: 80, 
					height: 22,  
					theme:'blueish'});
	     		$("#jqxCombobox02").jqxComboBox('selectIndex', yyyyList.length-1 );  
	        }//makeYearCombo
	        
	        function makeStraListTree(straid,substraid){
				//tree초기화
				var div = $("#jqxTree01").parent();
				 $("#jqxTree01").jqxTree('destroy');
				 div.append("<div id='jqxTree01' class='tree'/>");
				 
	    		var dat = $i.sqlhouse(389, {MS_ID:$("#ms_id").val(),YYYY:$("#yyyy").val()});
	    		dat.done(function(res){
	    			
	    			res = res.returnArray;
	    			res.forEach(function(dataOne) {
	    				dataOne.STRA_NM = $i.secure.scriptToText(dataOne.STRA_NM);
	    			});
	    			
	    			var source =
					{
		                datatype: "json",
		                datafields: [
		                    { name: 'P_ID'},
		                    { name: 'C_ID'},
		                    { name: 'STRA_NM'},
		                    { name: 'STRA_ID'},
		                    { name: 'SUBSTRA_ID'}
		                ],
		                id : 'C_ID',
		                localdata: res
		            };
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            dataAdapter.dataBind();
		            
		            var records = dataAdapter.getRecordsHierarchy('C_ID', 'P_ID', 'items', [{ name: 'C_ID', map: 'id'} ,{ name: 'STRA_NM', map: 'label'}, { name:'STRA_ID', map:'value'}]);
		            $('#jqxTree01').jqxTree({source: records, height: '100%', width: '100%', theme:'blueish',allowDrag: false, allowDrop: false });
		            
					// tree init
		            var items = $('#jqxTree01').jqxTree('getItems');
		            var item = null;
		            var size = 0;
		            var img = '';
		            var label = '';
		            var afterLabel = '';
		            
					for(var i = 0; i < items.length; i++) {
						item = items[i];
						
						if(i == 0) {
							//root
							if(straid==undefined && substraid==undefined){
								$("#jqxTree01").jqxTree('expandItem', item);
							}
							img = 'icon-foldernoopen.png';
							afterLabel = "";
						} else {
							//children
							size = $(item.element).find('li').size();
							
							if(size > 0) {
								//have a child
								var itemId = item.id;
								if(itemId.indexOf("ASSIGN")==-1){
									img = 'icon-foldernoopen.png';
									if(itemId.indexOf("SUB")==-1){
										afterLabel = "";
									}else{
										afterLabel = "<span style='color: Blue;'> (" + size + ")</span>";
									}
								}
							} else {
								var itemId = item.id;
								if(itemId.indexOf("ASSIGN")==-1){
									img = 'icon-foldernoopen.png';
								}else{
									img = 'icon-page.png';
								}
								//no have a child
								//내부에서 사용하는 트리는 팝업 없음.
								//afterLabel = "<span style='color: Blue; margin-left:5px; vertical-align:middle;'> <img src='${WWW.CSS}/images/img_icons/popupIcon.png' alt='팝업창' title='새창보기'/></span>";
								afterLabel = "";
							}
						}
						
						label = "<img style='float: left; margin-right: 5px;' src='../../resources/cmresource/image/" + img + "'/><span item-title='truec style='vertical-align:middle;'>" + $i.secure.scriptToText(item.label) + "</span>" + afterLabel;
						$('#jqxTree01').jqxTree('updateItem', item, { label: label});
					}
		            
		            //add event
		            $('#jqxTree01')
		            .on("expand", function(eve) {
		            	var args = eve.args;
		            	var label = $('#jqxTree01').jqxTree('getItem', args.element).label.replace('icon-foldernoopen', 'icon-folderopen');
		            	args.element.firstChild.className = args.element.firstChild.className.replace('jqx-tree-item-arrow-collapse', '').replace('jqx-icon-arrow-right', '');
		            	$('#jqxTree01').jqxTree('updateItem', args.element, { label: label});
		            })
		            .on("collapse", function(eve) {
		            	var args = eve.args;
		            	var label = $('#jqxTree01').jqxTree('getItem', args.element).label.replace('icon-folderopen', 'icon-foldernoopen');
		            	$('#jqxTree01').jqxTree('updateItem', args.element, { label: label});
		            })       
		            .on("select", function(eve) {
		            	var args = eve.args;
		            	var pid = $("#jqxTree01").jqxTree("getItem", args.element).parentId;
		            	var cid = $("#jqxTree01").jqxTree("getItem", args.element).id;
		            	//var pValue = $("#jqxTree01").jqxTree("getItem", args.parentElement).value;
		            	var value = $("#jqxTree01").jqxTree("getItem", args.element).value;
		            
		            	$("#txtTabSubstr").val(cid);
		            	if(pid==0){//전략
		            	}else{
		            		if(isNaN(pid)){//추진
		            			//var ppValue = $("#jqxTree01").jqxTree("getItem", args.parentElement).value;
		            		
		            			var idx = pid.indexOf('SUB');
		            			var id1=pid.substr(0,idx);
		            			var id2=pid.substr(parseInt(idx)+3);
		            			getTitle(id1,id2);
		            			makeAssignCombo(2,value);
		            		}else{//세부
		            			var idx = cid.indexOf('SUB')+3;
		            			getTitle(pid,cid.substr(idx));
		            			makeAssignCombo(1,pid,value,selectAssignCombo);
		            		}
		            	}
		            });
		            if(straid!=undefined && substraid!=undefined){
			            $('#jqxTree01').jqxTree('expandItem',$("#jqxTree01").find('#'+straid)[0]);
			      
			            $('#jqxTree01').jqxTree('selectItem',$("#jqxTree01").find('#'+straid+'SUB'+substraid)[0]);
		            }
	    		});
	    	}//makeStrategyTree
	        
	    	function getTitle(straid,substraid){
	    		var dat = $i.sqlhouse(408, {MS_ID:$("#ms_id").val(),STRA_ID:straid,SUBSTRA_ID:substraid});
	    		dat.done(function(res){
	    			res = res.returnArray[0];
	    			
	    			var html = $i.secure.scriptToText(res.STRA_NM) + '<span class="label sublabel type2">'+$i.secure.scriptToText(res.SUBSTRA_CD+' '+res.SUBSTRA_NM)+'</span><span class="label sublabel type2" style="margin-right:0;">&nbsp;</span>'
	    			$("#selectStraNm").html(html);
	    		});
	    	}
	    	
	    	
			function makeAssignSearch(){
				var ms_id= $("#ms_id").val();
				var yyyy = $("#yyyy").val();
				var keyword = $("#txtSearchWord").val();
		           
				var div = $("#searchGrid").parent();
				 $("#searchGrid").jqxGrid('destroy');
				 div.append("<div id='searchGrid'/>");
				 
				var dat = $i.sqlhouse(394,{MS_ID:ms_id,YYYY:yyyy,KEYWORD:keyword});
				dat.done(function(res){
	   				var data = res.returnArray;
	               	var source =
	               	{
		                   datatype: "json",
		                   datafields: [
		                       { name: 'MS_ID', type: 'string' },
		                       { name: 'ASSIGN_ID', type: 'string' },
		                       { name: 'ASSIGN_CD', type: 'string' },
		   					   { name: 'ASSIGN_NM', type: 'string'},
		   						{ name: 'STRA_ID', type: 'string'},
		   						{ name: 'SUBSTRA_ID', type: 'string'},
		   						{ name: 'PROGRAM_NM', type: 'string'},
		   						{ name: 'PROGRAM_ID', type: 'string'}
		                   ],
		                   localdata: data,
		                   updaterow: function (rowid, rowdata, commit) {
		                       commit(true);
		                   }
	               	};
	               	var noScript = function (row, columnfield, value) {
	               		var newValue = $i.secure.scriptToText(value);
	                       return '<div id="userName-' + row + '"style="text-align: center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
								
	                  }
	               	var alignLeft = function (row, columnfield, value) {
	               		var assignCd = $("#searchGrid").jqxGrid('getrowdata',row).ASSIGN_CD;
	               		var newValue = $i.secure.scriptToText(assignCd)+' '+$i.secure.scriptToText(value);
	                    return '<div id="userName-' + row + '"style="text-align: left; margin:4px 0px 0px 10px;" title="'+value+'">' + newValue + '</div>';
								
	                  } 
	   				var detail = function (row, columnfield, value, rowData) {
						var assign_id = $("#searchGrid").jqxGrid('getrowdata',row).ASSIGN_ID;
						var newValue = $i.secure.scriptToText(value);
		                return '<div id="userName-' + row + '" onclick="searchGridClick('+row+','+assign_id+');" title="'+value+'" style="text-align:left; margin:4px 0px 0px 10px; cursor:pointer; text-decoration:underline;">' + newValue + '</div>';
					}	     
					var dataAdapter = new $.jqx.dataAdapter(source, {
						downloadComplete: function (data, status, xhr) { },
							loadComplete: function (data) { },
							loadError: function (xhr, status, error) { }
					});
					var dataAdapter = new $.jqx.dataAdapter(source);
					$("#searchGrid").jqxGrid(
					{
	                 		width: '100%',
	                 		height: '100%',
	            			altrows:true,
	            			pageable: false,
	            			source: dataAdapter,
	            			theme:'blueish',
	            			columnsheight:25 ,
	            			rowsheight: 25,
	            			sortable:true,
	            			columnsresize: true,
	                        columns: [
	                            { text: '추진과제', datafield: 'ASSIGN_NM', width: 150, align:'center', cellsalign: 'left' ,  cellsrenderer: alignLeft },
	                            { text: '프로그램명', datafield: 'PROGRAM_NM', width: 200, align:'center', cellsalign: 'left' ,  cellsrenderer: detail}
	                            
	                        ]
	                 });
	   	
	       		});   
	       }
			function searchGridClick(row,assignId){
				var straid = $("#searchGrid").jqxGrid('getrowdata',row).STRA_ID;
				var substraid = $("#searchGrid").jqxGrid('getrowdata',row).SUBSTRA_ID;
				getTitle(straid,substraid);
				makeAssignCombo(2,assignId);
			}
	    	
	    	var selectAssignCombo = "";
	    	function search(straid,substraid,assignid){
	    		if(assignid != undefined)
	    			selectAssignCombo = assignid;
	    		var div = $("#selectStraNm").parent();
				 $("#jqxAssignCombo").jqxComboBox('destroy');
				 div.append('<div class="combobox f_left"  id="jqxAssignCombo" style="margin-left:5px;" ></div>');
	    	
	    		$("#selectStraNm").html("전락목표");
	    		
	       		missionDat.done(function(){
	       			$("#ms_id").val($("#jqxCombobox01").val());
	       			$("#ms_id0").val($("#jqxCombobox01").val());
	       			$("#ms_id1").val($("#jqxCombobox01").val());
	       			$("#ms_id2").val($("#jqxCombobox01").val());
	       			$("#ms_id3").val($("#jqxCombobox01").val());
	       			$("#ms_id4").val($("#jqxCombobox01").val());
	       			
	           		$("#yyyy").val($("#jqxCombobox02").val());
	           		$("#yyyy0").val($("#jqxCombobox02").val());
	           		$("#yyyy1").val($("#jqxCombobox02").val());
	           		$("#yyyy2").val($("#jqxCombobox02").val());
	           		$("#yyyy3").val($("#jqxCombobox02").val());
	           		$("#yyyy4").val($("#jqxCombobox02").val());

	           		//좌측 TAB영역
	           		makeStraListTree(straid,substraid);
	    			makeAssignSearch();
	    			
	    			//우측TAB영역
	           		//makeAssignCombo();
	           		makeProgramGrid();
	       		});
	       	}
	    	
	    	function makeAssignCombo(type,keyword,keyword2,keyword3){
	        	var div = $("#selectStraNm").parent();
				 $("#jqxAssignCombo").jqxComboBox('destroy');
				 div.append('<div class="combobox f_left"  id="jqxAssignCombo" style="margin-left:5px;" ></div>');
	        	
				 
	        	var ms_id= $("#ms_id").val();
	            var yyyy = $("#yyyy").val();
	            var searchS = "";
	            var searchA = "";
	            var searchD = "";
	            var dat = null;
	            if(type==1)//substra
	            	dat = $i.sqlhouse(410,{MS_ID:ms_id,YYYY:yyyy,STRA_ID:keyword,SUBSTRA_ID:keyword2});
	            else if(type==2)//assignment
	            	dat = $i.sqlhouse(411,{MS_ID:ms_id,YYYY:yyyy,ASSIGN_ID:keyword});
	            
	    		dat.done(function(res){
	    			var data = res.returnArray;
	    			var arr = [];
	    			
	                if(data.length<=0){
	                	makeProgramGrid();
	                	return;
	                } 
	                	
	                
	                data.forEach(function(dataOne) {
	    				dataOne.ASSIGN_NM = $i.secure.scriptToText(dataOne.ASSIGN_NM);
	    				arr.push(dataOne);
	    			});
	                
	                	var source = 
		        		{
		        			datatype:"json",
		        			datafields:[
		        				{ name : "ASSIGN_ID" },
		        				{ name : "ASSIGN_NM" }
		        			],
		        			id:"id",
		        			localdata:arr,
		        			async : false
		        		};
		        		var dataAdapter = new $.jqx.dataAdapter(source);
		        		 $("#jqxAssignCombo").jqxComboBox({ 
		        				//selectedIndex: 0, 
		        				source: dataAdapter, 
		        				animationType: 'fade', 
		        				dropDownHorizontalAlignment: 'right', 
		        				displayMember: "ASSIGN_NM", 
		        				valueMember: "ASSIGN_ID", 
		        				dropDownWidth: 450, 
		        				dropDownHeight: 150, 
		        				width: 450, 
		        				height: 22,  
		        				theme:'blueish'});
		        		 
		        		 $('#jqxAssignCombo').on('select', function (event) 
		     	        		{
		     	        			var args = event.args;
		     	        			if (args) {             
		     	        				var value = args.item.value;
		     	        				makeProgramGrid(value);
		     	        				$("#assign_id").val(value);
		     	        				$("#assign_id0").val(value);
		     	        				$("#assign_id1").val(value);
		     	        				$("#assign_id2").val(value);
		     	        				$("#assign_id3").val(value);
		     	        			}	     
		     	        		}); 
		        		 
		        		 if(keyword3!=undefined && keyword3!=""){
		        		 	$("#jqxAssignCombo").jqxComboBox('selectItem', keyword3 );
		        		 	selectAssignCombo = "";
		        		 }else{
		        			 $("#jqxAssignCombo").jqxComboBox('selectIndex', 0 );
		        		 }
		        		 if(type==undefined)
		        		 	$("#selectStraNm").html("추친과제명:&nbsp;");
	        	});
	        	
	        }//makeAssignCombo
	        
	        function makeProgramGrid(assignId,selectedRow){
	        	
	        	resetForm0();
	    		resetForm1();
	    		resetForm2();
	    		resetForm3();
	    		resetForm4();
	    		$("#selectProgramNm").html("추진과제명");
	        	$("#programGrid").jqxGrid('clearselection');
	        	var ms_id= $("#ms_id").val();
	            var yyyy = $("#yyyy").val();
	            
	            if(assignId== undefined) assignId = ''; 
	        	
	        	var dat = $i.sqlhouse(412,{MS_ID:ms_id,YYYY:yyyy,ASSIGN_ID:assignId});
	    		dat.done(function(res){
	    			var data = res.returnArray;
	    			
		        	var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                    	{ name: 'ASSIGN_ID', type: 'string' },
	                    	{ name: 'ASSIGN_CD', type: 'string' },
	                        { name: 'ASSIGN_NM', type: 'string' },
	                        { name: 'PROGRAM_ID', type: 'string' },
	                        { name: 'PROGRAM_NM', type: 'string' },
	                        { name: 'PROGRAM_STATUS', type: 'string' },
	    					{ name: 'DEPT_CD', type: 'string'},
	    					{ name: 'DEPT_NM', type: 'string' },
	    					{ name: 'BUDGET_AMOUNT', type: 'number'},
	    					{ name: 'ENFORCE_AMOUNT', type: 'number'},
	    					{ name: 'CHANGE_AMOUNT', type: 'number'}
	    					
	                    ],
	                    localdata: data,
	                    updaterow: function (rowid, rowdata, commit) {
	                        commit(true);
	                    }
	                };
	    			
	    			var noScript = function (row, columnfield, value) {//left정렬
	    				var newValue = $i.secure.scriptToText(value);
	                     return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
	    							
	                } 
	    			var alignLeft = function (row, columnfield, value) {//left정렬
	    				var assignCd = $("#programGrid").jqxGrid('getrowdata',row).ASSIGN_CD;
	    				var newValue = $i.secure.scriptToText(assignCd)+' '+$i.secure.scriptToText(value);
	                     return '<div id="userName-' + row + '"  title="'+value+'" style="text-align:left; margin:4px 0px 0px 10px;">' + newValue + '</div>';
	    							
	                } 
	    			var alignRight = function (row, columnfield, value) {//left정렬
	    				var newValue = numberWithCommas(value);
	                    return '<div id="userName-' + row + '"style="text-align:right; margin:4px 4px 0px 0px;">' + newValue + '</div>';
	   							
	               }  
	
	    			var goDetail = function (row, columnfield, value, rowData) {
						var program_id = $("#programGrid").jqxGrid('getrowdata',row).PROGRAM_ID;
	                    
						
	                    return '<div id="userName-' + row + '"onclick="detailProgram('+row+','+program_id+');" title="'+value+'" style="text-align:left; margin:4px 0px 0px 10px; cursor:pointer; text-decoration:underline;">' + $i.secure.scriptToText(value) + '</div>';
	
	                }	     
	                var dataAdapter = new $.jqx.dataAdapter(source, {
	
	                    downloadComplete: function (data, status, xhr) { },
	
	                    loadComplete: function (data) { },
	
	                    loadError: function (xhr, status, error) { }
	
	                });
	
	                var dataAdapter = new $.jqx.dataAdapter(source);
	
	                $("#programGrid").jqxGrid(
	                {
	                  		width: '100%',
			    			height: '100%',
			    			altrows:true,
			    			pageable: false,
			    			source: dataAdapter,
			    			theme:'blueish',
			    			columnsheight:25 ,
			    			rowsheight: 25,
			    			columnsresize: true,
			    			sortable:true,
		                   columns: [
		                      { text: '추진과제', datafield: 'ASSIGN_NM', width: '20%', align:'center', cellsalign: 'left',cellsrenderer:alignLeft},
		                      { text: '프로그램', datafield: 'PROGRAM_NM', width: '30%', align:'center', cellsalign: 'left' ,  cellsrenderer: goDetail },
		                      { text: '상태', datafield: 'PROGRAM_STATUS', width: '10%', align:'center', cellsalign: 'center'},
		    				  { text: '주관부서', datafield: 'DEPT_NM', width: '10%', align:'center', cellsalign: 'center',cellsrenderer:noScript},
		    				  { text: '예산액(천원)', datafield: 'BUDGET_AMOUNT', width: '10%', align:'center', cellsalign: 'center',cellsrenderer:alignRight},
		    				  { text: '집행액(천원)', datafield: 'ENFORCE_AMOUNT', width: '10%', align:'center', cellsalign: 'center',cellsrenderer:alignRight},
		    				  { text: '잔액(천원)', datafield: 'CHANGE_AMOUNT', width: '10%', align:'center', cellsalign: 'center',cellsrenderer:alignRight}
		                    ]
	                });
	                
	                if(selectedRow!=undefined){
	                	 $("#programGrid").jqxGrid('selectrow', selectedRow);
	  					programId = $("#programGrid").jqxGrid('getrowdata',selectedRow).PROGRAM_ID;
	  					detailProgram(selectedRow,programId);
	  					
	                }
	    		});
	        }
	        
	        function detailProgram(row,programId){
	        	$("#program_id0").val(programId);
	        	$("#program_id1").val(programId);
	        	$("#program_id2").val(programId);
	        	$("#program_id3").val(programId);
	        	$("#program_id4").val(programId);
	        	
	        	var ms_id=$("#ms_id").val();
	        	var assignCd = $("#programGrid").jqxGrid('getrowdata',row).ASSIGN_CD;
	        	var assignNm = $("#programGrid").jqxGrid('getrowdata',row).ASSIGN_NM;
	        	var programNm = $("#programGrid").jqxGrid('getrowdata',row).PROGRAM_NM;
	        	
	        	var html = $i.secure.scriptToText(assignCd+" "+assignNm)+'<span class="label sublabel type2">'+$i.secure.scriptToText(programNm)+'</span>';
	        	
	        	$("#selectProgramNm").html(html);
	        	
	        	
	        	var tabIdx = $('#jqxTabs02').jqxTabs('selectedItem');
	        	if(tabIdx==0){//기본
	        		detailProgramInfo(ms_id,row);
	        	}else if(tabIdx==1){
	        		detailProgramPlan(ms_id,row);
	        	}else if(tabIdx==2){
	        		detailProgramRun(ms_id,row);
	        	}else if(tabIdx==3){
	        		detailProgramDeptEval(ms_id,row);
	        	}else if(tabIdx==4){
	        		detailEvalConfirm(ms_id,row);
	        	}
	        	
	        }
	        function detailProgramInfo(ms_id,row){
	        	var programId = $("#program_id0").val();
	        	var dat = $i.sqlhouse(414,{MS_ID:ms_id,PROGRAM_ID:programId});
	    		dat.done(function(res){
	    			var programData = res.returnArray[0];
	    			
	    			$("#program_nm").val(programData.PROGRAM_NM);
	    			$("#changeAssignment").val("N");
		    		$("#txtAssignId").val(programData.ASSIGN_ID);
		    		$("#txtAssignNm").val(programData.ASSIGN_NM);
		    		$("#txtStraId").val(programData.STRA_ID);
		    		$("#txtSubstraId").val(programData.SUBSTRA_ID);
		    		$("#txtDeptcd").val(programData.DEPT_CD);
		    		$("#txtDeptnm").val(programData.DEPT_NM);
		    		$("#txtEmpno").val(programData.EMP_NO);
		    		$("#txtEmpnm").val(programData.EMP_NM);
		    		$("#txtDeptcd_c").val(programData.CONFIRM_DEPT_CD);
		    		$("#txtDeptnm_c").val(programData.CONFIRM_DEPT_NM);
		    		$("#txtEmpno_c").val(programData.CONFIRM_EMP_NO);
		    		$("#txtEmpnm_c").val(programData.CONFIRM_EMP_NM);
		    		$("#txtDeptcd_e").val(programData.EVAL_DEPT_CD);
		    		$("#txtDeptnm_e").val(programData.EVAL_DEPT_NM);
		    		$("#txtEmpno_e").val(programData.EVAL_EMP_NO);
		    		$("#txtEmpnm_e").val(programData.EVAL_EMP_NM);
		    		$("#program_status").val(programData.PROGRAM_STATUS);
		    		if(programData.PROGRAM_STATUS=="")
		    			$("#program_status").val("A");
		    		$("#sch_start_dat").val(programData.SCH_START_DAT);
		    		$("#sch_end_dat").val(programData.SCH_END_DAT);
		    		
		    		
		    		$("#budget_total0").val(numberWithCommas($("#programGrid").jqxGrid('getrowdata',row).BUDGET_AMOUNT));
	    			$("#enforce_total0").val(numberWithCommas($("#programGrid").jqxGrid('getrowdata',row).ENFORCE_AMOUNT));
	    			$("#change_total0").val(numberWithCommas($("#programGrid").jqxGrid('getrowdata',row).CHANGE_AMOUNT));
		    		
		    		
	    		});
	    		
	    		var real = $i.sqlhouse(423,{MS_ID:ms_id,PROGRAM_ID:programId});
	    		real.done(function(res){
	    			if(res.returnArray.length>0){
	    				res = res.returnArray[0];
	    				var sdat = res.REAL_START_DAT;
	    				var edat = res.REAL_END_DAT;
		    			$("#real_dat").val(sdat+" ~ "+edat);
		    			
	    			}else{
	    				$("#real_dat").val("");
	    			}
	    		});
	    		
	    		
	    		
	        }
	        function detailProgramPlan(ms_id,row){
	        	var programId = $("#program_id1").val();
	        	resetForm1('detail');
	        	
	        	var dat = $i.sqlhouse(414,{MS_ID:ms_id,PROGRAM_ID:programId});
	    		dat.done(function(res){
	    			var programData = res.returnArray[0];
	    			var size = $("#content").next("iframe").length;
					if(size<1)
						initSmartEditor('content');
					
		    		$("#quan_goal").val(programData.QUAN_GOAL);
		    		$("#quan_unit").val(programData.QUAN_UNIT);
		    		$("#quan_content").val(programData.QUAN_CONTENT);
		    		$("#qual_goal").val(programData.QUAL_GOAL);
		    		$("#summary").val(programData.SUMMARY);
		    		$("#effect").val(programData.EFFECT);
		    		$("#content").val(programData.CONTENT);
	    			oEditors.getById["content"].exec("LOAD_CONTENTS_FIELD");
	    			
	    			if(programData.PROGRAM_STATUS=="A"){
	    				$("#hidden1_1").css("display","none");
	    				$("#hidden1_2").css("display","block");
	    				$("#hidden1_3").css("display","block");
	    			}
	    			else if(programData.PROGRAM_STATUS=="B"){//계획완료
	    				$("#hidden1_1").css("display","block");
	    				$("#hidden1_2").css("display","none");
	    				$("#hidden1_3").css("display","none");
	    			}else{
	    				$("#hidden1_1").css("display","none");
	    				$("#hidden1_2").css("display","none");
	    				$("#hidden1_3").css("display","none");
	    			}
	    			
	    		});
	    		
	    		$("#inputBudget").empty();
	    		var budgetDat = $i.sqlhouse(419,{MS_ID:ms_id,PROGRAM_ID:programId});
	    		budgetDat.done(function(res){
	    			var budgetArr = res.returnArray;
	    			if(budgetArr.length>0){
	    				for(var i=0;i<budgetArr.length;i++){
	    					addBudget();
	    					var budget = budgetArr[i];
	    					$("#budget_id"+i).val(budget.BUDGET_ID);
	    					$("#budget_content"+i).val(budget.BUDGET_CONTENT);
	    					$("#budget_amount"+i).val(numberWithCommas(budget.BUDGET_AMOUNT));
	    					$("#budget_start_dat"+i).val(budget.BUDGET_START_DAT);
	    					$("#budget_end_dat"+i).val(budget.BUDGET_END_DAT);
	    					$("#budget_typ"+i).val(budget.BUDGET_TYP);
	    					$("#budget_def"+i).val(budget.BUDGET_DEF);
	    				}
	    			}else{
	    				
	    				addBudget();
	    			
	    			}
	    		});
	    		
	    		getFileListTr(ms_id,programId);
	        }
	        function detailProgramRun(ms_id,row){
	        	var programId = $("#program_id2").val();
	        	resetForm2('detail');
	        	var dat = $i.sqlhouse(414,{MS_ID:ms_id,PROGRAM_ID:programId});
	    		dat.done(function(res){
	    			
	    			
	    			var programData = res.returnArray[0];
	    			
	    			var size = $("#result").next("iframe").length;
					if(size<1)
						initSmartEditor('result');
	    			
		    		$("#start_dat").val(programData.START_DAT);
		    		$("#end_dat").val(programData.END_DAT);
		    		$("#result").val(programData.RESULT);
	    			oEditors.getById["result"].exec("LOAD_CONTENTS_FIELD");
	    			
	    			
	    			$("#budget_total2").val(numberWithCommas($("#programGrid").jqxGrid('getrowdata',row).BUDGET_AMOUNT));
	    			$("#enforce_total2").val(numberWithCommas($("#programGrid").jqxGrid('getrowdata',row).ENFORCE_AMOUNT));
	    			$("#change_total2").val(numberWithCommas($("#programGrid").jqxGrid('getrowdata',row).CHANGE_AMOUNT));
	    			
	    		/*	if(programData.PROGRAM_STATUS=="B"){//계획완료
	    				$("#hidden2_1").css("display","none");
	    				$("#hidden2_2").css("display","none");
	    				$("#hidden2_3").css("display","block");
	    			}else if(programData.PROGRAM_STATUS=="C"){//실행확정
	    				$("#hidden2_1").css("display","none");
	    				$("#hidden2_2").css("display","none");
	    				$("#hidden2_3").css("display","block");
	    			}else if(programData.PROGRAM_STATUS=="D"){//운영중
	    				$("#hidden2_1").css("display","none");
	    				$("#hidden2_2").css("display","block");
	    				$("#hidden2_3").css("display","block");
	    			}else if(programData.PROGRAM_STATUS=="E"){//운영완료
	    				$("#hidden2_1").css("display","block");
	    				$("#hidden2_2").css("display","none");
	    				$("#hidden2_3").css("display","none");
	    			}else{//처음단계,평가완료
	    				$("#hidden2_1").css("display","none");
	    				$("#hidden2_2").css("display","none");
	    				$("#hidden2_3").css("display","none");
	    			}*/
	    			if(programData.PROGRAM_STATUS=="C"){//실행확정
	    				$("#hidden2_1").css("display","none");
	    				$("#hidden2_2").css("display","none");
	    				$("#hidden2_3").css("display","block");
	    			}else if(programData.PROGRAM_STATUS=="D"){//운영중
	    				$("#hidden2_1").css("display","none");
	    				$("#hidden2_2").css("display","block");
	    				$("#hidden2_3").css("display","block");
	    			}else if(programData.PROGRAM_STATUS=="E"){//운영완료
	    				$("#hidden2_1").css("display","block");
	    				$("#hidden2_2").css("display","none");
	    				$("#hidden2_3").css("display","none");
	    			}else{//실행확정
	    				$("#hidden2_1").css("display","none");
	    				$("#hidden2_2").css("display","none");
	    				$("#hidden2_3").css("display","none");
	    			}
	    		});
	    		
	    		
	    		
	    		makeEnforceTab(ms_id,programId);
	    		
	    		getFileListTr2(ms_id,programId);
	    		
	    		
	        }
	        function makeEnforceTab(ms_id,programId){
	        	
	        	var budgetDat = $i.sqlhouse(422,{MS_ID:ms_id,PROGRAM_ID:programId});
	    		budgetDat.done(function(res){
	    			console.log(res);
	    			if(res.returnCode=="SUCCESS"){
	    				
	    			
		    			var budgetArr = res.returnArray;
		    			
		    			var source =
		                {
		                    datatype: "json",
		                    datafields: [
		                        { name: 'MS_ID', type: 'string' },
		                        { name: 'PROGRAM_ID', type: 'string' },
		    					{ name: 'BUDGET_ID', type: 'string'},
		    					{ name: 'BUDGET_TYPNM', type: 'string' },
		    					{ name: 'BUDGET_CONTENT', type: 'string' },
		    					{ name: 'BUDGET_START_DAT', type: 'string' },
		    					{ name: 'BUDGET_END_DAT', type: 'string' },
		    					{ name: 'BUDGET_AMOUNT', type: 'number' },
		    					{ name: 'ENFORCE_03', type: 'number' },
		    					{ name: 'ENFORCE_04', type: 'number' },
		    					{ name: 'ENFORCE_05', type: 'number' },
		    					{ name: 'ENFORCE_06', type: 'number' },
		    					{ name: 'ENFORCE_07', type: 'number' },
		    					{ name: 'ENFORCE_08', type: 'number' },
		    					{ name: 'ENFORCE_09', type: 'number' },
		    					{ name: 'ENFORCE_10', type: 'number' },
		    					{ name: 'ENFORCE_11', type: 'number' },
		    					{ name: 'ENFORCE_12', type: 'number' },
		    					{ name: 'ENFORCE_01', type: 'number' },
		    					{ name: 'ENFORCE_02', type: 'number' },
		    					{ name: 'ENFORCE_AMOUNT', type: 'number' }
		                    ],
		                    localdata: res,
		                    updaterow: function (rowid, rowdata, commit) {
		                        commit(true);
		                    }
		                };
		    			
		    			var alginLeft = function (row, columnfield, value) {//left정렬
		                           return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
		    							
		                      } 
		    			var alginRight = function (row, columnfield, value) {//right정렬
		                           return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
		    							
		                      } 	
		    			var budgetDate = function (row,columnfield, value) {
		    				var sdate = $("#budgetGrid").jqxGrid('getrowdata',row).BUDGET_START_DAT;
		    				var edate = $("#budgetGrid").jqxGrid('getrowdata',row).BUDGET_END_DAT;
		    				return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + sdate + '~' + edate + '</div>';
		    			};
		    			var titleColumn = function (row, columnfield, value) {//left정렬
		    				var newValue = $i.secure.scriptToText(value);
	                        return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 4px;" title="'+value+'">' + newValue + '</div>';
	    							
	                    }
		    			var noScript = function (row,columnfield, value) {
		    				var newValue = $i.secure.scriptToText(value);
		    				return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
		    			};
		    			var numberColumn = function (row,columnfield, value) {
		    				return '<div id="userName-' + row + '"style="text-align:right; margin:4px 4px 0px 0px;">' + numberWithCommas(value) + '</div>';
		    			};
		    			var ratefield = function (row,columnfield,value){
		    				var budgetAmount = $("#budgetGrid").jqxGrid('getrowdata',row).BUDGET_AMOUNT;
		    				var enforceAmount = $("#budgetGrid").jqxGrid('getrowdata',row).ENFORCE_AMOUNT;
		    				var rate = 0;
		    				if(budgetAmount!=0)
		    					rate = (parseInt(enforceAmount)/parseInt(budgetAmount)*100).toFixed(2);
		    				
		    				return '<div id="userName-' + row + '"style="text-align:right; margin:4px 4px 0px 0px;">' + numberWithCommas(rate) + '</div>';
		    			};
		    			var changefield = function (row,columnfield,value){
		    				var budgetAmount = $("#budgetGrid").jqxGrid('getrowdata',row).BUDGET_AMOUNT;
		    				var enforceAmount = $("#budgetGrid").jqxGrid('getrowdata',row).ENFORCE_AMOUNT;
		    				return '<div id="userName-' + row + '"style="text-align:right; margin:4px 4px 0px 0px;">' + numberWithCommas(budgetAmount-enforceAmount) + '</div>';
		    			};
		    			   
		                var dataAdapter = new $.jqx.dataAdapter(source, {
	
		                    downloadComplete: function (data, status, xhr) { },
	
		                    loadComplete: function (data) { },
	
		                    loadError: function (xhr, status, error) { }
	
		                });
	
		                var dataAdapter = new $.jqx.dataAdapter(source);
	
		                $("#budgetGrid").jqxGrid(
		                {
		                  	width: '100%',
			    			height: '100%',
			    			altrows:true,
			    			pageable: false,
			    			pageSize: 10,
			    			pageSizeOptions: ['10', '20', '30'],
			    			source: dataAdapter,
			    			theme:'blueish',
			    			columnsheight:25 ,
			    			rowsheight: 25,
		                   columns: [
		                       { text: '기획 및 예산', datafield: 'BUDGET_CONTENT', width: '15%', align:'center', cellsalign: 'left',cellsrenderer:titleColumn },
		                      { text: '구분', datafield: 'BUDGET_TYPNM', width: '15%', align:'center', cellsalign: 'left' ,cellsrenderer:noScript },
		                      { text: '기간', datafield: 'BUDGET_START_DAT', width: '15%', align:'center', cellsalign: 'center' ,cellsrenderer:budgetDate},
		    				  { text: '예산액(천원)', datafield: 'BUDGET_AMOUNT', width: '15%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn},
		    				  { text: '집행액(천원)', datafield: 'ENFORCE_AMOUNT', width: '15%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn},
		    				  { text: '%', datafield: 'f', width: '10%', align:'center', cellsalign: 'right',cellsrenderer:ratefield },
		    				  { text: '잔액(천원)', datafield: 'g', width: '15%', align:'center', cellsalign: 'right',cellsrenderer:changefield }
		                    ]
		                });
	    			}
	    			$("#monthEnforceTr").empty();
	    			if(budgetArr.length>0){
	    				
	    				for(var i=0;i<budgetArr.length;i++){
	    					var budget = budgetArr[i];
	    					console.log(budget);
	    					var rate = (parseInt(budget.ENFORCE_AMOUNT)/parseInt(budget.BUDGET_AMOUNT)*100).toFixed(2);
	    					var change = parseInt(budget.BUDGET_AMOUNT) - parseInt(budget.ENFORCE_AMOUNT);
	    					
	    					
		    				var tr = '<tr><input type="hidden" name="budget_id" value="'+budget.BUDGET_ID+'"/>';
		    				tr += '<td  style="width:10%;"><div class="cell" style="white-space: pre-line;word-break: break-all;">';
							tr += $i.secure.scriptToText(budget.BUDGET_CONTENT);
							tr += '</div></td>';
							tr += '<td style="width:5.5%;"><div class="cell t_center" style="white-space: pre-line;word-break: break-all;">';
							tr += $i.secure.scriptToText(budget.BUDGET_TYPNM);
							tr += '</div></td>';
							tr += '<td style="width:6.5%;"><div class="cell t_right">';
							tr += numberWithCommas(budget.BUDGET_AMOUNT);
							tr += '</div></td>';
							tr += '<td style="width:5.5%;"><div class="cell">';
							tr += '<input type="text" name="enforce03" value="'+numberWithCommas(budget.ENFORCE_03)+'" class="input type2  f_left t_right"  style="width:85%; margin:3px 0;" onkeyup="calSumEnforce('+i+');"/>';
							tr += '</div></td>';
							tr += '<td style="width:5.5%;"><div class="cell">';
							tr += '<input type="text" name="enforce04" value="'+numberWithCommas(budget.ENFORCE_04)+'" class="input type2  f_left t_right"  style="width:85%; margin:3px 0;" onkeyup="calSumEnforce('+i+');"/>';
							tr += '</div></td>';
							tr += '<td style="width:5.5%;"><div class="cell">';
							tr += '<input type="text" name="enforce05" value="'+numberWithCommas(budget.ENFORCE_05)+'" class="input type2  f_left t_right"  style="width:85%; margin:3px 0;" onkeyup="calSumEnforce('+i+');"/>';
							tr += '</div></td>';
							tr += '<td style="width:5.5%;"><div class="cell">';
							tr += '<input type="text" name="enforce06" value="'+numberWithCommas(budget.ENFORCE_06)+'" class="input type2  f_left t_right"  style="width:85%; margin:3px 0;" onkeyup="calSumEnforce('+i+');"/>';
							tr += '</div></td>';
							tr += '<td style="width:5.5%;"><div class="cell">';
							tr += '<input type="text" name="enforce07" value="'+numberWithCommas(budget.ENFORCE_07)+'" class="input type2  f_left t_right"  style="width:85%; margin:3px 0;" onkeyup="calSumEnforce('+i+');"/>';
							tr += '</div></td>';
							tr += '<td style="width:5.5%;"><div class="cell">';
							tr += '<input type="text" name="enforce08" value="'+numberWithCommas(budget.ENFORCE_08)+'" class="input type2  f_left t_right"  style="width:85%; margin:3px 0;" onkeyup="calSumEnforce('+i+');"/>';
							tr += '</div></td>';
							tr += '<td style="width:5.5%;"><div class="cell">';
							tr += '<input type="text" name="enforce09" value="'+numberWithCommas(budget.ENFORCE_09)+'" class="input type2  f_left t_right"  style="width:85%; margin:3px 0;" onkeyup="calSumEnforce('+i+');"/>';
							tr += '</div></td>';
							tr += '<td style="width:5.5%;"><div class="cell">';
							tr += '<input type="text" name="enforce10" value="'+numberWithCommas(budget.ENFORCE_10)+'" class="input type2  f_left t_right"  style="width:85%; margin:3px 0;" onkeyup="calSumEnforce('+i+');"/>';
							tr += '</div></td>';
							tr += '<td style="width:5.5%;"><div class="cell">';
							tr += '<input type="text" name="enforce11" value="'+numberWithCommas(budget.ENFORCE_11)+'" class="input type2  f_left t_right"  style="width:85%; margin:3px 0;" onkeyup="calSumEnforce('+i+');"/>';
							tr += '</div></td>';
							tr += '<td style="width:5.5%;"><div class="cell">';
							tr += '<input type="text" name="enforce12" value="'+numberWithCommas(budget.ENFORCE_12)+'" class="input type2  f_left t_right"  style="width:85%; margin:3px 0;" onkeyup="calSumEnforce('+i+');"/>';
							tr += '</div></td>';
							tr += '<td style="width:5.5%;"><div class="cell">';
							tr += '<input type="text" name="enforce01" value="'+numberWithCommas(budget.ENFORCE_01)+'" class="input type2  f_left t_right"  style="width:85%; margin:3px 0;" onkeyup="calSumEnforce('+i+');"/>';
							tr += '</div></td>';
							tr += '<td style="width:5.5%;"><div class="cell">';
							tr += '<input type="text" name="enforce02" value="'+numberWithCommas(budget.ENFORCE_02)+'" class="input type2  f_left t_right"  style="width:85%; margin:3px 0;" onkeyup="calSumEnforce('+i+');"/>';
							tr += '</div></td>';
							tr += '<td style="width:5.5%;"><div class="cell t_right" style="white-space: pre-line;word-break: break-all;"><span id="enforceSum'+i+'">';	
							tr += numberWithCommas(budget.ENFORCE_AMOUNT);
							tr += '</span></div></td>';
							tr += '<td style="width:6.5%;"><div class="cell t_right" style="white-space: pre-line;word-break: break-all;"><span id="change'+i+'">';
							tr += numberWithCommas(change);
							tr += '</span></div></td>';
							tr += '</tr>';
					
		    				$("#monthEnforceTr").append(tr);
		    				
		    				calSumEnforce(i);
	    				}
	    					
	    			}
	    		});
	        	
	        	
	        	
	        	
	        }
	        function calSumEnforce(index){
	        	var inputTr = $("#monthEnforceTr").find("tr").eq(index);
	        	var inputTd = inputTr.find("input[type=text]");
	        	var sum = 0;
	        	for(var i=0;i<inputTd.length;i++){
	        		var inputvalue = inputTd[i].value.replace(/,/gi,"");
	        		
	        		if(isNaN(inputvalue)){
	        			$i.dialog.warning("SYSTEM","집행액은 숫자로 입력해주세요");
	        			inputTd[i].value="";
	        			continue;
	        		}
	        		
	        		if(inputvalue=="") inputvalue = 0;
	        		
	        		var temp = sum+parseInt(inputvalue);
	        		var change = $("#change"+index).html().replace(/,/gi,"");
	        		if(temp>change){
	        			$i.dialog.warning("SYSTEM","집행액은 예산액을 초과할 수 없습니다.");
	        			inputTd[i].value="";
	        			continue;
	        		}
	        		sum+=parseInt(inputvalue);
	        	}
	        	$("#enforceSum"+index).html(numberWithCommas(sum));
	        }
	        
	        
	        
	        
	        function calAchRate(){
	        	var rate = "";
				if($("#quan_goal3").val()!="" && $("#quan_goal3").val()!="0" && $("#quan_ach_val").val()!="" && !isNaN($("#quan_goal3").val()) && !isNaN($("#quan_ach_val").val())){
					if(parseFloat($("#quan_goal3").val())<parseFloat($("#quan_ach_val").val())){
						/*$i.dialog.warning('SYSTEM','달성값은 목표값보다 클 수 없습니다.');
						$("#quan_ach_val").val("");
						$("#quan_ach_rate").val("");
						return;*/
					}
					rate = (parseFloat($("#quan_ach_val").val())/parseFloat($("#quan_goal3").val())*100).toFixed(2);
				}
					
				$("#quan_ach_rate").val(rate);	
	        }
	        function calCntTotal(){
	        	var total = "";
	        	var cntA = $("#cnt_a").val()==""?0:$("#cnt_a").val();
	        	var cntB = $("#cnt_b").val()==""?0:$("#cnt_b").val();
	        	var cntC = $("#cnt_c").val()==""?0:$("#cnt_c").val();
	        	var cntD = $("#cnt_d").val()==""?0:$("#cnt_d").val();
	        	var cntE = $("#cnt_e").val()==""?0:$("#cnt_e").val();
	        	
	        	if(!isNaN(cntA) && !isNaN(cntB) && !isNaN(cntC) && !isNaN(cntD) && !isNaN(cntE)){
	        		total = parseInt(cntA)+parseInt(cntB)+parseInt(cntC)+parseInt(cntD)+parseInt(cntE);
	        	}
	        	$("#cnt_tot").val(total);
	        	$("#cnt_tot_txt").html(total+'명');
	        }
	        
	        function detailProgramDeptEval(ms_id,row){
	        	var programId = $("#program_id3").val();
	        	var yyyy = $("#yyyy3").val();
	        	resetForm3('detail');
	        	var dat = $i.sqlhouse(414,{MS_ID:ms_id,PROGRAM_ID:programId});
	    		dat.done(function(res){
	    			var programData = res.returnArray[0];
	    			$("#quan_goal3").val(programData.QUAN_GOAL);
		    		$("#quan_unit3").val(programData.QUAN_UNITNM);
		    		
		    		var programStatus = programData.PROGRAM_STATUS;
		    		
		    	/*	if(programStatus=="E"){//운영완료
		    			$("#hidden3_3").css("display","block");
		    		}else if(programStatus=="F"){//평가완료
		    			$("#hidden3_1").css("display","block");
		    		}else{
		    			
		    		}*/
		    		if(programStatus=="E"){//운영완료
		    			$("#hidden3_1").css("display","none");
		    			$("#hidden3_2").css("display","block");
		    			$("#hidden3_3").css("display","block");
		    		}
		    		else if(programStatus=="F"){//평가완료
		    			$("#hidden3_1").css("display","block");
		    			$("#hidden3_2").css("display","none");
		    			$("#hidden3_3").css("display","none");
		    		}else{
		    			$("#hidden3_1").css("display","none");
		    			$("#hidden3_2").css("display","none");
		    			$("#hidden3_3").css("display","none");
		    		}
	    		});
	    		
	    		var eval = $i.sqlhouse(427,{MS_ID:ms_id,PROGRAM_ID:programId,YYYY:yyyy});
	    		eval.done(function(res){
	    			res = res.returnArray;
	    			if(res.length>0){
	    				res = res[0];
	    				$("#quan_goal3").val(res.QUAN_GOAL);
			    		$("#quan_unit3").val(res.QUAN_UNITNM);
			    		
	    				$("#quan_ach_val").val(res.QUAN_ACH_VAL);
	    				$("#quan_def3").val(res.QUAN_DEF);
	    				$("#quan_content3").val(res.QUAN_CONTENT);
	    				
	    				var yn = res.QUAL_ACH_YN;
	    			
	    				$("input:radio[name=qual_ach_yn]:input[value='"+yn+"']").prop("checked",true);
	    				
	    				
	    				$("#qual_content3").val(res.QUAL_CONTENT);
	    				$("#cnt_tot").val(res.CNT_TOT);
	    				$("#cnt_tot_txt").html(res.CNT_TOT+'명');
	    				$("#cnt_a").val(res.CNT_A);
	    				$("#cnt_b").val(res.CNT_B);
	    				$("#cnt_c").val(res.CNT_C);
	    				$("#cnt_d").val(res.CNT_D);
	    				$("#cnt_e").val(res.CNT_E);
	    				$("#eval_score").val(res.EVAL_SCORE);
	    				$("#eval_result").val(res.EVAL_RESULT);
	    				
	    				var rate = "";
	    				if($("#quan_goal3").val()!="" && $("#quan_goal3").val()!="0" && $("#quan_ach_val").val()!="")
	    					rate = (parseFloat($("#quan_ach_val").val())/parseFloat($("#quan_goal3").val())*100).toFixed(2);
	    				$("#quan_ach_rate").val(rate);
	    				
	    				if($("#cnt_tot").val()=="")	
	    					calCntTotal();
	    				
	    				
	    				/*
	    				var eval_confirm = res.EVAL_CONFIRM;
	    				if(eval_confirm=='Y'){
	    					$("#hidden3_1").css("display","block");
	    					$("#hidden3_2").css("display","none");
	    					$("#hidden3_3").css("display","block");
	    				}else{
		    				$("#hidden3_1").css("display","none");
		    				$("#hidden3_2").css("display","block");
		    				$("#hidden3_3").css("display","block");	
	    				}
	    			*/
	    				
	    			}
	    		});
	        }
	        
	        function detailEvalConfirm(ms_id,row){
	        	var programId = $("#program_id4").val();
	        	var yyyy = $("#yyyy4").val();
	        	resetForm4('detail');
	        	
	        	var dat = $i.sqlhouse(342,{ MS_ID:ms_id, PROGRAM_ID:programId });
	    		dat.done(function(res){
	    			var res = res.returnArray;
	    			console.log(res);
	    			if(res.length==0){
	    				$("#eval_score4").val("-");
	    			}else{
	    				for(var i=0; i<res.length; i++){
	    	  				
		    				if(res[i].EVAL_SCORE==""){
		    					$("#eval_score4").val("-");
		    				}else{
		    					$("#eval_score4").val(res[i].EVAL_SCORE+"점");
		    				}
		    			
				    		$("#imp_content4").val(res[i].IMPROVE_CONTENT);
				    		$("#best_content4").val(res[i].BEST_CONTENT);
				    		$("#good_content4").val(res[i].GOOD_CONTENT);
			    		
			    		
		    			
		    			}
	    			}
	    			
	    				//var programStatus = programData.PROGRAM_STATUS;
	        	});
	        }
	        
	        function numberWithCommas(x) {
	        	if(x==undefined) return "";
	            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	        }

	        
	        function resetForm0(){
	        	$('#saveForm0').jqxValidator('hide');
	        	$("#programGrid").jqxGrid('clearselection');
	        	$("#program_id0").val("");
	        	$("#program_nm").val("");
    			$("#txtAssignId").val("");
    			$("#txtAssignNm").val("");
    			$("#changeAssignment").val("N");
    			$("#txtStraId").val("");
	    		$("#txtSubstraId").val("");
    			$("#txtDeptcd").val("");
    			$("#txtDeptnm").val("");
    			$("#txtEmpno").val("");
    			$("#txtEmpnm").val("");
    			$("#txtDeptcd_c").val("");
    			$("#txtDeptnm_c").val("");
    			$("#txtEmpno_c").val("");
    			$("#txtEmpnm_c").val("");
    			$("#txtDeptcd_e").val("");
    			$("#txtDeptnm_e").val("");
    			$("#txtEmpno_e").val("");
    			$("#txtEmpnm_e").val("");
    			$("#program_status").jqxDropDownList({selectedIndex: 0 }); 
    			$("#sch_start_dat").val("");
    			$("#sch_end_dat").val("");
    			$("#budget_total0").val("");
    			$("#enforce_total0").val("");
    			$("#change_total0").val("");
    			$("#real_dat").val("");
    			
	        }
	        function saveForm0(assignId){
	        	if($("#saveForm0").jqxValidator("validate") == true){
	        		
					$i.insert("#saveForm0").done(function(args){
	        			if(args.returnCode == "EXCEPTION"){
	        				$i.dialog.error("SYSTEM","저장 중 오류가 발생했습니다."+args.returnMessage);
	        			}else{
	        				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
	        					if($("#changeAssignment").val()=="Y"){
	        						var straid = $("#txtStraId").val();
									var substraid = $("#txtSubstraId").val();
									var assignid = $("#txtAssignId").val();
		        					search(straid,substraid,assignid);
		        					makeProgramGrid(assignId);
	        					}else{
	        						var rowindex = $('#programGrid').jqxGrid('getselectedrowindex');
		        					makeProgramGrid(assignId,rowindex);
	        					}
	        				});
	        				
	        			}
	        		}).fail(function(e){ 
	        			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
	        		});
				}
	        	
	        }
	        function deleteForm0(assignId){
	        	var programId = $("#program_id0").val();
	        	if(programId==""){
	        		$i.dialog.alert('SYSTEM','삭제할 프로그램을 선택하세요.');
	        		return;
	        	}
	        	$i.dialog.confirm('SYSTEM','프로그램을 삭제하시겠습니까?',function(){
        			$i.remove("#saveForm0").done(function(args){
            			if(args.returnCode == "EXCEPTION"){
            				$i.dialog.error("SYSTEM","삭제 중 오류가 발생했습니다."+args.returnMessage);
            			}else{
            				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
	        					makeProgramGrid(assignId);
            				});
            				
            			}
            		}).fail(function(e){ 
            			$i.dialog.error("SYSTEM","삭제 중 오류가 발생 했습니다.");
            		});
        			
        		});	
	        }
	        
	        function resetForm1(detail){
	        	$('#saveForm1').jqxValidator('hide');
	        	if(detail!='detail'){
	        		$("#program_id1").val("");
	        	}
	        	$("#fileListTr").remove();
	        	$("#inputFile").empty();
	        	addFile();
	        	$("#inputBudget").empty();
	        	addBudget();	
	        	
	        	var size = $("#content").next("iframe").length;
				if(size>0){
					$("#content").val("");
					oEditors.getById["content"].exec("SET_IR", [""]);
				}
	        	$("#quan_goal").val("");
	        	$("#quan_unit").jqxDropDownList('selectIndex',0);
	        	$("#quan_content").val("");
	        	$("#qual_goal").val("");
	        	$("#summary").val("");
	        	$("#effect").val("");
	        	
	        	$("#hidden1_1").css("display","none");
				$("#hidden1_2").css("display","none");
				$("#hidden1_3").css("display","none");
	        }
	        
	        function saveForm1(assignId){
	        	oEditors.getById["content"].exec("UPDATE_CONTENTS_FIELD", []);
	        	var smartCheck = oEditors.getById["content"].getIR();
				if(byteCheck(fRemoveHtmlTag(smartCheck)) > 60000){ 
					$i.dialog.warning('SYSTEM',"1~20000자(한글 기준)의 한글/영문/특수문자 혼용만 가능합니다.");
					return false;
				}
				if(isNaN($("#quan_goal").val())){
					$i.dialog.alert('SYSTEM','정량목표는 숫자로 입력해주세요.');
					return;
				}
				
				if($("#program_id1").val()==""){
					$i.dialog.alert('SYSTEM','프로그램을 선택하세요.');
					return;
				}
				//if($("#budget_content").val()!=""||$("#budget_content").val()!="")
				var budgetLen = $("input[name=budget_content]").length;
				var rules = [];
				for(var i=0;i<budgetLen;i++){
					if($("#budget_content"+i).val()!=""||$("#budget_start_dat"+i).val()!=""||$("#budget_end_dat"+i).val()!=""||$("#budget_amount"+i).val()!=""||$("#budget_def"+i).val()!=""){
						if($("#budget_start_dat"+i).val()!="" && $("#budget_end_dat"+i).val()!="" && $("#budget_end_dat"+i).val()<$("#budget_start_dat"+i).val()){
							$i.dialog.warning("SYSTEM","종료일자가 시작일자보다 과거입니다.");
							return;
						}
						if($("#budget_amount"+i).val()!=""){
							var amount = $("#budget_amount"+i).val();
							var check = amount.replace(/,/gi,"");
							if(isNaN(check)){
								$i.dialog.warning("SYSTEM","예산액은 숫자로 입력해주세요");
								$("#budget_amount"+i).val("");
								return;
							}
						}
						rules.push({ input: "#budget_content"+i, message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' });
						rules.push({ input: "#budget_content"+i, message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'length=1,66' });
						rules.push({ input: "#budget_start_dat"+i, message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' });
						rules.push({ input: "#budget_end_dat"+i, message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' });
						rules.push({ input: "#budget_amount"+i, message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' });
						rules.push({ input: "#budget_def"+i, message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'length=0,250' });
					}
				}
				$('#saveForm1').jqxValidator('hide');
				$('#saveForm1').jqxValidator({
    				rtl:true,  
    				rules:rules
    			});
				if($("#saveForm1").jqxValidator("validate") == true){
					var form = new FormData(document.getElementById('saveForm1')); 
					$.ajax({ 
						url: "./insertPlan", //컨트롤러 URL 
						data: form, 
						dataType: 'json', 
						processData: false, 
						contentType: false, 
						type: 'POST', 
						success: function (response) { 
							$i.dialog.alert('SYSTEM','저장되었습니다.',function(){
								var rowindex = $('#programGrid').jqxGrid('getselectedrowindex');
	        					makeProgramGrid(assignId,rowindex);
							});
						},error: function (jqXHR) { 
							$i.dialog.error("SYSTEM","저장 중 오류가 발생 했습니다.");
						} 
					});
				}
	        }
			function confirmForm1(assignId){
				var ms_id= $("#ms_id").val();
				var yyyy = $("#yyyy").val();
				var program_id = $("#program_id1").val();
				
				if(program_id==""){
	        		$i.dialog.alert('SYSTEM','프로그램을 선택하세요');
	        		return;
	        	}
				
	        	$i.post("./confirmPlan",{ms_id:ms_id,program_id:program_id,yyyy:yyyy,status:"confirm"}).done(function(args){
        			if(args.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM","오류가 발생했습니다."+args.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
        					var rowindex = $('#programGrid').jqxGrid('getselectedrowindex');
        					makeProgramGrid(assignId,rowindex);
        				});
        				
        			}
        		}).fail(function(e){ 
        			$i.dialog.error("SYSTEM","오류가 발생 했습니다.");
        		});
	        }
			function cancleForm1(assignId){
				var ms_id= $("#ms_id").val();
				var yyyy = $("#yyyy").val();
				var program_id = $("#program_id1").val();
				
				if(program_id==""){
	        		$i.dialog.alert('SYSTEM','프로그램을 선택하세요');
	        		return;
	        	}
				
	        	$i.post("./confirmPlan",{ms_id:ms_id,program_id:program_id,yyyy:yyyy,status:"cancle"}).done(function(args){
        			if(args.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM","오류가 발생했습니다."+args.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
        					var rowindex = $('#programGrid').jqxGrid('getselectedrowindex');
        					makeProgramGrid(assignId,rowindex);
        				});
        				
        			}
        		}).fail(function(e){ 
        			$i.dialog.error("SYSTEM","오류가 발생 했습니다.");
        		});
	        }
			function getFileListTr(ms_id,programId){
				$("#fileListTr").remove();
	    		var fileDat = $i.sqlhouse(424,{MS_ID:ms_id,PROGRAM_ID:programId,FILE_GUBUN:'A',FILE_ID:''});
	    		fileDat.done(function(res){
	    			var programStatus = "";
	    			var fileArr = res.returnArray;
	    			if(fileArr.length>0){
	    				programStatus = fileArr[0].PROGRAM_STATUS;
	    				var html = '<tr id="fileListTr"><th colspan="2"><div>파일목록</div></th><td colspan="3"><div class="cell">';
	    				for(var i=0;i<fileArr.length;i++){
	    					//html+='<div style="font-weight: bold; padding-right: 10px;">'+fileArr[i].FILE_ORG_NAME+'<a href="./download?ms_id='+ms_id+'&program_id='+programId+'&file_gubun=A&file_id='+fileArr[i].FILE_ID+'"><img src="../../resources/cmresource/image/icon-file-put.png" border="0" alt="'+fileArr[i].FILE_ORG_NAME+'" /></a></div>';
	    					html+='<div name="fileUpload1" style="font-weight: bold; padding-right: 10px;"><a href="./download?ms_id='+ms_id+'&program_id='+programId+'&file_gubun=A&file_id='+fileArr[i].FILE_ID+'">'+fileArr[i].FILE_ORG_NAME+'</a><img class="programPlanFile" src="../../resources/cmresource/image/cancel.png" border="0" alt="'+fileArr[i].FILE_ORG_NAME+'" class="pointer" style="cursor:pointer;" onclick="deletePlanFile('+fileArr[i].FILE_ID+');"/></div>';
	    				}
	    				html+='</div></td></tr>';
	    				$("#fileUploadTr").before(html);
	    				/*if(programStatus!="A")
	    					$(".programPlanFile").css("display","none");*/
	    			}
	    		});
			}
			function getFileListTr2(ms_id,programId){
				$("#fileListTr2").remove();
	    		var fileDat = $i.sqlhouse(424,{MS_ID:ms_id,PROGRAM_ID:programId,FILE_GUBUN:'B',FILE_ID:''});
	    		fileDat.done(function(res){
	    			var programStatus = "";
	    			var fileArr = res.returnArray;
	    			if(fileArr.length>0){
	    				programStatus = fileArr[0].PROGRAM_STATUS;
	    				var html = '<tr id="fileListTr2"><th colspan="2"><div>파일목록</div></th><td colspan="3"><div class="cell">';
	    				for(var i=0;i<fileArr.length;i++){
	    					//html+='<div style="font-weight: bold; padding-right: 10px;">'+fileArr[i].FILE_ORG_NAME+'<a href="./download?ms_id='+ms_id+'&program_id='+programId+'&file_gubun=A&file_id='+fileArr[i].FILE_ID+'"><img src="../../resources/cmresource/image/icon-file-put.png" border="0" alt="'+fileArr[i].FILE_ORG_NAME+'" /></a></div>';
	    					html+='<div name="fileUpload2" style="font-weight: bold; padding-right: 10px;"><a href="./download?ms_id='+ms_id+'&program_id='+programId+'&file_gubun=B&file_id='+fileArr[i].FILE_ID+'">'+fileArr[i].FILE_ORG_NAME+'</a><img class="programRunFile" src="../../resources/cmresource/image/cancel.png" border="0" alt="'+fileArr[i].FILE_ORG_NAME+'" class="pointer" style="cursor:pointer;" onclick="deleteRunFile('+fileArr[i].FILE_ID+');"/></div>';
	    				}
	    				html+='</div></td></tr>';
	    				$("#fileUploadTr2").before(html);
	    				/*if(programStatus!="B" && programStatus!="C" && programStatus!="D")
	    					$(".programRunFile").css("display","none");*/
	    			}
	    		});
			}
			
			function deletePlanFile(file_id){
				var ms_id=$("#ms_id").val();
				var programId=$("#program_id1").val();
				$i.dialog.confirm('SYSTEM','파일을 삭제하시겠습니까?',function(){
					$i.post("./deleteFile",{ms_id:ms_id,program_id:programId,file_gubun:'A',file_id:file_id}).done(function(res){
						$i.dialog.alert('SYSTEM','삭제되었습니다.');
						getFileListTr(ms_id,programId);
					});
				});
				
			}
			function deleteRunFile(file_id){
				var ms_id=$("#ms_id").val();
				var programId=$("#program_id2").val();
				$i.dialog.confirm('SYSTEM','파일을 삭제하시겠습니까?',function(){
					$i.post("./deleteFile",{ms_id:ms_id,program_id:programId,file_gubun:'B',file_id:file_id}).done(function(res){
						$i.dialog.alert('SYSTEM','삭제되었습니다.');
						getFileListTr2(ms_id,programId);
					});
				});
				
			}
			
			function resetForm2(detail){
				if(detail!='detail'){
					$("#program_id2").val("");
					makeEnforceTab('','');
				}
				
	        	$("#fileListTr2").remove();
	        	$("#inputRunFile").empty();
	        	addFile2();
	        	
	        
	        	var size = $("#result").next("iframe").length;
	        
				if(size>0){
					$("#result").val("");
					oEditors.getById["result"].exec("SET_IR", [""]);
	    			
				}
				
	        	
	        	$("#budget_total2").val("");
    			$("#enforce_total2").val("");
    			$("#change_total2").val("");

	        	$("#insertBudgetId").val("");
	        	$("#insertBudgetNm").html("");
	        	$("#limitAmount").val("");
	        	$("#tbodyRunEnforce").empty();
	        	addEnforce();
	        	$("#hidden2_1").css("display","none");
				$("#hidden2_2").css("display","none");
				$("#hidden2_3").css("display","none");
	        }
			
	        function saveForm2(assignId){
	        	oEditors.getById["result"].exec("UPDATE_CONTENTS_FIELD", []);
	        	var smartCheck = oEditors.getById["result"].getIR();
				if(byteCheck(fRemoveHtmlTag(smartCheck)) > 60000){ 
					$i.dialog.warning('SYSTEM',"1~20000자(한글 기준)의 한글/영문/특수문자 혼용만 가능합니다.");
					return false;
				}
				
				if($("#program_id2").val()==""){
					$i.dialog.alert('SYSTEM','프로그램을 선택하세요.');
					return;
				}
				
				
				var form = new FormData(document.getElementById('saveForm2')); 
				$.ajax({ 
					url: "./insertRun", //컨트롤러 URL 
					data: form, 
					dataType: 'json', 
					processData: false, 
					contentType: false, 
					type: 'POST', 
					success: function (response) { 
						$i.dialog.alert('SYSTEM','저장되었습니다.',function(){
							var rowindex = $('#programGrid').jqxGrid('getselectedrowindex');
	        				makeProgramGrid(assignId,rowindex);
						});
					},error: function (jqXHR) { 
						$i.dialog.error("SYSTEM","저장 중 오류가 발생 했습니다.");
					} 
				});
				
	        	
	        }
	        
	      	function confirmForm2(assignId){
				var ms_id= $("#ms_id").val();
				var yyyy = $("#yyyy").val();
				var program_id = $("#program_id2").val();
				
				if(program_id==""){
	        		$i.dialog.alert('SYSTEM','프로그램을 선택하세요');
	        		return;
	        	}
				
	        	$i.post("./confirmRun",{ms_id:ms_id,program_id:program_id,yyyy:yyyy,status:"confirm"}).done(function(args){
        			if(args.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM","오류가 발생했습니다."+args.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
        					var rowindex = $('#programGrid').jqxGrid('getselectedrowindex');
        					makeProgramGrid(assignId,rowindex);
        				});
        				
        			}
        		}).fail(function(e){ 
        			$i.dialog.error("SYSTEM","오류가 발생 했습니다.");
        		});
	        }
	      	function cancleForm2(assignId){
				var ms_id= $("#ms_id").val();
				var yyyy = $("#yyyy").val();
				var program_id = $("#program_id2").val();
				
				if(program_id==""){
	        		$i.dialog.alert('SYSTEM','프로그램을 선택하세요');
	        		return;
	        	}
				
	        	$i.post("./confirmRun",{ms_id:ms_id,program_id:program_id,yyyy:yyyy,status:"cancle"}).done(function(args){
        			if(args.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM","오류가 발생했습니다."+args.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
        					var rowindex = $('#programGrid').jqxGrid('getselectedrowindex');
        					makeProgramGrid(assignId,rowindex);
        				});
        				
        			}
        		}).fail(function(e){ 
        			$i.dialog.error("SYSTEM","오류가 발생 했습니다.");
        		});
	        }
	        function saveForm3(assignId){
	        	if($("#program_id3").val()==""){
					$i.dialog.alert('SYSTEM','프로그램을 선택하세요.');
					return;
				}
	        	if($("#eval_score").val()=="0"){
					$i.dialog.alert('SYSTEM','평가점수를 선택하세요.');
					return;
				}
	       
	        	if($("#saveForm3").jqxValidator("validate") == true){
		        	$i.post("./insertDeptEval","#saveForm3").done(function(args){
	        			if(args.returnCode == "EXCEPTION"){
	        				$i.dialog.error("SYSTEM","저장 중 오류가 발생했습니다."+args.returnMessage);
	        			}else{
	        				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
	        					var rowindex = $('#programGrid').jqxGrid('getselectedrowindex');
	        					makeProgramGrid(assignId,rowindex);
	        				});
	        				
	        			}
	        		}).fail(function(e){ 
	        			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
	        		});
	        	}
	        	
	        }
	        function resetForm3(detail){
	        	$('#saveForm3').jqxValidator('hide');
	        	if(detail!='detail'){
	        		$("#program_id3").val("");
	        		document.getElementById("saveForm3").reset();
	        	}
	        	$("#quan_goal3").val("");
	        	$("#quan_unit3").val("");
	        	$("#quan_ach_val").val("");
	        	$("#quan_ach_rate").val("");
	        	$("#quan_def3").val("");
	        	$("#quan_content3").val("");
	        	$("#qual_content3").val("");
	        	$("#cnt_tot").val("");
	        	$("#cnt_tot_txt").html("");
	        	$("#cnt_a").val("");
	        	$("#cnt_b").val("");
	        	$("#cnt_c").val("");
	        	$("#cnt_d").val("");
	        	$("#cnt_e").val("");
	        	
	        	$("#eval_result").val("");
	        	
	        	$("#eval_score").jqxDropDownList({selectedIndex: 0 });
	        	$("input:radio[name='qual_ach_yn']").prop("checked",false);
	        	
	        	$("#hidden3_1").css("display","none");
				$("#hidden3_2").css("display","none");
				$("#hidden3_3").css("display","none");
	        }
	        
	        function confirmForm3(assignId){
	        	
	        	var ms_id = $("#ms_id").val();
	        	var yyyy = $("#yyyy").val();
	        	var program_id = $("#program_id3").val();
	        	
	        	if(program_id==""){
	        		$i.dialog.alert('SYSTEM','프로그램을 선택하세요');
	        		return;
	        	}
	        	
	        	
	        	var dat = $i.post("./confirmDeptEval",{ms_id:ms_id,program_id:program_id,yyyy:yyyy,status:"confirm"});
	        	dat.done(function(args){
	        		if(args.returnCode == "EXCEPTION"){
	    				$i.dialog.error("SYSTEM","오류가 발생했습니다."+args.returnMessage);
	    			}else{
	    				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
	    					var rowindex = $('#programGrid').jqxGrid('getselectedrowindex');
        					makeProgramGrid(assignId,rowindex);
	    				});
	    			}
	        	});
	        }
	        function cancleEval(assignId){
	        	var ms_id = $("#ms_id").val();
	        	var yyyy = $("#yyyy").val();
	        	var program_id = $("#program_id3").val();
	        	
	        	if(program_id==""){
	        		$i.dialog.alert('SYSTEM','프로그램을 선택하세요');
	        		return;
	        	}
	        	
	        	
	        	var dat = $i.post("./confirmDeptEval",{ms_id:ms_id,program_id:program_id,yyyy:yyyy,status:"cancle"});
	        	dat.done(function(args){
	        		if(args.returnCode == "EXCEPTION"){
	    				$i.dialog.error("SYSTEM","오류가 발생했습니다."+args.returnMessage);
	    			}else{
	    				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
	    					var rowindex = $('#programGrid').jqxGrid('getselectedrowindex');
        					makeProgramGrid(assignId,rowindex);
	    				});
	    			}
	        	});
	        	
	        }
	        
	        function popupAssignMent(){ 
	        	var ms_id = $("#ms_id").val();
	        	var yyyy = $("#yyyy").val();
        		window.open('./popupMappingAssign?ms_id='+ms_id+'&yyyy='+yyyy, 'popupMappingAssignMent','scrollbars=no, resizable=no, width=930, height=310');  
        	}
	        function popupUser(userType){ 
        		window.open('./popupMappingUser?userType='+userType, 'popupMappingUser','scrollbars=no, resizable=no, width=930, height=310');  
        	}
	        
	        function checkSize(idx) {
	        	var fileLength = $("input[name=planFile]").length;
	        	var fileUpload = $("[name='fileUpload1']").length;
	        	if ($("[name='fileUpload1']").length >= 5) {
	        		$i.dialog.warning('SYSTEM',"파일업로드는 5개 이상 할 수 없습니다.");
	        	} else {
	        		if ($("input[name='planFile']")[idx].value != "") {
	        			var FileFilter = /\.(txt|zip|jpg|png|xls|xlsx|ppt|pptx|doc|docx|hwp|pdf)$/i;
	        			var fileValue = $("input[name='planFile']")[idx].value
	        					.toLowerCase();
	        			var fileCheck = false;
	        			var fileSize = $("input[name='planFile']")[idx].files[0].size;
	        			var defaultSize =1000;
	        			if (fileValue.match(FileFilter)) {
	        				fileCheck = true;
	        			}
	        			if (fileCheck == true) {
	        				$("#fileName" + idx).val(
	        						$("input[name='planFile']")[idx].value);
	        			} else {
	        				$i.dialog.warning('SYSTEM',"업로드가 가능한 확장자가 아닙니다.");
	        			}
	        		}
	        	}
	        }
	        function checkSize2(idx) {
	        	var fileLength = $("input[name=runFile]").length;
	        	var fileUpload = $("[name='fileUpload2']").length;
	        	if ($("[name='fileUpload2']").length >= 5) {
	        		$i.dialog.warning('SYSTEM',"파일업로드는 5개 이상 할 수 없습니다.");
	        	} else {
	        		if ($("input[name='runFile']")[idx].value != "") {
	        			var FileFilter = /\.(txt|zip|jpg|png|xls|xlsx|ppt|pptx|doc|docx|hwp|pdf)$/i;
	        			var fileValue = $("input[name='runFile']")[idx].value
	        					.toLowerCase();
	        			var fileCheck = false;
	        			var fileSize = $("input[name='runFile']")[idx].files[0].size;
	        			var defaultSize =1000;
	        			if (fileValue.match(FileFilter)) {
	        				fileCheck = true;
	        			}
	        			if (fileCheck == true) {
	        				$("#runfileName" + idx).val(
	        						$("input[name='runFile']")[idx].value);
	        			} else {
	        				$i.dialog.warning('SYSTEM',"업로드가 가능한 확장자가 아닙니다.");
	        			}
	        		}
	        	}
	        }
		
	        function addFile() {
	        	var fileLength = $("#saveForm1").find("input[name=planFile]").length;
	        	var fileUpload = $("[name='fileUpload1']").length;
	        	var fileNo = fileLength;
	        	
	        	var fileName = "fileName" + fileNo;
	        	var fileName1 = "fileName1" + fileNo;
	        	if (fileLength >= 5 || $("[name='fileUpload1']").length >= 5
	        			|| parseInt(fileLength + fileUpload) >= 5) {
	        		$i.dialog.warning('SYSTEM',"파일업로드는 5개 이상 할 수 없습니다..");
	        	} else {
	        		var html = "";
	        		html += '<div class="group f_left" style="width:100%;" id="'+fileName1+'" >';
	        		html += '<div class="file_input" style="width:100%;">';
	        		html += '<input type="text" name="planFileText" id="'+fileName+'" class="file_input_textbox" readonly style="width:60%;height:18px;padding:1px 0;">';
	        		html += '<div class="button" style="width:22%;">';
	        		html += '<input type="button" value="찾아보기" class="file_input_button"  style="width:100% !important;height: 22px;padding: 1px 0;"/>';
	        		html += '<input type="file" name="planFile" class="file_input_hidden" accept=".zip,.jpg,.png,.xls,.xlsx,.ppt,.pptx,.doc,.docx,.hwp,.pdf" onchange="checkSize('
	        				+ fileNo + ');" />';
	        		html += '</div>';
	        		html += '</div>';
	        		html += '<div class="button_action" style="right:0;">';
	        		if(fileLength==0){
	        			html += '<span class="pointer f_right p_r10" style="margin:0;"> <img src="../../resources/cmresource/css/iplanbiz/theme/style01/img/icon-cir-plus.png" height="15" alt="추가" onclick="addFile();"/> </span> <br>';
	        		}else{
	        			html += '<span class="pointer f_right p_r20" style="margin:0;"> <img src="../../resources/cmresource/css/iplanbiz/theme/style01/img/icon-cir-minus.png" height="15" alt="삭제버튼" onclick="deleteFileTr(\''
	        				+ fileName1 + '\'); "> </span> <br>';
	        		}
	        		html += '</div></div>';

	        		$("#inputFile").append(html);
	        	}
	        }
	        function addFile2() {
	        	var fileLength = $("#saveForm2").find("input[name=runFile]").length;
	        	var fileUpload = $("[name='fileUpload2']").length;
	        	var fileNo = fileLength;
	        	
	        	var fileName = "runfileName" + fileNo;
	        	var fileName1 = "runfileName1" + fileNo;
	        	if (fileLength >= 5 || $("[name='fileUpload2']").length >= 5
	        			|| parseInt(fileLength + fileUpload) >= 5) {
	        		$i.dialog.warning('SYSTEM',"파일업로드는 5개 이상 할 수 없습니다..");
	        	} else {
	        		var html = "";
	        		html += '<div class="group f_left" style="width:100%;" id="'+fileName1+'" >';
	        		html += '<div class="file_input" style="width:100%;">';
	        		html += '<input type="text" name="runFileText" id="'+fileName+'" class="file_input_textbox" readonly style="width:60%;height: 18px;padding: 1px 0;">';
	        		html += '<div class="button" style="width:22%;">';
	        		html += '<input type="button" value="찾아보기" class="file_input_button"  style="width:100% !important;height: 22px;padding: 1px 0;"/>';
	        		html += '<input type="file" name="runFile" class="file_input_hidden" accept=".zip,.jpg,.png,.xls,.xlsx,.ppt,.pptx,.doc,.docx,.hwp,.pdf" onchange="checkSize2('
	        				+ fileNo + ');" />';
	        		html += '</div>';
	        		html += '</div>';
	        		html += '<div class="button_action" style="right:0;">';
	        		if(fileLength==0){
	        			html += '<span class="pointer f_right p_r10" style="margin:0;"> <img src="../../resources/cmresource/css/iplanbiz/theme/style01/img/icon-cir-plus.png" height="15" alt="추가" onclick="addFile2();"/> </span> <br>';
	        		}else{
	        			html += '<span class="pointer f_right p_r20" style="margin:0;"> <img src="../../resources/cmresource/css/iplanbiz/theme/style01/img/icon-cir-minus.png" height="15" alt="삭제버튼" onclick="deleteFileTr2(\''
	        				+ fileName1 + '\'); "> </span> <br>';
	        		}
	        		html += '</div></div>';

	        		$("#inputRunFile").append(html);
	        	}
	        }
	        function deleteFileTr(inputName) {
	        	$("#" + inputName).remove();
	        }
	        
	        function deleteFileTr2(inputName) {
	        	$("#" + inputName).remove();
	        }
	        
	        
	        
	        function addBudget(){
				var inputBudgetLength = $("[name=inputTr]").length;
				var inputNo = inputBudgetLength;
				var inputName = "input" + inputNo;
				var html = "";
				html += '<tr id="'+inputName+'" name="inputTr">';
				html += '<td style="width:20%;"><div class="cell">';
				html += '<input type="hidden" id="budget_id'+inputNo+'" name="budget_id"/>';
				html += '<input type="text" class="input type2" id="budget_content'+inputNo+'" name="budget_content" style="width:90%; margin:3px 0; border: 0;" />';
				html += '</div></td>';
				html += '<td style="width:15%;"><div class="cell">';
				html += '<select id="budget_typ'+inputNo+'" name="budget_typ" class="budgetType">';
				html += '<c:choose><c:when test="${budgetTypList != null && not empty budgetTypList}">';
				html += '<c:forEach items="${budgetTypList}" var="key" varStatus="loop"><option value="${key.COM_COD}">${key.COM_NM}</option></c:forEach>';
				html += '</c:when></c:choose>';
				html += '</select>';
				html += '</div></td>';
				html += '<td style="width:23%;"><div class="cell">';
				html += '<input id="budget_start_dat'+inputNo+'" name="budget_start_dat" type="text" readonly class="dateInput input type2 t_center f_left" style="width:80px; margin:3px 0;" value=""/>';
				html += '<div class="f_left m_l5 m_r5 m_t2">&sim;</div>';
				html += '<input id="budget_end_dat'+inputNo+'" name="budget_end_dat" type="text" readonly class="dateInput input type2 t_center f_left" style="width:80px; margin:3px 0;" value=""/>';
				html += '</div></td>';
				html += '<td style="width:12%;"><div class="cell">';
				html += '<input type="text" id="budget_amount'+inputNo+'" name="budget_amount" class="input type2 t_right" style="width:90%; margin:3px 0; border: 0;" />';
				html += '</div></td>';
				html += '<td style="width:20%;"><div class="cell">';
				html += '<input type="text" id="budget_def'+inputNo+'" name="budget_def" class="input type2 t_left" style="width:90%; margin:3px 0; border: 0;" />';
				html += '</div></td>';
				html += '<td ><div class="cell">';
				html += '<div class="pointer t_center">';
				html += '<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/icon-minus.png" alt="삭제" height="19" onclick="deleteBudget(\''+inputName+'\');">';
				html += '</div></div></td>';
				html +='</tr>';
				
				$("#inputBudget").append(html);
				$(".budgetType").jqxDropDownList({animationType: 'fade',dropDownHorizontalAlignment: 'right',dropDownVerticalAlignment:'top',dropDownWidth: 150, dropDownHeight:100, width: 150,height: 23,theme:'blueish'});
				$('.dateInput').datepicker({
					//changeYear: true,
					//changeMonth: true,
					showOn: 'button',
					buttonImage: '../../resources/cmresource/image/icon-calendar.png',
					buttonImageOnly: true,
					dateFormat: 'yymmdd' //2014.12.12
				});
				$('.ui-datepicker-trigger').addClass('icon-calendar f_left m_t7 pointer');
			}
	        function inputTrReset(idx){
				$("#budget_id0").val("");
				$("#budget_content0").val("");
				$("#budget_start_dat0").val("");
				$("#budget_end_dat0").val("");
				$("#budget_amount0").val("");
	   		} 
	        function deleteBudget(inputName) {
	        	$('#saveForm1').jqxValidator('hide');
				var input = $("[name=inputTr]").length;
				if(input == 1){
					$("#"+inputName).find($("[name=budget_id]")).val("");
					$("#"+inputName).find($("[name=budget_content]")).val("");
					$("#"+inputName).find($("[name=budget_start_dat]")).val("");
					$("#"+inputName).find($("[name=budget_end_dat]")).val("");
					$("#"+inputName).find($("[name=budget_amount]")).val("");
				}else{
					$('#'+inputName).remove();	
				}
		 }
	        
	        function addEnforce(){
				var inputEnforceLength = $("[name=inputEnforceTr]").length;
				var inputNo = inputEnforceLength;
				var inputName = "inputEnforce" + inputNo;
				var html = "";
				html += '<tr id="'+inputName+'" name="inputEnforceTr">';
				html += '<td style="width:25%;"><div class="cell">';
				html += '<input type="hidden" id="enforce_id'+inputNo+'" name="enforce_id"/>';
				html += '<input id="enforce_start_dat'+inputNo+'" name="enforce_start_dat" type="text" readonly class="dateInput input type2 t_center f_left" style="width:80px; margin:3px 0;" value=""/>';
				html += '<div class="f_left m_l5 m_r5 m_t2">&sim;</div>';
				html += '<input id="enforce_end_dat'+inputNo+'" name="enforce_end_dat" type="text" readonly class="dateInput input type2 t_center f_left" style="width:80px; margin:3px 0;" value=""/>';
				html += '</div></td>';
				html += '<td style="width:25%;"><div class="cell">';
				html += '<input type="text" value="" id="enforce_content'+inputNo+'" name="enforce_content" class="input type2  f_left"  style="width:98%; margin:3px 0;"/>'
				html += '</div></td>';
				html += '<td style="width:15%;"><div class="cell">';
				html += '<input type="text" value="" id="enforce_amount'+inputNo+'" name="enforce_amount" class="input type2  f_left t_right"  style="width:98%; margin:3px 0;"/>';
				html += '</div></td>';
				html += '<td style="width:25%;"><div class="cell t_right">';
				html += '<input type="text" value="" id="enforce_def'+inputNo+'" name="enforce_def" class="input type2  f_left"  style="width:98%; margin:3px 0;"/>';
				html += '</div></td>';
				html += '<td style="width:10%;"><div class="cell">';
				html += '<div class="pointer t_center">';
				html += '<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/icon-minus.png" alt="삭제" height="19" onclick="deleteEnforce(\''+inputName+'\');">';
				html += '</div></div></td>';
				html +='</tr>';
				
				$("#tbodyRunEnforce").append(html);
				$('.dateInput').datepicker({
					//changeYear: true,
					//changeMonth: true,
					showOn: 'button',
					buttonImage: '../../resources/cmresource/image/icon-calendar.png',
					buttonImageOnly: true,
					dateFormat: 'yymmdd' //2014.12.12
				});
				$('.ui-datepicker-trigger').addClass('icon-calendar f_left m_t7 pointer');
			}
	        function inputEnforceReset(idx){
				$("#enforce_id0").val("");
				$("#enforce_content0").val("");
				$("#enforce_start_dat0").val("");
				$("#enforce_end_dat0").val("");
				$("#enforce_amount0").val("");
				$("#enforce_def0").val("");
	   		} 
	        function deleteEnforce(inputName) {
				var input = $("[name=inputEnforceTr]").length;
				if(input == 1){
					$("#"+inputName).find($("[name=enforce_id]")).val("");
					$("#"+inputName).find($("[name=enforce_content]")).val("");
					$("#"+inputName).find($("[name=enforce_start_dat]")).val("");
					$("#"+inputName).find($("[name=enforce_end_dat]")).val("");
					$("#"+inputName).find($("[name=enforce_amount]")).val("");
					$("#"+inputName).find($("[name=enforce_def]")).val("");
				}else{
					$('#'+inputName).remove();	
				}
		 }
	        
	        function resetForm4(detail){
	        	if(detail!='detail'){
	        		$("#program_id4").val("");
	        		$("#programGrid").jqxGrid('clearselection');
	        	}
    			$("#eval_score4").val("");
    			$("#good_content4").val("");
    			$("#imp_content4").val("");
    			$("#best_content4").val("");
    			
    			
	        }
	        
		</script>
<style>
			.jqx-validator-hint{
        		background-color : transparent !important;
        		border:0px !important;
        	}
        	.jqx-validator-hint-arrow{
        		display:none !important;
        	}
</style>
</head>
<body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
<div class="wrap" style="width:98%; min-width:1580px; margin:0 1%;">
	<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
		<div class="label type1 f_left">
			비젼:
		</div>
		<div class="combobox f_left"  id='jqxCombobox01' >
		</div>
		<div class="label type1 f_left">
			년도:
		</div>
		<div class="combobox f_left"  id='jqxCombobox02' >
		</div>
		<div class="group_button f_right">
			<div class="button type1 f_left">
				<input type="button" value="조회" id='jqxButtonSearch' width="100%" height="100%" />
			</div>
		</div>
		<!--group_button-->
	</div>
	<!--//header-->
	<div class="container  f_left" style="width:100%;">
		<div class="content f_left" style=" width:20%; margin-right:1%;">
			<div class="tabs f_left" style=" width:100%; height:685px; margin-top:0px;">
				<div id='jqxTabs01'>
					<ul>
						<li style="margin-left: 0px;">전략기준</li>
						<li>검색</li>
					</ul>
					<div class="tabs_content" style="height:100%; ">
						<div  class="tree" style="border:none;"  id='jqxTree01'>
							
						</div>
						<!--//jqxTree-->
					</div>
					<!--//tabs_content-->
					<div  class="tabs_content" style="height:100%; ">
						<div class="content f_left" style="width:94%; margin:10px 3%;">
							<div class="group  w100p" style="margin:10px auto;">
								<div class="label type2 f_left">
									검색
								</div>
								<input type="text" value="" id="txtSearchWord" class="input type1  f_left  w50p"  style="width:100%; margin:3px 5px; "/>
								<div class="button type2 f_left">
									<input type="button" value="검색" id='jqxButtonFind01' width="100%" height="100%" onclick="makeAssignSearch();"/>
								</div>
							</div>
							<!--group-->
							<div class="grid f_left" style="width:100%; margin:5px 0%; height:560px;">
								<div id="searchGrid">
								</div>
							</div>
							<!--grid -->
						</div>
						<!--content-->
					</div>
					<!--//tabs_content-->
				</div>
				<!--//jqxTabs-->
			</div>
			
			<!--//tabs-->
		</div>
		<!--//content-->
		
		<div class="content f_right" style=" width:79%;">
			<div class="group f_left  w100p m_b5">
				<div class="label type2 f_left" id="selectStraNm">
				전락목표
				</div>
				<div class="combobox f_left"  id='jqxAssignCombo' >
				</div>
			</div>
			<!--group-->
			<div class="grid f_left" style="width:100%; margin:5px 0%; height:151px;">
				<div id="programGrid">
				</div>
			</div>
			<!--grid -->
			<div class="group f_left  w100p m_b5 m_t10">
				<div class="label type2 f_left" id="selectProgramNm">
				추진과제명
				</div>
			</div>
			<!--group-->
			
				<input type="hidden" id="ms_id"/>
				<input type="hidden" id="yyyy"/>
				<input type="hidden" id="assign_id"/>
			
			<div class="tabs f_left" style=" width:100%; height:456px; margin-top:0px;">
				<div id='jqxTabs02'>
					<ul>
						<li style="margin-left: 0px;">기본정보</li>
						<li>계획</li>
						<li>운영</li>
						<li>부서평가</li>
						<li>위원회평가</li>
						<li>보고서</li>
					</ul>
					
					<!-- 기본탭 S -->
					<div class="tabs_content" style="height:100%; ">
						<div class="content f_left" style="width:97%; margin:10px 1.5%;">
							<div class="table  f_left" style="width:100%; margin:0; ">
								<form name="saveForm0" id="saveForm0">
								<input type="hidden" id="ms_id0" name="ms_id"/>
								<input type="hidden" id="yyyy0" name="yyyy"/>
								<input type="hidden" id="program_id0" name="program_id"/>
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<th class="w15p"><div>
													프로그램명<span class="th_must"></span>
												</div></th>
											<td colspan="3"><div class="cell">
													<input type="text" value="" id="program_nm" name="program_nm" class="input type2  f_left"  style="width:99%; margin:3px 0;"/>
												</div></td>
										</tr>
										<tr>
											<th><div>
													추진과제<span class="th_must"></span>
												</div></th>
											<td class="w35p"><div  class="cell">
													<input type="hidden" name="changeAssignment" id="changeAssignment"/>
													<input type="hidden" name="txtstraid" id="txtStraId"/>
													<input type="hidden" name="txtsubstraid" id="txtSubstraId"/>
													<input type="hidden" name="assign_id" id="txtAssignId"/>
													<input type="text" value="" id="txtAssignNm" readonly class="input type2 f_left"  style="width:75%; margin:3px 0; "/>
													<div class="icon-search f_left m_t7 pointer" onclick="popupAssignMent();">
													</div>
												</div></td>
											<th class="w15p"><div  class="cell">
													
												</div></th>
											<td class="w35p"><div  class="cell">
													
												</div></td>
										</tr>
										<tr>
											<th><div>
													담당자<span class="th_must"></span>
												</div></th>
											<td class="w35p"><div  class="cell">
													<input type="hidden" id="txtDeptcd" name="dept_cd"/>
													<input type="hidden" id="txtDeptnm" name="dept_nm"/>
													<input type="hidden" id="txtEmpno" name="emp_no"/>
													<input type="text" value="" id="txtEmpnm" name="emp_nm" readonly class="input type2  f_left"  style="width:75%; margin:3px 0; "/>
													<div class="icon-search f_left m_t7 pointer" onclick="popupUser('main');">
													</div>
												</div></td>
											<th class="w15p"><div>
													확인자<span class="th_must"></span>
												</div></th>
											<td class="w35p"><div  class="cell">
													<input type="hidden" id="txtDeptcd_c" name="confirm_dept_cd"/>
													<input type="hidden" id="txtDeptnm_c" name="confirm_dept_nm"/>
													<input type="hidden" id="txtEmpno_c" name="confirm_emp_no"/>
													<input type="text" value="" id="txtEmpnm_c" name="confirm_emp_nm" readonly class="input type2  f_left"  style="width:75%; margin:3px 0; "/>
													<div class="icon-search f_left m_t7 pointer" onclick="popupUser('confirm');">
													</div>
												</div></td>
										</tr>
										<tr>
											<th><div>
													부서평가자<span class="th_must"></span>
												</div></th>
											<td class="w35p"><div  class="cell">
													<input type="hidden" id="txtDeptcd_e" name="eval_dept_cd"/>
													<input type="hidden" id="txtDeptnm_e" name="eval_dept_nm"/>
													<input type="hidden" id="txtEmpno_e" name="eval_emp_no"/>
													<input type="text" value="" id="txtEmpnm_e" name="eval_emp_nm" readonly class="input type2  f_left"  style="width:75%; margin:3px 0; "/>
													<div class="icon-search f_left m_t7 pointer" onclick="popupUser('eval');">
													</div>
												</div></td>
											<th class="w15p"><div>
													상태
												</div></th>
											<td class="w35p"><div  class="cell">
													<select id="program_status" name="program_status">
														<c:choose>
															<c:when test="${statusList != null && not empty statusList}">
																<c:forEach items="${statusList}" var="key" varStatus="loop">
																	<option value="${key.COM_COD}">${key.COM_NM}</option>
																</c:forEach>
															</c:when>
														</c:choose>
													</select>
												</div></td>
										</tr>
										<tr>
											<th><div>
													사업예정기간<span class="th_must"></span>
												</div></th>
											<td class="w35p"><div  class="cell">
													<input id="sch_start_dat" type="text" readonly name="sch_start_dat" class="input type2 t_center f_left" style="width:100px; margin:3px 0;" value=""/>
													<div class="f_left m_l5 m_r5 m_t2">
														&sim;
													</div>
													<input id="sch_end_dat" type="text" readonly name="sch_end_dat" class="input type2 t_center f_left" style="width:100px; margin:3px 0;" value=""/>
												</div></td>
											<th class="w15p"><div>
													실사업기간
												</div></th>
											<td class="w35p"><div  class="cell">
													<input type="text" value="" id="real_dat" readonly class="input type2  f_left"  style="width:97.5%; margin:3px 0;border:none;background:transparent; "/>
												</div></td>
										</tr>
									</tbody>
								</table>
								</form>
							</div>
							<!--table-->
							<div class="group f_left m_t20" style="width:100%; margin:10px 0;">
								<div class="label type2 f_left m_r5 none_bg_img">
									<span class="sublabel"  >예산액(천원):</span>
									<input type="text" value="" id="budget_total0" class="input type2 t_right"  readonly style="width:150px !important;height:18px !important;padding:1px 0; margin:3px 0;"/>
								</div>
								<div class="label type2 f_left m_r5 none_bg_img">
									<span class="sublabel" >집행액(천원):</span>
									<input type="text" value="" id="enforce_total0" class="input type2 t_right" readonly style="width:150px !important;height:18px !important;padding:1px 0; margin:3px 0; "/>
								</div>
								<div class="label type2 f_left m_r5 none_bg_img">
									<span class="sublabel" >잔액(천원):</span>
									<input type="text" value="" id="change_total0" class="input type2 t_right" readonly style="width:150px !important;height:18px !important;padding:1px 0; margin:3px 0; "/>
								</div>
							</div>
							<!--group-->
							<div class="group_button f_right m_b10" style="margin-top:50px;">
								<div class="button type2 f_left">
									<input type="button" value="신규" id='newProgramBtn' width="100%" height="100%" />
								</div>
								<div class="button type2 f_left">
									<input type="button" value="저장" id='saveProgramBtn' width="100%" height="100%" />
								</div>
								<div class="button type3 f_left">
									<input type="button" value="삭제" id='delProgramBtn' width="100%" height="100%" />
								</div>
							</div>
							<!--group_button-->
						</div>
						<!--content-->
					</div>
					<!--//tabs_content-->
					
					<!-- 계획탭 S -->
					<div  class="tabs_content" style=" height:100%; ">
					<form name="saveForm1" id="saveForm1" method="post" enctype="multipart/form-data">
							<input type="hidden" id="ms_id1" name="ms_id"/>
							<input type="hidden" id="yyyy1"/>
							<input type="hidden" id="assign_id1" name="assign_id" />
							<input type="hidden" id="program_id1" name="program_id"/>
						<div class="content f_left" style="width:97%; margin:10px 1.5%;">
							
							<div class="table  f_left" style="width:100%; margin:0; ">
							
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<th colspan="2" style="width:16%;"><div>
													목적 및 내용
												</div></th>
											<td colspan="3">
											<div  class="cell">
											 	<textarea id="content" name="content" class="textarea type1"  style=" width:99.5%; height:150px; margin:3px 0;" >${detailAssignMent.ASSIGN_STATUS}</textarea>
                             				</div></td>
										</tr>
										<tr>
											<th rowspan="3" style="width:8%;"><div>
													사업목표
												</div></th>
											<th rowspan="2" style="width:8%;">
												<div>
													정량목표
												</div>
											</th>
											<th style="width:8%;height:10px !important;">
												<div style="margin:0;">
													목표
												</div>
											</th>
											<th style="width:8%;height:10px !important;">
												<div style="margin:0;">
													단위
												</div>
											</th>
											<td rowspan="2">
												<div  class="cell">
													<textarea   id="quan_content" name="quan_content" class="textarea"  value="" style=" width:99.5%; height:65px; margin:3px 0;overflow:auto;"  placeholder="Varchar2 2000자"></textarea>
												</div>
											</td>
										</tr>
										<tr>
											<td><div  class="cell">
											<input type="text" id="quan_goal" name="quan_goal" value="" class="input type2 t_center"  style="width:90%; margin:3px 0;"/>
											</div></td>
											<td><div  class="cell">
											<select id="quan_unit" name="quan_unit">
												<option value="">선택</option>
														<c:choose>
															<c:when test="${listUnit != null && not empty listUnit}">
																<c:forEach items="${listUnit}" var="key" varStatus="loop">
																	<option value="${key.COM_COD}">${key.COM_NM}</option>
																</c:forEach>
															</c:when>
														</c:choose>
													</select>
											</div></td>
										</tr>
										<tr>
											<th><div>
													정성목표
												</div></th>
											<td colspan="3"><div  class="cell">
													<textarea   id="qual_goal" name="qual_goal" class="textarea"  value="" style=" width:99.5%; height:65px; margin:3px 0;overflow:auto;"  placeholder="Varchar2 2000자"></textarea>
												</div></td>
										</tr>
										<tr>
											<th colspan="2"><div>
													추진개요
												</div></th>
											<td colspan="3"><div  class="cell">
													<textarea   id="summary" name="summary" class="textarea"  value="" style=" width:99.5%; height:65px; margin:3px 0;overflow:auto;"  placeholder="Varchar2 2000자"></textarea>
												</div></td>
										</tr>
										<tr>
											<th  colspan="2"><div>
													기대효과
												</div></th>
											<td colspan="3"><div  class="cell">
													<textarea   id="effect" name="effect" class="textarea"  value="" style=" width:99.5%; height:65px; margin:3px 0;overflow:auto;"  placeholder="Varchar2 2000자"></textarea>
												</div></td>
										</tr>
										<tr id="fileUploadTr">
											<th  colspan="2"><div>
													파일첨부
												</div></th>
											<td colspan="3"><div  class="cell">
													<div id="inputFile" class="fileupload" style=" width:100%; margin:5px 0; height:80px; overflow-y:scroll;">
														<div class="group f_left" id="fileName10" style="width:100%;">
															<div class="file_input" style="width:100%;">
																<input type="text" id="fileName0" name="planFileText" class="file_input_textbox" readonly style="width:60%;height:18px;padding:1px 0;">
																<div class="button" style="width:22%;">
																	<input type="button" value="찾아보기" class="file_input_button"  style="width:100% !important;height: 22px;padding: 1px 0;"/>
																	<input type="file" name="planFile" class="file_input_hidden" accept=".zip,.jpg,.png,.xls,.xlsx,.ppt,.pptx,.doc,.docx,.hwp,.pdf" onchange="checkSize('0');" />
																</div>
															</div>
															<!--file_input-->
															<div class="button_action" style="right:0;">
																<span class="pointer f_right p_r10" style="margin:0;">
																<img src='../../resources/cmresource/css/iplanbiz/theme/style01/img/icon-cir-plus.png' alt="추가" onclick="addFile();" height=15>
																</span>
																<br>
															</div>
															<!--button_action-->
														</div>
														<!--group-->
													</div>
											
													
													
												</div></td>
										</tr>
									</tbody>
								</table>
								
							</div>
							<!--table-->
							
							<div class="group f_left  w100p m_t10">
								<div class="label type2 f_left">
									예산 및 집행내역
								</div>
							</div>
							<!--group-->
							<div class="blueish datatable f_left" style="width:100%; height:350px; margin:5px 0; overflow:hidden;">
								<!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 헤더가 50px일경우, 300px - 50px= 250px)-->
								<div class="datatable_fixed" style="width:100%; height:350px; float:left; padding:25px 0;">
									<!--group으로 헤더높이가 길어질 경우 padding  (기본높이는 25px)줘야 함-->
									<div style=" height:325px !important; overflow-x:hidden;">
										<table  width="100%" cellspacing="0" cellpadding="0" border="0"  class="none_hover">
											<thead style="width:100%;">
												<tr>
													<th style="width:20%;">기획 및 예산</th>
													<th style="width:15%;">구분</th>
													<th style="width:23%;">기간</th>
													<th style="width:12%;">예산액(천원)</th>
													<th style="width:20%;">비고</th>
													<th style="width:10%;"> <div class="pointer m_t3">
															<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/icon-plus.png" alt="추가" height="19" onclick="addBudget();">
														</div></th>
													<th style="width:17px; min-width:17px; border-bottom:0;"></th>
												</tr>
											</thead>
											<tbody id="inputBudget">
												<tr name="inputTr">
													<td style="width:20%;">
														<div class="cell">
														<input type="hidden" id="budget_id0" name="budget_id"/>
														<input type="text" class="input type2" id="budget_content0" name="budget_content" style="width:90%; margin:3px 0; border: 0;" />
														</div>
													</td>
													<td style="width:20%;"><div class="cell">
														<select id="budget_typ0" name="budget_typ" class="budgetType">
														<c:choose>
															<c:when test="${budgetTypList != null && not empty budgetTypList}">
																<c:forEach items="${budgetTypList}" var="key" varStatus="loop">
																	<option value="${key.COM_COD}">${key.COM_NM}</option>
																</c:forEach>
															</c:when>
														</c:choose>
													</select>
														</div>
													</td>
													<td style="width:23%;"><div class="cell">
															<input id="budget_start_dat0" name="budget_start_dat" type="text" readonly class="dateInput input type2 t_center f_left" style="width:80px; margin:3px 0;" value=""/>
															<div class="f_left m_l5 m_r5 m_t2">
																&sim;
															</div>
															<input id="budget_end_dat0" name="budget_end_dat" type="text" readonly class="dateInput input type2 t_center f_left" style="width:80px; margin:3px 0;" value=""/>
														</div>
													</td>
													<td style="width:12%;"><div class="cell">
														<input type="text" id="budget_amount0" name="budget_amount" class="input type2 t_right" style="width:90%; margin:3px 0; border: 0;" />
														</div>
													</td>
													<td style="width:15%;"><div class="cell">
														<input type="text" id="budget_def0" name="budget_def" class="input type2 t_left" style="width:90%; margin:3px 0; border: 0;" />
														</div>
													</td>
													<td ><div class="cell">
															<div class="pointer t_center">
																<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/icon-minus.png" alt="삭제" height="19" onclick="inputTrReset(0);">
															</div>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<!--table -->
							<div class="group_button f_right m_b10" style="margin-top:20px;">
								<div class="button type3 f_left" id="hidden1_1">
									<input type="button" value="계획취소" id='cancleBudgetBtn' width="100%" height="100%" />
								</div>
								<div class="button type3 f_left" id="hidden1_2">
									<input type="button" value="계획완료" id='confirmBudgetBtn' width="100%" height="100%" />
								</div>
								<div class="button type2 f_left" id="hidden1_3">
									<input type="button" value="저장" id='saveBudgetBtn' width="100%" height="100%" />
								</div>
							</div>
							<!--group_button-->
						</div>
						</form>
						<!--content-->
					</div>
					<!--//tabs_content-->
					
					<!-- 운영탭 S -->
					
					<div  class="tabs_content" style=" height:100%; ">
					<form name="saveForm2" id="saveForm2" method="post" enctype="multipart/form-data">
						<input type="hidden" id="ms_id2" name="ms_id"/>
						<input type="hidden" id="yyyy2"/>
						<input type="hidden" id="assign_id2" name="assign_id" />
						<input type="hidden" id="program_id2" name="program_id"/>
						<div class="content f_left" style="width:97%; margin:10px 1.5%;">
							<div class="table  f_left" style="width:100%; margin:0; ">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
									<!-- 	<tr>
											<th colspan="2" style="width:16%;"><div>
													실사업기간
												</div></th>
											<td><div class="cell">
												<input id="start_dat" name="start_dat" type="text" class="dateInput input type2 t_center f_left" style="width:100px; margin:3px 0;" value=""/>
												<div class="f_left m_l5 m_r5 m_t2">
													&sim;
												</div>
												<input id="end_dat" name="end_dat" type="text" class="dateInput input type2 t_center f_left" style="width:100px; margin:3px 0;" value=""/>
											</div></td>
                             				<td><div  class="cell"></div></td>
										</tr>-->
										<tr>
											<th colspan="2" style="width:16%;"><div>
													추진성과
												</div></th>
											<td colspan="2"><div  class="cell">
													<textarea id="result" name="result" class="textarea type1"  style=" width:99.5%; height:150px; margin:3px 0;overflow:auto;" >${detailAssignMent.ASSIGN_STATUS}</textarea>
                             				</div></td>
										</tr>
										<tr id="fileUploadTr2">
											<th colspan="2"><div>
													파일첨부
												</div></th>
											<td colspan="2"><div  class="cell">
													<div id="inputRunFile" class="fileupload" style=" width:100%; margin:5px 0; height:80px; overflow-y:scroll;">
														<div class="group f_left" id="runfileName10" style="width:100%;">
															<div class="file_input" style="width:100%;">
																<input type="text" id="runfileName0" name="runFileText" class="file_input_textbox" readonly style="width:60%;height: 18px;padding: 1px 0;">
																<div class="button" style="width:22%;">
																	<input type="button" value="찾아보기" class="file_input_button"  style="width:100% !important;height: 22px;padding: 1px 0;"/>
																	<input type="file" name="runFile" class="file_input_hidden" accept=".zip,.jpg,.png,.xls,.xlsx,.ppt,.pptx,.doc,.docx,.hwp,.pdf" onchange="checkSize2('0');" />
																</div>
															</div>
															<!--file_input-->
															<div class="button_action" style="right:0;">
																<span class="pointer f_right p_r10" style="margin:0;">
																<img src='../../resources/cmresource/css/iplanbiz/theme/style01/img/icon-cir-plus.png' alt="추가" onclick="addFile2();" height=15>
																</span>
																<br>
															</div>
															<!--button_action-->
														</div>
														<!--group-->
														
													</div>
													
													
												</div></td>
										</tr>
									</tbody>
								</table>
							</div>
							<!--table-->
							
							<div class="group f_left  w100p m_t10">
								<div class="label type2 f_left">
									예산 및 집행내역
								</div>
							</div>
							<!--group-->
							<div class="group f_left" style="width:100%; margin:10px 0;">
								<div class="label type2 f_left m_r5 none_bg_img">
									<span class="sublabel">예산액(천원):</span>
									<input type="text" value="" id="budget_total2" readonly class="input type2 t_right"  style="width:150px; margin:3px 0;height: 18px;padding: 1px 0;"/>
								</div>
								<div class="label type2 f_left m_r5 none_bg_img">
									<span class="sublabel">집행액(천원):</span>
									<input type="text" value="" id="enforce_total2" readonly class="input type2 t_right"  style="width:150px; margin:3px 0;height: 18px;padding: 1px 0;"/>
								</div>
								<div class="label type2 f_left m_r5 none_bg_img">
									<span class="sublabel">잔액(천원):</span>
									<input type="text" value="" id="change_total2" readonly class="input type2 t_right"  style="width:150px; margin:3px 0;height: 18px;padding: 1px 0;"/>
								</div>
							</div>
							<!--group-->
							<!-- 
							<div class="group f_left  w100p m_t10">
								<div class="label type2 f_left">
									<span class="f_left">집행내역:</span><span class="label sublabel type1" id="insertBudgetNm"></span>
									<input type="hidden" id="insertBudgetId" name="budget_id"/>
									<input type="hidden" id="limitAmount"/>
								</div>
							</div>
							<div class="blueish datatable f_left" style="width:100%; height:150px; margin:5px 0; overflow:hidden;">
								<div class="datatable_fixed" style="width:100%; height:150px; float:left; padding:25px 0;">
									<div style=" height:125px !important; overflow-x:hidden;">
										<table  width="100%" cellspacing="0" cellpadding="0" border="0"  class="none_hover">
											<thead>
												<tr>
													<th style="width:25%;">집행일</th>
													<th style="width:25%;">적요</th>
													<th style="width:15%;">금액(천원)</th>
													<th style="width:25%;">비고</th>
													<th style="width:10%;"> <div class="pointer m_t3">
															<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/icon-plus.png" alt="추가" height="19" onclick="addEnforce();">
														</div></th>
													<th style="width:17px; min-width:17px; border-bottom:0;"></th>
												</tr>
											</thead>
											<tbody id="tbodyRunEnforce">
												<tr name="inputEnforceTr">
													<td  style="width:25%;"><div class="cell">
															<input id="enforce_start_dat0" name="enforce_start_dat" type="text" readonly class="dateInput input type2 t_center f_left" style="width:80px; margin:3px 0;" value=""/>
															<div class="f_left m_l5 m_r5 m_t2">
																&sim;
															</div>
															<input id="enforce_end_dat0" name="enforce_end_dat" type="text" readonly class="dateInput input type2 t_center f_left" style="width:80px; margin:3px 0;" value=""/>
														</div></td>
													<td style="width:25%;"><div class="cell">
															<input type="text" value="" id="enforce_content0" name="enforce_content" class="input type2  f_left"  style="width:98%; margin:3px 0;"/>
														</div></td>
													<td style="width:15%;"><div class="cell t_right">
															<input type="text" value="" id="enforce_amount0" name="enforce_amount" class="input type2  f_left t_right"  style="width:98%; margin:3px 0;"/>
														</div></td>
													<td style="width:25%;"><div class="cell">
															<input type="text" value="" id="enforce_def0" name="enforce_def" class="input type2  f_left"  style="width:98%; margin:3px 0;"/>
														</div></td>
													<td style="width:10%;"><div class="cell">
															<div class="pointer t_center">
																<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/icon-minus.png" alt="삭제" height="19" onclick="inputEnforceReset(0);">
															</div>
														</div></td>
												</tr>
												
											</tbody>
										</table>
									</div>
								</div>
							</div>-->
							<!--table -->
							
							<!-- test -->
							<div class="tabs f_left" style=" width:100%; height:420px; margin-top:0px;">
								<div id='jqxTabs03'>
									<ul>
										<li style="margin-left: 0px;">예산 및 집행</li>
										<li>집행내역 상세</li>
									</ul>
									<div class="tabs_content" style="height:100%; ">
										<div class="content f_left" style="width:97%; margin:10px 1.5%;">
											<div class="grid f_left" style="width:100%; margin:5px 0%; height:351px;">
												<div id="budgetGrid">
												</div>
											</div>
											<!--grid -->
										</div>
									<!--content-->
									</div>
									<!--//tabs_content-->
									<div class="tabs_content" style="height:100%; ">
										<div class="content f_left" style="width:97%; margin:10px 1.5%;">
											<div class="blueish datatable f_left" style="width:100%; height:353px; margin:5px 0; overflow:hidden;">
											<!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 헤더가 50px일경우, 300px - 50px= 250px)-->
											<div class="datatable_fixed" style="width:100%; height:353px; float:left; padding:50px 0;">
												<!--group으로 헤더높이가 길어질 경우 padding  (기본높이는 25px)줘야 함-->
												<div style=" height:304px !important; overflow-x:auto;">
													<table  width="100%" cellspacing="0" cellpadding="0" border="0"  class="none_hover">
														<thead>
															<tr>
																<th rowspan="2" style="width:10%;">기획 및 예산</th>
																<th rowspan="2"  style="width:5.5%;">구분</th>
																<th rowspan="2"  style="width:6.5%;">예산액</th>
																<th colspan="13" style="width:71.5%;">집행액</th>
																<th rowspan="2"  style="width:6.5%;">잔액</th>
																<th rowspan="2" style="width:17px; min-width:17px; border-bottom:0;"></th>
															</tr>
															<tr>
																<th style="width:5.5%;">3월</th>
																<th style="width:5.5%;">4월</th>
																<th style="width:5.5%;">5월</th>
																<th style="width:5.5%;">6월</th>
																<th style="width:5.5%;">7월</th>
																<th style="width:5.5%;">8월</th>
																<th style="width:5.5%;">9월</th>
																<th style="width:5.5%;">10월</th>
																<th style="width:5.5%;">11월</th>
																<th style="width:5.5%;">12월</th>
																<th style="width:5.5%;">1월</th>
																<th style="width:5.5%;">2월</th>
																<th style="width:5.5%; border-right:1px solid #a1bad9 !important;">합계</th>
															</tr>
														</thead>
														<tbody id="monthEnforceTr">
															<tr>
																<td  style="width:10%;"><div class="cell">
																		연구위원회 구성1
																	</div></td>
																<td style="width:5.5%;"><div class="cell">
																		자체
																	</div></td>
																<td style="width:6.5%;"><div class="cell t_right">
																	</div></td>
																<td style="width:5.5%;"><div class="cell">
																		<input type="text" value="" id="" class="input type2  f_left"  style="width:85%; margin:3px 0;"/>
																	</div></td>
																	<td style="width:5.5%;"><div class="cell">
																		<input type="text" value="" id="" class="input type2  f_left"  style="width:85%; margin:3px 0;"/>
																	</div></td>
																	<td style="width:5.5%;"><div class="cell">
																		<input type="text" value="" id="" class="input type2  f_left"  style="width:85%; margin:3px 0;"/>
																	</div></td>
																	<td style="width:5.5%;"><div class="cell">
																		<input type="text" value="" id="" class="input type2  f_left"  style="width:85%; margin:3px 0;"/>
																	</div></td>
																	<td style="width:5.5%;"><div class="cell">
																		<input type="text" value="" id="" class="input type2  f_left"  style="width:85%; margin:3px 0;"/>
																	</div></td>
																	<td style="width:5.5%;"><div class="cell">
																		<input type="text" value="" id="" class="input type2  f_left"  style="width:85%; margin:3px 0;"/>
																	</div></td>
																	<td style="width:5.5%;"><div class="cell">
																		<input type="text" value="" id="" class="input type2  f_left"  style="width:85%; margin:3px 0;"/>
																	</div></td>
																	<td style="width:5.5%;"><div class="cell">
																		<input type="text" value="" id="" class="input type2  f_left"  style="width:85%; margin:3px 0;"/>
																	</div></td>
																	<td style="width:5.5%;"><div class="cell">
																		<input type="text" value="" id="" class="input type2  f_left"  style="width:85%; margin:3px 0;"/>
																	</div></td>
																	<td style="width:5.5%;"><div class="cell">
																		<input type="text" value="" id="" class="input type2  f_left"  style="width:85%; margin:3px 0;"/>
																	</div></td>
																	<td style="width:5.5%;"><div class="cell">
																		<input type="text" value="" id="" class="input type2  f_left"  style="width:85%; margin:3px 0;"/>
																	</div></td>
																	<td style="width:5.5%;"><div class="cell">
																		<input type="text" value="" id="" class="input type2  f_left"  style="width:85%; margin:3px 0;"/>
																	</div></td>
																	<td style="width:5.5%;"><div class="cell">	
																	</div></td>
																<td style="width:6.5%;"><div class="cell">
																	</div></td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
										<!--table -->
									</div>
									<!--content-->
								</div>
								<!--//tabs_content-->
							</div>
							<!--//jqxTabs-->
						</div>
						<!--//tabs-->	
							<!-- test -->
							
							<div class="group_button f_right m_b10" style="margin-top:20px;">
								<div class="button type3 f_left" id="hidden2_1">
									<input type="button" value="운영취소" id='cancleEnforceBtn' width="100%" height="100%" />
								</div>
								<div class="button type3 f_left" id="hidden2_2">
									<input type="button" value="운영완료" id='confirmEnforceBtn' width="100%" height="100%" />
								</div>
								<div class="button type2 f_left" id="hidden2_3">
									<input type="button" value="저장" id='saveEnforceBtn' width="100%" height="100%" />
								</div>
							</div>
							<!--group_button-->
						</div>
						<!--content-->
					</form>
					</div>
					<!--//tabs_content-->
					
					
					<!-- 부서평가탭S -->	
					<div  class="tabs_content" style=" height:100%; ">
					<form name="saveForm3" id="saveForm3">
						<input type="hidden" id="ms_id3" name="ms_id"/>
						<input type="hidden" id="yyyy3" name="yyyy"/>
						<input type="hidden" id="assign_id3" name="assign_id" />
						<input type="hidden" id="program_id3" name="program_id"/>
						<div class="content f_left" style="width:97%; margin:10px 1.5%;">
							<div class="table  f_left" style="width:100%; margin:0; ">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<th rowspan="5" width="8%"><div>
													사업목표
												</div></th>
											<th rowspan="3" width="8%"><div>
													정량
												</div></th>
											<th width="12.8%"><div>
													목표값
												</div></th>
											<th width="12.8%"><div>
													단위
												</div></th>
											<th width="12.8%"><div>
													달성값
												</div></th>
											<th width="12.8%"><div>
													달성률(%)
												</div></th>
											<th colspan="2" width="12.8%"><div>
													비고
												</div></th>
										</tr>
										<tr>
											<td><div  class="cell">
												<input type="text" class="input type2 t_center f_left" id="quan_goal3" readonly style="width:95%;border:none;background:transparent;"/>
												</div></td>
											<td><div  class="cell">
											<input type="text" class="input type2 t_center f_left" id="quan_unit3" readonly style="width:95%;border:none;background:transparent;"/>
												</div></td>
											<td><div  class="cell">
											<input type="text" class="input type2 t_center f_left" id="quan_ach_val" name="quan_ach_val" style="width:95%;" onkeyup="calAchRate();"/>
												</div></td>
											<td><div  class="cell">
											<input type="text" class="input type2 t_center f_left" id="quan_ach_rate" readonly name="quan_ach_rate" style="width:95%;border:none;background:transparent;"/>
												</div></td>
											<td colspan="2" ><div  class="cell">
											<input type="text" class="input type2 t_left f_left" id="quan_def3" name="quan_def" style="width:95%;"/>
												</div></td>
										</tr>
										<tr>
											<td colspan="6"><div  class="cell">
													<textarea   id="quan_content3" name="quan_content" class="textarea"  style=" width:99.5%; height:45px; margin:3px 0;overflow:auto;"  placeholder="Varchar2 2000자"></textarea>
												</div></td>
										</tr>
										<tr>
											<th rowspan="2" width="8%"><div>
													정성
												</div></th>
											<th width="16%"><div>
													달성여부
												</div></th>
											<th colspan="5"><div>
													사유
												</div></th>
										</tr>
										<tr>
											<td><div  class="cell t_center">
													<label for="">
														<input type="radio" id="qual_ach_y" name="qual_ach_yn" value="Y" class="m_b2 m_r3">
														달성</label>
													<label for="">
														<input type="radio" id="qual_ach_n" name="qual_ach_yn" value="N" class="m_b2 m_r3 m_l5">
														미달성</label>
												</div></td>
											<td colspan="5"><div  class="cell">
													<textarea   id="qual_content3" name="qual_content" class="textarea"  value="" style=" width:99.5%; height:45px; margin:3px 0;overflow:auto;"  placeholder="Varchar2 2000자"></textarea>
												</div></td>
										</tr>
										<tr>
											<th rowspan="2" colspan="2" width="16%"><div>
													만족도(명)
												</div></th>
											<th  width="16%"><div>
													응답자
												</div></th>
											<th width="12.8%"><div>
													매우만족
												</div></th>
											<th width="12.8%"><div>
													만족
												</div></th>
											<th width="12.8%" ><div>
													보통
												</div></th>
											<th width="12.8%"><div>
													불만족
												</div></th>
											<th width="12.8%"><div>
													매우불만족
												</div></th>
										</tr>
										<tr>
											<td><div  class="cell t_center">
													<input type="hidden" id="cnt_tot" name="cnt_tot" />
													<span id="cnt_tot_txt"></span>
												</div></td>
											<td><div  class="cell t_center">
													<input type="text" class="input type2 t_center f_left" id="cnt_a" name="cnt_a" style="width:85%;" onkeyup="calCntTotal();"/><div style="padding-top:5px;">명</div>
												</div></td>
											<td><div  class="cell t_center">
												<input type="text" class="input type2 t_center f_left" id="cnt_b" name="cnt_b" style="width:85%;" onkeyup="calCntTotal();"/><div style="padding-top:5px;">명</div>
												</div></td>
											<td><div  class="cell t_center">
												<input type="text" class="input type2 t_center f_left" id="cnt_c" name="cnt_c" style="width:85%;" onkeyup="calCntTotal();"/><div style="padding-top:5px;">명</div>
												</div></td>
											<td><div  class="cell t_center">
												<input type="text" class="input type2 t_center f_left" id="cnt_d" name="cnt_d" style="width:85%;" onkeyup="calCntTotal();"/><div style="padding-top:5px;">명</div>
												</div></td>
											<td><div  class="cell t_center">
												<input type="text" class="input type2 t_center f_left" id="cnt_e" name="cnt_e" style="width:85%;" onkeyup="calCntTotal();"/><div style="padding-top:5px;">명</div>
												</div></td>
										</tr>
										<tr>
											<th rowspan="2" width="8%"><div>
													성과평가
												</div></th>
											<th rowspan="2" width="8%"><div>
													자체
												</div></th>
											<th width="16%"><div>
													점수
												</div></th>
											<th colspan="5"><div>
													평가결과
												</div></th>
										</tr>
										<tr>
											<td style="text-align:center;"><div  class="cell t_center" style="width:100px;margin:0 auto;">
												<select id="eval_score" name="eval_score">
													<option value="0" selected>선택</option>
													<option value="5">5점</option>
													<option value="4">4점</option>
													<option value="3">3점</option>
													<option value="2">2점</option>
													<option value="1">1점</option>
													
												</select>
												</div></td>
											<td colspan="5"><div  class="cell">
													<textarea id="eval_result" name="eval_result" class="textarea" style=" width:99.5%; height:45px; margin:3px 0;overflow:auto;"  placeholder="Varchar2 2000자"></textarea>
												</div></td>
										</tr>
										
										</tbody>
									
								</table>
							</div>
							<!--table-->
							<div class="group_button f_right m_b10" style="margin-top:20px;">
								<div class="button type3 f_left" id="hidden3_1">
									<input type="button" value="평가취소" id='deptEvalCancleBtn' width="100%" height="100%" />
								</div>
								<div class="button type3 f_left" id="hidden3_2">
									<input type="button" value="평가완료" id='deptEvalConfirmBtn' width="100%" height="100%" />
								</div>
								<div class="button type2 f_left" id="hidden3_3">
									<input type="button" value="저장" id='deptEvalSaveBtn' width="100%" height="100%" />
								</div>
							</div>
							<!--group_button-->
						</div>
						<!--//content-->
					</form>
					</div>
					<!--//tabs_content-->
			
					<!-- 위원회평가탭S -->
					<div class="tabs_content" style="height:100%; ">
						<div class="content f_left" style="width:97%; margin:10px 1.5%;">
							<div class="table  f_left" style="width:100%; margin:0; ">
							<form name="saveForm4" id="saveForm4">
								<input type="hidden" id="ms_id4" name="ms_id"/>
								<input type="hidden" id="yyyy4" name="yyyy"/>
								<input type="hidden" id="program_id4" name="program_id"/>
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										
										<tr>
											<th style="width:10%;"><div>
													잘된 점
												</div></th>
											<td style="width:80%;"><div class="cell">
												<textarea id="good_content4" name="good_content4" class="textarea" readonly style=" width:99.5%; height:100px; margin:3px 0; background:#ffffff; border-color: #ffffff;overflow:auto;"></textarea>
											</div></td>
											<th style="width:10%;"><div>
												 점수
												</div></th>
										</tr>
										<tr>
											<th style="width:10%;"><div>
													권고 및 보완사항
												</div></th>
											<td><div  class="cell">
													<textarea  id="imp_content4" name="imp_content4" class="textarea" readonly style=" width:99.5%; height:100px; margin:3px 0; background:#ffffff; border-color: #ffffff;overflow:auto;"></textarea>
												</div></td>
												<td rowspan="2"><div class="cell">
												<textarea id="eval_score4" name="eval_score4" class="textarea" readonly style=" width:99.5%; height:90px; margin:3px 0; background:#ffffff; border-color: #ffffff; text-align: center;overflow:auto; padding: 100px 0px 50px;"></textarea>
											</div></td>
										</tr>
										<tr>
											<th style="width:10%;"><div>
													우수사례
												</div></th>
											<td style="border-right: 1px solid #e1e1e1 !important;"><div  class="cell">
													<textarea   id="best_content4" name="best_content4" class="textarea" readonly style=" width:99.5%; height:65px; margin:3px 0; background:#ffffff; border-color: #ffffff;overflow:auto;"></textarea>
												</div></td>
										</tr>
									</tbody>
								</table>
							</form>
							</div>
							
						</div>
					</div>
					<!--//tabs_content-->
					
					<!-- 보고서탭S -->
					<div class="tabs_content" style="height:100%; ">
						<div class="content f_left" style="width:97%; margin:10px 1.5%;">
							
						</div>
						<!--content-->
					</div>
					<!--//tabs_content-->
				</div>
				<!--//jqxtabs-->
			</div>
			<!--tabs-->
			
		</div>
		<!--content-->
	</div>
	<!--//tabs_content-->
</div>
<!--//jqxTabs-->

<!--//wrap-->
</body>
</html>