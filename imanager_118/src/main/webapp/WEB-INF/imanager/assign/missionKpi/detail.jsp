<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <title>지표관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/widget/Form.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
        <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
	    <script src="../../resources/cmresource/js/thirdparty/LGPL/v2/smartEditor2/js/HuskyEZCreator.js" charset="UTF-8"></script>
	    
        <script type="text/javascript">// button
            $(this).ready(function () {             
            	$i.dialog.setAjaxEventPopup("SYSTEM","데이터 처리중입니다");
            	init();
            	$('#saveForm').jqxValidator({
    				rtl:true,  
    				rules:[
    				    { input: "#assign_cd", message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' },
                        { input: '#assign_nm', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' },
                        { input: '#txtStranm', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' },
                        { input: '#txtSubstranm', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' },
                        { input: '#txtDeptnm', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' },
                        { input: '#dateInputStart', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' },
                        { input: '#dateInputClose', message: "<img src='../../resources/cmresource/image/icon_must_red.png'>", action: 'change, blur', rule: 'required' }
    		        ]
    			});
            	
            	 if($("#assign_id").val()==""){//추진과제 신규
            		 $("#hiddenBtn1").attr("style","display:none");
            	 }
            });
        	function init(){
        		$("#listBtn").jqxButton({ width: '',  theme:'blueish'}).on('click',goList);  
				$("#assignNewBtn").jqxButton({ width: '',  theme:'blueish'}).on('click',resetForm1);     
				$("#assignSaveBtn").jqxButton({ width: '',  theme:'blueish'}).on('click',saveAssignment);  
                $("#assignDeleteBtn").jqxButton({ width: '',  theme:'blueish'}).on('click',deleteAssignment);
					
				
                $('#dateInputStart, #dateInputClose').datepicker({
        			//changeYear: true,
        			//changeMonth: true,
        			showOn: 'button',
        			buttonImage: '../../resources/cmresource/image/icon-calendar.png',
        			buttonImageOnly: true,
        			dateFormat: 'yymmdd', //2014.12.12
        			onSelect:function(dateText){
    					if($("#dateInputClose").val() != ""){
    						if($("#dateInputClose").val() < $("#dateInputStart").val()){
    							$i.dialog.warning("SYSTEM","종료일자가 시작일자보다 과거입니다.");
    							$("#dateInputClose").val("");
    						}	
    					}
    				}
        		});
        		$('.ui-datepicker-trigger').addClass('icon-calendar f_left m_t7 pointer');
        		
            	initSmartEditor('assign_content');
            	makeKpiTable();
        	}
        
        	function goList(){
        		location.href='./crud?yyyy='+$("#yyyy").val()+"&ms_id="+$("#ms_id").val()+"&stra_id="+$("#txtStraid").val()+"&substra_id="+$("#txtSubstraid").val();
        	}
            var oEditors = [];
    		
    		function initSmartEditor(id) {
    			nhn.husky.EZCreator.createInIFrame({
    				oAppRef: oEditors,
    				elPlaceHolder: id,
    				sSkinURI : "../../resources/cmresource/js/thirdparty/LGPL/v2/smartEditor2/SmartEditor2Skin.html",
    				htParams : {bUseToolbar : true,
    					fOnBeforeUnload : function(){
    					}
    				},
    				fOnAppLoad : function(){
    					if(id=="assign_content"){
							$("#assign_content").val("${detailAssignMent.ASSIGN_CONTENT}");
    		    			oEditors.getById["assign_content"].exec("LOAD_CONTENTS_FIELD");
    					}
    				},
    				fCreator: "createSEditor2"
    			});
    		}
    		
        	function resetForm1(){
        		$('#saveForm').jqxValidator('hide');
        		$("#hiddenBtn1").attr("style","display:none");
        		$("#assign_id").val("");
        		$("#assign_cd").val("");
        		$("#assign_nm").val("");
        		$("#txtStraid").val("");
        		$("#txtStranm").val("");
        		$("#txtSubstraid").val("");
        		$("#txtSubstranm").val("");
        		$("#txtDeptcd").val("");
        		$("#txtDeptnm").val("");
        		
        		$("#dateInputStart").val("");
        		$("#dateInputClose").val("");
        		
        		oEditors.getById["assign_content"].exec("SET_IR", [""]);

        		$("#assign_desc").html("");
        		 
        		$('input:checkbox[name = "check"]').attr("checked",false);
        		
        		
        	}
        	
        	function byteCheck(code) {
    			var size = 0;
    			for (i = 0; i < code.length; i++) {
    				var temp = code.charAt(i);
    				if (escape(temp) == '%0D')
    			   		continue;
    			  	if (escape(temp).indexOf("%u") != -1) {
    			   		size += 3;
    			  	} else {
    			   		size++;
    			 	}
    		 	}
    		 	return size;
    		}
        	function fRemoveHtmlTag(string) { 
     		   var objReplace = new RegExp();
     		   var objnbsp = new RegExp();
     		   objReplace = /[<][^>]*[>]/gi; 
     		   objnbsp = /&nbsp;/gi;
     		   return string.replace(objReplace, "").replace(objnbsp,"").replace(/(\s*)/g, ""); 
        	}
        	function saveAssignment(){
        		oEditors.getById["assign_content"].exec("UPDATE_CONTENTS_FIELD", []);
        		
				
				var smartCheck = oEditors.getById["assign_content"].getIR();
				if(byteCheck(fRemoveHtmlTag(smartCheck)) > 60000){ 
					$i.dialog.warning('SYSTEM',"1~20000자(한글 기준)의 한글/영문/특수문자 혼용만 가능합니다.");
					return false;
				}
				
				
				if($("#saveForm").jqxValidator("validate") == true){
					$i.insert("#saveForm").done(function(args){
	        			if(args.returnCode == "EXCEPTION"){
	        				$i.dialog.error("SYSTEM","저장 중 오류가 발생했습니다."+args.returnMessage);
	        			}else{
	        				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
	        					goList();
	        				});
	        				
	        			}
	        		}).fail(function(e){ 
	        			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
	        		});
				}
				
        	}
        
        	
        	function deleteAssignment(){
        		var ms_id = $("#ms_id").val();
        		var assign_id = $("#assign_id").val();
        		if(assign_id!=""){
        			$i.dialog.confirm('SYSTEM','추진과제를 삭제하시겠습니까?',function(){
            			
            			$i.remove("#saveForm").done(function(args){
                			if(args.returnCode == "EXCEPTION"){
                				if(args.returnMessage=="WARNING"){
                					$i.dialog.warning("SYSTEM","매핑된 프로그램이 존재합니다.");
                				}else{
                					$i.dialog.error("SYSTEM","삭제 중 오류가 발생했습니다."+args.returnMessage);
                				}
                			}else{
                				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
                					goList();
                				});
                				
                			}
                		}).fail(function(e){ 
                			$i.dialog.error("SYSTEM","삭제 중 오류가 발생 했습니다.");
                		});
            			
            		});	
        		}
        	}
        	
        
        	function popupStraSubstra(){ 
        		var ms_id = $("#ms_id").val();
        		window.open('./popupMappingKpiStrategy?ms_id='+ms_id, 'popupMappingKpiFormula','scrollbars=no, resizable=no, width=935, height=380');  
        	}
        	function popupDept(type){ 
        		window.open('./popupMappingDeptList?type='+type, 'popupMappingKpiFormula','scrollbars=no, resizable=no,width=935, height=380');  
        	}
        	
        	function chkbox(){
	    		$("input[name=check]").click(function(){
	    			var rowData = new Array();
	    			var kpiArr = new Array();
	    			var checkbox = $("input[name=check]:checked");
	    			
	    			checkbox.each(function(i) {
	    				var tr = checkbox.parent().parent().eq(i);
	    				var td = tr.children();
	    				
	    				rowData.push(tr.text());
	    				
	    				var kpiId = td.eq(2).text();
	    			
	    				kpiArr.push(kpiId); 

	    				});
		    			$("#kpiid").html(kpiArr);
	    				$("#kpiList").val(kpiArr.toString());
	    			});
	    	}
        	function makeKpiTable(){
        		var ms_id = $("#ms_id").val();
        		var assign_id = $("#assign_id").val();
	    		var data = $i.sqlhouse(347,{ MS_ID : ms_id, ASSIGN_ID : assign_id });
    			$("#makeTable").empty();
    			data.done(function(res){
    				res = res.returnArray;
    				for(var i=0;i<res.length;i++){ 		            	
	    				var html ="";
	    				html += "<tr id='makeTr'>";
	    				html += "<td align='center' style='width:3%; display:none;'>"+ms_id+"</td>";
	    				html += "<td align='center' style='width:3%; display:none;'>"+assign_id+"</td>";
	    				html += "<td align='center' style='width:3%; display:none;'>"+res[i].KPI_ID+"</td>";
    					html += "<td align='center' style='width:20%;'>"+res[i].KPI_GBN+"</td>"; 
    					html += "<td align='center' style='width:40%;'>"+$i.secure.scriptToText(res[i].KPI_NM)+"</td>"; 
    					html += "<td align='center' style='width:30%;'>"+res[i].UNIT+"</td>"; 
    					html += "<td align='center' style='width:80%;'><input type='checkbox' id='chkBx"+i+"' name='check' style='width:18px; height:18px;' ></td>"; 
    					html += "</tr>";
    					$("#makeTable").append(html);
    					
    					chkbox();
    						if(res[i].YN == "Y"){ 
				        		$('input:checkbox[id = "chkBx'+i+'"]').attr("checked",true);
    	    				}
	    				}
    				});
    			}
    	</script>
 		
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
			#gridKpilist .jqx-cell {
			  padding: 3px 0px 3px 5px     
			}
			.jqx-validator-hint{
        		background-color : transparent !important;
        		border:0px !important;
        	}
        	.jqx-validator-hint-arrow{
        		display:none !important;
        	}
		</style>
    </head>
<body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
<div class="wrap" style="width:98%; min-width:1580px; margin:0 1%;">
  <div class="container  f_left" style="width:100%; margin:10px 0;">
    <div class="content f_left" style=" width:100%;">
		 <div class="group f_left  w100p m_b5">
		 		
             	 <div class="group_button f_right">	
             	 <div class="button type2 f_left" style="margin-bottom:0;">
                   <input type="button" value="목록" id='listBtn' width="100%" height="100%" />
                 </div>
				 <div class="button type2 f_left" style="margin-bottom:0;">
                   <input type="button" value="신규" id='assignNewBtn' width="100%" height="100%" />
                 </div>
				 <div class="button type2 f_left" style="margin-bottom:0;">
                   <input type="button" value="저장" id='assignSaveBtn' width="100%" height="100%" />
                 </div>
                  <div class="button type3 f_left" style="margin-bottom:0;" id="hiddenBtn1">
                   <input type="button" value="삭제" id='assignDeleteBtn' width="100%" height="100%" />
                 </div>				 
             </div><!--group_button-->
       		 </div>
            <!--group-->
        <form id="saveForm" name="saveForm">
        	<div class="table  f_left" style="width:100%; margin:0; ">
        		<input type="hidden" id="ms_id" name="ms_id" value="${param.ms_id}"/>
        		<input type="hidden" id="yyyy" name="yyyy" value="${param.yyyy}"/>
        		<input type="hidden" id="assign_id" name="assign_id" value="${detailAssignMent.ASSIGN_ID}"/>
                 <table width="100%" cellspacing="0" cellpadding="0" border="0">
                     <tbody>
						  <tr>
						  	 <th class="w15p"><div>실행과제코드 &frasl; 명<span class="th_must"></span>
                             <td colspan="3"><div class="cell"><input type="text" value="${detailAssignMent.ASSIGN_CD}" id="assign_cd" name="assign_cd" class="input type2  f_left t_center"  style="width:20%; margin:3px 0;"/> 
                             <span class="f_left" style=" margin:4px 6px;">&frasl;</span> <input type="text" value="${detailAssignMent.ASSIGN_NM}" id="assign_nm" name="assign_nm" class="input type2  f_left"  style="width:76%; margin:3px 0; "/></div></td>
						  </tr>
                           <tr>
                               <th><div>발전목표<span class="th_must"></span></div></th>
                               <td class="w35p"><div  class="cell"><input type="text" readonly value="${detailAssignMent.S_STRA_NM}" id="txtStranm" class="input type2 f_left"  style="width:97%; margin:3px 0; "/><input type="hidden" id="txtStraid" name="stra_id" value="${detailAssignMent.STRA_ID}"/> </div></td>
                               <th class="w15p"><div>혁신전략<span class="th_must"></span></div></th>
                               <td class="w35p"><div  class="cell"><input type="text" readonly value="${detailAssignMent.S_SUBSTRA_NM}" id="txtSubstranm" class="input type2  f_left"  style="width:90%; margin:3px 0; "/> <input type="hidden" id="txtSubstraid" name="substra_id" value="${detailAssignMent.SUBSTRA_ID}"/><div class="icon-search f_left m_t7 pointer" onclick="popupStraSubstra();"></div></div></td>
                          </tr>
                          <tr>
                               <th><div>주관부서<span class="th_must"></span></div></th>
                               <td class="w35p"><div  class="cell"><input type="text" readonly value="${detailAssignMent.DEPT_NM}" id="txtDeptnm" name="dept_nm" class="input type2  f_left"  style="width:90%; margin:3px 0; "/> <input type="hidden" id="txtDeptcd" name="dept_cd" value="100"/><div class="icon-search f_left m_t7 pointer" onclick="popupDept('main')"></div></div></td>
                             	<th><div>사용기간<span class="th_must"></span></div></th>
                               <td class="w35p"><div  class="cell">
                               <c:if test="${detailAssignMent==null}">
                               		<input id="dateInputStart" readonly type="text" class="input type2 t_center f_left" style="width:100px; margin:3px 0;" value="${SDAT}" name="start_dat"/>
    								<div class="f_left m_l5 m_r5 m_t2">
										&sim;
									</div>
									<input id="dateInputClose" type="text" class="input type2 t_center f_left" readonly style="width:100px; margin:3px 0;" value="${EDAT}" name="end_dat"/>                           
	                               	
                               </c:if>
                               <c:if test="${detailAssignMent!=null}">
                               		<input id="dateInputStart" readonly type="text" class="input type2 t_center f_left" style="width:100px; margin:3px 0;" value="${detailAssignMent.START_DAT}" name="start_dat"/>
                               		<div class="f_left m_l5 m_r5 m_t2">
										&sim;
									</div>
									<input id="dateInputClose" type="text" class="input type2 t_center f_left" readonly style="width:100px; margin:3px 0;" value="${detailAssignMent.END_DAT}" name="end_dat"/>
                               </c:if>
                               
								</div></td>
                             
                             
                             </tr>
						   
						<c:set var = "SQUOT">'</c:set>
						<c:set var = "DQUOT">"</c:set>
						    <tr>
                               <th><div>주요내용</div></th>
                              <td colspan="3"><div  class="cell">
                               <textarea id="assign_content" name="assign_content" class="textarea" style=" width:99.5%; height:150px; margin:3px 0;" >${fn:replace(fn:replace(fn:replace(fn:replace(detailAssignMent.ASSIGN_CONTENT,SQUOT,"&#39;"),DQUOT,"&quot;"),"<","&lt;"),">","&gt;")}</textarea>
                               </div></td>
                          </tr>
						     <tr>
                               <th><div>기타사항</div></th>
                               <td colspan="3"><div  class="cell"><textarea id="assign_desc" name="assign_desc" class="textarea"   style=" width:99.5%; height:65px; margin:3px 0;overflow:auto;"  placeholder="Varchar2 2000자">${detailAssignMent.ASSIGN_DESC}</textarea></div></td>
                          </tr>
                      </tbody>	
                </table>
            </div>
            <!--table-->
            
             <div class="label type2 f_left m_t5">  
				<div style="float: left;">지표</div>  
			</div>
            
            <div class="blueish datatable f_left" style="width:100%; height:250px; margin:10px 0; overflow:hidden;">     
						<input type="hidden" name="kpi_id" id="kpiList"/>	
						<!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 헤더가 50px일경우, 300px - 50px= 250px)-->
					<div class="datatable_fixed" style="width:100%; height:250px; float:left; padding:25px 0;">
							<!--group으로 헤더높이가 길어질 경우 padding  (기본높이는 25px)줘야 함-->
							<div style=" height:225px; !important; overflow-x:hidden;">
									<table  width="100%" cellspacing="0" cellpadding="0" border="0">
										<thead>
											<tr>
												<th style="width:20%;">지표구분</th>
												<th style="width:40%;">성과지표</th>
												<th style="width:30%;">단위</th> 
												<th style="width:80%;">선택</th>
												<th style="width:17px; min-width:17px; border-bottom:0;"></th>
											</tr>
										</thead>
										<tbody id="makeTable">
										</tbody>
									</table>
							</div>		
					</div>
			</div>		
       </form>
    </div>
    <!--//content--> 
  </div>
  <!--//container--> 
</div>
<!--//wrap-->
</body>
</html>

