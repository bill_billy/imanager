<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <title>실행과제관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" hrlef="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
    	<script type="text/javascript">// button
    	var paramMsid = "${param.ms_id}";
    	var paramYyyy = "${param.yyyy}";
    	var paramStraid = "${param.stra_id}";
    	var paramSubstraid = "${param.substra_id}";
    	$(this).ready(function () {    
    		init();
        });
    	
    	function init(){
    		
    		$("#jqxButtonSearch").jqxButton({ width: '',  theme:'blueish'}).on('click',search); 
			$("#jqxButtonNew01").jqxButton({ width: '',  theme:'blueish'}).on("click",function(){
				
				detailAssign(0);
			});
			$("#jqxButtonNew02").jqxButton({ width: '',  theme:'blueish'}).on("click",function(){
				save2();
			}); 
		
            
			$('#jqxTabs01').jqxTabs({ width: '100%', height: '100%', position: 'top', theme:'blueish', selectedItem: 0});
            $('#jqxTabs02').jqxTabs({ width: '100%', height: '100%', position: 'top',theme:'blueish', selectedItem: 0});
            
            
            $("#txtSearchWord").on('keydown',function() {
					if(event.keyCode==13) {
						makeAssignSearch();
						return false;
					}
				}
			);
			makeMissionCombo();
			
			search('init');
			
			
    		
    	}
    	var missionList = [];
    	var missionDat;
		function makeMissionCombo(){
			missionDat = $i.sqlhouse(322,{});
			missionDat.done(function(res){
				missionList = res.returnArray;
				
				missionList.forEach(function(dataOne) {
    				dataOne.MS_NM = $i.secure.scriptToText(dataOne.MS_NM);
    			});
				
        		var source = 
        		{
        			datatype:"json",
        			datafields:[
        				{ name : "MS_ID" },
        				{ name : "MS_NM" }
        			],
        			id:"id",
        			localdata:res,
        			async : false
        		};
        		var dataAdapter = new $.jqx.dataAdapter(source);
        		 $("#jqxCombobox01").jqxComboBox({ 
        			//	selectedIndex: 0, 
        				source: dataAdapter, 
        				animationType: 'fade', 
        				dropDownHorizontalAlignment: 'right', 
        				displayMember: "MS_NM", 
        				valueMember: "MS_ID", 
        				dropDownWidth: 300, 
        				dropDownHeight: 100, 
        				width: 300, 
        				height: 22,  
        				theme:'blueish'});
        		 
        		$('#jqxCombobox01').on('select', function (event) 
        		{
        			var args = event.args;
        			if (args) {                  
        				var index = args.index;
        				makeYearCombo(index);
        			}	     
        		}); 
        		if(paramMsid==''){
         			$("#jqxCombobox01").jqxComboBox('selectIndex', 0 );
         		}  
        		else
         			{$("#jqxCombobox01").jqxComboBox('selectItem', paramMsid ); 
         		}
        		
        	});  
     	}//makeMissionCombo
		
     	var yyyyList = [];
		function makeYearCombo(idx){
			var start = missionList[idx].START_YYYY;
     		$("#s_yyyy").val(start);
     		var end = missionList[idx].END_YYYY;
     		$("#e_yyyy").val(end);
     		var nowYear = new Date().getFullYear();
     		if(nowYear >= start && nowYear < end) //현재기준으로 설정
     			end = nowYear;
     		
     		yyyyList = [];
     		for(var i=Number(end);i>=Number(start);i--){ 
				yyyyList.push(i);
			}
     		$("#jqxCombobox02").jqxComboBox({ 
				source: yyyyList, 
				animationType: 'fade', 
				dropDownHorizontalAlignment: 'right', 
				dropDownWidth: 80, 
				dropDownHeight: 80, 
				width: 80, 
				height: 22,  
				theme:'blueish'});
     		if(paramYyyy=='')
     			$("#jqxCombobox02").jqxComboBox('selectIndex', 0 );  
     		else{
     			$("#jqxCombobox02").jqxComboBox('selectItem', paramYyyy );  
     			paramYyyy="";
     		}
     	
        }
		function makeStraListTree(){
			//tree초기화
			var div = $("#jqxTree01").parent();
			 $("#jqxTree01").jqxTree('destroy');
			 div.append("<div id='jqxTree01' class='tree'/>");
			 
    		var dat = $i.sqlhouse(407, {MS_ID:$("#ms_id").val()});
    		dat.done(function(res){
    			
    			res = res.returnArray;
    			res.forEach(function(dataOne) {
    				dataOne.STRA_NM = $i.secure.scriptToText(dataOne.STRA_NM);
    			});
    			
    			var source =
				{
	                datatype: "json",
	                datafields: [
	                    { name: 'P_ID'},
	                    { name: 'C_ID'},
	                    { name: 'STRA_NM'},
	                    { name: 'STRA_ID'},
	                    { name: 'SUBSTRA_ID'}
	                ],
	                id : 'C_ID',
	                localdata: res
	            };
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            dataAdapter.dataBind();
	            
	            var records = dataAdapter.getRecordsHierarchy('C_ID', 'P_ID', 'items', [{ name: 'C_ID', map: 'id'} ,{ name: 'STRA_NM', map: 'label'}, { name:'STRA_ID', map:'value'}]);
	            $('#jqxTree01').jqxTree({source: records, height: '100%', width: '100%', theme:'blueish', allowDrag: false, allowDrop: false });
	            
				// tree init
	            var items = $('#jqxTree01').jqxTree('getItems');
	            var item = null;
	            var size = 0;
	            var img = '';
	            var label = '';
	            var afterLabel = '';
	            
				for(var i = 0; i < items.length; i++) {
					item = items[i];
					if(i == 0) {
						//root
						if(paramStraid==''){
							$("#jqxTree01").jqxTree('expandItem', item);
							img = 'icon-foldernoopen.png';
							afterLabel = "";
						}else{
							img = 'icon-foldernoopen.png';
							afterLabel = "";
						}
						
					} else {
						//children
						size = $(item.element).find('li').size();
						
						if(size > 0) {
							//have a child
							var itemId = item.id;
							if(itemId.indexOf("ASSIGN")==-1){
								img = 'icon-foldernoopen.png';
								if(itemId.indexOf("SUB")==-1){
									afterLabel = "";
								}else{
									afterLabel = "<span style='color: Blue;'> (" + size + ")</span>";
								}
							}
						} else {
							var itemId = item.id;
							if(itemId.indexOf("ASSIGN")==-1){
								img = 'icon-foldernoopen.png';
							}else{
								img = 'icon-page.png';
							}
							//no have a child
							//내부에서 사용하는 트리는 팝업 없음.
							//afterLabel = "<span style='color: Blue; margin-left:5px; vertical-align:middle;'> <img src='${WWW.CSS}/images/img_icons/popupIcon.png' alt='팝업창' title='새창보기'/></span>";
							afterLabel = "";
						}
					}
					
					label = "<img style='float: left; margin-right: 5px;' src='../../resources/cmresource/image/" + img + "'/><span item-title='truec style='vertical-align:middle;'>" + $i.secure.scriptToText(item.label) + "</span>" + afterLabel;
					$('#jqxTree01').jqxTree('updateItem', item, { label: label});
				}
	            
	            //add event
	            $('#jqxTree01')
	            .on("expand", function(eve) {
	            	var args = eve.args;
	            	var label = $('#jqxTree01').jqxTree('getItem', args.element).label.replace('icon-foldernoopen', 'icon-folderopen');
	            	args.element.firstChild.className = args.element.firstChild.className.replace('jqx-tree-item-arrow-collapse', '').replace('jqx-icon-arrow-right', '');
	            	$('#jqxTree01').jqxTree('updateItem', args.element, { label: label});
	            })
	            .on("collapse", function(eve) {
	            	var args = eve.args;
	            	var label = $('#jqxTree01').jqxTree('getItem', args.element).label.replace('icon-folderopen', 'icon-foldernoopen');
	            	$('#jqxTree01').jqxTree('updateItem', args.element, { label: label});
	            })       
	            .on("select", function(eve) {
	            	var args = eve.args;
	            	console.log(args);
	            	var pid = $("#jqxTree01").jqxTree("getItem", args.element).parentId;
	            	var cid = $("#jqxTree01").jqxTree("getItem", args.element).id;
	            	var value = $("#jqxTree01").jqxTree("getItem", args.element).value;
	            
	            	$("#txtTabSubstr").val(cid);
	            	if(pid==0){//전략
	            		console.log('전략선택');
	            	}else{
	            		if(isNaN(pid)){//추진
	            			console.log('추진과제선택');
	            			var idx = pid.indexOf('SUB');
	            			var id1=pid.substr(0,idx);
	            			var id2=pid.substr(parseInt(idx)+3);
	            			getTitle(id1,id2);
	            			makeAssignGrid(2,value);
	            			/*makeCommitteeEval(2,value);
	            			makeAssignCombo(2,value);
	            			makeLastGrid(2,value);*/
	            			
	            		}else{//세부
	            			console.log('세부전략선택');
	            			var idx = cid.indexOf('SUB')+3;
	            			getTitle(pid,cid.substr(idx));
	            			makeAssignGrid(1,pid,value);
	            			makegrid2(1,pid,value); 
	            			makegrid4(1,pid,value);
	            			makegrid31(1,pid,value);
	            			makegrid32("-1");
	            			$("#grid3-1").jqxGrid('clearselection');
	            			reset3();
	            			/*makegrid32("");
	            			$("#grid3-1").jqxGrid('clearselection');
	            			makegrid4($("#jqxCombobox01").val(), $("#jqxCombobox02").val());
	            	
	            			/*makeCommitteeEval(1,pid,value);
	            			makeAssignCombo(1,pid,value);
	            			makeLastGrid(1,pid,value);*/

	            		}
	            	}
	            });
	            
	            if(paramStraid!=''){
		            $('#jqxTree01').jqxTree('expandItem',$("#jqxTree01").find('#'+paramStraid)[0]);
		      
		            $('#jqxTree01').jqxTree('selectItem',$("#jqxTree01").find('#'+paramStraid+'SUB'+paramSubstraid)[0]);
	            }
	            
	            
    		});
    	}//makeStrategyTree
    	 function numberWithCommas(x) {
	            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    }

    	function getTitle(straid,substraid){
    		var dat = $i.sqlhouse(408, {MS_ID:$("#ms_id").val(),STRA_ID:straid,SUBSTRA_ID:substraid});
    		dat.done(function(res){
    			console.log(res);
    			res = res.returnArray[0];
    			
    			var html = $i.secure.scriptToText(res.S_STRA_NM) + '<span class="label sublabel type2">'+$i.secure.scriptToText(res.SUBSTRA_CD)+' '+$i.secure.scriptToText(res.S_SUBSTRA_NM)+'</span>'
    			$("#selectStraNm").html(html);
    		});
    	}
    	
		function makeAssignSearch(){
			
			var ms_id= $("#ms_id").val();
			var yyyy = $("#yyyy").val();
			var keyword = $("#txtSearchWord").val();
           
			var div = $("#searchGrid").parent();
			 $("#searchGrid").jqxGrid('destroy');
			 div.append("<div id='searchGrid'/>");
			 
			var dat = $i.sqlhouse(402,{MS_ID:ms_id,YYYY:yyyy,KEYWORD:keyword});
			dat.done(function(res){
   				var data = res.returnArray;
               	var source =
               	{
	                   datatype: "json",
	                   datafields: [
	                       { name: 'MS_ID', type: 'string' },
	                       { name: 'ASSIGN_ID', type: 'string' },
	                       { name: 'ASSIGN_CD', type: 'string' },
	   					   { name: 'ASSIGN_NM', type: 'string'},
		   					{ name: 'STRA_ID', type: 'string' },
		   					{ name: 'SUBSTRA_ID', type: 'string' }
	                   ],
	                   localdata: data,
	                   updaterow: function (rowid, rowdata, commit) {
	                       commit(true);
	                   }
               	};
             	var noScript = function (row, columnfield, value) {//
               		var newValue = $i.secure.scriptToText(value);
                       return '<div id="userName-' + row + '"style="text-align: center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
							
                  } 
   				var detail = function (row, columnfield, value, rowData) {
   					var newValue = $i.secure.scriptToText(value);
					var assign_id = $("#searchGrid").jqxGrid('getrowdata',row).ASSIGN_ID;
	                return '<div id="userName-' + row + '" onclick="searchGridClick('+row+','+assign_id+');" title="'+value+'" style="text-align:left; margin:4px 0px 0px 10px; cursor:pointer; text-decoration:underline;">' + newValue + '</div>';
				}	     
				var dataAdapter = new $.jqx.dataAdapter(source, {
					downloadComplete: function (data, status, xhr) { },
						loadComplete: function (data) { },
						loadError: function (xhr, status, error) { }
				});
				var dataAdapter = new $.jqx.dataAdapter(source);
				$("#searchGrid").jqxGrid(
				{
                 		width: '100%',
                 		height: '100%',
            			altrows:true,
            			pageable: false,
            			source: dataAdapter,
            			theme:'blueish',
            			columnsheight:25 ,
            			rowsheight: 25,
            			sortable:true,
            			columnsresize: true,
                        columns: [
                        	{ text: '실행과제코드', datafield: 'ASSIGN_CD', width: '30%', align:'center', cellsalign: 'center',cellsrenderer : noScript},
                            { text: '실행과제명', datafield: 'ASSIGN_NM', width: '70%', align:'center', cellsalign: 'left' ,  cellsrenderer: detail }
                        ]
                 });
   	
       		});
			
       }
		function searchGridClick(row,assignId){
			var straid = $("#searchGrid").jqxGrid('getrowdata',row).STRA_ID;
			var substraid = $("#searchGrid").jqxGrid('getrowdata',row).SUBSTRA_ID;
			getTitle(straid,substraid);
			makeAssignGrid(2,assignId);
			makegrid2(2,assignId); 
			makegrid4(2,assignId);
			makegrid31(2,assignId);
			makegrid32("-1");
			$("#grid3-1").jqxGrid('clearselection');
			reset3();
			//makeCommitteeEval(2,assignId);
			//makeAssignCombo(2,assignId);
			//makeLastGrid(2,assignId);
		}
       function makeDeptGrid(){

    	   var div = $("#deptGrid").parent();	
			 $("#deptGrid").jqxGrid('destroy');
			 div.append("<div id='deptGrid'/>");
			 
			var dat = $i.sqlhouse(390, {MS_ID:$("#ms_id").val()});
	    	dat.done(function(res){
	    		var data = res.returnArray;
               	var source =
               	{
	                   datatype: "json",
	                   datafields: [
	                       { name: 'DEPT_CD', type: 'string' },
	                       { name: 'DEPT_NM', type: 'string' }
	                   ],
	                   localdata: data,
	                   updaterow: function (rowid, rowdata, commit) {
	                       commit(true);
	                   }
               	};
         
               	var noScript = function (row, columnfield, value) {//
               		var newValue = $i.secure.scriptToText(value);
                       return '<div id="userName-' + row + '"style="text-align: center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
							
                  } 
   				var detail = function (row, columnfield, value, rowData) {
   					var newValue = $i.secure.scriptToText(value);
					var deptcd = $("#deptGrid").jqxGrid('getrowdata',row).DEPT_CD;
	                return '<div id="userName-' + row + '" onclick="deptGridClick('+row+','+deptcd+');" style="text-align:left; margin:4px 0px 0px 10px; cursor:pointer; text-decoration:underline;">' + newValue + '</div>';
				}	     
				var dataAdapter = new $.jqx.dataAdapter(source, {
					downloadComplete: function (data, status, xhr) { },
						loadComplete: function (data) { },
						loadError: function (xhr, status, error) { }
				});

	          var dataAdapter = new $.jqx.dataAdapter(source);
	
	          $("#deptGrid").jqxGrid(
	          {
	            	width: '100%',
					height: '100%',
					altrows:true,
					pageable: false,
					sortable:true,
					source: dataAdapter,
					theme:'blueish',
					columnsheight:25 ,
					rowsheight: 25,
					columnsresize: true,
		            columns: [
		                 { text: '부서코드', datafield: 'DEPT_CD', width: '30%', align:'center', cellsalign: 'center', cellsrenderer:noScript},
		                { text: '부서명', datafield: 'DEPT_NM', width: '70%', align:'center', cellsalign: 'left' ,  cellsrenderer: detail }
		              ]
	          });
	    	});
      }
       function deptGridClick(row,deptcd){
    	   var deptNm = $("#deptGrid").jqxGrid('getrowdata',row).DEPT_NM;
    	   $("#selectStraNm").html("발전목표");
    	   makeAssignGrid(3,deptcd,deptNm);
    	   makegrid2(3,deptcd,deptNm);
    	   makegrid4(3,deptcd,deptNm);
    	   makegrid31(3,deptcd,deptNm);
    	   makegrid32("-1");
			$("#grid3-1").jqxGrid('clearselection');
			reset3();
       }
       	function search(init){
       		if(init!='init'){
       			paramStraid = '';
       			paramSubstraid = '';
       		}
       		$("#selectStraNm").html("발전목표");
       		missionDat.done(function(){
       			$("#ms_id").val($("#jqxCombobox01").val());
           		$("#yyyy").val($("#jqxCombobox02").val());
           		
           		//좌측 TAB영역
           		makeStraListTree();
    			makeDeptGrid();
    			makeAssignSearch();
    			
    			//우측TAB영역
           		makeAssignGrid();
           		makegrid2();
           		makegrid4();
    			makegrid31();
    			makegrid32("-1");
    			$("#grid3-1").jqxGrid('clearselection');
    			reset3();
           		
           		
       		});
       	}
       	
       	var assignData;
        function makeAssignGrid(type,keyword,keyword2){
        	var div = $("#assignGrid").parent();
			 $("#assignGrid").jqxGrid('destroy');
			 div.append("<div id='assignGrid'/>");
        	
        	var ms_id= "";
            var yyyy = "";
           	if(type!=undefined){
           		ms_id= $("#ms_id").val();
                yyyy = $("#yyyy").val();
           	}
          
            if(type==1)//substra
            	assignData = $i.sqlhouse(396,{MS_ID:ms_id,YYYY:yyyy,STRA_ID:keyword,SUBSTRA_ID:keyword2});
            else if(type==2)//assignment
            	assignData = $i.sqlhouse(398,{MS_ID:ms_id,YYYY:yyyy,ASSIGN_ID:keyword});
            else if(type==3)//dept
            	assignData = $i.sqlhouse(397,{MS_ID:ms_id,YYYY:yyyy,DEPT_CD:keyword,DEPT_NM:keyword2});
            else 
            	assignData = $i.sqlhouse(396,{MS_ID:ms_id,YYYY:yyyy,STRA_ID:'',SUBSTRA_ID:''});
            
            assignData.done(function(res){
    			var data = res.returnArray;
                

                // prepare the data
                var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'MS_ID', type: 'string' },
                        { name: 'ASSIGN_ID', type: 'string' },
                        { name: 'ASSIGN_CD', type: 'string' },
    					{ name: 'ASSIGN_NM', type: 'string'},
    					{ name: 'DEPT_NM', type: 'string' }
                    ],
                    localdata: data,
                    updaterow: function (rowid, rowdata, commit) {
                        commit(true);
                    }
                };
    			
    			var noScript = function (row, columnfield, value) {//left정렬
    				var newValue = $i.secure.scriptToText(value);
                     return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
    							
                } 
    				  

    			var goDetail = function (row, columnfield, value, rowData) {
					var assign_id = $("#assignGrid").jqxGrid('getrowdata',row).ASSIGN_ID;
                    
					
                    return '<div id="userName-' + row + '"onclick="detailAssign('+assign_id+');" title="'+value+'" style="text-align:left; margin:4px 0px 0px 10px; cursor:pointer; text-decoration:underline;">' + $i.secure.scriptToText(value) + '</div>';

                }	
    			var alignRight = function (row, columnfield, value) {//left정렬
    				var newValue = numberWithCommas(value);
                    return '<div id="userName-' + row + '"style="text-align:right; margin:4px 4px 0px 0px;">' + newValue + '</div>';
   							
               }  
                var dataAdapter = new $.jqx.dataAdapter(source, {

                    downloadComplete: function (data, status, xhr) { },

                    loadComplete: function (data) { },

                    loadError: function (xhr, status, error) { }

                });

                var dataAdapter = new $.jqx.dataAdapter(source);

                $("#assignGrid").jqxGrid(
                {
                  		width: '100%',
		    			height: '100%',
		    			altrows:true,
		    			pageable: true,
		    			pageSize: 10,
		    			pageSizeOptions: ['100', '200', '300'],
		    			source: dataAdapter,
		    			theme:'blueish',
		    			columnsheight:25 ,
		    			rowsheight: 25,
		    			columnsresize: true,
		    			autorowheight: true,
		    			sortable:true,
	                   columns: [
	                      { text: '실행과제코드', datafield: 'ASSIGN_CD', width: '15%', align:'center', cellsalign: 'center',cellsrenderer:noScript},
	                      { text: '실행과제명', datafield: 'ASSIGN_NM', width: '70%', align:'center', cellsalign: 'left' ,  cellsrenderer: goDetail },
	                      { text: '주관부서', datafield: 'DEPT_NM', width: '15%', align:'center', cellsalign: 'center' ,cellsrenderer:noScript},
	    				]
                });
    			//$("#assignGrid").jqxGrid('selectrow', 0);
    	
        	});
        	
        }
        function detailAssign(assign_id){
        	var yyyy = $("#yyyy").val();
        	var ms_id = $("#ms_id").val();
        	var url = "./detail?yyyy="+yyyy+"&ms_id="+ms_id+"&assign_id="+assign_id+"&s_yyyy="+$("#s_yyyy").val()+"&e_yyyy="+$("#e_yyyy").val();
        	if(assign_id==0)
        		url = "./detail?yyyy="+yyyy+"&ms_id="+ms_id+"&s_yyyy="+$("#s_yyyy").val()+"&e_yyyy="+$("#e_yyyy").val();
    		//var kpiForm = $("<iframe data-role='assignform' src='"+url+"'style='width:100%;height:100%;position:absolute;left:0px;border-style:none;z-index:99999;'></iframe>").appendTo("body"); 
    		location.href=url;
        	
        }
        //<mgl>-------------------------------------------------------------------------
        function makegrid2(type,keyword,keyword2){
        	var ms_id = $("#ms_id").val();
        	var yyyy = $("#yyyy").val();
        	
      
			
			var grid2Data;
			if(type==1)//substra
				grid2Data = $i.sqlhouse(487,{MS_ID:ms_id,YYYY:yyyy,STRA_ID:keyword,SUBSTRA_ID:keyword2});
            else if(type==2)//assignment
            	grid2Data = $i.sqlhouse(488,{MS_ID:ms_id,YYYY:yyyy,ASSIGN_ID:keyword});
            else if(type==3)//dept
            	grid2Data = $i.sqlhouse(489,{MS_ID:ms_id,YYYY:yyyy,DEPT_CD:keyword,DEPT_NM:keyword2});
            else 
            	grid2Data = $i.sqlhouse(487,{MS_ID:ms_id,YYYY:yyyy,STRA_ID:'',SUBSTRA_ID:''});
			
			$("#makeTable").empty();
			grid2Data.done(function(res){
				if(res.returnArray.length>0){
					var data = res.returnArray;
		         	var tableList ="";  
		         	for(var w=0;w<data.length;w++){
		         		
		         		var subkpiid=data[w].SUB_KPI_ID;
		         		var subkpinm=data[w].SUB_KPI_NM;
		         		var confempnm=data[w].CONF_EMP_NM;
		         		var empnm=data[w].EMP_NM;
		         		var msid=data[w].MS_ID;
		         		var kpiid=data[w].KPI_ID;
		         		var empno=data[w].EMP_NO;
		         		var deptcd=data[w].DEPT_CD;
		         		var deptnm=data[w].DEPT_NM;
		         		var confempno=data[w].CONF_EMP_NO;
		         		var confdeptcd=data[w].CONF_DEPT_CD;
		         		var confdeptnm=data[w].CONF_DEPT_NM;
		         		subkpinm = $i.secure.scriptToText(subkpinm);
		         		if(confempnm!="" && confempnm!=null){
		         			confempnm = $i.secure.scriptToText(confempnm);
		         		}
						if(empnm!="" && empnm!=null){
							empnm = $i.secure.scriptToText(empnm);
		         		}
		         		tableList +="<tr>";
		   			tableList +="<td class='t_center' style='width:31%;'>"+subkpinm+"";
		   		//	tableList +="<input type='hidden'   name='msid' value='"+msid+"'id='msid"+w+"'/>";
		   			tableList +="<input type='hidden'   name='kpiid' value='"+kpiid+"'id='kpiid"+w+"'/>";
		   			tableList +="<input type='hidden'   name='subkpiid' value='"+subkpiid+"'id='subkpiid"+w+"'/>";
		   			tableList +="</td>";
		   			tableList +="<td style='width:17%;'>";
		   			tableList +="<input type='hidden'   name='empno' value='"+empno+"'id='empno"+w+"'/>";
		   			tableList +="<input type='hidden'   name='deptcd' value='"+deptcd+"'id='deptcd"+w+"'/>";
		   		//	tableList +="<input type='hidden'   name='deptnm"+w+"' value='"+deptnm+"'id='deptnm"+w+"'/>";
		   			tableList +="<input type='text'   name='deptnm'  value='"+deptnm+"'";
		   			tableList +=" id='deptnm"+w+"' readonly style='width:90%;height:15px;padding:3px; margin:5px; background: #fefee8; border: 1px solid #eee; text-align: center;'/>";
		   			tableList +="</td>";
		   			tableList +="<td style='width:17%;'>";
		   			tableList +="<input type='text'   name='empnm'  value='"+empnm+"'";
		   			tableList +=" id='empnm"+w+"' readonly style='width:75%;height:15px;padding:3px; margin:5px; background: #fefee8; border: 1px solid #eee; text-align: center;'/><div class='icon-search f_right m_t7 pointer' onclick='popupUser(\""+w+"\",\"emp\");'></td>";
		   			tableList +="<td style='width:17%;'>";
		   			tableList +="<input type='hidden'   name='confempno' value='"+confempno+"'id='confempno"+w+"'/>";
		   			tableList +="<input type='hidden'   name='confdeptcd' value='"+confdeptcd+"'id='confdeptcd"+w+"'/>";
		   		//	tableList +="<input type='hidden'   name='confdeptnm"+w+"' value='"+confdeptnm+"'id='confdeptnm"+w+"'/>";
		   			tableList +="<input type='text'   name='confdeptnm'  value='"+confdeptnm+"'";
		   			tableList +=" id='confdeptnm"+w+"' readonly style='width:90%;height:15px;padding:3px; margin:5px; background: #fefee8; border: 1px solid #eee; text-align: center;'/>";
		   			tableList +="</td>";
		   			tableList +="<td style='width:17%;'>";
		   			tableList +="<input type='text' readonly  name='confempnm' value='"+confempnm+"' id='confempnm"+w+"' style='width:75%;height:15px;padding:3px; margin:5px; background: #fefee8; border: 1px solid #eee; text-align: center;' /><div class='icon-search f_right m_t7 pointer' onclick='popupUser(\""+w+"\",\"conf_emp\");'> </td>";
		   			
		   			tableList +="</tr>";
		           }
		         	$("#makeTable").append(tableList); 
					
				}
			});
		/*var dat1 = $i.sqlhouse(480,{MS_ID:msid});
		dat1.done(function(res){
           // prepare the data
			for(var i=0;i<res.returnArray.length;i++){
				var arraList = {};
				res.returnArray[i]['EMP_NM']="";
				res.returnArray[i]['EMP_NO']="";
				res.returnArray[i]['DEPT_CD']="";
				res.returnArray[i]['DEPT_NM']="";
				res.returnArray[i]['CONF_EMP_NM']="";
				res.returnArray[i]['CONF_EMP_NO']="";
				res.returnArray[i]['CONF_DEPT_CD']="";
				res.returnArray[i]['CONF_DEPT_NM']="";
			}
           	if(res1.returnArray.length>0 && res.returnArray.length>0){
           		for(var q=0; q<res.returnArray.length;q++){
           	for(var w=0; w<res1.returnArray.length;w++){
           		if(res.returnArray[q].SUB_KPI_ID==res1.returnArray[w].SUB_KPI_ID && res.returnArray[q].KPI_ID==res1.returnArray[w].KPI_ID){
           			res.returnArray[q].EMP_NM=res1.returnArray[w].EMP_NM;
           			res.returnArray[q].EMP_NO=res1.returnArray[w].EMP_NO;
           			res.returnArray[q].DEPT_CD=res1.returnArray[w].DEPT_CD;
           			res.returnArray[q].DEPT_NM=res1.returnArray[w].DEPT_NM;
           			res.returnArray[q].CONF_EMP_NM=res1.returnArray[w].CONF_EMP_NM;
           			res.returnArray[q].CONF_EMP_NO=res1.returnArray[w].CONF_EMP_NO;
           			res.returnArray[q].CONF_DEPT_CD=res1.returnArray[w].CONF_DEPT_CD;
           			res.returnArray[q].CONF_DEPT_NM=res1.returnArray[w].CONF_DEPT_NM;
           			
           		}
           	}
           }}
           	missionList1=res.returnArray;
         	
       
   			}); });*/
		
        }
        function popupUser(idx,target){  
			window.open('../../base/popup/popupUser?idx='+idx+"&target="+target, 'popupMappingUser','scrollbars=no, resizable=no, width=935, height=380');
		}
        
        function bindPopupData(data) {
            var idx = data.idx;
            var target = data.target;
            var deptCd = data.DEPTCD;
    		var deptNm = data.DEPT_NM;
    		var empNo  = data.EMPNO;
    		var empNm  = data.NAMEHAN;
    		
    		
    		if(target=="emp"){
    			$("#deptcd"+idx).val(deptCd);
	    		$("#deptnm"+idx).val(deptNm);
	    		$("#empno"+idx).val(empNo); 
	    		$("#empnm"+idx).val(empNm); 
    		}else if(target=="conf_emp"){
    			$("#confdeptcd"+idx).val(deptCd);
	    		$("#confdeptnm"+idx).val(deptNm);
	    		$("#confempno"+idx).val(empNo); 
	    		$("#confempnm"+idx).val(empNm); 
    		}
    	}		
        
        
        
        
        
        function makegrid31(type,keyword,keyword2){

			var ms_id = $("#ms_id").val();
        	var yyyy = $("#yyyy").val();
        	
        	var grid3Data;
			if(type==1)//substra
				grid3Data = $i.sqlhouse(503,{MS_ID:ms_id,YYYY:yyyy,STRA_ID:keyword,SUBSTRA_ID:keyword2});
            else if(type==2)//assignment
            	grid3Data = $i.sqlhouse(504,{MS_ID:ms_id,YYYY:yyyy,ASSIGN_ID:keyword});
            else if(type==3)//dept
            	grid3Data = $i.sqlhouse(505,{MS_ID:ms_id,YYYY:yyyy,DEPT_CD:keyword,DEPT_NM:keyword2});
            else 
            	grid3Data = $i.sqlhouse(503,{MS_ID:ms_id,YYYY:yyyy,STRA_ID:'',SUBSTRA_ID:''});
			
			
			grid3Data.done(function(res){
            // prepare the data
          	
            var source =
            {
                datatype: "json",
                datafields: [
                	    { name: 'MS_ID', type: 'string' },
	                    { name: 'KPI_ID', type: 'string' },
	                    { name: 'SUB_KPI_ID', type: 'string' },
						{ name: 'SUB_KPI_NM', type: 'string'},
						{ name: 'KPI_GBN', type: 'string'},
						{ name: 'UNIT', type: 'string'},
						{ name: 'KPI_TYPE', type: 'string'},
						{ name: 'KPI_DIR', type: 'string'},
						{ name: 'USE_YN', type: 'string'}
                ],
                localdata: res,
                updaterow: function (rowid, rowdata, commit) {
                    commit(true);
                }
            };
            var alginCenter = function (row, columnfield, value) {//left정렬
            	//value = $i.secure.scriptToText(value);
            	return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
			}; 
			var alginLeft = function (row, columnfield, value) {//left정렬
				//value = $i.secure.scriptToText(value);
				return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
			}; 
			var alginRight = function (row, columnfield, value) {//right정렬
				//value = $i.secure.scriptToText(value);
				return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
			};
			var detailRow = function (row, columnfield, value) {//그리드 선택시 하단 상세
				value = $i.secure.scriptToText(value);
				var link = "<a href=\"javascript:makegrid32('"+row+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
				return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
			};
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#grid3-1").jqxGrid(
            {
              	width: '100%',
                height:'580px',
				altrows:true,
				pageable: false,
				sortable:true,
                source: dataAdapter,
				theme:'blueish',
                columnsresize: true,
                columns: [
                	{ text: '지표속성', datafield: 'KPI_TYPE', width: '25%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
	                   { text: '지표명', datafield: 'SUB_KPI_NM', width: '50%', align:'center', cellsalign: 'center', cellsrenderer: detailRow},   
					   { text: '단위', datafield: 'UNIT', width: '25%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
				]
            });
		});
		
	
		
	
        }
		function makegrid32(selectRow){
			$("#subkpiid").val("");
			$("#yyyy3").val("");
			$("#goal").val("");
			$("#base").val("");
			$("#grid3-2").jqxGrid('clearselection');
			var kpinm="";
			var subkpiid="";
			var kpiid="";
			var msid="";
			var kpinm="";
			$("#subtitle1").html(kpinm);
			
			if(selectRow!="-1"){
				subkpiid = $("#grid3-1").jqxGrid("getrowdata", selectRow).SUB_KPI_ID;
				kpiid = $("#grid3-1").jqxGrid("getrowdata", selectRow).KPI_ID;
				kpinm = $("#grid3-1").jqxGrid("getrowdata", selectRow).SUB_KPI_NM;
				msid = $("#grid3-1").jqxGrid("getrowdata", selectRow).MS_ID;
				
			
				kpinm = $i.secure.scriptToText(kpinm);
    			$("#subtitle1").html(kpinm);
    			$("#msid").val(msid);
    			$("#kpiid").val(kpiid);
    			$("#subkpiid").val(subkpiid);
    			$("#row3").val(selectRow);
    			
    			
    		}
			msid="AND MS_ID = '"+msid+"'";
			kpiid="AND KPI_ID = '"+kpiid+"'";
			subkpiid="AND SUB_KPI_ID = '"+subkpiid+"'";
				
    			
    		var dat = $i.sqlhouse(474,{MS_ID:msid, KPI_ID:kpiid, SUB_KPI_ID:subkpiid});
    		dat.done(function(res){
	            // prepare the data
	           /*  missionList = res.returnArray;
				missionList.forEach(function(dataOne) {
    				dataOne.SUB_KPI_NM = $i.secure.scriptToText(dataOne.SUB_KPI_NM);
    			
    			}); */
	            var source =
	            {
	                datatype: "json",
	                datafields: [
	                	    { name: 'YYYY', type: 'string' },
	                	    { name: 'GOAL', type: 'string' },
	                	    { name: 'BASE', type: 'string' },
	                	    { name: 'SUB_KPI_ID', type: 'string' }
	                ],
	                localdata: res,
	                updaterow: function (rowid, rowdata, commit) {
	                    commit(true);
	                }
	            };
	            var alginCenter = function (row, columnfield, value) {//left정렬
	            	
					return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
				}; 
				var alginLeft = function (row, columnfield, value) {//left정렬
			
					return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
				}; 
				var alginRight = function (row, columnfield, value) {//right정렬
					
					return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
				};
				var detailRow = function (row, columnfield, value) {//그리드 선택시 하단 상세
					
					var link = "<a href=\"javascript:getRowDetail('"+row+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
					return "<div style='text-align:center; margin:4px 0px 0px 0px;'>" + link + "</div>";
					
				};
	            
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#grid3-2").jqxGrid(
	            {
	              	width: '100%',
	                height:'450px',
					altrows:true,
					sortable:true,
					pageable: false,
	                source: dataAdapter,
					theme:'blueish',
	                columnsresize: true,
	                columns: [
	                	   { text: '년도', datafield: 'YYYY', width: '30%', align:'center', cellsalign: 'center', cellsrenderer: detailRow},
		                   { text: '목표값', datafield: 'GOAL', width: '35%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},   
						   { text: '기준값', datafield: 'BASE', width: '35%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
						  
						 
	                ]
	            });
    		});
        }
		function makegrid4(type,keyword,keyword2){

			var ms_id = $("#ms_id").val();
        	var yyyy = $("#yyyy").val();
        	
        	var grid4Data;
			if(type==1)//substra
				grid4Data = $i.sqlhouse(500,{MS_ID:ms_id,YYYY:yyyy,STRA_ID:keyword,SUBSTRA_ID:keyword2});
            else if(type==2)//assignment
            	grid4Data = $i.sqlhouse(501,{MS_ID:ms_id,YYYY:yyyy,ASSIGN_ID:keyword});
            else if(type==3)//dept
            	grid4Data = $i.sqlhouse(502,{MS_ID:ms_id,YYYY:yyyy,DEPT_CD:keyword,DEPT_NM:keyword2});
            else 
            	grid4Data = $i.sqlhouse(500,{MS_ID:ms_id,YYYY:yyyy,STRA_ID:'',SUBSTRA_ID:''});
			
			
			grid4Data.done(function(res){

	            var source =
	            {
	                datatype: "json",
	                datafields: [
	                	    { name: 'SUB_KPI_NM', type: 'string' },
	                	    { name: 'UNIT', type: 'string' },
	                	    { name: 'KPI_VAL', type: 'number' },
							{ name: 'EMP_NM', type: 'string'},
							{ name: 'CONFIRM_YN', type: 'string'},
							{ name: 'CONF_EMP_NM', type: 'string'},
							{ name: 'CONF_CONFIRM_YN', type: 'string'}
							
	                ],
	                localdata: res,
	                updaterow: function (rowid, rowdata, commit) {
	                    commit(true);
	                }
	            };
	            var alginCenter = function (row, columnfield, value) {//left정렬
	            	value = $i.secure.scriptToText(value);
					return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
				}; 
				 var numberCenter = function (row, columnfield, value) {//left정렬
		            	
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					};
				var alginLeft = function (row, columnfield, value) {//left정렬
					value = $i.secure.scriptToText(value);
					return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
				}; 
				var alginRight = function (row, columnfield, value) {//right정렬
					value = $i.secure.scriptToText(value);
					return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
				};
				var detailRow = function (row, columnfield, value) {//그리드 선택시 하단 상세
					value = $i.secure.scriptToText(value);
					var link = "<a href=\"\" style='color:black; text-decoration:underline;' >" + value + "</a>";
					return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
					
				};
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#grid4").jqxGrid(
	            {
	              	width: '100%',
	                height:'580px',
					altrows:true,
					pageable: false,
	                source: dataAdapter,
					theme:'blueish',
	                columnsresize: true,
	                sortable:true,
	                columns: [
	                	   { text: '지표명', datafield: 'SUB_KPI_NM', width: '25%', align:'center', cellsalign: 'center', cellsrenderer: alginLeft},
		                   { text: '단위', datafield: 'UNIT', width: '12.5%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},   
						   { text: '지표값', datafield: 'KPI_VAL', width: '12.5%', align:'center', cellsalign: 'center', cellsrenderer: numberCenter},
						   { text: '입력 담당자', datafield: 'EMP_NM', width: '12.5%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '확인 여부', datafield: 'CONFIRM_YN', width: '12.5%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},   
						   { text: '확인 담당자', datafield: 'CONF_EMP_NM', width: '12.5%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
						   { text: '확인 여부', datafield: 'CONF_CONFIRM_YN', width: '12.5%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
		            ]
	            });
    		});
        }
        
		function getRowDetail(row){
    		
       		var data = $("#grid3-2").jqxGrid("getrowdata",row);
       		var  subkpiid, yyyy, goal, base;
       		if(data){
       			ac = "";
       			subkpiid=data.SUB_KPI_ID;
       			yyyy=data.YYYY;
       			goal=data.GOAL;
       			base=data.BASE;
       		}
       		else{
       			subkpiid="";
       			yyyy="";
       			goal="";
       			base="";
    		}
       		$("#subkpiid").val(subkpiid);
			$("#yyyy3").val(yyyy);
			$("#oldyyyy").val(yyyy);
			$("#goal").val(goal);
			$("#base").val(base);
			
			$("#yyyy3").attr("readonly",true);
			
		}
        function reset3(){
        	$("#subkpiid").val("");
			$("#yyyy3").val("");
			$("#goal").val("");
			$("#base").val("");
			$("#msid").val("");
			$("#kpiid").val("");
			$("#subkpiid").val("");
			$("#row3").val("");
			$("#subtitle1").html("");
			$("#grid3-2").jqxGrid('clearselection');
			$("#grid3-1").jqxGrid('clearselection');
			makegrid32("-1");
			$("#oldyyyy").val("");
			
			$("#yyyy3").removeAttr("readonly");
			
        	
			
		}
        function save3(){
        	if($("#kpiid").val()==""){
        		$i.dialog.warning("SYSTEM","지표를 선택하세요");
    			return;
        	}
        	if($("#subkpiid").val()==""){
        		$i.dialog.warning("SYSTEM","지표를 선택하세요");
    			return;
        	}
        	if($("#yyyy3").val().trim()==""){
    			$i.dialog.warning("SYSTEM","년도를 입력하십시오.");
    			return;
    		}
        	if(isNaN($("#yyyy3").val())||$("#yyyy3").val().length!=4){
    			$i.dialog.warning("SYSTEM","년도는 yyyy형식으로 입력해주세요");
    			return;
    		}
        	if($("#goal").val().trim()==""){
    			$i.dialog.warning("SYSTEM","목표값을 입력하십시오.");
    			return;
    		}
        	if(isNaN($("#goal").val())){
    			$i.dialog.warning("SYSTEM","목표값을 숫자로 입력해주세요");
    			return;
    		}
        	if($("#base").val().trim()==""){
    			$i.dialog.warning("SYSTEM","기준값을 입력하십시오.");
    			return;
    		}
        	if(isNaN($("#base").val())){
    			$i.dialog.warning("SYSTEM","기준값을 숫자로 입력해주세요");
    			return;
    		}
        	
        	
        	
        	var subkpiid=$("#subkpiid").val();
        	var yyyy=$("#yyyy3").val();
        	var goal=$("#goal").val();
        	var base=$("#base").val();
        	var msid=$("#msid").val();
        	var kpiid=$("#kpiid").val();
        	var kpinm=$("#kpinm").val();
        	var row=$("#row3").val();
        	var userId = "${sessionScope.loginSessionInfo.userId}";
        	
    		if($("#oldyyyy").val() == ""){
     			$i.post("./insertkpiGoal",{AC:'INSERT',MS_ID:msid*1,YYYY:yyyy*1,KPI_ID:kpiid*1,GOAL:goal*1,SUB_KPI_ID:subkpiid*1,BASE:base*1}).done(function(args){
     				if(args.returnCode == "EXCEPTION"){
     		       		$i.dialog.error("SYSTEM","저장 중 오류가 발생했습니다."+args.returnMessage);
     		       	}else{
     		       		$i.dialog.alert("SYSTEM","저장 되었습니다.");
     		       		reset3();
     				}
     			});
     						
     		}else{
	        	$i.post("./insertkpiGoal",{AC:'UPDATE',MS_ID:'AND MS_ID=\''+msid+'\'', KPI_ID:'AND KPI_ID=\''+kpiid+'\'', SUB_KPI_ID:'AND SUB_KPI_ID=\''+subkpiid+'\'', YYYY3:'AND YYYY=\''+$("#oldyyyy").val()+'\'', YYYY:yyyy, GOAL:goal, BASE:base}).done(function(args){
     				if(args.returnCode == "EXCEPTION"){
     		        	$i.dialog.error("SYSTEM","저장 중 오류가 발생했습니다."+args.returnMessage);
     		        }else{
     		        	$i.dialog.alert("SYSTEM","저장 되었습니다.");
     		        	reset3();
     				}
     			});
	        					
	        }
    	}
        function save2(){
        	$("#msid2").val($("#ms_id").val());
        	$("#yyyy2").val($("#yyyy").val());
        	
        	var len = $("#makeTable").find("tr").length;
        	if(len>0){
        		$i.post("./insertKpiUser","#saveForm2").done(function(args){
        			if(args.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM","저장 중 오류가 발생했습니다."+args.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
        					
        				});
        				
        			}
        		}).fail(function(e){ 
        			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
        		});
        	}else{
        		$i.dialog.warning('SYSTEM','저장할 내용이 없습니다.');
        		
        	}
        	
        /*	var syyyy=$("#jqxCombobox02").val();
        	if($("#jqxCombobox01").val()){
				var msid=$("#jqxCombobox01").val();
				var msid3="AND MS_ID='"+msid+"'";
			}
			else
				{var msid="";
        		var msid3="";}
        	var dat = $i.sqlhouse(482,{ MS_ID:msid3, YYYY:syyyy});
			dat.done(function(res1){
		var dat1 = $i.sqlhouse(480,{MS_ID:msid});
		dat1.done(function(res){
           // prepare the data
			for(var i=0;i<res.returnArray.length;i++){
				var smsid=$("#msid"+i).val();			
				var skpiid=$("#kpiid"+i).val();			
				var ssubkpiid=$("#subkpiid"+i).val();			
				var sempno=$("#empno"+i).val();			
				var sempnm=$("#empnm"+i).val();			
				var sdeptcd=$("#deptcd"+i).val();			
				var sdeptnm=$("#deptnm"+i).val();
				var sconfempno=$("#confempno"+i).val();			
				var sconfempnm=$("#confempnm"+i).val();			
				var sconfdeptcd=$("#confdeptcd"+i).val();			
				var sconfdeptnm=$("#confdeptnm"+i).val();
				var susername="${sessionScope.loginSessionInfo.userName}";
				if(sempnm=="")
     			{
     			sempno="";
     			sdeptcd="";
     			sdeptnm="";
     			}
     			if(sconfempnm=="")
 				{
     			sconfempno="";
     			sconfdeptcd="";
     			sconfdeptnm="";
     			}
     			var action="insert";
     			for(var t=0;t<res1.returnArray.length;t++){
     				if(res.returnArray[i]['KPI_ID']==res1.returnArray[t]['KPI_ID'] && res.returnArray[i]['SUB_KPI_ID']==res1.returnArray[t]['SUB_KPI_ID'])
     				{	action="update";
     					
     				}
     			}
					if(action=="insert"){
						//insert
						try {
     						$i.sqlhouse(483,{MS_ID:smsid,KPI_ID:skpiid, SUB_KPI_ID:ssubkpiid, YYYY:syyyy, EMP_NO:sempno, EMP_NM:sempnm, DEPT_CD:sdeptcd, DEPT_NM:sdeptnm, CONF_EMP_NO:sconfempno, CONF_EMP_NM:sconfempnm, CONF_DEPT_CD:sconfdeptcd, CONF_DEPT_NM:sconfdeptnm, INSERT_EMP:susername});
     						} catch (e) {
								$i.dialog.error("SYSTEM","저장지 않았다...");
							}
					}
					else{
						//update
						try {
							smsid="AND MS_ID='"+smsid+"'";
							skpiid="AND KPI_ID='"+skpiid+"'";
							ssubkpiid="AND SUB_KPI_ID='"+ssubkpiid+"'";
							ssyyyy="AND YYYY='"+syyyy+"'";
	        					$i.sqlhouse(484,{MS_ID:smsid,KPI_ID:skpiid, SUB_KPI_ID:ssubkpiid, YYYY:ssyyyy, EMP_NO:sempno, EMP_NM:sempnm, DEPT_CD:sdeptcd, DEPT_NM:sdeptnm, CONF_EMP_NO:sconfempno, CONF_EMP_NM:sconfempnm, CONF_DEPT_CD:sconfdeptcd, CONF_DEPT_NM:sconfdeptnm, UPDATE_EMP:susername});
	        				} catch (e) {
								$i.dialog.error("SYSTEM","업데이트 할 수 없다...");
							}
					}
			}
			$i.dialog.alert("SYSTEM","저장 되었습니다...");
			makegrid4($("#jqxCombobox01").val(), $("#jqxCombobox02").val());	
			});}); */
		}
		function remove3(){
			var subkpiid=$("#subkpiid").val();
        	var msid=$("#msid").val();
        	var kpiid=$("#kpiid").val();
        	var kpinm=$("#kpinm").val();
        	var dyyyy=$("#yyyy3").val();
        	if($("#kpiid").val() == ""){
				$i.dialog.warning("SYSTEM","지표를 선택하십시오.");
    			return;
			}
			if($("#subkpiid").val() == ""){
				$i.dialog.warning("SYSTEM","지표를 선택하십시오.");
    			return;
			}
			if($("#oldyyyy").val() == ""){
				$i.dialog.warning("SYSTEM","삭제할 년도를 선택하십시오.");
    			return;
			}
			
			
			
	 		$i.post("./removekpiGoal",{MS_ID:msid*1,KPI_ID:kpiid*1,SUB_KPI_ID:subkpiid*1, YYYY:dyyyy}).done(function(args){
	 			if(args.returnCode == "EXCEPTION"){
	 	           	$i.dialog.error("SYSTEM",args.returnMessage);
	 	        }else{
	 	           	$i.dialog.alert("SYSTEM",args.returnMessage);
	 	           	makegrid32('-1');
	 		 		reset3();
	 		 		$("#grid3-1").jqxGrid('clearselection');
	 	        }
	 							
	 		});
	 					
 			
		}
		//</mgl>------------------------------------------------------------------------
    </script>
    
		<style>
			#jqxGrid06{
				height: 97px !important;
			}
		</style>
    </head>
    <body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
<div class="wrap" style="width:98%; min-width:1580px; margin:0 1%;">
		<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
		<div class="label type1 f_left">
				비젼:
			</div>
		<div class="combobox f_left"  id='jqxCombobox01' >
			</div>
			<input type="hidden" id="ms_id"/>
		<div class="label type1 f_left">
				년도:
			</div>
		<div class="combobox f_left"  id='jqxCombobox02' >
			</div>
			<input type="hidden" id="yyyy"/>
			<input type="hidden" id="s_yyyy"/>
			<input type="hidden" id="e_yyyy"/>
		<div class="group_button f_right">
				<div class="button type1 f_left">
				<input type="button" value="조회" id='jqxButtonSearch' width="100%" height="100%" />
			</div>
				<div class="button type1 f_left">
				<input type="button" value="신규" id='jqxButtonNew01' width="100%" height="100%" />
			</div>
			</div>
		<!--group_button-->
	</div>
		<!--//header-->
		<div class="container  f_left" style="width:100%; margin:0;">
		<div class="content f_left" style=" width:20%; margin-right:1%;">
				<div class="tabs f_left" style=" width:100%; height:685px; margin-top:0px;">
				<div id='jqxTabs01'>
						<ul>
						<li style="margin-left: 0px;">전략기준</li>
						<li>주관부서</li>
						<li>검색</li>
					</ul>
						<div class="tabs_content" style="height:100%; ">
						<input type="hidden" id="txtTabSubstr" name="tabSubstr" />
						<div  class="tree" style="border:none;"  id='jqxTree01'>
								
							</div>
						<!--//jqxTree-->
					</div>
						<!--//tabs_content-->
						<div  class="tabs_content" style=" height:100%; ">
							<div class="content f_left" style="width:94%; margin:10px 3%;">
								<div class="grid f_left" style="width:100%; margin:5px 0%; height:600px;">
									<div id="deptGrid">
									</div>
								</div>
							</div>
						</div>
						<!--//tabs_content-->
						<div  class="tabs_content" style="height:100%; ">
						<div class="content f_left" style="width:94%; margin:10px 3%;">
								<div class="group  w100p" style="margin:10px auto;">
								<div class="label type2 f_left">
										검색
									</div>
								<input type="text" value="" id="txtSearchWord" class="input type1  f_left  w50p"  style="width:100%; margin:3px 5px; "/>
								<div class="button type2 f_left">
										<input type="button" value="검색" id='jqxButtonFind01' width="100%" height="100%" onclick="makeAssignSearch();"/>
									</div>
							</div>
								<!--group-->
								<div class="grid f_left" style="width:100%; margin:5px 0%; height:560px;">
									<div id="searchGrid">
									</div>
							</div>
								<!--grid -->
							</div>
						<!--content-->
					</div>
						<!--//tabs_content-->
					</div>
				<!--//jqxTabs-->
			</div>
				<!--//tabs-->
			</div>
		<!--//content-->
		
		<div class="content f_right" style=" width:79%;">
				<div class="group f_left  w100p m_b5">
				<div class="label type2 f_left" id="selectStraNm">
						발전목표
					</div>
			</div>
				<!--group-->
				<div class="tabs f_left" style=" width:100%; height:655px; margin-top:0px;">
				<div id='jqxTabs02'>
					<ul>
						<li style="margin-left: 0px;">실행과제</li>
						<li>담당자</li>
						<li>지표목표</li>
						<li>지표실적</li>
					</ul>
					<!-- 추진과제S -->
					<div class="tabs_content" style="height:100%; ">
						<div class="content f_left" style="width:97%; margin:10px 1.5%;">
								<div class="grid f_left" style="width:100%; margin:5px 0%; height:580px;">
									<div id="assignGrid">
									</div>
							</div>
								<!--grid -->
							</div>
						<!--content-->
					</div>
					<!-- 담당자탭 -->
					<div class="tabs_content" style="height:100%; ">
							<div class="content f_left" style="width:97%; margin:10px 1.5%;">
								 <form id="saveForm2" name="saveForm2">
								 <input type="hidden" name="ms_id" id="msid2"/>
								 <input type="hidden" name="yyyy" id="yyyy2"/>
								 <div class="blueish datatable f_left" style="width:100%; height:550px; margin:5px 0; overflow:hidden;">     
									<!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 헤더가 50px일경우, 300px - 50px= 250px)-->
									<div class="datatable_fixed" style="width:100%; height:550px; float:left; padding:25px 0;">
										<!--group으로 헤더높이가 길어질 경우 padding  (기본높이는 25px)줘야 함-->
										<div style=" height:525px; !important; overflow-x:hidden;">
											<table  width="100%" cellspacing="0" cellpadding="0" border="0"  > 
													<thead style="width:100%;"> 
														<tr> 
															<th style="width:31%">세부성과지표명</th>  
															<th style="width:17%">실적입력자부서</th> 
															<th style="width:17%">실적입력자</th> 
															<th style="width:17%">실적확인자부서</th>   
															<th style="width:17%">실적확인자</th> 
															<th style="width:17px; min-width:17px; border-bottom:0;"></th> 
														</tr>  
													</thead>      
													<tbody id="makeTable"> 
													
													</tbody>     
											</table> 
										</div>
									</div>
								</div>
								</form>
								<div class="group f_right" >
									<div class="button type1 f_left">
										<div class="button type2 f_left">
											<input type="button" value="저장" id='jqxButtonNew02' width="100%" height="100%" />
										</div>
									</div>
								</div>
							</div>
						<!--table-->
					</div>
					<!--  지표목표 탭 -->
					<div class="tabs_content" style="height:100%; ">
						<div class="content f_left" style="width:47%; margin:10px 1.5%;">
							<div id="grid3-1">
									</div>	
									<!--grid-3-1-->	
						</div>
						<div class="content f_right" style="width:47%; margin:10px 1.5%;">
						<div class="label type2 f_left" style="text-align:left;">
											        	지표명:<span class="label sublabel" id="subtitle1">&nbsp;</span>
												</div>
							<div id="grid3-2">
									</div>	
									<!--grid-3-2-->	
															<div class="blueish table f_right"  style="width:100%; height:; margin:10px 0;">
							<form id="saveForm2" name="saveForm2" method="post">
								<!-- <input type="hidden" name="ac" id="ac" value="sub"/> -->
								<input type="hidden" name="msid" id="msid" value=""/>
								<input type="hidden" name="kpiid" id="kpiid" value=""/>
								<input type="hidden" name="kpinm" id="kpinm" value=""/>
								<input type="hidden" name="subkpiid" id="subkpiid" value=""/>
								<input type="hidden" name="oldyyyy" id="oldyyyy" value=""/>
								<input type="hidden" name="row3" id="row3" value=""/>
								<table summary="가산점" style="width:100%;">
								<thead style="width:100%;">
									<tr>
										<th  scope="col" style="width:20%;">년도<span class="th_must"></span></th>
										<th  scope="col" style="width:40%;">목표값<span class="th_must"></span></th>
										<th scope="col" style="width:40%;">기준값<span class="th_must"></span></th>
										
										
									</tr>
									</thead>
								<tbody>
										<tr>
										<td ><div class="cell t_center">
												<input type="text" value="" id="yyyy3" name="yyyy" class="input type2  f_left"  style="width:96%; margin:3px 0; text-align: center; "/>
											</div>
										</td>
										<td ><div class="cell t_center">
												<input type="text" value="" id="goal" name="goal" class="input type2  f_left"  style="width:96%; margin:3px 0; text-align: right; "/>
											</div>
										</td>
										<td ><div class="cell t_center">
												<input type="text" value="" id="base" name="base" class="input type2  f_left"  style="width:96%; margin:3px 0; text-align: right; "/>
											</div>
										</td>
									</tr>
									</tbody>
							</table>
							</form>
						</div>
		
											<div class="group_button f_right">
												
												
												<div class="button type2 f_left">
													<input type="button" value="신규" id='jqxButtonReset2'
														width="100%" height="100%" onclick="reset3()" />
												</div>
												<div class="button type2 f_left">
													<input type="button" value="저장" id='jqxButtonSave2'
														width="100%" height="100%" onclick="save3()" />
												</div>
												<div class="button type3 f_left "  id='hide-del'>
													<input type="button" value="삭제" id='jqxButtonDelete2'
														width="100%" height="100%" onclick="remove3()" />
												</div>
											</div>
						</div>
						<!--content-->
					</div>
					<!-- 지표실적탭 -->
					<div class="tabs_content" style="height:100%; ">
						<div class="content f_left" style="width:97%; margin:10px 1.5%;">
							<div id="grid4" style="height:580px;margin:5px 0%;">
							</div>
									<!--grid-4--->	
						</div>
						<!--content-->
					</div>
						<!--//tabs_content-->						
					</div><!--//jqxtabs-->
				</div><!--tabs-->								 
			</div><!--content-->
		</div><!--//container-->
	</div>
<!--//wrap-->
</body>
</html>