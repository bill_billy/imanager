<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>부서조회</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		search();
        	}
        	//조회
        	function search(){
        		var data = $i.sqlhouse(409,{});
        		data.done(function(res){
		            // prepare the data
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'DEPT_CD', type: 'string'},
		                    { name: 'DEPT_NM', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 5px;">' + value + '</div>';
					};
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 5px 0px 0px;">' + value + '</div>';
					};
					var detail = function (row, columnfield, value) {//right정렬
						var deptcd = $("#deptGrid").jqxGrid("getrowdata", row).DEPT_CD;
						var deptnm = $("#deptGrid").jqxGrid("getrowdata", row).DEPT_NM;
						
						var link = "<a href=\"javascript:parentSend('"+deptcd+"','"+deptnm+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#deptGrid").jqxGrid(
		            {
	              	 	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
						sortable: true,
		                columnsresize: true,
						columnsheight: 25,
						rowsheight: 25,
		                columns: [
		                   { text: '부서코드', datafield: 'DEPT_CD', width: '40%', align:'center', cellsalign: 'center'},
		                   { text: '부서명', datafield: 'DEPT_NM', width:'60%', align:'center', cellsalign: 'left',  cellsrenderer: detail}
		                ]
		            });
        		});
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function parentSend(deptcd,deptnm){
        		
        		if("${param.type}"=="main"){
        			$("#txtDeptcd", opener.document).val(deptcd);
            		$("#txtDeptnm", opener.document).val(deptnm);
        		}else if("${param.type}"=="sub"){
        			$("#txtSubDeptcd", opener.document).val(deptcd);
            		$("#txtSubDeptnm", opener.document).val(deptnm);
        		}
				self.close();
        	}
        </script>
    </head>
    <body class='blueish'>
	<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:900px;min-height:300px; margin:0 1%;"><!--팝업창 사이즈 조정-->
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="grid f_left" style="width:100%; height:276px;">
					<div id="deptGrid"></div>
				</div>
			</div>
		</div>
	</body>
</html>

