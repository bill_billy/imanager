<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE HTML>
<html>
    <head>
    <meta charset="utf-8">
    <title id='Description'>평가자관리</title>
   	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
	    <script src="../../resources/cmresource/js/iplanbiz/widget/Form.js"></script>
	    <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	    <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	    <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
   		
         
        
        <script type="text/javascript">
        $(this).ready(function () {
        	makeMissionCombo();
        	
        	missionDat.done(function(res){
        		makeSubStra();   
    			deptChk();
        	});
        	 
    		$("#jqxButtonSearch").jqxButton({ width: '',  theme:'blueish'}).on('click',makeSubStra); 
 
        });
        
        </script> 
        <script type="text/javascript">  
        	
        var missionList = [];
    	var missionDat;
		function makeMissionCombo(){
			missionDat = $i.sqlhouse(322,{}); 
			missionDat.done(function(res){
				missionList = res.returnArray;
				
				missionList.forEach(function(dataOne) {
    				dataOne.MS_NM = $i.secure.scriptToText(dataOne.MS_NM);
    			});
				
        		var source = 
        		{
        			datatype:"json",
        			datafields:[
        				{ name : "MS_ID" },
        				{ name : "MS_NM" }
        			],
        			id:"id",
        			localdata:res,
        			async : false
        		};
        		var dataAdapter = new $.jqx.dataAdapter(source);
        		 $("#getMS").jqxComboBox({ 
        			//	selectedIndex: 0, 
        				source: dataAdapter, 
        				animationType: 'fade', 
        				dropDownHorizontalAlignment: 'right', 
        				displayMember: "MS_NM", 
        				valueMember: "MS_ID", 
        				dropDownWidth: 300, 
        				dropDownHeight: 100, 
        				width: 300, 
        				height: 22,  
        				theme:'blueish'});
        		 
        		$('#getMS').on('select', function (event) 
        		{
        			var args = event.args;
        			if (args) {
        				var index = args.index;
        				makeYearCombo(index);
        				
        			}	     
        		}); 
        		$("#getMS").jqxComboBox('selectIndex', 0 ); 
        		
        	});  
     	}//makeMissionCombo
     	function makeYearCombo(idx){
     		var start = missionList[idx].START_YYYY;
     		var end = missionList[idx].END_YYYY;
     		
     		var nowYear = new Date().getFullYear();
     		if(nowYear >= start && nowYear < end) //현재기준으로 설정 
     			end = nowYear;
     		
     		var yyyyList = [];
     		for(var i=Number(start);i<=end;i++){ 
     			yyyyList.push(i);
     		}
     		$("#getMSyy").jqxComboBox({ 
				source: yyyyList, 
				animationType: 'fade', 
				dropDownHorizontalAlignment: 'right', 
				dropDownWidth: 80, 
				dropDownHeight: 80, 
				width: 80, 
				height: 22,  
				theme:'blueish'});
     		$("#getMSyy").jqxComboBox('selectIndex', yyyyList.length-1 );  
        }//makeYearCombo
        
        function deptChk(){        
        	$ ( ".deptChk" ).jqxRadioButton({ width : 25, height : 25});
        }
         
        </script>
        
        <script type="text/javascript"> //table
        function makeSubStra(){ 
        	var msId = $("#getMS").val();   
        	console.log(msId);
   			var YYYY = $("#getMSyy").val();   
			var data = $i.sqlhouse(328,{MS_ID:$('#getMS').val(), YYYY:$('#getMSyy').val()});
			$("#makeTable").empty();
			data.done(function(res){  
				console.log(res);   
				res = res.returnArray;
			for(var i=0;i<res.length;i++){ 
				var tableList ="";  
				if(i==0 || res[i].ASSIGN_ID != res[i-1].ASSIGN_ID){
					tableList +="<tr>";
					tableList += "<td align='center' name='straNm"+res[i].ASSIGN_ID+"' class='td_side_left common' style='width:15%;'>"+$i.secure.scriptToText(res[i].STRA_NM)+"</td>";   
					tableList += "<td align='center' name='substraCd"+res[i].ASSIGN_ID+"'class='borderNone tr_hover odd common' style='width:7%;'>"+$i.secure.scriptToText(res[i].SUBSTRA_CD)+"</td>";
					tableList += "<td align='center' name='substraNm"+res[i].ASSIGN_ID+"' class='borderNone tr_hover odd common' style='width:15%;'>"+$i.secure.scriptToText(res[i].SUBSTRA_NM)+"</td>";
					tableList += "<td align='center' name='assignCd"+res[i].ASSIGN_ID+"' class='borderNone tr_hover odd common' style='width:7%;'>"+$i.secure.scriptToText(res[i].ASSIGN_CD)+"</td>";   
					tableList += "<td align='left' name='assignNm"+res[i].ASSIGN_ID+"' class='borderNone tr_hover odd common' style='width:35%; text-decoration:underline;padding:0 7px;'><a href=\"javaScript:detailRow('"+msId+"','"+res[i].ASSIGN_ID+"','"+YYYY+"');\" >"+$i.secure.scriptToText(res[i].ASSIGN_NM)+"</a></td>";
 
					if(i%2==0){  
						tableList += "<td align='center' name='deptNm"+res[i].ASSIGN_ID+"' class='borderNone tr_hover odd' style='width:10%;'>"+res[i].DEPT_NM+"</td>";
						tableList += "<td align='center' name='empNm"+res[i].ASSIGN_ID+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].EMP_NM+"</td>"; 
						tableList += "<td align='center' name='masterYn"+res[i].ASSIGN_ID+"' class='borderNone tr_hover odd' style='width:60%;'>"+res[i].MASTER_YN+"</td>";
   
					}else{ 
						tableList += "<td align='center' name='deptNm"+res[i].ASSIGN_ID+"' class='borderNone tr_hover odd' style='width:10%;'>"+res[i].DEPT_NM+"</td>";
						tableList += "<td align='center' name='empNm"+res[i].ASSIGN_ID+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].EMP_NM+"</td>";
						tableList += "<td align='center' name='masterYn"+res[i].ASSIGN_ID+"' class='borderNone tr_hover odd' style='width:60%;'>"+res[i].MASTER_YN+"</td>";					
						
					}
					tableList += "</tr>"; 
				}else if(res[i].ASSIGN_ID == res[i-1].ASSIGN_ID){ 
					tableList +="<tr>"; 
					if(i%2==0){
						tableList += "<td align='center' name='deptNm"+res[i].ASSIGN_ID+"' class='borderNone tr_hover odd' style='width:10%;'>"+res[i].DEPT_NM+"</td>";
						tableList += "<td align='center' name='empNm"+res[i].ASSIGN_ID+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].EMP_NM+"</td>";
						tableList += "<td align='center' name='masterYn"+res[i].ASSIGN_ID+"' class='borderNone tr_hover odd' style='width:60%;'>"+res[i].MASTER_YN+"</td>";					
					}else{
						tableList += "<td align='center' name='deptNm"+res[i].ASSIGN_ID+"' class='borderNone tr_hover odd' style='width:10%;'>"+res[i].DEPT_NM+"</td>";
						tableList += "<td align='center' name='empNm"+res[i].ASSIGN_ID+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].EMP_NM+"</td>";
						tableList += "<td align='center' name='masterYn"+res[i].ASSIGN_ID+"' class='borderNone tr_hover odd' style='width:60%;'>"+res[i].MASTER_YN+"</td>";					
						}
					tableList += "</tr>";	   
				}
			$("#makeTable").append(tableList); 
			} 
			
			var rowspanCount = "";
			for(var j=0;j<res.length;j++){  
				rowspanCount = $("[name=empNm"+res[j].ASSIGN_ID+"]").length;
				console.log(rowspanCount);
				//로우 병합
				$("#makeTable").find($("td[name=assignNm"+res[j].ASSIGN_ID+"]")).attr("rowSpan", rowspanCount);
				$("#makeTable").find($("td[name=assignCd"+res[j].ASSIGN_ID+"]")).attr("rowSpan", rowspanCount);
				$("#makeTable").find($("td[name=straNm"+res[j].ASSIGN_ID+"]")).attr("rowSpan", rowspanCount); 
				$("#makeTable").find($("td[name=substraCd"+res[j].ASSIGN_ID+"]")).attr("rowSpan", rowspanCount);
				$("#makeTable").find($("td[name=substraNm"+res[j].ASSIGN_ID+"]")).attr("rowSpan", rowspanCount);  

			} 
			$("#inputEmp").empty(); 
			resetF();
			addEmp();
			deptChk();
			setMargin(); 
			});
        }//makeSubStra
         
        function setMargin() {
			//헤더에 마진 추가
			
			var inputEmpLength = $('#makeTable').find('tr').length;
			
			if(inputEmpLength>9) { 
				$('.headtable').eq(0).css('margin-right','17px');
			} else {
				$('.headtable').eq(0).css('margin-right','-1px');
			}
		}
       
        function detailRow(msId, assignId, YYYY){

        	var straNm = $("td[name=straNm"+assignId+"]").eq(0).html();
        	var substraCd = $("td[name=substraCd"+assignId+"]").eq(0).html();
        	var substraNm = $("td[name=substraNm"+assignId+"]").eq(0).html();
        	var assignCd = $("td[name=assignCd"+assignId+"]").eq(0).html();
        	var assignNm = $("td[name=assignNm"+assignId+"]").eq(0).html(); 
        	var masterYn = $("td[name=masterYn"+assignId+"]").eq(0).html(); 
        	
        	$("#stranmLB").html(straNm+'<span class="label sublabel type2">'+substraCd+' '+substraNm+'</span><span class="label sublabel type2">'+assignCd+' '+assignNm+'</span>');

   			var msId = $("#getMS").val();  
   			var YYYY = $("#getMSyy").val();  
    	
			$("#msidLB").val(msId);   
			$("#yyyyLB").val(YYYY);    
			$("#assignidLB").val(assignId);  
			
            var data = $i.sqlhouse(330,{MS_ID:msId, YYYY:YYYY, ASSIGN_ID:assignId});
				$("#inputEmp").empty();
				data.done(function(res){
					res = res.returnArray;
					console.log(res);
					if(res.length > 0){
						for(var i=0;i<res.length;i++){
		 					var inputEmpLength = res.length;
		 					console.log(inputEmpLength);
		 					var inputNo = inputEmpLength + 1; 
							var inputName = "input" + i;   
							console.log(i);
							var html = ""; 
							html += '<tr class="tr_hover" id="'+inputName+'"  name="input">'; 
							html += '<td style="width:25%;"><div class="cell">'; 
							html += '<input type="hidden" id="txtDeptcd_e'+i+'" name="deptCd" value="'+res[i].DEPT_CD+'"/>';
							html += '<input type="text" id="txtDeptnm_e'+i+'" name="deptNm" readonly="readonly" class="input type2  f_left" style="width:98%; margin:3px 0;" value="'+res[i].DEPT_NM+'"/>';
							html += '</div></td>'; 
							html += '<td style="width:25%;"><div class="cell">';
							html += '<input type="text" id="txtEmpid_e'+i+'" name="empId" readonly="readonly" class="input type2  f_left"  style="width:98%; margin:3px 0;" value="'+res[i].EMP_ID+'"/>';
						    html += '</div></td>';       
							html += '<td style="width:25%;"><div  class="cell">'; 
							html += '<input type="text"  id="txtEmpnm_e'+i+'" name="empNm" readonly="readonly" class="input type2  f_left"  style="width:90%; margin:3px 0;" value="'+res[i].EMP_NM +'" />';
							html += '<div class="icon-search f_left m_t7 pointer" onclick="popupUser(\''+i+'\');">';       
							html += '</div>';  
							html += '</div></td>';   
							html += '<td style="width:15%;"><div class="cell" style="text-align:center;">'; 
							html += '<center><div class="deptChk" id="deptCheckbox'+i+'"></div></center><input type="hidden" name="masterYn" id="deptchk'+i+'">';            
							html += '</div></td>';       
							html += '<td style="width:15%;"><div class="cell">';  
							html += '<div class="pointer t_center">';  
							html += '<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/icon-minus.png" alt="삭제" height="19"  onclick="deleteEmp(\''+inputName+'\');">'; 
							html += '</div>';
							html += '</div></td>';
							html += '</tr>'; 
							
						$("#inputEmp").append(html);
						
						$ ( ".deptChk" ).jqxRadioButton({ width : 25, height : 25});  
				        	if(res[i].MASTER_YN == "Y"){ 
				        		$('.deptChk').jqxRadioButton('check');
				        	}
						}
						//setMargin();//헤더에 마진 추가  
					}else{
						console.log("add로그") 
						addEmp();  
						deptChk();
					} 
	  
				}) 
				
        }
        
        function popupUser(idx){  
			window.open('./popupMappingUser?idx='+idx, 'popupMappingUser','scrollbars=no, resizable=no, width=930, height=310');
		}
        
        function addEmp(){
			var inputEmpLength = $("input[name=empId]").length;
			var inputNo = inputEmpLength;
			var inputName = "input" + inputNo;
			var html = "";
			html += '<tr id="'+inputName+'" name="input">'; 
			html += '<td style="width:25%;"><div class="cell">'; 
			html += '<input type="hidden" id="txtDeptcd_e'+inputNo+'" name="deptCd" />';
			html += '<input type="text" id="txtDeptnm_e'+inputNo+'" name="deptNm" readonly="readonly" class="input type2  f_left" style="width:98%; margin:3px 0;" />';
			html += '</div></td>'; 
			html += '<td style="width:25%;"><div class="cell">';
			html += '<input type="text" id="txtEmpid_e'+inputNo+'" name="empId" readonly="readonly" class="input type2  f_left"  style="width:98%; margin:3px 0;"/>';
		    html += '</div></td>';      
			html += '<td style="width:25%;"><div  class="cell">'; 
			//html += '<input type="hidden" id="txtEmpid_e'+inputNo+'" name="empId"/>';
			html += '<input type="text"  id="txtEmpnm_e'+inputNo+'" name="empNm" readonly="readonly" class="input type2  f_left"  style="width:90%; margin:3px 0;"  />';
			html += '<div class="icon-search f_left m_t7 pointer" onclick="popupUser(\''+inputNo+'\');">';     
			html += '</div>'; 
			html += '</div></td>';  
			html += '<td style="width:15%;"><div class="cell" style="text-align:center;">';
			html += '<center><div class="deptChk" id="deptCheckbox'+inputNo+'" style="text-align:center;"></div></center><input type="hidden" name="masterYn" id="deptchk'+inputNo+'">';            
			html += '</div></td>';     
			html += '<td style="width:15%;"><div class="cell">';   
			html += '<div class="pointer t_center">';
			html += '<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/icon-minus.png" alt="삭제" height="19"  onclick="deleteEmp(\''+inputName+'\');">'; 
			html += '</div>';
			html += '</div></td>';
			html += '</tr>'; 
			
			$("#inputEmp").append(html);
			//setMargin();//헤더에 마진 추가
			$ ( ".deptChk" ).jqxRadioButton({ width : 25, height : 25});  
			   $ ( "#deptCheckbox"+inputNo ).on('change' , function (event) {
		            var checked = event.args.checked;
		            console.log(event);
		            var realVal = "N";
		            if(checked==true)	realVal = "Y";
		            else if(checked==false) realVal = "N";
					$("#deptchk"+inputNo).val(realVal);        
		        }); 
		}//addEmp 
			
       	function insertDept(){
      	    if($("#msidLB").val().trim()==""){
   			$i.dialog.warning("SYSTEM","추진과제를 선택하세요.");
   			return;
   		}
   	    if($("#assignidLB").val().trim()==""){ 
   			$i.dialog.warning("SYSTEM","추진과제를 선택하세요."); 
   			return;
   		} 
    
   	   if($("#yyyyLB").val().trim()==""){ 
   			$i.dialog.warning("SYSTEM","추진과제를 선택하세요."); 
   			return;
   		} 
   	   	   
			
   			var chkLen = $("input[name=masterYn]").length;
   			
   			var chkMaster = [];
   			for(var i=0;i<chkLen;i++){
   				if($("input[name=empId]").eq(i).val()==""){
   					continue;
   				}
   				
   				var checked = $(".deptChk").eq(i).jqxRadioButton("checked");
   				
   				var realVal = "N";
   				if(checked==true) realVal = "Y"; 
   				else if(checked==false) realVal = "N";
   				
   				$("input[name=masterYn]").eq(i).val(realVal);
   				
   				chkMaster.push(realVal);
   				
   			}
   				if(chkMaster.length > 0){
	   				if($.inArray("Y", chkMaster) == -1){
	   		   			$i.dialog.warning("SYSTEM","책임자를 선택하세요."); 
	   					return;
	   				}
   				}
   			
    		$i.insert("#infrm").done(function(args){ 
   			if(args.returnCode == "EXCEPTION"){ 
   				$i.dialog.error("SYSTEM",args.returnMessage); 
   			}else{
   				$i.dialog.alert("SYSTEM","저장 되었습니다.");
   				makeSubStra(); 
    			resetForm();  
   			} 
   		}).fail(function(e){ 
   			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
   		}); 
    		
    		missionDat.done(function(res){
            	deptChk();   
        	});
    		
       	}//insert
        
        
        
        function inputTrReset(idx){
			$("#txtDeptcd_e0").val("");
			$("#txtDeptnm_e0").val("");
			$("#txtEmpid_e0").val("");
			$("#txtEmpnm_e0").val("");
			$("#deptCheckbox0").val("");
   		} 
         
        function deleteEmp(inputName) {
        	$('#infrm').jqxValidator('hide');
			var input = $("[name=input]").length;
			console.log(input);
			if(input == 1){   
				$("#"+inputName).find($("[name=deptCd]")).val("");
				$("#"+inputName).find($("[name=deptNm]")).val("");
				$("#"+inputName).find($("[name=empId]")).val("");
				$("#"+inputName).find($("[name=empNm]")).val("");
				$("#"+inputName).find($("[name=masterYn]")).val(""); 
			}else{
				$('#'+inputName).remove();	 
			} 
	 }
       

   
    </script> 
    
    <script type="text/javascript"> 
   
    function deleteEnforce(inputName) {
    	$('#infrm').jqxValidator('hide'); 
		var input = $("[name=input]").length;
		if(input == 1){
			$("#"+inputName).find($("[name=deptNm]")).val("");
			$("#"+inputName).find($("[name=empId]")).val("");
			$("#"+inputName).find($("[name=deptCd]")).val("");
			$("#"+inputName).find($("[name=empNm]")).val("");
			$("#"+inputName).find($("[name=masterYn]")).val("");
		}else{
			$('#'+inputName).remove();	
		}
 } 
    
    function resetF(){
    	$("#msidLB").val("");
    	$("#assignidLB").val("");
    	$("#yyyyLB").val("");
    	$("#stranmLB").html("전략목표명");
    	
    	
    	


    } 
    
    </script>
    <style>
	  
	  
	 .blueish .datatable .datatable_fixed table tbody tr:nth-child(odd):hover td:not(.common){
	  	color:#223648 !important;
	  	border-color:#e1e1e1  !important;
	  	background:#ffffff !important;
	  }
	  
	  .blueish .datatable .datatable_fixed table tbody tr:nth-child(even):hover td:not(.common){
	  	color:#223648 !important;
	  	border-color:#e1e1e1  !important; 
	  	background:#f0f5f9 !important;
	  }
	  .blueish .datatable .datatable_fixed table tbody tr td.common{
	  	background:#ffffff !important;
	  
	  }
	  .blueish .datatable .datatable_fixed table tbody tr:hover td.common{
	  	color:#223648 !important;
	  	border-color:#e1e1e1  !important
	  }
	 
	</style>
        
   </head>
   <body class='blueish'>  
     <div class="wrap" style="width:98%; min-width:1580px; margin:0 1%;">
		<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
		
					<form id="searchFrm" name="searchFrm">
						<div class="label type1 f_left">
							비젼:
						</div>
						<div class="combobox f_left"  id='getMS' > 
						</div>
						<div class="label type1 f_left">
							년도:
						</div>
						<div class="combobox f_left"  id='getMSyy' > 
						</div>
						<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='jqxButtonSearch' width="100%" height="100%" />  
						</div>
					</div>
						<input type="hidden" id="msid" name="msId"/> 
						<input type="hidden" id="yyyy" name="yyyy"/>    
					</form> 
		<!--group_button-->
	</div>
	 <div class="container  f_left" style="width:100%;">
		<div class="content f_left" style=" width:100%; margin-right:1%; "> 
	<div class="blueish datatable f_left" style="width:100%; height:450px; margin:5px 0; overflow:hidden;">      
								<!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 헤더가 50px일경우, 300px - 50px= 250px)-->
								<div class="datatable_fixed" style="width:100%; height:450px; float:left; padding:25px 0;">
									<!--group으로 헤더높이가 길어질 경우 padding  (기본높이는 25px)줘야 함-->
									<div style=" height:450px; !important; overflow-x:hidden;">       
										<table  width="100%" cellspacing="0" cellpadding="0" border="0"  > 
							<thead> 
								<tr> 
									<th style="width:15%">전략목표</th>  
									<th style="width:7%">세부전략코드</th> 
									<th style="width:15%">세부전략명</th>    
									<th style="width:7%">추진과제코드</th> 
									<th style="width:35%">추진과제명</th>    
									<th style="width:10%">평가자부서</th>       
									<th style="width:7%">평가자</th>      
									<th style="width:80%">책임자여부</th>	
									<th style="width:17px; min-width:17px; border-bottom:0;"></th>   	       
								</tr>  
							</thead>      
							<tbody id="makeTable"> 
							</tbody>     
						</table> 
					</div>     
				</div>
			</div> 
			<div class="group f_left  w100p m_b5 m_t10">
				<div class="label type2 f_left">  
				<div id="stranmLB" align="left" style="float: left;">
				전략목표명 
				</div>  
			</div>
			</div>
	<form id="infrm" name="infrm">
	<div class="blueish datatable f_left" style="width:100%; height:120px; margin:5px 0; overflow:hidden;">
								<!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 헤더가 50px일경우, 300px - 50px= 250px)-->
								<div class="datatable_fixed" style="width:100%; height:120px; float:left; padding:25px 0;">
									<!--group으로 헤더높이가 길어질 경우 padding  (기본높이는 25px)줘야 함-->
									<div style=" height:95px !important; overflow-x:hidden;">  
								<table  width="100%" cellspacing="0" cellpadding="0" border="0"  class="none_hover">
											<thead>
												<tr>
													<th style="width:25%;">평가자부서</th> 
													<th style="width:25%;">사번</th> 
													<th style="width:25%;">평가자</th>
													<th style="width:15%;">책임자여부</th> 
													<th style="width:10%;"> <div class="pointer m_t3">
															<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/icon-plus.png" alt="추가" height="19" onclick="addEmp();">
														</div></th>
													<th style="width:17px; min-width:17px; border-bottom:0;"></th>
												</tr>
											</thead>
															<input type="hidden" id="msidLB" name="msId" />  
															<input type="hidden" id="assignidLB" name="assignId" />  
															<input type="hidden" id="yyyyLB" name="YYYY" />      
											<tbody id="inputEmp">
												<tr class="tr_hover" name="input">     
													<td  style="width:25%;"><div class="cell">    
															<input type="hidden" id="txtDeptcd_e0" name="deptCd"/> 
															<input type="text" id="txtDeptnm_e0" name="deptNm" class="input type2  f_left" style="width:98%; margin:3px 0;" value=""/>
														</div></td>
													<td style="width:25%;"><div class="cell">  
															<input type="text" id="txtEmpid_e0" name="empId" class="input type2  f_left"  style="width:98%; margin:3px 0;"/>
														</div></td> 
														<td style="width:25%;"><div  class="cell">  
													<input type="text" id="txtEmpnm_e0" name="empNm" class="input type2  f_left"  style="width:90%; margin:3px 0; "/>
													<div class="icon-search f_left m_t7 pointer" onclick="popupUser(0);"> 
													</div>    
												</div></td>  
															<td style="width:15%;"><div class="cell" style="text-align:center;">
															<div class="deptChk" id="deptCheckbox0"  style="text-align:center;"></div>
															<input type="hidden" name="masterYn" id="deptchk0">    
														</div></td> 
													<td style="width:15%;"><div class="cell">   
															<div class="pointer t_center">  
																<img src="../../resources/cmresource/css/iplanbiz/theme/blueish/img/icon-minus.png" alt="삭제" height="19" onclick="deleteEmp(inputTrReset(idx));">
															</div>
													</div></td>  
												</tr>
 											</tbody>
										</table> 	 

									</div>
								</div>
							</div> 
							</form>  
							<div class="button type2 f_right" style="margin-top:10px;"> 
									<input type="button" value="저장" id='' width="100%" height="100%" onclick="insertDept();" /> 
							</div> 
		</div>
		</div>
		</div>
       </body>
</html>
        