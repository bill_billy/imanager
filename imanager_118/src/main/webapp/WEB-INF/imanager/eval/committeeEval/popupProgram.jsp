<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <title>프로그램 현황 </title>
      	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/widget/Form.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
        <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <script src="../../resources/cmresource/js/thirdparty/LGPL/v2/smartEditor2/js/HuskyEZCreator.js" charset="UTF-8"></script>
		<script type="text/javascript">// button
            $(this).ready(function () {               
	        	init();
            });
            function init(){
            	$('#jqxTabs02').jqxTabs({ width: '100%', height: '100%', position: 'top',theme:'blueish', selectedItem: 0});
            	$('#jqxTabs03').jqxTabs({ width: '100%', height: '100%', position: 'top',theme:'blueish', selectedItem: 0});
 				
 				$('#jqxTabs02').on('selected', function (event) 
 				{ 
 					$('#saveForm0').jqxValidator('hide');
 					$('#saveForm1').jqxValidator('hide');
 					$('#saveForm2').jqxValidator('hide');
 					var tabIdx = event.args.item;
 					var gridIdx = $('#programGrid').jqxGrid('getselectedrowindex');
 					
 					
 					
 					if(gridIdx!=-1){
 						var programId = $("#programGrid").jqxGrid('getrowdata',gridIdx).PROGRAM_ID;
 						detailProgram(gridIdx,programId);
 					}else{
 						if(tabIdx==1){
 							detailProgramPlan('','');
 						}else if(tabIdx==2){
 	 						makeEnforceTab('','');
 	 					}
 					}
 					
 				}); 
 				var stra_id = "${param.stra_id}";
 				var substra_id = "${param.substra_id}";
 				var assign_id = "${param.assign_id}";
 				getTitle(stra_id,substra_id);
 				makeProgramGrid(assign_id);
            }
           
	    	function getTitle(straid,substraid){
	    		var dat = $i.sqlhouse(408, {MS_ID:$("#ms_id").val(),STRA_ID:straid,SUBSTRA_ID:substraid});
	    		dat.done(function(res){
	    			console.log(res);
	    			var nm = $("#selectAssignNm", opener.document).html();
	    			res = res.returnArray[0];
	    			
	    			var html = $i.secure.scriptToText(res.STRA_NM) + '<span class="label sublabel type2">'+$i.secure.scriptToText(res.SUBSTRA_NM)+'</span><span class="label sublabel type2">'+$i.secure.scriptToText(nm)+'</span>'
	    			$("#selectStraNm").html(html);
	    		});
	    	}
	    	
	        function makeProgramGrid(assignId){
	        	resetForm0();
	    		resetForm1();
	    		resetForm2();
	    		var nm = $("#selectAssignNm", opener.document).html();
	    		$("#selectProgramNm").html($i.secure.scriptToText(nm));
	        	$("#programGrid").jqxGrid('clearselection');
	        	var ms_id= $("#ms_id").val();
	            var yyyy = $("#yyyy").val();
	        	
	        	var dat = $i.sqlhouse(412,{MS_ID:ms_id,YYYY:yyyy,ASSIGN_ID:assignId});
	    		dat.done(function(res){
	    			var data = res.returnArray;
		        	var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                    	{ name: 'ASSIGN_ID', type: 'string' },
	                    	{ name: 'ASSIGN_CD', type: 'string' },
	                        { name: 'ASSIGN_NM', type: 'string' },
	                        { name: 'PROGRAM_ID', type: 'string' },
	                        { name: 'PROGRAM_NM', type: 'string' },
	                        { name: 'PROGRAM_STATUS', type: 'string' },
	    					{ name: 'DEPT_CD', type: 'string'},
	    					{ name: 'DEPT_NM', type: 'string' },
	    					{ name: 'BUDGET_AMOUNT', type: 'number'},
	    					{ name: 'ENFORCE_AMOUNT', type: 'number'},
	    					{ name: 'CHANGE_AMOUNT', type: 'number'}
	    					
	                    ],
	                    localdata: data,
	                    updaterow: function (rowid, rowdata, commit) {
	                        commit(true);
	                    }
	                };
	    			
	    			var noScript = function (row, columnfield, value) {//left정렬
	    				var newValue = $i.secure.scriptToText(value);
	                     return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
	    							
	                } 
	    			var alignLeft = function (row, columnfield, value) {//left정렬
	    				var assignCd = $("#programGrid").jqxGrid('getrowdata',row).ASSIGN_CD;
	    				var newValue = $i.secure.scriptToText(assignCd+' '+value);
	                     return '<div id="userName-' + row + '"  title="'+value+'" style="text-align:left; margin:4px 0px 0px 10px;">' + newValue + '</div>';
	    							
	                }
	    			var alignRight = function (row, columnfield, value) {//left정렬
	    				var newValue = numberWithCommas(value);
	                    return '<div id="userName-' + row + '"style="text-align:right; margin:4px 4px 0px 0px;">' + newValue + '</div>';
	   							
	               }
	    			var goDetail = function (row, columnfield, value, rowData) {
	    				var newValue = $i.secure.scriptToText(value);
						var program_id = $("#programGrid").jqxGrid('getrowdata',row).PROGRAM_ID;
	                    
						
	                    return '<div id="userName-' + row + '"onclick="detailProgram('+row+','+program_id+');" title="'+value+'" style="text-align:left; margin:4px 0px 0px 10px; cursor:pointer; text-decoration:underline;">' + newValue + '</div>';
	
	                }	     
	                var dataAdapter = new $.jqx.dataAdapter(source, {
	
	                    downloadComplete: function (data, status, xhr) { },
	
	                    loadComplete: function (data) { },
	
	                    loadError: function (xhr, status, error) { }
	
	                });
	
	                var dataAdapter = new $.jqx.dataAdapter(source);
	
	                $("#programGrid").jqxGrid(
	                {
	                  		width: '100%',
			    			height: '100%',
			    			altrows:true,
			    			pageable: false,
			    			source: dataAdapter,
			    			theme:'blueish',
			    			columnsheight:25 ,
			    			rowsheight: 25,
			    			columnsresize: true,
			    			sortable:true,
		                   columns: [
		                      { text: '추진과제', datafield: 'ASSIGN_NM', width: '20%', align:'center', cellsalign: 'center',cellsrenderer:alignLeft},
		                      { text: '프로그램', datafield: 'PROGRAM_NM', width: '30%', align:'center', cellsalign: 'left' ,  cellsrenderer: goDetail },
		                      { text: '상태', datafield: 'PROGRAM_STATUS', width: '10%', align:'center', cellsalign: 'center' ,cellsrenderer:noScript},
		    				  { text: '주관부서', datafield: 'DEPT_NM', width: '10%', align:'center', cellsalign: 'center' ,cellsrenderer:noScript},
		    				  { text: '예산액(천원)', datafield: 'BUDGET_AMOUNT', width: '10%', align:'center', cellsalign: 'right', cellsrenderer:alignRight},
		    				  { text: '집행액(천원)', datafield: 'ENFORCE_AMOUNT', width: '10%', align:'center', cellsalign: 'right', cellsrenderer:alignRight},
		    				  { text: '잔액(천원)', datafield: 'CHANGE_AMOUNT', width: '10%', align:'center', cellsalign: 'right', cellsrenderer:alignRight}
		                    ]
	                });
	    		});
	        }
	        function numberWithCommas(x) {
	            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	        }

	        function detailProgram(row,programId){
	        	$("#program_id0").val(programId);
	        	$("#program_id1").val(programId);
	        	$("#program_id2").val(programId);
	        	$("#program_id3").val(programId);
	        	var ms_id=$("#ms_id").val();
	        	var assignCd = $("#programGrid").jqxGrid('getrowdata',row).ASSIGN_CD;
	        	var assignNm = $("#programGrid").jqxGrid('getrowdata',row).ASSIGN_NM;
	        	var programNm = $("#programGrid").jqxGrid('getrowdata',row).PROGRAM_NM;
				var html = $i.secure.scriptToText(assignCd+" "+assignNm)+'<span class="label sublabel type2">'+$i.secure.scriptToText(programNm)+'</span>';
	        	
	        	$("#selectProgramNm").html(html);
	        	
	        	
	        	var tabIdx = $('#jqxTabs02').jqxTabs('selectedItem');
	        	if(tabIdx==0){//기본
	        		detailProgramInfo(ms_id,row);
	        	}else if(tabIdx==1){
	        		detailProgramPlan(ms_id,row);
	        	}else if(tabIdx==2){
	        		detailProgramRun(ms_id,row);
	        	}else if(tabIdx==3){
	        		detailProgramDeptEval(ms_id,row);
	        	}
	        	
	        }
	        function detailProgramInfo(ms_id,row){
	        	var programId = $("#program_id0").val();
	        	var dat = $i.sqlhouse(414,{MS_ID:ms_id,PROGRAM_ID:programId});
	    		dat.done(function(res){
	    			console.log(res);
	    			var programData = res.returnArray[0];
	    			
	    			$("#program_nm").val(programData.PROGRAM_NM);
		    		$("#txtAssignId").val(programData.ASSIGN_ID);
		    		$("#txtAssignNm").val(programData.ASSIGN_NM);
		    		$("#txtDeptcd").val(programData.DEPT_CD);
		    		$("#txtDeptnm").val(programData.DEPT_NM);
		    		$("#txtEmpno").val(programData.EMP_NO);
		    		$("#txtEmpnm").val(programData.EMP_NM);
		    		$("#txtDeptcd_c").val(programData.CONFIRM_DEPT_CD);
		    		$("#txtDeptnm_c").val(programData.CONFIRM_DEPT_NM);
		    		$("#txtEmpno_c").val(programData.CONFIRM_EMP_NO);
		    		$("#txtEmpnm_c").val(programData.CONFIRM_EMP_NM);
		    		$("#txtDeptcd_e").val(programData.EVAL_DEPT_CD);
		    		$("#txtDeptnm_e").val(programData.EVAL_DEPT_NM);
		    		$("#txtEmpno_e").val(programData.EVAL_EMP_NO);
		    		$("#txtEmpnm_e").val(programData.EVAL_EMP_NM);
		    		$("#program_status").val(programData.PROGRAM_STATUSNM);
		    		
		    		var sdat = programData.SCH_START_DAT;
    				var edat = programData.SCH_END_DAT;
		    		$("#sch_dat").val(sdat+" ~ "+edat)
		    		
		    		
		    		$("#budget_total0").val(numberWithCommas($("#programGrid").jqxGrid('getrowdata',row).BUDGET_AMOUNT));
	    			$("#enforce_total0").val(numberWithCommas($("#programGrid").jqxGrid('getrowdata',row).ENFORCE_AMOUNT));
	    			$("#change_total0").val(numberWithCommas($("#programGrid").jqxGrid('getrowdata',row).CHANGE_AMOUNT));
		    		
		    		
	    		});
	    		
	    		var real = $i.sqlhouse(423,{MS_ID:ms_id,PROGRAM_ID:programId});
	    		real.done(function(res){
	    			if(res.returnArray.length>0){
	    				res = res.returnArray[0];
	    				var sdat = res.REAL_START_DAT;
	    				var edat = res.REAL_END_DAT;
		    			$("#real_dat").val(sdat+" ~ "+edat);
		    			
	    			}else{
	    				$("#real_dat").val("");
	    			}
	    		});
	    		
	    		
	    		
	        }
	        function detailProgramPlan(ms_id,row){
	        	var programId = $("#program_id1").val();
	        	var dat = $i.sqlhouse(414,{MS_ID:ms_id,PROGRAM_ID:programId});
	    		dat.done(function(res){
	    			resetForm1('detail');
	    			var programData = res.returnArray[0];
	    			
		    		$("#quan_goal").html(programData.QUAN_GOAL);
		    		$("#quan_unit").html(programData.QUAN_UNITNM);
		    		$("#quan_content").val(programData.QUAN_CONTENT);
		    		$("#qual_goal").val(programData.QUAL_GOAL);
		    		$("#summary").val(programData.SUMMARY);
		    		$("#effect").val(programData.EFFECT);
		    		$("#content").html(programData.CONTENT);
	    		});
	    		
	    		var budgetDat = $i.sqlhouse(419,{MS_ID:ms_id,PROGRAM_ID:programId});
	    		budgetDat.done(function(res){
				var budgetArr = res.returnArray;
	    			
	    			if(res.returnCode=="SUCCESS"){
	    				
		    			
		    			var source =
		                {
		                    datatype: "json",
		                    datafields: [
		                        { name: 'MS_ID', type: 'string' },
		                        { name: 'PROGRAM_ID', type: 'string' },
		    					{ name: 'BUDGET_ID', type: 'string'},
		    					{ name: 'BUDGET_TYPNM', type: 'string' },
		    					{ name: 'BUDGET_CONTENT', type: 'string' },
		    					{ name: 'BUDGET_START_DAT', type: 'string' },
		    					{ name: 'BUDGET_END_DAT', type: 'string' },
		    					{ name: 'BUDGET_AMOUNT', type: 'number' },
		    					{ name: 'BUDGET_DEF', type: 'number' }
		                    ],
		                    localdata: budgetArr,
		                    updaterow: function (rowid, rowdata, commit) {
		                        commit(true);
		                    }
		                };
		    			
		    			var alginLeft = function (row, columnfield, value) {//left정렬
		                           return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
		    							
		                      } 
		    			var alginRight = function (row, columnfield, value) {//right정렬
		                           return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
		    							
		                      } 	
		    			var budgetDate = function (row,columnfield, value) {
		    				var sdate = $("#budgetGrid").jqxGrid('getrowdata',row).BUDGET_START_DAT;
		    				var edate = $("#budgetGrid").jqxGrid('getrowdata',row).BUDGET_END_DAT;
		    				return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + sdate + ' ~ ' + edate + '</div>';
		    			};
		    			var numberColumn = function (row,columnfield, value) {
		    				return '<div id="userName-' + row + '"style="text-align:right; margin:4px 4px 0px 0px;">' + numberWithCommas(value) + '</div>';
		    			};
		    			var ratefield = function (row,columnfield,value){
		    				var budgetAmount = $("#budgetGrid").jqxGrid('getrowdata',row).BUDGET_AMOUNT;
		    				var enforceAmount = $("#budgetGrid").jqxGrid('getrowdata',row).ENFORCE_AMOUNT;
		    				var rate = 0;
		    				if(budgetAmount!=0)
		    					rate = (parseInt(enforceAmount)/parseInt(budgetAmount)*100).toFixed(2);
		    				
		    				return '<div id="userName-' + row + '"style="text-align:right; margin:4px 4px 0px 0px;">' + numberWithCommas(rate) + '</div>';
		    			};
		    			var changefield = function (row,columnfield,value){
		    				var budgetAmount = $("#budgetGrid").jqxGrid('getrowdata',row).BUDGET_AMOUNT;
		    				var enforceAmount = $("#budgetGrid").jqxGrid('getrowdata',row).ENFORCE_AMOUNT;
		    				return '<div id="userName-' + row + '"style="text-align:right; margin:4px 4px 0px 0px;">' + numberWithCommas(budgetAmount-enforceAmount) + '</div>';
		    			};
		    			var noScript = function (row,columnfield,value){
		    				var newValue = $i.secure.scriptToText(value);
		    				return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 4px;">' + newValue + '</div>';
		    			};
		    			
		                var dataAdapter = new $.jqx.dataAdapter(source, {
	
		                    downloadComplete: function (data, status, xhr) { },
	
		                    loadComplete: function (data) { },
	
		                    loadError: function (xhr, status, error) { }
	
		                });
	
		                var dataAdapter = new $.jqx.dataAdapter(source);
	
		                $("#budgetGrid").jqxGrid(
		                {
		                  	width: '100%',
			    			height: '100%',
			    			altrows:true,
			    			sortable:true,
			    			columnsresize:true,
			    			pageable: false,
			    			pageSize: 10,
			    			pageSizeOptions: ['100', '200', '300'],
			    			source: dataAdapter,
			    			theme:'blueish',
			    			columnsheight:25 ,
			    			rowsheight: 25,
		                   columns: [
		                       { text: '기획 및 예산', datafield: 'BUDGET_CONTENT', width: '25%', align:'center', cellsalign: 'left',cellsrenderer:noScript },
		                      { text: '구분', datafield: 'BUDGET_TYPNM', width: '15%', align:'center', cellsalign: 'center'  },
		                      { text: '기간', datafield: 'BUDGET_START_DAT', width: '15%', align:'center', cellsalign: 'center' ,cellsrenderer:budgetDate},
		    				  { text: '예산액(천원)', datafield: 'BUDGET_AMOUNT', width: '15%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn},
		    				  { text: '비고', datafield: 'BUDGET_DEF', width: '30%', align:'center', cellsalign: 'left' ,cellsrenderer:alginLeft}
		                    ]
		                });
	    			}
	    		});
	    		
	    		getFileListTr(ms_id,programId);
	        }
	        function detailProgramRun(ms_id,row){
	        	var programId = $("#program_id2").val();
	        	var dat = $i.sqlhouse(414,{MS_ID:ms_id,PROGRAM_ID:programId});
	    		dat.done(function(res){
	    			resetForm2('detail');
	    			
	    			var programData = res.returnArray[0];
	    			
	    			
	    			
		    		$("#start_dat").val(programData.START_DAT);
		    		$("#end_dat").val(programData.END_DAT);
		    		$("#result").html(programData.RESULT);
	    			
	    			
	    			$("#budget_total2").val(numberWithCommas($("#programGrid").jqxGrid('getrowdata',row).BUDGET_AMOUNT));
	    			$("#enforce_total2").val(numberWithCommas($("#programGrid").jqxGrid('getrowdata',row).ENFORCE_AMOUNT));
	    			$("#change_total2").val(numberWithCommas($("#programGrid").jqxGrid('getrowdata',row).CHANGE_AMOUNT));
	    		});
	    		
	    		makeEnforceTab(ms_id,programId);
	    		getFileListTr2(ms_id,programId);
	    		
	    		
	        }
	        
			function makeEnforceTab(ms_id,programId){
	        	
	        	var budgetDat = $i.sqlhouse(422,{MS_ID:ms_id,PROGRAM_ID:programId});
	    		budgetDat.done(function(res){
	    			
	    			if(res.returnCode=="SUCCESS"){
	    				
	    			
		    			var budgetArr = res.returnArray;
		    			
		    			var source =
		                {
		                    datatype: "json",
		                    datafields: [
		                        { name: 'MS_ID', type: 'string' },
		                        { name: 'PROGRAM_ID', type: 'string' },
		    					{ name: 'BUDGET_ID', type: 'string'},
		    					{ name: 'BUDGET_TYPNM', type: 'string' },
		    					{ name: 'BUDGET_CONTENT', type: 'string' },
		    					{ name: 'BUDGET_START_DAT', type: 'string' },
		    					{ name: 'BUDGET_END_DAT', type: 'string' },
		    					{ name: 'BUDGET_AMOUNT', type: 'number' },
		    					{ name: 'ENFORCE_03', type: 'number' },
		    					{ name: 'ENFORCE_04', type: 'number' },
		    					{ name: 'ENFORCE_05', type: 'number' },
		    					{ name: 'ENFORCE_06', type: 'number' },
		    					{ name: 'ENFORCE_07', type: 'number' },
		    					{ name: 'ENFORCE_08', type: 'number' },
		    					{ name: 'ENFORCE_09', type: 'number' },
		    					{ name: 'ENFORCE_10', type: 'number' },
		    					{ name: 'ENFORCE_11', type: 'number' },
		    					{ name: 'ENFORCE_12', type: 'number' },
		    					{ name: 'ENFORCE_01', type: 'number' },
		    					{ name: 'ENFORCE_02', type: 'number' },
		    					{ name: 'ENFORCE_AMOUNT', type: 'number' }
		                    ],
		                    localdata: res,
		                    updaterow: function (rowid, rowdata, commit) {
		                        commit(true);
		                    }
		                };
		    			
		    			var alginLeft = function (row, columnfield, value) {//left정렬
		                           return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
		    							
		                      } 
		    			var alginRight = function (row, columnfield, value) {//right정렬
		                           return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
		    							
		                      } 	
		    			var budgetDate = function (row,columnfield, value) {
		    				var sdate = $("#planBudgetGrid").jqxGrid('getrowdata',row).BUDGET_START_DAT;
		    				var edate = $("#planBudgetGrid").jqxGrid('getrowdata',row).BUDGET_END_DAT;
		    				return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + sdate + ' ~ ' + edate + '</div>';
		    			};
		    			var numberColumn = function (row,columnfield, value) {
		    				var newValue = numberWithCommas(value);
		    				if(newValue=="") newValue="-";
		    				return '<div id="userName-' + row + '"style="text-align:right; margin:4px 4px 0px 0px;">' + newValue + '</div>';
		    			};
		    			var ratefield = function (row,columnfield,value){
		    				var budgetAmount = $("#planBudgetGrid").jqxGrid('getrowdata',row).BUDGET_AMOUNT;
		    				var enforceAmount = $("#planBudgetGrid").jqxGrid('getrowdata',row).ENFORCE_AMOUNT;
		    				var rate = 0;
		    				if(budgetAmount!=0)
		    					rate = (parseInt(enforceAmount)/parseInt(budgetAmount)*100).toFixed(2);
		    				
		    				return '<div id="userName-' + row + '"style="text-align:right; margin:4px 4px 0px 0px;">' + numberWithCommas(rate) + '</div>';
		    			};
		    			var changefield = function (row,columnfield,value){
		    				var budgetAmount = $("#planBudgetGrid").jqxGrid('getrowdata',row).BUDGET_AMOUNT;
		    				var enforceAmount = $("#planBudgetGrid").jqxGrid('getrowdata',row).ENFORCE_AMOUNT;
		    				return '<div id="userName-' + row + '"style="text-align:right; margin:4px 4px 0px 0px;">' + numberWithCommas(budgetAmount-enforceAmount) + '</div>';
		    			};
		    			
		    			var titleColumn = function (row, columnfield, value) {//left정렬
		    				var newValue = $i.secure.scriptToText(value);
	                           return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;" title="'+value+'">' + newValue + '</div>';
	    							
	                      }  
		    			   
		                var dataAdapter = new $.jqx.dataAdapter(source, {
	
		                    downloadComplete: function (data, status, xhr) { },
	
		                    loadComplete: function (data) { },
	
		                    loadError: function (xhr, status, error) { }
	
		                });
	
		                var dataAdapter = new $.jqx.dataAdapter(source);
	
		                $("#planBudgetGrid").jqxGrid(
		                {
		                  	width: '100%',
			    			height: '100%',
			    			altrows:true,
			    			sortable:true,
			    			columnsresize:true,
			    			pageable: false,
			    			pageSize: 10,
			    			pageSizeOptions: ['10', '20', '30'],
			    			source: dataAdapter,
			    			theme:'blueish',
			    			columnsheight:25 ,
			    			rowsheight: 25,
		                   columns: [
		                       { text: '기획 및 예산', datafield: 'BUDGET_CONTENT', width: '15%', align:'center', cellsalign: 'left',cellsrenderer:titleColumn },
		                      { text: '구분', datafield: 'BUDGET_TYPNM', width: '15%', align:'center', cellsalign: 'center'  },
		                      { text: '기간', datafield: 'BUDGET_START_DAT', width: '15%', align:'center', cellsalign: 'center' ,cellsrenderer:budgetDate},
		    				  { text: '예산액(천원)', datafield: 'BUDGET_AMOUNT', width: '15%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn},
		    				  { text: '집행액(천원)', datafield: 'ENFORCE_AMOUNT', width: '15%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn},
		    				  { text: '%', datafield: 'f', width: '10%', align:'center', cellsalign: 'right',cellsrenderer:ratefield },
		    				  { text: '잔액(천원)', datafield: 'g', width: '15%', align:'center', cellsalign: 'right',cellsrenderer:changefield }
		                    ]
		                });
	    			
		                var changefield2 = function (row,columnfield,value){
		    				var budgetAmount = $("#planEnforceGrid").jqxGrid('getrowdata',row).BUDGET_AMOUNT;
		    				var enforceAmount = $("#planEnforceGrid").jqxGrid('getrowdata',row).ENFORCE_AMOUNT;
		    				return '<div id="userName-' + row + '"style="text-align:right; margin:4px 4px 0px 0px;">' + numberWithCommas(budgetAmount-enforceAmount) + '</div>';
		    			};
	    			
		                $("#planEnforceGrid").jqxGrid(
				                {
				                  	width: '100%',
					    			height: '100%',
					    			altrows:true,
					    			sortable:true,
					    			columnsresize:true,
					    			pageable: false,
					    			pageSize: 10,
					    			pageSizeOptions: ['10', '20', '30'],
					    			source: dataAdapter,
					    			theme:'blueish',
					    			columnsheight:25 ,
					    			rowsheight: 25,
				                   columns: [
				                       { text: '기획 및 예산', datafield: 'BUDGET_CONTENT', width: '15%', align:'center', cellsalign: 'left',cellsrenderer:titleColumn },
				                      { text: '구분', datafield: 'BUDGET_TYPNM', width: '10%', align:'center', cellsalign: 'center'  },
				    				  { text: '예산액(천원)', datafield: 'BUDGET_AMOUNT', width: '7%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn},
				    				  { text: '3월', datafield: 'ENFORCE_03', width: '5.5%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn,columngroup:'ENFORCE_TITLE'},
				    				  { text: '4월', datafield: 'ENFORCE_04', width: '5.5%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn,columngroup:'ENFORCE_TITLE'},
				    				  { text: '5월', datafield: 'ENFORCE_05', width: '5.5%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn,columngroup:'ENFORCE_TITLE'},
				    				  { text: '6월', datafield: 'ENFORCE_06', width: '5.5%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn,columngroup:'ENFORCE_TITLE'},
				    				  { text: '7월', datafield: 'ENFORCE_07', width: '5.5%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn,columngroup:'ENFORCE_TITLE'},
				    				  { text: '8월', datafield: 'ENFORCE_08', width: '5.5%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn,columngroup:'ENFORCE_TITLE'},
				    				  { text: '9월', datafield: 'ENFORCE_09', width: '5.5%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn,columngroup:'ENFORCE_TITLE'},
				    				  { text: '10월', datafield: 'ENFORCE_10', width: '5.5%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn,columngroup:'ENFORCE_TITLE'},
				    				  { text: '11월', datafield: 'ENFORCE_11', width: '5.5%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn,columngroup:'ENFORCE_TITLE'},
				    				  { text: '12월', datafield: 'ENFORCE_12', width: '5.5%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn,columngroup:'ENFORCE_TITLE'},
				    				  { text: '1월', datafield: 'ENFORCE_01', width: '5.5%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn,columngroup:'ENFORCE_TITLE'},
				    				  { text: '2월', datafield: 'ENFORCE_02', width: '5.5%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn,columngroup:'ENFORCE_TITLE'},
				    				  { text: '합계', datafield: 'ENFORCE_AMOUNT', width: '5.5%', align:'center', cellsalign: 'right' ,cellsrenderer:numberColumn,columngroup:'ENFORCE_TITLE'},
				    				  { text: '잔액(천원)', datafield: 'g', width: '6.5%', align:'center', cellsalign: 'right',cellsrenderer:changefield2 }
				                    ],
				                    columngroups: [{
				                        text: '집행액',
				                        name: 'ENFORCE_TITLE',
				                        align: 'center'
				                    }]
				                });
	    			}
	    			
	    			
	    		});
	        }
	        function detailProgramDeptEval(ms_id,row){
	        	var programId = $("#program_id3").val();
	        	var yyyy = $("#yyyy").val();
	        	resetForm3('detail');
	        	var dat = $i.sqlhouse(414,{MS_ID:ms_id,PROGRAM_ID:programId});
	    		dat.done(function(res){
	    			var programData = res.returnArray[0];
	    			$("#quan_goal3").val(programData.QUAN_GOAL);
		    		$("#quan_unit3").val(programData.QUAN_UNITNM);
	    		});
	    		
	    		var eval = $i.sqlhouse(427,{MS_ID:ms_id,PROGRAM_ID:programId,YYYY:yyyy});
	    		eval.done(function(res){
	    			res = res.returnArray;
	    			if(res.length>0){
	    				res = res[0];
	    				$("#quan_goal3").val(res.QUAN_GOAL);
			    		$("#quan_unit3").val(res.QUAN_UNITNM);
			    		
	    				$("#quan_ach_val").val(res.QUAN_ACH_VAL);
	    				$("#quan_def3").val(res.QUAN_DEF);
	    				$("#quan_content3").val(res.QUAN_CONTENT);
	    				
	    				var yn = res.QUAL_ACH_YN;
	    				if(yn=="Y") $("#qual_ach_yn").html("달성");
	    				if(yn=="N") $("#qual_ach_yn").html("미달성");
	    				
	    				
	    				$("#qual_content3").val(res.QUAL_CONTENT);
	    				$("#cnt_tot").val(res.CNT_TOT);
	    				$("#cnt_tot_txt").html(res.CNT_TOT!=""?res.CNT_TOT+"명":"0명");
	    				$("#cnt_a").val(res.CNT_A!=""?res.CNT_A+"명":"0명");
	    				$("#cnt_b").val(res.CNT_B!=""?res.CNT_B+"명":"0명");
	    				$("#cnt_c").val(res.CNT_C!=""?res.CNT_C+"명":"0명");
	    				$("#cnt_d").val(res.CNT_D!=""?res.CNT_D+"명":"0명");
	    				$("#cnt_e").val(res.CNT_E!=""?res.CNT_E+"명":"0명");
	    				
	    				$("#eval_score").html(res.EVAL_SCORE!=""?res.EVAL_SCORE+'점':"");
	    				$("#eval_result").val(res.EVAL_RESULT);
	    				
	    				var rate = "";
	    				if($("#quan_goal3").val()!="" && $("#quan_goal3").val()!="0" && $("#quan_ach_val").val()!="")
	    					rate = (parseFloat($("#quan_ach_val").val())/parseFloat($("#quan_goal3").val())*100).toFixed(2);
	    				$("#quan_ach_rate").val(rate);
	    				
	    				if($("#cnt_tot").val()=="")	
	    					calCntTotal();
	    				
	    				
	    				
	    				var eval_confirm = res.EVAL_CONFIRM;
	    				if(eval_confirm=='Y'){
	    					$("#hidden3_1").css("display","block");
	    					$("#hidden3_2").css("display","none");
	    					$("#hidden3_3").css("display","none");
	    				}else{
		    				$("#hidden3_1").css("display","none");
		    				$("#hidden3_2").css("display","block");
		    				$("#hidden3_3").css("display","block");	
	    				}
	    			
	    				
	    			}
	    		});
	        }
	        function resetForm3(detail){
	        	$('#saveForm3').jqxValidator('hide');
	        	if(detail!='detail'){
	        		$("#program_id3").val("");
	        		document.getElementById("saveForm3").reset();
	        	}
	        	$("#quan_goal3").val("");
	        	$("#quan_unit3").val("");
	        	$("#quan_ach_val").val("");
	        	$("#quan_ach_rate").val("");
	        	$("#quan_def3").val("");
	        	$("#quan_content3").val("");
	        	$("#qual_content3").val("");
	        	$("#cnt_tot").val("");
	        	$("#cnt_tot_txt").html("");
	        	$("#cnt_a").val("");
	        	$("#cnt_b").val("");
	        	$("#cnt_c").val("");
	        	$("#cnt_d").val("");
	        	$("#cnt_e").val("");
	        	
	        	$("#eval_result").val("");
	        	
	        	$("#eval_score").html("");
	        	$("#qual_ach_yn").html("");
	        	
	        }
	        function resetForm0(){
	        	$('#saveForm0').jqxValidator('hide');
	        	$("#programGrid").jqxGrid('clearselection');
	        	$("#program_id0").val("");
	        	$("#program_nm").val("");
    			$("#txtAssignId").val("");
    			$("#txtAssignNm").val("");
    			$("#txtDeptcd").val("");
    			$("#txtDeptnm").val("");
    			$("#txtEmpno").val("");
    			$("#txtEmpnm").val("");
    			$("#txtDeptcd_c").val("");
    			$("#txtDeptnm_c").val("");
    			$("#txtEmpno_c").val("");
    			$("#txtEmpnm_c").val("");
    			$("#txtDeptcd_e").val("");
    			$("#txtDeptnm_e").val("");
    			$("#txtEmpno_e").val("");
    			$("#txtEmpnm_e").val("");
    			$("#sch_start_dat").val("");
    			$("#sch_end_dat").val("");
    			$("#budget_total0").val("");
    			$("#enforce_total0").val("");
    			$("#change_total0").val("");
    			
	        }
	      
	        
	        function resetForm1(detail){
	        	$('#saveForm1').jqxValidator('hide');
	        	if(detail!='detail'){
	        		$("#program_id1").val("");
	        		$("#fileListTr").remove();
		        	$("#inputFile").empty();
	        	}
	        		
	        	
	        	$("#content").html("");
	        	$("#quan_unit").html("");
	        	$("#quan_goal").html("");
	        	$("#quan_content").val("");
	        	$("#qual_goal").val("");
	        	$("#summary").val("");
	        	$("#effect").val("");
	        	
	        }
	        
	        
			function getFileListTr(ms_id,programId){
				$("#fileListTr").remove();
	    		var fileDat = $i.sqlhouse(424,{MS_ID:ms_id,PROGRAM_ID:programId,FILE_GUBUN:'A',FILE_ID:''});
	    		fileDat.done(function(res){
	    			var fileArr = res.returnArray;
	    			if(fileArr.length>0){
	    				var html = '<tr id="fileListTr"><th colspan="2"><div>파일목록</div></th><td colspan="3"><div class="cell">';
	    				for(var i=0;i<fileArr.length;i++){
	    					//html+='<div style="font-weight: bold; padding-right: 10px;">'+fileArr[i].FILE_ORG_NAME+'<a href="./download?ms_id='+ms_id+'&program_id='+programId+'&file_gubun=A&file_id='+fileArr[i].FILE_ID+'"><img src="../../resources/cmresource/image/icon-file-put.png" border="0" alt="'+fileArr[i].FILE_ORG_NAME+'" /></a></div>';
	    					html+='<div name="fileUpload1" style="font-weight: bold; padding-right: 10px;"><a href="./download?ms_id='+ms_id+'&program_id='+programId+'&file_gubun=A&file_id='+fileArr[i].FILE_ID+'">'+fileArr[i].FILE_ORG_NAME+'</a></div>';
	    				}
	    				html+='</div></td></tr>';
	    				$("#fileListBefore").after(html);
	    			}
	    		});
			}
			function getFileListTr2(ms_id,programId){
				$("#fileListTr2").remove();
	    		var fileDat = $i.sqlhouse(424,{MS_ID:ms_id,PROGRAM_ID:programId,FILE_GUBUN:'B',FILE_ID:''});
	    		fileDat.done(function(res){
	    			console.log(res);
	    			var fileArr = res.returnArray;
	    			if(fileArr.length>0){
	    				var html = '<tr id="fileListTr2"><th colspan="2"><div>파일목록</div></th><td colspan="3"><div class="cell">';
	    				for(var i=0;i<fileArr.length;i++){
	    					//html+='<div style="font-weight: bold; padding-right: 10px;">'+fileArr[i].FILE_ORG_NAME+'<a href="./download?ms_id='+ms_id+'&program_id='+programId+'&file_gubun=A&file_id='+fileArr[i].FILE_ID+'"><img src="../../resources/cmresource/image/icon-file-put.png" border="0" alt="'+fileArr[i].FILE_ORG_NAME+'" /></a></div>';
	    					html+='<div name="fileUpload2" style="font-weight: bold; padding-right: 10px;"><a href="./download?ms_id='+ms_id+'&program_id='+programId+'&file_gubun=B&file_id='+fileArr[i].FILE_ID+'">'+fileArr[i].FILE_ORG_NAME+'</a></div>';
	    				}
	    				html+='</div></td></tr>';
	    				$("#fileListBefore2").after(html);
	    			}
	    		});
			}
			
			
			
			function resetForm2(detail){
				$('#saveForm2').jqxValidator('hide');
				if(detail!='detail'){
					$("#program_id2").val("");
		        	
		        	$("#fileListTr2").remove();
		        	$("#inputRunFile").empty();
		        	
		        	makeEnforceTab('','');
				}
	        	
	        	$("#result").html("");
				
	        	
	        	$("#budget_total2").val("");
    			$("#enforce_total2").val("");
    			$("#change_total2").val("");

	        	$("#insertBudgetId").val("");
	        	$("#insertBudgetNm").html("");
	        	$("#tbodyRunEnforce").empty();
	        	
	        	
	        }
			
	        
	       
	       
	        
	        
	        
	       
	        function inputTrReset(idx){
				$("#budget_id0").val("");
				$("#budget_content0").val("");
				$("#budget_start_dat0").val("");
				$("#budget_end_dat0").val("");
				$("#budget_amount0").val("");
	   		} 
	        function deleteBudget(inputName) {
	        	$('#saveForm1').jqxValidator('hide');
				var input = $("[name=inputTr]").length;
				if(input == 1){
					$("#"+inputName).find($("[name=budget_id]")).val("");
					$("#"+inputName).find($("[name=budget_content]")).val("");
					$("#"+inputName).find($("[name=budget_start_dat]")).val("");
					$("#"+inputName).find($("[name=budget_end_dat]")).val("");
					$("#"+inputName).find($("[name=budget_amount]")).val("");
				}else{
					$('#'+inputName).remove();	
				}
		 }
	        
	        function inputEnforceReset(idx){
				$("#enforce_id0").val("");
				$("#enforce_content0").val("");
				$("#enforce_start_dat0").val("");
				$("#enforce_end_dat0").val("");
				$("#enforce_amount0").val("");
				$("#enforce_def0").val("");
	   		} 
	        function deleteEnforce(inputName) {
	        	$('#saveForm2').jqxValidator('hide');
				var input = $("[name=inputEnforceTr]").length;
				if(input == 1){
					$("#"+inputName).find($("[name=enforce_id]")).val("");
					$("#"+inputName).find($("[name=enforce_content]")).val("");
					$("#"+inputName).find($("[name=enforce_start_dat]")).val("");
					$("#"+inputName).find($("[name=enforce_end_dat]")).val("");
					$("#"+inputName).find($("[name=enforce_amount]")).val("");
					$("#"+inputName).find($("[name=enforce_def]")).val("");
				}else{
					$('#'+inputName).remove();	
				}
		 }
	        
		</script>
	
<style>
.blueish .input.type2{
	border:none;
	background:#ffffff;
}
.blueish .textarea{
	border:none;
	background:#ffffff;
	overflow:auto;
}
</style>
</head>
<body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
<div class="wrap" style="width:1200px; min-width:1200px; margin:0 1%;">
	
	<div class="container  f_left" style="width:100%;">
	
		<div class="content f_left" style=" width:98%;">
			<div class="group f_left  w100p m_b5">
				<div class="label type2 f_left" id="selectStraNm">
				전락목표
				</div>
				<div class="label type2 f_left" id="popupAssignNm">
			
				</div>
				<div class="combobox f_left"  id='jqxAssignCombo' >
				</div>
			</div>
			<!--group-->
			<div class="grid f_left" style="width:100%; margin:5px 0%; height:151px;">
				<div id="programGrid">
				</div>
			</div>
			<!--grid -->
			<div class="group f_left  w100p m_b5 m_t10">
				<div class="label type2 f_left" id="selectProgramNm">
				추진과제명
				</div>
			</div>
			<!--group-->
			
				<input type="hidden" id="ms_id" value="${param.ms_id}"/>
				<input type="hidden" id="yyyy" value="${param.yyyy}"/>
				<input type="hidden" id="assign_id" value="${param.assign_id}"/>
			
			<div class="tabs f_left" style=" width:100%; height:472px; margin-top:0px;">
				<div id='jqxTabs02'>
					<ul>
						<li style="margin-left: 0px;">기본정보</li>
						<li>계획</li>
						<li>운영</li>
						<li>부서평가</li>
					</ul>
					
					<!-- 기본탭 S -->
					<div class="tabs_content" style="height:100%; ">
						<div class="content f_left" style="width:97%; margin:10px 1.5%;">
							<div class="table  f_left" style="width:100%; margin:0; ">
								<form name="saveForm0" id="saveForm0">
								<input type="hidden" id="ms_id0" name="ms_id"/>
								<input type="hidden" id="yyyy0"/>
								<input type="hidden" id="program_id0" name="program_id"/>
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<th class="w15p"><div>
													프로그램명
												</div></th>
											<td colspan="3"><div class="cell">
													<input type="text" value="" id="program_nm" readonly name="program_nm" class="input type2  f_left"  style="width:99%; margin:3px 0;"/>
												</div></td>
										</tr>
										<tr>
											<th><div>
													추진과제
												</div></th>
											<td class="w35p"><div  class="cell">
													<input type="hidden" name="assign_id" id="txtAssignId"/>
													<input type="text" value="" id="txtAssignNm" readonly class="input type2 f_left"  style="width:75%; margin:3px 0; "/>
													
												</div></td>
											<th class="w15p"><div>
												</div></th>
											<td class="w35p"><div  class="cell">
													
												</div></td>
										</tr>
										<tr>
											<th><div>
													담당자
												</div></th>
											<td class="w35p"><div  class="cell">
													<input type="hidden" id="txtDeptcd" name="dept_cd"/>
													<input type="hidden" id="txtDeptnm" name="dept_nm"/>
													<input type="hidden" id="txtEmpno" name="emp_no"/>
													<input type="text" value="" id="txtEmpnm" name="emp_nm" readonly class="input type2  f_left"  style="width:75%; margin:3px 0; "/>
													
												</div></td>
											<th class="w15p"><div>
													확인자
												</div></th>
											<td class="w35p"><div  class="cell">
													<input type="hidden" id="txtDeptcd_c" name="confirm_dept_cd"/>
													<input type="hidden" id="txtDeptnm_c" name="confirm_dept_nm"/>
													<input type="hidden" id="txtEmpno_c" name="confirm_emp_no"/>
													<input type="text" value="" id="txtEmpnm_c" name="confirm_emp_nm" readonly class="input type2  f_left"  style="width:75%; margin:3px 0; "/>
													
												</div></td>
										</tr>
										<tr>
											<th><div>
													부서평가자
												</div></th>
											<td class="w35p"><div  class="cell">
													<input type="hidden" id="txtDeptcd_e" name="eval_dept_cd"/>
													<input type="hidden" id="txtDeptnm_e" name="eval_dept_nm"/>
													<input type="hidden" id="txtEmpno_e" name="eval_emp_no"/>
													<input type="text" value="" id="txtEmpnm_e" name="eval_emp_nm" readonly class="input type2  f_left"  style="width:75%; margin:3px 0; "/>
													
												</div></td>
											<th class="w15p"><div>
													상태
												</div></th>
											<td class="w35p"><div  class="cell">
													<input type="text" value="" id="program_status" name="program_status" readonly class="input type2  f_left"  style="width:75%; margin:3px 0; "/>
												</div></td>
										</tr>
										<tr>
											<th><div>
													사업예정기간
												</div></th>
											<td class="w35p"><div  class="cell">
													<input id="sch_dat" type="text" readonly class="input type2 f_left" style="width:97.5%; margin:3px 0;" value=""/>
													
												</div></td>
											<th class="w15p"><div>
													실사업기간
												</div></th>
											<td class="w35p"><div  class="cell">
													<input type="text" value="" id="real_dat" readonly class="input type2  f_left"  style="width:97.5%; margin:3px 0; "/>
												</div></td>
										</tr>
									</tbody>
								</table>
								</form>
							</div>
							<!--table-->
							<div class="group f_left m_t20" style="width:100%; margin:10px 0;">
								<div class="label type2 f_left m_r5 none_bg_img">
									<span class="sublabel" style="font-weight:bold;">예산액(천원):</span>
									<input type="text" value="" id="budget_total0" class="input type2 t_left"  readonly style="width:150px; margin:3px 0; "/>
								</div>
								<div class="label type2 f_left m_r5 none_bg_img">
									<span class="sublabel" style="font-weight:bold;">집행액(천원):</span>
									<input type="text" value="" id="enforce_total0" class="input type2 t_left" readonly style="width:150px; margin:3px 0; "/>
								</div>
								<div class="label type2 f_left m_r5 none_bg_img">
									<span class="sublabel" style="font-weight:bold;">잔액(천원):</span>
									<input type="text" value="" id="change_total0" class="input type2 t_left" readonly style="width:150px; margin:3px 0; "/>
								</div>
							</div>
							<!--group-->
							
						</div>
						<!--content-->
					</div>
					<!--//tabs_content-->
					
					<!-- 계획탭 S -->
					<div  class="tabs_content" style=" height:100%; ">
					<form name="saveForm1" id="saveForm1" method="post" enctype="multipart/form-data">
							<input type="hidden" id="ms_id1" name="ms_id"/>
							<input type="hidden" id="yyyy1"/>
							<input type="hidden" id="assign_id1" name="assign_id" />
							<input type="hidden" id="program_id1" name="program_id"/>
						<div class="content f_left" style="width:97%; margin:10px 1.5%;">
							
							<div class="table  f_left" style="width:100%; margin:0; ">
							
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<th colspan="2" style="width:16%;"><div>
													목적 및 내용
												</div></th>
											<td colspan="3">
											<div  class="cell">
												<div id="content"  style=" width:99.5%; height:150px; margin:3px 0; overflow:auto !important;"></div>
											 	
                             				</div></td>
										</tr>
										<tr>
											<th rowspan="3" style="width:8%;"><div>
													사업목표
												</div></th>
											<th rowspan="2" style="width:8%;">
												<div>
													정량목표
												</div>
											</th>
											<th style="width:8%;height:10px !important;">
												<div style="margin:0;">
													목표
												</div>
											</th>
											<th style="width:8%;height:10px !important;">
												<div style="margin:0;">
													단위
												</div>
											</th>
											<td rowspan="2">
												<div  class="cell">
													<textarea   id="quan_content" name="quan_content" class="textarea"  readonly style=" width:99.5%; height:65px; margin:3px 0;" ></textarea>
												</div>
											</td>
										</tr>
										<tr>
											<td style="width:8%;"><div  class="cell t_center">
											<span id="quan_goal"></span>
											</div></td>
											<td style="width:8%;"><div  class="cell t_center">
											<span id="quan_unit"></span>
											</div></td>
										</tr>
										<tr>
											<th><div>
													정성목표
												</div></th>
											<td colspan="3"><div  class="cell">
													<textarea   id="qual_goal" name="qual_goal" class="textarea"  readonly style=" width:99.5%; height:65px; margin:3px 0;" ></textarea>
												</div></td>
										</tr>
										<tr>
											<th colspan="2"><div>
													추진개요
												</div></th>
											<td colspan="3"><div  class="cell">
													<textarea   id="summary" name="summary" class="textarea"  readonly style=" width:99.5%; height:65px; margin:3px 0;" ></textarea>
												</div></td>
										</tr>
										<tr id="fileListBefore">
											<th  colspan="2"><div>
													기대효과
												</div></th>
											<td colspan="3"><div  class="cell">
													<textarea   id="effect" name="effect" class="textarea"  readonly style=" width:99.5%; height:65px; margin:3px 0;" ></textarea>
												</div></td>
										</tr>
										
									</tbody>
								</table>
								
							</div>
							<!--table-->
							
							<div class="group f_left  w100p m_t10">
								<div class="label type2 f_left">
									예산 및 집행내역
								</div>
							</div>
							<div class="grid f_left" style="width:100%; margin:5px 0%; height:351px;margin-bottom:10px;">
								<div id="budgetGrid">
								</div>
							</div>
							
						</div>
						</form>
						<!--content-->
					</div>
					<!--//tabs_content-->
					
					<!-- 운영탭 S -->
					
					<div  class="tabs_content" style=" height:100%; ">
					<form name="saveForm2" id="saveForm2" method="post" enctype="multipart/form-data">
						<input type="hidden" id="ms_id2" name="ms_id"/>
						<input type="hidden" id="yyyy2"/>
						<input type="hidden" id="assign_id2" name="assign_id" />
						<input type="hidden" id="program_id2" name="program_id"/>
						<div class="content f_left" style="width:97%; margin:10px 1.5%;">
							<div class="table  f_left" style="width:100%; margin:0; ">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr id="fileListBefore2">
											<th colspan="2" style="width:16%;"><div>
													추진성과
												</div></th>
											<td colspan="2"><div  class="cell">
												<div id="result" style=" width:99.5%; height:150px; margin:3px 0; overflow:auto !important;"></div>
                             				</div></td>
										</tr>
										
									</tbody>
								</table>
							</div>
							<!--table-->
							
							<div class="group f_left  w100p m_t10">
								<div class="label type2 f_left">
									예산 및 집행내역
								</div>
							</div>
							<!--group-->
							<div class="group f_left" style="width:100%; margin:10px 0;">
								<div class="label type2 f_left m_r5 none_bg_img">
									<span class="sublabel" style="font-weight:bold;">예산액(천원):</span>
									<input type="text" value="" id="budget_total2" readonly class="input type2 t_left"  style="width:150px; margin:3px 0; "/>
								</div>
								<div class="label type2 f_left m_r5 none_bg_img">
									<span class="sublabel" style="font-weight:bold;">집행액(천원):</span>
									<input type="text" value="" id="enforce_total2" readonly class="input type2 t_left"  style="width:150px; margin:3px 0; "/>
								</div>
								<div class="label type2 f_left m_r5 none_bg_img">
									<span class="sublabel" style="font-weight:bold;">잔액(천원):</span>
									<input type="text" value="" id="change_total2" readonly class="input type2 t_left"  style="width:150px; margin:3px 0; "/>
								</div>
							</div>
							<!--group-->
								<div class="tabs f_left" style=" width:100%; height:420px; margin-top:0px;margin-bottom:10px;">
								<div id='jqxTabs03'>
									<ul>
										<li style="margin-left: 0px;">예산 및 집행</li>
										<li>집행내역 상세</li>
									</ul>
									<div class="tabs_content" style="height:100%; ">
										<div class="content f_left" style="width:97%; margin:10px 1.5%;">
											<div class="grid f_left" style="width:100%; margin:5px 0%; height:351px;">
												<div id="planBudgetGrid">
												</div>
											</div>
											<!--grid -->
										</div>
									<!--content-->
									</div>
									<!--//tabs_content-->
									<div class="tabs_content" style="height:100%; ">
										<div class="content f_left" style="width:97%; margin:10px 1.5%;">
											<div class="grid f_left" style="width:100%; margin:5px 0%; height:351px;">
												<div id="planEnforceGrid">
												</div>
											</div>
											<!--grid -->
										</div>
									</div>
								</div>
							</div>
							
						</div>
						<!--content-->
					</form>
					</div>
					<!--//tabs_content-->
					
					<!-- 부서평가탭 -->
					<div  class="tabs_content" style=" height:100%; ">
					<form name="saveForm3" id="saveForm3">
						<input type="hidden" id="ms_id3" name="ms_id"/>
						<input type="hidden" id="yyyy3" name="yyyy"/>
						<input type="hidden" id="assign_id3" name="assign_id" />
						<input type="hidden" id="program_id3" name="program_id"/>
						<div class="content f_left" style="width:97%; margin:10px 1.5%;">
							<div class="table  f_left" style="width:100%; margin:0; ">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<th rowspan="5" width="8%"><div>
													사업목표
												</div></th>
											<th rowspan="3" width="8%"><div>
													정량
												</div></th>
											<th width="12.8%"><div>
													목표값
												</div></th>
											<th width="12.8%"><div>
													단위
												</div></th>
											<th width="12.8%"><div>
													달성값
												</div></th>
											<th width="12.8%"><div>
													달성률(%)
												</div></th>
											<th colspan="2" width="12.8%"><div>
													비고
												</div></th>
										</tr>
										<tr>
											<td><div  class="cell">
												<input type="text" class="input type2 t_center f_left" id="quan_goal3" readonly style="width:95%;border:none;background:transparent;"/>
												</div></td>
											<td><div  class="cell">
											<input type="text" class="input type2 t_center f_left" id="quan_unit3" readonly style="width:95%;border:none;background:transparent;"/>
												</div></td>
											<td><div  class="cell">
											<input type="text" class="input type2 t_center f_left" id="quan_ach_val" name="quan_ach_val" style="width:95%;" readonly/>
												</div></td>
											<td><div  class="cell">
											<input type="text" class="input type2 t_center f_left" id="quan_ach_rate" readonly name="quan_ach_rate" style="width:95%;border:none;background:transparent;"/>
												</div></td>
											<td colspan="2" ><div  class="cell">
											<input type="text" class="input type2 t_left f_left" id="quan_def3" name="quan_def" style="width:95%;" readonly/>
												</div></td>
										</tr>
										<tr>
											<td colspan="6"><div  class="cell">
													<textarea   id="quan_content3" name="quan_content" class="textarea"  style=" width:99.5%; height:45px; margin:3px 0;" readonly></textarea>
												</div></td>
										</tr>
										<tr>
											<th rowspan="2" width="8%"><div>
													정성
												</div></th>
											<th width="16%"><div>
													달성여부
												</div></th>
											<th colspan="5"><div>
													사유
												</div></th>
										</tr>
										<tr>
											<td><div  class="cell t_center">
													<span id="qual_ach_yn"></span>
												</div></td>
											<td colspan="5"><div  class="cell">
													<textarea   id="qual_content3" name="qual_content" class="textarea"  readonly style=" width:99.5%; height:45px; margin:3px 0;"  ></textarea>
												</div></td>
										</tr>
										<tr>
											<th rowspan="2" colspan="2" width="16%"><div>
													만족도(명)
												</div></th>
											<th  width="16%"><div>
													응답자
												</div></th>
											<th width="12.8%"><div>
													매우만족
												</div></th>
											<th width="12.8%"><div>
													만족
												</div></th>
											<th width="12.8%" ><div>
													보통
												</div></th>
											<th width="12.8%"><div>
													불만족
												</div></th>
											<th width="12.8%"><div>
													매우불만족
												</div></th>
										</tr>
										<tr>
											<td><div  class="cell t_center">
													<input type="hidden" id="cnt_tot" name="cnt_tot" />
													<span id="cnt_tot_txt"></span>
												</div></td>
											<td><div  class="cell t_center">
													<input type="text" class="input type2 t_center" id="cnt_a" name="cnt_a" style="width:85%;" readonly/>
												</div></td>
											<td><div  class="cell t_center">
												<input type="text" class="input type2 t_center" id="cnt_b" name="cnt_b" style="width:85%;" readonly/>
												</div></td>
											<td><div  class="cell t_center">
												<input type="text" class="input type2 t_center" id="cnt_c" name="cnt_c" style="width:85%;" readonly/>
												</div></td>
											<td><div  class="cell t_center">
												<input type="text" class="input type2 t_center" id="cnt_d" name="cnt_d" style="width:85%;" readonly/>
												</div></td>
											<td><div  class="cell t_center">
												<input type="text" class="input type2 t_center" id="cnt_e" name="cnt_e" style="width:85%;" readonly/>
												</div></td>
										</tr>
										<tr>
											<th rowspan="2" width="8%"><div>
													성과평가
												</div></th>
											<th rowspan="2" width="8%"><div>
													자체
												</div></th>
											<th width="16%"><div>
													점수
												</div></th>
											<th colspan="5"><div>
													평가결과
												</div></th>
										</tr>
										<tr>
											<td><div  class="cell t_center">
												<span id="eval_score"></span>
												</div></td>
											<td colspan="5"><div  class="cell">
													<textarea id="eval_result" name="eval_result" class="textarea" style=" width:99.5%; height:45px; margin:3px 0;"  readonly></textarea>
												</div></td>
										</tr>
										
										</tbody>
									
								</table>
							</div>
							<!--table-->
							
							<!--group_button-->
						</div>
						<!--//content-->
					</form>
					</div>
					<!--//tabs_content-->
				</div>
				<!--//jqxtabs-->
			</div>
			<!--tabs-->
			
		</div>
		<!--content-->
	</div>
	<!--//tabs_content-->
</div>
<!--//jqxTabs-->

<!--//wrap-->
</body>
</html>