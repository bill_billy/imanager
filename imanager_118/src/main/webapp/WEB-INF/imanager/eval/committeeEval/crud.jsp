<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>위원회평가</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" hrlef="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
    	<script type="text/javascript">// button
    	$(this).ready(function () {    
    		init();
        });
    	var userName = "${sessionScope.loginSessionInfo.userName}";
    	var userId = "${sessionScope.loginSessionInfo.userId}";
    	
    	
    	function init(){
    		$("#jqxButtonSearch").jqxButton({ width: '',  theme:'blueish'}).on('click',search); 
    		$("#detailProgram").jqxButton({ width: '',  theme:'blueish'}).on('click',popupProgram);
		
            
			$('#jqxTabs01').jqxTabs({ width: '100%', height: '100%', position: 'top', theme:'blueish', selectedItem: 0});
			
           
			makeMissionCombo();
			//makeEvalCombo();
			$("#loginUser").append("<span>"+decodeURI(decodeURIComponent(userName))+"</span>");

			search();
    		
    	}
    	var missionList = [];
    	var missionDat;
		function makeMissionCombo(){
			missionDat = $i.sqlhouse(322,{});
			missionDat.done(function(res){
				missionList = res.returnArray;
				
				missionList.forEach(function(dataOne) {
    				dataOne.MS_NM = $i.secure.scriptToText(dataOne.MS_NM);
    			});
				
        		var source = 
        		{
        			datatype:"json",
        			datafields:[
        				{ name : "MS_ID" },
        				{ name : "MS_NM" }
        			],
        			id:"id",
        			localdata:res,
        			async : false
        		};
        		var dataAdapter = new $.jqx.dataAdapter(source);
        		 $("#jqxCombobox01").jqxComboBox({ 
        			//	selectedIndex: 0, 
        				source: dataAdapter, 
        				animationType: 'fade', 
        				dropDownHorizontalAlignment: 'right', 
        				displayMember: "MS_NM", 
        				valueMember: "MS_ID", 
        				dropDownWidth: 300, 
        				dropDownHeight: 100, 
        				width: 300, 
        				height: 22,  
        				theme:'blueish'});
        		 
        		$('#jqxCombobox01').on('select', function (event) 
        		{
        			var args = event.args;
        			if (args) {                  
        				var index = args.index;
        				makeYearCombo(index);
        				
        			}	     
        		}); 
        		if("${param.ms_id}"=='')
         			$("#jqxCombobox01").jqxComboBox('selectIndex', 0 );  
         		else
         			$("#jqxCombobox01").jqxComboBox('selectItem', "${param.ms_id}" ); 
        		
        	});  
     	}//makeMissionCombo
		
		function makeYearCombo(idx){
     		var start = missionList[idx].START_YYYY;
     		var end = missionList[idx].END_YYYY;
     		
     		var nowYear = new Date().getFullYear();
     		if(nowYear >= start && nowYear < end) //현재기준으로 설정
     			end = nowYear;
     		
     		var yyyyList = [];
     		for(var i=Number(start);i<=end;i++){
     			yyyyList.push(i);
     		}
     		$("#jqxCombobox02").jqxComboBox({ 
				source: yyyyList, 
				animationType: 'fade', 
				dropDownHorizontalAlignment: 'right', 
				dropDownWidth: 80, 
				dropDownHeight: 80, 
				width: 80, 
				height: 22,  
				theme:'blueish'});
     		if("${param.yyyy}"=='')
     			$("#jqxCombobox02").jqxComboBox('selectIndex', yyyyList.length-1 );  
     		else
     			$("#jqxCombobox02").jqxComboBox('selectItem', "${param.yyyy}" );  
        }
     	
     	function makeEvalCombo(){
     		missionDat.done(function(res){
     			var dat = $i.sqlhouse(387,{MS_ID:$("#jqxCombobox01").val(),YYYY:$("#jqxCombobox02").val()});
     			dat.done(function(res){
     				var comboList = res.returnArray;
            		var source = 
            		{
            			datatype:"json",
            			datafields:[
            				{ name : "EMP_ID" },
            				{ name : "EMP_NM" }
            			],
            			localdata:comboList,
            			async : false
            		};
            		var dataAdapter = new $.jqx.dataAdapter(source);
            		 $("#jqxCombobox03").jqxComboBox({ 
            			selectedIndex: 0, 
            			source: dataAdapter, 
            			animationType: 'fade', 
            			dropDownHorizontalAlignment: 'right', 
            			displayMember: "EMP_NM", 
            			valueMember: "EMP_ID", 
            			dropDownWidth: 80, 
            			dropDownHeight: 80, 
            			width: 80, 
            			height: 22,  
            			theme:'blueish'});
     			});
     			
     		});
     		
     	}
     	
     	
		function makeStraListTree(){
			//tree초기화
			var div = $("#jqxTree01").parent();
			 $("#jqxTree01").jqxTree('destroy');
			 div.append("<div id='jqxTree01' class='tree'/>");
			 
    		var dat = $i.sqlhouse(432, {MS_ID:$("#ms_id").val(),YYYY:$("#yyyy").val(),EMP_ID:userId});
    		dat.done(function(res){
    			
    			res = res.returnArray;
    			res.forEach(function(dataOne) {
    				dataOne.STRA_NM = $i.secure.scriptToText(dataOne.STRA_NM);
    			});
    			var source =
				{
	                datatype: "json",
	                datafields: [
	                    { name: 'P_ID'},
	                    { name: 'C_ID'},
	                    { name: 'STRA_NM'},
	                    { name: 'STRA_ID'},
	                    { name: 'SUBSTRA_ID'}
	                ],
	                id : 'C_ID',
	                localdata: res
	            };
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            dataAdapter.dataBind();
	            
	            var records = dataAdapter.getRecordsHierarchy('C_ID', 'P_ID', 'items', [{ name: 'C_ID', map: 'id'} ,{ name: 'STRA_NM', map: 'label'}, { name:'STRA_ID', map:'value'}]);
	            $('#jqxTree01').jqxTree({source: records, height: '100%', width: '100%', theme:'blueish',allowDrag: false, allowDrop: false });
	            
				// tree init
	            var items = $('#jqxTree01').jqxTree('getItems');
	            var item = null;
	            var size = 0;
	            var img = '';
	            var label = '';
	            var afterLabel = '';
	            
				for(var i = 0; i < items.length; i++) {
					item = items[i];
					
					if(i == 0) {
						//root
						$("#jqxTree01").jqxTree('expandItem', item);
						img = 'icon-foldernoopen.png';
						afterLabel = "";
					} else {
						//children
						size = $(item.element).find('li').size();
						
						if(size > 0) {
							//have a child
							var itemId = item.id;
							if(itemId.indexOf("ASSIGN")==-1){
								img = 'icon-foldernoopen.png';
								if(itemId.indexOf("SUB")==-1){
									afterLabel = "";
								}else{
									afterLabel = "<span style='color: Blue;'> (" + size + ")</span>";
								}
							}
						} else {
							var itemId = item.id;
							if(itemId.indexOf("ASSIGN")==-1){
								img = 'icon-foldernoopen.png';
							}else{
								img = 'icon-page.png';
							}
							//no have a child
							//내부에서 사용하는 트리는 팝업 없음.
							//afterLabel = "<span style='color: Blue; margin-left:5px; vertical-align:middle;'> <img src='${WWW.CSS}/images/img_icons/popupIcon.png' alt='팝업창' title='새창보기'/></span>";
							afterLabel = "";
						}
					}
					
					label = "<img style='float: left; margin-right: 5px;' src='../../resources/cmresource/image/" + img + "'/><span item-title='truec style='vertical-align:middle;'>" + $i.secure.scriptToText(item.label) + "</span>" + afterLabel;
					$('#jqxTree01').jqxTree('updateItem', item, { label: label});
				}
	            
	            //add event
	            $('#jqxTree01')
	            .on("expand", function(eve) {
	            	var args = eve.args;
	            	var label = $('#jqxTree01').jqxTree('getItem', args.element).label.replace('icon-foldernoopen', 'icon-folderopen');
	            	args.element.firstChild.className = args.element.firstChild.className.replace('jqx-tree-item-arrow-collapse', '').replace('jqx-icon-arrow-right', '');
	            	$('#jqxTree01').jqxTree('updateItem', args.element, { label: label});
	            })
	            .on("collapse", function(eve) {
	            	var args = eve.args;
	            	var label = $('#jqxTree01').jqxTree('getItem', args.element).label.replace('icon-folderopen', 'icon-foldernoopen');
	            	$('#jqxTree01').jqxTree('updateItem', args.element, { label: label});
	            })       
	            .on("select", function(eve) {
	            	var args = eve.args;
	            	console.log(args);
	            	var pid = $("#jqxTree01").jqxTree("getItem", args.element).parentId;
	            	var cid = $("#jqxTree01").jqxTree("getItem", args.element).id;
	            	//var pValue = $("#jqxTree01").jqxTree("getItem", args.parentElement).value;
	            	var value = $("#jqxTree01").jqxTree("getItem", args.element).value;
	            
	            	$("#txtTabSubstr").val(cid);
	            	if(pid==0){//전략
	            		console.log('전략선택');
	            	}else{
	            		if(isNaN(pid)){//추진
	            			//var ppValue = $("#jqxTree01").jqxTree("getItem", args.parentElement).value;
	            			console.log('추진과제선택');
	            			var idx = pid.indexOf('SUB');
	            			var id1=pid.substr(0,idx);
	            			var id2=pid.substr(parseInt(idx)+3);
	            			getTitle(id1,id2);
	            			makeAssignGrid(2,value);
	            		
	            		}else{//세부
	            			console.log('세부전략선택');
	            			var idx = cid.indexOf('SUB')+3;
	            			getTitle(pid,cid.substr(idx));
	            			makeAssignGrid(1,pid,value);
	            			
	            		}
	            	}
	            });
    		});
    	}//makeStrategyTree
        
    	function getTitle(straid,substraid){
    		var dat = $i.sqlhouse(408, {MS_ID:$("#ms_id").val(),STRA_ID:straid,SUBSTRA_ID:substraid});
    		dat.done(function(res){
    			res = res.returnArray[0];
    			
    			var html = $i.secure.scriptToText(res.STRA_NM) + '<span class="label sublabel type2">'+$i.secure.scriptToText(res.SUBSTRA_CD+' '+res.SUBSTRA_NM)+'</span>'
    			$("#selectStraNm").html(html);
    		});
    	}
		
       	function search(){
       		
       		$("#selectStraNm").html("전략목표");
       		missionDat.done(function(){
       			$("#ms_id").val($("#jqxCombobox01").val());
           		$("#yyyy").val($("#jqxCombobox02").val());
           		//좌측 TAB영역
           		makeStraListTree();
    			
    			//우측TAB영역
           		makeAssignGrid();
       		});
       	}
       	
       	var assignData;
        function makeAssignGrid(type,keyword,keyword2){
        	resetForm0();
        	var div = $("#assignGrid").parent();
			 $("#assignGrid").jqxGrid('destroy');
			 div.append("<div id='assignGrid'/>");
        	
        	var ms_id= $("#ms_id").val();
            var yyyy = $("#yyyy").val();
            
            if(type==1)//substra
            	assignData = $i.sqlhouse(431,{MS_ID:ms_id,YYYY:yyyy,STRA_ID:keyword,SUBSTRA_ID:keyword2,EMP_ID:userId});
            else 
            	assignData = $i.sqlhouse(431,{MS_ID:ms_id,YYYY:yyyy,STRA_ID:'',SUBSTRA_ID:'',EMP_ID:userId});
            assignData.done(function(res){
    			var data = res.returnArray;
                console.log(data);

                // prepare the data
                var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'MS_ID', type: 'string' },
                        { name: 'STRA_ID', type: 'string' },
                        { name: 'SUBSTRA_ID', type: 'string' },
                        { name: 'ASSIGN_ID', type: 'string' },
                        { name: 'ASSIGN_CD', type: 'string' },
    					{ name: 'ASSIGN_NM', type: 'string'},
    					{ name: 'DEPT_NM', type: 'string' },
    					{ name: 'SUBDEPT_NM', type: 'string' },
    					{ name: 'EVAL_STATUS', type: 'string' },
    					{ name: 'EMP_ID', type: 'string' },
    					{ name: 'EMP_NM', type: 'string' }
                    ],
                    localdata: data,
                    updaterow: function (rowid, rowdata, commit) {
                        commit(true);
                    }
                };
    			
    			var noScript = function (row, columnfield, value) {//left정렬
    				var newValue = $i.secure.scriptToText(value);
                     return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
    							
                } 
    				  

    			var goDetail = function (row, columnfield, value, rowData) {
					var assign_id = $("#assignGrid").jqxGrid('getrowdata',row).ASSIGN_ID;
                    var stra_id = $("#assignGrid").jqxGrid('getrowdata',row).STRA_ID;
                    var substra_id = $("#assignGrid").jqxGrid('getrowdata',row).SUBSTRA_ID;
					
                    return '<div id="userName-' + row + '"onclick="detailAssign('+row+','+stra_id+','+substra_id+','+assign_id+');" style="text-align:left; margin:4px 0px 0px 10px; cursor:pointer; text-decoration:underline;">' + $i.secure.scriptToText(value) + '</div>';

                }	     
                var dataAdapter = new $.jqx.dataAdapter(source, {

                    downloadComplete: function (data, status, xhr) { },

                    loadComplete: function (data) { },

                    loadError: function (xhr, status, error) { }

                });

                var dataAdapter = new $.jqx.dataAdapter(source);

                $("#assignGrid").jqxGrid(
                {
                  		width: '100%',
		    			height: '100%',
		    			altrows:true,
		    			pageable: false,
		    			pageSize: 10,
		    			pageSizeOptions: ['10', '20', '30'],
		    			source: dataAdapter,
		    			theme:'blueish',
		    			columnsheight:25 ,
		    			rowsheight: 25,
		    			sortable:true,
	                   columns: [
	                      { text: '추진과제코드', datafield: 'ASSIGN_CD', width: '15%', align:'center', cellsalign: 'center',cellsrenderer:noScript},
	                      { text: '추진과제명', datafield: 'ASSIGN_NM', width: '55%', align:'center', cellsalign: 'left' ,  cellsrenderer: goDetail },
	                      { text: '주관부서', datafield: 'DEPT_NM', width: '15%', align:'center', cellsalign: 'center' ,cellsrenderer:noScript},
	    				  { text: '평가상태', datafield: 'EVAL_STATUS', width: '15%', align:'center', cellsalign: 'center' ,cellsrenderer:noScript}
	                    ]
                });
    			//$("#assignGrid").jqxGrid('selectrow', 0);
    	
        	});
        	
        }
        var userAjax;
        function detailAssign(row,stra_id,substra_id,assign_id){
        	resetForm0();
        	$("#stra_id").val(stra_id);
        	$("#substra_id").val(substra_id);
        	$("#assign_id").val(assign_id);
        	
        	getEvalUser(assign_id);
        	var assignCd = $("#assignGrid").jqxGrid('getrowdata',row).ASSIGN_CD;
        	var assignNm = $("#assignGrid").jqxGrid('getrowdata',row).ASSIGN_NM;
        	$("#selectAssignNm").html($i.secure.scriptToText(assignCd+" "+assignNm));
        }
       
        function getEvalUser(assign_id){
        	var yyyy = $("#yyyy").val();
        	var ms_id = $("#ms_id").val();
        	userAjax = $i.sqlhouse(386,{MS_ID:ms_id,YYYY:yyyy,ASSIGN_ID:assign_id});
        	userAjax.done(function(res){
        		var arrlist = res.returnArray;
        		if(arrlist.length<=0){
        			$("#masterUser").html("");
        			$("#nomasterUser").html("");
        			$("#masterId").val("");
        		}else{
        			var masterY="",masterId="",masterN=[];
            		for(var i=0;i<arrlist.length;i++){
            			if(arrlist[i].MASTER_YN=="Y"){
            				masterY=arrlist[i].EMP_NM;
            				masterId=arrlist[i].EMP_ID;
            			}else if(arrlist[i].MASTER_YN=="N"){
            				masterN.push(arrlist[i].EMP_NM);
            			}
            		}
            		
            		$("#masterUser").html(masterY);
            		$("#masterId").val(masterId);
            		if(masterN.length==0)
            			$("#nomasterUser").html("");
            		else
            			$("#nomasterUser").html(masterN.join(","));
        		}
        		getAssignEval(assign_id);
            	getProgramEval(assign_id);
        		
        	});
        	
        }
        
        function getAssignEval(assign_id){
        	var yyyy = $("#yyyy").val();
        	var ms_id = $("#ms_id").val();
        	var masterUserid = $("#masterId").val();
        	var dat = $i.sqlhouse(428,{MS_ID:ms_id,YYYY:yyyy,ASSIGN_ID:assign_id});
    		dat.done(function(res){
    			var data = res.returnArray;
    			if(data.length>0){
    				$("#ac").val("update");
    				$("#good_content").val(data[0].GOOD_CONTENT);
    				$("#improve_content").val(data[0].IMPROVE_CONTENT);
    				
    				var result = data[0].EVAL_RESULT;
    			
    				$("input:radio[name=eval_result]:input[value='"+result+"']").prop("checked",true);
    				
    				var status = data[0].EVAL_STATUS;
    				
    				console.log(masterUserid);
    				console.log(userId);
    				if(masterUserid==userId){
	    				if(status=="C"){ //평가완료
	    					$("#hiddenBtn1").css("display","block");
	    					$("#hiddenBtn2").css("display","none");
	    					$("#hiddenBtn3").css("display","none");
	    				}else if(status=="B"){ //평가중
	    					$("#hiddenBtn1").css("display","none");
	    					$("#hiddenBtn2").css("display","block");
	    					$("#hiddenBtn3").css("display","block");
	    				}else{
	    					$("#hiddenBtn1").css("display","none");
	    					$("#hiddenBtn2").css("display","none");
	    					$("#hiddenBtn3").css("display","block");
	    				}
	    				$("input:radio[name=eval_result]").removeAttr("disabled");
    				}else{
    					$("#hiddenBtn1").css("display","none");
    					$("#hiddenBtn2").css("display","none");
    					$("#hiddenBtn3").css("display","none");
    					
    					$("input:radio[name=eval_result]").attr("disabled");
    				}
    				
    			}else{
    				console.log(masterUserid);
    				console.log(userId);
    				if(masterUserid==userId){
	    				$("#hiddenBtn1").css("display","none");
	    				$("#hiddenBtn2").css("display","none");
	    				$("#hiddenBtn3").css("display","block");
	    				
    				}else{
    					$("#hiddenBtn1").css("display","none");
    					$("#hiddenBtn2").css("display","none");
    					$("#hiddenBtn3").css("display","none");
    				}
    				
    			}
    		});
        }
        function getProgramEval(assign_id){
        	var yyyy = $("#yyyy").val();
        	var ms_id = $("#ms_id").val();
        	
        	var dat = $i.sqlhouse(429,{MS_ID:ms_id,YYYY:yyyy,ASSIGN_ID:assign_id});
    		dat.done(function(res){
    			var data = res.returnArray;
    			console.log(data);
    			$("#tbodyProgram").empty();
    			if(data.length>0){
    				for(var i=0;i<data.length;i++){
    					var html = "";
	    				html += '<tr name="inputEvalTr">';
	    				html += '<td style="width:15%;"><div class="cell t_center"><span>';
	    				html += $i.secure.scriptToText(data[i].PROGRAM_NM);
	    				html += '</span><input type="hidden" name="program_id" value="'+data[i].P_PROGRAM_ID+'"/></div></td>';
	    				html += '<td style="width:25%;"><div class="cell">';
	    				html += '<textarea class="textarea" name="p_good_content" style=" width:99.5%; height:50px; margin:3px 0;overflow:auto;" placeholder="varChar2 2000자">'
	    				html += data[i].GOOD_CONTENT
	    				html += '</textarea>';
	    				//html += '<input type="text" value="'+data[i].GOOD_CONTENT+'" id="" name="p_good_content" class="input type2"  style="width:97%; margin:3px 0; "/>';
	    				html += '</div></td>';
	    				html += '<td style="width:25%;"><div class="cell">';
	    				html += '<textarea class="textarea" name="p_improve_content" style=" width:99.5%; height:50px; margin:3px 0;overflow:auto;" placeholder="varChar2 2000자">'
		    			html += data[i].IMPROVE_CONTENT
		    			html += '</textarea>';
	    				//html += '<input type="text" value="'+data[i].IMPROVE_CONTENT+'" id="" name="p_improve_content" class="input type2"  style="width:97%; margin:3px 0; "/>';
	    				html += '</div></td>';
	    				html += '<td style="width:25%;"><div class="cell">';
	    				html += '<textarea class="textarea" name="p_best_content" style=" width:99.5%; height:50px; margin:3px 0;overflow:auto;" placeholder="varChar2 2000자">'
		    			html += data[i].BEST_CONTENT
		    			html += '</textarea>';
	    				//html += '<input type="text" value="'+data[i].BEST_CONTENT+'" id="" name="p_best_content" class="input type2"  style="width:97%; margin:3px 0; "/>';
	    				html += '</div></td>';
	    				html += '<td style="width:10%;"><div class="cell t_center">';
	    				html += '<select class="scoreList" id="p_eval_score'+i+'" name="p_eval_score">';
	    				html += '<option value="0" selected>선택</option>';
	    				html += '<option value="5">5점</option>';
	    				html += '<option value="4">4점</option>';
	    				html += '<option value="3">3점</option>';
	    				html += '<option value="2">2점</option>';
	    				html += '<option value="1">1점</option>';
	    				html += '</select>';
	    				html += '</div></td>';
	    				html +='</tr>';
	    				$("#tbodyProgram").append(html);
	    				if(data[i].EVAL_SCORE!="")
	    					$("#p_eval_score"+i).val(data[i].EVAL_SCORE);
    				}
    				
    				$(".scoreList").jqxDropDownList({animationType: 'fade',dropDownHorizontalAlignment: 'right',dropDownVerticalAlignment: 'top',dropDownWidth: 90,dropDownHeight:100,width: 90,height: 23,theme:'blueish'});
    				
    				
    			}
    		});
        }
        
        
        function popupProgram(){ 
        	var ms_id = $("#ms_id").val();
        	var yyyy = $("#yyyy").val();
        	var stra_id = $("#stra_id").val();
        	var substra_id = $("#substra_id").val();
        	var assign_id = $("#assign_id").val();
        	if(assign_id==""){
        		$i.dialog.alert('SYSTEM','추진과제를 선택하세요');
        		return;
        	}
    		window.open('./popupProgram?ms_id='+ms_id+'&yyyy='+yyyy+'&stra_id='+stra_id+'&substra_id='+substra_id+'&assign_id='+assign_id, 'popupProgram','scrollbars=no, resizable=no, width=1200, height=800');  
    	}
		
        function saveForm0(){
        	var assign_id = $("#assign_id").val();
        	if(assign_id==""){
        		$i.dialog.alert('SYSTEM','추진과제를 선택하세요');
        		return;
        	}
        	
        	
        	if($("#good_content").val()==""){
        		$i.dialog.alert('SYSTEM','평가내용을 입력하세요');
        		return;
        	}
			if($("#improve_content").val()==""){
				$i.dialog.alert('SYSTEM','평가내용을 입력하세요');
        		return;
        	}
			if($("input:radio[name=eval_result]:checked").val()==undefined){
				$i.dialog.alert('SYSTEM','평가결과를 선택하세요');
        		return;
			}
        	var scoreLen = $("select[name=p_eval_score]").length;
        	for(var i=0;i<scoreLen;i++){
        		if($("select[name=p_eval_score]").eq(i).val()=="0"){
        			$i.dialog.alert('SYSTEM','평가점수를 선택하세요');
        			return;
        		}
        	}
        	
        	
        	
        	$i.insert("#saveForm").done(function(args){
    			if(args.returnCode == "EXCEPTION"){
    				$i.dialog.error("SYSTEM","저장 중 오류가 발생했습니다."+args.returnMessage);
    			}else{
    				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
    					var stra_id = $("#stra_id").val();
    		        	var substra_id = $("#substra_id").val();
    					//search(stra_id,substra_id);
    					makeAssignGrid(1,stra_id,substra_id);
    				});
    				
    			}
    		}).fail(function(e){ 
    			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
    		});
        	
        }   
        function resetForm0(){
        	$("#selectAssignNm").html("평가내용");
        	$("#ac").val("insert");
        	$("#assign_id").val("");
        	$("#good_content").val("");
        	$("#improve_content").val("");
        	$("input:radio[name='eval_result']").prop("checked",false);
        	$("#tbodyProgram").empty();
        	
        	$("#substra_id").val("");
        	$("#stra_id").val("");
        	
        	$("#masterUser").html("");
			$("#nomasterUser").html("");
			$("#masterId").val("");
        	
        	$("#hiddenBtn1").css("display","none");
			$("#hiddenBtn2").css("display","none");
			$("#hiddenBtn3").css("display","none");
        }
       
        function confirmEval(){
        	var assign_id = $("#assign_id").val();
        	if(assign_id==""){
        		$i.dialog.alert('SYSTEM','추진과제를 선택하세요');
        		return;
        	}
        	
        	var ms_id = $("#ms_id").val();
        	var yyyy = $("#yyyy").val();
        	
        	var dat = $i.post("./confirmEval",{ms_id:ms_id,assign_id:assign_id,yyyy:yyyy,status:"confirm"});
        	dat.done(function(args){
        		if(args.returnCode == "EXCEPTION"){
    				$i.dialog.error("SYSTEM","오류가 발생했습니다."+args.returnMessage);
    			}else{
    				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
    					search();
    				});
    			}
        	});
        	
        }
        function cancleEval(){
        	var assign_id = $("#assign_id").val();
        	if(assign_id==""){
        		$i.dialog.alert('SYSTEM','추진과제를 선택하세요');
        		return;
        	}
        	var ms_id = $("#ms_id").val();
        	var yyyy = $("#yyyy").val();
        	
        	var dat = $i.post("./confirmEval",{ms_id:ms_id,assign_id:assign_id,yyyy:yyyy,status:"cancle"});
        	dat.done(function(args){
        		if(args.returnCode == "EXCEPTION"){
    				$i.dialog.error("SYSTEM","오류가 발생했습니다."+args.returnMessage);
    			}else{
    				$i.dialog.alert("SYSTEM",args.returnMessage,function(){
    					search();
    				});
    			}
        	});
        	
        }
    
    </script>
    </head>
    <body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
<div class="wrap" style="width:98%; min-width:1580px; margin:0 1%;">
		<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
		<div class="label type1 f_left">
				비젼:
			</div>
		<div class="combobox f_left"  id='jqxCombobox01' >
			</div>
		<div class="label type1 f_left">
				년도:
			</div>
		<div class="combobox f_left"  id='jqxCombobox02' >
			</div>
		<div class="label type1 f_left" id="loginUser">
				평가자:
			</div>
		<div class="combobox f_left"  id='jqxCombobox03' >
			</div>
		<div class="group_button f_right">
				<div class="button type1 f_left">
				<input type="button" value="조회" id='jqxButtonSearch' width="100%" height="100%" />
			</div>
			</div>
		<!--group_button-->
	</div>
		<!--//header-->
		<div class="container  f_left" style="width:100%;">
		<div class="content f_left" style=" width:20%; margin-right:1%;">
				<div class="tabs f_left" style=" width:100%; height:685px; margin-top:0px;">
				<div id='jqxTabs01'>
						<ul>
						<li style="margin-left: 0px;">전략기준</li>
						
					</ul>
						<div class="tabs_content" style="height:100%; ">
						<input type="hidden" id="txtTabSubstr" name="tabSubstr" />
						<div  class="tree" style="border:none;"  id='jqxTree01'>
								
							</div>
						<!--//jqxTree-->
					</div>
						<!--//tabs_content-->
						
					</div>
				<!--//jqxTabs-->
			</div>
				<!--//tabs-->
			</div>
		<!--//content-->
		
		<div class="content f_right" style=" width:79%;">
			<form name="saveForm" id="saveForm">
			<input type="hidden" id="ac" name="ac" value="insert"/>
			<input type="hidden" id="ms_id" name="ms_id"/>
			<input type="hidden" id="yyyy" name="yyyy"/>
			<input type="hidden" id="stra_id"/>
			<input type="hidden" id="substra_id"/>
			<input type="hidden" id="assign_id" name="assign_id"/>
			<input type="hidden" id="emp_id" name="emp_id" value="iplan_pcm"/>
			<input type="hidden" id="eval_status" name="eval_status" value="B"/>
				<div class="f_right">
					<div class="label type4 f_left" style="width:auto;margin-right:30px;">
						평가책임자:<span id="masterUser" style="margin-left:3px;"></span><input type="hidden" id="masterId"/>
					</div>
					<div class="label type4 f_left" style="width:auto;">
						평가자:<span id="nomasterUser" style="margin-left:3px;"></span>
					</div>
				</div>
				
				<div class="group f_left  w100p m_t10">
					<div class="label type2 f_left" id="selectStraNm">
						전략목표
					</div>
				</div>
				<!--group-->
		
				<div class="grid f_left" style="width:100%; margin:5px 0%; height:151px;">
					<div id="assignGrid">
					</div>
				</div>
				<!--grid -->
				<div class="group f_left  w100p m_t10">
				<div class="label type2 f_left" id="selectAssignNm">
					평가내용
				</div>
			</div>
			<!--group-->
			<div class="blueish table f_left"  style="width:100%;  margin:5px 0;">
				<table summary="입력" style="width:100%;">
					<tr>
						<th style="width:20%;">
							<div class="cell">잘된 점
							</div></th>
						<td><div class="cell"><textarea   id="good_content" name="good_content" class="textarea"  value="" style=" width:99.5%; height:57px; margin:3px 0;overflow:auto;"  placeholder="Varchar2 2000자"></textarea>
						</div></td>
													
					</tr>
					<tr>
						<th style="width:20%;">
							<div class="cell">권고 및 보완사항
							</div></th>
						<td><div class="cell"><textarea   id="improve_content" name="improve_content" class="textarea"  value="" style=" width:99.5%; height:57px; margin:3px 0;overflow:auto;"  placeholder="Varchar2 2000자"></textarea>
						</div></td>
													
					</tr>
					<tr>
						<th><div class="cell">평가결과</div></th>
						<td><div class="cell t_center"><label for="" >
								<input type="radio" name="eval_result" value="5" class="m_b2 m_r3">
								매우만족</label>
							<label for=""  class="">
								<input type="radio" name="eval_result" value="4" class="m_b2 m_r3">
								만족</label>
							<label for=""  class="">
								<input type="radio" name="eval_result" value="3" class="m_b2 m_r3">
								보통</label>
							<label for=""  class="">
								<input type="radio" name="eval_result" value="2" class="m_b2 m_r3">
								불만족</label>	
							<label for=""  class="">
								<input type="radio" name="eval_result" value="1" class="m_b2 m_r3">
								매우불만족</label>
							</div></td>
					</tr>
					</table>
				</div>
								<!--table -->
			<div class="group f_left  w100p m_t10">
				<div class="label type2 f_left">
					프로그램
				</div>
				
				<div class="group_button f_right">
					
					<div class="button type3 f_left" style="display:none;" id="hiddenBtn1">
						<input type="button" value="평가취소" id='cancleBtn' width="100%" height="100%" onclick="cancleEval();"/>
					</div>
					<div class="button type3 f_left" style="display:none;" id="hiddenBtn2">
						<input type="button" value="평가완료" id='confirmBtn' width="100%" height="100%" onclick="confirmEval();"/>
					</div>
					<div class="button type2 f_left" id="hiddenBtn3">
						<input type="button" value="저장" id='saveBtn' width="100%" height="100%" onclick="saveForm0();"/>
					</div>
					<div class="button type2 f_left">
						<input type="button" value="상세현황" id='detailProgram' style="width:100%; height:26px;" />
					</div>
				</div>
				
				
			</div>
			<!--group-->
			<div class="blueish datatable f_left" style="width:100%; height:219px; margin:5px 0; overflow:hidden;">
								<!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 헤더가 50px일경우, 300px - 50px= 250px)-->
								<div class="datatable_fixed" style="width:100%; height:219px; float:left; padding:25px 0;">
										<!--group으로 헤더높이가 길어질 경우 padding  (기본높이는 25px)줘야 함-->
										<div style=" height:194px !important; overflow-x:hidden;">
										<table summary="가산점" style="width:100%;" class="none_hover">
												<thead style="width:100%;">
												<tr>
														<th  scope="col" style="width:15%;">프로그램</th>
														<th  scope="col" style="width:25%;">잘된 점</th>
														<th scope="col" style="width:25%;">권고 및 보완사항</th>
														<th scope="col" style="width:25%;">우수사례</th>
														<th scope="col" style="width:10%;">평가</th>
														<th style="width:17px; min-width:17px; border-bottom:0;"></th>
													</tr>
											</thead>
											<tbody id="tbodyProgram">
											</tbody>
											</table>
									</div>
									</div>
								<!--datatable_fixed-->
							</div>
								<!--datatable -->
							
						
			</form>
		</div><!--content-->
		</div><!--//container-->
	</div>
<!--//wrap-->
</body>
</html>