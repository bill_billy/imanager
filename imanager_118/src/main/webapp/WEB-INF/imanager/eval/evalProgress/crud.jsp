<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	   <title id='Description'>평가진행현황</title>
	   		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
			<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
			<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
	        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
	        <script src="../../resources/cmresource/js/iplanbiz/widget/Form.js"></script>
	        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	        <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	        <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
        	<script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
	   			  <style>
	 
	 				
				  .blueish .datatable .datatable_fixed table tbody tr:nth-child(odd):hover td:not(.common):not(.red){
				  	color:#223648 !important;
				  	border-color:#e1e1e1  !important;
				  	background:#ffffff !important;
				  }
				
				  .blueish .datatable .datatable_fixed table tbody tr:nth-child(even):hover td:not(.common):not(.red){
				  	color:#223648 !important;
				  	border-color:#e1e1e1  !important;
				  	background:#f0f5f9 !important;
				  }
				  
				  .blueish .datatable .datatable_fixed table tbody tr:nth-child(odd):hover td.red{
				  	color:red !important;
				  	border-color:#e1e1e1  !important;
				  	background:#ffffff !important;
				  }
				
				  .blueish .datatable .datatable_fixed table tbody tr:nth-child(even):hover td.red{
				  	color:red !important;
				  	border-color:#e1e1e1  !important;
				  	background:#f0f5f9 !important;
				  }
				  
				  
				  .blueish .datatable .datatable_fixed table tbody tr td.common{
				  	background:#ffffff !important;
				  
				  }
				  .blueish .datatable .datatable_fixed table tbody tr:hover td.common{ 
				  	color:#223648 !important;
				  	border-color:#e1e1e1  !important
				  }
				  
				  
				  .blueish .datatable .datatable_fixed table tbody tr td.common{
				  	background:#ffffff !important;
				  
				  }
				
				   
	  </style>
	   		<script type="text/javascript">
            $(this).ready(function () {               
				makeMissionCombo();
				makeStatCombo();
				
				var ajaxArr = [missionDat,statCombo];
				
				$.when.apply($, ajaxArr).then(function(res){
					makeCnt();
					makeList();
					
				}); 
			
				$("#BtnSrch").jqxButton({ width: '',  theme:'blueish'}).on('click',makeList); //조회 버튼1
				$("#BtnSrch").jqxButton({ width: '',  theme:'blueish'}).on('click',makeCnt); //조회 버튼 2

            });
	   		</script>
	   		
	   		<script type="text/javascript">  	
		    	var missionList = [];
		   		var missionDat;
			function makeMissionCombo(){
				missionDat = $i.sqlhouse(322,{}); 
					missionDat.done(function(res){
						missionList = res.returnArray;
						missionList.forEach(function(dataOne) {
		    				dataOne.MS_NM = $i.secure.scriptToText(dataOne.MS_NM);
		    			});
        				var source = 
        				{
		        			datatype:"json",
		        			datafields:[
		        				{ name : "MS_ID" },
		        				{ name : "MS_NM" }
		        			],
		        			id:"id",
		        			localdata:res,
		        			async : false 
		        		};
		        		var dataAdapter = new $.jqx.dataAdapter(source);
		        		 $("#getMission").jqxComboBox({ 
		        			//	selectedIndex: 0, 
		        				source: dataAdapter, 
		        				animationType: 'fade', 
		        				dropDownHorizontalAlignment: 'right', 
		        				displayMember: "MS_NM", 
		        				valueMember: "MS_ID",  
		        				dropDownWidth: 300, 
		        				dropDownHeight: 100, 
		        				width: 300, 
		        				height: 22,  
		        				theme:'blueish'} );
		        		 
		        		$('#getMission').on('select', function (event)  
		        		{
		        			var args = event.args;
		        			if (args) {
		        				var index = args.index;
		        				makeYearCombo(index);
		        			}	     
		        		}); 
		        		 
        			$("#getMission").jqxComboBox('selectIndex', 0 ); 
        			
        		});  
     		}//makeMissionCombo 
     	
     		function makeYearCombo(idx){
     			var start = missionList[idx].START_YYYY;
     			var end = missionList[idx].END_YYYY;
     			var nowYear = new Date().getFullYear();
     				if(nowYear >= start && nowYear < end) //현재기준으로 설정 
     					end = nowYear;
     			var yyyyList = [];
     				for(var i=Number(start);i<=end;i++){ 
     					yyyyList.push(i);
     				}
	     			$("#getYY").jqxComboBox({  
						source: yyyyList, 
						animationType: 'fade', 
						dropDownHorizontalAlignment: 'right', 
						dropDownWidth: 80, 
						dropDownHeight: 80, 
						width: 80, 
						height: 22,  
						theme:'blueish'});
     			
     			$("#getYY").jqxComboBox('selectIndex', yyyyList.length-1 );  
        	}//makeYearCombo
        	
        	var statCombo;
        	function makeStatCombo(){
        		statCombo = $i.sqlhouse(399,{COML_COD:"EVAL_STATUS"});     
        		statCombo.done(function(res){  
    				console.log(res);
    			 
    				var dat=res.returnArray; 
    				dat.unshift({COM_COD:"Z",COM_NM:"전체"});    				
    				 var source =
                 	{
	                     datatype: "json",
	                     datafields: [
	                         { name: 'COML_COD' }, 
	                         { name: 'COM_COD' },
	                         { name: 'COM_NM' }
	                     ],
	                     id: 'id', 
	 					localdata:dat,
	//                     url: url,
	                     async: false
	                 };
                 	var dataAdapter = new $.jqx.dataAdapter(source);
                 	$("#getStat").jqxComboBox({ 
		 				selectedIndex: 0,  
		 				source: dataAdapter, 
		 				animationType: 'fade', 
		 				dropDownHorizontalAlignment: 'right', 
		 				displayMember: "COM_NM",  
		 				valueMember: "COM_COD",        
		 				dropDownWidth: 100,  
		 				dropDownHeight: 100,     
		 				width: 100,  
		 				height: 22,  
		 				theme:'blueish'});
	    			});
    				
        			$("#getStat").jqxComboBox('selectIndex', 0 ); 
        		}//makeStatCombo
        </script><!-- ComboBox -->
	         
	     <script type="text/javascript">
	    	function makeCnt(){
	    		var data = $i.sqlhouse(333,{ MS_ID:$('#getMission').val(), YYYY:$('#getYY').val() });  
				$("#cntTable").empty();
				data.done(function(res){      
					res = res.returnArray;
					for(i=0; i<res.length; i++){
					console.log(res);
					var tableList ="";  
						tableList +="<tr>";
						tableList += "<td align='center' name='statA' class='td_side_left' style='width:25%;'>"+res[i].STATUS_A+"</td>";   
						tableList += "<td align='center' name='statB'class='borderNone tr_hover odd' style='width:25%;'>"+res[i].STATUS_B+"</td>";
						tableList += "<td align='center' name='statC' class='borderNone tr_hover odd' style='width:25%;'>"+res[i].STATUS_C+"</td>";
						tableList += "<td align='center' name='statALL' class='borderNone tr_hover odd' style='width:80%;'>"+res[i].STATUS_ALL+"</td>";
						tableList += "</tr>"; 
					} 
					$("#cntTable").append(tableList);    
				}); 
	     	 }//makeCnt      
	    	 
	        function makeList(){   
				var data = $i.sqlhouse(332,{ MS_ID:$('#getMission').val(), YYYY:$('#getYY').val(), EVAL_STATUS:$('#getStat').val() });
				$("#statusTable").empty();
				data.done(function(res){     
					res = res.returnArray; 
				for(var i=0;i<res.length;i++){ 
						var msId = $("#getMission").val();
						var YYYY = $("getYY").val();
						var assignId = res[i].ASSIGN_ID;
					var tableList ="";  
					if(i==0 || res[i].SUBSTRA_CD != res[i-1].SUBSTRA_CD){
						tableList +="<tr>";
						tableList += "<td align='center' name='straNm"+res[i].SUBSTRA_CD+"' class='td_side_left common' style='width:12%;'>"+res[i].STRA_NM+"</td>";   
						tableList += "<td align='center' name='substraCd"+res[i].SUBSTRA_CD+"'class='borderNone tr_hover odd common' style='width:7%;'>"+res[i].SUBSTRA_CD+"</td>"; 
						tableList += "<td align='center' name='substraNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd common' style='width:15%;'>"+res[i].SUBSTRA_NM+"</td>";
						
 						if(i%2==0){    
							tableList += "<td align='center' name='assignCd"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].ASSIGN_CD+"</td>";   
							tableList += "<td align='left' name='assignNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover od ' style='padding:0 7px;width:37%; text-decoration:underline; cursor:pointer;' onclick='showAssign("+assignId+")'>"+res[i].ASSIGN_NM+"</td>";
							//tableList += "<td align='center' name='evalSt"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].EVAL_STATUS+"</td>";
							if(res[i].EVAL_STATUS == "미평가"){
								tableList += "<td align='center' name='evalSt"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd red' style='color:red; width:7%;'>"+res[i].EVAL_STATUS+"</td>";
							}else{
								tableList += "<td align='center' name='evalSt"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].EVAL_STATUS+"</td>";
							}
							tableList += "<td align='center' name='masterNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].MASTER_NM+"</td>";  
							tableList += "<td align='center' name='empNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:80%;'>"+res[i].EVAL_NM+"</td>";
						}else{ 
							tableList += "<td align='center' name='assignCd"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].ASSIGN_CD+"</td>";   
							tableList += "<td align='left' name='assignNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='padding:0 7px;width:37%; text-decoration:underline; cursor:pointer;' onclick='showAssign("+assignId+")'>"+res[i].ASSIGN_NM+"</td>";
//							tableList += "<td align='center' name='evalSt"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].EVAL_STATUS+"</td>";
							if(res[i].EVAL_STATUS == "미평가"){
								tableList += "<td align='center' name='evalSt"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd red' style='color:red; width:7%;'>"+res[i].EVAL_STATUS+"</td>";
							}else{ 
								tableList += "<td align='center' name='evalSt"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].EVAL_STATUS+"</td>";
							}
							tableList += "<td align='center' name='masterNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].MASTER_NM+"</td>";  
							tableList += "<td align='center' name='empNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:80%;'>"+res[i].EVAL_NM+"</td>";
						}
						tableList += "</tr>"; 
					}else if(res[i].SUBSTRA_CD == res[i-1].SUBSTRA_CD){   
						tableList +="<tr>";  
						if(i%2==0){
							tableList += "<td align='center' name='assignCd"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].ASSIGN_CD+"</td>";   
							tableList += "<td align='left' name='assignNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='padding:0 7px;width:37%; text-decoration:underline; cursor:pointer;' onclick='showAssign("+assignId+")'>"+res[i].ASSIGN_NM+"</td>";
							//tableList += "<td align='center' name='evalSt"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].EVAL_STATUS+"</td>";
							if(res[i].EVAL_STATUS == "미평가"){
								tableList += "<td align='center' name='evalSt"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd red' style='color:red; width:7%;'>"+res[i].EVAL_STATUS+"</td>";
							}else{
								tableList += "<td align='center' name='evalSt"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].EVAL_STATUS+"</td>";
							}
							tableList += "<td align='center' name='masterNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].MASTER_NM+"</td>";  
							tableList += "<td align='center' name='empNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:80%;'>"+res[i].EVAL_NM+"</td>";
						}else{ 
							tableList += "<td align='center' name='assignCd"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].ASSIGN_CD+"</td>";   
							tableList += "<td align='left' name='assignNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='padding:0 7px;width:37%; text-decoration:underline; cursor:pointer;' onclick='showAssign("+assignId+")'>"+res[i].ASSIGN_NM+"</td>";
		//					tableList += "<td align='center' name='evalSt"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].EVAL_STATUS+"</td>";
							if(res[i].EVAL_STATUS == "미평가"){
								tableList += "<td align='center' name='evalSt"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd red' style='color:red; width:7%;'>"+res[i].EVAL_STATUS+"</td>";
							}else{
								tableList += "<td align='center' name='evalSt"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].EVAL_STATUS+"</td>";
							}
							tableList += "<td align='center' name='masterNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].MASTER_NM+"</td>";  
							tableList += "<td align='center' name='empNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:80%;'>"+res[i].EVAL_NM+"</td>";
							} 
						tableList += "</tr>";
					}
						$("#statusTable").append(tableList); 
					} 
				 
				var rowspanCount = ""; 
				for(var j=0;j<res.length;j++){  
					rowspanCount = $("[name=assignNm"+res[j].SUBSTRA_CD+"]").length;
					//로우 병합 
					$("#statusTable").find($("td[name=straNm"+res[j].SUBSTRA_CD+"]")).attr("rowSpan", rowspanCount);	
					$("#statusTable").find($("td[name=substraCd"+res[j].SUBSTRA_CD+"]")).attr("rowSpan", rowspanCount);				
					$("#statusTable").find($("td[name=substraNm"+res[j].SUBSTRA_CD+"]")).attr("rowSpan", rowspanCount);				
				}  
			});
	      }//makeList 
	     
	      function showAssign(assign_id){  
	    		var ms_id = $("#getMission").val();
				var yyyy = $("#getYY").val();  
	     		var popUrl = "/imanager/evalResult/committeeEvalResult/detailCommitteeEvalResult?yyyy="+yyyy+"&ms_id="+ms_id+"&assign_id="+assign_id; 
	  	 		var popOption = "width=970, height=680, resizable=no, status=no;";    //팝업창 옵션 
	  			window.open(popUrl,"",popOption);
			}
	  </script>  

	</head> 
	<body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1580px; margin:0 1%;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<form id="srchFrm" name="srchFrm"> 
					<div class="label type1 f_left">
						비젼:
					</div>
					<div class="combobox f_left"  id='getMission' >  
					</div>
					<div class="label type1 f_left">
						년도:
					</div>
					<div class="combobox f_left"  id='getYY' > 
					</div>
					<div class="label type1 f_left">
						평가상태:
					</div> 
					<div class="combobox f_left"  id='getStat' > 
					</div>
					<div class="group_button f_right"> 
						<div class="button type1 f_left">
							<input type="button" value="조회" id='BtnSrch' width="100%" height="100%" />  
						</div>
					</div>
						<input type="hidden" id="msid" name="msId"/> 
						<input type="hidden" id="yyyy" name="yyyy"/>    
				</form> 
			</div>
				<div class="container  f_left" style="width:100%;">
					<div class="content f_left" style=" width:100%; margin-right:1%;"> 
						<div class="blueish datatable f_left" style="width:100%; height:50px; margin:5px 0; overflow:hidden;">      
								<!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 헤더가 50px일경우, 300px - 50px= 250px)-->
								<div class="datatable_fixed" style="width:100%; height:80px; float:left; padding:25px 0;">
									<!--group으로 헤더높이가 길어질 경우 padding  (기본높이는 25px)줘야 함-->
									<div style=" height:100px; !important; overflow-x:hidden;">       
										<table  width="100%" cellspacing="0" cellpadding="0" border="0"  class="">
											<thead>
												<tr>
													<th style="width:25%">미평가</th> 
													<th style="width:25%">평가중</th>
													<th style="width:25%">평가완료</th>
													<th style="width:80%">합계</th>	
													<th style="width:16px; min-width:16px; border-bottom:0;"></th>   	        																												 										
												</tr>
											</thead>
											<tbody id="cntTable">
											</tbody>
										</table>
									</div>
								</div>
							</div><!-- //합계 테이블 -->
							<div class="group f_left  w100p m_b5 m_t10">
								<div class="label type2 f_left">  
									<div align="left" style="float: left;">
										상세현황
									</div>   
				 				</div>
							</div>
							<div class="blueish datatable f_left" style="width:100%; height:575px; margin:5px 0; overflow:hidden;">      
								<!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 헤더가 50px일경우, 300px - 50px= 250px)-->
								<div class="datatable_fixed" style="width:100%; height:575px; float:left; padding:25px 0;">
									<!--group으로 헤더높이가 길어질 경우 padding  (기본높이는 25px)줘야 함-->
									<div style=" height:550px; !important; overflow-x:hidden;">   
										<table  width="100%" cellspacing="0" cellpadding="0" border="0"  class="">
											<thead>
												<tr> 
													<th style="width:12%">전략목표</th> 
													<th style="width:7%">세부전략코드</th>
													<th style="width:15%">세부전략명</th>
													<th style="width:7%">추진과제코드</th>
													<th style="width:37%">추진과제명</th>
													<th style="width:7%">평가상태</th>
													<th style="width:7%">평가책임자</th>
													<th style="width:80%">평가자</th> 
													<th style="width:16px; min-width:16px; border-bottom:0;"></th>   	        													
												</tr>
											</thead>
											<tbody id="statusTable">						 						
											</tbody>
										</table><!-- //table -->
									</div><!-- //table div -->
								</div><!-- //datatable_fixed -->  
							</div><!-- //datatable_f_left -->
					</div><!-- //content --> 
				</div><!-- //container -->
			
		</div><!-- //wrap -->
	</body>
	