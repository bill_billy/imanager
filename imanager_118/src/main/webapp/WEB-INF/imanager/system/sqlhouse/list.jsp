<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
        <script>
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){ 
        		//$("body").append("jQuery 로드 완료");
        		init();
        		setEvent();
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		$("#searchText").focus();
        	}
        	
        	//조회
        	function search(){
        		
        	}
        	
        	//입력
        	function insert(){
        		if($("#frmInsert [name=description]").val().trim()==""){
        			alert("필수 값을 확인하세요.");
        			return;
        		}
        		$i.insert("#frmInsert").done(function(args){
        			alert("등록 되었습니다.");
        		}).fail(function(e){ 
        			alert("등록 중 오류가 발생 했습니다.");
        		});
        	}
        	
        	//수정
        	function update(idx,vendor){
        		/**
        		var post = $i.post("./update", { 
    				idx:idx,
    				vendor:vendor,
    				description:$("tbody[data-idx="+idx+"]").find("[name=description]").val(), 
    				sql:$("tbody[data-idx="+idx+"]").find("[name=sql]").val(),
    				sql_excel:$("tbody[data-idx="+idx+"]").find("[name=sql_excel]").val()
    				}
        		);
        		**/
        		var post = $i.update(
        				{ 
            				idx:idx,
            				vendor:vendor,
            				description:$("tbody[data-idx="+idx+"]").find("[name=description]").val(), 
            				sql:$("tbody[data-idx="+idx+"]").find("[name=sql]").val(),
            				sql_excel:$("tbody[data-idx="+idx+"]").find("[name=sql_excel]").val()
        				}
        		);
        		post.done(function(args){
        			alert("수정 되었습니다");
        		}); 
        		post.fail(function(e){
        			alert("수정 중 오류가 발생 했습니다.");
        		});
        		 
        	} 
        	
        	//삭제
        	function remove(){
        		
        	}
        	
        	//각종 이벤트 바인딩.
        	function setEvent(){
        		$("#btnInsert").on("click", function(event){
        			$("#tblInsert").toggle(); 
        		}); 
        	}
        	
        	//테스트 함수.
        	function test(){ 
        		var async = $i.sqlhouse(4007,{S_ID:"C"},function(res){});
        		async.done(function(res){ 
        			console.log(res);
        		}).fail(function(error){
        			console.log(error); 
        		});
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	
        	
        	function toggle(idx){
          		 $("tbody[data-idx="+idx+"]").toggle();
          	}
        </script>
        <style>
        	table {
        		table-layout: fixed; /*테이블 내에서 <td>의 넓이,높이를 고정한다.*/
        		border:1px;  
        		border-color:black;
        		border-style:solid;    
        		font-family:'돋움',Dotum,Helvetica,'Apple SD Gothic Neo',Sans-serif; 
        		
        	}
        	th{
        		background-color:#092E6E !important;
        		color:white;  
        	}
        	.sql{ 
        			width:100%;
        			overflow: hidden;
			    	text-overflow:ellipsis; /*overflow: hidden; 속성과 같이 써줘야 말줄임 기능이 적용된다.*/
			    	white-space:nowrap;
        	}
        </style>
    </head>
    <body>
        <h1>Sql House Manager</h1>
        <h2>Sql House 관리 화면</h2>
        <div class="condition_area" >    
        	<form>  
        		<label>검색 </label><input name="searchText" id="searchText" type="text" value=""/> 
        	</form> 
        </divi> 
        <div class="button_area">
        	<input id="btnInsert" type="button" value="신규" />
        </div>  
        검색어 : ${param.searchText}
        <form id="frmInsert" action="./insert" method="POST">
	        <table style='width:1024px; !important;display:none;' id="tblInsert">
	         	<tr>
					<td colspan = "2"  style="text-align:center;">
						※ 제목 
						<input  style="width:500px;" name="description" type='text' value=""/>
					</td>
					<td>
						Vendor
						<select name="vendor">  
							<option value="oracle">oracle</option>
							<option value="sqlserver">sqlserver</option>
							<option value="db2">db2</option>
							<option value="mysql">mysql</option>
							<option value="tibero">tibero</option>
						</select>
					</td>
				</tr> 
				<tr> 
					<td colspan = "3"  style="text-align:center;">
						쿼리
					</td>
				</tr>
				<tr>
					<td colspan = "3" ><textarea name="sql" style='width:100%;height:200px;'></textarea></td>
				</tr>  
				<tr>
					<td colspan = "3"  style="text-align:center;">엑셀</td>
				</tr>
				<tr>   
					<td colspan = "3" ><textarea name="sql_excel" style='width:100%;height:200px;'></textarea></td>  
				</tr>
				<tr>
					<td colspan = "3" style='text-align:center;'><input type='button' value="저장" onclick="insert();" /></td>   
				</tr> 
	        </table>
        </form> 
	    <table style='width:1024px; !important;'>  
	    	<thead> 
		    	<tr> 
		    		<th>IDX</th><th>제목</th><th>Vendor</th>
		    	</tr>
	    	</thead>
	        <c:choose>
				<c:when test="${mainList != null && not empty mainList}">
					<c:forEach items="${mainList}" var="row" varStatus="loop">
						<tr>
							<td style='text-align:center;'>${row.IDX }</td>
							<td style='text-align:center;'><a href="javascript:toggle(${row.IDX});">${row.DESCRIPTION }</a></td>
							<td style='text-align:center;'>${row.VENDOR }</td>
						</tr>
						<tbody data-idx="${row.IDX }" data-vendor="${row.VENDOR}" style='display:none;'>
							<tr> 
								<td colspan = "3"  style="text-align:center;">
									제목 : 
									<input style="width:500px;" name="description" type='text' value="${row.DESCRIPTION }"/>
								</td>
							</tr> 
							<tr> 
								<td colspan = "3"  style="text-align:center;">
									쿼리
									<input name="vendor" type='hidden' value="${row.VENDOR }"/>
								</td>
							</tr>  
							<tr>
								<td colspan = "3" ><textarea name="sql" style='width:100%;height:200px;'>${row.SQL}</textarea></td>
							</tr>  
							<tr>
								<td colspan = "3"  style="text-align:center;">엑셀</td>
							</tr>
							<tr>   
								<td colspan = "3" ><textarea name="sql_excel" style='width:100%;height:200px;'>${row.SQL_EXCEL}</textarea></td>  
							</tr>
							<tr>
								<td colspan = "3" style='text-align:center;'><input type='button' value="수정" onclick="update('${row.IDX}','${row.VENDOR}')"/></td>  
							</tr>
						</tbody>
					</c:forEach>
				</c:when>
			</c:choose>
		</table>
    </body>
</html>

