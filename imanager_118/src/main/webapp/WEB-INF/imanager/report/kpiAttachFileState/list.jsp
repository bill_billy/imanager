<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>지표별 증빙자료 현황</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        var evaGbn = '${param.pEvaGbn}';
        	$(document).ready(function(){
        		//조회 버튼
        		$("#btnSearch").jqxButton({ width: '50', height: 25, theme:'blueish'});   
        		init();
        	});	
        	//부서평가 년도 (실적 : A, 목표 : B 구분함)	
        	function init(){
        		var dat = $i.sqlhouse(182,{S_EVA_GBN:evaGbn, S_GUBUN:"A"}); 
        		dat.done(function(res){
        			var source = 
        			{
        				datatype:"json",
        				datafields:[
        					{ name : "COM_COD" },
        					{ name : "COM_NM" }
        				],
        				id:"id",
        				localdata:res,
        				async : false
        			};
        			var dataAdapter = new $.jqx.dataAdapter(source);
        			$("#cboYear").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 150, width: 100, height: 25, theme:'blueish'});
        			search();
        		});
        	}
        	function search(){
        		//지표별 증빙자료 현황 전체
        		var data = $i.sqlhouse(183,{S_YYYY:$("#cboYear").val(), S_EVA_GBN:evaGbn});
        		data.done(function(res){
        			if(res.returnCode == "SUCCESS"){
        				if(res.returnArray.length > 0){
        					$("#sumKpi").html(res.returnArray[0].SUM_KPI);
            				$("#sumNullKpi").html(res.returnArray[0].SUM_NULL_KPI);
            				$("#sumKpiCount").html(res.returnArray[0].SUM_KPI_COUNT);   
        				}	  
        			}
        		});      
                var customsortfunc = function (column, direction) {
                    var sortdata = new Array();

                    if (direction == 'ascending') direction = true;
                    if (direction == 'descending') direction = false;

                    if (direction != null) {
                        for (i = 0; i < data.length; i++) {
                            sortdata.push(data[i]);
                        }
                    }
                    else sortdata = data;
                    
                    source.localdata = sortdata;
                    $("#kpiattachfilelist").jqxGrid('pincolumn', 'columndatafield');
                    Object.prototype.toString = tmpToString;
                };
                //지표별 증빙자료 현황 리스트
        		var dat = $i.sqlhouse(184,{S_YYYY:$("#cboYear").val(),S_EVA_GBN:evaGbn});      
        		dat.done(function(data){      
		            // prepare the data   
		            var source =
		            {      
		                  localdata: data.returnArray,
		                  datafields:
		                  [
		                    { name: 'SC_ID', type: 'string' },
		                    { name: 'SC_NM', type: 'string' },
		                    { name: 'TOTAL_KPI', type: 'string' },
		                    { name: 'NULL_KPI', type: 'string' },
							{ name: 'KPI_COUNT', type: 'string' },
		                  ],
		                  datatype: "array"
		            };   
		            
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            var align = function(row,datafield,value){
		            	
		            	return "<div class='hover' style='text-align:left; padding-bottom:2px; margin-top:5px;margin-left:15px; color:black;'>" + value + "</div>";
		            };
		            var filePopup = function(row,datafield,value){
		            	var fileYn = $("#kpiattachfilelist").jqxGrid("getrowdata",row).FILE_YN;
		            	var fileID = $("#kpiattachfilelist").jqxGrid("getrowdata",row).FILE_ID;
		            	var yyyy = $("#kpiattachfilelist").jqxGrid("getrowdata",row).YYYY;
		            	var scID = $("#kpiattachfilelist").jqxGrid("getrowdata",row).SC_ID;
		            	var scNm = $("#kpiattachfilelist").jqxGrid("getrowdata",row).SC_NM;
		            	var kpiID = $("#kpiattachfilelist").jqxGrid("getrowdata",row).KPI_ID;
		            	var kpiNm = $("#kpiattachfilelist").jqxGrid("getrowdata",row).KPI_NM;
		            	if(fileYn != "" && fileYn != null){
		            		var link = "<a href=\"javaScript:fileDown('"+fileID+"');\" style='color:black;'>"+value+"</a>";
		            	}else{
		            		var link = "<a href=\"javaScript:filePopup('"+yyyy+"','"+scID+"','"+kpiID+"','"+scNm+"','"+kpiNm+"','"+fileID+"');\" style='color:black;'>등록</a>";
		            	}
		            	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px; color:black;'>" + link + "</div>";
		            	
		            };   
		            $("#kpiattachfilelist").jqxGrid(
		            {
		            	width: '100%',
						height: '100%',
		                source: dataAdapter,
		                sortable: false,
						theme:'blueish',
						columnsheight:30 ,
						rowsheight: 30,
						altrows:true,   
		                ready: function () {
		                    $("#kpiattachfilelist").jqxGrid();
		                },
		                columns: [    
		                          { text: '평가조직', dataField: 'SC_NM', width: '40%', align:'center', cellsalign: 'center', cellsrenderer:align},       
		                          { text: '총 지표수',  dataField: 'TOTAL_KPI', width: '20%', align:'center',  cellsalign: 'center'},
		                          { text: '등록 자료수',  dataField: 'KPI_COUNT', width: '20%', align:'center',  cellsalign: 'center'},
		                          { text: '미등록 자료수',  editable: false,  datafield: 'NULL_KPI', width: '20%', cellsalign: 'center', align:'center'},
		                		 ]
		            });
        		});
        	}
        </script>   
        <style>
        	table {
        		table-layout: fixed; /*테이블 내에서 <td>의 넓이,높이를 고정한다.*/
        		border:1px;  
        		border-color:black;
        		border-style:solid;    
        		font-family:'돋움',Dotum,Helvetica,'Apple SD Gothic Neo',Sans-serif; 
        		
        	}
        	.sql{ 
        			width:100%;
        			overflow: hidden;
			    	text-overflow:ellipsis; /*overflow: hidden; 속성과 같이 써줘야 말줄임 기능이 적용된다.*/
			    	white-space:nowrap;
        	}
        	.blueish .datatable table td{
				height:30px !important;
			}
        </style>
    </head>
    <body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 1%;">
			<div class="header f_left" style="width:100%; margin-top:10px;">
				<div class="group" style="right:0px;">
					<div class="label type1 f_left" style="float:left;">평가년도：</div>  
					<div id='cboYear' class="combobox f_left" style='float: left; width:140px;'></div>  
					<div class="button type1 f_right">
						<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
					</div>
				</div>
				<div class="container f_left" style="width:100%; height:100%; float:left; ">
					<div class="content f_left" style="width:100%; margin-right:1%;">
						<div class="table f_left" style="width:100%; height:100%; margin:13px 0 0 0;margin-top:30px; margin-bottom:13px;"> 
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<tr>
									<th style="width:40%;">총 지표수</th>
									<th style="width:30%;">등록 자료수</th>
									<th style="width:30%;">미등록 자료수</th>
								</tr>
								<tr>
									<td style="text-align:center;"><span id="sumKpi"></span></td>    
									<td style="text-align:center;"><span id="sumKpiCount"></span></td>
									<td style="text-align:center;"><span id="sumNullKpi"></span></td>
								</tr>   
							</table>   
						</div>
						<div class="grid f_left" style="width:100%; height:450px;">  
							<div id="kpiattachfilelist"></div>
					 	</div>	
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

