<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>부서별 지표현황</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		$("#btnSearch").jqxButton({ width: '',  theme:'blueish'}); 
				var evaGbnDat = $i.sqlhouse(139,{S_COML_COD:"EVA_GBN", S_COM_COD:"B"});
				evaGbnDat.done(function(res){
					if(res.returnArray.length > 0){
						$("#spanEvaLabel").html(res.returnArray[0].COM_NM);
					}
				}); 
        		$("#cboSearchYear").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,  theme:'blueish'});
        		$("#cboSearchMm").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,  theme:'blueish'});
        		$("#cboSearchKpiID").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 250, dropDownHeight: 250, width: 250, height: 22,  theme:'blueish'});
        		$("#cboSearchScID").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 150, dropDownHeight: 150, width: 150, height: 22,  theme:'blueish'});
//         		checkDate();
        	}
        	//조회 
        	function search(){
        		$("#searchForm").submit();
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function changeYearCombo(){
        		var data = $i.service("listMmCombo",["${param.pEvaGbn}", $("#cboSearchYear").val(), "A"]);
           		data.done(function(c){ 
           			if(c.returnCode=="SUCCESS"){
           				$("#cboSearchMm").jqxComboBox("clear");
           				for(var i=0;i<c.returnArray.length;i++){
           					$("#cboSearchMm").jqxComboBox("insertAt", {label:c.returnArray[i].MM_NM, value:c.returnArray[i].MM_ID},i);
           				}
           				$("#cboSearchMm").jqxComboBox("selectIndex", 0);
           			}else{
           				$("#cboSearchMm").jqxComboBox("clear");
           				$("#cboSearchMm").jqxComboBox("insertAt", {label:"==선택==",value:""},0);
           			} 
           		});
        	}
        	function changeMmCombo(){
        		var data = $i.service("listKpiCombo",["${param.pEvaGbn}", $("#cboSearchYear").val()+$("#cboSearchMm").val()]);
        		data.done(function(c){
        			if(c.returnCode == "SUCCESS"){
        				$("#cboSearchKpiID").jqxComboBox("clear");
        				for(var i=0;i<c.returnArray.length;i++){
        					$("#cboSearchKpiID").jqxComboBox("insertAt",{label:c.returnArray[i].MT_NM, value:c.returnArray[i].MT_ID},i);
        				}
        				$("#cboSearchKpiID").jqxComboBox("selectIndex", 0);
        			}else{
        				$("#cboSearchKpiID").jqxComboBox("clear");
        				$("#cboSearchKpiID").jqxComboBox("insertAt", {label:"==선택==",value:""},0);
        			}
        		});
        	}
        	function changeKpiCombo(){
        		var dataScid = $i.service("listScidCombo",["${param.pEvaGbn}", "", $("#cboSearchYear").val()+$("#cboSearchMm").val(), $("#cboSearchKpiID").val()]);
           		dataScid.done(function(data){
           			if(data.returnCode == "SUCCESS"){
           				$("#cboSearchScID").jqxComboBox("clear");
           				for(var i=0;i<data.returnArray.length;i++){
           					$("#cboSearchScID").jqxComboBox("insertAt", {label:data.returnArray[i].SC_NM, value:data.returnArray[i].SC_ID},i);
           				}
           				$("#cboSearchScID").jqxComboBox("selectIndex", 0);
           			}else{
           				$("#cboSearchScID").jqxComboBox("clear");
           				$("#cboSearchScID").jqxComboBox("insertAt", {label:"==선택==", value:""},0);
           			}
           		});
        	}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
		</style>
		   
    </head>
    <body class='blueish'>
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:1160px; min-width:1160px; margin:0 10px;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<form id="searchForm" name="searchForm" action="./list">
					<input type="hidden" id="hiddenpEvaGbn" name="pEvaGbn" value="${param.pEvaGbn}" />
					<div class="label type1 f_left">평가구분:<span class="label sublabel" id="spanEvaLabel"></span></div>
					<div class="label type1 f_left">년월:</div>
					<div class="combobox f_left" onchange="changeYearCombo();">
						<select id="cboSearchYear" name="searchYear">
							<c:choose>
								<c:when test="${listYear != null || not empty listYear}">
									<c:forEach items="${listYear}" var="key" varStatus="loop">
										<option value="${key.COM_COD}" <c:if test="${key.COM_COD == param.searchYear}">selected</c:if>>${key.COM_NM}</option>
									</c:forEach>
								</c:when>
							</c:choose>
						</select>
					</div>
					<div class="combobox f_left" onchange="changeMmCombo();">
						<select id="cboSearchMm" name="searchMm">
							<c:choose>
								<c:when test="${listMm != null || not empty listMm}">
									<c:forEach items="${listMm}" var="key" varStatus="loop">
										<option value="${key.MM_ID}" <c:if test="${key.MM_ID == param.searchMm}">selected</c:if>>${key.MM_NM}</option>
									</c:forEach>
								</c:when>
							</c:choose>
						</select>
					</div>
					<div class="label type1 f_left">지표:</div>
					<div class="combobox f_left" onchange="changeKpiCombo();">
						<select id="cboSearchKpiID" name="searchKpiID">
							<c:choose>
								<c:when test="${listKpiCombo != null || not empty listKpiCombo}">
									<c:forEach items="${listKpiCombo}" var="key" varStatus="loop">
										<option value="${key.MT_ID}" <c:if test="${key.MT_ID == param.searchKpiID}">selected</c:if>>${key.MT_NM}</option>
									</c:forEach>
								</c:when>
							</c:choose>
						</select>
					</div>
					<div class="label type1 f_left">조직:</div>
					<div class="combobox f_left">
						<select id="cboSearchScID" name="searchScID">
							<c:choose>
								<c:when test="${listScCombo != null || not empty listScCombo}">
									<c:forEach items="${listScCombo}" var="key" varStatus="loop">
										<option value="${key.SC_ID}" <c:if test="${key.SC_ID == param.serachScID}">selected</c:if>>${key.SC_NM}</option>
									</c:forEach>
								</c:when>
							</c:choose>
						</select>
					</div>
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
					</div>
				</form>
			</div>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:100%; margin:0%;">
					<div class="blueish datatable f_left" style="width:100%; height:600px; margin:0px 0; overflow:hidden;">
						<div class="datatable_fixed" style="width:100%; height:600px; float:left; padding:50px 0;"><!--group으로 헤더높이가 길어질 경우 padding  (기본높이는 25px)줘야 함-->
							<div style=" height:550px !important; overflow-x:hidden;"><!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 600px - 50px= 550px)-->
								<table summary="교원평가 대상설정" style="width:100%;"  class="none_hover none_alt">
									<thead style="width:100%;">
										<tr>
											<th rowspan="2" scope="col" style="width:10%;">지표</th>
											<th rowspan="2" scope="col" style="width:7%;">자료</th>
											<th rowspan="2"  scope="col" style="width:8%;">조직</th>
											<th rowspan="2"  scope="col" style="width:3%;">단위</th>
											<th rowspan="2"  scope="col" style="width:5%;">목표</th>
											<th rowspan="2"  scope="col" style="width:6%;">실적</th>
											<th colspan="3" scope="col" style="width:18%;">담당자</th>
											<th colspan="3" scope="col" style="width:18%;">승인자</th>
											<th colspan="4" scope="col" style="width:23%;">항목</th>
											<th style="width:1%; min-width:17px;"></th>
										</tr>
										<tr>
											<th scope="col" style="width:6%;">이름</th>
											<th scope="col" style="width:6%;">확인여부</th>
											<th scope="col" style="width:6%;">일자</th>
											<th scope="col" style="width:6%;">이름</th>
											<th scope="col" style="width:6%;">확인여부</th>
											<th scope="col" style="width:6%;">일자</th>
											<th scope="col" style="width:9%;">항목명</th>
											<th scope="col" style="width:3%;">단위</th>
											<th scope="col" style="width:5%;">입력..</th>
											<th scope="col" style="width:6%;">실적</th>
											<th style="width:1%; min-width:17px;"></th>
										</tr>
									</thead>
									<tbody>
										<c:choose>
											<c:when test="${mainList != null || not empty mainList}">
												<c:forEach items="${mainList}" var="mainList" varStatus="loop">
													<tr>
													<c:if test="${loop.count == 1 || kpiRowspan == 0}">
													<c:set var="kpiRowspan" value="${mainList.KPI_ROW_COUNT}" />
														<td rowspan="${kpiRowspan}" style="width:10%;">
															<div class="cell">${mainList.KPI_NM}</div>
														</td>
														<td rowspan="${kpiRowspan}" style="width:7%;">
															<div class="cell">${mainList.MT_UDC2}</div>
														</td>
													</c:if>
													<c:set var="kpiRowspan" value="${kpiRowspan-1}" />
													<c:if test="${loop.count == 1 || scRowspan == 0}">
													<c:set var="scRowspan" value="${mainList.SC_ROW_COUNT}" />
														<td rowspan="${scRowspan}" style="width:8%;">
															<div class="cell t_center">${mainList.SC_NM}</div>
														</td>
														<td rowspan="${scRowspan}" style="width:3%;">
															<div class="cell t_center">${mainList.UNIT_NM}</div>
														</td>
														<td rowspan="${scRowspan}" style="width:5%;">
															<div class="cell t_right">${mainList.TARGET_AMT}</div>
														</td>
														<td rowspan="${scRowspan}" style="width:6%;">
															<div class="cell t_center">${mainList.CAL_VNM}</div>
														</td>
														<td rowspan="${scRowspan}" style="width:6%;">
															<div class="celll t_center">${mainList.KPI_CHARGE_NM}</div>
														</td>
														<td rowspan="${scRowspan}" style="width:6%;">
															<c:if test="${mainList.CHARGE_CONFIRM == '' || mainList.CHARGE_CONFIRM == 'N'}">
																<div class="cell t_center"><span class="label sublabel type3">미확인</span></div>
															</c:if>
															<c:if test="${mainList.CHARGE_CONFIRM == 'Y'}">
																<div class="cell t_center"><span class="label sublabel type2">확인</span></div>
															</c:if>
														</td>	
														<td rowspan="${scRowspan}" style="width:6%;">
															<c:if test="${mainList.CHARGE_CONFIRM == 'Y'}">
																<div class="cell t_center">
																	${mainList.CHARGE_DATE}
																	</br>
																	${mainList.LAST_CHARGE_NM}
																</div>
															</c:if>
														</td>	
														<td rowspan="${scRowspan}" style="width:6%;">
															<div class="cell t_center">${mainList.USER_NM}</div>
														</td>
														<td rowspan="${scRowspan}" style="width:6%;">
															<c:if test="${mainList.CHARGE_CONFIRM == '' || mainList.CHARGE_CONFIRM == 'N'}">
																<div class="cell t_center">
																	<span class="label sublabel type3">담당자 미확인</span>
																</div>	
															</c:if>
															<c:if test="${mainList.CHARGE_CONFIRM == 'Y'}">
																<c:if test="${mainList.USER_CONFIRM == '' || mainList.USER_CONFIRM == 'N'}">
																	<div class="cell t_center">
																		<span class="label sublabel type3">미확인</span>
																	</div>	
																</c:if>
																<c:if test="${mainList.USER_CONFIRM == 'Y'}">
																	<div class="cell t_center">
																		<span class="label sublabel type2">확인</span>
																	</div>	
																</c:if>
															</c:if>
														</td>	
														<td rowspan="${scRowspan}" style="width:6%;">
															<c:if test="${mainList.USER_CONFIRM == 'Y'}">
																<div class="cell t_center">
																	${mainList.USER_DATE}
																	</br>
																	${mainList.LAST_USER_NM}
																</div>
															</c:if>
														</td>
													</c:if>
													<c:set var="scRowspan" value="${scRowspan -1}" />
														<td style="width:9%;">
															<div class="cell">${mainList.COL_NM}</div>
														</td>
														<td style="width:3%;">
															<div class="cell t_center">${mainList.COL_UNIT_NM}</div>
														</td>	
														<td style="width:5%;">
															<div class="cell t_center">${mainList.COL_SYSTEM_NM}</div>
														</td>
														<td style="width:6%;">
															<div class="cell t_right">${mainList.COL_VALUE}</div>
														</td>
												</c:forEach>
											</c:when>
										</c:choose>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>