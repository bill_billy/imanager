<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>지표현황</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        	var pEvaGbn = "B";
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init();
        		$("#tab1ExcelStart").show();
				$("#tab2ExcelStart ").hide();				
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		var dat = $i.sqlhouse(298,{S_COML_COD:"EVA_GRP"});
        		dat.done(function(res){
        			var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                        { name: 'COM_COD' },
	                        { name: 'COM_NM' }
	                    ],
	                    id: 'id',
						localdata:res,
						// url: url, 
	                    async: false
	                };
	                var dataAdapter = new $.jqx.dataAdapter(source);
	                $("#cboEvalG").jqxComboBox({selectedIndex: 0, source: dataAdapter,animationType: 'fade',dropDownHorizontalAlignment: 'right',displayMember: "COM_NM",valueMember: "COM_COD",dropDownWidth: 150,dropDownHeight: 150,width: 150,height: 23,theme:'blueish'});
        			changeYear();
        		});
        		$("#btnSearch").jqxButton({ width: '',  theme:'blueish'});
        		$("#tab1ExcelStart").jqxButton({ width: '',  theme:'blueish'});
        		$("#tab2ExcelStart").jqxButton({ width: '',  theme:'blueish'});   
        		$('#jqxTabs01').jqxTabs({ width: '100%', height: '100%', position: 'top', theme:'blueish', selectedItem: 0});
        	}
        	function changeYear(){
        		var yearMmDat = $i.sqlhouse(292,{S_EVA_GBN:pEvaGbn});
				yearMmDat.done(function(res){
					var source = 
        			{
        				datatype:"json",
        				datafields:[
        					{ name : "MM_ID"},
        					{ name : "MM_NM"}
        				],
        				id:"id",
        				localdata:res,
        				async : false
        			};
        			var dataAdapter = new $.jqx.dataAdapter(source);
        			$("#cboYearMm").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "MM_NM", valueMember: "MM_ID", dropDownWidth: 100, dropDownHeight: 80, width: 100, height: 22,  theme:'blueish'});
        			$("#cboYearMm").jqxComboBox({ selectedIndex: 0});
        			search();
				});
        	}
        	//조회
        	function search(){
        		if($("#jqxTabs01").jqxTabs("val") == "0"){
        			tab1Grid();
        		}else{
        			tab2Grid();
        		}
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        		$("#btnInsert").on("click", function(event){
        			$("#tblInsert").toggle(); 
        		}); 
        	}
        	
        	//테스트 함수.
        	function test(){ 
        		var async = $i.sqlhouse(4007,{S_ID:"C"},function(res){});
        		async.done(function(res){ 
        			console.log(res);
        		}).fail(function(error){
        			console.log(error); 
        		});
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	//첫번째 TAB 데이터 조회
        	function tab1Grid(){
        		tab1LeftGrid();
        		tab1RightGrid('','');
        		$("#tab1ExcelStart").show();
				$("#tab2ExcelStart ").hide();
        	}
        	//첫번째 TAB 좌측 그리드
        	function tab1LeftGrid(){
        		var dat = $i.sqlhouse(289,{S_EVAL_G:$("#cboEvalG").val(), S_EVA_GBN:pEvaGbn, S_YYYYMM:$("#cboYearMm").val()});
        		dat.done(function(res){
		            // prepare the data
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'ACCT_ID', type: 'string' },          
		                    { name: 'SC_ID', type: 'string' },
		                    { name: 'SC_NM', type: 'string' },
							{ name: 'CNT', type: 'string'},
							{ name: 'CHARGE_N', type: 'string'},
							{ name: 'USER_N', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var detailRow = function (row, columnfield, value, defaultHtml, property, rowdata) {//그리드 선택시 하단 상세
						var scID = rowdata.SC_ID;
						var scName = rowdata.SC_NM;
						var link = "<a href=\"javascript:tab1RightGrid('"+scID+"','"+scName+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
						
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridScKpiYnList").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
						sortable: true,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                   { text: '평가조직', datafield: 'SC_NM', width: 250, align:'center', cellsalign: 'center', cellsrenderer: detailRow},
		                   { text: '총 지표수', datafield: 'CNT', width:80, align:'center', cellsalign: 'center', cellsrenderer: alginRight},
		                   { text: '담당자 미확인', datafield: 'CHARGE_N',width:80,  align:'center', cellsalign: 'center', cellsrenderer: alginRight},
		                   { text: '책임자 미승인', datafield: 'USER_N', align:'center', cellsalign: 'center', cellsrenderer: alginRight}
		                ]
		            });
        		});
        	}
        	//첫번째 TAB 우측 그리드
        	function tab1RightGrid(scID, scName){
        		$("#labelScName").html(scName);
        		var dat = $i.sqlhouse(290,{S_SC_ID:scID,S_EVA_GBN:pEvaGbn, S_YYYYMM:$("#cboYearMm").val()});
        		dat.done(function(res){
		            // prepare the data
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'KPI_ID', type: 'string' },
		                    { name: 'MT_NM', type: 'string' },
							{ name: 'CHARGE_ID', type: 'string'},
							{ name: 'CHARGE_NM', type: 'string'},
							{ name: 'CHARGE_CONFIRM', type: 'string'},
							{ name: 'CHARGE_CONFIRM_NM', type: 'string'},
							{ name: 'CHARGE_DATE', type: 'string'},
							{ name: 'USER_ID', type: 'string'},
							{ name: 'USER_NM', type: 'string'},
							{ name: 'USER_CONFIRM', type: 'string'},
							{ name: 'USER_CONFIRM_NM', type: 'string'},
							{ name: 'USER_DATE', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var chargeConfirm = function(row, columnfield,value,defaultHtml, property, rowdata){
						 var confirm = rowdata.CHARGE_CONFIRM;
						 var color = "";
						 if(confirm == 'Y'){
							 color = "blue";
						 }else{
							 color = "red";
						 }
						 return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;color:'+color+';">' + value + '</div>';
					};
					var userConfirm = function(row, columnfield,value,defaultHtml, property, rowdata){
						 var confirm = rowdata.USER_CONFIRM;
						 var color = "";
						 if(confirm == 'Y'){
							 color = "blue";
						 }else{
							 color = "red";
						 }
						 return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;color:'+color+';">' + value + '</div>';
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridScKpiYnDetail").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
		                sortable: true,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                   { text: '지표명', datafield: 'MT_NM', width: 300, align:'center', cellsalign: 'center', cellsrenderer : alginLeft},
		                   { text: '이름', columngroup: 'A', datafield: 'CHARGE_NM', width:120, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '상태', columngroup: 'A', datafield: 'CHARGE_CONFIRM_NM',width:120,  align:'center', cellsalign: 'center', cellsrenderer: chargeConfirm},
		                   { text: '일자', columngroup: 'A', datafield: 'CHARGE_DATE', width:120, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '이름', columngroup: 'B', datafield: 'USER_NM', width:120, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '상태', columngroup: 'B', datafield: 'USER_CONFIRM_NM',width:120,  align:'center', cellsalign: 'center', cellsrenderer: userConfirm},
		                   { text: '일자', columngroup: 'B', datafield: 'USER_DATE', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
		                ],
		                columngroups:[
							{ text : '입력자', align:'center', name:'A'},
							{ text : '승인자', align:'center', name:'B'}
		                ]
		            });
        		});
        	}
        	function tab2Grid(){
        		tab2LeftGrid();
        		tab2RightGrid('', '');
        		$("#tab1ExcelStart").hide();
				$("#tab2ExcelStart ").show();
        	}
        	function tab2LeftGrid(){
        		var dat = $i.sqlhouse(291,{S_EVAL_G:$("#cboEvalG").val(), S_EVA_GBN:pEvaGbn, S_YYYYMM:$("#cboYearMm").val()});
        		dat.done(function(res){
		            // prepare the data
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'SC_ID', type: 'string' },
		                    { name: 'SC_NM', type: 'string' },
							{ name: 'CNT', type: 'string'},
							{ name: 'STATUS_COUNT', type: 'string'},
							{ name: 'FINAL_COUNT', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var detailRow = function (row, columnfield, value, defaultHtml, property, rowdata) {//그리드 선택시 하단 상세
						var scID = rowdata.SC_ID;
						var scName = rowdata.SC_NM;
						var link = "<a href=\"javascript:tab2RightGrid('"+scID+"','"+scName+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
						
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridScKpiConfirm").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                   { text: '평가조직', datafield: 'SC_NM', width: 250, align:'center', cellsalign: 'center', cellsrenderer: detailRow},
		                   { text: '총 지표수', datafield: 'CNT', width:80, align:'center', cellsalign: 'center', cellsrenderer: alginRight},
		                   { text: '미확인', datafield: 'STATUS_COUNT',width:80,  align:'center', cellsalign: 'center', cellsrenderer: alginRight},
		                   { text: '미승인', datafield: 'FINAL_COUNT', align:'center', cellsalign: 'center', cellsrenderer: alginRight}
		                ]
		            });
        		});
        	}
        	function tab2RightGrid(scID, scName){
        		$("#labelTab2ScName").html(scName);
        		var dat = $i.sqlhouse(293,{S_SC_ID:scID,S_EVA_GBN:pEvaGbn, S_YYYYMM:$("#cboYearMm").val()});
        		dat.done(function(res){
		            // prepare the data
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'KPI_ID', type: 'string' },
		                    { name: 'KPI_NM', type: 'string'},
		                    { name: 'KPI_CHARGE_ID', type: 'string'},
		                    { name: 'KPI_CHARGE_NM', type: 'string'},
		                    { name: 'OWNER_USER_ID', type: 'string'},
		                    { name: 'OWNER_USER_NM', type: 'string'},
		                    { name: 'STATUS', type: 'string'},
		                    { name: 'KPI_CONFIRM_NM', type: 'string'},
		                    { name: 'OWNER_CONFIRM_NM', type: 'string'},
		                    { name: 'CONFIRM_DATE', type: 'string'},
		                    { name: 'FINAL_CONFIRM_DATE', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var confirm = function (row, columnfield, value, defaultHtml, property, rowdata){
						var status = rowdata.STATUS;
						var color = "";
						if(status == 'D' || status == 'L'){
							color = "blue";
						}else{
							color = "red";
						}
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;color:'+color+';">' + value + '</div>';
					};
					var finalConfirm = function (row, columnfield, value, defaultHtml, property, rowdata){
						var status = rowdata.STATUS;
						var color = "";
						if(status == 'L'){
							color = "blue";
						}else{
							color = "red";
						}
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;color:'+color+';">' + value + '</div>';
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridScKpiConfirmDetail").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
						sortable: true,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                   { text: '지표명', datafield: 'KPI_NM', width: 300, align:'center', cellsalign: 'center', cellsrenderer : alginLeft},
		                   { text: '이름', columngroup: 'A', datafield: 'KPI_CHARGE_NM', width:120, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '상태', columngroup: 'A', datafield: 'KPI_CONFIRM_NM',width:120,  align:'center', cellsalign: 'center', cellsrenderer: confirm},
		                   { text: '일자', columngroup: 'A', datafield: 'CONFIRM_DATE', width:120, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '이름', columngroup: 'B', datafield: 'OWNER_USER_NM', width:120, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '상태', columngroup: 'B', datafield: 'OWNER_CONFIRM_NM',width:120,  align:'center', cellsalign: 'center', cellsrenderer: finalConfirm},
		                   { text: '일자', columngroup: 'B', datafield: 'FINAL_CONFIRM_DATE', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
		                ],
		                columngroups:[
							{ text : '입력자', align:'center', name:'A'},
							{ text : '승인자', align:'center', name:'B'}
		                ]
		            });
        		});
        	}
        	//지표실적입력현황 엑셀 다운로드
        	function tab1ExcelStart() {
        		 windowOpen('./kpiResultList_ExcelDown?pEvaGbn='+pEvaGbn+'&evalG='+$("#cboEvalG").val()+'&yyyymm='+$("#cboYearMm").val(), "_self");
			}
        	//지표자체평가 엑셀 다운로드
        	function tab2ExcelStart() {
        		windowOpen('./kpiEvalList_ExcelDown?pEvaGbn='+pEvaGbn+'&evalG='+$("#cboEvalG").val()+'&yyyymm='+$("#cboYearMm").val(), "_self");
			}
        	function windowOpen(url, windowName, width, height, location, scrollbars){
				var screenWidth=screen.width;
				var screenHeight=screen.height;
			
				var x=(screenWidth/2)-(width/2);
				var y=(screenHeight/2)-(height/2);
				//2013-12-18장민수 수정
				//새창 객체를 리턴하도록.
				return window.open(url, windowName,"width=" + width + ", height=" + height + ",  location=" + location + ", toolbar=no, menubar=no, directories=no, scrollbars=" + scrollbars + ", resizable=no, left="+ x + ", top=" + y);
			}
        	
        </script>
        <style>
        	table {
        		table-layout: fixed; /*테이블 내에서 <td>의 넓이,높이를 고정한다.*/
        		border:1px;  
        		border-color:black;
        		border-style:solid;    
        		font-family:'돋움',Dotum,Helvetica,'Apple SD Gothic Neo',Sans-serif; 
        		
        	}
        	.sql{ 
        			width:100%;
        			overflow: hidden;
			    	text-overflow:ellipsis; /*overflow: hidden; 속성과 같이 써줘야 말줄임 기능이 적용된다.*/
			    	white-space:nowrap;
        	}
        </style>
    </head>
    <body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 1%;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<div class="label type1 f_left">구분 : </div>
				<div class="combobox f_left" id="cboEvalG"></div>
				<div class="label type1 f_left">년월 : </div>
				<div class="combobox f_left" id="cboYearMm"></div>
<!-- 				<div class="combobox f_left" id="cboMm"></div> -->
				<div class="group f_right pos_a" style="right:0px;">
					<div class="button type1 f_left">
						<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
					</div>
					<div class="button type1 f_left">
						<input type="button" value="EXCEL" id='tab1ExcelStart' width="100%" height="100%" onclick="tab1ExcelStart();"/>
						<input type="button" value="EXCEL" id='tab2ExcelStart' width="100%" height="100%" onclick="tab2ExcelStart();"/>
					</div> 
				</div>
		  	</div>
		  	<div class="container  f_left" style="width:100%; margin:10px 0;">
		  		<div class="tabs f_left" style=" width:100%; height:580px; margin-top:0px;">
		  			<div id='jqxTabs01'>
		  				<ul>
		  					<li style="margin-left:0px" onclick="tab1Grid();">지표실적입력현황</li>
		  					<li onclick="tab2Grid();">지표 자체평가</li>
		  				</ul>
		  				<div class="tabs_content p_t10 p_l10" style="height:100%; ">   
			  				<div class="context f_left" style="width:33%; margin-right:1%;">
					  			<div class="grid f_left" style="width:100%; height:500px;">
					  				<div id="gridScKpiYnList">
					  				</div>
					  			</div>
					  		</div>
					  		<div class="content f_left" style="width:65%">
				  				<div class="group f_left w100p">
				  					<div class="label type2 f_left p_r5">조직 : </div>
									<span class="label sublabel f_left p_r10" style="font-weight:bold;" id="labelScName"> </span>
				  				</div>    
				  				<div class="grid f_left" style="width:100%; height:470px;">
					  				<div id="gridScKpiYnDetail"> </div>
					  			</div>
					  		</div>
					  	</div>
					  	<div class="tabs_content p_t10 p_l10" style="height:100%; ">
					  		<div class="context f_left" style="width:33%; margin-right:1%;">
					  			<div class="grid f_left" style="width:100%; height:500px;">
					  				<div id="gridScKpiConfirm"></div>
					  			</div>
					  		</div>
					  		<div class="content f_left" style="width:65%;'">
					  			<div class="group f_left w100p">
					  				<div class="label type2 f_left p_r5">조직 : </div>
					  				<span class="label sublabel f_left p_r10" style="font-weight:bold;'" id="labelTab2ScName"></span>
					  			</div>
					  			<div class="grid f_left" style="width:100%; height:470px;">
					  				<div id="gridScKpiConfirmDetail"></div>
					  			</div>
					  		</div>
					  	</div>
		  			</div>
		  		</div>
		  	</div>
		</div>
	</body>
</html>

