 <%@ page language="java" contentType="application/vnd.xls;charset=UTF-8" pageEncoding="utf-8"%>   -
<%@page import="java.net.URLEncoder"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%

	String header =request.getHeader("User-Agent");
	String browser="";  
	String docName = URLEncoder.encode("지표실적입력현황", "utf-8");  
	 
	System.out.println("header++++++++++++++++++++++++++++++"+header+":::::"+docName);
	 
	
    response.setHeader("Content-Disposition", "attachment; filename="+docName+".xls"); 
    response.setHeader("Content-Description", "JSP Generated Data");

%>   -
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<title>지표실적입력현황</title>
	
	</head>
 	<body>
		<table width="100%;" id="tableList" class="fix_rable" cellspacing="0" cellpadding="0" border="1">		
				<input type="hidden" id="pEvaGbn" name="pEvaGbn" value="${param.pEvaGbn}"/>
				<input type="hidden" id="evalG" name="evalG" value="${param.evalG}"/>
				<input type="hidden" id="yyyymm" name="yyyymm" value="${param.yyyymm}"/>						
							<tr> 			
								<th style="background-color:#eff0f0;"  >평가조직</th>
								<th style="background-color:#eff0f0;" >지표명</th>								
								<th style="background-color:#eff0f0;" >입력자</th>
								<th style="background-color:#eff0f0;" >입력자상태</th>
								<th style="background-color:#eff0f0;" >입력자일자</th>
								<th style="background-color:#eff0f0;" >승인자</th>
								<th style="background-color:#eff0f0;" >승인자상태</th> 
								<th style="background-color:#eff0f0;" >승인자일자</th>
							</tr>
						</thead>
					    <tbody>
						<c:choose> 		
								<c:when test="${mainList != null && not empty mainList}"> 	
									<c:forEach items="${mainList}" var="rows" varStatus="loop">
										<tr>		
											<td align="center">${rows.SC_NM}</td>
											<td>${rows.MT_NM}</td>
											<td align="center">${rows.CHARGE_NM}</td>
											<td>${rows.CHARGE_CONFIRM_NM}</td>
											<td>${rows.CHARGE_DATE}</td>
											<td align="center">${rows.USER_NM}</td>
											<td>${rows.USER_CONFIRM_NM}</td>
											<td>${rows.USER_DATE}</td>
										</tr>
									</c:forEach> 		
								</c:when> 		
								<c:otherwise> 				
									<tr> 					
										<td class="gtd"  colspan="8">조회된 데이터가 없습니다.</td> 			
									</tr> 		
								</c:otherwise> 
							</c:choose>  
						</tbody> 
					</table>
		<!--wrap-->
	</body>
</html>