<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>그룹코드관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
          <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <script>
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$("#btnSearch").jqxButton({ width: '',  theme:'blueish'});
				init();
//         		setEvent();
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		search();
        		makeMissionCombo();
        		//calc();
//         		$("#searchText").focus();

				
        	}
        	var missionList = [];
	   		var missionDat;
				function makeMissionCombo(){
					missionDat = $i.sqlhouse(322,{}); 
						missionDat.done(function(res){
							missionList = res.returnArray;
		    				var source = 
		    				{ 
			        			datatype:"json",
			        			datafields:[
			        				{ name : "MS_ID" },
			        				{ name : "MS_NM" } 
			        			],
			        			id:"id",
			        			localdata:res,
			        			async : false 
			        		};
			        		var dataAdapter = new $.jqx.dataAdapter(source);
			        		 $("#getMission").jqxComboBox({ 
			        			//	selectedIndex: 0, 
			        				source: dataAdapter, 
			        				animationType: 'fade', 
			        				dropDownHorizontalAlignment: 'right', 
			        				displayMember: "MS_NM", 
			        				valueMember: "MS_ID",  
			        				dropDownWidth: 300, 
			        				dropDownHeight: 100, 
			        				width: 300, 
			        				height: 22,  
			        				theme:'blueish'} );
			        		 
			        		$('#getMission').on('select', function (event)  
			        		{
			        			var args = event.args;
			        			if (args) 
			        			{
			        				var index = args.index;
			        				makeYearCombo(index);
			        			}	     
			        		}); 
			        		 
		    			$("#getMission").jqxComboBox('selectIndex', 0 ); 
		    			
		    		});  
		 		}//makeMissionCombo 
        	//조회
        	function makeYearCombo(idx){
		 			var start = missionList[idx].START_YYYY;
		 			var end = missionList[idx].END_YYYY;
		 			var nowYear = new Date().getFullYear();
		 				if(nowYear >= start && nowYear < end) //현재기준으로 설정 
		 					end = nowYear;
		 			var yyyyList = [];
		 				for(var i=Number(start);i<=end;i++){ 
		 					yyyyList.push(i);
		 				}
		     			$("#getNY").jqxComboBox({  
							source: yyyyList, 
							animationType: 'fade', 
							dropDownHorizontalAlignment: 'right', 
							dropDownWidth: 80, 
							dropDownHeight: 80, 
							width: 80, 
							height: 22,  
							theme:'blueish'});
		 			
		 			$("#getNY").jqxComboBox('selectIndex', yyyyList.length-1 );  
		    	}//makeYearCombo
		    	
        	function search(){
        		var dat = $i.sqlhouse(452,{MS_ID:$("#getMission").val(),YYYY:$("#getNY").val()});
        		dat.done(function(res){
		            // prepare the data
		            if(res.returnArray.length>0 || res.returnArray!=null){
		            	for(var i=0;i<res.returnArray.length;i++){
		            		var sum=((res.returnArray[i].QUAN_ACH_VAL/res.returnArray[i].QUAN_GOAL)*100).toFixed(3);
		            		if(sum>0)
		            			{
		            			res.returnArray[i]["CALC"] = sum;
		            	}
		            	}
		            }
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                	    { name: 'ASSIGN_CD', type: 'string' },
			                    { name: 'PROGRAM_NM', type: 'string' },
								{ name: 'QUAN_CONTENT', type: 'string'},
								{ name: 'QUAN_UNIT', type: 'string'},
								{ name: 'QUAN_GOAL', type: 'number'},
								{ name: 'QUAN_ACH_VAL', type: 'number'},
								{ name: 'CALC', type: 'number'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
		            	
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var detailRow = function (row, columnfield, value) {//그리드 선택시 하단 상세
						
						var comlCod = $("#gridlist").jqxGrid("getrowdata", row).COML_COD;
						
						var link = "<a href=\"javascript:getRowDetail('"+comlCod+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
						
					};
		            
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridlist").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                	{ text: '추진과제코드', datafield: 'ASSIGN_CD', width: '12%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
			                   { text: '프로그램명', datafield: 'PROGRAM_NM', width: '24%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},   
							   { text: '내용', datafield: 'QUAN_CONTENT', width: '32%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}, 
							   { text: '목표값', datafield: 'QUAN_GOAL', width: '8%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}, 
							   { text: '단위', datafield: 'QUAN_UNIT', width: '8%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}, 
							   { text: '달성값', datafield: 'QUAN_ACH_VAL', width: '8%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}, 
							   { text: '달성율', datafield: 'CALC', width: '8%', align:'center', cellsalign:'center', cellsrenderer: alginCenter}
		                ]
		            });
        		});
        		$("#gridlist").jqxGrid('clearselection');
        	}
		    	
		    	//calc
		    function calc(){
		    	var dat = $i.sqlhouse(452,{MS_ID:$("#getMission").val(),YYYY:$("#getNY").val()});
        		dat.done(function(res){
		            
        			// prepare the data
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                	    { name: 'ASSIGN_CD', type: 'string' },
			                    { name: 'PROGRAM_NM', type: 'string' },
								{ name: 'QUAN_CONTENT', type: 'string'},
								{ name: 'QUAN_UNIT', type: 'string'},
								{ name: 'QUAN_GOAL', type: 'number'},
								{ name: 'QUAN_ACH_VAL', type: 'number'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            
        		});
		    	}
        	//입력
        	
        	
        	/* function setEvent(){
        		$("#btnInsert").on("click", function(event){
        			$("#tblInsert").toggle(); 
        		}); 
        	} */
        	
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	
        	
        </script>
        <style>
        	table {
        		table-layout: fixed; /*테이블 내에서 <td>의 넓이,높이를 고정한다.*/
        		border:1px;  
        		border-color:black;
        		border-style:solid;    
        		font-family:'돋움',Dotum,Helvetica,'Apple SD Gothic Neo',Sans-serif; 
        		
        	}
        	.sql{ 
        			width:100%;
        			overflow: hidden;
			    	text-overflow:ellipsis; /*overflow: hidden; 속성과 같이 써줘야 말줄임 기능이 적용된다.*/
			    	white-space:nowrap;
        	}
        	.input{
        	height:auto !important;
        	
        	}
        	.input{
        	height:auto !important;
        	
        	}
        </style>
    </head>
    <body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 1%;">
			<form id="searchForm">
				<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
					<div class="label type1 f_left">
						비젼:
					</div> 
					<div class="combobox f_left"  id='getMission' >  
					</div>
					<div class="label type1 f_left">
						년도:  
					</div>
					<div class="combobox f_left"  id='getNY' > 
					</div>
					<div class="group f_right pos_a" style="right:0px;">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
					</div>
				</div>
			</form>
			<div class="container f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style="width:100%; margin-right:1%;">
					<div class="grid f_left" style="width:100%; height:450px;">
						<div id="gridlist"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

