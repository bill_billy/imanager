<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <title>종합현황</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        $(document).ready(function(){
	        if($("#evalStatus").val()=="L"){
				$("#resultCharge").hide(); 
			}else{  
				$("#resultCharge").show();              
			}
	            
	        if($("#statusResult").val()=="D"){   
				$("#resultEval").hide();    
			}else{  
				$("#resultEval").show(); 
			} 

        }); 

      //항목, 항목실적 
	 function mainDataResult(kpiId){      
		 var mainDataResult = $i.sqlhouse(245,{S_YYYY:"${param.yyyy}",S_EVA_GBN:"${param.pEvaGbn}",S_KPI_ID:kpiId});     
			mainDataResult.done(function(data){
				var tableDataResult = "";   
				for(var i=0;i<data.returnArray.length;i++){   
					tableDataResult += '<tr>';
					tableDataResult += '<td style="padding-left:5px !important;">'+data.returnArray[i].COL_NM+'</td>';
					tableDataResult += '<td style="padding-left:5px !important;">'+data.returnArray[i].COL_VALUE+'</td>';
					tableDataResult += '</tr>';     
				}
				if(tableDataResult==""){   
					tableDataResult += '<tr>';    
					tableDataResult += '<td colspan="2" class="cell t_center">No data to display</td>';   
					tableDataResult += '</tr>';
				}  
				$("#colListResult"+kpiId).append(tableDataResult);
			});
	 } 
		//증빙자료    
      function fileListResult(kpiId){
			var fileListResult = $i.sqlhouse(246,{S_YYYY:"${param.yyyy}",S_KPI_ID:kpiId});
			fileListResult.done(function(data){   
			if(data.returnArray.length>0){
				var html = '';       
				for(var j=0;j<data.returnArray.length;j++){
					var ext = data.returnArray[j].FILE_EXT;
					ext = ext.toLowerCase();    
					       
					var img = "";
					if(ext == 'ppt' || ext == 'pptx') {      
						img = '<img src="../../resources/cmresource/image/fileExtIcon/ppt.png" alt="ppt"/>';
					} else if(ext == 'doc' || ext == 'docx') {
						img = '<img src="../../resources/cmresource/image/fileExtIcon/doc.png" alt="word"/>';
					} else if(ext == 'hwp') {
						img = '<img src="../../resources/cmresource/image/fileExtIcon/hwp.png" alt="hwp"/>';
					} else if(ext == 'pdf') {
						img = '<img src="../../resources/cmresource/image/fileExtIcon/pdf.png" alt="pdf"/>';
					} else if(ext == 'txt') {
						img = '<img src="../../resources/cmresource/image/fileExtIcon/txt.png" alt="txt"/>';
					} else if(ext == 'xls' || ext == 'xlsx'){   
						img = '<img src="../../resources/cmresource/image/fileExtIcon/xls.png" alt="xls"/>';    
					}else if(ext == ""){   
						img = '';    
					} else {          
						img = '<img src="../../resources/cmresource/image/fileExtIcon/etc.png" alt="etc"/>';     
					}   
					
					var filename = data.returnArray[j].FILE_ORG_NAME;
					var fullFilename = filename;
					
					if(data != null && data.returnArray.length>14) {
						filename = filename.substring(0, 14) + '...';  
					}
					html += '<tr>';  
					html += "<td align='left' style='padding-left:5px !important;'><p class='label-link-table' title='"+fullFilename+"'><a href=\"javaScript:fileDownLoad('"+data.returnArray[j].FILE_ID+"','"+data.returnArray[j].KPI_ID+"','"+data.returnArray[j].YYYY+"')\";>"+ img + data.returnArray[j].FILE_ORG_NAME + "</a></p></td>";
					html += '</tr>'; 
				}      
			}else{   
				html += '<tr>';    
				html += '<td class="cell t_center">No data to display</td>';   
				html += '</tr>';
			}
			$('#fileListResult'+kpiId).append(html); 
			}); 
      }
	//증빙자료   
	function fileDownLoad(fileid,kpiId,yyyy){              
		location.replace("./kpidownload?fileid="+fileid+"&kpiId="+kpiId+"&yyyy="+yyyy);  
		
	}     
	    </script>   
    </head> 
    <body class='blueish'>                      
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%; margin:0 1%;">   
    	<div class="container  f_left"  style="width:100%; margin:10px 0;">  
			<input type="hidden" id="evaGbn" name="evaGbn" value="${param.pEvaGbn}" /> 
			<input type="hidden" id="yyyy" name="yyyy" value="${param.yyyy}"/>
			<input type="hidden" id="scNm" name="scNm" value="${param.scNm}"/>
			<input type="hidden" id="kpiNm" name="kpiNm" value="${param.kpiNm}"/>
			<input type="hidden" id="scId" name="scId" value="${param.scId}"/> 
			<input type="hidden" id="kpiId" name="kpiId" value="${param.kpiId}"/>    
			<input type="hidden" id="status" name="status" value="${param.status}"/> 
			<input type="hidden" id="mtCd" name="mtCd" value="${param.mtCd}"/> 
			<input type="hidden" id="gubn" name="gubn" value="${param.gubn}"/> 
			<input type="hidden" id="straId" name="straId" value="${param.straId}"/> 
			<input type="hidden" id="substrId" name="substrId" value="${param.substrId}"/> 
			<input type="hidden" id="csfId" name="csfId" value="${param.csfId}"/> 
			<input type="hidden" id="kpichargeId" name="kpichargeId" value="${param.kpichargeId}"/> 
			<input type="hidden" id="owneruserId" name="owneruserId" value="${param.owneruserId}"/> 
			<input type="hidden" id="evalUser" name="evalUser" value="${param.evalUser}"/> 
		<c:choose> 		   
			<c:when test="${mainList != null && not empty mainList}"> 	
				<c:forEach items="${mainList}" var="rows" varStatus="loop">
				<input type="hidden" id="evalStatus" name="evalStatus" value="${rows.STATUS}" />    
				<input type="hidden" id="statusResult" name="statusResult" value="${rows.EMP_STATUS}" />            
				<div class="content f_left" style="width:1241px; margin:5px 0 20px 0; background:#f2f6f8; border:1px solid #b3c7e0;">        
					<div class="group f_left  w100p p_b5" style="background:#b3c7e0;">  
						<div class="label type2 f_left m_l10">조직명:<span class="label sublabel" >${rows.SC_ID} &frasl; ${rows.SC_NM}</span></div>
						<div class="label type2 f_left">지표명:<span class="label sublabel">${rows.MT_CD} &frasl; ${rows.KPI_NM} </span></div>
							<div class="label sublabel" style="float:right;"><img src="../../resources/cmresource/image/icon-check-red.png" style="margin-bottom:3px;"/>
							<c:if test="${rows.EMP_STATUS == 'D'}"> ${rows.EVA_GRDNM}
							</c:if>
							</div>
					</div>
					<div class="content f_left" style="width:300px; height:402px; margin-left:17px; margin-top:12px;overflow:hidden; overflow-y:auto;"> 
						<div class="table f_left" style="width:100%; margin-bottom:10px; ">   
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<thead>
									<tr>
										<th style="width:35%;"><div>지표실적</div></th>
										<th style="width:35%;"><div>목표실적</div></th>
										<th style="width:30%;"><div>달성률</div></th>
									</tr> 
								</thead>  
							<tbody>
								<tr>
									<td align="right" style="padding-left:5px !important;" value="${rows.KPI_VALUE}">${rows.KPI_VALUE}</td>
									<td align="right" style="padding-left:5px !important;" value="${rows.TARGET_AMT}">${rows.TARGET_AMT}</td>
									<td align="right" style="padding-left:5px !important;" value="${rows.GOA}">${rows.GOA}</td>
								</tr>
							</tbody>
							</table>
						</div>
						<div class="table f_left" style="width:100%; margin-bottom:10px; ">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<thead>
									<tr>
										<th style="width:70%;"><div>항목</div></th>
										<th style="width:30%;"><div>항목실적</div></th>
								    </tr> 
								</thead>  
								<tbody id="colListResult${rows.KPI_ID}">   
									   
								</tbody>
								<script>  
									mainDataResult("${rows.KPI_ID}");                      
								</script>	 
							</table>
						</div>  
						<div class="table f_left" style="width:100%; margin-bottom:10px; ">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<thead>
									<tr>
										<th style="width:100%;"><div>증빙자료</div></th>
									</tr> 
								</thead>  
								<tbody id="fileListResult${rows.KPI_ID}">      
		              
								</tbody>
								<script>        
									fileListResult("${rows.KPI_ID}");                  
								</script>		
							</table>
						</div>
					</div>	
					
					
					<div class="content f_left" style="width:885px; margin:16px 20px 18px 19px; background:#dce7eb;">        
						<div class="group f_left" style="width:440px;">    
							<div class="group f_left  w100p m_t5">
								<div class="label type1 edit f_left m_l10">담당자 평가</div> 
									<c:if test="${rows.STATUS != 'L'}">
										<div class="f_left m_l10" style="font-weight:bold; color:red;margin-top:4px;" id="resultCharge"> [ 미승인 ] </div> 
								  	</c:if> 
							</div>
							<div class="group f_left  w100p m_t5">
								<div class="label sublabel type5  f_left m_l10">주요성과</div>
								<textarea class="textarea" name="gradeDescResult" style=" width:400px; height:145px; margin:3px; margin-left:19px; overflow-y:auto;" value="${rows.GRADE_DESC}" readonly>${rows.GRADE_DESC}</textarea>
							</div>   
							<div class="group f_left  w100p m_t5 m_b5">
								<div class="label sublabel type5  f_left m_l10">개선사항</div>
								<textarea class="textarea" name="ipvDescResult" style=" width:400px; height:145px; margin:3px; margin-left:19px; overflow-y:auto;" value="${rows.IPV_DESC}" readonly>${rows.IPV_DESC}</textarea>
							</div>
						</div>
						<div class="group f_left"  style="width:440px;">           
							<div class="group f_left  w100p m_t5">
								<div class="label type1 edit f_left m_l10">평가자 평가</div>
									<c:if test="${rows.EMP_STATUS != 'D'}">
										<div class="f_left m_l10" style="font-weight:bold; color:red;margin-top:4px;" id="resultEval"> [ 평가진행중입니다. ] </div>  
									</c:if>     
							</div>
							<div class="group f_left  w100p m_t5">
								<div class="label sublabel type5  f_left m_l10">평가근거</div>          
								<textarea class="textarea" name="kpiEvalGradeDesc" style=" width:400px; height:145px; margin:3px; margin-left:19px; overflow-y:auto;"  value="${rows.EMP_GRADE_DESC}" readonly>${rows.EMP_GRADE_DESC}</textarea>
							</div>
							<div class="group f_left  w100p m_t5 m_b5">     
								<div class="label sublabel type5  f_left m_l10">권고사항</div>    
								<textarea class="textarea" name="kpiEvalipvDesc" style=" width:400px; height:145px; margin:3px; margin-left:19px; overflow-y:auto;"  value="${rows.EMP_IPV_DESC}" readonly>${rows.EMP_IPV_DESC}</textarea>
						</div>
						</div>
					</div>
				
				</div>
				</c:forEach> 		
			</c:when>                	
		</c:choose>
		</div>    
		</div>
	</div>
	</body>
</html>

