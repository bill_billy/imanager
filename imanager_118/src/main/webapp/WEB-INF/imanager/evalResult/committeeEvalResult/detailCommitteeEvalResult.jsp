<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <title>발전계획평가_상세</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" hrlef="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        
         <style>
				 .blueish .datatable .datatable_fixed table tbody tr:nth-child(odd):hover td:not(.common){
				  	color:#223648 !important;
				  	border-color:#e1e1e1  !important;
				  	background:#ffffff !important;
				  }
				  
				  .blueish .datatable .datatable_fixed table tbody tr:nth-child(even):hover td:not(.common){
				  	color:#223648 !important;
				  	border-color:#e1e1e1  !important;  
				  	background:#f0f5f9 !important;
				  }
				  .blueish .datatable .datatable_fixed table tbody tr td.common{
				  	background:#ffffff !important;
				  
				  }
				  .blueish .datatable .datatable_fixed table tbody tr:hover td.common{
				  	color:#223648 !important;
				  	border-color:#e1e1e1  !important
				  } 
			</style>
		
    	<script type="text/javascript">
    	var ms_id = "${param.ms_id}";
    	var yyyy = "${param.yyyy}";
    	var assign_id = "${param.assign_id}";
    	console.log(ms_id + " " + yyyy + " " + assign_id);
    	
        $(this).ready(function () { 
    		getAssignEval(ms_id, yyyy, assign_id);
    		getProgramEval(ms_id, yyyy, assign_id);
        }); 
     	</script>  
     	 
     	<script type="text/javascript">
	     	function getAssignEval(ms_id, yyyy, assign_id){
	        	var dat = $i.sqlhouse(428,{MS_ID:ms_id,YYYY:yyyy,ASSIGN_ID:assign_id});
	    		dat.done(function(res){
	    			var data = res.returnArray;
	    			if(data.length>0){
	    				$("#good_content").val(data[0].GOOD_CONTENT); 
	    				$("#improve_content").val(data[0].IMPROVE_CONTENT);
	    				
	    				var result = data[0].EVAL_RESULT_NM;
	    				
	    				$("#eval_result").html(result);
	    				
	    			}
	    		});
	        }
     	 
	     	function getProgramEval(ms_id, yyyy, assign_id){
	        	console.log(ms_id + " " + yyyy + " " + assign_id);

	        	
	        	var dat = $i.sqlhouse(429,{MS_ID:ms_id,YYYY:yyyy,ASSIGN_ID:assign_id});
	    		dat.done(function(res){
	    			var data = res.returnArray;
	    			console.log(data);
	    			$("#makeProgramList").empty();  
	    			if(data.length>0){
	    				for(var i=0;i<data.length;i++){
	    					var html = "";
		    				html += '<tr name="inputEvalTr">';
		    				html += '<td style="width:15%;"><div class="cell t_center"><span>';
		    				html += data[i].PROGRAM_NM;
		    				html += '</span><input type="hidden" name="program_id" value="'+data[i].P_PROGRAM_ID+'"/></div></td>';
		    				html += '<td style="width:25%;"><div class="cell">';
		    				html += '<textarea class="textarea" name="p_good_content" style=" width:99.5%; height:50px; margin:3px 0;background:transparent;border:none;" readonly>'
		    				html += data[i].GOOD_CONTENT
		    				html += '</textarea>';
		    				html += '</div></td>';
		    				html += '<td style="width:25%;"><div class="cell">';
		    				html += '<textarea class="textarea" name="p_improve_content" style=" width:99.5%; height:50px; margin:3px 0;background:transparent;border:none;" readonly>'
			    			html += data[i].IMPROVE_CONTENT
			    			html += '</textarea>';
		    				html += '</div></td>';
		    				html += '<td style="width:25%;"><div class="cell">';
		    				html += '<textarea class="textarea" name="p_best_content" style=" width:99.5%; height:50px; margin:3px 0;background:transparent;border:none;" readonly>'
			    			html += data[i].BEST_CONTENT
			    			html += '</textarea>';
		    				html += '</div></td>';
		    				html += '<td style="width:10%;"><div class="cell t_center">';
				    		if(data[i].EVAL_SCORE != "")
		    					html += '<span>'+data[i].EVAL_SCORE+'점</span>';
		    					else html += '<span>-</span>';
		    				html += '</div></td>';
		    				html +='</tr>';
		    				$("#makeProgramList").append(html);
	    				}
	    			}
	    		});
	        }    
     	</script>
     <body class='blueish' style="overflow-X:hidden"> 
     <div class="wrap" style="width:80%; min-width:1580px; margin:0 1%;">
		<div class="container  f_left" style="width:98%; margin:0;">
				<div class="content f_left" style="width:97%; margin:10px 1.5%;">
					<div class="group f_left  w100p m_t10"> 
						<div class="group f_left  w100p">
							<div class="label type2 f_left" id="selectAssignNm2">
								평가내용  
							</div>
						</div><!--group in--> 
					</div><!--group out-->
								<div class="blueish table f_left"  style="width:60%;  margin:5px 0;">
									<table summary="입력" style="width:100%;">
										<tr>
											<th style="width:15%;"> 
												<div class="cell">잘된 점</div>
											</th>
											<td style="width:75%;">
												<div class="cell"><textarea id="good_content" name="good_content" class="textarea"  value="" style=" width:99.5%; height:65px; margin:3px 0; background:#ffff;border:none;"  readonly></textarea></div>
											</td>
											<th style="width:10%;">
												<div class="cell">평가결과<br/>(점수)</div>
											</th>						
										</tr>
										<tr>
											<th style="width:15%;">
												<div class="cell">권고 및 보완사항</div>
											</th>
											<td>
												<div class="cell"><textarea   id="improve_content" name="improve_content" class="textarea"  value="" style=" width:99.5%; height:65px; margin:3px 0;background:#ffff;border:none;" readonly ></textarea></div>
											</td>
											<td>
												<div class="cell t_center"><span id="eval_result"></span></div>
											</td>							
										</tr>
									</table> 
								</div><!-- table div -->
								
							<div class="group f_left  w100p ">
								<div class="group f_left  w100p">
									<div class="label type2 f_left">
										프로그램
									</div>
								</div><!--group in--> 
							</div><!--group out-->
							<div class="blueish datatable f_left" style="width:60%; height:400px; margin:5px 0; overflow:hidden;">      
									<!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 헤더가 50px일경우, 300px - 50px= 250px)-->
									<div class="datatable_fixed" style="width:100%; height:300px; float:left; padding:25px 0;">
										<!--group으로 헤더높이가 길어질 경우 padding  (기본높이는 25px)줘야 함-->
										<div style=" height:368px; !important; overflow-x:hidden;">   
											<table  width="100%" cellspacing="0" cellpadding="0" border="0"  class=""> 
												<thead style="width:100%;"> 
													<tr>
														<th  scope="col" style="width:15%;">프로그램</th>
														<th  scope="col" style="width:25%;">잘된 점</th> 
														<th scope="col" style="width:25%;">권고 및 보완사항</th>
														<th scope="col" style="width:25%;">우수사례</th>
														<th scope="col" style="width:10%;">평가</th>
														<th style="width:17px; min-width:17px; border-bottom:0;"></th>
													</tr>
												</thead> 
												<tbody id="makeProgramList">
												</tbody>
											</table>
										</div><!-- data div -->
									</div><!--datatable_fixed--> 
								</div><!-- //dataTable f_left -->
							</div><!--//content-->
					</div><!-- //container -->
				</div><!-- wrap -->
     		</body>
      
      