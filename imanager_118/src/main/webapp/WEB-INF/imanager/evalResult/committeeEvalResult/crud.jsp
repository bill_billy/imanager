<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	   <title id='Description'>발전계획평가결과</title>
	   	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/widget/Form.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
        <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        	 <style>
				 .blueish .datatable .datatable_fixed table tbody tr:nth-child(odd):hover td:not(.common){
				  	color:#223648 !important;
				  	border-color:#e1e1e1  !important;
				  	background:#ffffff !important;
				  }
				  
				  .blueish .datatable .datatable_fixed table tbody tr:nth-child(even):hover td:not(.common){
				  	color:#223648 !important;
				  	border-color:#e1e1e1  !important; 
				  	background:#f0f5f9 !important;
				  }
				  .blueish .datatable .datatable_fixed table tbody tr td.common{
				  	background:#ffffff !important;
				  
				  }
				  .blueish .datatable .datatable_fixed table tbody tr:hover td.common{
				  	color:#223648 !important;
				  	border-color:#e1e1e1  !important
				  } 
				  
				  .blueish .datatable .datatable_fixed table tbody tr:hover td.red{
				  	color:red !important;
				  	border-color:#e1e1e1  !important
				  } 
			</style>
			
        	<script type="text/javascript">
	            $(this).ready(function () { 
	            	makeMissionCombo();
	     
	        	missionDat.done(function(res){
	            	makeList();
				});  
					$("#BtnSrch").jqxButton({ width: '',  theme:'blueish'}).on('click',makeList); //조회 버튼
	            }); 
	            
        	</script> 
        	
        	<script type="text/javascript"> 
        	var missionList = [];
	   		var missionDat;
				function makeMissionCombo(){
					missionDat = $i.sqlhouse(322,{}); 
						missionDat.done(function(res){
							missionList = res.returnArray;
		    				var source = 
		    				{ 
			        			datatype:"json",
			        			datafields:[
			        				{ name : "MS_ID" },
			        				{ name : "MS_NM" } 
			        			],
			        			id:"id",
			        			localdata:res,
			        			async : false 
			        		};
			        		var dataAdapter = new $.jqx.dataAdapter(source);
			        		 $("#getMission").jqxComboBox({ 
			        			//	selectedIndex: 0, 
			        				source: dataAdapter, 
			        				animationType: 'fade', 
			        				dropDownHorizontalAlignment: 'right', 
			        				displayMember: "MS_NM", 
			        				valueMember: "MS_ID",  
			        				dropDownWidth: 300, 
			        				dropDownHeight: 100, 
			        				width: 300, 
			        				height: 22,  
			        				theme:'blueish'} );
			        		 
			        		$('#getMission').on('select', function (event)  
			        		{
			        			var args = event.args;
			        			if (args) {
			        				var index = args.index;
			        				makeYearCombo(index);
			        			}	     
			        		}); 
			        		 
		    			$("#getMission").jqxComboBox('selectIndex', 0 ); 
		    			
		    		});  
		 		}//makeMissionCombo 
		 	
		 		function makeYearCombo(idx){
		 			var start = missionList[idx].START_YYYY;
		 			var end = missionList[idx].END_YYYY;
		 			var nowYear = new Date().getFullYear();
		 				if(nowYear >= start && nowYear < end) //현재기준으로 설정 
		 					end = nowYear;
		 			var yyyyList = [];
		 				for(var i=Number(start);i<=end;i++){ 
		 					yyyyList.push(i);
		 				}
		     			$("#getYY").jqxComboBox({  
							source: yyyyList, 
							animationType: 'fade', 
							dropDownHorizontalAlignment: 'right', 
							dropDownWidth: 80, 
							dropDownHeight: 80, 
							width: 80, 
							height: 22,  
							theme:'blueish'});
		 			
		 			$("#getYY").jqxComboBox('selectIndex', yyyyList.length-1 );  
		    	}//makeYearCombo
        	</script>
        	
        	<script type="text/javascript">
        	function makeList(){   
				var data = $i.sqlhouse(334,{ MS_ID:$('#getMission').val(), YYYY:$('#getYY').val() });
				$("#resultTable").empty();
				data.done(function(res){     
					res = res.returnArray;
				var score = 0; 
				var straArr = [];
				for(var i=0;i<res.length;i++){ 
						var ms_id = $("#getMission").val(); 
						var yyyy = $("getYY").val();
						var assign_id = res[i].ASSIGN_ID;
						var tableList =""; 
						
						var score = res[i].EVAL_RESULT;
						if(score == ""){
							score = "-";
						}
					if(i==0 ||res[i].STRA_ID != res[i-1].STRA_ID){ 
						straArr.push(res[i].STRA_ID);
					}
					
					console.log('test:::'+res[i].SUBSTRA_SUM);
					if(i==0 || res[i].STRA_ID != res[i-1].STRA_ID){
						tableList +="<tr name='count"+res[i].STRA_ID+"'>";
						tableList += "<td align='center' name='straNm"+res[i].SUBSTRA_CD+"' class='td_side_left common rowspanStra' style='width:13%;'>"+res[i].STRA_NM+"</td>"; 
	//					tableList += "<td align='center' name='scoreA"+res[i].SUBSTRA_CD+"'class='td_side_left common rowspanStra' style='width:5%;'>"+res[i].STRA_SUM+"</td>"; 
						if(res[i].STRA_SUM < 3){
							tableList += "<td align='center' name='scoreA"+res[i].SUBSTRA_CD+"'class='td_side_left common rowspanStra red' style='color:red; width:5%;'>"+res[i].STRA_SUM+"</td>"; 
						}else{
							tableList += "<td align='center' name='scoreA"+res[i].SUBSTRA_CD+"'class='td_side_left common rowspanStra' style='width:5%;'>"+res[i].STRA_SUM+"</td>"; 
						}

						tableList += "<td align='center' name='substraCd"+res[i].SUBSTRA_CD+"'class='borderNone tr_hover odd common' style='width:7%;'>"+res[i].SUBSTRA_CD+"</td>"; 
						tableList += "<td align='center' name='substraNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd common' style='width:15%;'>"+res[i].SUBSTRA_NM+"</td>";

						if(res[i].SUBSTRA_SUM < 3){
							tableList += "<td align='center' id='scoreB"+i+"' name='scoreB"+res[i].SUBSTRA_CD+"'class='borderNone tr_hover odd common red' style='color:red; width:5%;'>"+res[i].SUBSTRA_SUM+"</td>";
						}else{
							tableList += "<td align='center' id='scoreB"+i+"' name='scoreB"+res[i].SUBSTRA_CD+"'class='borderNone tr_hover odd common' style='width:5%; '>"+res[i].SUBSTRA_SUM+"</td>"; 
						}

 						if(i%2==0){   
							tableList += "<td align='center' name='assignCd"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].ASSIGN_CD+"</td>";   
							tableList += "<td align='left' name='assignNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='padding:0 7px; width:35%; text-decoration:underline; cursor:pointer;' onclick='showAssign("+assign_id+")'>"+res[i].ASSIGN_NM+"</td>";
							tableList += "<td align='center' name='scoreC"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:5%;'>"+score+"</td>";
							tableList += "<td align='center' name='masterNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:80%;'>"+res[i].DEPT_NM+"</td>";  
						}else{ 
							tableList += "<td align='center' name='assignCd"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].ASSIGN_CD+"</td>";   
							tableList += "<td align='left' name='assignNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='padding:0 7px; width:35%; text-decoration:underline; cursor:pointer;' onclick='showAssign("+assign_id+")'>"+res[i].ASSIGN_NM+"</td>";
							tableList += "<td align='center' name='scoreC"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:5%;'>"+score+"</td>";
							tableList += "<td align='center' name='masterNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:80%;'>"+res[i].DEPT_NM+"</td>";  
						}
						tableList += "</tr>";
					}else if(res[i].STRA_ID == res[i-1].STRA_ID){
						if(res[i].SUBSTRA_CD != res[i-1].SUBSTRA_CD){
							tableList +="<tr name='count"+res[i].STRA_ID+"'>";
							tableList += "<td align='center' name='substraCd"+res[i].SUBSTRA_CD+"'class='borderNone tr_hover odd common' style='width:7%;'>"+res[i].SUBSTRA_CD+"</td>"; 
							tableList += "<td align='center' name='substraNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd common' style='width:15%;'>"+res[i].SUBSTRA_NM+"</td>";
							if(res[i].SUBSTRA_SUM < 3){
								tableList += "<td align='center' id='scoreB"+i+"' name='scoreB"+res[i].SUBSTRA_CD+"'class='borderNone tr_hover odd common red' style='color:red; width:5%;'>"+res[i].SUBSTRA_SUM+"</td>";
							}else{
								tableList += "<td align='center' id='scoreB"+i+"' name='scoreB"+res[i].SUBSTRA_CD+"'class='borderNone tr_hover odd common' style='width:5%; '>"+res[i].SUBSTRA_SUM+"</td>"; 
							}
	 						if(i%2==0){   
								tableList += "<td align='center' name='assignCd"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].ASSIGN_CD+"</td>";   
								tableList += "<td align='left' name='assignNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='padding:0 7px; width:35%; text-decoration:underline; cursor:pointer;' onclick='showAssign("+assign_id+")'>"+res[i].ASSIGN_NM+"</td>";
								tableList += "<td align='center' name='scoreC"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:5%;'>"+score+"</td>";
								tableList += "<td align='center' name='masterNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:80%;'>"+res[i].DEPT_NM+"</td>";  
							}else{ 
								tableList += "<td align='center' name='assignCd"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].ASSIGN_CD+"</td>";   
								tableList += "<td align='left' name='assignNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='padding:0 7px; width:35%; text-decoration:underline; cursor:pointer;' onclick='showAssign("+assign_id+")'>"+res[i].ASSIGN_NM+"</td>";
								tableList += "<td align='center' name='scoreC"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:5%;'>"+score+"</td>";
								tableList += "<td align='center' name='masterNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:80%;'>"+res[i].DEPT_NM+"</td>";  
							}
							tableList += "</tr>";
						}else if(res[i].SUBSTRA_CD == res[i-1].SUBSTRA_CD){    
							tableList +="<tr name='count"+res[i].STRA_ID+"'>";  
							if(i%2==0){
								tableList += "<td align='center' name='assignCd"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].ASSIGN_CD+"</td>";   
								tableList += "<td align='left' name='assignNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='padding:0 7px; width:35%; text-decoration:underline; cursor:pointer;' onclick='showAssign("+assign_id+")'>"+res[i].ASSIGN_NM+"</td>";
								tableList += "<td align='center' name='scoreC"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:5%;'>"+score+"</td>";
								tableList += "<td align='center' name='masterNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:80%;'>"+res[i].DEPT_NM+"</td>";  
							}else{  
								tableList += "<td align='center' name='assignCd"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].ASSIGN_CD+"</td>";   
								tableList += "<td align='left' name='assignNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='padding:0 7px; width:35%; text-decoration:underline; cursor:pointer;' onclick='showAssign("+assign_id+")'>"+res[i].ASSIGN_NM+"</td>";
								tableList += "<td align='center' name='scoreC"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:5%;'>"+score+"</td>";
								tableList += "<td align='center' name='masterNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:80%;'>"+res[i].DEPT_NM+"</td>";  
								}  
							tableList += "</tr>";
						} 
						
					}
					
					
					
					/*
					if(i==0 || res[i].SUBSTRA_CD != res[i-1].SUBSTRA_CD){
						tableList +="<tr name='count"+res[i].STRA_ID+"'>";
						tableList += "<td align='center' name='straNm"+res[i].SUBSTRA_CD+"' class='td_side_left common rowspanStra' style='width:13%;'>"+res[i].STRA_NM+"</td>"; 
						tableList += "<td align='center' name='scoreA"+res[i].SUBSTRA_CD+"'class='td_side_left common rowspanStra' style='width:7%;'>"+res[i].STRA_SUM+"</td>"; 
						tableList += "<td align='center' name='substraCd"+res[i].SUBSTRA_CD+"'class='borderNone tr_hover odd common' style='width:7%;'>"+res[i].SUBSTRA_CD+"</td>"; 
						tableList += "<td align='center' name='substraNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd common' style='width:15%;'>"+res[i].SUBSTRA_NM+"</td>";
						tableList += "<td align='center' id='scoreB"+i+"' name='scoreB"+res[i].SUBSTRA_CD+"'class='borderNone tr_hover odd common' style='width:7%;'>"+res[i].SUBSTRA_SUM+"</td>"; 

 						if(i%2==0){   
							tableList += "<td align='center' name='assignCd"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].ASSIGN_CD+"</td>";   
							tableList += "<td align='left' name='assignNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='padding:0 7px; width:30%; text-decoration:underline; cursor:pointer;' onclick='showAssign("+assign_id+")'>"+res[i].ASSIGN_NM+"</td>";
							tableList += "<td align='center' name='scoreC"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+score+"</td>";
							tableList += "<td align='center' name='masterNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:80%;'>"+res[i].DEPT_NM+"</td>";  
						}else{ 
							tableList += "<td align='center' name='assignCd"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].ASSIGN_CD+"</td>";   
							tableList += "<td align='left' name='assignNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='padding:0 7px; width:30%; text-decoration:underline; cursor:pointer;' onclick='showAssign("+assign_id+")'>"+res[i].ASSIGN_NM+"</td>";
							tableList += "<td align='center' name='scoreC"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+score+"</td>";
							tableList += "<td align='center' name='masterNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:80%;'>"+res[i].DEPT_NM+"</td>";  
						}
						tableList += "</tr>";
					}else if(res[i].SUBSTRA_CD == res[i-1].SUBSTRA_CD){    
						tableList +="<tr name='count"+res[i].STRA_ID+"'>";  
						if(i%2==0){
							tableList += "<td align='center' name='assignCd"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].ASSIGN_CD+"</td>";   
							tableList += "<td align='left' name='assignNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='padding:0 7px; width:30%; text-decoration:underline; cursor:pointer;' onclick='showAssign("+assign_id+")'>"+res[i].ASSIGN_NM+"</td>";
							tableList += "<td align='center' name='scoreC"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+score+"</td>";
							tableList += "<td align='center' name='masterNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:80%;'>"+res[i].DEPT_NM+"</td>";  
						}else{  
							tableList += "<td align='center' name='assignCd"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+res[i].ASSIGN_CD+"</td>";   
							tableList += "<td align='left' name='assignNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='padding:0 7px; width:30%; text-decoration:underline; cursor:pointer;' onclick='showAssign("+assign_id+")'>"+res[i].ASSIGN_NM+"</td>";
							tableList += "<td align='center' name='scoreC"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:7%;'>"+score+"</td>";
							tableList += "<td align='center' name='masterNm"+res[i].SUBSTRA_CD+"' class='borderNone tr_hover odd' style='width:80%;'>"+res[i].DEPT_NM+"</td>";  
							}  
						tableList += "</tr>";
					} */
					
					var num = $("[name=assignNm"+res[i].SUBSTRA_CD+"]").length;
					
					var substraSum = res[i].SUBSTRA_SUM;
							
						$("#resultTable").append(tableList); 
				} 
					var rowspanCount = "";
					for(var j=0;j<res.length;j++){  
						rowspanCount = $("[name=assignNm"+res[j].SUBSTRA_CD+"]").length;
						
						//로우 병합  
						$("#resultTable").find($("td[name=straNm"+res[j].SUBSTRA_CD+"]")).attr("rowSpan", rowspanCount);
						$("#resultTable").find( $("td[name=substraCd"+res[j].SUBSTRA_CD+"]")).attr("rowSpan", rowspanCount);	 
						$("#resultTable").find($("td[name=scoreA"+res[j].SUBSTRA_CD+"]")).attr("rowSpan", rowspanCount); 	
						$("#resultTable").find($("td[name=substraNm"+res[j].SUBSTRA_CD+"]")).attr("rowSpan", rowspanCount);	
						$("#resultTable").find($("td[name=scoreB"+res[j].SUBSTRA_CD+"]")).attr("rowSpan", rowspanCount);	
					}
					
					for(var j=0;j<straArr.length;j++){
						rowspanCount = $("[name=count"+straArr[j]+"]").length;
						var text = $("[name=count"+straArr[j]+"]").eq(0).find(".rowspanStra").attr("rowSpan", rowspanCount);
						
					}
				/* var rowspanCount = "";
				var rowspanCount1 = "";
				var rowspanCount2 = "";
				for(var j=0;j<res.length;j++){
					rowspanCount = $("[name=deptCd"+res[j].STRA_ID+"]").length;    
					rowspanCount2 = $("[name=empId"+res[j].STRA_ID+res[j].SUBSTRA_ID+res[j].KPI_ID+"]").length;
					$("#makeTable").find($("[name=straId"+res[j].STRA_ID+"]")).attr("rowSpan", rowspanCount);
					$("#makeTable").find($("[name=kpiNm"+res[j].STRA_ID+res[j].SUBSTRA_ID+res[j].KPI_ID+"]")).attr("rowSpan", rowspanCount2);
				}
				for(var j=0;j<res.length;j++){    
					rowspanCount1 = $("[name=countTr"+res[j].STRA_ID+res[j].SUBSTRA_ID+"]").length;
					$("#makeTable").find($("[name=substraId"+res[j].STRA_ID+res[j].SUBSTRA_ID+"]")).attr("rowSpan", rowspanCount1+1);
				} */

			});
	      }//makeList 
	         
	      function showAssign(assign_id){ 
	    		var ms_id = $("#getMission").val();
				var yyyy = $("#getYY").val();  
	     		var popUrl = "./detailCommitteeEvalResult?yyyy="+yyyy+"&ms_id="+ms_id+"&assign_id="+assign_id; 
	  	 		var popOption = "width=970, height=680, resizable=no, status=no;";    //팝업창 옵션 
	  			window.open(popUrl,"",popOption);
			}
        	</script> 
	</head> 
	<body class='blueish'> 
		<div class="wrap" style="width:98%; min-width:1060px; margin:0 1%;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<form id="srchFrm" name="srchFrm"> 	
					<div class="label type1 f_left">
						비젼:
					</div> 
					<div class="combobox f_left"  id='getMission' >  
					</div>
					<div class="label type1 f_left">
						년도:  
					</div>
					<div class="combobox f_left"  id='getYY' > 
					</div>
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='BtnSrch' width="100%" height="100%" />  
						</div>
					</div> 
						<input type="hidden" id="msid" name="msId"/>  
						<input type="hidden" id="yyyy" name="yyyy"/>    
				</form>
			</div><!-- //header -->
				<div class="container  f_left" style="width:100%;">
					<div class="content f_left" style=" width:100%; margin-right:1%;">
						<div class="blueish datatable f_left" style="width:100%; height:678px; margin:5px 0; overflow:hidden;">      
								<!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 헤더가 50px일경우, 300px - 50px= 250px)-->
								<div class="datatable_fixed" style="width:100%; height:678px; float:left; padding:25px 0;">
									<!--group으로 헤더높이가 길어질 경우 padding  (기본높이는 25px)줘야 함-->
									<div style=" height:678px; !important; overflow-x:hidden; margin-top: 0.4%;">   
										<table  width="100%" cellspacing="0" cellpadding="0" border="0"  class="">
											<thead>
												<tr>
													<th style="width:13%">전략목표</th> 
													<th style="width:5%">평가결과<br>(점수)</th> 
													<th style="width:7%">세부전략코드</th>
													<th style="width:15%">세부전략명</th>
													<th style="width:5%">평가결과<br>(점수)</th>
													<th style="width:7%">추진과제코드</th> 
													<th style="width:35%">추진과제명</th>
													<th style="width:5%">평가결과<br>(점수)</th>
													<th style="width:80%">주관부서</th>
													<th style="width:16px; min-width:16px; border-bottom:0;"></th>    	        														
												</tr>
											</thead>
											<tbody id="resultTable">
											</tbody>
										</table><!-- //table -->
									</div><!-- Table in -->
								</div><!-- dataTable --> 
							</div><!-- Table out --> 
					</div><!-- //content -->
				</div><!-- container -->
		</div><!-- //wrap -->
	</body> 
	