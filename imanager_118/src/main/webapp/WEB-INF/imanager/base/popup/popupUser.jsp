<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>담당자</title>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
	<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	<script type="text/javascript">
		//문서 로딩이 완료되면 처음 수행되는 로직
		$(document).ready(function(){
			init();
			gridUser();
			search("");
			searchCombo();
			setEvent();
		});
		function searchCombo(){
        		var dat  = [
                    {value:'1',text:'이름'},
                    {value:'2',text:'부서'}
                ];
                var source =
                {
                    datatype: "json",
                    datafields: [
                        { name: 'value' },
                        { name: 'text' }
                    ],
                    id: 'id',
                    localdata:dat,
                    async: false
                };
                var dataAdapter = new $.jqx.dataAdapter(source);
                $("#searchcombo").jqxComboBox({            
                selectedIndex: 0, 
                source: dataAdapter, 
                animationType: 'fade', 
                dropDownHorizontalAlignment: 'right', 
                displayMember: "text", 
                valueMember: "value", 
                dropDownWidth: 80, 
                dropDownHeight: 80, 
                width: 80, 
                height: 22,  
                theme:'blueish'});
             
        	}
		//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
		function init(){
			$('#btnSearch').jqxButton({width:'', theme:'blueish'});
			$txtSearch = $('#txtSearch');
			$txtSearch.jqxInput({placeHolder: '', height: 23, width: 250, minLength: 1, theme:'blueish' });
			
			//조직 그리드 리스트
			var dataDept = $i.sqlhouse(373,{S_SC_NM:$('#txtSearch').val()});
			dataDept.done(function(res){
				var source = {
					datatype: 'json',
					datafields: [
						{ name: 'SC_ID', type: 'string'},
						{ name: 'SC_NM', type: 'string'}
					],
					localdata: res,
					updaterow: function (rowid, rowdata, commit) {
						commit(true);
					}
				};
				var dataAdapter = new $.jqx.dataAdapter(source);
				
				var detail = function (row, columnfield, value, defaulthtml, columnproperties, rowdata) {
					var scid = rowdata.SC_ID;    
					var link = '<a href="javascript:search(\''+scid+'\')" style="color:black; text-decoration:underline;">'+value+'</a>';
					return '<div style="text-align:left; margin:4px 0px 0px 10px;">'+link+'</div>';
				};
				$('#gridDept').jqxGrid({
					width: '100%',
					height:'100%',
					altrows:true,
					pageable: false,
					source: dataAdapter,
					theme:'blueish',
					sortable: true,
					columnsresize: true,
					columnsheight: 25,
					rowsheight: 25,
					columns: [
						{ text: '조직명', datafield: 'SC_NM', align:'center', cellsalign: 'left',  cellsrenderer: detail}
					]
				});
			});
	
		}
		function gridUser(){
					//담당자 그리드 리스트
			var srcUser = {
				datatype: 'json',
				datafields: [
					{ name: 'DEPTCD', type: 'string'},
					{ name: 'DEPT_NM', type: 'string'},
					{ name: 'NAMEHAN', type: 'string'},
					{ name: 'EMPNO', type: 'string'},
					{ name: 'WKGD_NAME', type: 'string'},
					{ name: 'JIKWI_NM', type: 'string'}
				],
				localdata: [],
				updaterow: function (rowid, rowdata, commit) {
					commit(true);
				}
			};
			var adaUser = new $.jqx.dataAdapter(srcUser);
			
			var detail = function (row, columnfield, value, defaulthtml, columnproperties, rowdata) {
				var link = '<a href=\'javascript:sendParentData('+JSON.stringify(rowdata)+');\'" style="color:black; text-decoration:underline;">'+value+'</a>';
				return '<div style="text-align:left; margin:4px 0px 0px 10px;">'+link+'</div>';
			};
				
			$('#gridUser').jqxGrid({
				width: '100%',
				height:'100%',
				altrows:true,
				pageable: false,
				source: adaUser,
				theme:'blueish',
				sortable: true,
				columnsresize: true,
				columnsheight: 25,
				rowsheight: 25,
				columns: [
					{ text: '조직', datafield: 'DEPT_NM', width:'150px', align:'center', cellsalign: 'left'},
					{ text: '이름', datafield: 'NAMEHAN', width:'115px', align:'center', cellsalign: 'left',  cellsrenderer: detail},
					{ text: '사번', datafield: 'EMPNO', width:'90px', align:'center', cellsalign: 'center'},
					{ text: '직급', datafield: 'WKGD_NAME', width:'90px', align:'center', cellsalign: 'center'},
					{ text: '직위', datafield: 'JIKWI_NM', width:'90px', align:'center', cellsalign: 'center'},
					{ text: '조직ID', datafield: 'DEPTCD', align:'center', cellsalign: 'center'}
				]
			});
		}
		//각종 이벤트 바인딩.
		function setEvent() {
			
			//이름 검색 박스 이벤트
			$('#txtSearch').keydown(function(event){
				if(event.keyCode == 13) {
					var scid = '';
					if($("#searchcombo").val() == '1'){
						search(scid);
					}else{
						init();
					}
       			}
			});
			
			//조회 버튼
			$('#btnSearch').on('click', function() {
				var scid = '';
				if($("#searchcombo").val() == '1'){
						search(scid);
				}else{
					init();
				}
			});
		}
		//조회
		function search(scid){
			var data = $i.sqlhouse(415,{S_DEPTCD:scid, S_NAMEHAN:$('#txtSearch').val()});
			data.done(function(res){
				var $gridUser = $('#gridUser');
				$gridUser.jqxGrid('clearselection');
       			$gridUser.jqxGrid('source')._source.localdata = res;
       			$gridUser.jqxGrid('updatebounddata');
			});
		}
		//입력
		function insert(){
		}
		//삭제
		function remove(){    
		}
		//여기 부터 추가 스크립트를 작성해 주세요.
		//부모창으로 데이터 넘겨주기
		function sendParentData(data){
			//데이터 바인딩은 부모창에서 처리하되, target을 지정하여 해당 오브젝트에 데이터를 바인딩 한다. 
			data.target='${param.target}';
			data.idx='${param.idx}';
			opener.bindPopupData(data);
			self.close();
		}
	</script>
</head>
<body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
<div class="wrap" style="width:900px;min-height:300px; margin:0 1%;"><!--팝업창 사이즈 조정-->
	<div class="header f_left" style="width:100%; height: 27px; margin:10px 0;">
		<input type="hidden" id="hiddenSearchDept" />
		<div class="label type1 f_left"><div class="combobox f_left"  id="searchcombo" ></div></div>
		<div class="input f_left"><input type="text" id="txtSearch" name="searchusernm"/></div>
		<div class="group f_right pos_a" style="right:0px;">
			<div class="button type1 f_left"><input type="button" value="조회" id="btnSearch" width="100%" height="100%"/></div>
		</div>
	</div>
	<div class="container  f_left" style="width:100%; margin:10px 0;">
		<div class="content f_left" style=" width:28%; margin-right:1%;">
			<div class="grid f_left" style="width:100%; height:276px;">
				<div id="gridDept"></div>
			</div>
		</div>
		<div class="content f_right" style=" width:71%;">
			<div class="grid f_right" style="width:100%; height:276px;">
				<div id="gridUser"></div>
			</div>
		</div>
	</div>
</div>
</body>
</html>