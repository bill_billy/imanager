<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<title id='Description'>평가시기 설정</title>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	
	<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	<script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	<style type="text/css">
		.inputtable_grid {
			border:1px solid #eeeeee !important;
			background:#fefee8 !important;
			vertical-align:middle !important;
			text-align:left;
			color:#2a2f3f !important;
			margin-bottom: 1px !important;
/* 			height:17px !important; */
			line-height: 15px !important;	
		}
		.blueish .table table td{
			height:43px !important;      
		}
	</style>
		<script language="javascript" type="text/javascript">

		var pageName = "${param.pageName}"; 
		<%--
		춘해대 커스텀
		 - 커스텀파일로 새로 생성하는 것보다 관리용 테이블에서 조회 하는 방향이 어떤가 싶습니다.
		 - 관련하여 수정 된 곳이 IMPL5001_evaGbn.jsp, IMPL5001.jsp
		--%>
		var menuList = null;
/*  		if($iv.userinfo.acctid() == '343') {
			menuList = [
				[0, 'crud', '기본설정', '../basicPreferences/crud'],
				[1, 'crud_evaGbn', '평가시기 설정', '../basicPreferences/crud_evaGbn?pageName=crud_evaGbn'],
			];
		} else { */      
			menuList = [
				[0, 'crud', '기본설정', '../basicPreferences/crud'],
				[1, 'IMPL5001_appropriacy', '적절성평가 반영비율', ''],
				[2, 'IMPL5001_grade', '정성평가 등급별 비율', ''],
				[3, 'crud_evaGbn', '평가시기 설정', '../basicPreferences/crud_evaGbn?pageName=crud_evaGbn'],
				[4, 'IMPL5001_graderate', '조직점수 등급비율', ''],
				[5, 'IMPL5001_kpiscoregrade', '지표점수 등급비율', ''],
				[6, 'IMPL5001_upmuWeight', '업무평가 반영비율', ''],
				[7, 'IMPL5001_upmugraderate', '업무평가 등급비율', '']
			];
		//}
		function evaGbnList() {
			var source =
			{
				localdata: menuList,
				datafields: [
					{ name: 'index', type: 'int', map: '0'},
					{ name: 'pageName', type: 'string', map: '1'},
					{ name: 'pageKoNm', type: 'string', map: '2'},
					{ name: 'url', type: 'string', map: '3' }
				],
				datatype: "array"
			};
			
			var dataAdapter = new $.jqx.dataAdapter(source);
			
			//columns
			var cellsrenderer = function(row, datafieId, value) {
				var rowdata = $("#menuGrid").jqxGrid("getrowdata", row);
				
	        	var link = "<a href=\"javaScript:goPage('"+row+"');\">" + value + "</a>";
	        	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;'>" + link + "</div>";
	        };
	        
			$("#menuGrid").jqxGrid(
			{
				width: '100%',
				height: '100%',
				source: dataAdapter,
				sortable: false,
				theme:'blueish',
				rowsheight: 28,
				pageable: false,
				altrows:true,
				columns: [
					{ text: '구분', datafield: 'pageKoNm', width: '100%', align:'center', cellsalign: 'center', cellsrenderer:cellsrenderer }
				],
				rendered: function (type) {
					if (type == "full") {
						var selectrow = 0;
						if(pageName != '') {
							for(var i = 0; i < menuList.length; i++) {
								if(menuList[i][1] == pageName) { //menuList[i][1] = pageName
									selectrow = menuList[i][0];//menuList[i][0] = index
									break;
								}
							}
						}
						
						$('#menuGrid').jqxGrid('selectrow', selectrow);
					}
				}
			});
		}
		function goPage(index) {
			document.location.href=menuList[index][3]; //menuList[index][3] = url
		}
		var checked = true;
		function allCheck(){ 
			$('input:checkbox[type=checkbox]').each(function() {      
				this.checked = checked;
			});           
			if(checked){
				checked = false;
				$(".check").val("Y");
			}else{
				checked = true;         
				$(".check").val("N"); 
			}      
		}   
		function saveStart(){  
			var data = $i.post('./save', '#saveForm');   
    		$i.post.done(function(data){
    			if(data.returnCode == "EXCEPTION"){   
    				$i.dialog.error("SYSTEM", "저장 중 오류가 발생했습니다");
    			}else{
    				$i.dialog.alert("SYSTEM", "저장 되었습니다");   
    			}
    		});
    		
		}    
		function unChecked(evaGbn, idx) {
			if($("#checkMon"+evaGbn+idx)[0].checked == true){
				$("#mon"+evaGbn+idx).val("Y");
			}else{
				$("#mon"+evaGbn+idx).val("N");
			}
		}
		
		$(function(){   
			evaGbnList();
			$("#selectButton").jqxButton({ width: '',  theme:'blueish'});
	        $("#saveButton").jqxButton({ width: '',  theme:'blueish'});
		});
		</script>
</head>
<body class='blueish'>
	<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
	<div class="wrap" style="width:99%; margin:0 auto;">
		<div class="content f_left" style="width:100%; height:100%; float:left;  margin-top:5px;">
			<div class="content f_left" style="width:20%; height:100%; float:left; margin-right:2%;">
			<c:choose>
				<c:when test="${param.pageName == null || empty param.pageName }">
					<c:set var="pageName" value="crud" />
				</c:when>
				<c:otherwise>
					<c:set var="pageName" value="${param.pageName}" />
				</c:otherwise>
			</c:choose>
			<div class="jqx-grid-area" style=" clear:both;width:100%; margin:0px auto !important; border-left:1px solid #AAAAAA; ">
				<div style="border-left: none !important; width:100% !important; height:670px !important;" id="menuGrid" ></div>
			</div>	
			</div>			
		<div class="content f_right" style="width:67%; height:100%; float:left; margin-top:0px;">
			<div class="content f_right" style="width:100%; height:100%; float:left; margin-top:5px;">
				<div class="table f_right" style="width:100%; height:113px; float:left; margin-top:0px;">					
					<form id="saveForm" name="saveForm" method="post" action="./save">     
						<table width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-bottom:0 !important; ">
							<colgroup>         
								<col width="30%" />
								<col width="*" />
							</colgroup>
							<tr> 		   
								<th>평가구분</th>
								<th>평가시기</th>
							</tr>    
  							<c:choose>
								<c:when test="${mainList != null && not empty mainList}">
									<c:forEach items="${mainList}" var="rows" varStatus="loop">
									<tr>
										<td style="text-align: center !important;">${rows.COM_NM}</td>
										<td style="padding-left:24% !important;text-align: left">
										<div style="width:253px;float:left;margin-top:5px;margin-bottom:5px">
										<input type="hidden" name="evaGbn" value="${rows.COM_COD}" />
										<c:forEach begin="1" end="12" var="i" varStatus="s">   
	 										<c:set var="monName" value="MON${i}"></c:set>  
	 										<c:if test="${rows[monName] == 'Y'}"><c:set var="checked" value="checked"></c:set></c:if>
	 										<c:if test="${rows[monName] == 'N'}"><c:set var="checked" value=""></c:set></c:if>  
	 										<input type="hidden" class='check' id="mon${rows.COM_COD}${i}" name="mon${i}" value="${rows[monName]}" />
	 										<input type="checkbox" style="margin-left:5px;" id="checkMon${rows.COM_COD}${i}" ${checked} value="${rows[monName]}" onclick="unChecked('${rows.COM_COD}','${i}');">${i}월
										</c:forEach>                   
									    </div>           
									    </td>
									 </tr>
									</c:forEach>
								</c:when>
							<c:otherwise>
								<td colspan="2">조회된 데이터가 없습니다.</td>
							</c:otherwise>  
							</c:choose>  
						</table>       
						<div style="float:right; padding-right: 10px; padding-top:5px;">
							<input type="button" value="전체선택 및 해제" id="selectButton" width="100%" height="100%" onclick="allCheck();" />
							<input type="button" value="저장" id="saveButton" width="100%" height="100%" onclick="saveStart();" />
						</div>	
				</form>			     				
				</div>
				<!--table--> 
			</div><!--//content--> 
		</div><!--//container--> 
	</div><!--//header-->
	</div><!--//wrap-->
</body>
</html>