<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<title id='Description'>기본환경 설정</title>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	
	<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	<script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	<style type="text/css">
		.inputtable_grid {
			border:1px solid #eeeeee !important;
			background:#fefee8 !important;
			vertical-align:middle !important;
			text-align:left;
			color:#2a2f3f !important;
			margin-bottom: 1px !important;
/* 			height:17px !important; */
			line-height: 15px !important;	
		}
		.blueish .table table th{
			height:33px !important;
		}
	</style>
		<script language="javascript" type="text/javascript">

		var pageName = "${param.pageName}"; 
		<%--
		춘해대 커스텀
		 - 커스텀파일로 새로 생성하는 것보다 관리용 테이블에서 조회 하는 방향이 어떤가 싶습니다.
		 - 관련하여 수정 된 곳이 IMPL5001_evaGbn.jsp, IMPL5001.jsp
		--%>
		var menuList = null;
/* 		if($iv.userinfo.acctid() == '343') {
			menuList = [
				[0, 'crud', '기본설정', '../../basicPreferences/crud?pageName=crud'],  
				[1, 'IMPL5001_evaGbn', '평가시기 설정', '${WWW.IMANAGER}/IMPL5001_evaGbn?pageName=IMPL5001_evaGbn']
			];
		} else {   */
			menuList = [
				[0, 'crud', '기본설정', '../basicPreferences/crud'],
				[1, 'IMPL5001_appropriacy', '적절성평가 반영비율', '${WWW.IMANAGER}/IMPL5001_appropriacy?pageName=IMPL5001_appropriacy'],
				[2, 'IMPL5001_grade', '정성평가 등급별 비율', '${WWW.IMANAGER}/IMPL5001_grade?pageName=IMPL5001_grade'],
				[3, 'crud_evaGbn', '평가시기 설정', '../basicPreferences/crud_evaGbn?pageName=crud_evaGbn'],
				[4, 'IMPL5001_graderate', '조직점수 등급비율', '${WWW.IMANAGER}/IMPL5001_graderate?pageName=IMPL5001_graderate'],
				[5, 'IMPL5001_kpiscoregrade', '지표점수 등급비율', '${WWW.IMANAGER}/IMPL5001_kpiscoregrade?pageName=IMPL5001_kpiscoregrade'],
				[6, 'IMPL5001_upmuWeight', '업무평가 반영비율', '${WWW.IMANAGER}/IMPL5001_upmuWeight?pageName=IMPL5001_upmuWeight'],
				[7, 'IMPL5001_upmugraderate', '업무평가 등급비율', '${WWW.IMANAGER}/IMPL5001_upmugraderate?pageName=IMPL5001_upmugraderate']
			];
		//}
		function basicList() {
			var source =
			{
				localdata: menuList,
				datafields: [
					{ name: 'index', type: 'int', map: '0'},
					{ name: 'pageName', type: 'string', map: '1'},
					{ name: 'pageKoNm', type: 'string', map: '2'},
					{ name: 'url', type: 'string', map: '3' }
				],
				datatype: "array"
			};
			
			var dataAdapter = new $.jqx.dataAdapter(source);
			
			//columns
			var cellsrenderer = function(row, datafieId, value) {
				var rowdata = $("#menuGrid").jqxGrid("getrowdata", row);
				
	        	var link = "<a href=\"javaScript:goPage('"+row+"');\" >" + value + "</a>";
	        	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;'>" + link + "</div>";
	        };
	        
			$("#menuGrid").jqxGrid(
			{
				width: '100%',
				height: '100%',
				source: dataAdapter,
				sortable: false,
				theme:'blueish',
				rowsheight: 28,
				pageable: false,
				altrows:true,
				columns: [
					{ text: '구분', datafield: 'pageKoNm', width: '100%', align:'center', cellsalign: 'center', cellsrenderer:cellsrenderer }
				],
				rendered: function (type) {
					if (type == "full") {
						var selectrow = 0;
						if(pageName != '') {
							for(var i = 0; i < menuList.length; i++) {
								if(menuList[i][1] == pageName) { //menuList[i][1] = pageName
									selectrow = menuList[i][0];//menuList[i][0] = index
									break;
								}
							}
						}
						
						$('#menuGrid').jqxGrid('selectrow', selectrow);
					}
				}
			});
		}
		function goPage(index) {
			document.location.href=menuList[index][3]; //menuList[index][3] = url
		}
		function scNmSave(){
			if(saveForm.yyyy.value.length != 4) {
				alert("년도는 4자리 숫자를 입력해주세요!");
				return;
			} else if(saveForm.scNm.value == null || saveForm.scNm.value == "") {
				alert("평가 Root 조직명을 입력해주세요");
				return;
			}
			document.saveForm.submit();
		}
		$(function(){
			basicList();
		});
		</script>
</head>
<body class='blueish'>
	<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
	<div class="wrap" style="width:99%; margin:0 auto;">
		<div class="content f_left" style="width:100%; height:100%; float:left;  margin-top:5px;">
			<div class="content f_left" style="width:20%; height:100%; float:left; margin-right:2%;">
			<c:choose>
				<c:when test="${param.pageName == null || empty param.pageName }">
					<c:set var="pageName" value="crud" />
				</c:when>
				<c:otherwise>
					<c:set var="pageName" value="${param.pageName}" />
				</c:otherwise>
			</c:choose>
			<div class="jqx-grid-area" style=" clear:both;width:100%; margin:0px auto !important; border-left:1px solid #AAAAAA; ">
				<div style="border-left: none !important; width:100% !important; height:670px !important;" id="menuGrid" ></div>
			</div>	
			</div>			
		<div class="content f_right" style="width:67%; height:100%; float:left; margin-top:0px;">
			<div class="content f_right" style="width:100%; height:100%; float:left; margin-top:5px;">
				<div class="table f_right" style="width:100%; height:68px; float:left; margin-top:0px;">  
					<form action="../basicPreferences/crud?pageName=crud" name="saveForm" method="post">
						<table width="100%" cellspacing="0" cellpadding="0" border="0" style="margin-bottom:0 !important; ">
							<colgroup>
								<col width="30%" />
								<col width="*" />
							</colgroup>
							<tr> 		
								<th>회기시작년월</th>
								<td style="text-align:left">  
									<input name="yyyy" type="text" size="4" style="margin-left:6px; text-align:center" maxlength="4" value="${Preference.YYYY}" class="num_only inputtable_grid" />
									<select name="periodGb" class="inputtable_grid"> 
										<option value='01' <c:if test="${Preference.PERIOD_GB == '01'}">selected</c:if>>1월</option>
										<option value='02' <c:if test="${Preference.PERIOD_GB=='02'}">selected</c:if>>2월</option>
										<option value='03' <c:if test="${Preference.PERIOD_GB=='03'}">selected</c:if>>3월</option>
										<option value='04' <c:if test="${Preference.PERIOD_GB=='04'}">selected</c:if>>4월</option>
										<option value='05' <c:if test="${Preference.PERIOD_GB=='05'}">selected</c:if>>5월</option>
										<option value='06' <c:if test="${Preference.PERIOD_GB=='06'}">selected</c:if>>6월</option>
										<option value='07' <c:if test="${Preference.PERIOD_GB=='07'}">selected</c:if>>7월</option>
										<option value='08' <c:if test="${Preference.PERIOD_GB=='08'}">selected</c:if>>8월</option>
										<option value='09' <c:if test="${Preference.PERIOD_GB=='09'}">selected</c:if>>9월</option>
										<option value='10' <c:if test="${Preference.PERIOD_GB=='10'}">selected</c:if>>10월</option>
										<option value='11' <c:if test="${Preference.PERIOD_GB=='11'}">selected</c:if>>11월</option>
										<option value='12' <c:if test="${Preference.PERIOD_GB=='12'}">selected</c:if>>12월</option>
				                     </select>				  					
								</td>
							</tr>
							<tr>	 		
								<th>평가 Root 조직명</th>
									<td style="text-align:left"><input type="text" name="scNm" style="width:97.8%; margin-left:6px;" value="${scNm}" class="inputtable_grid"/></td>
							</tr>	
						</table>
						<c:choose>
							<c:when test="${scNm != null && scNm != ''}">	
								<span style="color:red;float:left;margin-left:10px;margin-top:10px;font-size: 12px;">※기본 환경 설정이 되어 있습니다.</span>
							</c:when>
							<c:otherwise>
								<span class="btn_right" style="margin-top:10px;">
									<input type="hidden" name="ac" value="save" />	
									<img src="../../button/save_btn.gif" class="button" onclick="scNmSave();" />
									<img src="../../button/cancel_btn.gif" class="button" onclick="document.lacation.reload();" />
								</span>										
							</c:otherwise>
						</c:choose>	
					</form>								
				</div>
				<!--table--> 
			</div><!--//content--> 
		</div><!--//container--> 
	</div><!--//header-->
	</div><!--//wrap-->
</body>
</html>