<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        <script src="./resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script> 
        <script>
        	$(document).ready(function(){
        		$("body").append("jQuery 로드 완료");    
        	}); 
        </script>
    </head>
    <body> 
        <h1>템플릿 페이지</h1>
        <p>(주)아이플랜비즈 프로젝트 초기 템플릿 입니다. 로그인 유저 = ${sessionScope.loginSessionInfo.userName}</p>
        <pre>
        1) 이 글은 프로젝트 생성 후에도 지우지 말아주세요.
        
        2) WEB-INF/views/
        					 /a/list.jsp
        					 /b/list.jsp
        					 /c/list.jsp	-> views폴더 바로 밑에 하위 폴더를 만들어 작업해주세요. (O)
        					 /d/a/list.jsp	-> 그 이하 레벨의 폴더를 만드는 것은 논의가 필요합니다. (X)
        					 /list.jsp	-> views폴더 바로 밑에 jsp를 만드는 것은 논의가 필요합니다. (X)
        					 
        3) 그 동안 스크립트, css, img에 대한 링크 참조를 절대 경로로 했는데 앞으로는 상대경로로 지정해 주시기 바랍니다.
        	/resources/cmresource/js/jquery/jquery-1.11.1.min.js -> 절대 경로 참조 (X)
        	../resources/cmresource/js/jquery/jquery-1.11.1.min.js -> 상대 경로 참조 (O)
        	
        4) resources폴더 밑의 commonResource폴더는 링크 디렉토리 입니다. commonResource 프로젝트를 참조 하고 있습니다.	
        	
        5) javascript 라이브러리 변경건.
        	기존의 프로젝트에서 $iv.[API]로 활용 되던 내용은 모두 변경 되었습니다.
        	접두어 : $iv -> $i
        	SqlHouse : $iv.iect9000, $iv.iect9000sync, $iv.data 등등 모두 폐지.
        	
        	$i.sqlhouse -> 기존 $iv.iect9000 와 유사합니다. 프로젝트별로 sqlhouse테이블이 다르니 유의하세요.
        				   -> 콜백 방식 변경. 
        				   	   $i.sqlhouse(500,{S_ID:"C"}).done(callback).fail(errorHandler);
        				   	   
        				   	   var step = $i.sqlhouse(500,{S_ID:"C"});
        				   	   step.done(callback);
        				   	   step.fail(error);
        				   	   //다양하게 표현 가능. 콜백 지옥에 대한 방어책.
        				   	   
        	$i.post			-> dwr 대용입니다. 앞으로 생성되는 프로젝트에 dwr은 사용하지 않습니다.
        						컨트롤에 ajax호출하세요.
        </pre>
    </body>
</html>

