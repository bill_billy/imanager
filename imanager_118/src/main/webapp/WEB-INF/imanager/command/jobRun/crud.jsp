<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<title id='Description'>작업실행관리</title>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	
	<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	<script type="text/javascript">
		//전역 변수
		
		//문서 로딩이 완료되면 처음 수행되는 로직
		$(document).ready(function(){ 
			init();
			setEvent();
		
		});
		
		//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
		function init() {
			// 버튼 초기화
			$('#btnSearch').jqxButton({ width: '',  theme:'blueish'});
		
			// jqxlorder 임시
			$('#jqxLoader').jqxLoader({ width: 100, height: 60, imagePosition: 'top' });
			
			
			// 평가구분 콤보박스
			var dataEvaGbn = $i.sqlhouse(13,{COML_COD: 'EVA_GBN'});
			dataEvaGbn.done(function(res) {
				if(res.returnCode == 'EXCEPTION') {
					alertMessage(res);
					return;
				}
			
				var source = {
					datatype: 'json',
					datafields: [
						{ name: 'COM_COD' },
						{ name: 'COM_NM' }
					],
					id: 'id',
					localdata: res,
					async: false
				};
				var dataAdapter = new $.jqx.dataAdapter(source);
				$('#cboEvaGbn').jqxComboBox({
					selectedIndex: 0,
					source: dataAdapter,
					animationType: 'fade',
					dropDownHorizontalAlignment: 'right',
					displayMember: 'COM_NM',
					valueMember: 'COM_COD',
					dropDownWidth: 150,
					dropDownHeight: 150,
					//autoDropDownHeight:true,
					width: 150,
					height: 22,
					theme:'blueish'
				});
			});
			
			// 학년도 콤보
			var dataYear = $i.sqlhouse(150, {S_COML_COD:'YEARS'});
			dataYear.done(function(res) {
				var source = {
					datatype: 'json',
					datafields: [
						{ name: 'COM_COD' },
						{ name: 'COM_NM' }
					],
					id: 'id',
					localdata: res,
					async: false
				};
				var dataAdapter = new $.jqx.dataAdapter(source);
					
				var cboYear = $('#cboYear');
				cboYear.jqxComboBox({
					selectedIndex: 0, 
					source: dataAdapter, 
					animationType: 'fade', 
					dropDownHorizontalAlignment: 'right', 
					displayMember: 'COM_NM', 
					valueMember: 'COM_COD', 
					dropDownWidth: 80, 
					dropDownHeight: 80,
					//autoDropDownHeight:true,
					width: 80, 
					height: 22,
					theme:'blueish'
				});
				cboYear.jqxComboBox({selectedIndex: 0});
			});
			
			// 평가구분, 학년도 콤보 완료 후 조회
			$.when(dataEvaGbn, dataYear).done(function(){
				search();
			});
		
		}
		
		//조회
		function search() {
			var dataJobRun = $i.select('#searchForm');
			dataJobRun.done(function(res) {
				
				if(res.returnCode == 'EXCEPTION') {
					alertMessage(res);
					return;
				}
				
				
				var list = res.returnArray;
				var size = list.length;
					
				var bodyContent = $('#bodyContent');
				bodyContent.empty();
				
				var html = '';
				
				if(size == 0) {
					html += '<tr>';
					html += '	<td colspan="4" height="312px;"><div class="cell t_center">No data to display</div></td>';
					html += '</tr>';
				} else {
					var obj = null;
					var oJobStatus = '';
					
					var statusA = false; // 상태
					var statusB = false;
					var statusC = false;
					
					var checkedA = ''; // ckecked
					var checkedB = '';
					var checkedC = '';
					
					var buttonHtml = ''; // button
					
					for(var i=0; i<size; i++) {
						obj = list[i];
						obj.idx = i;
						oJobStatus = obj.JOBSTATUS;
						
						statusA = (oJobStatus=='A')?true:false;
						statusB = (oJobStatus=='B')?true:false;
						statusC = (oJobStatus=='C')?true:false;
						
						if(statusA) {
							checkedA = 'checked';
							buttonHtml = '<input type="button" value="자료생성" id="btnJobRun'+i+'" width="100%" height="100%" style="padding:2px 10px 4px 10px !important;" class="selectorClass" onclick=\'jobRun('+JSON.stringify(obj)+');\'/>';
						} else if(statusB) {
							checkedB = 'checked';
							buttonHtml = '<input type="button" value="점수계산" id="btnJobRun'+i+'" width="100%" height="100%" style="padding:2px 10px 4px 10px !important;" class="selectorClass" onclick=\'jobRun('+JSON.stringify(obj)+');\'/>';
						} else if(statusC) {
							checkedC = 'checked';
							buttonHtml = '완료';
						}
						
						
						html += '<tr>';
						html += '	<td style="width:12%;"><div class="cell t_center">'+obj.YYYY+obj.MM+'</div></td>';
						html += '	<td style="width:45%;">';
						html += '		<div class="cell t_center">';
						html += '			<label><input type="radio" name="jobStatus'+i+'" value="A" class="m_b2 m_r3"       '+checkedA+'/>대기</label>';
						html += '			<label><input type="radio" name="jobStatus'+i+'" value="B" class="m_b2 m_r3 m_l10" '+checkedB+'/>평가중</label>';
						html += '			<label><input type="radio" name="jobStatus'+i+'" value="C" class="m_b2 m_r3 m_l10" '+checkedC+'/>마감완료</label>';
						html += '		</div>';
						html += '	</td>';
						html += '	<td style="width:20%;">';
						html += '		<div class="cell t_center">';
						html += '			<div class="button type2" style="margin:3px 0;">';
						html += '				<input type="button" value="저장" id="btnSave'+i+'" width="100%" height="100%" style="padding:2px 10px 4px 10px !important;" class="selectorClass" onclick=\'save('+JSON.stringify(obj)+');\'/>';
						html += '			</div>';
						html += '		</div>';
						html += '	</td>';
						html += '	<td style="width:20%;">';
						html += '		<div class="cell t_center">';
						html += '			<div class="button type2" style="margin:3px 0;">';
						html += buttonHtml;
						html += '			</div>';	
						html += '		</div>';
						html += '	</td>';
						html += '</tr>';
						
						
						// 초기화
						oJobStatus = '';
						checkedA = '';
						checkedB = '';
						checkedC = '';
						buttonHtml = '';
						statusA = false;
						statusB = false;
						statusC = false;
					} // end for
				} // end else
				
				bodyContent.html(html);
				
				if(size != 0) {
					$('.selectorClass').jqxButton({ width: '',  theme:'blueish'});
				}
				
			})
			.fail(function(e) {
				alertMessage(e);
			});
		}
		
		//입력
		function insert() {
		}
		
		//수정
		function update(idx,vendor) {
		} 
		
		//삭제
		function remove() {
		}
		
		//각종 이벤트 바인딩.
		function setEvent(){
			// 조회 버튼
			$('#btnSearch').on('click', function(event) {
				search();
			});

		}
		
		//테스트 함수.
		function test() {
			var async = $i.sqlhouse(4007,{S_ID:"C"},function(res){});
			async.done(function(res){ 
				console.log(res);
			}).fail(function(error){
				console.log(error); 
			});
		}
		
		//여기 부터 추가 스크립트를 작성해 주세요.
		
		// 개별 저장
		function save(o) {
			
			if(confirm('저장 하시겠습니까?')) {
				setFormData(o);
				
				var data = $i.post('./save', '#saveForm');
				data
				.done(function(res) {
					if(res.returnCode == 'EXCEPTION') {
						alertMessage(res);
						return;
					} else {
						alertMessage(res);
						$('#saveForm > input:hidden').val('');
						search();
					}
					
				})
				.fail(function(e) {
					alertMessage(e);
				});
				
			} // end confirm
		}
		
		// 자료생성 및 점수계산
		function jobRun(o) {
			
			var msg = '';
			if(o.JOBSTATUS == 'A') {
				msg = '자료를 생성 하시겠습니까?';
			} else if(o.JOBSTATUS == 'B') {
				msg = '점수계산을 실행 하시겠습니까?';
			}
			
			if(confirm(msg)) {
				setFormData(o);
				
				$('#jqxLoader').jqxLoader('open');
				var data = $i.post('./executeJobRun', '#saveForm');
				data
				.done(function(res) {
					if(res.returnCode == 'EXCEPTION') {
						alertMessage(res);
						return;
					}
				})
				.fail(function(e) {
					alertMessage(e);
				});
				
				$.when(data).done(function(){
					$('#jqxLoader').jqxLoader('close');
					
					if(o.JOBSTATUS == 'A') {
						alert('자료 생성이 완료 되었습니다.');
					} else if(o.JOBSTATUS == 'B') {
						alert('점수 계산이 완료 되었습니다.');
					}		
					
					search();
				});
			}
		}
		
		function setFormData(o) {
			$('#saveForm > input:hidden[name="evaGbn"]').val(o.EVA_GBN);
			$('#saveForm > input:hidden[name="gubun"]').val(o.GUBUN);
			$('#saveForm > input:hidden[name="yyyy"]').val(o.YYYY);
			$('#saveForm > input:hidden[name="mm"]').val(o.MM);
			$('#saveForm > input:hidden[name="jobStatus"]').val($('input:radio[name="jobStatus'+o.idx+'"]:checked').val());
		}
		
		function alertMessage(res) {
			var msg = res.returnMessage;
			if(msg == undefined) {
				msg = res.status + ' ' + res.statusText;
			}
			alert(msg);
		}
	</script>
</head>
<body class='blueish'>
	<div id="jqxLoader"></div>
	<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
	<div class="wrap" style="width:1160px; min-width:1160px; margin:0 10px;">
		<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
			<form id="searchForm" name="searchForm" action="select" method="post">
				<input type="hidden" name="gubun" value="A"/><%-- A: 실적, B: 목표 --%>
				<div class="label type1 f_left">평가구분:</div>
				<div class="combobox f_left" id="cboEvaGbn" name="evaGbn"></div>
				<div class="label type1 f_left">학년도:</div>
				<div class="combobox f_left" id="cboYear" name="year"></div>
				<div class="group_button f_right">
					<div class="button type1 f_left"><input type="button" value="조회" id="btnSearch" width="100%" height="100%" /></div>
				</div> 
			</form>
		</div><!--//header-->
		<div class="container  f_left" style="width:100%; margin:10px 0;">
			<form id="saveForm" name="saveForm" method="post">
				<input type="hidden" name="evaGbn"/>
				<input type="hidden" name="gubun"/>
				<input type="hidden" name="yyyy"/>
				<input type="hidden" name="mm"/>
				<input type="hidden" name="jobStatus"/>				
			</form>
			<div class="content f_left" style=" width:100%; margin:0%;">
				<div class="blueish datatable f_left" style="width:100%; height: 100%; margin:0px 0; overflow:hidden;"><!-- height: 600px; -->
					<div class="datatable_fixed" style="width:100%; height: 100%; float:left; padding-bottom: 0px;"><!-- height: 600px; -->
						<!--datatable과 datatable_fixed 높이 - 헤더높이(기본25px) (ex: 600px - 25px= 550px)-->
						<div style=" height:100% !important; overflow:hidden;"><!-- height: 575px; -->
							<table summary="작업실행 관리" style="width:100%;"  class="none_hover">
								<thead style="width:100%;">
									<tr>
										<th scope="col" style="width:12%;">평가년월</th>
										<th scope="col" style="width:45%;">마감상태</th>
										<th scope="col" style="width:20%;">개별저장</th>
										<th scope="col" style="width:20%;">자료생성 및 점수계산</th>
									</tr>
								</thead>
								<tbody id="bodyContent">
								</tbody>
							</table>
						</div>
					</div><!--datatable_fixed--> 
				</div><!--datatable -->  
			</div><!--//content-->
		</div><!--//container-->
	</div><!--//wrap-->
</body>
</html>