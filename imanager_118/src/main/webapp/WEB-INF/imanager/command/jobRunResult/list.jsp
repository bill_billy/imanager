<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>작업내역조회</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        
        	//조회 버튼
        	$(document).ready(function(){
        		$("#btnSearch").jqxButton({ width: '',  theme:'blueish'});
        		search();
        	}); 
        	var errCode = "${errCode}";
        	//조회
        	function search(){
        		var dat = $i.sqlhouse(177,{});      
        		dat.done(function(data){      
		            // prepare the data   
		            var source =
		            {      
		                  localdata: data.returnArray,
		                  datafields:
		                  [
		                     { name: 'YYYYMM', type: 'string' },
		                     { name: 'CAL_COD', type: 'string' },
		                     { name: 'CAL_NM', type: 'string' },
		                     { name: 'KPI_ID', type: 'string' },
		    		         { name: 'KPI_NM', type: 'string' },
		    			     { name: 'DMESSAGE', type: 'string' },
		    				 { name: 'DTINSERT', type: 'string' },
		                  ],
		                  datatype: "array"
		            };   
		            
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            var detail = function(row,datafieId,value){
		            	var ifCod = $("#gridJobResultlist").jqxGrid("getrowdata", row).IF_COD;  
		            	var link = "<a href=\"javaScript:goDetail('"+ifCod+"')\" style='color:black;'>" + value + "</a>";
		            	
		            	return "<div style='text-align:center; padding-bottom:2px; margin-top:5px;margin-left:10px; color:black;'>" + link + "</div>";
		            };
		               
		            $("#gridJobResultlist").jqxGrid(
		            {
		            	width: '100%',
						height: '100%',
		                source: dataAdapter,
		                sortable: false,
		                columnsresize:true,
						theme:'blueish',
						columnsheight:30 ,
						rowsheight: 30,
						altrows:true,   
		                ready: function () {
		                    $("#gridJobResultlist").jqxGrid();
		                },
		                columns: [    
		                          { text: '년월', dataField: 'YYYYMM', width: 200, align:'center', cellsalign: 'center' },
		                          { text: '산식ID',  dataField: 'CAL_COD', width: 200, align:'center',  cellsalign: 'center'},
		                          { text: '산식명',  dataField: 'CAL_NM', width: 300, align:'center',  cellsalign: 'center'},
		                          { text: '지표ID',  editable: false,  datafield: 'KPI_ID', width: 200, cellsalign: 'center', align:'center'},
		        				  { text: '지표명',  editable: false, dataField: 'KPI_NM', width: 300, cellsalign: 'center', align:'center' },
		        				  { text: '에러메세지',  editable: false, dataField: 'DMESSAGE', width: 403, cellsalign: 'center', align:'center' },
		        				  { text: '작업일시',  editable: false, dataField: 'DTINSERT', width: 240, cellsalign: 'center', align:'center' },
		                ]
		            });
        		});
        	}
        	
        	
        </script>   
        <style>
        	table {
        		table-layout: fixed; /*테이블 내에서 <td>의 넓이,높이를 고정한다.*/
        		border:1px;  
        		border-color:black;
        		border-style:solid;    
        		font-family:'돋움',Dotum,Helvetica,'Apple SD Gothic Neo',Sans-serif; 
        		
        	}
        	.sql{ 
        			width:100%;
        			overflow: hidden;
			    	text-overflow:ellipsis; /*overflow: hidden; 속성과 같이 써줘야 말줄임 기능이 적용된다.*/
			    	white-space:nowrap;
        	}
        </style>
    </head>
    <body class='blueish'>
		<div class="wrap" style="width:96%; margin:0 auto;">
				<div class="header f_left" style="width:100%; height:27px; float:left; margin-top:10px;">
					<div class="group f_right pos_a" style="right:0px;">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
					</div>
				</div>
			<div class="container f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style="width:100%; margin-right:1%;">
					<div class="grid f_left" style="width:100%; height:450px;">
						<div id="gridJobResultlist"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

