<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>지표실적입력</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$i.dialog.setAjaxEventPopup("SYSTEM","잠시만 기다려 주세요.");
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		$("#btnSearch").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnInsert").jqxButton({ width: '',  theme:'blueish'});
				$("#btnAllChargeY").jqxButton({ width: '', theme:'blueish'});
				$("#btnAllChargeN").jqxButton({ width: '', theme:'blueish'});
				$("#btnAllUserY").jqxButton({ width: '', theme:'blueish'});
				$("#btnAllUserN").jqxButton({ width: '', theme:'blueish'});
				$(".confirmButton").hide();
				$("#btnAllChargeY").hide();
				$("#btnAllChargeN").hide();
				$("#btnAllUserY").hide();
				$("#btnAllUserN").hide();
				if($("[name='btnConfrimY']").length > 0){
					$("[name='btnConfrimY']").jqxButton({ width: '', theme:'blueish'});
				}
				if($("[name='btnConfrimN']").length > 0){
					$("[name='btnConfrimN']").jqxButton({ width: '', theme:'blueish'});
				}
				var evaGbnDat = $i.sqlhouse(132,{S_COML_COD:"EVA_GBN", S_COM_COD:"${param.pEvaGbn}"});
				evaGbnDat.done(function(res){
					if(res.returnArray.length > 0){
						$("#spanEvaLabel").html(res.returnArray[0].COM_NM);
					}
				}); 
        		$("#cboSearchYear").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,  theme:'blueish'});
        		$("#cboSearchMm").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,  theme:'blueish'});
        		$("#cboSearchKpiID").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 250, dropDownHeight: 250, width: 250, height: 22,  theme:'blueish'});
        		$("#cboSearchScID").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 150, dropDownHeight: 150, width: 150, height: 22,  theme:'blueish'});
         		checkDate();
        	}
        	//조회 
        	function search(){
        		$("#searchForm").submit();
        	}
        	//입력
        	function insert(){
        		$i.dialog.setAjaxEventPopup("SYSTEM","잠시만 기다려 주세요.");
        		$("#hiddenAllSaveGubn").val("Kpivalue");
        		$i.post("./insert", "#allSaveForm").done(function(data){
        			if(data.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM","저장 중 오류가 발생했습니다");
        			}else{
        				$i.dialog.alert("SYSTEM","저장 되었습니다",function(){
        					search();
        				});
        			}
        		});
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function changeValue(idx){
				if($("#messageLayer").html() == ""){
					$("[name='colValue']").eq(idx).val($("[name='colValue']").eq(idx).val().replace(/[^0-9.-]/g,''));
					var returnValue = "";
					var kpiId = $("[name='colValue']").eq(idx).data("ckpiid");
					var hiddenInputBox = $("[name='colValue"+$("[name='colValue']").eq(idx).data("ckpiid")+"']");
					for(var i = 0;i<hiddenInputBox.length;i++){
						if($("[name='colValue"+$("[name='colValue']").eq(idx).data("ckpiid")+"']").eq(i).data("index") == idx){
							$("[name='colValue"+$("[name='colValue']").eq(idx).data("ckpiid")+"']").eq(i).val($("[name='colValue']").eq(idx).val());
						}
					}
					var result = "";
					for(var j=0;j<hiddenInputBox.length;j++){
						var index = $("[name='colValue"+$("[name='colValue']").eq(idx).data("ckpiid")+"']").eq(j).data("index");
						if($("[name='colValue"+$("[name='colValue']").eq(idx).data("ckpiid")+"']").eq(j).val() != ""){
							result += "var "+$("[name='colGbn']").eq(index).val()+" = "+ $("[name='colValue"+$("[name='colValue']").eq(idx).data("ckpiid")+"']").eq(j).val() + "; ";	
						}else{
							result += "var "+$("[name='colGbn']").eq(index).val()+" = 0; ";
						}
					}
					result += $("[name='hiddenCal"+kpiId+"']").val(); 
					if(eval(result) == Infinity){
						returnValue = "미입력";
					}else{
						returnValue = eval(result).toFixed(2);
					}
					$("[name='showCal"+kpiId+"']").eq(0).html(returnValue);
				}else{
					$i.dialog.warning("SYSTEM",$("#messageLayer").html());
					$("[name='colValue']").eq(idx).val($("#colValue"+idx).val().replace(/[^0-9.-]/g,''));
				}
			}
        	function checkDate(){
        		var data = $i.service("checkDateKpiResult",["AA", "${param.pEvaGbn}", $("#cboSearchYear").val()+$("#cboSearchMm").val()]);
        		data.done(function(c){
        			if(c.returnArray.length > 0){
        				if(c.returnCode=="SUCCESS"){
	        				if(c.returnArray[0].CHECK_DAT == "1"){
	        					$("#messageLayer").html("실적입력기한이 종료되었습니다.");
	        				}else if(c.returnArray[0].CHECK_DAT == "2"){
	        					$("#messageLayer").html("실적입력기한 및 확인기한이 종료되었습니다.");
	        				}
	        				confirmCheck();
	        			}else{
	        				$("#messageLayer").html("입력기간을 설정해주세요");
	        				confirmCheck();
	        			}
        			
        			}else{
        				$("#messageLayer").html("입력기간을 설정해주세요");
        				confirmCheck();
        			}
        		});
        	}
        	function confirmCheck(){
        		var data = $i.service("kpiResultCheck", ["${param.pEvaGbn}", $("#cboSearchYear").val(), $("#cboSearchMm").val()]);
        		data.done(function(res){
        			if(res.returnCode == "SUCCESS"){
        				if(res.returnArray.length == 0){
        					$(".confirmButton").hide();
        					$("#allButton").hide();
        				}else{
        					if(res.returnArray[0].DEALY_YN == "Y"){
        						$(".confirmButton").show();
        						$("#allButton").show();
        						var tbodyTr = $("#tableConfirm tbody").find("tr").length;
        						var userId = "${userInfo.UNID}";
        						for(var i=0;i<tbodyTr;i++){
        							if($("#btnAllChargeY")[0].style.display != "none" && $("#btnAllUserY")[0].style.display != "none"){
        								return;
        							}else{
        								if($("#btnAllChargeY")[0].style.display == "none"){
        									if($("[name='chargeID']").eq(i).val() == userId){
        										$("#btnAllChargeY").show();
        										$("#btnAllChargeN").show();
        									}
        								}
        								if($("#btnAllUserY")[0].style.display == "none"){
        									if($("[name='userID']").eq(0).val() == userId){
        										$("#btnAllUserY").show();
        										$("#btnAllUserN").show();
        									}
        								}
        							}
        						}
        					}else{
        						if($("#messageLayer").html() == ""){
									$("#messageLayer").append("확인기한이 종료되었습니다.");
								}
								$(".confirmButton").hide();
								$("#allButton").hide();
        					}
        				}
        			}
        		});
        	}
        	function confirmAll(confirm, gubn){
        		for(var i=0;i<$("[name='colValue']").length;i++){
        			if($("#colValueOrg"+i).val() == null || $("#colValueOrg"+i).val() == ""){
        				if(gubn == 'Charge'){
        					$("[name='userConfirm'][data-kpiid='"+$("[name='kpiID']")[i].value+"'][data-scid='"+$("[name='scID']")[i].value+"']").eq(0).val("Y");
        					$("[name='chargeConfirm'][data-kpiid='"+$("[name='kpiID']")[i].value+"'][data-scid='"+$("[name='scID']")[i].value+"']").eq(0).val("fail");
        				}else{
        					$("[name='userConfirm'][data-kpiid='"+$("[name='kpiID']")[i].value+"'][data-scid='"+$("[name='scID']")[i].value+"']").eq(0).val("Y");
        					$("[name='chargeConfirm'][data-kpiid='"+$("[name='kpiID']")[i].value+"'][data-scid='"+$("[name='scID']")[i].value+"']").eq(0).val("N");
        				}
					}else if($("#colValueOrg"+i).val() != $("[name='colValue']").eq(i).val() ){
						if(gubn == 'Charge'){
							$("[name='userConfirm'][data-kpiid='"+$("[name='kpiID']")[i].value+"'][data-scid='"+$("[name='scID']")[i].value+"']").eq(0).val("Y");
							$("[name='chargeConfirm'][data-kpiid='"+$("[name='kpiID']")[i].value+"'][data-scid='"+$("[name='scID']")[i].value+"']").eq(0).val("fail");
        				}else{
        					$("[name='userConfirm'][data-kpiid='"+$("[name='kpiID']")[i].value+"'][data-scid='"+$("[name='scID']")[i].value+"']").eq(0).val("Y");
        					$("[name='chargeConfirm'][data-kpiid='"+$("[name='kpiID']")[i].value+"'][data-scid='"+$("[name='scID']")[i].value+"']").eq(0).vql("N");
        				}
					}
        			if(gubn == 'Charge'){
        				if($("[name='chargeID'][data-kpiid='"+$("[name='kpiID']")[i].value+"'][data-scid='"+$("[name='scID']")[i].value+"']").eq(0).val() != "${userInfo.UNID}"){
        					$("[name='userConfirm'][data-kpiid='"+$("[name='kpiID']")[i].value+"'][data-scid='"+$("[name='scID']")[i].value+"']").eq(0).val("Y");
        					$("[name='chargeConfirm'][data-kpiid='"+$("[name='kpiID']")[i].value+"'][data-scid='"+$("[name='scID']")[i].value+"']").eq(0).val("fail");
	        			}
        			}else{
        				if($("[name='userID'][data-kpiid='"+$("[name='kpiID']")[i].value+"'][data-scid='"+$("[name='scID']")[i].value+"']").eq(0).val() != "${userInfo.UNID}"){
        					$("[name='userConfirm'][data-kpiid='"+$("[name='kpiID']")[i].value+"'][data-scid='"+$("[name='scID']")[i].value+"']").eq(0).val("Y");
        					$("[name='chargeConfirm'][data-kpiid='"+$("[name='kpiID']")[i].value+"'][data-scid='"+$("[name='scID']")[i].value+"']").eq(0).vql("N");
        				}
        			}
        			
        		}
        		$("#hiddenAllSaveGubn").val("All");
        		$("#hiddenAllUserGubn").val(gubn);
        		$("#hiddenAllConfirm").val(confirm);
        		$i.post("./insert", "#allSaveForm").done(function(data){
        			if(data.returnCode == "EXCEPTION"){
        				if(confirm == "Y"){
      						$i.dialog.error("SYSTEM","일괄 확인 중 오류가 발생했습니다.");
      					}else{
      						$i.dialog.error("SYSTEM","일괄 취소 중 오류가 발생했습니다.");
      					}
        			}else{
        				if(confirm == "Y"){
      						$i.dialog.alert("SYSTEM","일괄 확인 되었습니다");
      					}else{
      						$i.dialog.alert("SYSTEM","일괄 취소 되었습니다");
      					}
        				search();
        			}
        		});
        	}
        	function confirmOne(index, confirm, gubn){
        		var idx = index;
        		var check = "";
        		for(var i = 0;i<$("[name='colValue"+$("[name='oneChargeKpiID']")[index].value+"']").length;i++){
        			if($("[name='colValue"+$("[name='oneChargeKpiID']")[index].value+"']").eq(i).val() == null || $("[name='colValue"+$("[name='oneChargeKpiID']")[index].value+"']").eq(i).val() == ""){
        				check = "false";
        			}else if($("[name='colValue"+$("[name='oneChargeKpiID']")[index].value+"']").eq(i).val() != $("[name='colValue'][data-ckpiId='"+$("[name='oneChargeKpiID']")[index].value+"']").eq(i).val()){
        				check = "false";
        			}
        		}
        		if(check == "false"){       
					$i.dialog.warning("SYSTEM","실적값을 먼저 저장하셔야합니다.");
					return;
				}else{
					$("#hiddenChargeOneYyyymm").val($("[name='oneChargeYyyymm']")[index].value);
	        		$("#hiddenChargeOneKpiID").val($("[name='oneChargeKpiID']")[index].value);
	        		$("#hiddenChargeOneID").val($("[name='oneChargeID']")[index].value);
	        		$("#hiddenChargeOneName").val($("[name='oneChargeName']")[index].value);
	        		$("#hiddenChargeOneDate").val($("[name='oneChargeDate']")[index].value);
	        		if(gubn == "charge"){
	        			$("#hiddenUserGubn").val("Charge");
	        			$("#hiddenChargeOneConfirm").val(confirm);
	        			$("#hiddenUserOneConfrim").val("");
	        		}else{
	        			$("#hiddenUserGubn").val("User");
	        			$("#hiddenUserGubn").val(confirm);
	        			$("#hiddenChargeOneConfirm").val($("[name='oneChargeConfirm']")[index].value);
	        			$("#hiddenUserOneConfirm").val(confirm);
	        		}
	        		$("#hiddenUserOneID").val($("[name='oneUserID']")[index].value);
	        		$("#hiddenUserOneName").val($("[name='oneUserName']")[index].value);
	        		$("#hiddenUserOneDate").val($("[name='oneUserDate']")[index].value);
	        		$i.post("./insert", "#oneSaveForm").done(function(data){
	        			if(data.returnCode == "EXCEPTION"){
	        				if(confirm == "Y"){
	      						$i.dialog.error("SYSTEM","확인 중 오류가 발생했습니다.");
	      					}else{
	      						$i.dialog.error("SYSTEM","일괄 취소 중 오류가 발생했습니다.");
	      					}
	        			}else{
	        				if(confirm == "Y"){
	      						$i.dialog.alert("SYSTEM","확인 되었습니다");
	      					}else{
	      						$i.dialog.alert("SYSTEM","취소 되었습니다");
	      					}
	        				search();
	        			}
	        		});
				}
        	}
        	function changeYearCombo(){
        		var data = $i.service("listMmCombo",["${param.pEvaGbn}", $("#cboSearchYear").val(), "A"]);
           		data.done(function(c){ 
           			if(c.returnCode=="SUCCESS"){
           				$("#cboSearchMm").jqxComboBox("clear");
           				for(var i=0;i<c.returnArray.length;i++){
           					$("#cboSearchMm").jqxComboBox("insertAt", {label:c.returnArray[i].MM_NM, value:c.returnArray[i].MM_ID},i);
           				}
           				$("#cboSearchMm").jqxComboBox("selectIndex", 0);
           			}else{
           				$("#cboSearchMm").jqxComboBox("clear");
           				$("#cboSearchMm").jqxComboBox("insertAt", {label:"==선택==",value:""},0);
           			} 
           		});
        	}
        	function chageeMmCombo(){
        		var data = $i.service("listKpiCombo",["${param.pEvaGbn}", $("#cboSearchYear").val()+$("#cboSearchMm").val()]);
        		data.done(function(c){
        			if(c.returnCode == "SUCCESS"){
        				$("#cboSearchKpiID").jqxComboBox("clear");
        				for(var i=0;i<c.returnArray.length;i++){
        					$("#cboSearchKpiID").jqxComboBox("insertAt",{label:c.returnArray[i].MT_NM, value:c.returnArray[i].MT_ID},i);
        				}
        				$("#cboSearchKpiID").jqxComboBox("selectIndex", 0);
        			}else{
        				$("#cboSearchKpiID").jqxComboBox("clear");
        				$("#cboSearchKpiID").jqxComboBox("insertAt", {label:"==선택==",value:""},0);
        			}
        		});
        	}
        	function changeKpiCombo(){
        		var dataScid = $i.service("listScidCombo",["${param.pEvaGbn}", "", $("#cboSearchYear").val()+$("#cboSearchMm").val(), $("#cboSearchKpiID").val()]);
           		dataScid.done(function(data){
           			if(data.returnCode == "SUCCESS"){
           				$("#cboSearchScID").jqxComboBox("clear");
           				for(var i=0;i<data.returnArray.length;i++){
           					$("#cboSearchScID").jqxComboBox("insertAt", {label:data.returnArray[i].SC_NM, value:data.returnArray[i].SC_ID},i);
           				}
           				$("#cboSearchScID").jqxComboBox("selectIndex", 0);
           			}else{
           				$("#cboSearchScID").jqxComboBox("clear");
           				$("#cboSearchScID").jqxComboBox("insertAt", {label:"==선택==", value:""},0);
           			}
           		});
        	}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
		</style>
		   
    </head>
    <body class='blueish'>
		<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:1160px; min-width:1160px; margin:0 10px;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<form id="searchForm" name="searchForm" action="./crud">
					<input type="hidden" id="hiddenEvaGbn" name="pEvaGbn" value="${param.pEvaGbn}" />
					<div class="label type1 f_left">평가구분:<span class="label sublabel" id="spanEvaLabel"></span></div>
					<div class="label type1 f_left">년월:</div>
					<div class="combobox f_left" onchange="changeYearCombo();">
						<select id="cboSearchYear" name="searchYear">
							<c:choose>
								<c:when test="${listYear != null && not empty listYear}">
									<c:forEach items="${listYear}" var="key" varStatus="loop">
										<option value="${key.YYYY_ID}" <c:if test="${key.YYYY_ID == param.searchYear}">selected</c:if>>${key.YYYY_NM}</option>
									</c:forEach>
								</c:when>
							</c:choose>
						</select>
					</div>
					<div class="combobox f_left" onchange="chageeMmCombo();">
						<select id="cboSearchMm" name="searchMm">
							<c:choose>
								<c:when test="${listMm != null && not empty listMm}">
									<c:forEach items="${listMm}" var="key" varStatus="loop">
										<option value="${key.MM_ID}" <c:if test="${key.MM_ID == param.searchMm}">selected</c:if>>${key.MM_NM}</option>
									</c:forEach>
								</c:when>
							</c:choose>
						</select>
					</div>
					<div class="label type1 f_left">지표:</div>
					<div class="combobox f_left" onchange="changeKpiCombo();">
						<select id="cboSearchKpiID" name="searchKpiID">
							<c:choose>
								<c:when test="${listKpiCombo != null && not empty listKpiCombo}">
									<c:forEach items="${listKpiCombo}" var="key" varStatus="loop">
										<option value="${key.MT_ID}" <c:if test="${key.MT_ID == param.searchKpiID}">selected</c:if>>${key.MT_NM}</option>
									</c:forEach>
								</c:when>
							</c:choose>
						</select>
					</div>
					<div class="label type1 f_left">조직:</div>
					<div class="combobox f_left">
						<select id="cboSearchScID" name="searchScID">
							<c:choose>
								<c:when test="${listScCombo != null && not empty listScCombo}">
									<c:forEach items="${listScCombo}" var="key" varStatus="loop">
										<option value="${key.SC_ID}" <c:if test="${key.SC_ID == param.searchScID}">selected</c:if>>${key.SC_NM}</option>
									</c:forEach>
								</c:when>
							</c:choose>
						</select>
					</div>
					<div class="label type1 f_left">담당자&frasl;승인자:<span class="label sublabel">${userInfo.NAMEHAN}</span></div>
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();"/>
						</div>
						<div class="button type1 f_left">
							<input type="button" value="저장" id='btnInsert' width="100%" height="100%" onclick="insert();"/>
						</div>
					</div>
				</form>
			</div>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:100%; margin:0%;">
					<div class="group f_right  w100p m_b5">
						<div class="label type3 f_right" id="messageLayer"></div>
						<div id="allButton" class="group_button f_right">
							<div class="button type2 f_left">
								<input type="button" value="담당자 일괄확인" id="btnAllChargeY" width="100%" height="100%" style="text-indent:0 !important; margin:2px 0 !important;" class='button type2' onclick="confirmAll('Y', 'Charge');"  />
							</div>
							<div class="button type3 f_left">
								<input type="button" value="담당자 일괄취소" id="btnAllChargeN" width="100%" height="100%" style="text-indent:0 !important; margin:2px 0 !important;" class='button type3' onclick="confirmAll('N', 'Charge');"  />
							</div>
							<div class="button type2 f_left">
								<input type="button" value="승인자 일괄확인" id="btnAllUserY" width="100%" height="100%" style="text-indent:0 !important; margin:2px 0 !important;" class='button type2' onclick="confirmAll('Y', 'User');"  />
							</div>
							<div class="button type3 f_left">
								<input type="button" value="승인자 일괄취소" id="btnAllUserN" width="100%" height="100%" style="text-indent:0 !important; margin:2px 0 !important;" class='button type3' onclick="confirmAll('N', 'User');"  />
							</div>
						</div>
					</div>
					<div class="blueish datatable f_left" style="width:100%; height:600px; margin:0px 0; overflow:hidden;">
						<div class="datatable_fixed" style="width:100%; height:600px; float:left; padding:50px 0;"><!--group으로 헤더높이가 길어질 경우 padding  (기본높이는 25px)줘야 함-->
							<div style=" height:550px !important; overflow-x:hidden;"><!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 600px - 50px= 550px)-->
								<form id="allSaveForm" name="allSaveForm" action="./insert">
									<input type="hidden" id="hiddenAllSaveGubn" name="saveGubn" />
									<input type="hidden" id="hiddenAllConfirm" name="confirm" />
									<input type="hidden" id="hiddenAllUserGubn" name="userGubn" /> 
									<input type="hidden" id="hiddenAllpEvaGbn" name="pEvaGbn" value="${param.pEvaGbn}" />
									<table id="tableConfirm" summary="교원평가 대상설정" style="width:100%;"  class="none_hover none_alt">
										<thead style="width:100%;">
											<tr>
												<th rowspan="2" scope="col" style="width:12%;">지표</th>
												<th rowspan="2"  scope="col" style="width:10%;">조직</th>
												<th rowspan="2"  scope="col" style="width:5%;">단위</th>
												<th rowspan="2"  scope="col" style="width:6%;">목표</th>
												<th rowspan="2"  scope="col" style="width:6%;">실적</th>
												<th colspan="3" scope="col" style="width:18%;">담당자</th>
												<th colspan="3" scope="col" style="width:18%;">승인자</th>
												<th colspan="4" scope="col" style="width:23%;">항목</th>
												<th style="width:1%; min-width:17px;"></th>
											</tr>
											<tr>
												<th scope="col" style="width:6%;">이름</th>
												<th scope="col" style="width:6%;">확인여부</th>
												<th scope="col" style="width:6%;">일자</th>
												<th scope="col" style="width:6%;">이름</th>
												<th scope="col" style="width:6%;">확인여부</th>
												<th scope="col" style="width:6%;">일자</th>
												<th scope="col" style="width:9%;">항목명</th>
												<th scope="col" style="width:3%;">단위</th>
												<th scope="col" style="width:5%;">입력..</th>
												<th scope="col" style="width:6%;">실적</th>
												<th style="width:1%; min-width:17px;"></th>
											</tr>
										</thead>
										<tbody>
											<c:choose>
												<c:when test="${mainList != null && not empty mainList}">
													<c:forEach items="${mainList}" var="mainList" varStatus="loop">
														<tr>
														<c:if test="${loop.count == 1 || kpiRowspan == 0}">
														<c:set var="kpiRowspan" value="${mainList.KPI_ROW_COUNT}" />
															<td rowspan="${kpiRowspan}" style="width:12%">
																<div class="cell">${mainList.KPI_NM}</div>
															</td>
														</c:if>
														<c:set var="kpiRowspan" value="${kpiRowspan-1}" />
														<c:if test="${loop.count == 1 || scRowspan == 0}">
														<c:set var="scRowspan" value="${mainList.SC_ROW_COUNT}" />
															<td rowspan="${scRowspan}" style="width:10%;">
																<input type="hidden" name="chargeYyyymm"  value="${mainList.YYYYMM}" />	
																<input type="hidden" name="chargeID" data-kpiid="${mainList.KPI_ID}" data-scid="${mainList.SC_ID}" value="${mainList.KPI_CHARGE_ID}" />
																<input type="hidden" name="chargeName" value="${mainList.KPI_CHARGE_NM}" />
																<input type="hidden" name="chargeDate" value="${mainList.CHARGE_DATE}" />
																<input type="hidden" name="userID" data-kpiid="${mainList.KPI_ID}" data-scid="${mainList.SC_ID}" value="${mainList.OWNER_USER_ID}" />
																<input type="hidden" name="userName" value="${mainList.OWNER_USER_NM}" />
																<input type="hidden" name="userDate" value="${mainLIst.USER_DATE}" />
																<input type="hidden" name="chargeKpiID" value="${mainList.KPI_ID}" />
																<input type="hidden" name="chargeConfirm" data-kpiid="${mainList.KPI_ID}" data-scid="${mainList.SC_ID}" value="${mainList.CHARGE_CONFIRM}" />
																<input type="hidden" name="userConfirm" data-kpiid="${mainList.KPI_ID}" data-scid="${mainList.SC_ID}" value="${mainList.USER_CONFRIM}" />
																<div class="cell t_center">${mainList.SC_NM}</div>
															</td>
															<td rowspan="${scRowspan}" style="width:5%;">
																<div class="cell t_center">${mainList.UNIT_NM}</div>
															</td>
															<td rowspan="${scRowspan}" style="width:6%;">
																<div class="cell t_right">${mainList.TARGET_AMT}</div>
															</td>
															<td rowspan="${scRowspan}" style="width:6%;">
																<input type="hidden" name="hiddenCal${mainList.KPI_ID}" value="${mainList.CAL}" />
																<div class="cell t_center">${mainList.CAL_VNM}</div>
															</td>
															<td rowspan="${scRowspan}" style="width:6%;">
																<div class="celll t_center">${mainList.KPI_CHARGE_NM}</div>
															</td>
															<td rowspan="${scRowspan}" style="width:6%;">
																<c:if test="${mainList.KPI_CHARGE_ID == userInfo.UNID}">
																	<c:if test="${mainList.USER_CONFIRM == '' || mainList.USER_CONFIRM == 'N' }">
																		<c:if test="${mainList.CHARGE_CONFIRM == '' || mainList.CHARGE_CONFIRM == 'N'}">
																			<div class="cell t_center">
																				미확인
																				<div class="button type2" style="margin:3px 0;">
																					<input type="button" value="확인" class="confirmButton" name="btnConfrimY" width="100%" height="100%" style="padding:2px 5px 4px 6px !important;" onclick="confirmOne('${loop.index}','Y','charge');" />
																				</div>
																			</div>
																		</c:if>
																		<c:if test="${mainList.CHARGE_CONFIRM == 'Y'}">
																			<div class="cell t_center">
																				확인
																				<div class="button type3" style="margin:3px 0;">
																					<input type="button" value="확인 취소" class="confirmButton" name="btnConfrimN" width="100%" height="100%" style="padding:2px 5px 4px 6px !important;" onclick="confirmOne('${loop.index}','N','charge');" />
																				</div>
																			</div>
																		</c:if>
																	</c:if>
																	<c:if test="${mainList.USER_CONFIRM == 'Y'}">
																		<div class="cell t_center">확인</div>
																	</c:if>
																</c:if>
																<c:if test="${mainList.KPI_CHARGE_ID != userInfo.UNID}">
																	<c:if test="${mainList.CHARGE_CONFIRM == '' || mainList.CHARGE_CONFIRM == 'N' }">
																		<div class="cell t_center">미확인</div>
																	</c:if>
																	<c:if test="${mainList.CHARGE_CONFIRM == 'Y'}">
																		<div class="cell t_center">확인</div>
																	</c:if>
																</c:if>
															</td>	
															<td rowspan="${scRowspan}" style="width:6%;">
																<c:if test="${mainList.CHARGE_CONFIRM == 'Y' }">
																	<div class="cell t_center">
																		${mainList.CHARGE_DATE}
																		</br>
																		${mainList.LAST_CHARGE_NM}
																	</div>
																</c:if>
																
															</td>	
															<td rowspan="${scRowspan}" style="width:6%;">
																<div class="cell t_center">${mainList.OWNER_USER_NM}</div>
															</td>
															<td rowspan="${scRowspan}" style="width:6%;">
																<c:if test="${mainList.OWNER_USER_ID == userInfo.UNID}">
																	<c:if test="${mainList.CHARGE_CONFIRM == '' || mainList.CHARGE_CONFIRM == 'N'}">
																		<div class="cell t_center">담당자 미확인</div>
																	</c:if>
																	<c:if test="${mainList.CHARGE_CONFIRM == 'Y'}">
																		<c:if test="${mainList.USER_CONFIRM == '' || mainList.USER_CONFIRM == 'N'}">
																			<div class="cell t_center">
																				미확인
																				<div class="button type2" style="margin:3px 0;">
																					<input type="button" value="확인" name="btnConfrimY" class="confirmButton" width="100%" height="100%" style="padding:2px 5px 4px 6px !important;" onclick="confirmOne('${loop.index}','Y','user');" />
																				</div>
																			</div>
																		</c:if>
																		<c:if test="${mainList.USER_CONFIRM == 'Y'}">
																			<div class="cell t_center">
																				확인
																				<div class="button type3" style="margin:3px 0;">
																					<input type="button" value="확인 취소" name="btnConfrimN" class="confirmButton" width="100%" height="100%" style="padding:2px 5px 4px 6px !important;" onclick="confirmOne('${loop.index}','N','user');" />
																				</div>
																			</div>
																		</c:if>
																	</c:if>
																</c:if>
																<c:if test="${mainList.OWNER_USER_ID != userInfo.UNID}">
																	<c:if test="${mainList.CHARGE_CONFIRM == '' || mainList.CHARGE_CONFIRM == 'N'}">
																		<div class="cell t_center">담당자 미확인</div>
																	</c:if>
																	<c:if test="${mainList.CHARGE_CONFIRM == 'Y'}">
																		<c:if test="${mainList.USER_CONFIRM == '' || mainList.USER_CONFIRM == 'N'}">
																			<div class="cell t_center">미확인</div>
																		</c:if>
																		<c:if test="${mainList.USER_CONFIRM == 'Y'}">
																			<div class="cell t_center">확인</div>
																		</c:if>
																	</c:if>
																</c:if>
															</td>	
															<td rowspan="${scRowspan}" style="width:6%;">
																<c:if test="${mainList.USER_CONFIRM == 'Y' }">
																	<div class="cell t_center">
																		${mainList.USER_DATE}
																		</br>
																		${mainList.LAST_USER_NM}
																	</div>
																</c:if>
															</td>
														</c:if>
														<c:set var="scRowspan" value="${scRowspan-1}" />
														<td style="width:9%;">
															<div class="cell">${mainList.COL_NM}</div>
														</td>
														<td style="width:3%;">
															<div class="cell t_center">${mainList.COL_UNIT}</div>
														</td>	
														<td style="width:5%;">
															<div class="cell t_center">${mainList.COL_SYSTEM_NM}</div>
														</td>
														<td style="width:6%;">
															<input type="hidden" name="yyyymm" value="${mainList.YYYYMM}" />
															<input type="hidden" name="kpiID" value="${mainList.KPI_ID}" />
															<input type="hidden" name="scID" value="${mainList.SC_ID}" />
															<input type="hidden" name="analCycle" value="${mainList.ANAL_CYCLE}" />
															<input type="hidden" name="colGbn" value="${mainList.COL_GBN}" />
															<input type="hidden" name="oneChargeYyyymm" value="${mainList.YYYYMM}" />
															<input type="hidden" name="oneChargeConfirm" value="${mainList.CHARGE_CONFIRM}" />
															<input type="hidden" name="oneUserConfirm" value="${mainList.USER_CONFIRM}" />
															<input type="hidden" name="oneChargeID" value="${mainList.KPI_CHARGE_ID}" />
															<input type="hidden" name="oneChargeName" value="${mainList.KPI_CHARGE_NM}" />
															<input type="hidden" name="oneChargeDate" value="${mainList.CHARGE_DATE}" />
															<input type="hidden" name="oneUserID" value="${mainList.OWNER_USER_ID}" />
															<input type="hidden" name="oneUserName" value="${mainList.OWNER_USER_NM}" />
															<input type="hidden" name="oneUserDate" value="${mainLIst.USER_DATE}" />
															<input type="hidden" name="oneChargeKpiID" value="${mainList.KPI_ID}" />
															<input type="hidden" id="colValueOrg${loop.index}" value="${mainList.COL_VALUE}" />
															<input type="hidden" id="colValue${loop.index}" name="colValue${mainList.KPI_ID}" data-index="${loop.index }" value="${mainList.COL_VALUE}" />
															<c:if test="${mainList.KPI_CHARGE_ID == userInfo.UNID}" >
																<c:if test="${mainList.COL_SYSTEM == 'A'}">
																	<c:if test="${mainList.CHARGE_CONFIRM == '' || mainList.CHARGE_CONFIRM == 'N' }">
																		<div class="cell t_center">    
																			<input type="text" class="input type2  f_left t_right" data-ckpiId="${mainList.KPI_ID}" data-gbn="${mainList.COL_GBN}" onkeyup="changeValue('${loop.index}');"  name="colValue" value="${mainList.COL_VALUE}" style="width:90%; text-align:right !important; text-indent:0 !important;" />
																		</div>
																	</c:if>
																	<c:if test="${mainList.CHARGE_CONFIRM == 'Y'}">
																		<div class="cell t_center">
																			<input type="text" data-ckpiId="${mainList.KPI_ID}" data-gbn="${mainList.COL_GBN}" name="colValue" value="${mainList.COL_VALUE}" style="width:90%; text-align:right !important; text-indent:0 !important; border:0;" readonly="readonly"/>
																		</div>
																	</c:if>
																</c:if>
																<c:if test="${mainList.COL_SYSTEM != 'A'}">
																	<div class="cell t_center">
																		<input type="text" data-ckpiId="${mainList.KPI_ID}" data-gbn="${mainList.COL_GBN}" name="colValue" value="${mainList.COL_VALUE}" style="width:90%; text-align:right !important; text-indent:0 !important; border:0;" readonly="readonly"/>
																	</div>
																</c:if>
															</c:if>
															<c:if test="${mainList.KPI_CHARGE_ID != userInfo.UNID}">
																<div class="cell t_center">
																	<input type="text" data-ckpiId="${mainList.KPI_ID}" data-gbn="${mainList.COL_GBN}" name="colValue" value="${mainList.COL_VALUE}" style="width:90%; text-align:right !important; text-indent:0 !important; border:0;" readonly="readonly"/>
																</div>
															</c:if>
														</td>
													</c:forEach>
												</c:when>
											</c:choose>
										</tbody>
									</table>
								</form>
								<form id="oneSaveForm" name="oneSaveForm" action="./insert">
									<input type="hidden" id="hiddenSaveGubn" name="saveGubn" value="One" />
									<input type="hidden" id="oneEvaGbn" name="pEvaGbn" value="${param.pEvaGbn}" />
									<input type="hidden" id="hiddenUserGubn" name="userGubn" />
									<input type="hidden" id="hiddenChargeOneYyyymm" name="chargeOneYyyymm" />
									<input type="hidden" id="hiddenChargeOneKpiID" name="chargeOneKpiID" />
									<input type="hidden" id="hiddenChargeOneID" name="chargeOneID" />
									<input type="hidden" id="hiddenChargeOneName" name="chargeOneName" />
									<input type="hidden" id="hiddenChargeOneConfirm" name="chargeOneConfirm" />
									<input type="hidden" id="hiddenChargeOneDate" name="chargeOneDate" />
									<input type="hidden" id="hiddenUserOneID" name="userOneID" />
									<input type="hidden" id="hiddenUserOneName" name="userOneName" />
									<input type="hidden" id="hiddenUserOneConfirm" name="userOneConfirm" />
									<input type="hidden" id="hiddenUserOneDate" name="userOneDate" />
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>