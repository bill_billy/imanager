<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>지표목표입력</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		$("#btnSearch").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnSave").jqxButton({ width: '',  theme:'blueish'});
				$("#btnAllChargeY").jqxButton({ width: '', theme:'blueish'});
				$("#btnAllChargeN").jqxButton({ width: '', theme:'blueish'});
				$("#btnAllUserY").jqxButton({ width: '', theme:'blueish'});
				$("#btnAllUserN").jqxButton({ width: '', theme:'blueish'});
				$(".confirmButton").hide();
				$("#btnAllChargeY").hide();
				$("#btnAllChargeN").hide();
				$("#btnAllUserY").hide();
				$("#btnAllUserN").hide();
				if($("[name='btnConfrimY']").length > 0){
					$("[name='btnConfrimY']").jqxButton({ width: '', theme:'blueish'});
				}
				if($("[name='btnConfrimN']").length > 0){
					$("[name='btnConfrimN']").jqxButton({ width: '', theme:'blueish'});
				}
				var evaGbnDat = $i.sqlhouse(139,{S_COML_COD:"EVA_GBN", S_COM_COD:"B"});
				evaGbnDat.done(function(res){
					if(res.returnArray.length > 0){
						$("#spanEvaLabel").html(res.returnArray[0].COM_NM);
					}
				}); 
        		$("#cboSearchYear").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,  theme:'blueish'}); 
        		$("#cboSearchKpi").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 250, dropDownHeight: 250, width: 250, height: 22,  theme:'blueish'});
        		$("#cboSearchScID").jqxComboBox({ animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 150, dropDownHeight: 150, width: 150, height: 22,  theme:'blueish'});
        		checkDate();
        	}
        	//조회 
        	function search(){
        		$("#searchForm").submit();
        	}
        	//입력
        	function insert(){
        		$("#hiddenAllSaveGubn").val("Kpivalue");
        		$i.post("./insert", "#allSaveForm").done(function(data){
        			if(data.returnCode == "EXCEPTION"){
        				alert("저장 중 오류가 발생했습니다");
        			}else{
        				alert("저장 되었습니다");
        				search();
        			}
        		});
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function calValueCheck(idx){
				if($("#messageLayer").html() != ""){
					alert($("#messageLayer").html());
					$("#targetAmt"+idx).val($("#targetAmt"+idx).val());
				}
			}
        	function checkDate(){
        		var data = $i.service("checkDate",["AB", "${param.pEvaGbn}", $("#cboSearchYear").val()]);
        		data.done(function(c){
        			if(c.returnCode=="SUCCESS"){
        				if(c.returnArray.length > 0){
        					if(c.returnArray[0].CHECK_DAT == "1"){
	        					$("#messageLayer").html("목표입력기한이 종료되었습니다");
	        				}else if(c.returnArray[0].CHECK_DAT == "2"){
	        					$("#messageLayer").html("목표입력기한 및 확인기한이 종료되었습니다.");
	        				}
	        				confirmCheck();
        				}else{
        					$("#messageLayer").html("입력기간을 설정해주세요");
        					confirmCheck();
        				}
        			}else{
        				$("#messageLayer").html("입력기간을 설정해주세요");
        				confirmCheck();
        			}
        		});
        	}
        	function confirmCheck(){
        		var data = $i.service("kpiTargetCheck", ["${param.pEvaGbn}", $("#cboSearchYear").val()]);
        		data.done(function(res){
        			if(res.returnCode == "SUCCESS"){
        				if(res.returnArray.length == 0){
        					$(".confirmButton").hide();
        					$("#allButton").hide();
        				}else{
        					if(res.returnArray[0].DEALY_YN == "Y"){
        						$(".confirmButton").show();
        						$("#allButton").show();
        						var tbodyTr = $("#tableConfirm tbody").find("tr").length;
        						var userId = "${userInfo.UNID}";
        						for(var i=0;i<tbodyTr;i++){
        							if($("#btnAllChargeY")[0].style.display != "none" && $("#btnAllUserY")[0].style.display != "none"){
        								return;
        							}else{
        								if($("#btnAllChargeY")[0].style.display == "none"){
        									if($("[name='chargeID']").eq(i).val() == userId){
        										$("#btnAllChargeY").show();
        										$("#btnAllChargeN").show();
        									}
        								}
        								if($("#btnAllUserY")[0].style.display == "none"){
        									if($("[name='userID']").eq(0).val() == userId){
        										$("#btnAllUserY").show();
        										$("#btnAllUserN").show();
        									}
        								}
        							}
        						}
        					}else{
        						if($("#messageLayer").html() == ""){
									$("#messageLayer").append("확인기한이 종료되었습니다.");
								}
								$(".confirmButton").hide();
								$("#allButton").hide();
        					}
        				}
        			}
        		});
        	}
        	function confirmAll(confirm, gubn){
        		for(var i=0; i<$("[name='targetAmt']").length;i++){
        			if(($("#targetAmt"+i).val() == null || $("#targetAmt"+i).val() == "")){
        				if(gubn == "Charge"){
        					$("[name='userConfirm']").eq(i).val("Y");
        				}else{
        					$("[name='chargeConfirm']").eq(i).val("N");
        				}
					}else if($("#targetAmt"+i).val() != $("[name='targetAmt']").eq(i).val()){
						if(gubn == "Charge"){
        					$("[name='userConfirm']").eq(i).val("Y");
        				}else{
        					$("[name='chargeConfirm']").eq(i).val("N");
        				}
					}
        			if(gubn == "Charge"){
        				if($("[name='chargeID']").eq(i).val() != "${userInfo.UNID}"){
        					$("[name='userConfirm']").eq(i).val("Y");
        				}
        			}else{
        				if($("[name='userID']").eq(i).val() != "${userInfo.UNID}"){
        					$("[name='chargeConfirm']").eq(i).val("N");
        				}
        			}
        		}
        		$("#hiddenAllSaveGubn").val("All");
        		$("#hiddenAllUserGubn").val(gubn);
        		$("#hiddenAllConfirm").val(confirm);
        		$i.post("./insert", "#allSaveForm").done(function(data){
        			if(data.returnCode == "EXCEPTION"){
        				if(confirm == "Y"){
      						alert("일괄 확인 중 오류가 발생했습니다.");
      					}else{
      						alert("일괄 취소 중 오류가 발생했습니다.");
      					}
        			}else{
        				if(confirm == "Y"){
      						alert("일괄 확인 되었습니다");
      					}else{
      						alert("일괄 취소 되었습니다");
      					}
        				search();
        			}
        		});
        	}
        	function confirmOne(index, confirm, gubn){
        		var idx = index;
        		if(($("#targetAmt"+idx).val() == null || $("#targetAmt"+idx).val() == "")){
					alert("목표값을 먼저 저장하셔야합니다.");
					return;
				}else if($("#targetAmt"+idx).val() != $("[name='targetAmt']").eq(idx).val()){
					alert("목표값을 먼저 저장하셔야합니다.");
					return;
				}else{
					$("#hiddenChargeOneYyyymm").val($("[name='chargeYyyymm']")[index].value);
	        		$("#hiddenChargeOneKpiID").val($("[name='chargeKpiID']")[index].value);
	        		$("#hiddenChargeOneID").val($("[name='chargeID']")[index].value);
	        		$("#hiddenChargeOneName").val($("[name='chargeName']")[index].value);
	        		$("#hiddenChargeOneDate").val($("[name='chargeDate']")[index].value);
	        		if(gubn == "charge"){
	        			$("#hiddenUserGubn").val("Charge");
	        			$("#hiddenChargeOneConfirm").val(confirm);
	        			$("#hiddenUserOneConfrim").val("");
	        		}else{
	        			$("#hiddenUserGubn").val("User");
	        			$("#hiddenUserGubn").val(confirm);
	        			$("#hiddenChargeOneConfirm").val($("[name='chargeConfirm']")[index].value);
	        			$("#hiddenUserOneConfirm").val(confirm);
	        		}
	        		$("#hiddenUserOneID").val($("[name='userID']")[index].value);
	        		$("#hiddenUserOneName").val($("[name='userName']")[index].value);
	        		$("#hiddenUserOneDate").val($("[name='userDate']")[index].value);
	        		$i.post("./insert", "#oneSaveForm").done(function(data){
	        			if(data.returnCode == "EXCEPTION"){
	        				if(confirm == "Y"){
	      						alert("확인 중 오류가 발생했습니다.");
	      					}else{
	      						alert("확인취소 중 오류가 발생했습니다.");
	      					}
	        			}else{
	        				if(confirm == "Y"){
	      						alert("확인 되었습니다");
	      					}else{
	      						alert("확인취소 되었습니다");
	      					}
	        				search();
	        			}
	        		});
				}
        	}
        	function changeYearCombo(){
        		var data = $i.service("listKpiCombo",[$("#cboSearchYear").val()]);
           		data.done(function(c){ 
           			if(c.returnCode=="SUCCESS"){
           				$("#cboSearchKpi").jqxComboBox("clear");
           				for(var i=0;i<c.returnArray.length;i++){
           					$("#cboSearchKpi").jqxComboBox("insertAt", {label:c.returnArray[i].MT_NM, value:c.returnArray[i].MT_ID},i);
           				}
           				$("#cboSearchKpi").jqxComboBox("selectIndex", 0);
           			}else{
           				$("#cboSearchKpi").jqxComboBox("clear");
           				$("#cboSearchKpi").jqxComboBox("insertAt", {label:"==선택==",value:""},0);
           			} 
           		});
//            		listScidCombo(String yyyy, String evagbn)
           		
        	}
        	function changeKpiCombo(){
        		var dataScid = $i.service("listScidCombo",[$("#cboSearchYear").val(),$("#cboSearchKpi").val(), "${param.pEvaGbn}"]);
           		dataScid.done(function(data){
           			if(data.returnCode == "SUCCESS"){
           				$("#cboSearchScID").jqxComboBox("clear");
           				for(var i=0;i<data.returnArray.length;i++){
           					$("#cboSearchScID").jqxComboBox("insertAt", {label:data.returnArray[i].SC_NM, value:data.returnArray[i].SC_ID},i);
           				}
           				$("#cboSearchScID").jqxComboBox("selectIndex", 0);
           			}else{
           				$("#cboSearchScID").jqxComboBox("clear");
           				$("#cboSearchScID").jqxComboBox("insertAt", {label:"==선택==", value:""},0);
           			}
           		});
        	}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
		</style>
		   
    </head>
    <body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:1160px; min-width:1160px; margin:0 10px;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<form id="searchForm" name="searchForm" action="./crud">
					<input type="hidden" id="hiddenEvaGbn" name="pEvaGbn" value="${param.pEvaGbn}" />
					<div class="label type1 f_left">평가구분:<span class="label sublabel" id="spanEvaLabel"></span></div>
					<div class="label type1 f_left">학년도:</div>
					<div class="combobox f_left" onchange="changeYearCombo();">
						<select id="cboSearchYear" name="searchYear">
							<c:choose>
								<c:when test="${listYear != null && not empty listYear}">
									<c:forEach items="${listYear}" var="key" varStatus="loop">
										<option vluae="${key.COM_COD}" <c:if test="${key.COM_COD == param.searchYear}">selected</c:if>>${key.COM_NM}</option>
									</c:forEach>
								</c:when>
							</c:choose>
						</select>
					</div>
					<div class="label type1 f_left">지표:</div>
					<div class="combobox f_left" onchange="changeKpiCombo();">
						<select id="cboSearchKpi" name="searchKpi">
							<c:choose>
								<c:when test="${listKpi != null && not empty listKpi}">
									<c:forEach items="${listKpi}" var="key" varStatus="loop">
										<option value="${key.MT_ID}" <c:if test="${key.MT_ID == param.searchKpi}">selected</c:if>>${key.MT_NM}</option>
									</c:forEach>
								</c:when>
							</c:choose>
						</select>
					</div>
					<div class="label type1 f_left">조직:</div>
					<div class="combobox f_left">
						<select id="cboSearchScID" name="searchScID">
							<c:choose>
								<c:when test="${listScid != null && not empty listScid}">
									<c:forEach items="${listScid}" var="key" varStatus="loop">
										<option value="${key.SC_ID}" <c:if test="${key.SC_ID == param.searchScID}">selected</c:if>>${key.SC_NM}</option>
									</c:forEach>
								</c:when>
							</c:choose>
						</select>
					</div>
					<div class="label type1 f_left">담당자&frasl;승인자:<span class="label sublabel">${userInfo.NAMEHAN}</span></div>
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
						<div class="button type1 f_left">
							<input type="button" value="저장" id='btnSave' width="100%" height="100%" onclick="insert();" />
						</div>
					</div>
				</form>
			</div>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:100%; margin:0%;">
					<div id="messageLayer"style="text-align:right;margin: 0px 10px 10px 0px;font-size:12px;color:red"></div>
					<div id="allButton" class="group_button f_right">
						<div class="button type2 f_left">
							<input type="button" value="담당자 일괄확인" id="btnAllChargeY" width="100%" height="100%" style="text-indent:0 !important; margin:2px 0 !important;" class='button type2' onclick="confirmAll('Y', 'Charge');"  />
						</div>
						<div class="button type3 f_left">
							<input type="button" value="담당자 일괄취소" id="btnAllChargeN" width="100%" height="100%" style="text-indent:0 !important; margin:2px 0 !important;" class='button type3' onclick="confirmAll('N', 'Charge');"  />
						</div>
						<div class="button type2 f_left">
							<input type="button" value="승인자 일괄확인" id="btnAllUserY" width="100%" height="100%" style="text-indent:0 !important; margin:2px 0 !important;" class='button type2' onclick="confirmAll('Y', 'User');"  />
						</div>
						<div class="button type3 f_left">
							<input type="button" value="승인자 일괄취소" id="btnAllUserN" width="100%" height="100%" style="text-indent:0 !important; margin:2px 0 !important;" class='button type3' onclick="confirmAll('N', 'User');"  />
						</div>
					</div>
					<div class="blueish datatable f_left" style="width:100%; height:600px; margin:0px 0; overflow:hidden;">
						<div class="datatable_fixed" style="width:100%; height:600px; float:left;">
							<div style=" height:575px !important; overflow-x:hidden;">
								<form id="allSaveForm" name="allSaveForm" action="./insert">
									<input type="hidden" id="hiddenAllSaveGubn" name="saveGubn" />
									<input type="hidden" id="hiddenAllUserGubn" name="userGubn" />
									<input type="hidden" id="hiddenAllConfirm" name="confirm" />
									<input type="hidden" id="hiddenAllpEvaGbn" name="pEvaGbn" value="${param.pEvaGbn}" />
									<table id="tableConfirm" style="width:100%;"  class="none_hover none_alt">
										<thead style="width:100%;">
											<tr>
												<th scope="col" style="width:15%;">조직명</th>
												<th scope="col" style="width:25%;">지표명</th>
												<th colspan="3" scope="col" style="width:24%;">담당자</th>
												<th colspan="3" scope="col" style="width:24%;">승인자</th>
												<th scope="col" style="width:5%;">단위</th>
												<th scope="col" style="width:5%;">목표</th>
												<th style="width:1%; min-width:17px;"></th>
											</tr>
										</thead>
										<tbody>
											<c:choose>
												<c:when test="${mainList != null && not empty mainList}">
													<c:forEach items="${mainList}" var="mainList" varStatus="loop">
														<tr>
															<td style="width:15%;">
																<div class="cell t_center">${mainList.SC_NM}</div>
															</td>
															<td style="width:25%;">
																<input type="hidden" name="chargeYyyymm" value="${mainList.YYYYMM}" />
																<input type="hidden" name="chargeKpiID" value="${mainList.KPI_ID}" />
																<div class="cell">${mainList.KPI_NM}</div>
															</td>
															<td style="width:8%;">
																<input type="hidden" name="chargeID" value="${mainList.TGT_CHARGE_ID}" />
																<input type="hidden" name="chargeName" value="${mainList.TGT_CHARGE_NM}" />
																<div class="celll t_center">${mainList.TGT_CHARGE_NM}</div>
															</td>
															<td style="width:8%;">
																<div class="cell t_center">
																	<c:if test="${mainList.TGT_CHARGE_ID == userInfo.UNID}">
																		<c:if test="${mainList.USER_CONFIRM == '' || mainList.USER_CONFIRM == 'N' }">
																			<c:if test="${mainList.CHARGE_CONFIRM == '' || mainList.CHARGE_CONFIRM == 'N'}">
																				미확인
																				<div class="button type2" style="margin:3px 0;">
																					<input type="button" value="확인" name='btnConfrimY' class="confirmButton" width="100%" height="100%" style="padding:2px 5px 4px 6px !important;" onclick="confirmOne('${loop.index}','Y','charge');" />
																				</div>
																			</c:if>
																			<c:if test="${mainList.CHARGE_CONFIRM == 'Y'}">
																				확인
																				<div class="button type3" style="margin:3px 0;">
																					<input type="button" value="확인취소" name='btnConfrimN' class="confirmButton" width="100%" height="100%" style="padding:2px 5px 4px 6px !important;" onclick="confirmOne('${loop.index}','N','charge');" />
																				</div>
																			</c:if>
																		</c:if>
																		<c:if test="${mainList.USER_CONFIRM == 'Y'}">
																			확인
																		</c:if>
																	</c:if>
																	<c:if test="${mainList.TGT_CHARGE_ID != userInfo.UNID}">
																		<c:if test="${mainList.CHARGE_CONFIRM == '' || mainList.CHARGE_CONFIRM == 'N' }">
																			미확인
																		</c:if>
																		<c:if test="${mainList.USER_CONFIRM == 'Y' }">
																			확인
																		</c:if>
																	</c:if>
																</div>
															</td>	
															<td style="width:8%; ">
																<div class="cell t_center">
																	<input type="hidden" id="hiddenChargeDate" name="chargeDate" value="${mainList.ORG_CHARGE_DATE}" />
																	<c:if test="${mainList.CHARGE_CONFIRM == 'Y'}">
																		${mainList.CHARGE_DATE}
																		</br>
																		${mainList.LAST_CHARGE_NM}
																	</c:if>
																</div>
															</td>	
															<td style="width:8%;">
																<input type="hidden" name="userID" value="${mainList.TGT_USER_ID}" />
																<input type="hidden" name="userName" value="${mainList.TGT_USER_NM}" />
																<div class="cell t_center">${mainList.TGT_USER_NM}</div>
															</td>
															<td style="width:8%;">
																<div class="cell t_center">
																	<c:if test="${mainList.TGT_USER_ID == userInfo.UNID}">
																		<c:if test="${mainList.CHARGE_CONFIRM == '' || mainList.CHARGE_CONFIRM == 'N'}">
																			담당자 미확인
																		</c:if>
																		<c:if test="${mainList.CHARGE_CONFIRM == 'Y'}">
																			<c:if test="${mainList.USER_CONFIRM == '' || mainList.USER_CONFIRM == 'N'}">
																				미확인
																				<div class="button type2" style="margin:3px 0;">
																					<input type="button" value="확인" name='btnConfrimY' class="confirmButton" width="100%" height="100%" style="padding:2px 5px 4px 6px !important;" onclick="confirmOne('${loop.index}','Y','user');" />
																				</div>
																			</c:if>
																			<c:if test="${mainList.USER_CONFIRM == 'Y'}">
																				확인
																				<div class="button type3" style="margin:3px 0;">
																					<input type="button" value="확인취소" name='btnConfrimN' class="confirmButton" width="100%" height="100%" style="padding:2px 5px 4px 6px !important;" onclick="confirmOne('${loop.index}','N','user');" />
																				</div>
																			</c:if>
																		</c:if>
																	</c:if>
																	<c:if test="${mainList.TGT_USER_ID != userInfo.UNID}">
																		<c:if test="${mainList.CHARGE_CONFIRM == '' || mainList.CHARGE_CONFIRM == 'N'}">
																			담당자 미확인
																		</c:if>
																		<c:if test="${manList.CHARGE_CONFIRM == 'Y'}">
																			<c:if test="${mainList.USER_CONFIRM == '' || mainList.USER_CONFIRM == 'N'}">
																				미확인
																			</c:if>
																			<c:if test="${mainList.USER_CONFIRM == 'Y'}">
																				확인
																			</c:if>
																		</c:if>
																	</c:if>
																</div>
															</td>	
															<td style="width:8%;"> 
																<div class="cell t_center">
																	<input type="hidden" id="hiddenUserDate" name="userDate" value="${mainList.ORG_USER_DATE}" />
																	<c:if test="${mainList.USER_CONFIRM == 'Y'}">
																		${mainList.USER_DATE}
																		</br>
																		${mainList.LAST_USER_NM}
																	</c:if>
																</div>
															</td>
															<td style="width:5%;">
																<div class="cell t_center">${mainList.UNIT_NM}</div>
															</td>
															<td style="width:5%;">
																<div class="cell t_right">
																	 <input type="hidden" name="chargeConfirm" value="${mainList.CHARGE_CONFIRM}"/>
																	 <input type="hidden" name="userConfirm" value="${mainList.USER_CONFIRM}" />
																	 <input type="hidden" name="tgtSeq" value="${mainList.TGT_SEQ}"/>
																	 <input type="hidden" name="yyyy" value="${mainList.YYYY}" />
																	 <input type="hidden" name="mm" value="${mainList.MM}" />
																	 <input type="hidden" name="kpiID" value="${mainList.KPI_ID}" />
																	 <input type="hidden" name="analCycle" value="${mainList.ANAL_CYCLE}" />
																	 <input type="hidden" id="targetAmt${loop.index}" value="${mainList.TARGET_AMT}" />
																	 <c:if test="${mainList.TGT_CHARGE_ID == userInfo.UNID}">
																	 	<c:if test="${mainList.CHARGE_CONFIRM == '' || mainList.CHARGE_CONFIRM == 'N'}">
																			<input type="text" class="edit_inputSearch" name="targetAmt" value="${mainList.TARGET_AMT}" onkeyup="calValueCheck('${loop.index}');" style="width:90%; text-align:right !important; text-indent:0 !important;" />
																		</c:if>
																		<c:if test="${mainList.CHARGE_CONFIRM == 'Y'}">
																			<input type="text" name="targetAmt" value="${mainList.TARGET_AMT}" style="width:90%; text-align:right !important; text-indent:0 !important; border: 0;" readonly="readonly" />
																		</c:if>
																	</c:if>
																	<c:if test="${mainList.TGT_CHARGE_ID != userInfo.UNID}">
																		<input type="text" name="targetAmt" value="${mainList.TARGET_AMT}" style="width:90%; text-align:right !important; text-indent:0 !important; border: 0;" readonly="readonly" />
																	</c:if>
																</div>
															</td>
				            							</tr> 
													</c:forEach>
												</c:when>
											</c:choose>
										</tbody>
									</table>
								</form>
								<form id="oneSaveForm" name="oneSaveForm" action="./insert">
									<input type="hidden" id="hiddenSaveGubn" name="saveGubn" value="One" />
									<input type="hidden" id="oneEvaGbn" name="pEvaGbn" value="${param.pEvaGbn}" />
									<input type="hidden" id="hiddenUserGubn" name="userGubn" />
									<input type="hidden" id="hiddenChargeOneYyyymm" name="chargeOneYyyymm" />
									<input type="hidden" id="hiddenChargeOneKpiID" name="chargeOneKpiID" />
									<input type="hidden" id="hiddenChargeOneID" name="chargeOneID" />
									<input type="hidden" id="hiddenChargeOneName" name="chargeOneName" />
									<input type="hidden" id="hiddenChargeOneConfirm" name="chargeOneConfirm" />
									<input type="hidden" id="hiddenChargeOneDate" name="chargeOneDate" />
									<input type="hidden" id="hiddenUserOneID" name="userOneID" />
									<input type="hidden" id="hiddenUserOneName" name="userOneName" />
									<input type="hidden" id="hiddenUserOneConfirm" name="userOneConfirm" />
									<input type="hidden" id="hiddenUserOneDate" name="userOneDate" />
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>



