<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>세부전략목표관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$("#btnSearch").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnNew").jqxButton({ width: '',  theme:'blueish'});
				$("#btnSave").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnDelete").jqxButton({ width: '',  theme:'blueish'});              
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		var dat = $i.sqlhouse(13,{COML_COD:"YEARS"});
        		dat.done(function(res){
        			var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                        { name: 'COM_COD' },
	                        { name: 'COM_NM' }
	                    ],
	                    id: 'id',
						localdata:res,
						// url: url,
	                    async: false
	                };
	                var dataAdapter = new $.jqx.dataAdapter(source);
	                $("#cboSearchyyyy").jqxComboBox({selectedIndex: 0, source: dataAdapter,animationType: 'fade',dropDownHorizontalAlignment: 'right',displayMember: "COM_NM",valueMember: "COM_COD",dropDownWidth: 80,width: 80,height: 23,theme:'blueish'});
	                var dat = $i.sqlhouse(13,{COML_COD:"USE"});
	        		dat.done(function(res){
	        			var source =
		                {
		                    datatype: "json",
		                    datafields: [
		                        { name: 'COM_COD' },
		                        { name: 'COM_NM' }
		                    ],
		                    id: 'id',
							localdata:res,
							// url: url,
		                    async: false
		                };
		                var dataAdapter = new $.jqx.dataAdapter(source); 
		                $("#cboUseyn").jqxComboBox({selectedIndex: 0, source: dataAdapter,animationType: 'fade',dropDownHorizontalAlignment: 'right',displayMember: "COM_NM",valueMember: "COM_COD",dropDownWidth: 150,dropDownHeight: 60,width: 150,height: 23,theme:'blueish'});
	        		});
	                search();   
	                getRowDetail('','','');
        		}); 
        	}
        	//조회
        	function search(){
        		var dat = $i.select("#searchForm");
        		dat.done(function(res){
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'ACCT_ID', type: 'string' },
		                    { name: 'STRA_ID', type: 'string' },
		                    { name: 'STRA_CD', type: 'string' },
							{ name: 'STRA_NM', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var detailRow = function (row, columnfield, value) {//그리드 선택시 하단 상세
						var straId = $("#gridStralist").jqxGrid("getrowdata", row).STRA_ID;
						var straNm = $("#gridStralist").jqxGrid("getrowdata", row).STRA_NM;
						var acctId = $("#gridStralist").jqxGrid("getrowdata", row).ACCT_ID;
						
						var link = "<a href=\"javascript:getRowDetail('"+straId+"','"+straNm+"','"+acctId+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
						
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridStralist").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                   { datafield: 'STRA_ID', hidden:true},
		                   { text: '전략목표코드', datafield: 'STRA_CD', width: 122, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '전략목표명', datafield: 'STRA_NM', align:'center', cellsalign: 'center', cellsrenderer: detailRow}
		                ]
		            });
        		});
        	}
        	//입력
        	function insert(){
        		if($("#hiddenStraID").val().trim()==""){
        			$i.dialog.warning("SYSTEM","전략목표를 선택하세요.");
        			return;
        		}
        		if($("#txtSubStraCD").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","세부전략코드를 입력하세요");
        			return
        		}
        		if($("#txtSubstranm").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","세부전략명을 입력하세요");
        			return
        		}
        		if($("#txtStartyyyy").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","시작년도를 입력하세요");
        			return
        		}
        		if($("#txtEndyyyy").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","종료년도를 입력하세요");
        			return
        		}
        		var check = $i.sqlhouse("123",{ACCT_ID:$("#txtAcctid").val(), STRA_ID:$("#hiddenStraID").val(),SUBSTRA_ID:$("#hiddenSubStraID").val()});
        		check.done(function(res){
       				if($("#hiddenSubStraID").val() != $("#hiddenSubStraID").val() || $("#hiddenSubStraID").val() == ""){
      						if(res.returnArray.length > 0){
      							$i.dialog.warning("SYSTEM","해당 세부전략코드가 존재합니다");
      						}else{
	        				$i.insert("#saveForm").done(function(args){
			        			if(args.returnCode == "EXCEPTION"){
			        				$i.dialog.error("SYSTEM",args.returnMessage);
			        			}else{
			        				$i.dialog.alert("SYSTEM","저장 되었습니다.");
			        				search();
			        				getRowDetail($("#straidLabel").html(), $("#stranmLabel").html(), $("#txtAcctid").val());
			        				resetForm();
			        			}
			        		}).fail(function(e){ 
			        			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
			        		});
	        			}
       				}else{
       					$i.insert("#saveForm").done(function(args){
		        			if(args.returnCode == "EXCEPTION"){
		        				$i.dialog.error("SYSTEM",args.returnMessage);
		        			}else{
		        				$i.dialog.alert("SYSTEM","저장 되었습니다.");
		        				search();
		        				getRowDetail($("#straidLabel").html(), $("#stranmLabel").html(), $("#txtAcctid").val());
		        				resetForm();
		        			}
		        		}).fail(function(e){ 
		        			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
		        		});
       				}
        		});
        	}
        	//삭제
        	function remove(){    
        		if($("#hiddenStraID").val().trim()==""){
        			$i.dialog.warning("SYSTEM","전략목표를 선택하세요.");
        			return;
        		}
        		if($("#hiddenSubStraID").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","삭제 할 세부전략목표를 선택하세요");
        			return;
        		}
        		if(confirm("삭제 하시겠습니까?")){
        			var check = $i.sqlhouse("68",{ACCT_ID:$("#txtAcctid").val(), STRA_ID:$("#hiddenStraID").val(), SUBSTRA_ID:$("#hiddenSubStraID").val()});
        			check.done(function(res){
        				if(res.returnArray.length > 0){
        					$i.dialog.warning("SYSTEM","사용 중인 세부전략목표입니다");
        				}else{
        					$i.remove("#saveForm").done(function(args){
			        			if(args.returnCode == "EXCEPTION"){
			        				$i.dialog.error("SYSTEM",args.returnMessage);
			        			}else{
			        				$i.dialog.alert("SYSTEM","삭제 되었습니다.");
			        				search();
			        				getRowDetail($("#straidLabel").html(), $("#stranmLabel").html(), $("#txtAcctid").val());
			        				resetForm();
			        			}
			        		}).fail(function(e){
			        			$i.dialog.error("SYSTEM","삭제 중 오류가 발생했습니다.");
			        		});
        				}
        			});
        		}
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        		$("#btnInsert").on("click", function(event){
        			$("#tblInsert").toggle(); 
        		}); 
        	}
        	
        	//테스트 함수.
        	function test(){ 
        		var async = $i.sqlhouse(4007,{S_ID:"C"},function(res){});
        		async.done(function(res){ 
        			console.log(res);
        		}).fail(function(error){
        			console.log(error); 
        		});
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	
        	//gridCallist 로우 상세
        	function getRowDetail(straId, straNm, acctId){
        		$("#txtAcctid").val(acctId);
        		$("#straidLabel").html(straId);
	        	$("#stranmLabel").html(straNm);
	        	$("#hiddenStraID").val(straId);
        		var dat = $i.sqlhouse(38,{ACCT_ID:acctId, STRA_ID:straId, SEARCH_YYYY:$("#cboSearchyyyy").val()});
        		dat.done(function(res){
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'ACCT_ID', type: 'string'},
		                    { name: 'STRA_ID', type: 'string'},		                    
		                    { name: 'SUBSTRA_ID', type: 'string'},
		                    { name: 'SUBSTRA_CD', type: 'string'},
		                    { name: 'SUBSTRA_NM', type: 'string'},
							{ name: 'SORT_ORDER', type: 'string'},
							{ name: 'START_YYYY', type: 'string'},
							{ name: 'END_YYYY', type: 'string'},
							{ name: 'USE_NM', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var detailRow = function (row, columnfield, value,defaultHtml, rowProperty, rowdata) {//그리드 선택시 하단 상세						
						var link = "<a href='javascript:getSubstrlistGridDetail("+JSON.stringify(rowdata)+")' style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";						
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridSubstrlist").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                   { datafield: 'SUBSTRA_ID', hidden:true},
		                   { text: '세부전략코드', datafield: 'SUBSTRA_CD', width: 116, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '세부전략명', datafield: 'SUBSTRA_NM', width: 520, align:'center', cellsalign: 'center', cellsrenderer: detailRow},
						   { text: '정렬순서', datafield: 'SORT_ORDER', width: 116, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
						   { text: '시작년도', datafield: 'START_YYYY', width: 116, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
						   { text: '종료년도', datafield: 'END_YYYY', width: 116, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
						   { text: '사용여부', datafield: 'USE_NM', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
						   
		                ]
		            });
        		});
        	}
        	function getSubstrlistGridDetail(rowdata){
        		$("#txtAcctid").val(rowdata.ACCT_ID);
        		
        		$("#hiddenSubStraID").val(rowdata.SUBSTRA_ID);
	        	$("#hiddenSubStraID").val(rowdata.SUBSTRA_ID);
	        	$("#txtSubStraCD").val(rowdata.SUBSTRA_CD); 
	        	$("#txtSubstranm").val(rowdata.SUBSTRA_NM);
	        	$("#txtSortorder").val(rowdata.SORT_ORDER);
	        	$("#txtStartyyyy").val(rowdata.START_YYYY);
	        	$("#txtEndyyyy").val(rowdata.END_YYYY);
	        	if(rowdata.USE_YN != ''){
	        		$("#cboUseyn").val(rowdata.USE_YN);
	        	}
        	}
        	//신규 버튼 클릭 또는 초기화 해야 할 경우
        	function resetForm(){
				$("#hiddenSubStraID").val('');
				$("#txtSubStraCD").val(''); 
				$("#txtSubstranm").val(''); 
				$("#txtSortorder").val('');
				$("#txtStartyyyy").val('');
				$("#txtEndyyyy").val('');
       			$("#cboUseyn").val('Y');
        	}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
		</style>   
    </head>
    <body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 1%;">
			<form id="searchForm" name="searchForm">
				<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
					<div class="label type1 f_left">사용여부 : </div>
					<div class="combobox f_left" id="cboSearchyyyy" name="searchyyyy"></div>
					<div class="group f_right pos_a" style="right:0px;">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
					</div>
				</div>
			</form>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style="width:34%; margin-right:1%;">
					<div class="grid f_left" style="width:100%; height:600px;">	
						<div id="gridStralist"> </div>
					</div>
				</div> 
				<div class="content f_right" style="width:65%;">
					<form id="saveForm" name="saveForm">
						<div class="group f_left  w100p">
							<input type="hidden" id="txtAcctid" />
							<div class="label type2 f_left p_r10">전략목표ID:<span class="label sublabel" style="font-weight:bold;" id="straidLabel"></span></div>
							<div class="label type2 f_left">전략목표명:<span class="label sublabel" style="font-weight:bold;" id="stranmLabel"></span></div>
						</div>
						<div class="grid f_left" style="width:100%; height:470px;">
							<div id="gridSubstrlist"> </div>
						</div>
						<div class="table  f_left" style="width:100%; height:100%;">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<tr>
									<th style="width:10% !important; font-weight:bold !important;">세부전략코드<span class="th_must"></span></th>
									<th style="width:50% !important; font-weight:bold !important;">세부전략목표명<span class="th_must"></span></th>
									<th style="width:10% !important; font-weight:bold !important;">정렬순서</th>
									<th style="width:10% !important; font-weight:bold !important;">시작년도<span class="th_must"></span></th>
									<th style="width:10% !important; font-weight:bold !important;">종료년도<span class="th_must"></span></th>
									<th style="width:10% !important; font-weight:bold !important;">사용여부</th>
								</tr>
								<tr>
									<td>
										<input type="hidden" id="hiddenStraID" name="straid" />
										<input type="hidden" id="hiddenSubStraID" name="substraid" />
										<input type="text" id="txtSubStraCD" name="substracd" class="input type2 f_left" style="width:81%;margin:3px 5px !important;" />
									</td>     
									<td>
										<input type="text" id="txtSubstranm" name="substranm" class="input type2 f_left" style="width:95.9%;margin:3px 5px !important;" />
									</td>
									<td>
										<input type="text" id="txtSortorder" name="sortorder" class="input type2 f_left" style="width:81%;margin:3px 5px !important;" />
									</td>
									<td>
										<input type="text" id="txtStartyyyy" name="startyyyy" class="input type2 f_left" style="width:81%;margin:3px 5px !important;" />
									</td>    
									<td>
										<input type="text" id="txtEndyyyy" name="endyyyy" class="input type2 f_left" style="width:81%;margin:3px 5px !important;" />
									</td>
									<td>
										<div class="combobox f_left" style="margin:3px 5px !important;" id="cboUseyn" name="useyn"></div>
									</td>
								</tr>
							</table>
						</div>
						<div class="f_right m_t5">
							<div class="button type2 f_left">
								<input type="button" value="초기화" id='btnNew' width="100%" height="100%" onclick="resetForm();" />
							</div>
							<div class="button type2 f_left">
								<input type="button" value="저장" id='btnSave' width="100%" height="100%" onclick="insert();" />
							</div>
							<div class="button type3 f_left">
								<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();" />
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>

