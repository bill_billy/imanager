<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>담당자</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$("#btnSearch").jqxButton({width:'', theme:'blueish'});
        		$("#txtSearch").jqxInput({placeHolder: "", height: 23, width: 250, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return detailScid(' '); } });
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		search();
        		detailScid(' ');
        	}
        	//조회
        	function search(){
        		var acctid = "${param.acctid}";
        		var data = $i.sqlhouse(92,{});
        		data.done(function(res){
		            // prepare the data
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'SC_ID', type: 'string'},
		                    { name: 'SC_NM', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 5px;">' + value + '</div>';
					};
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 5px 0px 0px;">' + value + '</div>';
					};
					var detail = function (row, columnfield, value) {//right정렬
						var scid = $("#gridSclist").jqxGrid("getrowdata", row).SC_ID;    
					
						var link = "<a href=\"javascript:detailScid('"+scid+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridSclist").jqxGrid(
		            {
	              	 	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
						sortable: true,
		                columnsresize: true,
						columnsheight: 25,
						rowsheight: 25,
		                columns: [
		                   { text: '조직명', datafield: 'SC_NM', align:'center', cellsalign: 'left',  cellsrenderer: detail}
		                ]
		            });
        		});
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function detailScid(scid){
        		if(scid == ' '){
        			if($("#hiddenSearchDept").val() == ""){
        				scid = ' ';
        			}else{
        				scid = $("#hiddenSearchDept").val();
        			}
        		}
        		$("#hiddenSearchDept").val(scid);
        		var nameHan = ' ';
        		if($("#txtSearch").val() != ""){ 
        			nameHan = $("#txtSearch").val();
        		}
        		var data = $i.sqlhouse(93,{ACCT_ID:"${param.acctid}",DEPTCD:scid, S_NAMEHAN:nameHan});
        		data.done(function(res){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'DEPTCD', type: 'string'},
		                    { name: 'DEPT_NM', type: 'string'},
		                    { name: 'NAMEHAN', type: 'string'},
		                    { name: 'EMPNO', type: 'string'},
		                    { name: 'WKGD_NAME', type: 'string'},
		                    { name: 'JIKWI_NM', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 5px;">' + value + '</div>';
					};
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 5px 0px 0px;">' + value + '</div>';
					};
					var detail = function (row, columnfield, value) {//right정렬
						var dept = $("#gridDeptlist").jqxGrid("getrowdata", row).DEPTCD;
						var empno = $("#gridDeptlist").jqxGrid("getrowdata", row).EMPNO;
						var namehan = $("#gridDeptlist").jqxGrid("getrowdata", row).NAMEHAN;
					
						var link = "<a href=\"javascript:sendParentData('"+dept+"','"+empno+"','"+namehan+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridDeptlist").jqxGrid(
		            {
	              	 	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
						sortable: true,
		                columnsresize: true,
						columnsheight: 25,
						rowsheight: 25,
		                columns: [
		                   { text: '조직', datafield: 'DEPT_NM', width:157, align:'center', cellsalign: 'left'},
		                   { text: '이름', datafield: 'NAMEHAN', width:126, align:'center', cellsalign: 'left',  cellsrenderer: detail},
		                   { text: '사번', datafield: 'EMPNO', width:94, align:'center', cellsalign: 'center'},
		                   { text: '직급', datafield: 'WKGD_NAME', width:94, align:'center', cellsalign: 'center'},
		                   { text: '직위', datafield: 'JIKWI_NM', width:94, align:'center', cellsalign: 'center'},
		                   { text: '조직ID', datafield: 'DEPTCD', align:'center', cellsalign: 'center'}
		                ]
		            });
        		});
        	}
        	function sendParentData(dept, empno, namehan){
        		var paramGubn = "${param.gubn}";     
        		var paramScid = "${param.scid}";
        		var paramKpiid = "${param.kpiid}";
        		if(paramGubn == "owneruser"){
        			$("[name='owneruserid'][data-scid='"+paramScid+"'][data-kpiid='"+paramKpiid+"']", opener.document).val(empno);
        			$("[name='ownerusernm'][data-scid='"+paramScid+"'][data-kpiid='"+paramKpiid+"']", opener.document).val(namehan);
        		}else if(paramGubn == "kpicharge"){
        			$("[name='kpichargeid'][data-scid='"+paramScid+"'][data-kpiid='"+paramKpiid+"']", opener.document).val(empno);
        			$("[name='kpichargenm'][data-scid='"+paramScid+"'][data-kpiid='"+paramKpiid+"']", opener.document).val(namehan);
        		}else if(paramGubn == "tgtuser"){
        			$("[name='tgtuserid'][data-scid='"+paramScid+"'][data-kpiid='"+paramKpiid+"']", opener.document).val(empno);
        			$("[name='tgtusernm'][data-scid='"+paramScid+"'][data-kpiid='"+paramKpiid+"']", opener.document).val(namehan);
        		}else if(paramGubn == "tgtcharge"){
        			$("[name='tgtchargeid'][data-scid='"+paramScid+"'][data-kpiid='"+paramKpiid+"']", opener.document).val(empno);
        			$("[name='tgtchargenm'][data-scid='"+paramScid+"'][data-kpiid='"+paramKpiid+"']", opener.document).val(namehan);
        		}else if(paramGubn == "empuser"){
        			$("[name='empid'][data-scid='"+paramScid+"'][data-kpiid='"+paramKpiid+"']", opener.document).val(empno);
        			$("[name='empusernm'][data-scid='"+paramScid+"'][data-kpiid='"+paramKpiid+"']", opener.document).val(namehan);
        		}else{ 
        			 
//         			var dept = $("#gridSclist").jqxGrid("getrowdatabyid",$("#gridSclist").jqxGrid("getrowid",$("#gridSclist").jqxGrid("selectedrowindex"))).SC_ID;
        			$("[name=mtUserID]",opener.document).val(empno);
        			$("[name='mtUserNM']", opener.document).val(namehan);
        			$("[name='mtUserDept']", opener.document).val(dept);
        			
        		}
				self.close();
        	}
        </script>
        <style type="text/css"> 
		.input[type=text] { 
			-webkit-ime-mode:active; 
			-moz-ime-mode:active; 
			-ms-ime-mode:active; 
			ime-mode:active; 
		} 
		</style> 
    </head>
    <body class='blueish'>
	<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:900px;min-height:300px; margin:0 1%;"><!--팝업창 사이즈 조정-->
			<div class="header f_left" style="width:100%; height: 27px; margin:10px 0;">
				<input type="hidden" id="hiddenSearchDept" />
				<div class="label type1 f_left">이름 : </div>
				<div class="input f_left">
					<input type="text" id="txtSearch" name="searchusernm" style="ime-mode:active;" />
				</div>
				<div class="group f_right pos_a" style="right:0px;">
					<div class="button type1 f_left">
						<input type="button" value="조회" id="btnSearch" width="100%" height="100%" onclick="detailScid(' ');" />
					</div>
				</div>
			</div>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:29%; margin-right:1%;">
					<div class="grid f_left" style="width:100%; height:276px;">
						<div id="gridSclist"></div>
					</div>
				</div>
				<div class="content f_right" style=" width:70%;">
					<div class="grid f_left" style="width:100%; height:276px;">
						<div id="gridDeptlist"></div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

