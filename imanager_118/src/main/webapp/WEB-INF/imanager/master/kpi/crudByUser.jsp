<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>           
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <title>지표관리</title>
   	 	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        var reportIndex = null;
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
				 
        		$i.dialog.setAjaxEventPopup("SYSTEM","처리 중 입니다.");
        		$("#btnSearch").jqxButton({width:'', theme:'blueish'});
        		$("#btnSearchkpi").jqxButton({width:'', theme:'blueish'});
        		$("#btnTaregetreset").jqxButton({width:'', theme:'blueish'});
        		$("#btnTargetinsert").jqxButton({width:'', theme:'blueish'});
        		$("#btnTargetremove").jqxButton({width:'', theme:'blueish'});  
        		$("#btnInsertResult").jqxButton({width:'', theme:'blueish'});
				//지표정의서버튼
				$("#btnKpiDetail").jqxButton({width:'', theme:'blueish'});   
        		//지표자체평가버튼(insert/save/submit)
        		$("#btnUserEvalinsert").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnUserEvalsave").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnUserEvalsubmit").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnUserEvalsubmitCencel").jqxButton({ width: '',  theme:'blueish'}); 
				
				//지표자체평가 승인자 최종확정/ 최종확정취소
                $("#lastEvalsubmitSave").jqxButton({ width: '',  theme:'blueish'});
                $("#lastEvalsubmitCencel").jqxButton({ width: '',  theme:'blueish'});
				
        		$("#txtSearchkpinm").jqxInput({placeHolder: "", height: 23, width: 100, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return makeKpiList(); } });
        		$("#txtSearchTgtnm").jqxInput({placeHolder: "", height: 20, width: 100, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return  makeTab3Grid('','');} });   
        		$("#txtTargetSearchKpinm").jqxInput({placeHolder: "", height: 20, width: 100, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return  makeTab3Grid('','');} });   
        		
        		$("#txtSearchChargenm").jqxInput({placeHolder: "", height: 20, width: 100, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return makeResultList(); } });   
        		$("#txtInputSearchKpinm").jqxInput({placeHolder: "", height: 20, width: 100, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return makeResultList(); } });   
        		
        		$('#jqxTabs01').jqxTabs({ width: '100%', height: '100%', position: 'top', theme:'blueish', selectedItem: 0});
            	$('#jqxTabs02').jqxTabs({ 
					width: '100%', height: '100%', position: 'top', theme:'blueish', selectedItem: 0,
					initTabContent:initTabFunction
				});
            	 
            	$('#jqxTabs02').on('selected', function (event) 
				{ 
				    var selectedTab = event.args.item;
				    initTabFunction(selectedTab,"event method");
				}); 
            	 
				$("#btnAllChargeY").jqxButton({ width: '', theme:'blueish'});
				$("#btnAllChargeN").jqxButton({ width: '', theme:'blueish'});
				$("#btnAllUserY").jqxButton({ width: '', theme:'blueish'});
				$("#btnAllUserN").jqxButton({ width: '', theme:'blueish'});   
				
            	init(); 
        		setEvent();
        		makeRadio();
        	}); 
        	var isInit = {};
        	function initTabFunction(tab, display){
						
						if(isInit[tab]==undefined){
							isInit[tab]=true;
						}
						else{
							console.log("switch : "+tab+", method ="+display);
							try{
								switch (tab) {
									case 0:
										makeTab1Grid('','');
										break;
									case 1:
										tabClack2();
										break;
									case 2:
										makeResultList();
										break;
									case 3:
										kpiAttachFilelist();
										break;
									case 4:
										kpiUserEvallist();
										break;
									case 5:
										kpiEvalresult();
										break;
									case 6:
										makeKpiReport();
										break; 
								}
							}catch(e){
								console.log(e);
							}
						}
					
			}
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		var yearDat = $i.sqlhouse(13,{COML_COD:"YEARS"});
        		yearDat.done(function(res){
        			var source = 
        			{
        				datatype:"json",
        				datafields:[
        					{ name : "COM_COD" },
        					{ name : "COM_NM" }
        				],
        				id:"id",
        				localdata:res,
        				async : false
        			};
        			var dataAdapter = new $.jqx.dataAdapter(source);
        			$("#cboSearchyear").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,  theme:'blueish'});
        			changeYear();
        			$("#labelShow").html("전체");
        		});
        	}
        	//조회 
        	function searchStart(){ 
        		$("#labelShow").val("");
				
        		makeStraListTree();
        		makeTab1Grid('','');
        		resetGradeValue();
        		kpiUserEvallist(); 
        		$("#txtTabscid").val("100");                    
        		$("#txtGubn").val("0");  
        		
        		tabClack2();
        		kpiAttachFilelist();             
        		makeResultList();  
        		kpiEvalresult();
        		
        		getDataList('','','','','','','',''); 
        		getDataListResult('','','','','','','','');
        		resetTarget();  
        	}
        	//입력
        	function insert(){
        		$("#saveForm").submit();
        	}
        	//삭제
        	function remove(){    
        		if($("#txtStraid").val().trim()==""){
        			$i.dialog.warning("SYSTEM","전략목표를 선택하세요.");
        			return;
        		}
        		if($("#txtSubstraid").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","삭제 할 세부전략목표를 선택하세요");
        			return;
        		}
        		$i.remove("#saveForm").done(function(args){
        			if(args.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM",args.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM","삭제 되었습니다.");
        				searchStart();
        				reset();
        			}
        		}).fail(function(e){
        			$i.dialog.error("SYSTEM","삭제 중 오류가 발생했습니다.");
        		});
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        		$("#btnInsertResult").on("click", function(event){
        			$("#tblInsert").toggle(); 
        		});          
        		$('.num_only').css('imeMode','disabled').keyup(function(){
			        if( $(this).val() != null && $(this).val() != '' ) {
			            $(this).val($(this).val().replace(/[a-zA-Z]/gi,""));
			        }
			    });
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function changeYear(){
        		var resultMmDat = $i.sqlhouse(162,{S_YYYY:$("#cboSearchyear").val(), S_EVA_GBN:"${param.pEvaGbn}", S_GUBUN:"A"});
        		resultMmDat.done(function(res){
        			var source = 
        			{
        				datatype:"json",
        				datafields:[
        					{ name : "MM_ID"},
        					{ name : "MM_NM"}
        				],
        				id:"id",
        				localdata:res,
        				async : false
        			};
        			var dataAdapter = new $.jqx.dataAdapter(source);
        			$("#cboSearchResultMm").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "MM_NM", valueMember: "MM_ID", dropDownWidth: 100, dropDownHeight: 80, width: 100, height: 22,  theme:'blueish'});
        			$("#cboSearchResultMm").jqxComboBox({ selectedIndex: 0});
        			
        		});
        		searchStart();
        	}
        	function labelChange(gubn){   
        		if(gubn != null ){
        			$('#jqxTabs01').jqxTabs('select', gubn);
        		}
        		if(gubn == "0"){
        			$("#labelShow").html("전체");
        		}
        		if(gubn == "2"){      
        			var htmlShow = "";    
        			var data = $i.sqlhouse(271,{ACCT_ID:"${paramAcctid}",S_YYYY:$("#cboSearchyear").val(),S_C_ID:$("#txtTabscid").val()});        
        			data.done(function(res){            
        				if(res.returnArray.length > 0){
        					for(var i=0;i<res.returnArray.length;i++){   
	        					if(i == 0){
	        						htmlShow += res.returnArray[i].STRA_NM;                                                             
	        					}else{                             
	        						htmlShow += "<span class='label sublabel type2'>"+res.returnArray[i].STRA_NM+"</span>";      
	        					}
	        				}
	        				$("#labelShow").html(htmlShow);
        				}
        			});
        		}else if(gubn == "1"){
        			var htmlShow = ""; 
        			var data = $i.sqlhouse(200,{ACCT_ID:"${paramAcctid}",MT_ID:$("#txtTabscid").val()});
        			data.done(function(res){
        				if(res.returnArray.length > 0){
        					htmlShow += res.returnArray[0].MT_NM;
        					$("#labelShow").html(htmlShow);
        				}
        			});   
        		}else{
        			     
        		}
        	} 
        	function makeKpiList(){
        		var dat = $i.post("./selectKpiAdmin",{searchyear:$("#cboSearchyear").val(), searchnm:$("#txtSearchkpinm").val()});
        		dat.done(function(data){
        			
        			try{
	        			var source =
			            {
			                datatype: "json",
			                datafields: [
			                    { name: 'MT_ID', type: 'string' },
			                    { name: 'MT_CD', type: 'string' },
			                    { name: 'MT_NM', type: 'string' }
			                ],
			                localdata: data,
			                updaterow: function (rowid, rowdata, commit) {
			                    commit(true);
			                }
			            };
						var alginRight = function (row, columnfield, value) {//right정렬
							return '<div id="userName-' + row + '"style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
	                  	};
	                  	var alginLeftLink = function (row, columnfield, value,defaultHtml,property,rowdata) {//left정렬  
							return "<div id='userName-" + row + "' onclick ='listboxKpiClick("+JSON.stringify(rowdata)+");' style='text-align:left; margin:4px 0px 0px 10px;text-decoration:underline;' >" + value + "</div>";
						}; 
			            var dataAdapter = new $.jqx.dataAdapter(source);
			            $("#listboxKpi").jqxGrid({ source: dataAdapter,
			            rowsHeight:25, 
			            pageable: true,
			            pageSizeOptions:['100','200','300'],
			            sortable: true,
			            columns:[
			            	{  datafield: 'MT_ID', hidden:true},
			            	{ text: '지표코드', datafield: 'MT_CD', width: '30%', align:'center', cellsalign: 'left'}, 
					  		{ text: '지표명', datafield: 'MT_NM', width: '70%', align:'center', cellsalign: 'left', cellsrenderer:alginLeftLink}
			            ],width: '100%',  height: 485, theme:"blueish"}); 
        			}catch(e){}
		            
        		});
        	}   
        	function listboxKpiClick(rowdata){ 
			    $("#txtTabscid").val(rowdata.MT_ID);
            	$("#txtGubn").val("1");
			    //makeTab1Grid('1', rowdata.MT_ID); 			    
			    //$("#jqxTabs02").find("li").eq($("#jqxTabs02").jqxTabs("selectedItem")).trigger("click");
			    initTabFunction($("#jqxTabs02").jqxTabs("selectedItem"));
			    labelChange(1);   
	}
        	function tabClack2(){
        		makeTab3Grid('','');
        		makeTab3DetailTargetList('','','','','','');
        	}
        	function makeStraListTree(){
        		var dat = $i.sqlhouse(202, {S_YYYY:$("#cboSearchyear").val()});
        		dat.done(function(res){
        			var source =           
					{
		                datatype: "json",
		                datafields: [
		                    { name: 'P_ID'},
		                    { name: 'C_ID'},
		                    { name: 'STRA_NM'},
		                    { name: 'STRA_ID'},
		                    { name: 'SUBSTRA_ID'}
		                ],
		                id : 'C_ID',
		                localdata: res
		            };            
        		    var dataAdapter = new $.jqx.dataAdapter(source);                         
		            dataAdapter.dataBind();     
		            var records = dataAdapter.getRecordsHierarchy('C_ID', 'P_ID', 'items', [{ name: 'C_ID', map: 'id'} ,{ name: 'STRA_NM', map: 'label'}, { name:'STRA_ID', map:'value'}]);
		            $('#treeStraList').jqxTree({source: records, height: '100%', width: '100%', theme:'blueish' });                                                              
					// tree init            
		            var items = $('#treeStraList').jqxTree('getItems');
		            var item = null;
		            var size = 0;
		            var img = '';
		            var label = '';
		            var afterLabel = '';
		            
					for(var i = 0; i < items.length; i++) {
						item = items[i];
						
						if(i == 0) {
							//root
							$("#treeStraList").jqxTree('expandItem', item);
							img = 'icon-foldernoopen.png';
							afterLabel = "";
						} else {
							//children
							size = $(item.element).find('li').size();
							
							if(size > 0) {
								//have a child
								img = 'icon-foldernoopen.png';
								afterLabel = "<span style='color: Blue;'> (" + size + ")</span>";
							} else {
								//no have a child
								img = 'icon-page.png';
								//내부에서 사용하는 트리는 팝업 없음.
								//afterLabel = "<span style='color: Blue; margin-left:5px; vertical-align:middle;'> <img src='${WWW.CSS}/images/img_icons/popupIcon.png' alt='팝업창' title='새창보기'/></span>";
								afterLabel = "";
							}
						}
						      
						label = "<img style='float: left; margin-right: 5px;' src='../../resources/cmresource/image/" + img + "'/><span item-title='truec style='vertical-align:middle;'>" + item.label + "</span>" + afterLabel;
						$('#treeStraList').jqxTree('updateItem', item, { label: label});
					}
		            //add event
		            $('#treeStraList')
		            .on("expand", function(eve) {
		            	var args = eve.args;
		            	var label = $('#treeStraList').jqxTree('getItem', args.element).label.replace('icon-foldernoopen', 'icon-folderopen');
		            	args.element.firstChild.className = args.element.firstChild.className.replace('jqx-tree-item-arrow-collapse', '').replace('jqx-icon-arrow-right', '');
		            	$('#treeStraList').jqxTree('updateItem', args.element, { label: label});
		            })
		            .on("collapse", function(eve) {
		            	var args = eve.args;
		            	var label = $('#treeStraList').jqxTree('getItem', args.element).label.replace('icon-folderopen', 'icon-foldernoopen');
		            	$('#treeStraList').jqxTree('updateItem', args.element, { label: label});    
		            })       
		            .on("select", function(eve) {
		            	$("#txtTabscid").val($('#treeStraList').jqxTree('getItem', args.element).id);
		            	$("#txtTabSubstr").val($("#treeStraList").jqxTree("getItem", args.element).id);
		            	if($("#txtTabscid").val()=="0"){
		        			$("#txtGubn").val("0");
		        			$("#labelShow").html("전체");
		        		}else{
		            	$("#txtGubn").val("2");
		        		}
		            	//$("#jqxTabs02").find("li").eq($("#jqxTabs02").jqxTabs("selectedItem")).trigger("click");
		            	initTabFunction($("#jqxTabs02").jqxTabs("selectedItem"));
		            	labelChange(2);          
		            });
		            //$("#treeStraList").jqxTree("expandAll");
        		}); 
        		
        	}
        	function makeTab1Grid(gubn, scid){
        		if(scid == ""){
        			scid = $("#txtTabscid").val();
        		}  
        		if(gubn == ""){
        			gubn = $("#txtGubn").val();
        		}
        		var date = $i.post("./selectKpilistAdmin",{gubn:gubn, scid:scid, yyyy:$("#cboSearchyear").val()});
        		date.done(function(res){        
		            // prepare the data
		            var source = 
		            {
		                datatype: "json",
		                datafields: [
		 		                    { name: 'SC_ID', type: 'string'},
				                    { name: 'SC_NM', type: 'string'},
				                    { name: 'MT_ID', type: 'string'}, 
				                    { name: 'MT_CD', type: 'string'}, 
									{ name: 'KPI_ID', type: 'string'},
									{ name: 'P_KPI_ID', type: 'string'}, 
				                    { name: 'KPI_NM', type: 'string'},
									{ name: 'MEAS_CYCLE', type: 'string'},
									{ name: 'MEAS_CYCLE_NM', type: 'string'},
									{ name: 'KPI_CHARGE_ID', type: 'string'},
									{ name: 'KPI_CHARGE_NM', type: 'string'},
									{ name: 'OWNER_USER_ID', type: 'string'},
									{ name: 'OWNER_USER_NM', type: 'string'},
									{ name: 'EVAL_NM', type: 'string'},
									{ name: 'ACCT_ID', type: 'string'}
		                ],
		                hierarchy:{ 
		                	keyDataField:{name:"KPI_ID"},
		                	parentDataField:{name:"P_KPI_ID"}
		                },	
		                id:"KPI_ID", 
		                localdata: res 
		            };
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 5px;">' + value + '</div>';
					};
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 5px 0px 0px;">' + value + '</div>';
					};
					var detailRow = function (row, columnfield, value, rowData) {//그리드 선택시 하단 상세
						var scid = $("#gridKpilist").jqxGrid("getrowdata", row).SC_ID;
						var mtid = $("#gridKpilist").jqxGrid("getrowdata", row).MT_ID;  
						var acctId = $("#gridKpilist").jqxGrid("getrowdata", row).ACCT_ID;
						
						//var link = "<a href=\"javascript:getRowDetail('"+scid+"','"+mtid+"','"+acctId+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + value + "</div>";
						
					}; 
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridKpilist").jqxGrid(
		            {
	              	 	width: '100%',
		                height:'100%',
						altRows:true, 
						pageable: true,    
						pagerMode: 'advanced',
						pageSizeOptions:['100','200','300'],
		                source: dataAdapter, 
						theme:'blueish',
						sortable: true, 
		                columnsResize: true, 
						columnsHeight: 25,   
		                columns: [
		                   { text: '조직ID', datafield: 'SC_ID', width: '7%', align:'center', cellsalign: 'center'},
		                   { text: '조직명', datafield: 'SC_NM', width: '20%', align:'center', cellsalign: 'left'},
						   { text: '지표코드', datafield: 'MT_CD', width: '10%', align:'center', cellsalign: 'center'},
						   { text: '지표명', datafield: 'KPI_NM', width: '20%', align:'center', cellsalign: 'left' ,  cellsrenderer: detailRow},
						   { text: '측정주기', datafield: 'MEAS_CYCLE_NM', width: '7%', align:'center', cellsalign: 'center' },
						   { text: '평가방식', datafield: 'EVAL_NM', width: '10%', align:'center', cellsalign: 'center' },
						   { text: '입력자', datafield: 'KPI_CHARGE_NM', width: '13%', align:'center', cellsalign: 'center'  },
						   { text: '책임자', datafield: 'OWNER_USER_NM', width: '13%', align:'center', cellsalign: 'center'  }
		                ]
		            });
        		});   
        	}
   		function setPagerLayout(selector) {
    			try{
	    			var pagesize = $('#'+selector).jqxGrid('pagesize');
	    			
	    			var w = 49; 
	    				
	    			if(pagesize<100) {
	    				w = 44;
	    			} else if(pagesize>99&&pagesize<1000) {
	    				w = 49;
	    			} else if(pagesize>999&&pagesize<10000) {
	    				w = 54;
	    			}
	    			
	    			//디폴트 셋팅
	    			$('#gridpagerlist'+selector).jqxDropDownList({ width: w+'px' });
	    			
	    			//체인지 이벤트 처리
	    			$('#'+selector).on("pagesizechanged", function (event) {
	    				var args = event.args;
	    				
	    				if(args.pagesize<100) {
	    					$('#gridpagerlist'+selector).jqxDropDownList({ width: '44px' });
	    				} else if(args.pagesize>99 && args.pagesize<1000) {
	    					$('#gridpagerlist'+selector).jqxDropDownList({ width: '49px' });
	    				} else if(args.pagesize>999 && args.pagesize<10000) {
	    					$('#gridpagerlist'+selector).jqxDropDownList({ width: '54px' });
	    				} else {
	    					$('#gridpagerlist'+selector).jqxDropDownList({ width: 'auto' });
	    				}
	    			});
    			}catch(e){
    				console.log(e);
    			}
    		}
        	function getRowDetail(scid, mtid, acctid){
        		var programid = "";
        		if(scid=="100") programid = "detailOfUnivByUser";
        		else programid = "detailOfDeptByUser";
        		
        		var url = "./"+programid+"?acctid=${paramAcctid}&scid="+scid+"&mtid="+mtid+"&gubn="+$("#txtGubn").val()+"&searchyear="+$("#cboSearchyear").val();
        		var kpiForm = $("<iframe data-role='kpiform' src='"+url+"'style='width:100%;height:100%;position:absolute;left:0px;border-style:none;z-index:99999;'></iframe>").appendTo("body"); 
        	}
        	function closeKpiForm(flag){
        		$("[data-role=kpiform]").remove();  
        		 
        		//$("#jqxTabs02").find("li").eq($("#jqxTabs02").jqxTabs("selectedItem")).trigger("click");
        		initTabFunction($("#jqxTabs02").jqxTabs("selectedItem"));
        	}
        	function makeTab3Grid(gubn, scid){
        		if(scid == ""){
        			scid = $("#txtTabscid").val();
        		}
        		if(gubn == ""){
        			gubn = $("#txtGubn").val();
        		}
        		var date = $i.post("./selectKpilistTargetAdmin",{gubn:gubn, scid:scid, yyyy:$("#cboSearchyear").val(),tgtusernm:$("#txtSearchTgtnm").val(),tgtchangenm:$("#txtSearchTgtnm").val(),kpichangenm:$("#txtTargetSearchKpinm").val()});
        		date.done(function(res){   
		            // prepare the data
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		 		                    { name: 'ACCT_ID', type: 'string'},
				                    { name: 'SC_ID', type: 'string'},
				                    { name: 'SC_NM', type: 'string'},
									{ name: 'KPI_ID', type: 'string'},
									{ name: 'MT_CD', type: 'string'},
				                    { name: 'KPI_NM', type: 'string'},
				                    { name: 'TGT_USER_NM', type: 'string'},
				                    { name: 'TGT_CHARGE_NM', type: 'string'},
									{ name: 'UNIT_NM', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 5px;">' + value + '</div>';
					};
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 5px 0px 0px;">' + value + '</div>';
					};
					var targetDetail = function (row, columnfield, value, defaulthtml, rowdata) {//그리드 선택시 하단 상세
						var acctid = $("#gridKpitarget").jqxGrid("getrowdata", row).ACCT_ID;
						var scid = $("#gridKpitarget").jqxGrid("getrowdata", row).SC_ID;
						var scnm = $("#gridKpitarget").jqxGrid("getrowdata", row).SC_NM;
						var mtcd = $("#gridKpitarget").jqxGrid("getrowdata", row).MT_CD;
						var kpiid = $("#gridKpitarget").jqxGrid("getrowdata", row).KPI_ID;
						var kpinm = $("#gridKpitarget").jqxGrid("getrowdata", row).KPI_NM; 
						
						
						var link = "<a href=\"javascript:makeTab3DetailTargetList('"+acctid+"','"+scid+"','"+scnm+"','"+mtcd+"','"+kpiid+"','"+escape(kpinm)+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
						
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);

		            $("#gridKpitarget").jqxGrid(
		            {
	              	 	width: '100%',
		                height:455,
						source: dataAdapter,
						theme:'blueish',
						altrows:true,
						sortable: true,
		                columnsresize: true,
						columnsheight: 25,
						rowsheight: 25,
						pageable: true,
						pagesizeoptions:[100,200,300],
						
						/* 
						columnsheight: 25,
						rowsheight: 25,
						*/
		                columns: [    
				                   { text: '조직ID', datafield: 'SC_ID', width: '8%', align:'center', cellsalign: 'center'},
				                   { text: '조직명', datafield: 'SC_NM', width: '22%', align:'center', cellsalign: 'left',  cellsrenderer: targetDetail },
								   { text: '지표코드', datafield: 'MT_CD', width: '14%', align:'center', cellsalign: 'center'},
								   { text: '지표명', datafield: 'KPI_NM', width: '26%', align:'center', cellsalign: 'left', cellsrenderer: targetDetail },
		 						   { text: '입력담당자', datafield: 'TGT_CHARGE_NM', width: '11%', align:'center', cellsalign: 'center'},
								   { text: '승인담당자', datafield: 'TGT_USER_NM', width: '11%', align:'center', cellsalign: 'center'},   
								   { text: '단위', datafield: 'UNIT_NM', width: '8%', align:'center', cellsalign: 'center' }  
		                ]
		            });
		            //setPagerLayout('gridKpitarget'); //페이저 레이아웃     
		            console.log("gridKpitarget inited");
        		});
        	}
        	function makeTab3DetailTargetList(acctid, scid, scnm, mtcd, kpiid, kpinm){
        		resetTarget();
        		
        		$("#btnTaregetreset").hide();
                $("#btnTargetinsert").hide();
                $("#btnTargetremove ").hide();
        		kpinm = unescape(kpinm); 
        		$("#txtTargetacctid").val(acctid);
        		$("#txtTargetscid").val(scid);
        		$("#txtTargetscnm").val(scnm);
        		$("#txtTargetmtcd").val(mtcd);
        		$("#txtTargetkpiid").val(kpiid);
        		$("#txtTargetkpinm").val(kpinm);
        		$("#targetScid").html(scid+"/"+scnm);
        		$("#targetMtcd").html(mtcd+"/"+kpinm);
        		var valid = $i.sqlhouse(305,{S_ACCT_ID:acctid, S_KPI_ID:kpiid});
        		valid.done(function(result){
					if(result.returnArray.length!=0&&result.returnArray[0].TGT_CHARGE_ID==result.returnArray[0].UNID){
						$("#btnTaregetreset").show();
						$("#btnTargetinsert").show();
						$("#btnTargetremove ").show();
					}else{
						$("#btnTaregetreset").hide();
						$("#btnTargetinsert").hide();
						$("#btnTargetremove ").hide();

					}
        		});
        		var date = $i.sqlhouse(204,{S_ACCT_ID:acctid, S_KPI_ID:kpiid, S_MT_CD:mtcd});
        		date.done(function(res){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'ACCT_ID', type: 'string'},
		                    { name: 'YYYY', type: 'string'},
		                    { name: 'KPI_ID', type: 'string'},
		                    { name: 'MT_CD', type: 'string'},
							{ name: 'TARGET_AMT', type: 'string'},
		                    { name: 'TARGET_CURRAMT', type: 'string'},
		                    { name: 'TGT_CHARGE_ID', type: 'string'},
		                    { name: 'UNID', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 5px;">' + value + '</div>';
					};
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 5px 0px 0px;">' + value + '</div>';
					};
					var detail = function (row, columnfield, value) {
						var acctid = $("#gridTargetamt").jqxGrid("getrowdata", row).ACCT_ID;
						var kpiid = $("#gridTargetamt").jqxGrid("getrowdata", row).KPI_ID;
						var yyyy = $("#gridTargetamt").jqxGrid("getrowdata", row).YYYY;
						var mm = $("#gridTargetamt").jqxGrid("getrowdata", row).MM;
						var tgtchargeid = $("#gridTargetamt").jqxGrid("getrowdata", row).TGT_CHARGE_ID;
						var unid = $("#gridTargetamt").jqxGrid("getrowdata", row).UNID;
						
						if(tgtchargeid==unid){
                			$("#btnTaregetreset").show();
                			$("#btnTargetinsert").show();
                			$("#btnTargetremove ").show();
                		}else{
                			$("#btnTaregetreset").hide();
                			$("#btnTargetinsert").hide();
                			$("#btnTargetremove ").hide();
                		}	
						var link = "<a href=\"javascript:detailTargetValue('"+acctid+"','"+kpiid+"','"+yyyy+"','"+mm+"','"+tgtchargeid+"','"+unid+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:center; margin:4px 0px 0px 0px;'>" + link + "</div>";
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            try{
						console.log("gridTargetamt init");
						$("#gridTargetamt").jqxGrid(
						{
							width: '100%',
							height:'100%',
							altrows:true,
							pageable: false,
							source: dataAdapter,
							theme:'blueish',
							sortable: true,
							columnsresize: true,
							columnsheight: 25,
							rowsheight: 25,
							columns: [
							   { text: '년도', datafield: 'YYYY', width: '20%', align:'center', cellsalign: 'center' ,  cellsrenderer: detail },
							   { text: '목표', datafield: 'TARGET_AMT', width:'40%', align:'center', cellsalign: 'center'},
							   { text: '기준값', datafield: 'TARGET_CURRAMT', width:'40%', align:'center', cellsalign:'center'}
							]
						});
					}catch(e){}
		            console.log("gridTargetamt inited");
        		});
        	}
        	function detailTargetValue(acctid, kpiid, yyyy, mm, tgtchargeid, unid){  
        		    
        		var data = $i.sqlhouse(205,{ACCT_ID:acctid,KPI_ID:kpiid,YYYY:yyyy,MM:mm,EVA_GBN:"B"});
        		data.done(function(res){
        			if(res.returnArray.length > 0){
        				$("#txtTargetorgyyyy").val(res.returnArray[0].YYYY);
        				$("#txtTargetyyyy").val(res.returnArray[0].YYYY);
        				$("#txtTargetamt").val(res.returnArray[0].TARGET_AMT);
        				$("#txtTargetcurramt").val(res.returnArray[0].TARGET_CURRAMT);
        				$("#btnhiddenchtargeid").val(res.returnArray[0].TGT_CHARGE_ID);
        				$("#btnhiddenunid").val(res.returnArray[0].UNID);
        				
                		if(tgtchargeid==unid){
                			$("#btnTaregetreset").show();
                			$("#btnTargetinsert").show();
                			$("#btnTargetremove ").show();
                		}else{
                			$("#btnTaregetreset").hide();
                			$("#btnTargetinsert").hide();
                			$("#btnTargetremove ").hide();
                		}	
        			}
        		});
        	}

        	function checkInputLimit(){ 
        		var pEvaGbn = "${param.pEvaGbn}";
        		var data = $i.sqlhouse(206,{S_EVA_GBN:pEvaGbn, S_YYYY:$("#cboSearchyear").val(),S_MM:$("#cboSearchResultMm").val()});
        		data.done(function(c){                                                 
        			if(c.returnCode=="SUCCESS"){            
        				if(c.returnArray.length > 0){
        					if(c.returnArray[0].CHECK_DAT == "1"){
	        					$("#messageLayer").html("실적입력기한이 종료되었습니다.");
	        					$("#btnInsertResult").hide();
	        					$("#btnAllChargeY").hide();
	        					$("#btnAllChargeN").hide();
	        					$("#btnAllUserY").hide();
	        					$("#btnAllUserN").hide();
								$(".confirmButton").hide();
	        				}else if(c.returnArray[0].CHECK_DAT == "2"){
	        					$("#messageLayer").html("실적입력기한 및 확인기한이 종료되었습니다.");
	        					$("#btnInsertResult").hide();    
	        					$("#btnAllChargeY").hide();
	        					$("#btnAllChargeN").hide();
	        					$("#btnAllUserY").hide();
	        					$("#btnAllUserN").hide();
								$(".confirmButton").hide();
	        				}else if(c.returnArray[0].CHECK_DAT == "0"){
	        					$("#messageLayer").html("");
	        					$("#btnInsertResult").show();
	        					$("#btnAllChargeY").show();
	        					$("#btnAllChargeN").show();
	        					$("#btnAllUserY").show();
	        					$("#btnAllUserN").show();
	        				}
        				}else{
        					$("#messageLayer").html("입력기간을 설정해주세요");
        					$("#btnInsertResult").hide();
        					$("#btnAllChargeY").hide();
        					$("#btnAllChargeN").hide();
        					$("#btnAllUserY").hide();
        					$("#btnAllUserN").hide();
        				}
        			}else{
        				$("#messageLayer").html("입력기간을 설정해주세요");
        				$("#btnInsertResult").hide();
    					$("#btnAllChargeY").hide();
    					$("#btnAllChargeN").hide();
    					$("#btnAllUserY").hide();
    					$("#btnAllUserN").hide();
        			}
        		});
        		
        	}
        	function makeResultList(){
        		
        		var data = $i.sqlhouse(207,{S_TOP_YYYY:$("#cboSearchyear").val(),S_YYYY:$("#cboSearchyear").val(),S_MM:$("#cboSearchResultMm").val(),S_SC_ID:$("#txtTabscid").val(),S_GUBN:$("#txtGubn").val(),S_EVA_GBN:"${param.pEvaGbn}", S_MT_ID:' ', S_SUBSTRA_ID:$("#txtTabSubstr").val(),S_OWNER_USER_NM:$("#txtSearchChargenm").val(),S_KPI_CHARGE_NM:$("#txtSearchChargenm").val(),S_KPI_NM:$("#txtInputSearchKpinm").val()});
        		data.done(function(res){
        			
        			if($("#resultInputList > tr").length > 0){
        				$("#resultInputList > tr").remove();                
        			}          
         			if(res.returnCode == "SUCCESS"){
        				if(res.returnArray.length > 0){
        					    
        					var html = "";
        					var col1List = new Array();
        					var dataList = res.returnArray;
        					var kpi_count = 0;
        					for(var i=0;i<dataList.length;i++){   
        						html +="<tr>";    
        						html +="<td style='width:5%;' class='scId"+dataList[i].KPI_ID+"'><div class='cell t_center'>"+dataList[i].SC_ID+"</div>";
       							html +="<input type='hidden' name='yyyymm' value='"+dataList[i].YYYYMM+"' />";
       							html +="<input type='hidden' name='kpiID' value='"+dataList[i].KPI_ID+"' />";
       							html +="<input type='hidden' name='analCycle' value='"+dataList[i].ANAL_CYCLE+"' />";
       							html +="<input type='hidden' name='colGbn' value='"+dataList[i].COL_GBN+"' />";
       							html +="<input type='hidden' name='scID' value='"+dataList[i].SC_ID+"' />";
       							html +="<input type='hidden' name='chargeName' value='"+dataList[i].CHARGE_NM+"' />";
       							html +="<input type='hidden' name='chargeDate' value='"+dataList[i].CHARGE_DATE+"' />";    
       							html +="<input type='hidden' name='chargeConfirm' data-kpiid='"+dataList[i].KPI_ID+"' data-scid='"+dataList[i].SC_ID+"' value='"+dataList[i].CHARGE_CONFIRM+"' />";  
       							html +="<input type='hidden' name='userDate' value='"+dataList[i].USER_DATE+"' />";
       							html +="<input type='hidden' name='chargeID' data-kpiid='"+dataList[i].KPI_ID+"' data-scid='"+dataList[i].SC_ID+"'  value='"+dataList[i].KPI_CHARGE_ID+"' />";
       							html +="<input type='hidden' name='userID' data-kpiid='"+dataList[i].KPI_ID+"' data-scid='"+dataList[i].SC_ID+"' value='"+dataList[i].OWNER_USER_ID+"' />";
       							html +="<input type='hidden' name='userName' value='"+dataList[i].OWNER_USER_NM+"' />";
       							html +="<input type='hidden' name='userConfirm' data-kpiid='"+dataList[i].KPI_ID+"' data-scid='"+dataList[i].SC_ID+"' value='"+dataList[i].USER_CONFIRM+"' />";
        						html +="</td>";      
        						html +="<td style='width:9%;'class='scNm"+dataList[i].KPI_ID+"'><div class='cell t_left'>"+dataList[i].SC_NM+"</div></td>";
        						html +="<td style='width:24%;'class='kpinm"+dataList[i].KPI_ID+"'><div class='cell t_left'>"+dataList[i].KPI_NM+"</div></td>";
        						html +="<td style='width:6%;' class='chargeName"+dataList[i].KPI_ID+"'><div class='cell t_center'>"+dataList[i].KPI_CHARGE_NM+"</div></td>";    
        						html +="<td style='width:8%;' class='chargeConfirm"+dataList[i].KPI_ID+"'>"; 
        						if(dataList[i].KPI_CHARGE_ID == dataList[i].UNID){                      
	        						if(dataList[i].USER_CONFIRM == ''|| dataList[i].USER_CONFIRM =='N'){
	        							if(dataList[i].CHARGE_CONFIRM == ''|| dataList[i].CHARGE_CONFIRM =='N'){
	        								html +="<div class='cell t_center'>미확인";
	        								html +="<div class='button type2'>";
	        								html +="<input type='button' class='confirmButton' data-btnrole='chargeY' name='btnConfrimY' width=100%; height=100%; value='확인' onclick=\"confirmOne("+kpi_count+",'Y','charge');\" />";     
	        								html +="</div>";    
	        								html +="</div>";   
	        								html +="</td>";
	            						}else if(dataList[i].CHARGE_CONFIRM =='Y'){
	            							html +="<div class='cell t_center'>확인";
	        								html +="<div class='button type3'>";   
	        								html +="<input type='button' class='confirmButton' data-btnrole='chargeN' name='btnConfrimN' style='width:100%; height:100%;' value='확인 취소' onclick=\"confirmOne("+kpi_count+",'N','charge');\"/>";
	        								html +="<div class='cell t_center'>"+dataList[i].CHARGE_DATE+"</div>";   
	        								html +="</div>";
	        								html +="</div>";
	        								html +="</td>";    
	            						}        
	        						}else if(dataList[i].USER_CONFIRM =='Y'){
	    								html +="<div class='cell t_center'>확인";
	    								html +="</div>";
	    								html +="<div class='cell t_center'>"+dataList[i].CHARGE_DATE+"</div>";  
	    								html +="</td>";
	        						}
        						}else if(dataList[i].KPI_CHARGE_ID != dataList[i].UNID){
        							if(dataList[i].USER_CONFIRM == ''|| dataList[i].USER_CONFIRM =='N'){
        								html +="<div class='cell t_center'>미확인";
        							}else if(dataList[i].CHARGE_CONFIRM == 'Y'){
        								html +="<div class='cell t_center'>확인";
        								html +="</div>";  
        								html +="<div class='cell t_center'>"+dataList[i].CHARGE_DATE+"</div>";  
        								html +="</td>";
        							}
        						}
        						html +="<td style='width:6%;' class='userName"+dataList[i].KPI_ID+"'><div class='cell t_center'>"+dataList[i].OWNER_USER_NM+"</div></td>";
        						html +="<td style='width:8%;' class='userConfirm"+dataList[i].KPI_ID+"'>"; 
        						if(dataList[i].OWNER_USER_ID == dataList[i].UNID){
	        						if(dataList[i].CHARGE_CONFIRM == ''|| dataList[i].CHARGE_CONFIRM =='N'){
	        								html +="<div class='cell t_center'>담당자미확인";
	        								html +="</div>";
	        								html +="</div>";
	        								html +="</td>";
	        						}else if(dataList[i].CHARGE_CONFIRM =='Y'){
	        							if(dataList[i].USER_CONFIRM == ''|| dataList[i].USER_CONFIRM =='N'){
	        								html +="<div class='cell t_center'>담당자미확인";
	        								html +="<div class='button type2'>";
	        								html +="<input type='button' class='confirmButton' data-btnrole='userY' name='btnConfrimY' style='width:100%; height:100%;' value='확인' onclick=\"confirmOne("+kpi_count+",'Y','user');\"/>";    
	        								html +="</div>";
	        								html +="</div>";
	        								html +="</td>";
	            						}else if(dataList[i].USER_CONFIRM =='Y'){      
	            							html +="<div class='cell t_center'>확인";
	        								html +="<div class='button type3'>";
	        								html +="<input type='button' class='confirmButton' data-btnrole='userN' name='btnConfrimN' style='width:100%; height:100%;' value='확인 취소' onclick=\"confirmOne("+kpi_count+",'N','user');\"/>";      
	        								html +="<div class='cell t_center'>"+dataList[i].USER_DATE+"</div>";   
	        								html +="</div>";
	        								html +="</div>";
	        								html +="</td>";
	            						}
	        						}   
        						}else if(dataList[i].OWNER_USER_ID != dataList[i].UNID){            
	        						if(dataList[i].CHARGE_CONFIRM == ''|| dataList[i].CHARGE_CONFIRM =='N'){
	    								html +="<div class='cell t_center'>담당자미확인";
	    								html +="</div>";
	    								html +="</div>";
	    								html +="</td>";
	    							}else if(dataList[i].CHARGE_CONFIRM =='Y'){   
	    								if(dataList[i].USER_CONFIRM == ''|| dataList[i].USER_CONFIRM =='N'){
	    								html +="<div class='cell t_center'>담당자미확인";
	    								html +="</div>";
	    								html +="</td>";
	    								}else if(dataList[i].USER_CONFIRM == 'Y'){
	        								html +="<div class='cell t_center'>확인";    
	        								html +="</div>";
	        								html +="<div class='cell t_center'>"+dataList[i].USER_DATE+"</div>";   
	        								html +="</td>";
	    								}
	    							}
        						}
        						html +="<td style='width:10%;'><div class='cell t_left'>"+dataList[i].COL_NM+"</div></td>";    
        						html +="<td style='width:6%;'><div class='cell t_center'>"+dataList[i].COL_UNIT_NM+"</div></td>";          
        						html +="<td style='width:13%;'>";  
       							html +="<input type='hidden' name='unid' value='"+dataList[i].UNID+"' />";
       							html +="<input type='hidden' name='colYyyymm' value='"+dataList[i].YYYYMM+"' />";
       							html +="<input type='hidden' name='colKpiID' value='"+dataList[i].KPI_ID+"' />";
       							html +="<input type='hidden' name='colAnalcycle' value='"+dataList[i].ANAL_CYCLE+"' />";
       							html +="<input type='hidden' name='colColgbn' value='"+dataList[i].COL_GBN+"' />";  
       							html +="<input type='hidden' class='input type2 t_right num_only' style='width:65%;' id='colValueOrg"+i+"' data-kpiid='"+dataList[i].KPI_ID+"' data-scid='"+dataList[i].SC_ID+"' value='"+dataList[i].COL_VALUE+"' />";
       							html +="<input type='hidden' class='input type2 t_right num_only' style='width:65%;' id='colValue"+i+"' name='colValue"+dataList[i].KPI_ID+"' data-index="+i+" value='"+dataList[i].COL_VALUE+"' />";
       							html +="<input type='hidden' class='input type2 t_right num_only' style='width:65%;'  name='colValueBefore'  value='"+dataList[i].COL_VALUE+"' />";
       							if(dataList[i].KPI_CHARGE_ID == dataList[i].UNID){   
       								if(dataList[i].COL_SYSTEM == 'A'){                     
       									if(dataList[i].CHARGE_CONFIRM == ''|| dataList[i].CHARGE_CONFIRM =='N'){          
       										html +="<div class='cell t_center'>";
       										html +="<input type='text' class='input type2 t_right num_only' style='width:90%;' data-kpiid='"+dataList[i].KPI_ID+"' data-gbn='"+dataList[i].COL_GBN+"' data-scid='"+dataList[i].SC_ID+"' name='colValue' onkeyup=\"changeValue("+i+");\" value='"+dataList[i].COL_VALUE+"' />";
       										html +="</div>";
       									}else if(dataList[i].CHARGE_CONFIRM == 'Y'){
       										html +="<div class='cell t_center'>";
       										html +="<input type='text'  style='width:90%;text-align:right !important; text-indent:0 !important; border:0;' data-kpiid='"+dataList[i].KPI_ID+"' data-gbn='"+dataList[i].COL_GBN+"' data-scid='"+dataList[i].SC_ID+"' name='colValue' value='"+dataList[i].COL_VALUE+"' readonly='readonly'/>";  
       										html +="</div>";
       									}
       								}else if(dataList[i].COL_SYSTEM != 'A'){
       									html +="<div class='cell t_center'>";
       									html +="<input type='text'  style='width:90%;text-align:right !important; text-indent:0 !important; border:0;' data-kpiid='"+dataList[i].KPI_ID+"' data-gbn='"+dataList[i].COL_GBN+"' data-scid='"+dataList[i].SC_ID+"' name='colValue' value='"+dataList[i].COL_VALUE+"' readonly='readonly'/>";  
   										html +="</div>";
       								}
       							}else if(dataList[i].KPI_CHARGE_ID != dataList[i].UNID){
       								html +="<div class='cell t_center'>";
       								html +="<input type='text' style='width:90%;text-align:right !important; text-indent:0 !important; border:0;' data-kpiid='"+dataList[i].KPI_ID+"' data-gbn='"+dataList[i].COL_GBN+"' data-scid='"+dataList[i].SC_ID+"' name='colValue' value='"+dataList[i].COL_VALUE+"' readonly='readonly'/>";  
									html +="</div>";      
       							}              
       							html +="</td>";   
        						html +="</tr>";
        					                 
        						if(i==0 || dataList[i].KPI_ID != dataList[i-1].KPI_ID) {
        							col1List.push(dataList[i].KPI_ID);
        							kpi_count++;
        						}
        					}
        					$("#resultInputList").append(html);
        					     
        					//실적관리 확인버튼 
        					if($("[name='btnConfrimY']").length > 0){
        						$("[name='btnConfrimY']").jqxButton({ width: '', theme:'blueish'});
        					}
        					if($("[name='btnConfrimN']").length > 0){
        						$("[name='btnConfrimN']").jqxButton({ width: '', theme:'blueish'});
        					}
        					
        					setEvent();
        					var rowspanCount1 = 0;
        				
        					for(var i=0; i<col1List.length; i++) {   
        						rowspanCount1 = $('.scId'+col1List[i]).length;
        						$('.scId'+col1List[i]+':eq(0)').attr("rowSpan", rowspanCount1);
        					 	$('.scId'+col1List[i]+':not(:eq(0))').remove();
								
								$('.scNm'+col1List[i]+':eq(0)').attr("rowSpan", rowspanCount1);
        					 	$('.scNm'+col1List[i]+':not(:eq(0))').remove();
        					 	
        					 	$('.kpinm'+col1List[i]+':eq(0)').attr("rowSpan", rowspanCount1);
        					 	$('.kpinm'+col1List[i]+':not(:eq(0))').remove();
        					 	
        					 	$('.chargeName'+col1List[i]+':eq(0)').attr("rowSpan", rowspanCount1);
        					 	$('.chargeName'+col1List[i]+':not(:eq(0))').remove();
        					 	   
        					 	$('.chargeConfirm'+col1List[i]+':eq(0)').attr("rowSpan", rowspanCount1);
        					 	$('.chargeConfirm'+col1List[i]+':not(:eq(0))').remove();
        					 	
        					 	$('.userName'+col1List[i]+':eq(0)').attr("rowSpan", rowspanCount1);
        					 	$('.userName'+col1List[i]+':not(:eq(0))').remove();
        					 	
        					 	$('.userConfirm'+col1List[i]+':eq(0)').attr("rowSpan", rowspanCount1);
        					 	$('.userConfirm'+col1List[i]+':not(:eq(0))').remove();   
								   
        					} 
        				}
        			}
        	        checkInputLimit();
        		});   
        	}
        	
           	
        	function insertTarget(){
        		if($("#txtTargetkpinm").val() == ""){     
        			$i.dialog.warning("SYSTEM","지표를 선택하세요");       
					return;
        		}  
        		
        		$("#txtTargetevagbn").val($("#txtEvagbn").val());
        		var check = $i.sqlhouse(208,{ACCT_ID:$("#txtTargetacctid").val(),EVA_GBN:$("#txtTargetevagbn").val(),YYYY:$("#txtTargetyyyy").val(),KPI_ID:$("#txtTargetkpiid").val()});
        		check.done(function(res){
        			if($("#txtTargetorgyyyy").val() == ""){
        				if(res.returnArray.length != 0){
        					$i.dialog.warning("SYSTEM","년도가 이미 사용중입니다");     
        					return ;
	        			}
        			}
        			
        			$i.post("./insertTargetAdmin","#targetSave").done(function(data){
        				
		        		if(data.returnCode == "EXCEPTION"){
		        			$i.dialog.error("SYSTEM",data.returnMessage);
		        		}else{
		        			$i.dialog.alert("SYSTEM","저장 되었습니다");
		        			makeTab3DetailTargetList($("#txtTargetacctid").val(), $("#txtTargetscid").val(), $("#txtTargetscnm").val(), $("#txtTargetmtcd").val(), $("#txtTargetkpiid").val(), $("#txtTargetkpinm").val());
		        			resetTarget();
		        		}
		        	});
        			
        		});
        	}
        	function removeTarget(){
        		if($("#txtTargetkpinm").val() == ""){     
        			$i.dialog.warning("SYSTEM","지표를 선택하세요");       
					return;
        		}        

        		$("#txtTargetevagbn").val($("#txtEvagbn").val());
        		$i.post("./removeTargetAdmin", "#targetSave").done(function(data){
        			if(data.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM",data.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM","삭제되었습니다");   
        				makeTab3DetailTargetList($("#txtTargetacctid").val(), $("#txtTargetscid").val(), $("#txtTargetscnm").val(), $("#txtTargetmtcd").val(), $("#txtTargetkpiid").val(), $("#txtTargetkpinm").val());
        				resetTarget();
        			}
        		});
        	}
        	function resetTarget(){
        		$("#txtTargetorgyyyy").val("");
        		$("#txtTargetyyyy").val("");
        		$("#txtTargetamt").val("");
        		$("#txtTargetcurramt").val("");
        	}
        	//실적관리 항목값 저장
        	function saveKpiResult(){
        		$("#hiddenAllSaveGubn").val("Kpivalue");
        		$i.post("./insertResultColValueAdmin", "#saveResult").done(function(data){
        			if(data.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM", data.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM", "저장 되었습니다");
        				makeResultList();
        			}
        		});
        	}
        	//실적관리담당자승인자 모두확인/취소
        	function confirmAll(confirm, gubn){
        		$("#hiddenAllSaveGubn").val("Kpivalue");
        		var  p = $i.post("./insertResultColValueAdmin", "#saveResult");
        		p.done(function(){
        			var size = $("[data-btnrole="+gubn+confirm+"]").length;
        			var task = null;
        			for(var i = 0; i < size;i++){
        				task = confirmFunction(i, confirm, gubn);
        			}
        			if(task!=null){
        				if(task.returnCode == "EXCEPTION"){
			        		if(confirm == "Y"){
			        			$i.dialog.error("SYSTEM","확인 중 오류가 발생했습니다.");
			        		}else{
			        			$i.dialog.error("SYSTEM","일괄 취소 중 오류가 발생했습니다.");
			        		}
			        	}else{
			        		var message = null;
			        		if(confirm == "Y"){
	      						message="확인 되었습니다";
	      					}else{
	      						message="취소 되었습니다";
	      					}           
			        		$i.dialog.alert("SYSTEM",message, makeResultList);		        		
		        		}
        			}
        			else{
        				$i.dialog.alert("SYSTEM","실적값을 먼저 입력하셔야합니다.");
        			}
        		}); 
        	}
        	
           	//실적관리 항목값 체크
        	function changeValue(i){
				if($("#messageLayer").html() == ""){
					$("[name='colValue']").eq(i).val($("[name='colValue']").eq(i).val().replace(/[^0-9.-]/g,''));
					var returnValue = "";
					var kpiId = $("[name='colValue']").eq(i).data("kpiid");   
					var hiddenInputBox = $("[name='colValue"+$("[name='colValue']").eq(i).data("kpiid")+"']");
					for(var i = 0;i<hiddenInputBox.length;i++){
						if($("[name='colValue"+$("[name='colValue']").eq(i).data("kpiid")+"']").eq(i).data("index") == i){
							$("[name='colValue"+$("[name='colValue']").eq(i).data("kpiid")+"']").eq(i).val($("[name='colValue']").eq(i).val());
						}
					}
					var result = "";
					for(var j=0;j<hiddenInputBox.length;j++){
						var index = $("[name='colValue"+$("[name='colValue']").eq(i).data("kpiid")+"']").eq(j).data("index");
						if($("[name='colValue"+$("[name='colValue']").eq(i).data("kpiid")+"']").eq(j).val() != ""){
							result += "var "+$("[name='colGbn']").eq(index).val()+" = "+ $("[name='colValue"+$("[name='colValue']").eq(i).data("kpiid")+"']").eq(j).val() + "; ";	
						}else{
							result += "var "+$("[name='colGbn']").eq(index).val()+" = 0; ";
						}
					}
				}else{
					$i.dialog.warning("SYSTEM",$("#messageLayer").html());
					$("[name='colValue']").eq(i).val($("#colValue"+i).val().replace(/[^0-9.-]/g,''));
				}
			}
           	
        	//실적관리 확인여부 확인체크
        	function confirmOne(i, confirm, gubn){   
        		$("#hiddenAllSaveGubn").val("Kpivalue");
        		var  p = $i.post("./insertResultColValueAdmin", "#saveResult");
        		p.done(function(){
        			var task = confirmFunction(i, confirm, gubn);
        			if(task!=null){
        				if(task.returnCode == "EXCEPTION"){
			        		if(confirm == "Y"){
			        			$i.dialog.error("SYSTEM","확인 중 오류가 발생했습니다.");
			        		}else{
			        			$i.dialog.error("SYSTEM","일괄 취소 중 오류가 발생했습니다.");
			        		}
			        	}else{
			        		var message = null;
			        		if(confirm == "Y"){
	      						message="확인 되었습니다";
	      					}else{
	      						message="취소 되었습니다";
	      					}           
			        		$i.dialog.alert("SYSTEM",message, makeResultList);		        		
		        		}
        			}
        			else{
        				$i.dialog.alert("SYSTEM","실적값을 먼저 입력하셔야합니다.");
        			}
        		}); 
        	}
        	function confirmFunction(i, confirm, gubn){
        		var kpiid = $("[name='kpiID']")[i].value;
        		for(var j = 0;j<$("[name='colValue"+kpiid+"']").length;j++){
        			if(($("[name='colValue"+kpiid+"']").eq(j).val() == null || $("[name='colValue"+kpiid+"']").eq(j).val() == "" )&&( $("input[type=text][data-kpiid='"+kpiid+"'").eq(j).val()=="" )&&confirm!="N" ){        				
						return null;	        				
        			}else{ 
        					$("#hiddenChargeOneYyyymm").val($("[name='yyyymm']")[i].value);
			        		$("#hiddenChargeOneKpiID").val($("[name='kpiID']")[i].value);
			        		$("#hiddenChargeOneID").val($("[name='chargeID']")[i].value);
			        		$("#hiddenChargeOneName").val($("[name='chargeName']")[i].value);   
			        		$("#hiddenChargeOneDate").val($("[name='chargeDate']")[i].value);   
			        		
			        		if(gubn == "charge"){
				        		$("#hiddenUserGubn").val("Charge");
				        		$("#hiddenChargeOneConfirm").val(confirm);
				        		$("#hiddenUserOneConfrim").val("");       
			        		}else{
				        		$("#hiddenUserGubn").val("User");
				        		$("#hiddenUserGubn").val(confirm);   
				        		$("#hiddenChargeOneConfirm").val($("[name='chargeConfirm']")[i].value);
				        		$("#hiddenUserOneConfirm").val(confirm);
			        		}
			        		$("#hiddenUserOneID").val($("[name='userID']")[i].value);        
			        		$("#hiddenUserOneName").val($("[name='userName']")[i].value);
			        		$("#hiddenUserOneDate").val($("[name='userDate']")[i].value); 
			        		 
        			}
        		}      
        		$("#hiddenAllSaveGubn").val("One");
        		var cfmTask = $i.sync.post("./insertResultColValueAdmin", "#oneSaveForm");
        		return cfmTask;
        	}
        	
        	
        	
        	//지표자체평가 저장
        	function insertGradeValue(){
        		if($("#mtCd").val() == ""){     
        			$i.dialog.warning("SYSTEM","평가지표를 선택하세요");       
					return;
        		}  
        		if($(':radio[name="gradeValue"]:checked').length == 0){
					$i.dialog.warning("SYSTEM","평가등급을 선택하세요");       
					return;
        		}   
    			$("#yyyy").val($("#cboSearchyear").val());
				$("#mm").val("12");      
				$("#grade").val( $(':radio[name="gradeValue"]:checked').val());
				$("#status").val("S");   
				var check =$i.post("./insertGradeValue","#gradeValueSave");
        		check.done(function(data){        
        				if(args.returnCode == "EXCEPTION"){
		        		$i.dialog.error("SYSTEM",args.returnMessage);
		        		}else{    
		        			$i.dialog.alert("SYSTEM","저장 되었습니다"); 
		        			kpiUserEvallist();            
		        			buttonChange();        
		        		}
		        	});
        			
        	}
        	//지표자체평가 확정
        	function insertGradeSubmit(){
        		var obj = $('#kpiUserlist').jqxGrid('selectrow', 0);
        		if($(':radio[name="gradeValue"]:checked').length == 0){
					$i.dialog.warning("SYSTEM","평가등급을 선택하세요");       
					return;
        		}
        		if($("#mtCd").val() == ""){     
        			$i.dialog.warning("SYSTEM","평가지표를 선택하세요");       
					return;
        		}  
    			$("#yyyy").val($("#cboSearchyear").val());
				$("#mm").val("12");      
				$("#grade").val( $(':radio[name="gradeValue"]:checked').val());
				$("#status").val("D");   
				var check =$i.post("./insertGradeValue","#gradeValueSave");
        		check.done(function(data){        
        				if(args.returnCode == "EXCEPTION"){
		        		$i.dialog.error("SYSTEM",args.returnMessage);
		        		}else{    
		        			$i.dialog.alert("SYSTEM","확정 되었습니다");
		        			kpiUserEvallist();      
		        			buttonChange();         
		        		}
		        	});
        			
        	}
        	//지표자체평가 확정취소
        	function insertGradeCancelSubmit(){
        		var obj = $('#kpiUserlist').jqxGrid('selectrow', 0);
        		if($(':radio[name="gradeValue"]:checked').length == 0){
					$i.dialog.warning("SYSTEM","평가등급을 선택하세요");       
					return;
        		}
        		if($("#mtCd").val() == ""){     
        			$i.dialog.warning("SYSTEM","평가지표를 선택하세요");       
					return;
        		}      
    			$("#yyyy").val($("#cboSearchyear").val());
				$("#status").val("F");   
				var check =$i.post("./updateGradeValue","#gradeValueSave");
        		check.done(function(data){        
        				if(args.returnCode == "EXCEPTION"){
		        		$i.dialog.error("SYSTEM",args.returnMessage);
		        		}else{    
		        			$i.dialog.alert("SYSTEM","확정취소 되었습니다");
		        			kpiUserEvallist();    
		        			buttonChange();     
		        		}
		        	});
        			
        	}
        	//지표자체평가 최종확정
        	function lastGradeSaveSubmit(){
        		var obj = $('#kpiUserlist').jqxGrid('selectrow', 0);
        		if($(':radio[name="gradeValue"]:checked').length == 0){
					$i.dialog.warning("SYSTEM","평가등급을 선택하세요");       
					return;
        		}
        		if($("#mtCd").val() == ""){     
        			$i.dialog.warning("SYSTEM","평가지표를 선택하세요");       
					return;
        		}      
    			$("#yyyy").val($("#cboSearchyear").val());
				$("#status").val("L");   
				$("#mm").val("12");      
				$("#grade").val( $(':radio[name="gradeValue"]:checked').val());
				var check =$i.post("./insertLastGradeValue","#gradeValueSave");
        		check.done(function(data){        
        				if(args.returnCode == "EXCEPTION"){
		        		$i.dialog.error("SYSTEM",args.returnMessage);
		        		}else{    
		        			$i.dialog.alert("SYSTEM","최종확정 되었습니다");
		        			kpiUserEvallist();    
		        			buttonChange();     
		        		}
		        	});
        			
        	}
        	//지표자체평가 최종확정취소
        	function lastGradeCencelSubmit(){
        		var obj = $('#kpiUserlist').jqxGrid('selectrow', 0);
        		if($(':radio[name="gradeValue"]:checked').length == 0){
					$i.dialog.warning("SYSTEM","평가등급을 선택하세요");       
					return;
        		}
        		if($("#mtCd").val() == ""){     
        			$i.dialog.warning("SYSTEM","평가지표를 선택하세요");       
					return;
        		}      
    			$("#yyyy").val($("#cboSearchyear").val());
				$("#status").val("D");   
				var check =$i.post("./updateLastGradeValue","#gradeValueSave");
        		check.done(function(data){        
        				if(args.returnCode == "EXCEPTION"){
		        		$i.dialog.error("SYSTEM",args.returnMessage);
		        		}else{    
		        			$i.dialog.alert("SYSTEM","최종 확정취소 되었습니다");
		        			kpiUserEvallist();    
		        			buttonChange();     
		        		}
		        	});
        			
        	}
        	//버튼 숨기기
        	function buttonChange(){
        					$("#btnUserEvalinsert").show();
							$("#btnUserEvalsave ").show();
							$("#btnUserEvalsubmit").show();
							$("#btnUserEvalsubmitCencel").hide();  
							$("#lastEvalsubmitSave").hide();
							$("#lastEvalsubmitCencel").hide();
        		var dat = $i.sqlhouse(296, {S_ACCT_ID:"${paramAcctid}",S_YYYY:$("#cboSearchyear").val(),S_EVA_GBN:"${param.pEvaGbn}",S_KPI_ID:$("#kpiId").val()});
        		dat.done(function(res){     
        			if(res.returnArray.length > 0){
						if(res.returnArray[0].STATUS == "S"){    
							$("#btnUserEvalinsert").show();    //초기화
							$("#btnUserEvalsave ").show();
							$("#btnUserEvalsubmit").show();    //확정
							$("#btnUserEvalsubmitCencel").hide();
							$("#lastEvalsubmitSave").hide();
							$("#lastEvalsubmitCencel").hide();
						//확정
						}else if(res.returnArray[0].STATUS == "D"){
							//실적입력자도 true 실적승인자도 true
							if($("#kpichargeId").val()==$("#unId").val() && $("#owneruserId").val()==$("#unId").val()){
								$("#btnUserEvalinsert").hide();   
								$("#btnUserEvalsave ").hide();
								$("#btnUserEvalsubmit").hide();
								$("#btnUserEvalsubmitCencel").show();
								$("#lastEvalsubmitSave").show();     //최종확정
								$("#lastEvalsubmitCencel").hide();
							}else if($("#owneruserId").val()==$("#unId").val()){
								$("#btnUserEvalinsert").hide();   
								$("#btnUserEvalsave ").hide();
								$("#btnUserEvalsubmit").hide();
								$("#btnUserEvalsubmitCencel").hide();  
								$("#lastEvalsubmitSave").show();     //최종확정
								$("#lastEvalsubmitCencel").hide();
							}else{
								$("#btnUserEvalinsert").show();   //초기화
								$("#btnUserEvalsave ").hide();
								$("#btnUserEvalsubmit").hide();
								$("#btnUserEvalsubmitCencel").show();   //확정취소
								$("#lastEvalsubmitSave").hide();     
								$("#lastEvalsubmitCencel").hide();
							}  
						//최종확정	
						}else if(res.returnArray[0].STATUS == "L"){
							//실적승인자도 true
							if($("#owneruserId").val()==$("#unId").val()){
								$("#btnUserEvalinsert").hide();   
								$("#btnUserEvalsave ").hide();
								$("#btnUserEvalsubmit").hide();
								$("#btnUserEvalsubmitCencel").hide();   
								$("#lastEvalsubmitSave").hide();     
								$("#lastEvalsubmitCencel").show();       //최종확정취소
							}else{
								$("#btnUserEvalinsert").hide();   
								$("#btnUserEvalsave ").hide();
								$("#btnUserEvalsubmit").hide();
								$("#btnUserEvalsubmitCencel").hide();   
								$("#lastEvalsubmitSave").hide();     
								$("#lastEvalsubmitCencel").hide();       //최종확정취소
							}
						//미확정	
						}else if(res.returnArray[0].STATUS == "F"){
							//실적입력자도 true
							if($("#kpichargeId").val()==$("#unId").val()){
								$("#btnUserEvalinsert").show();   
								$("#btnUserEvalsave ").show();
								$("#btnUserEvalsubmit").show();
								$("#btnUserEvalsubmitCencel").hide();
								$("#lastEvalsubmitSave").hide();
								$("#lastEvalsubmitCencel").hide();
							}else{
							   $("#btnUserEvalinsert").hide();   
								$("#btnUserEvalsave ").hide();
								$("#btnUserEvalsubmit").hide();
								$("#btnUserEvalsubmitCencel").hide();
								$("#lastEvalsubmitSave").hide();
								$("#lastEvalsubmitCencel").hide();
							}
						}else{     
							$("#btnUserEvalinsert").show();
							$("#btnUserEvalsave ").show();
							$("#btnUserEvalsubmit").show();
							$("#btnUserEvalsubmitCencel").hide();  
							$("#lastEvalsubmitSave").hide();
							$("#lastEvalsubmitCencel").hide();
						}
						
						checkInputLimit();
        			}
        		});
     			}

        	function getScidDetail(scid){ 
        		var dat = $i.post("./selectDetail",{scid:scid});
        		dat.done(function(res){
        			if(res.returnArray.length > 0){
        				$("#txtPscid").val(res.returnArray[0].PSC_ID);
        				$("#txtPscnm").val(res.returnArray[0].PSC_NM);
        				$("#txtScid").val(res.returnArray[0].SC_ID);
        				$("#txtScnm").val(res.returnArray[0].SC_NM);
        				$("#txtareaScdesc").val(res.returnArray[0].SC_DESC);
        				$("#txtareaEtcdesc").val(res.returnArray[0].ETC_DESC);
        				if(res.returnArray[0].EVAL_G != ''){
        					$("#cboEvalg").val(res.returnArray[0].EVAL_G);
        				}else{
        					$("#cboEvalg").jqxComboBox("selectIndex", 0);
        				}
        				if(res.returnArray[0].SC_UDC1 != ''){
        					$("#cboScudc1").val(res.returnArray[0].SC_UDC1);
        				}else{
        					$("#cboScudc1").jqxComboBox('selectIndex', 0 ); 
        				}
        				$("#txtPptefile").val(res.returnArray[0].PPTFILE);
        				$("#txtSortorder").val(res.returnArray[0].SORT_ORDER);
        				$("#dateStartdate").val(res.returnArray[0].START_DAT);
        				$("#dateEnddate").val(res.returnArray[0].END_DAT);
        			}
        		});
        		var deptlist = $i.post("./getListDept",{scid:scid});
        		deptlist.done(function(res){
        			if(res.returnArray.length > 0){
        				var tmpHtml = "";
						var deptCd = "";
						for(var i=0;i<res.returnArray.length;i++){
							if(i != 0){
								tmpHtml += "<br />";
								deptCd += ",";
							}
							tmpHtml += res.returnArray[i].SC_NM + " ( " + res.returnArray[i].DEPT_CD + " ) ";
							deptCd += res.returnArray[i].DEPT_CD;
						}
						$("#labeldept").html(tmpHtml);
						$("#txtDeptids").val(deptCd);
        			}else{
        				$("#labeldept").html('');
						$("#txtDeptids").val('');
        			}
        		});
        		var scidlist = $i.post("./getListScid",{scid:scid});
        		scidlist.done(function(res){
        			if(res.returnArray.length > 0){
        				var tmpHtml = "";
						var sustIds = "";
						for(var i=0;i<res.returnArray.length;i++){
							if(i != 0){
								tmpHtml += "<br />";
								sustIds += ", ";
							}
							tmpHtml += res.returnArray[i].SUST_NM + " ( " + res.returnArray[i].SUST_ID + " ) ";
							sustIds += res.returnArray[i].SUST_ID;
						}
						$("#labelsustid").html(tmpHtml);
						$("#txtSustIds").val(sustIds);
        			}else{
        				$("#labelsustid").html('');
						$("#txtSustIds").val(''); 
        			}
        		});
        	}
 

        	function popupMappingKpiUser(acctid, scid, kpiid,  gubn){
        		window.open('./popupMappingKpiUser?acctid='+acctid+'&scid='+scid+'&kpiid='+kpiid+'&gubn='+gubn, 'popupMappingKpiUser','scrollbars=no, resizable=no, width=930, height=400');
        	}
        	function reset(){   
        		$("#txtPscid").val("");
        		$("#txtPscnm").val("");
        		$("#txtScid").val("");
        		$("#txtScnm").val("");
        		$("#txtareaScdesc").val("");
        		$("#txtareaEtcdesc").val("");
        		$("#txtSortorder").val("");
        		$("#dateStartdate").val("");
        		$("#dateEnddate").val("");
        		$("#labeldept").html("");
        		$("#txtDeptids").val("");
        		$("#labelsustid").html("");
        		$("#txtSustids").val("");
        		$("#labelShow").val("");
        		   

        	}
        	function selectDisplay(obj){
				if(obj == "save"){
					$("#save").show();
					$("#list").hide();
				}else if(obj == "list"){
					$("#save").hide();
					$("#list").show();
				}
			}
        	
        	function mappingSustId(){
				var sustId = $("#pSustId").val();
				var appLength = $("input[name='appValue']").length;
				var appValue = appLength - 1;
				var sustLength = "";
				if(sustId == ""){
					sustLength = 0;
				}else{
					sustLength = sustId.split(",").length;
				}
				$("#appValue"+appValue).html(sustLength);
				$("input[name='appValue']").eq(appValue).val(sustLength);
				$("#pSustWeight").val($("input[name='kpiCurrentAmt']").eq(appValue).val()/sustLength);
			}
        	
        	function kpiAttachFilelist(){
        		$("#kpiAttachFilelist").empty();
        		var iframe = '';
        		                                            
        		iframe += '<iframe width="100%" height="500px" src="../KpiAttachFileUser/KpiAttachFileByUser?pEvaGbn='+$("#resultEvaGbn").val()+'&yyyy='+$("#cboSearchyear").val()+'" frameborder="no" allowTransparency="true">';
				iframe += '</iframe>';   
				     
				$("#kpiAttachFilelist").append(iframe);		
        	}
			//담당자 평가
        	function kpiUserEvallist(){
        		var dat = $i.sqlhouse(242, {S_SC_ID:$("#txtTabscid").val(),S_GUBN:$("#txtGubn").val(),S_YYYY:$("#cboSearchyear").val(),S_EVA_GBN:"${param.pEvaGbn}",S_ACCT_ID:"${paramAcctid}"});
        		dat.done(function(res){        
        			var source =   
					{
		                datatype: "json",   
		                datafields: [
							{ name: 'ACCT_ID', type: 'string'},
							{ name: 'KPI_ID', type: 'string'},
		                    { name: 'SC_ID', type: 'string'},
		                    { name: 'OWNER_USER_ID', type: 'string'},
		                    { name: 'KPI_CHARGE_ID', type: 'string'},
		                    { name: 'SC_NM', type: 'string'},
		                    { name: 'MT_ID', type: 'string'},
		                    { name: 'MT_CD', type: 'string'},
		                    { name: 'STATUS', type: 'string'},
		                    { name: 'KPI_NM', type: 'string'},
		                    { name: 'UNID', type: 'string'}
		                ],
		                localdata: res
		            };
                var alginLeft = function (row, columnfield, value) {//left정렬
                	return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 5px;">' + value + '</div>';
                							
                }; 
                var alginRight = function (row, columnfield, value) {//right정렬
                     return '<div id="userName-' + row + '"style="text-align:right; margin:4px 5px 0px 0px;">' + value + '</div>';
                }; 	
				var dataAdapter = new $.jqx.dataAdapter(source, {

                downloadComplete: function (data, status, xhr) { },

                loadComplete: function (data) { },

                loadError: function (xhr, status, error) { }

                });
				var dataAdapter = new $.jqx.dataAdapter(source);
				
				var detailRow = function (row, columnfield, value) {  
					var acctId = $("#kpiUserlist").jqxGrid("getrowdata", row).ACCT_ID;
					var scNm = $("#kpiUserlist").jqxGrid("getrowdata", row).SC_NM;
					var kpiNm = $("#kpiUserlist").jqxGrid("getrowdata", row).KPI_NM;
					var scId = $("#kpiUserlist").jqxGrid("getrowdata", row).SC_ID;
					var kpiId = $("#kpiUserlist").jqxGrid("getrowdata", row).KPI_ID;
					var mtId = $("#kpiUserlist").jqxGrid("getrowdata", row).MT_ID;
					var mtCd = $("#kpiUserlist").jqxGrid("getrowdata", row).MT_CD;
					var status = $("#kpiUserlist").jqxGrid("getrowdata", row).STATUS;
					var owneruserId = $("#kpiUserlist").jqxGrid("getrowdata", row).OWNER_USER_ID;
					var kpichargeId = $("#kpiUserlist").jqxGrid("getrowdata", row).KPI_CHARGE_ID;
					var unId = $("#kpiUserlist").jqxGrid("getrowdata", row).UNID;
					
					     
					var link = "<a href=\"javascript:getDataList('"+acctId+"','"+scNm+"','"+kpiNm+"','"+scId+"','"+kpiId+"','"+mtId+"','"+mtCd+"','"+status+"','"+owneruserId+"','"+kpichargeId+"','"+unId+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
					return "<div style='text-align:left; margin-left:10px !important; margin:4px 0px 0px 0px;'>" + link + "</div>";  
				};          
				var statusRow = function (row,datafieId,value) {        
					var status = $("#kpiUserlist").jqxGrid("getrowdata", row).STATUS;
					     
					if(status == "S") {   
	    				return "<div style='text-align:center; margin:4px 0px 0px 0px;'" + row + "'>미확정</div>";	   
	    			}else if(status == "D"){
	    				return "<div style='text-align:center; margin:4px 0px 0px 0px;'" + row + "'>확정</div>";	
	    			}else if(status == "F"){
	    				return "<div style='text-align:center; margin:4px 0px 0px 0px;'" + row + "'>미평가</div>";	
	    			}else if(status == "L"){
	    				return "<div style='text-align:center; margin:4px 0px 0px 0px;'" + row + "'>최종확정</div>";	
	    			}else{
	    				return "<div style='text-align:center; margin:4px 0px 0px 0px;'" + row + "'>미평가</div>";	
	    			}
					return "<div style='text-align:center; margin-left:10px !important; margin:4px 0px 0px 0px;'" + row + "'>"+ status +"</div>";
				};       
				      
				$("#kpiUserlist").jqxGrid(               
                {
                	width: '100%',
                    height:'100%',
                	altrows:true,
                	pageable: false,
                    source: dataAdapter,
                	theme:'blueish',
                	sortable: true,
                    columnsresize: true,
                	columnsheight: 25,
                	rowsheight: 25,
                    columns: [    
                              { text: '조직명', datafield: 'SC_NM', width: '25%', align:'center', cellsalign: 'center'},
                              { text: '지표코드', datafield: 'MT_CD', width: '15%', align:'center', cellsalign: 'center' },
            				  { text: '지표명', datafield: 'KPI_NM', width: '40%', align:'center', cellsalign: 'left',  cellsrenderer: detailRow },       
            				  { text: '평가여부', datafield: 'STATUS', width: '20%', align:'center',cellsalign: 'center',  cellsrenderer: statusRow}
                  			 ]                
                });
            	});      
        		                           
			}

			function byteCheck(code) {
				var size = 0;
				for (i = 0; i < code.length; i++) {
					var temp = code.charAt(i);
					if (escape(temp) == '%0D')
				   		continue;
				  	if (escape(temp).indexOf("%u") != -1) {
				   		size += 3;
				  	} else {
				   		size++;
				 	}
			 	}
			 	return size;
			}
			function checkInput(id, size, gubn){
	    		if(byteCheck($("#"+id).val().replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/'"'/g,'&quot;')) > size){
	    			var length = "";
	    			if(gubn == "en"){
	   					length = parseInt((size / 1));	
	    				$i.dialog.alert("SYSTEM","1 ~ " + length + "자(영문 기준)의 영문/_ 혼용만 가능합니다.");
	    			}else if(gubn == "all"){
	   					length = parseInt((size / 3));
	    				$i.dialog.alert("SYSTEM","1 ~ " + length + "자(한글 기준)의 한글/영문/특수문자 혼용만 가능합니다.");
	    			}else if(gubn =="num"){
	   					length = parseInt((size / 1));	
	    				$i.dialog.alert("SYSTEM","1 ~ " + length + "자의 숫자만 가능합니다.");
	    			}else if(gubn =="kor"){
	   					length = parseInt((size / 3));
	    				$i.dialog.alert("SYSTEM","1 ~ " + length + "자(한글 기준)의 한글만 가능합니다.");
	    			}else if(gubn=="enNum"){
	   					length = parseInt((size / 1));
	    				$i.dialog.alert("SYSTEM","1 ~ " + length + "자(영문 기준)의 영문/_ /숫자 혼용만 가능합니다.");
	    			}else if(gubn=="query"){
	   					length = parseInt((size / 1));
	    				$i.dialog.alert("SYSTEM","1 ~ " + length + "의(영문 기준) 쿼리문장만 가능합니다.");
	    			}
	    			$("#"+id).focus();
	    			return false;
	    		}
	    	}
			//평가등급
			function makeRadio(){
				var radioData = $i.service("makeRadioButton",[]);      
				radioData.done(function(data){   
					var radio = "";       
					$("#radioList").empty();   
					if(data.returnArray!=null){
						for(var i=0;i<data.returnArray.length;i++){                               
							radio += '<input type="radio" class="m_b2 m_r3 m_l5" id="grade_'+data.returnArray[i].EVA_GRD+'" name="gradeValue" value="'+data.returnArray[i].EVA_GRD+'"/>'+data.returnArray[i].EVA_GRDNM;
						}                                   
						$("#radioList").append(radio);
					}
				});          
			}   
			//증빙자료   
			function fileDownLoad(fileid,kpiId,yyyy){              
				location.replace("./kpidownload?fileid="+fileid+"&kpiId="+kpiId+"&yyyy="+yyyy);  
				
			}  
        	//지표자체평가데이터List
        	function getDataList(acctId,scNm,kpiNm,scId,kpiId,mtId,mtCd,status,owneruserId,kpichargeId,unId){
        		resetGradeValue();
        		var html = '';    
        		//label(조직명,지표명)
        		$("#scIdlabel").html(scId+"/"+scNm);
        		$("#kpiIdlabel").html(mtCd+"/"+kpiNm);     
        		           
				$('#fileList').empty();
				$('#kpiList').empty();
				$('#colList').empty();
				
        		$("#acctId").val(acctId);
        		$("#scNm").val(scNm);     
        		$("#kpiNm").val(kpiNm);
        		$("#scId").val(scId);     
        		$("#kpiId").val(kpiId);
        		$("#mtId").val(mtId); 
        		$("#mtCd").val(mtCd); 
        		$("#owneruserId").val(owneruserId); 
        		$("#kpichargeId").val(kpichargeId); 
        		$("#unId").val(unId); 
        		
        		var status = $("#status").val(status);
        		if(kpiId == ""){
        			kpiId = $("#kpiId").val();     
        		}  
        		//지표실적, 목표실적, 달성률  
        		var kpiData = $i.service("getKpiTargetData",[$("#cboSearchyear").val(),"${param.pEvaGbn}",kpiId]);
        		kpiData.done(function(data){   
        		var kpiList = "";            
				if(data.returnArray.length >0){            
					kpiList += '<tr>';
					kpiList += '<td style="text-align:right; padding-right:5px !important;">'+data.returnArray[0].KPI_VALUE+'</td>';
					kpiList += '<td style="text-align:right; padding-right:5px !important;">'+data.returnArray[0].TARGET_AMT+'</td>';
					kpiList += '<td style="text-align:right; padding-right:5px !important;">'+data.returnArray[0].GOA+'</td>';
					kpiList += '</tr>';	      
				}else{    
					kpiList += '<tr>';       
					kpiList += '<td class="cell t_center" colspan="3">No data to display</td>';
					kpiList += '</tr>';  
				}
				$("#kpiList").append(kpiList);
        		});
 				//항목, 항목실적   
				var mainData = $i.service("kpicolValueList",[$("#cboSearchyear").val(),"${param.pEvaGbn}",kpiId]);
				mainData.done(function(data){
					var tableData = "";   
					for(var i=0;i<data.returnArray.length;i++){
						tableData += '<tr>';
						tableData += '<td style="padding-left:5px !important;">'+data.returnArray[i].COL_NM+'</td>';
						tableData += '<td style="padding-left:5px !important;">'+data.returnArray[i].COL_UNIT_NM+'</td>';
						tableData += '<td style="text-align:right; padding-right:5px !important;">'+data.returnArray[i].COL_VALUE+'</td>';
						tableData += '</tr>';     
					}
					if(tableData==""){         
						tableData += '<tr>';    
						tableData += '<td colspan="3" class="cell t_center">No data to display</td>';   
						tableData += '</tr>';
					}     
					$("#colList").append(tableData);
				});
				
				//증빙자료  
				var fileList = $i.service("kpifileList",[$("#cboSearchyear").val(),kpiId]);
				fileList.done(function(data){
				if(data.returnArray.length>0){   
					for(var j=0;j<data.returnArray.length;j++){
						var ext = data.returnArray[j].FILE_EXT;
						ext = ext.toLowerCase();  
						       
						var img = "";
						if(ext == 'ppt' || ext == 'pptx') {      
							img = '<img src="../../resources/cmresource/image/fileExtIcon/ppt.png" alt="pdf"/>';
						} else if(ext == 'doc' || ext == 'docx') {
							img = '<img src="../../resources/cmresource/image/fileExtIcon/doc.png" alt="word"/>';
						} else if(ext == 'hwp') {
							img = '<img src="../../resources/cmresource/image/fileExtIcon/hwp.png" alt="hwp"/>';
						} else if(ext == 'pdf') {
							img = '<img src="../../resources/cmresource/image/fileExtIcon/pdf.png" alt="pdf"/>';
						} else if(ext == 'txt') {
							img = '<img src="../../resources/cmresource/image/fileExtIcon/txt.png" alt="txt"/>';
						} else if(ext == 'xls' || ext == 'xlsx'){   
							img = '<img src="../../resources/cmresource/image/fileExtIcon/xls.png" alt="xls"/>';    
						} else {          
							img = '<img src="../../resources/cmresource/image/fileExtIcon/etc.png" alt="etc"/>';     
						}     
						var filename = data.returnArray[j].FILE_ORG_NAME;
						var fullFilename = filename;
						   
						if(filename != null && filename.length>14) {
							filename = filename.substring(0, 14) + '...';
						}
						html += '<tr>';  
						html += "<td align='left' style='padding-left:5px !important;'><p class='label-link-table' title='"+fullFilename+"'><a href=\"javaScript:fileDownLoad('"+data.returnArray[j].FILE_ID+"','"+data.returnArray[j].KPI_ID+"','"+data.returnArray[j].YYYY+"')\";>"+ img + data.returnArray[j].FILE_ORG_NAME + "</a></p></td>";
						html += '</tr>';
					}      
				}else{   
					html += '<tr>';    
					html += '<td class="cell t_center">No data to display</td>';   
					html += '</tr>';
				}
				$('#fileList').append(html); 
				});
				//확정,확정취소 버튼 숨기기
				//buttonChange(변수,변수,변수);
				buttonChange();
 
				//주요성과, 개선사항
				var resGrdList = $i.service("kpiChargeGradeDesc",[$("#cboSearchyear").val(),"${param.pEvaGbn}",kpiId]);
				resGrdList.done(function(resGrdList){
				if(resGrdList.returnArray!=null&&resGrdList.returnArray.length > 0){  
					if(resGrdList.returnArray[0].GRADE != ""){
						$("#grade_"+resGrdList.returnArray[0].GRADE).prop("checked","checked");	
						$("#gradeDesc").val(resGrdList.returnArray[0].GRADE_DESC);
						$("#ipvDesc").val(resGrdList.returnArray[0].IPV_DESC);
					}	
				}

				});
        	}     
			function resetGradeValue() {
				$('#gradeDesc').val("");
				$("#ipvDesc").val("");  
				$('#gradeDescResult').val("");
				$("#ipvDescResult").val("");  
				$('#kpiEvalGradeDesc').val("");
				$("#kpiEvalipvDesc").val("");  
				$('input:radio[name="gradeValue"]').prop('checked', "");  
			}

        	//평가결과
    		function kpiEvalresult(){
        		var dat = $i.sqlhouse(242, {S_SC_ID:$("#txtTabscid").val(),S_GUBN:$("#txtGubn").val(),S_YYYY:$("#cboSearchyear").val(),S_EVA_GBN:"${param.pEvaGbn}",S_ACCT_ID:"${paramAcctid}"});
        		dat.done(function(res){
        			var source =      
					{
		                datatype: "json",   
		                datafields: [
							{ name: 'ACCT_ID', type: 'string'},
							{ name: 'KPI_ID', type: 'string'},
		                    { name: 'SC_ID', type: 'string'},
		                    { name: 'SC_NM', type: 'string'},
		                    { name: 'MT_ID', type: 'string'},
		                    { name: 'MT_CD', type: 'string'},
		                    { name: 'STATUS', type: 'string'},
		                    { name: 'GRADE', type: 'string'}, 
		                    { name: 'EVA_GRDNM', type: 'string'},
		                    { name: 'STATUS_EVAL', type: 'string'},
		                    { name: 'EVA_GRDNM_EVAL', type: 'string'},		                    
		                    { name: 'KPI_NM', type: 'string'}               
		                ],
		                localdata: res
		            };
                var alginLeft = function (row, columnfield, value) {//left정렬
                	return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 5px;">' + value + '</div>';
                							
                }; 
                var alginRight = function (row, columnfield, value) {//right정렬
                     return '<div id="userName-' + row + '"style="text-align:right; margin:4px 5px 0px 0px;">' + value + '</div>';
                }; 	
				var dataAdapter = new $.jqx.dataAdapter(source, {

                downloadComplete: function (data, status, xhr) { },

                loadComplete: function (data) { },

                loadError: function (xhr, status, error) { }

                });
				var dataAdapter = new $.jqx.dataAdapter(source);
				
				var detailRow = function (row, columnfield, value) {  
					var acctId = $("#kpiEvalresultlist").jqxGrid("getrowdata", row).ACCT_ID;
					var scNm = $("#kpiEvalresultlist").jqxGrid("getrowdata", row).SC_NM;
					var kpiNm = $("#kpiEvalresultlist").jqxGrid("getrowdata", row).KPI_NM;
					var scId = $("#kpiEvalresultlist").jqxGrid("getrowdata", row).SC_ID;
					var kpiId = $("#kpiEvalresultlist").jqxGrid("getrowdata", row).KPI_ID;
					var mtId = $("#kpiEvalresultlist").jqxGrid("getrowdata", row).MT_ID;
					var mtCd = $("#kpiEvalresultlist").jqxGrid("getrowdata", row).MT_CD;
					var status = $("#kpiEvalresultlist").jqxGrid("getrowdata", row).STATUS;    
					var link = "<a href=\"javascript:getDataListResult('"+acctId+"','"+scNm+"','"+kpiNm+"','"+scId+"','"+kpiId+"','"+mtId+"','"+mtCd+"','"+status+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
					return "<div style='text-align:left; margin-left:10px !important; margin:4px 0px 0px 0px;'>" + link + "</div>";
				};  
				//담당자평가
				var hiddenGrade = function (row, columnfield, value) {  
					var status = $("#kpiEvalresultlist").jqxGrid("getrowdata", row).STATUS;
					var evagrdnm = $("#kpiEvalresultlist").jqxGrid("getrowdata", row).EVA_GRDNM;
					if(status == 'L'){
	            		return "<div style='text-align:center;margin-top:4px;'>" + value + "</div>";
	            	}else{
	            		return "<div style='text-align:center;margin-top:4px;color:red;'>미평가</div>";
	            	}
				}
				//평가자평가
				var hiddenGradeEval = function (row, columnfield, value) {  
					var statusEval = $("#kpiEvalresultlist").jqxGrid("getrowdata", row).STATUS_EVAL;
					var evagrdnmEval = $("#kpiEvalresultlist").jqxGrid("getrowdata", row).EVA_GRDNM_EVAL;
					if(statusEval == 'D'){
	            		return "<div style='text-align:center;margin-top:4px;'>" + value + "</div>";
	            	}else{
	            		return "<div style='text-align:center;margin-top:4px;color:red;'>미평가</div>";
	            	}
				}
				$("#kpiEvalresultlist").jqxGrid(
                {
                	width: '100%',  
                    height:'100%',
                	altrows:true,
                	pageable: false,
                    source: dataAdapter,
                	theme:'blueish',
                	sortable: true,
                    columnsresize: true,
                	columnsheight: 25,
                	rowsheight: 25,   
                    columns: [
                              { text: '조직명', datafield: 'SC_NM', width: '15%', align:'center', cellsalign: 'center'},
                              { text: '지표코드', datafield: 'MT_CD', width: '15%', align:'center', cellsalign: 'center' },
            				  { text: '지표명', datafield: 'KPI_NM', width: '40%', align:'center', cellsalign: 'left',  cellsrenderer: detailRow },       
            				  { text: '담당자평가등급', datafield: 'EVA_GRDNM', width: '15%', align:'center',cellsalign: 'center',  cellsrenderer: hiddenGrade},     
            				   { text: '평가자평가등급', datafield: 'EVA_GRDNM_EVAL', width: '15%', align:'center', cellsalign: 'left',  cellsrenderer: hiddenGradeEval }     
                  			 ]      
                });
            	}); 
        	
        	}     
        	//평가결과List
        	function getDataListResult(acctId,scNm,kpiNm,scId,kpiId,mtId,mtCd,status){
        		resetGradeValue();
        		var html = '';               
        		//label(조직명,지표명)
        		$("#scIdlabelResult").html(scId+"/"+scNm);
        		$("#kpiIdlabelResult").html(mtCd+"/"+kpiNm);  
        		        
				$('#fileListResult').empty();
				$('#kpiListResult').empty();
				$('#colListResult').empty();				
				
        		$("#acctIdResult").val(acctId);
        		$("#scNmResult").val(scNm);     
        		$("#kpiNmResult").val(kpiNm);
        		$("#scIdResult").val(scId);     
        		$("#kpiIdResult").val(kpiId);
        		$("#mtIdResult").val(mtId); 
        		$("#mtCdResult").val(mtCd); 
        		    
        		var status = $("#statusResult").val(status);
        		if(kpiId == ""){
        			kpiId = $("#kpiIdResult").val();     
        		}   
				if($("#statusResult").val() =="L"){     
					$("#resultCharge").hide(); 
				}else{
					$("#resultCharge").show(); 
				}    
        		//지표실적, 목표실적, 달성률  
        		var kpiDataResult = $i.service("getKpiTargetData",[$("#cboSearchyear").val(),"${param.pEvaGbn}",kpiId]);
        		kpiDataResult.done(function(data){   
        		var kpiListResult = "";            
				if(data.returnArray.length >0){              
					kpiListResult += '<tr>';
					kpiListResult += '<td style="text-align:right; padding-right:5px !important;">'+data.returnArray[0].KPI_VALUE+'</td>';
					kpiListResult += '<td style="text-align:right; padding-right:5px !important;">'+data.returnArray[0].TARGET_AMT+'</td>';
					kpiListResult += '<td style="text-align:right; padding-right:5px !important;">'+data.returnArray[0].GOA+'</td>';
					kpiListResult += '</tr>';	      
				}else{    
					kpiListResult += '<tr>';       
					kpiListResult += '<td class="cell t_center" colspan="3">No data to display</td>';
					kpiListResult += '</tr>';  
				}
				$("#kpiListResult").append(kpiListResult);
        		});
 				//항목, 항목실적   
				var mainDataResult = $i.service("kpicolValueList",[$("#cboSearchyear").val(),"${param.pEvaGbn}",kpiId]);
				mainDataResult.done(function(data){
					var tableDataResult = "";   
					for(var i=0;i<data.returnArray.length;i++){
						tableDataResult += '<tr>';
						tableDataResult += '<td style="padding-left:5px !important;">'+data.returnArray[i].COL_NM+'</td>';
						tableDataResult += '<td style="padding-left:5px !important;">'+data.returnArray[i].COL_UNIT_NM+'</td>';
						tableDataResult += '<td style="text-align:right; padding-right:5px !important;">'+data.returnArray[i].COL_VALUE+'</td>';
						tableDataResult += '</tr>';     
					}
					if(tableDataResult==""){   
						tableDataResult += '<tr>';    
						tableDataResult += '<td colspan="2" class="cell t_center">No data to display</td>';   
						tableDataResult += '</tr>';
					}  
					$("#colListResult").append(tableDataResult);
				});
				//증빙자료  
				var fileListResult = $i.service("kpifileList",[$("#cboSearchyear").val(),kpiId]);
				fileListResult.done(function(data){
				if(data.returnArray.length>0){
					for(var j=0;j<data.returnArray.length;j++){
						var ext = data.returnArray[j].FILE_EXT;
						ext = ext.toLowerCase();    
						       
						var img = "";
						if(ext == 'ppt' || ext == 'pptx') {      
							img = '<img src="../../resources/cmresource/image/fileExtIcon/ppt.png" alt="ppt"/>';
						} else if(ext == 'doc' || ext == 'docx') {
							img = '<img src="../../resources/cmresource/image/fileExtIcon/doc.png" alt="word"/>';
						} else if(ext == 'hwp') {
							img = '<img src="../../resources/cmresource/image/fileExtIcon/hwp.png" alt="hwp"/>';
						} else if(ext == 'pdf') {
							img = '<img src="../../resources/cmresource/image/fileExtIcon/pdf.png" alt="pdf"/>';
						} else if(ext == 'txt') {
							img = '<img src="../../resources/cmresource/image/fileExtIcon/txt.png" alt="txt"/>';
						} else if(ext == 'xls' || ext == 'xlsx'){   
							img = '<img src="../../resources/cmresource/image/fileExtIcon/xls.png" alt="xls"/>';    
						}else if(ext == ""){   
							img = '';    
						} else {          
							img = '<img src="../../resources/cmresource/image/fileExtIcon/etc.png" alt="etc"/>';     
						}   
						
						var filename = data.returnArray[j].FILE_ORG_NAME;
						var fullFilename = filename;
						
						if(data != null && data.returnArray.length>14) {
							filename = filename.substring(0, 14) + '...';  
						}
						html += '<tr>';  
						html += "<td align='left' style='padding-left:5px !important;'><p class='label-link-table' title='"+fullFilename+"'><a href=\"javaScript:fileDownLoad('"+data.returnArray[j].FILE_ID+"','"+data.returnArray[j].KPI_ID+"','"+data.returnArray[j].YYYY+"')\";>"+ img + data.returnArray[j].FILE_ORG_NAME + "</a></p></td>";
						html += '</tr>'; 
					}      
				}else{   
					html += '<tr>';    
					html += '<td class="cell t_center">No data to display</td>';   
					html += '</tr>';
				}
				$('#fileListResult').append(html); 
				});
				
				//평가결과: 평가근거, 개선사항(담당자 평가)
				var resChargeListResult = $i.service("kpiGradeDesc",[$("#cboSearchyear").val(),"${param.pEvaGbn}",kpiId]);
				resChargeListResult.done(function(data){   
					for(var i=0;i<data.returnArray.length;i++){    
			        	$("#gradeDescResult").val(data.returnArray[i].GRADE_DESC);
				        $("#ipvDescResult").val(data.returnArray[i].IPV_DESC);  
				        $("#evalStatus").val(data.returnArray[i].STATUS);
				        $("#gradeCharge").html(data.returnArray[i].EVA_GRDNM);    
				        //최종확정(L)     
				        if($("#evalStatus").val()=="L"){
							$("#resultCharge").hide();    
						}else{  
							$("#resultCharge").show(); 
							$("#resultCharge").val("");  
						}  
                     }
					  
				});           
				//평가결과: 평가근거, 개선사항(평가자 평가)
				var resGrdListResult = $i.service("kpiEvalResultGradeDesc",[$("#cboSearchyear").val(),"${param.pEvaGbn}",kpiId]);
				resGrdListResult.done(function(data){
					if(data.returnArray!=null){ 
						for(var i=0;i<data.returnArray.length;i++){   
				        	$("#kpiEvalGradeDesc").val(data.returnArray[i].GRADE_DESC);
					        $("#kpiEvalipvDesc").val(data.returnArray[i].IPV_DESC); 
					        $("#statusResult").val(data.returnArray[i].STATUS);
					        $("#gradeEval").html(data.returnArray[i].EVA_GRDNM);
					        //확정(D)
					        if($("#statusResult").val()=="D"){   
								$("#resultEval").hide(); 
								$("#gradeEval").show(); 
							}else{  
								$("#resultEval").show(); 
								$("#gradeEval").hide(); 
							}
	                     }
						  
					}
				});
        	}   
        	//지표정의서
        	function makeKpiReport(){
        		$i.sqlhouse(272,{S_YYYY:$("#cboSearchyear").val(),S_SC_ID:$("#txtTabscid").val(),S_GUBN:$("#txtGubn").val()}).done(function(res){
	        	   var data = res.returnArray;
	        		// prepare the data
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'MT_CD', type: 'string' },
		                    { name: 'MT_ID', type: 'string' },
		                    { name: 'MT_NM', type: 'string'}
		                ],
		                localdata: data,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		
					var alginLeft = function (row, columnfield, value) {//left정렬
		                       return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
									
		            } ;
					var alginRight = function (row, columnfield, value) {//right정렬
		                       return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
									
		             }	  ;
		             var detailRow = function (row, columnfield, value, defaultHtml,attribute, row) {//그리드 선택시 하단 상세
							var link = "<a href='javascript:getDetailKpiReport("+JSON.stringify(row)+")' style='color:black; text-decoration:underline;' >" + value + "</a>";
							return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
							
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		
		                downloadComplete: function (data, status, xhr) { },
		
		                loadComplete: function (data) { },
		
		                loadError: function (xhr, status, error) { }
		
		            });
		
		            var dataAdapter = new $.jqx.dataAdapter(source);
		
		            $("#gridKpiReportList").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: true,
						pagesizeoptions:['100','200','300'],
		                source: dataAdapter,
						theme:'blueish',
						sortable: true,						
		                columnsresize: true,
		                pagesize: 100,
		                columns: [
		                   { text: '지표코드', datafield: 'MT_CD', width: '30%', align:'center', cellsalign: 'center'},
		                   { text: '지표명', datafield: 'MT_NM', width: '70%', align:'center', cellsalign: 'center',  cellsrenderer: detailRow  }
		                ]
		            });
					$("#gridKpiReportList").jqxGrid('selectrow', 0);
					if(reportIndex != null){
						$('#gridKpiReportList').jqxGrid('selectrow', reportIndex);
        				getDetailKpiReport($("#gridKpiReportList").jqxGrid('getrowdata', reportIndex));
        				reportIndex = null;
					}else{
						if($("#gridKpiReportList").jqxGrid('getrowdata', 0) != null){
							getDetailKpiReport($("#gridKpiReportList").jqxGrid('getrowdata', 0));
						}else{
							getDetailKpiReport("");
						}
					}
					setPagerLayout('gridKpiReportList'); //페이저 레이아웃
	           });
        	}
        	function getDetailKpiReport(row){
        		if(row == null){
       				$("#txtPersName").html("");
        			$("#txtOwnerUserName").html("");
        			$("#txtKpiChargeName").html("");    
        			$("#txtEvalTypeName").html("");
        			$("#txtCalName").html("");
        			$("#txtAreaMtDef").val("");
        			$("#txtTargetDesc").html("");
        			$("#txtMtDesc").html("");
        			$("#txtKpiEvaTypName").html("");
        			$("#txtMeasCycleName").html("");
        			$("#txtMmName").html("");
        			$("#txtKpiDept0").html("");
       				$("#txtKpiDept1").html("");
       				$("#txtKpiDept2").html("");
       				$("#txtKpiDept3").html("");
       				$("#txtKpiDept4").html("");
       				$("#txtKpiCurramt0").html("");
       				$("#txtKpiCurramt1").html("");
       				$("#txtKpiCurramt2").html("");
       				$("#txtKpiCurramt3").html("");
       				$("#txtKpiCurramt4").html("");
       				$("#txtKpiID0").html("");
       				$("#txtKpiID1").html("");
       				$("#txtKpiID2").html("");
       				$("#txtKpiID3").html("");
       				$("#txtKpiID4").html("");
       				$("#targetYyyy0").html("");
       				$("#targetYyyy1").html("");
       				$("#targetYyyy2").html("");
       				$("#targetYyyy3").html("");
       				$("#targetYyyy4").html("");
       				$("#targetValue0").html("");
       				$("#targetValue1").html("");
       				$("#targetValue2").html("");
       				$("#targetValue3").html("");
       				$("#targetValue4").html("");
       				$("#txtKpiWeight").html("");
       				$("#kpiUpmpDetail tr").remove();
       				var relatedWorkHtml = "";
       				relatedWorkHtml += "<tr>";
					relatedWorkHtml += "<th class='w15p'>유관업무</th>";
					relatedWorkHtml += "<td colspan='2'><div class='cell'></div></td>";
					relatedWorkHtml += "<td colspan='7'><div class='cell'></div></td>";
					relatedWorkHtml += "</tr>";
					$("#kpiUpmpDetail").append(relatedWorkHtml);
					$("#kpiReportKpiGubn").html("");
        			$("#kpiCode").html("");
        		}else{
	        		var mtid = row.MT_ID;
	        		var scid = row.SC_ID;
	        		var kpiId = row.MT_CD;
	        		var kpiNm = row.MT_NM;
	        		var kpiData = $i.sqlhouse(273,{S_MT_ID:mtid, S_YYYY:$("#cboSearchyear").val()});
	        		kpiData.done(function(kpi){
	        			if(kpi.returnArray!=null&&kpi.returnArray.length > 0){
	        				$("#txtPersName").html(kpi.returnArray[0].PERS_NM);
		        			$("#txtOwnerUserName").html(kpi.returnArray[0].OWNER_USER_NM);
		        			$("#txtKpiChargeName").html(kpi.returnArray[0].KPI_CHARGE_NM);    
		        			$("#txtEvalTypeName").html(kpi.returnArray[0].EVAL_TYPE_NM);
		        			$("#txtCalName").html(kpi.returnArray[0].CAL_NM);
		        			$("#txtAreaMtDef").val(kpi.returnArray[0].MT_DEF);
		        			$("#txtTargetDesc").html(kpi.returnArray[0].TARGET_DESC);
		        			$("#txtMtDesc").html(kpi.returnArray[0].MT_DESC);
		        			$("#txtKpiEvaTypName").html(kpi.returnArray[0].EVA_TYP_NM);
		        			$("#txtMeasCycleName").html(kpi.returnArray[0].MEAS_CYCLE_NM);
		        			$("#txtMmName").html(kpi.returnArray[0].MM_NM);
		        			$("#txtKpiWeight").html(kpi.returnArray[0].KPI_WEIGHT);
		        			if(kpi.returnArray[0].SC_ID == "100"){ 
		        				$("#kpiReportKpiGubn").html("대학 KPI");
		        				$("#kpiWeightTable").show();
			        		}else{
			        			$("#kpiReportKpiGubn").html("부서 KPI");
			        			$("#kpiWeightTable").hide();
			        		}
		        			$("#kpiCode").html(kpiId+"<span class='label sublabel'>"+kpiNm+"</span>");
	        			}else{
	        				$("#txtPersName").html("");
		        			$("#txtOwnerUserName").html("");
		        			$("#txtKpiChargeName").html("");    
		        			$("#txtEvalTypeName").html("");
		        			$("#txtCalName").html("");
		        			$("#txtAreaMtDef").val("");
		        			$("#txtTargetDesc").html("");
		        			$("#txtMtDesc").html("");
		        			$("#txtKpiEvaTypName").html("");
		        			$("#txtMeasCycleName").html("");
		        			$("#txtMmName").html("");
	        			}
	        		});
	        		var kpiCurramtDate = $i.sqlhouse(274, {S_MT_ID:mtid, S_YYYY:$("#cboSearchyear").val()});
	        		kpiCurramtDate.done(function(curramt){
	        			$("#txtKpiDept0").html("");
        				$("#txtKpiDept1").html("");
        				$("#txtKpiDept2").html("");
        				$("#txtKpiDept3").html("");
        				$("#txtKpiDept4").html("");
        				$("#txtKpiCurramt0").html("");
        				$("#txtKpiCurramt1").html("");
        				$("#txtKpiCurramt2").html("");
        				$("#txtKpiCurramt3").html("");
        				$("#txtKpiCurramt4").html("");
        				$("#txtKpiID0").html("");
        				$("#txtKpiID1").html("");
        				$("#txtKpiID2").html("");
        				$("#txtKpiID3").html("");
        				$("#txtKpiID4").html("");
        				$("#txtKprCurramtSum").html("");
        				
	        			if(curramt.returnArray!=null&&curramt.returnArray.length > 0){
	        				var sum = "0";
	        				for(var i=0;i<curramt.returnArray.length;i++){
	        					if(curramt.returnArray[i].KPI_CURRAMT != ""){
	        						$("#txtKpiDept"+i).html(curramt.returnArray[i].SC_NM);
		        					$("#txtKpiCurramt"+i).html(parseInt(curramt.returnArray[i].KPI_CURRAMT)+"%");
		        					$("#txtKpiID"+i).html(curramt.returnArray[i].MT_CD);
		        					sum = parseInt(parseInt(sum)+parseInt(curramt.returnArray[i].KPI_CURRAMT));
	        					}
	        				}
	        				$("#txtKprCurramtSum").html(sum+"%");
	        			}
	        		});
	        		var kpiTargetDate = $i.sqlhouse(275, {S_MT_ID:mtid});
	        		kpiTargetDate.done(function(target){
	        			$("#targetYyyy0").html("");
        				$("#targetYyyy1").html("");
        				$("#targetYyyy2").html("");
        				$("#targetYyyy3").html("");
        				$("#targetYyyy4").html("");
        				$("#targetValue0").html("");
        				$("#targetValue1").html("");
        				$("#targetValue2").html("");
        				$("#targetValue3").html("");
        				$("#targetValue4").html("");
	        			if(target.returnArray!=null&&target.returnArray.length > 0){
	        				for(var k=0;k < 5;k++){
	        					if(target.returnArray[k] == null){
	        						if(i!=0){
	        							$("#targetYyyy"+k).html(parseInt(target.returnArray[0].YYYY)+k);
		        						$("#targetValue"+k).html("");
	        						}
	        					}else{
		        					$("#targetYyyy"+k).html(parseInt(target.returnArray[0].YYYY)+k);
		        					$("#targetValue"+k).html(target.returnArray[k].TARGET_AMT);	
	        					}
	        				}
	        			}
	        		});
	        		var upmuData = $i.sqlhouse(276,{S_MT_ID:mtid});
	       			upmuData.done(function(umpu){
	       				var relatedWorkHtml = "";
	       				$("#kpiUpmpDetail tr").remove();
	       				if(umpu.returnArray!=null&&umpu.returnArray.length>0){
	       					for(var j=0;j<umpu.returnArray.length;j++){
	       						if(j == 0){
	       							relatedWorkHtml += "<tr>";
	       							relatedWorkHtml += "<th class='w15p' rowspan='"+umpu.returnArray.length+1+"'>유관업무</th>";
	       							relatedWorkHtml += "<td colspan='2'><div class='cell'>"+umpu.returnArray[j].RELATED_WORK1+"</div></td>";
	       							relatedWorkHtml += "<td colspan='7'><div class='cell'>"+umpu.returnArray[j].RELATED_WORK2+"</div></td>";
	       							relatedWorkHtml += "</tr>";
	       						}else{
	       							relatedWorkHtml += "<tr>";
	       							relatedWorkHtml += "<td colspan='2'><div class='cell'>"+umpu.returnArray[j].RELATED_WORK1+"</div></td>";
	       							relatedWorkHtml += "<td colspan='7'><div class='cell'>"+umpu.returnArray[j].RELATED_WORK2+"</div></td>";
	       							relatedWorkHtml += "</tr>";
	       						}
	       					}
	       				}else{
	       					relatedWorkHtml += "<tr>";
							relatedWorkHtml += "<th class='w15p'>유관업무</th>";
							relatedWorkHtml += "<td colspan='2'><div class='cell'></div></td>";
							relatedWorkHtml += "<td colspan='7'><div class='cell'></div></td>";
							relatedWorkHtml += "</tr>";
	       				}
	       				$("#kpiUpmpDetail").append(relatedWorkHtml);
	       			});
        		}
        	} 	
        	function getReportDetail(){
        		reportIndex = $('#gridKpiReportList').jqxGrid('getselectedrowindex');
        		var data = $('#gridKpiReportList').jqxGrid('getrowdata', $('#gridKpiReportList').jqxGrid('getselectedrowindex'));
        		var scid = data.SC_ID;
        		var mtid = data.MT_ID;
        		var programid = "";
        		if(scid=="100") programid = "detailOfUnivByUser";
        		else programid = "detailOfDeptByUser";   
        		
        		var url = "./"+programid+"?acctid=${paramAcctid}&scid="+scid+"&mtid="+mtid+"&gubn="+$("#txtGubn").val()+"&searchyear="+$("#cboSearchyear").val()+"&grdList="+$('#gridKpiReportList').jqxGrid('getselectedrowindex');
        		var kpiForm = $("<iframe data-role='kpiform' src='"+url+"'style='width:100%;height:100%;position:absolute;left:0px;border-style:none;z-index:99999;'></iframe>").appendTo("body");
        	}	

	    </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			} 
			iframe {
				background:transparent;
			}     
		</style>
		      
    </head>
    <body class='blueish' style="overflow-y: hidden; overflow-x: hidden;">    
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 1%;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<form id="searchForm" name="searchForm" action="./crudByUser">
					<input type="hidden" id="txtTabscid" name="tabscid" value="100" />     
					<input type="hidden" id="txtTabList" name="tablist" />
					<input type="hidden" id="txtTabSubstr" name="tabSubstr"/>    
					<input type="hidden" id="txtGubn" name="gubn" value="0" /> 
					<input type="hidden" id="txtEvagbn" name="evagbn" value="${param.pEvaGbn}" />
					<div class="label type1 f_left">평가년도:</div>
					<div class="combobox f_left"  id='cboSearchyear' name="searchyear"></div>
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="searchStart();"/>
						</div>
					</div>
				</form> 
			</div>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:24%; margin-right:1%;">
					<div class="tabs f_left" style=" width:100%; height:580px; margin-top:0px;">
						<div id='jqxTabs01'>
							<ul>
							    <li onclick="makeStraListTree();">전략기준</li> 
								<li onclick="makeKpiList();" style="margin-left: 0px;">지표기준</li>
								  
							</ul>
							<div class="tabs_content" style="height:100%;">
								<div class="tree" style="border:none;" id="treeStraList"></div>
							</div>
							<div  class="tabs_content" style=" height:100%; ">
								<div class="group  w90p" style="margin:10px auto;">
									<div class="label type2 f_left">검색</div>
									<input type="text" id="txtSearchkpinm" name="searchkpinm" class="input type1  f_left  w50p"  style="width:100%; margin:3px 5px; "/>
    								<div class="button type2 f_left">
  										<input type="button" value="검색" id='btnSearchkpi' width="100%" height="100%" />
									</div>
  								</div>
								<div class="listbox w90p" style="margin:0px auto;">
									<div id='listboxKpi' class="f_left" style=" width:100%; margin:10px 0;"> </div>
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="content f_right" style=" width:75%;">
					<div class="group f_left  w100p m_b5">
      					<div class="label type2 f_left" id="labelShow">&nbsp;</div>
					</div>
					<div class="tabs f_left" style=" width:100%; height:550px; margin-top:0px;">
						<div id='jqxTabs02'>
   							<ul>
    							<li style="margin-left: 0px;" >지표조회</li>
    							<li >목표관리</li>
    							<li >실적관리</li>
  								<li >증빙자료</li>       
  								<li >지표자체평가</li>
  								<li >평가결과</li>
  								<li >지표정의서</li>   
							</ul>
    						<div class="tabs_content" style="height:100%; ">
								<div class="grid f_left" style="width:98%; margin:10px 1%; height:495px;">
 									<div id="gridKpilist"> </div>
								</div>
							</div>
							<!-- 목표관리 -->
							<div  class="tabs_content" style=" height:100%; "> 
								<div class="content f_left" style="width:56%; margin:0 1%;"> 
									<div class="group f_left m_t5 m_l10">     
										<div class="label type2 f_left">담당자 : </div>          
										<input type="text" id="txtSearchTgtnm" name="searchTgtnm" class="input type1" style="width:100px; margin:3px 5px;"/> 
									</div>   
									<div class="group f_left m_t5 m_l10">   
										<div class="label type2 f_left">지표명 : </div>          
										<input type="text" id="txtTargetSearchKpinm" name="searchTargetKpinm" class="input type1" style="width:100px; margin:3px 5px;"/>                 
									</div>  
									<div class="grid f_left" style="width:100%; margin:5px 0%; height:455px;">
										<div id="gridKpitarget" style="height:455px;"> </div>
									</div>
								</div>
								<div class="content f_right" style="width:40%; margin:0 1%;"> 
									<div class="group f_left  w100p m_t5">   
										<div class="label type2 f_left">조직명:<span class="label sublabel type1" id="targetScid">101 &frasl; 기획처</span></div>
										<div class="label type2 f_left">지표명:<span class="label sublabel" id="targetMtcd">101 &frasl; 취업률</span></div>
									</div>
									<div class="grid f_left" style="width:100%; margin:5px 0%; height:300px;">
										<div id="gridTargetamt"> </div>
									</div>
									<div class="datatable f_left auto" style="width:100%; margin:10px 0%; border-bottom:1px solid #b0c5df;">
										<input type="hidden" id="txtTargetacctid" />
										<input type="hidden" id="txtTargetscnm" />

										<input type="hidden" id="txtTargetmtcd" name="targetmtcd" />    
										<input type="hidden" id="txtTargetkpinm" />
										<form id="targetSave" name="targetSave">
											<input type="hidden" id="txtTargetevagbn" name="targetevagbn" />
											<input type="hidden" id="txtTargetscid" name="targetscid" />
											<input type="hidden" id="txtTargetkpiid" name="targetkpiid" />

											<table width="99.8%" cellspacing="0" cellpadding="0" border="0" >
												<thead>
		        									<tr>
		          										<th style="width:20%;"><div>년도</div></th>
		          										<th style="width:40%;"><div>목표값</div></th>
		          										<th style="width:40%;"><div>기준값</div></th>
		          									</tr> 
		          								</thead>
		          								<tbody>
		          									<tr>   
			          									<td>
			          										<input type="hidden" id="btnhiddenchtargeid" name="btnhiddenchtargeid"/>
															<input type="hidden" id="btnhiddenunid" name="btnhiddenunid"/>      
			          										<input type="hidden" id="txtTargetorgyyyy" name="targetorgyyyy" />
			          										<input type="text" class="input type2 t_center num_only" id="txtTargetyyyy" name="targetyyyy" style="width:86%;" />
			          									</td>
			          									<td>
			          										<input type="text" class="input type2 t_right num_only" id="txtTargetamt" name="targetamt" style="width:93%;" />
			          									</td>
			          									<td>
			          										<input type="text" class="input type2 t_right num_only" id="txtTargetcurramt" name="targetcurramt" style="width:93%;" />
			          									</td>
		          									</tr>  
												</tbody>
											</table>
										</form>
									</div>
									<div class="group_button f_right m_t5">
										<div class="button type2 f_left">
											<input type="button" value="초기화" id='btnTaregetreset' class="confirmButton" width="100%" height="100%" onclick="resetTarget();" />
										</div>
										<div class="button type2 f_left">
											<input type="button" value="저장" id='btnTargetinsert' class="confirmButton" width="100%" height="100%" onclick="insertTarget();" />
										</div>
										<div class="button type3 f_left">
											<input type="button" value="삭제" id='btnTargetremove' class="confirmButton" width="100%" height="100%" onclick="removeTarget();"/>
										</div>
									</div>
								</div>
							</div>		
							<!-- 실적관리 -->
							<div  class="tabs_content" style=" height:100%; ">
								<div class="group f_left m_t5 m_l10">
									<div class="label type2 f_left">월 : </div> 
									<div class="combobox f_left m_l5" id="cboSearchResultMm" name="searchResultMm" onchange="makeResultList();"></div>
								</div>   
								<div class="group f_left m_t5 m_l10">   
									<div class="label type2 f_left">담당자 : </div>          
									<input type="text" id="txtSearchChargenm" name="searchChargenm" class="input type1" style="width:100px; margin:3px 5px;"/>         
								</div>      
								<div class="group f_left m_t5 m_l10">   
									<div class="label type2 f_left">지표명 : </div>          
									<input type="text" id="txtInputSearchKpinm" name="searchInputKpinm" class="input type1" style="width:100px; margin:3px 5px;"/>         
								</div>  
								<div class="group f_right m_t10 m_r10">
									<div class="label type3 f_left" id="messageLayer"></div>
										<div id="allButton" class="button type2 f_left">
											<input type="button" value="담당자 일괄확인" class="confirmButton" id="btnAllChargeY" width="100%" height="100%" onclick="confirmAll('Y', 'charge');"  />
										</div>
										<div class="button type3 f_left">
											<input type="button" value="담당자 일괄취소" class="confirmButton" id="btnAllChargeN" width="100%" height="100%" onclick="confirmAll('N', 'charge');"  />
										</div>
										<div class="button type2 f_left">
											<input type="button" value="승인자 일괄확인" class="confirmButton" id="btnAllUserY" width="100%" height="100%" onclick="confirmAll('Y', 'user');"  />
										</div>
										<div class="button type3 f_left">
											<input type="button" value="승인자 일괄취소"class="confirmButton"  id="btnAllUserN" width="100%" height="100%" onclick="confirmAll('N', 'user');"  />
										</div>
									<div class="button type2 f_left">
										<input type="button" value="저장" id='btnInsertResult' class="confirmButton" width="100%" height="100%" onclick="saveKpiResult();"/>
									</div>
								</div>
								<div class="blueish datatable f_left" style="width:98%; height:470px; margin:0px 0; margin-left:15px; overflow:hidden;">
									<div class="datatable_fixed" style="width:100%;height:470px; float:left;">
										<div style=" height:444px !important; overflow-x:hidden;"><!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 600px - 50px= 550px)-->
											<form id="saveResult" name="saveResult" action="./insertResultColValueAdmin">       
												<input type="hidden" id="resultEvaGbn" name="pEvaGbn" value="${param.pEvaGbn}" />
												<input type="hidden" id="hiddenAllSaveGubn" name="saveGubn" />
												<input type="hidden" id="hiddenAllConfirm" name="confirm" />
												<input type="hidden" id="hiddenAllUserGubn" name="userGubn" /> 
												<table summary="지표실적입력" style="width:100%;" calss="none_hover none_alt">
													<thead style="width:100%;">
														<tr>
															<th style="width:5%;">조직ID</th>
															<th style="width:9%;">조직명</th>
															<th style="width:24%;">지표명</th>
															<th style="width:6%;">입력담당자</th>
															<th style="width:8%;">확인여부</th>
															<th style="width:6%;">승인담당자</th>
															<th style="width:8%;">확인여부</th>
															<th style="width:10%;">항목명</th>
															<th style="width:6%;">항목단위</th>  
															<th style="width:13%;">항목값</th>   
															<th style="width:1%; min-width:17px;"></th>
														</tr>
													</thead>
													<tbody id="resultInputList">
													</tbody>
												</table>
											</form>
											<form id="oneSaveForm" name="oneSaveForm" action="./insertResultColValueAdmin">
												<input type="hidden" id="hiddenSaveGubn" name="saveGubn" value="One" />
												<input type="hidden" id="oneEvaGbn" name="pEvaGbn" value="${param.pEvaGbn}" />
												<input type="hidden" id="hiddenUserGubn" name="userGubn" />
												<input type="hidden" id="hiddenChargeOneYyyymm" name="chargeOneYyyymm" />
												<input type="hidden" id="hiddenChargeOneKpiID" name="chargeOneKpiID" />
												<input type="hidden" id="hiddenChargeOneID" name="chargeOneID" />   
												<input type="hidden" id="hiddenChargeOneName" name="chargeOneName" />
												<input type="hidden" id="hiddenChargeOneConfirm" name="chargeOneConfirm" />
												<input type="hidden" id="hiddenChargeOneDate" name="chargeOneDate" />
												<input type="hidden" id="hiddenUserOneID" name="userOneID" />
												<input type="hidden" id="hiddenUserOneName" name="userOneName" />
												<input type="hidden" id="hiddenUserOneConfirm" name="userOneConfirm" />
												<input type="hidden" id="hiddenUserOneDate" name="userOneDate" />
											</form>
										</div>
									</div>
								</div>
							</div>												
							<!-- 증빙자료-->   
							<div  class="tabs_content" style=" height:100%; ">
								<div id="kpiAttachFilelist" width="100%" height="500px" >   
								</div>
							</div>
							<!-- 담당자평가 -->
							<div  class="tabs_content" style=" height:100%; ">
							<form id="gradeValueSave" name="gradeValueSave">           
								<div class="content f_left" style="width:50%; margin:0 1%;"> 
					             	<div class="grid f_left" style="width:100%; margin:10px 0%; height:495px;">
					               		<div id="kpiUserlist"> </div>
					                </div> 
								</div>       
								<div class="content f_right" style="width:46%; margin:0 1%;"> 
                 					<div class="group f_left  w100p m_t5">      
                        				<div class="label type2 f_left">조직명:<span class="label sublabel type1" id="scIdlabel"> &frasl; </span></div>
										<div class="label type2 f_left">지표명:<span class="label sublabel" id="kpiIdlabel"> &frasl; </span></div>
                   	 				</div>          
				   				<div class="table f_left" style="width:100%; margin-bottom:10px;">
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<thead>
							  			<tr>
											<th style="width:35%;"><div>지표실적</div></th>
											<th style="width:35%;"><div>목표실적</div></th>
											<th style="width:30%;"><div>달성률</div></th>
							  			</tr> 
									</thead>  
										<tbody id="kpiList">
										</tbody>
									</table>
								</div>
								<div class="table f_left" style="width:100%; margin-bottom:10px;">
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<thead>
										  <tr>
											<th style="width:50%;"><div>항목</div></th>
											<th style="width:20%;"><div>단위</div></th>
											<th style="width:30%;"><div>항목실적</div></th>
										  </tr> 
										</thead>  
										<tbody id="colList">   
										</tbody>
									</table>
								</div>
								<div class="table f_left" style="width:100%; margin-bottom:10px;">
								<input type="hidden" id="fileid" name="fileid" />
									<table width="100%" cellspacing="0" cellpadding="0" border="0">
										<thead>
										  <tr>
											<th style="width:100%;"><div>증빙자료</div></th>
										  </tr> 
										</thead>  
										<tbody id="fileList">               
									  </tbody>
									</table>
								</div>   
									<input type="hidden" id="kpiNm" name="kpiNm" />
									<input type="hidden" id="scNm" name="scNm" /> 
									<input type="hidden" id="acctId" name="acctId" />   
									<input type="hidden" id="status" name="status" />
								    <input type="hidden" id="evaGbn" name="evaGbn" value="${param.pEvaGbn}" /> 
								    <input type="hidden" id="mtId" name="mtId" />
								    <input type="hidden" id="mtCd" name="mtCd" />   
									<input type="hidden" id="scId" name="scId"/>   
								    <input type="hidden" id="grade" name="grade" />   
								    <input type="hidden" id="yyyy" name="yyyy"/>   
								    <input type="hidden" id="mm" name="mm" />   
								    <input type="hidden" id="kpiId" name="kpiId" /> 
								    <input type="hidden" id="owneruserId" name="owneruserId" />   
								    <input type="hidden" id="kpichargeId" name="kpichargeId" />
								    <input type="hidden" id="unId" name="unId" /> 
								<div class="group f_left  w100p m_t5">    
									<div class="label type2 f_left">주요성과</div>      
									<textarea id="gradeDesc" name="gradeDesc" class="textarea" value="" style=" width:100%; height:80px; margin:3px 0;" onblur="checkInput('gradeDesc','4000','all');"></textarea>
								</div>
								<div class="group f_left  w100p m_t5">   
									<div class="label type2 f_left">개선 사항</div>
									<textarea id="ipvDesc" name="ipvDesc" class="textarea" value="" style=" width:100%; height:80px; margin:3px 0;" onblur="checkInput('ipvDesc','4000','all');"></textarea>
								</div>	
								<div style=" width:100%;">
										<p class="label type2 f_left" style="padding-bottom:5px !important; margin-left:0px !important; margin-bottom:5px !important;">평가등급</p>
									<div style="float:left;margin-top:5px;margin-left:10px;" id="radioList">  
									</div>
								</div>
								<div class="group_button f_right m_t15">
				                    <div class="button type2 f_left">
				                    	<input type="button" value="초기화" class="confirmButton" id='btnUserEvalinsert' width="100%" height="100%" onclick="resetGradeValue();"/>
				                    </div>   
				                    <div class="button type2 f_left">
				                    	<input type="button" value="저장" class="confirmButton" id='btnUserEvalsave' width="100%" height="100%" onclick="insertGradeValue();"/>
				                    </div>
			                    	<div class="button type2 f_left">  
			                       		<input type="button" value="확정" class="confirmButton" id='btnUserEvalsubmit' width="100%" height="100%" onclick="insertGradeSubmit();"/>   
			                      	</div>  
			                    	<div class="button type3 f_left">
			                       		<input type="button" value="확정취소" id='btnUserEvalsubmitCencel' width="100%" height="100%" onclick="insertGradeCancelSubmit();"/>   
			                      	</div>    
			                      	<div class="button type2 f_left">
			                       		<input type="button" value="최종확정" id='lastEvalsubmitSave' width="100%" height="100%" onclick="lastGradeSaveSubmit();"/>   
			                      	</div>   
			                      	<div class="button type3 f_left">
			                       		<input type="button" value="최종확정취소" id='lastEvalsubmitCencel' width="100%" height="100%" onclick="lastGradeCencelSubmit();"/>   
			                      	</div>   
			                    </div>		    
								</div>
							</form>			
							</div>
							<!-- 평가결과 --> 
							<div  class="container tabs_content" style=" height:100%; ">
								<input type="hidden" id="acctIdResult" name="acctIdResult" />
								<input type="hidden" id="scNmResult" name="scNmResult" />
								<input type="hidden" id="kpiNmResult" name="kpiNmResult" />
								<input type="hidden" id="scIdResult" name="scIdResult"/> 
								<input type="hidden" id="kpiIdResult" name="kpiIdResult" />  
								<input type="hidden" id="mtIdResult" name="mtIdResult" />
								<input type="hidden" id="mtCdResult" name="mtCdResult" />    
								<input type="hidden" id="statusResult" name="statusResult" />   
								<input type="hidden" id="evalStatus" name="evalStatus" />    
							<!-- 평가결과리스트 -->
							<div class="content f_left" style="width:100%; margin:0 0;"> 
						 		<div class="content f_left" style="width:50%; margin:0 1%;"> 
					   				<div class="grid f_left" style="width:100%; margin:10px 0%; height:213px;">
					               		<div id="kpiEvalresultlist"> </div>
					                </div> 
								</div>
							<!-- 지표조회 : 지표리스트(right) -->     
							<div class="content f_right" style="width:46%; margin:0 1%;">
								<div class="group f_left  w100p m_t5">
									<div class="label type2 f_left">조직명:<span class="label sublabel type1" id="scIdlabelResult"> &frasl; </span></div>
									<div class="label type2 f_left">지표명:<span class="label sublabel" id="kpiIdlabelResult"> &frasl; </span></div>
								</div>
								<div class="table f_left" style="width:100%; margin-bottom:10px;">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<thead>
								  		<tr>
											<th style="width:35%;"><div>지표실적</div></th>
											<th style="width:35%;"><div>목표실적</div></th>
											<th style="width:30%;"><div>달성률</div></th>
								  		</tr> 
									</thead>  
									<tbody id="kpiListResult">
										<tr>
											<td class="cell t_center" colspan="3">No data to display</td>
										</tr>   
									</tbody>
							  </table>
							  </div>
							  <div class="table f_left" style="width:100%; margin-bottom:10px;">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<thead>
									  <tr>
										<th style="width:50%;"><div>항목</div></th>
										<th style="width:20%;"><div>단위</div></th>
										<th style="width:30%;"><div>항목실적</div></th>
									  </tr> 
									</thead>  
									<tbody id="colListResult">  
										<tr>
											<td class="cell t_center" colspan="2">No data to display</td>
										</tr>
									</tbody>
								</table>
							</div>  
							<!--table --> 
							<div class="table f_left" style="width:100%; margin-bottom:10px;">
								<table width="100%" cellspacing="0" cellpadding="0" border="0">
									<thead>
									  <tr>
										<th style="width:100%;"><div>증빙자료</div></th>
									  </tr> 
									</thead>  
									<tbody id="fileListResult">      
										<tr>
											<td><div class="cell t_center">No data to display</div></td>
										</tr>
								    </tbody>
								</table>
							</div>
							</div> 
							</div>
							<!-- 평가결과하단 -->
								<div class="content f_left" style="width:100%; margin:5px 0;"> 
									<div class="content f_left" style="width:50%; margin:0 1%; background:#f2f2f2;"> 
										<div class="group f_left  w100p m_t5">
											<div class="label type1 edit f_left m_l10">담당자 평가</div>  
											<div class="f_left m_l10" style="font-weight:bold; color:red;margin-top:4px;" id='resultCharge'> [ 평가진행중입니다. ] </div>   
											<div  class="f_left m_l10" style="font-weight:bold; margin-top:4px;"  id='LabelChargehidden'>[ 등급 : <span id='gradeCharge'></span> ]</div>
										</div>
										<div class="group f_left  w100p m_t5">
											<div class="label type2 f_left m_l10">주요성과</div>
											<textarea id="gradeDescResult" name="gradeDescResult" style=" width:96%; height:80px; margin:3px 2%;"></textarea>
										</div>   
										<div class="group f_left  w100p m_t5 m_b5">
											<div class="label type2 f_left m_l10">개선사항</div>
											<textarea id="ipvDescResult" name="ipvDescResult" style=" width:96%; height:80px; margin:3px 2%;"></textarea>
										</div>     
									</div>
									<div class="content f_left" style="width:46%; margin:0 1%; background:#f2f2f2;"> 
										<div class="group f_left  w100p m_t5">
											<div class="label type1 edit f_left m_l10">평가자 평가</div>
											<div class="f_left m_l10" style="font-weight:bold; color:red;margin-top:4px;" id='resultEval'> [ 평가진행중입니다. ] </div>  
											<div  class="f_left m_l10" style="font-weight:bold; margin-top:4px;" id='LabelEvalhidden'>[ 등급 : <span id='gradeEval'></span> ]</div>
										</div>
										<div class="group f_left  w100p m_t5">
											<div class="label type2 f_left m_l10">평가근거</div>
											<textarea   id="kpiEvalGradeDesc" name="kpiEvalGradeDesc" value="" style=" width:96%; height:80px; margin:3px 2%;"></textarea>
										</div>
										<div class="group f_left  w100p m_t5 m_b5"> 
											<div class="label type2 f_left m_l10">권고사항</div>
											<textarea   id="kpiEvalipvDesc" name="kpiEvalipvDesc" value="" style=" width:96%; height:80px; margin:3px 2%;"></textarea>
										</div>
									</div>
				          		</div>
							</div>
						<!-- 지표정의서 -->
						<div  class="tabs_content" style=" height:100%; ">      
								<div class="content f_left" style="width:35%; margin:0 1%;"> 
					             	<div class="grid f_left" style="width:100%; margin:10px 0%; height:495px;">
					               		<div id="gridKpiReportList"> </div>
					                </div> 
								</div>       
								<div class="content f_right" style="width:61%; margin:0 1%;"> 
									<div class="group f_left  w100p m_t5">
										<div class="label type2 f_left" style="margin-right: 15px;" id="kpiReportKpiGubn"></div>
										<div class="label type2 f_left" id="kpiCode"><span class="label sublabel"></span></div>
										<div class="button type2 f_right">
											<input type="button" value="지표상세" id='btnKpiDetail' width="100%" height="100%" onclick="getReportDetail();" />
										</div>
									</div>        
									<div class="table  f_left m_t5" style="width:100%; margin:0;">
											<table width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody>
													<tr>
														<th class="w15p"><div>관점</div></th>
														<td class="w10p" ><div class="cell t_center" id="txtPersName"></div></td>
														<th><div>지표입력자</div></th>
														<td class="w10p" style="background:#ffe9b5;"><div class="cell t_center w100p" id="txtKpiChargeName"></div></td>
														<th><div>책임자</div></th>
														<td class="w10p" colspan="2"><div class="cell t_center" id="txtOwnerUserName"></div></td>
														<th><div>측정단위</div></th>
														<td colspan="2" class="w10p" ><div class="cell t_center" id="txtEvalTypeName"></div></td>
													</tr>
													<tr>
														<th><div>산출공식</div></th>
														<td colspan="9"><div class="cell" id="txtCalName"></div></td>
													</tr>
													<tr>
														<th style="height:100px;"><div>세부내용</div></th>
														<td colspan="9" class="v_top p_t5"><div class="cell"><textarea id="txtAreaMtDef" class="textarea type1" style="width:100%; height:98px; margin:3px 0; border:0;" ></textarea></div>
													</td>
													</tr>
													<tr>
														<th><div>고려사항</div></th>
														<td colspan="9"><div class="cell" id="txtTargetDesc"></div></td>
													</tr>
												</tbody>
											</table>
											<table width="100%" cellspacing="0" cellpadding="0" border="0" id="kpiWeightTable">
													<tbody>
														<tr>
															<th class="w15p" rowspan="3"><div>가중치(W1)</div></th>
															<td class="w10p" rowspan="3"><div class="cell t_center" id="txtKpiWeight"></div></td>
															<th rowspan="3"><div>가치형성</div></th>
															<th><div>부서</div></th>
															<td class="w10p"><div class="cell t_center" id="txtKpiDept0"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiDept1"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiDept2"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiDept3"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiDept4"></div></td>
															<td class="w10p"><div class="cell t_center bold">소계</div></td>
														</tr>
														<tr>
															<th><div>비율(C1)</div></th>
															<td class="w10p"><div class="cell t_center" id="txtKpiCurramt0"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiCurramt1"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiCurramt2"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiCurramt3"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiCurramt4"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKprCurramtSum"></div></td>
														</tr>
														<tr>
															<th><div>지표</div></th>
															<td><div class="cell t_center" id="txtKpiID0"></div></td>
															<td><div class="cell t_center" id="txtKpiID1"></div></td>
															<td><div class="cell t_center" id="txtKpiID2"></div></td>
															<td><div class="cell t_center" id="txtKpiID3"></div></td>
															<td><div class="cell t_center" id="txtKpiID4"></div></td>
															<td><div class="cell t_center" ></div></td>
														</tr>
													</tbody>
											</table>						
											<table width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody>
													<tr>
														<th class="w15p"><div>증빙자료</div></th>
														<td colspan="9"><div class="cell" id="txtMtDesc"></div></td>
														</tr>
												</tbody>    
											</table>
												<table width="100%" cellspacing="0" cellpadding="0" border="0">
													<tbody>
														<tr>
															<th class="w15p" rowspan="2"><div>측정주기</div></th>
															<td class="w10p" rowspan="2"><div class="cell t_center" id="txtMeasCycleName"></div></td>
															<th rowspan="2"><div>보고시기</div></th>
															<td class="w10p" rowspan="2"><div class="cell t_center" id="txtMmName"></div></td>
															<th rowspan="2"><div>목표</div></th>
															<td class="w10p"><div class="cell t_center bold" id="targetYyyy0"></div></td>
															<td class="w10p"><div class="cell t_center bold" id="targetYyyy1"></div></td>
															<td class="w10p"><div class="cell t_center bold" id="targetYyyy2"></div></td>
															<td class="w10p"><div class="cell t_center bold" id="targetYyyy3"></div></td>
															<td class="w10p"><div class="cell t_center bold" id="targetYyyy4"></div></td>
														</tr>
														<tr>
															<td style="height:40px;"><div class="cell t_center" id="targetValue0"></div></td>
															<td><div class="cell t_center" id="targetValue1"></div></td>
															<td><div class="cell t_center" id="targetValue2"></div></td>
															<td><div class="cell t_center" id="targetValue3"></div></td>
															<td><div class="cell t_center" id="targetValue4"></div></td>
														</tr>
													</tbody>
												</table>
												<table width="100%" cellspacing="0" cellpadding="0" border="0">
													<tbody id="kpiUpmpDetail">
														<tr>
															<th class="w15p" rowspan="3"><div>유관업무</div></th>
															<td colspan="2"><div class="cell"></div></td>
															<td colspan="7"><div class="cell"></div></td>
														</tr>
														<tr>
															<td colspan="2"><div class="cell"></div></td>
															<td colspan="7"><div class="cell"></div></td>
														</tr>
														<tr>
															<td colspan="2"><div class="cell"></div></td>
															<td colspan="7"><div class="cell"></div></td>
														</tr>
													</tbdoy>
												</tbody>
											<table width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody>
													<tr>
														<th class="w15p"><div class="cell">평가방법</div></th>
														<td colspan="9"><div class="cell" id="txtKpiEvaTypName"></div></td>
													</tr>
												</tbody>
											</table>
									</div>
								</div>
							</div>				
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</body>
</html>