<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>부서별 지표평가</title>
   	 	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script> 
    	//문서 로딩이 완료되면 처음 수행되는 로직
    	$(document).ready(function(){
    		$("#btnSearch").jqxButton({width:'', theme:'blueish'});  //조회 버튼 
    		
    		$("#btnKpiDocument").jqxButton({ width: '',  theme:'blueish'});
			$("#btnUserEvalsave").jqxButton({ width: '',  theme:'blueish'}); 
			$("#btnUserEvalsubmit").jqxButton({ width: '',  theme:'blueish'}); 
			$("#btnUserEvalsubmitCencel").jqxButton({ width: '',  theme:'blueish'}); 
    		$('#jqxTabs').jqxTabs({ width: '100%', height: '100%', position: 'top', theme:'blueish', selectedItem: 0});  
    		init();           
    		
			$("#kpiEvalresultlist").on("rowClick", function(event) {
				$("#ipvDesc").prop("readonly","");
				$("#gradeDesc").prop("readonly","");
				$("[name='gradeValue']").prop("disabled",false);
				var args = event.args; 
				var row = args.row;
				var key = args.key; 

				if($("#kpiEvalresultlist").jqxTreeGrid("getRow",event.args.key).expanded == true){
					$("#kpiEvalresultlist").jqxTreeGrid('collapseRow', event.args.key);
				}else{
					$("#kpiEvalresultlist").jqxTreeGrid('expandRow', event.args.key);
				}
				
				if(row.NO1 == "3"){    
					getDataList(row);   
				}
			});
    	});
    	//조회 
    	function searchStart(){ 
    		kpiResultSearch();   
    		buttonChange();  
    	}
    	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
    	function init(){
    		var resultYearDat = $i.sqlhouse(13,{COML_COD:"YEARS"});
    		resultYearDat.done(function(res){
    			var source = 
    			{
    				datatype:"json",
    				datafields:[
    					{ name : "COM_COD"},
    					{ name : "COM_NM"}
    				],
    				id:"id",
    				localdata:res,
    				async : false
    			};
    			var dataAdapter = new $.jqx.dataAdapter(source);
    			$("#cboSearchResultYear").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,  theme:'blueish'});
    			monthChange();    			  
    			$("#resultEval").hide();    
    		});
    	}
		function monthChange(){
			var pEvaGbn = "${param.pEvaGbn}";
			var empData = $i.sqlhouse(278, {S_YYYY:$("#cboSearchResultYear").val(),S_EVA_GBN:"${param.pEvaGbn}"});
			empData.done(function(res){
				var source = 
    			{	
					datatype:"json",
					datafields:[
							{ name: 'EMP_ID'},
							{ name: 'EMP_NM'}
						],
	    				id:"id",
	    				localdata:res,
	    				async : false
				};
			var dataAdapter = new $.jqx.dataAdapter(source);
			$('#empCombo').jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'EMP_NM', valueMember: 'EMP_ID', dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22, theme:'blueish'});     
			$("#empCombo").jqxComboBox('selectIndex', 0 ); 
			searchStart();       
			makeRadio();   
			});
    	}
  		function kpiResultSearch(){          
  			$("#kpiEvalresultlist").jqxTreeGrid('clear');
			$("#kpiEvalresultlist").jqxTreeGrid('clearSelection');       
			var empData = $i.sqlhouse(254, {S_USER_ID:$("#empCombo").val(),S_YYYY:$("#cboSearchResultYear").val(),S_EVA_GBN:"${param.pEvaGbn}",S_EVAL_G:"${param.pEvalG}"});
			empData.done(function(res){
    			var source =   
				{       
	                datatype: "json",   
	                datafields: [
	     				{ name: 'ACCT_ID'},
						{ name: 'C_ID'},
						{ name: 'P_ID'},
						{ name: 'C_NM'},
						{ name: 'SC_ID'},
						{ name: 'SC_NM'},
						{ name: 'MT_ID'},
	                    { name: 'OWNER_USER_ID', type: 'string'},
	                    { name: 'KPI_CHARGE_ID', type: 'string'},
	                    { name: 'UNID', type: 'string'},
						{ name: 'NO1'},      
 						{ name :'STATUS'}
	                ],
	                icons:true,
					altrows:false,
					pageable:false,  
					localdata: res,
					hierarchy:{
						keyDataField:{name:'C_ID'},
						parentDataField:{name:'P_ID'} 
					},
					id:'C_ID'
	            };
			var dataAdapter = new $.jqx.dataAdapter(source);
			
			
			$("#kpiEvalresultlist").jqxTreeGrid({
				source: dataAdapter, 
				height: '100%', 
				width: '100%',
				sortable: true,
				theme:'blueish', 
				icons:function(rowKey, rowdata){
	     			var icon = ""; 
	     			if(rowdata.NO1!=null&&rowdata.NO1=="3"){   
	     				icon = '../../resources/cmresource/image/pageIcon.png';  
	     			}else if(rowdata.NO1=="2"){
	     				icon = '../../resources/cmresource/image/icon-darkblue-folder.png';  
	     			}else{   
	     				icon = '../../resources/cmresource/image/icon-folder.png';        
	     			}
	     			
	     			return icon;     
	     		}, 
				columns:[
					{ dataField:'C_ID',text:'C_ID',align:'center', hidden:true},
					{ dataField:'P_ID',text:'P_ID',align:'center', hidden:true},
					{ dataField:'C_NM',text:'평가지표', algin:'center',  
						cellsrenderer:function(row,datafield,value,rowdata) { 
							if(rowdata.records!=null){
								var cntlabel = "<span style='color: Blue;' data-role=='treefoldersize'> (" + rowdata.records.length + ")</span>";
								return rowdata.C_NM+cntlabel;
							}else if(rowdata.NO1 == "3"){
								var redLabel = "<span id='"+rowdata.C_ID+"' style='color: red;' data-role=='treefoldersize'>[" + rowdata.SC_NM + "]</span>";
								var blueLabel = "<span id='"+rowdata.C_ID+"' style='color: Blue;' data-role=='treefoldersize'>[" + rowdata.SC_NM + "]</span>";
								var blackLabel = "<span id='"+rowdata.C_ID+"' style='color: black;' data-role=='treefoldersize'>[" + rowdata.SC_NM + "]</span>";
								if(rowdata.STATUS == "D"){
									return "<span id='b"+rowdata.C_ID+"' style='color: black;'>●</span>"+rowdata.C_NM+blackLabel;
								}else if(rowdata.STATUS == "S"){
									return "<span id='b"+rowdata.C_ID+"' style='color: blue;'>●</span>"+rowdata.C_NM+blueLabel;
								}else{
									return "<span id='b"+rowdata.C_ID+"' style='color: red;'>●</span>"+rowdata.C_NM+ redLabel;
								}
							} else {
								return rowdata.C_NM;
							}
						}
					}
				]
			});
			});
  		}
    		  
		function reRowData(){
			var row = $("#kpiEvalresultlist").jqxTreeGrid("getSelection")[0];
			getDataList(row);         
		}
		//증빙자료다운로드   
		function fileDownLoad(fileid,kpiId,yyyy){              
			location.replace("./kpidownload?fileid="+fileid+"&kpiId="+kpiId+"&yyyy="+yyyy);  
			
		}  
  		
    	//평가결과List
    	function getDataList(obj){
			$("#gradeDescResult").val("");
			$("#ipvDescResult").val("");			
    		resetTarget();   
    		var html = '';        
    		        
			$('#fileList').empty();
			$('#kpiList').empty();
			$('#colList').empty();
			$("#evalGrade").empty();
			
			var acctId = obj.ACCT_ID;  
    		var kpiId  = obj.C_ID;      
    		var status  = obj.STATUS; 
    		var kpiNm  = obj.C_NM;
    		var scId  = obj.SC_ID;
    		var scNm  = obj.SC_NM;
    		var mtId  = obj.MT_ID;
    		var owneruserId = obj.OWNER_USER_ID;
    		var kpichargeId =obj.KPI_CHARGE_ID;
    		var unId =	obj.UNID;
    		
    		$("#kpiId").val(kpiId);   
    		$("#status").val(status);   
    		$("#acctId").val(acctId);
    		$("#scNm").val(scNm);     
    		$("#kpiNm").val(kpiNm);
    		$("#scId").val(scId);     
    		$("#mtId").val(mtId); 
    		
    		$("#owneruserId").val(owneruserId);   
    		$("#kpichargeId").val(kpichargeId);   
    		$("#unId").val(unId);
    		    
    		//지표실적, 목표실적, 달성률  
        	var kpiData = $i.service("getKpiTargetData",[$("#cboSearchResultYear").val(),"${param.pEvaGbn}",kpiId]);
        		kpiData.done(function(data){
        		var kpiList = "";               
				if(data.returnArray.length >0){            
					kpiList += '<tr>';
					kpiList += '<td align="right" style="padding-left:5px !important;">'+data.returnArray[0].KPI_VALUE+'</td>';
					kpiList += '<td align="right" style="padding-left:5px !important;">'+data.returnArray[0].TARGET_AMT+'</td>';
					kpiList += '<td align="right" style="padding-left:5px !important;">'+data.returnArray[0].GOA+'</td>';
					kpiList += '</tr>';	      
					if(data.returnArray[0].CHARGE_CONFIRM=="N"){
						$("#confirmYNText").html("[ 미승인 ]");
					}
					else{
						$("#confirmYNText").html("");
					}
				}else{       
					kpiList += '<tr>';       
					kpiList += '<td class="cell t_center" colspan="3">No data to display</td>';
					kpiList += '</tr>';  
				}
				$("#kpiList").append(kpiList);
        	});
			//항목, 항목실적   
			var mainData = $i.service("kpicolValueList",[$("#cboSearchResultYear").val(),"${param.pEvaGbn}",kpiId]);
			mainData.done(function(data){
				var tableData = "";   
				for(var i=0;i<data.returnArray.length;i++){
					tableData += '<tr>';
					tableData += '<td style="padding-left:5px !important;">'+data.returnArray[i].COL_NM+'</td>';
					tableData += '<td style="padding-left:5px !important;">'+data.returnArray[i].COL_VALUE+'</td>';
					tableData += '</tr>';     
				}
				if(tableData==""){   
					tableData += '<tr>';    
					tableData += '<td colspan="2" class="cell t_center">No data to display</td>';   
					tableData += '</tr>';
				}  
				$("#colList").append(tableData);
			});
			//증빙자료  
			var fileList = $i.service("kpifileList",[$("#cboSearchResultYear").val(),kpiId]);
			fileList.done(function(data){
			if(data.returnArray.length>0){
				for(var j=0;j<data.returnArray.length;j++){
					var ext = data.returnArray[j].FILE_EXT;
					ext = ext.toLowerCase();    
					       
					var img = "";
					if(ext == 'ppt' || ext == 'pptx') {      
						img = '<img src="../../resources/cmresource/image/fileExtIcon/ppt.png" alt="pdf"/>';
					} else if(ext == 'doc' || ext == 'docx') {
						img = '<img src="../../resources/cmresource/image/fileExtIcon/doc.png" alt="word"/>';
					} else if(ext == 'hwp') {
						img = '<img src="../../resources/cmresource/image/fileExtIcon/hwp.png" alt="hwp"/>';
					} else if(ext == 'pdf') {
						img = '<img src="../../resources/cmresource/image/fileExtIcon/pdf.png" alt="pdf"/>';
					} else if(ext == 'txt') {
						img = '<img src="../../resources/cmresource/image/fileExtIcon/txt.png" alt="txt"/>';
					} else if(ext == 'xls' || ext == 'xlsx'){   
						img = '<img src="../../resources/cmresource/image/fileExtIcon/xls.png" alt="xls"/>';    
					} else {          
						img = '<img src="../../resources/cmresource/image/fileExtIcon/etc.png" alt="etc"/>';     
					}       
					
					var filename = data.returnArray[j].FILE_ORG_NAME;
					var fullFilename = filename;
					
					if(filename != null && data.returnArray.length>14) {
						filename = data.substring(0, 14) + '...';
					}   
					html += '<tr>';  
					html += "<td align='left' style='padding-left:5px !important;'><p class='label-link-table' title='"+fullFilename+"'><a href=\"javaScript:fileDownLoad('"+data.returnArray[j].FILE_ID+"','"+data.returnArray[j].KPI_ID+"','"+data.returnArray[j].YYYY+"')\";>"+ img + data.returnArray[j].FILE_ORG_NAME + "</a></p></td>";
					html += '</tr>';
				}   
			}else{       
				html += '<tr>';    
				html += '<td class="cell t_center">No data to display</td>';   
				html += '</tr>';
			}
			$('#fileList').append(html);    
			});
			

    		$("#owneruserId").val(owneruserId);   
    		$("#kpichargeId").val(kpichargeId);   
    		$("#unId").val(unId);
			
			//확정,확정취소 버튼 숨기기
			if($("#status").val() == "S"){  
					
					$("#btnUserEvalsave ").show();  
					$("#btnUserEvalsubmit").show();
					$("#btnUserEvalsubmitCencel").hide();  
			}else if($("#status").val() == "D"){
					
					$("#btnUserEvalsave ").hide();
					$("#btnUserEvalsubmit").hide();
					$("#btnUserEvalsubmitCencel").show();
			}else if($("#status").val() == "F"){         
					      
					$("#btnUserEvalsave ").show();
					$("#btnUserEvalsubmit").show();
					$("#btnUserEvalsubmitCencel").hide();
			}else{      
					
					$("#btnUserEvalsave ").show();
					$("#btnUserEvalsubmit").show();
					$("#btnUserEvalsubmitCencel").hide();  
			} 

			//평가결과: 평가근거, 개선사항(담당자 평가)
			var resGrdListgrade = $i.service("kpiGradeDesc",[$("#cboSearchResultYear").val(),"${param.pEvaGbn}",kpiId]);
			resGrdListgrade.done(function(res){ 
				var evalGrade = "";

				for(var i=0;i<res.returnArray.length;i++){            
		        	$("#gradeDescResult").val(res.returnArray[i].GRADE_DESC);
			        $("#ipvDescResult").val(res.returnArray[i].IPV_DESC);  
			        $("#evalStatus").val(res.returnArray[i].STATUS);  
			        $("#evalGrade").val(res.returnArray[i].EVA_GRDNM);       
			        	evalGrade += '<div class="f_left m_l10 bold" id="evalGrade" name="evalGrade" value="'+res.returnArray[i].EVA_GRDNM+'"/>'+res.returnArray[i].EVA_GRDNM;  
                 }           
				  
				if($("#evalStatus").val()=="L"){
					$("#resultEval").hide();  
					$("#evalStatus").val("");
				}else{  
					$("#resultEval").show();  				
				} 
	    		$("#evalGrade").append(evalGrade); 
			});
			//평가근거/개선사항       
			var resGrdList = $i.service("kpiEvalGradeDesc",[$("#cboSearchResultYear").val(),"${param.pEvaGbn}",kpiId]);  
			resGrdList.done(function(data){    
				for(var i=0;i<data.returnArray.length;i++){   
					if(data.returnArray[0].GRADE != ""){
						$("#grade_"+data.returnArray[0].GRADE).prop("checked","checked");	
		        		$("#gradeDesc").val(data.returnArray[i].GRADE_DESC);
			        	$("#ipvDesc").val(data.returnArray[i].IPV_DESC);  
					}
                 }  					
			});
			
    	}   
		function makeRadio(){
			var radioData = $i.service("makeRadioButton",[]);      
			radioData.done(function(data){   
			var radio = "";       
			$("#radioList").empty();    
			for(var i=0;i<data.returnArray.length;i++){                             
				radio += '<input type="radio" class="m_b2 m_r3 m_l5" id="grade_'+data.returnArray[i].EVA_GRD+'" name="gradeValue" value="'+data.returnArray[i].EVA_GRD+'"/>'+data.returnArray[i].EVA_GRDNM;
			}                                   
			$("#radioList").append(radio);                 
			});          
		}      
		
		function changeColor(value){
			var row = $("#kpiEvalresultlist").jqxTreeGrid("getSelection")[0];
			var kpiId = row.C_ID;
			if(value == "S"){
				$('#kpiEvalresultlist').jqxTreeGrid('getSelection')[0].STATUS = "S";
				$("#"+kpiId)[0].style.color = "blue";
				$("#b"+kpiId)[0].style.color = "blue";
				$("#ipvDesc").prop("readonly","");
				$("#gradeDesc").prop("readonly","");
				$("[name='gradeValue']").prop("disabled",false);
			}else if(value == "D"){
				$('#kpiEvalresultlist').jqxTreeGrid('getSelection')[0].STATUS = "D";
				$("#"+kpiId)[0].style.color = "black";
				$("#b"+kpiId)[0].style.color = "black";
				$("#ipvDesc").prop("readonly","readonly");
				$("#gradeDesc").prop("readonly","readonly");
				$("[name='gradeValue']").prop("disabled",true);
			}else if(value == "F"){
				$('#kpiEvalresultlist').jqxTreeGrid('getSelection')[0].STATUS = "F";
				$("#"+kpiId)[0].style.color = "red";
				$("#b"+kpiId)[0].style.color = "red";
				$("#ipvDesc").prop("readonly","");
				$("#gradeDesc").prop("readonly","");
				$("[name='gradeValue']").prop("disabled",false);
			}else{
				$('#kpiEvalresultlist').jqxTreeGrid('getSelection')[0].STATUS = "";
					$("#"+kpiId)[0].style.color = "red";
					$("#ipvDesc").prop("readonly","");
					$("#gradeDesc").prop("readonly","");
					$("[name='gradeValue']").prop("disabled",false);
			}
		}
    	//평가지표관리 저장
    	function insertKpiValue(){
        		if($("#kpiId").val() == ""){         
        			$i.dialog.warning("SYSTEM","평가지표를 선택하세요");       
					return;
        		}    
        		if($(':radio[name="gradeValue"]:checked').length == 0){
					$i.dialog.warning("SYSTEM","평가등급을 선택하세요");       
					return;
        		}   
    			$("#yyyy").val($("#cboSearchResultYear").val());
				$("#mm").val("12");          
				$("#grade").val( $(':radio[name="gradeValue"]:checked').val());
				$("#status").val("S");   
				
				var check =$i.post("./insertkpiValue","#kpiValueSave");
        		check.done(function(data){        
        				if(args.returnCode == "EXCEPTION"){
		        		$i.dialog.error("SYSTEM",args.returnMessage);
		        		}else{    
		        			$i.dialog.alert("SYSTEM","저장 되었습니다"); 
		        			changeColor("S");
		        			buttonChange();  
		        			reRowData();  
		        		}
		        	});
        			
        	}
    	//신규   
    	function resetTarget(){       
        		$("#gradeDesc").val("");
        		$("#ipvDesc").val("");          
        		$('input:radio[name="gradeValue"]').prop('checked', "");      
        	}
    	
		//확정
		function insertKpiSubmit(){
    		if($("#kpiId").val() == ""){     
    			$i.dialog.warning("SYSTEM","평가지표를 선택하세요");       
				return;
    		}  
        		if($(':radio[name="gradeValue"]:checked').length == 0){
					$i.dialog.warning("SYSTEM","평가등급을 선택하세요");       
					return;
        		}
        		if($("#ipvDesc").val().trim()==""){
	    			$i.dialog.warning("SYSTEM","권고사항을 입력해 주세요");
	    			return ;
	    		}
	    		if($("#gradeDesc").val().trim()==""){
	    			$i.dialog.warning("SYSTEM","평가근거를 입력해 주세요"); 
	    			return ;
	    		}
        		$("#yyyy").val($("#cboSearchResultYear").val());
				$("#mm").val("12");      
				$("#grade").val( $(':radio[name="gradeValue"]:checked').val());
				$("#status").val("D");   
				var check =$i.post("./insertkpiValue","#kpiValueSave");
        		check.done(function(data){        
        				if(args.returnCode == "EXCEPTION"){
		        		$i.dialog.error("SYSTEM",args.returnMessage);
		        		}else{    
		        			$i.dialog.alert("SYSTEM","확정 되었습니다");
		        			changeColor("D");
		        			buttonChange();
		        			reRowData();
		        		}
		        	});
        	}
		//확정취소
    	function insertKpiCancelSubmit(){
    		if($("#kpiId").val() == ""){     
    			$i.dialog.warning("SYSTEM","평가지표를 선택하세요");       
				return;
    		} 
    		if($(':radio[name="gradeValue"]:checked').length == 0){
				$i.dialog.warning("SYSTEM","평가등급을 선택하세요");       
				return;
    		}     
    		
    		$("#yyyy").val($("#cboSearchResultYear").val());
			$("#mm").val("12");      
			$("#grade").val( $(':radio[name="gradeValue"]:checked').val());
			$("#status").val("F");   
			var check =$i.post("./updatekpiValue","#kpiValueSave");
    		check.done(function(data){        
    				if(args.returnCode == "EXCEPTION"){
	        		$i.dialog.error("SYSTEM",args.returnMessage);
	        		}else{    
	        			$i.dialog.alert("SYSTEM","확정취소 되었습니다");
	        			changeColor("F");
	        			buttonChange();
	        			reRowData();
	        		}
	        	});   
    	}
		

    	//확정후 버튼 CHANGE
    	function buttonChange(){
			if($("#status").val() == "S"){  
				
				$("#btnUserEvalsave ").show();  
				$("#btnUserEvalsubmit").show();
				$("#btnUserEvalsubmitCencel").hide();  
			}else if($("#status").val() == "D"){
					
					$("#btnUserEvalsave ").hide();
					$("#btnUserEvalsubmit").hide();
					$("#btnUserEvalsubmitCencel").show();
			}else if($("#status").val() == "F"){         
					      
					$("#btnUserEvalsave ").show();
					$("#btnUserEvalsubmit").show();
					$("#btnUserEvalsubmitCencel").hide();
			}else{      
					
					$("#btnUserEvalsave ").show();
					$("#btnUserEvalsubmit").show();
					$("#btnUserEvalsubmitCencel").hide();  
			}    
    	}       
    	
    	
    	function getReportDetail(){    		
    		
	    		if($("#kpiId").val() == ""){     
	    			$i.dialog.warning("SYSTEM","평가지표를 선택하세요");       
					return;
	    		}  
        		var scid = $("#kpiId").val().replace("S","").split("M")[0];
        		var mtid = $("#kpiId").val().replace("S","").split("M")[1];
        		var programid = "popupKpiDetail";  
        		var url = "./"+programid+"?kpiid="+$("#kpiId").val()+"&searchyear="+$("#cboSearchResultYear").val();
        		var cwin = window.open(url,"","width=800, resizable=no, scrollbars=no, status=no;");        		
        }	
    	
    	
	    </script>
    </head>
    <body class='blueish' > 
    	<div class="wrap" style="width:98% !important; margin:0 auto !important;">
    	<div class="container  f_left" style="width:1150px;">
    		<input type="hidden" id="evaGbn" name="evaGbn" value="${param.pEvaGbn}" />   
		    <div class="header f_left" style="width:100%; height:27px; margin:10px 0">		
				<div class="label type1 f_left">년도:</div>      
				<div class="combobox f_left"  id='cboSearchResultYear' name="searchResultYear"></div>
				<div class="label type1 f_left" style="float:left;">평가자：</div>      
					<div style="float:left;width:100px; margin-right:10px;" id="empCombo" class="iwidget_combobox"></div>
				<div class="group_button f_right">  
					<div class="button type1 f_left">
						<img src="../../resources/css/images/img_icons/eva_sign.png" style="float:left; padding-right:20px;">
						<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="searchStart();" />
					</div>    
				</div>   
			</div>
		</div>	
			<div class="container  f_left" style="width:1150px;">
	   	 		<div class="content f_left" style=" width:100%;">
					<div class="grid f_left" style="width:100%; margin:10px 0%; height:213px;">
						<div class="treegrid" id="kpiEvalresultlist"></div>    
					</div>      
	    		</div>     
	    	</div>
  			<div class="content f_left" style=" width:1150px; margin:10px 0; background:#ffffff;">
		 		<div class="content f_left" style="width:47%; margin:10px 1%;">
		 		<form id="kpiValueSave" name="kpiValueSave"> 
				 	<input type="hidden" id="kpiNm" name="kpiNm" />
					<input type="hidden" id="scNm" name="scNm" /> 
					<input type="hidden" id="acctId" name="acctId" />   
					<input type="hidden" id="status" name="status" />         
					<input type="hidden" id="evaGbn" name="evaGbn" value="${param.pEvaGbn}" /> 
					<input type="hidden" id="mtId" name="mtId" />
					<input type="hidden" id="scId" name="scId" value="100"/>   
					<input type="hidden" id="grade" name="grade" />   
					<input type="hidden" id="yyyy" name="yyyy"/>   
					<input type="hidden" id="mm" name="mm" />  
				    <input type="hidden" id="kpiId" name="kpiId" />     
					<input type="hidden" id="gubn" name="gubn" value="0" />
					<input type="hidden" id="evalStatus" name="evalStatus" /> 
					<input type="hidden" id="owneruserId" name="owneruserId" />   
					<input type="hidden" id="kpichargeId" name="kpichargeId"/>   
					<input type="hidden" id="unId" name="unId" />      
		 			<div class="tabs f_left" style=" width:100%; height:376px; margin-top:0px;">		 			
	        		<div id='jqxTabs'>
			        	<ul>  
				          	<li style="margin-left: 0px;">실적현황</li>
				            <li style="margin-left: 0px;">주요성과/개선사항</li>   
			            </ul>
						<div class="tabs_content" style="height:100%; padding:0 2%; ">
							<div class="group f_left  w100p m_t5">    
								<div class="label type2 f_left">실적현황</div>
								<div class="f_left m_l10" style="font-weight:bold; color:red;margin-top:4px;" id='confirmYNText'></div> 
									<div class="group_button f_right"></div>
						 	</div>      
	    				<div class="table f_left" style="width:100%; margin-bottom:30px;">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<thead>
									 <tr>
										<th style="width:35%;"><div>지표실적</div></th>
										<th style="width:35%;"><div>목표실적</div></th>
										<th style="width:30%;"><div>달성률</div></th>
									 </tr> 
								</thead>  
								<tbody id="kpiList">
									<tr>
										<td class="cell t_center" colspan="3">No data to display</td>
									</tr>   
								</tbody>
							</table>
						</div>  
						<div class="table f_left" style="width:100%; margin-bottom:30px;">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<thead>
									<tr>
										<th style="width:70%;"><div>항목</div></th>
										<th style="width:30%;"><div>항목실적</div></th>
									</tr> 
								</thead>  
								<tbody id="colList">  
										<tr>
											<td class="cell t_center" colspan="2">No data to display</td>
										</tr>
								</tbody>
							</table>
						</div>  
						<div class="table f_left" style="width:100%; margin-bottom:10px;">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<thead>
									<tr>
										<th style="width:100%;"><div>증빙자료</div></th>
									</tr> 
								</thead>  
								<tbody id="fileList">      
									<tr>
										<td><div class="cell t_center">No data to display</div></td>
									</tr>
								</tbody>
							</table>
						</div>
			 		</div>
			  		<!--//tabs_content-->
						<div class="tabs_content" style="height:100%; padding:0 2%; background-color:#eeeeee ">   
						 	<div class="group f_left  w100p m_t5">
								<div class="label type2 f_left">주요성과</div>   
								<div class="f_left m_l10" style="font-weight:bold; color:red;margin-top:4px;" id='resultEval'> [ 미승인 ] </div>  
								<textarea id="gradeDescResult" name="gradeDescResult" style=" width:97%; height:113px; margin:6px 1%; padding-left:5px; padding-top:3px;" readonly></textarea>   
							</div>
							<!--group-->
							<div class="group f_left  w100p">
								<div class="label type2 f_left">개선사항</div>
								<textarea id="ipvDescResult" name="ipvDescResult" style=" width:97%; height:113px; margin:6px 1%; padding-left:5px; padding-top:4px;" readonly></textarea>           
							</div> 
							<div class="group f_left  w100p ">
								<div class="label type2 f_left" id ='labelGrade'>평가등급</div>
								<div class="f_left m_l10 bold" style="font-size:13px; margin-top:5px;" id='evalGrade' ></div>         
							</div> 
							<!--group-->	          
					 	</div>
					  	<!--//tabs_content-->
					</div>
					<!--//jqxTabs-->    
				 	</div>
				  	<!--//tabs-->
				</div>
			<!--content-->   
			<div class="content f_right" style="width:49%; margin:10px 1%;"> 
				<div class="group_button f_left m_t15">
					<div class="button type2 f_left">
						<input type="button" value="지표정의서" id='btnKpiDocument' width="100%" height="100%" onclick="getReportDetail();"/> 
					</div>   
				</div>	
				<div class="group_button f_right m_t15"> 
					<div class="button type2 f_left">
						<input type="button" value="저장" id='btnUserEvalsave' width="100%" height="100%" onclick="insertKpiValue();"/>
					</div>
					<div class="button type2 f_left">  
						<input type="button" value="확정" id='btnUserEvalsubmit' width="100%" height="100%" onclick="insertKpiSubmit();"/>   
					</div>  
					<div class="button type3 f_left">  
						<input type="button" value="확정취소" id='btnUserEvalsubmitCencel' width="100%" height="100%" onclick="insertKpiCancelSubmit();"/>   
					</div> 
				 </div>	
				<!--group_button--> 
				<div class="group f_left  w100p m_t5">
					<div class="label type2 f_left">평가근거</div>
					<textarea id="gradeDesc" name="gradeDesc" class="textarea"style=" width:100%; height:100px; margin:3px 0; padding-left:5px !important; padding-top:3px !important;"></textarea>
				</div>       
				<!--group-->  
				<div class="group f_left  w100p m_t5">
					<div class="label type2 f_left">권고사항</div>   
					<textarea id="ipvDesc" name="ipvDesc" class="textarea" style=" width:100%; height:100px; margin:3px 0; padding-left:5px !important; padding-top:3px !important;"></textarea>
				</div>  
				<!--group-->   
				<div class="group f_left  w100p m_t5">
					<div class="label type2 f_left">평가등급</div>
				</div>
				<!--group-->
				<div style="float:left;margin-top:5px;margin-left:10px;" id="radioList"></div>
		  	</div>
	  	</form> 
		</div>
	</div>       
	</body>
</html>

   