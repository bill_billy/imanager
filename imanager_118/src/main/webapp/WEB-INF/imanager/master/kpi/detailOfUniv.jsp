<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>지표관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	    
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/widget/Form.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
        <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
        <script>
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){     
        		$("#btnList").jqxButton({width:'', theme:'blueish'});
        		$("#btnInsert").jqxButton({width:'', theme:'blueish'});
        		$("#btnInsertnew").jqxButton({width:'', theme:'blueish'});
        		$("#btnRemove").jqxButton({width:'', theme:'blueish'});  
        		$("#cboPerscd").jqxDropDownList({animationType: 'fade',dropDownHorizontalAlignment: 'right',dropDownWidth: 150,width: 150,height: 23,theme:'blueish'});
        		$("#cboMeascycle").jqxDropDownList({animationType: 'fade',dropDownHorizontalAlignment: 'right',dropDownWidth: 150,width: 150,height: 23,theme:'blueish'});
        		$("#cboEvaltype").jqxDropDownList({animationType: 'fade',dropDownHorizontalAlignment: 'right',dropDownWidth: 150,width: 150,height: 23,theme:'blueish'});
        		$("#cboUnit").jqxDropDownList({animationType: 'fade',dropDownHorizontalAlignment: 'right',dropDownWidth: 150,width: 150,height: 23,theme:'blueish'});
        		$("#cboEvatyp").jqxDropDownList({animationType: 'fade',dropDownHorizontalAlignment: 'right',dropDownWidth: 300,width: 300,height: 23,theme:'blueish'});
        		$("#cboMtUdc3").jqxDropDownList({animationType: 'fade',dropDownHorizontalAlignment: 'right',dropDownWidth: 150,width: 150,height: 23,theme:'blueish'});
        		$("#cboMtgbn").jqxDropDownList({animationType: 'fade',dropDownHorizontalAlignment: 'right',dropDownWidth: 150,width: 150,height: 23,theme:'blueish'});
        		$("#cboMtdir").jqxDropDownList({animationType: 'fade',dropDownHorizontalAlignment: 'right',dropDownWidth: 150,width: 150,height: 23,theme:'blueish'});
        		
        		$('#dateStartdate').datepicker({
					changeYear: true,
					showOn: 'button',
					buttonImage: '../../resources/cmresource/image/icon-calendar.png',
					buttonImageOnly: true,
					dateFormat: "yymmdd", //20141212
				});
				$('#dateEnddate').datepicker({
					changeYear: true,
					showOn: 'button',
					buttonImage: '../../resources/cmresource/image/icon-calendar.png',
					buttonImageOnly: true,
					dateFormat: "yymmdd", //20141212
				});
        		init(); 
        	});
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		if("${param.mtid}"!=""){
        			$("#btnInsertnew").hide();
        		}
        		checkMm();
        		getScidList();
        	}
        	//조회
        	function search(){
        		
        	}
        	//입력
        	function insert(gubn){
        		if($("#txtMtCD").val().trim()==""){
        			$i.dialog.warning("SYSTEM","지표코드를 입력하세요");
        			return;
        		}
        		if($("#txtMtnm").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","지표명을 입력하세요");
        			return;
        		}
        		if($("#txtCalnm").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","산출공식을 선택하세요");
        			return;
        		}
        		if($("#txtStraid").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","전략 및 세부전략을 선택하세요");
        			return;
        		}
        		if($("#dateStartdate").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","시작일자를 선택하세요");
        			return;
        		}
        		if($("#dateEnddate").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","종료일자를 선택하세요");
        			return;
        		}
        		if($("#txtScid").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","평가부서를 선택하세요");
        			return;
        		}
        		if($("#txtKpiWeight").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","가중치를 입력하세요");
        			return;
        		}
        		
				if("${param.mtid}" != ''){
					$i.dialog.confirm("SYSTEM","수정사항은 전체 평가부서에 동일하게 적용됩니다",
						function(){
							var mtudc2ChkValue = "";
							for(var i=0;i<$("[name='chkmtudc2']").length;i++){
								if($("[name='chkmtudc2']")[i].checked == true){
									if(mtudc2ChkValue == ""){
										mtudc2ChkValue += $("[name='chkmtudc2']")[i].value;
									}else{
										mtudc2ChkValue += "," + $("[name='chkmtudc2']")[i].value;
									}
								}
							}
							$("#txtMtudc2").val(mtudc2ChkValue);
			        		$i.insert("#saveForm").done(function(args){
			        			if(args.returnCode == "EXCEPTION"){
			        				$i.dialog.error("SYSTEM",args.returnMessage);
			        			}else{
			        				$i.dialog.alert("SYSTEM","저장 되었습니다.",function(){
			        					if(gubn == "S"){
			        						goList();
			        					}else{
			        						resetForm();
			        					}	
			        				});
			        				
			        			}
			        		}).fail(function(e){ 
			        			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
			        		});
						}
					);
				}else{
					var mtudc2ChkValue = "";
					for(var i=0;i<$("[name='chkmtudc2']").length;i++){
						if($("[name='chkmtudc2']")[i].checked == true){
							if(mtudc2ChkValue == ""){
								mtudc2ChkValue += $("[name='chkmtudc2']")[i].value;
							}else{
								mtudc2ChkValue += "," + $("[name='chkmtudc2']")[i].value;
							}
						}
					}
					$("#txtMtudc2").val(mtudc2ChkValue);
	        		$i.insert("#saveForm").done(function(args){
	        			if(args.returnCode == "EXCEPTION"){
	        				$i.dialog.error("SYSTEM","저장 중 오류가 발생했습니다."+args.returnMessage);
	        			}else{
	        				$i.dialog.alert("SYSTEM","저장 되었습니다.",function(){
	        					if(gubn == "S"){
	        						goList();
	        					}else{
	        						resetForm();
	        					}
	        				});
	        				
	        			}
	        		}).fail(function(e){ 
	        			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
	        		});
				}
        	}
        	//삭제
        	function remove(){
        		var check = $i.sqlhouse(128,{ACCT_ID:"${param.acctid}",EVA_GBN:"B",MT_ID:"${param.mtid}"});
        		check.done(function(res){
        			if(res.returnArray.length > 0){
        				$i.dialog.warning("SYSTEM","이미 평가가 마감이된 지표입니다");
        				return;
        			}else{
        				$i.remove("#saveForm").done(function(args){
		        			if(args.returnCode == "EXCEPTION"){
		        				$i.dialog.error("SYSTEM","삭제 중 오류가 발생했습니다.");
		        			}else{
		        				$i.dialog.alert("SYSTEM","삭제 되었습니다.",function(){
		        					goList('D');
		        				});
		        				
		        			}
		        		}).fail(function(e){
		        			$i.dialog.error("SYSTEM","삭제 중 오류가 발생했습니다.");
		        		});
        			}
        		});
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        		
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function checkStart(){
        		var searchyear = "${param.searchyear}";
        		var startDate = $("#dateStartdate").val().substring(0,4);
        		if(searchyear < startDate){
        			$i.dialog.warning("SYSTEM", "시작일자가 평가년도보다 나중입니다");
        			$("#dateStartdate").val("");
        		}
        	}
        	function popupFormula(){       		
        		
        		window.open('', 'popupMappingKpiFormula','scrollbars=no, resizable=no, width=930, height=400'); 
        		
        		var form = new Form();
        		form.addInput("acctid","${param.acctid}");
        		form.addInput("mtid","${param.mtid}");
        		form.addInput("calculateList",escape($("#hiddenCalculateList").val())); 
        		form.action = "./popupMappingKpiFormula";
        		form.method = "post";
        		form.target = "popupMappingKpiFormula";
        		form.submit();
        		
        	}
        	function popupStraSubstra(){
        		window.open('./popupMappingKpiStrategy?acctid=${param.acctid}', 'popupMappingKpiFormula','scrollbars=no, resizable=no, width=930, height=300');
        	}
        	function popupKpiuser(gubn){
        		window.open('./popupMappingKpiUser?acctid=${param.acctid}&txtGubn='+gubn, 'popupMappingKpiUser','scrollbars=no, resizable=no, width=930, height=400');
        	}
        	function popupKpidept(){
        		window.open('./popupMappingKpiDept?acctid=${param.acctid}&orgscid='+$("#txtScid").val()+'&yyyy='+$("#txtSearchyyyy").val(), 'popupMappingKpiDept','scrollbars=no, resizable=no, width=607, height=600');
        	}
        	function getScidList(){
        		var mtid = "${param.mtid}";
        		var acctid = "${param.acctid}";
        		var scid = "";
        		var scnm = "";
        		if(mtid != ""){
        			var scidlist = $i.sqlhouse(94,{ACCT_ID:acctid, MT_ID:mtid});
        			scidlist.done(function(res){
        				if(res.returnArray.length > 0){
        					for(var i=0;i<res.returnArray.length;i++){
        						if(i==0){
        							scid += res.returnArray[i].SC_ID;
        							scnm += res.returnArray[i].SC_NM+"["+res.returnArray[i].SC_ID+"]";
        						}else if(i == res.returnArray.length-1){
        							scid += ","+res.returnArray[i].SC_ID;
        							scnm += ","+res.returnArray[i].SC_NM+"["+res.returnArray[i].SC_ID+"]";
        						}else{
        							scid += ","+res.returnArray[i].SC_ID;
        							scnm += ","+res.returnArray[i].SC_NM+"["+res.returnArray[i].SC_ID+"]";
        						}
        					}
        				}
        				$("#txtOrgscid").val(scid);
        				$("#txtScid").val(scid);
        				$("#txtareaScnm").val(scnm);
        			});
        		}
        	}
        	function resetForm(){
        		$("#txtMtid").val("");
        		$("#txtMtCD").val("");
        		$("#txtMtnm").val("");
        		$("#txtareaMtdef").val("");
        		$("#hiddenCalculateList").val("");
        		$("#txtCalcod").val("");
        		$("#txtCalnm").val("");
        		$("[name='mm']").prop("checked",false);
        		$("#txtSubstraid").val("");
        		$("#txtSubstranm").val("");
        		$("#txtStraid").val("");
        		$("#txtStranm").val("");
        		$("#dateStartdate").val("");
        		$("#dateEnddate").val("");
        		$("#txtDecimalplaces").val("");
        		$("#txtareaTargetdesc").val("");
        		$("#txtareaMtdesc").val("");
        		$("#txtOrgsacid").val("");
        		$("#txtScid").val("");
        		$("#txtareaScnm").val("");
        		$("#hiddenMtUserID").val("");
        		$("#hiddenMtUserDept").val("");
        		$("#txtMtUserNM").val("");
        		$("#txtKpiWeight").val("");
        		$("#cboPerscd").jqxDropDownList({selectedIndex : 0});
        		$("#cboMtdir").jqxDropDownList({selectedIndex : 0});
        		$("#cboMeascycle").jqxDropDownList({selectedIndex : 0});
        		$("#cboEvaltype").jqxDropDownList({selectedIndex : 0});
        		$("#cboUnit").jqxDropDownList({selectedIndex : 0});
        		$("#cboEvatyp").jqxDropDownList({selectedIndex : 0});
        		$("#cboMtUdc3").jqxDropDownList({selectedIndex : 0});
        		$("#cboMtgbn").jqxDropDownList({selectedIndex : 0});
        		$("#relatedWorkArea > tbody").remove()
        	}
        	function goList(flag){
        		var paramGridList = "${param.grdList}";
        		if(paramGridList != ""){
        			flag = "G";
        		}
        		var acctid = "${param.acctid}";
        		var searchyear = "${param.searchyear}";
        		var tabscid = "${param.scid}";
        		var gubn = "${param.gubn}";
        		if(parent!=null&&parent.closeKpiForm!=undefined){
        			parent.closeKpiForm(flag); 
        		}else{
        			location.replace("./crud?tabscid="+tabscid+"&acctid="+acctid+"&searchyear="+searchyear+"&gubn="+gubn);
        		}
        	}
        	function checkMm(){
        		var pAcctid = "${param.acctid}";
        		var mtid = "${param.mtid}";
        		if(mtid != ""){
        			var dat = $i.sqlhouse(85,{ACCT_ID:pAcctid,MT_ID:mtid});
	        		dat.done(function(res){
	        			if(res.returnArray.length > 0){
	        				for(var i=0;i<res.returnArray.length;i++){
	        					for(var j=0;j<$("[name='mm']").length;j++){
	        						if($("[name='mm']")[j].value == res.returnArray[i].MM){
	        							$("[name='mm']").eq(j).prop("checked","checked");
	        						}
	        					}
	        				}
	        			}
	        		});
        		}
        	}
        	function removeRelatedWork(event){
        		$($(event.currentTarget).parents("tr")[0]).remove();
        	}
        	function addRelatedWork(){ 
        			var appendStr = "<tr><td><input type='text'  name='relatedWork1' class='input type2  f_left t_left'  style='width:120px; margin:3px 0; '/><img onClick='removeRelatedWork(event)' width='16' title='삭제' style='cursor:pointer;margin: 0px auto; padding: 0px; text-align: right; float: right; display: inline-table;' alt='remove' src='../../resources/cmresource/image/icon-minus.png'></td><td><input type='text'  name='relatedWork2' class='input type2  f_left t_left'  style='width:320px; margin:3px 0; '/></td></tr>";
        			var newItem = $(appendStr).appendTo("#relatedWorkArea"); 
        	}
        	
 
        </script>
        <style  type="text/css">
        	.ui-datepicker-trigger{
        		float:left !important;
        		margin-top:5px !important;
        	}
        </style>
    </head>
    <body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 1%;">
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:100%;">
					<div class="group f_left  w100p m_b5">
						<div class="group_button f_right">	
							<div class="button type2 f_left" style="margin-bottom:0;">
								<input type="button" value="목록" id='btnList' width="100%" height="100%" onclick="goList();"/>
							</div>
							<div class="button type2 f_left" style="margin-bottom:0;">
								<input type="button" value="저장" id='btnInsert' width="100%" height="100%" onclick="insert('S');" />
							</div>
							<div class="button type2 f_left" style="margin-bottom:0;">
								<input type="button" value="저장 후 신규" id='btnInsertnew' width="100%" height="100%" onclick="insert('N');" />
							</div>
							<div class="button type3 f_left" style="margin-bottom:0;">
								<input type="button" value="삭제" id='btnRemove' width="100%" height="100%" onclick="remove();"/>
							</div>
						</div>
					</div>
					<div class="table  f_left" style="width:100%; margin:0; ">
						<form id="saveForm" name="saveForm">
							<input type="hidden" id="txtSearchyyyy" name="yyyy" value="${param.searchyear}"/>
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<tbody>
									<tr> 
										<th class="w15p"><div>지표코드<span class="th_must"></span></div></th>
										<td>
											<div class="cell">												
												<input type="text" value="${fn:escapeXml(detailKpi.MT_CD)}" id="txtMtCD" name="mtcd" class="input type2  f_left"  style="width:99%; margin:3px 0; "/>
											</div>
										</td>
										<th class="w15p"><div>지표명<span class="th_must"></span></div></th>
										<td>
											<div class="cell">
												<input type="hidden" value="${detailKpi.MT_ID}" id="txtMtid" name="mtid" />
												<input type="text" value="${fn:escapeXml(detailKpi.MT_NM)}" id="txtMtnm" name="mtnm" class="input type2  f_left"  style="width:99%; margin:3px 0; "/>
												<input type="hidden" id="txtScid" name="scid" value="100" /><!-- 대학지표 전용 화면  -->
												<input type="hidden" id="txtOrgscid" name="orgscid" value="100" />
											</div>
										</td>
									</tr>
									<tr>
										<th><div>산출공식<span class="th_must"></span></div></th>
										<td colspan="3">
											<div class="cell">
												<input type="hidden" id="hiddenCalculateList" name="calculateList" /> 

												<input type="hidden" id="txtCalcod" name="calcod" value="${detailKpi.CAL_COD}" />
												<input type="text" value="${detailKpi.CAL_NM}" id="txtCalnm" class="input type2  f_left"  style="width:95%; margin:3px 0; " readonly/>
												<div class="icon-search f_left m_t7 pointer" onclick="popupFormula();"></div>
											</div>
										</td>
									</tr>
									<tr>
										<th><div>전략<span class="th_must"></span></div></th>
										<td colspan="3">
											<div class="cell">
												<input type="hidden" value="${detailKpi.STRA_ID}" id="txtStraid" name="straid" />
												<input type="text" value="${detailKpi.STRA_NM}" id="txtStranm" name="stranm" class="input type2  f_left"  style="width:97%; margin:3px 0; " readonly/>
											</div>
										</td> 
									</tr>
									<tr>
										<th><div>세부전략<span class="th_must"></span></div></th>
										<td>
											<div class="cell">
												<input type="hidden" value="${detailKpi.SUBSTRA_ID}" id="txtSubstraid" name="substraid" />
												<input type="text" value="${detailKpi.SUBSTRA_NM}" id="txtSubstranm" name="substranm" class="input type2  f_left"  style="width:80%; margin:3px 0; " readonly/>
<!-- 												<div class="icon-search f_left m_t7 pointer" onclick="popupStraSubstra();"></div> -->
											</div>
										</td>
										<th><div>CSF<span class="th_must"></span></div></th>
										<td>
											<div class="cell">
												<input type="hidden" value="${detailKpi.CSF_ID}" id="txtCsfID" name="csfID" />
												<input type="text" value="${detailKpi.CSF_NM}" id="txtCsfNm" name="csfNm" class="input type2  f_left"  style="width:80%; margin:3px 0; " readonly/>
												<div class="icon-search f_left m_t7 pointer" onclick="popupStraSubstra();"></div>
											</div>
										</td>
									</tr>
									<tr>
										<th><div>지표책임자<span class="th_must"></span></div></th>
										<td class="w30p">  
											<div class="cell">
												<input type="hidden" value="${detailKpi.MT_USER_ID}" id="hiddenMtUserID" name="mtUserID" />
												<input type="hidden" value="${detailKpi.MT_USER_DEPT}" id="hiddenMtUserDept" name="mtUserDept" />
												<input type="text" value="${detailKpi.MT_USER_NM}" id="txtMtUserNM" name="mtUserNM" class="input type2  f_left"  style="width:80%; margin:3px 0; " readonly/>	
												<div class="icon-search f_left m_t7 pointer" onclick="popupKpiuser();"></div>											
											</div>
										</td>
										<th class="w15p"><div>관점<span class="th_must"></span></div></th>
										<td class="w30p">
											<div class="cell">
												<div class="combobox">
													<select id="cboPerscd" name="perscd">
														<c:choose>
															<c:when test="${listPers != null && not empty listPers}">
																<c:forEach items="${listPers}" var="key" varStatus="loop">
																	<option value="${key.COM_COD}" <c:if test="${key.COM_COD == detailKpi.PERS_CD}">selected</c:if>>${key.COM_NM}</option>
																</c:forEach>
															</c:when>
														</c:choose>
													</select>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<th><div>측정주기<span class="th_must"></span></div></th>
										<td class="w30p">
											<div class="cell">
												<div  class="combobox">
													<select id="cboMeascycle" name="meascycle">
														<c:choose>
															<c:when test="${listCycle != null && not empty listCycle}">
																<c:forEach items="${listCycle}" var="key" varStatus="loop">
																	<option value="${key.COM_COD}" <c:if test="${key.COM_COD == detailKpi.MEAS_CYCLE}">selected</c:if>>${key.COM_NM}</option>
																</c:forEach>
															</c:when>
														</c:choose>
													</select>
												</div>
											</div>
										</td>
										<th class="w15p"><div>측정단위<span class="th_must"></span></div></th>
										<td class="w30p">
											<div class="cell">
												<div  class="combobox">
													<select id="cboUnit" name="unit">
														<c:choose>
															<c:when test="${listUnit != null && not empty listUnit}">
																<c:forEach items="${listUnit}" var="key" varStatus="loop">
																	<option value="${key.COM_COD}" <c:if test="${key.COM_COD == detailKpi.UNIT}">selected</c:if>>${key.COM_NM}</option>
																</c:forEach>
															</c:when>
														</c:choose>
													</select>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<th><div>지표속성<span class="th_must"></span></div></th>
										<td class="w30p">
											<div class="cell">
												<div class="combobox">
													<select id="cboEvaltype" name="evaltype">
														<c:choose>
															<c:when test="${listEvaltyp1 != null && not empty listEvaltyp1}">
																<c:forEach items="${listEvaltyp1}" var="key" varStatus="loop">
																	<option value="${key.COM_COD}" <c:if test="${key.COM_COD == detailKpi.EVAL_TYPE}">selected</c:if>>${key.COM_NM}</option>
																</c:forEach>
															</c:when>
														</c:choose>
													</select>
												</div>
											</div>
										</td>
										<th class="w15p"><div>사용기간<span class="th_must"></span></div></th>
										<td class="w30p">
											<div class="cell">
												<input type="text" value="${detailKpi.START_DAT}" id="dateStartdate" name="startdat" class="input type2 t_center f_left"  style="width:200px; margin:3px 0;" onchange="checkStart();"/>
	<!-- 											<div class="icon-calendar f_left m_t7 pointer"></div> -->
												<div class="f_left m_l5 m_r5 m_t2">&sim;</div>
												<input type="text" value="${detailKpi.END_DAT}" id="dateEnddate" name="enddat" class="input type2 f_left t_center"  style="width:200px; margin:3px 0; "/>
	<!-- 											<div class="icon-calendar f_left m_t7 pointer"></div> -->
											</div>
										</td>
									</tr>
									<tr>
										<th><div>평가방법<span class="th_must"></span></div></th>
										<td class="w30p">
											<div class="cell">
												<div class="combobox">
													<select id="cboEvatyp" name="evatyp">
														<c:choose>
															<c:when test="${listEvatyp != null && not empty listEvatyp}">
																<c:forEach items="${listEvatyp}" var="key" varStatus="loop">
																	<option value="${key.COM_COD}" <c:if test="${key.COM_COD == detailKpi.EVA_TYP}">selected</c:if>>${key.COM_NM}</option>
																</c:forEach>
															</c:when>
														</c:choose>
													</select>
												</div>     
											</div>
										</td>
										<th class="w15p"><div>가감지표여부</div></th>
										<td class="w30p">
											<div class="cell">
												<div class="combobox">
													<select id="cboMtUdc3" name="mtudc3">
														<c:choose>
															<c:when test="${listMtudc3 != null && not empty listMtudc3}">
																<c:forEach items="${listMtudc3}" var="key" varStatus="loop"> 
																	<option value="${key.COM_COD}" <c:if test="${key.COM_COD == detailKpi.MT_UDC3}">selected</c:if>>${key.COM_NM}</option>
																</c:forEach>
															</c:when>
														</c:choose>
													</select>
												</div>
											</div>
										</td>
									</tr>
									<tr>
										<th><div>지표구분</div></th>
										<td class="w30p">
											<div class="cell">
												<div class="combobox">
													<select id="cboMtgbn" name="mtgbn">
														<c:choose>
															<c:when test="${listMtgbn != null && not empty listMtgbn}">
																<c:forEach items="${listMtgbn}" var="key" varStatus="loop">
																	<option value="${key.COM_COD}" <c:if test="${key.COM_COD == detailKpi.MT_GBN}">selected</c:if>>${key.COM_NM}</option>
																</c:forEach>
															</c:when>
														</c:choose>
													</select>
												</div>
											</div>
										</td>
										<th class="w15p"><div>소수자리수</div></th>
										<td class="w30p">
											<div class="cell">
												<input type="text" value="${detailKpi.DECIMAL_PLACES}" id="txtDecimalplaces" name="decimalplaces" class="input type2  f_left t_center"  style="width:30px; margin:3px 0; "/>
											</div>
										</td>
									</tr>
									<tr>
										<th><div>지표극성<span class="th_must"></span></div></th>
										<td class="w30p">
											<div class="cell">
												<div class="combobox">
													<select id="cboMtdir" name="mtdir">
														<c:choose>
															<c:when test="${listMtdir != null && not empty listMtdir}">
																<c:forEach items="${listMtdir}" var="key" varStatus="loop">
																	<option value="${key.COM_COD}" <c:if test="${key.COM_COD == detailKpi.MT_DIR}">selected</c:if>>${key.COM_NM}</option>
																</c:forEach>
															</c:when>
														</c:choose>
													</select>
												</div>
											</div>
										</td>
										<th><div>가중치<span class="th_must"></span></div></th>
										<td class="w30p">
											<div class="cell">
												<input type="text" value="${detailKpi.KPI_WEIGHT}" id="txtKpiWeight" name="kpiWeight" class="input type2  f_left t_center"  style="width:30px; margin:3px 0; "/>
											</div>
										</td>
									</tr>
 
									<tr>
										<th><div>세부내용</div></th>
										<td colspan="3">
											<div class="cell">
												<textarea id="txtareaMtdef" name="mtdef" class="textarea" style=" width:100%; height:65px; margin:3px 0;">${detailKpi.MT_DEF}</textarea>
											</div>
										</td>
									</tr>
									<tr>
										<th><div>고려사항</div></th>
										<td colspan="3">
											<div class="cell">
												<textarea id="txtareaTargetdesc" name="targetdesc" class="textarea" value="" style=" width:100%; height:65px; margin:3px 0;" >${detailKpi.TARGET_DESC}</textarea>
											</div>
										</td>
									</tr>
									<tr>
										<th><div>증빙자료</div></th>
										<td colspan="3">
											<div class="cell">
												<textarea id="txtareaMtdesc" name="mtdesc" class="textarea" value="" style=" width:100%; height:30px; margin:3px 0;" >${detailKpi.MT_DESC}</textarea>
											</div>
										</td>
									</tr>
									<tr>
										<th><div>보고시기</div></th>
										<td colspan="3">
											<div class="cell">
												<label for=""><input type="checkbox" name="mm" value="03" class="m_b2 m_r3">3월</label>
												<label for=""><input type="checkbox" name="mm" value="04" class="m_b2 m_r3 m_l5">4월</label>
												<label for=""><input type="checkbox" name="mm" value="05" class="m_b2 m_r3 m_l5">5월</label>
												<label for=""><input type="checkbox" name="mm" value="06" class="m_b2 m_r3 m_l5">6월</label>
												<label for=""><input type="checkbox" name="mm" value="07" class="m_b2 m_r3 m_l5">7월</label>
												<label for=""><input type="checkbox" name="mm" value="08" class="m_b2 m_r3 m_l5">8월</label>
												<label for=""><input type="checkbox" name="mm" value="09" class="m_b2 m_r3 m_l5">9월</label>
												<label for=""><input type="checkbox" name="mm" value="10" class="m_b2 m_r3 m_l5">10월</label>
												<label for=""><input type="checkbox" name="mm" value="11" class="m_b2 m_r3 m_l5">11월</label>
												<label for=""><input type="checkbox" name="mm" value="12" class="m_b2 m_r3 m_l5">12월</label>
												<label for=""><input type="checkbox" name="mm" value="01" class="m_b2 m_r3 m_l5">1월</label>
												<label for=""><input type="checkbox" name="mm" value="02" class="m_b2 m_r3 m_l5">2월</label>
											</div>
										</td>
									</tr>
									<tr >
										<th>   
											<div>
												유관업무  
												<img onClick="addRelatedWork()" width="16" title="추가" style="cursor:pointer;margin: 0px auto; padding: 5px 0px; text-align: center; display: inline-table;" alt="add" src="../../resources/cmresource/image/icon-plus.png">
											</div>
										</th>
										<td colspan="3"> 
											<table id="relatedWorkArea">
												<c:choose> 
														<c:when test="${relatedWork != null && not empty relatedWork}"> 
															<c:forEach items="${relatedWork}" var="row" varStatus="loop">																						
																<tr><td width="150px"><input type="text" name = "relatedWork1"   type="text"  class="input type2"  style="width:120px; margin:3px 0;" value="${row.RELATED_WORK1}"/>  <img onClick="removeRelatedWork(event)" width="16" title="삭제" style="cursor:pointer;margin: 0px auto; padding: 0px; text-align: right; float: right; display: inline-table;" alt="remove" src="../../resources/cmresource/image/icon-minus.png"> </td><td style="padding-left:3px;"><input type="text"  class="input type2"  name="relatedWork2" style="width:320px; margin:3px 0;"  value="${fn:escapeXml(row.RELATED_WORK2)}"/></td></tr>																
															</c:forEach>
														</c:when>
												</c:choose>
											</table> 
										</td>
									</tr> 
									
								</tbody>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>