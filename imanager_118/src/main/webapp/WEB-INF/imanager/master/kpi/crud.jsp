<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <title>지표관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" hrlef="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        	var reportIndex = null;
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$i.dialog.setAjaxEventPopup("SYSTEM","데이터 로딩중 입니다.");
        		$("#btnSearch").jqxButton({width:'', theme:'blueish'});
        		$("#btnInsertUniv").jqxButton({width:'', theme:'blueish'});
        		$("#btnInsertDept").jqxButton({width:'', theme:'blueish'});
        		$("#btnSearchkpi").jqxButton({width:'', theme:'blueish'});
        		$("#btnKpiDetail").jqxButton({width:'', theme:'blueish'});
        		$("#btnevalResult").jqxButton({width:'', theme:'blueish'});
        		$("#btnDeptSelect").jqxButton({width:'', theme:'blueish'}).on("click", function(event){
        			popupMappingKpiSust();
        		});
        		$("#btnValueWeightSave").jqxButton({width:'', theme:'blueish'});
        		$("#btnValueWeightSave").on("click",function(event){
        			/**
        			  컨트롤러 Argument
        			  String   yyyy					//년도
					  String   univMtID				//지표ID
					  String   univMtWeight		//지표 가중치
					  String[] scIDList					//부서SC_ID
					  String[] targetSustList			//학과SC_ID
					  String[] kpiCurrentAmtList		//가치형성비율
					**/
					var jsonString = $("#hiddenTargetSustList").val();
        			var jsonArray = []; 
        			if(jsonString!="") jsonArray = JSON.parse($("#hiddenTargetSustList").val());
        			
        			//var valueWeightProcess = $i.post("./insertValueWeight","#formValueWeight"); 
        			
					var valueWeightProcess = $i.post("./insertValueWeight",{ 
						yyyy:$("#cboSearchyear").val(),
						univMtID:$("#hiddenValueTargetMtID").val(),
						univMtWeight:$("#hiddenValueTargetMtWeight").val(),
						scIDList:Enumerable.From($("#tblValueDetail [name=scID]")).Select(function(c){ return c.value;}).ToArray(),
						targetSustList:Enumerable.From(jsonArray).Select(function(c){ return c.SC_ID;}).ToArray(),
						kpiCurrentAmtList:Enumerable.From($("#tblValueDetail [name=kpiCurrentAmt]")).Select(function(c){ return c.value;}).ToArray()						
					},true);        			
					
					valueWeightProcess.done(function(result){
						if(result.returnCode=="SUCCESS")
							$i.dialog.alert("SYSTEM",result.returnMessage);
						else
							$i.dialog.error("SYSTEM",result.returnMessage);
					});
        		});        
        		$("#btnTaregetreset").jqxButton({width:'', theme:'blueish'});
        		$("#btnTargetinsert").jqxButton({width:'', theme:'blueish'});
        		$("#btnTargetremove").jqxButton({width:'', theme:'blueish'});  
        		$("#btnInsertuser").jqxButton({width:'', theme:'blueish'});
        		$("#btnInsertweight").jqxButton({width:'', theme:'blueish'});
        		$("#btnInsertResult").jqxButton({width:'', theme:'blueish'});                 
        		$("#txtSearchkpinm").jqxInput({placeHolder: "", height: 20, width: 100, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return makeKpiList(); } });
        		$("#txtSearchTgtnm").jqxInput({placeHolder: "", height: 20, width: 100, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return  makeTab3Grid('','');} });   
        		$("#txtTargetSearchKpinm").jqxInput({placeHolder: "", height: 20, width: 100, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return  makeTab3Grid('','');} });   
        		$("#txtSearchChargenm").jqxInput({placeHolder: "", height: 20, width: 100, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return makeResultList(); } });   
        		$("#txtInputSearchKpinm").jqxInput({placeHolder: "", height: 20, width: 100, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return makeResultList(); } });   
        		$('#jqxTabs01').jqxTabs({ width: '100%', height: '100%', position: 'top', theme:'blueish', selectedItem: 0});    
            	$('#jqxTabs02').jqxTabs({ width: '100%', height: '100%', position: 'top', theme:'blueish', selectedItem: 0});
        		init(); 
        		setEvent();   
        		
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		var yearDat = $i.sqlhouse(13,{COML_COD:"YEARS"});
        		yearDat.done(function(res){
        			var source = 
        			{
        				datatype:"json",
        				datafields:[
        					{ name : "COM_COD" },
        					{ name : "COM_NM" }
        				],
        				id:"id",
        				localdata:res,
        				async : false
        			};
        			var dataAdapter = new $.jqx.dataAdapter(source);
        			$("#cboSearchyear").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,  theme:'blueish'});
        			changeYear();
        			search();   
        			makeRadio();
        		});
        	}
        	//조회 
        	function search(){
        		makeKpiList();
        		makeTree();
        		$('#jqxTabs02').jqxTabs('select', 0);
        		makeTab1Grid('','');      
        	}
        	//입력
        	function insert(){
        		$("#saveForm").submit();
        	}
        	//삭제
        	function remove(){    
        		if($("#txtStraid").val().trim()==""){
        			$i.dialog.warning("SYSTEM","전략목표를 선택하세요.");
        			return;
        		}
        		if($("#txtSubstraid").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","삭제 할 세부전략목표를 선택하세요");
        			return;
        		}
        		$i.remove("#saveForm").done(function(args){
        			if(args.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM",args.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM","삭제 되었습니다.");
        				search();
        				reset();
        			}
        		}).fail(function(e){
        			$i.dialog.error("SYSTEM","삭제 중 오류가 발생했습니다.");
        		});
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        		$("#btnInsert").on("click", function(event){
        			$("#tblInsert").toggle(); 
        		}); 
        		$('.num_only').css('imeMode','disabled').keyup(function(){
			        if( $(this).val() != null && $(this).val() != '' ) {
			            $(this).val($(this).val().replace(/[a-zA-Z]/gi,""));
			        }
			    });
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function changeYear(){
        		var resultMmDat = $i.sqlhouse(162,{S_YYYY:$("#cboSearchyear").val(), S_EVA_GBN:"${param.pEvaGbn}", S_GUBUN:"A"});
        		resultMmDat.done(function(res){
        			var source = 
        			{
        				datatype:"json",
        				datafields:[
        					{ name : "MM_ID"},
        					{ name : "MM_NM"}
        				],
        				id:"id",
        				localdata:res,
        				async : false
        			};
        			var dataAdapter = new $.jqx.dataAdapter(source);
        			$("#cboSearchResultMm").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "MM_NM", valueMember: "MM_ID", dropDownWidth: 100, dropDownHeight: 80, width: 100, height: 22,  theme:'blueish'});
        			$("#cboSearchResultMm").jqxComboBox({ selectedIndex: 0});
        		});
        	}
        	function labelChange(gubn){ 
        		if(gubn != null ){
        			$('#jqxTabs01').jqxTabs('select', gubn);
        		}
        		if(gubn == "0"){
        			var htmlShow = "";
        			var data = $i.sqlhouse(109,{ACCT_ID:"${paramAcctid}",SC_ID:$("#txtTabscid").val()});
        			data.done(function(res){
        				if(res.returnArray.length > 0){
        					for(var i=0;i<res.returnArray.length;i++){
	        					if(i == 0){
	        						htmlShow += res.returnArray[i].SC_NM;
	        					}else{
	        						htmlShow += "<span class='label sublabel type2'>"+res.returnArray[i].SC_NM+"</span>";
	        					}
	        				}
	        				$("#labelShow").html(htmlShow);
        				}
        			});
        		}else if(gubn == "1"){
        			var htmlShow = "";    
        			var data = $i.sqlhouse(221,{ACCT_ID:"${paramAcctid}",S_YYYY:$("#cboSearchyear").val(),S_C_ID:$("#txtTabscid").val()});        
        			data.done(function(res){    
        				if(res.returnArray.length > 0){
        					console.log("res.returnArray.length="+res.returnArray.length);  
        					for(var i=0;i<res.returnArray.length;i++){   
	        					if(i == 0){
	        						htmlShow += res.returnArray[i].STRA_NM;                                                             
	        					}else{                             
	        						htmlShow += "<span class='label sublabel type2'>"+res.returnArray[i].STRA_NM+"</span>";      
	        					}
	        				}
	        				$("#labelShow").html(htmlShow);
        				}
        			});
        		}else if(gubn == "2"){          
        			var htmlShow = ""; 
        			var data = $i.sqlhouse(110,{ACCT_ID:"${paramAcctid}",MT_ID:$("#txtTabscid").val()});
        			data.done(function(res){
        				if(res.returnArray.length > 0){
        					htmlShow += res.returnArray[0].MT_NM;
        					$("#labelShow").html(htmlShow);
        				}
        			});
        		}else{
        			
        		}
        	}
        	function makeTree(){
        		var dat = $i.select("#searchForm");
        		dat.done(function(res){
        			var source =
					{
		                datatype: "json",
		                datafields: [
		                    { name: 'SC_ID'},
		                    { name: 'PSC_ID'},
		                    { name: 'SC_NM'}
		                ],
		                id : 'SC_ID',
		                localdata: res
		            };
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            dataAdapter.dataBind();
		            
		            var records = dataAdapter.getRecordsHierarchy('SC_ID', 'PSC_ID', 'items', [{ name: 'SC_ID', map: 'id'} ,{ name: 'SC_NM', map: 'label'}]);
		            $('#treeScidlist').jqxTree({source: records, height: '100%', width: '100%', theme:'blueish' });
		            
					// tree init
		            var items = $('#treeScidlist').jqxTree('getItems');
		            var item = null;
		            var size = 0;
		            var img = '';
		            var label = '';
		            var afterLabel = '';
		            
					for(var i = 0; i < items.length; i++) {
						item = items[i];
						
						if(i == 0) {
							//root
							$("#treeScidlist").jqxTree('expandItem', item);
							img = 'icon-foldernoopen.png';
							afterLabel = "";
						} else {
							//children
							size = $(item.element).find('li').size();
							
							if(size > 0) {
								//have a child
								img = 'icon-foldernoopen.png';
								afterLabel = "<span style='color: Blue;'> (" + size + ")</span>";
							} else {
								//no have a child
								img = 'icon-page.png';
								//내부에서 사용하는 트리는 팝업 없음.
								//afterLabel = "<span style='color: Blue; margin-left:5px; vertical-align:middle;'> <img src='${WWW.CSS}/images/img_icons/popupIcon.png' alt='팝업창' title='새창보기'/></span>";
								afterLabel = "";
							}
						}
						
						label = "<img style='float: left; margin-right: 5px;' src='../../resources/cmresource/image/" + img + "'/><span item-title='truec style='vertical-align:middle;'>" + item.label + "</span>" + afterLabel;
						$('#treeScidlist').jqxTree('updateItem', item, { label: label});
					}      
		               
		            //add event
		            $('#treeScidlist')
		            .on("expand", function(eve) {   
		            	var args = eve.args;
		            	var label = $('#treeScidlist').jqxTree('getItem', args.element).label.replace('icon-foldernoopen', 'icon-folderopen');
		            	args.element.firstChild.className = args.element.firstChild.className.replace('jqx-tree-item-arrow-collapse', '').replace('jqx-icon-arrow-right', '');
		            	$('#treeScidlist').jqxTree('updateItem', args.element, { label: label});
		            })
		            .on("collapse", function(eve) {
		            	var args = eve.args;
		            	var label = $('#treeScidlist').jqxTree('getItem', args.element).label.replace('icon-folderopen', 'icon-foldernoopen');
		            	$('#treeScidlist').jqxTree('updateItem', args.element, { label: label});       
		            })
		            .on("select", function(eve) {
		            	$("#txtTabscid").val($('#treeScidlist').jqxTree('getItem', args.element).id);
		            	$("#txtGubn").val("0");
		            	makeTab1Grid('0', $('#treeScidlist').jqxTree('getItem', args.element).id); 
		            	$("#jqxTabs02").find("li").eq($("#jqxTabs02").jqxTabs("selectedItem")).trigger("click");
		            	labelChange(0);
		            });
		            $("#treeScidlist").jqxTree("expandAll");
        		});
        	}
        	function makeKpiList(){
        		var dat = $i.post("./selectKpi",{searchyear:$("#cboSearchyear").val(), searchnm:$("#txtSearchkpinm").val()});
        		dat.done(function(data){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'MT_ID', type: 'string' },
		                    { name: 'MT_CD', type: 'string' },
		                    { name: 'MT_NM', type: 'string' }
		                ],
		                localdata: data,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
                  	};
					var alginLeftLink = function (row, columnfield, value,defaultHtml,property,rowdata) {//left정렬  
						return "<div id='userName-" + row + "' onclick ='listboxKpiClick("+JSON.stringify(rowdata)+");' style='text-align:left; margin:4px 0px 0px 10px;text-decoration:underline;' >" + value + "</div>";
					}; 
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            try{
			            $("#listboxKpi").jqxGrid({ source: dataAdapter,
			            rowsHeight:25, 
			            pagesize: 100,
			            pageable: true,
			            pageSizeOptions:['100','200','300'],
			            sortable: true,
			            columns:[
			            	{  datafield: 'MT_ID', hidden:true},
			            	{ text: '지표코드', datafield: 'MT_CD', width: '30%', align:'center', cellsalign: 'left'}, 
					  		{ text: '지표명', datafield: 'MT_NM', width: '70%', align:'center', cellsalign: 'left', cellsrenderer:alginLeftLink}
			            ],width: '100%',  height: 485, theme:"blueish"});
		            }catch(e){
		            	console.log(e);
		            }
		            
        		});
        	}

        	function listboxKpiClick(rowdata){ 
					    $("#txtTabscid").val(rowdata.MT_ID);
		            	$("#txtGubn").val("1");
					    //makeTab1Grid('1', rowdata.MT_ID); 			    
					    $("#jqxTabs02").find("li").eq($("#jqxTabs02").jqxTabs("selectedItem")).trigger("click");  
					    labelChange(2);   
        	}
        	function tabClack2(){
        		makeTab3Grid('','');
        		makeTab3DetailTargetList('','','','','','');   
        	}
        	function makeStraListTree(){
        		var dat = $i.sqlhouse(195, {S_YYYY:$("#cboSearchyear").val()});
        		dat.done(function(res){
        			var source =
					{
		                datatype: "json",
		                datafields: [
		                    { name: 'P_ID'},
		                    { name: 'C_ID'},
		                    { name: 'STRA_NM'},
		                    { name: 'STRA_ID'},
		                    { name: 'SUBSTRA_ID'}
		                ],
		                id : 'C_ID',
		                localdata: res
		            };
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            dataAdapter.dataBind();
		            
		            var records = dataAdapter.getRecordsHierarchy('C_ID', 'P_ID', 'items', [{ name: 'C_ID', map: 'id'} ,{ name: 'STRA_NM', map: 'label'}, { name:'STRA_ID', map:'value'}]);
		            $('#treeStraList').jqxTree({source: records, height: '100%', width: '100%', theme:'blueish' });
		            
					// tree init
		            var items = $('#treeStraList').jqxTree('getItems');
		            var item = null;
		            var size = 0;
		            var img = '';
		            var label = '';
		            var afterLabel = '';
		            
					for(var i = 0; i < items.length; i++) {
						item = items[i];
						
						if(i == 0) {
							//root
							$("#treeStraList").jqxTree('expandItem', item);
							img = 'icon-foldernoopen.png';
							afterLabel = "";
						} else {
							//children
							size = $(item.element).find('li').size();
							
							if(size > 0) {
								//have a child
								img = 'icon-foldernoopen.png';
								afterLabel = "<span style='color: Blue;'> (" + size + ")</span>";
							} else {
								//no have a child
								img = 'icon-page.png';
								//내부에서 사용하는 트리는 팝업 없음.
								//afterLabel = "<span style='color: Blue; margin-left:5px; vertical-align:middle;'> <img src='${WWW.CSS}/images/img_icons/popupIcon.png' alt='팝업창' title='새창보기'/></span>";
								afterLabel = "";
							}
						}
						
						label = "<img style='float: left; margin-right: 5px;' src='../../resources/cmresource/image/" + img + "'/><span item-title='truec style='vertical-align:middle;'>" + item.label + "</span>" + afterLabel;
						$('#treeStraList').jqxTree('updateItem', item, { label: label});
					}
		            
		            //add event
		            $('#treeStraList')
		            .on("expand", function(eve) {
		            	var args = eve.args;
		            	var label = $('#treeStraList').jqxTree('getItem', args.element).label.replace('icon-foldernoopen', 'icon-folderopen');
		            	args.element.firstChild.className = args.element.firstChild.className.replace('jqx-tree-item-arrow-collapse', '').replace('jqx-icon-arrow-right', '');
		            	$('#treeStraList').jqxTree('updateItem', args.element, { label: label});
		            })
		            .on("collapse", function(eve) {
		            	var args = eve.args;
		            	var label = $('#txtTabSubstr').jqxTree('getItem', args.element).label.replace('icon-folderopen', 'icon-foldernoopen');
		            	$('#txtTabSubstr').jqxTree('updateItem', args.element, { label: label});
		            })       
		            .on("select", function(eve) {
		            	$("#txtTabscid").val($('#treeStraList').jqxTree('getItem', args.element).id);
		            	$("#txtTabSubstr").val($("#treeStraList").jqxTree("getItem", args.element).id);
		            	$("#txtGubn").val("2");
		            	//makeTab1Grid('2', $('#treeStraList').jqxTree('getItem', args.element).parentId); 
		            	$("#jqxTabs02").find("li").eq($("#jqxTabs02").jqxTabs("selectedItem")).trigger("click");
		            	labelChange(1);
		            });
		            //$("#treeStraList").jqxTree("expandAll");
        		});
        	}
        	function makeTab1Grid(gubn, scid){
        		if(scid == ""){
        			scid = $("#txtTabscid").val();
        		}  
        		if(gubn == ""){
        			gubn = $("#txtGubn").val();
        		}      
        		var date = $i.post("./selectKpilist",{gubn:gubn, scid:scid, yyyy:$("#cboSearchyear").val(),tgtusernm:$("#txtSearchTgtnm").val(),tgtchangenm:$("#txtSearchTgtnm").val(),kpichangenm:""});
        		date.done(function(res){
		            // prepare the data     
		            var source = 
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'SC_ID', type: 'string'},
		                    { name: 'SC_NM', type: 'string'},
		                    { name: 'MT_ID', type: 'string'}, 
		                    { name: 'MT_CD', type: 'string'}, 
							{ name: 'KPI_ID', type: 'string'},
							{ name: 'P_KPI_ID', type: 'string'}, 
		                    { name: 'KPI_NM', type: 'string'},
							{ name: 'MEAS_CYCLE', type: 'string'},
							{ name: 'MEAS_CYCLE_NM', type: 'string'},
							{ name: 'KPI_CHARGE_ID', type: 'string'},
							{ name: 'KPI_CHARGE_NM', type: 'string'},
							{ name: 'OWNER_USER_ID', type: 'string'},
							{ name: 'OWNER_USER_NM', type: 'string'},
							{ name: 'ACCT_ID', type: 'string'}
		                ],
		                hierarchy:{ 
		                	keyDataField:{name:"KPI_ID"},
		                	parentDataField:{name:"P_KPI_ID"}
		                },	
		                id:"KPI_ID", 
		                localdata: res 
		            };
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 5px;">' + value + '</div>';
					};
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 5px 0px 0px;">' + value + '</div>';
					};
					var detailRow = function (row, columnfield, value, rowData) {//그리드 선택시 하단 상세
						var scid = rowData.SC_ID;
						var mtid = rowData.MT_ID;  
						var acctId = rowData.ACCT_ID;
						
						var link = "<a href=\"javascript:getRowDetail('"+scid+"','"+mtid+"','"+acctId+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
						
					}; 
		            var dataAdapter = new $.jqx.dataAdapter(source); 
		            $("#gridKpilist").jqxTreeGrid(  
		            {
	              	 	width: '100%',
		                height:'100%',
						altRows:true, 
						pageable: true,    
						pagerMode: 'advanced',
						pageSizeOptions:['100','200','300'],
		                source: dataAdapter, 
						theme:'blueish',
						sortable: true, 
		                columnsResize: true, 
						columnsHeight: 25,
						pageSize: 100,
		                columns: [    
		                          { text: '조직ID', datafield: 'SC_ID', width: '10%', align:'center', cellsalign: 'center'},
				                  { text: '조직명', datafield: 'SC_NM', width: '20%', align:'center', cellsalign: 'left'}, 
								  { text: '지표코드', datafield: 'MT_CD', width: '10%', align:'center', cellsalign: 'center'}, 
								  { text: '지표명', datafield: 'KPI_NM', width: '20%', align:'center', cellsalign: 'left' ,  cellsrenderer: detailRow},
								  { text: '측정주기', datafield: 'MEAS_CYCLE_NM', width: '10%', align:'center', cellsalign: 'center' },
								  { text: '입력자', datafield: 'KPI_CHARGE_NM', width: '15%', align:'center', cellsalign: 'center'  },
								  { text: '책임자', datafield: 'OWNER_USER_NM', width: '15%', align:'center', cellsalign: 'center'  }
		                ]
		            });  
		            //setPagerLayout('gridKpilist'); //페이저 레이아웃
        		});
        	}
    		function setPagerLayout(selector) {
    			
    			var pagesize = $('#'+selector).jqxGrid('pagesize');
    			
    			var w = 49; 
    				
    			if(pagesize<100) {
    				w = 44;
    			} else if(pagesize>99&&pagesize<1000) {
    				w = 49;
    			} else if(pagesize>999&&pagesize<10000) {
    				w = 54;
    			}
    			
    			//디폴트 셋팅
    			$('#gridpagerlist'+selector).jqxDropDownList({ width: w+'px' });
    			
    			//체인지 이벤트 처리
    			$('#'+selector).on("pagesizechanged", function (event) {
    				var args = event.args;
    				
    				if(args.pagesize<100) {
    					$('#gridpagerlist'+selector).jqxDropDownList({ width: '44px' });
    				} else if(args.pagesize>99 && args.pagesize<1000) {
    					$('#gridpagerlist'+selector).jqxDropDownList({ width: '49px' });
    				} else if(args.pagesize>999 && args.pagesize<10000) {
    					$('#gridpagerlist'+selector).jqxDropDownList({ width: '54px' });
    				} else {
    					$('#gridpagerlist'+selector).jqxDropDownList({ width: 'auto' });
    				}
    			});
    		}
        	function getRowDetail(scid, mtid, acctid){
        		//location.replace("./detail?acctid=${paramAcctid}&scid="+scid+"&mtid="+mtid+"&gubn="+$("#txtGubn").val()+"&searchyear="+$("#cboSearchyear").val());
        		var programid = "";
        		if(scid=="100") programid = "detailOfUniv";
        		else programid = "detailOfDept";
        		
        		var url = "./"+programid+"?acctid=${paramAcctid}&scid="+scid+"&mtid="+mtid+"&gubn="+$("#txtGubn").val()+"&searchyear="+$("#cboSearchyear").val();
        		var kpiForm = $("<iframe data-role='kpiform' src='"+url+"'style='width:100%;height:100%;position:absolute;left:0px;border-style:none;z-index:99999;'></iframe>").appendTo("body"); 
        	}
        	function getReportDetail(){
        		reportIndex = $('#gridKpiReportList').jqxGrid('getselectedrowindex');
        		var data = $('#gridKpiReportList').jqxGrid('getrowdata', $('#gridKpiReportList').jqxGrid('getselectedrowindex'));
        		var scid = data.SC_ID;
        		var mtid = data.MT_ID;
        		var programid = "";
        		if(scid=="100") programid = "detailOfUniv";
        		else programid = "detailOfDept";
        		
        		var url = "./"+programid+"?acctid=${paramAcctid}&scid="+scid+"&mtid="+mtid+"&gubn="+$("#txtGubn").val()+"&searchyear="+$("#cboSearchyear").val()+"&grdList="+$('#gridKpiReportList').jqxGrid('getselectedrowindex');
        		var kpiForm = $("<iframe data-role='kpiform' src='"+url+"'style='width:100%;height:100%;position:absolute;left:0px;border-style:none;z-index:99999;'></iframe>").appendTo("body");
        	}
        	function closeKpiForm(flag){
        		$("[data-role=kpiform]").remove(); 
        		
        		//지표 삭제이고 현재 지표기준으로 보고 있을 경우.. 왼쪽 탭 refresh
        		if(flag!=null&&flag=="D"&&$('#jqxTabs01').jqxTabs('selectedItem')!=0){
        			$("#labelShow").html(""); 
    				makeKpiList();
        		}
        		//지표저장 폼 닫기 로직 공통 (오른쪽 탭 refresh)
        		makeKpiList();
        		if(flag == "G"){
        			makeKpiReport();
//         			$('#gridKpiReportList').jqxGrid('selectrow', reportIndex);
//         			getDetailKpiReport($("#gridKpiReportList").jqxGrid('getrowdata', reportIndex));
        		}else{
        			$("#jqxTabs02").find("li").eq($("#jqxTabs02").jqxTabs("selectedItem")).trigger("click");
        		}
        	}
        	function makeTab3Grid(gubn, scid){
        		if(scid == ""){
        			scid = $("#txtTabscid").val();
        		}
        		if(gubn == ""){
        			gubn = $("#txtGubn").val();
        		}
//         		var date = $i.post("./selectKpilist",{gubn:gubn, scid:scid, yyyy:$("#cboSearchyear").val(),tgtusernm:$("#txtSearchTgtnm").val(),tgtchangenm:$("#txtSearchTgtnm").val(),kpichangenm:$("#txtTargetSearchKpinm").val()});
        		var date = $i.sqlhouse(270,{S_GUBN:gubn, S_SC_ID:scid, S_YYYY:$("#cboSearchyear").val(),S_TGT_USER_NM:$("#txtSearchTgtnm").val(),S_TGT_CHARGE_NM:$("#txtSearchTgtnm").val(),S_KPI_NM:$("#txtTargetSearchKpinm").val()});
        		date.done(function(res){  
		            // prepare the data   
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'ACCT_ID', type: 'string'},
		                    { name: 'SC_ID', type: 'string'},
		                    { name: 'SC_NM', type: 'string'},
							{ name: 'KPI_ID', type: 'string'},
							{ name: 'MT_CD', type: 'string'},
		                    { name: 'KPI_NM', type: 'string'},
		                    { name: 'TGT_USER_NM', type: 'string'},
		                    { name: 'TGT_CHARGE_NM', type: 'string'},
							{ name: 'UNIT_NM', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 5px;">' + value + '</div>';
					};
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 5px 0px 0px;">' + value + '</div>';
					};
					var targetDetail = function (row, columnfield, value) {//그리드 선택시 하단 상세
						var acctid = $("#gridKpitarget").jqxGrid("getrowdata", row).ACCT_ID;
						var scid = $("#gridKpitarget").jqxGrid("getrowdata", row).SC_ID;
						var scnm = $("#gridKpitarget").jqxGrid("getrowdata", row).SC_NM;
						var mtcd = $("#gridKpitarget").jqxGrid("getrowdata", row).MT_CD;
						var kpiid = $("#gridKpitarget").jqxGrid("getrowdata", row).KPI_ID;
						var kpinm = $("#gridKpitarget").jqxGrid("getrowdata", row).KPI_NM; 
						
						var link = "<a href=\"javascript:makeTab3DetailTargetList('"+acctid+"','"+scid+"','"+scnm+"','"+mtcd+"','"+kpiid+"','"+escape(kpinm)+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
						
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }      
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridKpitarget").jqxGrid(
		            {
	              	 	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: true,
						pagesizeoptions:['100','200','300'],
		                source: dataAdapter,
						theme:'blueish',
						sortable: true,
		                columnsresize: true,
						columnsheight: 25,
						rowsheight: 25,
						pagesize: 100,
		                columns: [        
		                   { text: '조직ID', datafield: 'SC_ID', width: '8%', align:'center', cellsalign: 'center'},
		                   { text: '조직명', datafield: 'SC_NM', width: '22%', align:'center', cellsalign: 'left',  cellsrenderer: targetDetail },
						   { text: '지표코드', datafield: 'MT_CD', width: '14%', align:'center', cellsalign: 'center'},
						   { text: '지표명', datafield: 'KPI_NM', width: '26%', align:'center', cellsalign: 'left', cellsrenderer: targetDetail },
 						   { text: '입력담당자', datafield: 'TGT_CHARGE_NM', width: '11%', align:'center', cellsalign: 'center'},
						   { text: '승인담당자', datafield: 'TGT_USER_NM', width: '11%', align:'center', cellsalign: 'center'},   
						   { text: '단위', datafield: 'UNIT_NM', width: '8%', align:'center', cellsalign: 'center' }                                 
		                ]     
		            });
		            setPagerLayout('gridKpitarget'); //페이저 레이아웃         
        		});
        	}
        	function makeTab3DetailTargetList(acctid, scid, scnm, mtcd, kpiid, kpinm){
        		kpinm = unescape(kpinm);        
        		$("#txtTargetacctid").val(acctid);
        		$("#txtTargetscid").val(scid);
        		$("#txtTargetscnm").val(scnm);
        		$("#txtTargetmtcd").val(mtcd);
        		$("#txtTargetkpiid").val(kpiid);
        		$("#txtTargetkpinm").val(kpinm);
        		$("#targetScid").html(scid+"/"+scnm);
        		$("#targetMtcd").html(mtcd+"/"+kpinm);  
        		var date = $i.sqlhouse(81,{ACCT_ID:acctid,KPI_ID:kpiid, MT_CD:mtcd});
        		date.done(function(res){
        			var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'ACCT_ID', type: 'string'},
		                    { name: 'YYYY', type: 'string'},
		                    { name: 'KPI_ID', type: 'string'},
		                    { name: 'MT_CD', type: 'string'},
							{ name: 'TARGET_AMT', type: 'string'},
		                    { name: 'TARGET_CURRAMT', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 5px;">' + value + '</div>';
					};
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 5px 0px 0px;">' + value + '</div>';
					};
					var detail = function (row, columnfield, value) {
						var acctid = $("#gridTargetamt").jqxGrid("getrowdata", row).ACCT_ID;
						var kpiid = $("#gridTargetamt").jqxGrid("getrowdata", row).KPI_ID;
						var yyyy = $("#gridTargetamt").jqxGrid("getrowdata", row).YYYY;
						var mm = $("#gridTargetamt").jqxGrid("getrowdata", row).MM;
						
						var link = "<a href=\"javascript:detailTargetValue('"+acctid+"','"+kpiid+"','"+yyyy+"','"+mm+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:center; margin:4px 0px 0px 0px;'>" + link + "</div>";
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridTargetamt").jqxGrid(
		            {
	              	 	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
						sortable: true,
		                columnsresize: true,
						columnsheight: 25,
						rowsheight: 25,
						pagesize: 100,
		                columns: [
				                   { text: '년도', datafield: 'YYYY', width: '20%', align:'center', cellsalign: 'center' ,  cellsrenderer: detail },
				                   { text: '목표', datafield: 'TARGET_AMT', width:'40%', align:'center', cellsalign: 'center'},
				                   { text: '기준값', datafield: 'TARGET_CURRAMT', width:'40%', align:'center', cellsalign:'center'}
		                ]
		            });
        		});
        	}
        	function detailTargetValue(acctid, kpiid, yyyy, mm){
        		var data = $i.sqlhouse(102,{ACCT_ID:acctid,KPI_ID:kpiid,YYYY:yyyy,MM:mm,EVA_GBN:"B"});
        		data.done(function(res){
        			if(res.returnArray.length > 0){
        				$("#txtTargetorgyyyy").val(res.returnArray[0].YYYY);
        				$("#txtTargetyyyy").val(res.returnArray[0].YYYY);
        				$("#txtTargetamt").val(res.returnArray[0].TARGET_AMT);
        				$("#txtTargetcurramt").val(res.returnArray[0].TARGET_CURRAMT);
        			}
        		});
        	}
        	function makeMappingKpiuserlist(){
    			if($("#mappingKpiuserlist > tr").length > 0){
    				$("#mappingKpiuserlist > tr").remove(); 
    			}
        		var acctid = "${paramAcctid}";
        		var data = $i.sqlhouse(82,{ACCT_ID:acctid,YYYY:$("#cboSearchyear").val(),SC_ID:$("#txtTabscid").val(),GUBN:$("#txtGubn").val(),SUBSTRA_ID:$("#txtTabSubstr").val()});
        		data.done(function(res){    
        			var html = "";
        			if(res.returnArray.length > 0){
        				$("#mappingKpiuserlist > tr").remove();
	        			for(var i=0;i<res.returnArray.length;i++){
	        				html += '<tr>';
	        				html += '<td style="width:5%">';
	        				html += '<div class="cell t_center">';
	        				html += '<input type="hidden" name="userscid" value="'+res.returnArray[i].SC_ID+'" />'+res.returnArray[i].SC_ID;
	        				html += '</div>';
	        				html += '</td>';
	        				html += '<td style="width:15%"><div class="cell t_left">'+res.returnArray[i].SC_NM+'</div></td>';
	        				html += '<td style="width:8%">';
	        				html += '<div class="cell t_center">';   
	        				html += '<input type="hidden" name="userkpiid" value="'+res.returnArray[i].KPI_ID+'"/>'+res.returnArray[i].MT_CD;
	        				html += '</div>';     
	        				html += '</td>';  
	        				html += '<td style="width:16%"><div class="cell t_left">'+res.returnArray[i].KPI_NM+'</div></td>';
	        				html += '<td style="width:11%;min-width:97px;">';
	        				html += '<input type="hidden" data-scid="'+res.returnArray[i].SC_ID+'" data-kpiid="'+res.returnArray[i].KPI_ID+'" name="kpichargeid" value="'+res.returnArray[i].KPI_CHARGE_ID+'" />';
	        				html += '<input type="text" data-scid="'+res.returnArray[i].SC_ID+'" data-kpiid="'+res.returnArray[i].KPI_ID+'" class="input type2 t_center" style="width:55%;" name="kpichargenm" value="'+res.returnArray[i].KPI_CHARGE_NM+'" readonly/>';
	        				html += "<div class='cell icon-search f_right pointer' style='width:16px;margin-top:7px;' onclick=\"popupMappingKpiUser('"+res.returnArray[i].ACCT_ID+"','"+res.returnArray[i].SC_ID+"','"+res.returnArray[i].KPI_ID+"','kpicharge');\"></div>";
	        				html += '</td>';
	        				html += '<td style="width:11%;min-width:97px;">';
	        				html += '<input type="hidden" data-scid="'+res.returnArray[i].SC_ID+'" data-kpiid="'+res.returnArray[i].KPI_ID+'" name="owneruserid" value="'+res.returnArray[i].OWNER_USER_ID+'" />';
	        				html += '<input type="text" data-scid="'+res.returnArray[i].SC_ID+'" data-kpiid="'+res.returnArray[i].KPI_ID+'" class="input type2 t_center" style="width:55%;" name="ownerusernm" value="'+res.returnArray[i].OWNER_USER_NM+'" readonly/>';
	        				html += "<div class='cell icon-search f_right pointer' style='width:16px;margin-top:7px;' onclick=\"popupMappingKpiUser('"+res.returnArray[i].ACCT_ID+"','"+res.returnArray[i].SC_ID+"','"+res.returnArray[i].KPI_ID+"','owneruser');\"></div>";
	        				html += '</td>';
	        				html += '<td style="width:11%;min-width:97px;">';
	        				html += '<input type="hidden" data-scid="'+res.returnArray[i].SC_ID+'" data-kpiid="'+res.returnArray[i].KPI_ID+'" name="tgtchargeid" value="'+res.returnArray[i].TGT_CHARGE_ID+'" />';
	        				html += '<input type="text" data-scid="'+res.returnArray[i].SC_ID+'" data-kpiid="'+res.returnArray[i].KPI_ID+'" class="input type2 t_center" style="width:55%;" name="tgtchargenm" value="'+res.returnArray[i].TGT_CHARGE_NM+'" readonly />';
	        				html += "<div class='cell icon-search f_right pointer' style='width:16px;margin-top:7px;' onclick=\"popupMappingKpiUser('"+res.returnArray[i].ACCT_ID+"','"+res.returnArray[i].SC_ID+"','"+res.returnArray[i].KPI_ID+"','tgtcharge');\"></div>";
	        				html += '</td>';
	        				html += '<td style="width:11%;min-width:97px;">';            
	        				html += '<input type="hidden" data-scid="'+res.returnArray[i].SC_ID+'" data-kpiid="'+res.returnArray[i].KPI_ID+'" name="tgtuserid" value="'+res.returnArray[i].TGT_USER_ID+'" />';
	        				html += '<input type="text" data-scid="'+res.returnArray[i].SC_ID+'" data-kpiid="'+res.returnArray[i].KPI_ID+'" class="input type2 t_center" style="width:55%;" name="tgtusernm" value="'+res.returnArray[i].TGT_USER_NM+'" readonly/>';
	        				html += "<div class='cell icon-search f_right pointer' style='width:16px;margin-top:7px;' onclick=\"popupMappingKpiUser('"+res.returnArray[i].ACCT_ID+"','"+res.returnArray[i].SC_ID+"','"+res.returnArray[i].KPI_ID+"','tgtuser');\"></div>";
	        				html += '</td>';
	        				html += '<td style="width:11%;min-width:97px;">';            
	        				html += '<input type="hidden" data-scid="'+res.returnArray[i].SC_ID+'" data-kpiid="'+res.returnArray[i].KPI_ID+'" name="empid" value="'+res.returnArray[i].EMP_ID+'" />';
	        				html += '<input type="text" data-scid="'+res.returnArray[i].SC_ID+'" data-kpiid="'+res.returnArray[i].KPI_ID+'" class="input type2 t_center" style="width:55%;" name="empusernm" value="'+res.returnArray[i].EMP_NM+'" readonly/>';
	        				html += "<div class='cell icon-search f_right pointer' style='width:16px;margin-top:7px;' onclick=\"popupMappingKpiUser('"+res.returnArray[i].ACCT_ID+"','"+res.returnArray[i].SC_ID+"','"+res.returnArray[i].KPI_ID+"','empuser');\"></div>";
	        				html += '</td>';
	        				html += '</tr>';
	        			}
        			}else{
        				$("#mappingKpiuserlist > tr").remove();
        			}
        			$("#mappingKpiuserlist").append(html);
        		});
        	}
        	function mappingKpiweightlist(){
        		var acctid = "${paramAcctid}";
        		var data = $i.sqlhouse(83,{ACCT_ID:acctid,YYYY:$("#cboSearchyear").val(),SC_ID:$("#txtTabscid").val(),GUBN:$("#txtGubn").val(),substr:$("#txtTabSubstr").val()});
        		data.done(function(res){    
        			var html = "";
        			if(res.returnArray.length > 0){
        				$("#mappingKpiweightlist > tr").remove();
	        			for(var i=0;i<res.returnArray.length;i++){
	        				html += '<tr>';
	        				html += '<td>';
	        				html += '<div class="cell t_center">';
	        				html += '<input type="hidden" name="weightscid" value="'+res.returnArray[i].SC_ID+'" />';
	        				html += '<input type="hidden" name="weightmtid" value="'+res.returnArray[i].MT_ID+'" />'+res.returnArray[i].SC_ID;
	        				html += '</div>';
	        				html += '</td>';
	        				html += '<td><div class="cell t_left">'+res.returnArray[i].SC_NM+'</div></td>';
	        				html += '<td>';
	        				html += '<div class="cell t_center">';
	        				html += '<input type="hidden" name="weightkpiid" value="'+res.returnArray[i].KPI_ID+'"/>'+res.returnArray[i].KPI_ID;
	        				html += '</div>';
	        				html += '</td>';
	        				html += '<td><div class="cell t_left">'+res.returnArray[i].KPI_NM+'</div></td>';
	        				html += '<td>'; 
	        				html += '<div class="cell">';
	        				html += '<input type="text" class="input type2 t_right num_only" style="width:90%;" name="kpiweight" value="'+res.returnArray[i].KPI_WEIGHT+'" />';
	        				html += '</div>';
	        				html += '</td>';
	        				html += '<td>';
	        				html += '<div class="cell">';
	        				html += '<input type="text" class="input type2 t_right num_only" style="width:90%;" name="kpicurramt" value="'+res.returnArray[i].KPI_CURRAMT+'" />';
	        				html += '</div>';
	        				html += '</td>';
	        				html += '</tr>';
	        			}
        			}else{
        				$("#mappingKpiweightlist > tr").remove();
        			}
        			$("#mappingKpiweightlist").append(html);
        			setEvent();
        		});
        	}
        	function makeResultList(){
        		var pEvaGbn = "${param.pEvaGbn}";
        		var data = $i.sqlhouse(158,{S_EVA_GBN:pEvaGbn, S_YYYY:$("#cboSearchyear").val(),S_MM:$("#cboSearchResultMm").val()});
        		data.done(function(c){
        			if(c.returnCode=="SUCCESS"){    
        				if(c.returnArray.length > 0){
        					if(c.returnArray[0].CHECK_DAT == "1"){
	        					$("#messageLayer").html("실적입력기한이 종료되었습니다.");
	        					$("#btnInsertResult").hide();
	        				}else if(c.returnArray[0].CHECK_DAT == "2"){
	        					$("#messageLayer").html("실적입력기한 및 확인기한이 종료되었습니다.");
	        					$("#btnInsertResult").hide();
	        				}else if(c.returnArray[0].CHECK_DAT == "0"){
	        					$("#messageLayer").html("");
	        					$("#btnInsertResult").show();
	        				}
        				}else{
        					$("#messageLayer").html("입력기간을 설정해주세요");
        					$("#btnInsertResult").hide();
        				}
        			}else{
        				$("#messageLayer").html("입력기간을 설정해주세요");
        				$("#btnInsertResult").hide();
        			}
        		});
        		var data = $i.sqlhouse(179,{S_TOP_YYYY:$("#cboSearchyear").val(),S_SC_ID:$("#txtTabscid").val(),S_GUBN:$("#txtGubn").val(),S_EVA_GBN:"${param.pEvaGbn}",S_YYYY:$("#cboSearchyear").val(), S_MM:$("#cboSearchResultMm").val(), S_MT_ID:' ', S_SUBSTRA_ID:$("#txtTabSubstr").val(),S_OWNER_USER_NM:$("#txtSearchChargenm").val(),S_KPI_CHARGE_NM:$("#txtSearchChargenm").val(),S_KPI_NM:$("#txtInputSearchKpinm").val()});
        		data.done(function(res){       
        			if($("#resultInputList > tr").length > 0){
        				$("#resultInputList > tr").remove(); 
        			}
        			if(res.returnCode == "SUCCESS"){
        				if(res.returnArray.length > 0){
        					
        					var html = "";
        					var col1List = new Array();

        					var dataList = res.returnArray;
        					for(var i=0;i<dataList.length;i++){
        						html +="<tr>";
        						html +="<td style='width:6%;' name='scid"+dataList[i].KPI_ID+"' ><div class='cell t_center'>"+dataList[i].SC_ID+"</div>";
        						html +="<input type='hidden' name='userDate' value='"+dataList[i].USER_DATE+"' />";
        						html +="<input type='hidden' name='chargeDate' value='"+dataList[i].CHARGE_DATE+"' />";  
        						html +="</td>";
        						html +="<td style='width:10%;' name='scnm"+dataList[i].KPI_ID+"'><div class='cell t_left'>"+dataList[i].SC_NM+"</div></td>";
        						html +="<td style='width:24%;' name='kpinm"+dataList[i].KPI_ID+"' ><div class='cell t_left'>"+dataList[i].KPI_NM+"</div></td>";
        						html +="<td style='width:6%;' name='kpichargenm"+dataList[i].KPI_ID+"'><div class='cell t_center'>"+dataList[i].KPI_CHARGE_NM+"</div></td>";
        						html +="<td style='width:6%;' name='kpichargeconfirm"+dataList[i].KPI_ID+"'>";
        						if(dataList[i].CHARGE_CONFIRM == ''|| dataList[i].CHARGE_CONFIRM =='N'){
        							html +="<div class='cell t_center' style='color:red;'>미확인";  
        							html +="</div>";   
        							html +="</td>";
            					}else if(dataList[i].CHARGE_CONFIRM =='Y'){
            						html +="<div class='cell t_center'>확인";
        							html +="<div class='cell t_center'>"+dataList[i].CHARGE_DATE+"</div>";   
        							html +="</div>";
        							html +="</td>";    
            					}        
        						html +="<td style='width:6%;' name='ownerusernm"+dataList[i].KPI_ID+"'><div class='cell t_center'>"+dataList[i].OWNER_USER_NM+"</div></td>";
        						html +="<td style='width:6%;' name='owneruserconfirm"+dataList[i].KPI_ID+"'>"; 
        						if(dataList[i].USER_CONFIRM == ''|| dataList[i].USER_CONFIRM =='N'){
    								html +="<div class='cell t_center' style='color:red;'>미확인";
    								html +="</div>";   
    								html +="</td>";
        						}else if(dataList[i].USER_CONFIRM =='Y'){   
        							html +="<div class='cell t_center'>확인";
    								html +="<div class='cell t_center'>"+dataList[i].USER_DATE+"</div>";   
    								html +="</div>";
    								html +="</td>";    
        						}      
        						html +="<td style='width:15%;'><div class='cell t_left'>"+dataList[i].COL_NM+"</div></td>";    
        						html +="<td style='width:6%;'><div class='cell t_center'>"+dataList[i].COL_UNIT_NM+"</div></td>";       
        						html +="<td style='width:13%;'>";
       							html +="<div class='cell t_center'>";
       							html +="<input type='hidden' name='yyyymm' value='"+dataList[i].YYYYMM+"' />";
       							html +="<input type='hidden' name='kpiID' value='"+dataList[i].KPI_ID+"' />";
       							html +="<input type='hidden' name='analCycle' value='"+dataList[i].ANAL_CYCLE+"' />";
       							html +="<input type='hidden' name='colGbn' value='"+dataList[i].COL_GBN+"' />";
       							html +="<input type='text' class='input type2 t_right num_only' style='width:90%;' name='colValue' value='"+dataList[i].COL_VALUE+"' />";
       							html +="</div>";
       							html +="</td>";
        						html +="</tr>";
        						
								 
        						if(i==0 || dataList[i].KPI_ID != dataList[i-1].KPI_ID) {
									
        							col1List.push(dataList[i].KPI_ID);
									  
        						} 
        					}
        					$("#resultInputList").append(html);
        					setEvent();
        					var rowspanCount1 = 0;
        					var rowspanCount2 = 0;
        					var rowspanCount3 = 0;
        					
        					var rowspanCount4 = 0;
        					var rowspanCount5 = 0;
        					
							//지표명
        					for(var i=0; i<col1List.length; i++) {
        						rowspanCount1 = $('[name="scid'+col1List[i]+'"]').length;
        						$('#resultInputList [name="scid'+col1List[i]+'"]:eq(0)').attr("rowSpan", rowspanCount1);
        					 	$('#resultInputList [name="scid'+col1List[i]+'"]:not(:eq(0))').remove();
								
								$('#resultInputList [name="scnm'+col1List[i]+'"]:eq(0)').attr("rowSpan", rowspanCount1);
        					 	$('#resultInputList [name="scnm'+col1List[i]+'"]:not(:eq(0))').remove();
								
								$('#resultInputList [name="kpinm'+col1List[i]+'"]:eq(0)').attr("rowSpan", rowspanCount1);
        					 	$('#resultInputList [name="kpinm'+col1List[i]+'"]:not(:eq(0))').remove();
								
								$('#resultInputList [name="kpichargenm'+col1List[i]+'"]:eq(0)').attr("rowSpan", rowspanCount1);
        					 	$('#resultInputList [name="kpichargenm'+col1List[i]+'"]:not(:eq(0))').remove();
        					 	
        					 	$('#resultInputList [name="kpichargeconfirm'+col1List[i]+'"]:eq(0)').attr("rowSpan", rowspanCount1);
        					 	$('#resultInputList [name="kpichargeconfirm'+col1List[i]+'"]:not(:eq(0))').remove();
								
								$('#resultInputList [name="ownerusernm'+col1List[i]+'"]:eq(0)').attr("rowSpan", rowspanCount1);
        					 	$('#resultInputList [name="ownerusernm'+col1List[i]+'"]:not(:eq(0))').remove();		 
        					 	
        					 	$('#resultInputList [name="owneruserconfirm'+col1List[i]+'"]:eq(0)').attr("rowSpan", rowspanCount1);
        					 	$('#resultInputList [name="owneruserconfirm'+col1List[i]+'"]:not(:eq(0))').remove();
								
								console.log(col1List[i]+":"+rowspanCount1);
        					} 
        				}
        			}
        		});
        	}
        	//지표정의서
        	function makeKpiReport(){
        		$i.sqlhouse(268,{S_YYYY:$("#cboSearchyear").val(),S_SC_ID:$("#txtTabscid").val(),S_GUBN:$("#txtGubn").val()}).done(function(res){
	        	   var data = res.returnArray;
	        		// prepare the data
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'MT_CD', type: 'string' },
		                    { name: 'MT_ID', type: 'string' },
		                    { name: 'MT_NM', type: 'string'}
		                ],
		                localdata: data,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		
					var alginLeft = function (row, columnfield, value) {//left정렬
		                       return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
									
		            } ;
					var alginRight = function (row, columnfield, value) {//right정렬
		                       return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
									
		             }	  ;
		             var detailRow = function (row, columnfield, value, defaultHtml,attribute, row) {//그리드 선택시 하단 상세
							var link = "<a href='javascript:getDetailKpiReport("+JSON.stringify(row)+")' style='color:black; text-decoration:underline;' >" + value + "</a>";
							return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
							
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		
		                downloadComplete: function (data, status, xhr) { },
		
		                loadComplete: function (data) { },
		
		                loadError: function (xhr, status, error) { }
		
		            });
		
		            var dataAdapter = new $.jqx.dataAdapter(source);
		
		            $("#gridKpiReportList").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: true,
						pagesizeoptions:['100','200','300'],
		                source: dataAdapter,
						theme:'blueish',
						sortable: true,						
		                columnsresize: true,
		                pagesize: 100,
		                columns: [
		                   { text: '지표코드', datafield: 'MT_CD', width: '30%', align:'center', cellsalign: 'center'},
		                   { text: '지표명', datafield: 'MT_NM', width: '70%', align:'center', cellsalign: 'center',  cellsrenderer: detailRow  }
		                ]
		            });
					$("#gridKpiReportList").jqxGrid('selectrow', 0);
					if(reportIndex != null){
						$('#gridKpiReportList').jqxGrid('selectrow', reportIndex);
        				getDetailKpiReport($("#gridKpiReportList").jqxGrid('getrowdata', reportIndex));
        				reportIndex = null;
					}else{
						if($("#gridKpiReportList").jqxGrid('getrowdata', 0) != null){
							getDetailKpiReport($("#gridKpiReportList").jqxGrid('getrowdata', 0));
						}else{
							getDetailKpiReport("");
						}
					}
					setPagerLayout('gridKpiReportList'); //페이저 레이아웃
	           });
        	}
        	function getDetailKpiReport(row){
        		if(row == null){
       				$("#txtPersName").html("");
        			$("#txtOwnerUserName").html("");
        			$("#txtKpiChargeName").html("");    
        			$("#txtEvalTypeName").html("");
        			$("#txtCalName").html("");
        			$("#txtAreaMtDef").val("");
        			$("#txtTargetDesc").html("");
        			$("#txtMtDesc").html("");
        			$("#txtKpiEvaTypName").html("");
        			$("#txtMeasCycleName").html("");
        			$("#txtMmName").html("");
        			$("#txtKpiDept0").html("");
       				$("#txtKpiDept1").html("");
       				$("#txtKpiDept2").html("");
       				$("#txtKpiDept3").html("");
       				$("#txtKpiDept4").html("");
       				$("#txtKpiCurramt0").html("");
       				$("#txtKpiCurramt1").html("");
       				$("#txtKpiCurramt2").html("");
       				$("#txtKpiCurramt3").html("");
       				$("#txtKpiCurramt4").html("");
       				$("#txtKpiID0").html("");
       				$("#txtKpiID1").html("");
       				$("#txtKpiID2").html("");
       				$("#txtKpiID3").html("");
       				$("#txtKpiID4").html("");
       				$("#targetYyyy0").html("");
       				$("#targetYyyy1").html("");
       				$("#targetYyyy2").html("");
       				$("#targetYyyy3").html("");
       				$("#targetYyyy4").html("");
       				$("#targetValue0").html("");
       				$("#targetValue1").html("");
       				$("#targetValue2").html("");
       				$("#targetValue3").html("");
       				$("#targetValue4").html("");
       				$("#txtKpiWeight").html("");
       				$("#kpiUpmpDetail tr").remove();
       				var relatedWorkHtml = "";
       				relatedWorkHtml += "<tr>";
					relatedWorkHtml += "<th class='w15p'>유관업무</th>";
					relatedWorkHtml += "<td colspan='2'><div class='cell'></div></td>";
					relatedWorkHtml += "<td colspan='7'><div class='cell'></div></td>";
					relatedWorkHtml += "</tr>";
					$("#kpiUpmpDetail").append(relatedWorkHtml);
					$("#kpiReportKpiGubn").html("");
        			$("#kpiCode").html("");
        		}else{
	        		var mtid = row.MT_ID;
	        		var scid = row.SC_ID;
	        		var kpiId = row.MT_CD;
	        		var kpiNm = row.MT_NM;
	        		var kpiData = $i.sqlhouse(263,{S_MT_ID:mtid, S_YYYY:$("#cboSearchyear").val()});
	        		kpiData.done(function(kpi){
	        			if(kpi.returnArray.length > 0){
	        				$("#txtPersName").html(kpi.returnArray[0].PERS_NM);
		        			$("#txtOwnerUserName").html(kpi.returnArray[0].OWNER_USER_NM);
		        			$("#txtKpiChargeName").html(kpi.returnArray[0].KPI_CHARGE_NM);    
		        			$("#txtEvalTypeName").html(kpi.returnArray[0].EVAL_TYPE_NM);
		        			$("#txtCalName").html(kpi.returnArray[0].CAL_NM);
		        			$("#txtAreaMtDef").val(kpi.returnArray[0].MT_DEF);
		        			$("#txtTargetDesc").html(kpi.returnArray[0].TARGET_DESC);
		        			$("#txtMtDesc").html(kpi.returnArray[0].MT_DESC);
		        			$("#txtKpiEvaTypName").html(kpi.returnArray[0].EVA_TYP_NM);
		        			$("#txtMeasCycleName").html(kpi.returnArray[0].MEAS_CYCLE_NM);
		        			$("#txtMmName").html(kpi.returnArray[0].MM_NM);
		        			$("#txtKpiWeight").html(kpi.returnArray[0].KPI_WEIGHT);
		        			if(kpi.returnArray[0].SC_ID == "100"){ 
		        				$("#kpiReportKpiGubn").html("대학 KPI");
		        				$("#kpiWeightTable").show();
			        		}else{
			        			$("#kpiReportKpiGubn").html("부서 KPI");
			        			$("#kpiWeightTable").hide();
			        		}
		        			$("#kpiCode").html(kpiId+"<span class='label sublabel'>"+kpiNm+"</span>");
	        			}else{
	        				$("#txtPersName").html("");
		        			$("#txtOwnerUserName").html("");
		        			$("#txtKpiChargeName").html("");    
		        			$("#txtEvalTypeName").html("");
		        			$("#txtCalName").html("");
		        			$("#txtAreaMtDef").val("");
		        			$("#txtTargetDesc").html("");
		        			$("#txtMtDesc").html("");
		        			$("#txtKpiEvaTypName").html("");
		        			$("#txtMeasCycleName").html("");
		        			$("#txtMmName").html("");
	        			}
	        		});
	        		var kpiCurramtDate = $i.sqlhouse(266, {S_MT_ID:mtid, S_YYYY:$("#cboSearchyear").val()});
	        		kpiCurramtDate.done(function(curramt){
	        			$("#txtKpiDept0").html("");
        				$("#txtKpiDept1").html("");
        				$("#txtKpiDept2").html("");
        				$("#txtKpiDept3").html("");
        				$("#txtKpiDept4").html("");
        				$("#txtKpiCurramt0").html("");
        				$("#txtKpiCurramt1").html("");
        				$("#txtKpiCurramt2").html("");
        				$("#txtKpiCurramt3").html("");
        				$("#txtKpiCurramt4").html("");
        				$("#txtKpiID0").html("");
        				$("#txtKpiID1").html("");
        				$("#txtKpiID2").html("");
        				$("#txtKpiID3").html("");
        				$("#txtKpiID4").html("");
        				$("#txtKprCurramtSum").html("");
	        			if(curramt.returnArray.length > 0){
	        				var sum = "0";
	        				for(var i=0;i<curramt.returnArray.length;i++){
	        					if(curramt.returnArray[i].KPI_CURRAMT != ""){
	        						$("#txtKpiDept"+i).html(curramt.returnArray[i].SC_NM);
		        					$("#txtKpiCurramt"+i).html(parseInt(curramt.returnArray[i].KPI_CURRAMT)+"%");
		        					$("#txtKpiID"+i).html(curramt.returnArray[i].MT_CD);
		        					sum = parseInt(parseInt(sum)+parseInt(curramt.returnArray[i].KPI_CURRAMT));
	        					}
	        				}
	        				$("#txtKprCurramtSum").html(sum+"%");
	        			}
	        		});
	        		var kpiTargetDate = $i.sqlhouse(265, {S_MT_ID:mtid});
	        		kpiTargetDate.done(function(target){
	        			$("#targetYyyy0").html("");
        				$("#targetYyyy1").html("");
        				$("#targetYyyy2").html("");
        				$("#targetYyyy3").html("");
        				$("#targetYyyy4").html("");
        				$("#targetValue0").html("");
        				$("#targetValue1").html("");
        				$("#targetValue2").html("");
        				$("#targetValue3").html("");
        				$("#targetValue4").html("");
	        			if(target.returnArray.length > 0){
	        				for(var k=0;k < 5;k++){
	        					if(target.returnArray[k] == null){
	        						if(i!=0){
	        							$("#targetYyyy"+k).html(parseInt(target.returnArray[0].YYYY)+k);
		        						$("#targetValue"+k).html("");
	        						}
	        					}else{
		        					$("#targetYyyy"+k).html(parseInt(target.returnArray[0].YYYY)+k);
		        					$("#targetValue"+k).html(target.returnArray[k].TARGET_AMT);	
	        					}
	        				}
	        			}
	        		});
	        		var upmuData = $i.sqlhouse(264,{S_MT_ID:mtid});
	       			upmuData.done(function(umpu){
	       				var relatedWorkHtml = "";
	       				$("#kpiUpmpDetail tr").remove();
	       				if(umpu.returnArray.length>0){
	       					for(var j=0;j<umpu.returnArray.length;j++){
	       						if(j == 0){
	       							relatedWorkHtml += "<tr>";
	       							relatedWorkHtml += "<th class='w15p' rowspan='"+umpu.returnArray.length+1+"'>유관업무</th>";
	       							relatedWorkHtml += "<td colspan='2'><div class='cell'>"+umpu.returnArray[j].RELATED_WORK1+"</div></td>";
	       							relatedWorkHtml += "<td colspan='7'><div class='cell'>"+umpu.returnArray[j].RELATED_WORK2+"</div></td>";
	       							relatedWorkHtml += "</tr>";
	       						}else{
	       							relatedWorkHtml += "<tr>";
	       							relatedWorkHtml += "<td colspan='2'><div class='cell'>"+umpu.returnArray[j].RELATED_WORK1+"</div></td>";
	       							relatedWorkHtml += "<td colspan='7'><div class='cell'>"+umpu.returnArray[j].RELATED_WORK2+"</div></td>";
	       							relatedWorkHtml += "</tr>";
	       						}
	       					}
	       				}else{
	       					relatedWorkHtml += "<tr>";
							relatedWorkHtml += "<th class='w15p'>유관업무</th>";
							relatedWorkHtml += "<td colspan='2'><div class='cell'></div></td>";
							relatedWorkHtml += "<td colspan='7'><div class='cell'></div></td>";
							relatedWorkHtml += "</tr>";
	       				}
	//        				if(umpu.returnArray.length != 0) $("#kpiUpmpDetail").append(relatedWorkHtml);
	       				$("#kpiUpmpDetail").append(relatedWorkHtml);
	       			});
        		}
//         		<th rowspan="3"><div>유관업무</div></th>
// 				<td colspan="2"><div class="cell">정보공시</div></td>
// 				<td colspan="7"><div class="cell">5-다. 졸업생의 취업 현황</div></td>
        	}
        	function insertTarget(){
        		$("#txtTargetevagbn").val($("#txtEvagbn").val());
        		var check = $i.sqlhouse(106,{ACCT_ID:$("#txtTargetacctid").val(),EVA_GBN:$("#txtTargetevagbn").val(),YYYY:$("#txtTargetyyyy").val(),KPI_ID:$("#txtTargetkpiid").val()});
        		check.done(function(res){
        			if($("#txtTargetorgyyyy").val() == ""){
        				if(res.returnArray.length != 0){
        					$i.dialog.warning("SYSTEM","년도가 이미 사용중입니다");
        					return ;
	        			}
        			}
        			
        			$i.post("./insertTarget","#targetSave").done(function(data){
		        		if(data.returnCode == "EXCEPTION"){
		        			$i.dialog.error("SYSTEM",data.returnMessage);
		        		}else{
		        			$i.dialog.alert("SYSTEM","저장 되었습니다");
		        			makeTab3DetailTargetList($("#txtTargetacctid").val(), $("#txtTargetscid").val(), $("#txtTargetscnm").val(), $("#txtTargetmtcd").val(), $("#txtTargetkpiid").val(), $("#txtTargetkpinm").val());
		        			resetTarget();
		        		}
		        	});
        			
        		});
        	}
        	function removeTarget(){
        		$("#txtTargetevagbn").val($("#txtEvagbn").val());
        		$i.post("./removeTarget", "#targetSave").done(function(data){
        			if(data.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM",data.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM","삭제되었습니다");   
        				makeTab3DetailTargetList($("#txtTargetacctid").val(), $("#txtTargetscid").val(), $("#txtTargetscnm").val(), $("#txtTargetmtcd").val(),$("#txtTargetkpiid").val(), $("#txtTargetkpinm").val());
        				resetTarget();
        			}
        		});
        	}
        	function resetTarget(){
        		$("#txtTargetorgyyyy").val("");
        		$("#txtTargetyyyy").val("");
        		$("#txtTargetamt").val("");
        		$("#txtTargetcurramt").val("");
        	}
        	function saveKpiuser(){
        		$("#yyyyKpiUser").val($("#cboSearchyear").val());
        		$i.post("./insertKpiuser","#insertKpiuser").done(function(data){
        			if(data.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM",data.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM","저장되었습니다");
        			}
        		});
        	}
        	function saveWeight(){
        		$("#txtWeightyyyy").val($("#cboSearchyear").val());
        		$i.post("./insertKpiweight","#insertKpiweight").done(function(data){
        			if(data.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM",data.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM","저장되었습니다");
        			}
        		});
        	}
        	function saveKpiResult(){
        		$i.post("./insertResultColValue", "#saveResult").done(function(data){
        			if(data.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM",data.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM", "저장 되었습니다");
        			}
        		});
        	}
        	function allUserchane(gubn){
        		if(gubn == "owneruser"){
        			$("[name='owneruserid']").val($("[name='owneruserid']")[0].value);
        			$("[name='ownerusernm']").val($("[name='ownerusernm']")[0].value);
        		}else if(gubn == "kpicharge"){
        			$("[name='kpichargeid']").val($("[name='kpichargeid']")[0].value);
        			$("[name='kpichargenm']").val($("[name='kpichargenm']")[0].value);
        		}else if(gubn == "tgtuser"){
        			$("[name='tgtuserid']").val($("[name='tgtuserid']")[0].value);
        			$("[name='tgtusernm']").val($("[name='tgtusernm']")[0].value);
        		}else if(gubn == "tgtcharge"){
        			$("[name='tgtchargeid']").val($("[name='tgtchargeid']")[0].value);
        			$("[name='tgtchargenm']").val($("[name='tgtchargenm']")[0].value);
        		}else if(gubn == "empuser"){
        			$("[name='empid']").val($("[name='empid']")[0].value);
        			$("[name='empusernm']").val($("[name='empusernm']")[0].value);
        		}
        	}
        	function allWeightValue(){
        		$("[name='kpiweight']").val($("[name='kpiweight']")[0].value);
        	}
        	function getScidDetail(scid){
        		var dat = $i.post("./selectDetail",{scid:scid});
        		dat.done(function(res){
        			if(res.returnArray.length > 0){
        				$("#txtPscid").val(res.returnArray[0].PSC_ID);
        				$("#txtPscnm").val(res.returnArray[0].PSC_NM);
        				$("#txtScid").val(res.returnArray[0].SC_ID);
        				$("#txtScnm").val(res.returnArray[0].SC_NM);
        				$("#txtareaScdesc").val(res.returnArray[0].SC_DESC);
        				$("#txtareaEtcdesc").val(res.returnArray[0].ETC_DESC);
        				if(res.returnArray[0].EVAL_G != ''){
        					$("#cboEvalg").val(res.returnArray[0].EVAL_G);
        				}else{
        					$("#cboEvalg").jqxComboBox("selectIndex", 0);
        				}
        				if(res.returnArray[0].SC_UDC1 != ''){
        					$("#cboScudc1").val(res.returnArray[0].SC_UDC1);
        				}else{
        					$("#cboScudc1").jqxComboBox('selectIndex', 0 ); 
        				}
        				$("#txtPptefile").val(res.returnArray[0].PPTFILE);
        				$("#txtSortorder").val(res.returnArray[0].SORT_ORDER);
        				$("#dateStartdate").val(res.returnArray[0].START_DAT);
        				$("#dateEnddate").val(res.returnArray[0].END_DAT);
        			}
        		});
        		var deptlist = $i.post("./getListDept",{scid:scid});
        		deptlist.done(function(res){
        			if(res.returnArray.length > 0){
        				var tmpHtml = "";
						var deptCd = "";
						for(var i=0;i<res.returnArray.length;i++){
							if(i != 0){
								tmpHtml += "<br />";
								deptCd += ",";
							}
							tmpHtml += res.returnArray[i].SC_NM + " ( " + res.returnArray[i].DEPT_CD + " ) ";
							deptCd += res.returnArray[i].DEPT_CD;
						}
						$("#labeldept").html(tmpHtml);
						$("#txtDeptids").val(deptCd);
        			}else{
        				$("#labeldept").html('');
						$("#txtDeptids").val('');
        			}
        		});
        		var scidlist = $i.post("./getListScid",{scid:scid});
        		scidlist.done(function(res){
        			if(res.returnArray.length > 0){
        				var tmpHtml = "";
						var sustIds = "";
						for(var i=0;i<res.returnArray.length;i++){
							if(i != 0){
								tmpHtml += "<br />";
								sustIds += ", ";
							}
							tmpHtml += res.returnArray[i].SUST_NM + " ( " + res.returnArray[i].SUST_ID + " ) ";
							sustIds += res.returnArray[i].SUST_ID;
						}
						$("#labelsustid").html(tmpHtml);
						$("#txtSustIds").val(sustIds);
        			}else{
        				$("#labelsustid").html('');
						$("#txtSustIds").val(''); 
        			}
        		});
        	}
 
        	function popupMappingKpiSust(){   
        		if($("#hiddenValueTargetMtID").val()=="") {
        			$i.dialog.warning("SYSTEM","지표를 선택 해 주세요");
        			return ;
        		}
        		window.open('./popupMappingKpiSust?acctid=${paramAcctid}&yyyy='+$("#cboSearchyear").val()+"&mtid="+$("#hiddenValueTargetMtID").val() , 'popupMappingKpiSust', '300', '400', 'yes', 'yes');
        	}
        	function popupMappingKpiUser(acctid, scid, kpiid,  gubn){
        		window.open('./popupMappingKpiUser?acctid='+acctid+'&scid='+scid+'&kpiid='+kpiid+'&gubn='+gubn, 'popupMappingKpiUser','scrollbars=no, resizable=no, width=930, height=400');
        	}
        	function popupMappingKpiDetail(){
        		window.open('./popupKpiDetail_141?pYyyy='+$("#cboSearchyear").val(), 'popupMappingKpiUser','scrollbars=no, resizable=no, width=1200, height=750');
        	}
        	function reset(){
        		$("#txtPscid").val('');
        		$("#txtPscnm").val('');
        		$("#txtScid").val('');
        		$("#txtScnm").val('');
        		$("#txtareaScdesc").val('');
        		$("#txtareaEtcdesc").val('');
        		$("#txtSortorder").val('');
        		$("#dateStartdate").val('');
        		$("#dateEnddate").val('');
        		$("#labeldept").html('');
        		$("#txtDeptids").val('');
        		$("#labelsustid").html('');
        		$("#txtSustids").val('');
        		
//         		$("#cboEvalg")

        	}
        	function selectDisplay(obj){
				if(obj == "save"){
					$("#save").show();
					$("#list").hide();
				}else if(obj == "list"){
					$("#save").hide();
					$("#list").show();
				}
			}
        	
        	function mappingSustId(){
				var sustId = $("#pSustId").val();
				var appLength = $("input[name='appValue']").length;
				var appValue = appLength - 1;
				var sustLength = "";
				if(sustId == ""){
					sustLength = 0;
				}else{
					sustLength = sustId.split(",").length;
				}
				$("#appValue"+appValue).html(sustLength);
				$("input[name='appValue']").eq(appValue).val(sustLength);
				$("#pSustWeight").val($("input[name='kpiCurrentAmt']").eq(appValue).val()/sustLength);
			}
        	
        	
        	function makeTblValueDetail(data){
 
        		$("#hiddenValueTargetMtID").val(data.MT_ID); 
        		$("#spanValueTargetMtName").html(data.MT_ID+" > "+data.KPI_NM);
        		$("#hiddenValueTargetMtWeight").val(data.KPI_WEIGHT);
        		$("#spanValueTargetMtWeight").html(data.KPI_WEIGHT);
        		
        		/**
        			가치형성 비율 컬럼 정보
        			KPI_CURRAMT : 가치형성비율
        			KPI_WEIGHT	  : 세부가중치
        		**/
        		 
        		$i.sqlhouse(138,{S_ACCT_ID:"${paramAcctid}", S_SC_NM:"",S_YYYY:$("#cboSearchyear").val(),S_MT_ID:data.MT_ID,S_USE_YN:'Y'}).done(function(res){
        			$("#hiddenTargetSustList").val(JSON.stringify(res.returnArray)) ; 
        		});
        		$i.sqlhouse(137,{ 
        			MT_ID:data.MT_ID,
        			S_KPI_ID:data.KPI_ID,
        			S_YYYY:$("#cboSearchyear").val(),
        			ACCT_ID:"${paramAcctid}"
        		}).done(function(c){
        			if(c.returnCode=="SUCCESS"){
        				var arr = c.returnArray;
        				$("#tblValueDetail").empty(); 
        				for(var i = 0; i<arr.length;i++){
        					var rowString = "<tr  style='padding-left:5px;' data-role='valueDetailRow' data-idx='"+i+"' data-scid='"+arr[i].SC_ID+"'>"+
	        					"<td style='padding-left:5px;width:28%;'><input type='hidden' name='scID' value='"+arr[i].SC_ID+"' />"+arr[i].SC_NM+"</td>"+
	        					"<td style='text-align:right;padding-right:5px;width:18%;'><input name = 'kpiCurrentAmt' type ='text' style='width:105px;' value='"+arr[i].KPI_CURRAMT+"' onblur='valueWeightCalculate("+i+");' /></td>"+  
	        					"<td style='text-align:right;padding-right:5px;width:18%;'><span name = 'kpiGroupWeight' >"+arr[i].KPI_GROUP_WEIGHT+"</span></td>"+
	        					"<td style='text-align:right;padding-right:5px;width:18%;'><span name='targetCount' >"+arr[i].TGT_CNT+"</span></td>"+
	        					"<td style='text-align:right;padding-right:5px;width:18%;'><span name = 'kpiWeightDisplay' >"+arr[i].KPI_WEIGHT+"</span></td>"+ 
        					"</tr>"; 
        					$(rowString).appendTo("#tblValueDetail");
        					
        				}
        				valueWeightSum();
        			} 
        		});
        	}
        	function valueWeightCalculate(row){
        		var selector ="tr[data-role=valueDetailRow]";
        		if(row!=undefined) selector+="[data-idx="+row+"]";
        		//대학지표 가중치			(A)
        		var univWeight = parseFloat($("#spanValueTargetMtWeight").html());
        		//조직지표 가치형성 비율	(B)
        		var kpiCurrentAmt   = $(selector).find("[name=kpiCurrentAmt]").val();
        		//조직 세부가중치 계산		(C)	=	A * B / 100
        	    var kpiGroupWeight =univWeight * kpiCurrentAmt / 100;
        	    $(selector).find("[name=kpiGroupWeight]").html(kpiGroupWeight);
        	    //적용대상					(D)
        	    var targetCount = parseInt($(selector).find("[name=targetCount]").html());
        	    //적용 세부가중치 계산		(E)	=	C / D
        	    $(selector).find("[name=kpiWeightDisplay]").html(kpiGroupWeight/targetCount);
        	    //가치회계형성 합계 구하기.
        	    valueWeightSum();
			}
        	function valueWeightSum(){
        		$("#spanSumValueWeight").html(Enumerable.From($("#tblValueDetail [name=kpiCurrentAmt]")).Sum(function(c){ return parseFloat($(c).val()); }));
        	}
	        function makeGridValueList() {
	
	           $i.sqlhouse(135,{S_YYYY:$("#cboSearchyear").val(),S_ACCT_ID:"${paramAcctid}"}).done(function(res){
	        	   var data = res.returnArray;
	        		// prepare the data
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'SC_ID', type: 'string' },
		                    { name: 'SC_NM', type: 'string' },
		                    { name: 'MT_ID', type: 'string'},
		                    { name: 'MT_CD', type: 'string'},
							{ name: 'KPI_ID', type: 'string'},
							{ name: 'KPI_NM', type: 'string' },
							{ name: 'KPI_WEIGHT', type: 'string'}
		                ],
		                localdata: data,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		
					var alginLeft = function (row, columnfield, value) {//left정렬
		                       return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
									
		            } ;
					var alginRight = function (row, columnfield, value) {//right정렬
		                       return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
									
		             }	  ;
		             var detailRow = function (row, columnfield, value, defaultHtml,attribute, row) {//그리드 선택시 하단 상세
							var link = "<a href='javascript:makeTblValueDetail("+JSON.stringify(row)+")' style='color:black; text-decoration:underline;' >" + value + "</a>";
							return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
							
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		
		                downloadComplete: function (data, status, xhr) { },
		
		                loadComplete: function (data) { },
		
		                loadError: function (xhr, status, error) { }
		
		            });
		
		            var dataAdapter = new $.jqx.dataAdapter(source);
		
		            $("#gridValueList").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: true,
						pagesizeoptions:['100','200','300'],
						pagesize: 100,
		                source: dataAdapter,
						theme:'blueish',
						sortable: true,						
		                columnsresize: true,
		                columns: [
		                   { text: '조직ID', datafield: 'SC_ID', width: '15%', align:'center', cellsalign: 'center'},
		                   { text: '조직명', datafield: 'SC_NM', width: '15%', align:'center', cellsalign: 'center'},
		                   { text: '지표코드', datafield: 'MT_CD', width: '18%', align:'center', cellsalign: 'center' },
						   { text: '지표명', datafield: 'KPI_NM', width: '32%', align:'center', cellsalign: 'left',  cellsrenderer: detailRow  },
						   { text: '가중치(A)', datafield: 'KPI_WEIGHT', width: '20%', align:'center', cellsalign: 'center'}
		                ]
		            });
					$("#gridValueList").jqxGrid('selectrow', 0);
					 setPagerLayout('gridValueList'); //페이저 레이아웃
	           });
	        }
	        function makeReport(){
	        	$("#kpiAttachFileReport").empty();
        		var iframe = '';
        		                                            
        		iframe += '<iframe width="100%" height="500px" src="../../report/kpiAttachFileStateTab/list?pEvaGbn='+$("#resultEvaGbn").val()+'&pYyyy='+$("#cboSearchyear").val()+'" frameborder="no" allowTransparency="true">';
				iframe += '</iframe>';   
				     
				$("#kpiAttachFileReport").append(iframe);	
	        	
	        }
       	/********************************평가결과 관리********************************************/
        	function EvalResult(){
        		$i.sqlhouse(304,{S_YYYY:$("#cboSearchyear").val(),S_SC_ID:$("#txtTabscid").val(),S_GUBN:$("#txtGubn").val()}).done(function(res){
	        	   var data = res.returnArray;
	        		// prepare the data
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'MT_CD', type: 'string' },
		                    { name: 'MT_ID', type: 'string' },
		                    { name: 'MT_NM', type: 'string'},
		                    { name: 'SC_ID', type: 'string'},
						    { name: 'SC_NM', type: 'string'},
		                    { name: 'KPI_ID', type: 'string'}
		                ],
		                localdata: data,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		
					var alginLeft = function (row, columnfield, value) {//left정렬
		                       return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
									
		            } ;
					var alginRight = function (row, columnfield, value) {//right정렬
		                       return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
									
		             }	  ;
		             var detailRow = function (row, columnfield, value, defaultHtml,attribute, row) {//그리드 선택시 하단 상세
							var link = "<a href='javascript:getDetailEvalResult("+JSON.stringify(row)+")' style='color:black; text-decoration:underline;' >" + value + "</a>";
							return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
							
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		
		                downloadComplete: function (data, status, xhr) { },
		
		                loadComplete: function (data) { },
		
		                loadError: function (xhr, status, error) { }
		
		            });
		
		            var dataAdapter = new $.jqx.dataAdapter(source);
		
		            $("#EvalResultList").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: true,
						pagesizeoptions:['100','200','300'],
		                source: dataAdapter,
						theme:'blueish',
						sortable: true,						
		                columnsresize: true,
		                pagesize: 100,
		                columns: [
		                   { text: '지표코드', datafield: 'MT_CD', width: '20%', align:'center', cellsalign: 'center'},
						   { text: '조직명', datafield: 'SC_NM', width: '25%', align:'center', cellsalign: 'center'},
		                   { text: '지표명', datafield: 'MT_NM', align:'center', cellsalign: 'center',  cellsrenderer: detailRow  }
		                ]
		            });
					$("#EvalResultList").jqxGrid('selectrow', 0);
					if(reportIndex != null){
						$('#EvalResultList').jqxGrid('selectrow', reportIndex);
        				getDetailEvalResult($("#gridKpiReportList").jqxGrid('getrowdata', reportIndex));
        				reportIndex = null;
					}else{
						if($("#EvalResultList").jqxGrid('getrowdata', 0) != null){
							getDetailEvalResult($("#EvalResultList").jqxGrid('getrowdata', 0));
						}else{
							getDetailEvalResult("");
						}
					}
					setPagerLayout('EvalResultList'); //페이저 레이아웃
	           });
        	}	     
        	function getDetailEvalResult(row){
        		resetEvalResult();
	        	if(row == ""){	
					$("#evalResultGubn").html("");
					$("#kpiResultCode").html("");
					$("#gradeDesc").val("");
					$("#ipvDesc").val("");
					$("#btnevalResult").hide();
	        	}else{
	        		var mtId = row.MT_ID;
	        		var scId = row.SC_ID;
	        		var kpiId = row.KPI_ID;
	        		var kpiNm = row.MT_NM;
	        		
	        		$("#btnevalResult").show();
	        		$("#scIdResult").val(scId);
        			$("#yyyyResult").val($("#cboSearchyear").val());
        			$("#kpiIdResult").val(kpiId);
        			$("#mtIdResult").val(mtId);
        		
	        		if(scId == "100"){ 
		        		$("#evalResultGubn").html("대학 KPI");
			        }else{
			        	$("#evalResultGubn").html("부서 KPI");
			       }
		        	$("#kpiResultCode").html(kpiId+"<span class='label sublabel'>"+kpiNm+"</span>");
		        			
	        		var evalResult = $i.service("kpiEvalGradeDesc",[$("#cboSearchyear").val(),"${param.pEvaGbn}",kpiId]);  
					evalResult.done(function(data){    
						for(var i=0;i<data.returnArray.length;i++){   
							if(data.returnArray[0].GRADE != ""){
							$("#grade_"+data.returnArray[0].GRADE).prop("checked","checked");	
			        		$("#gradeDesc").val(data.returnArray[i].GRADE_DESC);
				        	$("#ipvDesc").val(data.returnArray[i].IPV_DESC);  
								}
			                 }  		
					});
	        	}	
        	}	
			function makeRadio(){
				var radioData = $i.service("makeRadioButton",[]);      
				radioData.done(function(data){   
				var radio = "";       
				$("#radioList").empty();    
				for(var i=0;i<data.returnArray.length;i++){                             
					radio += '<input type="radio" class="m_b2 m_r3 m_l5" id="grade_'+data.returnArray[i].EVA_GRD+'" name="gradeValue" value="'+data.returnArray[i].EVA_GRD+'"/>'+data.returnArray[i].EVA_GRDNM;
				}                                   
				$("#radioList").append(radio);                 
				});          
			}    
			function resetEvalResult(){
					$("#evalResultGubn").html("");
					$("#kpiResultCode").html("");
					$("#gradeDesc").val("");
					$("#ipvDesc").val("");
					$('input:radio[name="gradeValue"]').prop('checked', "");
			}
        	function evalResultSave(){
        		if($(':radio[name="gradeValue"]:checked').length == 0){
					$i.dialog.warning("SYSTEM","평가등급을 선택하세요");       
					return;
        		}   
        		        		
        		$("#grade").val($(':radio[name="gradeValue"]:checked').val());
        		$i.post("./evalResult","#evalResult").done(function(data){
        			if(data.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM",data.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM","저장되었습니다");
        			}
        		});
        	}			
			function setPagerLayout(selector) {
    			try{
	    			var pagesize = $('#'+selector).jqxGrid('pagesize');
	    			
	    			var w = 49; 
	    				
	    			if(pagesize<100) {
	    				w = 44;
	    			} else if(pagesize>99&&pagesize<1000) {
	    				w = 49;
	    			} else if(pagesize>999&&pagesize<10000) {
	    				w = 54;
	    			}
	    			
	    			//디폴트 셋팅
	    			$('#gridpagerlist'+selector).jqxDropDownList({ width: w+'px' });
	    			
	    			//체인지 이벤트 처리
	    			$('#'+selector).on("pagesizechanged", function (event) {
	    				var args = event.args;
	    				
	    				if(args.pagesize<100) {
	    					$('#gridpagerlist'+selector).jqxDropDownList({ width: '44px' });
	    				} else if(args.pagesize>99 && args.pagesize<1000) {
	    					$('#gridpagerlist'+selector).jqxDropDownList({ width: '49px' });
	    				} else if(args.pagesize>999 && args.pagesize<10000) {
	    					$('#gridpagerlist'+selector).jqxDropDownList({ width: '54px' });
	    				} else {
	    					$('#gridpagerlist'+selector).jqxDropDownList({ width: 'auto' });
	    				}
	    			});
    			}catch(e){console.log(e);}
    		}
	    </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
			#gridKpilist .jqx-cell {
			  padding: 3px 0px 3px 5px     
			}
		</style>
    </head>
    <body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 1%;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<form id="searchForm" name="searchForm" action="./crud">
					<input type="hidden" id="txtTabscid" name="tabscid" value="100" />
					<input type="hidden" id="txtTabList" name="tablist" />
					<input type="hidden" id="txtTabSubstr" name="tabSubstr" />
					<input type="hidden" id="txtGubn" name="gubn" value="0" /> 
					<input type="hidden" id="txtEvagbn" name="evagbn" value="${param.pEvaGbn}" />
					<div class="label type1 f_left">평가년도:</div>
					<div class="combobox f_left"  id='cboSearchyear' name="searchyear"></div>
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
						<div class="button type1 f_left">
							<input type="button" value="대학지표입력" id='btnInsertUniv' width="100%" height="100%" onclick="getRowDetail('100','','');" />
							<input type="button" value="부서/학과지표입력" id='btnInsertDept' width="100%" height="100%" onclick="getRowDetail('','','');" />
						</div>
					</div>
				</form> 
			</div>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:24%; margin-right:1%;">
					<div class="tabs f_left" style=" width:100%; height:580px; margin-top:0px;">
						<div id='jqxTabs01'>
							<ul>
								<li style="margin-left: 0px;">조직기준</li>    
								<li onclick="makeStraListTree();">전략기준</li>
								<li onclick="makeKpiList();">지표기준</li>
							</ul>
							<div class="tabs_content" style="height:100%; ">
								<div class="tree" style="border:none;" id="treeScidlist"></div>
							</div>
							<div class="tabs_content" style="height:100%;">
								<div class="tree" style="border:none;" id="treeStraList"></div>
							</div>         
							<div  class="tabs_content" style=" height:100%; ">
								<div class="group  w90p" style="margin:10px auto;">
									<div class="label type2 f_left">검색</div>
									<input type="text" id="txtSearchkpinm" name="searchkpinm" class="input type1  f_left  w50p"  style="width:100%; margin:3px 5px; "/>
    								<div class="button type2">            
  										<input type="button" value="검색" id='btnSearchkpi' width="100px" height="100%" style="margin-top:2px!important;"" onclick="makeKpiList();" />
									</div>   
  								</div>
								<div class="listbox w90p" style="margin:0px auto;">             
									<div id='listboxKpi' class="f_left" style=" width:100%; margin:2px 0;"> </div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="content f_right" style=" width:75%;">
					<div class="group f_left  w100p m_b5">   
      					<div class="label type2 f_left" id="labelShow">&nbsp;</div>
<!--        					<div class="group_button f_right">	 -->
<!-- 						</div> -->
					</div>
					<div class="tabs f_left" style=" width:100%; height:550px; margin-top:0px;">
						<div id='jqxTabs02'>
   							<ul>
    							<li style="margin-left: 0px;" onclick="makeTab1Grid('','');">지표관리</li>
  								<li onclick="makeMappingKpiuserlist();">담당자/평가자</li>
  								<li onclick="mappingKpiweightlist();" style='display:none;'>가중치</li>
  								<li onclick="makeGridValueList();">가치형성</li>
  								<li onclick="tabClack2();">지표목표</li>   
  								<li onclick="makeResultList();">지표실적</li>
  								<li onclick="makeKpiReport();">지표정의서</li>
  								<li onclick="makeReport();">증빙자료현황</li>
  								<li onclick="EvalResult();">평가결과 관리</li> 
							</ul>
							<!-- 지표관리 : 지표리스트 -->
    						<div class="tabs_content" style="height:100%; ">
								<div class="grid f_left" style="width:98%; margin:10px 1%; height:495px;">
 									<div id="gridKpilist"> </div>
								</div>
							</div>
							<!-- 담당자 -->
							<div  class="tabs_content" style=" height:100%; ">
								<div class="group f_right m_t10 m_r10">
									<div class="button type2 f_left">
										<input type="button" value="저장" id='btnInsertuser' width="100%" height="100%" onclick="saveKpiuser();"/>
									</div>
								</div>   
								<div class="datatable f_left" style="width:98%; margin:0px 1%; height:460px; border-bottom:1px solid #b0c5df; overflow: hidden;">
									<div class="datatable_fixed" style="width:100%;height:460px; ">        
										<div style=" height:435px !important; overflow-x:hidden;"><!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 600px - 50px= 550px)-->
											<form id="insertKpiuser" name="insertKpiuser">
												<input type="hidden" id="yyyyKpiUser" name="yyyyKpiUser" />
												<input type="hidden" id="evagbnKpiUser" name="evagbnKpiUser" value="${param.pEvaGbn}"/>
												<table style="width:100%;" cellspacing="0" cellpadding="0" border="0">   
													<thead style="width:100%;" >
														<tr>
															<th style="width:5%;"><div>조직ID</div></th>
															<th style="width:15%;"><div>조직명</div></th>
															<th style="width:8%;"><div>지표코드</div></th>
															<th style="width:16%;"><div>지표명</div></th>
															<th style="width:11%;min-width:97px;"><div>실적입력자<span class="th_img"><img src="../../resources/cmresource/image/icon-file-put.png" alt="일괄적용" title="일괄적용" width="16" class="pointer m_l3" onclick="allUserchane('kpicharge');"></span></div></th>
															<th style="width:11%;min-width:97px;"><div>실적승인자<span class="th_img"><img src="../../resources/cmresource/image/icon-file-put.png" alt="일괄적용" title="일괄적용" width="16" class="pointer m_l3" onclick="allUserchane('owneruser');"></span></div></th>
															<th style="width:11%;min-width:97px;"><div>목표입력자<span class="th_img"><img src="../../resources/cmresource/image/icon-file-put.png" alt="일괄적용" title="일괄적용" width="16" class="pointer m_l3" onclick="allUserchane('tgtcharge');"></span></div></th>
															<th style="width:11%;min-width:97px;"><div>목표승인자<span class="th_img"><img src="../../resources/cmresource/image/icon-file-put.png" alt="일괄적용" title="일괄적용" width="16" class="pointer m_l3" onclick="allUserchane('tgtuser');"></span></div></th>
															<th style="width:11%;min-width:97px;"><div>평가자<span class="th_img"><img src="../../resources/cmresource/image/icon-file-put.png" alt="일괄적용" title="일괄적용" width="16" class="pointer m_l3" onclick="allUserchane('empuser');"></span></div></th>
															<th style="width:1%; min-width:17px; border-bottom:0;"></th>
														</tr>
													</thead>  
													<tbody id="mappingKpiuserlist">
													</tbody>
												</table>
											</form>
										</div>
									</div>
								</div>
							</div>
							<!-- 가중치 -->
							<div  class="tabs_content" style=" height:100%; ">
								<div class="group f_right m_t10 m_r10">
									<div class="button type2 f_left">
										<input type="button" value="저장" id='btnInsertweight' width="100%" height="100%" onclick="saveWeight();" />
									</div>
								</div>
								<div class="datatable f_left auto" style="width:98%; margin:0px 1%; height:465px; border-bottom:1px solid #b0c5df;">
									<form id="insertKpiweight" name="insertKpiweight">
										<input type="hidden" id="txtWeightyyyy" name="weightyyyy" /> 
										<table width="100%" cellspacing="0" cellpadding="0" border="0" class="datatable datatable_fixed" > 
											<thead>
												<tr>
													<th style="width:5%;"><div>조직ID</div></th>
													<th style="width:15%;"><div>조직명</div></th>
													<th style="width:12%;"><div>지표코드</div></th>
													<th style="width:28%;"><div>지표명</div></th>
													<th style="width:20%;"><div>가중치<span class="th_img"><img src="../../resources/cmresource/image/icon-file-put.png" alt="일괄적용" title="일괄적용" width="16" class="pointer m_l3" onclick="allWeightValue();"></span></div></th>
													<th style="width:20%;"><div>기준값</div></th>
												</tr> 
											</thead>    
											<tbody id="mappingKpiweightlist">
											</tbody>
										</table>
									</form>
								</div>
							</div>
							<!-- 가치형성 --> 
							<div  class="container tabs_content" style=" height:100%; "> 
		                  		<div class="content f_left" style="width:44%; margin:0 1%;">
						        	<div class="iwidget_grid" style="float:left; margin:10px 0 !important; padding:0 !important; width:100%; height:495px;">
						            	<div id="gridValueList"></div> 
						            </div> 
						        </div>
							    <!--divi-->
							    <div class="content f_right" style="width:52%; margin:0 1%;">
							    	<form id="formValueWeight"> 
										<div class="group f_left  w100p m_t5">
											<input type ='hidden' name="yyyy" /> 
											<input type ='hidden' id="hiddenValueTargetMtID" name="univMtID" />
											<input type ='hidden' id="hiddenValueTargetMtWeight" name="univMtWeight"/>
											
											<input type ='hidden' id="hiddenTargetSustList" name = 'targetSustList'  value='' />
											<div class="label type2 f_left">지표명:<span class="label sublabel" id="spanValueTargetMtName">취업률</span></div>
											<div class="label type2 f_left">가중치:<span class="label sublabel" id="spanValueTargetMtWeight"></span></div>
											<div class="button-bottom" style="float:right; margin-right:5px; margin-bottom:5px;">
								            	<div class=" buttonStyle" style="float:left;">
								                	<input type="button" value="학과개별선택" id='btnDeptSelect' width="100%" height="100%" style="margin:0 !important;" />
								                </div>
								            </div>   
										</div> 
										<div class="blueish datatable f_left" style="width:100%; height:430px; margin:10px 0; overflow:hidden;"> 
											<div class="datatable_fixed" style="width:100%; height:430px; float:left; padding:25px 0;"><!--group으로 헤더높이가 길어질 경우 padding  25px간격으로 (기본헤더높이는 25px)줘야 함-->
						  						<div style=" height:405px !important; overflow-x:hidden;">    
						  						 
													<table style="width:100%;" cellspacing="0" cellpadding="0" border="0" >
														<thead>
															<tr> 
																<th scope="col" style="width:28%;">협력부서</th>
																<th scope="col" style="width:18%;">가치형성비율<br/>(B)</th>
																<th scope="col" style="width:18%;">세부가중치<br/>(C=A*B/100)</th>
																<th  scope="col" style="width:18%;">적용대상<br/>(D)</th> 
																<th scope="col"  style="width:18%;">적용세부가중치<br/>(E=C/D)</th> 
																<th style="width:2%; min-width:17px; border-bottom:0;"></th>
															</tr>
														</thead>
														<tbody id="tblValueDetail"> 
															<tr> 
																
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
  									</form>
							                <!--//iwidget_grid -->  
							         <div style="float:left; margin-top:5px;"><!--합계영역-->
							             <span class="label-5" style=" margin:0 !important; vertical-align:5px;">합계 : </span><span id="spanSumValueWeight" class="colorchange" style="vertical-align:5px; padding:0;">0</span>			
							         </div>
							             <div class="button-bottom" style="float:right;margin-right:5px; ">
							                 <div class=" buttonStyle" style="float:left;"> 
							                       <input type="button" value="저장" id='btnValueWeightSave' width="100%" height="100%" />
							                 </div>
							             </div>
							              <!--//button-bottom--> 
							    </div> 
							</div>
							<!-- 지표목표 -->
							<div  class="tabs_content" style=" height:100%; "> 
								<div class="content f_left" style="width:56%; margin:0 1%;"> 
									<div class="group f_left m_t5 m_l10">   
										<div class="label type2 f_left">담당자 : </div>          
										<input type="text" id="txtSearchTgtnm" name="searchTgtnm" class="input type1" style="width:100px; margin:3px 5px;"/> 
									</div>   
									<div class="group f_left m_t5 m_l10">   
										<div class="label type2 f_left">지표명 : </div>          
										<input type="text" id="txtTargetSearchKpinm" name="searchTargetKpinm" class="input type1" style="width:100px; margin:3px 5px;"/>                 
									</div>         
									<div class="grid f_left" style="width:100%; margin:5px 0%; height:455px;">      
										<div id="gridKpitarget"> </div>     
									</div>
								</div>
								<div class="content f_right" style="width:40%; margin:0 1%;"> 
									<div class="group f_left  w100p m_t5">
										<div class="label type2 f_left">조직명:<span class="label sublabel type1" id="targetScid"></span></div>
										<div class="label type2 f_left">지표명:<span class="label sublabel" id="targetMtcd"></span></div>
									</div>
									<div class="grid f_left" style="width:100%; margin:5px 0%; height:300px;">    
										<div id="gridTargetamt"> </div>
									</div>
									<div class="datatable f_left auto" style="width:100%; margin:10px 0%; border-bottom:1px solid #b0c5df;">
										<input type="hidden" id="txtTargetacctid" />
										<input type="hidden" id="txtTargetscnm" />
										<input type="hidden" id="txtTargetmtcd" name="targetmtcd" />     
										<input type="hidden" id="txtTargetkpinm" />
										<form id="targetSave" name="targetSave">        
											<input type="hidden" id="txtTargetevagbn" name="targetevagbn" />
											<input type="hidden" id="txtTargetscid" name="targetscid" />
											<input type="hidden" id="txtTargetkpiid" name="targetkpiid" />
											<table width="99.8%" cellspacing="0" cellpadding="0" border="0" >
												<thead>
		        									<tr>
		          										<th style="width:20%;"><div>년도</div></th>
		          										<th style="width:40%;"><div>목표값</div></th>
		          										<th style="width:40%;"><div>기준값</div></th>
		          									</tr> 
		          								</thead>
		          								<tbody>
		          									<tr>
			          									<td>
			          										<input type="hidden" id="txtTargetorgyyyy" name="targetorgyyyy" />
			          										<input type="text" class="input type2 t_center num_only" id="txtTargetyyyy" name="targetyyyy" style="width:86%;" />
			          									</td>
			          									<td>
			          										<input type="text" class="input type2 t_right num_only" id="txtTargetamt" name="targetamt" style="width:93%;" />
			          									</td>
			          									<td>
			          										<input type="text" class="input type2 t_right num_only" id="txtTargetcurramt" name="targetcurramt" style="width:93%;" />
			          									</td>
		          									</tr>  
												</tbody>
											</table>
										</form>
									</div>
									<div class="group_button f_right m_t5">
										<div class="button type2 f_left">
											<input type="button" value="신규" id='btnTaregetreset' width="100%" height="100%" onclick="resetTarget();" />
										</div>
										<div class="button type2 f_left">
											<input type="button" value="저장" id='btnTargetinsert' width="100%" height="100%" onclick="insertTarget();" />
										</div>
										<div class="button type3 f_left">
											<input type="button" value="삭제" id='btnTargetremove' width="100%" height="100%" onclick="removeTarget();"/>
										</div>
									</div>
								</div>
							</div>
							<!-- 실적입력 -->
							<div  class="tabs_content" style=" height:100%; ">
								<div class="group f_left m_t5 m_l10">   
									<div class="label type2 f_left">월 : </div> 
									<div class="combobox f_left m_l5" id="cboSearchResultMm" name="searchResultMm" onchange="makeResultList();"></div>
								</div>
								<div class="group f_left m_t5 m_l10">   
									<div class="label type2 f_left">담당자 : </div>          
									<input type="text" id="txtSearchChargenm" name="searchChargenm" class="input type1" style="width:100px; margin:3px 5px;"/>         
								</div>      
								<div class="group f_left m_t5 m_l10">   
									<div class="label type2 f_left">지표명 : </div>          
									<input type="text" id="txtInputSearchKpinm" name="searchInputKpinm" class="input type1" style="width:100px; margin:3px 5px;"/>         
								</div>      
								<div class="group f_right m_t5 m_r10">   
									<div class="label type3 f_left" id="messageLayer"></div>
									<div class="button type2 f_left">
										<input type="button" value="저장" id='btnInsertResult' width="100%" height="100%" onclick="saveKpiResult();"/>
									</div>
								</div>
								
								<div class="blueish datatable f_left" style="width:98%; height:469px; margin:0px 0; margin-left:15px; overflow:hidden;">
									<div class="datatable_fixed" style="width:100%;height:469px; float:left;">        
										<div style=" height:444px !important; overflow-x:hidden;"><!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 600px - 50px= 550px)-->
											<form id="saveResult" name="saveResult" action="./insertResultColValue">
												<input type="hidden" id="resultEvaGbn" name="pEvaGbn" value="${param.pEvaGbn}" />
												<table summary="지표실적입력" style="width:100%;" calss="none_hover none_alt">
													<thead style="width:100%;">
														<tr>
															<th style="width:6%;">조직ID</th>
															<th style="width:10%;">조직명</th>
															<th style="width:24%;">지표명</th>
															<th style="width:6%;">입력담당자</th>
															<th style="width:6%;">확인여부</th>   
															<th style="width:6%;">승인담당자</th>
															<th style="width:6%;">확인여부</th>
															<th style="width:15%;">항목명</th>
															<th style="width:6%;">항목단위</th>
															<th style="width:13%;">항목값</th>   
															<th style="width:1%; min-width:17px;"></th>
														</tr>
													</thead>
													<tbody id="resultInputList">
													</tbody>
												</table>
											</form>
										</div>        
									</div>
								</div>
							</div>
							 <!--*******************지표정의서 시작*********************-->
							<div class="tabs_content" style=" height:100%; ">
								<div class="content f_left" style="width:100%; margin:0 0;">
									<div class="content f_left" style="width:35%; margin:0 1%;"> 
										<div class="grid f_left" style="width:100%; margin:10px 0%; height:495px;">
											<div id="gridKpiReportList"> </div>
										</div>
									</div>
									<div class="content f_right" style="width:61%; margin:0 1%;">
										<div class="group f_left  w100p m_t5">
											<div class="label type2 f_left" style="margin-right: 15px;" id="kpiReportKpiGubn"></div>
											<div class="label type2 f_left" id="kpiCode"><span class="label sublabel"></span></div>
											<div class="button type2 f_right">
												<input type="button" value="지표상세" id='btnKpiDetail' width="100%" height="100%" onclick="getReportDetail();" />
											</div>
										</div>
										<div class="table  f_left m_t5" style="width:100%; margin:0;">
<!-- 											<table width="100%" cellspacing="0" cellpadding="0" border="0" style=" height:470px;"> -->
											<table width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody>
													<tr>
														<th class="w15p"><div>관점</div></th>
														<td class="w10p" ><div class="cell t_center" id="txtPersName"></div></td>
														<th><div>지표입력자</div></th>
														<td class="w10p" style="background:#ffe9b5;"><div class="cell t_center w100p" id="txtKpiChargeName"></div></td>
														<th><div>책임자</div></th>
														<td class="w10p" colspan="2"><div class="cell t_center" id="txtOwnerUserName"></div></td>
														<th><div>측정단위</div></th>
														<td colspan="2" class="w10p" ><div class="cell t_center" id="txtEvalTypeName"></div></td>
													</tr>
													<tr>
														<th><div>산출공식</div></th>
														<td colspan="9"><div class="cell" id="txtCalName"></div></td>
													</tr>
													<tr>
														<th style="height:100px;"><div>세부내용</div></th>
														<td colspan="9" class="v_top p_t5"><div class="cell"><textarea id="txtAreaMtDef" class="textarea type1" style="width:100%; height:98px; margin:3px 0; border:0;" ></textarea></div>
													</td>
													</tr>
													<tr>
														<th><div>고려사항</div></th>
														<td colspan="9"><div class="cell" id="txtTargetDesc"></div></td>
													</tr>
												</tbody>
											</table>
												<table width="100%" cellspacing="0" cellpadding="0" border="0" id="kpiWeightTable">
													<tbody>
														<tr>
															<th class="w15p" rowspan="3"><div>가중치(W1)</div></th>
															<td class="w10p" rowspan="3"><div class="cell t_center" id="txtKpiWeight"></div></td>
															<th rowspan="3"><div>가치형성</div></th>
															<th><div>부서</div></th>
															<td class="w10p"><div class="cell t_center" id="txtKpiDept0"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiDept1"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiDept2"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiDept3"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiDept4"></div></td>
															<td class="w10p"><div class="cell t_center bold">소계</div></td>
														</tr>
														<tr>
															<th><div>비율(C1)</div></th>
															<td class="w10p"><div class="cell t_center" id="txtKpiCurramt0"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiCurramt1"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiCurramt2"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiCurramt3"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiCurramt4"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKprCurramtSum"></div></td>
														</tr>
														<tr>
															<th><div>지표</div></th>
															<td><div class="cell t_center" id="txtKpiID0"></div></td>
															<td><div class="cell t_center" id="txtKpiID1"></div></td>
															<td><div class="cell t_center" id="txtKpiID2"></div></td>
															<td><div class="cell t_center" id="txtKpiID3"></div></td>
															<td><div class="cell t_center" id="txtKpiID4"></div></td>
															<td><div class="cell t_center" ></div></td>
														</tr>
													</tbody>
												</table>
												<table width="100%" cellspacing="0" cellpadding="0" border="0">
													<tbody>
														<tr>
															<th class="w15p"><div>증빙자료</div></th>
															<td colspan="9"><div class="cell" id="txtMtDesc"></div></td>
														</tr>
													</tbody>
												</table>
												<table width="100%" cellspacing="0" cellpadding="0" border="0">
													<tbody>
														<tr>
															<th class="w15p" rowspan="2"><div>측정주기</div></th>
															<td class="w10p" rowspan="2"><div class="cell t_center" id="txtMeasCycleName"></div></td>
															<th rowspan="2"><div>보고시기</div></th>
															<td class="w10p" rowspan="2"><div class="cell t_center" id="txtMmName"></div></td>
															<th rowspan="2"><div>목표</div></th>
															<td class="w10p"><div class="cell t_center bold" id="targetYyyy0"></div></td>
															<td class="w10p"><div class="cell t_center bold" id="targetYyyy1"></div></td>
															<td class="w10p"><div class="cell t_center bold" id="targetYyyy2"></div></td>
															<td class="w10p"><div class="cell t_center bold" id="targetYyyy3"></div></td>
															<td class="w10p"><div class="cell t_center bold" id="targetYyyy4"></div></td>
														</tr>
														<tr>
															<td style="height:40px;"><div class="cell t_center" id="targetValue0"></div></td>
															<td><div class="cell t_center" id="targetValue1"></div></td>
															<td><div class="cell t_center" id="targetValue2"></div></td>
															<td><div class="cell t_center" id="targetValue3"></div></td>
															<td><div class="cell t_center" id="targetValue4"></div></td>
														</tr>
													</tbody>
												</table>
												<table width="100%" cellspacing="0" cellpadding="0" border="0">
													<tbody id="kpiUpmpDetail">
														<tr>
															<th class="w15p" rowspan="3"><div>유관업무</div></th>
															<td colspan="2"><div class="cell"></div></td>
															<td colspan="7"><div class="cell"></div></td>
														</tr>
														<tr>
															<td colspan="2"><div class="cell"></div></td>
															<td colspan="7"><div class="cell"></div></td>
														</tr>
														<tr>
															<td colspan="2"><div class="cell"></div></td>
															<td colspan="7"><div class="cell"></div></td>
														</tr>
													</tbody>
												</table>
											<table width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody>
													<tr>
														<th class="w15p"><div class="cell">평가방법</div></th>
														<td colspan="9"><div class="cell" id="txtKpiEvaTypName"></div></td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<div class="tabs_content" style=" height:100%; ">
								<div id="kpiAttachFileReport" width="100%" height="500px" ></div>
							</div>
							<div class="tabs_content" style=" height:100%; ">
										<div class="content f_left" style="width:100%; margin:0 0;">
											<div class="content f_left" style="width:35%; margin:0 1%;"> 
												<div class="grid f_left" style="width:100%; margin:10px 0%; height:495px;">
													<div id="EvalResultList"> </div>
												</div>
											</div>
											<div class="content f_right" style="width:61%; margin:0 1%;">
												<form id="evalResult" name="evalResult">
														<input type="hidden" id="scIdResult" name="scIdResult" />
														<input type="hidden" id="yyyyResult" name="yyyyResult" />
														<input type="hidden" id="kpiIdResult" name="kpiIdResult" />
														<input type="hidden" id="mtIdResult" name="mtIdResult" />
														<input type="hidden" id="grade" name="grade" />
														<input type="hidden" id="evaGbnResult" name="evaGbnResult" value="${param.pEvaGbn}" />	
													<div class="group f_left  w100p m_t5">
														<div class="label type2 f_left" style="margin-right: 15px;" id="evalResultGubn"></div>
														<div class="label type2 f_left" id="kpiResultCode"><span class="label sublabel"></span></div>
														<div class="group_button f_right m_t5">
															<div class="button type2 f_left">
																<input type="button" value="저장" id="btnevalResult" width="100%" height="100%" onclick="evalResultSave();" />
															</div>
														</div>
													</div>
														<div class="group f_left  w100p m_t5">
															<div class="label type2 f_left">평가근거</div>
															<textarea id="gradeDesc" name="gradeDesc" class="textarea"style=" width:100%; height:170px; "></textarea>
														</div>       
														<!--group-->  
														<div class="group f_left  w100p m_t5">
															<div class="label type2 f_left">권고사항</div>   
															<textarea id="ipvDesc" name="ipvDesc" class="textarea" style=" width:100%; height:170px; "></textarea>
														</div>  
														<!--group-->   
														<div class="group f_left  w100p m_t5">
															<div class="label type2 f_left">평가등급</div>
														</div>
														<!--group-->
														<div style="float:left;margin-top:5px;margin-left:10px;" id="radioList"></div>
													</form>
											</div>	
										</div>
									</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

