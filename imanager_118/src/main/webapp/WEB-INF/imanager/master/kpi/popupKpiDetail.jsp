<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>           
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <title>지표관리</title>
   	 	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
    	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
		<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        	var searchyear="${param.searchyear}";
        	var kpiid="${param.kpiid}";
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){ 
        		var mt_id = kpiid.replace("S","").split("M")[1];
        		var sc_id = kpiid.replace("S","").split("M")[0];
        		
        		getDetailKpiReport({SC_ID:sc_id,MT_ID:mt_id});
        	}); 
        	 
 
        	function getDetailKpiReport(row){
        		if(row == null){
       				$("#txtPersName").html("");
        			$("#txtOwnerUserName").html("");
        			$("#txtKpiChargeName").html("");    
        			$("#txtEvalTypeName").html("");
        			$("#txtCalName").html("");
        			$("#txtAreaMtDef").val("");
        			$("#txtTargetDesc").html("");
        			$("#txtMtDesc").html("");
        			$("#txtKpiEvaTypName").html("");
        			$("#txtMeasCycleName").html("");
        			$("#txtMmName").html("");
        			$("#txtKpiDept0").html("");
       				$("#txtKpiDept1").html("");
       				$("#txtKpiDept2").html("");
       				$("#txtKpiDept3").html("");
       				$("#txtKpiDept4").html("");
       				$("#txtKpiCurramt0").html("");
       				$("#txtKpiCurramt1").html("");
       				$("#txtKpiCurramt2").html("");
       				$("#txtKpiCurramt3").html("");
       				$("#txtKpiCurramt4").html("");
       				$("#txtKpiID0").html("");
       				$("#txtKpiID1").html("");
       				$("#txtKpiID2").html("");
       				$("#txtKpiID3").html("");
       				$("#txtKpiID4").html("");
       				$("#targetYyyy0").html("");
       				$("#targetYyyy1").html("");
       				$("#targetYyyy2").html("");
       				$("#targetYyyy3").html("");
       				$("#targetYyyy4").html("");
       				$("#targetValue0").html("");
       				$("#targetValue1").html("");
       				$("#targetValue2").html("");
       				$("#targetValue3").html("");
       				$("#targetValue4").html("");
       				$("#txtKpiWeight").html("");
       				$("#kpiUpmpDetail tr").remove();
       				var relatedWorkHtml = "";
       				relatedWorkHtml += "<tr>";
					relatedWorkHtml += "<th class='w15p'>유관업무</th>";
					relatedWorkHtml += "<td colspan='2'><div class='cell'></div></td>";
					relatedWorkHtml += "<td colspan='7'><div class='cell'></div></td>";
					relatedWorkHtml += "</tr>";
					$("#kpiUpmpDetail").append(relatedWorkHtml);
					$("#kpiReportKpiGubn").html("");
        			$("#kpiCode").html("");
        		}else{
	        		var mtid = row.MT_ID;
	        		var scid = row.SC_ID; 
	        		var kpiData = $i.sqlhouse(273,{S_MT_ID:mtid, S_YYYY:searchyear});
	        		kpiData.done(function(kpi){
	        			if(kpi.returnArray!=null&&kpi.returnArray.length > 0){
	        				$("#txtPersName").html(kpi.returnArray[0].PERS_NM);
		        			$("#txtOwnerUserName").html(kpi.returnArray[0].OWNER_USER_NM);
		        			$("#txtKpiChargeName").html(kpi.returnArray[0].KPI_CHARGE_NM);    
		        			$("#txtEvalTypeName").html(kpi.returnArray[0].EVAL_TYPE_NM);
		        			$("#txtCalName").html(kpi.returnArray[0].CAL_NM);
		        			$("#txtAreaMtDef").val(kpi.returnArray[0].MT_DEF);
		        			$("#txtTargetDesc").html(kpi.returnArray[0].TARGET_DESC);
		        			$("#txtMtDesc").html(kpi.returnArray[0].MT_DESC);
		        			$("#txtKpiEvaTypName").html(kpi.returnArray[0].EVA_TYP_NM);
		        			$("#txtMeasCycleName").html(kpi.returnArray[0].MEAS_CYCLE_NM);
		        			$("#txtMmName").html(kpi.returnArray[0].MM_NM);
		        			$("#txtKpiWeight").html(kpi.returnArray[0].KPI_WEIGHT);
		        			if(kpi.returnArray[0].SC_ID == "100"){ 
		        				$("#kpiReportKpiGubn").html("대학 KPI");
		        				$("#kpiWeightTable").show();
			        		}else{
			        			$("#kpiReportKpiGubn").html("부서 KPI");
			        			$("#kpiWeightTable").hide();
			        		}
		        			$("#kpiCode").html(kpi.returnArray[0].MT_CD+"<span class='label sublabel'>"+kpi.returnArray[0].MT_NM+"</span>");
	        			}else{
	        				$("#txtPersName").html("");
		        			$("#txtOwnerUserName").html("");
		        			$("#txtKpiChargeName").html("");    
		        			$("#txtEvalTypeName").html("");
		        			$("#txtCalName").html("");
		        			$("#txtAreaMtDef").val("");
		        			$("#txtTargetDesc").html("");
		        			$("#txtMtDesc").html("");
		        			$("#txtKpiEvaTypName").html("");
		        			$("#txtMeasCycleName").html("");
		        			$("#txtMmName").html("");
	        			}
	        		});
	        		var kpiCurramtDate = $i.sqlhouse(274, {S_MT_ID:mtid, S_YYYY:searchyear});
	        		kpiCurramtDate.done(function(curramt){
	        			$("#txtKpiDept0").html("");
        				$("#txtKpiDept1").html("");
        				$("#txtKpiDept2").html("");
        				$("#txtKpiDept3").html("");
        				$("#txtKpiDept4").html("");
        				$("#txtKpiCurramt0").html("");
        				$("#txtKpiCurramt1").html(""); 
        				$("#txtKpiCurramt2").html("");
        				$("#txtKpiCurramt3").html("");
        				$("#txtKpiCurramt4").html("");
        				$("#txtKpiID0").html("");
        				$("#txtKpiID1").html("");
        				$("#txtKpiID2").html("");
        				$("#txtKpiID3").html("");
        				$("#txtKpiID4").html("");
        				$("#txtKprCurramtSum").html("");
        				
	        			if(curramt.returnArray!=null&&curramt.returnArray.length > 0){
	        				var sum = "0";
	        				for(var i=0;i<curramt.returnArray.length;i++){
	        					if(curramt.returnArray[i].KPI_CURRAMT != ""){
	        						$("#txtKpiDept"+i).html(curramt.returnArray[i].SC_NM);
		        					$("#txtKpiCurramt"+i).html(parseInt(curramt.returnArray[i].KPI_CURRAMT)+"%");
		        					$("#txtKpiID"+i).html(curramt.returnArray[i].MT_CD);
		        					sum = parseInt(parseInt(sum)+parseInt(curramt.returnArray[i].KPI_CURRAMT));
	        					}
	        				}
	        				$("#txtKprCurramtSum").html(sum+"%");
	        			}
	        		});
	        		var kpiTargetDate = $i.sqlhouse(275, {S_MT_ID:mtid});
	        		kpiTargetDate.done(function(target){
	        			$("#targetYyyy0").html("");
        				$("#targetYyyy1").html("");
        				$("#targetYyyy2").html("");
        				$("#targetYyyy3").html("");
        				$("#targetYyyy4").html("");
        				$("#targetValue0").html("");
        				$("#targetValue1").html("");
        				$("#targetValue2").html("");
        				$("#targetValue3").html("");
        				$("#targetValue4").html("");
	        			if(target.returnArray!=null&&target.returnArray.length > 0){
	        				for(var k=0;k < 5;k++){
	        					if(target.returnArray[k] == null){
	        						if(i!=0){
	        							$("#targetYyyy"+k).html(parseInt(target.returnArray[0].YYYY)+k);
		        						$("#targetValue"+k).html("");
	        						}
	        					}else{
		        					$("#targetYyyy"+k).html(parseInt(target.returnArray[0].YYYY)+k);
		        					$("#targetValue"+k).html(target.returnArray[k].TARGET_AMT);	
	        					}
	        				}
	        			}
	        		});
	        		var upmuData = $i.sqlhouse(276,{S_MT_ID:mtid});
	       			upmuData.done(function(umpu){
	       				var relatedWorkHtml = "";
	       				$("#kpiUpmpDetail tr").remove();
	       				if(umpu.returnArray!=null&&umpu.returnArray.length>0){
	       					for(var j=0;j<umpu.returnArray.length;j++){
	       						if(j == 0){
	       							relatedWorkHtml += "<tr>";
	       							relatedWorkHtml += "<th class='w15p' rowspan='"+umpu.returnArray.length+1+"'>유관업무</th>";
	       							relatedWorkHtml += "<td colspan='2'><div class='cell'>"+umpu.returnArray[j].RELATED_WORK1+"</div></td>";
	       							relatedWorkHtml += "<td colspan='7'><div class='cell'>"+umpu.returnArray[j].RELATED_WORK2+"</div></td>";
	       							relatedWorkHtml += "</tr>";
	       						}else{
	       							relatedWorkHtml += "<tr>";
	       							relatedWorkHtml += "<td colspan='2'><div class='cell'>"+umpu.returnArray[j].RELATED_WORK1+"</div></td>";
	       							relatedWorkHtml += "<td colspan='7'><div class='cell'>"+umpu.returnArray[j].RELATED_WORK2+"</div></td>";
	       							relatedWorkHtml += "</tr>";
	       						}
	       					}
	       				}else{
	       					relatedWorkHtml += "<tr>";
							relatedWorkHtml += "<th class='w15p'>유관업무</th>";
							relatedWorkHtml += "<td colspan='2'><div class='cell'></div></td>";
							relatedWorkHtml += "<td colspan='7'><div class='cell'></div></td>";
							relatedWorkHtml += "</tr>";
	       				}
	       				$("#kpiUpmpDetail").append(relatedWorkHtml);
	       			});
        		}
        	} 	

	    </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			} 
			iframe {
				background:transparent;
			}     
		</style>
		      
    </head>
    <body class='blueish' style="overflow-y: hidden; overflow-x: hidden;">    
		<div class="wrap" style="width:98%;margin-left:10px;">   
								<div class="content f_left" style="width:100%;">  
									<div class="group f_left  w100p m_t5"> 
										<div class="label type2 f_left" style="margin-right: 15px;" id="kpiReportKpiGubn"></div>
										<div class="label type2 f_left" id="kpiCode"><span class="label sublabel"></span></div> 
									</div>        
									<div class="table  f_left m_t5" style="width:100%; margin:0;">
											<table width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody>
													<tr>
														<th class="w15p"><div>관점</div></th>
														<td class="w10p" ><div class="cell t_center" id="txtPersName"></div></td>
														<th><div>지표입력자</div></th>
														<td class="w10p" style="background:#ffe9b5;"><div class="cell t_center w100p" id="txtKpiChargeName"></div></td>
														<th><div>책임자</div></th>
														<td class="w10p" colspan="2"><div class="cell t_center" id="txtOwnerUserName"></div></td>
														<th><div>측정단위</div></th>
														<td colspan="2" class="w10p" ><div class="cell t_center" id="txtEvalTypeName"></div></td>
													</tr>
													<tr>
														<th><div>산출공식</div></th>
														<td colspan="9"><div class="cell" id="txtCalName"></div></td>
													</tr>
													<tr>
														<th style="height:100px;"><div>세부내용</div></th>
														<td colspan="9" class="v_top p_t5"><div class="cell"><textarea id="txtAreaMtDef" class="textarea type1" style="width:100%; height:98px; margin:3px 0; border:0;" ></textarea></div>
													</td>
													</tr>
													<tr>
														<th><div>고려사항</div></th>
														<td colspan="9"><div class="cell" id="txtTargetDesc"></div></td>
													</tr>
												</tbody>
											</table>
											<table width="100%" cellspacing="0" cellpadding="0" border="0" id="kpiWeightTable">
													<tbody>
														<tr>
															<th class="w15p" rowspan="3"><div>가중치(W1)</div></th>
															<td class="w10p" rowspan="3"><div class="cell t_center" id="txtKpiWeight"></div></td>
															<th rowspan="3"><div>가치형성</div></th>
															<th><div>부서</div></th>
															<td class="w10p"><div class="cell t_center" id="txtKpiDept0"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiDept1"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiDept2"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiDept3"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiDept4"></div></td>
															<td class="w10p"><div class="cell t_center bold">소계</div></td>
														</tr>
														<tr>
															<th><div>비율(C1)</div></th>
															<td class="w10p"><div class="cell t_center" id="txtKpiCurramt0"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiCurramt1"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiCurramt2"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiCurramt3"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKpiCurramt4"></div></td>
															<td class="w10p"><div class="cell t_center" id="txtKprCurramtSum"></div></td>
														</tr>
														<tr>
															<th><div>지표</div></th>
															<td><div class="cell t_center" id="txtKpiID0"></div></td>
															<td><div class="cell t_center" id="txtKpiID1"></div></td>
															<td><div class="cell t_center" id="txtKpiID2"></div></td>
															<td><div class="cell t_center" id="txtKpiID3"></div></td>
															<td><div class="cell t_center" id="txtKpiID4"></div></td>
															<td><div class="cell t_center" ></div></td>
														</tr>
													</tbody>
											</table>						
											<table width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody>
													<tr>
														<th class="w15p"><div>증빙자료</div></th>
														<td colspan="9"><div class="cell" id="txtMtDesc"></div></td>
														</tr>
												</tbody>    
											</table>
												<table width="100%" cellspacing="0" cellpadding="0" border="0">
													<tbody>
														<tr>
															<th class="w15p" rowspan="2"><div>측정주기</div></th>
															<td class="w10p" rowspan="2"><div class="cell t_center" id="txtMeasCycleName"></div></td>
															<th rowspan="2"><div>보고시기</div></th>
															<td class="w10p" rowspan="2"><div class="cell t_center" id="txtMmName"></div></td>
															<th rowspan="2"><div>목표</div></th>
															<td class="w10p"><div class="cell t_center bold" id="targetYyyy0"></div></td>
															<td class="w10p"><div class="cell t_center bold" id="targetYyyy1"></div></td>
															<td class="w10p"><div class="cell t_center bold" id="targetYyyy2"></div></td>
															<td class="w10p"><div class="cell t_center bold" id="targetYyyy3"></div></td>
															<td class="w10p"><div class="cell t_center bold" id="targetYyyy4"></div></td>
														</tr>
														<tr>
															<td style="height:40px;"><div class="cell t_center" id="targetValue0"></div></td>
															<td><div class="cell t_center" id="targetValue1"></div></td>
															<td><div class="cell t_center" id="targetValue2"></div></td>
															<td><div class="cell t_center" id="targetValue3"></div></td>
															<td><div class="cell t_center" id="targetValue4"></div></td>
														</tr>
													</tbody>
												</table>
												<table width="100%" cellspacing="0" cellpadding="0" border="0">
													<tbody id="kpiUpmpDetail">
														<tr>
															<th class="w15p" rowspan="3"><div>유관업무</div></th>
															<td colspan="2"><div class="cell"></div></td>
															<td colspan="7"><div class="cell"></div></td>
														</tr>
														<tr>
															<td colspan="2"><div class="cell"></div></td>
															<td colspan="7"><div class="cell"></div></td>
														</tr>
														<tr>
															<td colspan="2"><div class="cell"></div></td>
															<td colspan="7"><div class="cell"></div></td>
														</tr>
													</tbdoy>
												</tbody>
											<table width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody>
													<tr>
														<th class="w15p"><div class="cell">평가방법</div></th>
														<td colspan="9"><div class="cell" id="txtKpiEvaTypName"></div></td>
													</tr>
												</tbody>
											</table>
									</div> 
								</div> 
								 
						</div>
		
	</body>
</html>