<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>학과리스트</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
	    <script src="../../resources/cmresource/js/thirdparty/MIT/dtree/dtree.js"></script>
        <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){        		
        		
        		$("#btnInsertlist").jqxButton({width:'', theme:'blueish'});
        		$("#btnSearch").jqxButton({width:'', theme:'blueish'});
        		
				$("#txtSearch").jqxInput({placeHolder: "", height: 23, width: 250, minLength: 1, theme:'blueish' });
				$("#txtSearch").on("keypress",function(event){
					if(event.keyCode=="13") 
						makeGrid();  
				});
        		init();
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		search();
        		makeGrid(); 
        	}
        	//조회
        	function search(){
        		  
        	}
        	//입력
        	function insert(){
        		
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function makeGrid(){
        		var acctid = "${param.acctid}";
				var yyyy = "${param.yyyy}";
				var mtid = "${param.mtid}";
        		$("#gridScidlist").jqxGrid("clearselection");
				var dat = $i.sqlhouse(138,{S_ACCT_ID:acctid, S_SC_NM:$("#txtSearch").val(),S_YYYY:yyyy,S_MT_ID:mtid,S_USE_YN:''});
        		dat.done(function(res){
					var source = 
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'SC_ID', type: 'string'},
		                    { name: 'SC_NM', type: 'string'},
							{ name: 'PSC_ID', type: 'string'},
							{ name: 'PSC_NM', type: 'string'},
							{ name: 'USE_YN', type: 'string'} 
		                ],
		                localdata: res.returnArray,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridScidlist").jqxGrid(
		            {
		                width: '100%',
		                height: '100%',
						altrows:true,
						pageable: false,
						sortable:true,
		                source: dataAdapter,
						theme:'blueish',
						columnsheight:25 ,
						rowsheight: 25,
		                columnsresize: true,
						selectionmode: 'checkbox',
		                columns: [
		                  { text: 'ID', datafield: 'SC_ID', width: 86, align:'center', cellsalign: 'center'},
		                  { text: '학과명', datafield: 'SC_NM', width: 247, align:'center', cellsalign: 'left', cellsrenderer: alginLeft },
						  { text: '상위부서', datafield: 'PSC_NM', align:'center', cellsalign: 'right', cellsrenderer: alginLeft }
		                ]
		            });
		            for(var k=0;k<$("#gridScidlist").jqxGrid("getRows").length;k++){
	            		if($("#gridScidlist").jqxGrid("getRows")[k].USE_YN =="Y"){
	            			$("#gridScidlist").jqxGrid("selectrow",k);
	            		} 
	            	}
        		});
        	}
        	function insertGrid(){
        		var selectItem = $("#gridScidlist").jqxGrid('getselectedrowindexes');
        		var grid = $("#gridScidlist");        		
        		var arr = [];
        		for(var i=0;i<selectItem.length;i++){
        			var rowid = grid.jqxGrid("getrowid",selectItem[i]);
        			var rowdata = grid.jqxGrid("getrowdatabyid",rowid);
        			if(rowdata.EVAL_G != "Z"){
        				arr.push({
        					SC_ID:rowdata.SC_ID,
        					SC_NM:rowdata.SC_NM
        				});
        			}
        		} 
        		var openertr = $("#tblValueDetail", opener.document).find("tr[data-role=valueDetailRow][data-scid=SUST]");
        		
        		$("#hiddenTargetSustList", opener.document).val(JSON.stringify(arr)) ;
        		openertr.find("span[name=targetCount]").html(arr.length);
        		opener.valueWeightCalculate(openertr.attr("data-idx"));
        		
				self.close();
        	}
        	function chkGbnFunction(group, idx) {
				var gbn = "groupChk_" + idx;
				var chkGbn = $("input:checkbox[name='groupChk']");
				if (chkGbn != null){
					$(chkGbn).each(function(i) {
						if(chkGbn.eq(i).attr("id") == gbn){
							chkGbn.eq(i).prop("checked", $(group).is(":checked") ? true : false);
							lowLevelChk(chkGbn.eq(i).val(), $(group).is(":checked"));
						}
					});
				}			
			}
		 	function singleCheck(x, group,id) {
			    if($(x).css("font-weight") != "bold"){
			    	var chkGbn =  $("input:checkbox[id='" + group + "']");
			    	for(var i=0;i<chkGbn.length;i++) {
						if(chkGbn.eq(i).val() == id) {
							chkGbn.eq(i).prop("checked", (chkGbn.eq(i).is(":checked")) ? false : true);
							return;
						}    
					}
			    }
			}
		 	function lowLevelChk(gbn, checked){//체크박스 제귀함수   
				var chkGbn = $("input:checkbox[name='groupChk']");
				if($("#groupChk_" + gbn).length > 0){
					$(chkGbn).each(function(i) {
						if(chkGbn.eq(i).attr("id") == "groupChk_" + gbn){
							chkGbn.eq(i).prop("checked", checked);
							lowLevelChk(chkGbn.eq(i).val(), checked);
						}
					});
				}
			}
        	function calcodNew(calcod){
        		location.replace("./popupMappingKpiFormula?mode=new&mtid=${param.mtid}&calcod="+calcod+"&acctid=${param.acctid}");
        	}
        	function sendParentData(scId, scNm){
				$("#txtPscid", opener.document).val(scId) ;
				$("#txtPscnm", opener.document).val(scNm) ;
				self.close();
        	}
        </script> 
    </head>
    <body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:600px; min-width:600px; margin:0 1%;">
			<div class="container  f_left" style="width:100%; margin:10px 0;"> 
				<div class="content f_left" style=" width:100%; margin-right:0;">
					<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
									<div class="label type1 f_left">학과명 :</div>
  									<input type="text" value="" id="txtSearch" name="searchscnm" class="input type1 f_left"  style="width:57%; margin:3px 5px; "/>
       								<div class="button type2 f_left">
       									<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="makeGrid();" />
     								</div>
     								<div class="button type2 f_left">
       									<input type="button" value="저장" id='btnInsertlist' width="100%" height="100%" onclick="insertGrid();" />
     								</div>
					</div> 
					<div class="grid f_left" style="width:100%; margin:10px 0; height:480px;"> 
	 								<div id="gridScidlist"></div>
					</div> 
				</div>
			</div>
		</div>
	</body>
</html>

