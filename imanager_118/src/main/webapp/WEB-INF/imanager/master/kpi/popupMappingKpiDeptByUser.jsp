<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>조직리스트</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
	    <script src="../../resources/cmresource/js/thirdparty/MIT/dtree/dtree.js"></script>
        <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$('#jqxTabs01').jqxTabs({ width: '100%', height: '100%', position: 'top', theme:'blueish', selectedItem: 0});
        		$("#btnInserttree").jqxButton({width:'', theme:'blueish'});
        		$("#btnInsertlist").jqxButton({width:'', theme:'blueish'});
        		$("#btnSearch").jqxButton({width:'', theme:'blueish'});
        		
				$("#txtSearch").jqxInput({placeHolder: "", height: 23, width: 250, minLength: 1, theme:'blueish' });
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		search();
        		makeGrid();
        	}
        	//조회
        	function search(){
        		var acctid = "${param.acctid}";
				var yyyy = "${param.yyyy}";
				var dat = $i.sqlhouse(223,{ACCT_ID:acctid, SC_NM:"", YYYY:yyyy});
        		dat.done(function(res){
        			dtree = new dTree('dtree');
					dtree.config.useCookie = true;		// Cookie 로 Tree 형태 저장.
					dtree.add(0, -1, "");// Root 생성
					
					var ancestorStoreID;
					for (var i=0; i<res.returnArray.length; i++) {
						
						ancestorStoreID = res.returnArray[i].PSC_ID; 
						
						$("#treeScidlist").css("text-align", "left");
						$("#treeScidlist").css("overflow-y:", "auto");
						$("#treeScidlist").css("height", "515px");
						$("#treeScidlist").css("border", "1px solid #B2B2B2");

						dtree.add(res.returnArray[i].SC_ID, ancestorStoreID, "<input type='checkbox' name='groupChk' style='height:12px;margin-right:2px;vertical-align: middle' evalG='" + res.returnArray[i].EVAL_G + "' scId='" + res.returnArray[i].SC_ID + "' scNm='" + res.returnArray[i].SC_NM + "' id='groupChk_" + ancestorStoreID + "' onclick=\"javascript:chkGbnFunction(this,'" + res.returnArray[i].SC_ID + "');\" value='" + res.returnArray[i].SC_ID + "' ><a href='javascript:void(0);'  id='" + res.returnArray[i].SC_ID + "' onClick=\"singleCheck(this,'groupChk_" + ancestorStoreID + "','"+res.returnArray[i].SC_ID+"');\" style='font-size:12px; vertical-align:middle;'>" + res.returnArray[i].SC_NM + "</a>");
					}
					document.getElementById('treeScidlist').innerHTML = dtree;
					$(".clip").css("width", "300px");
					var sourceData = "${param.orgscid}";
					for(var i=0;i<sourceData.split(",").length;i++){
		            	$("input:checkbox[name='groupChk']").each(function() {
				    		if($(this).attr("scId") == sourceData.split(",")[i]){
				    			this.checked = true;
				    		}
				    	});
		            }
					//$("#totSrcNum").text(res.length);
					$("#labelSize").text(res.returnArray.length);
					
        		});
        	}
        	//입력
        	function insert(){
        		var scid = "";
        		var scnm = "";
        		//체크 되었고 평가대상인 부서들 리스트만 가져오도록 .
        		$("input:checkbox[evalG!=Z][name='groupChk']:checked").each(function(i){
        			if(i!=0){ 
    					scid += ",";
    					scnm += ",";
    				}
    				scid  += $(this).attr("scId");
    				scnm += $(this).attr("scNm")+"["+$(this).attr("scId")+"]";
        		});
        		$("#txtScid", opener.document).val(scid);
				$("#txtareaScnm", opener.document).val(scnm) ;
				self.close();
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function makeGrid(){
        		var acctid = "${param.acctid}";
				var yyyy = "${param.yyyy}";
        		$("#gridScidlist").jqxGrid("clearselection");
				var dat = $i.sqlhouse(223,{ACCT_ID:acctid, SC_NM:$("#txtSearch").val(),YYYY:yyyy});
        		dat.done(function(res){
					var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'SC_ID', type: 'string'},
		                    { name: 'SC_NM', type: 'string'},
							{ name: 'PSC_ID', type: 'string'},
							{ name: 'PSC_NM', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridScidlist").jqxGrid(
		            {
		                width: '100%',
		                height: '100%',
						altrows:true,
						pageable: false,
						sortable:true,
		                source: dataAdapter,
						theme:'blueish',
						columnsheight:25 ,
						rowsheight: 25,
		                columnsresize: true,
						selectionmode: 'checkbox',
		                columns: [
		                  { text: 'ID', datafield: 'SC_ID', width: 86, align:'center', cellsalign: 'center'},
		                  { text: '조직명', datafield: 'SC_NM', width: 247, align:'center', cellsalign: 'left', cellsrenderer: alginLeft },
						  { text: '상위부서', datafield: 'PSC_NM', align:'center', cellsalign: 'right', cellsrenderer: alginLeft }
		                ]
		            });
	            	var sourceData = "${param.orgscid}";
		            for(var j=0;j<sourceData.split(",").length;j++){
		            	for(var k=0;k<$("#gridScidlist").jqxGrid("getRows").length;k++){
		            		if($("#gridScidlist").jqxGrid("getRows")[k].SC_ID == sourceData.split(",")[j]){
		            			$("#gridScidlist").jqxGrid("selectrow",k);
		            		}
		            	}
		            }
        		});
        	}
        	function insertGrid(){
        		var selectItem = $("#gridScidlist").jqxGrid('getselectedrowindexes');
        		var grid = $("#gridScidlist");
        		var scid = "";
        		var scnm = "";
        		for(var i=0;i<selectItem.length;i++){
        			var rowid = grid.jqxGrid("getrowid",selectItem[i]);
        			var rowdata = grid.jqxGrid("getrowdatabyid",rowid);
        			if(rowdata.EVAL_G != "Z"){
        				if(i!=0){
        					scid += ",";
        					scnm += ",";
        				}        				
        				scid += rowdata.SC_ID;
        				scnm += rowdata.SC_NM+"["+rowdata.SC_ID+"]"; 
        			}
        		}
        		$("#txtScid", opener.document).val(scid) ;
				$("#txtareaScnm", opener.document).val(scnm) ;
				self.close();
        	}
        	function chkGbnFunction(group, idx) {
				var gbn = "groupChk_" + idx;
				var chkGbn = $("input:checkbox[name='groupChk']");
				if (chkGbn != null){
					$(chkGbn).each(function(i) {
						if(chkGbn.eq(i).attr("id") == gbn){
							chkGbn.eq(i).prop("checked", $(group).is(":checked") ? true : false);
							lowLevelChk(chkGbn.eq(i).val(), $(group).is(":checked"));
						}
					});
				}			
			}
		 	function singleCheck(x, group,id) {
			    if($(x).css("font-weight") != "bold"){
			    	var chkGbn =  $("input:checkbox[id='" + group + "']");
			    	for(var i=0;i<chkGbn.length;i++) {
						if(chkGbn.eq(i).val() == id) {
							chkGbn.eq(i).prop("checked", (chkGbn.eq(i).is(":checked")) ? false : true);
							return;
						}    
					}
			    }
			}
		 	function lowLevelChk(gbn, checked){//체크박스 제귀함수   
				var chkGbn = $("input:checkbox[name='groupChk']");
				if($("#groupChk_" + gbn).length > 0){
					$(chkGbn).each(function(i) {
						if(chkGbn.eq(i).attr("id") == "groupChk_" + gbn){
							chkGbn.eq(i).prop("checked", checked);
							lowLevelChk(chkGbn.eq(i).val(), checked);
						}
					});
				}
			}
        	function calcodNew(calcod){
        		location.replace("./popupMappingKpiFormula?mode=new&mtid=${param.mtid}&calcod="+calcod+"&acctid=${param.acctid}");
        	}
        	function sendParentData(scId, scNm){
				$("#txtPscid", opener.document).val(scId) ;
				$("#txtPscnm", opener.document).val(scNm) ;
				self.close();
        	}
        </script>
    </head>
    <body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:600px; min-width:600px; margin:0 1%;">
			<div class="container  f_left" style="width:100%; margin:10px 0;"> 
				<div class="content f_left" style=" width:100%; margin-right:0;">
					<div class="tabs f_left" style=" width:100%; height:580px; margin-top:0px;">
						<div id='jqxTabs01'>
  							<ul>
    							<li style="margin-left: 0px;">조직도</li>
  								<li>리스트</li>
							</ul>
    						<div class="tabs_content hidden" style="height:100%; ">
								<div class="group" style="width:96%; margin:10px auto;">
									<div class="label type2 f_left m_r10">대상조직:<span class="label sublabel" id="labelSize"></span></div>
									<div class="group_button f_right">
    									<div class="button type2 f_left">  
        									<input type="button" value="저장" id='btnInserttree' width="100%" height="100%" onclick="insert();" />
    									</div>
									</div>
								</div>
								<div class="tree" style="border:none;  width:96% !important; margin:6% 2%; height:510px !important; overflow:auto;" id='treeScidlist'></div>
							</div>
							<div  class="tabs_content" style=" height:100%; ">
								<div class="group" style="width:96%; margin:10px 2%;">
									<div class="label type2 f_left">조직명</div>
  									<input type="text" value="" id="txtSearch" name="searchscnm" class="input type1 f_left"  style="width:57%; margin:3px 5px; "/>
       								<div class="button type2 f_left">
       									<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="makeGrid();" />
     								</div>
     								<div class="button type2 f_left">
       									<input type="button" value="저장" id='btnInsertlist' width="100%" height="100%" onclick="insertGrid();" />
     								</div>
								</div>
								<div class="grid f_left" style="width:96%; margin:10px 2%; height:480px;">
	 								<div id="gridScidlist"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

