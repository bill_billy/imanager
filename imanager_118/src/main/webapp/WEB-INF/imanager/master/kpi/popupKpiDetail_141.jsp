<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>지표정의서</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        	var drawcnt = 0;
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$("#txtSearch").jqxInput({placeHolder: "", height: 22, width: 150, minLength: 1,  theme:'blueish' });
        		$("#btnSearch").jqxButton({ width: '',  theme:'blueish'}); 
// 				$("#btnPrint").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnClose").jqxButton({ width: '',  theme:'blueish'});
        		init();
        	}); 
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		var dat = $i.sqlhouse(1,{COML_COD:"USE_YN"});
        		dat.done(function(res){
        			var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                        { name: 'COM_COD' },
	                        { name: 'COM_NM' }
	                    ],
	                    id: 'id',
						localdata:res,
						// url: url,
	                    async: false
	                };
	                var dataAdapter = new $.jqx.dataAdapter(source);
	                $("#cboSearchyn").jqxComboBox({selectedIndex: 0, source: dataAdapter,animationType: 'fade',dropDownHorizontalAlignment: 'right',displayMember: "COM_NM",valueMember: "COM_COD",dropDownWidth: 80,dropDownHeight: 60,width: 80,height: 23,theme:'blueish'});
	                if("${param.useyn}" != ""){
	                	$("#cboSearchyn").val("${param.useyn}");
	                }
        		});
        		getDetailKpi();
        	}
        	//조회
        	function search(){
        		$("#hiddenMtid").val(" ");
        		$("#searchForm").submit();
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	
        	//테스트 함수.
        	function test(){ 
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function searchOne(mtid){
        		$("#hiddenMtid").val(mtid);
        		$("#searchForm").submit();
        	}
        	function getDetailKpi(){
        		var mtList = $("#content-cneter").find("input[name=pmtId]");
        		var acctid = "${param.acctid}";
        		for(var i=0;i<mtList.length;i++){
        			var mtItem = mtList[i];
        			var data = $i.sqlhouse(264,{S_MT_ID:mtItem.value});
        			data.done(function(res){
        				if(res.returnArray.length>0){
        					var relatedWorkHtml = "";
        					for(var j=0;j<res.returnArray.length;j++){
        						if(j == 0){
        							relatedWorkHtml += "<tr>";
        							relatedWorkHtml += "<th rowspan='"+res.returnArray.length+1+"'>유관업무</th>";
        							relatedWorkHtml += "<td style='padding:0 2%;' colspan='2'>"+res.returnArray[j].RELATED_WORK1+"</td>";
        							relatedWorkHtml += "<td colspan='7'><textArea style='width:96%;height:100px; margin:0 2%;border:0;' readonly>"+res.returnArray[j].RELATED_WORK2+"</textArea></td>";
        							relatedWorkHtml += "</tr>";
        						}else{
        							relatedWorkHtml += "<tr>";
        							relatedWorkHtml += "<td style='padding:0 2%;' colspan='2'>"+res.returnArray[j].RELATED_WORK1+"</td>";
        							relatedWorkHtml += "<td colspan='7'><textArea style='width:96%;height:100px; margin:0 2%;border:0;' readonly>"+res.returnArray[j].RELATED_WORK2+"</textArea></td>";
        							relatedWorkHtml += "</tr>";
        						}
        					}
        				}
        				if(res.returnArray.length != 0) $("#relatedWorkTable"+res.returnArray[0].MT_ID).append(relatedWorkHtml);
        			});
        			var mmData = $i.sqlhouse(265, {S_MT_ID:mtItem.value});
        			mmData.done(function(dat){
        				if(dat.returnArray.length>0){
        					for(var k=0;k<dat.returnArray.length;k++){
        						$("#yyyy"+dat.returnArray[0].MT_ID+k).html(dat.returnArray[k].YYYY);
        						$("#target"+dat.returnArray[0].MT_ID+k).html(dat.returnArray[k].TARGET_AMT);
        					}
        				}
        			}); 
        			var kpiCurramtDate = $i.sqlhouse(266, {S_MT_ID:mtItem.value, S_YYYY:"${param.pYyyy}"});
        			kpiCurramtDate.done(function(kpi){
        				if(kpi.returnArray.length > 0){
        					var sum = "0";
        					for(var m=0;m<kpi.returnArray.length;m++){
        						if(kpi.returnArray[m].KPI_CURRAMT != ""){
        							$("#dept"+kpi.returnArray[0].MT_ID+m).html(kpi.returnArray[m].SC_NM);
        							$("#value"+kpi.returnArray[0].MT_ID+m).html(kpi.returnArray[m].KPI_CURRAMT);
        							sum = parseInt(parseInt(sum)+parseInt(kpi.returnArray[m].KPI_CURRAMT));
        						}
        					}
        					$("#sumTotal"+kpi.returnArray[0].MT_ID).html(sum);
        				}
        			});
        		}
        		
        	}
        	function printGrid(){
        		$("#content-cneter").printElement({
       				printMode:'popup',
       				overrideElementCSS:[//"${WWW.CSS}/iplanbiz/iplanbiz.print.base.css",
       				                    {href:"${WWW.CSS}/iplanbiz/iplanbiz.print.base.css",media:'all'}
       				],
       				pageTitle:'부서별 지표정의서',
       				leaveOpen:false
       			});
        	}
        </script>
        <style>
        	table {
        		table-layout: fixed; /*테이블 내에서 <td>의 넓이,높이를 고정한다.*/
        		border:1px;  
        		border-color:black;
        		border-style:solid;    
        		font-family:'돋움',Dotum,Helvetica,'Apple SD Gothic Neo',Sans-serif; 
        		
        	}
        	.sql{ 
        			width:100%;
        			overflow: hidden;
			    	text-overflow:ellipsis; /*overflow: hidden; 속성과 같이 써줘야 말줄임 기능이 적용된다.*/
			    	white-space:nowrap;
        	}
        </style>
    </head>
    <body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 1%;">
			<div class="group f_left  w100p">
				<div class="label  f_left m_t10 bold"><img src="../../resources/cmresource/image/icon-list.png" alt="icon-list" class="f_left m_r3">지표정의서</div>
			</div>
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<form id="searchForm" name="searchForm" action="./popupKpiDetail_141">
					<input type="hidden" id="hiddenMtid" name="mtid" />
					<input type="hidden" name="pYyyy" value="${param.pYyyy}" />
					<div class="label type1 f_left">사용여부:</div>
					<div class="combobox f_left" id="cboSearchyn" name="useyn"></div>
					<div class="label type1 f_left">지표:</div>
					<div class="input f_left">
						<input type="text" id="txtSearch" name="searchmtnm" value="${param.searchmtnm}" />
					</div>
					<div class="combobox f_left"  id='jqxCombobox01' ></div>
					<div class="group_button f_right">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
<!-- 						<div class="button type1 f_left"> -->
<!-- 							<input type="button" value="인쇄" id='btnPrint' width="100%" height="100%" /> -->
<!-- 						</div> -->
						<div class="button type1 f_left">
							<input type="button" value="닫기" id='btnClose' width="100%" height="100%" />
						</div>
					</div>
				</form>
			</div>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content" style=" width:100%;">
					<div class="group f_right w100p" style="margin:-10px 0 10px 0;">
						<div class="label type3 f_right">Internet explorer의 경우 다른이름으로 저장하시기 바랍니다. 확장자를 확인해주세요.</div>
					</div>
					<div class="content f_left" style=" width:39%; margin-right:1%;">
						<div class="table_title" style="width:100%;height:32px;margin:0;">
							<h1 class="f_left">지표리스트</h1>
						</div>
						<div class="datatable f_left auto" style="width:100%; height:530px; margin:0px 0; border-top:none;"><!--border-top만  인라인으로 빼야함-->
							<table width="100%" cellspacing="0" cellpadding="0" border="0" >
								<tbody>    
									<c:choose>
		  								<c:when test="${mtList != null && not empty mtList}">
		  									<c:forEach items="${mtList}" var="key" varStatus="loop">
		  										<tr>
													<td style="width:20%;"><div class="cell t_center">${key.MT_CD}</div></td>
													<td style="width:80%;"><div class="cell t_underline" onclick="searchOne(${key.MT_ID});">${key.MT_NM}</div></td>
												</tr>
		  									</c:forEach>
		  								</c:when>
		  							</c:choose>
				   				</tbody>
							</table>
						</div>
					</div> 
					<div class="content f_right" style=" width:60%;">
						<div class="table_title" style=" width: 100%; height:32px; margin:0 ;">
							<h1 class="f_left">상세정보</h1>
						</div>  
						<div id="content-cneter" style="width:100%; height:530px; margin:0px 0; border-top:none;  overflow-y:auto; overflow-x:hidden;">
							<c:choose>
								<c:when test="${mainList != null && not empty mainList}">
									<c:forEach items="${mainList}" var="mainList" varStatus="loop">
										<div class="table f_left auto" id="divarea${mainList.MT_ID}" name="divarea" data-mtid="${mainList.MT_ID}" style="width:100%;  margin:0; border-top:none; border-bottom:1px solid #a1bad9;"><!--border-top만  인라인으로 빼야함-->
											<input type="hidden" name="pmtId" value="${mainList.MT_ID}"/>
											<table width="100%" cellspacing="0" cellpadding="0" border="0">
												<tbody>
													<tr>
														<c:if test="${mainList.SC_ID == 100}">
															<th>대학KPI</th>
														</c:if>
														<c:if test="${mainList.SC_ID != 100}">
															<th>부서KPI</th>
														</c:if>
														<td style="padding:0 2%;" colspan="9">${mainList.MT_CD} ${mainList.MT_NM}</td>
													</tr>
													<tr>
														<th>관점</th>
														<td style="padding:0 2%;">${mainList.PERS_NM}</td>
														<th>책임자</th>
														<td colspan="2" style="padding:0 2%;">${mainList.OWNER_USER_NM}</td>
														<th>지표입력자</th>
														<td colspan="2" style="padding:0 2%;">${mainList.KPI_CHARGE_NM}</td>
														<th>측정단위</th>
														<td style="padding:0 2%;">${mainList.EVAL_TYPE_NM}</td>
													</tr>
													<tr>
														<th>산출공식</th>
														<td colspan="9" style="padding:0 2%;">${mainList.CAL_NM}</td>
													</tr>
													<tr>
														<th>세부내용</th>
														<td colspan="9"><textarea style=" width:96%; height:160px; margin:0 2%;border:0;" readonly>${mainList.MT_DEF}</textarea></td>
													</tr>
													<tr>
														<th>고려사항</th>
														<td colspan="9"><textarea style=" width:96%; height:160px; margin:0 2%;border:0;" readonly>${mainList.TARGET_DESC}</textarea></td>
													</tr>
												</tbody>
											</table>
											<c:if test="${mainList.SC_ID == 100}">
												<table width="100%" cellspacing="0" cellpadding="0" border="1" bordercolor="#ABCDEF">
													<tbody>
														<tr>
															<th rowspan="3">가중치</th>
															<td rowspan="3" style="text-align:center;">${mainList.KPI_WEIGHT}</td>
															<th rowspan="3">가치형성</th>
															<th>부서</th>
															<td style="text-align:center;"><span id="dept${mainList.MT_ID}0"></span></td>
															<td style="text-align:center;"><span id="dept${mainList.MT_ID}1"></span></td>
															<td style="text-align:center;"><span id="dept${mainList.MT_ID}2"></span></td>
															<td style="text-align:center;"><span id="dept${mainList.MT_ID}3"></span></td>
															<td style="text-align:center;"><span id="dept${mainList.MT_ID}4"></span></td>
															<td style="text-align:center;">소계</td>
														</tr>
														<tr>
															<th>비율</th>
															<td style="text-align:center;"><span id="value${mainList.MT_ID}0"></span></td>
															<td style="text-align:center;"><span id="value${mainList.MT_ID}1"></span></td>
															<td style="text-align:center;"><span id="value${mainList.MT_ID}2"></span></td>
															<td style="text-align:center;"><span id="value${mainList.MT_ID}3"></span></td>
															<td style="text-align:center;"><span id="value${mainList.MT_ID}4"></span></td>
															<td style="text-align:center;"><span id="sumTotal${mainList.MT_ID}"></span></td>
														</tr>
														<tr>
															<th>지표</th>
															<td style="text-align:center;"><span id="kpi${mainList.MT_ID}0"></span></td>
															<td style="text-align:center;"><span id="kpi${mainList.MT_ID}1"></span></td>
															<td style="text-align:center;"><span id="kpi${mainList.MT_ID}2"></span></td>
															<td style="text-align:center;"><span id="kpi${mainList.MT_ID}3"></span></td>
															<td style="text-align:center;"><span id="kpi${mainList.MT_ID}4"></span></td>
															<td style="padding:0 2%;"></td>
														</tr>
													</tbody>
												</table>
											</c:if>
											<table width="100%" cellspacing="0" cellpadding="0" border="1" bordercolor="#ABCDEF">
												<tr>
													<th>증빙자료</th>
													<td colspan="9"><textarea style=" width:96%; height:160px; margin:0 2%;border:0;" readonly>${mainList.MT_DESC}</textarea></td>
												</tr>
											</table>
<%-- 											<c:if test="${mainList.SC_ID == 100}"> --%>
											<table width="100%" cellspacing="0" cellpadding="0" border="1" bordercolor="#ABCDEF">
												<tbody id="collist${mainList.MT_ID}"> 
													<tr>
														<th rowspan="2">측정주기</th>
														<td rowspan="2" style="text-align:center;">${mainList.MEAS_CYCLE_NM}</td>
														<th rowspan="2">보고시기</th>
														<td rowspan="2" style="text-align:center;">${mainList.MM_NM}</td>
														<th rowspan="2">목표</th>
														<td style="text-align:center;"><span id="yyyy${mainList.MT_ID}0"></span></td>
														<td style="text-align:center;"><span id="yyyy${mainList.MT_ID}1"></span></td>
														<td style="text-align:center;"><span id="yyyy${mainList.MT_ID}2"></span></td>
														<td style="text-align:center;"><span id="yyyy${mainList.MT_ID}3"></span></td>
														<td style="text-align:center;"><span id="yyyy${mainList.MT_ID}4"></span></td>
													</tr>
													<tr>
														<td style="text-align:center;"><span id="target${mainList.MT_ID}0"></span></td>
														<td style="text-align:center;"><span id="target${mainList.MT_ID}1"></span></td>
														<td style="text-align:center;"><span id="target${mainList.MT_ID}2"></span></td>
														<td style="text-align:center;"><span id="target${mainList.MT_ID}3"></span></td>
														<td style="text-align:center;"><span id="target${mainList.MT_ID}4"></span></td>
													</tr>
												</tbody>
											</table>
<%-- 											</c:if> --%>
											<table width="100%" cellspacing="0" cellpadding="0" border="1" bordercolor="#ABCDEF">
												<tbody id="relatedWorkTable${mainList.MT_ID}">
												</tbody>
											</table>
											<c:if test="${mainList.SC_ID != 100}">
												<table width="100%" cellspacing="0" cellpadding="0" border="1" bordercolor="#ABCDEF">
													<tr>
														<th>주요활동</th>
														<td colspan="9"></td>
													</tr>
												</table>
											</c:if>
											<table width="100%" cellspacing="0" cellpadding="0" border="1" bordercolor="#ABCDEF">
												<tr>
													<th>평가방법</th>
<%-- 													<td colspan="9"><textarea style=" width:96%; height:160px; margin:0 2%;border:0;" readonly>${mainList.EVA_TYP_NM}</textarea></td> --%>
													<td colspan="9">${mainList.EVA_TYP_NM}</td>
												</tr>
											</table>
										</div>
									</c:forEach>
								</c:when>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
