<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<!DOCTYPE HTML>
<html>
    <head>
    <meta charset="utf-8">
    <title id='Description'>혁신전략관리</title>
   	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        
        
    <script type="text/javascript">// button 
        $(this).ready(function () {               
           	init();
        		
           	msCombo.done(function(res){   
           		search(); 
           		//subSearch();
           	});
        });
    
    	var msCombo;
    	function init(){
    		$("#jqxButtonSearch").jqxButton({ width: '',  theme:'blueish'}); 
			$("#jqxButtonNew").jqxButton({ width: '',  theme:'blueish'});  
            $("#jqxButtonSave").jqxButton({ width: '',  theme:'blueish'}); 
			$("#jqxButtonDelete").jqxButton({ width: '',  theme:'blueish'});  
			 
			msCombo = $i.sqlhouse(322,{});    
			msCombo.done(function(res){   
    			   
    			var dat=res.returnArray;
    			
    			dat.forEach(function(dataOne) {
    				dataOne.MS_NM = $i.secure.scriptToText(dataOne.MS_NM);
    			});
    			
    			
	            var source =
	            {
	                datatype: "json",
	                datafields: [
	                    { name: 'MS_ID' },
	                    { name: 'MS_NM' }
	                ],
	                id: 'id', 
					localdata:dat,   
	//                url: url,
	                async: false
	            };
	            var dataAdapter = new $.jqx.dataAdapter(source);
	            $("#msCombo").jqxComboBox({ 
				
				selectedIndex: 0, 
				source: dataAdapter, 
				animationType: 'fade', 
				dropDownHorizontalAlignment: 'right', 
				displayMember: "MS_NM", 
				valueMember: "MS_ID",   
				dropDownWidth: 300, 
				dropDownHeight: 100, 
				width: 300, 
				height: 22,  
				theme:'blueish'});
	 		});
    		
    		//사용여부 콤보박스
    		var dat = $i.sqlhouse(399,{COML_COD:"USE"});     
    		dat.done(function(res){  
    			console.log(res);
    			
    			var dat=res.returnArray; 
    			 var source = 
                 {
                     datatype: "json",
                     datafields: [
                         { name: 'COML_COD' }, 
                         { name: 'COM_COD' }
                     ],
                     id: 'id',
 					localdata:dat,
//                     url: url, 
                     async: false 
                 };
                 var dataAdapter = new $.jqx.dataAdapter(source);
                 $("#subStraUseCombobox").jqxComboBox({   
 				
 				selectedIndex: 0, 
 				source: dataAdapter, 
 				animationType: 'fade', 
 				dropDownHorizontalAlignment: 'right', 
 				displayMember: "COM_COD", 
 				valueMember: "COM_COD", 
 				dropDownWidth: 100,  
 				dropDownHeight: 100,   
 				width: 100, 
 				height: 22,  
 				theme:'blueish'});
    		});
    	}
	
        function search() {  
			$("#hiddenMsId").val($("#msCombo").val());            

			$("#msid").val("");
			$("#stid").val("");
			$("#substraid").html("");
			$("#substranm").html("");
			resetForm();
			var div = $("#jqxGrid01").parent();
			 $("#jqxGrid01").jqxGrid('destroy');
			 div.append("<div id='jqxGrid01'/>");
			 
			 
        	var dat = $i.post("./select1","#frm");
    		dat.done(function(res){    
    			console.log(res); 
    			res = res.returnArray;  			

            // prepare the data
            var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'MS_ID', type: 'string'}, 
                    { name: 'STRA_ID', type: 'string'},
                    { name: 'F_STRA_NM', type: 'string'},
					{ name: 'S_STRA_NM', type: 'string'}
                ],
                localdata: res,  
                updaterow: function (rowid, rowdata, commit) {
                    commit(true);
                }
            };

			var alginLeft = function (row, columnfield, value) {//left정렬
				var newValue = $i.secure.scriptToText(value); 
                return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + newValue + '</div>';
							
            } 
			var alginRight = function (row, columnfield, value) {//right정렬
                       return '<div id="userName-' + row + '"style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
							
                  }	  
			
			var detailRow = function (row, columnfield, value) {//그리드 선택시 하단 상세 
				var newValue = $i.secure.scriptToText(value); 
				var msId = $("#jqxGrid01").jqxGrid("getrowdata", row).MS_ID;   
				var straId = $("#jqxGrid01").jqxGrid("getrowdata", row).STRA_ID;   
				var straNm = $("#jqxGrid01").jqxGrid("getrowdata", row).F_STRA_NM;

				
				var link = "<a href=\"javascript:straGetRowDetail('"+row+"','"+msId+"','"+straId+"')\" style='color:black; text-decoration:underline;' >" + newValue + "</a>"; 
				return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";     
				
			};
           /*  var dataAdapter = new $.jqx.dataAdapter(source, { 
                downloadComplete: function (data, status, xhr) { },
                loadComplete: function (data) { },
                loadError: function (xhr, status, error) { }
            }); */ 
            var dataAdapter = new $.jqx.dataAdapter(source);
				  

            $("#jqxGrid01").jqxGrid(
            {
                width: '100%', 
                height: '100%',
				altrows:true,
				pageable: true,
				pageSize: 10,
			    pageSizeOptions: ['100', '200', '300'],
                source: dataAdapter,
				theme:'blueish',
				columnsheight:25 ,
				rowsheight: 25, 
                columnsresize: true,
                sortable:true,
				//autorowheight: true,
                columns: [
                  { datafield: 'MS_ID', hidden:true},
                  { text: '발전목표 ID', datafield: 'STRA_ID', width: '20%', align:'center', cellsalign: 'center'},
                  { text: '발전목표명', datafield: 'F_STRA_NM', width: '80%', align:'center', cellsalign: 'left', cellsrenderer: detailRow }
                ]
            });
    		});
    		subSearch();
        }

        function straGetRowDetail(row, msId){
			var msId = $("#jqxGrid01").jqxGrid("getrowdata", row).MS_ID; 
        	var straId = $("#jqxGrid01").jqxGrid("getrowdata", row).STRA_ID; 
			var straNm = $("#jqxGrid01").jqxGrid("getrowdata", row).F_STRA_NM; 
			
			$("#hiddenMsId").val(msId);             
			$("#submsid").html(msId);     
			$("#msid").val(msId);           
			$("#substraid").html(straId);         
			$("#stid").val(straId);           
			$("#substranm").html($i.secure.scriptToText(straNm));       
			
			resetForm();
		  	subSearch(msId, straId);    

        }
        
        function resetSearch(){
    		//$('#jqxGrid01').jqxGrid('clearselection');     
        }
   
    </script>
    <script type="text/javascript"> //grid		
		function subSearch(msId, straId){
	    	    	
		var div = $("#jqxGrid02").parent();
		$("#jqxGrid02").jqxGrid('destroy');
		 div.append("<div id='jqxGrid02'/>");
		 
		 
		var dat = $i.post("./select2",{submsId:msId,substraId:straId}); 
	    dat.done(function(res){    
	    	console.log(res); 
	    	res = res.returnArray;  		
            // prepare the data
            var source =
            {
                datatype: "json",
                datafields: [
                    { name: 'MS_ID', type: 'string'}, 
                    { name: 'STRA_ID', type: 'string'},  
                    { name: 'SUBSTRA_ID', type: 'string'}, 
                    { name: 'SUBSTRA_CD', type: 'string'},
                    { name: 'F_SUBSTRA_NM', type: 'string'},
                    { name: 'S_SUBSTRA_NM', type: 'string'},
					{ name: 'USE_YN', type: 'string'},
					{ name: 'SORT_ORDER', type: 'string'}, 
					{ name: 'SUBSTRA_DEF', type: 'string'}     
                ], 
                localdata: res,
                updaterow: function (rowid, rowdata, commit) {
                    commit(true);
                }
            };
 
			var alginLeft = function (row, columnfield, value) {//left정렬
                       return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
							
                  } 
			var alginRight = function (row, columnfield, value) {//right정렬
                       return '<div id="userName-' + row + '"style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';
							
                  }
			var noScript = function (row, columnfield, value) {
				var newValue = $i.secure.scriptToText(value);
		   		
				return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + newValue + "</div>";       	
			};
			var detailRow = function (row, columnfield, value, subId) {//그리드 선택시 하단 상세   
				var newValue = $i.secure.scriptToText(value);
		   		var msId = $("#jqxGrid02").jqxGrid("getrowdata", row).MS_ID;
		   		var straId = $("#jqxGrid02").jqxGrid("getrowdata", row).STRA_ID; 
		   		var subId = $("#jqxGrid02").jqxGrid("getrowdata", row).SUBSTRA_ID;
 
				var link = "<a href=\"javascript:subGetRowDetail('"+row+"','"+msId+"','"+straId+"','"+subId+"')\" style='color:black; text-decoration:underline;' >" + newValue + "</a>"; 
				return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";       	
			};	       
				  
            var dataAdapter = new $.jqx.dataAdapter(source);

            $("#jqxGrid02").jqxGrid( 
            {
                width: '100%',
                height: '100%',
				altrows:true, 
				pageable: false,
				pageSize: 10,
			    pageSizeOptions: ['100', '200', '300'],
                source: dataAdapter,
				theme:'blueish',
				columnsheight:25 ,
				rowsheight: 25,
                columnsresize: true,
                sortable:true,
				//autorowheight: true, 
                columns: [
                  { datafield: 'MS_ID', hidden: true },  
                  { datafield: 'STRA_ID', hidden: true }, 
                  { datafield: 'SUBSTRA_ID', hidden: true }, 
                  { text: '혁신전략코드', datafield: 'SUBSTRA_CD', width: '15%', align:'center', cellsalign: 'center'},
                  { text: '혁신전략명(Full Name)', datafield: 'F_SUBSTRA_NM', width: '35%', align:'center', cellsalign: 'left', cellsrenderer:detailRow},  
                  { text: '혁신전략명(Short Name)', datafield: 'S_SUBSTRA_NM', width: '30%', align:'center', cellsalign: 'left', cellsrenderer:noScript}, 
				  { text: '사용여부', datafield: 'USE_YN', width: '10%', align:'center', cellsalign: 'center'}, 
				  { text: '정렬순서', datafield: 'SORT_ORDER', width: '10%', align:'center', cellsalign: 'center'} 
                ] 
            }); 
			//$("#jqxGrid02").jqxGrid('selectrow', 0);
        });
    }
    
    function subGetRowDetail(row, msId, straId){     
   		var msId = $("#jqxGrid02").jqxGrid("getrowdata", row).MS_ID; 
   		var straId = $("#jqxGrid02").jqxGrid("getrowdata", row).STRA_ID;
   		var subId = $("#jqxGrid02").jqxGrid("getrowdata", row).SUBSTRA_ID;
   		var subCd = $("#jqxGrid02").jqxGrid("getrowdata", row).SUBSTRA_CD;
   		var subNm = $("#jqxGrid02").jqxGrid("getrowdata", row).F_SUBSTRA_NM; 
   		var subNm2 = $("#jqxGrid02").jqxGrid("getrowdata", row).S_SUBSTRA_NM; 
   		var subUse = $("#jqxGrid02").jqxGrid("getrowdata", row).USE_YN;
   		var subSort = $("#jqxGrid02").jqxGrid("getrowdata", row).SORT_ORDER; 
   		var subDef = $("#jqxGrid02").jqxGrid("getrowdata", row).SUBSTRA_DEF;
		
   		
   		$("#msid").val(msId);  
   		$("#stid").val(straId);          	 	 
		$("#subid").val(subId);  
		$("#subcd").val(subCd);    
		$("#subnm").val(subNm); 
		$("#subnm2").val(subNm2); 
		$("#subStraUseCombobox").val(subUse);  
		$("#subsort").val(subSort);
		$("#subdef").val(subDef); 

    }
    
   function subInsert(){
	   if($("#msid").val().trim()==""){
			$i.dialog.warning("SYSTEM","발전목표를 선택하세요."); 
			return;
		}
	   if($("#stid").val().trim()==""){
			$i.dialog.warning("SYSTEM","발전목표를 선택하세요."); 
			return;
		}
 
	   if($("#subcd").val().trim()==""){
			$i.dialog.warning("SYSTEM","혁신전략코드를 입력하세요."); 
			return;
		}
	   if($("#subnm").val().trim()==""){ 
			$i.dialog.warning("SYSTEM","혁신전략명을 입력하세요."); 
			return;
		} 
	   if($("#subnm2").val().trim()==""){ 
			$i.dialog.warning("SYSTEM","혁신전략명을 입력하세요."); 
			return;
		} 
		var sortTest = $("#subsort").val();
 		if ( !$.isNumeric(sortTest) ) {
 			$i.dialog.warning("SYSTEM","정렬순서는 숫자만 입력해주세요.");
 		$('#subsort').val('');    
 			return;
 		}  
 		$i.insert("#frm2").done(function(args){ 
			if(args.returnCode == "EXCEPTION"){ 
				$i.dialog.error("SYSTEM",args.returnMessage);
			}else{
				
				$i.dialog.alert("SYSTEM","저장 되었습니다.",function(){
					var msId = 	$("#msid").val();
					var stId =  $("#stid").val();
					subSearch(msId,stId);
    				resetForm(); 
				});
			} 
		}).fail(function(e){ 
			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
		});
   }
	
   //삭제
 	function remove(){ 
 		if($("#subid").val().trim()==""){  
			$i.dialog.warning("SYSTEM","삭제 할 혁신전략을 선택하세요.");
			return;  
		}  
	   $i.dialog.confirm('SYSTEM','삭제 하시겠습니까?',function(){
		   
			var check = $i.post("./check",{msId:$("#msid").val(),straId:$("#stid").val(),substId:$("#subid").val()},function(){}); 
			check.done(function(res){
				if(res.returnArray.length == 0){  
					var a = $i.post("./remove","#frm2");
	      				a.done(function(){
		        			if(args.returnCode == "EXCEPTION"){
		        				$i.dialog.error("SYSTEM",args.returnMessage);
		        			}else{ 
		        				$i.dialog.alert("SYSTEM","삭제 되었습니다.",function(){
		        					var msId = 	$("#msid").val();
		        					var stId =  $("#stid").val();
		        					subSearch(msId,stId);
			        				resetForm(); 	
		        				}); 
		        			 
		        			}  
		        		}).fail(function(e){
		        			$i.dialog.error("SYSTEM","삭제 중 오류가 발생했습니다.");
		        		});
				}else{
					$i.dialog.warning("SYSTEM","사용 중인 혁신전략입니다");
				}
			});
		   
	   });
 	}
   
 //신규 버튼 클릭 또는 초기화 해야 할 경우
	function resetForm(){
		$("#subid").val('');  
		$("#subcd").val(''); 
		$("#subnm").val('');    
		$("#subnm2").val(''); 
		$("#subStraUseCombobox").val('Y'); 
		$("#subsort").val(''); 
		$("#subdef").val('');
		$('#jqxGrid02').jqxGrid('clearselection');    
	}
 
    </script>
    </head>
    <body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
<div class="wrap" style="width:98%; min-width:1067px; margin:0 1%;">

		<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
			<form id="frm" name="frm">
		<div class="label type1 f_left">
				비젼:
			</div> 
		<div class="combobox f_left"  id='msCombo'>
			</div> 
			<input type="hidden" id="hiddenMsId" name="msid"/>
		<div class="group_button f_right">
				<div class="button type1 f_left">  
				<input type="button" value="조회" id='jqxButtonSearch'  width="100%" height="100%"  onclick="search(); resetSearch();"/>        
			</div>
			</div>
			</form>  
		<!--group_button-->
	</div>
		<!--//header-->
		<div class="container  f_left" style="width:100%; margin:10px 0;">
		<div class="content f_left" style=" width:40%; margin-right:1%;">
				<div class="grid f_left m_b20" style="width:100%; height:550px; margin:0;">
					<div id="jqxGrid01"> 
					</div>
			</div>
				<!--grid -->
			</div>
		<!--//content-->
		<div class="content f_right" style=" width:59%;">
				<div class="group f_left  w100p m_b5">
				<div class="label type2 f_left t_left">
						발전목표ID:<span class="label sublabel" id="substraid"></span>
					</div>  
				<div class="label type2 f_left t_left">
						발전목표명:<span class="label sublabel" id="substranm" ></span>   
					</div>
			</div>
				<!--group-->
				<div class="grid f_left m_b20" style="width:100%; height:293px; margin:0;">
					<div id="jqxGrid02">
					</div>
			</div>
				<!--grid -->
				<form id="frm2" name="frm2">
				<div class="blueish table f_left"  style="width:100%; height:; margin:10px 0;">
				<table summary="가산점" style="width:100%;">
						<thead style="width:100%;">
						<tr>
								<th  scope="col" style="width:15%;">혁신전략코드<span class="th_must"></span></th>
								<th  scope="col" style="width:35%;">혁신전략명(Full Name)<span class="th_must"></span></th>
								<th  scope="col" style="width:30%;">혁신전략명(Short Name)<span class="th_must"></span></th>
								<th scope="col" style="width:10%;">사용여부<span class="th_must"></span></th>
								<th scope="col" style="width:10%;">정렬순서<span class="th_must"></span></th> 
							</tr>
					</thead>
						<tbody> 
						<tr>
								<td style="width:15%;"><div class="cell">   
										<input type="hidden" id="msid" name="submsId" />      
										<input type="hidden" id="stid" name="substraId" />  
										<input type="hidden" id="subid" name="substId" />       
										<input type="text" id="subcd" name="substCd" class="input type2  f_left t_center"  style="width:90%; margin:3px 0; "/>
									</div></td>
								<td style="width:35%;"><div class="cell t_center">
										<input type="text"  id="subnm" name="substNm1" class="input type2  f_left"  style="width:96%; margin:3px 0; "/>
									</div></td>
								<td style="width:30%;"><div class="cell t_center">
										<input type="text"  id="subnm2" name="substNm2" class="input type2  f_left"  style="width:96%; margin:3px 0; "/>
									</div></td>	
								<td style="width:10%; "><div class="cell t_center">
										<div  class="combobox" id= "subStraUseCombobox" name="substUse">         
									</div></td>
								<td style="width:10%; "><div class="cell t_center">
										<input type="text" id="subsort" name="substSort" class="input type2  f_left t_center"  style="width:90%; margin:3px 0; "/>
									</div></td>
							</tr> 
					</tbody>
					<tbody>
							<td style="height:3px; background:#eff0ef;" colspan="7"></td><!--구분선-->
						</tbody>
					<tbody>	
							<tr>
								<th  colspan="5">비고</th> 
							</tr> 
							<tr> 
								<td  colspan="5"><div  class="cell"><textarea  id="subdef" name="substDef" class="textarea"  style=" width:99.5%; height:65px; margin:3px 0;overflow:auto;"></textarea></div></td>
							</tr>
					</tbody>
					</table>   
			</div>
				<!--table -->
				<div class="group_button f_right"> 
				<div class="button type2 f_left">
						<input type="button" value="신규" id='jqxButtonNew' width="100%" height="100%" onclick="resetForm();"/> 
					</div>
				<div class="button type2 f_left">
						<input type="button" value="저장" id='jqxButtonSave' width="100%" height="100%" onclick="subInsert();" /> 
					</div>  
				<div class="button type3 f_left">
						<input type="button" value="삭제" id='jqxButtonDelete' width="100%" height="100%" onclick="remove();" /> 
					</div>
			</div>
				<!--group_button-->
			</form>
			</div> 
		<!--//content-->
	</div>
		<!--//container-->
	</div>
<!--//wrap-->
</body>
</html>