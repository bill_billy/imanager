<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>발전목표관리</title> 
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <script>
            $(this).ready(function () {               
				$("#btnNew01").jqxButton({ width: '',  theme:'blueish'}); 
                $("#btnSave01").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnDelete01").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnNew02").jqxButton({ width: '',  theme:'blueish'}); 
                $("#btnSave02").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnDelete02").jqxButton({ width: '',  theme:'blueish'});
				search1(); 
				search2();  
            });
    </script>
    <script type="text/javascript">//combobox
            $(this).ready(function () {   
            	
        		
        	  	//사용여부1 콤보박스
            	var dat = $i.sqlhouse(399,{COML_COD:"USE"});     
        		dat.done(function(res){  
        			console.log(res);
        			
        			var dat=res.returnArray; 
        			 var source =
                     {
                         datatype: "json",
                         datafields: [
                             { name: 'COML_COD' }, 
                             { name: 'COM_COD' }
                         ],
                         id: 'id', 
     					localdata:dat,
//                         url: url,
                         async: false
                     };
                     var dataAdapter = new $.jqx.dataAdapter(source);
                     $("#msUseCombobox").jqxComboBox({ 
     				
     				selectedIndex: 0, 
     				source: dataAdapter, 
     				animationType: 'fade', 
     				dropDownHorizontalAlignment: 'right', 
     				displayMember: "COM_COD", 
     				valueMember: "COM_COD",    
     				dropDownWidth: 100,  
     				dropDownHeight: 100,   
     				width: 100, 
     				height: 22,  
     				theme:'blueish'});
        		});
        		
        	  	//사용여부2 콤보박스
            	var dat = $i.sqlhouse(399,{COML_COD:"USE"});     
        		dat.done(function(res){  
        			console.log(res);
        			
        			var dat=res.returnArray; 
        			 var source = 
                     {
                         datatype: "json",
                         datafields: [
                             { name: 'COML_COD' }, 
                             { name: 'COM_COD' }
                         ],
                         id: 'id',
     					localdata:dat,
//                         url: url, 
                         async: false 
                     };
                     var dataAdapter = new $.jqx.dataAdapter(source);
                     $("#straUseCombobox").jqxComboBox({   
     				
     				selectedIndex: 0, 
     				source: dataAdapter, 
     				animationType: 'fade', 
     				dropDownHorizontalAlignment: 'right', 
     				displayMember: "COM_COD", 
     				valueMember: "COM_COD",  
     				dropDownWidth: 100,  
     				dropDownHeight: 100,   
     				width: 100, 
     				height: 22,  
     				theme:'blueish'});
        		});
        		
        	getRowDetail1('');  
        	getRowDetail2(''); 
    	 }); 
	</script>
   
    <script type="text/javascript"> //grid		
   		function search1() {      
        	var dat = $i.post("./select1","#saveForm1");    
    		dat.done(function(res){   
    			console.log(res); 
    			res = res.returnArray;
            // prepare the data 
            var source = 
            { 
                datatype: "json", 
                datafields: [
                    { name: 'MS_ID', type: 'string'}, //미션/비젼명
                    { name: 'MS_NM', type: 'string'}, //미션/비젼명
                    { name: 'START_YYYY', type: 'string'}, //시작년도
					{ name: 'END_YYYY', type: 'string'}, //종료년도 
					{ name: 'USE_YN', type: 'string'}, // 사용여부 
					{ name: 'MS_DEF', type: 'string'} // 비고 
                ],	 
                localdata: res,  
                updaterow: function (rowid, rowdata, commit) { 
                    commit(true);
                }
            };
        	
			var alginLeft = function (row, columnfield, value) {//left정렬
                       return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
                  } 
			var alginRight = function (row, columnfield, value) {//right정렬
                       return '<div id="userName-' + row + '"style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';		
                  }	  
		/* 	var linkrenderer = function (row, column, value) {
                if (value.indexOf('#') != -1) {
                    value = value.substring(0, value.indexOf('#'));
                } 
		}*/
            var detailRow = function (row, columnfield, value, msId) {//그리드 선택시 하단 상세
				var newValue = $i.secure.scriptToText(value);
				var msId = $("#jqxGrid01").jqxGrid("getrowdata", row).MS_ID; //rowdata 가져오기 
				var msNm = $("#jqxGrid01").jqxGrid("getrowdata", row).MS_NM;
				//var acctId = $("#jqxGrid01").jqxGrid("getrowdata", row).ACCT_ID;  
				 
				var link = "<a href=\"javascript:getRowDetail1('"+row+"','"+msId+"')\" style='color:black; text-decoration:underline;' >" + newValue + "</a>";
				return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";  
				   
			};

				var dataAdapter = new $.jqx.dataAdapter(source);

            $("#jqxGrid01").jqxGrid( 
            {
                width: '100%',
                height: '100%',
				altrows:true,
				pageable: true,
				pageSize: 10,
			    pageSizeOptions: ['100', '200', '300'],
                source: dataAdapter,
				theme:'blueish', 
				columnsheight:25 ,
				rowsheight: 25, 
                columnsresize: true,  
               	sortable:true,
				//autorowheight: true,
                columns: [ 
                  { datafield: 'MS_ID', hidden:true},
                  { text: '미션&frasl;비젼', datafield: 'MS_NM', width: '55%', align:'center', cellsalign:'center', cellsrenderer: detailRow},     
                  { text: '시작년도', datafield: 'START_YYYY', width: '15%', align:'center', cellsalign: 'center'}, 
				  { text: '종료년도', datafield: 'END_YYYY', width: '15%', align:'center', cellsalign: 'center'}, 
				  { text: '사용여부', datafield: 'USE_YN', width: '15%', align:'center', cellsalign: 'center'} 
                ]
            });
			//$("#jqxGrid01").jqxGrid('selectrow', 0);
       	});
   	} 
    
    	function getRowDetail1(row){
    		var msId = $("#jqxGrid01").jqxGrid("getrowdata", row).MS_ID;
    		var msNm = $("#jqxGrid01").jqxGrid("getrowdata", row).MS_NM; 
    		var sYYYY = $("#jqxGrid01").jqxGrid("getrowdata", row).START_YYYY;
    		var eYYYY = $("#jqxGrid01").jqxGrid("getrowdata", row).END_YYYY;
    		var useYn = $("#jqxGrid01").jqxGrid("getrowdata", row).USE_YN;
    		var msDef = $("#jqxGrid01").jqxGrid("getrowdata", row).MS_DEF;
    	
    		$("#msIdLB").html(msId); 
			$("#msNmLB").html($i.secure.scriptToText(msNm));   
			$("#msid").val(msId);
			$("#MSname").val(msNm); 
			$("#startYYYY").val(sYYYY);
			$("#endYYYY").val(eYYYY);
			$("#msUseCombobox").val(useYn);  
			$("#msDEF").val(msDef);
			$("#stramsid").val(msId);
			
			search2(msId);
			resetForm2();  
   		 } 
    	 
        //입력1
        function insert1(){
      		if($("#MSname").val().trim()==""){ 
      			$i.dialog.warning("SYSTEM","미션/비전명을 입력해주세요.");  
      			return;
      		}
      		if($("#startYYYY").val().trim()==""){ 
      			$i.dialog.warning("SYSTEM","시작년도를 입력해주세요.");  
      			return;
      		}
      		if($("#endYYYY").val().trim()==""){  
      			$i.dialog.warning("SYSTEM","종료년도를 입력해주세요.");   
      			return;
      		}
      		var sYtest = $("#startYYYY").val();
      		var eNtest = $("#endYYYY").val();

      		if ( !$.isNumeric(sYtest) ) {
      			$i.dialog.warning("SYSTEM","시작년도는 숫자만 입력해주세요.");
      			$('#startYYYY').val('');  
      			return;
      		} 
      		if ( sYtest.length!=4 ) {
      			$i.dialog.warning("SYSTEM","년도는 YYYY로 입력해주세요.");
      			$('#startYYYY').val('');  
      			return;
      		}
      		if ( !$.isNumeric(eNtest) ) {
      			$i.dialog.warning("SYSTEM","종료년도는 숫자만 입력해주세요.");
      			$('#endYYYY').val('');   
  				return; 
      		} 
      		if ( eNtest.length!=4 ) {
      			$i.dialog.warning("SYSTEM","년도는 YYYY로 입력해주세요.");
      			$('#endYYYY').val('');  
      			return;
      		}
      		
      		if($("#startYYYY").val() > $("#endYYYY").val()){
      			$i.dialog.warning("SYSTEM","시작년도가 종료년도보다 큽니다.");
          	$('#startYYYY').val('');   
          	$('#endYYYY').val('');   
      			return; 
      		}
  
      		
      		var a = $i.post("./insert1","#saveForm1");
//      		var a = $i.post("./insert1",{com:'a'});
      		a.done(function(args){
//      		$i.insert1("#saveForm1").done(function(args){
      			if(args.returnCode == "EXCEPTION"){
      				$i.dialog.error("SYSTEM",args.returnMessage);
      			}else{
      				$i.dialog.alert("SYSTEM","저장 되었습니다.",function(){
      					search1();
          				resetForm1();
      				});
      			}
      		}).fail(function(e){ 
      			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
      		});
      	} 
      		
      //삭제
    	function remove1(){
    		if($("#msid").val().trim()==""){  
    			$i.dialog.warning("SYSTEM","삭제 할 발전목표를 선택하세요.");
    			return; 
    		}  	  
      
    	$i.dialog.confirm('SYSTEM','삭제 하시겠습니까?',function(){
    			
    			var check = $i.post("./check1",{msid:$("#msid").val()},function(){});
    			check.done(function(res){
    				if(res.returnArray.length == 0){  
    					var a = $i.post("./remove1","#saveForm1");
    	      				a.done(function(args){
		        			if(args.returnCode == "EXCEPTION"){
		        				$i.dialog.error("SYSTEM",args.returnMessage);
		        			}else{ 
		        				$i.dialog.alert("SYSTEM","삭제 되었습니다.",function(){
		        					search1(); 
			        				resetForm1(); 
		        				}); 
		        			}
		        		}).fail(function(e){
		        			$i.dialog.error("SYSTEM","삭제 중 오류가 발생했습니다.");
		        		});
    				}else{
    					$i.dialog.warning("SYSTEM","사용 중인 발전목표입니다");
    				}
    			});
    	});
    }
      		
    	//신규 버튼 클릭 또는 초기화 해야 할 경우
    	function resetForm1(){
    		$("#msIdLB").html('');
    		$("#msNmLB").html(''); 
    		$("#msid").val('');
    		$("#MSname").val('');
			$("#startYYYY").val('');
			$("#endYYYY").val('');
			$("#msUseCombobox").val('Y');   
			$("#msDEF").val('');
			$('#jqxGrid01').jqxGrid('clearselection'); 
			
			$("#stramsid").val("");
			$("#StId").val(""); 
			search2();
			resetForm2();  
    	}
    	
    	//미션/비전 테이블
    </script>
    
    <script type="text/javascript"> //grid		
		function search2(msId){
    		$("#stramsid").val(msId);	
    
			var dat = $i.post("./select2","#saveForm2");
			dat.done(function(res){   
			console.log(res); 
			res = res.returnArray;
        // prepare the data
    			var source =
            {
                datatype: "json",
                datafields: [ 
                	{ name: 'MS_ID', type: 'string'}, 
                    { name: 'STRA_ID', type: 'string'},
                    { name: 'F_STRA_NM', type: 'string'},
					{ name: 'S_STRA_NM', type: 'string'},
					{ name: 'USE_YN', type: 'string'},
					{ name: 'SORT_ORDER', type: 'string'}, 
					{ name: 'STRA_DEF', type: 'string'}
                ], 
                localdata: res,
                updaterow: function (rowid, rowdata, commit) { 
                    commit(true);
                } 
            };
            
        	var alginLeft = function (row, columnfield, value) {//left정렬
        		var newValue = $i.secure.scriptToText(value); 
        		return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + newValue + '</div>';
						
           }; 
			var alginRight = function (row, columnfield, value) {//right정렬
	                return '<div id="userName-' + row + '"style="text-align: right; margin:4px 10px 0px 0px;">' + value + '</div>';		
	        };	  
	         
	        var detailRow = function (row, columnfield, value, stid) {//그리드 선택시 하단 상세  
				var newValue = $i.secure.scriptToText(value); 
	        	var stMsid = $("#jqxGrid02").jqxGrid("getrowdata", row).MS_ID; //rowdata 가져오기    
		        var stid = $("#jqxGrid02").jqxGrid("getrowdata", row).STRA_ID; //rowdata 가져오기   
				      
					 
				var link = "<a href=\"javascript:getRowDetail2('"+row+"', '"+stMsid+"' ,'"+stid+"')\" style='color:black; text-decoration:underline;' >" + newValue + "</a>"; 
				return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";    
					  
			};  
				
	       
            var dataAdapter = new $.jqx.dataAdapter(source);

            $("#jqxGrid02").jqxGrid(
            {
                width: '100%',
                height: '100%',
				altrows:true,
				pageable: true,
				pageSize: 5,
			    pageSizeOptions: ['100', '200', '300'],
                source: dataAdapter,
				theme:'blueish',
				columnsheight:25 ,
				rowsheight: 25,
                columnsresize: true,
				autorowheight: true, 
				sortable:true,
                columns: [
                  { datafield: 'MS_ID', hidden: true},
                  { text: '발전목표 ID', datafield: 'STRA_ID', width: '10%', align:'center', cellsalign: 'center'},
                  { text: '발전목표명(Full Name)', datafield: 'F_STRA_NM', width: '40%', align:'center', cellsalign: 'left', cellsrenderer: detailRow},
				  { text: '발전목표명(Short Name)', datafield: 'S_STRA_NM', width: '34%', align:'center', cellsalign: 'left', cellsrenderer:alginLeft},
				  { text: '사용여부', datafield: 'USE_YN', width: '8%', align:'center', cellsalign: 'center'}, 
				  { text: '정렬순서', datafield: 'SORT_ORDER', width: '8%', align:'center', cellsalign: 'center'}
                ]
            });
			//$("#jqxGrid02").jqxGrid('selectrow', 0);
       	});
   	} 
    
			function getRowDetail2(row, msId){ 

		   		var stMsid = $("#jqxGrid02").jqxGrid("getrowdata", row).MS_ID; 
		   		var stid = $("#jqxGrid02").jqxGrid("getrowdata", row).STRA_ID;
	    		var stnm = $("#jqxGrid02").jqxGrid("getrowdata", row).F_STRA_NM;  
	    		var stsubnm = $("#jqxGrid02").jqxGrid("getrowdata", row).S_STRA_NM;   
	    		var stuse = $("#jqxGrid02").jqxGrid("getrowdata", row).USE_YN; 
	    		var stsort = $("#jqxGrid02").jqxGrid("getrowdata", row).SORT_ORDER; 
	    		var stdef = $("#jqxGrid02").jqxGrid("getrowdata", row).STRA_DEF; 
	    		
				$("#straid").html(stid);
				$("#StId").val(stid); 
				$("#straNMLabel").val(stnm);     
				$("#straSubNm").val(stsubnm); 
				$("#straUseCombobox").val(stuse);   
				$("#straSort").val(stsort); 
				$("#straDEF").val(stdef);  
				$("#stramsid").val(stMsid); //MS_ID값  
			
		}
		
      //입력2
    	function insert2(){ 
    		if($("#stramsid").val().trim()==""){
    			$i.dialog.warning("SYSTEM","미션/비전을 선택하세요."); 
    			return;
    		}
    	 	if($("#straNMLabel").val().trim()==""){
    			$i.dialog.warning("SYSTEM","발전목표명을 입력하세요.");
    			return;
    		}
    			if($("#straSubNm").val().trim()==""){  
    			$i.dialog.warning("SYSTEM","발전목표명을 입력하세요."); 
    			return;
    		}
    		if($("#straSort").val().trim()==""){
    			$i.dialog.warning("SYSTEM","정렬순서를 입력하세요."); 
    			return;
    		}
    		
    		var sortTest = $("#straSort").val();

      		if ( !$.isNumeric(sortTest) ) {
      			$i.dialog.warning("SYSTEM","정렬순서는 숫자만 입력해주세요.");
      		$('#straSort').val('');   
      			return;
      		} 
    		var a = $i.post("./insert2","#saveForm2");
			a.done(function(args){
    			if(args.returnCode == "EXCEPTION"){
    				$i.dialog.error("SYSTEM",args.returnMessage);
    			}else{
    				$i.dialog.alert("SYSTEM","저장 되었습니다.",function(){
    					search2();
        				resetForm2();
    				});
    			}
    		}).fail(function(e){ 
    			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
    		});
    	} //End Of insert2
    	
    
        //삭제
    	function remove2(){
    		if($("#StId").val().trim()==""){  
    			$i.dialog.warning("SYSTEM","삭제 할 발전목표를 선택하세요.");
    			return; 
    		}  
    		$i.dialog.confirm('SYSTEM','삭제 하시겠습니까?',function(){
    			
    			var check = $i.post("./check2",{ms_id:$("#stramsid").val(),straid:$("#StId").val()},function(){});
    			check.done(function(res){
    				if(res.returnArray.length == 0){  
    					var a = $i.post("./remove2","#saveForm2");  
    	      				a.done(function(args){
		        			if(args.returnCode == "EXCEPTION"){
		        				$i.dialog.error("SYSTEM",args.returnMessage);
		        			}else{ 
		        				$i.dialog.alert("SYSTEM","삭제 되었습니다.",function(){
		        					search2(); 
			        				resetForm2();  
		        				}); 
		        				
		        			}
		        		}).fail(function(e){
		        			$i.dialog.error("SYSTEM","삭제 중 오류가 발생했습니다.");
		        		});
    				}else{
    					$i.dialog.warning("SYSTEM","사용 중인 발전목표입니다");
    				}
    			});
    		});
    	}
      		
    	
    	//신규 버튼 클릭 또는 초기화 해야 할 경우
    	function resetForm2(){
    		$("#stramsid").html('');  
    		$("#straid").html('');
    		$("#StId").val(''); 
    		$("#straNMLabel").val('');
			$("#straSubNm").val('');    
			$("#straUseCombobox").val('Y');
			$("#straSort").val('0');  
			$("#straDEF").val('');
			$('#jqxGrid02').jqxGrid('clearselection');  
		}

    </script> 
    </head>
    <body class='blueish'>
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
<div class="wrap" style="width:98%; min-width:1067px; margin:0 1%;">
		<div class="container  f_left" style="width:100%; margin:10px 0;">
		<!-- form -->
		<!--  content -->
		<div class="content f_left" style=" width:49%; margin-right:1%;">
		<form id="saveForm1" name="saveForm1">
				<!--grid -->
				<div class="grid f_left m_b20" style="width:100%; height:394px; margin:0;">
					<div id="jqxGrid01"></div> 
				</div> 
				<!-- //grid -->
				<!-- table -->
				<div class="blueish table f_left"  style="width:100%; height:; margin:10px 0;">
					<table summary="가산점" style="width:100%;">
						<thead style="width:100%;">
							<tr>
								<th  scope="col" style="width:55%;">미션&frasl;비젼<span class="th_must"></th>
								<th  scope="col" style="width:15%;">시작년도<span class="th_must"></th>
								<th scope="col" style="width:15%;">종료년도<span class="th_must"></th>
								<th scope="col" style="width:15%;">사용여부<span class="th_must"></span></th> 
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="width:55%;"><div class="cell">
												<input type="hidden" id="msid" name="msid" />  <!-- msid확인 --> 
												<input type="text" id="MSname" name="msNM" class="input type2  f_left"  style="width:97%; margin:3px 0; "/>
									</div></td>	
								<td style="width:15%;"><div class="cell t_center">
										<input type="text" id="startYYYY" name= "startYYYY" class="input type2  f_left t_center"  style="width:96%; margin:3px 0; "/>
									</div></td>
								<td style="width:15%; "><div class="cell t_center">
										<input type="text" id="endYYYY" name="endYYYY" class="input type2  f_left t_center"  style="width:90%; margin:3px 0; "/>
									</div></td>
								<td style="width:15%; "><div class="cell t_center">
										<div  class="combobox" id= 'msUseCombobox' name="useYN"> 
									</div></td>	
							</tr>
						</tbody>
						 
						<tbody>
							<tr>
						<!--구분선-->
								<td style="height:3px; background:#eff0ef;" colspan="7"></td>
							</tr>
						</tbody>
						
						<tbody>
							<tr>
								<th  colspan="4">비고</th>
							</tr>
							<tr>
								<td  colspan="4">
									<div  class="cell"> 
										<textarea id="msDEF" name="msDEF" class="textarea" style=" width:99.5%; height:65px; margin:3px 0;overflow:auto;"></textarea> 
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--//table -->
				
				<!-- button pack-->
				<div class="group_button f_right">
					<div class="button type2 f_left">
						<input type="button" value="신규" id='btnNew01' width="100%" height="100%" onclick="resetForm1();" />   
					</div>
					<div class="button type2 f_left"> 
						<input type="button" value="저장" id='btnSave01' width="100%" height="100%" onclick="insert1();" /> 
					</div>
					<div class="button type3 f_left">
						<input type="button" value="삭제" id='btnDelete01' width="100%" height="100%"  onclick="remove1();"/> 
					</div>
				</div>
				<!-- // button pack  --> 
			</form>
				</div> 
				<!-- content -->
				<!--group_button-->
		<!--//content-->
		<div class="content f_right" style=" width:50%;">
				<form id="saveForm2" name="saveForm2"> 
					<div class="group f_left  w100p m_b5">
						<div class="label type2 f_left t_left">
							미션&frasl;비젼:<span class="label sublabel" id="msNmLB"></span>
						</div>
				</div>
				<!--group-->
				<div class="grid f_left m_b20" style="width:100%; height:365px; margin:0;">
					<div id="jqxGrid02">
					</div>
				</div>
				<!--grid --> 
				<div class="blueish table f_left"  style="width:100%; height:; margin:10px 0;">
				<table summary="가산점" style="width:100%;">
						<thead style="width:100%;"> 
						<tr> 
								<th  scope="col" style="width:35%;">발전목표명(Full Name)<span class="th_must"></span></th>  	  					
								<th  scope="col" style="width:30%;">발전목표명(Short Name)<span class="th_must"></span></th>  
								<th scope="col" style="width:10%;">사용여부<span class="th_must"></span></th>
								<th scope="col" style="width:10%;">정렬순서<span class="th_must"></span></th>  
							</tr>
					</thead>
						<tbody>								
						<tr> 
								<td style="width:35%;"><div class="cell"> 
										<input type="hidden" id="stramsid" name="stramsid" />  <!-- msid확인 -->
										<input type="hidden" id="StId" name="straid" />      
										<input type="text" id="straNMLabel" name="stranm" class="input type2  f_left"  style="width:97%; margin:3px 0; "/>
									</div></td>
								<td style="width:30%;"><div class="cell t_center"> 
										<input type="text" id="straSubNm" name="straSubNm" class="input type2  f_left"  style="width:96%; margin:3px 0; "/>
									</div></td>
								
									<td>
										<div  class="combobox" id= 'straUseCombobox' name="straUse">  
									</div></td>
								<td style="width:10%; "><div class="cell t_center">
										<input type="text" id="straSort" name="straSort" value="0" class="input type2  f_left t_center"  style="width:90%; margin:3px 0; "/>
									</div></td>	
							</tr>
					</tbody>
						<tbody>
						<!--구분선-->
						<tr>
							<td style="height:3px; background:#eff0ef;" colspan="7"></td>
						</tr>
						<!-- //구분선 -->					 	
						</tbody>
					<tbody>
						<tr>
							<th  colspan="5">비고</th>
						</tr>
						<tr>
							<td  colspan="5"><div  class="cell">
									<textarea  id="straDEF" name="straDEF" class="textarea"  style=" width:99.5%; height:65px; margin:3px 0;overflow:auto;"></textarea>
								</div></td>
						</tr>
					</tbody>
					</table>
			</div>
				<!--//table -->
				<!-- group button -->
				<div class="group_button f_right">
					<div class="button type2 f_left">
						<input type="button" value="신규" id='btnNew02' width="100%" height="100%" onclick="resetForm2();" />
					</div>
					<div class="button type2 f_left"> 
						<input type="button" value="저장" id='btnSave02' width="100%" height="100%" onclick="insert2();" />
					</div>
					<div class="button type3 f_left"> 
						<input type="button" value="삭제" id='btnDelete02' width="100%" height="100%" onclick="remove2();"/> 
					</div>	
				</div>
				<!--group_button-->
					</form>
				</div>
				<!-- //content -->
			</div>
			<!--//container-->
		</div>
			<!-- //wrap -->  
</body>
</html>