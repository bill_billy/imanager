<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <title>지표별 증빙 자료</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        	var straid = "";
        	
        	//문서 로딩이 완료되면 처음 수행되는 로직
			var globalUploadAble=false;
        	$(document).ready(function(){
				init();
				checkInputLimit();
        	}); 
			function checkInputLimit(){ 
        		var pEvaGbn = "B"; 
        		var data = $i.sqlhouse(206,{S_EVA_GBN:pEvaGbn, S_YYYY:"${param.yyyy}",S_MM:$("#cboMonth").val()});
        		data.done(function(c){                                                 
        			if(c.returnCode=="SUCCESS"){            
        				if(c.returnArray.length > 0){
        					if(c.returnArray[0].CHECK_DAT == "1"){ 
	        					$("#btnInsert").hide();
	        					
	        				}else if(c.returnArray[0].CHECK_DAT == "2"){
	        					 
	        					$("#btnInsert").hide();     
	        				}else if(c.returnArray[0].CHECK_DAT == "0"){ 
	        					$("#btnInsert").show(); 
								globalUploadAble=true;
	        				}
        				}else{ 
        					$("#btnInsert").hide(); 
        				}
        			}else{ 
        				$("#btnInsert").hide(); 
        			}
        		});
        		
        	}
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		initCboMonth();
        		$("#spanMyName").html(decodeURIComponent("${sessionScope.loginSessionInfo.userName}"));
        		initButton();
        		initAjaxForm();                
        	}
        	//조회  
        	function search(isclear){ 
        		if(isclear!=null&&isclear){   
        			clearKpiAttachFileList();        	 
        		}   
        		initSummary();    
        		evalKpiList('');       
   
        	}
        	function evalKpiList(scId){   
        		var scid =  $("#txtTabscid", parent.document).val();
        		var gubn = $("#txtGubn", parent.document).val();      
        		var task = $i.sqlhouse(224,{S_YYYY:"${param.yyyy}",S_MT_ID:scid,S_SC_ID:scId,S_EVA_GBN:$("#hiddenEvaGbn").val(),S_MM:$("#cboMonth").val(),S_GUBN:gubn});  
        		task.done(function(result){             
        			if(result.returnCode!="SUCCESS"){          
        				$i.dialog.error("SYSTEM",result.returnMessage);
        				return;
        			}
        			var data = result.returnArray;
        			//셀병합용 배열
        			var mergeList = [];
        			
        			var html = '';
        			if(data != null && data.length > 0) {
        				for(var i=0; i<data.length; i++) {
        				
        					var fileLink = '';
        					
        					if(data[i].FILE_ID != '') {
        						fileLink = '<a href="javaScript:fileDown(\''+data[i].FILE_ID+'\');" style="color:black;">'+setImgIcon(data[i].FILE_YN)+data[i].FILE_YN+'</a>';
        					} else {
        						fileLink = '<a href="javaScript:kpiAttachFileList(\''+data[i].YYYY+'\', \''+data[i].MM+'\',\''+data[i].KPI_ID+'\',\''+data[i]. SC_ID+'\',\''+data[i].KPI_NM+'\');" style="color:black; margin-left:45%;">미 등 록</a>';
        					}
        					
        					html += '<tr class="tr_hover" data-kpiId="'+data[i].KPI_ID+'">';            
        					html += '	<td style="text-align:center;width:15%;" name="scId_'+data[i].KPI_ID+'">'+data[i].SC_NM+'</td>';
        					html += '	<td id="selectKpihover" style="text-align:left;width:30%;"  name="kpi_'+data[i].KPI_ID+'">';  
        					html += '	<input type="hidden" id="hiddenunid" value="'+data[i].UNID+'" />';
        					html += '	<input type="hidden" id="hiddenchargeID" value="'+data[i].KPI_CHARGE_ID+'" />';    
        					html += '	<a style="margin-left:10px !important; color:black;text-decoration:underline;" href="javaScript:kpiAttachFileList(\''+data[i].YYYY+'\', \''+data[i].MM+'\',\''+data[i].KPI_ID+'\',\''+data[i].SC_ID+'\',\''+data[i].KPI_NM+'\',\''+data[i].UNID+'\',\''+data[i].KPI_CHARGE_ID+'\');" >'+ data[i].KPI_NM+'</a>';          
        					html += '	</td>';      
        					html += '	<td style="text-align:center;width:12%;"  name="kpiCharge_'+data[i].KPI_ID+'">'+data[i].KPI_CHARGE_NM+'</td>'; 
        					html += '	<td style="text-align:center;width:13%;" name="eval_'+data[i].KPI_ID+'">'+data[i].EVAL_NM+'</td>';  
        					html += '	<td style="text-align:left;width:30%;">'+fileLink+'</td>';  
        					html += '</tr>';       
        					
        					//셀병합용            
        					mergeList.push('scId_'+data[i].KPI_ID);
        					mergeList.push('kpi_'+data[i].KPI_ID);
        					mergeList.push('kpiCharge_'+data[i].KPI_ID);
        					mergeList.push('eval_'+data[i].KPI_ID);
        				}
        			} else {            
        				html += '<tr><td colspan="5" style="text-align:center;">No data to display</td></tr>';
        			}
        			
        			$('#resultTable').html(html);

        			//add class
        			$('#fixedTable > tbody > tr:odd').addClass('odd');
        			$('#fixedTable > tbody > tr').addClass('tr_hover');
        			
        			
        			//테이블 컬럼 머지
        			mergeTable(mergeList); 
        			setMargin();
        		}); 
    			
        	}
        	//입력
        	function insert(){
        		if($("#hiddenInputKpiID").val()==""){
        			$i.dialog.warning("SYSTEM", "지표를 선택해주세요");
        			return; 
        		}
        		if(Enumerable.From($("[name=attachFile]")).Where(function(c){return $(c).val()!="";}).Count()==0){
        			$i.dialog.warning("SYSTEM", "파일을 등록 해 주세요.");
        			return;
        		}
        		$("#frmUploadAttachFile").attr("action","./insert");
        		$("#frmUploadAttachFile").submit(); 
        	}
        	//삭제
        	function remove( idx){    
				if(globalUploadAble==false) $i.dialog.error("SYSTEM","실적 입력 기간이 아니기 때문에 삭제가 불가능 합니다.");
				else{
					
					$i.dialog.confirm("SYSTEM","데이터를 삭제 하시겠습니까?(삭제 한 자료는 복구 되지 않습니다.)",
							function(){
						$("#frmUploadAttachFile").attr("action","./remove");
						$("#hiddenRemoveFileID").val(idx);
						$("#frmUploadAttachFile").submit();	
					});
				}
        		
        		 
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요. 
        	function initAjaxForm(){
        		$("#frmUploadAttachFile").ajaxForm({
                    beforeSubmit: function (data,form,option) {
                    	if(form.attr("action").indexOf("insert")!=-1)
        			 		$i.dialog.progress("SYSTEM","FILEUPLOAD","파일을 업로드 하였습니다.");  
                        return true;
                    },
                    success: function(response,status){
                    	var result = null;
                    	try{
                    		result = JSON.parse(response);
                    	}catch(e){
                    		$i.dialog.error("SYSTEM","오류가 발생 하였습니다.");	
                    		return; 
                    	}
                    	var alert = null;  
            			if(result.returnCode!="SUCCESS") {
            				alert = $i.dialog.error;           				
            			}
            			else {
            				alert = $i.dialog.alert;
            				refreshKpiAttachFileList();
            				search();   
            			}  
            			//삭제 후 alert
            			if($("#frmUploadAttachFile").attr("action").indexOf("remove")!=-1 )
            				alert("SYSTEM", result.returnMessage);
                    },
                    error: function(){
        				
                    }                               
                });
        	} 
        	function refreshKpiAttachFileList(){
        		kpiAttachFileList(
	        		$("#hiddenInputYear").val(),
					$("#hiddenInputMonth").val(),
					$("#hiddenInputKpiID").val(),
					$("#hiddenInputScID").val(),					
					$("#spanKpiName").html()
				);
        	}
        	function clearKpiAttachFileList(){
        		$("#hiddenInputYear").val("");
				$("#hiddenInputMonth").val("");
				$("#hiddenInputScID").val("");
				$("#hiddenInputKpiID").val("");
				$("#spanKpiName").html("");        	
				
				var html = ""; 
	    		html += '<tr>';
	    		html += '	<td style="width:80%; text-align:left; text-indent: 10px;"><input type="file" id="attachFile" name="attachFile" size="99%" style="width:99%; float:left; margin-right:5px; margin-bottom:0px;"/></td>';
	    		html += '	<td style="width:20%; text-align:center;"><img src="../../resources/cmresource/image/icon-minus.png" alt="minus" width="19" style="padding:2px 0; cursor:pointer;" onclick="deleteEmptyAttachFile(this);" /></td>';
	    		html += '</tr>';
	    		$('#attachFile').html(html);
        	}
        	function kpiAttachFileList(year, month, kpiId, scId, kpiNm, unId, chargeID){
        		
        		$("#hiddenInputYear").val(year);
				$("#hiddenInputMonth").val(month);
				$("#hiddenInputScID").val(scId);
				$("#hiddenInputKpiID").val(kpiId);
				$("#hiddenunid").val(unId);
				$("#hiddenchargeID").val(chargeID);      
				$("#spanKpiName").html(kpiNm); 
				//선택되지 않은 지표들에 대한 처리
				$("#resultTable tr").find("td").style("background-color","#FFFFFF","important");
				$("#resultTable tr").find("td").style("border-color","#e1e1e1","important");   
				
				//선택된 지표의 tr스타일 처리.
				$("#resultTable tr[data-kpiId='"+kpiId+"']").find("td").style("background-color","#376BA1","important");
				$("#resultTable tr[data-kpiId='"+kpiId+"']").find("td").style("border-color","#376BA1","important");
				
				var task = $i.sqlhouse(225,{S_KPI_ID:kpiId, S_YYYY:year,S_MM:month}); 
				task.done(function(result){
					if(result.returnCode!="SUCCESS"){
						$i.dialog.error(result.returnMessage);
						return;
					}
					trDisplay(result.returnArray);
				});
				
			}
        	function trDisplay(data){   
    			var html = ''; 
    			var chargeID = $("#hiddenchargeID").val();
        		var unId = $("#hiddenunid").val();   
				if(chargeID==unId&&globalUploadAble){
					$("#btnInsert").show();  
					$("#plusicon").show();
				}else{
					$("#btnInsert").hide(); 
					$("#plusicon").hide(); 
				}
    			if(data != null && data.length>0) {
    				for(var i=0; i<data.length; i++) {
    					html += '<tr>';
    					html += '	<td style="width:80%; text-align:left; text-indent: 10px;">'+setImgIcon(data[i].FILE_ORG_NAME)+data[i].FILE_ORG_NAME+'</td>';
    					if(chargeID==unId){
    						html += '	<td style="width:20% !important; text-align:center;"><img src="../../resources/cmresource/image/icon-minus.png" alt="minus" width="19" style="padding:2px 0; cursor:pointer;" onclick="remove(\''+data[i].FILE_ID+'\');" /></td>';
    					}else{
    						html += '	<td style="width:20% !important; text-align:center;"></td>';
    					}
    					html += '	</td>';
    					html += '</tr>';
    				}	
    			} else {
    				html += '<tr>';
    				html += '	<td style="width:80%; text-align:left; text-indent: 10px;"><input type="file" id="attachFile" name="attachFile" size="99%" style="width:99%; float:left; margin-right:5px; margin-bottom:0px;"/></td>';
    				if(chargeID==unId){      
    					html += '	<td style="width:20%; text-align:center;"><img src="../../resources/cmresource/image/icon-minus.png" alt="minus" width="19" style="padding:2px 0; cursor:pointer;" onclick="deleteEmptyAttachFile(this);" /></td>';
    				}else{
    					html += '	<td style="width:20%; text-align:center;"></td>';    
    				}
    				html += '</tr>';
    			}

    			$('#attachFile').html(html);
    		}
        	function deleteEmptyAttachFile(ths) {
    			
    			if($(ths).parents('tbody').children('tr').length <= 1) {
    				var selected = $(ths).parents('tr:first').find(':input[name="fileNm"]');
    				selected.clearFields();
    			} else {
    				$(ths).parents('tr:first').remove();	
    			}
    		}
        	function initButton(){
        		 $("#btnSearch").jqxButton({width:'', theme:'blueish'}); 
        		$("#btnInsert").jqxButton({width:'', theme:'blueish'}); 
        	}

        	function initCboMonth(){
        		var monthTask = $i.service("getMonthForEvalUser",[$("#hiddenEvaGbn").val(),"${param.yyyy}"]);
        		monthTask.done(function(result){ 
        			var source =  
        			{ 
        				datatype:"json", 
        				datafields:[
        					{ name : "MM_ID" },
        					{ name : "MM_NM" }
        				],
        				id:"id",
        				localdata:result.returnArray,
        				async : false
        			};
        			var dataAdapter = new $.jqx.dataAdapter(source);
        			$("#cboMonth").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "MM_NM", valueMember: "MM_ID", dropDownWidth: 100, dropDownHeight: 80, width: 100, height: 22,  theme:'blueish'});        			
        			search(true);
        		}); 
        	}
        	function mergeTable(mergeList) {
    			//테이블 컬럼 머지
    			var tempList = mergeList;
    			mergeList = [];
    		
    			$.each(tempList, function(i, el){
    				if($.inArray(el, mergeList) === -1) mergeList.push(el);
    			});
    			
    			var cnt = 0;
    			for(var i=0;i<mergeList.length;i++){
    				var k = mergeList[i];
    				cnt = $('[name="'+k+'"]').length;
    				$('[name="'+k+'"]').not(':first').remove().end().attr('rowspan', cnt);
    			}
    			
    			tempList = null;
    			mergeList = null;
    		}
        	function setImgIcon(filename) {
    			var knownExt = {
    				xls:1,xlsx:1,ppt:1,pptx:1,doc:1,docx:1,hwp:1,pdf:1,txt:1
    			};
    			var ext = filename.substr(filename.lastIndexOf('.')+1, filename.length).toLowerCase();
    			var path = "../../resources/cmresource/image/fileExtIcon/";
				if(knownExt[ext]==undefined){
    				ext = "etc"	;
    			}
    			var img = '<img src="'+path+ext+'.png" alt="'+ext+'" style="margin-right: 5px; margin-left: 5px;"/>';  
    			
    			return img;
    		}
        	function setMargin() { 
    			//헤더에 마진 추가
    			var len = $('#fixedTable > tbody').css('height');
    			
    			if(parseInt(len)>parseInt($('#fixedTable').parents('.body').css('height'))) {
    				$('.headtable').eq(0).css('margin-right','17px');
    			} else {
    				$('.headtable').eq(0).css('margin-right','0px');
    			}	
    		} 
        	function addFileInput() {
    			if($('#hiddenInputKpiID').val() != '') {
    				var html = '';
    				html += '<tr>';
    				html += '	<td style="width:80%; text-align:center;"><input type="file" id="attachFile" name="attachFile" size="99%" style="width:99%; float:left; margin-right:5px; margin-bottom:0px;"/></td>';
    				html += '	<td style="width:20%; text-align:center;"><img src="../../resources/cmresource/image/icon-minus.png" alt="minus" width="19" style="padding:2px 0; cursor:pointer;" onclick="deleteFile(this);" /></td>';
    				html += '</tr>';
    			
    				$('#attachFile').append(html);
    			}else{
    				$i.dialog.warning("SYSTEM","지표를 선택 하세요");
    			}
    		} 
        	function deleteFile(ths) {
    			
    			if($(ths).parents('tbody').children('tr').length <= 1) {
    				var selected = $(ths).parents('tr:first').find(':input[name="attachFile"]');
    				selected.clearFields();
    			} else {
    				$(ths).parents('tr:first').remove();	
    			}
    		}
        	
        	function fileDown(fileId){
    			location.replace("./download?fileid=" + fileId);
    		} 
        	
        	
        	function initSummary(){
        		$("#summaryTable").empty();
        		var scid =  $("#txtTabscid", parent.document).val();          
        		var gubn = $("#txtGubn", parent.document).val();         
    			var task = $i.sqlhouse(230,{S_YYYY:"${param.yyyy}",S_MT_ID:scid,S_GUBN:gubn,S_EVA_GBN:$("#hiddenEvaGbn").val(),S_MM:$("#cboMonth").val()});
    			task.done(function(result){      
    				var res = result.returnArray;
    				var html = ""; 
    				if(res.length > 0){      
    					for(var i=0;i<res.length;i++){
    						html += '<tr>'; 
    						html += '<td style="text-align:center;" >'; 
    						html += '	<input type="hidden" id="scId" value="'+res[i].SC_ID+'" />';
    						html += '	<a style="margin-left:10px !important; color:black;text-decoration:underline;" href="javaScript:evalKpiList(\''+res[i].SC_ID+'\');" >'+res[i].SC_NM+'</a>';          
        					html += '</td>';         
    						html += '<td style="text-align:center;">'+res[i].TOTAL_KPI+'</td>'; 
    						html += '<td style="text-align:center;">'+res[i].KPI_COUNT+'</td>'; 
    						html += '<td style="text-align:center;">'+res[i].NULL_KPI+'</td>'; 
    						html += '</tr>';
    					}
    				}
    				else{
    					html += '<td colspan="4" class="cell t_center">No data to display</td>'; 
    				}
    				$("#summaryTable").append(html);
    			});
    		}
	    </script>
        <style type="text/css"> 
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
			.blueish .datatable table td{
				height:30px !important;
			}
		</style>
		   
    </head> 
    <body class='blueish' style="background:transparent !important; ">               
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap "style="width:99%; margin:0 10px;">  
		<div class="container  f_left" style="width:100%; margin:10px 0;">
        <div class="content f_left">  
			<div class="f_left group" style="width:100%;">   
				<form id="searchForm" name="searchForm" action="./crud">
					<input type="hidden" id="hiddenEvaGbn" name="evagbn" value="${evagbn}" />
					<input type="hidden" id="hiddenyyyy" name="yyyy" value="${param.yyyy}" />    
					<input type="hidden" id="txtTabSubstr" name="tabSubstr"/>            
					<div class="label type2 f_left">평가구분:<span class="label sublabel" id="spanEvaLabel">${evagbnName}</span></div> 
					<div class="label type2 f_left"style="margin-right: 2px;">월:</div>
					<div class="combobox f_left" style="margin-right: 5px;">
						<div id="cboMonth" name="month"> 
						</div>
					</div>
			</div>
		</div>
					<!-- <div class="label type1 f_left">담당자:<span class="label sublabel" id="spanMyName"></span></div> --> 
					<div class="group_button f_right">
						<div class="button type2 f_left" style="margin-right:3px;">
							 <input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search(true);"/> 
						</div>
						<div class="button type2 f_left">   
							<input type="button" value="저장" id='btnInsert' width="100%" height="100%" onclick="insert();"/>
						</div> 
					</div>
				</form>
			</div>

			<div class="container  f_left" style="width:100%; margin:10px 0;">   
				<div class="content f_left" style=" width:100%; margin:0%;">     
					<div class="blueish table f_left" style="width:100%; height:100%;overflow:hidden;  margin:0px 0;  ">  
									<table summary="증빙자료 업로드 요약" style="width:100%;"  class="none_hover none_alt">
										<col width="130"/>   
								        <col width="130"/> 
								        <col width="130"/>  
								        <col width="130"/> 
										<thead style="width:100%;">
											<tr>            
												<th scope="col" style='width:25%'>평가조직</th>  
												<th scope="col" style='width:25%'>총 지표수</th>
												<th scope="col" style='width:25%'>등록 지표수</th> 
												<th scope="col" style='width:25%'>미등록 지표수</th>  
											</tr> 
										</thead>
										<tbody id="summaryTable"> 
											<tr>
												<td colspan="4" style="text-align:center;">No data to display</td>
											</tr>
										</tbody>
									</table>
					</div>
				</div>
				<div class="content f_left" style=" width:100%; margin-top:20px;">        
					<div class="blueish datatable f_left" style="width:100%; height:230px; margin:0px 0; overflow:hidden;">
						<div class="datatable_fixed" style="width:100%; height:355px; float:left; padding:25px 0;"><!--group으로 헤더높이가 길어질 경우 padding  (기본높이는 25px)줘야 함-->
							<div style=" height:58%; overflow-x:hidden;"><!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 600px - 50px= 550px)--> 
									<table summary="증빙자료 리스트" style="width:100%;"  class="none_hover none_alt">   
										<col width="130"/>   
								        <col width="130"/>
								        <col width="130"/>  
								        <col width="130"/> 
										<thead style="width:100%;"> 
											<tr>      
												<th scope="col" style='width:15%'>평가조직</th>
												<th scope="col" style='width:30%'>지표</th>
												<th scope="col" style='width:12%'>담당자</th>
												<th scope="col" style='width:13%'>평가방식</th> 
												<th scope="col" style='width:30%'>증빙자료</th>
												<th style="width:1%; min-width:17px;"></th>
											</tr>
										</thead>
										<tbody id="resultTable" style="width:100%;">
											<tr>
												<td colspan="4" style="text-align:center;">No data to display</td>
											</tr>
										</tbody> 
									</table>  
							</div>
						</div>
					</div>
				</div>
				<div class="content f_left" style=" width:100%; margin-top:10px;">
					<span class='label  type2' >지표명 :</span><span id="spanKpiName" class='label' >지표명 :</span>        
					<div class="blueish table f_left" style="width:100%;  margin:0px 0;  "> 
								<form id="frmUploadAttachFile" name="uploadAttachFile" action="./insert" method="POST">    
									<input type="hidden" id="hiddenInputYear" name = "year" value="${param.yyyy}"/>
									<input type="hidden" id="hiddenInputMonth" name = "month" />
									<input type="hidden" id="hiddenInputScID" name = "scid" />
									<input type="hidden" id="hiddenInputKpiID" name = "kpiid" />
									<input type="hidden" id="hiddenRemoveFileID" name = "removefileid" />
									<table summary="증빙자료 업로드" style="width:100%;"  class="none_hover none_alt">
										<col width="130"/>
								        <col width="130"/>  
										<thead style="width:100%;">
											<tr> 
												<th scope="col" style='width:50%'>증빙자료</th>
												<th scope="col" style='width:50%'><img src="../../resources/cmresource/image/icon-plus.png" alt="plus" id="plusicon" width="19" style="padding:2px 0; cursor:pointer;" onclick="addFileInput();"></th> 
											</tr> 
										</thead>
										<tbody id="attachFile">  
											<tr>
												<td></td> 
												<td></td>
											</tr>
										</tbody>
									</table>
								</form>
					</div> 
					<div style="color:#ff0000; width:100%; margin: 5px 0px; clear: both; font-size: 12px;"> ※ 10M 이하 파일만 등록 할 수 있습니다.</div> 
				</div>
			</div>
		</div>
	</body>
</html>

