<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
        <title>담당자 종합현황</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css">
		<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css">
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$("#btnSearch").jqxButton({width:'', theme:'blueish'});  //조회 버튼
        		init();
        	}); 
        	function init(){
        		var resultYearDat = $i.sqlhouse(13,{COML_COD:"YEARS"});
        		resultYearDat.done(function(res){
        			var source = 
        			{
        				datatype:"json",
        				datafields:[
        					{ name : "COM_COD"},
        					{ name : "COM_NM"}
        				],
        				id:"id",
        				localdata:res,
        				async : false
        			};
        			var dataAdapter = new $.jqx.dataAdapter(source);
        			$("#cboSearchResultYear").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,  theme:'blueish'});  
        			scNmChange();
	        			$('#cboSearchResultYear').bind('change', function() {
							scNmChange($(this).val());
						});
        		});
        	}
    		function scNmChange(){
    			var pEvaGbn = "${param.pEvaGbn}";
    			var empData = $i.sqlhouse(285, {S_YYYY:$("#cboSearchResultYear").val()});
    			empData.done(function(res){
    				var source = 
        			{	
    					datatype:"json",
    					datafields:[
    							{ name: 'SC_ID'},
    							{ name: 'SC_NM'}
    						],
    	    				id:"id",
    	    				localdata:res,
    	    				async : false
    				};
    			var dataAdapter = new $.jqx.dataAdapter(source);
    			$('#scnmCombo').jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'SC_NM', valueMember: 'SC_ID', dropDownWidth: 240, dropDownHeight: 300, width: 240, height: 22, theme:'blueish'});     
    			$('#scnmCombo').jqxComboBox({ selectedIndex: 0 }); 
    			chargeNmChange();
	    			$('#scnmCombo').bind('change', function() {
						chargeNmChange($(this).val());
					});
    			});
        	}
    		function chargeNmChange(){
    			var pEvaGbn = "${param.pEvaGbn}";
    			var empData = $i.sqlhouse(286, {S_SC_ID:$("#scnmCombo").val(), S_YYYY:$("#cboSearchResultYear").val()});
    			empData.done(function(res){
    				var source = 
        			{	
    					datatype:"json",
    					datafields:[
    							{ name: 'KPI_CHARGE_ID'},
    							{ name: 'KPI_CHARGE_NM'}
    						],
    	    				id:"id",
    	    				localdata:res,
    	    				async : false
    				};
    			var dataAdapter = new $.jqx.dataAdapter(source);
    			$('#kpichargenmCombo').jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'KPI_CHARGE_NM', valueMember: 'KPI_CHARGE_ID', dropDownWidth: 180, dropDownHeight: 300, width: 180, height: 22, theme:'blueish'});     
    			$("#kpichargenmCombo").jqxComboBox('selectIndex', 0 ); 
    			ownerNmChange();
    			});
        	}
    		function ownerNmChange(){
    			var pEvaGbn = "${param.pEvaGbn}";
    			var empData = $i.sqlhouse(287, {S_SC_ID:$("#scnmCombo").val(), S_YYYY:$("#cboSearchResultYear").val()});
    			empData.done(function(res){
    				var source = 
        			{	
    					datatype:"json",
    					datafields:[
    							{ name: 'OWNER_USER_ID'},
    							{ name: 'OWNER_USER_NM'}
    						],
    	    				id:"id",
    	    				localdata:res,
    	    				async : false
    				};
    			var dataAdapter = new $.jqx.dataAdapter(source);
    			$('#ownernmCombo').jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: 'OWNER_USER_NM', valueMember: 'OWNER_USER_ID', dropDownWidth: 180, dropDownHeight: 300, width: 180, height: 22, theme:'blueish'});     
    			$("#ownernmCombo").jqxComboBox('selectIndex', 0 );      
    			makeResultList();  
    			});
        	}
         	function makeResultList(){
        		var data = $i.sqlhouse(284,{S_YYYY:$("#cboSearchResultYear").val(),S_EVA_GBN:"${param.pEvaGbn}",S_SC_ID:$("#scnmCombo").val(),S_OWNER_USER_ID:$("#ownernmCombo").val(),S_KPI_CHARGE_ID:$("#kpichargenmCombo").val()});
        		data.done(function(res){    
        			
        		    if($("#chargeConfirmList > tr").length > 0){
        				$("#chargeConfirmList > tr").remove();                
        			}    
         			if(res.returnCode == "SUCCESS"){
        				if(res.returnArray.length > 0){
        					var col1List = new Array();
        					var col2List = new Array();
        					var col3List = new Array();
        					var col4List = new Array();
        					var col5List = new Array();
        					
        					var html = "";
        					var dataList = res.returnArray;
        					for(var i=0;i<dataList.length;i++){   
        						html +="<tr>";    
        						html +="<td style='width:12%;' name='scnm"+dataList[i].SC_ID+"'><div class='cell t_left'>"+dataList[i].SC_NM+"</div></td>";
        						html +="<td style='width:15%;color:black; text-decoration:underline;' name='stranm"+dataList[i].SC_ID+"_"+dataList[i].STRA_ID+"'><div style='cursor:pointer !important;' class='cell t_left' onclick=\"javascript:onView('"+dataList[i].SC_ID+"','"+dataList[i].SC_NM+"','"+dataList[i].KPI_NM+"','"+dataList[i].KPI_ID+"','"+dataList[i].STATUS+"','"+dataList[i].MT_CD+"','"+dataList[i].STRA_ID+"','"+dataList[i].SUBSTRA_ID+"','"+dataList[i].CSF_ID+"','A')\";'>"+dataList[i].STRA_NM+"</div>";
        						html +="<input type='hidden' name='grade' value='"+dataList[i].GRADE+"' />";  
        						html +="<input type='hidden' name='gubn' />";    
        						html +="</td>";        
        						html +="<td style='width:15%;color:black; text-decoration:underline;' name='substranm"+dataList[i].SC_ID+"_"+dataList[i].STRA_ID+"_"+dataList[i].SUBSTRA_ID+"'><div class='cell t_left' style='cursor:pointer !important;' onclick=\"javascript:onView('"+dataList[i].SC_ID+"','"+dataList[i].SC_NM+"','"+dataList[i].KPI_NM+"','"+dataList[i].KPI_ID+"','"+dataList[i].STATUS+"','"+dataList[i].MT_CD+"','"+dataList[i].STRA_ID+"','"+dataList[i].SUBSTRA_ID+"','"+dataList[i].CSF_ID+"','B')\";'>"+dataList[i].SUBSTRA_NM+"</div></td>";
        						html +="<td style='width:15%;color:black; text-decoration:underline;' name='csfnm"+dataList[i].SC_ID+"_"+dataList[i].STRA_ID+"_"+dataList[i].SUBSTRA_ID+"_"+dataList[i].CSF_ID+"'><div class='cell t_left' style='cursor:pointer !important;' onclick=\"javascript:onView('"+dataList[i].SC_ID+"','"+dataList[i].SC_NM+"','"+dataList[i].KPI_NM+"','"+dataList[i].KPI_ID+"','"+dataList[i].STATUS+"','"+dataList[i].MT_CD+"','"+dataList[i].STRA_ID+"','"+dataList[i].SUBSTRA_ID+"','"+dataList[i].CSF_ID+"','C')\";'>"+dataList[i].CSF_NM+"</div></td>"; 
        						html +="<td style='width:15%;color:black; text-decoration:underline;' name='kpinm"+dataList[i].SC_ID+"_"+dataList[i].STRA_ID+"_"+dataList[i].SUBSTRA_ID+"_"+dataList[i].CSF_ID+"_"+dataList[i].KPI_ID+"'><div class='cell t_left' style='cursor:pointer !important;'  onclick=\"javascript:onView('"+dataList[i].SC_ID+"','"+dataList[i].SC_NM+"','"+dataList[i].KPI_NM+"','"+dataList[i].KPI_ID+"','"+dataList[i].STATUS+"','"+dataList[i].MT_CD+"','"+dataList[i].STRA_ID+"','"+dataList[i].SUBSTRA_ID+"','"+dataList[i].CSF_ID+"','D')\";'>"+dataList[i].KPI_NM+"</div></td>"; 
        						if(dataList[i].EVA_GRDNM==''){
        							html +="<td style='width:7%;color:red;'><div class='cell t_center'>미평가</div></td>";   
        						}else if(dataList[i].STATUS != 'D'){
        							html +="<td style='width:7%;color:red;'><div class='cell t_center'>미평가</div></td>";  
        						}else{
        							html +="<td style='width:7%;color:blue;'><div class='cell t_center'>"+dataList[i].EVA_GRDNM+"</div></td>";
        						}
        						html +="<td style='width:10%;'><div class='cell t_center'>"+dataList[i].KPI_CHARGE_NM+"</div></td>";  
        						html +="<td style='width:10%;'><div class='cell t_center'>"+dataList[i].OWNER_USER_NM+"</div></td>"; 
        					
        						if(i==0 || dataList[i].SC_ID != dataList[i-1].SC_ID) {
        							col1List.push('scnm'+dataList[i].SC_ID);
        						}
        						if(i==0 || dataList[i].SC_ID+"_"+dataList[i].STRA_ID != dataList[i-1].SC_ID+"_"+dataList[i-1].STRA_ID) {
        							col2List.push('stranm'+dataList[i].SC_ID+"_"+dataList[i].STRA_ID);
        						}
        						if(i==0 || dataList[i].SC_ID+"_"+dataList[i].STRA_ID+"_"+dataList[i].SUBSTRA_ID != dataList[i-1].SC_ID+"_"+dataList[i-1].STRA_ID+"_"+dataList[i-1].SUBSTRA_ID) {
        							col3List.push('substranm'+dataList[i].SC_ID+"_"+dataList[i].STRA_ID+"_"+dataList[i].SUBSTRA_ID);
        						} 
        						if(i==0 || dataList[i].SC_ID+"_"+dataList[i].STRA_ID+"_"+dataList[i].SUBSTRA_ID+"_"+dataList[i].CSF_ID != dataList[i-1].SC_ID+"_"+dataList[i-1].STRA_ID+"_"+dataList[i-1].SUBSTRA_ID+"_"+dataList[i-1].CSF_ID) {
        							col4List.push('csfnm'+dataList[i].SC_ID+"_"+dataList[i].STRA_ID+"_"+dataList[i].SUBSTRA_ID+"_"+dataList[i].CSF_ID);	
        						} 
        						if(i==0 || dataList[i].SC_ID+"_"+dataList[i].STRA_ID+"_"+dataList[i].SUBSTRA_ID+"_"+dataList[i].CSF_ID+"_"+dataList[i].KPI_ID != dataList[i-1].SC_ID+"_"+dataList[i-1].STRA_ID+"_"+dataList[i-1].SUBSTRA_ID+"_"+dataList[i-1].CSF_ID+"_"+dataList[i-1].KPI_ID) {
        							col5List.push('kpinm'+dataList[i].SC_ID+"_"+dataList[i].STRA_ID+"_"+dataList[i].SUBSTRA_ID+"_"+dataList[i].CSF_ID+"_"+dataList[i].KPI_ID);    
        						}
									
        				}
        					$("#chargeConfirmList").append(html);    
        					var rowspanCount1 = 0;
        					var rowspanCount2 = 0;
        					var rowspanCount3 = 0;
        					var rowspanCount4 = 0;
        					var rowspanCount5 = 0;
            				
        					for(var i=0; i<col1List.length; i++) {
        						rowspanCount1 = $('[name="'+col1List[i]+'"]').length;
        						$('[name="'+col1List[i]+'"]:eq(0)').attr("rowSpan", rowspanCount1);
        					 	$('[name="'+col1List[i]+'"]:not(:eq(0))').remove();
        					}
        					for(var i=0; i<col2List.length; i++) {
        						rowspanCount2 = $('[name="'+col2List[i]+'"]').length;
        						$('[name="'+col2List[i]+'"]:eq(0)').attr("rowSpan", rowspanCount2);
        					 	$('[name="'+col2List[i]+'"]:not(:eq(0))').remove();
        					}
        					for(var i=0; i<col3List.length; i++) {
        						rowspanCount3 = $('[name="'+col3List[i]+'"]').length;
        						$('[name="'+col3List[i]+'"]:eq(0)').attr("rowSpan", rowspanCount3);
        					 	$('[name="'+col3List[i]+'"]:not(:eq(0))').remove();
        					} 
        					for(var i=0; i<col4List.length; i++) {
        						rowspanCount4 = $('[name="'+col4List[i]+'"]').length;
        						$('[name="'+col4List[i]+'"]:eq(0)').attr("rowSpan", rowspanCount4);
        					 	$('[name="'+col4List[i]+'"]:not(:eq(0))').remove();
        					} 
        					for(var i=0; i<col5List.length; i++) {
        						rowspanCount5 = $('[name="'+col5List[i]+'"]').length;
        						$('[name="'+col5List[i]+'"]:eq(0)').attr("rowSpan", rowspanCount5);
        					 	$('[name="'+col5List[i]+'"]:not(:eq(0))').remove();
        					} 
    						
        			}
         			}  
        		});   
        	} 
			function onView(scId, scNm, kpiNm, kpiId, status, mtCd ,straId, substrId, csfId, gubn) {
				var yyyy = $('#cboSearchResultYear').val();   
				var kpichargeId = $('#kpichargenmCombo').val();  
				var owneruserId	 = $('#ownernmCombo').val();  
				var pEvaGbn = $('#resultEvaGbn').val();
				window.open('./confirmResultPopup?yyyy='+yyyy+'&scId='+scId+'&scNm='+scNm+'&kpiNm='+kpiNm+'&kpiId='+kpiId+'&status='+status+'&pEvaGbn='+pEvaGbn+'&mtCd='+mtCd+'&straId='+straId+'&substrId='+substrId+'&csfId='+csfId+'&kpichargeId='+kpichargeId+'&owneruserId='+owneruserId+'&gubn='+gubn);
			}   
	    </script>    
    </head> 
    <body class='blueish' style="background:transparent !important; ">               
<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:98% !important; margin:0 auto !important;">
    	<div class="container  f_left" style="width:98%;">  
		    <div class="header f_left" style="width:100%; height:27px; margin:10px 0">		
				<div class="label type1 f_left">년도:</div>      
				<div class="combobox f_left"  id='cboSearchResultYear' name="searchResultYear"></div>
				<div class="label type1 f_left">부서:</div>
				<div style="float:left;width:100px; margin-right:10px;" id="scnmCombo" class="iwidget_combobox"></div>
				<div class="label type1 f_left">담당자:</div>
				<div style="float:left;width:100px; margin-right:10px;" id="kpichargenmCombo" class="iwidget_combobox"></div>
				<div class="label type1 f_left">책임자:</div>
				<div style="float:left;width:100px; margin-right:10px;" id="ownernmCombo" class="iwidget_combobox"></div>
				<div class="group_button f_right">  
					<div class="button type1 f_left">
						<input type="button" value="조회" id='btnSearch' width="100%" height="100%"  onclick="makeResultList();"/>  
					</div>    
				</div>   
			</div>
		</div>
			<div class="blueish datatable f_left " style="width:98%; height:640px; overflow:hidden;">
			<div class="datatable_fixed" style="width:100%;height:640px;"> 
				<div style=" height:615px !important; overflow-x:hidden;"><!--datatable과 datatable_fixed 높이 - 헤더높이 (ex: 600px - 50px= 550px)-->    
				<input type="hidden" id="resultEvaGbn" name="pEvaGbn" value="${param.pEvaGbn}" />
				<table width="100%" class="none_hover" cellspacing="0" cellpadding="0" border="0" style="margin-bottom:0 !important;">
					<thead style="width:100%;">  
						<tr>
						    <th style="width:12%">부서</th>       
							<th style="width:15%">전략</th>
							<th style="width:15%">세부전략</th>
							<th style="width:15%">CSF</th>
							<th style="width:15%">지표명</th>    
							<th style="width:7%;">판정</th>
						    <th style="width:10%">담당자</th>  
						    <th style="width:10%">책임자</th>   
						    <th style="width:1%; min-width:17px;"></th>
						</tr>
					</thead>
				<tbody id="chargeConfirmList">
				</tbody> 
				</table>
				</div>
			</div>	
			</div>
		</div>
	</body>
</html>

