<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>조직리스트</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
        <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
        <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
        <script>
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$("#btnSearch").jqxButton({width:'', theme:'blueish'});
				$("#txtSearch").jqxInput({placeHolder: "", height: 23, width: 250, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return makeGridList(); } });
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		search();
        		makeGridList();
        	}
        	//조회
        	function search(){
				var acctid = "${param.acctid}";
        		var yyyy = "${param.yyyy}";
        		var dat = $i.sqlhouse(50,{ACCT_ID:acctid, YYYY:yyyy});
        		dat.done(function(res){
        			var source =
					{
		                datatype: "json",
		                datafields: [
		                    { name: 'SC_ID'},
		                    { name: 'PSC_ID'},
		                    { name: 'SC_NM'}
		                ],
		                id : 'SC_ID',
		                localdata: res
		            };
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            dataAdapter.dataBind();
		            
		            var records = dataAdapter.getRecordsHierarchy('SC_ID', 'PSC_ID', 'items', [{ name: 'SC_ID', map: 'id'} ,{ name: 'SC_NM', map: 'label'}]);
		            $('#treeScidlist').jqxTree({source: records, height: '100%', width: '100%', theme:'blueish' });
		            
					// tree init
		            var items = $('#treeScidlist').jqxTree('getItems');
		            var item = null;
		            var size = 0;
		            var img = '';
		            var label = '';
		            var afterLabel = '';
		            
					for(var i = 0; i < items.length; i++) {
						item = items[i];
						
						if(i == 0) {
							//root
							$("#treeScidlist").jqxTree('expandItem', item);
							img = 'icon-folder.png';
							afterLabel = "";
						} else {
							//children
							size = $(item.element).find('li').size();
							
							if(size > 0) {
								//have a child
								img = 'icon-folder.png';
								afterLabel = "<span style='color: Blue;'> (" + size + ")</span>";
							} else {
								//no have a child
								img = 'icon-page.png';
								//내부에서 사용하는 트리는 팝업 없음.
								//afterLabel = "<span style='color: Blue; margin-left:5px; vertical-align:middle;'> <img src='${WWW.CSS}/images/img_icons/popupIcon.png' alt='팝업창' title='새창보기'/></span>";
								afterLabel = "";
							}
						}
						
						label = "<img style='float: left; margin-right: 5px;' src='../../resources/cmresource/image/" + img + "'/><span item-title='truec style='vertical-align:middle;'>" + item.label + "</span>" + afterLabel;
						$('#treeScidlist').jqxTree('updateItem', item, { label: label});
					}
		            
		            //add event
		            $('#treeScidlist')
		            .on("expand", function(eve) {
		            	var args = eve.args;
		            	var label = $('#treeScidlist').jqxTree('getItem', args.element).label.replace('icon-folder', 'icon-folderopen');
		            	args.element.firstChild.className = args.element.firstChild.className.replace('jqx-tree-item-arrow-collapse', '').replace('jqx-icon-arrow-right', '');
		            	$('#treeScidlist').jqxTree('updateItem', args.element, { label: label});
		            })
		            .on("collapse", function(eve) {
		            	var args = eve.args;
		            	var label = $('#treeScidlist').jqxTree('getItem', args.element).label.replace('icon-folderopen', 'icon-folder');
		            	$('#treeScidlist').jqxTree('updateItem', args.element, { label: label});
		            })
		            .on("select", function(eve) {
		            	sendParentData($('#treeScidlist').jqxTree('getItem', args.element).id,$('#treeScidlist').jqxTree('getItem', args.element).originalTitle);
		            });
        		});
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	//gridCallist 로우 상세
        	function makeGridList(){
				var acctid = "${param.acctid}";
        		var yyyy = "${param.yyyy}";
        		var dat = $i.sqlhouse(51,{ACCT_ID:acctid, YYYY:yyyy,SC_NM:$("#txtSearch").val()});
        		dat.done(function(res){
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'SC_ID', type: 'string'},
		                    { name: 'SC_NM', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var sendData = function(row, columnfield, value){
						var scId = $("#gridScidlist").jqxGrid("getrowdata", row)['SC_ID'];
		            	var scNm = $("#gridScidlist").jqxGrid("getrowdata", row)['SC_NM'];
		            	var link = "<a href=\"javaScript:sendParentData('"+scId+"', '" + scNm + "');\" style='color:black; text-decoration:underline;'>" + value + "</a>";
		            	return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
					}
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridScidlist").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                   { text: '조직ID', datafield: 'SC_ID', width: 216, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '조직명', datafield: 'SC_NM', align:'center', cellsalign: 'center', cellsrenderer: sendData}
						   
		                ]
		            });
        		});
        	}
        	function sendParentData(scId, scNm){
				$("#txtPscid", opener.document).val(scId) ;
				$("#txtPscnm", opener.document).val(scNm) ;
				self.close();
        	}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
		</style>   
    </head>
    <body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 1%;">
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<form id="searchForm" name="searchForm">
					<div class="content f_left" style="width:24%; margin-right:1%;">
						<div class="tree f_left" style="width:100%; height:540px;">
							<div id="treeScidlist"> </div>
						</div>
					</div>
				</form>
				<div class="content f_right" style="width:75%;">
					<div id="list">
						<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
							<div class="label type1 f_left">조직명 : </div>
							<div class="input f_left">
								<input type="text" id="txtSearch" name="searchscnm" />
							</div>
							<div class="group f_right pos_a" style="right:0px;">
								<div class="button type1 f_left">
									<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="makeGridList();" />
								</div>
							</div>
						</div>
						<div class="grid f_left" style="width:100%; height:470px;">
							<div id="gridScidlist"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

