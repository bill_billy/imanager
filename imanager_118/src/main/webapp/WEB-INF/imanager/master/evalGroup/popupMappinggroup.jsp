<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>조직리스트</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
        <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
        <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
        <script>
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$("#btnSearch").jqxButton({ width: '',  theme:'blueish'});
        		$("#txtSearch").jqxInput({placeHolder: "", height: 23, width: 250, minLength: 1, theme:'blueish' });
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		search('new');
        		makeGridList('new');
        	}
        	//조회
        	function search(gubn){
        		var pScid = "${param.scId}";
        		var dat = $i.post("./selectPopMappinggoup", {scid:pScid,searchscnm:$("#txtSearch").val()});
        		dat.done(function(res){
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'SUST_ID', type: 'string'},
		                    { name: 'SUST_NM', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridAllScidlist").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                   { text: '학과ID', datafield: 'SUST_ID', width: 216, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '학과명', datafield: 'SUST_NM', align:'center', cellsalign: 'center', cellsrenderer: alginLeft}
						   
		                ]
		            });
        		});
        	}
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	//gridCallist 로우 상세
        	function saveStart(){
				var tmpHtml = "";
				var scIds = new Array();
				var i = 0;
				var data = $("#gridScidlist").jqxGrid("getrows");
				for(var i=0;i<data.length;i++){
					scIds.push(data[i].SUST_ID);
					tmpHtml += data[i].SUST_ID + " ( " + data[i].SUST_NM + " )";
				}
				$(opener.document).find("#txtSustids").val(scIds);
				$(opener.document).find("#labelsustid").html(tmpHtml);
				
				self.close(); 
			}
			function add(){
				var src = "#gridAllScidlist";
				var target = "#gridScidlist";
				var rows = $(src).jqxGrid('getselectedrowindexes');
				var rowids=[];
				var datas=[];
				for(var i=0;i<rows.length;i++){
					var id = $(src).jqxGrid('getrowid',rows[i]);
					rowids.push(id);
					datas.push($(src).jqxGrid('getrowdatabyid',id));
				}
				$(target).jqxGrid('addrow', null, datas);
				$(src).jqxGrid('deleterow',rowids);
				$(src).jqxGrid('clearselection');
			}
			function remove2(){
				var src = "#gridScidlist";
				var target = "#gridAllScidlist";
				var rows = $(src).jqxGrid('getselectedrowindexes');
				var rowids=[];
				var datas=[];
				for(var i=0;i<rows.length;i++){
					var id = $(src).jqxGrid('getrowid',rows[i]);
					rowids.push(id);
					datas.push($(src).jqxGrid('getrowdatabyid',id));
				}
				$(target).jqxGrid('addrow', null, datas);
				$(src).jqxGrid('deleterow',rowids);
				$(src).jqxGrid('clearselection');
			}
			function addAll(){
				$('#gridAllScidlist').jqxGrid('selectallrows');
				add();
			}
			function removeAll(){
				$('#gridScidlist').jqxGrid('selectallrows');
				remove2();
			} 
        	function makeGridList(gubn){
        		var scId = "${param.scId}";
        		if(gubn == "new"){
        			var dat = $i.post("./choisePopMappingdept", {scid:scId});
	        		dat.done(function(res){
			            var source =
			            {
			                datatype: "json",
			                datafields: [
			                    { name: 'SUST_ID', type: 'string'},
			                    { name: 'SUST_NM', type: 'string'}
			                ],
			                localdata: res,
			                updaterow: function (rowid, rowdata, commit) {
			                    commit(true);
			                }
			            };
			            var alginCenter = function (row, columnfield, value) {//left정렬
							return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
						}; 
						var alginLeft = function (row, columnfield, value) {//left정렬
							return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
						}; 
						var alginRight = function (row, columnfield, value) {//right정렬
							return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
						};
			            var dataAdapter = new $.jqx.dataAdapter(source, {
			                downloadComplete: function (data, status, xhr) { },
			                loadComplete: function (data) { },
			                loadError: function (xhr, status, error) { }
			            });
			            var dataAdapter = new $.jqx.dataAdapter(source);
			            $("#gridScidlist").jqxGrid(
			            {
			              	width: '100%',
			                height:'100%',
							altrows:true,
							pageable: false,
			                source: dataAdapter,
							theme:'blueish',
			                columnsresize: true,
			                columns: [
			                   { text: '학과ID', datafield: 'SUST_ID', width: 216, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
			                   { text: '학과명', datafield: 'SUST_NM', align:'center', cellsalign: 'center', cellsrenderer: alginLeft}
							   
			                ]
			            });
	        		});
        		}
        	}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
		</style>   
    </head>
    <body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 1%;">
			<form id="searchForm" name="searchForm">
				<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
					<div class="label type1 f_left">조직명 : </div>
					<div class="input f_left">
						<input type="text" id="txtSearch" name="searchscnm" />
					</div>
					<div class="group f_right pos_a" style="right:0px;">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
					</div>
				</div>
			</form>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style="width:44%; margin-right:1%;">
					<div class="grid f_left" style="width:100%; height:500px;">
						<div id="gridAllScidlist"> </div>
					</div> 
				</div>
				<div class="move_button" style="width:10%; float:left; height:500px; margin:0 !important;">
					<ul class="move_button_align" style="text-align:center;">
						<li class="move1">
							<img src="../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-move-right.png" onmouseover="this.src='../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-hover-move-right.png';" onmouseout="this.src='../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-move-right.png';" alt="오른쪽으로" onclick="add();">
						</li>
						<li class="move2">
							<img src="../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-move-all-right.png" onmouseover="this.src='../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-hover-move-all-right.png';" onmouseout="this.src='../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-move-all-right.png';" alt="모두오른쪽으로"  onclick="addAll();">
						</li>   
						<li class="move3">
							<img src="../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-move-left.png" onmouseover="this.src='../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-hover-move-left.png';" onmouseout="this.src='../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-move-left.png';" alt="왼쪽으로" onclick="remove2();">
						</li>
						<li class="move4">
							<img src="../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-move-all-left.png" onmouseover="this.src='../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-hover-move-all-left.png';" onmouseout="this.src='../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-move-all-left.png';" alt="모두왼쪽으로" onclick="removeAll();" >
						</li>
						<li class="move4">
							<img src="../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-ok.png" onmouseover="this.src='../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-hover-ok.png';" onmouseout="this.src='../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-ok.png';" alt="저장" onclick="saveStart();">
						</li>
						<li class="move4">
							<img src="../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-close.png" onmouseover="this.src='../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-hover-close.png';" onmouseout="this.src='../../resources/cmresource/css/iplanbiz/theme/style01/img/btn-close.png';" alt="닫기" onclick="self.close();">
						</li>
					</ul>
				</div>
				<div class="content f_right" style="width:44%;">
					<div class="grid f_left" style="width:100%; height:500px;">
						<div id="gridScidlist"> </div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

