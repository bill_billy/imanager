<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>평가조직관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
        <script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
        <script>
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){     
        		$('#jqxSplitter').jqxSplitter({ height: 600, width: "100%",  panels: [{ size: '20%' }, { size: '80%'}], theme:"blueish"  }); 
        		$("#btnTopSearch").jqxButton({width:'', theme:'blueish'});
				$("#btnNew").jqxButton({ width: '',  theme:'blueish'});
				$("#btnList").jqxButton({ width: '', theme:'blueish'});
				$("#btnSave").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnSavenew").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnDelete").jqxButton({ width: '',  theme:'blueish'});
				$("#btnSearchdept").jqxButton({width:'', theme:'blueish'});
				$("#btnSearchscid").jqxButton({width:'', theme:'blueish'});
				$("#btnSearchpscid").jqxButton({width:'', theme:'blueish'});
				$("#txtSearch").jqxInput({placeHolder: "", height: 23, width: 250, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return makeGridList(); } });
				$('#dateStartdate').datepicker({
					changeYear: true,
					showOn: 'button',
					buttonImage: '../../resources/cmresource/image/icon-calendar.png',
					buttonImageOnly: true,
					dateFormat: "yymmdd", //20141212
				});
				$('#dateEnddate').datepicker({
					changeYear: true,
					showOn: 'button',
					buttonImage: '../../resources/cmresource/image/icon-calendar.png',
					buttonImageOnly: true,
					dateFormat: "yymmdd", //20141212
				});
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		var yeardat = $i.sqlhouse(13,{COML_COD:"YEARS"});
        		yeardat.done(function(res){
        			var source = 
        			{
        				datatype:"json",
        				datafields:[
        					{ name : "COM_COD" },
        					{ name : "COM_NM" }
        				],
        				id:"id",
        				localdata:res,
        				async : false
        			};
        			var dataAdapter = new $.jqx.dataAdapter(source);
        			$("#cboSearchyear").jqxComboBox({ selectedIndex: 0, source: dataAdapter, animationType: 'fade', dropDownHorizontalAlignment: 'right', displayMember: "COM_NM", valueMember: "COM_COD", dropDownWidth: 80, dropDownHeight: 80, width: 80, height: 22,  theme:'blueish'});
        			searchYear();
        			
        		});
                var dat = $i.sqlhouse(13,{COML_COD:"EVA_GRP"});
        		dat.done(function(res){
        			var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                        { name: 'COM_COD' },
	                        { name: 'COM_NM' }
	                    ],
	                    id: 'id',
						localdata:res,
						// url: url,
	                    async: false
	                };
	                var dataAdapter = new $.jqx.dataAdapter(source); 
	                $("#cboEvalg").jqxComboBox({selectedIndex: 0, source: dataAdapter,animationType: 'fade',dropDownHorizontalAlignment: 'right',displayMember: "COM_NM",valueMember: "COM_COD",dropDownWidth: 150,dropDownHeight: 60,width: 150,height: 23,theme:'blueish'});
        		});
        		var dat = $i.sqlhouse(13,{COML_COD:"EVA_GBN"});
        		dat.done(function(res){
        			var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                        { name: 'COM_COD' },
	                        { name: 'COM_NM' }
	                    ],
	                    id: 'id',
						localdata:res,
						// url: url,
	                    async: false
	                };
	                var dataAdapter = new $.jqx.dataAdapter(source); 
	                $("#cboEvagbn").jqxComboBox({selectedIndex: 0, source: dataAdapter,animationType: 'fade',dropDownHorizontalAlignment: 'right',displayMember: "COM_NM",valueMember: "COM_COD",dropDownWidth: 150,dropDownHeight: 60,width: 150,height: 23,theme:'blueish'});
        		});
        		var dat = $i.sqlhouse(13,{COML_COD:"EVAL_GY"});
        		dat.done(function(res){
        			var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                        { name: 'COM_COD' },
	                        { name: 'COM_NM' }
	                    ],
	                    id: 'id',
						localdata:res,
						// url: url,
	                    async: false
	                };
	                var dataAdapter = new $.jqx.dataAdapter(source); 
	                $("#cboScudc1").jqxComboBox({selectedIndex: 0, source: dataAdapter,animationType: 'fade',dropDownHorizontalAlignment: 'right',displayMember: "COM_NM",valueMember: "COM_COD",dropDownWidth: 150,dropDownHeight: 60,width: 150,height: 23,theme:'blueish'});
	                $("#cboScudc1").jqxComboBox("insertAt", {label:"=선택=", value:""},0);
        		});
        		selectDisplay('list', 'nopix');
        	}
        	//조회
        	function search(){
        		var acctid = "${paramAcctid}";
        		var dat = $i.sqlhouse(45,{ACCT_ID:acctid,YYYY:$("#cboSearchyear").val()});
        		dat.done(function(res){
        			var source =
					{
		                datatype: "json",
		                datafields: [
		                    { name: 'SC_ID'},
		                    { name: 'PSC_ID'},
		                    { name: 'SC_NM'}
		                ],
		                id : 'SC_ID',
		                localdata: res
		            };
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            dataAdapter.dataBind();
		            
		            var records = dataAdapter.getRecordsHierarchy('SC_ID', 'PSC_ID', 'items', [{ name: 'SC_ID', map: 'id'} ,{ name: 'SC_NM', map: 'label'}]);
		            $('#treeScidlist').jqxTree({source: records, height: '100%', width: '100%', theme:'blueish' });
		            
					// tree init
		            var items = $('#treeScidlist').jqxTree('getItems');
		            var item = null;
		            var size = 0;
		            var img = '';
		            var label = '';
		            var afterLabel = '';
		            
					for(var i = 0; i < items.length; i++) {
						item = items[i];
						
						if(i == 0) {
							//root
							$("#treeScidlist").jqxTree('expandItem', item);
							img = 'icon-foldernoopen.png';
							afterLabel = "";
						} else {
							//children
							size = $(item.element).find('li').size();
							
							if(size > 0) {
								//have a child
								img = 'icon-foldernoopen.png';
								afterLabel = "<span style='color: Blue;'> (" + size + ")</span>";
							} else {
								//no have a child
								img = 'icon-page.png';
								//내부에서 사용하는 트리는 팝업 없음.
								//afterLabel = "<span style='color: Blue; margin-left:5px; vertical-align:middle;'> <img src='${WWW.CSS}/images/img_icons/popupIcon.png' alt='팝업창' title='새창보기'/></span>";
								afterLabel = "";
							}
						}
						
						label = "<img style='float: left; margin-right: 5px;' src='../../resources/cmresource/image/" + img + "'/><span item-title='truec style='vertical-align:middle;'>" + item.label + "</span>" + afterLabel;
						$('#treeScidlist').jqxTree('updateItem', item, { label: label});
					}
		            
		            //add event
		            $('#treeScidlist')
		            .on("expand", function(eve) {
		            	var args = eve.args;
	            		var label = $('#treeScidlist').jqxTree('getItem', args.element).label.replace('icon-foldernoopen', 'icon-folderopen');
		            	args.element.firstChild.className = args.element.firstChild.className.replace('jqx-tree-item-arrow-collapse', '').replace('jqx-icon-arrow-right', '');
		            	$('#treeScidlist').jqxTree('updateItem', args.element, { label: label});
		            })
		            .on("collapse", function(eve) {
		            	var args = eve.args;
		            	var label = $('#treeScidlist').jqxTree('getItem', args.element).label.replace('icon-folderopen', 'icon-foldernoopen');
		            	$('#treeScidlist').jqxTree('updateItem', args.element, { label: label});
		            })
		            .on("select", function(eve) {
		            	getScidDetail($('#treeScidlist').jqxTree('getItem', args.element).id);
	            		selectDisplay('save','nopix');
		            });
		            $("#treeScidlist").jqxTree("expandAll");
        		});
        	}
        	//입력
        	function insert(gubn){
        		if($("#txtScid").val()!="100"&&$("#txtPscid").val().trim()==""){
        			$i.dialog.warning("SYSTEM","상위 조직을 선택하세요");
        			return;
        		}
        		if($("#txtScnm").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","조직 명을 입력하세요");
        			return;
        		}
        		if($("#dateStartdate").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","시작일을 입력하세요");
        			return;
        		}
        		if($("#dateEnddate").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","종료일을 입력하세요");
        			return;
        		}
        		$i.insert("#saveForm").done(function(args){
        			if(args.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM","저장 중 오류가 발생했습니다 : "+args.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM","저장 되었습니다");
        				search();
        				makeGridList();
						if(gubn == "list"){
							selectDisplay('list','nopix');
						}else{
							selectDisplay('save','pix');
						}
        				
        			}
        		});
//         		$("#saveForm").submit();
        	}
        	//삭제
        	function remove(){
        		var acctid = "${paramAcctid}";
        		var check = $i.sqlhouse(125,{ACCT_ID:acctid, SC_ID:$("#txtScid").val()});
        		check.done(function(res){
        			if(res.returnArray.length > 0){
        				$i.dialog.warning("SYSTEM","이미 사용중인 조직입니다");
        			}else{
        				$i.remove("#saveForm").done(function(args){
		        			if(args.returnCode == "EXCEPTION"){
		        				$i.dialog.error("SYSTEM",args.returnMessage);
		        			}else{
		        				$i.dialog.alert("SYSTEM","삭제 되었습니다.");
		        				search();
		        				makeGridList();
		        				selectDisplay('list','nopix');
		        			}
		        		}).fail(function(e){
		        			$i.dialog.error("SYSTEM","삭제 중 오류가 발생했습니다.");
		        		});
        			}
        		});
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        		$("#btnInsert").on("click", function(event){
        			$("#tblInsert").toggle(); 
        		}); 
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	function searchYear(){
        		search();
	        	makeGridList();
        	}
        	function fileDown(){
        		location.replace("./downloadFile?fileid="+$("#txtPptefile").val());
        	}
        	function getScidDetail(scid){
        		var dat = $i.post("./selectDetail",{scid:scid});
        		dat.done(function(res){
        			if(res.returnArray.length > 0){
        				$("#btnSavenew").hide();
        				$("#cboEvagbn").val(res.returnArray[0].EVA_GBN);  
        				$("#txtPscid").val(res.returnArray[0].PSC_ID);
        				$("#txtPscnm").val(res.returnArray[0].PSC_NM);
        				$("#txtScid").val(res.returnArray[0].SC_ID);
        				$("#txtScnm").val(res.returnArray[0].SC_NM);
        				$("#txtareaScdesc").val(res.returnArray[0].SC_DESC);
        				$("#txtareaEtcdesc").val(res.returnArray[0].ETC_DESC);
        				if(res.returnArray[0].EVAL_G != ''){
        					$("#cboEvalg").val(res.returnArray[0].EVAL_G);
        				}else{
        					$("#cboEvalg").jqxComboBox("selectIndex", 0);
        				}
        				if(res.returnArray[0].SC_UDC1 != ''){
        					$("#cboScudc1").val(res.returnArray[0].SC_UDC1);
        				}else{
        					$("#cboScudc1").jqxComboBox('selectIndex', 0 ); 
        				}
        				if(res.returnArray[0].PPTFILE != ''){
        					$("#divFiledown").show();
        					$("#divFileupload").hide();
        					$("#txtPptefile").val(res.returnArray[0].PPTFILE);
        				}else{
        					$("#divFiledown").hide();
        					$("#divFileupload").show();
        				}
        				
        				$("#txtSortorder").val(res.returnArray[0].SORT_ORDER);
        				$("#dateStartdate").val(res.returnArray[0].START_DAT);
        				$("#dateEnddate").val(res.returnArray[0].END_DAT);
        			}
        		});
        		var deptlist = $i.post("./getListDept",{scid:scid});
        		deptlist.done(function(res){
        			if(res.returnArray.length > 0){
        				var tmpHtml = "";
						var deptCd = "";
						for(var i=0;i<res.returnArray.length;i++){
							if(i != 0){
								tmpHtml += "<br />";
								deptCd += ",";
							}
							tmpHtml += res.returnArray[i].SC_NM + " ( " + res.returnArray[i].DEPT_CD + " ) ";
							deptCd += res.returnArray[i].DEPT_CD;
						}
						$("#labeldept").html(tmpHtml);
						$("#txtDeptids").val(deptCd);
        			}else{
        				$("#labeldept").html('');
						$("#txtDeptids").val('');
        			}
        		});
        		var scidlist = $i.post("./getListScid",{scid:scid});
        		scidlist.done(function(res){
        			if(res.returnArray.length > 0){
        				var tmpHtml = "";
						var sustIds = "";
						for(var i=0;i<res.returnArray.length;i++){
							if(i != 0){
								tmpHtml += "<br />";
								sustIds += ", ";
							}
							tmpHtml += res.returnArray[i].SUST_NM + " ( " + res.returnArray[i].SUST_ID + " ) ";
							sustIds += res.returnArray[i].SUST_ID;
						}
						$("#labelsustid").html(tmpHtml);
						$("#txtSustids").val(sustIds);
        			}else{
        				$("#labelsustid").html('');
						$("#txtSustids").val('');
        			}
        		});
        	}
        	function popSclist(){
				var acctid = "${paramAcctid}";
        		window.open('./popupMappingscid?yyyy='+$("#cboSearchyear").val()+'&acctid='+acctid, 'orgSearch','scrollbars=no, resizable=no, width=1200, height=560');  
        	}
        	function popMappingdept(){
        		window.open('./popupMappingdept?scId='+$("#txtScid").val(), 'popMappingdept','scrollbars=no, resizable=no, width=1200, height=600');
        	}
        	function popMappingSust(){
        		window.open('./popupMappinggroup?scId='+$("#txtScid").val(), 'popMappingdept','scrollbars=no, resizable=no, width=1200, height=600');
        	}
        	function makeGridList(){
        		var dat = $i.post("./selectByConditional", {searchscnm:$("#txtSearch").val(), scid:"100", yyyy:$("#cboSearchyear").val()});
        		dat.done(function(res){
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'SC_ID', type: 'string'},
		                    { name: 'SC_NM', type: 'string'},
		                    { name: 'PSC_NM', type: 'string'},
		                    { name: 'EVAL_G', type: 'string'},
		                    { name: 'START_DAT', type: 'string'},
							{ name: 'END_DAT', type: 'string'},
							{ name: 'TXT', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var detailrow = function(row,columnfield,value){
						var scid = $("#gridScidlist").jqxGrid("getrowdata", row).SC_ID;
						
						var link = "<a href=\"javascript:getDetailGrid('"+scid+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>"; 
					}
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridScidlist").jqxGrid(
		            {
		              	width: '100%',
		                height: 520,
						altrows:true,
						pageable: false,
						sortable: true,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                   { text: '조직ID', datafield: 'SC_ID', width: 107, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '조직명', datafield: 'SC_NM', width: 300, align:'center', cellsalign: 'center', cellsrenderer: detailrow},
						   { text: '상위조직명', datafield: 'PSC_NM', width: 250, align:'center', cellsalign: 'center', cellsrenderer: alginLeft},
						   { text: '평가군', datafield: 'EVAL_G', width: 221, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
						   { text: '시작일', datafield: 'START_DAT', width: 147, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
						   { text: '종료일', datafield: 'END_DAT', width:147, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
						   { text: '기간계 조직연결', datafield: 'TXT', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
						   
		                ]
		            });
        		});
        	}
        	function getDetailGrid(scid){
        		getScidDetail(scid);
		        selectDisplay('save','nopix');
        	}
        	function reset(){
        		
        		$("#txtScid").val('');
        		$("#txtScnm").val('');
        		$("#txtareaScdesc").val('');
        		$("#txtareaEtcdesc").val('');
        		$("#txtSortorder").val('');
        		$("#dateStartdate").val('');
        		$("#dateEnddate").val('99991231');
        		$("#labeldept").html('');
        		$("#txtDeptids").val('');
        		$("#labelsustid").html('');
        		$("#txtSustids").val('');
        		$("#btnSavenew").show();
//         		$("#cboEvalg")

        	}
        	function selectDisplay(obj, gubn){
        		reset();
				if(gubn == 'nopix'){
					$("#txtPscid").val('');
					$("#txtPscnm").val('');
				}
				if(obj == "save"){
					$("#save").show();
					$("#list").hide();
				}else if(obj == "list"){
					$("#save").hide();
					$("#list").show();
				}
			}
        	//신규 버튼 클릭 또는 초기화 해야 할 경우
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
		</style>   
    </head>
    <body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 1%;">
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<div class="label type1 f_left">평가년도:</div>
				<div class="combobox f_left"  id='cboSearchyear' name="searchyear"></div>
				<div class="label type1 f_left">조직명 : </div>
				<div class="input f_left">
					<input type="text" id="txtSearch" name="searchscnm" />
				</div>
				<div class="group_button f_right">
					<div class="button type1 f_left">
						<input type="button" value="조회" id='btnTopSearch' width="100%" height="100%" onclick="searchYear();makeGridList();" />
					</div>
					<div class="button type1 f_left">
						<input type="button" value="신규" id='btnNew' width="100%" height="100%" onclick="selectDisplay('save');" />
					</div>
				</div>
			</div>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style="width:100%; margin-right:1%;">
					<div class="splitter  f_left" style="width:100%; height:; margin:0;">
						<div id="jqxSplitter">
							<div>
								<div class="tree" style="width:100%; height:100%; border:none;">
									<div id="treeScidlist"> </div>
								</div>
							</div>
							<div>
								<div id="list">
									<div class="grid f_left" style="width:99%; height:97% !important; margin:3px 5px !important;">
										<div id="gridScidlist"> </div>
									</div>
								</div>
								<div id="save">
									<form id="saveForm" name="saveForm" method="post" action="./insert" target="saveframe" enctype="multipart/form-data">
										<div class="table  f_left" style="width:99%; height:100%; margin:3px 5px !important;" >
											<table width="100%" cellspacing="0" cellpadding="0" border="0">
												<tr>
													<th style="width:15% !important; font-weight:bold !important;">상위조건명</th>
													<td colspan="3">
														<input type="hidden" id="txtPscid" name="pscid" />
														<input type="text" id="txtPscnm" name="pscnm" class="input type2 f_left" style="width:50%; margin:3px 5px !important;" readonly="readonly" />
														<input type="button" value="검색" id="btnSearchpscid" width="100%" height="100%" style="margin:3px 5px !important;" onclick="popSclist();" />
													</td> 
												</tr>
												<tr>
													<th style="width:15% !important; font-weight:bold !important;">조직명</th>
													<td style="width:35% !important;">
														<input type="hidden" id="txtScid" name="scid" />    
														<input type="text" id="txtScnm" name="scnm" class="input type2 f_left" style="width:95%; margin:3px 5px !important;" />
													</td> 
													<th style="width:15% !important; font-weight:bold !important;">평가군</th>
													<td style="width:35% !important;"> 
														<div class="combobox f_left" id="cboEvalg" name="evalg" style="margin:3px 5px !important;"></div>
													</td>
												</tr>
												<tr>
													<th style="width:15% !important; font-weight:bold !important;">조직설명</th>
													<td colspan="3">
														<textarea class="textarea type2 f_left" style="width:99%; height:80px; margin:3px 5px !important;" id="txtareaScdesc" name="scdesc"></textarea> 
													</td>
												</tr>
												<tr>
													<th style="width:15% !important; font-weight:bold !important;">기타설명</th>
													<td colspan="3">
														<textarea class="textarea type2 f_left" style="width:99%; height:80px; margin:3px 5px !important;" id="txtareaEtcdesc" name="etcdesc"></textarea> 
													</td>
												</tr>
												<tr>
													<th style="width:15% !important; font-weight:bold !important;">평가구분</th>
													<td colspan="3">
														<div class="combobox f_left" id="cboEvagbn" name="evagbn" style="margin:3px 5px !important;"></div>
													</td>
												</tr>
												<tr>
													<th style="width:15% !important; font-weight:bold !important;">계열</th>
													<td>
														<div class="combobox f_left" id="cboScudc1" name="scudc1" style="margin:3px 5px !important;"></div>
													</td>
													<th style="width:15% !important; font-weight:bold !important;">정렬순서</th>
													<td>
														<input type="text" id="txtSortorder" name="sortorder" class="input type2 f_left" style="width:96%; margin:3px 5px !important;" />
													</td>
												</tr>
												<tr>
													<th style="width:15% !important; font-weight:bold !important;">시작일</th>
													<td>
														<input type="text" id="dateStartdate" name="startdate" class="input type2 f_left" style="width:85%; margin:3px 5px !important;" />
													</td>
													<th style="width:15% !important; font-weight:bold !important;">종료일</th>
													<td>
														<input type="text" id="dateEnddate" name="enddate" value="99991231" class="input type2 f_left" style="width:85%; margin:3px 5px !important;" />
													</td>
												</tr>
<!-- 												<tr> -->
<!-- 													<th style="width:15% !important; font-weight:bold !important;">미션/버전 업로드</th> -->
<!-- 													<td colspan="3"> -->
<!-- 														<div id="divFiledown"> -->
<!-- 															<input type="hidden" id="txtPptefile" name="pptefile"/> -->
<!-- 															<a href="javascript:fileDown();" id="aFilenm"></a> -->
<!-- 														</div> -->
<!-- 														<div class="fileupload" id="divFileupload"> -->
<!-- 															<div class="file_input" id="fileInput"> -->
<!-- 																<input type="text" id="txtShowfilenm" name="showfilenm" class="file_input_textbox" style="width:600px;" onchange="javascript:document.getElementById('txtFilenm').value = this.value;" readonly  /> -->
<!-- 																<div class="button"> -->
<!-- 																	<input type="button" value="찾아보기" class="file_input_button" /> -->
<!-- 																	<input type="file" id="txtFilenm" name="filenm" class="file_input_hidden" onchange="javascript:document.getElementById('txtShowfilenm').value = this.value;" /> -->
<!-- 																</div> -->
<!-- 															</div> -->
<!-- 														</div> -->
<!-- 													</td> -->
<!-- 												</tr> -->
												<tr>
													<th style="width:15% !important; font-weight:bold !important;">기간계 조직연결</th>
													<td colspan="3">
														<input type="button" value="기간계 조직연결" id="btnSearchdept" width="100%" height="100%" style="margin:3px 5px !important;" onclick="popMappingdept();" />
														<span id="labeldept"></span>
														<input type="hidden" id="txtDeptids" name="deptids" />
													</td>
												</tr>
												<tr>
													<th style="width:15% !important; font-weight:bold !important;">정보공시 학과연결</th>
													<td colspan="3">
														<input type="button" value="정보공시 학과연결" id="btnSearchscid" width="100%" height="100%" style="margin:3px 5px !important;" onclick="popMappingSust();" />
														<span id="labelsustid"></span>
														<input type="hidden" id="txtSustids" name="sustids" />
													</td>
												</tr>
											</table>
										</div>
										<div class="f_right m_t5">
											<div class="button type2 f_left">
												<input type="button" value="목록" id='btnList' width="100%" height="100%" onclick="reset();selectDisplay('list', 'nopix');" />
											</div>
											<div class="button type2 f_left">
												<input type="button" value="저장" id='btnSave' width="100%" height="100%" onclick="insert('list');" />
											</div>
											<div class="button type2 f_left">
												<input type="button" value="저장후신규" id='btnSavenew' width="100%" height="100%" onclick="insert('save');" />
											</div>
											<div class="button type3 f_left" style="margin-right:5px;">
												<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();" />
											</div>
										</div>
									</form>
									<iframe name="saveframe" src="./crud" style="border:0px;width:1px;height:1px;"></iframe>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

