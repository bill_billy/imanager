<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>핵심성공요인관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$("#btnSearch").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnNew").jqxButton({ width: '',  theme:'blueish'});
				$("#btnSave").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnDelete").jqxButton({ width: '',  theme:'blueish'});              
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		var dat = $i.sqlhouse(13,{COML_COD:"YEARS"});
        		dat.done(function(res){
        			var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                        { name: 'COM_COD' },
	                        { name: 'COM_NM' }
	                    ],
	                    id: 'id',
						localdata:res,
						// url: url,
	                    async: false
	                };
	                var dataAdapter = new $.jqx.dataAdapter(source);
	                $("#cboSearchyyyy").jqxComboBox({selectedIndex: 0, source: dataAdapter,animationType: 'fade',dropDownHorizontalAlignment: 'right',displayMember: "COM_NM",valueMember: "COM_COD",dropDownWidth: 80,width: 80,height: 23,theme:'blueish'});
	                var dat = $i.sqlhouse(13,{COML_COD:"USE"});
	        		dat.done(function(res){
	        			var source =
		                {
		                    datatype: "json",
		                    datafields: [
		                        { name: 'COM_COD' },
		                        { name: 'COM_NM' }
		                    ],
		                    id: 'id',
							localdata:res,
							// url: url,
		                    async: false
		                };
		                var dataAdapter = new $.jqx.dataAdapter(source); 
		                $("#cboUseyn").jqxComboBox({selectedIndex: 0, source: dataAdapter,animationType: 'fade',dropDownHorizontalAlignment: 'right',displayMember: "COM_NM",valueMember: "COM_COD",dropDownWidth: 150,dropDownHeight: 60,width: 150,height: 23,theme:'blueish'});
	        		});
	                search();   
	                getRowDetail('','','');   
	                csflist('','','','');         
        		}); 
        	}
        	//조회
        	function search(){
        		var dat = $i.select("#searchForm");      
        		dat.done(function(res){
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'ACCT_ID', type: 'string' },
		                    { name: 'STRA_ID', type: 'string' },
							{ name: 'STRA_NM', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var detailRow = function (row, columnfield, value, a, b, rowData) {//그리드 선택시 하단 상세 
						var link = "<a href=\"javascript:getRowDetail('"+rowData.STRA_ID+"','"+rowData.STRA_NM+"','"+rowData.ACCT_ID+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridStralist").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                   { text: '전략ID', datafield: 'STRA_ID', width: '25%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '전략명', datafield: 'STRA_NM', width: '75%', align:'center', cellsalign: 'center', cellsrenderer: detailRow}
		                ]
		            });
        		}); 
        	}
        	//입력
        	function insert(){
        		if($("#hiddenStraID").val().trim()==""){
        			$i.dialog.warning("SYSTEM","전략명을 선택하세요.");
        			return;
        		}
        		if($("#txtSubstraID").val().trim()==""){
        			$i.dialog.warning("SYSTEM","세부전략명을 선택하세요.");
        			return;
        		}
        		if($("#txtCsfcd").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","핵심성공요인코드를 입력하세요");
        			return;
        		}
        		if($("#txtCsfnm").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","핵심성공요인명을 입력하세요");
        			return;
        		}   
        		if($("#txtStartyyyy").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","시작년도를 입력하세요");
        			return;
        		}
        		if($("#txtEndyyyy").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","종료년도를 입력하세요");
        			return;
        		}
        		var check = $i.sqlhouse("236",{S_ACCT_ID:$("#txtAcctid").val(), S_STRA_ID:$("#hiddenStraID").val(),S_SUBSTRA_ID:$("#txtSubstraID").val(),S_CSF_ID:$("#txtCsfcd").val()});
        		check.done(function(res){   
       				if($("#txtCsfcd").val() != $("#hiddenCsfID").val() || $("#hiddenCsfID").val() == ""){
       						$i.insert("#saveForm").done(function(args){
				        			if(args.returnCode == "EXCEPTION"){
				        				$i.dialog.error("SYSTEM",args.returnMessage);
				        			}else{ 
				        				$i.dialog.alert("SYSTEM","저장 되었습니다.");
				        				search();    
				    	                csflist($("#txtAcctid").val(),$("#hiddenStraID").val(),$("#txtSubstraID").val(),$("#substranm").val());    
				        				resetForm();
				        			}
				        	}).fail(function(e){ 
				        			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
				        	});
      						
       				}else{
       					$i.insert("#saveForm").done(function(args){
		        			if(args.returnCode == "EXCEPTION"){
		        				$i.dialog.error("SYSTEM",args.returnMessage);
		        			}else{
		        				$i.dialog.alert("SYSTEM","저장 되었습니다.");
		        				search();     
		        				csflist($("#txtAcctid").val(),$("#hiddenStraID").val(),$("#txtSubstraID").val(),$("#substranm").val());    
		        				resetForm();
		        			}              
		        		}).fail(function(e){ 
		        			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
		        		});
       				}
        		});
        	}
        	//삭제
        	function remove(){       
        		if($("#hiddenStraID").val().trim()==""){
        			$i.dialog.warning("SYSTEM","전략목표를 선택하세요.");
        			return;
        		}
        		if($("#txtSubstraID").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","삭제 할 세부전략목표를 선택하세요");
        			return;
        		}
        		if($("#txtCsfcd").val().trim() == ""){
        			$i.dialog.warning("SYSTEM","삭제 할 핵심성공요인명를 선택하세요");
        			return;          
        		}
        		if($i.dialog.confirm("SYSTEM","삭제 하시겠습니까?", function(){
        			$i.remove("#saveForm").done(function(args){
			        			if(args.returnCode == "EXCEPTION"){
			        				$i.dialog.error("SYSTEM",args.returnMessage);
			        			}else{
			        				$i.dialog.alert("SYSTEM","삭제 되었습니다.");
			        				search(); 
			        				csflist($("#txtAcctid").val(),$("#hiddenStraID").val(),$("#txtSubstraID").val(),$("#substranm").val());       
			        				resetForm();
			        			}
			        		}).fail(function(e){
			        			$i.dialog.error("SYSTEM","삭제 중 오류가 발생했습니다.");             
			        });
        		}));       
        	}
        	
        	//각종 이벤트 바인딩.
        	function setEvent(){
        		$("#btnSave").on("click", function(event){
        			$("#btnSave").toggle(); 
        		}); 
        		$("#btnDelete").on("click", function(event){
        			$("#btnDelete").toggle();      
        		}); 
        		
        	}
        	   
        	//테스트 함수.
        	function test(){ 
        		var async = $i.sqlhouse(4007,{S_ID:"C"},function(res){});
        		async.done(function(res){ 
        			console.log(res);
        		}).fail(function(error){
        			console.log(error); 
        		});
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	
        	//gridCallist 로우 상세
        	function getRowDetail(straId, straNm, acctId){
        		$("#txtAcctid").val(acctId);
        		$("#straIDLabel").html(straId);
	        	$("#stranmLabel").html(straNm);
	        	$("#hiddenStraID").val(straId);
        		var dat = $i.sqlhouse(239,{ACCT_ID:acctId, STRA_ID:straId, SEARCH_YYYY:$("#cboSearchyyyy").val()});
        		dat.done(function(res){
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'ACCT_ID', type: 'string'},
		                    { name: 'STRA_ID', type: 'string'},
		                    { name: 'SUBSTRA_ID', type: 'string'},
		                    { name: 'SUBSTRA_NM', type: 'string'},
							{ name: 'SORT_ORDER', type: 'string'},
							{ name: 'START_YYYY', type: 'string'},
							{ name: 'END_YYYY', type: 'string'},
							{ name: 'USE_NM', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
  					var detailRow = function (row, columnfield, value) {//그리드 선택시 하단 상세
						var straID = $("#gridSubstrlist").jqxGrid("getrowdata", row).STRA_ID;
						var subStraID = $("#gridSubstrlist").jqxGrid("getrowdata", row).SUBSTRA_ID;
						var substranm = $("#gridSubstrlist").jqxGrid("getrowdata", row).SUBSTRA_NM;
						var acctid = $("#gridSubstrlist").jqxGrid("getrowdata", row).ACCT_ID;
						          
						var link = "<a href=\"javascript:csflist('"+acctid+"','"+straID+"','"+subStraID+"','"+substranm+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
						
					};  
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridSubstrlist").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                   { text: '세부전략코드', datafield: 'SUBSTRA_ID', width: 116, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '세부전략명', datafield: 'SUBSTRA_NM', width: 520, align:'center', cellsalign: 'center', cellsrenderer: detailRow},  
						   { text: '정렬순서', datafield: 'SORT_ORDER', width: 116, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
						   { text: '시작년도', datafield: 'START_YYYY', width: 116, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
						   { text: '종료년도', datafield: 'END_YYYY', width: 116, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
						   { text: '사용여부', datafield: 'USE_NM', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
						   
		                ]    
		            });
        		});
        	}
         	function getCsflistGridDetail(rowdata){
         		
        		$("#txtAcctid").val(rowdata.ACCT_ID); 
        		$("#hiddenCsfID").val(rowdata.CSF_ID); 
	        	$("#txtCsfcd").val(rowdata.CSF_CD);
	        	$("#txtCsfnm").val(rowdata.CSF_NM);
	        	$("#txtSubstraID").val(rowdata.SUBSTRA_ID);      
	        	$("#txtSortorder").val(rowdata.SORT_ORDER);
	        	$("#txtStartyyyy").val(rowdata.START_YYYY);
	        	$("#txtEndyyyy").val(rowdata.END_YYYY);
	        	if(rowdata.USE_YN != ''){   
	        		$("#cboUseyn").val(rowdata.USE_YN);
	        	}
        	} 
        	//신규 버튼 클릭 또는 초기화 해야 할 경우
        	function resetForm(){
        		$("#hiddenCsfID").val('');
				$("#txtCsfcd").val('');
				$("#txtCsfnm").val('');
				$("#txtSortorder").val('');
				$("#txtStartyyyy").val('');
				$("#txtEndyyyy").val('');
       			$("#cboUseyn").val('Y');
        	}
           	function csflist(acctId, straId,  subStraID, substranm){   
            		$("#txtAcctid").val(acctId);
            		$("#txtSubstraID").val(subStraID);
    	        	$("#hiddenStraID").val(straId);
    	        	$("#substranmLabel").html(substranm);      
        		var dat = $i.sqlhouse(232,{S_ACCT_ID:acctId, S_STRA_ID:straId, S_SEARCH_YYYY:$("#cboSearchyyyy").val(),S_SUBSTRA_ID:subStraID});
        		dat.done(function(res){
		            var source =
		            {   
		                datatype: "json",
		                datafields: [
		                    { name: 'ACCT_ID', type: 'string'}, 
		                    { name: 'STRA_ID', type: 'string'},
		                    { name: 'SUBSTRA_ID', type: 'string'},   
		                    { name: 'CSF_ID', type: 'string'},
		                    { name: 'CSF_CD', type: 'string'},    
		                    { name: 'CSF_NM', type: 'string'},
							{ name: 'SORT_ORDER', type: 'string'},
							{ name: 'START_YYYY', type: 'string'},
							{ name: 'END_YYYY', type: 'string'},
							{ name: 'USE_YN', type: 'string'}   
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };    
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
  					 var detailRow = function (row, columnfield, value,defaultHtml,rowProperty,rowData) {//그리드 선택시 하단 상세  					 	
						var link = "<a href='javascript:getCsflistGridDetail("+JSON.stringify(rowData)+")' style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
						   
					};    
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridCsflist").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                		{ datafield: 'CSF_ID' , hidden:true},    
			                   	{ text: '핵심성공요인코드', datafield: 'CSF_CD', width: '18%', align:'center', cellsalign: 'center'},
		                        { text: '핵심성공요인명', datafield: 'CSF_NM', width: '32%', align:'center', cellsalign: 'left',  cellsrenderer: detailRow},
		                        { text: '정렬순서', datafield: 'SORT_ORDER', width: '10%', align:'center', cellsalign: 'right', cellsrenderer:alginRight },
		      				  	{ text: '시작년도', datafield: 'START_YYYY', width: '15%', align:'center', cellsalign: 'center' },
		      				  	{ text: '종료년도', datafield: 'END_YYYY', width: '15%', align:'center', cellsalign: 'center'},
		                        { text: '사용여부', datafield: 'USE_YN', width: '10%', align:'center', cellsalign: 'center' }
						   
		                ]
		            });
        		});
        	}
            //숫자만 입력 함수
            function showKeyCode(event) {
        		event = event || window.event;
        		var keyID = (event.which) ? event.which : event.keyCode;
        		if( ( keyID >=48 && keyID <= 57 ) || ( keyID >=96 && keyID <= 105 ) || keyID == 8 || keyID == 9 || keyID == 190 || keyID == 110 || keyID == 45 || keyID == 46 ){
        			return;
        		}else{
        			return false;
        		}
        	}
        </script>
        <style type="text/css">
			.edit_inputSearch {   
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
		</style>   
    </head>
    <body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 1%;">
			<form id="searchForm" name="searchForm">
				<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
					<div class="label type1 f_left">평가년도 : </div>
					<div class="combobox f_left" id="cboSearchyyyy" name="searchyyyy"></div>
					<div class="f_right pos_a" style="right:0px;">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
					</div>
				</div>   
			</form>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style="width:34%; margin-right:1%;">
					<div class="grid f_left" style="float:left;margin:10px auto !important; margin-bottom:10px !important;  width:100%; height:310px;">
						<div id="gridStralist"> </div>
					</div>
				</div> 
				<div class="content f_right" style="width:65%;">   
						<div class="group f_left  w100p">
							<input type="hidden" id="txtAcctid" />
							<div class="label type2 f_left p_r10">전략ID:<span class="label sublabel" style="font-weight:bold;" id="straIDLabel"></span></div>
							<div class="label type2 f_left">전략명:<span class="label sublabel" style="font-weight:bold;" id="stranmLabel"></span></div>
						</div>
						<div class="grid f_left" style="float:left;margin:0px auto !important; margin-bottom:10px !important;  width:100%; height:293px;">
							<div id="gridSubstrlist"> </div>
						</div>
				</div>
				<form id="saveForm" name="saveForm">
				<div class="content f_left" style="width:100%;">
					<div class="group f_left  w100p"style="margin:0 !important; width:100%; float:left; ">
						<div class="label type2 f_left p_r10">Best DIT &gt;<span class="label" style="font-weight:bold;" id="substranmLabel"></span></div>
					</div> 
					<div class="content f_left" style="float:left;margin:0px auto !important; margin-bottom:10px !important;  width:100%; height:151px;">
						<div id="gridCsflist"></div>           
					</div> 	
				</div>  	     
				<div class="table f_left" style="width:100%; height:100%;">        
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>      
							<th style="width:10% !important; font-weight:bold !important;">핵심성공요인코드<span class="th_must"></span></th>
							<th style="width:50% !important; font-weight:bold !important;">핵심성공요인명<span class="th_must"></span></th>
							<th style="width:10% !important; font-weight:bold !important;">정렬순서</th>
							<th style="width:10% !important; font-weight:bold !important;">시작년도<span class="th_must"></span></th>
							<th style="width:10% !important; font-weight:bold !important;">종료년도<span class="th_must"></span></th>
							<th style="width:10% !important; font-weight:bold !important;">사용여부</th>
						</tr>
						<tr>
							<td>
								<input type="hidden" id="hiddenStraID" name="straID" />   
								<input type="hidden" id="txtSubstraID" name="subStraID" />
								<input type="hidden" id="hiddenCsfID" name="csfID" />
								
								<input type="text" id="txtCsfcd" name="csfcd" class="input type2 f_left" style="width:81%;margin:3px 5px !important;" />
							</td>     
							<td>
								<input type="text" id="txtCsfnm" name="csfnm" class="input type2 f_left" style="width:95.9%;margin:3px 5px !important;" />
							</td>
							<td>
								<input type="text" id="txtSortorder" name="sortorder" class="input type2 f_left" style="width:81%;margin:3px 5px !important;" />
							</td>
							<td>
								<input type="text" id="txtStartyyyy" name="startyyyy" class="input type2 f_left" style="width:81%;margin:3px 5px !important;" maxlength="4" onkeydown="return showKeyCode(event)"/>
							</td>    
							<td>
								<input type="text" id="txtEndyyyy" name="endyyyy" class="input type2 f_left" style="width:81%;margin:3px 5px !important;" maxlength="4" onkeydown="return showKeyCode(event)"/>
							</td>   
							<td>
								<div class="combobox f_left" style="margin:3px 5px !important;" id="cboUseyn" name="useyn"></div>
							</td>
						</tr>
					</table>
					</div>
					<div class="f_right m_t5">
						<div class="button type2 f_left">
							<input type="button" value="신규" id='btnNew' width="100%" height="100%" onclick="resetForm();" />
						</div>
						<div class="button type2 f_left">
							<input type="button" value="저장" id='btnSave' width="100%" height="100%" onclick="insert();" />
						</div>
						<div class="button type3 f_left">
							<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();" />
						</div>    
					</div>
				</form>
			</div>
		</div>
	</body>
</html>

