<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>전략목표관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$("#btnSearch").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnNew").jqxButton({ width: '',  theme:'blueish'});
				$("#btnSave").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnDelete").jqxButton({ width: '',  theme:'blueish'});              
				$("#txtSearch").jqxInput({placeHolder: "", height: 23, width: 250, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return search(); } });
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		var dat = $i.sqlhouse(13,{COML_COD:"USE"});
        		dat.done(function(res){
        			var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                        { name: 'COM_COD' },
	                        { name: 'COM_NM' }
	                    ],
	                    id: 'id',
						localdata:res,
						// url: url,
	                    async: false
	                };
	                var dataAdapter = new $.jqx.dataAdapter(source);
	                $("#cboSearchyn").jqxComboBox({selectedIndex: 0, source: dataAdapter,animationType: 'fade',dropDownHorizontalAlignment: 'right',displayMember: "COM_NM",valueMember: "COM_COD",dropDownWidth: 80,dropDownHeight: 90,width: 80,height: 23,theme:'blueish'});
	                var dat = $i.sqlhouse(13,{COML_COD:"USE"});
	        		dat.done(function(res){
	        			var source =
		                {
		                    datatype: "json",
		                    datafields: [
		                        { name: 'COM_COD' },
		                        { name: 'COM_NM' }
		                    ],
		                    id: 'id',
							localdata:res,
							// url: url,
		                    async: false
		                };
		                var dataAdapter = new $.jqx.dataAdapter(source); 
		                $("#cboUseyn").jqxComboBox({selectedIndex: 0, source: dataAdapter,animationType: 'fade',dropDownHorizontalAlignment: 'right',displayMember: "COM_NM",valueMember: "COM_COD",dropDownWidth: 150,dropDownHeight: 90,width: 150,height: 23,theme:'blueish'});
	        		});
	                search();
	                getRowDetail('','','');
        		}); 
        	}
        	//조회
        	function search(){
        		var dat = $i.select("#searchForm");
        		dat.done(function(res){
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'ACCT_ID', type: 'string' },
		                    { name: 'STRA_ID', type: 'string' },
		                    { name: 'STRA_CD', type: 'string' },
							{ name: 'STRA_NM', type: 'string'},
							{ name: 'USE_COUNT', type: 'string'}
		                ],
		                localdata: res, 
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var detailRow = function (row, columnfield, value) {//그리드 선택시 하단 상세
						var straId = $("#gridStralist").jqxGrid("getrowdata", row).STRA_ID;
						var straNm = $("#gridStralist").jqxGrid("getrowdata", row).STRA_NM;
						var acctId = $("#gridStralist").jqxGrid("getrowdata", row).ACCT_ID;
						
						var link = "<a href=\"javascript:getRowDetail('"+straId+"','"+straNm+"','"+acctId+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
						
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridStralist").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [ 
		                   { datafield: 'STRA_ID', hidden:true},
		                   { text: '전략목표코드', datafield: 'STRA_CD', width: 101, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '전략목표명', datafield: 'STRA_NM', width: 366, align:'center', cellsalign: 'center', cellsrenderer: detailRow},
		                   { text: '세부전략', datafield: 'USE_COUNT', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
		                ]
		            });
        		});
        	}
        	//입력
        	function insert(){
        		if($("#txtStranm").val().trim()==""){
        			$i.dialog.warning("SYSTEM","필수 값을 확인하세요.");
        			return;
        		}
        		$i.insert("#saveForm").done(function(args){
        			if(args.returnCode == "EXCEPTION"){
        				$i.dialog.error("SYSTEM",args.returnMessage);
        			}else{
        				$i.dialog.alert("SYSTEM","저장 되었습니다.");
        				search();
        				resetForm();
        			}
        		}).fail(function(e){ 
        			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
        		});
        	}
        	//삭제
        	function remove(){
        		if(confirm("삭제 하시겠습니까?")){
        			if($("#txtStraid").val().trim()==""){
	        			$i.dialog.warning("SYSTEM","삭제 할 전략목표를 선택하세요.");
	        			return;
	        		}
        			var check = $i.post("./check",{straid:$("#txtStraid").val()},function(){});
        			check.done(function(res){
        				if(res.returnArray.length == 0){
        					$i.remove("#saveForm").done(function(args){
			        			if(args.returnCode == "EXCEPTION"){
			        				$i.dialog.error("SYSTEM",args.returnMessage);
			        			}else{
			        				$i.dialog.alert("SYSTEM","삭제 되었습니다.");
			        				search();
			        				resetForm();
			        			}
			        		}).fail(function(e){
			        			$i.dialog.error("SYSTEM","삭제 중 오류가 발생했습니다.");
			        		});
        				}else{
        					$i.dialog.warning("SYSTEM","사용 중인 전략목표입니다");
        				}
        			});
        		}
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        		$("#btnInsert").on("click", function(event){
        			$("#tblInsert").toggle(); 
        		}); 
        	}
        	
        	//테스트 함수.
        	function test(){ 
        		var async = $i.sqlhouse(4007,{S_ID:"C"},function(res){});
        		async.done(function(res){ 
        			console.log(res);
        		}).fail(function(error){
        			console.log(error); 
        		});
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	
        	//gridCallist 로우 상세
        	function getRowDetail(straId, straNm, acctId){
        		var dat = $i.sqlhouse(30,{ACCT_ID:acctId, STRA_ID:straId});
        		dat.done(function(res){
        			if(res.returnArray.length > 0){
        				$("#straCDLabel").html(res.returnArray[0].STRA_CD);
	        			$("#straNmLabel").html(res.returnArray[0].STRA_NM);
	        			$("#txtStraid").val(res.returnArray[0].STRA_ID);
	        			$("#txtStraCD").val(res.returnArray[0].STRA_CD);
	        			$("#txtStranm").val(res.returnArray[0].STRA_NM);
	        			$("#txtareaStradef").val(res.returnArray[0].STRA_DEF);
	        			$("#txtSortorder").val(res.returnArray[0].SORT_ORDER);
	        			if(res.returnArray[0].USE_YN != ''){
	        				$("#cboUseyn").val(res.returnArray[0].USE_YN);
	        			}
        			}        				
        		});
        		var dat = $i.sqlhouse(31,{ACCT_ID:acctId, STRA_ID:straId});
        		dat.done(function(res){
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'SUBSTRA_ID', type: 'string' },
		                    { name: 'SUBSTRA_CD', type: 'string' },
		                    { name: 'SUBSTRA_NM', type: 'string' },
							{ name: 'USE_YN', type: 'string'},
							{ name: 'USE_NM', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridKpilist").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                   { text: '세부전략코드', datafield: 'SUBSTRA_CD', width: 130, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '세부전략명', datafield: 'SUBSTRA_NM', width: 816, align:'center', cellsalign: 'center', cellsrenderer: alginLeft},
						   { text: '사용여부', datafield: 'USE_NM', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
						   
		                ]
		            }); 
        		});
        	}
        	//신규 버튼 클릭 또는 초기화 해야 할 경우
        	function resetForm(){
        		$("#straCDLabel").html('');
        		$("#straNmLabel").html('');
				$("#txtStraid").val('');
				$("#txtStranm").val('');
				$("#txtStraCD").val('');
				$("#txtareaStradef").val('');
				$("#txtSortorder").val('');
       			$("#cboUseyn").val('Y'); 
        	}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
		</style>   
    </head>
    <body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 1%;">
			<form id="searchForm" name="searchForm">
				<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
					<div class="label type1 f_left">사용여부 : </div>
					<div class="combobox f_left" id="cboSearchyn" name="searchyn"></div>
					 
					<div class="label type1 f_left">전략목표명 : </div>
					<div class="input f_left">
						<input type="text" id="txtSearch" name="searchStranm" />
					</div>
					<div class="group f_right pos_a" style="right:0px;">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
						<div class="button type1 f_left">
							<input type="button" value="신규" id='btnNew' width="100%" height="100%" onclick="resetForm();" />
						</div>
					</div>
				</div>
			</form>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style="width:34%; margin-right:1%;">
					<div class="grid f_left" style="width:100%; height:600px;">	
						<div id="gridStralist"> </div>
					</div>
				</div> 
				<div class="content f_right" style="width:65%;">
					<form id="saveForm" name="saveForm">
						<div class="group f_left  w100p">
							<div class="label type2 f_left p_r10">전략목표코드:<span class="label sublabel" style="font-weight:bold;" id="straCDLabel"></span></div>
							<div class="label type2 f_left">전략목표명:<span class="label sublabel" style="font-weight:bold;" id="straNmLabel"></span></div>
						</div>
						<div class="table  f_left" style="width:100%; height:100%; margin:0;">
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<tr>
									<th style="width:15% !important; font-weight:bold !important;">전략목표코드<span class="th_must"></span></th>
									<td colspan="3">										
										<input type="text" id="txtStraCD" name="straCD" class="input type2 f_left" style="width:98%;margin:3px 5px !important;"/>
									</td>
								</tr>
								<tr>
									<th style="width:15% !important; font-weight:bold !important;">전략목표명<span class="th_must"></span></th>
									<td colspan="3">
										<input type="hidden" id="txtStraid" name="straid" />
										<input type="text" id="txtStranm" name="stranm" class="input type2 f_left" style="width:98%;margin:3px 5px !important;"/>
									</td>
								</tr>
								<tr>
									<th style="width:15% !important; font-weight:bold !important;">사용여부</th>
									<td>
										<div class="table_combo f_left" style="margin:3px 5px !important;" id="cboUseyn" name="useyn"></div>
									</td>
									<th style="width:15% !important; font-weight:bold !important;">정렬순서</th>
									<td><input type="text" id="txtSortorder" name="sortorder" class="input type2 f_left" style="width:95%;margin:3px 5px !important;" /></td>
								</tr>
								<tr>
									<th style="width:15% !important;">비고</th>
									<td colspan="3">
										<textarea class="textarea type2 f_left" style="width:98.5%;height:80px;margin:3px 5px !important;" id="txtareaStradef" name="stradef"></textarea>
									</td>
								</tr>
<!-- 								<tr> -->
<!-- 									<th style="width:15% !important;">선행전략</th> -->
<!-- 									<td colspan="3"> -->
<!-- 										<textarea class="textarea type2 f_left" style="width:99.5%;height:30px;" id="txtareaBigo" name="bigo"></textarea> -->
<!-- 									</td> -->
<!-- 								</tr> -->
<!-- 								<tr> -->
<!-- 									<th style="width:15% !important;">후행전략</th> -->
<!-- 									<td colspan="3"> -->
<!-- 										<textarea class="textarea type2 f_left" style="width:99.5%;height:30px;" id="txtareaBigo" name="bigo"></textarea> -->
<!-- 									</td> -->
<!-- 								</tr> -->
							</table>
						</div>
						<div class="f_right m_t5">
							<div class="button type2 f_left">
								<input type="button" value="저장" id='btnSave' width="100%" height="100%" onclick="insert();" />
							</div>
							<div class="button type3 f_left">
								<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();" />
							</div>
						</div>
						<div class="group f_left  w100p">
							<div class="label type2 f_left">적용지표</div>
						</div>
						<div class="grid f_left" style="width:100%; height:326px;">
							<div id="gridKpilist"> </div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>

