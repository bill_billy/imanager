<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>추진과제관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
          <script src="../../resources/cmresource/js/iplanbiz/secure/util.js"></script>
        <script>
        
        var gubn1,gubn2,gubn3;
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$("#btnSearch").jqxButton({ width: '',  theme:'blueish'});
				init();
//         		setEvent();
				
				$('#jqxTabs01').jqxTabs({ width: '100%', height: '100%', position: 'top', theme:'blueish', selectedItem: 0});
				$('#jqxTabs01').on('tabclick', function (event) {
					$('#saveForm1').jqxValidator('hide');
					 $('#saveForm2').jqxValidator('hide'); 
				});
				
				 var type="${type}";
				if(type=="2"){
					$('#jqxTabs01').jqxTabs({ selectedItem: 1});
				}
	    	
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		makeMissionCombo();
        		
        		search2("");
        		makecombobox("KPI_GBN","combobox1");
        		makecombobox("UNIT","combobox2");
        		makecombobox("KPI_TYPE","combobox3");
        		makecombobox("KPI_DIR","combobox4");
        		makecombobox("USE","combobox5");
        		makecombobox("UNIT","combobox7");
        		makecombobox("KPI_DIR","combobox6");
        		makecombobox("USE","combobox8");
        	}
        	function getRowDetail(row){
        		
           		var data = $("#gridlist").jqxGrid("getrowdata",row);
           		var msid, kpiid, kpinm, kpigbn, unit, kpitype, kpidir, kpidesc, useyn;
           		if(data){
           			ac = "";
           			
           			kpiid=data.KPI_ID;
           			kpinm=data.KPI_NM;
           			combobox1=data.KPI_GBN;
           			combobox2=data.UNIT;
           			combobox3=data.KPI_TYPE;
           			combobox4=data.KPI_DIR;
           			combobox5=data.USE_YN;
           			kpidesc=data.KPI_DESC;
           		}
           		else{
                 
           			kpiid="";
           			kpinm="";
           			kpigbn="";
           			unit="";
           			kpitype="";
           			kpidir="";
           			useyn="";
           			kpidesc="";
        		}
           	
    			
    			$("#kpiid").val(kpiid);
    			$("#kpinm").val(kpinm);
    			$("#kpidesc").val(kpidesc);
    			$("#combobox1").val(combobox1);
    			$("#combobox2").val(combobox2);
    			$("#combobox3").val(combobox3);
    			$("#combobox4").val(combobox4);
    			$("#combobox5").val(combobox5);
    		
    			
        	}
			function getRowDetail1(row){
				
				var data = $("#gridlist2").jqxGrid("getrowdata",row);
           		var msid, kpiid,subkpiid, subkpinm, unit, weight, kpidir, sortorder, useyn;
           		if(data){
           			ac = "";
           			
           			subkpiid=data.SUB_KPI_ID;
           			subkpinm=data.SUB_KPI_NM;
           			weight=data.WEIGHT;
           			sortorder=data.SORT_ORDER;
           			oldvalue=data.WEIGHT;
           			unit=data.UNIT;
           			kpidir=data.KPI_DIR;
           			useyn=data.USE_YN;
           			
           			
           			
           		}
           		else{
           			
           			subkpinm="";
           			weight="";
           			oldvalue="";
           			sortorder="";
           			kpiid="";
           			unit="";
           			kpidir="";
           			useyn="";
           			
        		}
           		
    			
    			
    			$("#subkpiid").val(subkpiid);
    			$("#subkpinm").val(subkpinm);
    			$("#weight").val(weight);
    			$("#oldvalue").val(weight);
    			$("#sortorder").val(sortorder);
    			$("#combobox6").val(kpidir);
    			$("#combobox7").val(unit);
    			$("#combobox8").val(useyn);
    			
    			
    			
    			
    			
        	}
			function reset(){
				$("#kpinm").val("");
    			$("#kpidesc").val("");
    			$("#kpiid").val("");
    			/* $("#combobox1").jqxComboBox('selectIndex', 0 ); 
    			$("#combobox2").jqxComboBox('selectIndex', 0 ); 
    			$("#combobox3").jqxComboBox('selectIndex', 0 ); 
    			$("#combobox4").jqxComboBox('selectIndex', 0 ); 
    			$("#combobox5").jqxComboBox('selectIndex', 0 );  */
    			$("#gridlist").jqxGrid('clearselection');
    			$("#gridlist1").jqxGrid('clearselection');
    			reset1();
    			search2('','','','');
				
			}
			function reset1(){
				
    			$("#subkpiid").val("");
    			$("#subkpinm").val("");
    			$("#weight").val("");
    			$("#oldvalue").val("");
    			$("#sortorder").val("");
    			/* $("#combobox6").jqxComboBox('selectIndex', 0 ); 
    			$("#combobox7").jqxComboBox('selectIndex', 0 ); 
    			$("#combobox8").jqxComboBox('selectIndex', 0 );  */
    			$("#gridlist2").jqxGrid('clearselection');
			}
			function save(){

        		if($("#kpinm").val().trim()==""){
        			$i.dialog.warning("SYSTEM","성과지표명 입력하십시오.");
        			return;
        		}
        		
        		
        		if($("#kpiid").val() == ""){
        				
        				var datkpiid = $i.sqlhouse(465,{});
        				datkpiid.done(function(res){
                			 var source =
        	                {
        	                    datatype: "json",
        	                    datafields: [
        	                        { name: 'MAXVAL' }
        	                       
        	                    ],
        	                    id: 'id',
        						localdata:res,
        						// url: url,
        	                    async: false
        	                };
        	                var dataAdapter = new $.jqx.dataAdapter(source);
        	             $("#newkpiid").val(source.localdata.returnObject.MAXVAL);
        				$i.insert("#saveForm1").done(function(args){
			        			if(args.returnCode == "EXCEPTION"){
			        				$i.dialog.error(args.returnMessage);
			        			}else{
			        				$i.dialog.alert("SYSTEM","저장 되었습니다.");
			        				search($("#getMission").val());
			        				search1($("#getMission").val());
			        			
			        				
			        				
			        				
			        			}
			        		}).fail(function(e){ 
			        			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
			        		});
        				});
        				
        			}else{
        				$i.insert("#saveForm1").done(function(args){
		        			if(args.returnCode == "EXCEPTION"){
		        				$i.dialog.error("SYSTEM",args.returnMessage);
		        			}else{
		        				$i.dialog.alert("SYSTEM","저장 되었습니다.");
		        				search($("#getMission").val());
		        				search1($("#getMission").val());
		        				
		        				
		        			}
		        		}).fail(function(e){ 
		        			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
		        		});
        			}
        		//reset();
        			
			}
			function save1(){
				if($("#msid2").val()=="" && $("#kpiid2").val()==""){
        			$i.dialog.warning("SYSTEM","성과지표를 선택하세요.");
        			return;
        		}
        		
				if($("#subkpinm").val().trim()==""){
        			$i.dialog.warning("SYSTEM","세부성과지표명을 입력하세요.");
        			return;
        		}
        		if($("#weight").val().trim()==""){
        			$i.dialog.warning("SYSTEM","가중치를 입력하세요.");
        			return;
        		}
        		if(isNaN($("#weight").val().trim())){
        			$i.dialog.warning("SYSTEM","가중치는 숫자로 입력해주세요.");
        			return;
        		}
        		if($("#weight").val()*1<=0){
        			$i.dialog.warning("SYSTEM","가중치는 0보다 큰 숫자 값으로 입력해주세요.");
        			return;
        		}
        		if(isNaN($("#sortorder").val().trim())){
        			$i.dialog.warning("SYSTEM","정렬순서는 숫자로 입력해주세요.");
        			return;
        		}
        		var sum1=$("#calc").val()*1;
        		var sum2=$("#weight").val()*1;
        		var sum3=$("#oldvalue").val()*1;
        		var sum4=sum1+sum2-sum3;
        		if(sum4>100){
        			$i.dialog.warning("SYSTEM","가중치의 합은 100이 되어야합니다.");
        			return;
        		} 
        		if($("#subkpiid").val() == ""){
        				
        				var datkpiid = $i.sqlhouse(466,{});
        				datkpiid.done(function(res){
                			 var source =
        	                {
        	                    datatype: "json",
        	                    datafields: [
        	                        { name: 'MAXVAL' }
        	                       
        	                    ],
        	                    id: 'id',
        						localdata:res,
        						// url: url,
        	                    async: false
        	                };
        	                var dataAdapter = new $.jqx.dataAdapter(source);
        	             $("#newsubkpiid").val(source.localdata.returnObject.MAXVAL);
        				$i.insert("#saveForm2").done(function(args){
			        			if(args.returnCode == "EXCEPTION"){
			        				$i.dialog.error(args.returnMessage);
			        			}else{
			        				$i.dialog.alert("SYSTEM","저장 되었습니다.");
			        				search2($("#msid2").val(),$("#kpiid2").val(),$("#calc").val(),$("#kpinm2").val());
			        				search1($("#getMission").val());
			        				$("#oldvalue").val("0");
			        				reset1();
			        			}
			        		}).fail(function(e){ 
			        			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
			        		});
        				});
        				
        			}else{
        				$i.insert("#saveForm2").done(function(args){
		        			if(args.returnCode == "EXCEPTION"){
		        				$i.dialog.error("SYSTEM",args.returnMessage);
		        			}else{
		        				$i.dialog.alert("SYSTEM","저장 되었습니다.");
		        				search2($("#msid2").val(),$("#kpiid2").val(),$("#calc").val(),$("#kpinm2").val());
		        				search1($("#getMission").val());
		        				$("#oldvalue").val("0");
		        				reset1();
		        			}
		        		}).fail(function(e){ 
		        			$i.dialog.error("SYSTEM","등록 중 오류가 발생 했습니다.");
		        		});
        			}
        		
        		//reset1();
			}
			function remove(){
				if($("#kpiid").val()=="" ){
        			$i.dialog.warning("SYSTEM","삭제 할 성과지표를 선택하세요.");
        			return;
        		}
				
				var msId = $("#getMission").val();
				var kpiId = $("#kpiid").val();
				
        		$i.dialog.confirm("SYSTEM","삭제하시겠습니까?",function(){
        			
        			var check = $i.post("./check",{msid:msId, kpiid:kpiId},function(){});
        			check.done(function(res){
        				if(res.returnArray.length == 0){  
        					var a = $i.post("./remove","#saveForm1");
    	      					a.done(function(args){
			        			if(args.returnCode == "EXCEPTION"){
			        				$i.dialog.error("SYSTEM",args.returnMessage);
			        			}else{
			        				$i.dialog.alert("SYSTEM","삭제 되었습니다.");
			        				search($("#getMission").val());
			        				search1($("#getMission").val());
			        			}
			        		}).fail(function(e){
			        			$i.dialog.error("SYSTEM","삭제 중 오류가 발생했습니다.");
			        		});
        				}else{ 
        					$i.dialog.warning("SYSTEM","사용 중인 성과지표입니다"); 
        				}
        			}); 
        		});
        		
			}
			function remove1(){
				if( $("#subkpiid").val()==""){
        			$i.dialog.warning("SYSTEM","삭제 할 세부성과지표를 선택하세요.");
        			return;
        		}
        		$i.dialog.confirm("SYSTEM","삭제하시겠습니까?",function(){
        			$i.remove("#saveForm2").done(function(args){
			        			if(args.returnCode == "EXCEPTION"){
			        				$i.dialog.error("SYSTEM",args.returnMessage);
			        			}else{
			        				$i.dialog.alert("SYSTEM","삭제 되었습니다.");
			        				search($("#getMission").val());
			        				search1($("#getMission").val());
			        				
			        				
			        			}
			        		}).fail(function(e){
			        			$i.dialog.error("SYSTEM","삭제 중 오류가 발생했습니다.");
			        		});
        		});
        		
        		
			}
        	function makecombobox(code,combo){
        		var dat = $i.sqlhouse(458,{COML_COD:code});
        		dat.done(function(res){
        			missionList = res.returnArray;
					missionList.forEach(function(dataOne) {
	    				dataOne.COM_NM = $i.secure.scriptToText(dataOne.COM_NM);
	    				dataOne.COM_COD = $i.secure.scriptToText(dataOne.COM_COD);
	    			});
        			var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                        { name: 'COM_COD' },
	                        { name: 'COM_NM' }
	                    ],
	                    id: 'id',
						localdata:res,
						// url: url,
	                    async: false
	                };
	                var dataAdapter = new $.jqx.dataAdapter(source);
	                $("#"+combo).jqxComboBox({selectedIndex: 0, source: dataAdapter,animationType: 'fade',dropDownHorizontalAlignment: 'right',displayMember: "COM_NM",valueMember: "COM_COD",dropDownWidth: 100,dropDownHeight: 60,width: 100,height: 23,theme:'blueish'});
	              
        		});
        	}
        	var missionList = [];
	   		var missionDat;
				function makeMissionCombo(){
					missionDat = $i.sqlhouse(322,{}); 
						missionDat.done(function(res){
							missionList = res.returnArray;
							missionList.forEach(function(dataOne) {
			    				dataOne.MS_NM = $i.secure.scriptToText(dataOne.MS_NM);
			    			});
		    				var source = 
		    				{ 
			        			datatype:"json",
			        			datafields:[
			        				{ name : "MS_ID" },
			        				{ name : "MS_NM" } 
			        			],
			        			id:"id",
			        			localdata:res,
			        			async : false 
			        		};
			        		var dataAdapter = new $.jqx.dataAdapter(source);
			        		 $("#getMission").jqxComboBox({ 
			        				selectedIndex: 0, 
			        				source: dataAdapter, 
			        				animationType: 'fade', 
			        				dropDownHorizontalAlignment: 'right', 
			        				displayMember: "MS_NM", 
			        				valueMember: "MS_ID",  
			        				dropDownWidth: 300, 
			        				dropDownHeight: 100, 
			        				width: 300, 
			        				height: 22,  
			        				theme:'blueish'} );
			        		 
			        		 
			        		 
		    			$("#getMission").jqxComboBox('selectIndex', 0 ); 
		    			search($("#getMission").val());
		    			search1($("#getMission").val());
		    			$("#msid").val($("#getMission").val());
		    			
		    		});  
		 		}//makeMissionCombo 
        	//조회
        	
		    	
        	function search(msid){
		 			if(msid){
		 				var msid2="AND MS_ID='"+msid+"' ";
		 			}
		 			else
		 				var msid2="";
        		var dat = $i.sqlhouse(455,{MS_ID:msid2});
        		dat.done(function(res){
		            // prepare the data
		          	/*missionList = res.returnArray;
					missionList.forEach(function(dataOne) {
	    				dataOne.KPI_NM = $i.secure.scriptToText(dataOne.KPI_NM);
	    				dataOne.KPI_DESC = $i.secure.scriptToText(dataOne.KPI_DESC);
	    			});*/
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                	    { name: 'MS_ID', type: 'number' },
			                    { name: 'KPI_ID', type: 'number' },
								{ name: 'KPI_NM', type: 'string'},
								{ name: 'KPI_GBN', type: 'string'},
								{ name: 'UNIT', type: 'string'},
								{ name: 'KPI_TYPE', type: 'string'},
								{ name: 'KPI_DIR', type: 'string'},
								{ name: 'USE_YN', type: 'string'},
								{ name: 'KPI_DESC', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
		            	newValue = $i.secure.scriptToText(value);
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
		            	newValue = $i.secure.scriptToText(value);
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + newValue + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						newValue = $i.secure.scriptToText(value);
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + newValue + '</div>';
					};
					var detailRow = function (row, columnfield, value) {//그리드 선택시 하단 상세
						newValue = $i.secure.scriptToText(value);
						var link = "<a href=\"javascript:getRowDetail('"+row+"')\" style='color:black; text-decoration:underline;' >" + newValue + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
						
					};
		            
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridlist").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
						sortable:true,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                	{ text: '지표구분', datafield: 'KPI_GBN', width: '25%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
			                   { text: '성과지표', datafield: 'KPI_NM', width: '50%', align:'center', cellsalign: 'center', cellsrenderer: detailRow},   
							   { text: '단위', datafield: 'UNIT', width: '25%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
							 
		                ]
		            });
        		});
        		
        		reset();
        		search1($("#getMission").val());
        		$("#msid").val($("#getMission").val());
        		
        	}
        	function search1(msid){
        		if(msid){
	 				var msid2="AND MS_ID='"+msid+"' ";
	 			}
	 			else
	 				var msid2="";
    		
        		var dat = $i.sqlhouse(456,{MS_ID:msid2, KPI_TYPE:'A'});
        		dat.done(function(res){
        			 missionList = res.returnArray;
					missionList.forEach(function(dataOne) {
	    				dataOne.KPI_NM = $i.secure.scriptToText(dataOne.KPI_NM);
	    				dataOne.KPI_DESC = $i.secure.scriptToText(dataOne.KPI_DESC);
	    			}); 
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                	    { name: 'MS_ID', type: 'number' },
			                    { name: 'KPI_ID', type: 'number' },
								{ name: 'KPI_NM', type: 'string'},
								{ name: 'KPI_GBN', type: 'string'},
								{ name: 'UNIT', type: 'string'},
								{ name: 'KPI_TYPE', type: 'string'},
								{ name: 'KPI_DIR', type: 'string'},
								{ name: 'USE_YN', type: 'string'},
								{ name: 'KPI_DESC', type: 'string'},
								{ name: 'COUNT', type: 'number'},
								{ name: 'CALC', type: 'number'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var weightCell = function (row, columnfield, value) {//left정렬
						var newValue = value;
						var count = $("#gridlist1").jqxGrid("getrowdata", row).COUNT;
						if(count>0 && value<100){
							newValue = '<a style="color:red;">'+value+'</a>';
						}
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + newValue + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						newValue = $i.secure.scriptToText(value);
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + newValue + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						newValue = $i.secure.scriptToText(value);
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + newValue + '</div>';
					};
					var detailRow = function (row, columnfield, value) {//그리드 선택시 하단 상세
						newValue = $i.secure.scriptToText(value);
						var kpiid = $("#gridlist1").jqxGrid("getrowdata", row).KPI_ID;
						var kpinm = $("#gridlist1").jqxGrid("getrowdata", row).KPI_NM;
						if(kpinm.indexOf("&#39;")!=-1){
							
							kpinm=kpinm.replace("&#39;","&#39;,&#39;");
							//alert(kpinm);
						}
						
						var msid = $("#gridlist1").jqxGrid("getrowdata", row).MS_ID;
						var calc = $("#gridlist1").jqxGrid("getrowdata", row).CALC;
						var link = "<a href=\"javascript:search2('"+msid+"','"+kpiid+"','"+calc+"','"+kpinm+"')\" style='color:black; text-decoration:underline;' >" + newValue + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
						
					};
		            
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridlist1").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
						sortable:true,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                	{ text: '지표구분', datafield: 'KPI_GBN', width: '20%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
			                   { text: '성과지표명', datafield: 'KPI_NM', width: '40%', align:'center', cellsalign: 'center', cellsrenderer: detailRow},   
							   { text: '세부성과 지표수', datafield: 'COUNT', width: '20%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
							   { text: '가중치 합', datafield: 'CALC', width: '20%', align:'center', cellsalign: 'center', cellsrenderer: weightCell}
							 
		                ]
		            });
		        });
        		
        		
        		
        	
        		
        	}
        	function search0(){
        		search($("#getMission").val());
        		
        		$("#subtitle1").html(""+"&nbsp;&frasl;&nbsp;"+"");
        	}
        	function search2(msid,kpiid,calc,kpinm){
        		reset1();
        		if(kpiid && kpinm && msid){
        			$("#subtitle1").html(kpiid+"&nbsp;&frasl;&nbsp;"+$i.secure.scriptToText(kpinm));
        			$("#msid2").val(msid);
        			$("#kpiid2").val(kpiid);
        			$("#kpinm2").val(kpinm); 
        			$("#calc").val(calc);
        		} 
        		
        		var dat = $i.sqlhouse(457,{MS_ID:msid, KPI_ID:kpiid});
        		dat.done(function(res){
		            // prepare the data
		            /* missionList = res.returnArray;
						  missionList.forEach(function(dataOne) {
	    				dataOne.SUB_KPI_NM = $i.secure.scriptToText(dataOne.SUB_KPI_NM);
	    		
	    			});   */
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                	    { name: 'MS_ID', type: 'number' },
		                	    { name: 'KPI_ID', type: 'number' },
		                	    { name: 'SUB_KPI_ID', type: 'number' },
								{ name: 'SUB_KPI_NM', type: 'string'},
								{ name: 'UNIT', type: 'string'},
								{ name: 'KPI_DIR', type: 'string'},
								{ name: 'WEIGHT', type: 'number'},
								{ name: 'USE_YN', type: 'string'},
								{ name: 'SORT_ORDER', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						var newValue = $i.secure.scriptToText(value);
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + newValue + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						var newValue = $i.secure.scriptToText(value);
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + newValue + '</div>';
					};
					var detailRow = function (row, columnfield, value) {//그리드 선택시 하단 상세
						var newValue = $i.secure.scriptToText(value);
						var link = "<a href=\"javascript:getRowDetail1('"+row+"')\" style='color:black; text-decoration:underline;' >" + newValue + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
						
					};
		            
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridlist2").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
						sortable:true,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                	   { text: '세부성과 지표명', datafield: 'SUB_KPI_NM', width: '40%', align:'center', cellsalign: 'center', cellsrenderer: detailRow},
			                   { text: '지표극성', datafield: 'KPI_DIR', width: '12%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},   
							   { text: '단위', datafield: 'UNIT', width: '12%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
							   { text: '가중치', datafield: 'WEIGHT', width: '12%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
							   { text: '사용여부', datafield: 'USE_YN', width: '12%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
							   { text: '정렬순서', datafield: 'SORT_ORDER', width: '12%', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
							 
		                ]
		            });
        		});
        		
        	
        	}
		 		
		 		
		    
        	
        </script>
        <style>
        	table {
        		table-layout: fixed; /*테이블 내에서 <td>의 넓이,높이를 고정한다.*/
        		border:1px;  
        		border-color:black;
        		border-style:solid;    
        		font-family:'돋움',Dotum,Helvetica,'Apple SD Gothic Neo',Sans-serif; 
        		
        	}
        	.sql{ 
        			width:100%;
        			overflow: hidden;
			    	text-overflow:ellipsis; /*overflow: hidden; 속성과 같이 써줘야 말줄임 기능이 적용된다.*/
			    	white-space:nowrap;
        	}
        	.input{
        	height:auto !important;
        	
        	}
        	.input{
        	height:auto !important;
        	
        	}
        </style>
    </head>
    <body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 1%;">
			<form id="searchForm">
				<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
					<div class="label type1 f_left">
						비젼:
					</div> 
					<div class="combobox f_left"  id='getMission' >  
					</div>
					<div class="group f_right pos_a" style="right:0px;">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search0()" />
						</div>
					</div>
				</div>
			</form>
			<div class="tabs f_left"
				style="width: 100%; height: 700px; margin-top: 0px;">
			<div id='jqxTabs01'>
					<ul>
						<li>성과지표</li>
						<li>세부성과지표</li>
					</ul>
					<div class="tabs_content"
								style="height: 100%; overflow-y: auto; overflow-x: hidden; padding: 20px 15px;">
									
										<div class="content f_left" style="width:100%; margin-right:1%;">
											<div class="grid f_left" style="width:47%; height:630px;">
												<div id="gridlist"></div>
											</div>
											<div class="grid f_right" style="width:50%; height:630px;">
											<div class="blueish table  f_left " style="width: 100%; margin: 10px 0;">
												<form id="saveForm1" name="saveForm" method="post">
												<input type="hidden" id="msid" name="msid"  value/>
												<input type="hidden" id="newkpiid" name="newkpiid"  value/>
												<input type="hidden" id="kpiid" name="kpiid"  value/>
												
												<table width="100%" cellspacing="0" cellpadding="0" border="0">
													<tr>
														<th>
															<div>성과지표명<span class="th_must"></span></div>
														</th>
														<td colspan="3">
															<div class="cell">
																<input type="text" id="kpinm" name="kpinm"
																	class="input type2  f_left"
																	style="width: 98%; margin: 3px;" />
															</div>
														</td>
													</tr>
													
													<tr>
														<th>
															<div>지표구분<span class="th_must"></span></div>
														</th>
														<td>
															<div class="cell">
																<div class="combobox"  id='combobox1' name="kpigbn" style="margin:3px 0;"></div>
															</div>
														</td>
														
														<th>
															<div>단위<span class="th_must"></span></div>
														</th>
														<td>
															<div class="cell">
																<div class="combobox"  id='combobox2' name="unit" style="margin:3px 0;"></div>
															</div>
														</td>
													</tr>
													
													<tr>
														<th>
															<div>지표속성<span class="th_must"></span></div>
														</th>
														<td>
															<div class="cell">
																<div class="combobox" id='combobox3' name="kpitype" style="margin:3px 0;"></div>
															</div>
														</td>
														
														<th>
															<div>지표극성<span class="th_must"></span></div>
														</th>
														<td>
															<div class="cell">
																<div class="combobox"  id='combobox4' name="kpidir" style="margin:3px 0;"></div>
															</div>
														</td>
													</tr>
													<tr>
														<th>
															<div>사용여부<span class="th_must"></span></div>
														</th>
														<td>
															<div class="cell">
																<div class="combobox"  id='combobox5' name="useyn" style="margin:3px 0;"></div>
															</div>
														</td>
														
														<th>
															<div></div>
														</th>
														<td>
															<div class="cell">
															
															</div>
														</td>
													</tr>
													<tr>
														<th>
															<div>기타사항</div>
														</th>
														<td colspan="3">
															<div class="cell">
																<textarea id="kpidesc"  class="textarea" value="" name="kpidesc"
																	style="width: 98%; height: 200px; margin: 3px;"></textarea>
															</div>
														</td>
													</tr>
													
													
												</table>
												</form>
											</div>
											<div class="group_button f_right">
												<div class="button type3 f_left "  id='hide-del'>
													<input type="button" value="삭제" id='jqxButtonDelete2'
														width="100%" height="100%" onclick="remove()"/>
												</div>
												
												<div class="button type2 f_left">
													<input type="button" value="신규" id='jqxButtonReset2'
														width="100%" height="100%" onclick="reset()" />
												</div>
												<div class="button type2 f_left">
													<input type="button" value="저장" id='jqxButtonSave2'
														width="100%" height="100%" onclick="save()" />
												</div>
											</div>
										</div>
										</div>
										
									
					</div><!-- tabs_content -->
					<div class="tabs_content"
								style="height: 100%; overflow-y: auto; overflow-x: hidden; padding: 20px 15px;">
									
										<div class="content f_left" style="width:100%; margin-right:1%;">
											<div class="grid f_left" style="width:47%; height:630px;">
												<div id="gridlist1"></div>
											</div>
											<div class="grid f_right" style="width:50%; height:500px;">
												<div class="label type2 f_left">
											        	지표ID&nbsp;⁄&nbsp;지표명:<span class="label sublabel" id="subtitle1">&nbsp;⁄&nbsp;</span>
												</div>
												<div id="gridlist2"></div>
						<div class="blueish table f_right"  style="width:100%; height:; margin:10px 0;">
							<form id="saveForm2" name="saveForm2" method="post">
								<input type="hidden" name="ac" id="ac" value="sub"/>
								<input type="hidden" name="msid" id="msid2" value=""/>
								<input type="hidden" name="kpiid" id="kpiid2" value=""/>
								<input type="hidden" name="kpinm" id="kpinm2" value/>
								<input type="hidden" name="subkpiid" id="subkpiid" value/>
								<input type="hidden" name="newsubkpiid" id="newsubkpiid" value/>
								<input type="hidden" id="calc" name="calc"  value/>
								<input type="hidden"  id="oldvalue" name="oldvalue"  value="0"/>
								<table summary="가산점" style="width:100%;">
								<thead style="width:100%;">
									<tr>
										<th  scope="col" style="width:25%;">세부성과지표명<span class="th_must"></span></th>
										<th  scope="col" style="width:15%;">지표극성<span class="th_must"></span></th>
										<th scope="col" style="width:15%;">단위<span class="th_must"></span></th>
										<th  scope="col" style="width:15%;">가중치<span class="th_must"></span></th>
										<th scope="col" style="width:15%;">사용여부<span class="th_must"></span></th>
										<th scope="col" style="width:15%;">정렬순서</th>
										
									</tr>
									</thead>
								<tbody>
										<tr>
										<td ><div class="cell t_center">
												<input type="text" value="" id="subkpinm" name="subkpinm" class="input type2  f_left"  style="width:96%; margin:3px 0; "/>
												
											</div></td>
										<td ><div class="cell t_center">
												<div class="combobox"  id='combobox6' name="kpidir" style="margin:3px 0;"></div>
												
											</div></td>
										<td ><div class="cell t_center">
												<div class="combobox"  id='combobox7' name="unit" style="margin:3px 0;"></div>
											</div></td>
										<td ><div class="cell t_center">
												<input type="text" value="" id="weight" name="weight" class="input type2  f_left t_center"  style="width:96%; margin:3px 0; "/>
											</div></td>
										<td ><div class="cell t_center">
												<div class="combobox"  id='combobox8' name="useyn" style="margin:3px 0;"></div>
											</div></td>
										<td ><div class="cell">
												<input type="text" value="" id="sortorder" name="sortorder" class="input type2  f_left t_center"  style="width:96%; margin:3px 0; "/>
											</div></td>
									</tr>
									</tbody>
							</table>
							</form>
		</div>
		
											<div class="group_button f_right">
												<div class="button type3 f_left "  id='hide-del'>
													<input type="button" value="삭제" id='jqxButtonDelete2'
														width="100%" height="100%" onclick="remove1()" />
												</div>
												
												<div class="button type2 f_left">
													<input type="button" value="신규" id='jqxButtonReset2'
														width="100%" height="100%" onclick="reset1()" />
												</div>
												<div class="button type2 f_left">
													<input type="button" value="저장" id='jqxButtonSave2'
														width="100%" height="100%" onclick="save1()" />
												</div>
											</div>
											</div>
										</div>
										
									
					</div><!-- tabs_content -->
			
			</div><!-- jqxTabs01 -->
			</div><!-- tabs f_left -->
		</div>
	</body>
</html>

