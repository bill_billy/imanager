<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>연계항목관리</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
        		$("#btnSearch").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnNew").jqxButton({ width: '',  theme:'blueish'});
				$("#btnSave").jqxButton({ width: '',  theme:'blueish'}); 
				$("#btnDelete").jqxButton({ width: '',  theme:'blueish'});              
				$("#txtSearch").jqxInput({placeHolder: "", height: 23, width: 250, minLength: 1, theme:'blueish' }).keydown(function(event){ if(event.keyCode == 13) { return search(); } });
// 				$("#cboUseyn").jqxComboBox({animationType: 'fade',dropDownHorizontalAlignment: 'right',width: '98%',height: 17,dropDownHeight: 80,disabled:false,theme:'blueish'});
        		init(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		var dat = $i.sqlhouse(13,{COML_COD:"USE"});
        		dat.done(function(res){
        			var source =
	                {
	                    datatype: "json",
	                    datafields: [
	                        { name: 'COM_COD' },
	                        { name: 'COM_NM' }
	                    ],
	                    id: 'id',
						localdata:res,
						// url: url,
	                    async: false
	                };
	                var dataAdapter = new $.jqx.dataAdapter(source);
	                $("#cboSearchyn").jqxComboBox({selectedIndex: 0, source: dataAdapter,animationType: 'fade',dropDownHorizontalAlignment: 'right',displayMember: "COM_NM",valueMember: "COM_COD",dropDownWidth: 80,width: 80,height: 23,theme:'blueish'});
	                var dat = $i.sqlhouse(13,{COML_COD:"IF_SYS"});
	        		dat.done(function(res){
	        			var source = 
	        			{
	        				datatype:"json",
	        				datafield:[
	        					{ name: 'COM_COD'},
	        					{ name: 'COM_NM'}
	        				],
	        				id:'id',
	        				localdata:res,
	        				async:false
	        			};
	        			var dataAdapter = new $.jqx.dataAdapter(source);
		                $("#cboSearchifsys").jqxComboBox({selectedIndex: 0, source: dataAdapter,animationType: 'fade',dropDownHorizontalAlignment: 'right',displayMember: "COM_NM",valueMember: "COM_COD",dropDownWidth: 150,width: 150,height: 23,theme:'blueish'});
		                search();
		                getRowDetail('','','');
	        		});
        		});
                var dat = $i.sqlhouse(13,{COML_COD:"IF_SYS"});
        		dat.done(function(dat){
        			var source = 
        			{
        				datatype:"json",
        				datafield:[
        					{ name: 'COM_COD'},
        					{ name: 'COM_NM'}
        				],
        				id:'id',
        				localdata:dat,
        				async:false
        			};
        			var dataAdapter = new $.jqx.dataAdapter(source);
	                $("#cboIfsys").jqxComboBox({selectedIndex: 0, source: dataAdapter,animationType: 'fade',dropDownHorizontalAlignment: 'right',displayMember: "COM_NM",valueMember: "COM_COD",dropDownWidth: 150,width: 150,height: 23,theme:'blueish'});
        		});
        		var dat = $i.sqlhouse(13,{COML_COD:"USEUNIT"});
        		dat.done(function(dat1){
        			var source = 
        			{
        				datatype:"json",
        				datafield:[
        					{ name: 'COM_COD'},
        					{ name: 'COM_NM'}
        				],
        				id:'id',
        				localdata:dat1,
        				async:false
        			};
        			var dataAdapter = new $.jqx.dataAdapter(source);
	                $("#cboIfunit").jqxComboBox({selectedIndex: 0, source: dataAdapter,animationType: 'fade',dropDownHorizontalAlignment: 'right',displayMember: "COM_NM",valueMember: "COM_COD",dropDownWidth: 150,width: 150,height: 23,theme:'blueish'});
        		});
        		var dat = $i.sqlhouse(13,{COML_COD:"USE"});
        		dat.done(function(dat1){
        			var source = 
        			{
        				datatype:"json",
        				datafield:[
        					{ name: 'COM_COD'},
        					{ name: 'COM_NM'}
        				],
        				id:'id',
        				localdata:dat1,
        				async:false
        			};
        			var dataAdapter = new $.jqx.dataAdapter(source);
	                $("#cboUseyn").jqxComboBox({selectedIndex: 0, source: dataAdapter,animationType: 'fade',dropDownHorizontalAlignment: 'right',displayMember: "COM_NM",valueMember: "COM_COD",dropDownWidth: 150,width: 150,height: 23,theme:'blueish'});
        		});
        	}
        	//조회
        	function search(){
        		var dat = $i.select("#searchForm");
        		dat.done(function(res){
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'ACCT_ID', type: 'string' },
		                    { name: 'IF_COD', type: 'string' },
							{ name: 'IF_COD_NM', type: 'string'},
							{ name: 'UNIT_NM', type: 'string'},
							{ name: 'CYCLE_NM', type: 'string'},
							{ name: 'BIGO', type: 'string'},
							{ name: 'SQL_DATA', type: 'string'},
							{ name: 'USE_YN', type: 'string'},
							{ name: 'USE_COUNT', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
					var detailRow = function (row, columnfield, value) {//그리드 선택시 하단 상세
						var ifCod = $("#gridIfcodlist").jqxGrid("getrowdata", row).IF_COD;
						var ifCodNm = $("#gridIfcodlist").jqxGrid("getrowdata", row).IF_COD_NM;
						var acctId = $("#gridIfcodlist").jqxGrid("getrowdata", row).ACCT_ID;
						
						var link = "<a href=\"javascript:getRowDetail('"+ifCod+"','"+ifCodNm+"','"+acctId+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
						
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridIfcodlist").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                   { text: 'ID', datafield: 'IF_COD', width: 91, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '연계항목명', datafield: 'IF_COD_NM', width: 440, align:'center', cellsalign: 'center', cellsrenderer: detailRow},
						   { text: '항목단위', datafield: 'UNIT_NM', width: 110, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
						   { text: '적용지표', datafield: 'USE_COUNT', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
		                ]
		            });
        		});
        	}
        	//입력
        	function insert(){
        		if($("#txtIfcodnm").val().trim()==""){
        			alert("필수 값을 확인하세요.");
        			return;
        		}
        		$i.insert("#saveForm").done(function(args){
        			if(args.returnCode == "EXCEPTION"){
        				alert("저장 중 오류가 발생했습니다.");
        			}else{
        				alert("저장 되었습니다.");
        				search();
        				resetForm();
        			}
        		}).fail(function(e){ 
        			alert("등록 중 오류가 발생 했습니다.");
        		});
        	}
        	//삭제
        	function remove(){    
        		if($("#txtIfcod").val().trim()==""){
        			alert("삭제 할 산식을 선택하세요.");
        			return;
        		}
        		if(confirm("연계항목을 삭제하시겠습니까?")){
        			var check = $i.post("./check",{ifcod:$("#txtIfcod").val()});
        			check.done(function(res){
        				if(res.returnArray.length == 0){
        					$i.remove("#saveForm").done(function(args){
			        			if(args.returnCode == "EXCEPTION"){
			        				alert("삭제 중 오류가 발생했습니다.");
			        			}else{
			        				alert("삭제 되었습니다.");
			        				search();
			        				resetForm();
			        			}
			        		}).fail(function(e){
			        			alert("삭제 중 오류가 발생했습니다.");
			        		});
        				}else{
        					alert("해당 연계항목이 사용중입니다");
        				}
        			});
        			
        		}
        		
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        		$("#btnInsert").on("click", function(event){
        			$("#tblInsert").toggle(); 
        		}); 
        	}
        	
        	//테스트 함수.
        	function test(){ 
        		var async = $i.sqlhouse(4007,{S_ID:"C"},function(res){});
        		async.done(function(res){ 
        			console.log(res);
        		}).fail(function(error){
        			console.log(error); 
        		});
        	}
        	
        	//여기 부터 추가 스크립트를 작성해 주세요.
        	
        	//gridCallist 로우 상세
        	function showSql(){
        		window.open('./popupMappingSql?acctid='+$("#txtPacctid").val()+'&ifcod='+$("#txtIfcod").val(), 'popupMappingSql','scrollbars=no, resizable=no, width=630, height=620');
        	}
        	function getRowDetail(ifCod, ifCodNm, acctId){
        		$("#txtPacctid").val(acctId);
        		var dat = $i.sqlhouse(22,{ACCT_ID:acctId, IF_COD:ifCod});
        		dat.done(function(res){
        			if(res.returnArray.length > 0){
        				$("#itemLabel").html(res.returnArray[0].IF_COD);
	        			$("#itemNmLabel").html(res.returnArray[0].IF_COD_NM);
	        			$("#txtIfcod").val(res.returnArray[0].IF_COD);
	        			$("#txtIfcodnm").val(res.returnArray[0].IF_COD_NM);
	        			$("#txtareaSqldata").val(res.returnArray[0].SQL_DATA);
	        			$("#txtareaBigo").val(res.returnArray[0].BIGO);
	        			if(res.returnArray[0].IF_UNIT != ''){
	        				$("#cboIfunit").val(res.returnArray[0].IF_UNIT);
	        			}
	        			$("#cboIfsys").val(res.returnArray[0].IF_SYS);
        			}        				
        		});
        		var dat = $i.sqlhouse(23,{ACCT_ID:acctId, IF_COD:ifCod});
        		dat.done(function(res){
		            var source =
		            {
		                datatype: "json",
		                datafields: [
		                    { name: 'MT_ID', type: 'string' },
		                    { name: 'MT_NM', type: 'string' },
							{ name: 'START_DAT', type: 'string'},
							{ name: 'END_DAT', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
		            var alginCenter = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:center; margin:4px 0px 0px 0px;">' + value + '</div>';
					}; 
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 10px;">' + value + '</div>';
					}; 
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 10px 0px 0px;">' + value + '</div>';
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridKpilist").jqxGrid(
		            {
		              	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
		                columnsresize: true,
		                columns: [
		                   { text: '지표ID', datafield: 'MT_ID', width: 127, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
		                   { text: '지표명', datafield: 'MT_NM', width: 500, align:'center', cellsalign: 'center', cellsrenderer: alginLeft},
						   { text: '사용시작일', datafield: 'START_DAT', width: 147, align:'center', cellsalign: 'center', cellsrenderer: alginCenter},
						   { text: '사용종료일', datafield: 'END_DAT', align:'center', cellsalign: 'center', cellsrenderer: alginCenter}
						   
		                ]
		            });
        		});
        	}
        	//신규 버튼 클릭 또는 초기화 해야 할 경우
        	function resetForm(){
        		$("#itemLabel").html('');
        		$("#itemNmLabel").html('');
				$("#txtIfcod").val('');
				$("#txtIfcodnm").val('');
				$("#cboIfunit").jqxComboBox({selectedIndex: 0});
				$("#cboIfsys").jqxComboBox({selectedIndex: 0});
				$("#txtareaSqldata").val('');
				$("#txtareaBigo").val('');
				$("#cboUseyn").val("Y");
        	}
        </script>
        <style type="text/css">
			.edit_inputSearch {
				height:17px;
				/*width:auto;*/
				margin:5px 0 !important;
				padding-bottom:3px;
				margin-right:5px !important;
				background: #fefee8;
				text-align: left;
				color: #2a2f3f !important;
				vertical-align: middle;
				border: 1px solid #eeeeee !important;
				float: left;
				vertical-align: middle;
				font-size: 12px;
			}
			.table-style td:last-child {
				border-bottom-right-radius:0px !important;
			}
		</style>   
    </head>
    <body class='blueish'>
		<div class="wrap" style="width:98%; min-width:1040px; margin:0 1%;">
			<form id="searchForm" name="searchForm"0>
				<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
					<div class="label type1 f_left">사용여부 : </div>
					<div class="combobox f_left" id="cboSearchyn" name="searchyn"></div>
					<div class="label type1 f_left">입력구분 : </div>
					<div class="combobox f_left" id="cboSearchifsys" name="searchifsys"></div>
					<div class="label type1 f_left">연계항목명 : </div>
					<div class="input f_left">
						<input type="text" id="txtSearch" name="searchifcodnm" />
					</div>
					<div class="group f_right pos_a" style="right:0px;">
						<div class="button type1 f_left">
							<input type="button" value="조회" id='btnSearch' width="100%" height="100%" onclick="search();" />
						</div>
						<div class="button type1 f_left">
							<input type="button" value="신규" id='btnNew' width="100%" height="100%" onclick="resetForm();" />
						</div>
					</div>
				</div>
			</form>
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style="width:44%; margin-right:1%;">
					<div class="grid f_left" style="width:100%; height:630px;">	
						<div id="gridIfcodlist"> </div>
					</div>
				</div> 
				<div class="content f_right" style="width:55%;">
					<form id="saveForm" name="saveForm">
						<div class="group f_left  w100p">
							<div class="label type2 f_left p_r10">ID:<span class="label sublabel" style="font-weight:bold;" id="itemLabel"></span></div>
							<div class="label type2 f_left">연계항목명:<span class="label sublabel" style="font-weight:bold;" id="itemNmLabel"></span></div>
						</div>
						<div class="table  f_left" style="width:100%; height:100%; margin:0;">
							<input type="hidden" id="txtPacctid" />
							<table width="100%" cellspacing="0" cellpadding="0" border="0">
								<tr>
									<th style="width:15% !important; font-weight:bold !important;">연계항목명<span class="th_must"></span></th>
									<td colspan="3">
										<input type="hidden" id="txtIfcod" name="ifcod" />
										<input type="text" id="txtIfcodnm" name="ifcodnm" class="input type2 f_left" style="width:98%; margin:3px 5px !important;"/>
									</td>
								</tr>
								<tr>
									<th style="width:15% !important;">항목단위</th>
									<td style="width:35% !important;">
										<div class="combobox f_left" style="margin:3px 5px !important;" id="cboIfunit" name="ifunit"></div>
									</td>
									<th style="width:15% !important;">입력구분</th>
									<td style="width:35% !important;">
										<div class="combobox f_left" style="margin:3px 5px !important;" id="cboIfsys" name="ifsys"></div>
									</td>
								</tr>
								<tr>
									<th style="width:15% !important;">SQL<span class="th_view pointer" onclick="showSql();">[전체보기]</span></th>
									<td colspan="3">
										<textarea class="textarea type2 f_left" style="width:98.5%;height:80px;margin:3px 5px !important;" id="txtareaSqldata" name="sqldata"></textarea>
									</td>
								</tr>
								<tr>
									<th style="width:15% !important;">비고</th>
									<td colspan="3">
										<textarea class="textarea type2 f_left" style="width:98.5%;height:30px;margin: 3px 5px !important;" id="txtareaBigo" name="bigo"></textarea>
									</td>
								</tr>
								<tr>
									<th style="width:15% !important;">사용여부</th>
									<td colspan="3">
										<div class="combobox f_left" style="margin:3px 5px !important;" id="cboUseyn" name="useyn"></div>
									</td>
								</tr>
							</table>
						</div>
						<div class="f_right m_t5">
							<div class="button type2 f_left">
								<input type="button" value="저장" id='btnSave' width="100%" height="100%" onclick="insert();" />
							</div>
							<div class="button type3 f_left">
								<input type="button" value="삭제" id='btnDelete' width="100%" height="100%" onclick="remove();" />
							</div>
						</div>
						<div class="group f_left  w100p">
							<div class="label type2 f_left">적용지표</div>
						</div>
						<div class="grid f_left" style="width:100%; height:326px;">
							<div id="gridKpilist"> </div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</body>
</html>

