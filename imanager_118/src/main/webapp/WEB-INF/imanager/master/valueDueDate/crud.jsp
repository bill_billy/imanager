<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<title id='Description'>실적입력 기한관리</title>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/style.blueish.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/blueish/css/jqx.blueish.css"/>
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery-ui-1.10.4.custom.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/jquery/styles/jquery.ui.datepicker.css" type="text/css">
	
	<script src="../../resources/cmresource/js/jquery/jquery-1.11.1.min.js"></script>
	<script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
	<script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
	<script src="../../resources/cmresource/js/jquery/jquery-ui-1.9.1.custom.js"></script>
	<script src="../../resources/cmresource/js/jquery/i18n/jquery.ui.datepicker-ko.js"></script>
	<style type="text/css">
	.ui-datepicker-trigger {
		width: 16px;
		height: 16px;
		margin: 7px 4px 4px 4px;
		background: url("../../../image/icon-calendar.png") center center no-repeat;
		cursor: pointer;
		float: left;
	}
	</style>
	<script type="text/javascript">
		//전역 변수
		var IS_NEW = true;
		
		//문서 로딩이 완료되면 처음 수행되는 로직
		$(document).ready(function(){ 
			init();
			setEvent();
			
		});
		
		//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
		function init() {
			// 버튼 초기화
			$('#btnSearch').jqxButton({ width: '',  theme:'blueish'});
			$('#btnNew').jqxButton({ width: '',  theme:'blueish'});
			$('#btnSave').jqxButton({ width: '',  theme:'blueish'});
			$('#btnDelete').jqxButton({ width: '',  theme:'blueish'});
			
			// 평가구분 콤보박스
			var dataEvaGbn = $i.sqlhouse(13,{COML_COD: 'EVA_GBN'});
			dataEvaGbn.done(function(res) {
				if(res.returnCode == 'EXCEPTION') {
					alertMessage(res);
					return;
				}
				
				var source = {
					datatype: 'json',
					datafields: [
						{ name: 'COM_COD' },
						{ name: 'COM_NM' }
					],
					id: 'id',
					localdata: res,
					async: false
				};
				var dataAdapter = new $.jqx.dataAdapter(source);
				$('#cboEvaGbn').jqxComboBox({
					selectedIndex: 0,
					source: dataAdapter,
					animationType: 'fade',
					dropDownHorizontalAlignment: 'right',
					displayMember: 'COM_NM',
					valueMember: 'COM_COD',
					dropDownWidth: 150,
					dropDownHeight: 150,
					width: 150,
					height: 22,
					theme:'blueish'
				});
			});
			
			// 학년도 콤보
			var dataYear = $i.sqlhouse(150, {S_COML_COD:'YEARS'});
			dataYear.done(function(res) {
				var source = {
					datatype: 'json',
					datafields: [
						{ name: 'COM_COD' },
						{ name: 'COM_NM' }
					],
					id: 'id',
					localdata: res,
					async: false
				};
				var dataAdapter = new $.jqx.dataAdapter(source);
				
				var cboYear = $('#cboYear');
				cboYear.jqxComboBox({
					selectedIndex: 0, 
					source: dataAdapter, 
					animationType: 'fade', 
					dropDownHorizontalAlignment: 'right', 
					displayMember: 'COM_NM', 
					valueMember: 'COM_COD', 
					dropDownWidth: 80, 
					dropDownHeight: 80, 
					width: 80, 
					height: 22,
					theme:'blueish'
				});
				cboYear.jqxComboBox({selectedIndex: 0});
			});
			
			// 년월 grid 구조
			var source = {
				datatype: 'json',
				datafields: [
					{ name: 'EVA_GBN', type: 'string'},
					{ name: 'PERIOD_YYYYMM', type: 'string'},
					{ name: 'YYYYMM', type: 'string'},
					{ name: 'YYYYMM_NM', type: 'string'}
				],
				localdata: [],
				updaterow: function (rowid, rowdata, commit) {
					commit(true);
				}
			};
			
			var underlinerenderer = function (row, columnfield, value, defaulthtml, columnproperties, rowdata) {
				var html = '<a href=\'javascript:select('+JSON.stringify(rowdata)+');\'>'+value+'</a>';
				var margin = '';
				var cellsalign = columnproperties.cellsalign;
				if(cellsalign == 'center') {
					margin = 'margin:6px  0px 0px  0px';
				} else if(cellsalign == 'left') {
					margin = 'margin:6px  0px 0px 10px';
				} else if(cellsalign == 'right') {
					margin = 'margin:6px 10px 0px  0px';
				}
				return '<div style="text-align:'+columnproperties.cellsalign+'; '+margin+'; text-decoration: underline;">'+html+'</div>';			
			};
			var dataAdapter = new $.jqx.dataAdapter(source);
			$('#gridEvalYearMonth').jqxGrid({
				width: '100%',
				height: 600,
				altrows:true,
				enablehover: true, //셀 마우스오버 효과
				pageable: false,
				sortable:true,
				source: dataAdapter,
				theme:'blueish',
				columnsheight:25 ,
				rowsheight: 25,
				columnsresize: true,
				columns: [
					{ text: '년월', datafield: 'YYYYMM_NM', width: '100%', align:'center', cellsalign: 'center', cellsrenderer: underlinerenderer}
				]
			});
			
			
			// 입력시작일, 입력종료일, 확인종료일
			var datepickers = $('#dateInputStart, #dateInputClose, #dateComfirmClose');
			datepickers.datepicker({
				changeYear: true,
				//changeMonth: true,
				yearRange: 'c-6:c+2',
				showOn: 'button',
				buttonImage: '../../resources/cmresource/image/icon-calendar.png',
				buttonImageOnly: true,
				dateFormat: 'yy.mm.dd' //2014.12.12
			});
			datepickers.datepicker('setDate', new Date());
			
			var dateYyyyMm = $('#dateYyyyMm');
			dateYyyyMm.datepicker({
				changeYear: true,
				//changeMonth: true,
				yearRange: 'c-6:c+2',
				showOn: 'button',
				buttonImage: '../../resources/cmresource/image/icon-calendar.png',
				buttonImageOnly: true,
				dateFormat: 'yymm' //201412
			});
			dateYyyyMm.datepicker('setDate', new Date());
			
			
			$.when(dataEvaGbn, dataYear).done(function(){
				search();	
			});
			
			
       	}
       	
       	//조회
       	function search(obj) {
       		
       		var dataList = $i.post('./list', {evaGbn: $('#cboEvaGbn').val(), year:$('#cboYear').val()});
       		dataList
       		.done(function(res) {
       			
       			if(res.returnCode == 'EXCEPTION') {
       				alertMessage(res);
					return;
				}
       			
       			var gridEvalYearMonth = $('#gridEvalYearMonth');
       			gridEvalYearMonth.jqxGrid('source')._source.localdata = res;
       			gridEvalYearMonth.jqxGrid('updatebounddata');
       			
       			var selectedrowindex = -1;
       			var rows = gridEvalYearMonth.jqxGrid('getrows');
       			var size = rows.length;
       			
       			if(obj != undefined) {
       				var objYyyymm = obj.yyyymm;
					for(var i=0; i<size; i++) {
						gridObject = rows[i];
						if(gridObject.PERIOD_YYYYMM == objYyyymm) {
							selectedrowindex = i;
							break;
						}
					}
				} else if(obj == undefined && size>0 && selectedrowindex == -1) {
					selectedrowindex = 0;
       			}
       			
       			if(selectedrowindex > -1) {
       				gridEvalYearMonth.jqxGrid('selectedrowindex', selectedrowindex);
           			select(gridEvalYearMonth.jqxGrid('getrowdata', selectedrowindex));	
       			}
       		})
       		.fail(function(e) {
       			alertMessage(e);
			});
       		
       	}
       	
       	//입력
       	function insert() {
       	}
       	
       	//수정
       	function update(idx,vendor) {
       	} 
       	
       	//삭제
       	function remove(){
       		if(confirm('삭제 하시겠습니까?')) {
       			
	       		if($('#gridEvalYearMonth').jqxGrid('selectedrowindex') == -1) {
	       			alert('삭제할 년월을 선택하세요.');
					return;
				}
	       		
	       		$i.remove('#saveForm')
				.done(function(res) {
					if(res.returnCode == 'EXCEPTION') {
						alertMessage(res);
						return;
					}
					clearAllForm();
					alertMessage(res);
					search();
	       		})
	       		.fail(function(e) {
	       			alertMessage(e);
				});
       		}
       		
       	}
       	
		//각종 이벤트 바인딩.
		function setEvent(){
       		// 조회 버튼
       		$('#btnSearch').on('click', function(event){
       			clearDetailForm();
       			search();
    		});
       		
       		// 신규 버튼
       		$('#btnNew').on('click', function(event){
       			IS_NEW = true;
       			$('#gridEvalYearMonth').jqxGrid('clearselection');
           		clearDetailForm();
    		});
       		
       		// 저장 버튼
       		$('#btnSave').on('click', function(event){
       			save(); 
    		});
       		
       		// 삭제 버튼
       		$('#btnDelete').on('click', function(event){
       			remove(); 
    		});
       		
       		
       		// 입력시작일, 입력종료일, 확인종료일 날짜 범위 이벤트
       		var dateInputStart = $('#dateInputStart');
       		var dateInputClose = $('#dateInputClose');
       		var dateComfirmClose = $('#dateComfirmClose');
       		/*
       		dateInputStart.datepicker('option', 'maxDate', dateInputClose.val());
       		dateInputClose.datepicker('option', 'minDate', dateInputStart.val());
       		dateInputClose.datepicker('option', 'maxDate', dateComfirmClose.val());
       		dateComfirmClose.datepicker('option', 'minDate', dateInputClose.val());
       		*/
       		dateInputStart.datepicker('option', 'onClose', function ( selectedDate ) {
       			dateInputClose.datepicker('option', 'minDate', selectedDate );
       	    });
       		dateInputClose.datepicker('option', 'onClose', function ( selectedDate ) {
       			dateInputStart.datepicker('option', 'maxDate', selectedDate );
       			dateComfirmClose.datepicker('option', 'minDate', selectedDate );
       	    });
       		dateComfirmClose.datepicker('option', 'onClose', function ( selectedDate ) {
       			dateInputClose.datepicker('option', 'maxDate', selectedDate );
       	    });
       	}
       	
       	//테스트 함수.
       	function test(){ 
       		var async = $i.sqlhouse(4007,{S_ID:"C"},function(res){});
       		async.done(function(res){ 
       			console.log(res);
       		}).fail(function(error){
       			console.log(error); 
       		});
       	}
	
		//여기 부터 추가 스크립트를 작성해 주세요.
		// 학년도 그리드, 상세정보 초기화
		function clearAllForm() {
			IS_NEW = true;
			var gridEvalYearMonth = $('#gridEvalYearMonth');
			// 그리드 초기화
       		gridEvalYearMonth.jqxGrid('clear');
       		gridEvalYearMonth.jqxGrid('clearselection');
       		
       		$('#hiddenYyyyMm').val('');
       		
       		clearDetailForm();
		}
		
		// 상세정보 초기화
		function clearDetailForm() {
			// 상세정보 검색 조건
			$('#hiddenEvaGbn').val('');
			$('#hiddenSaveFormYyyyMm').val('');
			
       		// 상세정보 내용
			$('#dateInputClose, #dateComfirmClose').datepicker('setDate', new Date());
       		
			// 입력일, 입력자, 최종 수정일, 최종 수정자
			$('#spanInsertDat, #spanInsertEmp, #spanUpdateDat, #spanUpdateEmp').text('');
			
			var dateYyyyMm = $('#dateYyyyMm');
			dateYyyyMm.datepicker('setDate', new Date());
			dateYyyyMm.next('img.ui-datepicker-trigger').show();
		}
		
		// 상세정보 조회
		function select(rowdata) {
			IS_NEW = false;
			clearDetailForm();
			
			$('#hiddenEvaGbn').val(rowdata.EVA_GBN);
			$('#hiddenYyyyMm').val(rowdata.YYYYMM);
			$('#hiddenSaveFormYyyyMm').val(rowdata.YYYYMM);
			
			var dataDetailForm = $i.select('#searchForm');
			dataDetailForm
			.done(function(res) {
				
				if(res.returnCode == 'EXCEPTION') {
					alertMessage(res);
					return;
				}
       			var obj = res.returnObject;
       			
       			var dateYyyyMm = $('#dateYyyyMm');
       			dateYyyyMm.datepicker('setDate', convertStringToDate(obj.YYYYMM_NM+'01'));
       			$('#dateInputStart').datepicker('setDate', convertStringToDate(obj.START_DAT));
       			$('#dateInputClose').datepicker('setDate', convertStringToDate(obj.END_DAT));
       			$('#dateComfirmClose').datepicker('setDate', convertStringToDate(obj.DELAY_DAT));
       			
       			$('#spanInsertDat').text(obj.INSERT_DAT);
       			$('#spanInsertEmp').text(obj.INSERT_EMP);
       			$('#spanUpdateDat').text(obj.UPDATE_DAT);
       			$('#spanUpdateEmp').text(obj.UPDATE_EMP);
       			
       			dateYyyyMm.next('img.ui-datepicker-trigger').hide();
       		})
       		.fail(function(e) {
       			alertMessage(e);
			});
		}
		
		// yyyymmdd 문자열을 date 타입으로 변환
		function convertStringToDate(stringDate) {
			var yyyy = stringDate.substr(0, 4);
			var mm = stringDate.substr(4, 2)-1;
			var dd = stringDate.substr(6, 2);
			
			return new Date(yyyy, mm, dd);
		}
		
		// 공백체크
		function isnull(str) {
			var is = false;
			if(str == undefined || str.replace(/ /ig, '') == '') {
				is = true; 
			}
			return is;
		}
		// 숫자 범위 체크
		function numberbetween(val, startnum, endnum) {
			var is = false;
			if(isNaN(val)) {
				return false;
			}
			var v = parseInt(val);
			if(v >= startnum && v <= endnum) {
				is = true;
			}
			return is;
		}
		// 날짜체크
		function isdate(val) {
			if(isnull(val)) return false;
			
			var size = val.length
			var vals = [];
			if(size == 8) {
				vals[0] = val.substr(0, 4);
				vals[1] = val.substr(4, 2);
				vals[2] = val.substr(6, 2);
			} else if(size == 10) {
				vals[0] = val.substr(0, 4);
				vals[1] = val.substr(5, 2);
				vals[2] = val.substr(8, 2);
			}
			if(!numberbetween(vals[0], 1000, 9999)) return false;
			if(!numberbetween(vals[1], 1, 12)) return false;
			if(!numberbetween(vals[2], 1, 31)) return false;
			
			return true;
		}
		
		// 저장
		function save() {
			if(confirm('저장 하시겠습니까?')) {
				
				var hiddenSaveFormYyyyMm = $('#hiddenSaveFormYyyyMm');
				var dateYyyyMm = $('#dateYyyyMm');
				
				// 수정
				if(!IS_NEW && hiddenSaveFormYyyyMm.val() == '') {
					alert('저장할 년월을 선택하세요.');
					return;	
				}
				
				var hiddenEvaGbn = $('#hiddenEvaGbn');
				if(isnull(hiddenEvaGbn.val())) {
					var cboEvaGbn = $('#cboEvaGbn');
					if(isnull(cboEvaGbn.val())) {
						alert('평가구분을 선택하세요.');
						return;
					} else {
						hiddenEvaGbn.val($('#cboEvaGbn').val());	
					}
				}
				if(!numberbetween(dateYyyyMm.val().substring(0,4), 1000, 9999)
					|| !numberbetween(dateYyyyMm.val().substring(4,6), 1, 12)) {
					alert('올바른 기준년월을 선택하세요.');
					return;
				}
				if(!isdate($('#dateInputStart').val())) {
					alert('올바른 입력 시작일을 선택하세요.');
					return;
				}
				if(!isdate($('#dateInputClose').val())) {
					alert('올바른 입력 종료일을 선택하세요.');
					return;
				}
				if(!isdate($('#dateComfirmClose').val())) {
					alert('올바른 확인 종료일을 선택하세요.');
					return;
				}
				
				
				var gridEvalYearMonth = $('#gridEvalYearMonth');
				
				// 회기년월 체크
				$i.service('getPeriodYyyyMm', [dateYyyyMm.val()])
				.done(function(res) {
					if(res.returnCode == 'EXCEPTION') {
					} else {
						// 신규 입력시 체크 
						if(IS_NEW && dateYyyyMm.next('img.ui-datepicker-trigger').css('display') != 'none') {
							hiddenSaveFormYyyyMm.val(dateYyyyMm.val());
						
							var checkedYyyymm = res.returnObject.PERIOD_YYYYMM;
							
							var rows = gridEvalYearMonth.jqxGrid('getrows');
			       			var gridObject = null;
							for(var i=0; i<rows.length; i++) {
								gridObject = rows[i];
								if(gridObject.PERIOD_YYYYMM == checkedYyyymm) {
									alert('중복된 기준년월을 선택하였습니다..');
									return;
								}
							} // end for
						} // 신규입력 체크 종료
						
						
						$i.post('./save', '#saveForm')
						.done(function(res) {
							if(res.returnCode == 'EXCEPTION') {
								alertMessage(res);
							} else {
								alertMessage(res);
								search(res.returnObject);
							}
						})
						.fail(function(e){
							alertMessage(e);
						}); // end save ajax
					} // end else
				})
				.fail(function(e){
					alertMessage(e);
				}); // 회기년월 체크 종료
			}
		}
		function alertMessage(res) {
			alert(res.returnMessage);
		}
	</script>
</head>
<body class='blueish'>
	<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
	<div class="wrap" style="width:1160px; min-width:1160px; margin:0 10px;">
		<form id="searchForm" name="searchForm" action="list" method="get">
			<input type="hidden" id="hiddenYyyyMm" name="yyyymm"/>
			<div class="header f_left" style="width:100%; height:27px; margin:10px 0">
				<div class="label type1 f_left">평가구분:</div>
				<div class="combobox f_left" id="cboEvaGbn" name="evaGbn"></div>
				<div class="label type1 f_left">학년도:</div>
				<div class="combobox f_left" id="cboYear" name="year"></div>
				<div class="group_button f_right">
					<div class="button type1 f_left"><input type="button" value="조회" id="btnSearch" width="100%" height="100%" /></div>
					<div class="button type1 f_left"><input type="button" value="신규" id="btnNew" width="100%" height="100%" /></div>
					</div><!--group_button--> 
			</div><!--//header-->
		</form><!-- //searchForm -->
		<div class="container  f_left" style="width:100%; margin:10px 0;">
			<div class="content f_left" style=" width:19%; margin-right:1%;">
				<div class="grid f_left" style="width:100%; height:600px;"><div id="gridEvalYearMonth"></div></div>
			</div><!--//content-->    
			<div class="content f_right" style="width:80%;">
				<div class="group f_left w100p m_b5">
					<div class="label type2 f_left m_r10">상세정보</div>
				</div><!--group-->
				<div class="table f_left" style="width:100%; height:; margin:0;">
					<form id="saveForm" name="saveForm" action="save" method="post">
						<input type="hidden" id="hiddenEvaGbn" name="evaGbn"/>
						<input type="hidden" id="hiddenSaveFormYyyyMm" name="yyyymm" />
						
						<table width="100%" cellspacing="0" cellpadding="0" border="0">
							<tbody>
								<tr>
									<th class="w15p"><div>기준년월</div></th>
									<td colspan="3">
										<div class="cell">
											<input id="dateYyyyMm" name="yyyymm_nm" type="text" class="input type1 t_left f_left" style="width:140px; margin:3px 0;" readonly="readonly"/>
										</div>
									</td>
								</tr>
								<tr>
									<th class="w15p"><div>입력 시작일</div></th>
									<td>
										<div class="cell">
											<input id="dateInputStart" name="inputStart" type="text" class="input type1 t_left f_left" style="width:140px; margin:3px 0;" readonly="readonly"/>
										</div>
									</td>
									<th class="w15p"><div>입력 종료일</div></th>
									<td>
										<div class="cell">
											<input id="dateInputClose" name="inputClose" type="text" class="input type1 t_left f_left" style="width:140px; margin:3px 0;" readonly="readonly"/>
										</div>
									</td>
								</tr>
								<tr>
									<th class="w15p"><div>확인 종료일</div></th>
									<td colspan="3">
										<div class="cell">
											<input id="dateComfirmClose" name="confirmClose" type="text" class="input type1 t_left f_left" style="width:140px; margin:3px 0;" readonly="readonly"/>
										</div>
									</td>
								</tr>
								<tr>
									<th class="w15p"><div>입력일</div></th>
									<td><div class="cell"><span id="spanInsertDat"></span></div></td>
									<th class="w15p"><div>입력자</div></th>
									<td><div class="cell"><span id="spanInsertEmp"></span></div></td>
								</tr>
								<tr>
									<th class="w15p"><div>최종 수정일</div></th>
									<td><div class="cell"><span id="spanUpdateDat"></span></div></td>
									<th class="w15p"><div>최종 수정자</div></th>
									<td><div class="cell"><span id="spanUpdateEmp"></span></div></td>
								</tr>
							</tbody>
						</table>
					</form><!-- //saveForm -->
				</div>
				<!--table--> 
				<div class="group_button f_right m_t5">
					<div class="button type2 f_left"><input type="button" value="저장" id="btnSave" width="100%" height="100%" /></div>
					<div class="button type3 f_left"><input type="button" value="삭제" id="btnDelete" width="100%" height="100%" /></div>
				</div><!--group_button-->
			</div><!--//content--> 
		</div><!--//container--> 
	</div><!--//wrap-->
</body>
</html>