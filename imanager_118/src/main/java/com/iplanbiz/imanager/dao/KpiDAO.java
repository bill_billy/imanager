package com.iplanbiz.imanager.dao;

import java.util.HashMap;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class KpiDAO extends SqlHouseDAO {

	public  String createKpiValue(String acctid, String evaGbn, String kpiID, String yyyymm) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		String msg = "";
		param.put("ACCT_ID", acctid);
		param.put("EVA_GBN", evaGbn);
		param.put("KPI_ID", kpiID);
		param.put("YYYYMM", yyyymm);
		param.put("ERROR", msg);
		
		getSqlSession().selectList("createKpiValue", param);
		
		String retrunValue = (String)param.get("ERROR");
		
		// procedure 에서 에러시 리턴되는 값은 1
		return retrunValue;
	}

}
