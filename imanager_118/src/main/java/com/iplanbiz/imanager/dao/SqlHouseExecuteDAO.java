package com.iplanbiz.imanager.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSession;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional; 

import com.iplanbiz.core.session.LoginSessionInfo;

@Repository
public class SqlHouseExecuteDAO extends SqlHouseDAO {

	enum SqlColumn { SQL, SQL_EXCEL };
	private Logger logger = LoggerFactory.getLogger(getClass()); 
	
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	/**
	 * Sql House 에서 insert, update, delete, merge등 crud성 문장을 실행할때 사용.
	 * @param idx
	 * @param param
	 * @param longColumnName 파라미터에 CLOB 데이터가 있는경우 사용.   생략 가능 ex)  new String[]{"Column1"} or null
	 * @return
	 * @throws Exception
	 */
	public int crudQuery(int idx, HashMap<String,Object> param, String[] longColumnName) throws Exception{
		logger.debug("sqlHouseExecuteDAO.crudQuery,idx="+idx+",param={}",param.toString());
		HashMap<String,Object> sqlHouse = this.getSqlHouseOne(idx); 
		String[] query = sqlHouse.get("SQL").toString().split(";");
		return this.crudQuery(query, param, longColumnName); 
	}
	
	public int crudQuery(String sql, HashMap<String,Object> param, String[] longColumnName) throws Exception{
		return this.crudQuery(sql.split(";"), param, longColumnName);		
	}
	
	public int crudQuery(String[] sql, HashMap<String,Object> param, String[] longColumnName) throws Exception{
		logger.debug("sqlHouseExecuteDAO.crudQuery,sql="+sql+",param={}",param.toString());
		SqlSession session = null;
		PreparedStatement ps = null;
		int	 rs = 0; 

		String[] query = sql;
		String str = "";
		
		try{
			session = sqlSessionFactory.openSession();
				
			for(int i = 0; i<query.length;i++){
				str = query[i];
				if(str.trim().equals("")==false){
					if(longColumnName != null){
						for(int j=0; j<longColumnName.length; j++){
							
							str = str.replace(longColumnName[j], "?");
							logger.info("Custom Query : {}" , str);	
						}
					}
					ps = session.getConnection().prepareStatement(replaceQuery(str,param));
					if(longColumnName != null){
						for(int j=0; j<longColumnName.length; j++){
							StringReader sr = new StringReader((String) param.get(longColumnName[j]));
							ps.setCharacterStream(j+1, sr, ((String) param.get(longColumnName[j])).length());
						}
					}
					rs = ps.executeUpdate();
					logger.info("Custom rs: {}" ,rs);
				}
			}
		}catch(SQLException e){
			logger.error("SQLException: {}", e.toString());
			throw e;
		}catch(RuntimeException e){
			logger.error("RuntimeException: {}", e.toString());
			throw e;
		}
		finally{
			if(session != null) 	session.close();
			if(ps != null) 			try{ ps.close(); }catch(Exception e){
				throw new RuntimeException(e.getMessage());
			}
		}
		return rs;
	}
	/**
	 * Sql House 에서 insert, update, delete, merge등 crud성 문장을 실행할때 사용.
	 * @param idx
	 * @param param 
	 * @return
	 * @throws Exception
	 */
	public int crudQuery(int idx, HashMap<String,Object> param) throws Exception{
		return this.crudQuery(idx,param,null);
	}
	
	/**
	 * Sql House에서 select 문장 실행시.. 리턴 값을 List< Linked HashMap> 으로 반환, Clob 자동 변환됨.
	 * @param idx
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<LinkedHashMap<String,Object>> selectHashMap(int idx, HashMap<String,Object> param) throws Exception{
		return this.selectHashMap(idx, param, SqlColumn.SQL);
	}
	
	/**
	 * Sql House에서 select 문장 실행시.. 리턴 값을 Json Array 으로 반환, Clob 자동 변환됨.
	 * @param idx
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public JSONArray selectJSONArray(int idx, HashMap<String,Object> param) throws Exception{
		return this.selectJSONArray(idx, param, SqlColumn.SQL);
	}
	
	
	/**
	 * Sql House에서 Excel Query 실행시.. 리턴 값을 List< Linked HashMap> 으로 반환, Clob 자동 변환됨.
	 * @param idx
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<LinkedHashMap<String,Object>> excelHashMap(int idx, HashMap<String,Object> param) throws Exception{
		return this.selectHashMap(idx, param, SqlColumn.SQL_EXCEL);
	}
	
	/**
	 * Sql House에서 Excel Query 실행시.. 리턴 값을 Json Array 으로 반환, Clob 자동 변환됨.
	 * @param idx
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public JSONArray excelJSONArray(int idx, HashMap<String,Object> param) throws Exception{
		return this.selectJSONArray(idx, param, SqlColumn.SQL_EXCEL);
	}
	
	
	
	/*
	 * 아래부터 private 메소드. 디버그 중이 아니라면 볼 필요 없음.
	 */	
	
	private List<LinkedHashMap<String,Object>> selectHashMap(int idx, HashMap<String,Object> param, SqlColumn column) throws Exception{
		HashMap<String,Object> sqlHouse = this.getSqlHouseOne(idx);
		String query = sqlHouse.get(column.toString()).toString().replace(";","");
		return this.selectHashMap(query, param);
	}
	private List<LinkedHashMap<String,Object>> selectHashMap(String query) throws Exception{
		HashMap<String,Object> param = new HashMap<String, Object>();
		return this.selectHashMap(query, param);
	}
	private List<LinkedHashMap<String,Object>> selectHashMap(String query, HashMap<String,Object> param) throws Exception{
		
		List<LinkedHashMap<String,Object>> returnRows = new ArrayList<LinkedHashMap<String,Object>>(); 
		SqlSession session = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			  
			query = this.replaceQuery(query, param);
			logger.debug("selectQuery : {}", query);
			
			
			session = sqlSessionFactory.openSession();
			st = session.getConnection().createStatement();
			rs = st.executeQuery(query);	
			ResultSetMetaData rsMeta = rs.getMetaData();
			   int colCnt = rsMeta.getColumnCount();
			   
			while(rs.next()){				
				LinkedHashMap<String, Object> rowMap = new LinkedHashMap<String, Object>();    
				for (int i = 1; i <= colCnt; i++) {
					Object obj = rs.getObject(i);
					String value = "";
					if(obj!=null){
						if(obj instanceof Clob){
							value = this.clobToString((Clob)obj);
						}else{
							value = obj.toString();
						}
					}

					rowMap.put(rsMeta.getColumnName(i), value);
					
		        }
				returnRows.add(rowMap);
				logger.debug("Custom rowMap: {}" , rowMap.toString());
			}
			logger.debug("Custom rowMap Count: {}", returnRows.size());

		}catch(SQLException e){
			logger.error("SQLException: {}", e.toString());
			throw e;
		}catch(RuntimeException e){
			logger.error("RuntimeException: {}", e.toString());
			throw e;
		}finally{
			if(session != null) 	session.close();
			if(st != null) 			try{ st.close(); }catch(SQLException e){throw e;}
			if(rs != null) 			try{ rs.close(); }catch(SQLException e){throw e;}
		}
		return returnRows;
	}

	
	private JSONArray selectJSONArray(int idx, HashMap<String,Object> param, SqlColumn column) throws Exception{
		
		JSONArray returnRows = new JSONArray();
		HashMap<String,Object> sqlHouse = null;
		SqlSession session = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			sqlHouse = this.getSqlHouseOne(idx);
			String query = sqlHouse.get(column.toString()).toString().replace(";","");
			query = this.replaceQuery(query, param);
			logger.debug("selectQuery : {}", query);
			
			
			session = sqlSessionFactory.openSession();
			st = session.getConnection().createStatement();
			rs = st.executeQuery(query);
			ResultSetMetaData rsMeta = rs.getMetaData();
			   int colCnt = rsMeta.getColumnCount();
			   
			while(rs.next()){
				JSONObject rowMap = new JSONObject();    
				for (int i = 1; i <= colCnt; i++) {
					Object obj = rs.getObject(i);            					
					String value = "";
					if(obj!=null){
						if(obj instanceof Clob){
							value = this.clobToString((Clob)obj);
						}else{
							value = obj.toString();
						}
					} 
					rowMap.put(rsMeta.getColumnName(i), value);
		        }
				returnRows.add(rowMap);
				logger.debug("Custom rowMap: {}" , rowMap.toString());
			}
			logger.debug("Custom rowMap Count: {}", returnRows.size());

		}catch(SQLException e){
			logger.error("SQLException: {}", e.toString());
			throw e;
		}catch(RuntimeException e){
			logger.error("RuntimeException: {}", e.toString());
			throw e;
		}finally{
			if(session != null) 	session.close();
			if(st != null) 			try{ st.close(); }catch(SQLException e){throw e;}
			if(rs != null) 			try{ rs.close(); }catch(SQLException e){throw e;}
		}
		return returnRows;
	} 
	
	// 패턴 제거
	private String removeRex(String rex, String inp){
		//Pattern.DOTALL : .*(모든문자열)패턴에 개행 문자도 함께 포함시킴.
		Pattern numP = Pattern.compile(rex,Pattern.DOTALL); 
		Matcher mat = numP.matcher(inp);
	
		inp = mat.replaceAll("");
		return inp ; 
	}
	// 패턴 Replace
	private String replaceRex(String rex, String inp,String replaceStr){
			//Pattern.DOTALL : .*(모든문자열)패턴에 개행 문자도 함께 포함시킴.
			Pattern numP = Pattern.compile(rex,Pattern.DOTALL); 
			Matcher mat = numP.matcher(inp);
	 
			inp = mat.replaceAll(replaceStr);
			return inp ; 
	}
	// 패턴 검색
	private HashMap<String,List<String>> searchRex(String rex, String inp){
				//Pattern.DOTALL : .*(모든문자열)패턴에 개행 문자도 함께 포함시킴.
				Pattern numP = Pattern.compile(rex,Pattern.DOTALL); 
				Matcher mat = numP.matcher(inp);
				HashMap<String,List<String>> hashResult = new HashMap<String,List<String>>();
	
				while(mat.find()){ 
					String key = mat.group(1).replace("@(","").replace("@","").replace(")","").trim();
					if(hashResult.containsKey(key)==false)
						hashResult.put(key,new ArrayList<String>());
					
					hashResult.get(key).add(mat.group()); 
					for(int i = 0;i<mat.groupCount();i++){
						//logger.info("mat.group="+mat.group(i+1));
					}
					
				}
				return hashResult ; 
	}
			
	
	private String replaceQuery(String query, Map<String, Object> map){
		//공통 변수 추가.
		//보안상 ACCT_ID, USER_ID는 로그인한 유저로 자동 바인딩.
		map.put("@(S_ACCT_ID)", loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_USER_ID)", loginSessionInfoFactory.getObject().getUserId());
		String typeStringpattern="@\\(\\s*([a-zA-Z0-9_]+)\\s*\\,?\\s*([a-zA-Z0-9_.]*)\\s*\\)";
		HashMap<String,List<String>> newParam = this.searchRex(typeStringpattern,query);
		String entryKey = null;
		try {
			for (Map.Entry<String, Object> entry : map.entrySet()) {
				
				entryKey = entry.getKey(); 
				String key = entry.getKey();
				if(key.indexOf("@")==-1) key+="@("+key+")";
				String value = entry.getValue().toString();
				
				System.out.println("key : "+key+"  value : "+value);
				String newparamkey = entry.getKey().replace("@(","").replace(")","").split(",")[0].trim(); 
				if(newParam.containsKey(newparamkey)){
					if(key.indexOf("@(S_")!=-1){
						//싱글 쿼테이션 SQL문에 적합하게 변경 //SQL인젝션 방어
						value = value.replace("'", "''");
						value = "'"+value+"'";
						List<String> paramkeys = newParam.get(newparamkey);
						//System.out.println("paramkeys"+paramkeys.size());
						for(int i = 0;i<paramkeys.size();i++){
							//System.out.println("parameter["+paramkeys.get(i)+"] value["+value+"]");
							query = query.replace( paramkeys.get(i), value );
						}
					}
					else if(key.indexOf("@(AS_")!=-1){
						//싱글쿼테이션 삭제
						//value = value.replace("'", "''");
						//value = "'"+value+"'";
						List<String> paramkeys = newParam.get(newparamkey);
						//System.out.println("paramkeys"+paramkeys.size());
						for(int i = 0;i<paramkeys.size();i++){
							//System.out.println("parameter["+paramkeys.get(i)+"] value["+value+"]");
							query = query.replace( paramkeys.get(i), value );
						}
					}
					else if(key.indexOf("@(N_")!=-1){
						//숫자타입일 경우 숫자가 맞는지 검증
						try{
							Double.parseDouble(value);
						}catch(Exception e){
							value="null";
						}
						List<String> paramkeys = newParam.get(newparamkey);
						for(int i = 0;i<paramkeys.size();i++){
							//System.out.println("parameter["+paramkeys.get(i)+"] value["+value+"]");
							query = query.replace( paramkeys.get(i), value );
						} 
					}
					else if(key.indexOf("@(SI_")!=-1){
						List<String> paramkeys = newParam.get(newparamkey);
						String args ="";
						if(paramkeys.get(0).split(",").length!=1){
							args = paramkeys.get(0).split(",")[1].replace(")","");
						}
						if(value.trim().equals("")==false){
							value = value.replace("'", "''");
							value = args+" IN ('"+value.replace(",","','")+"') ";
						}else{
							value = " 1=1 ";
						}
						for(int i = 0;i<paramkeys.size();i++){
							//System.out.println("parameter["+paramkeys.get(i)+"] value["+value+"]");
							
							query = query.replace( paramkeys.get(i), value );
						}
					}
					else if(key.indexOf("@(NI_")!=-1){
						if(value.trim().equals("")==false){
							try{
								Double.parseDouble(value.replace(",","").trim()); 
							}catch(Exception e){
								value="";
							}
							List<String> paramkeys = newParam.get(newparamkey);
							String args ="";
							if(paramkeys.get(0).split(",").length!=1){
								args = paramkeys.get(0).split(",")[1].replace(")","");
							}
							if(value.trim().equals("")==false){
								value = value.replace("'", "''");
								value = args+" IN ("+value+") ";
							}else{
								value = " 1=1 ";
							}
							for(int i = 0;i<paramkeys.size();i++){
								//System.out.println("parameter["+paramkeys.get(i)+"] value["+value+"]");
								
								query = query.replace( paramkeys.get(i), value );
							}
						}
					}
					else{
						//싱글 쿼테이션 SQL문에 적합하게 변경 //SQL인젝션 방어
						//value = value.replace("'", "''"); 
						List<String> paramkeys = newParam.get(newparamkey);
						for(int i = 0;i<paramkeys.size();i++){
							//System.out.println("parameter["+paramkeys.get(i)+"] value["+value+"]");
							query = query.replace( paramkeys.get(i), value );
						}
					}
				}
				else{
					//싱글 쿼테이션 SQL문에 적합하게 변경 //SQL인젝션 방어
					//value = value.replace("'", "''");
					query = query.replace(key, value );
				}
			} 
			
			return query;
		} catch (Exception ne) {
			// TODO: handle exception
			logger.error("Exception: {}", ne.toString() +ne.getMessage()); 

		}
		return query;
	}
	
	/*
	  * Clob 를 String 으로 변경
	  */
	 private String clobToString(Clob clob) throws SQLException,
	   IOException {

		  if (clob == null) {
		   return "";
		  }
	
		  StringBuffer strOut = new StringBuffer();
	
		  String str = "";
	
		  BufferedReader br = new BufferedReader(clob.getCharacterStream());
	
		  while ((str = br.readLine()) != null) {
		   strOut.append(str);
		  }
		  return strOut.toString();
	 } 
}
