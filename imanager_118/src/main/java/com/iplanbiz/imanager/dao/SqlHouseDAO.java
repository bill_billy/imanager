package com.iplanbiz.imanager.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSService;

@Repository
public class SqlHouseDAO extends SqlSessionDaoSupport {
	
	@Autowired
	private DBMSService dbmsService;
	
	@Autowired SqlSessionFactory sqlSessionFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	//참고..databaseId 가져오기.
	//String databaseId = this.getSqlSession().getConfiguration().getDatabaseId();
	
	
	public HashMap<String,Object> getSqlHouseOne(int idx) throws Exception{
		HashMap<String,Object> sqlmap  = null;
		try {
			
			HashMap<String,Integer> param = new HashMap<String, Integer>();
			param.put("IDX", idx);
			sqlmap = (HashMap<String,Object>)this.getSqlSession().selectOne("getSqlHouseOne", param);
			logger.info("SqlHouseDAO idx : {}",idx);
			logger.info("SqlHouseDAO SQL : {}",sqlmap.get("SQL"));
			logger.info("SqlHouseDAO SQL_EXCEL : {}",sqlmap.get("SQL_EXCEL"));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error",e);
			throw e;
		}
		return sqlmap;
	}
	
	public List<HashMap<String,Object>> getSqlHouseList() throws Exception{
		List<HashMap<String,Object>> sqlmapList  = null;
		try {
			HashMap<String,Object> param = new HashMap<String, Object>();
			sqlmapList = this.getSqlSession().selectList("getSqlHouseList", param);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error",e);
			throw e;
		}
		return sqlmapList;
	}
	
	public List<HashMap<String,Object>> getSqlHouseList(String searchText) throws Exception{
		List<HashMap<String,Object>> sqlmapList  = null;
		try {
			HashMap<String,Object> param = new HashMap<String, Object>();
			param.put("searchText", searchText);
			sqlmapList = this.getSqlSession().selectList("getSqlHouseList", param); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error",e);
			throw e;
		}
		return sqlmapList;
	} 

	public int insertSqlHouse(String vendor, String description,
			String sql, String sql_excel) throws Exception {
		Integer result = 0;
		try {
			HashMap<String,Object> param = new HashMap<String, Object>(); 
			param.put("VENDOR", vendor);
			param.put("DESCRIPTION", description);
			param.put("SQL", sql);
			param.put("SQL_EXCEL", sql_excel);
			param.put("USER_ID", "admin");
			result = this.getSqlSession().insert("insertSqlHouse", param); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error",e);
			throw e;
		}
		return result;
	}
	
	public int updateSqlHouse(String idx, String vendor, String description,
			String sql, String sql_excel) throws Exception {
		Integer result = 0;
		try {
			HashMap<String,Object> param = new HashMap<String, Object>();
			param.put("IDX", idx);
			param.put("VENDOR", vendor);
			param.put("DESCRIPTION", description);
			param.put("SQL", sql);
			param.put("SQL_EXCEL", sql_excel);
			param.put("USER_ID", "admin"); 
			logger.info(param.toString());
			result = this.getSqlSession().update("updateSqlHouse", param); 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("error",e);
			throw e;
		}
		return result;
	}

}
