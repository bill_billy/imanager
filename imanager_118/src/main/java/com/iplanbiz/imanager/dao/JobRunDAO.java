/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.imanager.dao;

import java.util.HashMap;

import org.springframework.stereotype.Repository;

@Repository
public class JobRunDAO extends SqlHouseDAO {
	
	
	public int executeSP_IMFE0012(HashMap<String, Object> param) {
		
		getSqlSession().selectList("executeSP_IMFE0012", param);
		
		String retrunValue = (String)param.get("ERROR");
		
		// procedure 에서 에러시 리턴되는 값은 1
		return (retrunValue == null)?1:-1;
	}
}
