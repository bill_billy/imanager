package com.iplanbiz.imanager.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iplanbiz.core.io.dbms.DBMSService;

@Repository
public class BasicDAO extends SqlSessionDaoSupport {
	
 
	@Autowired SqlSessionFactory sqlSessionFactory;
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	 
	
	
	/*
	 *getSqlSession 의 메소드 구현. 시작
	 */
	
	/**
	 * @param mybatisId : 마이바티스 아이디
	 * @param map : 파라미터 HashMap 권장
	 * @return
	 */
	public List<HashMap<String, Object>> selectList(String mybatisId, Object map){
		return getSqlSession().selectList(mybatisId, map);
	}
	
	/**
	 * @param mybatisId : 마이바티스 아이디 
	 * @return
	 */
	public List<HashMap<String, Object>> selectList(String mybatisId){
		return getSqlSession().selectList(mybatisId);
	}
	
	
	/**
	 * @param mybatisId : 마이바티스 아이디
	 * @param map : 파라미터 HashMap 권장
	 * @return update 결과
	 */
	public int update(String mybatisId,Object map ){
		return getSqlSession().update(mybatisId,map);		
	}
	/**
	 * @param mybatisId : 마이바티스 아이디 
	 * @return update 결과
	 */
	public int update(String mybatisId){
		return getSqlSession().update(mybatisId);		
	}
	
	/**
	 * @param mybatisId : 마이바티스 아이디
	 * @param map : 파라미터 HashMap 권장
	 * @return insert 결과
	 */	
	public int insert(String mybatisId,Object map ){
		return getSqlSession().insert(mybatisId,map);
	}
	/**
	 * @param mybatisId : 마이바티스 아이디 
	 * @return insert 결과
	 */
	public int insert(String mybatisId ){
		return getSqlSession().insert(mybatisId);
	}
	
	/**
	 * @param mybatisId : 마이바티스 아이디
	 * @param map : 파라미터 HashMap 권장
	 * @return delete 결과
	 */
	public int delete(String mybatisId,Object map ){
		return getSqlSession().delete(mybatisId,map);
	}
	
	/**
	 * @param mybatisId : 마이바티스 아이디 
	 * @return delete 결과
	 */
	public int delete(String mybatisId){
		return getSqlSession().delete(mybatisId);
	}
	
	/**
	 * @param mybatisId : 마이바티스 아이디
	 * @param map : 파라미터 HashMap 권장
	 * @return  
	 */
	public Object selectOne(String mybatisId,Object map ){
		return getSqlSession().selectOne(mybatisId,map);
	}
	
	/**
	 * @param mybatisId : 마이바티스 아이디 
	 * @return  
	 */
	public Object selectOne(String mybatisId ){
		return getSqlSession().selectOne(mybatisId);
	}
	
	/*
	 *getSqlSession 의 메소드 구현. 끝
	 */ 
	
}
