package com.iplanbiz.imanager.config;

import java.util.Iterator;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 
public class WebConfig {
    
	

	private static Logger logger = LoggerFactory.getLogger(WebConfig.class);
	private static Properties props = null; 
	    
	static{
		String propsResource = "";
			
	    props = new Properties();
	    	
		try{
			propsResource = "config/webConfig.properties";
		    props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(propsResource));
		
		    StringBuffer logStr = new StringBuffer();
		    Iterator iter = props.keySet().iterator();
		    logger.info("====>>> {} Open!!", propsResource);
		    	
			while(iter.hasNext()){
				Object key = iter.next();
			    Object val = props.get(key);
			    logger.info("====>>> * {}  = {}", key, val);
			}
			    
			logger.debug("====>>> {} Close!!", propsResource);
		 }catch(Exception e){
			 logger.error("===>>>> Load {} Fail: {}", propsResource, e.toString());
		 }	
	}
		
	public static String getProperty(String property){
		return props.getProperty(property);
	}
	public static int getSystemRdbms(){
		return Integer.parseInt(props.getProperty("system.rdbms"));
	}
	
}