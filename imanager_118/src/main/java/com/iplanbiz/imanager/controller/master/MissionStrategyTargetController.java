package com.iplanbiz.imanager.controller.master;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.imanager.service.master.MissionStrategyTargetService;

@Controller
public class MissionStrategyTargetController {

	@Autowired
	MissionStrategyTargetService missionStrategyTargetService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 * 
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	
	/**
	 * 전략목표관리 기본 화면
	 * @return
	 */
	@RequestMapping(value="/master/missionStrategyTarget/crud", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView("master/missionStrategyTarget/crud");
		return modelAndView;
	}
	
	/**
	 * 전략목표관리 리스트 1
	 * @param response
	 * @param request
	 */
	@RequestMapping(value="/master/missionStrategyTarget/select1", method={RequestMethod.POST, RequestMethod.GET})
	public void select1( HttpServletResponse response, HttpServletRequest request
																						){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = missionStrategyTargetService.selectStrategyTarget1();
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			msg.setExceptionText(e.getMessage()); 
			logger.error("error : {}",e);
		}
		msg.send(response);
	}
	
  
	/**
	 * 전략목표관리 리스트 2
	 * @param response
	 * @param request
	 */
	@RequestMapping(value="/master/missionStrategyTarget/select2", method={RequestMethod.POST, RequestMethod.GET})
	public void select2( HttpServletResponse response, HttpServletRequest request,@RequestParam(required=false) String stramsid
																						){
		System.out.println("값 확인::::"+stramsid);
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = missionStrategyTargetService.selectStrategyTarget2(stramsid);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			msg.setExceptionText(e.getMessage()); 
			logger.error("error : {}",e);
		}
		msg.send(response);
	}
	
	/**
	 * 전략지표 신규 및 수정 - insert1
	 * @param response
	 * @param request
	 * @param msid		  미션/비전ID
	 * @param msNM       미션/비전명
	 * @param startYYYY  시작년도
	 * @param endYYYY  	  종료년도
	 * @param useYN      사용여부(Y,N)
	 */
	@RequestMapping(value="/master/missionStrategyTarget/insert1", method=RequestMethod.POST)
	public void insert1( HttpServletResponse response, HttpServletRequest request
								      ,@RequestParam(required=false) String msid
			                          ,@RequestParam(required=false) String msNM
			                          ,@RequestParam(required=false) String startYYYY
			                          ,@RequestParam(required=false) String endYYYY
			                          ,@RequestParam(required=false) String useYN
			                          ,@RequestParam(required=false) String msDEF) {
			                          
		AjaxMessage msg = new AjaxMessage();
		System.out.println("insert테스트: MSId: " +msid + " 미션/비젼명: " + msNM + " 시작: "+ startYYYY + " 종료: " + endYYYY + "사용여부: " + useYN + " 비고:" + msDEF); //인서트 값 찍기  
		try{ 
			missionStrategyTargetService.insertStrategtTarget1(msid, msNM,startYYYY, endYYYY, useYN, msDEF);
			msg.setSuccessText("저장 완료");
		}catch(Exception e){
			logger.error("error : {}",e); //체크
			msg.setExceptionText("입력에러 : " + e.getMessage());  
			//TEST
		}
		msg.send(response);
	}
	
	/**
	 * 전략지표 신규 및 수정 - insert2
	 * @param request
	 * @param straid     	전략목표ID
	 * @param straNmLabel   전략목표명
	 * @param straSubNm  	세부전략목표명
	 * @param combo			영역
	 * @param straUse    	사용여부(Y,N)
	 * @param straSort  	정렬순서
	 * @param straDEF      	비고
	 */
	@RequestMapping(value="/master/missionStrategyTarget/insert2", method=RequestMethod.POST)
	public void insert2( HttpServletResponse response, HttpServletRequest request
            					      ,@RequestParam(required=false) String stramsid
			                          ,@RequestParam(required=false) String straid
			                          ,@RequestParam(required=false) String stranm
			                          ,@RequestParam(required=false) String straSubNm
			                          ,@RequestParam(required=false) String straUse
			                          ,@RequestParam(required=false) String straSort 
			                          ,@RequestParam(required=false) String straDEF){ 
		 
		AjaxMessage msg = new AjaxMessage(); 
		try{ 
			missionStrategyTargetService.insertStrategtTarget2(stramsid, straid,stranm, straSubNm, straUse, straSort, straDEF); 
			msg.setSuccessText("저장 완료");
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText("입력에러 : " + e.getMessage());  
			//TEST
		}
		msg.send(response);
		}
	
	/**
	 * 전략목표관리 삭제 1
	 * @param response
	 * @param request 
	 * @param msid  미션/비전ID
	 */
	@RequestMapping(value="/master/missionStrategyTarget/remove1", method=RequestMethod.POST)
	public void remove( HttpServletResponse response, HttpServletRequest request
			                          ,@RequestParam(required=false) String msid){
		 
		AjaxMessage msg = new AjaxMessage();
		try{
			missionStrategyTargetService.removeStrategyTarget1(msid);
			msg.setSuccessText("삭제되었습니다");
		}catch(Exception e){
			logger.error("error : {}",e); 
			msg.setExceptionText("삭제 중 오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	/**
	 * 전략목표관리 삭제 전 사용여부 체크
	 * @param response
	 * @param request
	 * @param straid    전략목표ID 
	 */
	@RequestMapping(value="/master/missionStrategyTarget/check1", method={RequestMethod.POST, RequestMethod.GET})
	public void check( HttpServletResponse response, HttpServletRequest request
			                         ,@RequestParam(required=false) String msid){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = missionStrategyTargetService.checkStrategtTarget1(msid); //resultValue에 값 저장
			msg.setSuccessMessage(resultValue); // 값 넣어
		}catch(Exception e){
			logger.error("error : {}",e); 
			msg.setExceptionText("데이터 로드 중 에러 발생" + e.getMessage());
		}
		msg.send(response);
	}


	/**
	 * 전략목표관리 삭제 2
	 * @param response
	 * @param request 
	 * @param straid 전략목표ID
	 */
	@RequestMapping(value="/master/missionStrategyTarget/remove2", method=RequestMethod.POST)
	public void remove2( HttpServletResponse response, HttpServletRequest request
			,@RequestParam(required=false) String stramsid,@RequestParam(required=false) String straid){
		 
		AjaxMessage msg = new AjaxMessage();
		try{
			missionStrategyTargetService.removeStrategyTarget2(stramsid,straid);
			msg.setSuccessText("삭제되었습니다");
		}catch(Exception e){
			logger.error("error : {}",e); 
			msg.setExceptionText("삭제 중 오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	/**
	 * 전략목표관리 삭제 전 사용여부 체크
	 * @param response
	 * @param request
	 * @param straid    전략목표ID
	 */
	@RequestMapping(value="/master/missionStrategyTarget/check2", method={RequestMethod.POST, RequestMethod.GET})
	public void check2( HttpServletResponse response, HttpServletRequest request
			,@RequestParam(required=false) String ms_id
			                         ,@RequestParam(required=false) String straid){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = missionStrategyTargetService.checkStrategtTarget2(ms_id,straid); //resultValue에 값 저장
			msg.setSuccessMessage(resultValue); // 값 넣어
		}catch(Exception e){
			logger.error("error : {}",e); 
			msg.setExceptionText("데이터 로드 중 에러 발생" + e.getMessage());
		}
		msg.send(response);
	}


}