package com.iplanbiz.imanager.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.imanager.service.JobRunRusultService;

@Controller
public class JobRunResultController {
	@Autowired
	JobRunRusultService jobRunRusultService;
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax)
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	/**
	 * 작업내역조회 리스트 
	 * @return
	 */

	@RequestMapping(value="/command/jobRunResult/list", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView list(){
		ModelAndView modelAndView = new ModelAndView("command/jobRunResult/list");
		return modelAndView;
	}

}