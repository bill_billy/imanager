package com.iplanbiz.imanager.controller;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.config.WebConfig;
import com.iplanbiz.imanager.dto.KpiAttachFile;
import com.iplanbiz.imanager.service.KpiService;
import com.iplanbiz.imanager.service.KpiStatusService;
import com.iplanbiz.imanager.service.command.KpiAttachFileService;

@Controller
public class KpiStatusController {

	@Autowired
	KpiStatusService kpiStatusService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 insert를 prefix로 메소드를 추가 등록 합니다. ex) insertPerson
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	
	
	/*
	 * DWR 대용 메소드.
	*/
	@RequestMapping(value="/report/kpiStatus/callService", method={RequestMethod.POST})
	public void callService( HttpServletResponse response, HttpServletRequest request ){ 
		AjaxMessage msg = new AjaxMessage();
		try{
			Class<?> testClass = kpiStatusService.getClass();
            Object newObj = kpiStatusService;
            String dsmsg = request.getParameter("directServiceMsg");
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject)parser.parse(dsmsg);
            String method = json.get("method").toString();
            JSONArray parameters = (JSONArray)json.get("parameters");
            
            Class<?>[] arrParamClass = new Class<?>[parameters.size()];
            Object[] arrParam = new Object[parameters.size()];
            for(int i = 0; i < parameters.size();i++){
            	Object param = parameters.get(i);
            	arrParamClass[i] = param.getClass();
            	arrParam[i] = param;
            }            
 
            Method m = testClass.getDeclaredMethod(method,arrParamClass);
            
            Object returnValue = m.invoke(newObj,arrParam);
            if(returnValue instanceof JSONObject){
            	msg.setSuccessMessage((JSONObject)returnValue);
            }
            else if(returnValue instanceof JSONArray){
            	msg.setSuccessMessage((JSONArray)returnValue);
            }
            else{
            	msg.setSuccessText("메서드 호출에 성공 하였습니다.");
            } 
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText("메서드 호출 중에 오류가 발생 하였습니다. : "+e.getMessage());
		} 
		msg.send(response);
	}
	
	@RequestMapping(value="/report/KpiStatus/list", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView list ( HttpServletResponse response, HttpServletRequest request){
		ModelAndView modelAndView = new ModelAndView("/report/kpiStatus/list");
		return modelAndView;
	}
	//지표실적입력현황 엑셀 다운
	@RequestMapping(value="/report/KpiStatus/kpiResultList_ExcelDown", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView kpiResultList_ExcelDown (  @RequestParam(required=false) String pEvaGbn
            					   ,  @RequestParam(required=false) String evalG
            					   ,  @RequestParam(required=false) String yyyymm) throws Exception{
		ModelAndView modelAndView = new ModelAndView();	
		
		modelAndView.addObject("mainList", kpiStatusService.kpiResultList_ExcelDown(pEvaGbn ,evalG ,yyyymm));
		
		modelAndView.setViewName("/report/kpiStatus/kpiResultList_ExcelDown");
		return modelAndView;
	}
	//지표자체평가 엑셀다운
	@RequestMapping(value="/report/KpiStatus/kpiEvalList_ExcelDown", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView kpiEvalList_ExcelDown ( @RequestParam(required=false) String pEvaGbn
            					   ,  @RequestParam(required=false) String evalG
            					   ,  @RequestParam(required=false) String yyyymm) throws Exception{
		ModelAndView modelAndView = new ModelAndView();	
		
		modelAndView.addObject("mainList", kpiStatusService.kpiEvalList_ExcelDown(pEvaGbn ,evalG ,yyyymm));
		
		modelAndView.setViewName("/report/kpiStatus/kpiEvalList_ExcelDown");
		return modelAndView;
	}
	
}
