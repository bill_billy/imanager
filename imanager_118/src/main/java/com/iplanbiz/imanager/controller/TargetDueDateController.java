package com.iplanbiz.imanager.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.imanager.dto.TargetDueDate;
import com.iplanbiz.imanager.service.TargetDueDateService;;

@Controller
public class TargetDueDateController {

	@Autowired
	TargetDueDateService targetDueDateService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	
	/**
	 * 목표입력기한관리 화면
	 * @return
	 */
	@RequestMapping(value="/master/targetDueDate/crud", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView("master/targetDueDate/crud");
		return modelAndView;
	}
	
	/**
	 * 학년도 리스트
	 * @param evaGbn : 평가구분
	 */
	@RequestMapping(value="/master/targetDueDate/list", method={RequestMethod.POST, RequestMethod.GET})
	public void list( HttpServletResponse response, HttpServletRequest request
						, @RequestParam(required=false) String evaGbn){
		
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = targetDueDateService.getEvalYearList(evaGbn);
			
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			e.printStackTrace();
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	
	/**
	 * 상세정보
	 * @param evaGbn : 평가구분
	 * @param year : 학년도
	 * @param tgtSeq : 차수
	 */
	@RequestMapping(value="/master/targetDueDate/select", method={RequestMethod.POST, RequestMethod.GET})
	public void select( HttpServletResponse response, HttpServletRequest request
			, @RequestParam(required=false) String evaGbn
			, @RequestParam(required=false) String yyyy
			, @RequestParam(required=false) String tgtSeq){
		
		JSONArray resultArray = null;
		JSONObject resultObject = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultArray = targetDueDateService.getEvalDetail(evaGbn, yyyy, tgtSeq);
			if(resultArray.size()>0) {
				resultObject = (JSONObject)resultArray.get(0);
			}
			
			msg.setSuccessMessage(resultArray, resultObject);	
				
				
		}catch(Exception e){
			e.printStackTrace();
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}

	/**
	 * 저장
	 * @param targetDueDate
	 */
	@RequestMapping(value="/master/targetDueDate/save", method=RequestMethod.POST)
	public void save(HttpServletResponse response, HttpServletRequest requeset, @ModelAttribute TargetDueDate targetDueDate) {
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			
			resultValue = targetDueDateService.save(targetDueDate);
			
			if(resultValue == 1) {
				msg.setSuccessMessage(targetDueDate.getJSONObject());
				msg.setSuccessText("저장 되었습니다");
			} else {
				msg.setExceptionText("저장 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다.\r\n" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	
	/**
	 * 삭제
	 * @param targetDueDate
	 */
	@RequestMapping(value="/master/targetDueDate/remove", method=RequestMethod.POST)
	public void remove( HttpServletResponse response, HttpServletRequest request, @ModelAttribute TargetDueDate targetDueDate){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = targetDueDateService.remove(targetDueDate);
			if(resultValue == 1){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("삭제 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			e.printStackTrace();
			msg.setExceptionText("삭제 중 오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
}
