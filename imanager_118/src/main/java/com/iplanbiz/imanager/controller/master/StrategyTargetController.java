package com.iplanbiz.imanager.controller.master;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.imanager.service.StrategyTargetService;

@Controller
public class StrategyTargetController {

	@Autowired
	StrategyTargetService strategyTargetService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	/**
	 * 전략목표관리 기본 화면
	 * @return
	 */
	@RequestMapping(value="/master/strategyTarget/crud", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView("master/strategyTarget/crud");
		return modelAndView;
	}
	/**
	 * 전략목표관리 리스트
	 * @param response
	 * @param request
	 * @param searchyn      사용여부
	 * @param searchStranm  전략지표명
	 */
	@RequestMapping(value="/master/strategyTarget/select", method={RequestMethod.POST, RequestMethod.GET})
	public void select( HttpServletResponse response, HttpServletRequest request
			                          ,@RequestParam(required=false) String searchyn
			                          ,@RequestParam(required=false) String searchStranm){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = strategyTargetService.selectStrategyTarget(searchyn, searchStranm);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			msg.setExceptionText(e.getMessage());
			logger.error("error : {}",e);
		}
		msg.send(response);
	}
	/**
	 * 전략지표 신규 및 수정
	 * @param response
	 * @param request
	 * @param straid     전략목표ID
	 * @param stranm     전략목표명
	 * @param stradef    비고
	 * @param sortorder  정렬순서
	 * @param useyn      사용여부(Y,N)
	 */
	@RequestMapping(value="/master/strategyTarget/insert", method=RequestMethod.POST)
	public void insert( HttpServletResponse response, HttpServletRequest request
			                          ,@RequestParam(required=false) String straid
			                          ,@RequestParam(required=false) String straCD
			                          ,@RequestParam(required=false) String stranm
			                          ,@RequestParam(required=false) String stradef
			                          ,@RequestParam(required=false) String sortorder
			                          ,@RequestParam(required=false) String useyn){
		 
		AjaxMessage msg = new AjaxMessage();
		try{ 
			strategyTargetService.insertStrategtTarget(straid,straCD, stranm, stradef, useyn, sortorder);
			msg.setSuccessText("저장이 되었습니다");
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText("저장 중 오류가 발생했습니다 : " + e.getMessage());  
			//TEST
		}
		msg.send(response);
	}
	/**
	 * 전략목표관리 삭제
	 * @param response
	 * @param request
	 * @param straid  전략목표ID
	 */
	@RequestMapping(value="/master/strategyTarget/remove", method=RequestMethod.POST)
	public void remove( HttpServletResponse response, HttpServletRequest request
			                          ,@RequestParam(required=false) String straid){
		 
		AjaxMessage msg = new AjaxMessage();
		try{
			strategyTargetService.removeStrategyTarget(straid);
			msg.setSuccessText("삭제되었습니다");
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText("삭제 중 오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	/**
	 * 전략목표관리 삭제 전 사용여부 체크
	 * @param response
	 * @param request
	 * @param straid    전략목표ID
	 */
	@RequestMapping(value="/master/strategyTarget/check", method={RequestMethod.POST, RequestMethod.GET})
	public void check( HttpServletResponse response, HttpServletRequest request
			                         ,@RequestParam(required=false) String straid){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = strategyTargetService.checkStrategtTarget(straid);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText("데이터 로드 중 에러 발생" + e.getMessage());
		}
		msg.send(response);
	}
}
