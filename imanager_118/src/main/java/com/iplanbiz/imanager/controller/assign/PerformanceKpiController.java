package com.iplanbiz.imanager.controller.assign;

import java.lang.reflect.Method;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dto.AssignMent;
import com.iplanbiz.imanager.dto.AssignMentKpi;
import com.iplanbiz.imanager.service.assign.PerformanceKpiService;

@Controller
public class PerformanceKpiController {

	@Autowired
	PerformanceKpiService msKpiService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	
	
	
	@RequestMapping(value="/assign/performanceKpi/crud", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView crud(@RequestParam(required=false) String yyyy
			                 ,@RequestParam(required=false) String ms_id
			                 ,@RequestParam(required=false) String stra_id
			                 ,@RequestParam(required=false) String substra_id) throws Exception{
		ModelAndView modelAndView = new ModelAndView("/assign/performanceKpi/crud");
		
		try{
			modelAndView.addObject("yyyy", yyyy);
			modelAndView.addObject("ms_id", ms_id);
			modelAndView.addObject("stra_id", stra_id);
			modelAndView.addObject("substra_id", substra_id);
			
		}catch(Exception e){
			
			logger.error("error:{}",e);
			
			
		}
		return modelAndView;   
	}
	

	@RequestMapping(value="/assign/performanceKpi/insertKpiUser", method=RequestMethod.POST)
	public void insertUser(HttpServletResponse response, HttpServletRequest requeset
			 ,@RequestParam(required=false) String ms_id,@RequestParam(required=false) String yyyy,@RequestParam(required=false) String kpiid[],@RequestParam(required=false) String subkpiid[],
			 @RequestParam(required=false) Integer kpival[],@RequestParam(required=false) String confirmyn[],@RequestParam(required=false) String confconfirmyn[]){
		
		
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = msKpiService.insertKpiUser(ms_id,yyyy,kpiid,subkpiid,kpival,confirmyn,confconfirmyn);
			if(resultValue == 1){
				msg.setSuccessText("저장되었습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("수정되었습니다");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다.");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
}