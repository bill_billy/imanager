package com.iplanbiz.imanager.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;

@Controller
public class UserProgressController {

	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@RequestMapping(value="/system/progress/getProgressState", method={RequestMethod.POST})
	public void getProgressState(HttpServletResponse response, HttpServletRequest request,String programid){
		
		JSONArray jarr=null;
		Integer jobsize = null;
		String status = null;
		AjaxMessage msg = new AjaxMessage(); 
		try{		 
			
			//loginSessionInfoFactory.getObject().initProgress(programid,3); 
			//loginSessionInfoFactory.getObject().pushJob(programid,"2", "FILE 꺼내는중");
			//loginSessionInfoFactory.getObject().pushJob(programid,"1", "FILE 넣는중");
			//loginSessionInfoFactory.getObject().pushJob(programid,"Exception", "오류1 : 프로시져 맛감");
			//loginSessionInfoFactory.getObject().pushJob(programid,"3", "FILE 꺼내는gg중");
			//loginSessionInfoFactory.getObject().completeProgress(programid);
	 
			
			jarr = ((JSONArray)(loginSessionInfoFactory.getObject().getJobList(programid)));
			status = ((String)(loginSessionInfoFactory.getObject().getJobStatus(programid)));
			jobsize = ((Integer)(loginSessionInfoFactory.getObject().getJobSize(programid)));
			JSONObject returnMessage = new JSONObject(); 
			returnMessage.put("JOBSIZE" , jobsize);
			returnMessage.put("STATUS"	, status);
			returnMessage.put("STACK"	, jarr);
			msg.setSuccessMessage(returnMessage);
		}catch(Exception e){			
			logger.error("Exception:",e);
			msg.setExceptionText("프로세스 정보 로드 중 오류 발생.");
			
		}finally{
			msg.send(response);
		}
	}
	
	@RequestMapping(value="/system/progress/remove", method={RequestMethod.POST})
	public void remove(HttpServletResponse response, HttpServletRequest request,String programid){
		 
		AjaxMessage msg = new AjaxMessage(); 
		try{		 
			loginSessionInfoFactory.getObject().removeProgress(programid);
			//msg.setSuccessMessage(returnMessage);
		}catch(Exception e){			
			logger.error("Exception:",e);
			msg.setExceptionText("프로세스 정보 삭제 중 오류 발생.");
			
		}finally{
			msg.send(response);
		}
	}
	
	@RequestMapping(value="/system/progress/insert", method={RequestMethod.POST})
	public void insert(HttpServletResponse response, HttpServletRequest request,String programid, String jobid, String name){
		 
		AjaxMessage msg = new AjaxMessage(); 
		try{		 
			loginSessionInfoFactory.getObject().pushJob(programid, jobid, name);
			//msg.setSuccessMessage(returnMessage);
		}catch(Exception e){			
			logger.error("Exception:",e);
			msg.setExceptionText("프로세스 정보 삭제 중 오류 발생.");
			
		}finally{
			msg.send(response);
		}
	}
	
	@RequestMapping(value="/system/progress/create", method={RequestMethod.POST})
	public void create(HttpServletResponse response, HttpServletRequest request,String programid, String size){
		 
		AjaxMessage msg = new AjaxMessage(); 
		try{		 
			loginSessionInfoFactory.getObject().initProgress(programid,Integer.valueOf(size));
			//msg.setSuccessMessage(returnMessage);
		}catch(Exception e){			
			logger.error("Exception:",e);
			msg.setExceptionText("프로세스 생성 중 오류 발생.");
			
		}finally{
			msg.send(response);
		}
	}
	
	@RequestMapping(value="/system/progress/cancel", method={RequestMethod.POST})
	public void cancel(HttpServletResponse response, HttpServletRequest request,String programid){
		 
		AjaxMessage msg = new AjaxMessage(); 
		try{		 
			
			loginSessionInfoFactory.getObject().pushJob(programid, "CancelProgramException", "작업이 취소 되었습니다.");
			//msg.setSuccessMessage(returnMessage);
		}catch(Exception e){			
			logger.error("Exception:",e);
			msg.setExceptionText("프로세스 정보 삭제 중 오류 발생.");
			
		}finally{
			msg.send(response);
		}
	}

}
