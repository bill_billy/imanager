package com.iplanbiz.imanager.controller.master;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.imanager.service.master.MissionStrategyTargetService;
import com.iplanbiz.imanager.service.master.MissionSubStrategyTargetService;
import com.iplanbiz.imanager.service.StrategyTargetService;

@Controller
public class MissionSubStrategyTargetController {

	@Autowired
	MissionSubStrategyTargetService missionSubStrategyTargetService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 * 
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	
	/**
	 * 전략목표관리 기본 화면
	 * @return
	 */
	@RequestMapping(value="/master/missionSubStrategyTarget/crud", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView("master/missionSubStrategyTarget/crud");  
		return modelAndView;
	}
	
	/**
	 * 전략목표관리 리스트 1
	 * @param response
	 * @param request
	 */
	@RequestMapping(value="/master/missionSubStrategyTarget/select1", method={RequestMethod.POST, RequestMethod.GET})
	public void select1( HttpServletResponse response, HttpServletRequest request,@RequestParam(required=false) String msid
																						){
		System.out.println("값 확인::::"+msid); 
		JSONArray resultValue = null;  
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = missionSubStrategyTargetService.selectStrategyTarget1(msid);
			msg.setSuccessMessage(resultValue);
			System.out.println("값 확인2::::"+msid); 

		}catch(Exception e){ 
			msg.setExceptionText(e.getMessage()); 
			logger.error("error : {}",e);
		}
		msg.send(response);
	}
	
	/**
	 * 전략목표관리 리스트 2
	 * @param response
	 * @param request
	 */
	@RequestMapping(value="/master/missionSubStrategyTarget/select2", method={RequestMethod.POST, RequestMethod.GET})
	public void select2( HttpServletResponse response, HttpServletRequest request
										,@RequestParam(required=false) String submsId
										,@RequestParam(required=false) String substraId){
		JSONArray resultValue = null;   
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = missionSubStrategyTargetService.selectStrategyTarget2(submsId, substraId); 
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){ 
			msg.setExceptionText(e.getMessage());   
			logger.error("error : {}",e); 
		}
		msg.send(response);
	}
	/**
	 * 전략지표 신규 및 수정 - insert1
	 * @param response
	 * @param request
	 * @param msid		  미션/비전ID
	 * @param msNM       미션/비전명
	 * @param startYYYY  시작년도
	 * @param endYYYY  	  종료년도
	 * @param useYN      사용여부(Y,N)
	 */
	@RequestMapping(value="/master/missionSubStrategyTarget/insert", method=RequestMethod.POST)
	public void insert( HttpServletResponse response, HttpServletRequest request
		      						  ,@RequestParam(required=false) String submsId
		      						  ,@RequestParam(required=false) String substraId 
								      ,@RequestParam(required=false) String substId
			                          ,@RequestParam(required=false) String substCd 
			                          ,@RequestParam(required=false) String substNm1
			                          ,@RequestParam(required=false) String substNm2
			                          ,@RequestParam(required=false) String substUse
			                          ,@RequestParam(required=false) String substSort 
			                          ,@RequestParam(required=false) String substDef) {
			                          
		AjaxMessage msg = new AjaxMessage();
		try{  
			missionSubStrategyTargetService.insertStrategtTarget(submsId, substraId, substId, substCd, substNm1,substNm2, substUse, substSort, substDef);
			msg.setSuccessText("저장 완료");  
		}catch(Exception e){  
			logger.error("error : {}",e); //체크
			msg.setExceptionText("입력에러 : " + e.getMessage());  
			//TEST
		}
		msg.send(response);
	}
	
	/**
	 * 전략목표관리 삭제 1
	 * @param response
	 * @param request 
	 * @param msid  미션/비전ID
	 */
	@RequestMapping(value="/master/missionSubStrategyTarget/remove", method=RequestMethod.POST)
	public void remove( HttpServletResponse response, HttpServletRequest request 
									  ,@RequestParam(required=false) String submsId
									  ,@RequestParam(required=false) String substraId
			                          ,@RequestParam(required=false) String substId){ 

		 
		AjaxMessage msg = new AjaxMessage(); 
		try{
			missionSubStrategyTargetService.removeStrategyTarget(submsId, substraId, substId);
			msg.setSuccessText("삭제되었습니다");
		}catch(Exception e){
			logger.error("error : {}",e); 
			msg.setExceptionText("삭제 중 오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response); 
	}
	/**
	 * 전략목표관리 삭제 전 사용여부 체크
	 * @param response
	 * @param request
	 * @param straid    전략목표ID 
	 */
	@RequestMapping(value="/master/missionSubStrategyTarget/check", method={RequestMethod.POST, RequestMethod.GET})
	public void check( HttpServletResponse response, HttpServletRequest request
			,@RequestParam(required=false) String msId
			,@RequestParam(required=false) String straId
			,@RequestParam(required=false) String substId) {   
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = missionSubStrategyTargetService.checkStrategtTarget(msId,straId,substId); //resultValue에 값 저장
			msg.setSuccessMessage(resultValue); // 값 넣어
		}catch(Exception e){
			logger.error("error : {}",e);   
			msg.setExceptionText("데이터 로드 중 에러 발생" + e.getMessage());
		}
		msg.send(response);
	}
	
	
}