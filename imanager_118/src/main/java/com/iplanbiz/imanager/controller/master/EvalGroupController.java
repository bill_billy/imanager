package com.iplanbiz.imanager.controller.master;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.service.EvalGroupService;

@Controller
public class EvalGroupController {

	@Autowired
	EvalGroupService evalGroupService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	/**
	 * 평가조직 리스트 화면
	 * @return
	 */
	@RequestMapping(value="/master/evalGroup/crud", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView("master/evalGroup/crud");
		try{
			modelAndView.addObject("paramAcctid", loginSessionInfoFactory.getObject().getAcctID());
		}catch(Exception e){
			logger.error("error : {}",e);
		}
		return modelAndView;
	}
	/**
	 * 평가조직 화면 좌측 트리그리드 조회
	 * @param response
	 * @param request
	 */
	@RequestMapping(value="/master/evalGroup/select", method={RequestMethod.POST, RequestMethod.GET})
	public void select( HttpServletResponse response, HttpServletRequest request){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = evalGroupService.selectEvalGroupManager();
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	/**
	 * 평가조직관리 우측 그리드 조회
	 * @param response
	 * @param request
	 * @param searchscnm  조직명
	 * @param scid        
	 */
	@RequestMapping(value="/master/evalGroup/selectByConditional", method={RequestMethod.POST, RequestMethod.GET})
	public void selectByConditional( HttpServletResponse response, HttpServletRequest request
			                                ,@RequestParam(required=false) String searchscnm
			                                ,@RequestParam(required=false) String scid
			                                ,@RequestParam(required=false) String yyyy){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = evalGroupService.selectgridEvalGroupManager(searchscnm, scid, yyyy);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	/**
	 * 기간계 조직연결 Dept List 가져오는 로직
	 * @param response
	 * @param request
	 * @param scid     평가조직ID
	 */
	@RequestMapping(value="/master/evalGroup/getListDept", method={RequestMethod.POST, RequestMethod.GET})
	public void getListDept( HttpServletResponse response, HttpServletRequest request
			                                 ,@RequestParam(required=false) String scid){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = evalGroupService.deptlistEvalGroupManager(scid);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	/**
	 * 정보공시 학과연결 Sust List 가져오는 로직
	 * @param response
	 * @param request
	 * @param scid    평가조직ID
	 */
	@RequestMapping(value="/master/evalGroup/getListScid", method={RequestMethod.POST, RequestMethod.GET})
	public void getListScid( HttpServletResponse response, HttpServletRequest request
			                                 ,@RequestParam(required=false) String scid){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = evalGroupService.scidlistEvalGroupManager(scid);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	/**
	 * 평가조직관리 1Row 상세 조회
	 * @param response
	 * @param request
	 * @param scid     평가조직ID
	 */
	@RequestMapping(value="/master/evalGroup/selectDetail", method={RequestMethod.POST, RequestMethod.GET})
	public void selectDetail( HttpServletResponse response, HttpServletRequest request
			                                ,@RequestParam(required=false) String scid){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = evalGroupService.selectscidEvalGroupManager(scid);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	/**
	 * 평가조직 관리 신규 및 수정
	 * @param response
	 * @param request
	 * @param pscid      상위조직ID
	 * @param scid       조직ID
	 * @param scnm       조직명
	 * @param evalg      평가군
	 * @param scdesc     조직설명
	 * @param etcdesc    기타설명
	 * @param evagbn     평가구분
	 * @param scudc1     계열
	 * @param startdate  시작일
	 * @param enddate    종료일
	 * @param deptids    기간계조직연결
	 * @param sustids    정보공시학과연결
	 * @param sortorder  정렬순서
	 * @param filenm     파일경로
	 */
	@RequestMapping(value="/master/evalGroup/insert", method=RequestMethod.POST)
	public void insert( HttpServletResponse response, HttpServletRequest request
			                             ,@RequestParam(required=false) String pscid
			                             ,@RequestParam(required=false) String scid
			                             ,@RequestParam(required=false) String scnm
			                             ,@RequestParam(required=false) String evalg
			                             ,@RequestParam(required=false) String scdesc
			                             ,@RequestParam(required=false) String etcdesc
			                             ,@RequestParam(required=false) String evagbn
			                             ,@RequestParam(required=false) String scudc1
			                             ,@RequestParam(required=false) String startdate
			                             ,@RequestParam(required=false) String enddate
			                             ,@RequestParam(required=false) String deptids
			                             ,@RequestParam(required=false) String sustids
			                             ,@RequestParam(required=false) String sortorder
			                             ,@RequestParam(required=false) String pptefile){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = evalGroupService.insertEvalGroupManager(pscid, scid, scnm, evalg, scdesc, etcdesc, evagbn, scudc1, startdate, enddate, deptids, sustids, sortorder);
		}catch(Exception e){
			msg.setExceptionText(e.getMessage());
			logger.error("error : {}",e);
		}
		msg.send(response);
	}
	/**
	 * 평가조직관리 삭제
	 * @param response
	 * @param request
	 * @param scid     평가조직ID
	 */
	@RequestMapping(value="/master/evalGroup/remove", method=RequestMethod.POST)
	public void remove( HttpServletResponse response, HttpServletRequest request
			                            ,@RequestParam(required=false) String scid){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = evalGroupService.removeEvalGroupManager(scid);
			if(resultValue == 0){
				msg.setExceptionText("삭제 중 에러가 발생했습니다");
			}else if(resultValue == 1){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("삭제 중 에러가 발생했습니다");
			}
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	/**
	 * 정보공시학과연결 Popup 리스트
	 * @return
	 */
	@RequestMapping(value="/master/evalGroup/popupMappingscid", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popMappingscid(){
		ModelAndView modelAndView = new ModelAndView("master/evalGroup/popupMappingscid");
		return modelAndView;
	}
	/**
	 * 상위조직명 팝업 리스트
	 * @param response
	 * @param request
	 */
	@RequestMapping(value="/master/evalGroup/selectPopMappingscid", method={RequestMethod.POST, RequestMethod.GET})
	public void popMappingscidlist( HttpServletResponse response, HttpServletRequest request){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = evalGroupService.selectEvalGroupManagerPopupScid();
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	/**
	 * 상위조직명 팝업 리스트 
	 * @param response
	 * @param request
	 * @param searchscnm 조직명
	 */
	@RequestMapping(value="/master/evalGroup/selectByConditionalPopMappingscid", method={RequestMethod.POST, RequestMethod.GET})
	public void selectByConditionalPopMappingscid( HttpServletResponse response, HttpServletRequest request
	                                               ,@RequestParam(required=false) String searchscnm){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = evalGroupService.selectEvalGroupManagerPopscidGrid(searchscnm);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	/**
	 * 기간계 조직연결 팝업
	 * @return
	 */
	@RequestMapping(value="/master/evalGroup/popupMappingdept", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popMappingdept(){
		ModelAndView modelAndView = new ModelAndView("master/evalGroup/popupMappingdept");
		return modelAndView;
	}
	/**
	 * 기간계 조직연결 팝업 좌측 선택안된 기간계 조직 리스트
	 * @param response
	 * @param request
	 * @param scid     조직ID
	 * @param scnm     조직명
	 * @param minscid  기존 선택된 조직 ID
	 */
	@RequestMapping(value="/master/evalGroup/selectPopMappingdept", method={RequestMethod.POST, RequestMethod.GET})
	public void selectPopMappingdept( HttpServletResponse response, HttpServletRequest request
			                                       ,@RequestParam(required=false) String scid
			                                       ,@RequestParam(required=false) String scnm
			                                       ,@RequestParam(required=false) String minscid){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = evalGroupService.selectEvalGoupManagerPopupDeptGrid(scid, scnm, minscid);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	/**
	 * 기간계 조직연결 팝업 우측 선택된 기간계 조직 리스트
	 * @param response
	 * @param request
	 * @param scid    조직ID
	 */
	@RequestMapping(value="/master/evalGroup/choisePopMappingdept", method={RequestMethod.POST, RequestMethod.GET})
	public void choisePopMappingdept( HttpServletResponse response, HttpServletRequest request
			                                       ,@RequestParam(required=false) String scid){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = evalGroupService.selectEvalGroupManagerPopupSustGrid1(scid);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	/**
	 * 정보공시 학과연결 팝업
	 * @return
	 */
	@RequestMapping(value="/master/evalGroup/popupMappinggroup", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView evalGroupManagerPopMappingGroup(){
		ModelAndView modelAndView = new ModelAndView("master/evalGroup/popupMappinggroup");
		return modelAndView;
	}
	/**
	 * 정보공시 학과연결 좌측 정보공시 학과 리스트
	 * @param response
	 * @param request
	 * @param scid       조직ID
	 * @param searchscnm 조직명
	 */
	@RequestMapping(value="/master/evalGroup/selectPopMappinggoup", method={RequestMethod.POST, RequestMethod.GET})
	public void selectPopMappinggoup( HttpServletResponse response, HttpServletRequest request
			                                       ,@RequestParam(required=false) String scid
			                                       ,@RequestParam(required=false) String searchscnm){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = evalGroupService.selectEvalGroupManagerPopupSustGrid(scid, searchscnm);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	/**
	 * 정보공시 학과연결 우측 선택된 학과 리스트
	 * @param response
	 * @param request
	 * @param scid    조직ID
	 */
	@RequestMapping(value="/master/evalGroup/choicePopMappinggroup", method={RequestMethod.POST, RequestMethod.GET})
	public void choicePopMappinggroup( HttpServletResponse response, HttpServletRequest request
			                                       ,@RequestParam(required=false) String scid){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = evalGroupService.selectEvalGroupManagerPopupSustGrid1(scid);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/master/evalGroup/downloadFile", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView downloadFile ( HttpServletRequest request, HttpServletResponse response
			                           , @RequestParam(required=false) String fileid) throws Exception{
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("downloadFile", evalGroupService.getFileInfo(fileid));
		modelAndView.setViewName("downloadView");
		return modelAndView;
	}
}