package com.iplanbiz.imanager.controller.master;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.imanager.service.SubstrStrategyTargetService;

@Controller
public class SubstrStrategyTargetController {

	@Autowired
	SubstrStrategyTargetService substrStrategyTargetService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	/**
	 * 세부전략목표관리 기본화면
	 * @return
	 */
	@RequestMapping(value="/master/substrStrategyTarget/crud", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView("master/substrStrategyTarget/crud");
		return modelAndView;
	}
	/**
	 * 세부전략목표관리 좌측 전략목표조회
	 * @param response
	 * @param request
	 */
	@RequestMapping(value="/master/substrStrategyTarget/select", method={RequestMethod.POST, RequestMethod.GET})
	public void select( HttpServletResponse response, HttpServletRequest request){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = substrStrategyTargetService.selectSubstrStrategyTarget();
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			msg.setExceptionText(e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	/**
	 * 세부전략목표관리 신규 및 수정
	 * @param response
	 * @param request
	 * @param straid        전략목표ID
	 * @param substraid     세부전략목표ID
	 * @param substranm     세부전략목표명
	 * @param sortorder     정렬순서
	 * @param startyyyy     시작년도
	 * @param endyyyy       종료년도
	 * @param useyn         사용여부(Y,N)
	 * @param orgsubstraid  기존세부전략목표ID(업데이트 시 필요)
	 */
	@RequestMapping(value="/master/substrStrategyTarget/insert", method=RequestMethod.POST)
	public void insert( HttpServletResponse response, HttpServletRequest request											
											
			                                ,@RequestParam(required=false) String straid
			                                ,@RequestParam(required=false) String substraid
			                                ,@RequestParam(required=false) String substracd
			                                ,@RequestParam(required=false) String substranm
			                                ,@RequestParam(required=false) String sortorder
			                                ,@RequestParam(required=false) String startyyyy
			                                ,@RequestParam(required=false) String endyyyy
			                                ,@RequestParam(required=false) String useyn){
		
		AjaxMessage msg = new AjaxMessage();
		try{
			substrStrategyTargetService.insertSubstrStrategyTarget( straid, substraid,substracd, substranm, sortorder, useyn,startyyyy, endyyyy);
			msg.setSuccessText("저장 되었습니다");
		}catch(Exception e){
			msg.setExceptionText("저장 중 에러가 발생했습니다 : " + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	/**
	 * 세부전략목표 삭제
	 * @param response
	 * @param request
	 * @param straid     전략목표ID
	 * @param substraid  세부전략목표ID
	 */
	@RequestMapping(value="/master/substrStrategyTarget/remove", method=RequestMethod.POST)
	public void remove( HttpServletResponse response, HttpServletRequest request
			                                ,@RequestParam(required=false) String straid
			                                ,@RequestParam(required=false) String substraid){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = substrStrategyTargetService.removeSubstrStrategyTarget(straid, substraid);
			if(resultValue == 0){
				msg.setExceptionText("삭제 중 에러가 발생했습니다");
			}else if(resultValue == 1){
				msg.setSuccessText("삭제 되었습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("삭제 중 에러가 발생했습니다 : " + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	/**
	 * 세부전략목표 삭제 전 사용여부 체크
	 * @param response
	 * @param request
	 * @param straid     전략목표ID
	 * @param substrid   세부전략목표ID
	 */
	@RequestMapping(value="/master/substrStrategyTarget/check", method={RequestMethod.POST, RequestMethod.GET})
	public void checkSubstrStrategyTarget( HttpServletResponse response, HttpServletRequest request
			                               ,@RequestParam(required=false) String straid
			                               ,@RequestParam(required=false) String substrid){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = substrStrategyTargetService.checkSubstrStrategyTarget(straid, substrid);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			e.printStackTrace();
			msg.setExceptionText("데이터 로드 중 에러 발생 : " + e.getMessage());
		}
		msg.send(response);
	}
}
