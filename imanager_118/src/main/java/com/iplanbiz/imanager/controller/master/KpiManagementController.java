package com.iplanbiz.imanager.controller.master;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.imanager.service.master.KpiManagementService;

@Controller	
public class KpiManagementController {
		
		@Autowired
		KpiManagementService kpiManagementService; 
		private Logger logger = LoggerFactory.getLogger(getClass());
		
		@RequestMapping(value="/master/kpiManagement/crud", method={RequestMethod.POST, RequestMethod.GET}) 
		public ModelAndView crud(){ 
			ModelAndView modelAndView = new ModelAndView("/master/kpiManagement/crud");     
			return modelAndView;
		}
		@RequestMapping(value="/master/kpiManagement/insert", method=RequestMethod.POST)
		public void insert( HttpServletResponse response, HttpServletRequest request
				                     ,@RequestParam(required=false) String msid
									 ,@RequestParam(required=false) String kpiid
				                     ,@RequestParam(required=false) String kpinm
				                     ,@RequestParam(required=false) String kpigbn
				                     ,@RequestParam(required=false) String unit
				                     ,@RequestParam(required=false) String kpitype
				                     ,@RequestParam(required=false) String kpidir
				                     ,@RequestParam(required=false) String useyn
				                     ,@RequestParam(required=false) String kpidesc
				                     ,@RequestParam(required=false) String newsubkpiid
				                     ,@RequestParam(required=false) String subkpiid
				                     ,@RequestParam(required=false) String subkpinm
				                     ,@RequestParam(required=false) String weight
				                     ,@RequestParam(required=false) String sortorder
				                     ,@RequestParam(required=false) String ac
				                     ,@RequestParam(required=false) String newkpiid){
			int resultValue = 0;
			AjaxMessage msg = new AjaxMessage();
			try {
				
				resultValue = kpiManagementService.insertproject(msid, kpiid ,kpinm ,kpigbn , unit , kpitype , kpidir , useyn , kpidesc, subkpiid , subkpinm , weight , sortorder, ac, newkpiid , newsubkpiid);
				if(resultValue == 1){
					msg.setSuccessText("저장 되었습니다.");
				}else if(resultValue == 2){
					msg.setSuccessText("수정 되었습니다.");
				}else{
					msg.setExceptionText("저장 중 오류가 발생했습니다.");
				}
			} catch (Exception e) {
				msg.setExceptionText("저장 중 오류가 발생했습니다. : "+e.getMessage());
				logger.error("error : {}",e);
			}
			msg.send(response);
		}
		/**
		 * ê·¸ë£¹ì½”ë“œê´€ë¦¬ ì‚­ì œ
		 * @param response
		 * @param request
		 * @param orgcomlcod  ê¸°ì¡´ê·¸ë£¹ì½”ë“œ
		 */
		@RequestMapping(value="/master/kpiManagement/remove", method=RequestMethod.POST)
		public void remove(HttpServletResponse response, HttpServletRequest request,
				                     @RequestParam(required=false) String kpiid
				                     ,@RequestParam(required=false) String subkpiid
				                     ,@RequestParam(required=false) String msid){
			int resultValue = 0;
			AjaxMessage msg = new AjaxMessage();
			try{
				resultValue = kpiManagementService.deleteproject(kpiid,subkpiid,msid);
			}catch(Exception e){
				msg.setExceptionText("ì‚­ì œ ì¤‘ ì˜¤ë¥˜ê°€ ë°œìƒ�í–ˆìŠµë‹ˆë‹¤ : " + e.toString());
				logger.error("error : {}",e);
			}
			msg.send(response);
		}
		/**
		 * 성과지표 삭제 전 사용여부 체크
		 * @param response
		 * @param request
		 * @param straid    전략목표ID 
		 */
		@RequestMapping(value="/master/kpiManagement/check", method={RequestMethod.POST, RequestMethod.GET})
		public void check( HttpServletResponse response, HttpServletRequest request
				                         ,@RequestParam(required=false) String msid
				                         ,@RequestParam(required=false) String kpiid){
			JSONArray resultValue = null;
			AjaxMessage msg = new AjaxMessage();
			try{
				resultValue = kpiManagementService.checkStrategtTarget(msid,kpiid); //resultValue에 값 저장
				msg.setSuccessMessage(resultValue); // 값 넣어
			}catch(Exception e){
				logger.error("error : {}",e); 
				msg.setExceptionText("데이터 로드 중 에러 발생" + e.getMessage());
			}
			msg.send(response);
		}
		
}
