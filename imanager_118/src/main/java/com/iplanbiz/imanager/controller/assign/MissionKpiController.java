package com.iplanbiz.imanager.controller.assign;

import java.lang.reflect.Method;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dto.AssignMent;
import com.iplanbiz.imanager.dto.AssignMentKpi;
import com.iplanbiz.imanager.service.assign.MissionKpiService;

@Controller
public class MissionKpiController {

	@Autowired
	MissionKpiService msKpiService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;

	@RequestMapping(value="/assign/missionKpi/callService", method={RequestMethod.POST})
	public void callService( HttpServletResponse response, HttpServletRequest request ){ 
		AjaxMessage msg = new AjaxMessage();
		try{
			Class<?> testClass = msKpiService.getClass();
            Object newObj = msKpiService;
            String dsmsg = request.getParameter("directServiceMsg");
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject)parser.parse(dsmsg);
            String method = json.get("method").toString();
            JSONArray parameters = (JSONArray)json.get("parameters");
           
            Class<?>[] arrParamClass = new Class<?>[parameters.size()];
            
            Object[] arrParam = new Object[parameters.size()];
            for(int i = 0; i < parameters.size();i++){
            	Object param = parameters.get(i);  
            	arrParamClass[i] = param.getClass();
            	arrParam[i] = param;
            }            
 
            Method m = testClass.getDeclaredMethod(method,arrParamClass);
            
            Object returnValue = m.invoke(newObj,arrParam);
            if(returnValue instanceof JSONObject){
            	msg.setSuccessMessage((JSONObject)returnValue);
            }
            else if(returnValue instanceof JSONArray){
            	msg.setSuccessMessage((JSONArray)returnValue);
            }
            else{
            	msg.setSuccessText("ë©”ì„œë“œ í˜¸ì¶œì—� ì„±ê³µ í•˜ì˜€ìŠµë‹ˆë‹¤.");
            } 
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText("ë©”ì„œë“œ í˜¸ì¶œ ì¤‘ì—� ì˜¤ë¥˜ê°€ ë°œìƒ� í•˜ì˜€ìŠµë‹ˆë‹¤. : "+e.getMessage());
		} 
		msg.send(response);
	}
	
	
	
	@RequestMapping(value="/assign/missionKpi/crud", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView crud(@RequestParam(required=false) String yyyy
			                 ,@RequestParam(required=false) String ms_id
			                 ,@RequestParam(required=false) String stra_id
			                 ,@RequestParam(required=false) String substra_id) throws Exception{
		ModelAndView modelAndView = new ModelAndView("/assign/missionKpi/crud");
		
		try{
			modelAndView.addObject("yyyy", yyyy);
			modelAndView.addObject("ms_id", ms_id);
			modelAndView.addObject("stra_id", stra_id);
			modelAndView.addObject("substra_id", substra_id);
			
		}catch(Exception e){
			
			logger.error("error:{}",e);
			
			
		}
		return modelAndView;   
	}

	
	@RequestMapping(value="/assign/missionKpi/detail", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView detail( @RequestParam(required=false) String yyyy  
			                    ,@RequestParam(required=false) String ms_id
			                    ,@RequestParam(required=false,defaultValue="") String assign_id
			                    ,@RequestParam(required=false,defaultValue="") String s_yyyy
			                    ,@RequestParam(required=false,defaultValue="") String e_yyyy
			                    ){
		ModelAndView modelAndView = new ModelAndView("/assign/missionKpi/detail");
		
		try{
			modelAndView.addObject("SDAT", s_yyyy+"0101");
			modelAndView.addObject("EDAT", (Integer.parseInt(e_yyyy)+1)+"0228");
			
			if(!assign_id.equals("")){
				
				JSONObject assignment = (JSONObject) msKpiService.detailAssignMent(yyyy,ms_id,assign_id).get(0);
				
				String assignContent = assignment.get("ASSIGN_CONTENT").toString();
				if(!assignContent.equals("")) {
					assignContent = assignContent.replaceAll("\"", "&quot;").replaceAll("'", "&#39;");
					assignment.put("ASSIGN_CONTENT", assignContent);
				}
				
				modelAndView.addObject("detailAssignMent", assignment);
	
			}
			modelAndView.addObject("ms_id", ms_id);
			modelAndView.addObject("yyyy", yyyy);
		}catch(Exception e){
			
			logger.error("error:{}",e);
			
		}
		return modelAndView;
	}
	@RequestMapping(value="/assign/missionKpi/popupMappingUser", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingUser(@RequestParam(required=false) String idx, String pos1, String pos2, String pos3, String pos4){
		ModelAndView modelAndView = new ModelAndView("assign/missionKpi/popupMappingUser");
		modelAndView.addObject("idx", idx); 
		modelAndView.addObject("pos", pos1); 
		modelAndView.addObject("pos", pos2); 
		modelAndView.addObject("pos", pos3); 
		modelAndView.addObject("pos", pos4); 
		return modelAndView;    
	}
	@RequestMapping(value="/assign/missionKpi/popupMappingKpiStrategy", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingKpiStrategy(@RequestParam(required=false) String ms_id){
		ModelAndView modelAndView = new ModelAndView("/assign/missionKpi/popupMappingKpiStrategy");
		modelAndView.addObject("ms_id", ms_id);
		return modelAndView;
	}
	@RequestMapping(value="/assign/missionKpi/popupMappingDeptList", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingDeptList(@RequestParam(required=false) String type){
		ModelAndView modelAndView = new ModelAndView("/assign/missionKpi/popupMappingDeptList");
		modelAndView.addObject("type", type);
		return modelAndView;
	}
	
	@RequestMapping(value="/assign/missionKpi/insert", method=RequestMethod.POST)
	public void insert(HttpServletResponse response, HttpServletRequest requeset
			          ,@ModelAttribute AssignMent assign){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = msKpiService.insert(assign);
			if(resultValue == 1){
				msg.setSuccessText("저장되었습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("수정되었습니다");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다.");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다.: " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	
	
	@RequestMapping(value="/assign/missionKpi/remove", method=RequestMethod.POST)
	public void remove( HttpServletResponse response, HttpServletRequest request
			            ,@RequestParam(required=false) String ms_id
			            ,@RequestParam(required=false) String assign_id){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = msKpiService.remove(ms_id, assign_id);
			if(resultValue == 1){
				msg.setSuccessText("삭제되었습니다.");
			}else if(resultValue == 2) {
				msg.setExceptionText("WARNING");
			}else{
				msg.setExceptionText("삭제 중 오류가 발생했습니다.");
			}
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText("삭제 중 오류가 발생했습니다 : " + e.getMessage());
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/assign/missionKpi/insertKpi", method=RequestMethod.POST)
	public void insertKpi(HttpServletResponse response, HttpServletRequest requeset
			          ,@ModelAttribute AssignMentKpi kpi){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = msKpiService.insertKpi(kpi);
			if(resultValue == 1){
				msg.setSuccessText("저장되었습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("수정되었습니다");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다.");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	
	
	@RequestMapping(value="/assign/missionKpi/removeKpi", method=RequestMethod.POST)
	public void removeKpi( HttpServletResponse response, HttpServletRequest request
			            ,@RequestParam(required=false) String ms_id
			            ,@RequestParam(required=false) String assign_id
			            ,@RequestParam(required=false) String mt_id){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = msKpiService.removeKpi(ms_id, assign_id,mt_id);
			if(resultValue == 1){
				msg.setSuccessText("삭제되었습니다.");
			}else{
				msg.setExceptionText("삭제 중 오류가 발생했습니다.");
			}
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText("삭제 중 오류가 발생했습니다:" + e.getMessage());
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/assign/missionKpi/insertKpiUser", method=RequestMethod.POST)
	public void insertUser(HttpServletResponse response, HttpServletRequest requeset
			 ,@RequestParam(required=false) String ms_id,@RequestParam(required=false) String yyyy,@RequestParam(required=false) String kpiid[],@RequestParam(required=false) String subkpiid[],@RequestParam(required=false) String empno[]
			 ,@RequestParam(required=false) String empnm[],@RequestParam(required=false) String deptcd[],@RequestParam(required=false) String deptnm[],@RequestParam(required=false) String confempno[]
			 ,@RequestParam(required=false) String confempnm[],@RequestParam(required=false) String confdeptcd[],@RequestParam(required=false) String confdeptnm[]){
		
		
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = msKpiService.insertKpiUser(ms_id,yyyy,kpiid,subkpiid,empno,empnm,deptcd,deptnm,confempno,confempnm,confdeptcd,confdeptnm);
			if(resultValue == 1){
				msg.setSuccessText("저장되었습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("수정되었습니다");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다.");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/assign/missionKpi/insertkpiGoal", method=RequestMethod.POST)
	public void insertkpiGoal(HttpServletResponse response, HttpServletRequest requeset
			 ,@RequestParam(required=false) String AC,@RequestParam(required=false) String MS_ID,@RequestParam(required=false) String YYYY,@RequestParam(required=false) String YYYY3,@RequestParam(required=false) String KPI_ID,@RequestParam(required=false) String GOAL
			 ,@RequestParam(required=false) String SUB_KPI_ID,@RequestParam(required=false) String BASE){
		
		
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = msKpiService.insertkpiGoal(AC,MS_ID,YYYY,KPI_ID,SUB_KPI_ID,GOAL,BASE,YYYY3);
			if(resultValue == 1){
				msg.setSuccessText("저장되었습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("수정되었습니다");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다.");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/assign/missionKpi/removekpiGoal", method=RequestMethod.POST)
	public void removekpiGoal(HttpServletResponse response, HttpServletRequest requeset
			,@RequestParam(required=false) String MS_ID,@RequestParam(required=false) String YYYY,@RequestParam(required=false) String KPI_ID,@RequestParam(required=false) String SUB_KPI_ID){
		
		
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = msKpiService.removekpiGoal(MS_ID,YYYY,KPI_ID,SUB_KPI_ID);
			if(resultValue == 1){
				msg.setSuccessText("삭제되었습니다.");
			}else{
				msg.setExceptionText("삭제 중 오류가 발생했습니다.");
			}
		}catch(Exception e){
			msg.setExceptionText("삭제 중 오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	
}