package com.iplanbiz.imanager.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.imanager.dto.JobRun;
import com.iplanbiz.imanager.service.JobRunService;;

@Controller
public class JobRunController {

	@Autowired
	JobRunService jobRunService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	
	/**
	 * 작업실행관리 화면
	 * @return
	 */
	@RequestMapping(value="/command/jobRun/crud", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView("command/jobRun/crud");
		return modelAndView;
	}
	
	/**
	 * 작업 실행 내역 리스트
	 * @param evaGbn : 평가구분
	 * @param year : 학년도 
	 * @param gubun : 목표, 실적구분 
	 */
	@RequestMapping(value="/command/jobRun/select", method={RequestMethod.POST, RequestMethod.GET})
	public void list( HttpServletResponse response, HttpServletRequest request
						, @RequestParam(required=false) String evaGbn
						, @RequestParam(required=false) String year
						, @RequestParam(required=false) String gubun){
		
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = jobRunService.select(evaGbn, year, gubun);
			
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	
	/**
	 * 작업실행관리 > 개별저장
	 * @param jobRun
	 */
	@RequestMapping(value="/command/jobRun/save", method={RequestMethod.POST})
	public void save(HttpServletResponse response, HttpServletRequest request, @ModelAttribute JobRun jobRun) {
		
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			
			resultValue = jobRunService.save(jobRun);
			
			if(resultValue > 0) {
				msg.setSuccessText("저장 되었습니다.");
			} else {
				msg.setExceptionText("저장 중 오류가 발생했습니다.");
			}
			
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다.\r\n" + e.getMessage());
			logger.error("error : {}",e);
		}
		msg.send(response);
	}
	
	/**
	 * 작업실행관리 > 자료생성 및 점수계산
	 * @param jobRun
	 */
	@RequestMapping(value="/command/jobRun/executeJobRun", method={RequestMethod.POST})
	public void executeJobRun(HttpServletResponse response, HttpServletRequest request, @ModelAttribute JobRun jobRun) {
		
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			
			resultValue = jobRunService.executeJobRun(jobRun);
			
			if(resultValue > 0) {
				msg.setSuccessText("프로시저가 정상적으로 수행 되었습니다.");
			} else {
				msg.setExceptionText("프로시져 실행이 실패하였습니다.");
			}
		}catch(Exception e){
			msg.setExceptionText("프로시져 실행이 실패하였습니다.\r\n"+e.getMessage());
			logger.error("error : {}",e);
		}
		msg.send(response);
	}
}
