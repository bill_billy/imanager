package com.iplanbiz.imanager.controller.performance;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.imanager.service.performance.PerformanceClosingService;

@Controller	
public class PerformanceClosingController {
		
		@Autowired
		PerformanceClosingService performanceClosingService; 
		private Logger logger = LoggerFactory.getLogger(getClass());
		
		@RequestMapping(value="/performance/performanceClosing/crud", method=RequestMethod.GET) 
		public ModelAndView crud(){ 
			ModelAndView modelAndView = new ModelAndView("/performance/performanceClosing/crud");     
			return modelAndView;
		}
		
		
		@RequestMapping(value="/performance/performanceClosing/saveClosing", method=RequestMethod.POST) 
		public void saveClosing(HttpServletResponse response, HttpServletRequest requeset
				 ,@RequestParam(required=false) String ac,@RequestParam(required=false) String ms_id
				 ,@RequestParam(required=false) String yyyy,@RequestParam(required=false) String step) {
			AjaxMessage msg = new AjaxMessage();
			
			try {
				performanceClosingService.saveClosing(ac,ms_id,yyyy,step);  
				msg.setSuccessText("저장 되었습니다");
			}catch(Exception e) {
				msg.setExceptionText("저장 중 오류가 발생했습니다.: " + e.getMessage());
				logger.error("error:{}",e);
			}
			msg.send(response);
		}
		
		
		@RequestMapping(value="/performance/performanceClosing/calculateKpiVal", method=RequestMethod.POST) 
		public void calculate(HttpServletResponse response, HttpServletRequest requeset
				 ,@RequestParam(required=false) String MS_ID,@RequestParam(required=false) String YYYY) {
			AjaxMessage msg = new AjaxMessage();
			
			try {
				performanceClosingService.calculateKpiValue(MS_ID,YYYY);  
				msg.setSuccessText("집계 되었습니다");
			}catch(Exception e) {
				msg.setExceptionText("집계 중 오류가 발생했습니다.: " + e.getMessage());
				logger.error("error:{}",e);
			}
			msg.send(response);
		}
		
}
