package com.iplanbiz.imanager.controller.master;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dto.Kpi;
import com.iplanbiz.imanager.service.KpiService;

@Controller
public class KpiController {

	@Autowired
	KpiService kpiService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 insert를 prefix로 메소드를 추가 등록 합니다. ex) insertPerson
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	
	/*
	 * DWR 대용 메소드.
	*/
	@RequestMapping(value="/master/kpi/callService", method={RequestMethod.POST})
	public void callService( HttpServletResponse response, HttpServletRequest request ){ 
		AjaxMessage msg = new AjaxMessage();
		try{
			Class<?> testClass = kpiService.getClass();
            Object newObj = kpiService;
            String dsmsg = request.getParameter("directServiceMsg");
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject)parser.parse(dsmsg);
            String method = json.get("method").toString();
            JSONArray parameters = (JSONArray)json.get("parameters");
           
            Class<?>[] arrParamClass = new Class<?>[parameters.size()];
            
            Object[] arrParam = new Object[parameters.size()];
            for(int i = 0; i < parameters.size();i++){
            	Object param = parameters.get(i);  
            	arrParamClass[i] = param.getClass();
            	arrParam[i] = param;
            }            
 
            Method m = testClass.getDeclaredMethod(method,arrParamClass);
            
            Object returnValue = m.invoke(newObj,arrParam);
            if(returnValue instanceof JSONObject){
            	msg.setSuccessMessage((JSONObject)returnValue);
            }
            else if(returnValue instanceof JSONArray){
            	msg.setSuccessMessage((JSONArray)returnValue);
            }
            else{
            	msg.setSuccessText("메서드 호출에 성공 하였습니다.");
            } 
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText("메서드 호출 중에 오류가 발생 하였습니다. : "+e.getMessage());
		} 
		msg.send(response);
	}
	
	
	/**
	 * 지표관리 초기화면
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/master/kpi/crud", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView crud(@RequestParam(required=false, defaultValue="2012") String yyyy
			                 ,@RequestParam(required=false, defaultValue="100") String tabscid
			                 ,@RequestParam(required=false, defaultValue="0") String gubn) throws Exception{
		ModelAndView modelAndView = new ModelAndView("/master/kpi/crud");
		
		try{
			modelAndView.addObject("paramAcctid", loginSessionInfoFactory.getObject().getAcctID());
//			modelAndView.addObject("listStraid", kpiService.listStraid());
//			modelAndView.addObject("listSubstraid", kpiService.listSubstraid());
//			modelAndView.addObject("mappingKpiStrategy", kpiService.mappingKpiStrategy(yyyy, tabscid, gubn));
			modelAndView.addObject("mappingKpiUser", kpiService.getMappingKpiUser(yyyy, tabscid, gubn));
			modelAndView.addObject("mappingKpiWeight", kpiService.getMappingKpiWeight(yyyy, tabscid, gubn));
			
		}catch(Exception e){
			
			logger.error("error:{}",e);
			
			
		}
		return modelAndView;   
	}
	/**
	 * 지표관리 사용자 초기화면
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/master/kpi/crudByUser", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView crudByUser(@RequestParam(required=false, defaultValue="2012") String yyyy
			                 ,@RequestParam(required=false, defaultValue="100") String tabscid
			                 ,@RequestParam(required=false, defaultValue="0") String gubn) throws Exception{
		ModelAndView modelAndView = new ModelAndView("/master/kpi/crudByUser");
		
		try{
			modelAndView.addObject("paramAcctid", loginSessionInfoFactory.getObject().getAcctID());
			modelAndView.addObject("mappingKpiUser", kpiService.getMappingKpiAdminUser(yyyy, tabscid, gubn));
			modelAndView.addObject("mappingKpiWeight", kpiService.getMappingKpiAdminWeight(yyyy, tabscid, gubn));
			
		}catch(Exception e){
			
			logger.error("error:{}",e);
			
			
		}
		return modelAndView;   
	}
	@RequestMapping(value="/master/kpi/detail", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView detail( @RequestParam(required=false) String scid  
			                    ,@RequestParam(required=false) String mtid
			                    ,@RequestParam(required=false) String acctid
			                    ,@RequestParam(required=false) String searchyear
			                    ){
		ModelAndView modelAndView = new ModelAndView("/master/kpi/detail");
		
		try{
			//관점
			modelAndView.addObject("listPers", kpiService.listCode("PERS"));
			//측정주기
			modelAndView.addObject("listCycle", kpiService.listCode("CYCLE"));
			//평가방식
			modelAndView.addObject("listEvaltyp1", kpiService.listCode("EVAL_TYP1"));
			//단위
			modelAndView.addObject("listUnit", kpiService.listCode("USEUNIT"));
			//평가방법
			modelAndView.addObject("listEvatyp", kpiService.listCode("EVA_TYP"));
			//사용여부
			modelAndView.addObject("listMtudc3", kpiService.listCode("MT_ADD"));
			//지표구분
			modelAndView.addObject("listMtgbn", kpiService.listCode("MT_GBN"));
			//지표극성
			modelAndView.addObject("listMtdir", kpiService.listCode("MT_DIR"));
			
			//Main 쿼리
			if(!mtid.equals("")){
				modelAndView.addObject("detailKpi", kpiService.detailKpi(scid, mtid, searchyear).get(0));
				modelAndView.addObject("relatedWork", kpiService.getRelatedWork(mtid));
			}
			
		}catch(Exception e){
			
			logger.error("error:{}",e);
			
		}
		return modelAndView;
	}
	
	@RequestMapping(value="/master/kpi/detailOfUniv", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView detailOfUniv( @RequestParam(required=false) String scid  
			                    ,@RequestParam(required=false) String mtid
			                    ,@RequestParam(required=false) String acctid
			                    ,@RequestParam(required=false) String searchyear
			                    ){
		ModelAndView modelAndView = new ModelAndView("/master/kpi/detailOfUniv");
		
		try{
			//관점
			modelAndView.addObject("listPers", kpiService.listCode("PERS"));
			//측정주기
			modelAndView.addObject("listCycle", kpiService.listCode("CYCLE"));
			//평가방식
			modelAndView.addObject("listEvaltyp1", kpiService.listCode("EVAL_TYP1"));
			//단위
			modelAndView.addObject("listUnit", kpiService.listCode("USEUNIT"));
			//평가방법
			modelAndView.addObject("listEvatyp", kpiService.listCode("EVA_TYP"));
			//사용여부
			modelAndView.addObject("listMtudc3", kpiService.listCode("MT_ADD"));
			//지표구분
			modelAndView.addObject("listMtgbn", kpiService.listCode("MT_GBN"));
			//지표극성
			modelAndView.addObject("listMtdir", kpiService.listCode("MT_DIR"));

			//Main 쿼리
			if(!mtid.equals("")){
				modelAndView.addObject("detailKpi", kpiService.detailKpi(scid, mtid, searchyear).get(0));
				modelAndView.addObject("relatedWork", kpiService.getRelatedWork(mtid));
			}
			
		}catch(Exception e){
			
			logger.error("error:{}",e);
			
		}
		return modelAndView;
	}
	@RequestMapping(value="/master/kpi/detailOfUnivByUser", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView detailOfUnivByUser( @RequestParam(required=false) String scid  
			                    ,@RequestParam(required=false) String mtid
			                    ,@RequestParam(required=false) String acctid
			                    ,@RequestParam(required=false) String searchyear
			                    ){
		ModelAndView modelAndView = new ModelAndView("/master/kpi/detailOfUnivByUser");
		
		try{
			//관점
			modelAndView.addObject("listPers", kpiService.listCode("PERS"));
			//측정주기
			modelAndView.addObject("listCycle", kpiService.listCode("CYCLE"));
			//평가방식
			modelAndView.addObject("listEvaltyp1", kpiService.listCode("EVAL_TYP1"));
			//단위
			modelAndView.addObject("listUnit", kpiService.listCode("USEUNIT"));
			//평가방법
			modelAndView.addObject("listEvatyp", kpiService.listCode("EVA_TYP"));
			//사용여부
			modelAndView.addObject("listMtudc3", kpiService.listCode("MT_ADD"));
			//지표구분
			modelAndView.addObject("listMtgbn", kpiService.listCode("MT_GBN"));
			//지표극성
			modelAndView.addObject("listMtdir", kpiService.listCode("MT_DIR"));

			//Main 쿼리
			if(!mtid.equals("")){
				modelAndView.addObject("detailKpi", kpiService.detailKpiAdmin(scid, mtid, searchyear).get(0));
				modelAndView.addObject("relatedWork", kpiService.getRelatedWorkAdmin(mtid));
			}
			
		}catch(Exception e){
			
			logger.error("error:{}",e);
			
		}
		return modelAndView;
	}	
	@RequestMapping(value="/master/kpi/detailOfDept", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView detailOfDept( @RequestParam(required=false) String scid  
			                    ,@RequestParam(required=false) String mtid
			                    ,@RequestParam(required=false) String acctid
			                    ,@RequestParam(required=false) String searchyear
			                    ){
		ModelAndView modelAndView = new ModelAndView("/master/kpi/detailOfDept");
		
		try{
			//관점
			modelAndView.addObject("listPers", kpiService.listCode("PERS"));
			//측정주기
			modelAndView.addObject("listCycle", kpiService.listCode("CYCLE"));
			//평가방식
			modelAndView.addObject("listEvaltyp1", kpiService.listCode("EVAL_TYP1"));
			//단위
			modelAndView.addObject("listUnit", kpiService.listCode("USEUNIT"));
			//평가방법
			modelAndView.addObject("listEvatyp", kpiService.listCode("EVA_TYP"));
			//사용여부
			modelAndView.addObject("listMtudc3", kpiService.listCode("MT_ADD"));
			//지표구분
			modelAndView.addObject("listMtgbn", kpiService.listCode("MT_GBN"));			
			//지표극성
			modelAndView.addObject("listMtdir", kpiService.listCode("MT_DIR"));

			//Main 쿼리
			if(!mtid.equals("")){
				modelAndView.addObject("detailKpi", kpiService.detailKpi(scid, mtid, searchyear).get(0));
				modelAndView.addObject("relatedWork", kpiService.getRelatedWork(mtid));
			}
			
		}catch(Exception e){
			
			logger.error("error:{}",e);
			
		}
		return modelAndView;
	}
	@RequestMapping(value="/master/kpi/detailOfDeptByUser", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView detailOfDeptByUser( @RequestParam(required=false) String scid  
			                    ,@RequestParam(required=false) String mtid
			                    ,@RequestParam(required=false) String acctid
			                    ,@RequestParam(required=false) String searchyear
			                    ){
		ModelAndView modelAndView = new ModelAndView("/master/kpi/detailOfDeptByUser");
		
		try{
			//관점
			modelAndView.addObject("listPers", kpiService.listCode("PERS"));
			//측정주기
			modelAndView.addObject("listCycle", kpiService.listCode("CYCLE"));
			//평가방식
			modelAndView.addObject("listEvaltyp1", kpiService.listCode("EVAL_TYP1"));
			//단위
			modelAndView.addObject("listUnit", kpiService.listCode("USEUNIT"));
			//평가방법
			modelAndView.addObject("listEvatyp", kpiService.listCode("EVA_TYP"));
			//사용여부
			modelAndView.addObject("listMtudc3", kpiService.listCode("MT_ADD"));
			//지표구분
			modelAndView.addObject("listMtgbn", kpiService.listCode("MT_GBN"));			
			//지표극성
			modelAndView.addObject("listMtdir", kpiService.listCode("MT_DIR"));

			//Main 쿼리
			if(!mtid.equals("")){
				modelAndView.addObject("detailKpi", kpiService.detailKpiAdmin(scid, mtid, searchyear).get(0));
				modelAndView.addObject("relatedWork", kpiService.getRelatedWorkAdmin(mtid));
			}
			
		}catch(Exception e){
			
			logger.error("error:{}",e);
			
		}
		return modelAndView;
	}
	@RequestMapping(value="/master/kpi/select", method={RequestMethod.POST, RequestMethod.GET})
	public void select ( HttpServletResponse response, HttpServletRequest request,
						 @RequestParam(required=false) String searchyear){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiService.getDeptList(searchyear);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/master/kpi/selectKpi", method={RequestMethod.POST, RequestMethod.GET})
	public void selectKpi (HttpServletResponse response, HttpServletRequest requeset,
			               @RequestParam(required=false) String searchyear
			               ,@RequestParam(required=false) String searchnm){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiService.getKpiList(searchyear, searchnm);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/master/kpi/selectKpiAdmin", method={RequestMethod.POST, RequestMethod.GET})
	public void selectKpiAdmin (HttpServletResponse response, HttpServletRequest requeset,
			               @RequestParam(required=false) String searchyear
			               ,@RequestParam(required=false) String searchnm){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiService.getKpiListAdmin(searchyear, searchnm);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}	
	@RequestMapping(value="/master/kpi/selectKpilist", method={RequestMethod.POST, RequestMethod.GET})
	public void selectKpilist( HttpServletResponse response, HttpServletRequest request,
			                   @RequestParam(required=false) String gubn,
			                   @RequestParam(required=false) String scid,
			                   @RequestParam(required=false) String yyyy,
			                   @RequestParam(required=false) String tgtusernm,
			                   @RequestParam(required=false) String tgtchangenm,
			                   @RequestParam(required=false) String kpichangenm){ 
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{  
			resultValue = kpiService.getKpiList(gubn, scid, yyyy, tgtusernm, tgtchangenm, kpichangenm);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/master/kpi/selectKpilistAdmin", method={RequestMethod.POST, RequestMethod.GET})
	public void selectKpilistAdmin( HttpServletResponse response, HttpServletRequest request,
			                   @RequestParam(required=false) String gubn,
			                   @RequestParam(required=false) String scid,
			                   @RequestParam(required=false) String yyyy){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiService.getKpiListAdmin(gubn, scid, yyyy);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}	
	@RequestMapping(value="/master/kpi/selectKpilistTargetAdmin", method={RequestMethod.POST, RequestMethod.GET})
	public void selectKpilistTargetAdmin( HttpServletResponse response, HttpServletRequest request,
			                   @RequestParam(required=false) String gubn,
			                   @RequestParam(required=false) String scid,
			                   @RequestParam(required=false) String yyyy,
			                   @RequestParam(required=false) String tgtusernm,
			                   @RequestParam(required=false) String tgtchangenm,
			                   @RequestParam(required=false) String kpichangenm){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiService.getKpiListTargetAdmin(gubn, scid, yyyy, tgtusernm, tgtchangenm, kpichangenm);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}	
	@RequestMapping(value="/master/kpi/popupMappingParentKpi", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingParentKpi(@RequestParam(required=false) String mtid){
		ModelAndView modelAndView = new ModelAndView("/master/kpi/popupMappingParentKpi");		 
		return modelAndView;
	}
	@RequestMapping(value="/master/kpi/popupMappingParentKpiByUser", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingParentKpiByUser(@RequestParam(required=false) String mtid){
		ModelAndView modelAndView = new ModelAndView("/master/kpi/popupMappingParentKpiByUser");		 
		return modelAndView;
	}
	@RequestMapping(value="/master/kpi/popupMappingKpiFormula", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingKpiFormula(@RequestParam(required=false) String mtid){
		ModelAndView modelAndView = new ModelAndView("/master/kpi/popupMappingKpiFormula");		 
		return modelAndView;
	}
	@RequestMapping(value="/master/kpi/popupMappingKpiFormulaByUser", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingKpiFormulaByUser(@RequestParam(required=false) String mtid){
		ModelAndView modelAndView = new ModelAndView("/master/kpi/popupMappingKpiFormulaByUser");		 
		return modelAndView;
	}
	@RequestMapping(value="/master/kpi/popupMappingKpiStrategy", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingKpiStrategy(){
		ModelAndView modelAndView = new ModelAndView("/master/kpi/popupMappingKpiStrategy");
		return modelAndView;
	}
	@RequestMapping(value="/master/kpi/popupMappingKpiStrategyByUser", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingKpiStrategyByUser(){
		ModelAndView modelAndView = new ModelAndView("/master/kpi/popupMappingKpiStrategyByUser");
		return modelAndView;
	}
	@RequestMapping(value="/master/kpi/popupMappingKpiDept", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingKpiDept(){
		ModelAndView modelAndView = new ModelAndView("/master/kpi/popupMappingKpiDept");
		return modelAndView;
	}
	@RequestMapping(value="/master/kpi/popupMappingKpiDeptByUser", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingKpiDeptByUser(){
		ModelAndView modelAndView = new ModelAndView("/master/kpi/popupMappingKpiDeptByUser");
		return modelAndView;
	}
	@RequestMapping(value="/master/kpi/popupMappingKpiSust", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingKpiSust(){
		ModelAndView modelAndView = new ModelAndView("/master/kpi/popupMappingKpiSust");
		return modelAndView;
	}
	@RequestMapping(value="/master/kpi/popupMappingKpiUser", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingKpiUser(){
		ModelAndView modelAndView = new ModelAndView("/master/kpi/popupMappingKpiUser");
		return modelAndView;
	}
	@RequestMapping(value="/master/kpi/popupKpiDetail", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupKpiDetail(@RequestParam(required=false, defaultValue="") String useyn
			                           ,@RequestParam(required=false, defaultValue="") String mtattr
			                           ,@RequestParam(required=false, defaultValue="") String searchmtnm
			                           ,@RequestParam(required=false, defaultValue=" ") String mtid){
		ModelAndView modelAndView = new ModelAndView("/master/kpi/popupKpiDetail");
		
		try{
			modelAndView.addObject("useCodeList"     , kpiService.listCode("USE"));
			modelAndView.addObject("mtattrCodeList"  , kpiService.listCode("MT_ATTR"));
			JSONObject defaultUse = (JSONObject) kpiService.listCode("USE").get(0);
			JSONObject detaultMtattr = (JSONObject) kpiService.listCode("MT_ATTR").get(0);
			String defaultUseyn = "";
			String defaultMtattr = "";
			if("".equals(useyn)){
				defaultUseyn = defaultUse.get("COM_COD").toString();
			}else{
				defaultUseyn = useyn;
			}
			if("".equals(mtattr)){
				defaultMtattr = detaultMtattr.get("COM_COD").toString();
			}else{
				defaultMtattr = mtattr;
			}
			modelAndView.addObject("mtList"			 , kpiService.popupKpiDetailMtList(defaultUseyn, defaultMtattr, searchmtnm));
			modelAndView.addObject("mainList"		 , kpiService.popupKpiDetailMainList(mtid));
			
		}catch(Exception e){
			
			logger.error("error:{}",e);
			
		}
		return modelAndView;
	}
	@RequestMapping(value="/master/kpi/popupKpiDetail_141", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupKpiDetail_141(@RequestParam(required=false, defaultValue="") String pYyyy
			                           ,@RequestParam(required=false, defaultValue="") String useyn
			                           ,@RequestParam(required=false, defaultValue="") String mtattr
			                           ,@RequestParam(required=false, defaultValue="") String searchmtnm
			                           ,@RequestParam(required=false, defaultValue=" ") String mtid){
		ModelAndView modelAndView = new ModelAndView("/master/kpi/popupKpiDetail_141");
		 
		try{
			modelAndView.addObject("useCodeList"     , kpiService.listCode("USE"));
			modelAndView.addObject("mtattrCodeList"  , kpiService.listCode("MT_ATTR"));
			JSONObject defaultUse = (JSONObject) kpiService.listCode("USE").get(0);
			JSONObject detaultMtattr = (JSONObject) kpiService.listCode("MT_ATTR").get(0);
			String defaultUseyn = "";
			String defaultMtattr = "";
			if("".equals(useyn)){
				defaultUseyn = defaultUse.get("COM_COD").toString();
			}else{
				defaultUseyn = useyn;
			}
			if("".equals(mtattr)){
				defaultMtattr = detaultMtattr.get("COM_COD").toString();
			}else{
				defaultMtattr = mtattr;
			}
			modelAndView.addObject("mtList"			 , kpiService.popupKpiDetailMtList_141(defaultUseyn, defaultMtattr, searchmtnm));
			modelAndView.addObject("mainList"		 , kpiService.popupKpiDetailMainList_141(mtid, pYyyy));
			
		}catch(Exception e){
			
			logger.error("error:{}",e);
			
		}
		return modelAndView;
	}
	//연계항목 마스터
	@RequestMapping(value="/master/kpi/getIfCod", method=RequestMethod.POST)
	public void getIfCod(HttpServletResponse response, HttpServletRequest requeset){ 
		AjaxMessage msg = new AjaxMessage();
		try{
			msg.setSuccessMessage(kpiService.listIfcod());
			msg.setSuccessText("정상 조회 되었습니다.");
			 
		}catch(Exception e){ 
			msg.setExceptionText("조회 중 오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	//연계항목 마스터(관리자)
	@RequestMapping(value="/master/kpi/getIfCodAdmin", method=RequestMethod.POST)
	public void getIfCodAdmin(HttpServletResponse response, HttpServletRequest requeset){ 
		AjaxMessage msg = new AjaxMessage();
		try{
			msg.setSuccessMessage(kpiService.listIfcodAdmin());
			msg.setSuccessText("정상 조회 되었습니다.");
			 
		}catch(Exception e){ 
			msg.setExceptionText("조회 중 오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	//지표별 년도별 산식 조회
	@RequestMapping(value="/master/kpi/getCalculateListByMtID", method=RequestMethod.POST)
	public void getCalculateListByMtID(HttpServletResponse response, HttpServletRequest requeset, String mtid, String endyyyy){ 
		AjaxMessage msg = new AjaxMessage();
		try{
			msg.setSuccessMessage(kpiService.getCalculateListByMtID(mtid, endyyyy));
			msg.setSuccessText("정상 조회 되었습니다.");
			 
		}catch(Exception e){ 
			msg.setExceptionText("조회 중 오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	//지표별 년도별 산식 조회(관리자)
	@RequestMapping(value="/master/kpi/getCalculateListByMtIDAdmin", method=RequestMethod.POST)
	public void getCalculateListByMtIDAdmin(HttpServletResponse response, HttpServletRequest requeset, String mtid, String endyyyy){ 
		AjaxMessage msg = new AjaxMessage();
		try{
			msg.setSuccessMessage(kpiService.getCalculateListByMtIDAdmin(mtid, endyyyy));
			msg.setSuccessText("정상 조회 되었습니다.");
			 
		}catch(Exception e){ 
			msg.setExceptionText("조회 중 오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	@RequestMapping(value="/master/kpi/insert", method=RequestMethod.POST)
	public void insert(HttpServletResponse response, HttpServletRequest requeset
			          ,@ModelAttribute Kpi kpi){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiService.insert(kpi);
			if(resultValue == 1){
				msg.setSuccessText("저장 되었습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	
	/**
	 * 가치 형성 회계 설정
	 * @param response
	 * @param requeset
	 * @param univKpiID
	 * @param scIdList
	 * @param sustIdList
	 * @param currentAmt
	 */
	@RequestMapping(value="/master/kpi/insertValueWeight", method=RequestMethod.POST)
	public void insertValueWeight(HttpServletResponse response, HttpServletRequest requeset
								  ,@RequestParam(required=false) String   yyyy					//년도
								  ,@RequestParam(required=false) String   univMtID				//지표 ID
								  ,@RequestParam(required=false) float    univMtWeight			//지표 가중치
								  ,@RequestParam(required=false) String[] scIDList				//부서SC_ID
								  ,@RequestParam(required=false) String[] targetSustList		//학과SC_ID JSONString
								  ,@RequestParam(required=false) float[] kpiCurrentAmtList		//가치형성비율
								  )
	{ 
		logger.info("scID.length:{}",scIDList.length);
		logger.debug("scID.length:{}",scIDList.length);
		System.out.println("scID.length:"+scIDList.length);
		int resultValue = 0; 
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiService.insertValueWeight(yyyy,univMtID,univMtWeight, scIDList, targetSustList,kpiCurrentAmtList);
			if(resultValue == 1){
				msg.setSuccessText("저장 되었습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	
	
	@RequestMapping(value="/master/kpi/remove", method=RequestMethod.POST)
	public void remove( HttpServletResponse response, HttpServletRequest request
			            ,@RequestParam(required=false) String mtid
			            ,@RequestParam(required=false) String yyyy){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiService.remove(mtid, yyyy);
			if(resultValue == 1){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("삭제 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText("삭제 중 오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/master/kpi/insertTarget", method=RequestMethod.POST)
	public void insertTarget( HttpServletResponse response, HttpServletRequest request
			                  ,@RequestParam(required=false) String targetevagbn
			                  ,@RequestParam(required=false) String targetyyyy
			                  ,@RequestParam(required=false) String targetorgyyyy
			                  ,@RequestParam(required=false) String targetkpiid
			                  ,@RequestParam(required=false) String targetamt
			                  ,@RequestParam(required=false) String targetcurramt){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiService.insertTarget(targetevagbn, targetyyyy, targetorgyyyy, targetkpiid, targetamt, targetcurramt);
			if(resultValue == 1){
				msg.setSuccessText("저장 되었습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다." + e.getMessage());
			
			logger.error("error:{}",e);
		}   
		msg.send(response);
	}
	@RequestMapping(value="/master/kpi/insertTargetAdmin", method=RequestMethod.POST)
	public void insertTargetAdmin( HttpServletResponse response, HttpServletRequest request
			                  ,@RequestParam(required=false) String targetevagbn
			                  ,@RequestParam(required=false) String targetyyyy
			                  ,@RequestParam(required=false) String targetorgyyyy
			                  ,@RequestParam(required=false) String targetkpiid
			                  ,@RequestParam(required=false) String targetamt
			                  ,@RequestParam(required=false) String targetcurramt){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiService.insertTargetAdmin(targetevagbn, targetyyyy, targetorgyyyy, targetkpiid, targetamt, targetcurramt);
			if(resultValue == 1){
				msg.setSuccessText("저장 되었습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다." + e.getMessage());
			
			logger.error("error:{}",e);
		}   
		msg.send(response);
	}
	@RequestMapping(value="/master/kpi/removeTarget", method=RequestMethod.POST)
	public void removeTarget( HttpServletResponse response, HttpServletRequest request
			                  ,@RequestParam(required=false) String targetevagbn
			                  ,@RequestParam(required=false) String targetorgyyyy
			                  ,@RequestParam(required=false) String targetkpiid){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiService.removeTarget(targetevagbn, targetorgyyyy, targetkpiid);
			if(resultValue == 1){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("삭제 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("삭제 중 오류가 발생했습니다"+e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	@RequestMapping(value="/master/kpi/removeTargetAdmin", method=RequestMethod.POST)
	public void removeTargetAdmin( HttpServletResponse response, HttpServletRequest request
			                  ,@RequestParam(required=false) String targetevagbn
			                  ,@RequestParam(required=false) String targetorgyyyy
			                  ,@RequestParam(required=false) String targetkpiid){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiService.removeTargetAdmin(targetevagbn, targetorgyyyy, targetkpiid);
			if(resultValue == 1){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("삭제 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("삭제 중 오류가 발생했습니다"+e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	@RequestMapping(value="/master/kpi/insertKpiuser", method=RequestMethod.POST)
	public void insertKpiuser( HttpServletResponse response, HttpServletRequest request
			                   ,@RequestParam(required=false) String[] userkpiid
			                   ,@RequestParam(required=false) String[] userscid
			                   ,@RequestParam(required=false) String[] owneruserid
			                   ,@RequestParam(required=false) String[] kpichargeid
			                   ,@RequestParam(required=false) String[] tgtuserid
			                   ,@RequestParam(required=false) String[] tgtchargeid
			                   ,@RequestParam(required=false) String[] empid
			                   ,@RequestParam(required=false) String yyyyKpiUser
			                   ,@RequestParam(required=false) String evagbnKpiUser){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiService.insertKpiuser(tgtuserid, tgtchargeid, owneruserid, kpichargeid, userkpiid, userscid, empid, yyyyKpiUser, evagbnKpiUser);
			if(resultValue == 1){
				msg.setSuccessText("저장 되었습니다");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다" + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	@RequestMapping(value="/master/kpi/insertKpiweight", method=RequestMethod.POST)
	public void insertKpiweight( HttpServletResponse response, HttpServletRequest request
								 ,@RequestParam(required=false) String weightyyyy
			                     ,@RequestParam(required=false) String[] weightscid
			                     ,@RequestParam(required=false) String[] weightmtid
			                     ,@RequestParam(required=false) String[] weightkpiid
			                     ,@RequestParam(required=false) String[] kpiweight
			                     ,@RequestParam(required=false) String[] kpicurramt){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiService.insertKpiweight(weightkpiid, weightscid, weightmtid, weightyyyy, kpicurramt, kpiweight);
			if(resultValue == 1){
				msg.setSuccessText("저장 되었습니다");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다" + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	@RequestMapping(value="/master/kpi/insertResultColValue", method=RequestMethod.POST)
	public void insertResultColValue ( HttpServletResponse response, HttpServletRequest request
			                           ,@ModelAttribute Kpi kpi){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiService.insertKpiResultColValue(kpi);
			msg.setSuccessText("저장 되었습니다");
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/master/kpi/insertResultColValueAdmin", method=RequestMethod.POST)
	public void insertResultColValueAdmin ( HttpServletResponse response, HttpServletRequest request
			                           ,@ModelAttribute Kpi kpi){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			if(kpi.getSaveGubn().equals("Kpivalue")){
				kpiService.insertResultColValueAdmin(kpi);  
			}else if(kpi.getSaveGubn().equals("One")){
				kpiService.kpiResultInsertOne(kpi); 
			}else if(kpi.getSaveGubn().equals("All")){
				kpiService.kpiResultInsertAll(kpi); 
			}
			msg.setSuccessText("저장 되었습니다");
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}

	//담당자평가(저장 OR 제출)
	@RequestMapping(value="/master/kpi/insertGradeValue", method=RequestMethod.POST)
	public void insertGradeValue (HttpServletResponse response
									 ,@RequestParam(required=false) String[] yyyy
									 ,@RequestParam(required=false) String[] mm
						             ,@RequestParam(required=false) String[] evaGbn
						             ,@RequestParam(required=false) String[] kpiId
						             ,@RequestParam(required=false) String[] scId
						             ,@RequestParam(required=false) String[] mtId
						             ,@RequestParam(required=false) String[] grade
						             ,@RequestParam(required=false) String[] gradeDesc
						             ,@RequestParam(required=false) String[] status
						             ,@RequestParam(required=false) String[] ipvDesc){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
				resultValue = kpiService.insertGradeValue(yyyy, mm, evaGbn, kpiId, scId, mtId, grade, gradeDesc, status, ipvDesc);
			msg.setSuccessText("저장 되었습니다");
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	@RequestMapping(value="/master/kpi/insertLastGradeValue", method=RequestMethod.POST)
	public void insertLastGradeValue (HttpServletResponse response
									 ,@RequestParam(required=false) String[] yyyy
									 ,@RequestParam(required=false) String[] mm
						             ,@RequestParam(required=false) String[] evaGbn
						             ,@RequestParam(required=false) String[] kpiId
						             ,@RequestParam(required=false) String[] scId
						             ,@RequestParam(required=false) String[] mtId
						             ,@RequestParam(required=false) String[] grade
						             ,@RequestParam(required=false) String[] gradeDesc
						             ,@RequestParam(required=false) String[] status
						             ,@RequestParam(required=false) String[] ipvDesc){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
				resultValue = kpiService.insertLastGradeValue(yyyy, mm, evaGbn, kpiId, scId, mtId, grade, gradeDesc, status, ipvDesc);
			msg.setSuccessText("저장 되었습니다");
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}	
	//지표자체평가:제출취소
	@RequestMapping(value="/master/kpi/updateGradeValue", method=RequestMethod.POST)
	public void updateGradeValue (HttpServletResponse response,
									  @RequestParam(required=false) String[] yyyy
									 ,@RequestParam(required=false) String[] mm
						             ,@RequestParam(required=false) String[] evaGbn
						             ,@RequestParam(required=false) String[] kpiId
						             ,@RequestParam(required=false) String[] status){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{				
			resultValue = kpiService.updateGradeValue(yyyy, mm, evaGbn, kpiId, status);
			msg.setSuccessText("저장 되었습니다");
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	//지표자체평가 최종확정취소
	@RequestMapping(value="/master/kpi/updateLastGradeValue", method=RequestMethod.POST)
	public void updateLastGradeValue (HttpServletResponse response,
									  @RequestParam(required=false) String[] yyyy
									 ,@RequestParam(required=false) String[] mm
						             ,@RequestParam(required=false) String[] evaGbn
						             ,@RequestParam(required=false) String[] kpiId
						             ,@RequestParam(required=false) String[] status){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{				
			resultValue = kpiService.updateLastGradeValue(yyyy, mm, evaGbn, kpiId, status);
			msg.setSuccessText("저장 되었습니다");
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}	
	/**
	 * 평가지표관리화면
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/master/kpi/crudKpiResult", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView crudKpiResult(@RequestParam(required=false, defaultValue="2012") String yyyy
			                 ,@RequestParam(required=false, defaultValue="100") String tabscid
			                 ,@RequestParam(required=false, defaultValue="0") String gubn) throws Exception{
		ModelAndView modelAndView = new ModelAndView("/master/kpi/crudKpiResult");
		
		try{
			modelAndView.addObject("paramAcctid", loginSessionInfoFactory.getObject().getAcctID());
			
		}catch(Exception e){
			
			logger.error("error:{}",e);
			
			
		}
		return modelAndView;   
	}
	//평가지표관리 저장
	@RequestMapping(value="/master/kpi/insertkpiValue", method=RequestMethod.POST)
	public void insertkpiValue (HttpServletResponse response
									 ,@RequestParam(required=false) String[] yyyy
									 ,@RequestParam(required=false) String[] mm
						             ,@RequestParam(required=false) String[] evaGbn
						             ,@RequestParam(required=false) String[] kpiId
						             ,@RequestParam(required=false) String[] scId
						             ,@RequestParam(required=false) String[] mtId
						             ,@RequestParam(required=false) String[] grade
						             ,@RequestParam(required=false) String[] gradeDesc
						             ,@RequestParam(required=false) String[] status
						             ,@RequestParam(required=false) String[] ipvDesc){

		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiService.insertkpiValue(yyyy, mm, evaGbn, kpiId, scId, mtId, grade, gradeDesc, status, ipvDesc);
			msg.setSuccessText("저장 되었습니다");
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	//평가지표관리 제출/미제출
	@RequestMapping(value="/master/kpi/updatekpiValue", method=RequestMethod.POST)
	public void updatekpiValue (HttpServletResponse response
								  ,@RequestParam(required=false) String[] yyyy
								  ,@RequestParam(required=false) String[] mm
					              ,@RequestParam(required=false) String[] evaGbn
					              ,@RequestParam(required=false) String[] kpiId
					              ,@RequestParam(required=false) String[] status){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{			
			resultValue = kpiService.updatekpiValue(yyyy, mm, evaGbn, kpiId, status);
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}

	 
	@RequestMapping(value="/master/kpi/kpidownload", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView kpidownload ( HttpServletRequest request, HttpServletResponse response
			                           , @RequestParam(required=false) String fileid
			                           , @RequestParam(required=false) String kpiId
			                           , @RequestParam(required=false) String yyyy) throws Exception{
		ModelAndView modelAndView = new ModelAndView();
		
		modelAndView.addObject("downloadFile", kpiService.getFileInfo(fileid,kpiId,yyyy).get(0));
		modelAndView.setViewName("downloadView");
		return modelAndView;
	}
	//지표관리 - 평가결과관리 저장
	@RequestMapping(value="/master/kpi/evalResult", method=RequestMethod.POST)
	public void evalResult( HttpServletResponse response, HttpServletRequest request
							  ,@RequestParam(required=false) String scIdResult
			                   ,@RequestParam(required=false) String yyyyResult
			                   ,@RequestParam(required=false) String kpiIdResult
			                   ,@RequestParam(required=false) String evaGbnResult
			                   ,@RequestParam(required=false) String mtIdResult
			                   ,@RequestParam(required=false) String gradeDesc
			                   ,@RequestParam(required=false) String ipvDesc
			                   ,@RequestParam(required=false) String grade){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiService.evalResultSave(scIdResult, yyyyResult, kpiIdResult, evaGbnResult, mtIdResult, gradeDesc, ipvDesc, grade);
			 
			msg.setSuccessText("저장 되었습니다");
			 
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
}