package com.iplanbiz.imanager.controller;

import java.lang.reflect.Method;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.service.CodeService;
import com.iplanbiz.imanager.service.KpiCurrentStateService; 
import com.iplanbiz.imanager.service.KpiResultService;
import com.iplanbiz.imanager.service.KpiTargetService;

@Controller
public class KpiCurrentStateController {

	@Autowired
	KpiCurrentStateService kpiCurrentStateService;
	@Autowired
	KpiResultService kpiResultService;
	
	@Autowired
	KpiTargetService kpiTargetService;

	@Autowired
	CodeService codeService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	@RequestMapping(value="/report/kpiCurrentState/list", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView list( @RequestParam(required=false, defaultValue="") String pEvaGbn
							  ,@RequestParam(required=false, defaultValue="") String pEvalg
			                  ,@RequestParam(required=false, defaultValue="") String searchYear
			                  ,@RequestParam(required=false, defaultValue="") String searchMm
			                  ,@RequestParam(required=false, defaultValue="") String searchKpiID
			                  ,@RequestParam(required=false, defaultValue="") String searchScID){
		ModelAndView modelAndView = new ModelAndView("/report/kpiCurrentState/list"); 
		AjaxMessage msg = new AjaxMessage();
		String defaultYear = "";
		String defaultMm = "";
		String defaultKpiID = "";
		String defaultScID = "";
		try{
			modelAndView.addObject("listYear"					, codeService.getCodeListDESC("YEARS"));
			JSONObject listYear = (JSONObject) codeService.getCodeListDESC("YEARS").get(0);
			if(searchYear.equals("")){
				defaultYear = listYear.get("COM_COD").toString();
			}else{
				defaultYear = searchYear;
			}
			modelAndView.addObject("listMm"						, kpiResultService.listMmCombo(pEvaGbn, defaultYear, "A"));
			JSONObject listMm = (JSONObject) kpiResultService.listMmCombo(pEvaGbn, defaultYear, "A").get(0);
			if(searchMm.equals("")){
				defaultMm = listMm.get("MM_ID").toString();
			}else{
				defaultMm = searchMm;
			}
			modelAndView.addObject("listKpiCombo"				, kpiResultService.listKpiCombo(pEvaGbn, defaultYear+defaultMm));
			JSONObject listKpiCombo = (JSONObject) kpiResultService.listKpiCombo(pEvaGbn, defaultYear+defaultMm).get(0);
			if(searchKpiID.equals("")){
				defaultKpiID = listKpiCombo.get("MT_ID").toString();
			}else{
				defaultKpiID = searchKpiID;
			}
			modelAndView.addObject("listScCombo"				, kpiResultService.listScidCombo(pEvaGbn, pEvalg, defaultYear+defaultMm, defaultKpiID));
			JSONObject listScCombo = (JSONObject) kpiResultService.listScidCombo(pEvaGbn,pEvalg, defaultYear+defaultMm, defaultKpiID).get(0);
			if(searchScID.equals("")){
				defaultScID = listScCombo.get("SC_ID").toString();
			}else{
				defaultScID = searchScID;
			}
			modelAndView.addObject("mainList"					, kpiCurrentStateService.selectKpiCurrentState(defaultScID, defaultKpiID, pEvaGbn, defaultYear+defaultMm, pEvalg));
			msg.setSuccessMessage(new JSONArray(), new JSONObject());
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText("");
		}
		return modelAndView;
	}
	/*
	 * DWR 대용 메소드. 
	*/
	@RequestMapping(value="/report/kpiCurrentState/callService", method={RequestMethod.POST})
	public void callService( HttpServletResponse response, HttpServletRequest request){ 
		AjaxMessage msg = new AjaxMessage();
		try{
			Class<?> testClass = kpiResultService.getClass();
            Object newObj = kpiResultService;
            String dsmsg = request.getParameter("directServiceMsg");
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject)parser.parse(dsmsg);
            String method = json.get("method").toString();
            JSONArray parameters = (JSONArray)json.get("parameters");
            
            Class<?>[] arrParamClass = new Class<?>[parameters.size()];
            Object[] arrParam = new Object[parameters.size()]; 
            for(int i = 0; i < parameters.size();i++){
            	Object param = parameters.get(i);
            	arrParamClass[i] = param.getClass();
            	arrParam[i] = param;
            }            
 
            Method m = testClass.getDeclaredMethod(method,arrParamClass);
            
            Object returnValue = m.invoke(newObj,arrParam);
            if(returnValue instanceof JSONObject){
            	msg.setSuccessMessage((JSONObject)returnValue);
            }
            else if(returnValue instanceof JSONArray){
            	msg.setSuccessMessage((JSONArray)returnValue);
            }
            else{
            	msg.setSuccessText("메서드 호출에 성공 하였습니다.");
            } 
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText("메서드 호출 중에 오류가 발생 하였습니다. : "+e.getMessage());
		} 
		msg.send(response);
	}

}
