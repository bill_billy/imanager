package com.iplanbiz.imanager.controller.master;



import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dto.KpiAttachFileUser;
import com.iplanbiz.imanager.service.kpiAttachFileUserService;

@Controller
public class KpiAttachFileUserController {

	@Autowired
	kpiAttachFileUserService kpiAttachFileUserService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 insert를 prefix로 메소드를 추가 등록 합니다. ex) insertPerson
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	
	/*
	 * DWR 대용 메소드.
	*/
	@RequestMapping(value="/master/KpiAttachFileUser/callService", method={RequestMethod.POST})
	public void callService( HttpServletResponse response, HttpServletRequest request ){ 
		AjaxMessage msg = new AjaxMessage();
		try{
			Class<?> testClass = kpiAttachFileUserService.getClass();
            Object newObj = kpiAttachFileUserService;
            String dsmsg = request.getParameter("directServiceMsg");
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject)parser.parse(dsmsg);
            String method = json.get("method").toString();
            JSONArray parameters = (JSONArray)json.get("parameters");
            
            Class<?>[] arrParamClass = new Class<?>[parameters.size()];
            Object[] arrParam = new Object[parameters.size()];
            for(int i = 0; i < parameters.size();i++){
            	Object param = parameters.get(i);
            	arrParamClass[i] = param.getClass();
            	arrParam[i] = param;
            }            
 
            Method m = testClass.getDeclaredMethod(method,arrParamClass);
            
            Object returnValue = m.invoke(newObj,arrParam);
            if(returnValue instanceof JSONObject){
            	msg.setSuccessMessage((JSONObject)returnValue);
            }
            else if(returnValue instanceof JSONArray){
            	msg.setSuccessMessage((JSONArray)returnValue);
            }
            else{
            	msg.setSuccessText("메서드 호출에 성공 하였습니다.");
            } 
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText("메서드 호출 중에 오류가 발생 하였습니다. : "+e.getMessage());
		} 
		msg.send(response);
	}
	@RequestMapping(value="/master/KpiAttachFileUser/KpiAttachFileByUser", method={RequestMethod.GET})
	public ModelAndView crud( HttpServletResponse response, HttpServletRequest request ,
							  String evagbn
			){ 
		
		ModelAndView modelAndView = new ModelAndView("/master/KpiAttachFileUser/KpiAttachFileByUser");
		try {
			LinkedHashMap<String,Object> evaGbnInfo = kpiAttachFileUserService.getEvaGbnInfoUser(evagbn);
			modelAndView.addObject("evagbn",evaGbnInfo.get("EVA_GBN").toString());
			modelAndView.addObject("evagbnName",evaGbnInfo.get("EVA_GBN_NM").toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return modelAndView;   
	}
	@RequestMapping(value="/master/KpiAttachFileUser/remove", method={RequestMethod.POST})
	public void remove( HttpServletResponse response, HttpServletRequest request,
						 @RequestParam(required=true) String year,
						 @RequestParam(required=true) String month,
						 @RequestParam(required=true) String scid,
						 @RequestParam(required=true) String kpiid,
						 @RequestParam(required=true) String removefileid 
						 ){		
		AjaxMessage msg = new AjaxMessage();
		try{ 
			List<LinkedHashMap<String,Object>> info =  kpiAttachFileUserService.getAttachFileInfoUser(removefileid);	  
			String filepath = info.get(0).get("FILE_PATH").toString();
			String fileSavedName = info.get(0).get("FILE_SAVE_NAME").toString();
			String fileExtName = info.get(0).get("FILE_EXT").toString();
			kpiAttachFileUserService.removeKpiAttachFileUser(year,month,scid,kpiid,removefileid,filepath,fileSavedName,fileExtName);
			msg.setSuccessText("증빙 자료가 삭제 되었습니다.");
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText("증빙 자료 삭제 중 오류 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/master/KpiAttachFileUser/insert", method={RequestMethod.POST})
	public void insert( HttpServletResponse response, HttpServletRequest request,
				KpiAttachFileUser KpiAttachFileUser
						
						 ){		
		AjaxMessage msg = new AjaxMessage();
		try{ 
			kpiAttachFileUserService.uploadKpiAttachFileUser(KpiAttachFileUser);
			msg.setSuccessText("증빙 자료가 저장 되었습니다.");
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText("증빙 자료 저장 중 오류 : " + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="/master/KpiAttachFileUser/download", method={RequestMethod.GET})
	public ModelAndView download( HttpServletResponse response, HttpServletRequest request ,
							  String fileid
			){ 
		
		ModelAndView modelAndView = new ModelAndView();
		try {
			modelAndView.addObject("downloadFile", kpiAttachFileUserService.getAttachFileInfoUser(fileid).get(0));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		modelAndView.setViewName("downloadView");
		return modelAndView;
	}
	
}