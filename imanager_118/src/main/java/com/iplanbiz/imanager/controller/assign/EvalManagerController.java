package com.iplanbiz.imanager.controller.assign;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.imanager.service.assign.EvalManagerService;

@Controller
public class EvalManagerController {


	@Autowired
	EvalManagerService EvalManagerService; 
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 * 
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	
	/**
	 * 평가자관리 기본 화면
	 * @return 
	 */
	@RequestMapping(value="/assign/evalManager/crud", method={RequestMethod.POST, RequestMethod.GET}) 
	public ModelAndView crud(){ 
		ModelAndView modelAndView = new ModelAndView("assign/evalManager/crud");    
		return modelAndView;
	}
	
	@RequestMapping(value="/assign/evalManager/select", method={RequestMethod.POST, RequestMethod.GET}) 
	public void select( HttpServletResponse response, HttpServletRequest request
										,@RequestParam(required=false) String msId, @RequestParam(required=false) String yyyy){
		JSONArray resultValue = null;  
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = EvalManagerService.selectStrategyTarget(msId,yyyy);  
			msg.setSuccessMessage(resultValue);
			System.out.println("값 확인: " + msId + "/" + yyyy);  
		}catch(Exception e){  
			msg.setExceptionText(e.getMessage()); 
			logger.error("error : {}",e);
		}
		msg.send(response);
	}
	/**
	 * 평가자 조회 search
	 **/
	@RequestMapping(value="/assign/evalManager/popupMappingUser", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingUser(@RequestParam(required=false) String idx){
		ModelAndView modelAndView = new ModelAndView("assign/evalManager/popupMappingUser");
		modelAndView.addObject("idx", idx); 
		return modelAndView;    
	}
	
	/**
	 * 평가자관리 신규 및 수정 - insert
	 * @param response
	 * @param request 
	 
	 */
	@RequestMapping(value="/assign/evalManager/insert", method=RequestMethod.POST)
	public void insert( HttpServletResponse response, HttpServletRequest request
									  ,@RequestParam(required=false) String msId 
									  ,@RequestParam(required=false) String assignId
									  ,@RequestParam(required=false) String YYYY 
									  ,@RequestParam(required=false) String[] deptCd
		      						  ,@RequestParam(required=false) String[] deptNm
		      						  ,@RequestParam(required=false) String[] empId 
								      ,@RequestParam(required=false) String[] empNm
			                          ,@RequestParam(required=false) String[] masterYn) { 
			                          
		AjaxMessage msg = new AjaxMessage();
		System.out.println("insert테스트: " + msId + " " + assignId + " " + YYYY + " " + deptCd[0] + " " + deptNm[0] + " "+ empId[0] + " " + empNm[0] + " " + masterYn[0]); //인서트 값 찍기     
		try{   
			EvalManagerService.insertStrategtTarget(msId, assignId, YYYY, deptCd, deptNm, empId, empNm, masterYn);  
			msg.setSuccessText("저장 완료");   
		}catch(Exception e){  
			logger.error("error : {}",e); //체크 
			msg.setExceptionText("입력에러 : " + e.getMessage());  
			//TEST
		} 
		msg.send(response);
	}
}