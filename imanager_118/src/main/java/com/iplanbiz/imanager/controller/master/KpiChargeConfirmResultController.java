package com.iplanbiz.imanager.controller.master;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.imanager.service.KpiChargeConfirmResultService;

@Controller
public class KpiChargeConfirmResultController{
	@Autowired
	KpiChargeConfirmResultService KpiChargeConfirmResultService;
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax)
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	/**
	 * 작업내역조회 리스트 
	 * @return
	 */

	@RequestMapping(value="/master/kpiChargeConfirmResult/kpiChargeConfirmResult", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView list(){
		ModelAndView modelAndView = new ModelAndView("/master/kpiChargeConfirmResult/kpiChargeConfirmResult");
		return modelAndView;
	}
	@RequestMapping(value="/master/kpiChargeConfirmResult/confirmResultPopup", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView ConfirmResultPopup( @RequestParam(required=false) String yyyy
											,  @RequestParam(required=false) String pEvaGbn
											,  @RequestParam(required=false) String scId
											,  @RequestParam(required=false) String owneruserId
											,  @RequestParam(required=false) String kpichargeId
											,  @RequestParam(required=false) String gubn
											,  @RequestParam(required=false) String straId
											,  @RequestParam(required=false) String substrId
											,  @RequestParam(required=false) String csfId
											,  @RequestParam(required=false) String kpiId) throws Exception{
		ModelAndView modelAndView =  new ModelAndView();
		List<LinkedHashMap<String, Object>> mainList =  KpiChargeConfirmResultService.ConfirmResultPopupList(yyyy, pEvaGbn, scId, owneruserId, kpichargeId, gubn, straId, substrId, csfId, kpiId); 
		modelAndView.addObject("mainList", mainList);   

		return modelAndView;
	}
	@RequestMapping(value="/master/kpiChargeConfirmResult/kpidownload", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView kpidownload ( HttpServletRequest request, HttpServletResponse response
			                           , @RequestParam(required=false) String fileid
			                           , @RequestParam(required=false) String kpiId
			                           , @RequestParam(required=false) String yyyy) throws Exception{
		ModelAndView modelAndView = new ModelAndView();
		
		modelAndView.addObject("downloadFile", KpiChargeConfirmResultService.getFileInfo(fileid,kpiId,yyyy).get(0));
		modelAndView.setViewName("downloadView");
		return modelAndView;
	}
}