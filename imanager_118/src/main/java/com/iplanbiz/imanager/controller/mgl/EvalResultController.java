package com.iplanbiz.imanager.controller.mgl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.imanager.service.evalResult.EvalResultService;

@Controller	
public class EvalResultController {
		
		@Autowired
		EvalResultService EvalResultService; 
		private Logger logger = LoggerFactory.getLogger(getClass());
		
		@RequestMapping(value="/evalResult/EvalResult/crud", method={RequestMethod.POST, RequestMethod.GET}) 
		public ModelAndView crud(){ 
			ModelAndView modelAndView = new ModelAndView("evalResult/EvalResult/crud");     
			return modelAndView;
		}
		
}
