package com.iplanbiz.imanager.controller.master;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.imanager.service.GroupCodeService2;

@Controller
public class GroupCodeController2 {

	@Autowired
	GroupCodeService2 groupCodeService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax)
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	/**
	 * 그룹코드관리 리스트 
	 * @return
	 */
	@RequestMapping(value="/base/GroupCode/crud2", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView curd(){
		ModelAndView modelAndView = new ModelAndView("base/groupCode/crud2");
		return modelAndView;
	}
	/**
	 * 그룹코드 관리 신규및수정
	 * @param response
	 * @param request
	 * @param comlcod     그룹코드
	 * @param orgcomlcod  기존그룹코드
	 * @param comlnm      그룹코드명
	 * @param useyn       사용여부 (Y,N)
	 * @param sortorder   정렬순서
	 * @param bigo        비고
	 */
	@RequestMapping(value="/base/GroupCode/insert", method=RequestMethod.POST)
	public void insert( HttpServletResponse response, HttpServletRequest request
			                     ,@RequestParam(required=false) String comlcod
								 ,@RequestParam(required=false) String orgcomlcod
			                     ,@RequestParam(required=false) String comlnm
			                     ,@RequestParam(required=false) String useyn
			                     ,@RequestParam(required=false) String sortorder
			                     ,@RequestParam(required=false) String bigo){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try {
			resultValue = groupCodeService.insertGroupCode(orgcomlcod, comlcod, comlnm, useyn, sortorder, bigo);
			if(resultValue == 1){
				msg.setSuccessText("저장 되었습니다.");
			}else if(resultValue == 2){
				msg.setSuccessText("수정 되었습니다.");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다");
			}
		} catch (Exception e) {
			msg.setExceptionText("저장 중 오류가 발생했습니다 : "+e.getMessage());
			logger.error("error : {}",e);
		}
		msg.send(response);
	}
	/**
	 * 그룹코드관리 삭제
	 * @param response
	 * @param request
	 * @param orgcomlcod  기존그룹코드
	 */
	@RequestMapping(value="/base/GroupCode/remove", method=RequestMethod.POST)
	public void remove(HttpServletResponse response, HttpServletRequest request,
			                     @RequestParam(required=false) String orgcomlcod){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = groupCodeService.deleteGroupCode(orgcomlcod);
		}catch(Exception e){
			msg.setExceptionText("삭제 중 오류가 발생했습니다 : " + e.toString());
			logger.error("error : {}",e);
		}
		msg.send(response);
	}
}
