package com.iplanbiz.imanager.controller.program;

import java.lang.reflect.Method;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dto.AssignMent;
import com.iplanbiz.imanager.dto.AssignMentKpi;
import com.iplanbiz.imanager.dto.DeptEval;
import com.iplanbiz.imanager.dto.Program;
import com.iplanbiz.imanager.service.assign.MissionKpiService;
import com.iplanbiz.imanager.service.program.ProgramFileService;
import com.iplanbiz.imanager.service.program.ProgramManagerService;

@Controller
public class ProgramManagerController {

	@Autowired
	ProgramManagerService programService;
	
	@Autowired
	ProgramFileService fileService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 insert를 prefix로 메소드를 추가 등록 합니다. ex) insertPerson
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	
	/*
	 * DWR 대용 메소드.
	*/
	@RequestMapping(value="/program/programManager/callService", method={RequestMethod.POST})
	public void callService( HttpServletResponse response, HttpServletRequest request ){ 
		AjaxMessage msg = new AjaxMessage();
		try{
			Class<?> testClass = programService.getClass();
            Object newObj = programService;
            String dsmsg = request.getParameter("directServiceMsg");
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject)parser.parse(dsmsg);
            String method = json.get("method").toString();
            JSONArray parameters = (JSONArray)json.get("parameters");
           
            Class<?>[] arrParamClass = new Class<?>[parameters.size()];
            
            Object[] arrParam = new Object[parameters.size()];
            for(int i = 0; i < parameters.size();i++){
            	Object param = parameters.get(i);  
            	arrParamClass[i] = param.getClass();
            	arrParam[i] = param;
            }            
 
            Method m = testClass.getDeclaredMethod(method,arrParamClass);
            
            Object returnValue = m.invoke(newObj,arrParam);
            if(returnValue instanceof JSONObject){
            	msg.setSuccessMessage((JSONObject)returnValue);
            }
            else if(returnValue instanceof JSONArray){
            	msg.setSuccessMessage((JSONArray)returnValue);
            }
            else{
            	msg.setSuccessText("메서드 호출에 성공 하였습니다.");
            } 
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText("메서드 호출 중에 오류가 발생 하였습니다. : "+e.getMessage());
		} 
		msg.send(response);
	}
	
	
	/**
	 * 지표관리 초기화면
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/program/programManager/crud", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView crud(@RequestParam(required=false) String yyyy
			                 ,@RequestParam(required=false, defaultValue="100") String ms_id) throws Exception{
		ModelAndView modelAndView = new ModelAndView("/program/programManager/crud");
		
		try{
			modelAndView.addObject("yyyy", yyyy);
			modelAndView.addObject("ms_id", ms_id);
			//프로그램상태
			modelAndView.addObject("statusList", programService.listCode("PROGRAM_STATUS"));
			//단위
			modelAndView.addObject("listUnit", programService.listCode("UNIT"));
			//예산구분
			modelAndView.addObject("budgetTypList", programService.listCode("BUDGET_TYP"));
			
		}catch(Exception e){
			
			logger.error("error:{}",e);
			
			
		}
		return modelAndView;   
	}
	/**
	 * 지표관리 추진과제 입력화면
	 * @return
	 * @throws Exception 
	 */
	
	
	@RequestMapping(value="/program/programManager/popupMappingAssign", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingAssign(@RequestParam(required=false) String ms_id,@RequestParam(required=false) String yyyy){
		ModelAndView modelAndView = new ModelAndView("/program/programManager/popupMappingAssign");
		modelAndView.addObject("ms_id", ms_id);
		modelAndView.addObject("yyyy", yyyy);
		return modelAndView;
	}
	@RequestMapping(value="/program/programManager/popupMappingUser", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingUser(@RequestParam(required=false) String userType){
		ModelAndView modelAndView = new ModelAndView("/program/programManager/popupMappingUser");
		modelAndView.addObject("userType", userType);
		return modelAndView;
	}
	@RequestMapping(value="/program/programManager/popupMappingKpi", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingKpi(){
		ModelAndView modelAndView = new ModelAndView("/program/programManager/popupMappingKpi");
		return modelAndView;
	}
	
	@RequestMapping(value="/program/programManager/insert", method=RequestMethod.POST)
	public void insert(HttpServletResponse response, HttpServletRequest requeset
			          ,@ModelAttribute Program program){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = programService.insert(program);
			if(resultValue == 1){
				msg.setSuccessText("저장 되었습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	
	
	@RequestMapping(value="/program/programManager/insertPlan", method=RequestMethod.POST)
	public void insertPlan(HttpServletResponse response, HttpServletRequest requeset
			          ,@ModelAttribute Program program){
		
		
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = programService.insertPlan(program);
			if(resultValue == 1){
				msg.setSuccessText("저장 되었습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	@RequestMapping(value="/program/programManager/download", method={RequestMethod.GET})
	public ModelAndView download(@RequestParam String ms_id,@RequestParam String program_id, @RequestParam String file_gubun,@RequestParam String file_id) throws SQLException{
		
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("downloadFile", fileService.getFile(ms_id,program_id,file_gubun,file_id));
		modelAndView.setViewName("downloadView");
		
		
		return modelAndView;
	}
	@RequestMapping(value="/program/programManager/deleteFile", method={RequestMethod.POST})
	public void deletePlanFile(HttpServletResponse response, HttpServletRequest request,
			@RequestParam String ms_id,@RequestParam String program_id, @RequestParam String file_gubun,@RequestParam String file_id) throws SQLException{
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = programService.deletePlanFile(ms_id, program_id,file_gubun,file_id);
			if(resultValue == 1){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("삭제 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText("삭제 중 오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	
	
	
	
	
	@RequestMapping(value="/program/programManager/insertRun", method=RequestMethod.POST)
	public void insertRun(HttpServletResponse response, HttpServletRequest requeset
			          ,@ModelAttribute Program program){
		
	
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = programService.insertRun(program);
			if(resultValue == 1){
				msg.setSuccessText("저장 되었습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	
	
	@RequestMapping(value="/program/programManager/insertDeptEval", method=RequestMethod.POST)
	public void insertDeptEval(HttpServletResponse response, HttpServletRequest requeset
			,@ModelAttribute DeptEval depteval){
		
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = programService.insertDeptEval(depteval);
			if(resultValue == 1){
				msg.setSuccessText("저장 되었습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("저장 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/program/programManager/confirmPlan", method=RequestMethod.POST)
	public void confirmPlan(HttpServletResponse response, HttpServletRequest requeset
			 ,@RequestParam(required=false) String ms_id
	            ,@RequestParam(required=false) String program_id
	            ,@RequestParam(required=false) String yyyy
	            ,@RequestParam(required=false) String status){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = programService.confirmPlan(ms_id, program_id,yyyy,status);
			if(resultValue == 1){
				msg.setSuccessText("계획이 완료 되었습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("계획이 취소 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/program/programManager/confirmRun", method=RequestMethod.POST)
	public void confirmRun(HttpServletResponse response, HttpServletRequest requeset
			 ,@RequestParam(required=false) String ms_id
	            ,@RequestParam(required=false) String program_id
	            ,@RequestParam(required=false) String yyyy
	            ,@RequestParam(required=false) String status){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = programService.confirmRun(ms_id, program_id,yyyy,status);
			if(resultValue == 1){
				msg.setSuccessText("운영완료 되었습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("운영이 취소 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	@RequestMapping(value="/program/programManager/confirmDeptEval", method=RequestMethod.POST)
	public void confirmDept(HttpServletResponse response, HttpServletRequest requeset
			 ,@RequestParam(required=false) String ms_id
	            ,@RequestParam(required=false) String program_id
	            ,@RequestParam(required=false) String yyyy
	            ,@RequestParam(required=false) String status){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = programService.confirmDept(ms_id, program_id,yyyy,status);
			if(resultValue == 1){
				msg.setSuccessText("평가완료 되었습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("평가취소 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	
	@RequestMapping(value="/program/programManager/confirmManager", method=RequestMethod.POST)
	public void confirmManager(HttpServletResponse response, HttpServletRequest requeset
			 ,@RequestParam(required=false) String ms_id
	            ,@RequestParam(required=false) String program_id
	            ,@RequestParam(required=false) String yyyy
	            ,@RequestParam(required=false) String status){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = programService.confirmManager(ms_id, program_id,yyyy,status);
			if(resultValue == 1){
				msg.setSuccessText("확정 되었습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("취소 되었습니다");
			}else{
				msg.setExceptionText("오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("오류가 발생했습니다 : " + e.getMessage());
			
			logger.error("error:{}",e);
		}
		msg.send(response);
	}
	
	
	
	@RequestMapping(value="/program/programManager/remove", method=RequestMethod.POST)
	public void update( HttpServletResponse response, HttpServletRequest request
			            ,@RequestParam(required=false) String ms_id
			            ,@RequestParam(required=false) String program_id){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = programService.remove(ms_id, program_id);
			if(resultValue == 1){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("삭제 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			
			logger.error("error:{}",e);
			msg.setExceptionText("삭제 중 오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
	
	
}