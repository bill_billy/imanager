package com.iplanbiz.imanager.controller.master;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.imanager.service.ConnectionItemService;

@Controller
public class ConnectionItemController {

	@Autowired
	ConnectionItemService connectionItemService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	/**
	 * 연계항목관리 조회화면
	 * @return
	 */
	@RequestMapping(value="master/connectionItem/crud", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView("/master/connectionItem/crud");
		return modelAndView;
	}
	/**
	 * 연계항목관리 좌측 연계항목리스트
	 * @param response
	 * @param request
	 * @param searchyn      사용여부(Y,N)
	 * @param searchifsys   입력구분
	 * @param searchifcodnm 연계항목명
	 */
	@RequestMapping(value="master/connectionItem/select", method={RequestMethod.GET, RequestMethod.POST})
	public void select( HttpServletResponse response, HttpServletRequest request
			                          ,@RequestParam(required=false) String searchyn
			                          ,@RequestParam(required=false) String searchifsys
			                          ,@RequestParam(required=false) String searchifcodnm
			                          ){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = connectionItemService.selectConnectionItem(searchyn, searchifsys, searchifcodnm);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			msg.setExceptionText(e.getMessage());
			logger.error("error : {}",e);
		}
		msg.send(response);
	}
	/**
	 * 연계항목관리 Insert
	 * @param response
	 * @param request
	 * @param ifcod   연계항목코드
	 * @param ifcodnm 연계항목명
	 * @param ifunit  항목단위
	 * @param ifsys   입력구분
	 * @param bigo    비고
	 * @param sqldata SQL문장
	 * @param useyn   사용여부(Y,N)
	 */
	@RequestMapping(value="master/connectionItem/insert",method= RequestMethod.POST)
	public void insert( HttpServletResponse response, HttpServletRequest request
			                          ,@RequestParam(required=false) String ifcod
			                          ,@RequestParam(required=false) String ifcodnm
			                          ,@RequestParam(required=false) String ifunit
			                          ,@RequestParam(required=false) String ifsys
			                          ,@RequestParam(required=false) String bigo
			                          ,@RequestParam(required=false) String sqldata
			                          ,@RequestParam(required=false) String useyn){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = connectionItemService.insertConnectionItem(ifcod, ifcodnm, ifunit, ifsys, sqldata, bigo, useyn);
			if(resultValue == 1){
				msg.setSuccessText("저장이 되었습니다.");
			}else if(resultValue == 2){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("저장 중 에러가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 에러가 발생했습니다" + e.getMessage());
			logger.error("error : {}",e);
		}
		msg.send(response); 
	}
	/**
	 * 연계항목관리 삭제
	 * @param response
	 * @param request
	 * @param ifcod   연계항목코드
	 */
	@RequestMapping(value="master/connectionItem/remove", method=RequestMethod.POST)
	public void remove( HttpServletResponse response, HttpServletRequest request
			                      ,@RequestParam(required=false) String ifcod){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = connectionItemService.removeConnectionItem(ifcod);
			if(resultValue == 1){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("삭제 중 에러가 발생했습니다");
			}
		}catch (Exception e) {
			msg.setExceptionText("삭제 중 에러가 발생했습니다");
			logger.error("error : {}",e);
		}
		msg.send(response);
	}
	/**
	 * 연계항목을 삭제하기 전에 사용중인지 아닌지 체크
	 * @param response
	 * @param request
	 * @param ifcod   연계항목코드
	 */
	@RequestMapping(value="master/connectionItem/check", method={RequestMethod.POST, RequestMethod.GET})
	public void check( HttpServletResponse response, HttpServletRequest request
			                     ,@RequestParam(required=false) String ifcod){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = connectionItemService.checkConnectionItem(ifcod);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText("데이터 체크 중 에러 발생" + e.getMessage());
		}
		msg.send(response);
	}
	@RequestMapping(value="master/connectionItem/popupMappingSql", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView popupMappingSql (HttpServletResponse response, HttpServletRequest request
			                             ,@RequestParam(required=false) String ifcod){
		ModelAndView modelAndView = new ModelAndView("/master/connectionItem/popupMappingSql");
		
		return modelAndView;
	}
	
}
