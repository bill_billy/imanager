package com.iplanbiz.imanager.controller.master;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.service.KpiFormulaService;

@Controller
public class KpiFormulaController {

	@Autowired
	KpiFormulaService kpiFormulaService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax)
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	/**
	 * 지표산식관리 기본화면
	 * @return
	 */
	@RequestMapping(value="/master/kpiFormula/crud", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView("master/kpiFormula/crud");
		AjaxMessage msg = new AjaxMessage();
		try{
			modelAndView.addObject("paramAcctid",loginSessionInfoFactory.getObject().getAcctID());
			msg.setSuccessMessage(new JSONArray(),new JSONObject());
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText("");
		}
		return modelAndView;
	}
	/**
	 * 지표산식관리 좌측 grid 리스트
	 * @param response
	 * @param request
	 * @param cboSearchyn  사영여부(Y,N)
	 * @param txtSearch    산식패턴명
	 */
	@RequestMapping(value="/master/kpiFormula/select", method={RequestMethod.GET, RequestMethod.POST})
	public void select( HttpServletResponse response, HttpServletRequest request
			                      ,@RequestParam(required=false) String cboSearchyn
			                      ,@RequestParam(required=false) String txtSearch){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiFormulaService.selectKpiFormula(cboSearchyn, txtSearch);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			msg.setExceptionText(e.getMessage());
			logger.error("error : {}",e);
		}
		msg.send(response);
	}
	/**
	 * 지표산식관리 신규 및 수정
	 * @param response
	 * @param requst
	 * @param calcod   산식코드
	 * @param calnm    산식명
	 * @param caludc1  산식패턴
	 * @param useyn    사용여부(Y,N)
	 */
	@RequestMapping(value="/master/kpiFormula/insert", method=RequestMethod.POST)
	public void insert( HttpServletResponse response, HttpServletRequest requst
			                      ,@RequestParam(required=false) String calcod  //산식코드
			                      ,@RequestParam(required=false) String calnm   //산식명
			                      ,@RequestParam(required=false) String caludc1 //산식패턴
			                      ,@RequestParam(required=false) String useyn   //사용여부
			                      ){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiFormulaService.insertKpiFormula(calcod, calnm, caludc1, useyn);
			if(resultValue == 1){
				msg.setExceptionText("산식없음 저장 중 에러가 발생했습니다");
			}else if(resultValue == 2){
				msg.setSuccessText("저장이 되었습니다.");
			}else if(resultValue == 3){
				msg.setSuccessText("수정 되었습니다");
			}else{
				msg.setExceptionText("저장 중 에러가 발생했습니다.");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 에러가 발생했습니다 : " + e.getMessage());
			logger.error("error : {}",e);
		}
		msg.send(response);
	}
	/**
	 * 지표산식관리 삭제
	 * @param response
	 * @param requset
	 * @param calcod  산식코드
	 */
	@RequestMapping(value="/master/kpiFormula/remove", method=RequestMethod.POST)
	public void remove(HttpServletResponse response, HttpServletRequest requset
								 ,@RequestParam(required=false) String calcod){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiFormulaService.removeKpiFormula(calcod);
			msg.setSuccessText("삭제 되었습니다");
		}catch(Exception e){
			msg.setExceptionText("삭제 중 에러가 발생했습니다 : "+e.getMessage());
			logger.error("error : {}",e);
		}
		msg.send(response);
	}
	/**
	 * 지표산식관리 삭제 전 사용여부 체크
	 * @param response
	 * @param request
	 * @param calcod   산식코드
	 */
	@RequestMapping(value="/master/kpiFormula/check", method={RequestMethod.POST, RequestMethod.GET})
	public void check( HttpServletResponse response, HttpServletRequest request
			                     ,@RequestParam(required=false) String calcod){
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = kpiFormulaService.checkKpiFormula(calcod);
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			logger.error("error : {}",e);
			msg.setExceptionText("데이터를 불러오는 중 ERROR 발생" + e.getMessage());
		}
		msg.send(response);
	}
}
