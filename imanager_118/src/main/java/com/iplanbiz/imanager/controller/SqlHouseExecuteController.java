package com.iplanbiz.imanager.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.imanager.service.SqlHouseExecuteService;

@Controller
public class SqlHouseExecuteController {

	@Autowired
	SqlHouseExecuteService sqlHouseExecuteService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		삭제 : POST 			/remove
	 *
	 */
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value= "/sqlhouseExecute/select", method=RequestMethod.POST)
	public void select(    	HttpServletResponse response,HttpServletRequest request, 
							@RequestParam(required=true) int idx,
							@RequestParam(required=true) String param){
		
		HashMap<String, Object> paramMap = null;
		JSONArray resultArray = null;
		JSONObject resultObject = null;
		AjaxMessage msg = new AjaxMessage();
		try {
			paramMap = new ObjectMapper().readValue(param, HashMap.class);
			
			resultArray = sqlHouseExecuteService.selectJSONArray(idx, paramMap);
			//logger.debug("test:{}",((JSONObject)resultValue.get(0)).get("KO"));
			if(resultArray.size()==1) resultObject = (JSONObject)resultArray.get(0); 
			msg.setSuccessMessage(resultArray,resultObject);
		} catch (Exception e) {
			msg.setExceptionText(e.getMessage());
			logger.error("error : {}",e);
		}
		
		msg.send(response);
	}
	

}
