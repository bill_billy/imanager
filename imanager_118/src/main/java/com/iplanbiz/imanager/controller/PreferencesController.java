package com.iplanbiz.imanager.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.imanager.dto.Preferences;
import com.iplanbiz.imanager.service.PreferencesService;

@Controller
public class PreferencesController {
	@Autowired
	PreferencesService preferencesService;
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax)
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	/**
	 * 기본환경 설정 
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value="/preferences/basicPreferences/crud", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView crud(@RequestParam(required=false, defaultValue="") String scNm) throws Exception
	 {
		ModelAndView modelAndView = new ModelAndView();
		
		Map<String, String> Preference = new HashMap<String, String>();
		@SuppressWarnings("unchecked")
		List<HashMap<String, String>> selectPreferencesList  = preferencesService.selectPreferences();
		if(selectPreferencesList .size() > 0) Preference = selectPreferencesList .get(0);
		modelAndView.addObject("Preference", Preference);
		
		@SuppressWarnings("unchecked")
		List<HashMap<String, String>> scList = preferencesService.selectEvalOrg("100");
		if(scList.size() > 0) scNm = scList.get(0).get("SC_NM");
		modelAndView.addObject("scNm", scNm);
		
		modelAndView.setViewName("preferences/basicPreferences/crud");
		return modelAndView;
	}
	//평가시기설정 조회
	@RequestMapping(value="/preferences/basicPreferences/crud_evaGbn", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView crud_evaGbn(@RequestParam(required=false)String pageName)
	{
		int errCode = 0;
		ModelAndView modelAndView = new ModelAndView();
		
		try {
			modelAndView.addObject("mainList", preferencesService.selectEvaGbnSetting());
		} catch (Exception e) {
			e.printStackTrace();
		}
		modelAndView.addObject("errCode", errCode);

		modelAndView.setViewName("preferences/basicPreferences/crud_evaGbn");
		return modelAndView;
	}
	/**
	 * 환경설정 > 평가시기 저장
	 * @param preferences
	 */
	@RequestMapping(value="/preferences/basicPreferences/save", method=RequestMethod.POST)
	public void save(HttpServletResponse response, HttpServletRequest requeset
						,@ModelAttribute Preferences pf){
		ModelAndView modelAndView = new ModelAndView();
		
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();  

		modelAndView.addObject("preferenceslist", pf);
			try{
				resultValue = preferencesService.insertEvaGbn(pf);
				if(resultValue == 0){
					msg.setExceptionText("저장 중 에러가 발생했습니다");
				}else if(resultValue == 1){
					msg.setSuccessText("저장 되었습니다");
				}else if(resultValue == 2){
					msg.setSuccessText("수정 되었습니다");
				}
			}catch(Exception e){
				msg.setExceptionText("저장 중 에러가 발생했습니다 : " + e.getMessage());
			e.printStackTrace();
			}
		msg.send(response);
	}
}