package com.iplanbiz.imanager.controller.evalResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.imanager.service.evalResult.CommitteeEvalResultService;

@Controller	
public class CommitteeEvalResultController {
		
		@Autowired
		CommitteeEvalResultService committeeEvalResultService; 
		private Logger logger = LoggerFactory.getLogger(getClass());
		/*
		 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
		 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
		 * 
		 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
		 *	
		 *  구분 | 메소드	 | 		메소드명
			생성 : POST 			/insert
			수정 : POST 			/update
			조회 : GET,POST 		/list
			조회 : GET,POST 		/select(ajax) 
			상세 : GET,POST         /detail
			삭제 : POST 			/remove
		 *
		 */
		 
		/**
		 * 평가진행현황 기본 화면
		 * @return  
		 */ 
		@RequestMapping(value="/evalResult/committeeEvalResult/crud", method={RequestMethod.POST, RequestMethod.GET}) 
		public ModelAndView crud(){ 
			ModelAndView modelAndView = new ModelAndView("evalResult/committeeEvalResult/crud");     
			return modelAndView;
		}
		 
		/**
		 * 평가진행현황detail 기본 화면
		 * @return  
		 */ 
		@RequestMapping(value="/evalResult/committeeEvalResult/detailCommitteeEvalResult", method={RequestMethod.POST, RequestMethod.GET}) 
		public ModelAndView detailCommitteeEvalResult(){ 
			ModelAndView modelAndView = new ModelAndView("evalResult/committeeEvalResult/detailCommitteeEvalResult");     
			return modelAndView;
		}
}
