package com.iplanbiz.imanager.controller.program;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.imanager.service.program.ProgramEvalConfirmService;

@Controller
public class ProgramEvalConfirmController {
	@Autowired
	ProgramEvalConfirmService programEvalConfirmService;
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 * 
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	
	/**
	 * 전략목표관리 기본 화면
	 * @return
	 */
	@RequestMapping(value="/program/programEvalConfirm/crud", method={RequestMethod.POST, RequestMethod.GET})
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView("/program/programEvalConfirm/crud");  
		return modelAndView;
	}
	
	/**
	 * 프로그램확정화면 확정여부 (확인) insert1
	 * @param response
	 * @param request
	 */
	@RequestMapping(value="/program/programEvalConfirm/insert1", method=RequestMethod.POST)
	public void insert1( HttpServletResponse response, HttpServletRequest request
								      ,@RequestParam(required=false) String ms_id
								      ,@RequestParam(required=false) String yy_yy
								      ,@RequestParam(required=false) String assign_id
								      ,@RequestParam(required=false) String program_id
			                         ) {
			                          
		AjaxMessage msg = new AjaxMessage();
		try{ 
			programEvalConfirmService.insertStrategtTarget1(ms_id, yy_yy, assign_id, program_id);
			msg.setSuccessText("저장 완료");
		}catch(Exception e){ 
			logger.error("error : {}",e); //체크
			msg.setExceptionText("입력에러 : " + e.getMessage());  
			//TEST
		}
		msg.send(response);
	}
	
	/**
	 * 프로그램확정화면 확정여부 (취소) insert2
	 * @param response
	 * @param request
	 */
	@RequestMapping(value="/program/programEvalConfirm/insert2", method=RequestMethod.POST)
	public void insert2( HttpServletResponse response, HttpServletRequest request
								      ,@RequestParam(required=false) String ms_id
								      ,@RequestParam(required=false) String yy_yy
								      ,@RequestParam(required=false) String assign_id
								      ,@RequestParam(required=false) String program_id
			                         ) {
			                          
		AjaxMessage msg = new AjaxMessage();
		try{ 
			programEvalConfirmService.insertStrategtTarget2(ms_id, yy_yy, assign_id, program_id);
			msg.setSuccessText("저장 완료");
		}catch(Exception e){ 
			logger.error("error : {}",e); //체크
			msg.setExceptionText("입력에러 : " + e.getMessage());  
			//TEST
		}
		msg.send(response);
	}
}
