package com.iplanbiz.imanager.controller;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.imanager.dto.ValueDueDate;
import com.iplanbiz.imanager.service.ValueDueDateService;;

@Controller
public class ValueDueDateController {

	@Autowired
	ValueDueDateService valueDueDateService;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	/*
	 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
	 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
	 *
	 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
	 *	
	 *  구분 | 메소드	 | 		메소드명
		생성 : POST 			/insert
		수정 : POST 			/update
		조회 : GET,POST 		/list
		조회 : GET,POST 		/select(ajax) 
		상세 : GET,POST         /detail
		삭제 : POST 			/remove
	 *
	 */
	
	/*
	 * DWR 대용 메소드.
	*/
	@RequestMapping(value="/master/valueDueDate/callService", method={RequestMethod.POST})
	public void callService( HttpServletResponse response, HttpServletRequest request ){ 
		AjaxMessage msg = new AjaxMessage();
		try{
			Class<?> testClass = valueDueDateService.getClass();
            Object newObj = valueDueDateService;
            String dsmsg = request.getParameter("directServiceMsg");
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject)parser.parse(dsmsg);
            String method = json.get("method").toString();
            JSONArray parameters = (JSONArray)json.get("parameters");
            
            Class<?>[] arrParamClass = new Class<?>[parameters.size()];
            Object[] arrParam = new Object[parameters.size()];
            for(int i = 0; i < parameters.size();i++){
            	Object param = parameters.get(i);
            	arrParamClass[i] = param.getClass();
            	arrParam[i] = param;
            }            
 
            Method m = testClass.getDeclaredMethod(method,arrParamClass);
            
            Object returnValue = m.invoke(newObj,arrParam);
            if(returnValue instanceof JSONObject){
            	msg.setSuccessMessage((JSONObject)returnValue);
            }
            else if(returnValue instanceof JSONArray){
            	msg.setSuccessMessage((JSONArray)returnValue);
            }
            else{
            	msg.setSuccessText("메서드 호출에 성공 하였습니다.");
            } 
		}catch(Exception e){
			e.printStackTrace();
			msg.setExceptionText("메서드 호출 중에 오류가 발생 하였습니다. : "+e.getMessage());
		} 
		msg.send(response);
	}
	
	/**
	 * 실적입력기한관리 화면
	 * @return
	 */
	@RequestMapping(value="/master/valueDueDate/crud", method={RequestMethod.GET, RequestMethod.POST})
	public ModelAndView crud(){
		ModelAndView modelAndView = new ModelAndView("master/valueDueDate/crud");
		return modelAndView;
	}
	
	/**
	 * 년월 리스트
	 * @param evaGbn : 평가구분
	 * @param year : 학년도 
	 */
	@RequestMapping(value="/master/valueDueDate/list", method={RequestMethod.POST, RequestMethod.GET})
	public void list( HttpServletResponse response, HttpServletRequest request
						, @RequestParam(required=false) String evaGbn
						, @RequestParam(required=false) String year){
		
		JSONArray resultValue = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = valueDueDateService.getEvalYearMonthList(evaGbn, year);
			
			msg.setSuccessMessage(resultValue);
		}catch(Exception e){
			e.printStackTrace();
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	
	/**
	 * 상세정보
	 * @param evaGbn : 평가구분
	 * @param yyyymm : 기준년월(회기년월 아님) 
	 */
	@RequestMapping(value="/master/valueDueDate/select", method={RequestMethod.POST, RequestMethod.GET})
	public void select( HttpServletResponse response, HttpServletRequest request
			, @RequestParam(required=false) String evaGbn
			, @RequestParam(required=false) String yyyymm){
		
		JSONArray resultArray = null;
		JSONObject resultObject = null;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultArray = valueDueDateService.getEvalDetail(evaGbn, yyyymm);
			if(resultArray.size()>0) {
				resultObject = (JSONObject)resultArray.get(0);
			}
			
			msg.setSuccessMessage(resultArray, resultObject);	
				
				
		}catch(Exception e){
			e.printStackTrace();
			msg.setExceptionText(e.getMessage());
		}
		msg.send(response);
	}
	
	/**
	 * 저장
	 * @param valueDueDate
	 */
	@RequestMapping(value="/master/valueDueDate/save", method=RequestMethod.POST)
	public void save(HttpServletResponse response, HttpServletRequest requeset, @ModelAttribute ValueDueDate valueDueDate) {
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = valueDueDateService.save(valueDueDate);
			
			if(resultValue > 0) {
				// insert, update 성공 시 회기년월 다시 select
				JSONObject periodYyyyMm = valueDueDateService.getPeriodYyyyMm(valueDueDate.getYyyymm());
				valueDueDate.setYyyymm((String)periodYyyyMm.get("PERIOD_YYYYMM"));
				
				msg.setSuccessMessage(valueDueDate.getJSONObject());
				msg.setSuccessText("저장 되었습니다");
			} else {
				msg.setExceptionText("저장 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			msg.setExceptionText("저장 중 오류가 발생했습니다.\r\n" + e.getMessage());
			e.printStackTrace();
		}
		msg.send(response);
	}
	
	/**
	 * 삭제
	 * @param valueDueDate
	 */
	@RequestMapping(value="/master/valueDueDate/remove", method=RequestMethod.POST)
	public void remove( HttpServletResponse response, HttpServletRequest request, @ModelAttribute ValueDueDate valueDueDate){
		int resultValue = 0;
		AjaxMessage msg = new AjaxMessage();
		try{
			resultValue = valueDueDateService.remove(valueDueDate);
			if(resultValue == 1){
				msg.setSuccessText("삭제 되었습니다");
			}else{
				msg.setExceptionText("삭제 중 오류가 발생했습니다");
			}
		}catch(Exception e){
			e.printStackTrace();
			msg.setExceptionText("삭제 중 오류가 발생했습니다" + e.getMessage());
		}
		msg.send(response);
	}
}
