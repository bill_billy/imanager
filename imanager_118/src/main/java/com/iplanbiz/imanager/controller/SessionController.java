package com.iplanbiz.imanager.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SessionController {


private Logger logger = LoggerFactory.getLogger(getClass());
	
/*
 *	※필히 준수해 주세요.한 컨트롤에 아래 메소드는 하나씩만 있어야 하며
 *	insert 로직이 추가되어야 할 경우 컨트롤을 추가 등록 합니다.
 *
 *	컨트롤을 생성하신 분은 이 메시지를 동일하게 넣어주세요.
 *	
 *  구분 | 메소드	 | 		메소드명
	생성 : POST 			/insert
	수정 : POST 			/update
	조회 : GET,POST 		/list
	조회 : GET,POST 		/select(ajax) 
	삭제 : POST 			/remove
 *
 */
	
	@RequestMapping(value="/system/session/create", method={RequestMethod.GET,RequestMethod.POST})
	public ModelAndView create(HttpServletResponse response) throws IOException{
		ModelAndView mview = new ModelAndView("system/session/create");
		return mview;
	}
	
	@RequestMapping(value="/system/session/check", method={RequestMethod.GET,RequestMethod.POST})
	public ModelAndView check(HttpServletResponse response) throws IOException{
		ModelAndView mview = new ModelAndView("system/session/check"); 
		return mview;
	}
	
	@RequestMapping(value="/system/session/reload", method={RequestMethod.GET,RequestMethod.POST})
	public ModelAndView reload(HttpServletResponse response) throws IOException{
		ModelAndView mview = new ModelAndView("system/session/reload"); 
		return mview;
	}
	
	@RequestMapping(value="/system/session/logout", method={RequestMethod.GET,RequestMethod.POST})
	public ModelAndView logout(HttpSession session,HttpServletResponse response) throws IOException{
		ModelAndView mview = new ModelAndView("system/session/logout"); 
		session.invalidate();
		return mview;
	}
	 
}
