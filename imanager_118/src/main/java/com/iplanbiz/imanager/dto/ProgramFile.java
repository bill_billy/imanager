package com.iplanbiz.imanager.dto;

public class ProgramFile {
    
	private String ms_id;	
	private String program_id;
	private String file_id;		
	private String fileSaveName;	
	private String fileOrgName;	
	private String filePath;	
	private String fileSize;	
	private String fileExt;	
	private String file_gubun;
	
	
	
	@Override
	public String toString() {
		return "ProgramFile [fileId=" + file_id + ", programId=" + program_id
				+ ", fileSaveName=" + fileSaveName + ", fileOrgName="
				+ fileOrgName + ", filePath=" + filePath + ", fileSize="
				+ fileSize + ", fileExt=" + fileExt + "]";
	}



	public String getMs_id() {
		return ms_id;
	}



	public void setMs_id(String ms_id) {
		this.ms_id = ms_id;
	}



	public String getProgram_id() {
		return program_id;
	}



	public void setProgram_id(String program_id) {
		this.program_id = program_id;
	}



	public String getFile_id() {
		return file_id;
	}



	public void setFile_id(String file_id) {
		this.file_id = file_id;
	}



	public String getFileSaveName() {
		return fileSaveName;
	}



	public void setFileSaveName(String fileSaveName) {
		this.fileSaveName = fileSaveName;
	}



	public String getFileOrgName() {
		return fileOrgName;
	}



	public void setFileOrgName(String fileOrgName) {
		this.fileOrgName = fileOrgName;
	}



	public String getFilePath() {
		return filePath;
	}



	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}



	public String getFileSize() {
		return fileSize;
	}



	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}



	public String getFileExt() {
		return fileExt;
	}



	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}



	public String getFile_gubun() {
		return file_gubun;
	}



	public void setFile_gubun(String file_gubun) {
		this.file_gubun = file_gubun;
	}
}
