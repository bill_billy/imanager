package com.iplanbiz.imanager.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory; 

public class AssignMentKpi {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private String ms_id;
    private String assign_id;
    private String mt_id;
    
	private String stra_id;
    private String substra_id;
    private String mt_nm; 
    private String cal_nm;
    private String meas_cycle;
    private String unit;
    private String eval_type;
    private String mt_gbn;
    private String eva_typ;
    private String mt_dir;
    private String decimal_places;
    private String mt_desc;
    
    private String ac;
	public Logger getLogger() {
		return logger;
	}
	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	public String getMs_id() {
		return ms_id;
	}
	public void setMs_id(String ms_id) {
		this.ms_id = ms_id;
	}
	public String getAssign_id() {
		return assign_id;
	}
	public void setAssign_id(String assign_id) {
		this.assign_id = assign_id;
	}
	public String getMt_id() {
		return mt_id;
	}
	public void setMt_id(String mt_id) {
		this.mt_id = mt_id;
	}
	public String getStra_id() {
		return stra_id;
	}
	public void setStra_id(String stra_id) {
		this.stra_id = stra_id;
	}
	public String getSubstra_id() {
		return substra_id;
	}
	public void setSubstra_id(String substra_id) {
		this.substra_id = substra_id;
	}
	public String getMt_nm() {
		return mt_nm;
	}
	public void setMt_nm(String mt_nm) {
		this.mt_nm = mt_nm;
	}
	public String getCal_nm() {
		return cal_nm;
	}
	public void setCal_nm(String cal_nm) {
		this.cal_nm = cal_nm;
	}
	public String getMeas_cycle() {
		return meas_cycle;
	}
	public void setMeas_cycle(String meas_cycle) {
		this.meas_cycle = meas_cycle;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getEval_type() {
		return eval_type;
	}
	public void setEval_type(String eval_type) {
		this.eval_type = eval_type;
	}
	public String getMt_gbn() {
		return mt_gbn;
	}
	public void setMt_gbn(String mt_gbn) {
		this.mt_gbn = mt_gbn;
	}
	public String getEva_typ() {
		return eva_typ;
	}
	public void setEva_typ(String eva_typ) {
		this.eva_typ = eva_typ;
	}
	public String getMt_dir() {
		return mt_dir;
	}
	public void setMt_dir(String mt_dir) {
		this.mt_dir = mt_dir;
	}
	public String getDecimal_places() {
		return decimal_places;
	}
	public void setDecimal_places(String decimal_places) {
		this.decimal_places = decimal_places;
	}
	public String getMt_desc() {
		return mt_desc;
	}
	public void setMt_desc(String mt_desc) {
		this.mt_desc = mt_desc;
	}
	public String getAc() {
		return ac;
	}
	public void setAc(String ac) {
		this.ac = ac;
	}
    
	
}
