/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.imanager.dto;

import org.json.simple.JSONObject;

/**
 * <b>목표입력 기한관리</b> 
 */
public class kpiResult {
	
	private String userGubn;
	private String saveGubn;
	private String pEvaGbn;
	private String confirm;
	private String[] chargeYyyymm;
	private String[] chargeKpiID;
	private String[] chargeID;
	private String[] chargeName;
	private String[] userID;
	private String[] userName;
	private String[] chargeConfirm;
	private String[] userConfirm;
	private String[] chargeDate;
	private String[] userDate;
	private String[] tgtSeq;
	private String[] yyyy;
	private String[] mm;
	private String[] kpiID;
	private String[] analCycle;
	private String[] colGbn;
	private String[] colValue;
	private String[] yyyymm;
	
	private String chargeOneYyyymm;
	private String chargeOneKpiID;
	private String chargeOneID;
	private String chargeOneName;
	private String userOneID;
	private String userOneName;
	private String chargeOneConfirm;
	private String userOneConfirm;
	private String chargeOneDate;
	private String userOneDate;
	private String tgtSeqOne;
	private String yyyyOne;
	private String mmOne;
	private String anayCylceOne;
	private String targetAmtOne;
	
	
	
	
	
	
	
	
	public String getConfirm() {
		return confirm;
	}
	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}
	public String[] getColGbn() {
		return colGbn;
	}
	public void setColGbn(String[] colGbn) {
		this.colGbn = colGbn;
	}
	public String[] getColValue() {
		return colValue;
	}
	public void setColValue(String[] colValue) {
		this.colValue = colValue;
	}
	public String[] getYyyymm() {
		return yyyymm;
	}
	public void setYyyymm(String[] yyyymm) {
		this.yyyymm = yyyymm;
	}
	public String[] getChargeDate() {
		return chargeDate;
	}
	public void setChargeDate(String[] chargeDate) {
		this.chargeDate = chargeDate;
	}
	public String[] getUserDate() {
		return userDate;
	}
	public void setUserDate(String[] userDate) {
		this.userDate = userDate;
	}
	public String getChargeOneDate() {
		return chargeOneDate;
	}
	public void setChargeOneDate(String chargeOneDate) {
		this.chargeOneDate = chargeOneDate;
	}
	public String getUserOneDate() {
		return userOneDate;
	}
	public void setUserOneDate(String userOneDate) {
		this.userOneDate = userOneDate;
	}
	public String getChargeOneYyyymm() {
		return chargeOneYyyymm;
	}
	public void setChargeOneYyyymm(String chargeOneYyyymm) {
		this.chargeOneYyyymm = chargeOneYyyymm;
	}
	public String getUserGubn() {
		return userGubn;
	}
	public void setUserGubn(String userGubn) {
		this.userGubn = userGubn;
	}
	public String getChargeOneName() {
		return chargeOneName;
	}
	public void setChargeOneName(String chargeOneName) {
		this.chargeOneName = chargeOneName;
	}
	public String getUserOneName() {
		return userOneName;
	}
	public void setUserOneName(String userOneName) {
		this.userOneName = userOneName;
	}
	public String[] getChargeName() {
		return chargeName;
	}
	public void setChargeName(String[] chargeName) {
		this.chargeName = chargeName;
	}
	public String[] getUserName() {
		return userName;
	}
	public void setUserName(String[] userName) {
		this.userName = userName;
	}
	public String getSaveGubn() {
		return saveGubn;
	}
	public void setSaveGubn(String saveGubn) {
		this.saveGubn = saveGubn;
	}
	public String getpEvaGbn() {
		return pEvaGbn;
	}
	public void setpEvaGbn(String pEvaGbn) {
		this.pEvaGbn = pEvaGbn;
	}
	public String[] getChargeYyyymm() {
		return chargeYyyymm;
	}
	public void setChargeYyyymm(String[] chargeYyyymm) {
		this.chargeYyyymm = chargeYyyymm;
	}
	public String[] getChargeKpiID() {
		return chargeKpiID;
	}
	public void setChargeKpiID(String[] chargeKpiID) {
		this.chargeKpiID = chargeKpiID;
	}
	public String[] getChargeID() {
		return chargeID;
	}
	public void setChargeID(String[] chargeID) {
		this.chargeID = chargeID;
	}
	public String[] getUserID() {
		return userID;
	}
	public void setUserID(String[] userID) {
		this.userID = userID;
	}
	public String[] getChargeConfirm() {
		return chargeConfirm;
	}
	public void setChargeConfirm(String[] chargeConfirm) {
		this.chargeConfirm = chargeConfirm;
	}
	public String[] getUserConfirm() {
		return userConfirm;
	}
	public void setUserConfirm(String[] userConfirm) {
		this.userConfirm = userConfirm;
	}
	public String[] getTgtSeq() {
		return tgtSeq;
	}
	public void setTgtSeq(String[] tgtSeq) {
		this.tgtSeq = tgtSeq;
	}
	public String[] getYyyy() {
		return yyyy;
	}
	public void setYyyy(String[] yyyy) {
		this.yyyy = yyyy;
	}
	public String[] getMm() {
		return mm;
	}
	public void setMm(String[] mm) {
		this.mm = mm;
	}
	public String[] getKpiID() {
		return kpiID;
	}
	public void setKpiID(String[] kpiID) {
		this.kpiID = kpiID;
	}
	
	public String[] getAnalCycle() {
		return analCycle;
	}
	public void setAnalCycle(String[] analCycle) {
		this.analCycle = analCycle;
	}
	public String getChargeOneKpiID() {
		return chargeOneKpiID;
	}
	public void setChargeOneKpiID(String chargeOneKpiID) {
		this.chargeOneKpiID = chargeOneKpiID;
	}
	public String getChargeOneID() {
		return chargeOneID;
	}
	public void setChargeOneID(String chargeOneID) {
		this.chargeOneID = chargeOneID;
	}
	public String getUserOneID() {
		return userOneID;
	}
	public void setUserOneID(String userOneID) {
		this.userOneID = userOneID;
	}
	public String getChargeOneConfirm() {
		return chargeOneConfirm;
	}
	public void setChargeOneConfirm(String chargeOneConfirm) {
		this.chargeOneConfirm = chargeOneConfirm;
	}
	public String getUserOneConfirm() {
		return userOneConfirm;
	}
	public void setUserOneConfirm(String userOneConfirm) {
		this.userOneConfirm = userOneConfirm;
	}
	public String getTgtSeqOne() {
		return tgtSeqOne;
	}
	public void setTgtSeqOne(String tgtSeqOne) {
		this.tgtSeqOne = tgtSeqOne;
	}
	public String getYyyyOne() {
		return yyyyOne;
	}
	public void setYyyyOne(String yyyyOne) {
		this.yyyyOne = yyyyOne;
	}
	public String getMmOne() {
		return mmOne;
	}
	public void setMmOne(String mmOne) {
		this.mmOne = mmOne;
	}
	public String getAnayCylceOne() {
		return anayCylceOne;
	}
	public void setAnayCylceOne(String anayCylceOne) {
		this.anayCylceOne = anayCylceOne;
	}
	public String getTargetAmtOne() {
		return targetAmtOne;
	}
	public void setTargetAmtOne(String targetAmtOne) {
		this.targetAmtOne = targetAmtOne;
	}
}
