package com.iplanbiz.imanager.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory; 

public class AssignMent {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private String yyyy;
    private String ms_id;
    private String assign_id;
    private String kpi_id;
    private String stra_id;
    private String substra_id;
    private String assign_cd;
    private String assign_nm;
    private String dept_cd;
    private String dept_nm;
    private String subdept_cd;
    private String subdept_nm;
    private String start_dat;
    private String end_dat;
    private String assign_status;
    private String assign_content;
    private String assign_effect;
    private String assign_desc;
	private String assign_cycle;
	public Logger getLogger() {
		return logger;
	}
	public void setLogger(Logger logger) {
		this.logger = logger;
	}
	public String getYyyy() {
		return yyyy;
	}
	public void setYyyy(String yyyy) {
		this.yyyy = yyyy;
	}
	public String getMs_id() {
		return ms_id;
	}
	public void setMs_id(String ms_id) {
		this.ms_id = ms_id;
	}
	public String getAssign_id() {
		return assign_id;
	}
	public void setAssign_id(String assign_id) {
		this.assign_id = assign_id;
	}
	public String getKpi_id() {
		return kpi_id;
	}
	public void setKpi_id(String kpi_id) {
		this.kpi_id = kpi_id;
	}
	public String getStra_id() {
		return stra_id;
	}
	public void setStra_id(String stra_id) {
		this.stra_id = stra_id;
	}
	public String getSubstra_id() {
		return substra_id;
	}
	public void setSubstra_id(String substra_id) {
		this.substra_id = substra_id;
	}
	public String getAssign_cd() {
		return assign_cd;
	}
	public void setAssign_cd(String assign_cd) {
		this.assign_cd = assign_cd;
	}
	public String getAssign_nm() {
		return assign_nm;
	}
	public void setAssign_nm(String assign_nm) {
		this.assign_nm = assign_nm;
	}
	public String getDept_cd() {
		return dept_cd;
	}
	public void setDept_cd(String dept_cd) {
		this.dept_cd = dept_cd;
	}
	public String getDept_nm() {
		return dept_nm;
	}
	public void setDept_nm(String dept_nm) {
		this.dept_nm = dept_nm;
	}
	public String getSubdept_cd() {
		return subdept_cd;
	}
	public void setSubdept_cd(String subdept_cd) {
		this.subdept_cd = subdept_cd;
	}
	public String getSubdept_nm() {
		return subdept_nm;
	}
	public void setSubdept_nm(String subdept_nm) {
		this.subdept_nm = subdept_nm;
	}
	public String getStart_dat() {
		return start_dat;
	}
	public void setStart_dat(String start_dat) {
		this.start_dat = start_dat;
	}
	public String getEnd_dat() {
		return end_dat;
	}
	public void setEnd_dat(String end_dat) {
		this.end_dat = end_dat;
	}
	public String getAssign_status() {
		return assign_status;
	}
	public void setAssign_status(String assign_status) {
		this.assign_status = assign_status;
	}
	public String getAssign_content() {
		return assign_content;
	}
	public void setAssign_content(String assign_content) {
		this.assign_content = assign_content;
	}
	public String getAssign_effect() {
		return assign_effect;
	}
	public void setAssign_effect(String assign_effect) {
		this.assign_effect = assign_effect;
	}
	public String getAssign_desc() {
		return assign_desc;
	}
	public void setAssign_desc(String assign_desc) {
		this.assign_desc = assign_desc;
	}
	public String getAssign_cycle() {
		return assign_cycle;
	}
	public void setAssign_cycle(String assign_cycle) {
		this.assign_cycle = assign_cycle;
	}
	
	
	
	 

}
