package com.iplanbiz.imanager.dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class Program {


	private String ms_id;
	private String yyyy;
	private String program_id;
	private String program_nm;
	private String assign_id;

	private String kpi_id;
	private String dept_cd;
	private String dept_nm;
	private String emp_no;
	private String emp_nm;
	private String confirm_dept_cd;
	private String confirm_dept_nm;
	private String confirm_emp_no;
	private String confirm_emp_nm;
	private String eval_dept_cd;
	private String eval_dept_nm;
	private String eval_emp_no;
	private String eval_emp_nm;

	private String program_status;
	private String sch_start_dat;
	private String sch_end_dat;
	private String start_dat;
	private String end_dat;
	private String content;
	private String quan_goal;
	private String quan_unit;
	private String quan_content;
	private String qual_goal;
	private String summary;
	private String effect;
	private String result;
	private CommonsMultipartFile[] planFile;	//파일
	private CommonsMultipartFile[] runFile;	//파일
	//예산
	private String[] budget_id;
	private String[] budget_content;
	private String[] budget_typ;
	private String[] budget_start_dat;
	private String[] budget_end_dat;
	private String[] budget_amount;
	private String[] budget_def;
	//집행
	private String[] enforce03;
	private String[] enforce04;
	private String[] enforce05;
	private String[] enforce06;
	private String[] enforce07;
	private String[] enforce08;
	private String[] enforce09;
	private String[] enforce10;
	private String[] enforce11;
	private String[] enforce12;
	private String[] enforce01;
	private String[] enforce02;
	
	private String[] enforce_id;
	private String[] enforce_content;
	private String[] enforce_def;
	private String[] enforce_start_dat;
	private String[] enforce_end_dat;
	private String[] enforce_amount;

	public String getMs_id() {
		return ms_id;
	}

	public void setMs_id(String ms_id) {
		this.ms_id = ms_id;
	}

	public String getProgram_id() {
		return program_id;
	}

	public void setProgram_id(String program_id) {
		this.program_id = program_id;
	}

	public String getProgram_nm() {
		return program_nm;
	}

	public void setProgram_nm(String program_nm) {
		this.program_nm = program_nm;
	}

	public String getAssign_id() {
		return assign_id;
	}

	public void setAssign_id(String assign_id) {
		this.assign_id = assign_id;
	}

	public String getKpi_id() {
		return kpi_id;
	}

	public void setKpi_id(String kpi_id) {
		this.kpi_id = kpi_id;
	}

	public String getDept_cd() {
		return dept_cd;
	}

	public void setDept_cd(String dept_cd) {
		this.dept_cd = dept_cd;
	}

	public String getDept_nm() {
		return dept_nm;
	}

	public void setDept_nm(String dept_nm) {
		this.dept_nm = dept_nm;
	}

	public String getEmp_no() {
		return emp_no;
	}

	public void setEmp_no(String emp_no) {
		this.emp_no = emp_no;
	}

	public String getEmp_nm() {
		return emp_nm;
	}

	public void setEmp_nm(String emp_nm) {
		this.emp_nm = emp_nm;
	}

	public String getConfirm_dept_cd() {
		return confirm_dept_cd;
	}

	public void setConfirm_dept_cd(String confirm_dept_cd) {
		this.confirm_dept_cd = confirm_dept_cd;
	}

	public String getConfirm_dept_nm() {
		return confirm_dept_nm;
	}

	public void setConfirm_dept_nm(String confirm_dept_nm) {
		this.confirm_dept_nm = confirm_dept_nm;
	}

	public String getConfirm_emp_no() {
		return confirm_emp_no;
	}

	public void setConfirm_emp_no(String confirm_emp_no) {
		this.confirm_emp_no = confirm_emp_no;
	}

	public String getConfirm_emp_nm() {
		return confirm_emp_nm;
	}

	public void setConfirm_emp_nm(String confirm_emp_nm) {
		this.confirm_emp_nm = confirm_emp_nm;
	}

	public String getEval_dept_cd() {
		return eval_dept_cd;
	}

	public void setEval_dept_cd(String eval_dept_cd) {
		this.eval_dept_cd = eval_dept_cd;
	}

	public String getEval_dept_nm() {
		return eval_dept_nm;
	}

	public void setEval_dept_nm(String eval_dept_nm) {
		this.eval_dept_nm = eval_dept_nm;
	}

	public String getEval_emp_no() {
		return eval_emp_no;
	}

	public void setEval_emp_no(String eval_emp_no) {
		this.eval_emp_no = eval_emp_no;
	}

	public String getEval_emp_nm() {
		return eval_emp_nm;
	}

	public void setEval_emp_nm(String eval_emp_nm) {
		this.eval_emp_nm = eval_emp_nm;
	}

	public String getProgram_status() {
		return program_status;
	}

	public void setProgram_status(String program_status) {
		this.program_status = program_status;
	}

	public String getSch_start_dat() {
		return sch_start_dat;
	}

	public void setSch_start_dat(String sch_start_dat) {
		this.sch_start_dat = sch_start_dat;
	}

	public String getSch_end_dat() {
		return sch_end_dat;
	}

	public void setSch_end_dat(String sch_end_dat) {
		this.sch_end_dat = sch_end_dat;
	}

	public String getStart_dat() {
		return start_dat;
	}

	public void setStart_dat(String start_dat) {
		this.start_dat = start_dat;
	}

	public String getEnd_dat() {
		return end_dat;
	}

	public void setEnd_dat(String end_dat) {
		this.end_dat = end_dat;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getQuan_goal() {
		return quan_goal;
	}

	public void setQuan_goal(String quan_goal) {
		this.quan_goal = quan_goal;
	}

	public String getQuan_unit() {
		return quan_unit;
	}

	public void setQuan_unit(String quan_unit) {
		this.quan_unit = quan_unit;
	}

	public String getQuan_content() {
		return quan_content;
	}

	public void setQuan_content(String quan_content) {
		this.quan_content = quan_content;
	}

	public String getQual_goal() {
		return qual_goal;
	}

	public void setQual_goal(String qual_goal) {
		this.qual_goal = qual_goal;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String[] getBudget_id() {
		return budget_id;
	}

	public void setBudget_id(String[] budget_id) {
		this.budget_id = budget_id;
	}

	public String[] getBudget_content() {
		return budget_content;
	}

	public void setBudget_content(String[] budget_content) {
		this.budget_content = budget_content;
	}

	public String[] getBudget_typ() {
		return budget_typ;
	}

	public void setBudget_typ(String[] budget_typ) {
		this.budget_typ = budget_typ;
	}

	public String[] getBudget_start_dat() {
		return budget_start_dat;
	}

	public void setBudget_start_dat(String[] budget_start_dat) {
		this.budget_start_dat = budget_start_dat;
	}

	public String[] getBudget_end_dat() {
		return budget_end_dat;
	}

	public void setBudget_end_dat(String[] budget_end_dat) {
		this.budget_end_dat = budget_end_dat;
	}

	public String[] getBudget_amount() {
		return budget_amount;
	}

	public void setBudget_amount(String[] budget_amount) {
		this.budget_amount = budget_amount;
	}

	public CommonsMultipartFile[] getPlanFile() {
		return planFile;
	}

	public void setPlanFile(CommonsMultipartFile[] planFile) {
		this.planFile = planFile;
	}

	public CommonsMultipartFile[] getRunFile() {
		return runFile;
	}

	public void setRunFile(CommonsMultipartFile[] runFile) {
		this.runFile = runFile;
	}

	public String[] getEnforce_id() {
		return enforce_id;
	}

	public void setEnforce_id(String[] enforce_id) {
		this.enforce_id = enforce_id;
	}

	public String[] getEnforce_content() {
		return enforce_content;
	}

	public void setEnforce_content(String[] enforce_content) {
		this.enforce_content = enforce_content;
	}

	public String[] getEnforce_def() {
		return enforce_def;
	}

	public void setEnforce_def(String[] enforce_def) {
		this.enforce_def = enforce_def;
	}

	public String[] getEnforce_start_dat() {
		return enforce_start_dat;
	}

	public void setEnforce_start_dat(String[] enforce_start_dat) {
		this.enforce_start_dat = enforce_start_dat;
	}

	public String[] getEnforce_end_dat() {
		return enforce_end_dat;
	}

	public void setEnforce_end_dat(String[] enforce_end_dat) {
		this.enforce_end_dat = enforce_end_dat;
	}

	public String[] getEnforce_amount() {
		return enforce_amount;
	}

	public void setEnforce_amount(String[] enforce_amount) {
		this.enforce_amount = enforce_amount;
	}

	public String[] getBudget_def() {
		return budget_def;
	}

	public void setBudget_def(String[] budget_def) {
		this.budget_def = budget_def;
	}

	public String getYyyy() {
		return yyyy;
	}

	public void setYyyy(String yyyy) {
		this.yyyy = yyyy;
	}

	public String[] getEnforce03() {
		return enforce03;
	}

	public void setEnforce03(String[] enforce03) {
		this.enforce03 = enforce03;
	}

	public String[] getEnforce04() {
		return enforce04;
	}

	public void setEnforce04(String[] enforce04) {
		this.enforce04 = enforce04;
	}

	public String[] getEnforce05() {
		return enforce05;
	}

	public void setEnforce05(String[] enforce05) {
		this.enforce05 = enforce05;
	}

	public String[] getEnforce06() {
		return enforce06;
	}

	public void setEnforce06(String[] enforce06) {
		this.enforce06 = enforce06;
	}

	public String[] getEnforce07() {
		return enforce07;
	}

	public void setEnforce07(String[] enforce07) {
		this.enforce07 = enforce07;
	}

	public String[] getEnforce08() {
		return enforce08;
	}

	public void setEnforce08(String[] enforce08) {
		this.enforce08 = enforce08;
	}

	public String[] getEnforce09() {
		return enforce09;
	}

	public void setEnforce09(String[] enforce09) {
		this.enforce09 = enforce09;
	}

	public String[] getEnforce10() {
		return enforce10;
	}

	public void setEnforce10(String[] enforce10) {
		this.enforce10 = enforce10;
	}

	public String[] getEnforce11() {
		return enforce11;
	}

	public void setEnforce11(String[] enforce11) {
		this.enforce11 = enforce11;
	}

	public String[] getEnforce12() {
		return enforce12;
	}

	public void setEnforce12(String[] enforce12) {
		this.enforce12 = enforce12;
	}

	public String[] getEnforce01() {
		return enforce01;
	}

	public void setEnforce01(String[] enforce01) {
		this.enforce01 = enforce01;
	}

	public String[] getEnforce02() {
		return enforce02;
	}

	public void setEnforce02(String[] enforce02) {
		this.enforce02 = enforce02;
	}


}
