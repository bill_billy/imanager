package com.iplanbiz.imanager.dto;

public class DeptEval {
	
	private String ms_id;
	private String program_id;
	private String yyyy;
	private String quan_ach_val;
	private String quan_def;
	private String quan_content;
	private String qual_ach_yn;
	private String qual_content;
	private String cnt_tot;
	private String cnt_a;
	private String cnt_b;
	private String cnt_c;
	private String cnt_d;
	private String cnt_e;
	private String eval_score;
	private String eval_result;
	public String getMs_id() {
		return ms_id;
	}
	public void setMs_id(String ms_id) {
		this.ms_id = ms_id;
	}
	public String getProgram_id() {
		return program_id;
	}
	public void setProgram_id(String program_id) {
		this.program_id = program_id;
	}
	public String getYyyy() {
		return yyyy;
	}
	public void setYyyy(String yyyy) {
		this.yyyy = yyyy;
	}
	public String getQuan_ach_val() {
		return quan_ach_val;
	}
	public void setQuan_ach_val(String quan_ach_val) {
		this.quan_ach_val = quan_ach_val;
	}
	public String getQuan_def() {
		return quan_def;
	}
	public void setQuan_def(String quan_def) {
		this.quan_def = quan_def;
	}
	public String getQuan_content() {
		return quan_content;
	}
	public void setQuan_content(String quan_content) {
		this.quan_content = quan_content;
	}
	public String getQual_ach_yn() {
		return qual_ach_yn;
	}
	public void setQual_ach_yn(String qual_ach_yn) {
		this.qual_ach_yn = qual_ach_yn;
	}
	public String getQual_content() {
		return qual_content;
	}
	public void setQual_content(String qual_content) {
		this.qual_content = qual_content;
	}
	public String getCnt_tot() {
		return cnt_tot;
	}
	public void setCnt_tot(String cnt_tot) {
		this.cnt_tot = cnt_tot;
	}
	public String getCnt_a() {
		return cnt_a;
	}
	public void setCnt_a(String cnt_a) {
		this.cnt_a = cnt_a;
	}
	public String getCnt_b() {
		return cnt_b;
	}
	public void setCnt_b(String cnt_b) {
		this.cnt_b = cnt_b;
	}
	public String getCnt_c() {
		return cnt_c;
	}
	public void setCnt_c(String cnt_c) {
		this.cnt_c = cnt_c;
	}
	public String getCnt_d() {
		return cnt_d;
	}
	public void setCnt_d(String cnt_d) {
		this.cnt_d = cnt_d;
	}
	public String getCnt_e() {
		return cnt_e;
	}
	public void setCnt_e(String cnt_e) {
		this.cnt_e = cnt_e;
	}
	public String getEval_score() {
		return eval_score;
	}
	public void setEval_score(String eval_score) {
		this.eval_score = eval_score;
	}
	public String getEval_result() {
		return eval_result;
	}
	public void setEval_result(String eval_result) {
		this.eval_result = eval_result;
	}
	 

}
