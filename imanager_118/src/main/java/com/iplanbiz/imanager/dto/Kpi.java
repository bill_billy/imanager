package com.iplanbiz.imanager.dto;

import org.json.simple.JSONArray; 
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory; 

public class Kpi {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	private String yyyy;
    private String mtid;
    private String mtcd;
    
	private String mtnm;
    private String mtdir;
    private String mtdef; 
    private String[] mm;
    private String substraid;
    private String straid;
    private String perscd;
    private String meascycle;
    private String unit;
    private String evaltype;
    private String startdat;
    private String enddat;
    private String evatyp;
    private String[] relatedWork1;		//유관업무 중
    private String[] relatedWork2;		//유관업무 소
    private String mtgbn;
    private String decimalplaces;
    private String mtudc2;
    private String orgscid; 
    private String scid;
    private String targetdesc;
    private String mtdesc;  			
    private String mtUserID;			//지표책임자의 ID
    private String mtUserDept;			//지표책임자의 부서
    private float kpiWeight;			//가중치(대학지표)
	private String calculateList;		//년도별 산식 JSON String
	private String calText;				//산출공식 Text
	private String parentKpiID;			//대학 KPI(부서 지표)
	private String[] yyyymm;
	private String[] kpiID;
	private String[] analCycle;
	private String[] colGbn;
	private String[] colValue;
	
	//담당자,승인자 모두확인&취소
	private String[] chargeID;
	private String[] chargeName;
	private String[] userID;
	private String[] userName;
	private String[] chargeConfirm;
	private String[] userConfirm;
	private String[] chargeDate;
	private String[] userDate;
	
	private String pEvaGbn;
	private String csfID;
	private String csfNm;
	
	private String userGubn;
	private String saveGubn;
	private String confirm;
	private String chargeOneYyyymm;
	private String chargeOneKpiID;
	private String chargeOneID;
	private String chargeOneName;
	private String userOneID;
	private String userOneName;
	private String chargeOneConfirm;
	private String userOneConfirm;
	private String chargeOneDate;
	private String userOneDate;
	
	//(담당자)실적관리 저장
	private String[] colYyyymm;
	private String[] colKpiID;
	private String[] colAnalcycle;
	private String[] colColgbn;
	
	
	
	
	
	
	
	
	
	public String[] getColYyyymm() {
		return colYyyymm;
	}
	public void setColYyyymm(String[] colYyyymm) {
		this.colYyyymm = colYyyymm;
	}
	public String[] getColKpiID() {
		return colKpiID;
	}
	public void setColKpiID(String[] colKpiID) {
		this.colKpiID = colKpiID;
	}
	public String[] getColAnalcycle() {
		return colAnalcycle;
	}
	public void setColAnalcycle(String[] colAnalcycle) {
		this.colAnalcycle = colAnalcycle;
	}
	public String[] getColColgbn() {
		return colColgbn;
	}
	public void setColColgbn(String[] colColgbn) {
		this.colColgbn = colColgbn;
	}
	public String[] getChargeID() {
		return chargeID;
	}
	public void setChargeID(String[] chargeID) {
		this.chargeID = chargeID;
	}
	public String[] getChargeName() {
		return chargeName;
	}
	public void setChargeName(String[] chargeName) {
		this.chargeName = chargeName;
	}
	public String[] getUserID() {
		return userID;
	}
	public void setUserID(String[] userID) {
		this.userID = userID;
	}
	public String[] getUserName() {
		return userName;
	}
	public void setUserName(String[] userName) {
		this.userName = userName;
	}
	public String[] getChargeConfirm() {
		return chargeConfirm;
	}
	public void setChargeConfirm(String[] chargeConfirm) {
		this.chargeConfirm = chargeConfirm;
	}
	public String[] getUserConfirm() {
		return userConfirm;
	}
	public void setUserConfirm(String[] userConfirm) {
		this.userConfirm = userConfirm;
	}
	public String[] getChargeDate() {
		return chargeDate;
	}
	public void setChargeDate(String[] chargeDate) {
		this.chargeDate = chargeDate;
	}
	public String[] getUserDate() {
		return userDate;
	}
	public void setUserDate(String[] userDate) {
		this.userDate = userDate;
	}
	public String getUserGubn() {
		return userGubn;
	}
	public void setUserGubn(String userGubn) {
		this.userGubn = userGubn;
	}
	public String getSaveGubn() {
		return saveGubn;
	}
	public void setSaveGubn(String saveGubn) {
		this.saveGubn = saveGubn;
	}
	public String getConfirm() {
		return confirm;
	}
	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}
	public String getChargeOneYyyymm() {
		return chargeOneYyyymm;
	}
	public void setChargeOneYyyymm(String chargeOneYyyymm) {
		this.chargeOneYyyymm = chargeOneYyyymm;
	}
	public String getChargeOneKpiID() {
		return chargeOneKpiID;
	}
	public void setChargeOneKpiID(String chargeOneKpiID) {
		this.chargeOneKpiID = chargeOneKpiID;
	}
	public String getChargeOneID() {
		return chargeOneID;
	}
	public void setChargeOneID(String chargeOneID) {
		this.chargeOneID = chargeOneID;
	}
	public String getChargeOneName() {
		return chargeOneName;
	}
	public void setChargeOneName(String chargeOneName) {
		this.chargeOneName = chargeOneName;
	}
	public String getUserOneID() {
		return userOneID;
	}
	public void setUserOneID(String userOneID) {
		this.userOneID = userOneID;
	}
	public String getUserOneName() {
		return userOneName;
	}
	public void setUserOneName(String userOneName) {
		this.userOneName = userOneName;
	}
	public String getChargeOneConfirm() {
		return chargeOneConfirm;
	}
	public void setChargeOneConfirm(String chargeOneConfirm) {
		this.chargeOneConfirm = chargeOneConfirm;
	}
	public String getUserOneConfirm() {
		return userOneConfirm;
	}
	public void setUserOneConfirm(String userOneConfirm) {
		this.userOneConfirm = userOneConfirm;
	}
	public String getChargeOneDate() {
		return chargeOneDate;
	}
	public void setChargeOneDate(String chargeOneDate) {
		this.chargeOneDate = chargeOneDate;
	}
	public String getUserOneDate() {
		return userOneDate;
	}
	public void setUserOneDate(String userOneDate) {
		this.userOneDate = userOneDate;
	}
	public String getMtcd() {
		return mtcd;
	}
	public void setMtcd(String mtcd) {
		this.mtcd = mtcd;
	}
	public String getCsfID() {
		return csfID;
	}
	public void setCsfID(String csfID) {
		this.csfID = csfID;
	}
	public String getCsfNm() {
		return csfNm;
	}
	public void setCsfNm(String csfNm) {
		this.csfNm = csfNm;
	}
	public String[] getYyyymm() {
		return yyyymm;
	}
	public void setYyyymm(String[] yyyymm) {
		this.yyyymm = yyyymm;
	}
	public String[] getKpiID() {
		return kpiID;
	}
	public void setKpiID(String[] kpiID) {
		this.kpiID = kpiID;
	}
	public String[] getAnalCycle() {
		return analCycle;
	}
	public void setAnalCycle(String[] analCycle) {
		this.analCycle = analCycle;
	}
	public String[] getColGbn() {
		return colGbn;
	}
	public void setColGbn(String[] colGbn) {
		this.colGbn = colGbn;
	}
	public String[] getColValue() {
		return colValue;
	}
	public void setColValue(String[] colValue) {
		this.colValue = colValue;
	}
	public String getpEvaGbn() {
		return pEvaGbn;
	}
	public void setpEvaGbn(String pEvaGbn) {
		this.pEvaGbn = pEvaGbn;
	}
	public String getCalText() {
		return calText;
	}
	public void setCalText(String calText) {
		this.calText = calText;
	}
	public String getParentKpiID() {
		return parentKpiID;
	}
	public void setParentKpiID(String parentKpiID) {
		this.parentKpiID = parentKpiID;
	}
	public float getKpiWeight() {
		return kpiWeight;
	}
	public void setKpiWeight(float kpiWeight) {
		this.kpiWeight = kpiWeight;
	}
	public String getYyyy() {
		return yyyy;
	}
	public void setYyyy(String yyyy) {
		this.yyyy = yyyy;
	}
	public String getMtid() {
		return mtid;
	}
	public void setMtid(String mtid) {
		this.mtid = mtid;
	}
	public String getMtnm() {
		return mtnm;
	}
	public void setMtnm(String mtnm) {
		this.mtnm = mtnm;
	}
	public String getMtdir() {
		return mtdir;
	}
	public void setMtdir(String mtdir) {
		this.mtdir = mtdir;
	}
	public String getMtdef() {
		return mtdef;
	}
	public void setMtdef(String mtdef) {
		this.mtdef = mtdef;
	} 
	public String[] getMm() {
		return mm;
	}
	public void setMm(String[] mm) {
		this.mm = mm;
	}
	public String getSubstraid() {
		return substraid;
	}
	public void setSubstraid(String substraid) {
		this.substraid = substraid;
	}
	public String getStraid() {
		return straid;
	}
	public void setStraid(String straid) {
		this.straid = straid;
	}
	public String getPerscd() {
		return perscd;
	}
	public void setPerscd(String perscd) {
		this.perscd = perscd;
	}
	public String getMeascycle() {
		return meascycle;
	}
	public void setMeascycle(String meascycle) {
		this.meascycle = meascycle;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getEvaltype() {
		return evaltype;
	}
	public void setEvaltype(String evaltype) {
		this.evaltype = evaltype;
	}
	public String getStartdat() {
		return startdat;
	}
	public void setStartdat(String startdat) {
		this.startdat = startdat;
	}
	public String getEnddat() {
		return enddat;
	}
	public void setEnddat(String enddat) {
		this.enddat = enddat;
	}
	public String getEvatyp() {
		return evatyp;
	}
	public void setEvatyp(String evatyp) {
		this.evatyp = evatyp;
	}
	public String[] getRelatedWork1() {
		return relatedWork1;
	}
	public void setRelatedWork1(String[] relatedWork1) {
		this.relatedWork1 = relatedWork1;
	}
	public String[] getRelatedWork2() {
		return relatedWork2;
	}
	public void setRelatedWork2(String[] relatedWork2) {
		this.relatedWork2 = relatedWork2;
	}
	public String getMtgbn() {
		return mtgbn;
	}
	public void setMtgbn(String mtgbn) {
		this.mtgbn = mtgbn;
	}
	public String getDecimalplaces() {
		return decimalplaces;
	}
	public void setDecimalplaces(String decimalplaces) {
		this.decimalplaces = decimalplaces;
	}
	public String getMtudc2() {
		return mtudc2;
	}
	public void setMtudc2(String mtudc2) {
		this.mtudc2 = mtudc2;
	}
	public String getOrgscid() {
		return orgscid;
	}
	public void setOrgscid(String orgscid) {
		this.orgscid = orgscid;
	}
	public String getScid() {
		return scid;
	}
	public void setScid(String scid) {
		this.scid = scid;
	}
	public String getTargetdesc() {
		return targetdesc;
	}
	public void setTargetdesc(String targetdesc) {
		this.targetdesc = targetdesc;
	}
	public String getMtdesc() {
		return mtdesc;
	}
	public void setMtdesc(String mtdesc) {
		this.mtdesc = mtdesc;
	} 
	/**
	 * @return the mtUserDept
	 */
	public String getMtUserDept() {
		return mtUserDept;
	}
	/**
	 * @param mtUserDept the mtUserDept to set
	 */
	public void setMtUserDept(String mtUserDept) {
		this.mtUserDept = mtUserDept;
	}
	/**
	 * @return the mtUserID
	 */
	public String getMtUserID() {
		return mtUserID;
	}
	/**
	 * @param mtUserID the mtUserID to set
	 */
	public void setMtUserID(String mtUserID) {
		this.mtUserID = mtUserID;
	}
	/**
	 * @return the calculateList
	 */
	public String getCalculateList() {
		return this.calculateList;
		/*
		JSONParser parser = new JSONParser();
		JSONArray arr;
		try {
			arr = (JSONArray)parser.parse(this.calculateList);
		} catch (ParseException e) {
			arr = new JSONArray();
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return arr;
		*/
	}
	/**
	 * @param calculateList the calculateList to set
	 */
	public void setCalculateList(String calculateList)  {
		logger.debug("setCalculateList : {}",calculateList);
		this.calculateList = calculateList;
	}
	 

}
