/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.imanager.dto;

import org.json.simple.JSONObject;

/**
 * <b>목표입력 기한관리</b> 
 */
public class ValueDueDate {

	private String evaGbn;			// 평가구분
	private String yyyymm;			// 년월
	private String inputStart;		// 입력 시작일
	private String inputClose;		// 입력 종료일
	private String confirmClose;	// 확인 종료일
	
	public String getEvaGbn() {
		return evaGbn;
	}
	public void setEvaGbn(String evaGbn) {
		this.evaGbn = evaGbn;
	}
	public String getYyyymm() {
		return yyyymm;
	}
	public void setYyyymm(String yyyymm) {
		this.yyyymm = yyyymm;
	}
	public String getInputStart() {
		return inputStart;
	}
	public void setInputStart(String inputStart) {
		this.inputStart = inputStart;
	}
	public String getInputClose() {
		return inputClose;
	}
	public void setInputClose(String inputClose) {
		this.inputClose = inputClose;
	}
	public String getConfirmClose() {
		return confirmClose;
	}
	public void setConfirmClose(String confirmClose) {
		this.confirmClose = confirmClose;
	}
	
	
	
	@Override
	public String toString() {
		return "ValueDueDate [evaGbn=" + evaGbn + ", yyyymm=" + yyyymm + ", inputStart=" + inputStart + ", inputClose="
				+ inputClose + ", confirmClose=" + confirmClose + ", getJSONObject()=" + getJSONObject() + "]";
	}
	public JSONObject getJSONObject() {
		
		JSONObject object = new JSONObject();
		object.put("evaGbn", this.evaGbn);
		object.put("yyyymm", this.yyyymm);
		object.put("inputStart", this.inputStart);
		object.put("inputClose", this.inputClose);
		object.put("confirmClose", this.confirmClose);
		
		return object;
	}
	
}
