package com.iplanbiz.imanager.dto;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

public class KpiAttachFileUser {
	
	private String year;
	private String month;
	private String scid;
	private String kpiid;
	private CommonsMultipartFile[] attachFile;
	private String[] 			   fileId;				// 파일 NO      
	private String fileSaveName;						// 서버에 저장된 파일명
	private String fileOrgName;							// 원본 파일명
	private String filePath;							// 파일경로
	private long fileSize;								// 파일크기
	private String fileExt;								// 파일 확장자
	
	
	
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getScid() {
		return scid;
	}
	public void setScid(String scid) {
		this.scid = scid;
	}
	public String getKpiid() {
		return kpiid;
	}
	public void setKpiid(String kpiid) {
		this.kpiid = kpiid;
	}
	public CommonsMultipartFile[] getAttachFile() {
		return attachFile;
	}
	public void setAttachFile(CommonsMultipartFile[] attachFile) {
		this.attachFile = attachFile;
	}
	public String[] getFileId() {
		return fileId;
	}
	public void setFileId(String[] fileId) {
		this.fileId = fileId;
	}
	public String getFileSaveName() {
		return fileSaveName;
	}
	public void setFileSaveName(String fileSaveName) {
		this.fileSaveName = fileSaveName;
	}
	public String getFileOrgName() {
		return fileOrgName;
	}
	public void setFileOrgName(String fileOrgName) {
		this.fileOrgName = fileOrgName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public long getFileSize() {
		return fileSize;
	}
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}
	public String getFileExt() {
		return fileExt;
	}
	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}
	 
	
	
	

} 