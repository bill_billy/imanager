package com.iplanbiz.imanager.dto;

public class CommitteeEval {
	private String ac;
	private String ms_id;
	private String yyyy;
	private String assign_id;
	private String emp_id;
	private String good_content;
	private String improve_content;
	private String eval_result;
	private String eval_status;
	private String[] program_id;
	private String[] p_good_content;
	private String[] p_improve_content;
	private String[] p_best_content;
	private String[] p_eval_score;
	public String getMs_id() {
		return ms_id;
	}
	public void setMs_id(String ms_id) {
		this.ms_id = ms_id;
	}
	public String getYyyy() {
		return yyyy;
	}
	public void setYyyy(String yyyy) {
		this.yyyy = yyyy;
	}
	public String getAssign_id() {
		return assign_id;
	}
	public void setAssign_id(String assign_id) {
		this.assign_id = assign_id;
	}
	public String getGood_content() {
		return good_content;
	}
	public void setGood_content(String good_content) {
		this.good_content = good_content;
	}
	public String getImprove_content() {
		return improve_content;
	}
	public void setImprove_content(String improve_content) {
		this.improve_content = improve_content;
	}
	public String getEval_result() {
		return eval_result;
	}
	public void setEval_result(String eval_result) {
		this.eval_result = eval_result;
	}
	public String[] getProgram_id() {
		return program_id;
	}
	public void setProgram_id(String[] program_id) {
		this.program_id = program_id;
	}
	public String[] getP_good_content() {
		return p_good_content;
	}
	public void setP_good_content(String[] p_good_content) {
		this.p_good_content = p_good_content;
	}
	public String[] getP_improve_content() {
		return p_improve_content;
	}
	public void setP_improve_content(String[] p_improve_content) {
		this.p_improve_content = p_improve_content;
	}
	public String[] getP_best_content() {
		return p_best_content;
	}
	public void setP_best_content(String[] p_best_content) {
		this.p_best_content = p_best_content;
	}
	
	public String getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(String emp_id) {
		this.emp_id = emp_id;
	}
	public String getEval_status() {
		return eval_status;
	}
	public void setEval_status(String eval_status) {
		this.eval_status = eval_status;
	}
	public String[] getP_eval_score() {
		return p_eval_score;
	}
	public void setP_eval_score(String[] p_eval_score) {
		this.p_eval_score = p_eval_score;
	}
	public String getAc() {
		return ac;
	}
	public void setAc(String ac) {
		this.ac = ac;
	}
	
	 

}
