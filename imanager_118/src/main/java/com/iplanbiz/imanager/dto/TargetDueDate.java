/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.imanager.dto;

import org.json.simple.JSONObject;

/**
 * <b>목표입력 기한관리</b> 
 */
public class TargetDueDate {

	private String evaGbn;			// 평가구분
	private String year;			// 학년도
	private String targetSeq;		// 차수
	private String inputStart;		// 입력 시작일
	private String inputClose;		// 입력 종료일
	private String confirmClose;	// 확인 종료일
	
	public String getEvaGbn() {
		return evaGbn;
	}
	public void setEvaGbn(String evaGbn) {
		this.evaGbn = evaGbn;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getTargetSeq() {
		return targetSeq;
	}
	public void setTargetSeq(String targetSeq) {
		this.targetSeq = targetSeq;
	}
	public String getInputStart() {
		return inputStart;
	}
	public void setInputStart(String inputStart) {
		this.inputStart = inputStart;
	}
	public String getInputClose() {
		return inputClose;
	}
	public void setInputClose(String inputClose) {
		this.inputClose = inputClose;
	}
	public String getConfirmClose() {
		return confirmClose;
	}
	public void setConfirmClose(String confirmClose) {
		this.confirmClose = confirmClose;
	}
	
	@Override
	public String toString() {
		return "TargetDueDate [evaGbn=" + evaGbn + ", year=" + year + ", targetSeq=" + targetSeq + ", inputStart="
				+ inputStart + ", inputClose=" + inputClose + ", confirmClose=" + confirmClose + "]";
	}
	
	public JSONObject getJSONObject() {
		
		JSONObject object = new JSONObject();
		object.put("evaGbn", this.evaGbn);
		object.put("year", this.year);
		object.put("targetSeq", this.targetSeq);
		object.put("inputStart", this.inputStart);
		object.put("inputClose", this.inputClose);
		object.put("confirmClose", this.confirmClose);
		
		return object;
	}
}
