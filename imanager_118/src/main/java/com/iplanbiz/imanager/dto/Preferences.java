/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.imanager.dto;

import java.util.List;

/**
 * <b>평가시기 설정</b> 
 */
public class Preferences {
	
	private String[] evaGbn;
	private String[] mon1;
	private String[] mon2;
	private String[] mon3;
	private String[] mon4;
	private String[] mon5;
	private String[] mon6;
	private String[] mon7;
	private String[] mon8;
	private String[] mon9;
	private String[] mon10;
	private String[] mon11;
	private String[] mon12;

	@Override
	public String toString() {
		return "Preferences [evaGbn=" + evaGbn + ", mon1=" + mon1
				+ ", mon2=" + mon2 + ", mon3=" + mon3 + ", mon4=" + mon4
				+ ", mon5=" + mon5 + ", mon6=" + mon6 + ", mon7=" + mon7
				+ ", mon8=" + mon8 + ", mon9=" + mon9 + ", mon10=" + mon10
				+ ", mon11=" + mon11 + ", mon12=" + mon12 + "]";
	}

	public String[] getEvaGbn() {
		return evaGbn;
	}
	public void setEvaGbn(String[] evaGbn) {
		this.evaGbn = evaGbn;
	}
	public String[] getMon1() {
		return mon1;
	}
	public void setMon1(String[] mon1) {
		this.mon1 = mon1;
	}
	public String[] getMon2() {
		return mon2;
	}
	public void setMon2(String[] mon2) {
		this.mon2 = mon2;
	}
	public String[] getMon3() {
		return mon3;
	}
	public void setMon3(String[] mon3) {
		this.mon3 = mon3;
	}
	public String[] getMon4() {
		return mon4;
	}
	public void setMon4(String[] mon4) {
		this.mon4 = mon4;
	}
	public String[] getMon5() {
		return mon5;
	}
	public void setMon5(String[] mon5) {
		this.mon5 = mon5;
	}
	public String[] getMon6() {
		return mon6;
	}
	public void setMon6(String[] mon6) {
		this.mon6 = mon6;
	}
	public String[] getMon7() {
		return mon7;
	}
	public void setMon7(String[] mon7) {
		this.mon7 = mon7;
	}
	public String[] getMon8() {
		return mon8;
	}
	public void setMon8(String[] mon8) {
		this.mon8 = mon8;
	}
	public String[] getMon9() {
		return mon9;
	}
	public void setMon9(String[] mon9) {
		this.mon9 = mon9;
	}
	public String[] getMon10() {
		return mon10;
	}
	public void setMon10(String[] mon10) {
		this.mon10 = mon10;
	}
	public String[] getMon11() {
		return mon11;
	}
	public void setMon11(String[] mon11) {
		this.mon11 = mon11;
	}
	public String[] getMon12() {
		return mon12;
	}
	public void setMon12(String[] mon12) {
		this.mon12 = mon12;
	}

}
