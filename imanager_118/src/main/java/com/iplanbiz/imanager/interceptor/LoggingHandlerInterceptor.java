/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.imanager.interceptor;
 
import java.util.Enumeration; 

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;  

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory; 
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.web.servlet.ModelAndView; 
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter; 

import com.iplanbiz.core.secure.*; 
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.config.WebConfig;
 

/**
 * @author : 장민수
 * @date   : 2016. 10. 19.
 * @desc   : 
 */
public class LoggingHandlerInterceptor extends HandlerInterceptorAdapter {
	
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource(name="loginSessionInfoFactory") ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
 
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object obj) throws Exception {
    	
    	response.setHeader("P3P","CP='CAO PSA CONi OTR OUR DEM ONL'");
    	this.printRequest(request);
    	if(request.getRequestURI().indexOf("/session") > -1) return true;
		if(loginSessionInfoFactory.getObject().isLogin()){
			return true;
		}
		else{
			Crypto cs = new Crypto();
    		logger.info("PAGEACCESS_FAIL");  	
    		cs.setSecretKey("ioneiplan");
    		cs.setAlgorithm("BlowFish");
    				
    		String strKey = request.getContextPath();//+ ":::" + WebConfig.getDivision();
    		String encStr = cs.encrypt(strKey);
    		logger.info("암호화 키 생성!!~~~!!!: {}", strKey);
    	 
    		String requestUri = request.getRequestURI();
    		String queryString = request.getQueryString();
    		if(queryString==null) queryString = ""; 
    		if(queryString.trim().equals("")==false)
    			requestUri+="?"+queryString;
    		
    		String reporturl = Base64.encodeBase64String(requestUri.getBytes());
    		logger.info("Full URL : {}", requestUri );  
    		String returnURL = "/system/session/check?purl="+WebConfig.getProperty("system.portal.url") + "&key=" + encStr+"&link="+reporturl;
    		logger.info("redirect: {}", returnURL);
    		throw new ModelAndViewDefiningException(new ModelAndView("redirect:" + returnURL));
		} 
    }
    
	private void printRequest(HttpServletRequest request) {
		@SuppressWarnings("rawtypes")
		Enumeration eParam = request.getParameterNames();
    	logger.debug("=====================================preHandle Print Request Values=====================================");
	    logger.debug("				Request Url		:{}", request.getRequestURL());
		logger.debug("				Request Uri		:{}", request.getRequestURI());
		logger.debug("				Context		:{}", request.getContextPath());
		logger.debug("				getServletPath		:{}", request.getServletPath());
		logger.debug("				Method Name		:{}", request.getMethod());
		while (eParam.hasMoreElements()) {  
	         String pName = (String)eParam.nextElement();  
	         logger.debug("				{}			:{}", pName, request.getParameter(pName));
	    } 
		logger.debug("==============================================================================================");
		
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception{
 
		this.printRequest(request);
	}
	
    public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3) throws Exception {
		/** 공통 Exception 처리를 해주는것이 좋다. 에러 메일을 보내는것등 */
    	logger.debug("Interceptor:afterComplete");
    }
}