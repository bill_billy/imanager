package com.iplanbiz.imanager.service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;


import javax.annotation.Resource;
 
import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;
import com.iplanbiz.imanager.dto.Preferences;

@Service
@Transactional(readOnly=true)
public class PreferencesService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;

	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	public JSONArray selectPreferences() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		return sqlHouseExcuteDao.selectJSONArray(185,map);
	}
	@Transactional
	public int saveDefaultSetting(final String periodGb, final String yyyy, final String scNm) {
		
		String[] arrayClob = null;
		try
		{
			final String userId = loginSessionInfoFactory.getObject().getUserId();
			
			HashMap<String, Object> map1 = new HashMap<String, Object>();
			map1.put("@(PERIOD_GB)", 		periodGb);
			map1.put("@(YYYY)", 			yyyy);
			map1.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
			sqlHouseExcuteDao.crudQuery(186, map1, arrayClob);
			logger.debug("기본환경설정1 종료!! time: {}", System.currentTimeMillis());
			 
			
			sqlHouseExcuteDao.crudQuery(187, map1, arrayClob);
			logger.debug("기본환경설정2 종료!! time: {}", System.currentTimeMillis());
			
			HashMap<String, Object> map4 = new HashMap<String, Object>();
			map4.put("@(YYYY)", 			yyyy);
			sqlHouseExcuteDao.crudQuery(188, map4, arrayClob);
			logger.debug("기본환경설정4 종료!! time: {}", System.currentTimeMillis());
			
			 
			HashMap<String, Object> map5 = new HashMap<String, Object>();
			map5.put("@(SC_NM)", 			scNm);
			map5.put("@(YYMMDD)", 			yyyy + "0101");
			map5.put("@(INSERT_EMP)", 			userId);
			map5.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
			sqlHouseExcuteDao.crudQuery(189, map5, arrayClob);
			logger.debug("기본환경설정5 종료!! time: {}", System.currentTimeMillis());
		}catch(Exception e){
			logger.debug(e.getMessage());
			return 0;
		}
			
		return 100;
	}	
	//평가조직 조회
	public JSONArray selectEvalOrg(String scId) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(SC_ID)", 	scId);
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		return sqlHouseExcuteDao.selectJSONArray(190,map);
	}
	//평가구분 및 평가시기 가져오기
	public JSONArray selectEvaGbnSetting() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		return sqlHouseExcuteDao.selectJSONArray(191,map);
	}
	//평가시기 저장
	public int insertEvaGbn(Preferences pf) throws Exception{

		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		for(int i=0;i<pf.getEvaGbn().length;i++){
			map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
			map.put("@(S_USER_ID)"					, loginSessionInfoFactory.getObject().getUserId()); 
			map.put("@(S_EVA_GBN)"		            , pf.getEvaGbn()[i]);
			map.put("@(S_MON01)"						, pf.getMon1()[i]);
			map.put("@(S_MON02)"						, pf.getMon2()[i]);
			map.put("@(S_MON03)"						, pf.getMon3()[i]);
			map.put("@(S_MON04)"						, pf.getMon4()[i]);  
			map.put("@(S_MON05)"						, pf.getMon5()[i]);
			map.put("@(S_MON06)"						, pf.getMon6()[i]);
			map.put("@(S_MON07)"						, pf.getMon7()[i]);
			map.put("@(S_MON08)"						, pf.getMon8()[i]);
			map.put("@(S_MON09)"						, pf.getMon9()[i]);
			map.put("@(S_MON10)"						, pf.getMon10()[i]);
			map.put("@(S_MON11)"						, pf.getMon11()[i]);
			map.put("@(S_MON12)"						, pf.getMon12()[i]);
		try {
			// COUNT  
			List<LinkedHashMap<String, Object>> rs = sqlHouseExcuteDao.selectHashMap(192, map);

			if(rs != null && rs.size()>0) {
				int cnt = Integer.parseInt((String)rs.get(0).get("CNT"));
				int idx = 0;
				
				if(cnt == 0) {
					// insert
					idx = 194;
					result = 1; 
				} else {
					// update
					idx = 193;
					result = 2; 
				}
				
				// INSERT, UPDATE
				sqlHouseExcuteDao.crudQuery(idx, map);
				result = 1;
				
			} else {
				result = -1;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		  }
		}
		return result;
		}
	
}
