package com.iplanbiz.imanager.service.assign;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.KpiDAO;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;
import com.iplanbiz.imanager.dto.AssignMent;
import com.iplanbiz.imanager.dto.AssignMentKpi;
import com.iplanbiz.imanager.dto.Kpi;



@Service
@Transactional(readOnly=true)
public class MissionKpiService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory; 
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExecuteDao;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	@Autowired
	KpiDAO kpiDao;
	
	
	
	public JSONArray listCode(String comlcod) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(COML_COD)"				, comlcod);
		
		return sqlHouseExecuteDao.selectJSONArray(399, map);
	}
	public JSONArray hasProgram(String ms_id,String assign_id) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(ASSIGN_ID)"				, assign_id);
		
		return sqlHouseExecuteDao.selectJSONArray(395, map);
	}
	
	
	public JSONArray detailAssignMent(String yyyy, String msId, String assignId) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, msId);
		map.put("@(ASSIGN_ID)"				, assignId);
		map.put("@(YYYY)"					, yyyy);
		
		return sqlHouseExecuteDao.selectJSONArray(403,map);
	}
	public JSONArray getMaxAssignid(String ms_id) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		
		return sqlHouseExecuteDao.selectJSONArray(401, map);
	}
	@Transactional
	public int insert(AssignMent assign) throws Exception{
		int result = 0;
		
		TableObject idpfe0100 = new TableObject(); 
		TableObject idpfe0101 = new TableObject(); 
		
		JSONObject jsonObjectassignId = (JSONObject) getMaxAssignid(assign.getMs_id()).get(0);
		
		
		
		if(assign.getAssign_id()!=null&&!assign.getAssign_id().equals("")){ //수정
			idpfe0100.put("ASSIGN_ID"					, assign.getAssign_id(),true);
			logger.debug("지표 수정 : {}"			, assign.getAssign_id());
		}else{
			idpfe0100.put("ASSIGN_ID"					, jsonObjectassignId.get("ASSIGN_ID"), true);
			logger.debug("지표 신규 : {}"			, jsonObjectassignId.get("ASSIGN_ID"));
		}
		
		
		idpfe0100.put("MS_ID"				, assign.getMs_id(),true);
		idpfe0100.put("STRA_ID"				, assign.getStra_id());
		idpfe0100.put("SUBSTRA_ID"			, assign.getSubstra_id());
		idpfe0100.put("ASSIGN_CD"			, assign.getAssign_cd());
		idpfe0100.put("ASSIGN_NM"			, assign.getAssign_nm()); 
		idpfe0100.put("DEPT_CD"				, assign.getDept_cd());
		idpfe0100.put("DEPT_NM"				, assign.getDept_nm());
		idpfe0100.put("START_DAT"			, assign.getStart_dat());  
		idpfe0100.put("END_DAT"				, assign.getEnd_dat());  
		idpfe0100.put("ASSIGN_CONTENT"		, assign.getAssign_content());  
		idpfe0100.put("ASSIGN_DESC"			, assign.getAssign_desc());
		
		idpfe0100.putCurrentDate("INSERT_DAT");
		idpfe0100.put("INSERT_EMP"			, loginSessionInfoFactory.getObject().getUserId());
		idpfe0100.putCurrentDate("UPDATE_DAT");
		idpfe0100.put("UPDATE_EMP"			, loginSessionInfoFactory.getObject().getUserId()); 
		
		String msId = idpfe0100.get("MS_ID").toString();
		String assignId = idpfe0100.get("ASSIGN_ID").toString();
		String[] kpiId = assign.getKpi_id().split(",");
		
		try{
			TableObject deleteipfe0101 = new TableObject(); 
			deleteipfe0101.put("MS_ID"				, msId, true);
			deleteipfe0101.put("ASSIGN_ID"				, assignId, true);
			
			dbmsDao.deleteTable("IDPFE0101", deleteipfe0101);
			
			if(!assign.getKpi_id().equals("")) {

				for(int i=0; i<kpiId.length; i++) {
					idpfe0101.put("MS_ID"				, msId, true);
					idpfe0101.put("ASSIGN_ID"				, assignId, true);
					idpfe0101.put("KPI_ID"			, kpiId[i]);	
					
					
					dbmsDao.insertTable("IDPFE0101", idpfe0101);
				}
			}
			if(assign.getAssign_id()==null||assign.getAssign_id().equals("")){
				//IMFE0009 INSERT 
				dbmsDao.insertTable("IDPFE0100", idpfe0100);
				
				result = 1;
			}else{
				//IMFE0009 UPDATE 
				dbmsDao.updateTable("IDPFE0100",idpfe0100.getKeyMap(), idpfe0100);
				result = 2;
			}
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		
		return result;
	}
	public int remove(String ms_id, String assign_id) throws Exception {
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(ASSIGN_ID)"					, assign_id);
		map.put("@(MT_ID)"					, "AND 1=1");
		
		
		JSONArray jsonHasProgram =  hasProgram(ms_id,assign_id);
		
		
		String[] arrayClob = null;
		try{
			if(jsonHasProgram.size()>0) {
				result=2;
				return result;
			}
			sqlHouseExecuteDao.crudQuery(404, map, arrayClob);
			result = 1;	
		}catch(Exception e){
			logger.error("error:",e);			
			result = 0;
			throw e;
		}
		if(result==1) {
			//result = removeKpi(ms_id,assign_id,"");
		}
		return result; 
	}
	
	
	@Transactional
	public int insertKpi(AssignMentKpi kpi) throws Exception{
		int result = 0;
		
		TableObject idpfe0101 = new TableObject(); 
		
		
		idpfe0101.put("MS_ID"				, kpi.getMs_id(),true);
		idpfe0101.put("ASSIGN_ID"			, kpi.getAssign_id(),true);
		idpfe0101.put("MT_ID"				, kpi.getMt_id(),true);
		idpfe0101.put("STRA_ID"				, kpi.getStra_id());
		idpfe0101.put("SUBSTRA_ID"			, kpi.getSubstra_id());
		idpfe0101.put("MT_NM"				, kpi.getMt_nm());
		idpfe0101.put("CAL_NM"				, kpi.getCal_nm()); 
		idpfe0101.put("MEAS_CYCLE"			, kpi.getMeas_cycle());
		idpfe0101.put("UNIT"				, kpi.getUnit());
		idpfe0101.put("EVAL_TYPE"			, kpi.getEval_type());  
		idpfe0101.put("MT_GBN"				, kpi.getMt_gbn());  
		idpfe0101.put("EVA_TYP"				, kpi.getEva_typ());  
		idpfe0101.put("MT_DIR"				, kpi.getMt_dir());  
		idpfe0101.put("DECIMAL_PLACES"		, kpi.getDecimal_places());  
		idpfe0101.put("MT_DESC"				, kpi.getMt_desc());  
		
		idpfe0101.putCurrentDate("INSERT_DAT");
		idpfe0101.put("INSERT_EMP"			, loginSessionInfoFactory.getObject().getUserId());
		idpfe0101.putCurrentDate("UPDATE_DAT");
		idpfe0101.put("UPDATE_EMP"			, loginSessionInfoFactory.getObject().getUserId()); 
		try{
			if(kpi.getAc().equals("insert")){
				//IMFE0009 INSERT 
				dbmsDao.insertTable("IDPFE0101", idpfe0101);
				
				result = 1;
			}else{
				//IMFE0009 UPDATE 
				dbmsDao.updateTable("IDPFE0101",idpfe0101.getKeyMap(), idpfe0101);
				result = 2;
			}
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		
		return result;
	}
	public int removeKpi(String ms_id, String assign_id,String mt_id) throws Exception {
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(ASSIGN_ID)"				, assign_id);
		if(mt_id.equals("")) 
			map.put("@(MT_ID)","");
		else
			map.put("@(MT_ID)"					, "AND MT_ID="+mt_id);
		String[] arrayClob = null;
		try{
			sqlHouseExecuteDao.crudQuery(405, map, arrayClob);
			result = 1;
		}catch(Exception e){
			logger.error("error:",e);			
			result = 0;
			throw e;
		}
		return result; 
	}
	public int insertKpiUser(String msid,String yyyy,String kpiid[],String subkpiid[],String empno[],String empnm[],String deptcd[],String deptnm[],String confempno[],String confempnm[],String confdeptcd[],String confdeptnm[]) throws Exception {
		int result = 0;
		
		
		for(int i=0;i<kpiid.length;i++) {
			JSONArray jsonHasUser =  checkUser(msid,yyyy,kpiid[i],subkpiid[i]);
			
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("@(MS_ID)"				, msid);
			map.put("@(YYYY)"				, yyyy);
			map.put("@(KPI_ID)"				, kpiid[i]);
			map.put("@(SUB_KPI_ID)"			, subkpiid[i]);
			map.put("@(EMP_NO)"				, empno[i]);
			map.put("@(EMP_NM)"				, empnm[i]);
			map.put("@(DEPT_CD)"			, deptcd[i]);
			map.put("@(DEPT_NM)"			, deptnm[i]);
			map.put("@(CONF_EMP_NO)"		, confempno[i]);
			map.put("@(CONF_EMP_NM)"		, confempnm[i]);
			map.put("@(CONF_DEPT_CD)"		, confdeptcd[i]);
			map.put("@(CONF_DEPT_NM)"		, confdeptnm[i]);
			
			if(jsonHasUser.size()>0) {
				map.put("@(UPDATE_EMP)"				, loginSessionInfoFactory.getObject().getUserId());
			}else {
				map.put("@(INSERT_EMP)"				, loginSessionInfoFactory.getObject().getUserId());
			}
			String[] arrayClob = null;
			try{
				if(jsonHasUser.size()>0) {
					sqlHouseExecuteDao.crudQuery(491, map, arrayClob);
				}else {
					sqlHouseExecuteDao.crudQuery(483, map, arrayClob);
				}
				result = 1;
			}catch(Exception e){
				logger.error("error:",e);			
				result = 0;
				throw e;
			}
			
		}
		return result; 
	}
	public JSONArray checkUser(String ms_id,String yyyy,String kpiid,String subkpiid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(YYYY)"					, yyyy);
		map.put("@(KPI_ID)"					, kpiid);
		map.put("@(SUB_KPI_ID)"				, subkpiid);
		return sqlHouseExecuteDao.selectJSONArray(490, map);
	}
	public int insertkpiGoal(String ac, String msid, String yyyy, String kpiid, String subkpiid, String goal,String base,String yyyy3) throws Exception {
		int result= 0;
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"				, msid);
		map.put("@(YYYY)"				, yyyy);
		map.put("@(KPI_ID)"				, kpiid);
		map.put("@(SUB_KPI_ID)"			, subkpiid);
		map.put("@(GOAL)"				, goal);
		map.put("@(BASE)"				, base);
		
		
		if(ac.equals("UPDATE")) {
			map.put("@(YYYY3)"					, yyyy3);
			map.put("@(UPDATE_EMP)"				, loginSessionInfoFactory.getObject().getUserId());
		}else {
			map.put("@(INSERT_EMP)"				, loginSessionInfoFactory.getObject().getUserId());
		}
		String[] arrayClob = null;
		try{
			if(ac.equals("UPDATE")) {
				sqlHouseExecuteDao.crudQuery(478, map, arrayClob);
			}else {
				sqlHouseExecuteDao.crudQuery(477, map, arrayClob);
			}
			result = 1;
		}catch(Exception e){
			logger.error("error:",e);			
			result = 0;
			throw e;
		}
		return result;
	}
	public int removekpiGoal(String msid, String yyyy, String kpiid, String subkpiid) throws Exception {
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, msid);
		map.put("@(YYYY)"					, yyyy);
		map.put("@(KPI_ID)"					, kpiid);
		map.put("@(SUB_KPI_ID)"				, subkpiid);
		String[] arrayClob = null;
		try{
			sqlHouseExecuteDao.crudQuery(479, map, arrayClob);
			result = 1;
		}catch(Exception e){
			logger.error("error:",e);			
			result = 0;
			throw e;
		}
		return result; 
	}
	
}
