/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.imanager.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;
import com.iplanbiz.imanager.dao.TargetDueDateDAO;
import com.iplanbiz.imanager.dto.TargetDueDate;

@Service
@Transactional(readOnly=true)
public class TargetDueDateService {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	TargetDueDateDAO targetDueDateDAO;

	@Autowired
	DBMSDAO dbmsDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;

	/**
	 * <b>목표입력기한 평가 학년도 리스트 조회</b>
	 * @param evaGbn : 평가구분
	 * @return
	 * @throws Exception 
	 */
	public JSONArray getEvalYearList(String evaGbn) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("@(N_ACCT_ID)", loginSessionInfoFactory.getObject().getAcctID());
		param.put("@(S_EVA_GBN)", evaGbn);
		return sqlHouseExcuteDao.selectJSONArray(142, param);
	}

	/**
	 * <b>목표입력기한 평가학년도에 대한 상세 정보</b>
	 * @param evaGbn : 평가구분
	 * @param yyyy : 학년도
	 * @param tgtSeq : 차수
	 * @return
	 * @throws Exception 
	 */
	public JSONArray getEvalDetail(String evaGbn, String yyyy, String tgtSeq) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("@(N_ACCT_ID)"	, 	loginSessionInfoFactory.getObject().getAcctID());
		param.put("@(S_EVA_GBN)"	, 	evaGbn);
		param.put("@(S_YYYY)"		, 	yyyy);
		param.put("@(N_TGT_SEQ)"	, 	tgtSeq);
		
		return sqlHouseExcuteDao.selectJSONArray(145, param);
	}

	/**
	 * <b>목표입력 기한관리 저장(IMFE0055)</b>
	 * @param targetDueDate
	 * @return
	 * @throws Exception 
	 */
	@Transactional
	public int save(TargetDueDate targetDueDate) throws Exception {
		String userID = loginSessionInfoFactory.getObject().getUserId();
		String acctID = loginSessionInfoFactory.getObject().getAcctID();
		
		
		int result = 0;
		
		
		TableObject imfe0055 = new TableObject();
		
		// 입력시작일, 입력종료일, 확인 종료일 날짜 체크 - 시작
		String inputStart = targetDueDate.getInputStart().replace(".", "");
		String inputClose = targetDueDate.getInputClose().replace(".", "");
		String confirmClose = targetDueDate.getConfirmClose().replace(".", "");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date dateInputStart = sdf.parse(inputStart);
		Date dateInputClose = sdf.parse(inputClose);
		Date dateConfirmClose = sdf.parse(confirmClose);
		
		if(dateInputStart.compareTo(dateInputClose) > 0) {
			throw new Exception("입력 시작일은 입력 종료일 보다 이전 날짜를 선택해야 됩니다.");
		}
		if(dateInputClose.compareTo(dateConfirmClose) > 0) {
			throw new Exception("입력 종료일은 확인 종료일 보다 이전 날짜를 선택해야 됩니다.");
		}
		// 입력시작일, 입력종료일, 확인 종료일 날짜 체크 - 종료
		
		
		// key
		imfe0055.put("ACCT_ID"		,	acctID,true);
		imfe0055.put("EVA_GBN"		,	targetDueDate.getEvaGbn(),true);
		imfe0055.put("YYYY"			,	targetDueDate.getYear(),true);
		imfe0055.put("TGT_SEQ"		,	targetDueDate.getTargetSeq(),true);
		
		// data
		imfe0055.put("START_DAT"	,	inputStart);
		imfe0055.put("END_DAT"		,	inputClose);
		imfe0055.put("DELAY_DAT"	,	confirmClose);
		
		try {
			
			String imfe0055Table = "IMFE0055";
			
			// COUNT
			int imfe0055Count = dbmsDao.countTable(imfe0055Table, imfe0055.getKeyMap());
			
			if(imfe0055Count == 0) {
				// insert
				imfe0055.putCurrentTimeStamp("INSERT_DAT");
				imfe0055.put("INSERT_EMP",	userID);
				
				// IMFE0055 : 목표입력 기한관리 insert
				dbmsDao.insertTable(imfe0055Table, imfe0055);
			} else {
				// update
				imfe0055.putCurrentTimeStamp("UPDATE_DAT");
				imfe0055.put("UPDATE_EMP",	userID);
				
				// IMFE0055 : 목표입력 기한관리 update
				dbmsDao.updateTable(imfe0055Table, imfe0055.getKeyMap(), imfe0055);
			}
			
			result = 1;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return result;
	}

	/**
	 * <b>목표입력 기한관리 삭제(IMFE0055)</b>
	 * @param targetDueDate
	 * @return
	 * @throws Exception 
	 */
	@Transactional
	public int remove(TargetDueDate targetDueDate) throws Exception {
		String acctID = loginSessionInfoFactory.getObject().getAcctID();
		
		int result = 0;
		
		TableObject imfe0055 = new TableObject();
		// key
		imfe0055.put("ACCT_ID"	,	acctID, true);
		imfe0055.put("EVA_GBN"	,	targetDueDate.getEvaGbn(), true);
		imfe0055.put("YYYY"		,	targetDueDate.getYear(), true);
		imfe0055.put("TGT_SEQ"	,	targetDueDate.getTargetSeq(), true);
		
		try {
			dbmsDao.deleteTable("IMFE0055", imfe0055.getKeyMap());
			result = 1;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}
	
}
