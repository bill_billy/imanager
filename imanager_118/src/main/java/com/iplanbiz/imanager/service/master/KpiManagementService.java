package com.iplanbiz.imanager.service.master;

import java.util.HashMap;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO; 
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class KpiManagementService {
	
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	public int insertproject(String msid, String kpiid, String kpinm, String kpigbn, String unit, String kpitype, String kpidir, String useyn, String kpidesc, String subkpiid, String subkpinm, String weight, String sortorder, String ac, String newkpiid, String newsubkpiid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		int result = 0;
		String[] clobArray = null;
		
		map.put("@(MS_ID)",				msid);
		map.put("@(KPI_ID)",					kpiid);
		map.put("@(KPI_DIR)",					kpidir);
		map.put("@(UNIT)",				unit);
		map.put("@(USE_YN)",				useyn);
		map.put("@(INSERT_EMP)",				loginSessionInfoFactory.getObject().getUserId());
		map.put("@(UPDATE_EMP)",				loginSessionInfoFactory.getObject().getUserId());
		
		
		
		try{
			if(ac!=null) {
				map.put("@(SUB_KPI_ID)",				subkpiid);
				map.put("@(SUB_KPI_NM)",					subkpinm.replaceAll("'", "''"));
				map.put("@(WEIGHT)",					weight);
				map.put("@(SORT_ORDER)",					sortorder);
				if(subkpiid==""){
					map.put("@(SUB_KPI_ID)",				newsubkpiid);
					sqlHouseExcuteDao.crudQuery(462, map, clobArray);
					result = 1;
				}else{
					sqlHouseExcuteDao.crudQuery(463, map, clobArray);
					result = 2;
				}	
			}else {
				map.put("@(KPI_NM)",					kpinm.replaceAll("'", "''"));
				map.put("@(KPI_GBN)",					kpigbn);
				map.put("@(KPI_DESC)",				kpidesc.replaceAll("'", "''"));
				map.put("@(KPI_TYPE)",						kpitype);
			
			if(kpiid==""){
				map.put("@(KPI_ID)",					newkpiid);
				sqlHouseExcuteDao.crudQuery(459, map, clobArray);
				result = 1;
			}else{
				sqlHouseExcuteDao.crudQuery(460, map, clobArray);
				result = 2;
			}
			}
		}catch(Exception e){
			result = 3;
			e.printStackTrace(); 
			throw e;
		}
		
		return result;
	}
	public JSONArray checkStrategtTarget(String msid, String kpiid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"				, msid);
		map.put("@(KPI_ID)"				, kpiid);
		
		return sqlHouseExcuteDao.selectJSONArray(348, map); 
	}
	public int deleteproject(String kpiid, String subkpiid, String msid)  throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		int result = 0;
		map.put("@(MS_ID)",					msid);
		map.put("@(KPI_ID)",					kpiid);
		
		String[] clobArray = null;
		try{
			if(subkpiid==null) {
				map.put("@(SUB_KPI_ID)",					999999);
				sqlHouseExcuteDao.crudQuery(461, map, clobArray);
				sqlHouseExcuteDao.crudQuery(485, map, clobArray);
				sqlHouseExcuteDao.crudQuery(486, map, clobArray);
			
			}
			else {
				map.put("@(SUB_KPI_ID)",					subkpiid);
				sqlHouseExcuteDao.crudQuery(464, map, clobArray);
				sqlHouseExcuteDao.crudQuery(485, map, clobArray);
				sqlHouseExcuteDao.crudQuery(486, map, clobArray);
			}
			
			result = 1;
		}catch(Exception e){
			result = 2;
			e.printStackTrace();
		}
		return result;
	}

}
