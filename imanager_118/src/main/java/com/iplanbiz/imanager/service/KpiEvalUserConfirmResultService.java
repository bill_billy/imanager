package com.iplanbiz.imanager.service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class KpiEvalUserConfirmResultService {
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;

	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	public JSONArray selectChargeConfirmRusult() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		return sqlHouseExcuteDao.selectJSONArray(177,map);
	}
	
	public List<LinkedHashMap<String, Object>> getFileInfo(String fileid,String kpiId,String yyyy) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_FILE_ID)"				, fileid);
		map.put("@(S_KPI_ID)"					, kpiId);
		map.put("@(S_YYYY)"					, yyyy); 
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		List<LinkedHashMap<String, Object>> fileList = this.sqlHouseExcuteDao.selectHashMap(261, map);
		return fileList;
	}
	

	public List<LinkedHashMap<String, Object>> ConfirmResultPopupList(String yyyy,String pEvaGbn,String scId,String owneruserId,String kpichargeId,String gubn,String straId,String substrId,String csfId,String kpiId, String evalUser) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
	 
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_YYYY)"					, yyyy); 
		map.put("@(S_EVA_GBN)"					, pEvaGbn); 
		map.put("@(S_SC_ID)"					, scId); 
		map.put("@(S_OWNER_USER_ID)"					, owneruserId); 
		map.put("@(S_KPI_CHARGE_ID)"					, kpichargeId); 
		map.put("@(S_GUBN)"					, gubn); 
		map.put("@(S_STRA_ID)"					, straId); 
		map.put("@(S_SUBSTRA_ID)"					, substrId); 
		map.put("@(S_CSF_ID)"					, csfId); 
		map.put("@(S_KPI_ID)"					, kpiId); 
		map.put("@(S_EVAL_USER_ID)"					, evalUser); 
		
		List<LinkedHashMap<String, Object>> mainList = this.sqlHouseExcuteDao.selectHashMap(300, map);
		return mainList;
	}


}
