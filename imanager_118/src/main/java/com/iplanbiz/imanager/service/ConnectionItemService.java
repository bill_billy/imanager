package com.iplanbiz.imanager.service;

import java.util.HashMap;

import javax.annotation.Resource;
 
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO; 
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class ConnectionItemService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	public JSONArray selectConnectionItem(String searchyn, String searchifsys, String searchifcodnm) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		logger.debug("PARAM VALUE = " + searchyn + "//" + searchifsys + "///" + searchifcodnm);
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(USE_YN)"					, searchyn);
		map.put("@(IF_SYS)"					, searchifsys);
		map.put("@(IF_COD)"					, searchifcodnm);
		
		return sqlHouseExcuteDao.selectJSONArray(21, map);
	}
	public JSONArray checkIfcod100() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		return sqlHouseExcuteDao.selectJSONArray(27, map);
	}
	public int insertConnectionItem(String ifcod, String ifcodnm, String ifunit, String ifsys, String sqldata, String bigo, String useyn) throws Exception{
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(IF_COD)"					, ifcod);
		map.put("@(IF_COD_NM)"				, ifcodnm);
		map.put("@(IF_SYS)"					, ifsys);
		map.put("@(IF_UNIT)"				, ifunit);
		map.put("@(SQL_DATA)"				, sqldata.replaceAll("'","''"));
		map.put("@(BIGO)"					, bigo.replaceAll("'", "''"));
		map.put("@(USE_YN)"					, useyn);
		map.put("@(INSERT_USER_ID)"			, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(UUPDATE_USER_ID)"		, loginSessionInfoFactory.getObject().getUserId());
		String[] arrayClob = null;
		JSONObject checkIfcod = null;
		checkIfcod = (JSONObject) checkIfcod100().get(0);
		try{
			if("".equals(ifcod)){
				logger.debug("CHECK IF_COD = " + checkIfcod.get("CNT"));
				if((checkIfcod.get("CNT")).equals("0")){
					HashMap<String, Object> newMap = new HashMap<String, Object>();
					newMap.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
					newMap.put("@(IF_SYS)"				, ifsys);
					newMap.put("@(INSERT_USER_ID)"		, loginSessionInfoFactory.getObject().getUserId());
					sqlHouseExcuteDao.crudQuery(28, newMap, arrayClob);
					sqlHouseExcuteDao.crudQuery(24, map, arrayClob);
					result = 1;
				}else{
					sqlHouseExcuteDao.crudQuery(24, map, arrayClob);
					result = 1;
				}
			}else{
				sqlHouseExcuteDao.crudQuery(25, map, arrayClob);
				result = 2;
			}
		}catch(Exception e){
			result = 3;
			logger.error("error : {}",e);
		}
		return result;
	}
	public int removeConnectionItem(String ifcod){
		HashMap<String, Object> map = new HashMap<String, Object>();
		int result = 0;
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(IF_COD)"					, ifcod);
		String[] arrayClob = null;
		try{
			sqlHouseExcuteDao.crudQuery(26, map, arrayClob);
			result = 1;
		}catch(Exception e){
			result = 2;
			logger.error("error : {}",e);
		}
		return result;
	}
	public JSONArray checkConnectionItem(String ifcod) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(IF_COD)"					, ifcod);
		
		return sqlHouseExcuteDao.selectJSONArray(66, map);
	}
	

}
