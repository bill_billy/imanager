package com.iplanbiz.imanager.service.program;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.io.file.FileService;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.config.WebConfig;
import com.iplanbiz.imanager.dao.BasicDAO;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;
import com.iplanbiz.imanager.dto.Program;
import com.iplanbiz.imanager.dto.ProgramFile;

@Service
@Transactional(readOnly=true)
public class ProgramFileService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExecuteDao;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	public ProgramFile saveFile(CommonsMultipartFile file, String[] extType) throws IOException
	{
		String fileSaveName = "";
		int result = 0;
		com.iplanbiz.core.io.file.FileService fileService = new com.iplanbiz.core.io.file.FileService(WebConfig.getProperty("system.upload.SaveDir"),Long.valueOf(WebConfig.getProperty("system.upload.maxUploadSize").toString()));
		
		fileSaveName = fileService.setFileName();
		result = fileService.saveFile(file, extType, fileSaveName);
		
		ProgramFile returnFile = new ProgramFile();
		
		if(result == 0){
			returnFile.setFileOrgName(file.getOriginalFilename());
			returnFile.setFileSaveName(fileSaveName);
			returnFile.setFilePath(fileService.getUpLoadPath());
			returnFile.setFileSize(String.valueOf(file.getSize()));
			returnFile.setFileExt(fileService.getFileExt());
		}
		
		return returnFile;
	}
	public int removeFile(String ms_id,String program_id,String file_gubun,String fileId){
 
		Map<String, String> fileMap =this.getFile(ms_id,program_id,file_gubun,fileId);
		java.io.File realFile = new java.io.File(fileMap.get("FILE_PATH")+fileMap.get("FILE_SAVE_NAME")+"."+fileMap.get("FILE_EXT"));
		
		int result = 0;
		FileService fileService = new FileService(WebConfig.getProperty("system.upload.SaveDir"),Long.valueOf(WebConfig.getProperty("system.upload.maxUploadSize").toString()));
		
		try {
			String fileName = fileMap.get("FILE_ORG_NAME");
			if(fileName==null) fileName="";
			result = fileService.deleteFile(realFile,fileName);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
		
	}
	
	public int deleteFile(String ms_id,String program_id,String file_id) throws Exception {
		int result=0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(PROGRAM_ID)"				, program_id);
		map.put("@(FILE_ID)"				, file_id);
		String[] arrayClob = null;
		try{
			sqlHouseExecuteDao.crudQuery(425, map, arrayClob);
			result = 1;
		}catch(Exception e){
			logger.error("error:",e);			
			result = 0;
			throw e;
		}
		return result;
		
	}
	
	
	private JSONArray getMaxFileid(String ms_id,String program_id) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(PROGRAM_ID)"				, program_id);
		return sqlHouseExecuteDao.selectJSONArray(417, map);
	}
	public int insertFile(ProgramFile file) throws Exception {
		
		int result = 0;
		JSONObject maxFileId = (JSONObject) getMaxFileid(file.getMs_id(),file.getProgram_id()).get(0);
		TableObject idpfe0201 = new TableObject(); 
		
		idpfe0201.put("FILE_ID"				, maxFileId.get("FILE_ID"),true);
		idpfe0201.put("MS_ID"				, file.getMs_id(),true);
		idpfe0201.put("PROGRAM_ID"			, file.getProgram_id(),true);
		idpfe0201.put("FILE_GUBUN"			, file.getFile_gubun());
		idpfe0201.put("FILE_SAVE_NAME"		, file.getFileSaveName());
		idpfe0201.put("FILE_ORG_NAME"		, file.getFileOrgName());
		idpfe0201.put("FILE_PATH"			, file.getFilePath());
		idpfe0201.put("FILE_SIZE"			, file.getFileSize());
		idpfe0201.put("FILE_EXT"			, file.getFileExt());
		
		idpfe0201.putCurrentDate("INSERT_DAT");
		idpfe0201.put("INSERT_EMP"			, loginSessionInfoFactory.getObject().getUserId());
		idpfe0201.putCurrentDate("UPDATE_DAT");
		idpfe0201.put("UPDATE_EMP"			, loginSessionInfoFactory.getObject().getUserId()); 
		
		try{
				dbmsDao.insertTable("IDPFE0201", idpfe0201);
				result = 1;
			
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		
		return result;
	}
	
	public JSONArray getFilelist(String ms_id,String program_id) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(PROGRAM_ID)"				, program_id);
		return sqlHouseExecuteDao.selectJSONArray(418, map);
	}
	public Map<String, String> getFile(String ms_id, String program_id, String file_gubun, String file_id)  {
		JSONArray resultList = null;
		Map<String,String> resultMap = new HashMap<String,String>();
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(PROGRAM_ID)"				, program_id);
		map.put("@(FILE_GUBUN)"				, file_gubun);
		map.put("@(FILE_ID)"				, "AND FILE_ID="+file_id);
		
		try {
			resultList = sqlHouseExecuteDao.selectJSONArray(424, map);
			JSONObject jo = (JSONObject) resultList.get(0);
			resultMap =  new ObjectMapper().readValue(jo.toJSONString(), Map.class) ;
		} catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultMap;
	}
}
