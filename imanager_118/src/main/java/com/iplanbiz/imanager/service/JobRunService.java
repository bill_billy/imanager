/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.imanager.service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.JobRunDAO;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;
import com.iplanbiz.imanager.dto.JobRun;

@Service
@Transactional(readOnly=true)
public class JobRunService {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	JobRunDAO JobRunDAO;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;

	@Autowired
	ValueDueDateService valueDueDateService;
	
	/**
	 * <b>작업실행관리 조회</b>
	 * @param evaGbn : 평가구분
	 * @param year : 학년도
	 * @param gubun : 실적, 목표 구분
	 * @return
	 * @throws Exception 
	 */
	public JSONArray select(String evaGbn, String year, String gubun) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("@(N_ACCT_ID)", loginSessionInfoFactory.getObject().getAcctID());
		param.put("@(S_EVA_GBN)", evaGbn);
		param.put("@(S_YYYY)"	, year);
		param.put("@(S_GUBUN)"	, gubun);
		
		return sqlHouseExcuteDao.selectJSONArray(161, param);
	}

	/**
	 * <b>작업실행관리 개별저장</b>
	 * @param jobRun
	 * @return
	 * @throws Exception 
	 */
	@Transactional
	public int save(JobRun jobRun) throws Exception {
		int result = 0;
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		// 실적
		param.put("@(N_ACCT_ID)"	,   loginSessionInfoFactory.getObject().getAcctID());
		param.put("@(S_EVA_GBN)"	,	jobRun.getEvaGbn());
		param.put("@(S_GUBUN)"		,	jobRun.getGubun());
		param.put("@(S_YYYY)"		,	jobRun.getYyyy());
		param.put("@(S_MM)"			,	jobRun.getMm());
		param.put("@(S_JOBSTATUS)"	,	jobRun.getJobStatus());
		param.put("@(S_INSERT_EMP)"	,	loginSessionInfoFactory.getObject().getUserId());
		
		try {
			// COUNT
			List<LinkedHashMap<String, Object>> rs = sqlHouseExcuteDao.selectHashMap(167, param);
			
			if(rs != null && rs.size()>0) {
				int cnt = Integer.parseInt((String)rs.get(0).get("CNT"));
				int idx = 0;
				
				if(cnt == 0) {
					// insert
					idx = 168;
					logger.info("IMFE0072 INSERT => SQLHouse IDX = 168");
					result = 1; // insert
				} else {
					// update
					idx = 169;
					logger.info("IMFE0072, IMFE0066, IMFE0016 UPDATE, IMFE0022 DELETE => SQLHouse IDX = 160");
					result = 2; // update
				}
				
				// IMFE0072 : 작업실행관리 INSERT, UPDATE
				sqlHouseExcuteDao.crudQuery(idx, param);
				result = 1;
				
			} else {
				// 에러 발생 한 것, 저장 실패
				logger.info("IMFE0054 COUNT ERROR, SQLHouse IDX = 167");
				result = -1;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return result;
	}

	/**
	 * <b>작업실행관리 : 자료생성, 점수계산 프로시저 실행</b>
	 * @param jobRun
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public int executeJobRun(JobRun jobRun) throws Exception {
		
		int result = 0;
		
		
		try {
			
			String yyyyMm = ""; // 년월
			yyyyMm = jobRun.getYyyy()+jobRun.getMm();
			
			String acctID = loginSessionInfoFactory.getObject().getAcctID();
			
			int resultCalled = 0; 
			
			
			// 실적일 경우에는 회기년월이 필요함.
			HashMap<String, Object> param = new HashMap<String, Object>();
			param.put("@(N_ACCT_ID)", loginSessionInfoFactory.getObject().getAcctID());
			param.put("@(S_YYYYMM)"	, yyyyMm);
			if(jobRun.getGubun().equals("A")) {
				List<LinkedHashMap<String, Object>> rs = sqlHouseExcuteDao.selectHashMap(156, param);
				if(rs.size() >0) {
					yyyyMm = (String)rs.get(0).get("PERIOD_YYYYMM");
				}
			}
			
			
			HashMap<String, Object> procParam = new HashMap<String, Object>();
			procParam.put("ACCT_ID",  acctID);
			procParam.put("EVN_GBN", jobRun.getEvaGbn());
			procParam.put("YYYYMM", yyyyMm);
			procParam.put("GUBUN", jobRun.getGubun());
			procParam.put("ERROR", "0");
		
			resultCalled = JobRunDAO.executeSP_IMFE0012(procParam);
			
			
			// 대기상태에서 자료생성 시 평가중으로 변경
			if(resultCalled != 0) {
				if(jobRun.getJobStatus().equals("A")) {
					jobRun.setJobStatus("B"); // 평가중
					this.save(jobRun);
				}
				
				result = 1;
			} else {
				// 프로시저 실행 오류
				result = -1;	
			}			
			
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return result;
	}
	
}
