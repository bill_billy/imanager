package com.iplanbiz.imanager.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.KpiDAO;
import com.iplanbiz.imanager.dao.KpiTargetDAO;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;
import com.iplanbiz.imanager.dto.kpiTarget;
import com.iplanbiz.core.io.dbms.DBMSDAO;

@Service
@Transactional(readOnly=true)
public class KpiTargetService {
	private Logger logger = LoggerFactory.getLogger(getClass()); 
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;

	@Autowired
	DBMSDAO dbmsDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	public JSONArray listKpiCombo(String yyyy) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_USER_ID)"					, loginSessionInfoFactory.getObject().getUserId()); 
		map.put("@(S_YYYY)"						, yyyy);
		
		return sqlHouseExcuteDao.selectJSONArray(140, map);
	}
	
	public JSONArray listScidCombo(String yyyy, String kpiid, String evagbn) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EMP_ID)"					, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(S_MT_ID)"					, kpiid);
		map.put("@(S_EVA_GBN)"					, evagbn);
		map.put("@(S_YYYY)"						, yyyy); 
		
		return sqlHouseExcuteDao.selectJSONArray(141, map);
	}
	public JSONArray getUserInfo() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EMP_ID)"					, loginSessionInfoFactory.getObject().getUserId());
		
		return sqlHouseExcuteDao.selectJSONArray(143, map);
	}
	public JSONArray selectKpiTarget(String yyyy, String kpiid, String scid, String evagbn) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EMP_ID)"					, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(S_KPI_ID)"					, kpiid);
		map.put("@(S_YYYY)"						, yyyy);
		map.put("@(S_SC_ID)"					, scid); 
		map.put("@(S_EVA_GBN)"					, evagbn);
		
		return sqlHouseExcuteDao.selectJSONArray(144, map);
	}
	public JSONArray checkIMFE0020(String evagbn, String gubun, String yyyymm, String kpiid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EVA_GBN)"					, evagbn);
		map.put("@(S_GUBUN)"					, gubun);
		map.put("@(S_YYYYMM)"					, yyyymm);
		map.put("@(S_KPI_ID)"					, kpiid);
		
		return sqlHouseExcuteDao.selectJSONArray(146, map);
	}
	public JSONArray checkDate(String gubun, String evagbn, String yyyymm) throws Exception{
		JSONArray checkValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EVA_GBN)"					, evagbn);
		map.put("@(S_YYYY)"						, yyyymm);
		if(gubun.equals("AA")){
			checkValue = sqlHouseExcuteDao.selectJSONArray(158, map);
		}else if(gubun.equals("AB")){
			checkValue = sqlHouseExcuteDao.selectJSONArray(159, map); 
		}
		return checkValue;
	}
	public JSONArray kpiTargetCheck(String evagbn, String yyyy) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EVA_GBN)"					, evagbn);
		map.put("@(S_YYYY)"						, yyyy);
		
		return sqlHouseExcuteDao.selectJSONArray(160,map);
	}
	@Transactional
	public int updateKpiTarget(kpiTarget kpi) throws Exception{
		int result = 0;
		List<LinkedHashMap<String, Object>> arrInsertIMFE0013 = new ArrayList<LinkedHashMap<String,Object>>();
		for(int i=0;i<kpi.getTargetAmt().length;i++){
			TableObject imfe0013 = new TableObject();
			logger.debug("PARAM VALUE = " + kpi.getpEvaGbn() + " /// " + kpi.getYyyy()[i] + " /// " + kpi.getMm()[i] + " /// " + kpi.getAnalCycle()[i] + " /// " + kpi.getTgtSeq()[i] + " /// " + kpi.getTargetAmt()[i] + " /// " + kpi.getKpiID()[i]);
			imfe0013.put("ACCT_ID"					, loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0013.put("EVA_GBN"					, kpi.getpEvaGbn(), true);
			imfe0013.put("YYYY"						, kpi.getYyyy()[i], true);
			imfe0013.put("MM"						, kpi.getMm()[i], true);
			imfe0013.put("KPI_ID"					, kpi.getKpiID()[i], true);
			imfe0013.put("ANAL_CYCLE"				, kpi.getAnalCycle()[i], true);
			imfe0013.put("TGT_SEQ"					, kpi.getTgtSeq()[i], true);
			imfe0013.put("TARGET_AMT"				, kpi.getTargetAmt()[i]);
			arrInsertIMFE0013.add(imfe0013);
		}  
		try{
 			dbmsDao.updateTable("IMFE0013", arrInsertIMFE0013);
			result = 1;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return result;
	}
	@Transactional
	public int insertAll(kpiTarget kpi) throws Exception{
		int result = 0;
		ArrayList<LinkedHashMap<String, Object>> arrInsertIMFE0020 = new ArrayList<LinkedHashMap<String,Object>>();
		ArrayList<LinkedHashMap<String, Object>> arrUpdateChargeIMFE0020 = new ArrayList<LinkedHashMap<String,Object>>();
		ArrayList<LinkedHashMap<String, Object>> arrUpdateUserIMFE0020 = new ArrayList<LinkedHashMap<String,Object>>();
		for(int i=0;i<kpi.getKpiID().length;i++){
			TableObject imfe0020 = new TableObject();
			TableObject imfe0020_charge = new TableObject();
			TableObject imfe0020_user = new TableObject();
			imfe0020.put("ACCT_ID"				, loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0020.put("EVA_GBN"				, kpi.getpEvaGbn(), true);
			imfe0020.put("GUBUN"				, "B", true);
			imfe0020.put("YYYYMM"				, kpi.getChargeYyyymm()[i], true);
			imfe0020.put("KPI_ID"				, kpi.getKpiID()[i], true);
			imfe0020.put("CHARGE_ID"			, kpi.getChargeID()[i]);
			imfe0020.put("CHARGE_NM"			, kpi.getChargeName()[i]);
			imfe0020.put("CHARGE_CONFIRM"		, kpi.getConfirm());
			imfe0020.putCurrentTimeStamp("CHARGE_DATE");
			imfe0020.put("USER_ID"				, kpi.getUserID()[i]);
			imfe0020.put("USER_NM"				, kpi.getUserName()[i]);
			imfe0020.put("USER_CONFIRM"			, "");
			imfe0020.put("USER_DATE"			, "");
			
			imfe0020_charge.put("ACCT_ID"				, loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0020_charge.put("EVA_GBN"				, kpi.getpEvaGbn(), true);
			imfe0020_charge.put("GUBUN"					, "B", true);
			imfe0020_charge.put("YYYYMM"				, kpi.getChargeYyyymm()[i], true);
			imfe0020_charge.put("KPI_ID"				, kpi.getKpiID()[i], true);
			imfe0020_charge.put("CHARGE_ID"				, kpi.getChargeID()[i]);
			imfe0020_charge.put("CHARGE_NM"				, kpi.getChargeName()[i]);
			imfe0020_charge.put("CHARGE_CONFIRM"		, kpi.getConfirm());
			imfe0020_charge.putCurrentTimeStamp("CHARGE_DATE");
			
			imfe0020_user.put("ACCT_ID"				, loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0020_user.put("EVA_GBN"				, kpi.getpEvaGbn(), true);
			imfe0020_user.put("GUBUN"				, "B", true);
			imfe0020_user.put("YYYYMM"				, kpi.getChargeYyyymm()[i], true);
			imfe0020_user.put("KPI_ID"				, kpi.getKpiID()[i], true);
			imfe0020_user.put("USER_ID"				, kpi.getUserID()[i]);
			imfe0020_user.put("USER_NM"				, kpi.getUserName()[i]);
			imfe0020_user.put("USER_CONFIRM"		, kpi.getConfirm());
			imfe0020_user.putCurrentTimeStamp("USER_DATE");
			
 			if(checkIMFE0020(kpi.getpEvaGbn(),"B", kpi.getChargeYyyymm()[i],kpi.getChargeKpiID()[i]).size() == 0){
				if(!kpi.getUserConfirm()[i].equals("Y")){
					if(kpi.getChargeConfirm()[i].equals("")){
						arrInsertIMFE0020.add(imfe0020);
					} 
				}
			}else{
				if(kpi.getUserGubn().equals("Charge")){ 
					if(!kpi.getUserConfirm()[i].toString().equals("Y")){
						arrUpdateChargeIMFE0020.add(imfe0020_charge);
					}
				}else{
					logger.debug("CHARGE_CONFRIM = " + kpi.getUserConfirm()[i].toString());
					if(kpi.getChargeConfirm()[i].toString().equals("Y")){
						arrUpdateUserIMFE0020.add(imfe0020_user);
					}
				}
			}
		}
		try{
			if(arrInsertIMFE0020.size() > 0){
				dbmsDao.insertTable("IMFE0020", arrInsertIMFE0020);
			}
			if(kpi.getUserGubn().equals("Charge")){
				dbmsDao.updateTable("IMFE0020", arrUpdateChargeIMFE0020);
			}else{
				dbmsDao.updateTable("IMFE0020", arrUpdateUserIMFE0020);
			}
			result = 1;
		}catch(Exception e){
			e.printStackTrace();
			result = 0;
			throw e;
		}
		return result;
	}
	@Transactional
	public int insertOne(kpiTarget kpi) throws Exception{
		int result = 0;
		TableObject imfe0020 = new TableObject();
		imfe0020.put("ACCT_ID"						, loginSessionInfoFactory.getObject().getAcctID(),true);
		imfe0020.put("EVA_GBN"						, kpi.getpEvaGbn(), true);
		imfe0020.put("GUBUN"						, "B", true);
		imfe0020.put("YYYYMM"						, kpi.getChargeOneYyyymm(), true);
		imfe0020.put("KPI_ID"						, kpi.getChargeOneKpiID(), true);
		imfe0020.put("CHARGE_ID"					, kpi.getChargeOneID());
		imfe0020.put("CHARGE_NM"					, kpi.getChargeOneName());
		imfe0020.put("CHARGE_CONFIRM"				, kpi.getChargeOneConfirm());
		imfe0020.putCurrentTimeStamp("CHARGE_DATE");
		imfe0020.put("USER_ID"						, kpi.getUserOneID());
		imfe0020.put("USER_NM"						, kpi.getUserOneName());
		imfe0020.put("USER_CONFIRM"					, "");
		imfe0020.put("USER_DATE"					, "");
		
		TableObject imfe0020_charge = new TableObject();
		imfe0020_charge.put("ACCT_ID"				, loginSessionInfoFactory.getObject().getAcctID(),true);
		imfe0020_charge.put("EVA_GBN"				, kpi.getpEvaGbn(), true);
		imfe0020_charge.put("GUBUN"					, "B", true);
		imfe0020_charge.put("YYYYMM"				, kpi.getChargeOneYyyymm(), true);
		imfe0020_charge.put("KPI_ID"				, kpi.getChargeOneKpiID(), true);
		imfe0020_charge.put("CHARGE_ID"				, kpi.getChargeOneID());
		imfe0020_charge.put("CHARGE_NM"				, kpi.getChargeOneName());
		imfe0020_charge.put("CHARGE_CONFIRM"		, kpi.getChargeOneConfirm());
		imfe0020_charge.putCurrentTimeStamp("CHARGE_DATE");
		
		TableObject imfe0020_user = new TableObject();
		imfe0020_user.put("ACCT_ID"					, loginSessionInfoFactory.getObject().getAcctID(),true);
		imfe0020_user.put("EVA_GBN"					, kpi.getpEvaGbn(), true);
		imfe0020_user.put("GUBUN"					, "B", true);
		imfe0020_user.put("YYYYMM"					, kpi.getChargeOneYyyymm(), true);
		imfe0020_user.put("KPI_ID"					, kpi.getChargeOneKpiID(), true);
		imfe0020_user.put("USER_ID"					, kpi.getUserOneID());
		imfe0020_user.put("USER_NM"					, kpi.getUserOneName()); 
		imfe0020_user.put("USER_CONFIRM"			, kpi.getUserOneConfirm());
		imfe0020_user.putCurrentTimeStamp("USER_DATE");
		if(checkIMFE0020(kpi.getpEvaGbn(),"B", kpi.getChargeOneYyyymm(),kpi.getChargeOneKpiID()).size()==0){
			dbmsDao.insertTable("IMFE0020", imfe0020);
			result = 1;
		}else{
			if(kpi.getUserGubn().equals("Charge")){
				dbmsDao.updateTable("IMFE0020", imfe0020_charge.getKeyMap(),imfe0020_charge);
			}else{
				dbmsDao.updateTable("IMFE0020", imfe0020_user.getKeyMap(), imfe0020_user);
			}
			result = 2;
		}
		return result;
	}
}
