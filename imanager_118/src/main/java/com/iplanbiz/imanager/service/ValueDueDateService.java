/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.imanager.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;
import com.iplanbiz.imanager.dao.ValueDueDateDAO;
import com.iplanbiz.imanager.dto.ValueDueDate;

@Service
@Transactional(readOnly=true)
public class ValueDueDateService {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	ValueDueDateDAO valueDueDateDAO;

	@Autowired
	DBMSDAO dbmsDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	/**
	 * <b>실적입력기한 평가 년월 리스트 조회</b>
	 * @param evaGbn : 평가구분
	 * @param year : 학년도
	 * @return
	 * @throws Exception 
	 */
	public JSONArray getEvalYearMonthList(String evaGbn, String year) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("@(N_ACCT_ID)", loginSessionInfoFactory.getObject().getAcctID());
		param.put("@(S_EVA_GBN)", evaGbn);
		param.put("@(S_YYYY)"	, year);
		return sqlHouseExcuteDao.selectJSONArray(151, param);
	}

	/**
	 * <b>실적입력기한 년월에 대한 상세 정보</b>
	 * @param evaGbn : 평가구분
	 * @param yyyymm : 년월
	 * @return
	 * @throws Exception 
	 */
	public JSONArray getEvalDetail(String evaGbn, String yyyymm) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("@(N_ACCT_ID)"	, 	loginSessionInfoFactory.getObject().getAcctID());
		param.put("@(S_EVA_GBN)"	, 	evaGbn);
		param.put("@(S_YYYYMM)"		, 	yyyymm);
		
		return sqlHouseExcuteDao.selectJSONArray(152, param);
	}
	
	/**
	 * <b>실적입력 기한관리 저장(IMFE0054)</b>
	 * @param valueDueDate
	 * @return
	 * @throws Exception 
	 */
	@Transactional
	public int save(ValueDueDate valueDueDate) throws Exception {
		String userID = loginSessionInfoFactory.getObject().getUserId();
		String acctID = loginSessionInfoFactory.getObject().getAcctID();
		
		
		int result = 0;
		
		// 입력시작일, 입력종료일, 확인 종료일 날짜 체크 - 시작
		String inputStart = valueDueDate.getInputStart().replace(".", "");
		String inputClose = valueDueDate.getInputClose().replace(".", "");
		String confirmClose = valueDueDate.getConfirmClose().replace(".", "");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date dateInputStart = sdf.parse(inputStart);
		Date dateInputClose = sdf.parse(inputClose);
		Date dateConfirmClose = sdf.parse(confirmClose);
		
		if(dateInputStart.compareTo(dateInputClose) > 0) {
			throw new Exception("입력 시작일은 입력 종료일 보다 이전 날짜를 선택해야 됩니다.");
		}
		if(dateInputClose.compareTo(dateConfirmClose) > 0) {
			throw new Exception("입력 종료일은 확인 종료일 보다 이전 날짜를 선택해야 됩니다.");
		}
		// 입력시작일, 입력종료일, 확인 종료일 날짜 체크 - 종료
		
		
		// key
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("@(N_ACCT_ID)",	acctID);
		param.put("@(S_EVA_GBN)",	valueDueDate.getEvaGbn());
		param.put("@(S_YYYYMM)"	,	valueDueDate.getYyyymm());
		
		// data
		param.put("@(S_START_DAT)"	,	inputStart);
		param.put("@(S_END_DAT)"	,	inputClose);
		param.put("@(S_DELAY_DAT)"	,	confirmClose);
		param.put("@(S_USER_ID)"	,	userID);
		
		try {
			// COUNT
			List<LinkedHashMap<String, Object>> rs = sqlHouseExcuteDao.selectHashMap(153, param);
			
			if(rs != null && rs.size()>0) {
				int cnt = Integer.parseInt((String)rs.get(0).get("CNT"));
				int idx = 0;
				
				if(cnt == 0) {
					// insert
					idx = 154;
					logger.info("IMFE0054 INSERT => SQLHouse IDX = 154");
					result = 1; // insert
				} else {
					// update
					idx = 155;
					logger.info("IMFE0054 UPDATE => SQLHouse IDX = 155");
					result = 2; // update
				}
				
				// imfe0054 : 실적입력 기한관리 insert
				sqlHouseExcuteDao.crudQuery(idx, param);
				
			} else {
				// 에러 발생 한 것, 저장 실패
				logger.info("IMFE0054 COUNT ERROR, SQLHouse IDX = 153");
				result = -1;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return result;
	}

	/**
	 * <b>실적입력 기한관리 삭제(IMFE0054)</b>
	 * @param valueDueDate
	 * @return
	 * @throws Exception 
	 */
	@Transactional
	public int remove(ValueDueDate valueDueDate) throws Exception {
		String acctID = loginSessionInfoFactory.getObject().getAcctID();
		
		int result = 0;
		
		HashMap<String, Object> param = new HashMap<String, Object>();
		
		// key
		param.put("@(N_ACCT_ID)",	acctID);
		param.put("@(S_EVA_GBN)",	valueDueDate.getEvaGbn());
		param.put("@(S_YYYYMM)"	,	valueDueDate.getYyyymm());
		
		try {
			sqlHouseExcuteDao.crudQuery(157, param);
			result = 1;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	/**
	 * <b>회기년월 구하기</b>
	 * @param yyyymm
	 * @return 입력된 yyyymm 에 대한 회기년월
	 * @throws Exception 
	 */
	public JSONObject getPeriodYyyyMm(String yyyymm) throws Exception {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("@(N_ACCT_ID)", loginSessionInfoFactory.getObject().getAcctID());
		param.put("@(S_YYYYMM)"	, yyyymm);
		
		return (JSONObject) sqlHouseExcuteDao.selectJSONArray(156, param).get(0);
	}
}
