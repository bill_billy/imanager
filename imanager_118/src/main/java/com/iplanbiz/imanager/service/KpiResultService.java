package com.iplanbiz.imanager.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.apache.ibatis.metadata.Table;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.imanager.dao.BasicDAO;
import com.iplanbiz.imanager.dao.KpiResultDAO;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;
import com.iplanbiz.imanager.dto.kpiResult;

@Service
@Transactional(readOnly=true)
public class KpiResultService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;

//	@Autowired
//	KpiTargetDAO kpiTargetDao;
	
	@Autowired
	DBMSDAO dbmsDao;
//	@Autowired
//	BasicDAO basicDao;
	
	@Autowired   
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	public JSONArray listMmCombo(String evagbn, String yyyy, String gubun) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EVA_GBN)"					, evagbn);
		map.put("@(S_YYYY)"						, yyyy);
		map.put("@(S_GUBUN)"					, gubun);
		
		return sqlHouseExcuteDao.selectJSONArray(162,map);
	}
	public JSONArray listKpiCombo(String evagbn, String yyyymm) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EMP_ID)"					, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(S_YYYYMM)"					, yyyymm);
		map.put("@(S_EVA_GBN)"					, evagbn);
		
		return sqlHouseExcuteDao.selectJSONArray(163,map);
	}
	
	public JSONArray listScidCombo(String evagbn, String evalg, String yyyymm, String kpiid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EMP_ID)"					, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(S_EVAL_G)"					, evalg);
		map.put("@(S_MT_ID)"					, kpiid);
		map.put("@(S_YYYYMM)"					, yyyymm);
		map.put("@(S_EVA_GBN)"					, evagbn);
		
		return sqlHouseExcuteDao.selectJSONArray(164,map);
	}
	public JSONArray selectKpiResult(String evagbn, String evalg, String yyyymm, String scid, String kpiid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EMP_ID)"					, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(S_SC_ID)"					, scid);
		map.put("@(S_MT_ID)"					, kpiid);
		map.put("@(S_EVAL_G)"					, evalg);
		map.put("@(S_EVA_GBN)"					, evagbn);
		map.put("@(S_YYYYMM)"					, yyyymm);
		
		return sqlHouseExcuteDao.selectJSONArray(165,map);
	}
	public JSONArray kpiResultCheck(String evagbn, String yyyy, String mm) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EVA_GBN)"					, evagbn);
		map.put("@(S_YYYY)"						, yyyy);
		map.put("@(S_MM)"						, mm);
		
		return sqlHouseExcuteDao.selectJSONArray(166, map);
	}
	public JSONArray checkDateKpiResult(String gubun, String evagbn, String yyyymm) throws Exception{
		JSONArray checkValue = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EVA_GBN)"					, evagbn);
		map.put("@(S_YYYY)"						, yyyymm);
		//주석좀 써놓았으면 종말 좋았을것을.. AA가 뭐고AB가 뭔지..
		if(gubun.equals("AA")){
			checkValue = sqlHouseExcuteDao.selectJSONArray(158, map);
		}else if(gubun.equals("AB")){
			checkValue = sqlHouseExcuteDao.selectJSONArray(159, map); 
		}
		return checkValue;
	}
	public JSONArray kpiResultCheckIMFE0020(String evagbn, String gubun, String yyyymm, String kpiid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EVA_GBN)"					, evagbn);
		map.put("@(S_GUBUN)"					, gubun);
		map.put("@(S_YYYYMM)"					, yyyymm);
		map.put("@(S_KPI_ID)"					, kpiid);
		
		return sqlHouseExcuteDao.selectJSONArray(146, map);
	}
	@Transactional
	public int kpiResultInsertOne(kpiResult kpi) throws Exception{
		int result = 0;
		TableObject imfe0020 = new TableObject();
		imfe0020.put("ACCT_ID"					, loginSessionInfoFactory.getObject().getAcctID());
		imfe0020.put("EVA_GBN"						, kpi.getpEvaGbn(), true);
		imfe0020.put("GUBUN"						, "A", true);
		imfe0020.put("YYYYMM"						, kpi.getChargeOneYyyymm(), true);
		imfe0020.put("KPI_ID"						, kpi.getChargeOneKpiID(), true);
		imfe0020.put("CHARGE_ID"					, kpi.getChargeOneID());
		imfe0020.put("CHARGE_NM"					, kpi.getChargeOneName());
		imfe0020.put("CHARGE_CONFIRM"				, kpi.getChargeOneConfirm());
		imfe0020.putCurrentTimeStamp("CHARGE_DATE");
		imfe0020.put("USER_ID"						, kpi.getUserOneID());
		imfe0020.put("USER_NM"						, kpi.getUserOneName());
		imfe0020.put("USER_CONFIRM"					, "");
		imfe0020.put("USER_DATE"					, "");
		
		TableObject imfe0020_charge = new TableObject();
		imfe0020_charge.put("ACCT_ID"				, loginSessionInfoFactory.getObject().getAcctID(),true);
		imfe0020_charge.put("EVA_GBN"				, kpi.getpEvaGbn(), true);
		imfe0020_charge.put("GUBUN"					, "A", true);
		imfe0020_charge.put("YYYYMM"				, kpi.getChargeOneYyyymm(), true);
		imfe0020_charge.put("KPI_ID"				, kpi.getChargeOneKpiID(), true);
		imfe0020_charge.put("CHARGE_ID"				, kpi.getChargeOneID());
		imfe0020_charge.put("CHARGE_NM"				, kpi.getChargeOneName());
		imfe0020_charge.put("CHARGE_CONFIRM"		, kpi.getChargeOneConfirm());
		imfe0020_charge.putCurrentTimeStamp("CHARGE_DATE");
		
		TableObject imfe0020_user = new TableObject();
		imfe0020_user.put("ACCT_ID"					, loginSessionInfoFactory.getObject().getAcctID(),true);
		imfe0020_user.put("EVA_GBN"					, kpi.getpEvaGbn(), true);
		imfe0020_user.put("GUBUN"					, "A", true);
		imfe0020_user.put("YYYYMM"					, kpi.getChargeOneYyyymm(), true);
		imfe0020_user.put("KPI_ID"					, kpi.getChargeOneKpiID(), true);
		imfe0020_user.put("USER_ID"					, kpi.getUserOneID());
		imfe0020_user.put("USER_NM"					, kpi.getUserOneName()); 
		imfe0020_user.put("USER_CONFIRM"			, kpi.getUserOneConfirm());
		imfe0020_user.putCurrentTimeStamp("USER_DATE");
		if(kpiResultCheckIMFE0020(kpi.getpEvaGbn(),"A", kpi.getChargeOneYyyymm(),kpi.getChargeOneKpiID()).size()==0){
			dbmsDao.insertTable("IMFE0020", imfe0020);
			result = 1;
		}else{
			if(kpi.getUserGubn().equals("Charge")){
				dbmsDao.updateTable("IMFE0020", imfe0020_charge.getKeyMap(),imfe0020_charge);
			}else{
				dbmsDao.updateTable("IMFE0020", imfe0020_user.getKeyMap(), imfe0020_user);
			}
			result = 2;
		}
		return result;
	}
	@Transactional
	public int kpiResultInsertAll(kpiResult kpi) throws Exception{
		int result = 0;
		ArrayList<LinkedHashMap<String, Object>> arrInsertIMFE0020 = new ArrayList<LinkedHashMap<String,Object>>();
		ArrayList<LinkedHashMap<String, Object>> arrUpdateChargeIMFE0020 = new ArrayList<LinkedHashMap<String,Object>>();
		ArrayList<LinkedHashMap<String, Object>> arrUpdateUserIMFE0020 = new ArrayList<LinkedHashMap<String,Object>>();
		for(int i=0;i<kpi.getChargeKpiID().length;i++){
			TableObject imfe0020 = new TableObject();
			TableObject imfe0020_charge = new TableObject();
			TableObject imfe0020_user = new TableObject();
			imfe0020.put("ACCT_ID"				, loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0020.put("EVA_GBN"				, kpi.getpEvaGbn(), true);
			imfe0020.put("GUBUN"				, "A", true);
			imfe0020.put("YYYYMM"				, kpi.getChargeYyyymm()[i], true);
			imfe0020.put("KPI_ID"				, kpi.getChargeKpiID()[i], true); 
			imfe0020.put("CHARGE_ID"			, kpi.getChargeID()[i]);
			imfe0020.put("CHARGE_NM"			, kpi.getChargeName()[i]);
			imfe0020.put("CHARGE_CONFIRM"		, kpi.getConfirm());
			imfe0020.putCurrentTimeStamp("CHARGE_DATE");
			imfe0020.put("USER_ID"				, kpi.getUserID()[i]);
			imfe0020.put("USER_NM"				, kpi.getUserName()[i]);
			imfe0020.put("USER_CONFIRM"			, "");
			imfe0020.put("USER_DATE"			, "");
			
			imfe0020_charge.put("ACCT_ID"				, loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0020_charge.put("EVA_GBN"				, kpi.getpEvaGbn(), true);
			imfe0020_charge.put("GUBUN"					, "A", true);
			imfe0020_charge.put("YYYYMM"				, kpi.getChargeYyyymm()[i], true);
			imfe0020_charge.put("KPI_ID"				, kpi.getChargeKpiID()[i], true);
			imfe0020_charge.put("CHARGE_ID"				, kpi.getChargeID()[i]);
			imfe0020_charge.put("CHARGE_NM"				, kpi.getChargeName()[i]);
			imfe0020_charge.put("CHARGE_CONFIRM"		, kpi.getConfirm());
			imfe0020_charge.putCurrentTimeStamp("CHARGE_DATE");
			
			imfe0020_user.put("ACCT_ID"				, loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0020_user.put("EVA_GBN"				, kpi.getpEvaGbn(), true);
			imfe0020_user.put("GUBUN"				, "A", true);
			imfe0020_user.put("YYYYMM"				, kpi.getChargeYyyymm()[i], true);
			imfe0020_user.put("KPI_ID"				, kpi.getChargeKpiID()[i], true);
			imfe0020_user.put("USER_ID"				, kpi.getUserID()[i]);
			imfe0020_user.put("USER_NM"				, kpi.getUserName()[i]);
			imfe0020_user.put("USER_CONFIRM"		, kpi.getConfirm());
			imfe0020_user.putCurrentTimeStamp("USER_DATE");
			
			if(kpiResultCheckIMFE0020(kpi.getpEvaGbn(),"A", kpi.getChargeYyyymm()[i],kpi.getChargeKpiID()[i]).size() == 0){
				if(!kpi.getUserConfirm()[i].toString().equals("Y")){
					if(kpi.getChargeConfirm()[i].toString().equals("")){
						arrInsertIMFE0020.add(imfe0020);
					}
				}
			}else{
				if(kpi.getUserGubn().equals("Charge")){
					if(!kpi.getUserConfirm()[i].toString().equals("Y")){
						arrUpdateChargeIMFE0020.add(imfe0020_charge);
					}
				}else{
					if(kpi.getChargeConfirm()[i].toString().equals("Y")){
						arrUpdateUserIMFE0020.add(imfe0020_user);
					}
				}
			}
		}
		try{
			if(arrInsertIMFE0020.size() > 0){
				dbmsDao.insertTable("IMFE0020", arrInsertIMFE0020);
			}
			if(kpi.getUserGubn().equals("Charge")){
				dbmsDao.updateTable("IMFE0020", arrUpdateChargeIMFE0020);
			}else{
				dbmsDao.updateTable("IMFE0020", arrUpdateUserIMFE0020);
			}
			result = 1;
		}catch(Exception e){
			e.printStackTrace();
			result = 0;
			throw e;
		}
		return result;
	}
	public JSONArray kpiResultCheckValue(String evaGbn, String yyyymm, String kpiid, String analCycle, String colGbn) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"									, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EVA_GBN)"									, evaGbn);
		map.put("@(S_YYYYMM)"									, yyyymm);
		map.put("@(S_KPI_ID)"									, kpiid);
		map.put("@(S_ANAL_CYCLE)"								, analCycle);
		map.put("@(S_COL_GBN)"									, colGbn);
		
		return sqlHouseExcuteDao.selectJSONArray(196, map);
	}
	@Transactional
	public int updateIMFE0029(kpiResult kpi) throws Exception{
		int result = 0;
		ArrayList<LinkedHashMap<String, Object>> arrInsertIMFE0029 = new ArrayList<LinkedHashMap<String,Object>>();
		ArrayList<LinkedHashMap<String, Object>> arrUpdateIMFE0029 = new ArrayList<LinkedHashMap<String,Object>>();
		for(int i=0;i<kpi.getColValue().length;i++){
			TableObject imfe0029 = new TableObject();
			imfe0029.put("ACCT_ID"					, loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0029.put("EVA_GBN"					, kpi.getpEvaGbn(), true);
			imfe0029.put("YYYYMM"					, kpi.getYyyymm()[i], true);
			imfe0029.put("KPI_ID"					, kpi.getKpiID()[i], true);
			imfe0029.put("ANAL_CYCLE"				, kpi.getAnalCycle()[i], true);
			imfe0029.put("COL_GBN"					, kpi.getColGbn()[i], true);
			imfe0029.put("COL_VALUE"				, kpi.getColValue()[i]);
			
			if(kpiResultCheckValue(kpi.getpEvaGbn(), kpi.getYyyymm()[i], kpi.getKpiID()[i], kpi.getAnalCycle()[i], kpi.getColGbn()[i]).size() > 0 ){
				imfe0029.putCurrentDate("INSERT_DAT");
				arrUpdateIMFE0029.add(imfe0029);
			}else{
				imfe0029.putCurrentDate("UPDATE_DAT");
				arrInsertIMFE0029.add(imfe0029);
			}
		}
		try{
			dbmsDao.insertTable("IMFE0029", arrInsertIMFE0029);
			dbmsDao.updateTable("IMFE0029", arrUpdateIMFE0029);
			result = 1;
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return result;
	}
}
