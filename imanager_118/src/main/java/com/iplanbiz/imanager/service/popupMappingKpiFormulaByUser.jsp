<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE HTML>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>산식리스트</title>
        <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/style/style.common.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/style.blueish.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/js/thirdparty/BUY/jqwidget/4.1.2/styles/jqx.base.css"/>
	    <link rel="stylesheet" type="text/css" href="../../resources/cmresource/css/iplanbiz/theme/style01/css/jqx.blueish.css"/>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_lv2.js"></script> 
        <script src="../../resources/cmresource/js/iplanbiz/web/core_2.0.js"></script>
        <script src="../../resources/cmresource/js/iplanbiz/web/include_lite_jqwidget.js"></script>
        <script>
        	//전역 변수.
        	//변경된 년도별 산식 데이터 
        	//변수명 : loadedData
        	/*
        	구조 예시
        	[
        	 {
        		 END_YYYY:2016,
        		 START_YYYY:2014,
        		 VALUE:[
        	     			COL_GBN:'A',COL_NM:'B',COL_UNIT:'A',COL_SYSTEM:'A',IF_COD:'B'
        		        ]
        	 } 
        	 ]
        	*/
        	var loadedData = []; 
        	//콤보 데이터
        	var cboUseUnit = [];
        	var cboDataSys = [];
        	var cboIfCod = [];
        	var cboYyyy = [];
        	//문서 로딩이 완료되면 처음 수행되는 로직
        	$(document).ready(function(){
				init();
				  
				setEvent(); 
        		search(); 
        	}); 
        	
        	//초기화, 그리드 초기 셋팅 등 화면을 초기화 할 때 사용함.
        	function init(){
        		
        		//단위
        		var step1= $i.sqlhouse(13,{COML_COD:"USEUNIT"});
        		step1.done(function(c){
        			if(c.returnCode=="SUCCESS"){
        				cboUseUnit = c.returnArray;
        			}else{
        				alert("단위 데이터가 없습니다.");
        			}
        		});
        		//항목 실적입력 구분
        		var step2= $i.sqlhouse(13,{COML_COD:"DATA_SYS"});
        		step2.done(function(c){
        			if(c.returnCode=="SUCCESS"){
        				cboDataSys = c.returnArray;
        			}else{
        				alert("항목 실적입력 구분 데이터가 없습니다.");
        			}
        		});
        		//연계 항목 리스트
        		var step3 = $i.post("./getIfCodAdmin",{});
        		step3.done(function(c){
        			if(c.returnCode=="SUCCESS"){
        				cboIfCod = c.returnArray;
        			}else{
        				alert("연계 항목 데이터가 없습니다.");
        			}
        		});
        		$.when(step1,step2,step3).done(function(){
        			var calculateListString = "${param.calculateList}";
            		if(calculateListString!=""){ 
            			loadedData = JSON.parse(unescape(calculateListString)); 
            		}
            		setCboYyyyData(); 
            		$("#btnInsert").jqxButton({width:'', theme:'blueish'});
            		$("#btnClose").jqxButton({width:'', theme:'blueish'});
            		
    				if($("[name='colunit']").length >0){  
    					$("[name='colunit']").jqxDropDownList({animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 90, dropDownHeight: 180, width: 90, height: 19,  theme:'blueish'});
    					$("[name='colsystem']").jqxDropDownList({animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 90, dropDownHeight: 180, width: 90, height: 19,  theme:'blueish'});
    					$("[name='ifcod']").jqxDropDownList({animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 170, dropDownHeight: 180, width: 170, height: 19,  theme:'blueish'});
    				}
        		});				
        	}
        	//조회
        	function search(){
        		var acctid = "${param.acctid}";
        		var data = $i.sqlhouse(218,{ACCT_ID:acctid,S_CAL_NM:$("#txtSearchText").val()});  
        		data.done(function(res){
		            // prepare the data
		            var source =
		            {
		                datatype: "json",
		                datafields: [ 
		                    { name: 'CAL_COD', type: 'string'},
		                    { name: 'CAL_NM', type: 'string'}
		                ],
		                localdata: res,
		                updaterow: function (rowid, rowdata, commit) {
		                    commit(true);
		                }
		            };
					var alginLeft = function (row, columnfield, value) {//left정렬
						return '<div id="userName-' + row + '"style="text-align:left; margin:4px 0px 0px 5px;">' + value + '</div>';
					};
					var alginRight = function (row, columnfield, value) {//right정렬
						return '<div id="userName-' + row + '"style="text-align:right; margin:4px 5px 0px 0px;">' + value + '</div>';
					};
					var detail = function (row, columnfield, value) {//right정렬
						var calcod = $("#gridCallist").jqxGrid("getrowdata", row).CAL_COD;
						var calnm = $("#gridCallist").jqxGrid("getrowdata", row).CAL_NM;
					
						var link = "<a href=\"javascript:detailNew('"+calcod+"','"+calnm+"')\" style='color:black; text-decoration:underline;' >" + value + "</a>";
						return "<div style='text-align:left; margin:4px 0px 0px 10px;'>" + link + "</div>";
					};
		            var dataAdapter = new $.jqx.dataAdapter(source, {
		                downloadComplete: function (data, status, xhr) { },
		                loadComplete: function (data) { },
		                loadError: function (xhr, status, error) { }
		            });
		            var dataAdapter = new $.jqx.dataAdapter(source);
		            $("#gridCallist").jqxGrid(
		            {
	              	 	width: '100%',
		                height:'100%',
						altrows:true,
						pageable: false,
		                source: dataAdapter,
						theme:'blueish',
						sortable: true,
		                columnsresize: true,
						columnsheight: 25,
						rowsheight: 25,
		                columns: [
		                   { text: 'ID', datafield: 'CAL_COD', width: 52, align:'center', cellsalign: 'center'},
		                   { text: '산식패턴', datafield: 'CAL_NM', align:'center', cellsalign: 'left',  cellsrenderer: detail}
		                ]
		            });
        		});
        	}
        	
        	//입력
        	function insert(){
        	}
        	//삭제
        	function remove(){    
        	}
        	//각종 이벤트 바인딩.
        	function setEvent(){
        		$("#cboYyyy").on("change",function(res){
					detail($("#cboYyyy").val());
					toggleElementNewItem(); 
				});
        	}
        	function toggleElementNewItem(){
        		if($("#cboYyyy").val()==""){
					$("[data-role='element_new_item']").show();
					var maxYear = Enumerable.From(loadedData).Max(function(c){return c.START_YYYY;});
					maxYear = parseInt(maxYear)+1;
					$("#txtStartYyyy").val(maxYear); 
				}
				else{
					$("[data-role='element_new_item']").hide(); 
				}
        	}
        	//여기 부터 추가 스크립트를 작성해 주세요. 
        	function makeCboYyyy(res){
				$("#cboYyyy").jqxDropDownList({selectedIndex:0,source:res,valueMember:"END_YYYY",displayMember:"YYYY",animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 170, dropDownHeight: 180, width: 170, height: 19,  theme:'blueish'});
				toggleElementNewItem();
				detail($("#cboYyyy").val());
        	}
        	function setCboYyyyData(){
        		cboYyyy = Enumerable.From(loadedData).Select(function(c){
					return { END_YYYY:c.END_YYYY, START_YYYY:c.START_YYYY,YYYY:c.START_YYYY+"~"+c.END_YYYY};
				}).OrderByDescending(function(c){return c.END_YYYY;}).ToArray();
        		
        		if(cboYyyy.length==0){  
        			var prom = $i.service("listYearAdmin",["${param.mtid}"]);
            		prom.done(function(c){ 
            			if(c.returnCode=="SUCCESS"){
            				cboYyyy = c.returnArray;
            				cboYyyy = cboYyyy.concat([{END_YYYY:"",START_YYYY:"",YYYY:"==신규=="}]);    
            				makeCboYyyy(cboYyyy);
            			}else{
            				alert("연계 항목 데이터가 없습니다.");
            			}
            		});
        		}else{
        			cboYyyy = cboYyyy.concat([{END_YYYY:"",START_YYYY:"",YYYY:"==신규=="}]);    
        			makeCboYyyy(cboYyyy);
        		}
        	}
        	
        	//detail
        	function detail(endyyyy){
        		var grdCalListSelectedIndex = $("#gridCallist").jqxGrid("selectedrowindex");
        		if(endyyyy==""&&grdCalListSelectedIndex!=-1){        			
	        			var rowid = $("#gridCallist").jqxGrid("getrowid",grdCalListSelectedIndex);
	        			var rowdata = $("#gridCallist").jqxGrid("getrowdatabyid",rowid);
	        			detailNew(rowdata.CAL_COD,rowdata.CAL_NM);
        		}else{
		        		//산식 항목명 불러오기 
		        		detailData = Enumerable.From(loadedData).Where(function(c){ return c.END_YYYY==endyyyy;}).FirstOrDefault();
		        		if(endyyyy!=""&&detailData==null){ 
			        		$i.post("./getCalculateListByMtIDAdmin",{mtid:"${param.mtid}",endyyyy:''}).done(function(c){
			        			if(c.returnCode=="SUCCESS"){
			        				var res = c.returnArray; 
			        				var beforeyyyy = ""; 
			        				var dataList = [];
			        				var dataObject = null;;
			        				for(var i = 0; i < res.length;i++){
			        					
			        					var calcod = res[i].CAL_COD;
			        					var calnm = res[i].CAL_NM;
			        					var endyyyy = res[i].END_YYYY;
			        					var startyyyy = res[i].START_YYYY;
			        					var colgbn = res[i].COL_GBN;
			        					var colnm = res[i].COL_NM;
			        					var unit = res[i].COL_UNIT;
			        					var system = res[i].COL_SYSTEM;
			        					var ifcod = res[i].IF_COD; 
 
			        					if(endyyyy!=beforeyyyy){
			        						beforeyyyy = endyyyy;
			        						if(dataObject!=null){
			        							dataList.push(dataObject);
			        						}
			        						dataObject = {
				        							CAL_COD : calcod,
					        						CAL_NM : calnm,
					        						END_YYYY:endyyyy,
					        						START_YYYY:startyyyy,
					        						VALUE:[]
				        					};
			        					}		
			        					dataObject.VALUE.push({
			        						COL_GBN:colgbn,
			        						COL_NM:colnm,
			        						COL_UNIT:unit,
			        						COL_SYSTEM:system,
			        						IF_COD : ifcod			        						
			        					}); 
			        					if(i==res.length-1){
		        							dataList.push(dataObject);
		        						}
			        				}
			        				loadedData = dataList; 
			        				if(dataList.length!=0)
			        					detailWrite(dataList[0]);			        				 
			        			}else{
			        				alert("연계 항목 데이터가 없습니다."); 
			        			}
			        		});
		        		}else{  
		        			var value = null;
		        			if(detailData!=null) value = detailData;
		        			else value ={ CAL_COD:"",CAL_NM:"",VALUE:[]}; 
		        			detailWrite(value); 
		        		}
        		}
        	}
        	function detailWrite(data){
        		
        		var calcod="";
        		var calnm="";
        		calcod = data.CAL_COD;
    			calnm = data.CAL_NM; 
        		var res = data.VALUE; 
        		writeLabel(calcod,calnm); 
        		
        		var tbody = $("#mainList"); 
				tbody.empty();  
				for(var i = 0; i < res.length;i++){
					var tr = $("<tr><td data-td='colgbn' style='text-align:center'><input type='hidden' name='colgbn' value='"+res[i].COL_GBN+"'/>"+res[i].COL_GBN+"</td>"+
										"<td data-td='colnm'><input type='text' name='colnm'/></td>"+
										"<td data-td='unit'><div name='unit'></div></td>"+
										"<td data-td='datasys'><div name='datasys' data-index='"+parseInt(i+1)+"'></div></td>"+
										"<td data-td='ifcod'><div id='cboIfCod"+parseInt(i+1)+"' name='ifcod'></div></td>"+  
								"</tr>").appendTo(tbody);
					
					tr.find("[name=colnm]").val(res[i].COL_NM);    
					tr.find("[name=unit]").jqxDropDownList({source:cboUseUnit,displayMember:"COM_NM",valueMember:"COM_COD", animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 90, dropDownHeight: 180, width: 90, height: 19,  theme:'blueish'});
					tr.find("[name=unit]").val(res[i].COL_UNIT);
					tr.find("[name=datasys]").jqxDropDownList({source:cboDataSys,displayMember:"COM_NM",valueMember:"COM_COD", animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 90, dropDownHeight: 180, width: 120, height: 19,  theme:'blueish'});
					tr.find("[name=datasys]").val(res[i].COL_SYSTEM);
					if(res[i].COL_SYSTEM == "A"){
						tr.find("[name=ifcod]").jqxDropDownList({source:cboIfCod,displayMember:"IF_COD_NM",valueMember:"IF_COD", animationType: 'fade', dropDownHorizontalAlignment: 'right', disabled:true, dropDownWidth: 90, dropDownHeight: 180, width: 180, height: 19,  theme:'blueish'});
						tr.find("[name=ifcod]").val(res[i].IF_COD);
					}else{
						tr.find("[name=ifcod]").jqxDropDownList({source:cboIfCod,displayMember:"IF_COD_NM",valueMember:"IF_COD", animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 90, dropDownHeight: 180, width: 180, height: 19,  theme:'blueish'});
						tr.find("[name=ifcod]").val(res[i].IF_COD);
					}
    			}
				$("[name='datasys']").on("change", function(event){
					var index = $(this).data("index");
					if(event.args.item.value == "A"){
						$("#cboIfCod"+index).val("100");
						$("#cboIfCod"+index).jqxDropDownList({ disabled : true });
					}else{
						$("#cboIfCod"+index).jqxDropDownList({ disabled : false });
					}
				});
        	}
			
        	function writeLabel(calcod,calnm){
        		$("#spanCalCod").html(calcod);
        		$("#spanCalNm").html(calnm);
        		$("#hiddenCalCod").val(calcod);
        		$("#hiddenCalNm").val(calnm);
        	}
        	
        	function detailNew(calcod, calnm){
				writeLabel(calcod,calnm);        		        		
        		var str = calnm.replace(/[^a-zA-Z]/gi,"");
        		var tbody = $("#mainList");
				tbody.empty();  
        		for(var i = 0; i < str.length; i++){ 
        			var tr = $("<tr><td data-td='colgbn' style='text-align:center'><input type='hidden' name='colgbn' value='"+str[i]+"'/>"+str[i]+"</td>"+
													"<td data-td='colnm'><input type='text' name='colnm'/></td>"+ 
													"<td data-td='unit'><div name='unit'></div></td>"+
													"<td data-td='datasys'><div name='datasys' data-index='"+parseInt(i+1)+"'></div></td>"+ 
													"<td data-td='ifcod'><div id='cboIfCod"+parseInt(i+1)+"' name='ifcod'></div></td>"+  
								"</tr>").appendTo(tbody);
					tr.find("[name=unit]").jqxDropDownList({selectedIndex:0,source:cboUseUnit,displayMember:"COM_NM",valueMember:"COM_COD", animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 90, dropDownHeight: 180, width: 90, height: 19,  theme:'blueish'});								
					tr.find("[name=datasys]").jqxDropDownList({selectedIndex:0,source:cboDataSys,displayMember:"COM_NM",valueMember:"COM_COD", animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 90, dropDownHeight: 180, width: 120, height: 19,  theme:'blueish'});								 
					tr.find("[name=ifcod]").jqxDropDownList({selectedIndex:0,source:cboIfCod,displayMember:"IF_COD_NM",valueMember:"IF_COD", disabled:true, animationType: 'fade', dropDownHorizontalAlignment: 'right', dropDownWidth: 90, dropDownHeight: 180, width: 180, height: 19,  theme:'blueish'});
					tr.find("[name=ifcod]").val("100");
        		}			 
        		$("[name='datasys']").on("change", function(event){
					var index = $(this).data("index");
					if(event.args.item.value == "A"){
						$("#cboIfCod"+index).val("100");
						$("#cboIfCod"+index).jqxDropDownList({ disabled : true });
					}else{
						$("#cboIfCod"+index).jqxDropDownList({ disabled : false });
					}
				});
        	}
        	
        	function save(gubn){
        		var key = $("#cboYyyy").val();
        		if(key==""){
        			var startYyyy = $("#txtStartYyyy").val();
        			var reg = /^[0-9]{4}$/;
        			if(reg.test(startYyyy)==false){
        				alert("올바른 연도가 아닙니다.");
        				if(gubn == "send"){
        					return "false";
        				}else{
        					return;
        				}
        			}
        			if(loadedData.length!=0){
	        			var maxYyyy = Enumerable.From(loadedData).Max(function(c){ return c.START_YYYY;});
	        			if(maxYyyy!=null&&parseInt(maxYyyy)>=parseInt(startYyyy )){
	        				alert("시작년도가 올바르지 않습니다 : 이미 해당 기간에 사용중인 산식이 있습니다.");
	        				if(gubn == "send"){
	        					return "false";
	        				}else{
	        					return;
	        				}
	        			} 
        			}
        		}
        		
        		if(key=="" && Enumerable.From(loadedData).Where(function(c){return c.END_YYYY=="";}).Count()==0){
					loadedData.push({CAL_COD : $("#hiddenCalCod").val(),					//산식코드
    					CAL_NM : $("#hiddenCalNm").val(),END_YYYY:"",START_YYYY:"",YYYY:"==신규==",VALUE:[]});  
				}
        		
        		
        		var data = Enumerable.From(loadedData).Where(function(c){ return c.END_YYYY==key;}).FirstOrDefault();
        		data.VALUE=[];
        		 
        		//신규일경우 전처리 
        		if(key==""){
        			//기존 최대년도의 END_YYYY 변경.	
    				var maxYyyy = Enumerable.From(loadedData).Max(function(c){ return c.START_YYYY;});
    				var maxData = Enumerable.From(loadedData).Where(function(c){return c.START_YYYY==maxYyyy;}).FirstOrDefault(); 
    				if(maxData!=null){
    					maxData.END_YYYY = (parseInt($("#txtStartYyyy").val())-1).toString(); 
    				} 
    				//신규의 시작 종료년도 설정.  
    				data.END_YYYY = $("#txtEndYyyy").val(); 
    				data.START_YYYY = $("#txtStartYyyy").val();    			
        		}
        		$.each($("#mainList").find("tr"),function(idx,item){
        			item = $(item); 
        			var value = {
        					END_YYYY:data.END_YYYY,
        					START_YYYY:data.START_YYYY,        					
        					CAL_COD : $("#hiddenCalCod").val(),					//산식코드
        					CAL_NM : $("#hiddenCalNm").val(),					//산식명
        					COL_GBN : item.find("[name=colgbn]").val(),		//항목코드
        					COL_NM : item.find("[name=colnm]").val(),			//항목명
        					COL_UNIT : item.find("[name=unit]").val(),			//항목단위
        					COL_SYSTEM : item.find("[name=datasys]").val(),	//입력구분
        					IF_COD : item.find("[name=ifcod]").val()				//연계항목
        			};
        			if(key==""){
        				value.END_YYYY = $("#txtEndYyyy").val();
        				value.START_YYYY = $("#txtStartYyyy").val(); 
        			}
        			data.CAL_COD = $("#hiddenCalCod").val();				//산식코드
        			data.CAL_NM  = $("#hiddenCalNm").val();				//산식명
        			data.VALUE.push(value);   
        		});
        		//신규일경우만 년도 콤보 재조회
        		if(key==""){
        			setCboYyyyData();
        		}
        		if(gubn == "send"){
        			
   				}else{
   					alert("임시 저장 되었습니다.");
   				}
        	}
        	
        	function sendParentData(){
        		var flag = save("send");
        		if(flag != "false"){
	        		var maxCalInfo = Enumerable.From(loadedData).Where(function(c){return c.END_YYYY=='9999';}).FirstOrDefault();
	        		if(maxCalInfo==null){
	        			alert("산식이 등록되지 않았습니다.");return false;
	        		}
	        		$("#txtCalnm"			   , opener.document).val(maxCalInfo.VALUE[0].CAL_NM);        		
	        		$("#hiddenCalculateList"  , opener.document).val(JSON.stringify(loadedData));
	        		alert("지표그룹수정 페이지에서 저장 버튼을 통해 최종 반영 됩니다.");
					self.close();
        		}
        	}
        </script>
    </head>
    <body class='blueish'>
	<!--**각 div사이즈, 너비 인라인 스타일 추가 및 수정 가능 (단, input,combobox,grid cell 높이 및 사이즈는 스크립트 참조)-->
		<div class="wrap" style="width:900px;min-height:300px; margin:0 1%;"><!--팝업창 사이즈 조정-->
			<div class="container  f_left" style="width:100%; margin:10px 0;">
				<div class="content f_left" style=" width:29%; margin-right:1%;">
					<div class="grid f_left" style="width:100%; height:276px;">
						<div class="label type2 f_left m_r10" style = "float:left;">산식패턴</div>
						<div class="combobox" style = "float:left;">
							<input id="txtSearchText" name="searchText" onKeyPress="if(event.keyCode==13){ search();}"/>   
						</div> 
						<div id="gridCallist" style="margin-top:53px;"></div>  
					</div>
				</div>
				<div class="content f_right" style=" width:70%;">
					<div class="group f_left  w100p m_b5" id="divYyyy">
						<div class="label type2 f_left m_r10" style = "float:left;">년도</div>
						<div class="combobox" style = "float:left;">
							<div id="cboYyyy" name="yyyy">	</div> 
						</div> 
						<div class="label type2 f_left m_r10" data-role='element_new_item' style = "float:left;">시작년도</div>
						<div class="combobox" style = "float:left;">
							<input type="text" data-role='element_new_item'  style="width:100px;" id="txtStartYyyy"/>
						</div>
						<div class="label type2 f_left m_r10" data-role='element_new_item'  style = "float:left;">종료년도</div>
						<div class="combobox" style = "float:left;">
							<input type="text" value="9999" data-role='element_new_item' id="txtEndYyyy" style="width:100px;" readonly/> 
						</div>
					</div>
					<div class="group f_left  w100p">
						<div class="label type2 f_left m_r10">ID:<span class="label sublabel" id="spanCalCod">${calcod}</span></div>
						<div class="label type2 f_left m_r10">산식패턴:<span class="label sublabel" id="spanCalNm">${calnm}</span></div>
					</div>
					<div class="datatable f_left auto" style="width:100%; margin:0px 0; height;">
						<input type="hidden" id="hiddenCalCod" name="calcod" value="${calcod}" />
						<input type="hidden" id="hiddenCalNm" name="calnm" value="${calnm}" />
						<table width="100%" cellspacing="0" cellpadding="0" border="0" >
							<thead>
								<tr>
									<th style="width:10%;"><div>항목</div></th>
									<th style="width:30%;"><div>항목명<span class="th_must"></span></div></th>
									<th style="width:10%;"><div>항목단위<span class="th_must"></span></div></th>
									<th style="width:20%;"><div>입력구분<span class="th_must"></span></div></th>
									<th style="width:30%;"><div>연계항목<span class="th_must"></span></div></th>
								</tr> 
							</thead>  
							<tbody id="mainList"> 
				
							</tbody>
						</table>
					</div>
					<div class="group_button f_right m_t5">
						<div class="button type2 f_left">  
							<input type="button" value="저장" id='btnInsert' width="100%" height="100%" onclick="save();" />
							<input type="button" value="확정 후 닫기" id='btnClose' width="100%" height="100%" onclick="sendParentData();" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>

