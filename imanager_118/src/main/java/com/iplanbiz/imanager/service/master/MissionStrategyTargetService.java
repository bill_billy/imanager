package com.iplanbiz.imanager.service.master;

import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO; 
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class MissionStrategyTargetService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	public JSONArray selectStrategyTarget1() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		return sqlHouseExcuteDao.selectJSONArray(307, map); //쿼리 메소드 만들기 
	}
	public JSONArray selectStrategyTarget2(String stramsid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"				, stramsid);
		return sqlHouseExcuteDao.selectJSONArray(308, map); //쿼리 메소드 만들기  
	} 
	public JSONArray checkMsId100() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
//		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		return sqlHouseExcuteDao.selectJSONArray(313, map);
	} 
	public JSONArray checkStraId100(String stramsid) throws Exception{ 
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"				, stramsid);    
		return sqlHouseExcuteDao.selectJSONArray(310, map);
	}
	public JSONArray maxStraid(String stramsid) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"				, stramsid);    
		return sqlHouseExcuteDao.selectJSONArray(309, map);
	}
	public JSONArray maxMsid(String msid) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		return sqlHouseExcuteDao.selectJSONArray(314, map);
	}
	public void insertStrategtTarget1(String msid, String msNM, String startYYYY, String endYYYY, String useYN, String msDEF) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		TableObject idpfe0001 = new TableObject();
		 
//		idpfe0001.put("MS_ID"		, loginSessionInfoFactory.getObject().getAcctID(), true);		
		idpfe0001.put("INSERT_EMP"	, loginSessionInfoFactory.getObject().getUserId());
		idpfe0001.put("UPDATE_EMP"	, loginSessionInfoFactory.getObject().getUserId());
		idpfe0001.putCurrentDate("INSERT_DAT");
		idpfe0001.putCurrentDate("UPDATE_DAT");		
		
		
		String[] arrayClob = null;
		JSONObject checkMsId= null;
		checkMsId = (JSONObject) checkMsId100().get(0);
		/*if(checkMsId.get("CNT").equals("0")){ 
				
			idpfe0001.put("MS_ID" 		, "100",true);
			idpfe0001.put("MS_NM" 		, "전략목표 없음");
			idpfe0001.put("START_YYYY"		, "0000");
			idpfe0001.put("END_YYYY"		, "0000");
			idpfe0001.put("USE_YN"		, "Y");
			idpfe0001.put("MS_DEF"		, "비고");  

			dbmsDao.insertTable("IDPFE0001",idpfe0001);
		}*/  
		
//		idpfe0001.put("MS_ID",  msid, true);    
		idpfe0001.put("MS_NM" , msNM); 
		idpfe0001.put("START_YYYY"		, startYYYY);
		idpfe0001.put("END_YYYY"	, endYYYY);
		idpfe0001.put("USE_YN"		, useYN);
		idpfe0001.put("MS_DEF"	, msDEF);   
				try{	 
			if(msid.equals("")){ //insert
				if(checkMsId.get("CNT").equals("0")){ 
					idpfe0001.put("MS_ID" 		, "100" , true);
				} else {
					JSONObject maxid = (JSONObject)maxMsid(msid).get(0); 
					idpfe0001.put("MS_ID" 		, maxid.get("MAX_MS_ID") , true);
					System.out.println("MSmaxid:::"+maxid.get("MAX_MS_ID"));
				}
//				idpfe0002.put("MS_ID"		, "100" , true);    
				dbmsDao.insertTable("IDPFE0001",idpfe0001);   
//				dbmsDao.insertTable("IDPFE0002","MS_ID",idpfe0002.getKeyMap(),idpfe0002);   
			}else{ //update 
				idpfe0001.put("MS_ID" 		, msid, true); 
				dbmsDao.updateTable("IDPFE0001",idpfe0001.getKeyMap(),idpfe0001); 
			}	
		}catch (Exception e) {			
			e.printStackTrace();
			throw e;
		}		
	}
 

	
	public void insertStrategtTarget2(String stramsid, String straid, String stranm, String straSubNm, String straUse, String straSort, String straDEF) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		TableObject idpfe0002 = new TableObject();
		  	
		idpfe0002.put("INSERT_EMP"	, loginSessionInfoFactory.getObject().getUserId());
		idpfe0002.put("UPDATE_EMP"	, loginSessionInfoFactory.getObject().getUserId());
		idpfe0002.putCurrentDate("INSERT_DAT");
		idpfe0002.putCurrentDate("UPDATE_DAT");		
			
		 
		String[] arrayClob = null;
		JSONObject checkStraId = null;
		checkStraId = (JSONObject) checkStraId100(stramsid).get(0);
		System.out.println("체크::::"+checkStraId);
	/*	if(checkStraId.get("CNT").equals("0")){ 
			
			idpfe0002.put("MS_ID" 		, "100", true);
			idpfe0002.put("STRA_ID" 	, "100", true);
			idpfe0002.put("STRA_NM"		, "전략목표 없음"); 
			idpfe0002.put("STRA_SUBNM"	, "세부전략목표 없음"); 
			idpfe0002.put("STRA_AREA"	, "영역"); 
			idpfe0002.put("USE_YN"		, "Y");
			idpfe0002.put("SORT_ORDER"	, 99999);
			idpfe0002.put("STRA_DEF"		, "비고란");

			
			dbmsDao.insertTable("IDPFE0002",idpfe0002);
		}*/ 
		
		idpfe0002.put("MS_ID" 		, stramsid, true);
		idpfe0002.put("F_STRA_NM"	, stranm);
		idpfe0002.put("S_STRA_NM"	, straSubNm);
		idpfe0002.put("USE_YN"		, straUse);   
		idpfe0002.put("SORT_ORDER"	, straSort);
		idpfe0002.put("STRA_DEF"	, straDEF);
		
		try{	
			if(straid.equals("")){ //insert
				if(checkStraId.get("CNT").equals("0")){ 
					idpfe0002.put("STRA_ID" 		, "100" , true);
				} else {
					JSONObject maxid = (JSONObject)maxStraid(stramsid).get(0); 
					idpfe0002.put("STRA_ID" 		, maxid.get("MAX_STRA_ID") , true);
					System.out.println("maxid:::"+maxid.get("MAX_STRA_ID"));
				}
//				idpfe0002.put("MS_ID"		, "100" , true);    
				dbmsDao.insertTable("IDPFE0002",idpfe0002);   
//				dbmsDao.insertTable("IDPFE0002","MS_ID",idpfe0002.getKeyMap(),idpfe0002);   
			}else{ //update
				idpfe0002.put("STRA_ID" 		, straid, true); 
				dbmsDao.updateTable("IDPFE0002",idpfe0002.getKeyMap(),idpfe0002); 
			}	
		}catch (Exception e) {			
			e.printStackTrace();
			throw e;
		}		 
	}
	
	//remove1
		public void removeStrategyTarget1(String msid) throws Exception{
		System.out.println(msid);
		//Validation 시작
		TableObject tableUsedStraID = new TableObject();
//		tableUsedStraID.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID(),true);
		tableUsedStraID.put("MS_ID", msid,true);
		if(dbmsDao.countTable("IDPFE0002", tableUsedStraID)>0) throw new RuntimeException("해당 코드에 매핑된 지표가 있어 삭제 할 수 없습니다.");
/*		if(dbmsDao.countTable("IMFE0005", tableUsedStraID)>0) throw new RuntimeException("해당 코드에 매핑된 세부전략이 있어 삭제 할 수 없습니다.");
		if(dbmsDao.countTable("IMFE0006", tableUsedStraID)>0) throw new RuntimeException("해당 코드에 매핑된 CSF가 있어 삭제 할 수 없습니다.");
*/		//Validation 끝
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		
//		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(MS_ID)"				, msid);
		String[] arrayClob = null;
		try{
			sqlHouseExcuteDao.crudQuery(317, map, arrayClob);
			
		}catch(Exception e){
			 
			e.printStackTrace();
			throw e;
		}		
	}
	public JSONArray checkStrategtTarget1(String msid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
//		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(MS_ID)"				, msid);
		 
		return sqlHouseExcuteDao.selectJSONArray(318, map);
	}
	
	
	//remove2
	public void removeStrategyTarget2(String msid,String straid) throws Exception{
		System.out.println(straid);
		//Validation 시작
		TableObject tableUsedStraID = new TableObject();
//		tableUsedStraID.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID(),true);
		tableUsedStraID.put("MS_ID", msid,true);
		tableUsedStraID.put("STRA_ID", straid,true);
		if(dbmsDao.countTable("IDPFE0003", tableUsedStraID)>0) throw new RuntimeException("해당 코드에 매핑된 지표가 있어 삭제 할 수 없습니다.");
/*		if(dbmsDao.countTable("IMFE0005", tableUsedStraID)>0) throw new RuntimeException("해당 코드에 매핑된 세부전략이 있어 삭제 할 수 없습니다.");
		if(dbmsDao.countTable("IMFE0006", tableUsedStraID)>0) throw new RuntimeException("해당 코드에 매핑된 CSF가 있어 삭제 할 수 없습니다.");
*/		//Validation 끝
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("@(MS_ID)"					, msid); 
		map.put("@(STRA_ID)"				, straid); 
		String[] arrayClob = null;
		try{
			sqlHouseExcuteDao.crudQuery(312, map, arrayClob);
			
		}catch(Exception e){
			 
			e.printStackTrace();
			throw e; 
		}		
	}
	public JSONArray checkStrategtTarget2(String msId,String straid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, msId);
		map.put("@(STRA_ID)"				, straid);
		 
		return sqlHouseExcuteDao.selectJSONArray(311, map); 
	}
	
	
}

