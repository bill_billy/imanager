package com.iplanbiz.imanager.service.master;

import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO; 
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class MissionSubStrategyTargetService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	public JSONArray selectStrategyTarget1(String msid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>(); 
		map.put("@(MS_ID)"				, msid);    
 
		return sqlHouseExcuteDao.selectJSONArray(321, map); //쿼리 메소드 만들기 
	}  
	
	public JSONArray selectStrategyTarget2(String submsId, String substraId) throws Exception{ 
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"				, submsId);    
		map.put("@(STRA_ID)"				, substraId);    
		return sqlHouseExcuteDao.selectJSONArray(323, map); //쿼리 메소드 만들기 
	}
	public JSONArray checkSubstraId100(String submsId, String substraId) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"				, submsId);    
		map.put("@(STRA_ID)"				, substraId);    
		return sqlHouseExcuteDao.selectJSONArray(324, map);  
	}
	public JSONArray maxSubstraid(String submsId, String substraId) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"				, submsId);    
		map.put("@(STRA_ID)"				, substraId);    
		return sqlHouseExcuteDao.selectJSONArray(325, map);
	}
	public void insertStrategtTarget(String submsId, String substraId, String substId, String substCd, String substNm1,String substNm2, String substUse, String substSort, String substDef) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		TableObject idpfe0003 = new TableObject(); 
		
		idpfe0003.put("MS_ID"		, submsId);	  
		idpfe0003.put("STRA_ID"		, substraId);		 
		idpfe0003.put("SUBSTRA_ID"		, loginSessionInfoFactory.getObject().getAcctID(), true);		 
		idpfe0003.put("INSERT_EMP"	, loginSessionInfoFactory.getObject().getUserId());
		idpfe0003.put("UPDATE_EMP"	, loginSessionInfoFactory.getObject().getUserId());
		idpfe0003.putCurrentDate("INSERT_DAT");
		idpfe0003.putCurrentDate("UPDATE_DAT");		
		
		
		String[] arrayClob = null;
		JSONObject checkSubstraId= null;
		checkSubstraId = (JSONObject) checkSubstraId100(submsId, substraId).get(0);
		System.out.println("체크::::"+checkSubstraId);
	/*	if(checkSubstraId.get("CNT").equals("0")){   
			
			idpfe0003.put("MS_ID" 		, "100",true);  
			idpfe0003.put("STRA_ID" 		, "100",true); 
			idpfe0003.put("SUBSTRA_ID" 		, "100",true);  
			idpfe0003.put("SUBSTRA_CD" 		, "E-01"); 
			idpfe0003.put("SUBSTRA_NM" 		, "미래가치");  
			idpfe0003.put("USE_YN"		, "Y");
			idpfe0003.put("SORT_ORDER"		, "0");  
			idpfe0003.put("SUBSTRA_DEF"		, "비고");   
			dbmsDao.insertTable("IDPFE0003",idpfe0003);
		} 
		*/ 
		idpfe0003.put("MS_ID", submsId, true);
		idpfe0003.put("STRA_ID",  substraId, true);   
		idpfe0003.put("SUBSTRA_ID",  substId, true);
		idpfe0003.put("SUBSTRA_CD" , substCd);
		idpfe0003.put("F_SUBSTRA_NM", substNm1);  
		idpfe0003.put("S_SUBSTRA_NM", substNm2);  
		idpfe0003.put("USE_YN"	, substUse); 
		idpfe0003.put("SORT_ORDER"		, substSort);
		idpfe0003.put("SUBSTRA_DEF"	, substDef); 
		 
		try{	
			if(substId.equals("")){ //insert
				if(checkSubstraId.get("CNT").equals("0")){ 
					idpfe0003.put("SUBSTRA_ID" 		, "100" , true); 
				} else { 
					JSONObject maxid = (JSONObject)maxSubstraid(submsId, substraId).get(0);   
					idpfe0003.put("SUBSTRA_ID" 		, maxid.get("MAX_SUBSTRA_ID") , true);
					System.out.println("maxid:::"+maxid.get("MAX_SUBSTRA_ID")); 
				} 
//				idpfe0002.put("MS_ID"		, "100" , true);    
				dbmsDao.insertTable("IDPFE0003",idpfe0003);    
//				dbmsDao.insertTable("IDPFE0003","MS_ID",idpfe0002.getKeyMap(),idpfe0002);   
			}else{ //update 
				idpfe0003.put("SUBSTRA_ID" 		, substId, true); 
				dbmsDao.updateTable("IDPFE0003",idpfe0003.getKeyMap(),idpfe0003); 
			}	
		}catch (Exception e) {			
			e.printStackTrace();
			throw e;
		}
	}
	
	//remove
			public void removeStrategyTarget(String submsId, String substraId, String substId) throws Exception{ 
			System.out.println("체크::::"+ submsId + " " + substraId + " " + substId);  
			//Validation 시작
			TableObject tableUsedStraID = new TableObject();
//			tableUsedStraID.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID(),true);
			tableUsedStraID.put("MS_ID", submsId,true); 
			tableUsedStraID.put("STRA_ID", substraId,true);   
			tableUsedStraID.put("SUBSTRA_ID", substId,true);  
			if(dbmsDao.countTable("IDPFE0100", tableUsedStraID)>0) throw new RuntimeException("해당 코드에 매핑된 지표가 있어 삭제 할 수 없습니다."); 
	/*		if(dbmsDao.countTable("IMFE0005", tableUsedStraID)>0) throw new RuntimeException("해당 코드에 매핑된 세부전략이 있어 삭제 할 수 없습니다.");
			if(dbmsDao.countTable("IMFE0006", tableUsedStraID)>0) throw new RuntimeException("해당 코드에 매핑된 CSF가 있어 삭제 할 수 없습니다.");
	*/		//Validation 끝
			
			HashMap<String, Object> map = new HashMap<String, Object>();
			
			map.put("@(MS_ID)"					, submsId);
			map.put("@(STRA_ID)"				, substraId);
			map.put("@(SUBSTRA_ID)"				, substId); 
			String[] arrayClob = null;
			try{ 
				sqlHouseExcuteDao.crudQuery(326, map, arrayClob); 
				
			}catch(Exception e){ 
				 
				e.printStackTrace();
				throw e;
			}		
		}
		public JSONArray checkStrategtTarget(String msId,String straId,String substId) throws Exception{ 
			HashMap<String, Object> map = new HashMap<String, Object>(); 
//			map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
			map.put("@(MS_ID)"					, msId);
			map.put("@(STRA_ID)"				, straId);
			map.put("@(SUBSTRA_ID)"				, substId);
			 
			return sqlHouseExcuteDao.selectJSONArray(327, map);
		}
		
	
}