package com.iplanbiz.imanager.service;

import java.util.HashMap;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class KpiAttachFileStateTabService {
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired   
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
}
