package com.iplanbiz.imanager.service;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.management.RuntimeErrorException;
 
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.commons.CommonsMultipartFile;


import com.iplanbiz.core.io.file.FileService;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.config.WebConfig;
import com.iplanbiz.imanager.dao.BasicDAO; 
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class EvalGroupService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	public JSONArray selectEvalGroupManager() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		
		return sqlHouseExcuteDao.selectJSONArray(45, map);
	}
	public JSONArray selectgridEvalGroupManager(String searchscnm, String scid, String yyyy) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(SC_NM)"					, searchscnm);   
		map.put("@(SC_ID)"					, scid);
		map.put("@(YYYY)"					, yyyy); 
		return sqlHouseExcuteDao.selectJSONArray(46, map);
	}
	public JSONArray selectscidEvalGroupManager(String scid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(SC_ID)"					, scid);
		return sqlHouseExcuteDao.selectJSONArray(47, map);
	}
	
	@Transactional
	public int insertEvalGroupManager(String pscid, String scid, String scnm, String evalg, String scdesc, String etcdesc, String evagbn
			                          ,String scudc1, String startdate, String enddate, String deptids, String sustids, String sortorder) throws Exception{
		int result = 0;
 
		logger.debug("scid = " + scid);
		String[] arrayClob = null;
  
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(PSC_ID)"					, pscid);    
		map.put("@(S_SC_NM)"					, scnm);
		map.put("@(EVAL_G)"					, evalg);
		map.put("@(S_SC_DESC)"				, scdesc);
		map.put("@(S_ETC_DESC)"				, etcdesc);
		map.put("@(EVA_GBN)"				, evagbn);
		map.put("@(SC_UDC1)"				, scudc1);
		map.put("@(START_DAT)"				, startdate);
		map.put("@(END_DAT)"				, enddate);
		map.put("@(PPTFILE)"				, "");
 
		map.put("@(SORT_ORDER)"				, sortorder);
		map.put("@(INSERT_EMP)"				, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(UPDATE_EMP)"				, loginSessionInfoFactory.getObject().getUserId());
 
		
		JSONObject maxScid = null;
		maxScid = (JSONObject) getMaxScid().get(0);
		try{
			if(scid.equals("")){
				map.put("@(SC_ID)"				, maxScid.get("SC_ID"));
				sqlHouseExcuteDao.crudQuery(57,map,arrayClob);
				result = 1;
			}else{
				map.put("@(SC_ID)"				, scid);
				sqlHouseExcuteDao.crudQuery(58, map, arrayClob);
				result = 2;
			}
			String[] deptCd = deptids.split(",");
			if(!scid.equals(""))sqlHouseExcuteDao.crudQuery(61,map);
			for(int i=0;i<deptCd.length;i++){
				if(!deptCd[i].equals("")){
					map.put("@(DEPT_CD)"			, deptCd[i]);
					sqlHouseExcuteDao.crudQuery(60, map, arrayClob);
				}
			}
			String[] sustCd = sustids.split(",");
			if(!scid.equals(""))sqlHouseExcuteDao.crudQuery(63,map,arrayClob);
			for(int j=0;j<sustCd.length;j++){
				if(!sustCd[j].equals("")){
					map.put("@(SUST_ID)"			, sustCd[j]);
					sqlHouseExcuteDao.crudQuery(62, map, arrayClob);
				}
			}
		}catch(Exception e){			
			result = 0;
			logger.error("error : ",e);
			logger.debug("error : {}",e.getMessage()); 
			throw e;
		}
		return result;
	}
	public int removeEvalGroupManager(String scid) throws Exception{
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(SC_ID)"					, scid);
		
		String[] arrayClob = null;
		try{
			sqlHouseExcuteDao.crudQuery(59, map, arrayClob);
			//기간계 맵핑 삭제
			sqlHouseExcuteDao.crudQuery(61, map, arrayClob);
			//정보공시 학과 맵핑 삭제
			sqlHouseExcuteDao.crudQuery(63, map, arrayClob);
			result = 1;
		}catch(Exception e){
			logger.error("error : {}",e);
			result = 0;
			throw e;
		}
		return result;
	}
	public JSONArray getMaxScid() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"			, loginSessionInfoFactory.getObject().getAcctID());
		return sqlHouseExcuteDao.selectJSONArray(56,map);
	}
	public JSONArray deptlistEvalGroupManager(String scid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(SC_ID)"					, scid);
		return sqlHouseExcuteDao.selectJSONArray(48, map);
	}
	public JSONArray scidlistEvalGroupManager(String scid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(SC_ID)"					, scid);
		return sqlHouseExcuteDao.selectJSONArray(49, map);
	}
	public JSONArray selectEvalGroupManagerPopupScid() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		
		return sqlHouseExcuteDao.selectJSONArray(50, map);
	}
	public JSONArray selectEvalGroupManagerPopscidGrid(String searchnm) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(SC_NM)"					, searchnm);
		
		return sqlHouseExcuteDao.selectJSONArray(51, map);
	}
	public JSONArray selectEvalGoupManagerPopupDeptGrid(String scid, String scnm, String minscid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(SC_ID)"					, scid);
		map.put("@(SC_NM)"					, scnm);
		map.put("@(MINSCID)"				, minscid);
		
		return sqlHouseExcuteDao.selectJSONArray(52, map);
	}
	public JSONArray selectEvalGoupManagerPopupDeptGrid1(String scid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(SC_ID)"					, scid);
		
		return sqlHouseExcuteDao.selectJSONArray(53, map);
	}
	public JSONArray selectEvalGroupManagerPopupSustGrid(String scid, String searchscnm) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(SC_ID)"					, scid);
		map.put("@(SUST_NM)"					, searchscnm);
		
		return sqlHouseExcuteDao.selectJSONArray(54, map);
	}
	public JSONArray selectEvalGroupManagerPopupSustGrid1(String scid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(SC_ID)"					, scid);
		
		return sqlHouseExcuteDao.selectJSONArray(55, map);
	}
	public JSONArray getFileid() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		return sqlHouseExcuteDao.selectJSONArray(70, map);
	}
	public int saveFile(CommonsMultipartFile filenm, String scid, String pptefile) throws Exception{
		int result = 0;
		
		return result;
	}
	public JSONArray getFileInfo(String fileid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(FILE_ID)"				, fileid);
		return sqlHouseExcuteDao.selectJSONArray(73, map);
	}
	
}
