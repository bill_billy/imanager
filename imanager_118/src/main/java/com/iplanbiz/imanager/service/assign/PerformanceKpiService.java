package com.iplanbiz.imanager.service.assign;


import java.util.HashMap;


import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSDAO;

import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.KpiDAO;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;
import com.iplanbiz.imanager.dto.AssignMent;
import com.iplanbiz.imanager.dto.AssignMentKpi;




@Service
@Transactional(readOnly=true)
public class PerformanceKpiService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory; 
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExecuteDao;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	@Autowired
	KpiDAO kpiDao;
	
	
	
	
	public int insertKpiUser(String msid, String yyyy, String[] kpiid, String[] subkpiid, Integer[] kpival,
			String[] confirmyn, String[] confconfirmyn) throws Exception {
			int result = 0;
		for(int i=0;i<kpiid.length;i++) {
			JSONArray jsonHasUser =  checkUser1(msid,yyyy,kpiid[i],subkpiid[i]);
			
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("@(MS_ID)"				, msid);
			map.put("@(YYYY)"				, yyyy);
			map.put("@(KPI_ID)"				, kpiid[i]);
			map.put("@(SUB_KPI_ID)"			, subkpiid[i]);
			map.put("@(CONFIRM_YN)"				, confirmyn[i]);
			map.put("@(CONF_CONFIRM_YN)"			, confconfirmyn[i]);
			
			if(jsonHasUser.size()>0) {
				map.put("@(UPDATE_EMP)"				, loginSessionInfoFactory.getObject().getUserId());
			}else {
				map.put("@(INSERT_EMP)"				, loginSessionInfoFactory.getObject().getUserId());
			}
			String[] arrayClob = null;
			try{
				if(jsonHasUser.size()>0) {
					sqlHouseExecuteDao.crudQuery(509, map, arrayClob);
					if(kpival[i] != null) 
					map.put("@(KPI_VAL)"				, kpival[i]);
					else
						map.put("@(KPI_VAL)"				, "");
					JSONArray jsonHasUser1 =  checkUser2(msid,yyyy,kpiid[i],subkpiid[i]);
					if(jsonHasUser1.size()>0)
					sqlHouseExecuteDao.crudQuery(510, map, arrayClob);
					else {
						map.put("@(INSERT_EMP)"				, loginSessionInfoFactory.getObject().getUserId());
						sqlHouseExecuteDao.crudQuery(512, map, arrayClob);
					
					//}
					}
				}
				result = 1;
			}catch(Exception e){
				logger.error("error:",e);			
				result = 0;
				throw e;
			}
			
		}
		return result; 
	}
	public JSONArray checkUser1(String ms_id,String yyyy,String kpiid,String subkpiid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(YYYY)"					, yyyy);
		map.put("@(KPI_ID)"					, kpiid);
		map.put("@(SUB_KPI_ID)"				, subkpiid);
		return sqlHouseExecuteDao.selectJSONArray(490, map);
	}
	public JSONArray checkUser2(String ms_id,String yyyy,String kpiid,String subkpiid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(YYYY)"					, yyyy);
		map.put("@(KPI_ID)"					, kpiid);
		map.put("@(SUB_KPI_ID)"				, subkpiid);
		return sqlHouseExecuteDao.selectJSONArray(511, map);
	}
	
	
	

}
