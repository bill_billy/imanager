package com.iplanbiz.imanager.service;

import java.util.HashMap;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO; 
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class CodeService2 {
	
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;

	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlhouseExcuteDao;
	
	@Transactional
	public int insertCode(String sComlCod, String orgComCod, String sComCod, String sComNm, String sUseYn, String sSortOrder, String sBigo) throws Exception{
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(COML_COD)"					, sComlCod.replaceAll("'", "''"));
		map.put("@(ORG_COD)"				    , orgComCod.replaceAll("'", "''"));
		map.put("@(COM_COD)"					, sComCod.replaceAll("'", "''"));
		map.put("@(COM_NM)"						, sComNm.replaceAll("'", "''"));
		map.put("@(USE_YN)"						, sUseYn);
		map.put("@(SORT_ORDER)"					, sSortOrder);
		map.put("@(BIGO)"						, sBigo.replaceAll("'", "''"));
		map.put("@(INSERT_EMP)"					, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(UPDATE_EMP)"					, loginSessionInfoFactory.getObject().getUserId());
		
		String[] clobArray = null;
		try{
			if(orgComCod.equals("")){
				sqlhouseExcuteDao.crudQuery(445, map, clobArray);
				result = 1;
			}else{
				sqlhouseExcuteDao.crudQuery(446, map, clobArray);
				result = 2;
			}
		}catch(Exception e){
			result = 3;			
			e.printStackTrace();
			throw e;
		}
		return result;
	}
	public int removeCode(String sComlCod, String sComCod) throws Exception{
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(COML_COD)"				, sComlCod);
		map.put("@(COM_COD)"				, sComCod);
		
		String[] clobArray = null;
		try{
			sqlhouseExcuteDao.crudQuery(447, map, clobArray);
		}catch(Exception e){
			result = 2;
			e.printStackTrace();
			throw e;
		}
		return result;
	}
	
	public JSONArray getCodeListASC(String comlcod) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(COML_COD)"				, comlcod);
		
		return sqlhouseExcuteDao.selectJSONArray(448, map);
	}
	
	public JSONArray getCodeListDESC(String comlcod) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_COML_COD"				, comlcod);
		
		return sqlhouseExcuteDao.selectJSONArray(449, map);
	}
}
