package com.iplanbiz.imanager.service;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.imanager.dao.SqlHouseDAO;

@Service
@Transactional(readOnly=true)
public class SqlHouseService {

	@Autowired
	SqlHouseDAO sqlHouseDAO;
	
	public List<HashMap<String, Object>> getSqlHouseList() throws Exception{ 
		return sqlHouseDAO.getSqlHouseList();  
	}
	public List<HashMap<String, Object>> getSqlHouseList(String searchText) throws Exception{ 
		return sqlHouseDAO.getSqlHouseList(searchText);  
	}
	public int insertSqlHouse(String vendor, String description,
			String sql, String sql_excel) throws Exception {
		return sqlHouseDAO.insertSqlHouse(vendor,description,sql,sql_excel); 
		
	}
	public int updateSqlHouse(String idx, String vendor, String description,
			String sql, String sql_excel) throws Exception {
		// TODO Auto-generated method stub
		return sqlHouseDAO.updateSqlHouse(idx,vendor,description,sql,sql_excel); 
	} 
}
