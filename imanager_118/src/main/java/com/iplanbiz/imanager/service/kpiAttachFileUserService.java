package com.iplanbiz.imanager.service;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.core.io.dbms.DBMSDAO; 
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.io.file.FileService;
import com.iplanbiz.core.session.LoginSessionInfo; 
import com.iplanbiz.imanager.config.WebConfig;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;
import com.iplanbiz.imanager.dto.KpiAttachFileUser;


@Service
@Transactional(readOnly=true)
public class kpiAttachFileUserService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory; 
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExecuteDao;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	public LinkedHashMap<String, Object> getEvaGbnInfoUser(String evagbn) throws Exception{
		HashMap<String,Object> param = new HashMap<String,Object>();
		if(evagbn == null) evagbn = "";
		param.put("@(S_ACCT_ID)"		, loginSessionInfoFactory.getObject().getAcctID());
		param.put("@(S_EVA_GBN)"		, evagbn);
		List<LinkedHashMap<String, Object>> result = sqlHouseExecuteDao.selectHashMap(226, param);
		return result.get(0);
	}
	public JSONArray getYearForEvalUser(String evagbn) throws Exception{
		HashMap<String,Object> param = new HashMap<String,Object>();
		param.put("@(S_ACCT_ID)"		, loginSessionInfoFactory.getObject().getAcctID());
		param.put("@(S_EVA_GBN)"		, evagbn);
		JSONArray result = sqlHouseExecuteDao.selectJSONArray(227, param);
		return result;
	}
	public JSONArray getMonthForEvalUser(String evagbn, String year) throws Exception{
		HashMap<String,Object> param = new HashMap<String,Object>();
		param.put("@(S_ACCT_ID)"		, loginSessionInfoFactory.getObject().getAcctID());
		param.put("@(S_EVA_GBN)"		, evagbn);
		param.put("@(S_YYYY)"			, year);
		JSONArray result = sqlHouseExecuteDao.selectJSONArray(228, param);
		return result;
	}
	public List<LinkedHashMap<String, Object>> getAttachFileInfoUser(String fileId) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)", loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_FILE_ID)", fileId);
		List<LinkedHashMap<String, Object>> fileList = this.sqlHouseExecuteDao.selectHashMap(231, map);
		return fileList;
	}
	/**
	 * 지표별 증빙자료 파일 삭제.
	 * @param year
	 * @param month
	 * @param scid
	 * @param kpiid
	 * @param fileid
	 * @throws Exception
	 */
	public void removeKpiAttachFileUser(String year, String month, String scid,
			String kpiid, String fileid, String filepath, String fileSavedName, String fileExtName) throws Exception{

		TableObject imfe9001 = new TableObject();
		imfe9001.put("ACCT_ID"	, loginSessionInfoFactory.getObject().getAcctID()		, true);
		imfe9001.put("FILE_ID"	, fileid												, true);
		
		dbmsDao.deleteTable("IMFE9001", imfe9001);
		//실제 파일 삭제
		java.io.File realFile = new java.io.File(filepath + fileSavedName+"."+fileExtName);
		realFile.delete();
		//FileService file = new FileService(WebConfig.getProperty("system.upload.SaveDir"),Long.valueOf(WebConfig.getProperty("system.maxUploadSize").toString()));		
	}
	public void uploadKpiAttachFileUser(KpiAttachFileUser KpiAttachFileUser) throws Exception{

		//실제 파일 업로드
		String savedFileName = "";
		 
		FileService fileService = new FileService(WebConfig.getProperty("system.upload.SaveDir"),Long.valueOf(WebConfig.getProperty("system.upload.maxUploadSize").toString()));
		String[] enableExtetionList = new String[]{"xls","xlsx","zip","pdf","hwp","doc","docx","png","jpg","txt","gif","ppt","pptx"};
		if(KpiAttachFileUser.getAttachFile()!=null){
			ArrayList<LinkedHashMap<String,Object>> arrImfe9001 = new ArrayList<LinkedHashMap<String, Object>>();
			
			//Progress Init
			loginSessionInfoFactory.getObject().initProgress("FILEUPLOAD", KpiAttachFileUser.getAttachFile().length);

			try{
				int i = 0;
				for(CommonsMultipartFile file : KpiAttachFileUser.getAttachFile()){
					//String originalFileName = new String(file.getOriginalFilename().getBytes("8859_1"),"utf-8");
					
					//Progress 상태가 Valid하지 않다면(오류가 중간에 난적이 있다면) 작업 중단.
					if(loginSessionInfoFactory.getObject().isValidProgress("FILEUPLOAD")==false){
						throw new RuntimeException("작업이 취소 되었습니다.");
					}
					String originalFileName = file.getOriginalFilename();
					savedFileName = fileService.setFileName();
					
					fileService.saveFile(file, enableExtetionList, savedFileName);		 
					
					TableObject imfe9001 = new TableObject();
					imfe9001.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID(),true);
					imfe9001.put("YYYY", KpiAttachFileUser.getYear());
					imfe9001.put("MM", KpiAttachFileUser.getMonth());
					imfe9001.put("SC_ID", KpiAttachFileUser.getScid());
					imfe9001.put("KPI_ID", KpiAttachFileUser.getKpiid());			
					imfe9001.put("FILE_SAVE_NAME", savedFileName);
					imfe9001.put("FILE_ORG_NAME", originalFileName);
					imfe9001.put("FILE_PATH", fileService.getUpLoadPath()); 
					imfe9001.put("FILE_SIZE", file.getSize());
					imfe9001.put("FILE_EXT", fileService.getFileExt());
					arrImfe9001.add(imfe9001);
					
					//Progress 상태에 진행 현황 UPDATE
					loginSessionInfoFactory.getObject().pushJob("FILEUPLOAD",String.valueOf(++i) , originalFileName);
				}	
				TableObject imfe9001_key = new TableObject();
				imfe9001_key.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID(),true);
				dbmsDao.insertTable("IMFE9001", "FILE_ID",imfe9001_key,  arrImfe9001);
				
				//Progress 완료
				loginSessionInfoFactory.getObject().completeProgress("FILEUPLOAD");
			}catch(Exception e){
				loginSessionInfoFactory.getObject().pushJob("FILEUPLOAD", e.getClass().getCanonicalName(), e.getMessage());   
				throw e;
			}
			
		}
		else{
			logger.info("업로드 할 증빙 자료가 없습니다. ");
		} 
	}

}
