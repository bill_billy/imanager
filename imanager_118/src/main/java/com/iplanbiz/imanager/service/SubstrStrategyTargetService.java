package com.iplanbiz.imanager.service;

import java.util.HashMap;

import javax.annotation.Resource;
 
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO; 
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class SubstrStrategyTargetService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	
	
	public JSONArray selectSubstrStrategyTarget() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>(); 
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		
		return sqlHouseExcuteDao.selectJSONArray(37, map);
	}
	
	@Transactional
	public void insertSubstrStrategyTarget(String straid,String substraid,String substracd,String substranm,String sortorder,
			String useyn,String startyyyy,String endyyyy) throws Exception{
 
		TableObject imfe0005 = new TableObject();
		
		imfe0005.put("ACCT_ID"				, loginSessionInfoFactory.getObject().getAcctID(),true);
		imfe0005.put("STRA_ID"				, straid,true);		
		imfe0005.put("SUBSTRA_ID"			, 100,true);
		
		if(dbmsDao.countTable("IMFE0005",imfe0005.getKeyMap()) == 0){
			imfe0005.put("SUBSTRA_ID"				, 100);
			imfe0005.put("SUBSTRA_CD"				, "");
			imfe0005.put("SUBSTRA_NM"				, "세부전략목표없음");
			imfe0005.put("SORT_ORDER"				, 99999);
			imfe0005.put("START_YYYY"				, "1000");
			imfe0005.put("END_YYYY"				, "9999");
			imfe0005.put("USE_YN"					, "Y");
			imfe0005.put("INSERT_EMP"				, loginSessionInfoFactory.getObject().getUserId());
			imfe0005.put("UPDATE_EMP"				, loginSessionInfoFactory.getObject().getUserId());
			dbmsDao.insertTable("IMFE0005", imfe0005);			 
		}
		imfe0005.put("SUBSTRA_ID"				, substraid, true);
		imfe0005.put("SUBSTRA_CD"				, substracd);
		imfe0005.put("SUBSTRA_NM"				, substranm); 
		imfe0005.put("SORT_ORDER"				, sortorder);
		imfe0005.put("START_YYYY"				, startyyyy);
		imfe0005.put("END_YYYY"					, endyyyy);
		imfe0005.put("USE_YN"					, useyn);
		imfe0005.put("UPDATE_EMP"				, loginSessionInfoFactory.getObject().getUserId());
		imfe0005.putCurrentDate("UPDATE_DAT");	 
		
		try{
			if(substraid == ""){  
			
				imfe0005.put("INSERT_EMP"				, loginSessionInfoFactory.getObject().getUserId());				
				imfe0005.putCurrentDate("INSERT_DAT");
				dbmsDao.insertTable("IMFE0005","SUBSTRA_ID", imfe0005.getKeyMap(),imfe0005);
				
			}
			else{
				
				dbmsDao.updateTable("IMFE0005", imfe0005.getKeyMap(),imfe0005);
				
			}
		}catch(Exception e){			
			e.printStackTrace();
			throw e;
		} 
	}
	public int removeSubstrStrategyTarget(String straid, String substraid) throws Exception{
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(STRA_ID)"				, straid);
		map.put("@(SUBSTRA_ID)"				, substraid);
		String[] arrayClob = null;
		try{
			sqlHouseExcuteDao.crudQuery(43, map, arrayClob);
			result = 1;
		}catch(Exception e){
			result = 0;
			e.printStackTrace();
			throw e;
		}
		return result; 
	}
	public JSONArray checkSubstrStrategyTarget(String straid, String substraid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(STRA_ID)"				, straid);
		map.put("@(SUBSTRA_ID)"				, substraid);
		
		return sqlHouseExcuteDao.selectJSONArray(68,map);
	}
}
