package com.iplanbiz.imanager.service.program;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;
import com.iplanbiz.imanager.dto.DeptEval;
import com.iplanbiz.imanager.dto.Program;
import com.iplanbiz.imanager.dto.ProgramFile;

@Service
@Transactional(readOnly=true)
public class ProgramManagerService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExecuteDao;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	@Autowired
	ProgramFileService fileService;

	public JSONArray listCode(String comlcod) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(COML_COD)"				, comlcod);
		
		return sqlHouseExecuteDao.selectJSONArray(399, map);
	}
	
	private JSONArray getMaxProgramid(String ms_id) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		return sqlHouseExecuteDao.selectJSONArray(413, map);
	}
	private JSONArray getMaxBudgetid(String ms_id,String program_id) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(PROGRAM_ID)"					, program_id);
		return sqlHouseExecuteDao.selectJSONArray(418, map);
	}
	private JSONArray getMaxEnforceid(String ms_id,String program_id,String budget_id) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(PROGRAM_ID)"				, program_id);
		map.put("@(BUDGET_ID)"				, budget_id);
		return sqlHouseExecuteDao.selectJSONArray(420, map);
	}
	
	private JSONArray getDeptEvalYYYY(String ms_id,String program_id,String yyyy) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(PROGRAM_ID)"					, program_id);
		map.put("@(YYYY)"					, yyyy);
		return sqlHouseExecuteDao.selectJSONArray(426, map);
	}
	
	public int insert(Program program) throws Exception {
		int result = 0;
		
		TableObject idpfe0200 = new TableObject(); 
		
		JSONObject jsonObjectprogramId = (JSONObject) getMaxProgramid(program.getMs_id()).get(0);
		
		
		
		if(program.getProgram_id()!=null&&!program.getProgram_id().equals("")){ //수정
			idpfe0200.put("PROGRAM_ID"					, program.getProgram_id(),true);
			logger.debug("프로그램 수정 : {}"			, program.getProgram_id());
		}else{
			idpfe0200.put("PROGRAM_ID"					, jsonObjectprogramId.get("PROGRAM_ID"), true);
			logger.debug("프로그램 신규 : {}"			, jsonObjectprogramId.get("PROGRAM_ID"));
		}
		
		
		idpfe0200.put("MS_ID"				, program.getMs_id(),true);
		idpfe0200.put("YYYY"				, program.getYyyy(),true);
		idpfe0200.put("PROGRAM_NM"			, program.getProgram_nm());
		idpfe0200.put("ASSIGN_ID"			, program.getAssign_id());
		idpfe0200.put("KPI_ID"				, program.getKpi_id());
		idpfe0200.put("DEPT_CD"				, program.getDept_cd());
		idpfe0200.put("DEPT_NM"				, program.getDept_nm());
		idpfe0200.put("EMP_NO"				, program.getEmp_no());  
		idpfe0200.put("EMP_NM"				, program.getEmp_nm());  
		idpfe0200.put("CONFIRM_DEPT_CD"		, program.getConfirm_dept_cd());
		idpfe0200.put("CONFIRM_DEPT_NM"		, program.getConfirm_dept_nm());
		idpfe0200.put("CONFIRM_EMP_NO"		, program.getConfirm_emp_no());  
		idpfe0200.put("CONFIRM_EMP_NM"		, program.getConfirm_emp_nm());  
		idpfe0200.put("EVAL_DEPT_CD"		, program.getEval_dept_cd());
		idpfe0200.put("EVAL_DEPT_NM"		, program.getEval_dept_nm());
		idpfe0200.put("EVAL_EMP_NO"			, program.getEval_emp_no());  
		idpfe0200.put("EVAL_EMP_NM"			, program.getEval_emp_nm());
		idpfe0200.put("PROGRAM_STATUS"		, program.getProgram_status());  
		idpfe0200.put("SCH_START_DAT"		, program.getSch_start_dat().replace("-", ""));  
		idpfe0200.put("SCH_END_DAT"			, program.getSch_end_dat().replace("-", "")); 
		
		
		idpfe0200.putCurrentDate("INSERT_DAT");
		idpfe0200.put("INSERT_EMP"			, loginSessionInfoFactory.getObject().getUserId());
		idpfe0200.putCurrentDate("UPDATE_DAT");
		idpfe0200.put("UPDATE_EMP"			, loginSessionInfoFactory.getObject().getUserId()); 
		try{
			if(program.getProgram_id()==null||program.getProgram_id().equals("")){
				//IMFE0009 INSERT 
				dbmsDao.insertTable("IDPFE0200", idpfe0200);
				
				result = 1;
			}else{
				//IMFE0009 UPDATE 
				dbmsDao.updateTable("IDPFE0200",idpfe0200.getKeyMap(), idpfe0200);
				result = 2;
			}
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		
		return result;
	}

	public int remove(String ms_id, String program_id) throws Exception {
		int result = 0;
		
		
		
		TableObject deleteidpfe0200 = new TableObject(); //프로그램
		deleteidpfe0200.put("MS_ID"				, ms_id,true);
		deleteidpfe0200.put("PROGRAM_ID"		, program_id,true);
	
		try {
			dbmsDao.deleteTable("IDPFE0200", deleteidpfe0200);
			result = 1;
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		
		TableObject deleteidpfe0201 = new TableObject(); //프로그램파일
		deleteidpfe0201.put("MS_ID"				, ms_id,true);
		deleteidpfe0201.put("PROGRAM_ID"		, program_id,true);
	
		try {
			dbmsDao.deleteTable("IDPFE0201", deleteidpfe0201);
			result = 1;
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		
		TableObject deleteidpfe0202 = new TableObject(); //예산
		deleteidpfe0202.put("MS_ID"				, ms_id,true);
		deleteidpfe0202.put("PROGRAM_ID"		, program_id,true);
	
		try {
			dbmsDao.deleteTable("IDPFE0202", deleteidpfe0202);
			result = 1;
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		
		TableObject deleteidpfe0203 = new TableObject(); //집행
		deleteidpfe0203.put("MS_ID"				, ms_id,true);
		deleteidpfe0203.put("PROGRAM_ID"		, program_id,true);
	
		try {
			dbmsDao.deleteTable("IDPFE0203", deleteidpfe0203);
			result = 1;
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		
		TableObject deleteidpfe0204 = new TableObject(); //부서평가
		deleteidpfe0204.put("MS_ID"				, ms_id,true);
		deleteidpfe0204.put("PROGRAM_ID"		, program_id,true);
	
		try {
			dbmsDao.deleteTable("IDPFE0204", deleteidpfe0204);
			result = 1;
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		
	/*	
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(PROGRAM_ID)"				, program_id);
		
		String[] arrayClob = null;
		try{
			sqlHouseExecuteDao.crudQuery(416, map, arrayClob);
			result = 1;
		}catch(Exception e){
			logger.error("error:",e);			
			result = 0;
			throw e;
		}*/
		return result; 
	}
	
	public int insertPlan(Program program) throws Exception {
		int result = 0;
		
		TableObject idpfe0200 = new TableObject(); 
		
		idpfe0200.put("PROGRAM_ID"			, program.getProgram_id(),true);
		idpfe0200.put("MS_ID"				, program.getMs_id(),true);
		idpfe0200.put("CONTENT"				, program.getContent());
		idpfe0200.put("QUAN_GOAL"			, program.getQuan_goal());
		idpfe0200.put("QUAN_UNIT"			, program.getQuan_unit());
		idpfe0200.put("QUAN_CONTENT"		, program.getQuan_content());
		idpfe0200.put("QUAL_GOAL"			, program.getQual_goal());
		idpfe0200.put("SUMMARY"				, program.getSummary());  
		idpfe0200.put("EFFECT"				, program.getEffect());  
		
		idpfe0200.putCurrentDate("UPDATE_DAT");
		idpfe0200.put("UPDATE_EMP"			, loginSessionInfoFactory.getObject().getUserId()); 
		try{
			dbmsDao.updateTable("IDPFE0200",idpfe0200.getKeyMap(), idpfe0200);
			result = 1;
			
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		
		//파일저장
		if(program.getPlanFile()!=null && program.getPlanFile().length > 0){
			savePlanFile(program);
		}
		
		//예산저장
		saveBudget(program);
		
		return result;
	}
	public int savePlanFile(Program program)  {
		int saveresult = 0;
		int result = 0;
		//파일저장
		try {
				if(program.getPlanFile().length > 0){
					
					String extType[] = {};
					
					for(CommonsMultipartFile cmfile : program.getPlanFile())
					{
						if(cmfile.getSize() > 0){
							ProgramFile file = new ProgramFile();
							//파일저장
							file = fileService.saveFile(cmfile, extType);
							file.setMs_id(program.getMs_id());
							file.setProgram_id(program.getProgram_id());
							file.setFile_gubun("A");
							result = fileService.insertFile(file);
							
						}
					}
				}
		}catch(Exception e) {
			logger.error("error:{}",e);
			result = 0;
		}
		return result;
		
	}
	public int saveRunFile(Program program)  {
		int saveresult = 0;
		int result = 0;
		//파일저장
		try {
				if(program.getRunFile().length > 0){
					
					String extType[] = {};
					
					for(CommonsMultipartFile cmfile : program.getRunFile())
					{
						if(cmfile.getSize() > 0){
							ProgramFile file = new ProgramFile();
							//파일저장
							file = fileService.saveFile(cmfile, extType);
							file.setMs_id(program.getMs_id());
							file.setProgram_id(program.getProgram_id());
							file.setFile_gubun("B");
							result = fileService.insertFile(file);
							
						}
					}
				}
		}catch(Exception e) {
			logger.error("error:{}",e);
			result = 0;
		}
		return result;
		
	}
	public int saveBudget(Program program) throws Exception{
		int result = 0;
		if(program.getBudget_content()!=null) {
			
			List<String> budgetArr = new ArrayList<String>();
			JSONObject jobudget_id = (JSONObject) getMaxBudgetid(program.getMs_id(),program.getProgram_id()).get(0);
			int budget_id = Integer.parseInt(String.valueOf(jobudget_id.get("BUDGET_ID")));
			//예산전부삭제
			TableObject deleteidpfe0202 = new TableObject(); 
			deleteidpfe0202.put("PROGRAM_ID"		, program.getProgram_id(),true);
			deleteidpfe0202.put("MS_ID"				, program.getMs_id(),true);
			
			try {
				dbmsDao.deleteTable("IDPFE0202", deleteidpfe0202);
				result = 1;
			}catch(Exception e){
				logger.error("error:",e);
				result = 0;
				throw e;
			} 
			if(result==0) return result;
			
			
			for(int i=0;i<program.getBudget_content().length;i++) {
				
				if(program.getBudget_content()[i].equals(""))
					continue;
				
				TableObject idpfe0202 = new TableObject(); 
				
				idpfe0202.put("PROGRAM_ID"			, program.getProgram_id(),true);
				idpfe0202.put("MS_ID"				, program.getMs_id(),true);
			if(program.getBudget_id()[i].equals("")) {
				idpfe0202.put("BUDGET_ID"			, budget_id++);
			}
			else {
				idpfe0202.put("BUDGET_ID"			, program.getBudget_id()[i]);
				budgetArr.add(program.getBudget_id()[i]);
			}
				idpfe0202.put("BUDGET_CONTENT"		, program.getBudget_content()[i]);
				idpfe0202.put("BUDGET_TYP"			, program.getBudget_typ()[i]);
				idpfe0202.put("BUDGET_START_DAT"	, program.getBudget_start_dat()[i]);
				idpfe0202.put("BUDGET_END_DAT"		, program.getBudget_end_dat()[i]);
				idpfe0202.put("BUDGET_AMOUNT"		, program.getBudget_amount()[i].replaceAll(",", ""));
				idpfe0202.put("BUDGET_DEF"			, program.getBudget_def()[i]);
				
				idpfe0202.putCurrentDate("INSERT_DAT");
				idpfe0202.put("INSERT_EMP"			, loginSessionInfoFactory.getObject().getUserId());
				idpfe0202.putCurrentDate("UPDATE_DAT");
				idpfe0202.put("UPDATE_EMP"			, loginSessionInfoFactory.getObject().getUserId()); 
				
				try{
					dbmsDao.insertTable("IDPFE0202", idpfe0202);
					result = 1;
					
				}catch(Exception e){
					logger.error("error:",e);
					result = 0;
					throw e;
				} 
			}
			
			if(result == 1) {
				result = deleteEnforce(program.getMs_id(),program.getProgram_id(),budgetArr);
			}
			
		}
		return result;
		
	}
	public int deleteEnforce(String ms_id,String program_id,List<String> deleteBudget) throws Exception {
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(PROGRAM_ID)"				, program_id);
		
		StringBuilder notIN = new StringBuilder();
		if(!deleteBudget.isEmpty()) {
			notIN.append("AND BUDGET_ID NOT IN(");
			
			for(int i=0;i<deleteBudget.size();i++) {
				notIN.append(deleteBudget.get(i));
				if(i!=deleteBudget.size()-1)
					notIN.append(",");
			}
			notIN.append(")");
		}
		map.put("@(AND_BUDGET)"				, notIN.toString());
		String[] arrayClob = null;
		try{
			sqlHouseExecuteDao.crudQuery(416, map, arrayClob);
			result = 1;
		}catch(Exception e){
			logger.error("error:",e);			
			result = 0;
			throw e;
		}
		return result;
	}
	public int insertRun(Program program) throws Exception {
		int result = 0;
		
		TableObject idpfe0200 = new TableObject(); 
		
		idpfe0200.put("PROGRAM_ID"			, program.getProgram_id(),true);
		idpfe0200.put("MS_ID"				, program.getMs_id(),true);
		idpfe0200.put("START_DAT"			, program.getStart_dat());
		idpfe0200.put("END_DAT"				, program.getEnd_dat());
		idpfe0200.put("RESULT"				, program.getResult());
		idpfe0200.put("PROGRAM_STATUS"		, "D");
		
		
		idpfe0200.putCurrentDate("UPDATE_DAT");
		idpfe0200.put("UPDATE_EMP"			, loginSessionInfoFactory.getObject().getUserId()); 
		try{
			dbmsDao.updateTable("IDPFE0200",idpfe0200.getKeyMap(), idpfe0200);
			result = 1;
			
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		
		//파일저장
		if(program.getRunFile()!=null && program.getRunFile().length > 0){
			saveRunFile(program);
		}
		
		//집행저장
		saveEnforce(program);
		
		return result;
	}
	public int saveEnforce_old(Program program) throws Exception{
		int result = 0;//getMaxEnforceid
		if(program.getEnforce_content()!=null) {
			//예산전부삭제
			TableObject deleteidpfe0203 = new TableObject(); 
			deleteidpfe0203.put("PROGRAM_ID"		, program.getProgram_id(),true);
			deleteidpfe0203.put("MS_ID"				, program.getMs_id(),true);
			deleteidpfe0203.put("BUDGET_ID"			, program.getBudget_id()[0],true);
			try {
				dbmsDao.deleteTable("IDPFE0203", deleteidpfe0203);
				result = 1;
			}catch(Exception e){
				logger.error("error:",e);
				result = 0;
				throw e;
			} 
			if(result==0) return result;
			
			JSONObject enforce_id = (JSONObject) getMaxEnforceid(program.getMs_id(),program.getProgram_id(),program.getBudget_id()[0]).get(0);
			for(int i=0;i<program.getEnforce_content().length;i++) {
				
				if(program.getEnforce_content()[i].equals(""))
					continue;
				
				TableObject idpfe0203 = new TableObject(); 
				
				idpfe0203.put("PROGRAM_ID"			, program.getProgram_id(),true);
				idpfe0203.put("MS_ID"				, program.getMs_id(),true);
				idpfe0203.put("BUDGET_ID"			, program.getBudget_id()[0]);
				idpfe0203.put("ENFORCE_ID"			, Integer.parseInt(String.valueOf(enforce_id.get("ENFORCE_ID")))+i);
				idpfe0203.put("ENFORCE_CONTENT"		, program.getEnforce_content()[i]);
				
				idpfe0203.put("ENFORCE_START_DAT"	, program.getEnforce_start_dat()[i]);
				idpfe0203.put("ENFORCE_END_DAT"		, program.getEnforce_end_dat()[i]);
				idpfe0203.put("ENFORCE_AMOUNT"		, program.getEnforce_amount()[i].replaceAll(",", ""));
				idpfe0203.put("ENFORCE_DEF"			, program.getEnforce_def()[i]);
				
				idpfe0203.putCurrentDate("INSERT_DAT");
				idpfe0203.put("INSERT_EMP"			, loginSessionInfoFactory.getObject().getUserId());
				idpfe0203.putCurrentDate("UPDATE_DAT");
				idpfe0203.put("UPDATE_EMP"			, loginSessionInfoFactory.getObject().getUserId()); 
				
				try{
					dbmsDao.insertTable("IDPFE0203", idpfe0203);
					result = 1;
					
				}catch(Exception e){
					logger.error("error:",e);
					result = 0;
					throw e;
				} 
			}
		}
		return result;
		
	}

	public int saveEnforce(Program program) throws Exception{
		int result = 0;//getMaxEnforceid
		if(program.getBudget_id()!=null) {
			
			for(int i=0;i<program.getBudget_id().length;i++) {
				
				TableObject idpfe0202 = new TableObject(); 
				
				idpfe0202.put("PROGRAM_ID"			, program.getProgram_id(),true);
				idpfe0202.put("MS_ID"				, program.getMs_id(),true);
				idpfe0202.put("BUDGET_ID"			, program.getBudget_id()[i],true);
				
				idpfe0202.put("ENFORCE_03"			, program.getEnforce03()[i]);
				idpfe0202.put("ENFORCE_04"			, program.getEnforce04()[i]);
				idpfe0202.put("ENFORCE_05"			, program.getEnforce05()[i]);
				idpfe0202.put("ENFORCE_06"			, program.getEnforce06()[i]);
				idpfe0202.put("ENFORCE_07"			, program.getEnforce07()[i]);
				idpfe0202.put("ENFORCE_08"			, program.getEnforce08()[i]);
				idpfe0202.put("ENFORCE_09"			, program.getEnforce09()[i]);
				idpfe0202.put("ENFORCE_10"			, program.getEnforce10()[i]);
				idpfe0202.put("ENFORCE_11"			, program.getEnforce11()[i]);
				idpfe0202.put("ENFORCE_12"			, program.getEnforce12()[i]);
				idpfe0202.put("ENFORCE_01"			, program.getEnforce01()[i]);
				idpfe0202.put("ENFORCE_02"			, program.getEnforce02()[i]);
				
				
				idpfe0202.putCurrentDate("UPDATE_DAT");
				idpfe0202.put("UPDATE_EMP"			, loginSessionInfoFactory.getObject().getUserId()); 
				
				try{
					dbmsDao.updateTable("IDPFE0202",idpfe0202.getKeyMap(), idpfe0202);
					result = 1;
					
				}catch(Exception e){
					logger.error("error:",e);
					result = 0;
					throw e;
				} 
			}
		}
		return result;
		
	}
	
	
	public int deletePlanFile(String ms_id, String program_id, String file_gubun, String file_id) throws Exception {
		int result = fileService.removeFile(ms_id, program_id, file_gubun, file_id);
		if(result==0)
			result=fileService.deleteFile(ms_id, program_id, file_id);
		return result;
	}

	
	public int insertDeptEval(DeptEval depteval) throws Exception {
		int result = 0;
		
		JSONObject joExist = (JSONObject)getDeptEvalYYYY(depteval.getMs_id(),depteval.getProgram_id(),depteval.getYyyy()).get(0);
		int isExist = Integer.parseInt(joExist.get("CNT").toString());
		
		TableObject idpfe0204 = new TableObject(); 
		
		idpfe0204.put("MS_ID"				, depteval.getMs_id(),true);
		idpfe0204.put("PROGRAM_ID"			, depteval.getProgram_id(),true);
		idpfe0204.put("YYYY"				, depteval.getYyyy(),true);
		idpfe0204.put("QUAN_ACH_VAL"		, depteval.getQuan_ach_val());
		idpfe0204.put("QUAN_DEF"			, depteval.getQuan_def());
		idpfe0204.put("QUAN_CONTENT"		, depteval.getQuan_content());
		idpfe0204.put("QUAL_ACH_YN"			, depteval.getQual_ach_yn());
		idpfe0204.put("QUAL_CONTENT"		, depteval.getQual_content());
		idpfe0204.put("CNT_TOT"				, depteval.getCnt_tot());
		idpfe0204.put("CNT_A"				, depteval.getCnt_a());
		idpfe0204.put("CNT_B"				, depteval.getCnt_b());
		idpfe0204.put("CNT_C"				, depteval.getCnt_c());
		idpfe0204.put("CNT_D"				, depteval.getCnt_d());
		idpfe0204.put("CNT_E"				, depteval.getCnt_e());
		idpfe0204.put("EVAL_SCORE"			, depteval.getEval_score());
		idpfe0204.put("EVAL_RESULT"			, depteval.getEval_result());
		
		
		idpfe0204.putCurrentDate("INSERT_DAT");
		idpfe0204.put("INSERT_EMP"			, loginSessionInfoFactory.getObject().getUserId()); 
		idpfe0204.putCurrentDate("UPDATE_DAT");
		idpfe0204.put("UPDATE_EMP"			, loginSessionInfoFactory.getObject().getUserId()); 
		try{
			if(isExist==1) {
				dbmsDao.updateTable("IDPFE0204",idpfe0204.getKeyMap(), idpfe0204);
				result = 2;
			}else {
				idpfe0204.put("EVAL_CONFIRM"		, "N");
				dbmsDao.insertTable("IDPFE0204", idpfe0204);
				result = 1;
			}
			
			
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		return result;
	}

	public int confirmDept(String ms_id, String program_id, String yyyy, String status) throws Exception {
		int result = 0;
		TableObject idpfe0204 = new TableObject(); 
		
		idpfe0204.put("MS_ID"				, ms_id,true);
		idpfe0204.put("PROGRAM_ID"			, program_id,true);
		idpfe0204.put("YYYY"				, yyyy,true);
		
		TableObject idpfe0200 = new TableObject(); 
		
		idpfe0200.put("MS_ID"				, ms_id,true);
		idpfe0200.put("PROGRAM_ID"			, program_id,true);
		idpfe0200.put("YYYY"				, yyyy,true);
		
		
		if(status.equals("confirm")) {
			idpfe0204.put("EVAL_CONFIRM"			, "Y"); //평가완료
			
			idpfe0200.put("PROGRAM_STATUS"			, "F"); //평가완료
		}
		else if(status.equals("cancle")) {
			idpfe0204.put("EVAL_CONFIRM"			, "N"); //평가중
			
			idpfe0200.put("PROGRAM_STATUS"			, "E"); //운영완료
		}
		try{
			dbmsDao.updateTable("IDPFE0204", idpfe0204.getKeyMap(), idpfe0204);
			
			dbmsDao.updateTable("IDPFE0200", idpfe0200.getKeyMap(), idpfe0200);
			if(status.equals("confirm"))
				result = 1;
			
			else if(status.equals("cancle"))
				result = 2;
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		return result;
	}
	
	public int confirmPlan(String ms_id, String program_id, String yyyy, String status) throws Exception {
		int result = 0;
		TableObject idpfe0200 = new TableObject(); 
		
		idpfe0200.put("MS_ID"				, ms_id,true);
		idpfe0200.put("PROGRAM_ID"			, program_id,true);
		idpfe0200.put("YYYY"				, yyyy,true);
		if(status.equals("confirm"))
			idpfe0200.put("PROGRAM_STATUS"			, "B"); //계획완료
		else if(status.equals("cancle"))
			idpfe0200.put("PROGRAM_STATUS"			, "A"); //처음상태
		try{
			dbmsDao.updateTable("IDPFE0200", idpfe0200.getKeyMap(), idpfe0200);
			if(status.equals("confirm"))
				result = 1;
			
			else if(status.equals("cancle"))
				result = 2;
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		return result;
	}
	
	public int confirmRun(String ms_id, String program_id, String yyyy, String status) throws Exception {
		int result = 0;
		TableObject idpfe0200 = new TableObject(); 
		
		idpfe0200.put("MS_ID"				, ms_id,true);
		idpfe0200.put("PROGRAM_ID"			, program_id,true);
		idpfe0200.put("YYYY"				, yyyy,true);
		if(status.equals("confirm"))
			idpfe0200.put("PROGRAM_STATUS"			, "E"); //운영완료
		else if(status.equals("cancle"))
			idpfe0200.put("PROGRAM_STATUS"			, "D"); //운영중
		try{
			dbmsDao.updateTable("IDPFE0200", idpfe0200.getKeyMap(), idpfe0200);
			if(status.equals("confirm"))
				result = 1;
			
			else if(status.equals("cancle"))
				result = 2;
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		return result;
	}
	
	public int confirmManager(String ms_id, String program_id, String yyyy, String status) throws Exception {
		int result = 0;
		TableObject idpfe0200 = new TableObject(); 
		
		idpfe0200.put("MS_ID"				, ms_id,true);
		idpfe0200.put("PROGRAM_ID"			, program_id,true);
		idpfe0200.put("YYYY"				, yyyy,true);
		if(status.equals("confirm"))
			idpfe0200.put("PROGRAM_STATUS"			, "C"); //기획확정
		else if(status.equals("cancle"))
			idpfe0200.put("PROGRAM_STATUS"			, "B"); //계획완료
		try{
			dbmsDao.updateTable("IDPFE0200", idpfe0200.getKeyMap(), idpfe0200);
			if(status.equals("confirm"))
				result = 1;
			
			else if(status.equals("cancle"))
				result = 2;
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		return result;
	}
}
