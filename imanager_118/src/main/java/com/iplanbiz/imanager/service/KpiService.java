package com.iplanbiz.imanager.service;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSDAO; 
import com.iplanbiz.core.io.dbms.TableCondition;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.KpiDAO;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;
import com.iplanbiz.imanager.dto.Kpi;



@Service
@Transactional(readOnly=true)
public class KpiService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory; 
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExecuteDao;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	@Autowired
	KpiDAO kpiDao;
	
	
	public List<LinkedHashMap<String, Object>> getRelatedWork(String mtid) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(MT_ID)"				, mtid);
		return sqlHouseExecuteDao.selectHashMap(132, map);
	}
	public List<LinkedHashMap<String, Object>> getRelatedWorkAdmin(String mtid) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(MT_ID)"				, mtid);
		return sqlHouseExecuteDao.selectHashMap(213, map);
	}
	
	public JSONArray getDeptList(String searchyyyy) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(YYYY)"					, searchyyyy);
		
		return sqlHouseExecuteDao.selectJSONArray(74, map);
	}
	public JSONArray getKpiList(String searchyyyy, String searchnm) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(MT_NM)"					, searchnm); 
		map.put("@(S_YYYY)"					, searchyyyy);
		map.put("@(S_KPI_NM)"					, searchnm);
		logger.debug("@@@@@@@ACCT_ID" + loginSessionInfoFactory.getObject().getAcctID());
		 
		return sqlHouseExecuteDao.selectJSONArray(75, map);
	}
	public JSONArray getKpiListAdmin(String searchyyyy, String searchnm) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_MT_NM)"					, searchnm); 
		map.put("@(S_YYYY)"					, searchyyyy);
		map.put("@(S_KPI_NM)"					, searchnm);
		logger.debug("@@@@@@@ACCT_ID" + loginSessionInfoFactory.getObject().getAcctID());
		 
		return sqlHouseExecuteDao.selectJSONArray(201, map);
	}
	public JSONArray getNotUsedKpiList(String searchyyyy, String searchkpinm) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(YYYY)"					, searchyyyy);
		map.put("@(MT_NM)"					, searchkpinm);
		
		return sqlHouseExecuteDao.selectJSONArray(76, map);
	}
	public JSONArray getKpiList(String gubn, String scid, String yyyy, String tgtusernm, String tgtchangenm, String kpichangenm) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(GUBN)"					, gubn);
		map.put("@(SC_ID)"					, scid);
		map.put("@(YYYY)"					, yyyy);
		map.put("@(TGT_USER_NM)"					, tgtusernm);
		map.put("@(TGT_CHARGE_NM)"					, tgtchangenm);
		map.put("@(KPI_NM)"					, kpichangenm);
		
		return sqlHouseExecuteDao.selectJSONArray(77, map);
	}
	public JSONArray getKpiListAdmin(String gubn, String scid, String yyyy) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_USER_ID)"				, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(S_GUBN)"					, gubn);
		map.put("@(S_SC_ID)"					, scid);
		map.put("@(S_YYYY)"					, yyyy);
		
		return sqlHouseExecuteDao.selectJSONArray(203, map);
	}	
	public JSONArray getKpiListTargetAdmin(String gubn, String scid, String yyyy, String tgtusernm, String tgtchangenm, String kpichangenm) throws Exception{    
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_USER_ID)"				, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(S_GUBN)"					, gubn);   
		map.put("@(S_SC_ID)"					, scid);
		map.put("@(S_YYYY)"					, yyyy);
		map.put("@(TGT_USER_NM)"					, tgtusernm);
		map.put("@(TGT_CHARGE_NM)"					, tgtchangenm);
		map.put("@(KPI_NM)"					, kpichangenm);
		
		return sqlHouseExecuteDao.selectJSONArray(277, map);
	}	
	public JSONArray getMappingKpiStrategy(String yyyy, String scid, String gubn) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(YYYY)"					, yyyy);
		map.put("@(SC_ID)"					, scid);
		map.put("@(GUBN)"					, gubn);
		
		return sqlHouseExecuteDao.selectJSONArray(78, map);
	}
	public JSONArray getMappingKpiUser(String yyyy, String scid, String gubn) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(YYYY)"					, yyyy);
		map.put("@(SC_ID)"					, scid);
		map.put("@(GUBN)"					, gubn);
		
		return sqlHouseExecuteDao.selectJSONArray(82, map);
	}
	public JSONArray getMappingKpiAdminUser(String yyyy, String scid, String gubn) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(YYYY)"					, yyyy);
		map.put("@(SC_ID)"					, scid);
		map.put("@(GUBN)"					, gubn);
		
		return sqlHouseExecuteDao.selectJSONArray(197, map);
	}	
	public JSONArray getParentKpiList() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		return sqlHouseExecuteDao.selectJSONArray(134, map);
	}
	public JSONArray getParentKpiListAdmin() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		return sqlHouseExecuteDao.selectJSONArray(222, map);
	}
	
	public JSONArray getMappingKpiWeight(String yyyy, String scid, String gubn) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(YYYY)"					, yyyy);
		map.put("@(SC_ID)"					, scid);
		map.put("@(GUBN)"					, gubn);
		
		return sqlHouseExecuteDao.selectJSONArray(83, map);
	}
	public JSONArray getMappingKpiAdminWeight(String yyyy, String scid, String gubn) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(YYYY)"					, yyyy);
		map.put("@(SC_ID)"					, scid);
		map.put("@(GUBN)"					, gubn);
		
		return sqlHouseExecuteDao.selectJSONArray(198, map);
	}	
	public JSONArray popupKpiDetailMtList(String useyn, String mtattr, String searchmtnm) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(USE_YN)"					, useyn);
		map.put("@(MT_UDC2)"				, mtattr);
		map.put("@(MT_NM)"					, searchmtnm);
		
		return sqlHouseExecuteDao.selectJSONArray(126, map);
	}
	public JSONArray popupKpiDetailMainList(String mtid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(MT_ID)"					, mtid);
		
		return sqlHouseExecuteDao.selectJSONArray(127, map);
	}
	public JSONArray popupKpiDetailMtList_141(String useyn, String mtattr, String searchmtnm) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(USE_YN)"					, useyn);
		map.put("@(MT_UDC2)"				, mtattr);
		map.put("@(MT_NM)"					, searchmtnm);
		
		return sqlHouseExecuteDao.selectJSONArray(126, map);
	}
	public JSONArray popupKpiDetailMainList_141(String mtid, String pYyyy) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(MT_ID)"					, mtid);
		map.put("@(S_YYYY)"					, pYyyy); 
		
		return sqlHouseExecuteDao.selectJSONArray(263, map);
	}
	public JSONArray listCode(String comlcod) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(COML_COD)"				, comlcod);
		
		return sqlHouseExecuteDao.selectJSONArray(13, map);
	}
	public JSONArray listStraid() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		
		return sqlHouseExecuteDao.selectJSONArray(79, map);
	}
	public JSONArray listSubstraid() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		
		return sqlHouseExecuteDao.selectJSONArray(80, map);
	}
	public JSONArray detailKpi(String scid, String mtid, String year) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(SC_ID)"					, scid);
		map.put("@(MT_ID)"					, mtid);
		map.put("@(S_YYYY)"					, year);
		
		return sqlHouseExecuteDao.selectJSONArray(84,map);
	}
	public JSONArray detailKpiAdmin(String scid, String mtid, String year) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(SC_ID)"					, scid);
		map.put("@(MT_ID)"					, mtid);
		map.put("@(S_YYYY)"					, year);
		
		return sqlHouseExecuteDao.selectJSONArray(212,map);
	}
	public JSONArray listYear(String mtid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(MT_ID)"					, mtid);
		
		return sqlHouseExecuteDao.selectJSONArray(87,map);
	}
	public JSONArray listYearAdmin(String mtid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(MT_ID)"					, mtid);
		
		return sqlHouseExecuteDao.selectJSONArray(220,map);
	}
	public JSONArray getCalculateListByMtID(String mtid, String yyyy) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(MT_ID)"					, mtid);
		map.put("@(END_YYYY)"					, yyyy);
		
		return sqlHouseExecuteDao.selectJSONArray(89, map);
	}
	public JSONArray getCalculateListByMtIDAdmin(String mtid, String yyyy) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(MT_ID)"					, mtid);
		map.put("@(END_YYYY)"					, yyyy);
		
		return sqlHouseExecuteDao.selectJSONArray(219, map);
	}
	public JSONArray listIfcod() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		
		return sqlHouseExecuteDao.selectJSONArray(90, map);
	}
	public JSONArray listIfcodAdmin() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		
		return sqlHouseExecuteDao.selectJSONArray(217, map);
	}
	public JSONArray getCalCodOne(String calcod) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(CAL_COD)"				, calcod);
		
		return sqlHouseExecuteDao.selectJSONArray(88, map);
	}
	public JSONArray getMaxMtid() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		
		return sqlHouseExecuteDao.selectJSONArray(115, map);
	}
	@Transactional
	public int insert(Kpi kpi) throws Exception{
		int result = 0;
		
		TableObject imfe0009 = new TableObject(); 
		
		JSONObject jsonObjectmtid = (JSONObject) getMaxMtid().get(0);
		
		
		imfe0009.put("ACCT_ID"				, loginSessionInfoFactory.getObject().getAcctID(),true); 
		
		if(!kpi.getMtid().equals("")){
			imfe0009.put("MT_ID"					, kpi.getMtid(),true);
			logger.debug("지표 수정 : {}"			, kpi.getMtid());
		}else{
			imfe0009.put("MT_ID"					, jsonObjectmtid.get("MT_ID"), true);
			logger.debug("지표 신규 : {}"			, jsonObjectmtid.get("MT_ID"));
		}
		String realMtid = imfe0009.get("MT_ID").toString();
		
		TableObject imfe0009_1_delete = new TableObject();
		imfe0009_1_delete.put("ACCT_ID"	, loginSessionInfoFactory.getObject().getAcctID(), true);
		imfe0009_1_delete.put("MT_ID"	,realMtid, true);
		//유관업무 삭제 후 등록. 
		logger.debug("imfe0009_1_delete : {}",imfe0009_1_delete); 
		dbmsDao.deleteTable("IMFE0009_1", imfe0009_1_delete);			
		
		if(kpi.getRelatedWork1()!=null){
			ArrayList<LinkedHashMap<String,Object>> arrIMFE0009_1 = new ArrayList<LinkedHashMap<String,Object>>();
			for(int i = 0; i<kpi.getRelatedWork1().length;i++){
				TableObject imfe0009_1 = new TableObject();
				imfe0009_1.put("ACCT_ID"		, loginSessionInfoFactory.getObject().getAcctID(), true);
				imfe0009_1.put("MT_ID"			, realMtid, true);
				imfe0009_1.put("IDX"			, i);
				imfe0009_1.put("RELATED_WORK1"	, kpi.getRelatedWork1()[i]);
				imfe0009_1.put("RELATED_WORK2"	, kpi.getRelatedWork2()[i]);
				arrIMFE0009_1.add(imfe0009_1);
				logger.debug("imfe0009_1 : {}",imfe0009_1);
			}
			dbmsDao.insertTable("IMFE0009_1", arrIMFE0009_1);
		}		 
		
		imfe0009.put("MT_CD"				, kpi.getMtcd());
		imfe0009.put("MT_NM"				, kpi.getMtnm());
		imfe0009.put("MT_DEF"				, kpi.getMtdef());
		imfe0009.put("TARGET_DESC"			, kpi.getTargetdesc());
		imfe0009.put("MT_DESC"				, kpi.getMtdesc()); 
		imfe0009.put("UNIT"					, kpi.getUnit());
		imfe0009.put("MT_TYPE"				, "Z");
		imfe0009.put("ROLLUP_G"				, "D");  
		imfe0009.put("DECIMAL_PLACES"		, kpi.getDecimalplaces());
		imfe0009.put("MT_GBN"				, kpi.getMtgbn());
		imfe0009.put("MEAS_CYCLE"			, kpi.getMeascycle()); 
		imfe0009.put("MT_DIR"			, kpi.getMtdir());
		imfe0009.put("EVAL_TYPE"			, kpi.getEvaltype());
		imfe0009.put("EVA_TYP"				, kpi.getEvatyp());
		imfe0009.put("PERS_CD"				, kpi.getPerscd());
		imfe0009.put("TIME_TARGET_ROLLUP_CD"				, "LAST");
		imfe0009.put("TIME_ACTUAL_ROLLUP_CD"				, "LAST");
		imfe0009.put("STRA_ID"				, kpi.getStraid()); 
		imfe0009.put("SUBSTRA_ID"			, kpi.getSubstraid());
		imfe0009.put("CSF_ID"				, kpi.getCsfID());
		imfe0009.put("START_DAT"			, kpi.getStartdat());
		imfe0009.put("END_DAT"				, kpi.getEnddat());
		imfe0009.put("CAL_TEXT"				, kpi.getCalText());		
		imfe0009.put("MT_USER_ID"				, kpi.getMtUserID());
		imfe0009.put("MT_USER_DEPT"				, kpi.getMtUserDept());		
		
		imfe0009.putCurrentDate("INSERT_DAT");
		imfe0009.put("INSERT_EMP"			, loginSessionInfoFactory.getObject().getUserId());
		imfe0009.putCurrentDate("UPDATE_DAT");
		imfe0009.put("UPDATE_EMP"			, loginSessionInfoFactory.getObject().getUserId()); 
		try{
			if(kpi.getMtid().equals("")){
				//IMFE0009 INSERT 
				dbmsDao.insertTable("IMFE0009", imfe0009);
				
				result = 1;
			}else{
				//IMFE0009 UPDATE 
				dbmsDao.updateTable("IMFE0009",imfe0009.getKeyMap(), imfe0009);
				//IMFE0010_1 UPDATE 
				TableObject imfe0010_1 = new TableObject();
				imfe0010_1.put("ACCT_ID"	, loginSessionInfoFactory.getObject().getAcctID()	,true);
				imfe0010_1.put("MT_ID"		, realMtid											,true);
				imfe0010_1.put("START_DAT"	, kpi.getStartdat());
				imfe0010_1.put("END_DAT"	, kpi.getEnddat());
				imfe0010_1.put("P_KPI_ID"		, kpi.getParentKpiID());
				dbmsDao.updateTable("IMFE0010_1", imfe0010_1.getKeyMap(),imfe0010_1); 
				result = 2;
			}
			
			if(kpi.getScid()!=null&&kpi.getScid().equals("")==false){
				//IMFE0010 DELETE
				TableObject imfe0010_delete = new TableObject();
				imfe0010_delete.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID(), true);
				imfe0010_delete.put("MT_ID", realMtid, true); 
				dbmsDao.deleteTable("IMFE0010", imfe0010_delete);								 
				
				//IMFE0010 INSERT				
				ArrayList<LinkedHashMap<String,Object>> arrImfe0010 = new ArrayList<LinkedHashMap<String,Object>>();
				//ArrayList<LinkedHashMap<String,Object>> arrImfe0010_1 = new ArrayList<LinkedHashMap<String,Object>>();
				for(int k=0;k<kpi.getScid().split(",").length;k++){
					TableObject imfe0010 = new TableObject();
					TableObject imfe0010_1 = new TableObject();
					String scOne = kpi.getScid().split(",")[k];
					imfe0010.put("ACCT_ID"		, loginSessionInfoFactory.getObject().getAcctID(), true);
					imfe0010.put("SC_ID"		, scOne, true);
					imfe0010.put("MT_ID"		, realMtid, true);
					imfe0010.put("KPI_ID"		, "S"+scOne+"M"+realMtid); 
					imfe0010.putCurrentDate("INSERT_DAT");
					imfe0010.put("INSERT_EMP"	, loginSessionInfoFactory.getObject().getUserId());
					imfe0010.putCurrentDate("UPDATE_DAT");
					imfe0010.put("UPDATE_EMP"	, loginSessionInfoFactory.getObject().getUserId());	 
					arrImfe0010.add(imfe0010);
					
					//신규일경우 imfe0010_1 insert
					
					//if(kpi.getMtid().equals("")){
					imfe0010_1.put("ACCT_ID"	, loginSessionInfoFactory.getObject().getAcctID()	,true);
					imfe0010_1.put("MT_ID"		, realMtid											,true);
					imfe0010_1.put("SC_ID"		, scOne,true);
					imfe0010_1.put("KPI_ID"		, "S"+scOne+"M"+realMtid,true);
					imfe0010_1.put("START_DAT"	, kpi.getStartdat());
					imfe0010_1.put("END_DAT"	, kpi.getEnddat());
					imfe0010_1.put("P_KPI_ID"		, kpi.getParentKpiID());
					
					if(kpi.getMtid().equals("")){
						imfe0010_1.putCurrentDate("INSERT_DAT");
						imfe0010_1.put("INSERT_EMP"	, loginSessionInfoFactory.getObject().getUserId());
					}
						
					imfe0010_1.putCurrentDate("UPDATE_DAT");
					imfe0010_1.put("UPDATE_EMP"	, loginSessionInfoFactory.getObject().getUserId());
						//arrImfe0010_1.add(imfe0010);
					//}
					
					dbmsDao.upsertTable("IMFE0010_1", imfe0010_1.getKeyMap(),imfe0010_1);
				}
				dbmsDao.insertTable("IMFE0010", arrImfe0010); 
				
				//dbmsDao.insertTable("IMFE0010_1", arrImfe0010_1);
				
				
				
			}
			
			//대학지표일 경우 가중치 등록
			if(kpi.getScid().equals("100")){
				//IMFE0026 : 가중치 
				TableObject imfe0026 = new TableObject();
				imfe0026.put("ACCT_ID"		, loginSessionInfoFactory.getObject().getAcctID(),true);
				imfe0026.put("YYYY"			, kpi.getYyyy(),true);
				imfe0026.put("KPI_ID"		, "S"+kpi.getScid()+"M"+realMtid,true);				
				imfe0026.put("SC_ID"		, kpi.getScid(),true);
				imfe0026.put("MT_ID"		, realMtid);				
				imfe0026.put("KPI_WEIGHT"	, kpi.getKpiWeight());
				imfe0026.put("KPI_CURRAMT"	, 0);
				imfe0026.putCurrentDate("INSERT_DAT");
				imfe0026.put("INSERT_EMP"	, loginSessionInfoFactory.getObject().getUserId());
				imfe0026.putCurrentDate("UPDATE_DAT");
				imfe0026.put("UPDATE_EMP"	, loginSessionInfoFactory.getObject().getUserId());
				//IMFE0026 : 가중치 입력
				dbmsDao.upsertTable("IMFE0026",imfe0026.getKeyMap(),imfe0026);
			}
			 
			//IMFE0030 측정월 
			//IMFE0030 DELETE
			TableObject imfe0030_delete = new TableObject();
			imfe0030_delete.put("ACCT_ID"	, loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0030_delete.put("MT_ID"		, realMtid, true);
			dbmsDao.deleteTable("IMFE0030", imfe0030_delete);
		
			ArrayList<LinkedHashMap<String,Object>> arrImfe0030 = new ArrayList<LinkedHashMap<String,Object>>();
			//IMFE0030 INSERT 
			if(kpi.getMm()!=null){
				for(int j=0;j<kpi.getMm().length;j++){
					TableObject imfe0030 = new TableObject();
					imfe0030.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID(), true);
					imfe0030.put("MT_ID"		, realMtid, true);
					imfe0030.put("MM"			, kpi.getMm()[j]);
					imfe0030.putCurrentDate("INSERT_DAT");
					imfe0030.put("INSERT_EMP"	, loginSessionInfoFactory.getObject().getUserId());
					arrImfe0030.add(imfe0030);
				}
				dbmsDao.insertTable("IMFE0030", arrImfe0030);
			}
			 
			JSONParser parser = new JSONParser();
			JSONArray calculateList = null;
			if(kpi.getCalculateList().equals("")==false)
				calculateList = (JSONArray)parser.parse(kpi.getCalculateList());
			
			
			//산식 변경건이 있을 경우만 
			if(calculateList!=null&&calculateList.size()!=0){
			
				ArrayList<LinkedHashMap<String,Object>> arrImfe0082 = new ArrayList<LinkedHashMap<String,Object>>();
				ArrayList<LinkedHashMap<String,Object>> arrImfe0011 = new ArrayList<LinkedHashMap<String,Object>>(); 
				
				TableObject deleteKey = new TableObject();
				deleteKey.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID(),true);
				deleteKey.put("MT_ID", realMtid,true);

				dbmsDao.deleteTable("IMFE0082", deleteKey);
				dbmsDao.deleteTable("IMFE0011", deleteKey);
				
				for(Object obj : calculateList){
					JSONObject calculate = (JSONObject)obj;
					JSONArray detailList = (JSONArray)calculate.get("VALUE");
					
					//IMFE0082
					TableObject imfe0082 = new TableObject();
					imfe0082.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID(),true);
					imfe0082.put("MT_ID", realMtid,true);
					imfe0082.put("CAL_COD", calculate.get("CAL_COD"));
					imfe0082.put("END_YYYY", calculate.get("END_YYYY"));
					imfe0082.put("START_YYYY", calculate.get("START_YYYY"));
					
					for(Object cobj : detailList){
						JSONObject value = (JSONObject)cobj;
						//IMFE0011
						TableObject imfe0011 = new TableObject();
						imfe0011.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID(),true);
						imfe0011.put("MT_ID", realMtid,true);
						imfe0011.put("END_YYYY", calculate.get("END_YYYY"));
						imfe0011.put("START_YYYY", calculate.get("START_YYYY"));
						
						imfe0011.put("COL_GBN", value.get("COL_GBN"));
						
						imfe0011.put("COL_NM", value.get("COL_NM"));
						imfe0011.put("COL_UNIT", value.get("COL_UNIT"));
						imfe0011.put("COL_SYSTEM", value.get("COL_SYSTEM"));
						imfe0011.put("IF_COD", value.get("IF_COD"));
						arrImfe0011.add(imfe0011);
					}
					arrImfe0082.add(imfe0082);
				}
				dbmsDao.insertTable("IMFE0082", arrImfe0082);
				dbmsDao.insertTable("IMFE0011", arrImfe0011); 
			}
			
 
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		
		return result;
	}
	public int remove(String mtid, String yyyy) throws Exception {
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(MT_ID)"					, mtid);
		map.put("@(YYYY)"					, yyyy);
		
		String[] arrayClob = null;
		try{
			sqlHouseExecuteDao.crudQuery(124, map, arrayClob);
			result = 1;
		}catch(Exception e){
			logger.error("error:",e);			
			result = 0;
			throw e;
		}
		return result; 
	}
	public int insertTarget(String targetevagbn, String targetyyyy, String targetorgyyyy, String targetkpiid, String targetamt, String  targetcurramt) throws Exception{
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(EVA_GBN)"				, targetevagbn);
		map.put("@(ORG_YYYY)"				, targetorgyyyy);
		map.put("@(YYYY)"					, targetyyyy);
		map.put("@(KPI_ID)"					, targetkpiid);
		map.put("@(TARGET_AMT)"				, targetamt);
		map.put("@(TARGET_CURRAMT)"			, targetcurramt);
		map.put("@(UPDATE_EMP)"				, loginSessionInfoFactory.getObject().getUserId());
		String[] arrayClob = null;
		try{
			logger.debug("targetorgyyyy = " + targetorgyyyy);
			if(targetorgyyyy.equals("")){
				//신규
				sqlHouseExecuteDao.crudQuery(103,map,arrayClob);
				result = 1;
			}else{
				//수정
				sqlHouseExecuteDao.crudQuery(104,map,arrayClob);
				result = 2;
			}
		}catch(Exception e){
			result = 0; 
			logger.error("error:",e);
			throw e;
		}
		return result;
	}
	public int insertTargetAdmin(String targetevagbn, String targetyyyy, String targetorgyyyy, String targetkpiid, String targetamt, String  targetcurramt) throws Exception{
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(EVA_GBN)"				, targetevagbn);
		map.put("@(ORG_YYYY)"				, targetorgyyyy);
		map.put("@(YYYY)"					, targetyyyy);
		map.put("@(KPI_ID)"					, targetkpiid);
		map.put("@(TARGET_AMT)"				, targetamt);
		map.put("@(TARGET_CURRAMT)"			, targetcurramt);
		map.put("@(UPDATE_EMP)"				, loginSessionInfoFactory.getObject().getUserId());
		String[] arrayClob = null;
		try{
			logger.debug("targetorgyyyy = " + targetorgyyyy);
			if(targetorgyyyy.equals("")){
				//신규
				sqlHouseExecuteDao.crudQuery(209,map,arrayClob);
				result = 1;
			}else{
				//수정
				sqlHouseExecuteDao.crudQuery(210,map,arrayClob);
				result = 2;
			}
		}catch(Exception e){
			result = 0; 
			logger.error("error:",e);
			throw e;
		}
		return result;
	}
	public int removeTarget(String targetevagbn, String targetorgyyyy, String targetkpiid) throws Exception{
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(EVA_GBN)"					, targetevagbn);
		map.put("@(YYYY)"						, targetorgyyyy);
		map.put("@(KPI_ID)"						, targetkpiid);
		String[] arrayClob = null;
		try{
			sqlHouseExecuteDao.crudQuery(105, map, arrayClob);
			result = 1;
		}catch(Exception e){
			result = 0;
			logger.error("error:",e);
			throw e;
		}
		return result;
	}
	public int removeTargetAdmin(String targetevagbn, String targetorgyyyy, String targetkpiid) throws Exception{
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(EVA_GBN)"					, targetevagbn);
		map.put("@(YYYY)"						, targetorgyyyy);
		map.put("@(KPI_ID)"						, targetkpiid);
		String[] arrayClob = null;
		try{
			sqlHouseExecuteDao.crudQuery(211, map, arrayClob);
			result = 1;
		}catch(Exception e){
			result = 0;
			logger.error("error:",e);
			throw e;
		}
		return result;
	}
	@Transactional
	public int insertKpiuser(String[] tgtuserid, String[] tgtchargeid, String[] owneruserid, String[] kpichargeid, String[] kpiid, String[] userscid, String[] empid, String yyyyKpiUser, String evagbnKpiUser) throws Exception{
		int result = 0;
		ArrayList<LinkedHashMap<String,Object>> arrImfe0010_1 = new ArrayList<LinkedHashMap<String,Object>>();
		ArrayList<LinkedHashMap<String,Object>> arrImfe0038 = new ArrayList<LinkedHashMap<String,Object>>();
			for(int i=0;i<tgtuserid.length;i++){
				TableObject imfe0010_1 = new TableObject();			 
				imfe0010_1.put("ACCT_ID"						, loginSessionInfoFactory.getObject().getAcctID(), true);			
				imfe0010_1.put("KPI_ID"				            , kpiid[i], true);
				imfe0010_1.put("SC_ID"							, userscid[i], true); 
				imfe0010_1.put("TGT_USER_ID"					, tgtuserid[i]);
				imfe0010_1.put("TGT_CHARGE_ID"					, tgtchargeid[i]);
				imfe0010_1.put("OWNER_USER_ID"					, owneruserid[i]);
				imfe0010_1.put("KPI_CHARGE_ID"					, kpichargeid[i]);
				imfe0010_1.put("UPDATE_EMP"						, loginSessionInfoFactory.getObject().getUserId());
				imfe0010_1.putCurrentTimeStamp("UPDATE_DAT");
				arrImfe0010_1.add(imfe0010_1);
			}
			//평가자 저장
				for(int i=0;i<empid.length;i++){
					TableObject imfe0038 = new TableObject();			 
					imfe0038.put("ACCT_ID"						, loginSessionInfoFactory.getObject().getAcctID(), true);
					imfe0038.put("KPI_ID"				            , kpiid[i], true);
					imfe0038.put("SC_ID"							, userscid[i], true); 
					imfe0038.put("EP_NO"							, empid[i]); 
					imfe0038.put("YYYY"							, yyyyKpiUser, true); 
					imfe0038.put("EVA_GBN"							, evagbnKpiUser, true); 
					imfe0038.put("UPDATE_EMP"						, loginSessionInfoFactory.getObject().getUserId());
					imfe0038.putCurrentTimeStamp("UPDATE_DAT");
					arrImfe0038.add(imfe0038);
				}		
		try{
				dbmsDao.updateTable("IMFE0010_1",arrImfe0010_1);
				dbmsDao.updateTable("IMFE0038",arrImfe0038);
				
				result = 1;
			}catch(Exception e){
				result = 0;
				logger.error("error:",e);
				throw e;
			}
		return result;
	}
	public int insertKpiweight(String[] weightkpiid, String[] weightscid, String[] weightmtid, String weightyyyy, String[] kpicurramt, String[] kpiweight) throws Exception {
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"						, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(UPDATE_EMP)"						, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(YYYY)"							, weightyyyy);
		String[] arrayClob = null;
		for(int i=0;i<weightkpiid.length;i++){
			map.put("@(KPI_ID)"					, weightkpiid[i]);
			map.put("@(SC_ID)"					, weightscid[i]);
			map.put("@(MT_ID)"					, weightmtid[i]);
			map.put("@(KPI_CURRAMT)"			, kpicurramt[i]);
			map.put("@(KPI_WEIGHT)"				, kpiweight[i]);
			try{
				sqlHouseExecuteDao.crudQuery(108, map, arrayClob);
				sqlHouseExecuteDao.crudQuery(112, map, arrayClob);
				result = 1;
			}catch(Exception e){
				result = 0;
				logger.error("error:",e);
				throw e;
			}
		}
		return result;
	}
	public JSONArray kpiResultCheck(String evaGbn, String yyyymm, String kpiid, String analCycle, String colGbn) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"							, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EVA_GBN)"							, evaGbn);
		map.put("@(S_YYYYMM)"							, yyyymm);
		map.put("@(S_KPI_ID)"							, kpiid);
		map.put("@(S_ANAL_CYCLE)"						, analCycle);
		map.put("@(S_COL_GBN)"							, colGbn);
		
		return sqlHouseExecuteDao.selectJSONArray(196, map);
	}
	public JSONArray kpiResultCheckAdmin(String evaGbn, String yyyymm, String kpiid, String analCycle, String colGbn) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"							, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EVA_GBN)"							, evaGbn);
		map.put("@(S_YYYYMM)"							, yyyymm);
		map.put("@(S_KPI_ID)"							, kpiid);
		map.put("@(S_ANAL_CYCLE)"						, analCycle);
		map.put("@(S_COL_GBN)"							, colGbn);

		return sqlHouseExecuteDao.selectJSONArray(262, map);
	}
	@Transactional
	public int insertKpiResultColValue(Kpi kpi) throws Exception{
		int result = 0;
		List<LinkedHashMap<String, Object>> arrayInsertImfe0029 = new ArrayList<LinkedHashMap<String,Object>>();
		List<LinkedHashMap<String, Object>> arrayImfe0029 = new ArrayList<LinkedHashMap<String,Object>>();
		for(int i=0;i<kpi.getKpiID().length;i++){
			TableObject imfe0029 = new TableObject();
			imfe0029.put("ACCT_ID"						, loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0029.put("EVA_GBN"						, kpi.getpEvaGbn(), true);
			imfe0029.put("YYYYMM"						, kpi.getYyyymm()[i], true);
			imfe0029.put("KPI_ID"						, kpi.getKpiID()[i], true);
			imfe0029.put("ANAL_CYCLE"					, kpi.getAnalCycle()[i], true);
			imfe0029.put("COL_GBN"						, kpi.getColGbn()[i], true);
			imfe0029.put("COL_VALUE"					, kpi.getColValue()[i]); 
			if(kpiResultCheck(kpi.getpEvaGbn(), kpi.getYyyymm()[i], kpi.getKpiID()[i], kpi.getAnalCycle()[i], kpi.getColGbn()[i]).size() > 0){
				imfe0029.putCurrentDate("UPDATE_DAT");
				arrayImfe0029.add(imfe0029);	
			}else{
				imfe0029.putCurrentDate("INSERT_DAT");
				arrayInsertImfe0029.add(imfe0029);
			}
		}
		try{
			dbmsDao.insertTable("IMFE0029", arrayInsertImfe0029);
			dbmsDao.updateTable("IMFE0029", arrayImfe0029);
			result = 1;
		}catch(Exception e){
			result = 0;
			logger.error("error:",e);
			throw e;
		}
		return result;
	}
	public JSONArray kpiResultCheckIMFE0020(String evagbn, String gubun, String yyyymm, String kpiid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EVA_GBN)"					, evagbn);
		map.put("@(S_GUBUN)"					, gubun);
		map.put("@(S_YYYYMM)"					, yyyymm);
		map.put("@(S_KPI_ID)"					, kpiid);
		
		return sqlHouseExecuteDao.selectJSONArray(280, map);
	}
	@Transactional
	public void kpiResultInsertOne(Kpi kpi) throws Exception{
		
		TableObject imfe0020 = new TableObject();
		imfe0020.put("ACCT_ID"					, loginSessionInfoFactory.getObject().getAcctID(), true);
		imfe0020.put("EVA_GBN"						, kpi.getpEvaGbn(), true);
		imfe0020.put("GUBUN"						, "A", true);
		imfe0020.put("YYYYMM"						, kpi.getChargeOneYyyymm(), true);
		imfe0020.put("KPI_ID"						, kpi.getChargeOneKpiID(), true); 
		
		if(kpi.getUserGubn().equals("Charge")){
			imfe0020.put("CHARGE_ID"				, kpi.getChargeOneID());
			imfe0020.put("CHARGE_NM"				, kpi.getChargeOneName());
			imfe0020.put("CHARGE_CONFIRM"		, kpi.getChargeOneConfirm());
			imfe0020.putCurrentTimeStamp("CHARGE_DATE");
			
		}else{
			imfe0020.put("USER_ID"					, kpi.getUserOneID());
			imfe0020.put("USER_NM"					, kpi.getUserOneName()); 
			imfe0020.put("USER_CONFIRM"			, kpi.getUserOneConfirm());
			imfe0020.putCurrentTimeStamp("USER_DATE");  
		}
		kpiDao.createKpiValue(loginSessionInfoFactory.getObject().getAcctID(), kpi.getpEvaGbn(), kpi.getChargeOneKpiID(), kpi.getChargeOneYyyymm());
		dbmsDao.upsertTable("IMFE0020", imfe0020.getKeyMap(), imfe0020); 
	}
	//실적관리 체크
	public JSONArray kpiResultCheck(String evagbn, String yyyy, String mm) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EVA_GBN)"					, evagbn);
		map.put("@(S_YYYY)"						, yyyy);
		map.put("@(S_MM)"						, mm);
		
		return sqlHouseExecuteDao.selectJSONArray(279, map);
	}
	@Transactional
	public int kpiResultInsertAll(Kpi kpi) throws Exception{
		int result = 0;
		ArrayList<LinkedHashMap<String, Object>> arrInsertIMFE0020 = new ArrayList<LinkedHashMap<String,Object>>();
		ArrayList<LinkedHashMap<String, Object>> arrUpdateChargeIMFE0020 = new ArrayList<LinkedHashMap<String,Object>>();
		ArrayList<LinkedHashMap<String, Object>> arrUpdateUserIMFE0020 = new ArrayList<LinkedHashMap<String,Object>>();
		TableCondition empViewCondition = new TableCondition();
		empViewCondition.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID());
		empViewCondition.put("EMP_ID", loginSessionInfoFactory.getObject().getUserId());
		List<LinkedHashMap<String,Object>> empList = dbmsDao.selectTable("EMP_VIEW", empViewCondition);
		String unid = "";
		if(empList.size()!=0){
			unid = empList.get(0).get("UNID").toString();
		}
		for(int i=0;i<kpi.getKpiID().length;i++){
			if(kpi.getUserGubn().equals("Charge")){ 
					if( kpi.getChargeID()[i].equals(unid)==false){
						continue;
					}
			}else{ 
					if( kpi.getUserID()[i].equals(unid)==false){
						continue;
					}
			}
			TableObject imfe0020 = new TableObject();
			TableObject imfe0020_charge = new TableObject();
			TableObject imfe0020_user = new TableObject();
			imfe0020.put("ACCT_ID"				, loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0020.put("EVA_GBN"				, kpi.getpEvaGbn(), true);
			imfe0020.put("GUBUN"				, "A", true);
			imfe0020.put("YYYYMM"				, kpi.getYyyymm()[i], true);     
			imfe0020.put("KPI_ID"				, kpi.getKpiID()[i], true); 
			imfe0020.put("CHARGE_ID"			, kpi.getChargeID()[i]);
			imfe0020.put("CHARGE_NM"			, kpi.getChargeName()[i]);
			imfe0020.put("CHARGE_CONFIRM"		, kpi.getConfirm());
			imfe0020.putCurrentTimeStamp("CHARGE_DATE");
			imfe0020.put("USER_ID"				, kpi.getUserID()[i]);
			imfe0020.put("USER_NM"				, kpi.getUserName()[i]);
			imfe0020.put("USER_CONFIRM"			, "");
			imfe0020.put("USER_DATE"			, "");
			
			imfe0020_charge.put("ACCT_ID"				, loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0020_charge.put("EVA_GBN"				, kpi.getpEvaGbn(), true);
			imfe0020_charge.put("GUBUN"					, "A", true);
			imfe0020_charge.put("YYYYMM"				, kpi.getYyyymm()[i], true);
			imfe0020_charge.put("KPI_ID"				, kpi.getKpiID()[i], true);
			imfe0020_charge.put("CHARGE_ID"				, kpi.getChargeID()[i]);
			imfe0020_charge.put("CHARGE_NM"				, kpi.getChargeName()[i]);
			imfe0020_charge.put("CHARGE_CONFIRM"		, kpi.getConfirm());
			imfe0020_charge.putCurrentTimeStamp("CHARGE_DATE");
			
			imfe0020_user.put("ACCT_ID"				, loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0020_user.put("EVA_GBN"				, kpi.getpEvaGbn(), true);
			imfe0020_user.put("GUBUN"				, "A", true);
			imfe0020_user.put("YYYYMM"				, kpi.getYyyymm()[i], true);
			imfe0020_user.put("KPI_ID"				, kpi.getKpiID()[i], true);
			imfe0020_user.put("USER_ID"				, kpi.getUserID()[i]);
			imfe0020_user.put("USER_NM"				, kpi.getUserName()[i]);
			imfe0020_user.put("USER_CONFIRM"		, kpi.getConfirm());
			imfe0020_user.putCurrentTimeStamp("USER_DATE"); 
			
			if(kpiResultCheckIMFE0020(kpi.getpEvaGbn(),"A", kpi.getYyyymm()[i],kpi.getKpiID()[i]).size() == 0){ 
				if(!kpi.getUserConfirm()[i].toString().trim().equals("Y")){
					 
					if(kpi.getChargeConfirm()[i].toString().equals("")){ 
						arrInsertIMFE0020.add(imfe0020);
					}
				}
			}else{
				if(kpi.getUserGubn().equals("Charge")){ 
					if(!kpi.getUserConfirm()[i].toString().trim().equals("Y")){
						arrUpdateChargeIMFE0020.add(imfe0020_charge);
					}
				}else{ 
					if(kpi.getChargeConfirm()[i].toString().trim().equals("Y")){    
						arrUpdateUserIMFE0020.add(imfe0020_user);
					}
				}
			}
			kpiDao.createKpiValue(loginSessionInfoFactory.getObject().getAcctID(), kpi.getpEvaGbn(),  kpi.getKpiID()[i], kpi.getYyyymm()[i]);
		}
		
		try{
			if(arrInsertIMFE0020.size() > 0){
				dbmsDao.insertTable("IMFE0020", arrInsertIMFE0020);
			}else if(kpi.getUserGubn().equals("Charge")){
				dbmsDao.updateTable("IMFE0020", arrUpdateChargeIMFE0020);
			}else{
				dbmsDao.updateTable("IMFE0020", arrUpdateUserIMFE0020);
			}
			result = 1;
		}catch(Exception e){
			e.printStackTrace();
			result = 0;
			throw e;
		}
		return result;
	}
	@Transactional
	public void insertResultColValueAdmin(Kpi kpi) throws Exception{
		int result = 0;
		List<LinkedHashMap<String, Object>> arrayInsertImfe0029 = new ArrayList<LinkedHashMap<String,Object>>();
		List<LinkedHashMap<String, Object>> arrayImfe0029 = new ArrayList<LinkedHashMap<String,Object>>();
		for(int i=0;i<kpi.getColValue().length;i++){
			TableObject imfe0029 = new TableObject();
			imfe0029.put("ACCT_ID"						, loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0029.put("EVA_GBN"						, kpi.getpEvaGbn(), true);
			imfe0029.put("YYYYMM"						, kpi.getColYyyymm()[i], true);
			imfe0029.put("KPI_ID"						, kpi.getColKpiID()[i], true);
			imfe0029.put("ANAL_CYCLE"					, kpi.getColAnalcycle()[i], true); 
			imfe0029.put("COL_GBN"						, kpi.getColColgbn()[i], true);
			imfe0029.put("COL_VALUE"					, kpi.getColValue()[i]);
			

			
			if(kpiResultCheckAdmin(kpi.getpEvaGbn(), kpi.getColYyyymm()[i], kpi.getColKpiID()[i], kpi.getColAnalcycle()[i], kpi.getColColgbn()[i]).size() > 0){
				
				imfe0029.putCurrentDate("UPDATE_DAT");
				logger.info("imfe0029:{}",imfe0029.toString());
				arrayImfe0029.add(imfe0029);	   
			}else{ 
				imfe0029.putCurrentDate("INSERT_DAT");
				logger.info("imfe0029:{}",imfe0029.toString());
				arrayInsertImfe0029.add(imfe0029);
			}
		}
		try{
			if(arrayInsertImfe0029.size() > 0){
				dbmsDao.insertTable("IMFE0029", arrayInsertImfe0029);
			}
			if(arrayImfe0029.size() > 0){
				dbmsDao.updateTable("IMFE0029", arrayImfe0029);
			} 
		}catch(Exception e){ 
			logger.error("error:",e);
			throw e;
		} 
	}
	public List<String> getAlphabet(String source, int op){
		String pattern  =  "";  
		ArrayList<String> arrList1 = new ArrayList<String>();
		
		   
		switch(op){
			case 1: pattern=  "^[a-zA-Z]$"; break;
		}
		
		
		for(int i=0; i<source.length(); i++){
			
			Pattern  p  =  Pattern.compile(pattern);
			Matcher  m  =  p.matcher(String.valueOf(source.charAt(i)));
			
			if(m.find()) arrList1.add(String.valueOf(source.charAt(i)));
			
		}
		
		HashSet<String> hashSet = new HashSet<String>(arrList1);
		ArrayList<String> arrList2 = new ArrayList<String>(hashSet) ; 
		Collections.sort(arrList2);
		
		return arrList2;
	}

 
	@Transactional
	public int insertValueWeight(String yyyy, String univMtID,
			float univMtWeight, String[] scIDList, String[] targetSustList,
			float[] kpiCurrentAmtList) throws Exception {
		// TODO Auto-generated method stub
		float sum = 0;
		int sustIDX = -1;
		float sustKpiCurrentAmt;
		float sustKpiWeight;
		
		ArrayList<LinkedHashMap<String,Object>> arrImfe0026 = new ArrayList<LinkedHashMap<String,Object>>();
		TableCondition imfe0026_delete = new TableCondition();
		imfe0026_delete.put("ACCT_ID"		, loginSessionInfoFactory.getObject().getAcctID());
		imfe0026_delete.put("YYYY"			, yyyy);
		imfe0026_delete.put("KPI_ID"		, "S100M"+univMtID);
		imfe0026_delete.put("SC_ID"			, "100"				,TableCondition.Condition.NotEqual);
		dbmsDao.deleteTable("IMFE0026"		, imfe0026_delete);

		for(int i = 0 ; i<scIDList.length;i++){
			
			float weight = univMtWeight *(kpiCurrentAmtList[i]/100);
			sum+=kpiCurrentAmtList[i];
			if(scIDList[i].equals("SUST")){
				sustIDX = i;				
				continue;
			}
			
			TableObject imfe0026 = new TableObject();
			imfe0026.put("ACCT_ID"		, loginSessionInfoFactory.getObject().getAcctID());
			imfe0026.put("YYYY"			, yyyy);
			imfe0026.put("KPI_ID"		, "S100M"+univMtID);
			imfe0026.put("SC_ID"		, scIDList[i]);
			imfe0026.put("MT_ID"		, univMtID);
			imfe0026.put("KPI_CURRAMT"	, kpiCurrentAmtList[i]);			
			imfe0026.put("KPI_WEIGHT"	, weight);	
			imfe0026.put("INSERT_EMP"   , loginSessionInfoFactory.getObject().getUserId());
			imfe0026.put("UPDATE_EMP"   , loginSessionInfoFactory.getObject().getUserId());
			imfe0026.putCurrentTimeStamp("INSERT_DAT");
			imfe0026.putCurrentTimeStamp("UPDATE_DAT");
			logger.info("row info : {}",imfe0026);
			arrImfe0026.add(imfe0026);
		}
		if(sustIDX!=-1&&targetSustList!=null&&targetSustList.length!=0){
			sustKpiCurrentAmt = kpiCurrentAmtList[sustIDX] / targetSustList.length;
			sustKpiWeight = univMtWeight * (sustKpiCurrentAmt/100);
			for(int i = 0;i<targetSustList.length;i++){
				TableObject imfe0026 = new TableObject();
				imfe0026.put("ACCT_ID"		, loginSessionInfoFactory.getObject().getAcctID());
				imfe0026.put("YYYY"			, yyyy);
				imfe0026.put("KPI_ID"		, "S100M"+univMtID);
				imfe0026.put("SC_ID"		, targetSustList[i]);
				imfe0026.put("MT_ID"		, univMtID);
				imfe0026.put("KPI_CURRAMT"	, sustKpiCurrentAmt);			
				imfe0026.put("KPI_WEIGHT"	, sustKpiWeight);	 
				imfe0026.put("INSERT_EMP"   , loginSessionInfoFactory.getObject().getUserId());
				imfe0026.put("UPDATE_EMP"   , loginSessionInfoFactory.getObject().getUserId());
				imfe0026.putCurrentTimeStamp("INSERT_DAT");
				imfe0026.putCurrentTimeStamp("UPDATE_DAT");
				logger.info("row info : {}",imfe0026);
				arrImfe0026.add(imfe0026);
			}
		}	
		logger.info("지표id : {},가치형성 비율 총 합계 : {}",univMtID,sum);
		if(sum>100) throw new RuntimeException("가치형성비율 합은 100을 초과 할 수 없습니다.");
		
		dbmsDao.insertTable("IMFE0026", arrImfe0026);
		return 1;
	}
	 
	//담당자평가(저장 OR 제출)
	public int insertGradeValue(String[] yyyy, String[] mm, String[] evaGbn, String[] kpiId,  String[] scId, String[] mtId, String[] grade, String[] gradeDesc,String[] status, String[] ipvDesc) throws Exception{
		int result = 0;
		List<LinkedHashMap<String, Object>> arrayInsertImfe0039 = new ArrayList<LinkedHashMap<String,Object>>();
		List<LinkedHashMap<String, Object>> arrayImfe0039 = new ArrayList<LinkedHashMap<String,Object>>();
		for(int i=0;i<kpiId.length;i++){
			
			TableObject imfe0039 = new TableObject();
			
			//회기 기준 날짜 환산
			HashMap<String, Object> param = new HashMap<String, Object>();
			String yyyyMm = "";
			param.put("@(S_ACCT_ID)", loginSessionInfoFactory.getObject().getAcctID());
			param.put("@(S_YYYY)"	, yyyy[i]);
			//List<LinkedHashMap<String, Object>> rs = sqlHouseExecuteDao.selectHashMap(267, param);
			yyyyMm = yyyy[i]+"12";
	
			//담당자 사번 조회
			HashMap<String, Object> unid= new HashMap<String, Object>();
			String unId = "";
			unid.put("@(S_ACCT_ID)" , loginSessionInfoFactory.getObject().getAcctID());
			unid.put("@(S_USER_ID)" , loginSessionInfoFactory.getObject().getUserId());
			List<LinkedHashMap<String, Object>> rs1 = sqlHouseExecuteDao.selectHashMap(269, unid);
			unId = (String)rs1.get(0).get("UNID");		
	
			imfe0039.put("ACCT_ID",                             loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0039.put("YYYYMM",                             yyyyMm, true);
			imfe0039.put("EVA_GBN",							evaGbn[i], true);
			imfe0039.put("KPI_ID",								 	kpiId[i], true);
			imfe0039.put("SC_ID",									scId[i], true);
			imfe0039.put("MT_ID",									mtId[i], true);
			imfe0039.put("GRADE",								grade[i]);
			imfe0039.put("GRADE_DESC",						gradeDesc[i]);
			imfe0039.put("IPV_DESC",                           ipvDesc[i]);
			imfe0039.put("STATUS",                              status[i]);
			imfe0039.put("EP_NO",                             unId);
			imfe0039.put("UPDATE_EMP",						loginSessionInfoFactory.getObject().getUserId());
			imfe0039.put("INSERT_EMP",						loginSessionInfoFactory.getObject().getUserId());
			imfe0039.putCurrentDate("CONFIRM_DATE"); 
			imfe0039.putCurrentDate("INSERT_DAT"); 
			imfe0039.putCurrentDate("UPDATE_DAT"); 
			dbmsDao.upsertTable("IMFE0039", imfe0039.getKeyMap(), imfe0039);

		} 
		return result;
	}
	//담당자평가(최종확정)
	public int insertLastGradeValue(String[] yyyy, String[] mm, String[] evaGbn, String[] kpiId,  String[] scId, String[] mtId, String[] grade, String[] gradeDesc,String[] status, String[] ipvDesc) throws Exception{
		int result = 0;
		for(int i=0;i<kpiId.length;i++){
			
			TableObject imfe0039 = new TableObject();
			
			//회기 기준 날짜 환산
			HashMap<String, Object> param = new HashMap<String, Object>();
			String yyyyMm = "";
			param.put("@(S_ACCT_ID)", loginSessionInfoFactory.getObject().getAcctID());
			param.put("@(S_YYYY)"	, yyyy[i]);
			//List<LinkedHashMap<String, Object>> rs = sqlHouseExecuteDao.selectHashMap(267, param);
			yyyyMm = yyyy[i]+"12";
	
			//담당자 사번 조회
			HashMap<String, Object> unid= new HashMap<String, Object>();
			String unId = "";
			unid.put("@(S_ACCT_ID)" , loginSessionInfoFactory.getObject().getAcctID());
			unid.put("@(S_USER_ID)" , loginSessionInfoFactory.getObject().getUserId());
			List<LinkedHashMap<String, Object>> rs1 = sqlHouseExecuteDao.selectHashMap(269, unid);
			unId = (String)rs1.get(0).get("UNID");		
	
			imfe0039.put("ACCT_ID",                             loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0039.put("YYYYMM",                             yyyyMm, true);
			imfe0039.put("EVA_GBN",							evaGbn[i], true);
			imfe0039.put("KPI_ID",								 	kpiId[i], true);
			imfe0039.put("SC_ID",									scId[i], true);
			imfe0039.put("MT_ID",									mtId[i], true);
			imfe0039.put("GRADE",								grade[i]);
			imfe0039.put("GRADE_DESC",						gradeDesc[i]);
			imfe0039.put("IPV_DESC",                           ipvDesc[i]);
			imfe0039.put("STATUS",                              status[i]);
			imfe0039.put("OWNER_ID",                             unId);
			imfe0039.put("UPDATE_EMP",						loginSessionInfoFactory.getObject().getUserId());
			imfe0039.put("INSERT_EMP",						loginSessionInfoFactory.getObject().getUserId());
			imfe0039.putCurrentTimeStamp("FINAL_CONFIRM_DATE"); 
			imfe0039.putCurrentTimeStamp("INSERT_DAT"); 
			imfe0039.putCurrentTimeStamp("UPDATE_DAT"); 
			dbmsDao.upsertTable("IMFE0039", imfe0039.getKeyMap(), imfe0039);

		System.out.println("status[i]===="+status[i]);
		} 
		return result;
	}	
	//담당자평가 제출취소
	public int updateGradeValue(String[] yyyy, String[] mm, String[] evaGbn, String[] kpiId,String[] status) throws Exception{
		int result = 0;
		List<LinkedHashMap<String, Object>> arrayImfe0039 = new ArrayList<LinkedHashMap<String,Object>>();
		for(int i=0;i<kpiId.length;i++){
			//회기 기준 날짜 환산
			HashMap<String, Object> param = new HashMap<String, Object>();
			String yyyyMm = "";
			param.put("@(S_ACCT_ID)", loginSessionInfoFactory.getObject().getAcctID());
			param.put("@(S_YYYY)"	, yyyy[i]);
			//List<LinkedHashMap<String, Object>> rs = sqlHouseExecuteDao.selectHashMap(267, param);
			//yyyyMm = (String)rs.get(0).get("YYYYMM");
			yyyyMm = yyyy[i]+"12";

			//담당자 사번 조회
			HashMap<String, Object> unid= new HashMap<String, Object>();
			String unId = "";
			unid.put("@(S_ACCT_ID)" , loginSessionInfoFactory.getObject().getAcctID());
			unid.put("@(S_USER_ID)" , loginSessionInfoFactory.getObject().getUserId());
				List<LinkedHashMap<String, Object>> rs1 = sqlHouseExecuteDao.selectHashMap(269, unid);
				unId = (String)rs1.get(0).get("UNID");					
			
		TableObject imfe0039 = new TableObject();
		imfe0039.put("ACCT_ID",                               loginSessionInfoFactory.getObject().getAcctID(), true);
		imfe0039.put("YYYYMM",                             yyyyMm, true);
		imfe0039.put("EVA_GBN",							evaGbn[i], true);
		imfe0039.put("KPI_ID",								 	kpiId[i], true);
		imfe0039.put("EP_NO",                             unId);
		imfe0039.put("STATUS",                              status[i]);
		imfe0039.put("UPDATE_EMP",						loginSessionInfoFactory.getObject().getUserId());
		imfe0039.putCurrentDate("UPDATE_DAT");
		arrayImfe0039.add(imfe0039);	
		}
		try{
			dbmsDao.updateTable("IMFE0039", arrayImfe0039);
			result = 1;
		}catch(Exception e){
			result = 0;
			logger.error("error:",e);
			throw e;
		}
		return result;
	}
	//지표자체평가 최종확정취소
	public int updateLastGradeValue(String[] yyyy, String[] mm, String[] evaGbn, String[] kpiId,String[] status) throws Exception{
		int result = 0;
		List<LinkedHashMap<String, Object>> arrayImfe0039 = new ArrayList<LinkedHashMap<String,Object>>();
		for(int i=0;i<kpiId.length;i++){
			//회기 기준 날짜 환산
			HashMap<String, Object> param = new HashMap<String, Object>();
			String yyyyMm = "";
			param.put("@(S_ACCT_ID)", loginSessionInfoFactory.getObject().getAcctID());
			param.put("@(S_YYYY)"	, yyyy[i]);
			//List<LinkedHashMap<String, Object>> rs = sqlHouseExecuteDao.selectHashMap(267, param);
			//yyyyMm = (String)rs.get(0).get("YYYYMM");
			yyyyMm = yyyy[i]+"12";

			//담당자 사번 조회
			HashMap<String, Object> unid= new HashMap<String, Object>();
			String unId = "";
			unid.put("@(S_ACCT_ID)" , loginSessionInfoFactory.getObject().getAcctID());
			unid.put("@(S_USER_ID)" , loginSessionInfoFactory.getObject().getUserId());
				List<LinkedHashMap<String, Object>> rs1 = sqlHouseExecuteDao.selectHashMap(269, unid);
				unId = (String)rs1.get(0).get("UNID");					
			
		TableObject imfe0039 = new TableObject();
		imfe0039.put("ACCT_ID",                               loginSessionInfoFactory.getObject().getAcctID(), true);
		imfe0039.put("YYYYMM",                             yyyyMm, true);
		imfe0039.put("EVA_GBN",							evaGbn[i], true);
		imfe0039.put("KPI_ID",								 	kpiId[i], true);
		imfe0039.put("OWNER_ID",                             unId);
		imfe0039.put("STATUS",                              status[i]);
		imfe0039.put("UPDATE_EMP",						loginSessionInfoFactory.getObject().getUserId());
		imfe0039.putCurrentDate("UPDATE_DAT");
		
		arrayImfe0039.add(imfe0039);	
		}
		try{
			dbmsDao.updateTable("IMFE0039", arrayImfe0039);
			result = 1;
		}catch(Exception e){
			result = 0;
			logger.error("error:",e);
			throw e;
		}
		return result;
	}	
	public JSONArray getKpiTargetData(String yyyy, String evaGbn, String kpiId) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_YYYY)"					, yyyy); 
		map.put("@(S_EVA_GBN)"					, evaGbn);
		map.put("@(S_KPI_ID)"					, kpiId);
		
		return sqlHouseExecuteDao.selectJSONArray(241, map);
	}
	public JSONArray kpicolValueList(String yyyy, String evaGbn, String kpiId) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_YYYY)"					, yyyy); 
		map.put("@(S_EVA_GBN)"					, evaGbn);
		map.put("@(S_KPI_ID)"					, kpiId);
		
		return sqlHouseExecuteDao.selectJSONArray(245, map);
	}
	public JSONArray kpifileList(String yyyy, String kpiId) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_YYYY)"					, yyyy); 
		map.put("@(S_KPI_ID)"					, kpiId);
		
		return sqlHouseExecuteDao.selectJSONArray(246, map);
	}
	public JSONArray kpiGradeDesc(String yyyy, String evaGbn, String kpiId) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_USER_ID)"				, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(S_YYYY)"					, yyyy); 
		map.put("@(S_EVA_GBN)"					, evaGbn);
		map.put("@(S_KPI_ID)"					, kpiId);
		
		return sqlHouseExecuteDao.selectJSONArray(247, map);
	}
	public JSONArray kpiChargeGradeDesc(String yyyy, String evaGbn, String kpiId) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_USER_ID)"				, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(S_YYYY)"					, yyyy); 
		map.put("@(S_EVA_GBN)"					, evaGbn);
		map.put("@(S_KPI_ID)"					, kpiId);
		
		return sqlHouseExecuteDao.selectJSONArray(281, map);
	}
	public JSONArray makeRadioButton() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());	
		return sqlHouseExecuteDao.selectJSONArray(243, map);
	}
	//지표관리 담당자체크
	public JSONArray kpiadminListSubmitCnt(String yyyy, String evaGbn, String kpiId) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_USER_ID)"				, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(S_YYYY)"					, yyyy); 
		map.put("@(S_EVA_GBN)"					, evaGbn);
		map.put("@(S_KPI_ID)"					, kpiId);
		
		return sqlHouseExecuteDao.selectJSONArray(250, map);
	}
	//지표관리 상태체크
	public JSONArray kpiadminStateSubmitCnt(String yyyymm, String evaGbn, String kpiId) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_YYYYMM)"					, yyyymm); 
		map.put("@(S_EVA_GBN)"					, evaGbn);
		map.put("@(S_KPI_ID)"					, kpiId);
		
		return sqlHouseExecuteDao.selectJSONArray(251, map);
	}
	//평가지표관리 월콤보박스 불러오기
	public JSONArray cboSearchResultMm(String yyyymm, String evaGbn, String gubun) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_YYYYMM)"					, yyyymm); 
		map.put("@(S_EVA_GBN)"					, evaGbn);
		map.put("@(S_GUBUN)"					, gubun);
		
		return sqlHouseExecuteDao.selectJSONArray(253, map);
	}
	//평가지표관리 카운트
	public JSONArray checkKpiValue(String kpiId, String evaGbn, String yyyy) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_USER_ID)"				, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(S_EVA_GBN)",							evaGbn);
		map.put("@(S_KPI_ID)"				, kpiId);
		map.put("@(S_YYYY)"				, yyyy);
		return sqlHouseExecuteDao.selectJSONArray(257, map);
	}
	//평가지표관리 저장or제출
	public int insertkpiValue(String[] yyyy, String[] mm, String[] evaGbn, String[] kpiId,  String[] scId, String[] mtId, String[] grade, String[] gradeDesc,String[] status, String[] ipvDesc) throws Exception{
		int result = 0;
		List<LinkedHashMap<String, Object>> arrayInsertImfe0039_1 = new ArrayList<LinkedHashMap<String,Object>>();
		List<LinkedHashMap<String, Object>> arrayImfe0039_1 = new ArrayList<LinkedHashMap<String,Object>>();
		for(int i=0;i<kpiId.length;i++){
			
		TableObject imfe0039_1 = new TableObject();
		
		//회기 기준 날짜 환산
		HashMap<String, Object> param = new HashMap<String, Object>();
		String yyyyMm = "";
		param.put("@(S_ACCT_ID)", loginSessionInfoFactory.getObject().getAcctID());
		param.put("@(S_YYYY)"	, yyyy[i]);
		//List<LinkedHashMap<String, Object>> rs = sqlHouseExecuteDao.selectHashMap(267, param);
		//yyyyMm = (String)rs.get(0).get("YYYYMM");
		yyyyMm = yyyy[i]+"12";

		//담당자 사번 조회
		HashMap<String, Object> unid= new HashMap<String, Object>();
		String unId = "";
		unid.put("@(S_ACCT_ID)" , loginSessionInfoFactory.getObject().getAcctID());
		unid.put("@(S_USER_ID)" , loginSessionInfoFactory.getObject().getUserId());
			List<LinkedHashMap<String, Object>> rs1 = sqlHouseExecuteDao.selectHashMap(269, unid);
			unId = (String)rs1.get(0).get("UNID");		
			
			imfe0039_1.put("ACCT_ID",                             loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0039_1.put("YYYYMM",                             yyyyMm, true);
			imfe0039_1.put("EP_NO",                             unId, true);
			imfe0039_1.put("EVA_GBN",							evaGbn[i], true);
			imfe0039_1.put("KPI_ID",								 	kpiId[i], true);
			imfe0039_1.put("SC_ID",									scId[i], true);
			imfe0039_1.put("MT_ID",									mtId[i], true);
			imfe0039_1.put("GRADE",								grade[i]);
			imfe0039_1.put("GRADE_DESC",						gradeDesc[i]);
			imfe0039_1.put("IPV_DESC",                           ipvDesc[i]);
			imfe0039_1.put("STATUS",                              status[i]);
			imfe0039_1.put("UPDATE_EMP",						loginSessionInfoFactory.getObject().getUserId());
			imfe0039_1.put("INSERT_EMP",						loginSessionInfoFactory.getObject().getUserId());
			if(checkKpiValue(kpiId[i], evaGbn[i], yyyy[i]).size() == 0){
				imfe0039_1.putCurrentDate("INSERT_DAT");
				arrayInsertImfe0039_1.add(imfe0039_1);
			}else{
				imfe0039_1.putCurrentDate("UPDATE_DAT");
				arrayImfe0039_1.add(imfe0039_1);	
			}
		}
		try{
			dbmsDao.insertTable("IMFE0039_1", arrayInsertImfe0039_1);
			dbmsDao.updateTable("IMFE0039_1", arrayImfe0039_1);
			result = 1;
		}catch(Exception e){
			result = 0;
			logger.error("error:",e);
			throw e;
		}
		return result;
	}
	//평가지표관리:제출취소
	public int updatekpiValue(String[] yyyy, String[] mm, String[] evaGbn, String[] kpiId,String[] status) throws Exception{
		int result = 0;
		List<LinkedHashMap<String, Object>> arrayImfe0039_1 = new ArrayList<LinkedHashMap<String,Object>>();
		for(int i=0;i<kpiId.length;i++){
			//회기 기준 날짜 환산
			HashMap<String, Object> param = new HashMap<String, Object>();
			String yyyyMm = "";
			param.put("@(S_ACCT_ID)", loginSessionInfoFactory.getObject().getAcctID());
			param.put("@(S_YYYY)"	, yyyy[i]);
			//List<LinkedHashMap<String, Object>> rs = sqlHouseExecuteDao.selectHashMap(267, param);
			//yyyyMm = (String)rs.get(0).get("YYYYMM");
			yyyyMm = yyyy[i]+"12";

			//담당자 사번 조회
			HashMap<String, Object> unid= new HashMap<String, Object>();
			String unId = "";
			unid.put("@(S_ACCT_ID)" , loginSessionInfoFactory.getObject().getAcctID());
			unid.put("@(S_USER_ID)" , loginSessionInfoFactory.getObject().getUserId());
				List<LinkedHashMap<String, Object>> rs1 = sqlHouseExecuteDao.selectHashMap(269, unid);
				unId = (String)rs1.get(0).get("UNID");					
				
				TableObject imfe0039_1 = new TableObject();
				imfe0039_1.put("ACCT_ID",                               loginSessionInfoFactory.getObject().getAcctID(), true);
				imfe0039_1.put("YYYYMM",                             yyyyMm, true);
				imfe0039_1.put("EP_NO",                             unId, true);
				imfe0039_1.put("EVA_GBN",							evaGbn[i], true);
				imfe0039_1.put("KPI_ID",								 	kpiId[i], true);
				imfe0039_1.put("STATUS",                              status[i]);
				imfe0039_1.put("UPDATE_EMP",						loginSessionInfoFactory.getObject().getUserId());
				imfe0039_1.putCurrentDate("UPDATE_DAT");

				arrayImfe0039_1.add(imfe0039_1);	
		}
		try{
			dbmsDao.updateTable("IMFE0039_1", arrayImfe0039_1);
			result = 1;
		}catch(Exception e){
			result = 0;
			logger.error("error:",e);
			throw e;
		}
		return result;
	}
	//평가지표관리-주요성과,개선사항
	public JSONArray kpiEvalResultGradeDesc(String yyyy, String evaGbn, String kpiId) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_USER_ID)"				, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(S_YYYY)"					, yyyy); 
		map.put("@(S_EVA_GBN)"					, evaGbn);
		map.put("@(S_KPI_ID)"					, kpiId);
		
		return sqlHouseExecuteDao.selectJSONArray(258, map);
	}
	public JSONArray kpiEvalGradeDesc(String yyyy, String evaGbn, String kpiId) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_USER_ID)"				, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(S_YYYY)"					, yyyy); 
		map.put("@(S_EVA_GBN)"					, evaGbn);
		map.put("@(S_KPI_ID)"					, kpiId);
		
		return sqlHouseExecuteDao.selectJSONArray(282, map);
	}
	//평가지표관리 버튼체크
	public JSONArray kpiEvalStateSubmitCnt(String yyyymm, String evaGbn, String kpiId) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_YYYYMM)"					, yyyymm); 
		map.put("@(S_EVA_GBN)"					, evaGbn);
		map.put("@(S_KPI_ID)"					, kpiId);
		
		return sqlHouseExecuteDao.selectJSONArray(260, map);
	}

	public List<LinkedHashMap<String, Object>> getFileInfo(String fileid,String kpiId,String yyyy) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_FILE_ID)"				, fileid);
		map.put("@(S_KPI_ID)"					, kpiId);
		map.put("@(S_YYYY)"					, yyyy); 
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		List<LinkedHashMap<String, Object>> fileList = this.sqlHouseExecuteDao.selectHashMap(261, map);
		return fileList;
	}
	
	public int evalResultSave(String scIdResult, String yyyyResult, String kpiIdResult, String evaGbnResult, String mtIdResult, String gradeDesc, String ipvDesc, String grade) throws Exception{
		int result = 0;
		List<LinkedHashMap<String, Object>> arrayImfe0039_1 = new ArrayList<LinkedHashMap<String,Object>>();
		for(int i=0;i<kpiIdResult.length();i++){
			
		TableObject imfe0039_1 = new TableObject();
		
		//회기 기준 날짜 환산
		HashMap<String, Object> param = new HashMap<String, Object>();
		String yyyyMm = "";
		param.put("@(S_ACCT_ID)", loginSessionInfoFactory.getObject().getAcctID());
		param.put("@(S_YYYY)"	, yyyyResult);
		yyyyMm =yyyyResult+"12";

			
			imfe0039_1.put("ACCT_ID",                             loginSessionInfoFactory.getObject().getAcctID(), true);
			imfe0039_1.put("YYYYMM",                             yyyyMm, true);
			imfe0039_1.put("EVA_GBN",							evaGbnResult, true);
			imfe0039_1.put("KPI_ID",								 	kpiIdResult, true);
			imfe0039_1.put("SC_ID",									scIdResult);
			imfe0039_1.put("MT_ID",									mtIdResult);
			imfe0039_1.put("GRADE",								grade);
			imfe0039_1.put("GRADE_DESC",						gradeDesc);
			imfe0039_1.put("IPV_DESC",                           ipvDesc);
			imfe0039_1.put("UPDATE_EMP",						loginSessionInfoFactory.getObject().getUserId());
			imfe0039_1.put("INSERT_EMP",						loginSessionInfoFactory.getObject().getUserId());
			imfe0039_1.putCurrentDate("UPDATE_DAT");
			arrayImfe0039_1.add(imfe0039_1);	
		}
		try{
			dbmsDao.updateTable("IMFE0039_1", arrayImfe0039_1);
			result = 1;
		}catch(Exception e){
			result = 0;
			logger.error("error:",e);
			throw e;
		}
		return result;
	}

}
