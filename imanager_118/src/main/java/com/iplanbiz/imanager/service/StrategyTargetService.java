package com.iplanbiz.imanager.service;

import java.util.HashMap;

import javax.annotation.Resource;
 
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO; 
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class StrategyTargetService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	public JSONArray selectStrategyTarget(String searchyn, String searchstranm) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(USE_YN)"					, searchyn);
		map.put("@(STRA_NM)"				, searchstranm);
		
		return sqlHouseExcuteDao.selectJSONArray(29, map);
	}
	public JSONArray checkStraId100() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		return sqlHouseExcuteDao.selectJSONArray(32, map);
	}
	public void insertStrategtTarget(String straid, String straCD,String stranm, String stradef, String useyn, String sortorder) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		TableObject imfe0004 = new TableObject();
		
		imfe0004.put("ACCT_ID"		, loginSessionInfoFactory.getObject().getAcctID(), true);		
		
		imfe0004.put("INSERT_EMP"	, loginSessionInfoFactory.getObject().getUserId());
		imfe0004.put("UPDATE_EMP"	, loginSessionInfoFactory.getObject().getUserId());
		imfe0004.putCurrentDate("INSERT_DAT");
		imfe0004.putCurrentDate("UPDATE_DAT");		
		
		
		String[] arrayClob = null;
		JSONObject checkStraId = null;
		checkStraId = (JSONObject) checkStraId100().get(0);
		if(checkStraId.get("CNT").equals("0")){
			
			imfe0004.put("STRA_ID" 		, "100", true);
			imfe0004.put("STRA_CD"		, "100");
			imfe0004.put("STRA_NM"		, "전략목표 없음");
			imfe0004.put("STRA_DEF"		, "전략목표 없음");
			imfe0004.put("USE_YN"		, "Y");
			imfe0004.put("SORT_ORDER"	, 99999);
			dbmsDao.insertTable("IMFE0004",imfe0004);
		}
		
		imfe0004.put("STRA_ID" 		, straid, true);
		imfe0004.put("STRA_CD"		, straCD);
		imfe0004.put("STRA_NM"		, stranm);
		imfe0004.put("STRA_DEF"		, stradef);
		imfe0004.put("USE_YN"		, useyn);
		imfe0004.put("SORT_ORDER"	, sortorder);
		
		try{
			if(straid.equals("")){ 
				dbmsDao.insertTable("IMFE0004","STRA_ID" ,imfe0004.getKeyMap(),imfe0004);
				
			}else{
				dbmsDao.updateTable("IMFE0004",imfe0004.getKeyMap(),imfe0004);
			}
		}catch (Exception e) {			
			e.printStackTrace();
			throw e;
		}		
	}
	public void removeStrategyTarget(String straid) throws Exception{
		
		//Validation 시작
		TableObject tableUsedStraID = new TableObject();
		tableUsedStraID.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID(),true);
		tableUsedStraID.put("STRA_ID", straid,true);
		if(dbmsDao.countTable("IMFE0009", tableUsedStraID)>0) throw new RuntimeException("해당 코드에 매핑된 지표가 있어 삭제 할 수 없습니다.");
		if(dbmsDao.countTable("IMFE0005", tableUsedStraID)>0) throw new RuntimeException("해당 코드에 매핑된 세부전략이 있어 삭제 할 수 없습니다.");
		if(dbmsDao.countTable("IMFE0006", tableUsedStraID)>0) throw new RuntimeException("해당 코드에 매핑된 CSF가 있어 삭제 할 수 없습니다.");
		//Validation 끝
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(STRA_ID)"				, straid);
		String[] arrayClob = null;
		try{
			sqlHouseExcuteDao.crudQuery(36, map, arrayClob);
			
		}catch(Exception e){
			
			e.printStackTrace();
			throw e;
		}		
	}
	public JSONArray checkStrategtTarget(String straid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(STRA_ID)"				, straid);
		
		return sqlHouseExcuteDao.selectJSONArray(67, map);
	}
}
