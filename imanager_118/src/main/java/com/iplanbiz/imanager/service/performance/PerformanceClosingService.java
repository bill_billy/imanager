package com.iplanbiz.imanager.service.performance;

import java.util.HashMap;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO; 
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class PerformanceClosingService {
	
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	public int insertproject(String msid, String kpiid, String kpinm, String kpigbn, String unit, String kpitype, String kpidir, String useyn, String kpidesc, String subkpiid, String subkpinm, String weight, String sortorder, String ac, String newkpiid, String newsubkpiid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		int result = 0;
		String[] clobArray = null;
		
		map.put("@(MS_ID)",				msid);
		map.put("@(KPI_ID)",					kpiid);
		map.put("@(KPI_DIR)",					kpidir);
		map.put("@(UNIT)",				unit);
		map.put("@(USE_YN)",				useyn);
		map.put("@(INSERT_EMP)",				loginSessionInfoFactory.getObject().getUserId());
		map.put("@(UPDATE_EMP)",				loginSessionInfoFactory.getObject().getUserId());
		
		
		
		try{
			if(ac!=null) {
				map.put("@(SUB_KPI_ID)",				subkpiid);
				map.put("@(SUB_KPI_NM)",					subkpinm.replaceAll("'", "''"));
				map.put("@(WEIGHT)",					weight);
				map.put("@(SORT_ORDER)",					sortorder);
				if(subkpiid==""){
					map.put("@(SUB_KPI_ID)",				newsubkpiid);
					sqlHouseExcuteDao.crudQuery(462, map, clobArray);
					result = 1;
				}else{
					sqlHouseExcuteDao.crudQuery(463, map, clobArray);
					result = 2;
				}	
			}else {
				map.put("@(KPI_NM)",					kpinm.replaceAll("'", "''"));
				map.put("@(KPI_GBN)",					kpigbn);
				map.put("@(KPI_DESC)",				kpidesc.replaceAll("'", "''"));
				map.put("@(KPI_TYPE)",						kpitype);
			
			if(kpiid==""){
				map.put("@(KPI_ID)",					newkpiid);
				sqlHouseExcuteDao.crudQuery(459, map, clobArray);
				result = 1;
			}else{
				sqlHouseExcuteDao.crudQuery(460, map, clobArray);
				result = 2;
			}
			}
		}catch(Exception e){
			result = 3;
			e.printStackTrace(); 
			throw e;
		}
		
		return result;
	}
	public int deleteproject(String kpiid, String subkpiid, String msid)  throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		int result = 0;
		map.put("@(MS_ID)",					msid);
		map.put("@(KPI_ID)",					kpiid);
		
		String[] clobArray = null;
		try{
			if(subkpiid==null) {
				sqlHouseExcuteDao.crudQuery(461, map, clobArray);
			}
			else {
				map.put("@(SUB_KPI_ID)",					subkpiid);
				sqlHouseExcuteDao.crudQuery(464, map, clobArray);
			}
			
			result = 1;
		}catch(Exception e){
			result = 2;
			e.printStackTrace();
		}
		return result;
	}

	public int saveClosing(String ac, String ms_id, String yyyy, String step) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		int result = 0;
		String[] clobArray = null;
		
		map.put("@(MS_ID)",				ms_id);
		map.put("@(YYYY)",			yyyy);
		map.put("@(CLOSING_STEP)",			step);
		
		try{
			if(ac.equals("insert")) {
				map.put("@(INSERT_EMP)",				loginSessionInfoFactory.getObject().getUserId());
				sqlHouseExcuteDao.crudQuery(471, map, clobArray);
			}else {
				map.put("@(UPDATE_EMP)",				loginSessionInfoFactory.getObject().getUserId());
				sqlHouseExcuteDao.crudQuery(472, map, clobArray);
			}
			result = 1;
		}catch(Exception e){
			result = 0;
			throw e;
		}
		return result;
	}
	
	public void calculateKpiValue(String ms_id, String yyyy) throws Exception {
		String whereSubkpi = "AND SUB_KPI_ID=99999999";
		JSONArray targetList = getTargetList(ms_id,yyyy,whereSubkpi);
		if(targetList.size()>0) {
			for(int i=0;i<targetList.size();i++) {
				JSONObject IDPFE0103 = (JSONObject)targetList.get(i);
				String kpiId = IDPFE0103.get("KPI_ID").toString();
				
				float sumKpiValue = calculateSubkpi(ms_id,yyyy,kpiId);
				
				
				System.out.println("성과지표의 실적: "+sumKpiValue);
				
				updateKpivalue(ms_id,yyyy,kpiId,sumKpiValue);
				
				
			}
			
			
		}else {
			
			System.out.println("집계할 데이터없음");
		}
		
	}

	private int updateKpivalue(String ms_id, String yyyy, String kpiId, float sumKpiValue) {
		int result = 0;
		String[] clobArray = {};
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(YYYY)"					, yyyy);
		map.put("@(KPI_ID)"					, kpiId);
		map.put("@(KPI_VAL)"				, sumKpiValue);
		
		try {
			sqlHouseExcuteDao.crudQuery(494, map, clobArray);
			result=1;
		}catch(Exception e) {
			result=0;
		}
		return result;
	}
	public JSONArray getTargetList(String ms_id, String yyyy, String subkpi) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(YYYY)"					, yyyy);
		map.put("@(AND_SUBKPI)"				, subkpi);
		System.out.println(map);
		return sqlHouseExcuteDao.selectJSONArray(492,map);
	}
	
	public JSONArray getMaxValueAndWeight(String ms_id, String kpiid, String subkpiid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"					, ms_id);
		map.put("@(KPI_ID)"					, kpiid);
		map.put("@(SUB_KPI_ID)"				, subkpiid);
		return sqlHouseExcuteDao.selectJSONArray(493,map);
	}
	
	public float calculateSubkpi(String ms_id,String yyyy, String kpiid) throws Exception {
		String kpivalue,maxGoal,weight;
		float sumValue = 0;
		String whereSubkpi = "AND SUB_KPI_ID!=99999999 AND KPI_ID="+kpiid;
		JSONArray targetList = getTargetList(ms_id,yyyy,whereSubkpi);
		if(targetList.size()>0) {
			for(int i=0;i<targetList.size();i++) {
				
				JSONObject IDPFE0103 = (JSONObject)targetList.get(i);
				
				String subkpiId = IDPFE0103.get("SUB_KPI_ID").toString();
				if(IDPFE0103.get("KPI_VAL")!=null)
					kpivalue = IDPFE0103.get("KPI_VAL").toString();
				else
					kpivalue = "";
				
				JSONArray maxAndweight = getMaxValueAndWeight(ms_id,kpiid,subkpiId);
				if(maxAndweight.size()==0) continue;
				
				JSONObject value = (JSONObject)maxAndweight.get(0);
				if(value.get("GOAL")!=null)
					maxGoal = value.get("GOAL").toString();
				else
					maxGoal = "";
				
				if(value.get("WEIGHT")!=null)
					weight = value.get("WEIGHT").toString();
				else
					weight = "";
				if(kpivalue.equals("")||maxGoal.equals("")||weight.equals(""))
					continue;
				
				System.out.println(kpivalue+"/"+maxGoal+"/"+weight);
				
				sumValue += (Float.parseFloat(kpivalue)/ Float.parseFloat(maxGoal))*Float.parseFloat(weight);
				System.out.println(sumValue);
			}
		}
		
		//update 
		return sumValue;
	}
	
}
