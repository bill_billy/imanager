package com.iplanbiz.imanager.service.eval;

import java.util.HashMap;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;
import com.iplanbiz.imanager.dto.CommitteeEval;

@Service
@Transactional(readOnly=true)
public class CommitteeEvalService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	@Autowired
	DBMSDAO dbmsDao;

	

	public int insert(CommitteeEval committee) throws Exception {
		int result = 0;
		
		TableObject idpfe0103 = new TableObject(); 
		
		idpfe0103.put("MS_ID"				, committee.getMs_id(),true);
		idpfe0103.put("ASSIGN_ID"			, committee.getAssign_id(),true);
		idpfe0103.put("YYYY"				, committee.getYyyy(),true);
		idpfe0103.put("EMP_ID"				, committee.getEmp_id(),true);
		idpfe0103.put("GOOD_CONTENT"		, committee.getGood_content());
		idpfe0103.put("IMPROVE_CONTENT"		, committee.getImprove_content());
		idpfe0103.put("EVAL_RESULT"			, committee.getEval_result());
		idpfe0103.put("EVAL_STATUS"			, "B"); //평가중
		
		idpfe0103.putCurrentDate("INSERT_DAT");
		idpfe0103.put("INSERT_EMP"			, loginSessionInfoFactory.getObject().getUserId()); 
		idpfe0103.putCurrentDate("UPDATE_DAT");
		idpfe0103.put("UPDATE_EMP"			, loginSessionInfoFactory.getObject().getUserId()); 
		try{
			if(committee.getAc().equals("insert")){
				//IMFE0009 INSERT 
				dbmsDao.insertTable("IDPFE0103", idpfe0103);
				result = 1;
			}else if(committee.getAc().equals("update")){
				//IMFE0009 UPDATE 
				dbmsDao.updateTable("IDPFE0103", idpfe0103.getKeyMap(), idpfe0103);
				result = 1;
			}
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		result = insertProgramEval(committee);
		return result;
	}
	
	private JSONArray checkProgramEval(String ms_id,String program_id,String yyyy) throws Exception {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(MS_ID)"						, ms_id);
		map.put("@(PROGRAM_ID)"					, program_id);
		map.put("@(YYYY)"						, yyyy);
		return sqlHouseExcuteDao.selectJSONArray(430, map);
	}
	public int insertProgramEval(CommitteeEval committee) throws Exception {
		int result = 1;
		
		TableObject idpfe0104 = new TableObject(); 
		
		
		if(committee.getProgram_id()!=null) {
			for(int i=0;i<committee.getProgram_id().length;i++) {
				System.out.println(checkProgramEval(committee.getMs_id(),committee.getProgram_id()[i], committee.getYyyy()));
				idpfe0104.put("MS_ID"				, committee.getMs_id(),true);
				idpfe0104.put("PROGRAM_ID"			, committee.getProgram_id()[i],true);
				idpfe0104.put("YYYY"				, committee.getYyyy(),true);
				idpfe0104.put("EMP_ID"				, committee.getEmp_id(),true);
				idpfe0104.put("GOOD_CONTENT"		, committee.getP_good_content()[i]);
				idpfe0104.put("IMPROVE_CONTENT"		, committee.getP_improve_content()[i]);
				idpfe0104.put("BEST_CONTENT"		, committee.getP_best_content()[i]);
				idpfe0104.put("EVAL_SCORE"			, committee.getP_eval_score()[i]); 
		
				idpfe0104.putCurrentDate("INSERT_DAT");
				idpfe0104.put("INSERT_EMP"			, loginSessionInfoFactory.getObject().getUserId()); 
				idpfe0104.putCurrentDate("UPDATE_DAT");
				idpfe0104.put("UPDATE_EMP"			, loginSessionInfoFactory.getObject().getUserId()); 
				try{
					if(checkProgramEval(committee.getMs_id(),committee.getProgram_id()[i], committee.getYyyy()).size()==0){
						//IMFE0009 INSERT 
						dbmsDao.insertTable("IDPFE0104", idpfe0104);
					}else{
						//IMFE0009 UPDATE 
						dbmsDao.updateTable("IDPFE0104", idpfe0104.getKeyMap(), idpfe0104);
					}
				}catch(Exception e){
					logger.error("error:",e);
					result = 0;
					throw e;
				} 
		
			}
		}
		return result;
	}

	public int confirm(String ms_id, String assign_id, String yyyy, String status) throws Exception {
		int result = 0;
		TableObject idpfe0103 = new TableObject(); 
		
		idpfe0103.put("MS_ID"				, ms_id,true);
		idpfe0103.put("ASSIGN_ID"			, assign_id,true);
		idpfe0103.put("YYYY"				, yyyy,true);
		if(status.equals("confirm"))
			idpfe0103.put("EVAL_STATUS"			, "C"); //평가완료
		else if(status.equals("cancle"))
			idpfe0103.put("EVAL_STATUS"			, "B"); //평가중
		try{
			dbmsDao.updateTable("IDPFE0103", idpfe0103.getKeyMap(), idpfe0103);
			if(status.equals("confirm"))
				result = 1;
			else if(status.equals("cancle"))
				result = 2;
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		return result;
	}
	
}
