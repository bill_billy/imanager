package com.iplanbiz.imanager.service.assign;

import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO; 
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class EvalManagerService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	public JSONArray selectStrategyTarget(String msId, String yyyy) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>(); 
		map.put("@(MS_ID)"				, msId); 
		map.put("@(YYYY)"				, yyyy);       
		return sqlHouseExcuteDao.selectJSONArray(328, map);
	}
	
	public void insertStrategtTarget(String msId, String assignId, String YYYY, String[] deptCd, String[] deptNm, String[] empId, String[] empNm, String[] masterYn) throws Exception{
		
		
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		
		int result = 0; 
		
		TableObject deleteidpfe102 = new TableObject();
		deleteidpfe102.put("MS_ID"		, msId, true);	  
		deleteidpfe102.put("ASSIGN_ID"		, assignId, true);		 
		deleteidpfe102.put("YYYY"		, YYYY, true);   
		System.out.println("delete:" + msId + " " + assignId + " " + YYYY); 
		 
		try {
			dbmsDao.deleteTable("IDPFE0102", deleteidpfe102); 
			result = 1;
		}catch(Exception e){
			logger.error("error:",e);
			result = 0;
			throw e;
		} 
		if(result==0) return;  
		
		TableObject idpfe0102 = new TableObject(); 
	
		 
		idpfe0102.put("MS_ID"		, msId, true);	  
		idpfe0102.put("ASSIGN_ID"		, assignId, true);		 
		idpfe0102.put("YYYY"		, YYYY, true);      
		idpfe0102.put("INSERT_EMP"	, loginSessionInfoFactory.getObject().getUserId());
		idpfe0102.put("UPDATE_EMP"	, loginSessionInfoFactory.getObject().getUserId());
		idpfe0102.putCurrentDate("INSERT_DAT");
		idpfe0102.putCurrentDate("UPDATE_DAT");		
		
		
		String[] arrayClob = null; 
		 
		System.out.println(deptCd.length);
		System.out.println(deptCd[0]); 
		try{	 
			for(int i=0; i < deptCd.length; i++) {
				if(deptCd[i].equals(""))
					continue;
				idpfe0102.put("DEPT_CD", deptCd[i]);  
				idpfe0102.put("DEPT_NM", deptNm[i]);  
				idpfe0102.put("EMP_ID",  empId[i]);    
				idpfe0102.put("EMP_NM",  empNm[i]);  
				idpfe0102.put("MASTER_YN" ,masterYn[i]);
				
				System.out.println(idpfe0102);
				dbmsDao.insertTable("IDPFE0102", idpfe0102);

			}
			
		}catch (Exception e) {			
			e.printStackTrace(); 
			throw e;
		}
	}
}
