package com.iplanbiz.imanager.service.evalResult;

	import java.util.HashMap;

	import javax.annotation.Resource;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;

	import org.json.simple.JSONArray;
	import org.json.simple.JSONObject;
	import org.slf4j.Logger;
	import org.slf4j.LoggerFactory;
	import org.springframework.beans.factory.ObjectFactory;
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.stereotype.Service;
	import org.springframework.transaction.annotation.Transactional;
	import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.bind.annotation.RequestMethod;
	import org.springframework.web.bind.annotation.RequestParam;

	import com.iplanbiz.core.io.dbms.DBMSDAO;
	import com.iplanbiz.core.io.dbms.TableObject;
	import com.iplanbiz.core.message.AjaxMessage;
	import com.iplanbiz.core.session.LoginSessionInfo;
	import com.iplanbiz.imanager.dao.BasicDAO; 
	import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

	@Service
	@Transactional(readOnly=true)
	public class CommitteeEvalResultService {
		
		private Logger logger = LoggerFactory.getLogger(getClass());
		@Resource(name="loginSessionInfoFactory")
		ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
		
		@Autowired
		BasicDAO basicDao; 
		
		@Autowired
		SqlHouseExecuteDAO sqlHouseExcuteDao;
		
		@Autowired
		DBMSDAO dbmsDao;

}
