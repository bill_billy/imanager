package com.iplanbiz.imanager.service.program;

import java.util.HashMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO; 
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class ProgramEvalConfirmService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	public void insertStrategtTarget1(String ms_id, String yy_yy, String assign_id, String program_id) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		TableObject idpfe0200 = new TableObject();
		
		idpfe0200.put("MS_ID"				,ms_id,true);
		idpfe0200.put("YYYY"				,yy_yy,true);
		idpfe0200.put("ASSIGN_ID"				,assign_id,true);
		idpfe0200.put("PROGRAM_ID"				,program_id,true);


		idpfe0200.put("INSERT_EMP"	, loginSessionInfoFactory.getObject().getUserId());
		idpfe0200.put("UPDATE_EMP"	, loginSessionInfoFactory.getObject().getUserId());
		idpfe0200.putCurrentDate("INSERT_DAT");
		idpfe0200.putCurrentDate("UPDATE_DAT");		
		
		String[] arrayClob = null;
		
		idpfe0200.put("PROGRAM_STATUS" 	, "C");
		
			try{	 
				dbmsDao.updateTable("IDPFE0200",idpfe0200.getKeyMap(),idpfe0200); 
				
		}catch (Exception e) {			
			e.printStackTrace();
			throw e;
		}		
	}
	
	public void insertStrategtTarget2(String ms_id, String yy_yy, String assign_id, String program_id) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		TableObject idpfe0200 = new TableObject();
		
		idpfe0200.put("MS_ID"				,ms_id,true);
		idpfe0200.put("YYYY"				,yy_yy,true);
		idpfe0200.put("ASSIGN_ID"				,assign_id,true);
		idpfe0200.put("PROGRAM_ID"				,program_id,true);


		idpfe0200.put("INSERT_EMP"	, loginSessionInfoFactory.getObject().getUserId());
		idpfe0200.put("UPDATE_EMP"	, loginSessionInfoFactory.getObject().getUserId());
		idpfe0200.putCurrentDate("INSERT_DAT");
		idpfe0200.putCurrentDate("UPDATE_DAT");		
		
		String[] arrayClob = null;
		
		idpfe0200.put("PROGRAM_STATUS" 	, "B");
		
			try{	 
				dbmsDao.updateTable("IDPFE0200",idpfe0200.getKeyMap(),idpfe0200); 
				
		}catch (Exception e) {			
			e.printStackTrace();
			throw e;
		}		
	}
}
