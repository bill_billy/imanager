package com.iplanbiz.imanager.service;

import java.util.HashMap;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO; 
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class KpiFormulaService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	public JSONArray selectKpiFormula(String useYn, String calNm) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(USE_YN)"						, useYn);
		map.put("@(CAL_NM)"						, calNm); 
		
		return sqlHouseExcuteDao.selectJSONArray(14, map);
	}
	public JSONArray check100() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		return sqlHouseExcuteDao.selectJSONArray(17, map);
	}
	public int insertKpiFormula(String sCalCod, String sCalNm, String sCalUdc1, String sUseYn) throws Exception{
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		logger.debug("PARAM = " + sCalCod + "//"+sCalNm+"//"+sCalUdc1+"//"+sUseYn);
		map.put("@(ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(CAL_COD)"					, sCalCod);
		map.put("@(CAL_NM)"						, sCalNm);
		map.put("@(CAL_UDC1)"					, sCalUdc1);
		map.put("@(USE_YN)"						, sUseYn);
		map.put("@(INSERT_EMP)"					, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(UPDATE_EMP)"					, loginSessionInfoFactory.getObject().getUserId());
		String[] clobArray = null;
		try{
			if(sCalCod == ""){
				if(check100().size() > 0){
					sqlHouseExcuteDao.crudQuery(18, map, clobArray);
					result = 2;
				}else{
					try{
						sqlHouseExcuteDao.crudQuery(129, map, clobArray);
						sqlHouseExcuteDao.crudQuery(18, map, clobArray);
						result = 2;
					} catch(Exception e1){
						e1.printStackTrace();
						result = 1;
					}
				}
			}else{
				sqlHouseExcuteDao.crudQuery(19, map, clobArray);
				result = 3;
			}
		} catch(Exception e){
			e.printStackTrace();
			result = 4;
			throw e;
		}
		return result;
	}
	public int removeKpiFormula(String calcod)  throws Exception{
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(CAL_COD)"				, calcod);
		
		String[] clobArray = null;
		try{
			sqlHouseExcuteDao.crudQuery(20, map, clobArray);
			result = 1;
		}catch(Exception e){
			e.printStackTrace();
			result = 2;
			throw e;
		}
		return result;
	}
	public JSONArray checkKpiFormula(String calcod) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(CAL_COD)"				, calcod);
		
		return sqlHouseExcuteDao.selectJSONArray(65, map);
	}
}
