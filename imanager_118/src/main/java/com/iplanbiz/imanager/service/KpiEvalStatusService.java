package com.iplanbiz.imanager.service;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.io.file.FileService;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.config.WebConfig;
import com.iplanbiz.imanager.dao.BasicDAO;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;
import com.iplanbiz.imanager.dto.KpiAttachFile;

@Service
@Transactional(readOnly=true)
public class KpiEvalStatusService {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExecuteDao;
	
}
