package com.iplanbiz.imanager.service;

import java.util.HashMap;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO; 
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class CodeService {
	
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;

	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlhouseExcuteDao;
	
	@Transactional
	public int insertCode(String sComlCod, String orgComCod, String sComCod, String sComNm, String sUseYn, String sSortOrder, String sBigo) throws Exception{
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(COML_COD)"					, sComlCod);
		map.put("@(ORG_COD)"				    , orgComCod);
		map.put("@(COM_COD)"					, sComCod);
		map.put("@(COM_NM)"						, sComNm);
		map.put("@(USE_YN)"						, sUseYn);
		map.put("@(SORT_ORDER)"					, sSortOrder);
		map.put("@(BIGO)"						, sBigo.replaceAll("'", "''"));
		map.put("@(INSERT_EMP)"					, loginSessionInfoFactory.getObject().getUserId());
		map.put("@(UPDATE_EMP)"					, loginSessionInfoFactory.getObject().getUserId());
		
		String[] clobArray = null;
		try{
			if(orgComCod.equals("")){
				sqlhouseExcuteDao.crudQuery(10, map, clobArray);
				result = 1;
			}else{
				sqlhouseExcuteDao.crudQuery(11, map, clobArray);
				result = 2;
			}
		}catch(Exception e){
			result = 3;			
			e.printStackTrace();
			throw e;
		}
		return result;
	}
	public int removeCode(String sComlCod, String sComCod) throws Exception{
		int result = 0;
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(COML_COD)"				, sComlCod);
		map.put("@(COM_COD)"				, sComCod);
		
		String[] clobArray = null;
		try{
			sqlhouseExcuteDao.crudQuery(12, map, clobArray);
		}catch(Exception e){
			result = 2;
			e.printStackTrace();
			throw e;
		}
		return result;
	}
		//공통코드에서 코드명으로 조회 해서 LIST 불러오기 ASC 방식 
	public JSONArray getCodeListASC(String comlcod) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(COML_COD)"				, comlcod);
		
		return sqlhouseExcuteDao.selectJSONArray(13, map);
	}
	//공통코드에서 코드명으로 조회 해서 LIST 불러오기 DESC 방식
	public JSONArray getCodeListDESC(String comlcod) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_COML_COD"				, comlcod);
		
		return sqlhouseExcuteDao.selectJSONArray(150, map);
	}
}
