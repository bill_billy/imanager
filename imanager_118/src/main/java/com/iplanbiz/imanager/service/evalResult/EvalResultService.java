package com.iplanbiz.imanager.service.evalResult;

import java.util.HashMap;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO; 
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class EvalResultService {
	
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	public int insertGroupCode(String orgComlCod, String sComlCod, String sComlNm, String sUseYn, String sSortOrder, String sBigo) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		int result = 0;
		map.put("@(ORG_COML_COD)",				orgComlCod);
		map.put("@(COML_COD)",					sComlCod);
		map.put("@(COML_NM)",					sComlNm);
		map.put("@(USE_YN)",					sUseYn);
		map.put("@(SORT_ORDER)",				sSortOrder);
		map.put("@(BIGO)",						sBigo.replaceAll("'", "''"));
		map.put("@(INSERT_EMP)",				loginSessionInfoFactory.getObject().getUserId());
		map.put("@(UPDATE_EMP)",				loginSessionInfoFactory.getObject().getUserId());
		
		String[] clobArray = null;
		try{
			if(orgComlCod == ""){
				sqlHouseExcuteDao.crudQuery(439, map, clobArray);
				result = 1;
			}else{
				sqlHouseExcuteDao.crudQuery(440, map, clobArray);
				result = 2;
			}
		}catch(Exception e){
			result = 3;
			e.printStackTrace(); 
			throw e;
		}
		
		return result;
	}
	public int deleteGroupCode(String dComlCod)  throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		int result = 0;
		map.put("@(COML_COD)",					dComlCod);
		String[] clobArray = null;
		try{
			sqlHouseExcuteDao.crudQuery(441, map, clobArray);
			result = 1;
		}catch(Exception e){
			result = 2;
			e.printStackTrace();
		}
		return result;
	}
	public JSONArray checkStrategtTarget(String msid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
//		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(MS_ID)"				, msid);
		 
		return sqlHouseExcuteDao.selectJSONArray(318, map);
	}

}
