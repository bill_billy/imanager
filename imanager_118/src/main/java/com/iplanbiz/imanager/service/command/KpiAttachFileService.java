package com.iplanbiz.imanager.service.command;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.io.file.FileService;
import com.iplanbiz.core.message.AjaxMessage;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.config.WebConfig;
import com.iplanbiz.imanager.dao.BasicDAO;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;
import com.iplanbiz.imanager.dto.KpiAttachFile;
import com.iplanbiz.imanager.service.KpiService;

@Service
@Transactional(readOnly=true)
public class KpiAttachFileService {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExecuteDao;
	
	public LinkedHashMap<String, Object> getEvaGbnInfo(String evagbn) throws Exception{
		HashMap<String,Object> param = new HashMap<String,Object>();
		if(evagbn == null) evagbn = "";
		param.put("@(S_ACCT_ID)"		, loginSessionInfoFactory.getObject().getAcctID());
		param.put("@(S_EVA_GBN)"		, evagbn);
		List<LinkedHashMap<String, Object>> result = sqlHouseExecuteDao.selectHashMap(171, param);
		return result.get(0);
	}
	
	public JSONArray getYearForEvaluating(String evagbn) throws Exception{
		HashMap<String,Object> param = new HashMap<String,Object>();
		param.put("@(S_ACCT_ID)"		, loginSessionInfoFactory.getObject().getAcctID());
		param.put("@(S_EVA_GBN)"		, evagbn);
		JSONArray result = sqlHouseExecuteDao.selectJSONArray(172, param);
		return result;
	}
	
	public JSONArray getMonthForEvaluating(String evagbn, String year) throws Exception{
		HashMap<String,Object> param = new HashMap<String,Object>();
		param.put("@(S_ACCT_ID)"		, loginSessionInfoFactory.getObject().getAcctID());
		param.put("@(S_EVA_GBN)"		, evagbn);
		param.put("@(S_YYYY)"			, year);
		JSONArray result = sqlHouseExecuteDao.selectJSONArray(173, param);
		return result;
	}
	
	/**
	 * 지표별 증빙자료 파일 삭제.
	 * @param year
	 * @param month
	 * @param scid
	 * @param kpiid
	 * @param fileid
	 * @throws Exception
	 */
	public void removeKpiAttachFile(String year, String month, String scid,
			String kpiid, String fileid, String filepath, String fileSavedName, String fileExtName) throws Exception{

		TableObject imfe9001 = new TableObject();
		imfe9001.put("ACCT_ID"	, loginSessionInfoFactory.getObject().getAcctID()		, true);
		imfe9001.put("FILE_ID"	, fileid												, true);
		
		dbmsDao.deleteTable("IMFE9001", imfe9001);
		//실제 파일 삭제
		java.io.File realFile = new java.io.File(filepath + fileSavedName+"."+fileExtName);
		realFile.delete();
		//FileService file = new FileService(WebConfig.getProperty("system.upload.SaveDir"),Long.valueOf(WebConfig.getProperty("system.maxUploadSize").toString()));		
	}
	public void uploadKpiAttachFile(KpiAttachFile kpiAttachFile) throws Exception{

		//실제 파일 업로드
		String savedFileName = "";
		 
		FileService fileService = new FileService(WebConfig.getProperty("system.upload.SaveDir"),Long.valueOf(WebConfig.getProperty("system.upload.maxUploadSize").toString()));
		String[] enableExtetionList = new String[]{"xls","xlsx","zip","pdf","hwp","doc","docx","png","jpg","txt","gif","ppt","pptx"};
		if(kpiAttachFile.getAttachFile()!=null){
			ArrayList<LinkedHashMap<String,Object>> arrImfe9001 = new ArrayList<LinkedHashMap<String, Object>>();
			
			//Progress Init
			loginSessionInfoFactory.getObject().initProgress("FILEUPLOAD", kpiAttachFile.getAttachFile().length);

			try{
				int i = 0;
				for(CommonsMultipartFile file : kpiAttachFile.getAttachFile()){
					//String originalFileName = new String(file.getOriginalFilename().getBytes("8859_1"),"utf-8");
					
					//Progress 상태가 Valid하지 않다면(오류가 중간에 난적이 있다면) 작업 중단.
					if(loginSessionInfoFactory.getObject().isValidProgress("FILEUPLOAD")==false){
						throw new RuntimeException("작업이 취소 되었습니다.");
					}
					String originalFileName = file.getOriginalFilename();
					savedFileName = fileService.setFileName();
					
					fileService.saveFile(file, enableExtetionList, savedFileName);		 
					
					TableObject imfe9001 = new TableObject();
					imfe9001.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID(),true);
					imfe9001.put("YYYY", kpiAttachFile.getYear());
					imfe9001.put("MM", kpiAttachFile.getMonth());
					imfe9001.put("SC_ID", kpiAttachFile.getScid());
					imfe9001.put("KPI_ID", kpiAttachFile.getKpiid());			
					imfe9001.put("FILE_SAVE_NAME", savedFileName);
					imfe9001.put("FILE_ORG_NAME", originalFileName);
					imfe9001.put("FILE_PATH", fileService.getUpLoadPath()); 
					imfe9001.put("FILE_SIZE", file.getSize());
					imfe9001.put("FILE_EXT", fileService.getFileExt());
					arrImfe9001.add(imfe9001);
					
					//Progress 상태에 진행 현황 UPDATE
					loginSessionInfoFactory.getObject().pushJob("FILEUPLOAD",String.valueOf(++i) , originalFileName);
				}	
				TableObject imfe9001_key = new TableObject();
				imfe9001_key.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID(),true);
				dbmsDao.insertTable("IMFE9001", "FILE_ID",imfe9001_key,  arrImfe9001);
				
				//Progress 완료
				loginSessionInfoFactory.getObject().completeProgress("FILEUPLOAD");
			}catch(Exception e){
				loginSessionInfoFactory.getObject().pushJob("FILEUPLOAD", e.getClass().getCanonicalName(), e.getMessage());   
				throw e;
			}
			
		}
		else{
			logger.info("업로드 할 증빙 자료가 없습니다. ");
		} 
	}
	public List<LinkedHashMap<String, Object>> getAttachFileInfo(String fileId) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)", loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_FILE_ID)", fileId);
		List<LinkedHashMap<String, Object>> fileList = this.sqlHouseExecuteDao.selectHashMap(180, map);
		return fileList;
	}
	
}
