package com.iplanbiz.imanager.service;

import java.util.HashMap;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class KpiAttachFileStateService {
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired   
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	//지표별 증빙자료 현황 리스트
	public JSONArray selectKpiAttachFile(String evagbn, String yyyy) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EVA_GBN)"					, evagbn);
		map.put("@(S_YYYY)"						, yyyy);
		return sqlHouseExcuteDao.selectJSONArray(184,map);
	}
	//부서평가 년도 (실적 : A, 목표 : B 구분함)	
	public JSONArray selectYyyycbo(String evagbn, String gubun) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EVA_GBN)"					, evagbn);
		map.put("@(S_GUBUN)"					, gubun);
		return sqlHouseExcuteDao.selectJSONArray(182,map);
	}
	//지표별 증빙자료 현황 전체
	public JSONArray selectSumTotalKpi(String evagbn, String yyyy) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EVA_GBN)"					, evagbn);
		map.put("@(S_YYYY)"						, yyyy);
		return sqlHouseExcuteDao.selectJSONArray(183,map);
	}
}
