package com.iplanbiz.imanager.service;

import java.util.HashMap;

import javax.annotation.Resource;
 
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.io.dbms.TableObject;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO; 
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class kpiSuccessManagementService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExcuteDao;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	public JSONArray selectkpiSuccessManagement() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>(); 
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		
		return sqlHouseExcuteDao.selectJSONArray(37, map);
	}
	public JSONArray checkCsfId100() throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		return sqlHouseExcuteDao.selectJSONArray(237, map);
	}
	public int insertkpiSuccessManagement(String csfid,String csfcd, String straid, String substraid, String csfnm, String sortorder, String startyyyy, String endyyyy, String useyn) throws Exception{
		int result = 0;
		
		TableObject imfe0006 = new TableObject();
		imfe0006.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID(),true);
		imfe0006.put("CSF_CD" , csfcd);
		imfe0006.put("STRA_ID", straid);
		imfe0006.put("SUBSTRA_ID", substraid);
		imfe0006.put("CSF_NM", csfnm);
		imfe0006.put("SORT_ORDER", sortorder);
		imfe0006.put("START_YYYY", startyyyy);
		imfe0006.put("END_YYYY", endyyyy);
		imfe0006.put("USE_YN", useyn);
		
		imfe0006.putCurrentDate("UPDATE_DAT");
		
		imfe0006.put("UPDATE_EMP", loginSessionInfoFactory.getObject().getUserId());
 
		try{
			if(csfid.equals("")){
				imfe0006.putCurrentDate("INSERT_DAT");
				imfe0006.put("INSERT_EMP", loginSessionInfoFactory.getObject().getUserId());
				this.dbmsDao.insertTable("IMFE0006","CSF_ID",imfe0006.getKeyMap(),imfe0006);
			}else{
				imfe0006.put("CSF_ID", csfid, true);
				//수정
				this.dbmsDao.updateTable("IMFE0006", imfe0006.getKeyMap(),imfe0006);
				result = 2;
			}
		}catch(Exception e){
			result = 0; 
			throw e;
		}
		return result;
	}
	public JSONArray checkSuccessManagement(String straid, String substraid, String csfid) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"				, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_STRA_ID)"				, straid);
		map.put("@(S_SUBSTRA_ID)"				, substraid);
		map.put("@(S_CSF_ID)"				, csfid);
		
		return sqlHouseExcuteDao.selectJSONArray(236,map);
	}
	public void removekpiSuccessManagement(String csfid) throws Exception{
  
		TableObject imfe0006 = new TableObject();
		imfe0006.put("ACCT_ID", loginSessionInfoFactory.getObject().getAcctID(), true);		
		imfe0006.put("CSF_ID", csfid, true); 
		if(dbmsDao.countTable("IMFE0009", imfe0006)>0) throw new RuntimeException("해당 코드에 매핑된 지표가 있어 삭제 할 수 없습니다.");
		
		dbmsDao.deleteTable("IMFE0006", imfe0006); 
		 
	}

}
