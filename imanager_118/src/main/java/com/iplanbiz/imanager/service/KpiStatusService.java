package com.iplanbiz.imanager.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iplanbiz.core.io.dbms.DBMSDAO;
import com.iplanbiz.core.session.LoginSessionInfo;
import com.iplanbiz.imanager.dao.BasicDAO;
import com.iplanbiz.imanager.dao.SqlHouseExecuteDAO;

@Service
@Transactional(readOnly=true)
public class KpiStatusService {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	@Resource(name="loginSessionInfoFactory")
	ObjectFactory<LoginSessionInfo> loginSessionInfoFactory;
	
	@Autowired
	BasicDAO basicDao;
	
	@Autowired
	DBMSDAO dbmsDao;
	
	@Autowired
	SqlHouseExecuteDAO sqlHouseExecuteDao;
	
	public JSONArray kpiEvalList_ExcelDown(String pEvaGbn, String evalG, String yyyymm) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EVA_GBN)"					, pEvaGbn);
		map.put("@(S_EVAL_G)"					, evalG);
		map.put("@(S_YYYYMM)"					, yyyymm);
		
		return sqlHouseExecuteDao.excelJSONArray(293, map);
		
	}
	
		public JSONArray kpiResultList_ExcelDown(String pEvaGbn, String evalG, String yyyymm) throws Exception{
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("@(S_ACCT_ID)"					, loginSessionInfoFactory.getObject().getAcctID());
		map.put("@(S_EVA_GBN)"					, pEvaGbn);
		map.put("@(S_EVAL_G)"					, evalG);
		map.put("@(S_YYYYMM)"					, yyyymm);
		
		return sqlHouseExecuteDao.excelJSONArray(290, map);
		
	}
	
}
