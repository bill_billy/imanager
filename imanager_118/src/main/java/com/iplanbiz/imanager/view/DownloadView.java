/**
 * Copyright(c) 2012 IPALBNZ All Rights Reserved
 */
package com.iplanbiz.imanager.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;

import com.iplanbiz.imanager.config.WebConfig;

/**
 * @author : woon sik Shin(sky3473@iplanbiz.co.kr or gmail.com)
 * @date   : 2012. 7. 5.
 * @desc   : 
 */
public class DownloadView extends AbstractView {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	public void Download(){                 
		setContentType("application/download; utf-8");             
	}
	
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		String filePath = "";
		String fileOrgName = "";
		String fileSaveName = "";
		
		if("temp".equals(model.get("fileType"))){
			filePath = WebConfig.getProperty("system.upload.SaveDir");
//			filePath = WebConfig.getDefultTempDir();
			fileSaveName = (String) model.get("downloadFile");
			fileOrgName = fileSaveName.split("_")[1];
		}else{
			@SuppressWarnings("unchecked")
			Map<String, Object> fileInfo = (Map<String, Object>) model.get("downloadFile");
			
//			
			filePath = String.valueOf(fileInfo.get("FILE_PATH"));
			fileOrgName = String.valueOf(fileInfo.get("FILE_ORG_NAME"));
			fileSaveName = String.valueOf(fileInfo.get("FILE_SAVE_NAME")) + "." + String.valueOf(fileInfo.get("FILE_EXT"));
//			
		}
		

		File file = new File(filePath + fileSaveName); 
				
		response.setContentType(getContentType());        
		response.setContentLength((int)file.length());                 
		
		String userAgent = request.getHeader("User-Agent");
		boolean ie = userAgent.indexOf("MSIE") > -1;
		boolean chrom = userAgent.indexOf("Chrome") > -1;
		String fileName = "";                 
		
		logger.debug("--->origin filename : {}",fileName);
		logger.debug("--->URLEncoder.encode(fileName, utf-8) : {}",URLEncoder.encode(fileName, "utf-8"));
		logger.debug("--->new String(fileName.getBytes(MS949) : {}" ,new String(fileName.getBytes("MS949")));
				
		logger.debug("--->new String(fileName.getBytes(utf-8)) : {} " , new String(fileName.getBytes("utf-8")));
		logger.debug("--->!new String(fileName.getBytes(KSC5601), 8859_1) : {} " , new String(fileName.getBytes("KSC5601"), "8859_1"));
		fileName = new String(fileOrgName.getBytes("KSC5601"), "8859_1"); 
		
		
		
		logger.debug("===>User-Agent : {}", userAgent.toString());
		logger.info("===>>>>file.getPath() : {}", filePath);
		logger.info("===>>>>file.getName() : {}", fileOrgName);
		
		response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");                 
		response.setHeader("Content-Transfer-Encoding", "binary");                 
		
		OutputStream out = response.getOutputStream();                 
		FileInputStream fis = null;                 
		
		try {                         
			fis = new FileInputStream(file);                         
			FileCopyUtils.copy(fis, out);
			logger.info("===>>>>FileDownload Complete : {}", fileName);
		}catch(Exception e){                         
			logger.error("===>>>>FileDownload Error : {}", e.toString());
			}finally{ 
				if(fis != null){try{fis.close();}catch(Exception e){}
			}                     
		}                 
		out.flush();             
	}

}
